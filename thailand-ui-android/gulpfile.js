var gulp = require('gulp'),
    gp_concat = require('gulp-concat'),
    gp_rename = require('gulp-rename'),
    gp_uglify = require('gulp-uglify');
    var obfuscate = require('gulp-obfuscate');
    var annotate = require('gulp-ng-annotate');
    var packer = require('gulp-packer')
  , streamify = require('gulp-streamify')


gulp.task('js-obf', function(){
                var source = [
				
				"assets/www/modules/login/controllers/*.js",
				"assets/www/modules/eApp/controllers/EappNavigationController.js",
				"assets/www/modules/eApp/controllers/BeneficiaryController.js",
				"assets/www/modules/eApp/controllers/DocumentUploadController.js",
				"assets/www/modules/eApp/controllers/EappProductListingController.js",
				"assets/www/modules/eApp/controllers/GLI_QuestionnaireInsuredController.js",
				"assets/www/modules/eApp/controllers/GLI_QuestionnaireProposerController.js",
				"assets/www/modules/eApp/controllers/GLI_QuestionnaireAdditionalInsuredOneController.js",
				"assets/www/modules/eApp/controllers/GLI_QuestionnaireAdditionalInsuredTwoController.js",
				"assets/www/modules/eApp/controllers/GLI_QuestionnaireAdditionalInsuredThreeController.js",
				"assets/www/modules/eApp/controllers/GLI_QuestionnaireAdditionalInsuredFourController.js",
				"assets/www/modules/eApp/controllers/HomeController.js",
				"assets/www/modules/eApp/controllers/InsuredController.js",
				"assets/www/modules/eApp/controllers/PayerController.js",
				"assets/www/modules/eApp/controllers/ProductController.js",
				"assets/www/modules/eApp/controllers/GLI_ProductController.js",
				"assets/www/modules/eApp/controllers/EappProductListingController.js",
				"assets/www/modules/eApp/controllers/ProposerController.js",
				"assets/www/modules/eApp/controllers/GLI_ProposerController.js",
				"assets/www/modules/eApp/controllers/SummaryController.js",
				"assets/www/modules/eApp/controllers/GLI_SummaryAgentController.js",
				"assets/www/modules/eApp/controllers/GLI_SummaryQuestionaireController.js",
				"assets/www/modules/eApp/controllers/GLI_SummarySPAJController.js",
				"assets/www/modules/eApp/controllers/GLI_SummaryDeclarationController.js",
				"assets/www/modules/eApp/controllers/GLI_EappNavigationController.js",
				"assets/www/modules/eApp/controllers/GLI_AdditionalInsuredOneController.js",
				"assets/www/modules/eApp/controllers/GLI_AdditionalInsuredTwoController.js",
				"assets/www/modules/eApp/controllers/GLI_AdditionalInsuredThreeController.js",
				"assets/www/modules/eApp/controllers/GLI_AdditionalInsuredFourController.js",
				"assets/www/modules/eApp/controllers/GLI_InsuredController.js",
				"assets/www/modules/eApp/controllers/GLI_BeneficiaryController.js",
				"assets/www/modules/eApp/controllers/GLI_FinancialDetailsController.js",
				"assets/www/modules/eApp/controllers/PaymentController.js",
				"assets/www/modules/eApp/controllers/FeedbackIssueController.js",
				"assets/www/modules/eApp/services/EappVariables.js",
				"assets/www/modules/eApp/services/GLI_EappVariables.js",
				"assets/www/modules/eApp/services/EappService.js",
				"assets/www/modules/illustrator/controllers/ProductsController.js",
				"assets/www/modules/illustrator/controllers/GLI_ProductsController.js",
				"assets/www/modules/illustrator/controllers/PersonalDetailsController.js",
				"assets/www/modules/illustrator/controllers/GLI_PersonalDetailsController.js",
				"assets/www/modules/illustrator/controllers/ProductDetailsController.js",
				"assets/www/modules/illustrator/controllers/GLI_ProductDetailsController.js",
				"assets/www/modules/illustrator/controllers/IllustrationDetailsController.js",
				"assets/www/modules/illustrator/services/IllustratorService.js",
				"assets/www/modules/illustrator/services/GLI_IllustratorService.js",
				"assets/www/modules/illustrator/services/IllustratorVariables.js",
				"assets/www/modules/illustrator/services/GLI_IllustratorVariables.js",
				"assets/www/modules/illustrator/controllers/NavigationTabController.js",
				"assets/www/modules/illustrator/controllers/GLI_NavigationTabController.js",
				"assets/www/modules/lms/controllers/*.js",
				"assets/www/modules/lms/services/*.js"
				
				
				]
    return gulp.src(source)
        .pipe(gp_concat('concat.js'))
        .pipe(gulp.dest('www/dist'))
        .pipe(gp_rename('uglify.js'))
        .pipe(gp_uglify('uglify.js'))
        .pipe(annotate())
        .pipe(streamify(packer({base62: true})))
        .pipe(gulp.dest('assets/www/dist'));
});

gulp.task('default', ['js-obf']);
