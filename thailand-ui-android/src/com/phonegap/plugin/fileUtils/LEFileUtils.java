/**
 *
 * Copyright 2014, Cognizant
 *
 * @version       : 2.2.0.0, Apr 25, 2016
 */
package com.phonegap.plugin.fileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.lang.reflect.Method;

import org.apache.cordova.CordovaPlugin;
//import org.apache.cordova.api.PluginResult;
import org.apache.cordova.CallbackContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Base64;
import android.util.Log;


public class LEFileUtils extends CordovaPlugin {
	
	Context con;
	String DB_PATH="";
	String DB_NAME = "";
	/**
     * Executes the request and returns PluginResult.
     *
     * @param action        The action to execute.
     * @param args          JSONArry of arguments for the plugin.
     * @param //callbackId    The callback id used when calling back into JavaScript.
     * @return              A PluginResult object with a status and message.
     */
    //public PluginResult execute(String action, JSONArray args, String callbackId) {
	public boolean execute(String action, JSONArray args, 
			CallbackContext callbackContext) throws JSONException {
        try {
			con = this.cordova.getActivity().getApplicationContext();
            if (action.equals("copyFileFromPackage"))
            {

            	/*
            	JSONObject obj = args.getJSONObject(0);
            	String fileName = obj.getString("fileName");
//            	String filePath = obj.getString("filePath");
            	String filePath = "";
            	copyFileFromPackage(fileName, filePath);*/
            	String filePath = "";
            	// Take an array of files and copy all these files. Once they are done, then call the callback.
            	for( int i=0; i< args.length(); i++){
            		JSONObject obj = args.getJSONObject(i);
            		String fileName = obj.getString("fileName");
            		boolean isForceCopyRequired = false;
            		if (obj.has("filePath")) {
            		    filePath = obj.getString("filePath");
            		} else if (obj.has("sourcePath")) {
            		    filePath = obj.getString("sourcePath");
            		}
            		if (obj.has("isForceCopyRequired")) {
            			isForceCopyRequired = obj.getBoolean("isForceCopyRequired");
            		}
            		try {
                        copyFileFromPackage(fileName, filePath, isForceCopyRequired);
            		} catch (FileNotFoundException fe) {
            		    Log.i("LEFileUtils", "FileNotFoundException occured while copying file." + fe.getMessage());
            		} catch (IOException ie) {
            		    Log.i("LEFileUtils", "IOException occured while copying file. " + ie.getMessage());
            		} catch (Exception e) {
            		    Log.i("LEFileUtils", "Exception occured while copying file. " + e.getMessage());
            		}
            	}

            	//return new PluginResult(PluginResult.Status.OK, "File copied successfully");   
				callbackContext.success("File copied successfully");
				return true;
            }
            else if (action.equals("getApplicationPath")) {
                    String appPath = con.getApplicationInfo().dataDir;
                    //return new PluginResult(PluginResult.Status.OK, appPath);
                    callbackContext.success(appPath);
                    return true;
                } else if (action.equals("isFileExists")) {
                    String fileName = args.getString(0);

                    if (fileName.contains(".db")) {

                        File dbfile = this.cordova.getActivity().getDatabasePath(fileName);

                        if (dbfile.exists()) {
                            //return new PluginResult(PluginResult.Status.OK, appPath);
                            callbackContext.success("File Exists");
                        }else{
							 callbackContext.error("File Not Found");
						}

                    } else {
                        File configFile = new File(con.getApplicationInfo().dataDir+"/"+fileName);
                        
                        if (configFile.exists()) {
                            //return new PluginResult(PluginResult.Status.OK, appPath);
                            callbackContext.success("File Exists");
                        }else{
							 callbackContext.error("File Not Found");
						}

                    }


                    return true;
                } else if (action.equals("deleteFile")) {
                    JSONObject obj = args.getJSONObject(0);
                    String fileName = obj.getString("fileName");
                    boolean isErrorIfFileNotExists = true;
                    if (obj.has("isErrorIfFileNotExists")) {
                        isErrorIfFileNotExists = obj.getBoolean("isErrorIfFileNotExists");
                    }
                    String filePath = obj.getString("filePath");
					if(null == filePath){
						filePath ="";
					}
					String deleteStatus = deleteFile(fileName, filePath, isErrorIfFileNotExists);
                    if (deleteStatus.equals("success")) {
                        //return new PluginResult(PluginResult.Status.OK, "File deleted successfully");
                        callbackContext.success("File deleted successfully");
                        return true;
                    } else {
                        //return new PluginResult(PluginResult.Status.ERROR, deleteStatus);
                        callbackContext.error(deleteStatus);
                    }
                } else if (action.equals("renameFile")) {
                    JSONObject obj = args.getJSONObject(0);
                    String oldFileName = obj.getString("oldFileName");
                    String newFileName = obj.getString("newFileName");
                    boolean isErrorIfSourceFileNotExists = true;
                    if (obj.has("isErrorIfSourceFileNotExists")) {
                        isErrorIfSourceFileNotExists = obj.getBoolean("isErrorIfSourceFileNotExists");
                    }
                    String renameStatus = renameFile(oldFileName, newFileName, isErrorIfSourceFileNotExists);
                    if (renameStatus.equals("success")) {
                        //return new PluginResult(PluginResult.Status.OK, "File deleted successfully");
                        callbackContext.success("File renamed successfully");
                        return true;
                    } else {
                        //return new PluginResult(PluginResult.Status.ERROR, deleteStatus);
                        callbackContext.error(renameStatus);
                    }
                } else if (action.equals("copyFile")) {
                JSONObject obj = args.getJSONObject(0);
                String fileName = obj.getString("fileName");
                String srcPath = obj.getString("srcPath");
                String newName = obj.getString("newName");
                String folderName = obj.getString("folderName");

                String copyStatus = copyFile(fileName, srcPath, newName, folderName);
                if (copyStatus.equals("success")){
                    callbackContext.success("File copied successfully");
                    return  true;
                } else {
                    callbackContext.error(copyStatus);
                }
            } else if (action.equals("readFileAsBase64")) {
                JSONObject obj = args.getJSONObject(0);
                String fileName = obj.getString("fileName");
                String destFolder = obj.getString("destFolder");

                String encodeStatus = readFileAsBase64(fileName, destFolder);

                if (encodeStatus.equals("Error while encoding")) {
                    callbackContext.error(encodeStatus);
                } else {
                    callbackContext.success(encodeStatus);
                    return true;
                }
            } else if (action.equals("writeBase64AsFile")) {
                JSONObject obj = args.getJSONObject(0);
                String fileName = obj.getString("fileName");
                String destFolder = obj.getString("destFolder");
                String base64String = obj.getString("base64String");

                String decodeStatus = writeBase64AsFile(fileName, destFolder, base64String);

                if (decodeStatus.equals("success")){
                    callbackContext.success("Decoded successfully");
                    return true;
                } else {
                    callbackContext.error(decodeStatus);
                }
            } else {
                    //return new PluginResult(PluginResult.Status.INVALID_ACTION);
                    callbackContext.error("INVALID ACTION");
                }
            }
        catch (JSONException e) {
            Log.i("LEFileUtils", "JSONException occured while LEFileUtils operation. " + e.getMessage());
			callbackContext.error(e.getMessage());
        } catch (Exception e) {
            Log.i("LEFileUtils", "Exception occured while LEFileUtils operation. " + e.getMessage());
            callbackContext.error(e.getMessage());
        }
		return false;
    }
	private String renameFile(String oldFileName, String newFileName, boolean isErrorIfSourceFileNotExists) {
	    DB_NAME = oldFileName;
	    int lastIndex = oldFileName.lastIndexOf(".");
        String extenstion = oldFileName.substring(lastIndex + 1);
        Log.i("LEFileUtils",oldFileName + " being renamed");
        if(extenstion.equalsIgnoreCase("db"))
        {
            DB_PATH = con.getApplicationInfo().dataDir+"/databases/";               
        }
        else
        {
            DB_PATH = con.getApplicationInfo().dataDir +"/";
        }
        File oldFile = new File(DB_PATH + DB_NAME);
        File newFile = new File(DB_PATH + newFileName);
        if((oldFile.exists()+"").equalsIgnoreCase("false")){
            Log.i("LEFileUtils",DB_NAME + " does not exist.");
            if(isErrorIfSourceFileNotExists) {
            	return "File does not exist "  + DB_NAME;            	
            } else {
            	return "success";
            }

        } else{
            if(oldFile.renameTo(newFile)){
                Log.i("LEFileUtils",oldFileName + " successfully renamed to "+ newFileName +".");
                return "success";
            }else{
                Log.i("LEFileUtils",oldFileName + " delete failed.");
                return "File rename failed for file:" + oldFileName;
            }
       }
	}
    private void copyFileFromPackage(String fileName, String filePath, boolean isForceCopyRequired) throws IOException {
    	
    	DB_NAME = fileName;
    	
    	int lastIndex = fileName.lastIndexOf(".");
    	String extenstion = fileName.substring(lastIndex + 1);
    	Log.i("LEFileUtils",fileName + " being copied");
    	if(extenstion.equalsIgnoreCase("db"))
    	{
        	DB_PATH = con.getApplicationInfo().dataDir +"/databases/";
    	}
    	else
    	{
        	DB_PATH = con.getApplicationInfo().dataDir +"/";
    	}
    	// Check if the folder exists . If the folder does not exist, then create it.
    	File dbFile = new File(DB_PATH);   
        if (!dbFile.exists()) {
            dbFile.mkdir();
        }
	
		 dbFile = new File(DB_PATH + DB_NAME);
		 Log.i("LEFileUtils", "Alredy exists?" + dbFile.exists());
         // If the file already exists in the target path, do nothing.
		 if(isForceCopyRequired || (dbFile.exists()+"").equalsIgnoreCase("false")){
			// Open your local db as the input stream
			InputStream myInput = con.getAssets().open(filePath + DB_NAME);
			// Path to the just created empty db
			String outFileName = DB_PATH + DB_NAME;
			// Open the empty db as the output stream
			OutputStream myOutput = new FileOutputStream(outFileName);
			// transfer bytes from the inputfile to the outputfile
			byte[] buffer = new byte[1024];
			int length;
			while ((length = myInput.read(buffer)) > 0) {
				myOutput.write(buffer, 0, length);
			}
			// Close the streams
			myOutput.flush();
			myOutput.close();
			myInput.close();
			Log.i("", fileName + " file copied successfully");
		 } else {
			 Log.i("LEFileUtils", fileName + " file already exists");
		 }
	}	
    
    private String deleteFile(String fileName, String filePath, boolean isErrorIfFileNotExists) {
		// TODO Auto-generated method stub
    	DB_NAME = fileName;
    	
    	int lastIndex = fileName.lastIndexOf(".");
    	String extenstion = fileName.substring(lastIndex + 1);
    	Log.i("LEFileUtils", fileName + " being deleted");
    	if(extenstion.equalsIgnoreCase("db"))
    	{
        	DB_PATH = con.getApplicationInfo().dataDir+"/databases/";    			
    	}
    	else
    	{
            if(!filePath.equals("")){
				DB_PATH = con.getApplicationInfo().dataDir +"/"+filePath+"/";
			}else{
				DB_PATH = con.getApplicationInfo().dataDir +"/";
			}
    	}
    	    	
//    	DB_PATH = con.getApplicationInfo().dataDir+"/databases/";
    			
		 File dbFile = new File(DB_PATH + DB_NAME);

		 if((dbFile.exists()+"").equalsIgnoreCase("false")){
			Log.i("LEFileUtils",fileName + " does not exist.");
			if (isErrorIfFileNotExists) {
				return "File does not exist "  + fileName;
			} else {
				return "success";
			}

		 }
		 else{
			 if(dbFile.delete()){
                Log.i("LEFileUtils",fileName + " successfully deleted.");
				 return "success";
			 }else{
                Log.i("LEFileUtils",fileName + " delete failed.");
				 return "File delete failed for file:" + fileName;
			 }
		 }
		
	}
	private String copyFile (String fileName, String srcPath, String newName, String destFolder) {
        try {
            String path = con.getApplicationInfo().dataDir + '/' + destFolder + '/';

            File myFile = new File(path);
            if (!myFile.exists()) {
                myFile.mkdir();
            }

            File file = new File(srcPath, fileName);

            InputStream in = new FileInputStream(file);
            OutputStream out = new FileOutputStream(path + newName);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0){
                out.write(buf, 0, len);
            }
            in.close();
            out.close();

            Log.i("", " file copied successfully");
            return "success";
        } catch (IOException e){
            return "File copy failed";
        } catch (Exception e) {
            return "File copy failed";
        }
    }

    private String readFileAsBase64 (String fileName, String destFolder) {
        String path = con.getApplicationInfo().dataDir + "/" + destFolder + "/" + fileName;
        String base64String = null;

        try {
            File file = new File(path);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            FileInputStream fis = new FileInputStream(file);
            byte[] buf = new byte[1024];

            for (int readNum; (readNum = fis.read(buf)) != -1;) {
                bos.write(buf, 0, readNum);
                System.out.println("read " + readNum + " bytes,");
            }

            byte[] bytes = bos.toByteArray();

            base64String = Base64.encodeToString(bytes,Base64.DEFAULT);
            base64String = base64String.replace("\n", "").replace("\r", "");
            bos.close();

            return base64String;
        } catch (IOException e) {
            e.printStackTrace();
            return "Error while encoding";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "Error while encoding";
        }
    }

    private String writeBase64AsFile (String fileName, String destFolder, String base64String) {
        String path = con.getApplicationInfo().dataDir + "/" + destFolder + '/';

        try {
            File myFile = new File(path);
            if (!myFile.exists()) {
                myFile.mkdir();
            }

            FileOutputStream os= new FileOutputStream(path + fileName);
            byte[] imageAsBytes = Base64.decode(base64String, 0);
            os.write(imageAsBytes);

            os.close();

            return "success";
        } catch (IOException e) {
            e.printStackTrace();
            return "Error while decoding";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "Error while decoding";
        }
    }
    
}
