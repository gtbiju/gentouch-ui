/*
 * PhoneGap is available under *either* the terms of the modified BSD license *or* the
 * MIT License (2008). See http://opensource.org/licenses/alphabetical for full text.
 *
 * Copyright (c) 2005-2010, Nitobi Software Inc.
 * Copyright (c) 2010, IBM Corporation
 *
 * version 2.1.0.0
 *
 */
package com.phonegap.plugin.encryption;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.KeyStore.Entry;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import net.sqlcipher.database.SQLiteDatabase;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.SharedPreferences;
import android.os.Build;
import android.content.Context;
import android.util.Base64;
import android.util.Log;
import com.google.common.base.Charsets;

public class LEEncryption extends CordovaPlugin {
	private java.security.KeyStore mKeyStore;
	private static final String LE_ENCRYPTION_KEY = "com.cognizant.appName.encryption.key";
	private static final String KEY_EXISTS = "key exists";
	private static final String KEY_NOT_EXISTS = "key not exists";
	private static final String KEY_ENTRY_SUCCESS = "key inserted successfully";
	private static final String KEY_ENTRY_FAILURE = "faild to insert key";
	private static final String SUCCESS = "SUCCESS";
	
	private Context con;
	

	/**
	 * Executes the request and returns PluginResult.
	 * 
	 * @param action
	 *            The action to execute.
	 * 
	 * @param args
	 *            JSONArry of arguments for the plugin.
	 * 
	 * @param cbc
	 *            Callback context from Cordova API (not used here)
	 * 
	 */
	@Override
	public boolean execute(String action, JSONArray args, CallbackContext cbc) {
		String result = "";
		try {
			init();
			con = this.cordova.getActivity().getApplicationContext();
			if (action.equals("checkKeyAvailability")) {
				result = checkKeyAvailability(args.getJSONObject(0).getString("appName"));
				if (KEY_EXISTS.equals(result) || KEY_NOT_EXISTS.equals(result)) {
					cbc.success(result);
				} else {
					cbc.error(result);
				}

			} else if (action.equals("insertKey")) {
				result = insertKey(args.getJSONObject(0).getString("key"),args.getJSONObject(0).getString("appName"));
				if (SUCCESS.equals(result)) {
					cbc.success(KEY_ENTRY_SUCCESS);
				} else {
					cbc.error(KEY_ENTRY_FAILURE);
				}
			} else if (action.equals("fetchKey")) {
				result = fetchKey(args.getJSONObject(0).getString("appName"));
				if (result == null) {
					cbc.error("Failed to get encryption Key");
				} else {
					cbc.success(result);
				}

			} else if (action.equals("encryptDB")) {
                JSONArray filesArray = args.getJSONObject(0).getJSONArray("fileArray");
				for (int i = 0; i < filesArray.length(); i++) {
					JSONObject obj = filesArray.getJSONObject(i);
					String fileName = obj.getString("fileName");
					File dbFile = new File(con.getApplicationInfo().dataDir
							+ "/databases/" + fileName);
					encryptDB(dbFile,args.getJSONObject(0).getString("appName"));
				}
				cbc.success("DBs encrypted successfully");

			}
			return true;
		} catch (KeyStoreException e) {
			Log.i("LEEncryption", "KeyStoreException ", e);
			return false;
		} catch (NoSuchAlgorithmException e) {
			Log.i("LEEncryption", "NoSuchAlgorithmException", e);
			return false;
		} catch (CertificateException e) {
			Log.i("LEEncryption", "CertificateException", e);
			return false;
		} catch (IOException e) {
			Log.i("LEEncryption", "IOException", e);
			return false;
		} catch (JSONException e) {
			Log.i("LEEncryption", "JSON Exception", e);
			return false;
		}
	}

	private void init() throws KeyStoreException, NoSuchAlgorithmException,
			CertificateException, IOException {
        if(android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
            mKeyStore = java.security.KeyStore.getInstance("AndroidKeyStore");
            mKeyStore.load(null, null);
        }

	}

	private void encryptDB(File dbFile, String appName) {
		SQLiteDatabase.loadLibs(this.cordova.getActivity());
		String password = fetchKey(appName);
		SQLiteDatabase mydb = null;
	
		String bytestr= "";
		try {
			bytestr = com.google.common.io.Files.toString(dbFile,Charsets.UTF_8);
		} catch (IOException e) {
			Log.i("LEEncryption", "IOException : " + e.getMessage(), e);
		}
		if (bytestr.contains("SQLite format")) {			
			mydb = SQLiteDatabase.openOrCreateDatabase(dbFile, "", null);
			mydb.rawExecSQL("ATTACH DATABASE '"
					+ this.cordova.getActivity().getDatabasePath("newdb.db")
					+ "' AS 'encryptedDB' KEY '" + password + "'");
			mydb.rawExecSQL("SELECT sqlcipher_export('encryptedDB')");
			mydb.rawExecSQL("DETACH DATABASE encryptedDB;");
			dbFile.delete();
			File newfile = this.cordova.getActivity().getDatabasePath(
					"newdb.db");
			newfile.renameTo(this.cordova.getActivity().getDatabasePath(
					dbFile.getName()));
			mydb.close();
		}
	}

	private String checkKeyAvailability(String appName) {
		Entry actualEntry = null;
        String sharedEntry = "";
		try {
            if(android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
                actualEntry = mKeyStore.getEntry(LE_ENCRYPTION_KEY.replace("appName", appName), null);
            }else{
                SharedPreferences settings = con.getSharedPreferences(LE_ENCRYPTION_KEY.replace("appName", appName),con.MODE_PRIVATE);
                sharedEntry =  settings.getString("encryptionKey","");
            }
		} catch (NoSuchAlgorithmException e) {
			Log.i("LEEncryption",
					"NoSuchAlgorithmException occured while checkKeyAvailability."
							+ e.getMessage(), e);
			return "NoSuchAlgorithmException : " + e.getMessage();
		} catch (UnrecoverableEntryException e) {
			Log.i("LEEncryption",
					"UnrecoverableEntryException occured while checkKeyAvailability."
							+ e.getMessage(), e);
			return "UnrecoverableEntryException : " + e.getMessage();
		} catch (KeyStoreException e) {
			Log.i("LEEncryption",
					"KeyStoreException occured while checkKeyAvailability."
							+ e.getMessage(), e);
			return "KeyStoreException : " + e.getMessage();
		}
		if (actualEntry != null || ! sharedEntry.equals("")) {
			return KEY_EXISTS;
		} else {
			return KEY_NOT_EXISTS;
		}
	}
	


	private String insertKey(String keyString, String appName) {
		try {
            if(android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
                final CertificateFactory f = CertificateFactory
                        .getInstance("X.509");

                byte[] certBase = Base64.decode(keyString, 0);

                Certificate certificate = f.generateCertificate(new ByteArrayInputStream(certBase));

                mKeyStore.setCertificateEntry(LE_ENCRYPTION_KEY.replace("appName", appName), certificate);
            }else{
                SharedPreferences settings = con.getSharedPreferences(LE_ENCRYPTION_KEY.replace("appName", appName), con.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("encryptionKey", keyString);
                editor.commit();

            }


		} catch (CertificateException e) {
			Log.i("LEEncryption",
					"CertificateException occured while insertKey."
							+ e.getMessage(), e);
			return "CertificateException : " + e.getMessage();
		} 
		catch (KeyStoreException e) {
			Log.i("LEEncryption", "KeyStoreException occured while insertKey."
					+ e.getMessage(), e);
			return "KeyStoreException : " + e.getMessage();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		
		return SUCCESS;
	}
	



	public String fetchKey(String appName) {
		String key = null;
		try {
			if(android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
                Certificate cert = mKeyStore.getCertificate(LE_ENCRYPTION_KEY.replace("appName", appName));
                byte[] certByte = cert.getEncoded();
                key = Base64.encodeToString(certByte, 0);
            }else{
                SharedPreferences settings = con.getSharedPreferences(LE_ENCRYPTION_KEY.replace("appName", appName), con.MODE_PRIVATE);
                key = settings.getString("encryptionKey","");
            }
		} catch (KeyStoreException e) {
			Log.i("LEEncryption",
					"KeyStoreException occured while fetching Key."
							+ e.getMessage(), e);
			return null;
		} catch (CertificateEncodingException e) {
			Log.i("LEEncryption",
					"CertificateEncodingException occured while fetching Key."
							+ e.getMessage(), e);
			return null;
		} catch (Exception e) {
            Log.i("LEEncryption",
                    "Exception occured while fetching Key."
                            + e.getMessage(), e);
            return null;
        }

		return key;

	}

}
