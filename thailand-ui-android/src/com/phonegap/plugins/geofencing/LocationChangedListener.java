package com.phonegap.plugins.geofencing;


import android.location.Location;

/**
 * Listener to receive location updates.
 */
public interface LocationChangedListener {

    void onLocationChanged(Location location);
}
