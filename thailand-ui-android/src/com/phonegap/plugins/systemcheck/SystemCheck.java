package com.phonegap.plugins.systemcheck;

import java.io.File;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;

import com.cognizant.lifeengage.fgli.R;

/**
 * @System Check plugin
 */

public class SystemCheck extends CordovaPlugin {
	Context context;

	private Context getApplicationContext() {// Returns the application Context
		return this.cordova.getActivity().getApplicationContext();
	}

	private boolean isFirstTime() { // Check if the app is running first time
		context = getApplicationContext();
		final SharedPreferences prefs = context.getSharedPreferences(
				context.getPackageName(), Context.MODE_PRIVATE);

		String pin = prefs.getString(
				context.getResources().getString(R.string.pin_key),
				new String()); // Taking saved PIN from Shared Preferences
		if (pin.equals("")) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean execute(String action, JSONArray data,
			CallbackContext callbackContext) {
		boolean isDeviceRooted = false;
		if (action.equals("RootCheck")) {

			if (isRunningOnEmulator()) {// If the app running in emulator, allow
										// the app to run
				return false;
			} else {

				isDeviceRooted = isRooted();
				// Log.v("RootCheck", "########################" + isRooted());
				JSONObject result = new JSONObject();
				try {
					result.put("rootedStatus", isDeviceRooted);
					if (isDeviceRooted)
						result.put("message", "rootedAndroid");// key for
																// Android alert
																// message in
																// resource
					callbackContext.success(result);
				} catch (JSONException e) {
					e.printStackTrace();
					callbackContext.error(e.getMessage());
				}
			}

		} else if (action.equals("PinCheck")) {
			JSONObject obj = null;
			boolean firstTime = false;
			try {
				obj = data.getJSONObject(0);
				firstTime = obj.has("firstTime") ? obj.getBoolean("firstTime")
						: false;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (firstTime) {

				if (isFirstTime()) {

					callbackContext.success("firstTime");
				} else {

					callbackContext.error("notFirstTime");
				}

			} else {

				String pinNumber = "";
				try {
					pinNumber = obj.getString("pinNumber"); // Entered PIN
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (!isFirstTime()) {
					context = getApplicationContext();
					final SharedPreferences prefs = context
							.getSharedPreferences(context.getPackageName(),
									Context.MODE_PRIVATE);
					String PIN = prefs.getString(context.getResources()
							.getString(R.string.pin_key), new String()); // Taking
																			// saved
																			// PIN
																			// from
																			// Shared
																			// Preferences

					if (pinNumber.equals(PIN)) {
						callbackContext.success("success");
					} else {

						callbackContext.error("error");
					}

				} else {
					context = getApplicationContext();
					final SharedPreferences prefs = context
							.getSharedPreferences(context.getPackageName(),
									Context.MODE_PRIVATE);
					prefs.edit()
							.putString(
									context.getResources().getString(
											R.string.pin_key), pinNumber)
							.commit();// Set the PIN in Shared Preferences
					callbackContext.success("firstTime");
				}

			}

		}
		return true;
	}

	private static boolean isRooted() {
		return findBinary("su");
	}

	public static boolean findBinary(String binaryName) {// Checking for su
															// binary in these
															// paths to check
															// whether the
															// device is rooted
		boolean found = false;
		if (!found) {
			String[] places = { "/sbin/", "/system/bin/", "/system/xbin/",
					"/data/local/xbin/", "/data/local/bin/",
					"/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/" };
			for (String where : places) {
				if (new File(where + binaryName).exists()) {
					found = true;

					break;
				}
			}
		}
		return found;
	}

	public static boolean isRunningOnEmulator() {// Checking whether it is
													// running on emulator

		boolean result = //
		Build.FINGERPRINT.startsWith("generic")//
				|| Build.FINGERPRINT.startsWith("unknown")//
				|| Build.MODEL.contains("google_sdk")//
				|| Build.MODEL.contains("Emulator")//
				|| Build.MODEL.contains("Android SDK built for x86")
				|| Build.MANUFACTURER.contains("Genymotion");
		if (result)
			return true;
		result |= Build.BRAND.startsWith("generic")
				&& Build.DEVICE.startsWith("generic");
		if (result)
			return true;
		result |= "google_sdk".equals(Build.PRODUCT);
		return result;
	}
}
