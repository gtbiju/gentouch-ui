package com.phonegap.plugins.gcm;


import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.cognizant.lifeengage.fgli.R;
import com.google.android.gcm.GCMBaseIntentService;


@SuppressLint("NewApi")
public class GCMIntentService extends GCMBaseIntentService {

	public static final int NOTIFICATION_ID = 237;
	private static final String TAG = "GCMIntentService";
	
	public GCMIntentService() {
		super("GCMIntentService");
	}

	@Override
	public void onRegistered(Context context, String regId) {

		Log.v(TAG, "onRegistered: "+ regId);

		JSONObject json;

		try
		{
			json = new JSONObject().put("event", "registered");
			json.put("regid", regId);

			Log.v(TAG, "onRegistered: " + json.toString());

			// Send this JSON data to the JavaScript application above EVENT should be set to the msg type
			// In this case this is the registration ID
			PushPlugin.sendJavascript( json );

		}
		catch( JSONException e)
		{
			PushPlugin.sendJavascriptECB();
			// No message to the user is sent, JSON failed
			Log.e(TAG, "onRegistered: JSON exception");
		}
	}

	@Override
	public void onUnregistered(Context context, String regId) {
		Log.d(TAG, "onUnregistered - regId: " + regId);
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		Log.d(TAG, "onMessage - context: " + context);

		//count++;
		// Extract the payload from the message
		Bundle extras = intent.getExtras();
		if (extras != null)
		{
			// if we are in the foreground, just surface the payload, else post it to the statusbar
            if (PushPlugin.isInForeground()) {
				extras.putBoolean("foreground", true);
                PushPlugin.sendExtras(extras);	
			}
			else {
				extras.putBoolean("foreground", false);

                // Send a notification if there is a message
                if (extras.getString("message") != null && extras.getString("message").length() != 0) {
                	  PushPlugin.sendExtras(extras);
                	try {
						createNotification(context, extras);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
            }
        }
	}

	public void createNotification(Context context, Bundle extras) throws JSONException
	{
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		String appName = getAppName(this);

		Intent notificationIntent = new Intent(this, PushHandlerActivity.class);
		notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
		notificationIntent.putExtra("pushBundle", extras);

		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, Intent.FLAG_ACTIVITY_NEW_TASK);

		NotificationCompat.Builder mBuilder =
			new NotificationCompat.Builder(context)
				.setDefaults(Notification.DEFAULT_ALL)
				.setSmallIcon(R.drawable.icon)
				.setWhen(System.currentTimeMillis())
				.setContentTitle(extras.getString("title"))
				.setTicker(extras.getString("title"))
				.setContentIntent(contentIntent);

		String message = extras.getString("message");
		String msgcnt = extras.getString("msgcnt");
		Log.v(TAG, "###: extras.getString(message)##########"+extras.getString("message"));
		
		
		
		String notificationMessage = "";
		String type = "";
		/*try {
			JSONObject jsonObject = new JSONObject(message) ;
			type = jsonObject.getJSONObject("message").getString("type");
			Log.v(TAG, "###: jsonObject.getJSONObject(message)##########"+jsonObject.getJSONObject("message").getString("type"));
			Log.v(TAG, "###: type##########"+jsonObject.getJSONObject("message"));
			switch(type)
			{
				case "Bdy":

					Log.v(TAG, "###: "+type);
					//mBuilder.setContentText(msgcnt+" Birthday Alerts  ");
				     birthdayPNcount++;
				break;
				case "Ansry":
					Log.v(TAG, "###: "+type);
				//	mBuilder.setContentText(msgcnt+" Anniversary Alerts ");
				    anniversaryPNcount++;
				break;
				case "OpnSrRqt":
					Log.v(TAG, "###: "+type);
					//mBuilder.setContentText(msgcnt+" open request alerts ");
					//Log.d(TAG, "onMessage - msg context " + a);
				     openSerReqcount++;
				break;
				case "Rnwl":
					Log.v(TAG, "###: "+type);
					//mBuilder.setContentText(msgcnt+" renewal alerts and ");
					//Log.d(TAG, "onMessage - msg context " + a);
				      renewalPNcount++;
				break;
				case "Upsell":
					Log.v(TAG, "###: "+type);
					//mBuilder.setContentText(msgcnt+" upsell opportunities alerts and ");
					//Log.d(TAG, "onMessage - msg context " + a);
				upsellPNcount++;
				break;
				case "IssuPndg":
					Log.v(TAG, "###: "+type);
					//mBuilder.setContentText(msgcnt+" issuance pending alerts and ");
					//Log.d(TAG, "onMessage - msg context " + a);
				      issuancePendingPNcount++;
				break;
				case "RR":
					Log.v(TAG, "###: "+type);
					//mBuilder.setContentText(msgcnt+" R&R alerts and ");
					//Log.d(TAG, "onMessage - msg context " + a);
				    RRPNcpcount++;
				break;
				case "Disburse":
					Log.v(TAG, "###: "+type);
					//mBuilder.setContentText(msgcnt+" disbursement alerts and ");
					//Log.d(TAG, "onMessage - msg context " + a);
				disbursePNcount++;
				break;
				case "Issu":
					Log.v(TAG, "###: "+type);
				//	mBuilder.setContentText(msgcnt+" issuance alerts and ");
					//Log.d(TAG, "onMessage - msg context " + a);
				issuancePNcount++;
				break;
				case "Srndr":
					Log.v(TAG, "###: "+type);
					//mBuilder.setContentText(msgcnt+" surrender alerts and ");
					//Log.d(TAG, "onMessage - msg context " + a);
				     surrenderPNcount++;
				break;
				case "Lps":
					Log.v(TAG, "###: "+type);
					//mBuilder.setContentText(msgcnt+" lapse alerts and ");
					//Log.d(TAG, "onMessage - msg context " + a);
				  lapseAlertsPNcount++;
				break;
				case "SchMtng":
					Log.v(TAG, "###: "+type);
				//	mBuilder.setContentText(msgcnt+" scheduled meeting alerts and ");
					//Log.d(TAG, "onMessage - msg context " + a);
				schMtngPNcount++;
				break;
				case "LdActn":
					Log.v(TAG, "###: "+type);
				//	mBuilder.setContentText(msgcnt+" leads for action alerts and ");
					//Log.d(TAG, "onMessage - msg context " + a);
				    leadActnPNcount++;
				break;
				case "Trng":
					Log.v(TAG, "###: "+type);
					//mBuilder.setContentText(msgcnt+" my training alerts and ");
					//Log.d(TAG, "onMessage - msg context " + a);
				trainingPNcount++;
				break;
				case "FTrng":
					Log.v(TAG, "###: "+type);
					//mBuilder.setContentText(msgcnt+" future training alerts and ");
					//Log.d(TAG, "onMessage - msg context " + a);
				futureTrainingPNcount++;
				break;
				case "OthrCom":
					Log.v(TAG, "###: "+type);
					//mBuilder.setContentText(msgcnt+" important communication alerts and ");
					//Log.d(TAG, "onMessage - msg context " + a);
				   otherCommPNcount++;
				break;

			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		// creating notification message
		
		/*if(birthdayPNcount>0)
			notificationMessage =notificationMessage +birthdayPNcount+" Birthday ,";
		if(anniversaryPNcount>0)
			notificationMessage =notificationMessage +anniversaryPNcount+" Anniversary ,";
		if(openSerReqcount>0)
			notificationMessage =notificationMessage +openSerReqcount+" Open Service Request ,";
		if(renewalPNcount>0)
			notificationMessage =notificationMessage +renewalPNcount+" Renewal ,";
		if(upsellPNcount>0)
			notificationMessage =notificationMessage +upsellPNcount+" Upsell Opportunities ,";
		if(issuancePendingPNcount>0)
			notificationMessage =notificationMessage +issuancePendingPNcount+" Issuance Pending ,";
		
		if(RRPNcpcount>0)
			notificationMessage =notificationMessage +RRPNcpcount+" R&R ,";
		if(disbursePNcount>0)
			notificationMessage =notificationMessage +disbursePNcount+" Disbursement ,";
		if(issuancePNcount>0)
			notificationMessage =notificationMessage +issuancePNcount+" Issuance ,";
		if(surrenderPNcount>0)
			notificationMessage =notificationMessage +surrenderPNcount+" Surrender ,";
		if(lapseAlertsPNcount>0)
			notificationMessage =notificationMessage +lapseAlertsPNcount+" Lapse ,";
		if(schMtngPNcount>0)
			notificationMessage =notificationMessage +schMtngPNcount+" Scheduled Meeting ,";
		if(leadActnPNcount>0)
			notificationMessage =notificationMessage +leadActnPNcount+" Leads For Action ,";
		if(trainingPNcount>0)
			notificationMessage =notificationMessage +trainingPNcount+" My Training ,";
		if(futureTrainingPNcount>0)
			notificationMessage =notificationMessage +futureTrainingPNcount+" Future Training ,";
		if(otherCommPNcount>0)
			notificationMessage =notificationMessage +otherCommPNcount+" Important Communication ,";
		
		notificationMessage = notificationMessage.substring(0,notificationMessage.length()-1) +"Alert(s).";
		int index = notificationMessage.lastIndexOf(",");
		if(index>0)
		notificationMessage =notificationMessage.substring(0,index)+" and "+notificationMessage.substring(index+1);*/
		//notificationMessage = type;
		Log.v(TAG, "###############message " + notificationMessage);
		notificationMessage+=" Tap to launch application and view alerts";
		//For mServicing 	
		mBuilder.setContentTitle("GeneraliThailand");
		mBuilder.setContentText(message);
		
	/*	if (message != null) {
			
			mBuilder.setContentText("Tap to launch Application and view Alerts");//("+count+")
		} else {
			mBuilder.setContentText("<missing message content>");
		}*/

		
		if (msgcnt != null) {
			mBuilder.setNumber(Integer.parseInt(msgcnt));
		}
		
		mNotificationManager.notify((String) appName, NOTIFICATION_ID, mBuilder.build());
	
	}
	
	public static void cancelNotification(Context context)
	{
		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancel((String)getAppName(context), NOTIFICATION_ID);	
	}
	
	private static String getAppName(Context context)
	{
		CharSequence appName = 
				context
					.getPackageManager()
					.getApplicationLabel(context.getApplicationInfo());
		
		return (String)appName;
	}
	
	@Override
	public void onError(Context context, String errorId) {
		Log.e(TAG, "onError - errorId: " + errorId);
	}

}
