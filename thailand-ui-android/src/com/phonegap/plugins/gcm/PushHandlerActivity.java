package com.phonegap.plugins.gcm;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

public class PushHandlerActivity extends Activity
{
	private static String TAG = "PushHandlerActivity"; 

	/*
	 * this activity will be started if the user touches a notification that we own. 
	 * We send it's data off to the push plugin for processing.
	 * If needed, we boot up the main activity to kickstart the application. 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Log.v(TAG, "######onCreate");

		boolean isPushPluginActive = PushPlugin.isActive();
		processPushBundle(isPushPluginActive);

		GCMIntentService.cancelNotification(this);

		/*GCMIntentService.birthdayPNcount=0;
		GCMIntentService.anniversaryPNcount=0;
		GCMIntentService.openSerReqcount=0;
		GCMIntentService.renewalPNcount=0;
		GCMIntentService.upsellPNcount=0;
		GCMIntentService.issuancePendingPNcount=0;
		GCMIntentService.RRPNcpcount=0;
		GCMIntentService.disbursePNcount=0;
		GCMIntentService.issuancePNcount=0;
		GCMIntentService.surrenderPNcount=0;
		GCMIntentService.lapseAlertsPNcount=0;
		GCMIntentService.schMtngPNcount=0;
		GCMIntentService.leadActnPNcount=0;
		GCMIntentService.trainingPNcount=0;
		GCMIntentService.futureTrainingPNcount=0;
		GCMIntentService.otherCommPNcount=0;*/
		
		finish();

		if (!isPushPluginActive) {
			forceMainActivityReload();
		}
	}

	/**
	 * Takes the pushBundle extras from the intent, 
	 * and sends it through to the PushPlugin for processing.
	 */
	private void processPushBundle(boolean isPushPluginActive)
	{
		Bundle extras = getIntent().getExtras();

		if (extras != null)	{
			Bundle originalExtras = extras.getBundle("pushBundle");
            
            originalExtras.putBoolean("foreground", false);
            originalExtras.putBoolean("coldstart", !isPushPluginActive);

			PushPlugin.sendExtras(originalExtras);
		}
	}

	/**
	 * Forces the main activity to re-launch if it's unloaded.
	 */
	private void forceMainActivityReload()
	{
		Bundle extras = getIntent().getExtras();
		//Bundle originalExtras = extras.getBundle("pushBundle");
	
		PackageManager pm = getPackageManager();
		Intent launchIntent = pm.getLaunchIntentForPackage(getApplicationContext().getPackageName());  
		Log.v(TAG, "forceMainActivityReload"+getApplicationContext().getPackageName());
		startActivity(launchIntent);
		//GCMIntentService.count=0;
	}

}