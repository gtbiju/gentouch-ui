/*
 * PhoneGap is available under *either* the terms of the modified BSD license *or* the
 * MIT License (2008). See http://opensource.org/licenses/alphabetical for full text.
 *
 * Copyright (c) 2005-2010, Nitobi Software Inc.
 * Copyright (c) 2010, IBM Corporation
 */
package com.phonegap.plugins.pdfViewer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import com.cognizant.lifeengage.fgli.MainActivity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

public class PdfViewer extends CordovaPlugin {
    private static String DIR_PATH ;
    private static final String ASSETS = "file:///android_asset/";
    Context con;
    /**
     * Executes the request and returns PluginResult.
     * 
     * @param action
     *            The action to execute.
     * @param args
     *            JSONArry of arguments for the plugin.
     //* @param callbackId
     *            The callback id used when calling back into JavaScript.
     * @return A PluginResult object with a status and message.
     */
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        try {
        	con = this.cordova.getActivity().getApplicationContext();
        	
            if (action.equals("showPdfFromBase64")){
            	String result = args.getString(0);
            	byte[] pdfAsBytes = Base64.decode(result, 0);
            	DIR_PATH = this.cordova.getActivity().getApplicationInfo().dataDir + "/PDF/";
            	File dir = new File(DIR_PATH);
                if (!dir.exists()) {
                    dir.mkdir();
                }
                
            	File filePath = new File(DIR_PATH  + "/" + "test.pdf");
            	FileOutputStream os;
				try {
					os = new FileOutputStream(filePath, true);
					os.write(pdfAsBytes);
					os.close();
					this.showPdf(filePath.getAbsolutePath(), true);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	
            	
            }
			
			if (action.equals("downloadPdf")){
            	String result = args.getString(0);
            	String proposalNumber = args.getString(1);
            	String downloadFolder = args.getString(2);
            	byte[] pdfAsBytes = Base64.decode(result, 0);           	
            	DIR_PATH = Environment.getExternalStorageDirectory().getPath() + "/" + downloadFolder;

            	File dir = new File(DIR_PATH);
                if (!dir.exists()) {
                    dir.mkdir();
                }
                File filePath = new File(DIR_PATH  + "/" +  proposalNumber + ".pdf");
            	FileOutputStream os;
				try {
					os = new FileOutputStream(filePath, true);
					os.write(pdfAsBytes);
					os.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
            
            if (action.equals("printPdf")){
            	String result = args.getString(0);
            	String proposalNumber = args.getString(1);
            	String downloadFolder = args.getString(2);
            	byte[] pdfAsBytes = Base64.decode(result, 0);           	
            	DIR_PATH = Environment.getExternalStorageDirectory().getPath() + "/" + downloadFolder;

            	File dir = new File(DIR_PATH);
                if (!dir.exists()) {
                    dir.mkdir();
                }
                File filePath = new File(DIR_PATH  + "/" +  proposalNumber + ".pdf");
            	FileOutputStream os;
				try {
					os = new FileOutputStream(filePath, true);
					os.write(pdfAsBytes);
					os.close();
					this.openPdf(filePath.getAbsolutePath());
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
			
        	if (action.equals("showPdf")) {
                String result = this.showPdf(args.getString(0), false);
                if (result.length() > 0) {
                    return false;
                }
            }
            if (action.equals("illustration")) {
                String result = this.saveFile(args.getString(0));
                if (result.length() > 0) {
                    return false;
                }
            }
            if(action.equals("openPdf")){
            	return openPdf(args.getString(0));
            }
            return true;
        } catch (JSONException e) {
            return false;
        }
    }

    /**
     * Called by AccelBroker when listener is to be shut down. Stop listener.
     */
    public void onDestroy() {
    }

    // --------------------------------------------------------------------------
    // LOCAL METHODS
    // --------------------------------------------------------------------------
    private String getFileName(String fileName){
    	if(fileName.contains("/")){
    		int lastIdx = fileName.lastIndexOf("/");
    		return fileName.substring(lastIdx +1);
    	}else{
    		return fileName;
    	}
    }
    private boolean openPdf(String fileName){
    	try{
            /** Commenting the old cordova path implementaion which need sdcard keyword */
    		//String extStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
    		//fileName = fileName.substring(fileName.lastIndexOf("sdcard")+6);
    		File pdfFile = new File(fileName);
    		if(pdfFile.exists()){
    			Intent intent = new Intent(Intent.ACTION_VIEW);
    			intent.setDataAndType(Uri.fromFile(pdfFile), "application/pdf");
    			this.webView.getContext().startActivity(intent);
    		}
    	}catch(ActivityNotFoundException e){
    		return false;
    	}
    	
    	return true;
    }
    public String showPdf(String url, Boolean isPdfFolderRequired) {
    	String fileName = getFileName(url);
    	
    	if(url.contains(ASSETS)) {
            // get file path in assets folder
            String filepath = url.replace(ASSETS, "");
            DIR_PATH = this.cordova.getActivity().getFilesDir() + "/PDF/";
            File file = new File(DIR_PATH + fileName);
            if(!file.exists()){
            	copyFile(filepath);
            }
            
            try {
                
                this.webView.getContext().startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(MainActivity.CONTENT_URI
                                + "files/PDF/" + fileName)));
                return "";
            } catch (android.content.ActivityNotFoundException e) {
                System.out.println("PdfViewer: Error loading url " + fileName + ":" + e.toString());
                return e.toString();
            }catch (Exception e) {
                Log.e("tag", e.getMessage());
                return e.getMessage();
            }
    	}
    	
        try {
            if(!url.contains("Uploads")){
                String path = MainActivity.CONTENT_URI.toString();
                if (isPdfFolderRequired) {
                    path = path + "PDF/";
                }
                path = path + fileName;
                this.webView.getContext().startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(path)));
                return "";

























            }else {
                String path = MainActivity.CONTENT_URI.toString();
                path = path + "Uploads/"+fileName;
                this.webView.getContext().startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(path)));
                return "";
            }
        } catch (android.content.ActivityNotFoundException e) {
            System.out.println("PdfViewer: Error loading url " + fileName + ":" + e.toString());
            return e.toString();
        }catch (Exception e) {
            Log.e("tag", e.getMessage());
            return e.getMessage();
        }
    }

    private void copyFile(String filePath) {
        AssetManager assetManager = this.cordova.getActivity().getAssets();

        InputStream in = null;
        OutputStream out = null;
        try {
        	String fileName = getFileName(filePath);
            in = assetManager.open(filePath);
            String newFileName = DIR_PATH + fileName;
            File dir = new File(DIR_PATH);
            if(!dir.exists()){
            	dir.mkdirs();
            }
            out = new FileOutputStream(newFileName);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }
    }

    /**
     * @param contents
     * @return
     */
    private String saveFile(String contents) {
        File dir = new File(DIR_PATH);
        if (!dir.exists()) {
            dir.mkdir();
        }

        dir = new File(DIR_PATH);
        String fileName = "illustration.pdf";
        File file = new File(DIR_PATH + "/" + fileName);

        if (file.exists()) {
            file.delete();
        }

        try {
            byte[] contentBytes = Base64.decode(contents, Base64.DEFAULT);

            file = new File(DIR_PATH + "/" + fileName);
            writeFile(file, contentBytes);
            
            /*Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            this.webView.getContext().startActivity(intent);*/
            
            this.webView.getContext().startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(MainActivity.CONTENT_URI
                            + fileName)));
            
            return "";
        } catch (Exception e) {
            System.out.println("PdfViewer: Error loading url " + fileName + ":" + e.toString());
            return e.toString();
        }
    }

    private void writeFile(File file, byte[] contentBytes) {
        FileOutputStream writer = null;
        try {
            writer = new FileOutputStream(file);
            writer.write(contentBytes);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}