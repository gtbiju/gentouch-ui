package com.phonegap.plugins.pdfViewer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;

public class FileProvider extends ContentProvider {
	private static final HashMap<String, String> MIME_TYPES = new HashMap<String, String>();
	private final static String[] OPENABLE_PROJECTION = {
			OpenableColumns.DISPLAY_NAME, OpenableColumns.SIZE };

	static {
		MIME_TYPES.put(".pdf", "application/pdf");
		MIME_TYPES.put(".mp4", "application/video");
	}

	@Override
	public boolean onCreate() {
		
		return (true);
	}

	@Override
	public String getType(Uri uri) {
		String path = uri.toString();

		for (String extension : MIME_TYPES.keySet()) {
			if (path.endsWith(extension)) {
				return (MIME_TYPES.get(extension));
			}
		}

		return (null);
	}

	@Override
	public ParcelFileDescriptor openFile(Uri uri, String mode)
			throws FileNotFoundException {
		File f = new File(getContext().getApplicationInfo().dataDir,
				uri.getPath());

		if (f.exists()) {
			return (ParcelFileDescriptor.open(f,
					ParcelFileDescriptor.MODE_READ_ONLY));
		}

		throw new FileNotFoundException(uri.getPath());
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sort) {
		if (projection == null) {
			projection = OPENABLE_PROJECTION;
		}

		final MatrixCursor cursor = new MatrixCursor(projection, 1);

		MatrixCursor.RowBuilder b = cursor.newRow();

		for (String col : projection) {
			if (OpenableColumns.DISPLAY_NAME.equals(col)) {
				b.add(getFileName(uri));
			} else if (OpenableColumns.SIZE.equals(col)) {
				b.add(getDataLength(uri));
			} else { // unknown, so just add null
				b.add(null);
			}
		}

		return (cursor);
	}

	protected String getFileName(Uri uri) {
		return (uri.getLastPathSegment());
	}

	protected long getDataLength(Uri uri) {
		return (AssetFileDescriptor.UNKNOWN_LENGTH);
	}

	@Override
	public Uri insert(Uri uri, ContentValues initialValues) {
		throw new RuntimeException("Operation not supported");
	}

	@Override
	public int update(Uri uri, ContentValues values, String where,
			String[] whereArgs) {
		throw new RuntimeException("Operation not supported");
	}

	@Override
	public int delete(Uri uri, String where, String[] whereArgs) {
		throw new RuntimeException("Operation not supported");
	}

	static private void copy(InputStream in, File dst) throws IOException {
		FileOutputStream out = new FileOutputStream(dst);
		byte[] buf = new byte[1024];
		int len;

		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}

		in.close();
		out.close();
	}
}