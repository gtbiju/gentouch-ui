package com.cognizant.lifeengage.fgli;

import org.apache.cordova.Config;
import org.apache.cordova.*;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Button;
import com.cognizant.lifeengage.fgli.R;


public class MainActivity extends CordovaActivity {
	public static Uri CONTENT_URI;
    Button enterPin;
    Context appContext;
    private boolean pinEntryShown;
    
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
		    WebView.setWebContentsDebuggingEnabled(true);
		}
		//setContentView(R.layout.activity_main);
		//super.setIntegerProperty("splashscreen", R.drawable.splash);
		String packageName = getApplicationContext().getPackageName();
		String url = "content://" + packageName + "/";
		CONTENT_URI = Uri.parse(url);
		//super.loadUrl(Config.getStartUrl(), 10000);
        loadUrl(launchUrl);
	}
	@Override
    public void onResume() {
        super.onResume();
       
    } 
}