//
//  SystemCheck.m
//
//
//  Created by Sajith M on 19/01/15.
//
//

#import "SystemCheck.h"
#import <CommonCrypto/CommonDigest.h>

@interface SystemCheck ()
@end

@implementation SystemCheck

- (void)RootCheck:(CDVInvokedUrlCommand*)command{
    
   // NSLog(@"command== %@",command.arguments);
    
    CDVPluginResult* pluginResult = nil;
    BOOL isJailbroken = [SystemCheck isJailBroken];
    
    NSString *message = @"rootediOS";
    NSDictionary *results = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithBool:isJailbroken],message,nil] forKeys:[NSArray arrayWithObjects:@"rootedStatus",@"message",nil]];
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:results];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)DeleteCheck:(CDVInvokedUrlCommand*)command{

}

+(BOOL)isJailBroken{
    
#if !(TARGET_IPHONE_SIMULATOR)
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Applications/Cydia.app"]){
        return YES;
    }else if([[NSFileManager defaultManager] fileExistsAtPath:@"/Library/MobileSubstrate/MobileSubstrate.dylib"]){
        return YES;
    }else if([[NSFileManager defaultManager] fileExistsAtPath:@"/bin/bash"]){
        return YES;
    }else if([[NSFileManager defaultManager] fileExistsAtPath:@"/usr/sbin/sshd"]){
        return YES;
    }else if([[NSFileManager defaultManager] fileExistsAtPath:@"/etc/apt"]){
        return YES;
    }
    
    NSError *error;
    NSString *stringToBeWritten = @"This is a test for file wrie permission.";
    [stringToBeWritten writeToFile:@"/private/jailbreak.txt" atomically:YES
                          encoding:NSUTF8StringEncoding error:&error];
    if(error==nil){
        //Device is jailbroken
        return YES;
    } else {
        [[NSFileManager defaultManager] removeItemAtPath:@"/private/jailbreak.txt" error:nil];
    }
    
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"cydia://package/com.example.package"]]){
        //Device is jailbroken
        return YES;
    }
#endif
    
    //All checks have failed. Most probably, the device is not jailbroken
    return NO;
}




@end
