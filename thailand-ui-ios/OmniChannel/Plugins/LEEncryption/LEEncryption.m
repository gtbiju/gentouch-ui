/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 *
 * version 2.1.0.0
 */

#import "LEEncryption.h"
#import <Security/Security.h>
static NSString *SFHFKeychainUtilsErrorDomain = @"SFHFKeychainUtilsErrorDomain";
static NSString *key = @"com.cognizant.appName.encryption.key";
static NSString *serviceName = @"Keychain";


@implementation LEEncryption

- (CDVPlugin*) initWithWebView:(UIWebView*)theWebView
{
    self = (LEEncryption*)[super initWithWebView:(UIWebView*)theWebView];
    if (self) {
        // initialization here
    }
    return self;
}

- (void) fetchKey:(CDVInvokedUrlCommand*)command
{
    NSArray* arguments = command.arguments;
    NSString* appName = [(NSDictionary *)[arguments objectAtIndex:0] objectForKey:@"appName"];
    CDVPluginResult* pluginResult = nil;
    
    NSError* error = nil;
    NSLog(@"%@", key);
    NSLog(@"%@", serviceName);
    
    NSString* value = [LEEncryption getValueForKey:&error appName:appName];//[LEEncryption getValueForKey  : error:&error appName:appName];
    if (error == nil && value != nil) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:value];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                         messageAsString:[NSString stringWithFormat:@"error retrieving value for key '%@' : %@", key, [error localizedDescription]]];
    }
    
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) insertKey:(CDVInvokedUrlCommand*)command
{
    NSArray* arguments = command.arguments;
    CDVPluginResult* pluginResult = nil;
    
    if ([arguments count] == 1 && [(NSDictionary *)[arguments objectAtIndex:0] objectForKey:@"key"] && [(NSDictionary *)[arguments objectAtIndex:0] objectForKey:@"key"])
    {
        NSString* value = [(NSDictionary *)[arguments objectAtIndex:0] objectForKey:@"key"];
        NSString* appName = [(NSDictionary *)[arguments objectAtIndex:0] objectForKey:@"appName"];
        NSError* error = nil;
        NSLog(@"%@", value);
        BOOL stored = [LEEncryption storeKey:value error:&error appName : appName];
        NSLog(@"%hhd", stored);
        
        if (stored && error == nil) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"key inserted successfully"];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[error localizedDescription]];
        }
    }
    else
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                         messageAsString:@"incorrect number of arguments for setForKey"];
    }
    
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) removeForKey:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    
    
    NSError* error = nil;
    NSLog(@"%@", key);
    NSLog(@"%@", serviceName);
    
    BOOL deleted = [LEEncryption deleteItemForUsername:&error appName:@""];//[LEEncryption deleteItemForUsername:&error];
    if (deleted && error == nil) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[error localizedDescription]];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) checkKeyAvailability:(CDVInvokedUrlCommand*)command
{
    NSLog(@"%s", "checkKeyAvailability");
    
    NSArray* arguments = command.arguments;
    NSString* appName = [(NSDictionary *)[arguments objectAtIndex:0] objectForKey:@"appName"];
    CDVPluginResult* pluginResult = nil;
    
    
    NSError* error = nil;
    NSLog(@"%@", key);
    NSLog(@"%@", serviceName);
    
    BOOL deleted = [LEEncryption checkKeyAvailableOrNot:&error appName:appName];//[LEEncryption checkKeyAvailableOrNot:&error appName : appName];
    NSLog(@"%hhd", deleted);
    if (deleted && error == nil) {
        NSLog(@"%s", "inside deleted if");
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"key exists"];
    } else {
        NSLog(@"%s", "inside deleted else");
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"key not exists"];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) encryptDB:(CDVInvokedUrlCommand*)command
{
    NSArray* arguments = command.arguments;
    NSString* appName = [(NSDictionary *)[arguments objectAtIndex:0] objectForKey:@"appName"];
    NSDictionary *fileDescList = [(NSDictionary *)[arguments objectAtIndex:0] objectForKey:@"fileArray"];
    CDVPluginResult* pluginResult = nil;
    NSString *documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask, YES) objectAtIndex:0];
    for (NSDictionary *fileDesc in fileDescList){
        NSString *fileName =  [fileDesc objectForKey:@"fileName"];
        NSString *filePath = [documentDirectory stringByAppendingPathComponent:fileName];
        [LEEncryption encryptDbIfNot: filePath appName : (NSString *) appName];
    }
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"DBs encrypted successfully"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

+ (BOOL) checkKeyAvailableOrNot: (NSError **) error appName: (NSString *) appName
{
    NSLog(@"%s", "inside checkKeyAvailableOrNot");
    
    
    // See if we already have a password entered for these credentials.
    NSError *getError = nil;
    NSString *existingPassword = [self getValueForKey:&getError appName:appName];//[self getValueForKey:&getError appName:appName];
    NSLog(@"%@", existingPassword);
    if ([getError code] == -1999)
    {
        // There is an existing entry without a password properly stored (possibly as a result of the previous incorrect version of this code.
        // Delete the existing item before moving on entering a correct one.
        
        getError = nil;
        
        [self deleteItemForUsername:&getError appName:appName];
        //[self deleteItemForUsername: &getError];
        
        if ([getError code] != noErr)
        {
            if (error != nil)
            {
                *error = getError;
            }
            return NO;
        }
    }
    else if ([getError code] != noErr)
    {
        if (error != nil)
        {
            *error = getError;
        }
        return NO;
    }
    
    if (error != nil)
    {
        *error = nil;
    }
    NSLog(@"%@", existingPassword);
    if (existingPassword)
    {
        NSLog(@"%s", "inside if");
        return YES;
    }
    else
    {
        NSLog(@"%s", "inside else");
        return NO;
    }
}



+ (NSString *) getValueForKey: (NSError **) error appName : (NSString *) appName {
    NSLog(@"%s", "inside getValueForKey");
    
    if (error != nil) {
        *error = nil;
    }
    
    // Set up a query dictionary with the base query attributes: item type (generic), username, and service
    NSArray *keys = [[[NSArray alloc] initWithObjects: (NSString *) kSecClass, kSecAttrAccount, kSecAttrService, nil] autorelease];
    NSArray *objects = [[[NSArray alloc] initWithObjects: (NSString *) kSecClassGenericPassword, [key stringByReplacingOccurrencesOfString:@"appName" withString: appName], serviceName, nil] autorelease];
    
    NSMutableDictionary *query = [[[NSMutableDictionary alloc] initWithObjects: objects forKeys: keys] autorelease];
    
    // First do a query for attributes, in case we already have a Keychain item with no password data set.
    // One likely way such an incorrect item could have come about is due to the previous (incorrect)
    // version of this code (which set the password as a generic attribute instead of password data).
    
    NSDictionary *attributeResult = NULL;
    NSMutableDictionary *attributeQuery = [query mutableCopy];
    [attributeQuery setObject: (id) kCFBooleanTrue forKey:(id) kSecReturnAttributes];
    OSStatus status = SecItemCopyMatching((CFDictionaryRef) attributeQuery, (CFTypeRef *) &attributeResult);
    
    [attributeResult release];
    [attributeQuery release];
    
    if (status != noErr) {
        // No existing item found--simply return nil for the password
        if (error != nil && status != errSecItemNotFound) {
            //Only return an error if a real exception happened--not simply for "not found."
            *error = [NSError errorWithDomain: SFHFKeychainUtilsErrorDomain code: status userInfo: nil];
        }
        //NSLog(@"%s", "status != noErr");
        return nil;
    }
    //NSLog(@"%s", "inside getValueForKey1");
    // We have an existing item, now query for the password data associated with it.
    
    NSData *resultData = nil;
    NSMutableDictionary *passwordQuery = [query mutableCopy];
    [passwordQuery setObject: (id) kCFBooleanTrue forKey: (id) kSecReturnData];
    
    status = SecItemCopyMatching((CFDictionaryRef) passwordQuery, (CFTypeRef *) &resultData);
    
    [resultData autorelease];
    [passwordQuery release];
    //NSLog(@"%s", "inside getValueForKey2");
    if (status != noErr) {
        if (status == errSecItemNotFound) {
            // We found attributes for the item previously, but no password now, so return a special error.
            // Users of this API will probably want to detect this error and prompt the user to
            // re-enter their credentials.  When you attempt to store the re-entered credentials
            // using storeUsername:andPassword:forServiceName:updateExisting:error
            // the old, incorrect entry will be deleted and a new one with a properly encrypted
            // password will be added.
            if (error != nil) {
                *error = [NSError errorWithDomain: SFHFKeychainUtilsErrorDomain code: -1999 userInfo: nil];
            }
        }
        else {
            // Something else went wrong. Simply return the normal Keychain API error code.
            if (error != nil) {
                *error = [NSError errorWithDomain: SFHFKeychainUtilsErrorDomain code: status userInfo: nil];
            }
        }
        
        return nil;
    }
    // NSLog(@"%s", "inside getValueForKey 3");
    NSString *password = nil;
    
    if (resultData) {
        password = [[NSString alloc] initWithData: resultData encoding: NSUTF8StringEncoding];
    }
    else {
        // There is an existing item, but we weren't able to get password data for it for some reason,
        // Possibly as a result of an item being incorrectly entered by the previous code.
        // Set the -1999 error so the code above us can prompt the user again.
        if (error != nil) {
            *error = [NSError errorWithDomain: SFHFKeychainUtilsErrorDomain code: -1999 userInfo: nil];
        }
    }
    //NSLog(@"%s", "inside getValueForKey 4");
    return [password autorelease];
}

+ (BOOL) storeKey: (NSString *) value error: (NSError **) error appName : (NSString *) appName
{
    if ( !value)
    {
        if (error != nil)
        {
            *error = [NSError errorWithDomain: SFHFKeychainUtilsErrorDomain code: -2000 userInfo: nil];
        }
        return NO;
    }
    OSStatus status = noErr;
    
    NSArray *keys = [[[NSArray alloc] initWithObjects: (NSString *) kSecClass,
                      kSecAttrService,
                      kSecAttrLabel,
                      kSecAttrAccount,
                      kSecValueData,
                      nil] autorelease];
    
    NSArray *objects = [[[NSArray alloc] initWithObjects: (NSString *) kSecClassGenericPassword,
                         serviceName,
                         serviceName,
                         [key stringByReplacingOccurrencesOfString:@"appName" withString: appName],
                         [value dataUsingEncoding: NSUTF8StringEncoding],
                         nil] autorelease];
    
    NSDictionary *query = [[[NSDictionary alloc] initWithObjects: objects forKeys: keys] autorelease];
    
    status = SecItemAdd((CFDictionaryRef) query, NULL);
    
    
    if (status != noErr)
    {
        // Something went wrong with adding the new item. Return the Keychain error code.
        if (error != nil) {
            *error = [NSError errorWithDomain: SFHFKeychainUtilsErrorDomain code: status userInfo: nil];
        }
        
        return NO;
    }
    
    return YES;
}

+ (BOOL) deleteItemForUsername: (NSError **) error appName : (NSString *) appName
{
    if (error != nil)
    {
        *error = nil;
    }
    
    NSArray *keys = [[[NSArray alloc] initWithObjects: (NSString *) kSecClass, kSecAttrAccount, kSecAttrService, kSecReturnAttributes, nil] autorelease];
    NSArray *objects = [[[NSArray alloc] initWithObjects: (NSString *) kSecClassGenericPassword, [key stringByReplacingOccurrencesOfString:@"appName" withString: appName], serviceName, kCFBooleanTrue, nil] autorelease];
    
    NSDictionary *query = [[[NSDictionary alloc] initWithObjects: objects forKeys: keys] autorelease];
    
    OSStatus status = SecItemDelete((CFDictionaryRef) query);
    
    if (status != noErr)
    {
        if (error != nil) {
            *error = [NSError errorWithDomain: SFHFKeychainUtilsErrorDomain code: status userInfo: nil];
        }
        
        return NO;
    }
    
    return YES;
}

+(void) encryptDbIfNot:(NSString *)srcFilePath appName : (NSString *) appName
{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSError *err = NULL;
    
    sqlite3 *db;
    // sqlite3_open([filePath UTF8String] , &db)==SQLITE_OK
    NSFileManager *fm = [[NSFileManager alloc] init];
    
    NSString *data = [LEEncryption readFile: srcFilePath];
    if([data rangeOfString:@"SQLite format"].location != NSNotFound)
    {
        
        if(sqlite3_open([srcFilePath UTF8String] , &db)==SQLITE_OK)
        {
            //
        }
        
        const char *defaulKkey = [@"" UTF8String];
        sqlite3_key(db, defaulKkey, strlen(defaulKkey));
        
        NSString* value = [LEEncryption getValueForKey:&err appName:appName];
        
        NSString *newdbpath = [documentsDirectory stringByAppendingPathComponent:@"newdb.db"];
        
        NSString *str=[NSString stringWithFormat:@"ATTACH DATABASE \'%@\' AS newdb KEY \'%@\';",newdbpath,value];
        sqlite3_exec(db,[str UTF8String], NULL, NULL, NULL);
        sqlite3_exec(db, "SELECT sqlcipher_export('newdb');", NULL, NULL, NULL);
        sqlite3_exec(db, "DETACH DATABASE newdb;", NULL, NULL, NULL);
        
        @synchronized([NSFileManager defaultManager]){
            
            if([[NSFileManager defaultManager] fileExistsAtPath:srcFilePath]){
                [[NSFileManager defaultManager] removeItemAtPath:srcFilePath error:nil];
            }
        }
        
        NSLog(@"----db newdbpath---%@",newdbpath);
        NSLog(@"----db filePath---%@",srcFilePath);
        @synchronized([NSFileManager defaultManager]){
            BOOL result = [fm moveItemAtPath:newdbpath toPath:srcFilePath error:&err];
            if(!result)
                NSLog(@"Error: %@", err);
            
        }
        NSLog(@"-----renamed-----");
        // [NSThread sleepForTimeInterval:3];
    } else {
        NSLog(@"-----Alredy Encrypted-----");
    }
    
}

+(NSString *)readFile:(NSString *)fileName

{
    NSLog(@"readFile");
    
    NSString *appFile = fileName;
    NSFileManager *fileManager=[NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:appFile])
    {
        NSError *error= NULL;
        NSString *resultData = [NSString stringWithContentsOfFile: appFile encoding: NSASCIIStringEncoding error: &error];
        if (error == NULL)
            return resultData;
    }
    return NULL;
}

@end
