// PDFViewer based on ChildBrowser

//  Created by Jesse MacFadyen on 10-05-29.
//  Copyright 2010 Nitobi. All rights reserved.
//  Copyright 2012, Randy McMillan

#import "PdfViewer.h"

#import <Cordova/CDVViewController.h>


@implementation PdfViewer

@synthesize pdfViewer;

// args: url

//	if (self.pdfViewer == nil) {
//#if __has_feature(objc_arc)
//			self.pdfViewer = [[PDFViewerViewController alloc] initWithScale:NO];
//#else
//			self.pdfViewer = [[[PDFViewerViewController alloc] initWithScale:NO] autorelease];
//#endif
//		self.pdfViewer.delegate				= self;
//		self.pdfViewer.orientationDelegate	= self.viewController;
//	}
//	//TODO: Add better Modal Pres options
//    pdfViewer.modalPresentationStyle = UIModalPresentationFullScreen;
//    //pdfViewer.modalPresentationStyle = UIModalPresentationPageSheet;
//	//pdfViewer.modalPresentationStyle = UIModalPresentationFormSheet;
//	pdfViewer.modalTransitionStyle = UIModalTransitionStylePartialCurl;
//    //pdfViewer.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
//    //pdfViewer.modalTransitionStyle  =  UIModalTransitionStyleFlipHorizontal;
//    
//    [self.viewController presentModalViewController:pdfViewer animated:YES];
  ////    
//   [self.pdfViewer loadPDF:pdfName];	// @"YingYang.pdf"];

-(void)showPdf:(CDVInvokedUrlCommand*)command{

    NSLog(@"AM Hereeeeee");
    if(command.arguments){
    
        if (command.arguments.count>0) {
            
            
            NSLog(@"HIT");
            
            
            NSString *pdfName = (NSString *)[command.arguments objectAtIndex:0];
            
            NSLog(@"%@",pdfName);
            
            //NSString* videoPath=(NSString *)[command.arguments objectAtIndex:0];
            NSString* theFileName = [pdfName lastPathComponent];
            //check the file path for assets
            if([pdfName rangeOfString:@"file:///android_asset"].location != NSNotFound)
            {
                NSError *error;
                NSString* bundlePath = [pdfName stringByReplacingOccurrencesOfString:@"file:///android_asset/" withString:@""];
                //[bundlePath stringByAppendingString:theFileName];
                NSString *file = [[NSBundle mainBundle] pathForResource:bundlePath ofType:nil];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *docDirectory = [paths objectAtIndex:0];
                NSString *filePath = [docDirectory stringByAppendingPathComponent:theFileName];
                pdfName = filePath;
                
                if([[NSFileManager defaultManager] copyItemAtPath:file toPath:filePath error:&error]){
                    NSLog(@"File successfully copied");
                    self.pdfCnt=[[PDFwebViewcontroller alloc] init];
                    
                    [self.viewController presentModalViewController:_pdfCnt animated:YES];
                    
                    [self.pdfCnt loadPDFFromLocal:pdfName];
                    return;
                    
                }
            }
            
            self.pdfCnt=[[PDFwebViewcontroller alloc] init];
            
            [self.viewController presentModalViewController:_pdfCnt animated:YES];
            
            [self.pdfCnt loadPDFFromLocal:pdfName];
        }
    }
}

- (void)showPdf:(NSMutableArray *)arguments withDict:(NSMutableDictionary *)options

{
    NSLog(@"HIT");
    
    
    NSString *pdfName = (NSString *)[arguments objectAtIndex:1];
    
    NSLog(@"%@",pdfName);
    
    //NSString* videoPath=(NSString *)[command.arguments objectAtIndex:0];
    NSString* theFileName = [pdfName lastPathComponent];
    //check the file path for assets
    if([pdfName rangeOfString:@"file:///android_asset"].location != NSNotFound)
    {
        NSError *error;
        NSString* bundlePath = [pdfName stringByReplacingOccurrencesOfString:@"file:///android_asset/" withString:@""];
        //[bundlePath stringByAppendingString:theFileName];
        NSString *file = [[NSBundle mainBundle] pathForResource:bundlePath ofType:nil];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docDirectory = [paths objectAtIndex:0];
        NSString *filePath = [docDirectory stringByAppendingPathComponent:theFileName];
        pdfName = filePath;
        
        if([[NSFileManager defaultManager] copyItemAtPath:file toPath:filePath error:&error]){
            NSLog(@"File successfully copied");
            self.pdfCnt=[[PDFwebViewcontroller alloc] init];
            
            [self.viewController presentModalViewController:_pdfCnt animated:YES];
            
            [self.pdfCnt loadPDFFromLocal:pdfName];
            return;

        }
    }

    self.pdfCnt=[[PDFwebViewcontroller alloc] init];
    
    [self.viewController presentModalViewController:_pdfCnt animated:YES];
    
    [self.pdfCnt loadPDFFromLocal:pdfName];
    
}

-(void)showPdfFromBase64:(CDVInvokedUrlCommand*)command
{
    //UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"inside native code" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    //[alert show];
    NSString *pdfStr= @"";
  
    if (command.arguments)
    {
        if (command.arguments.count > 0)
        {
            pdfStr=(NSString *)[command.arguments objectAtIndex:0];
            
            
            self.pdfCnt=[[PDFwebViewcontroller alloc] init];
            
            [self.viewController presentModalViewController:_pdfCnt animated:YES];
        }
    }
    else{
        return;
    }
    

    
    @try{
        

        NSData *bytes = [self base64DataFromString:pdfStr];
        
        NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        
        
        
        NSString *path = [documents stringByAppendingPathComponent:@"mypdf.pdf"];
        
        NSError *error;
        
        [bytes writeToFile:path options:NSUTF8StringEncoding error:&error];
      
 
        
        [self.pdfCnt loadPDFFromDocuments:path];
        
        
}
    @catch (NSException *ex) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"%@",ex]
                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    
}

- (NSData *)base64DataFromString: (NSString *)string
{
    unsigned long ixtext, lentext;
    unsigned char ch, inbuf[4], outbuf[3];
    short i, ixinbuf;
    Boolean flignore, flendtext = false;
    const unsigned char *tempcstring;
    NSMutableData *theData;
    
    if (string == nil)
    {
        return [NSData data];
    }
    
    ixtext = 0;
    
    tempcstring = (const unsigned char *)[string UTF8String];
    
    lentext = [string length];
    
    theData = [NSMutableData dataWithCapacity: lentext];
    
    ixinbuf = 0;
    
    while (true)
    {
        if (ixtext >= lentext)
        {
            break;
        }
        
        ch = tempcstring [ixtext++];
        
        flignore = false;
        
        if ((ch >= 'A') && (ch <= 'Z'))
        {
            ch = ch - 'A';
        }
        else if ((ch >= 'a') && (ch <= 'z'))
        {
            ch = ch - 'a' + 26;
        }
        else if ((ch >= '0') && (ch <= '9'))
        {
            ch = ch - '0' + 52;
        }
        else if (ch == '+')
        {
            ch = 62;
        }
        else if (ch == '=')
        {
            flendtext = true;
        }
        else if (ch == '/')
        {
            ch = 63;
        }
        else
        {
            flignore = true;
        }
        
        if (!flignore)
        {
            short ctcharsinbuf = 3;
            Boolean flbreak = false;
            
            if (flendtext)
            {
                if (ixinbuf == 0)
                {
                    break;
                }
                
                if ((ixinbuf == 1) || (ixinbuf == 2))
                {
                    ctcharsinbuf = 1;
                }
                else
                {
                    ctcharsinbuf = 2;
                }
                
                ixinbuf = 3;
                
                flbreak = true;
            }
            
            inbuf [ixinbuf++] = ch;
            
            if (ixinbuf == 4)
            {
                ixinbuf = 0;
                
                outbuf[0] = (inbuf[0] << 2) | ((inbuf[1] & 0x30) >> 4);
                outbuf[1] = ((inbuf[1] & 0x0F) << 4) | ((inbuf[2] & 0x3C) >> 2);
                outbuf[2] = ((inbuf[2] & 0x03) << 6) | (inbuf[3] & 0x3F);
                
                for (i = 0; i < ctcharsinbuf; i++)
                {
                    [theData appendBytes: &outbuf[i] length: 1];
                }
            }
            
            if (flbreak)
            {
                break;
            }
        }
    }
    
    return theData;
}
- (void)close:(NSMutableArray *)arguments withDict:(NSMutableDictionary *)options	// args: url
{
	[self.pdfViewer closeViewer];
}

- (void)onClose
{
	[self.webView stringByEvaluatingJavaScriptFromString:@"window.plugins.PDFViewer.onClose();"];
}

- (NSString *)copyFileToDocumentDirectory:(NSString *)fileName {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString *documentDirPath = [documentsDir stringByAppendingPathComponent:fileName];
    NSArray *file = [fileName componentsSeparatedByString:@"."];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[file objectAtIndex:0] ofType:[file lastObject]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager fileExistsAtPath:documentDirPath];
    if (!success) {
        success = [fileManager copyItemAtPath:filePath toPath:documentDirPath error:&error];
        if (!success) {
            //            NSAssert1(0, @"Failed to create writable txt file file with message \                                          '%@'.", [error localizedDescription]);
        }
    }
    return documentDirPath;
}

#if !__has_feature(objc_arc)
	- (void)dealloc
	{
		self.pdfViewer = nil;

		[super dealloc];
	}

#endif

@end
