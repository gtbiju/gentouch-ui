//
//  PDFwebViewcontroller.m
//  MSales
//
//  Created by Dileep on 4/3/13.
//
//

#import "PDFwebViewcontroller.h"



@interface PDFwebViewcontroller ()

@end

@implementation PDFwebViewcontroller
@synthesize myWEBVIEW;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadPDFFromDocuments:(NSString *)pdfName
{
	NSLog(@"Opening Url : %@", pdfName);
    
    NSArray *arrComponent = [pdfName componentsSeparatedByString:@"/"];
    if (arrComponent.count > 0)
    {
        pdfName = [arrComponent lastObject];
        /*  NSURL *targetURL = [NSURL fileURLWithPath:pdfName];
         NSLog(@"targetURL:%@",targetURL);
         NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"file://localhost/Users/dileep/Library/Application Support/iPhone Simulator/6.1/Applications/3214B2EA-250C-4FEF-959A-8687D40AEBE6/Documents/whole_life_plan_leaflet.pdf"]];
         */
        
        NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSString *filepath = [path stringByAppendingPathComponent:pdfName];
        
        NSURLRequest *request = [[NSURLRequest alloc]initWithURL:[NSURL fileURLWithPath:filepath]];
        
        //Load the request in the UIWebView.
        [myWEBVIEW loadRequest:request];
    }
    
    

}

-(void)loadPDFFromLocal:(NSString *)pdfName
{
    
    if ([pdfName rangeOfString:@"/Documents"].location == NSNotFound)
    {
        NSURL *targetURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:pdfName ofType:nil]];
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        
        //Load the request in the UIWebView.
        [myWEBVIEW loadRequest:request];

    } else
    {
        [self loadPDFFromDocuments:pdfName];
    }
    
}
- (void)closeBrowser
{
	
    
	if ([self respondsToSelector:@selector(presentingViewController)]) {
		// Reference UIViewController.h Line:179 for update to iOS 5 difference - @RandyMcMillan
		[[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
	} else {
		[[self parentViewController] dismissModalViewControllerAnimated:YES];
	}
}
- (IBAction)onDoneButtonPress:(id)sender
{
	[self closeBrowser];
}
-(void)dealloc
{
    [super dealloc];
    [myWEBVIEW release];
}

@end
