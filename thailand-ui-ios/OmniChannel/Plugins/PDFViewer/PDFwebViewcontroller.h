//
//  PDFwebViewcontroller.h
//  MSales
//
//  Created by Dileep on 4/3/13.
//
//

#import <UIKit/UIKit.h>

@interface PDFwebViewcontroller : UIViewController<UIWebViewDelegate>
{
   IBOutlet UIWebView *myWEBVIEW;
    
}
@property(nonatomic,retain)IBOutlet UIWebView *myWEBVIEW;

- (void)loadPDFFromDocuments:(NSString *)pdfName;
-(void)loadPDFFromLocal:(NSString *)pdfName;
- (IBAction)onDoneButtonPress:(id)sender;
@end
