// PDFViewer based on ChildBrowser

//  Created by Jesse MacFadyen on 10-05-29.
//  Copyright 2010 Nitobi. All rights reserved.
//  Copyright 2012, Randy McMillan

#import <Cordova/CDVPlugin.h>
#import "PDFViewerViewController.h"
#import "PDFwebViewcontroller.h"

@interface PdfViewer : CDVPlugin <PDFViewerDelegate>{}

@property (nonatomic, strong) PDFViewerViewController *pdfViewer;

- (void)showPdf:(NSMutableArray *)arguments withDict:(NSMutableDictionary *)options;
- (void)showPdfFromBase64:(CDVInvokedUrlCommand*)command;
- (NSString *)copyFileToDocumentDirectory:(NSString *)fileName;
- (void)showPdf:(CDVInvokedUrlCommand*)command;

@property (nonatomic, strong) PDFwebViewcontroller *pdfCnt;

@end
