//
//  LEFileUtils.h
//  IllustratorPOC
//  version - 2.2.0.0
//  Created by InsMobility Mobility on 9/12/13.
//
//

#import <Cordova/CDV.h>

@interface LEFileUtils : CDVPlugin
{
    
}

-(void)copyFileFromPackage:(CDVInvokedUrlCommand *)command;

-(void)getAppFullPath:(CDVInvokedUrlCommand *)command;
- (void)getApplicationPath:(CDVInvokedUrlCommand *)command;
- (void)deleteFile:(CDVInvokedUrlCommand *)command;
- (void)renameFile:(CDVInvokedUrlCommand *)command;
- (void)isFileExists:(CDVInvokedUrlCommand *)command;
- (void)copyFile:(CDVInvokedUrlCommand *)command;
- (void)writeBase64AsFile:(CDVInvokedUrlCommand *)command;
- (void)readFileAsBase64:(CDVInvokedUrlCommand *)command;

@end
