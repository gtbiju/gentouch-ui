//
//  LEFileUtils.m
//  IllustratorPOC
//	version - 2.2.0.0
//  Created by InsMobility Mobility on 9/12/13.
//
//

#import "LEFileUtils.h"

@implementation LEFileUtils

-(void)getAppFullPath:(CDVInvokedUrlCommand *)command
{
    CDVPluginResult* pluginResult = nil;
    BOOL isError = NO;
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSLog(@"dest: %@", documentsDirectory);
    if (isError)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Error while getting path"];
    }
    else
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:documentsDirectory];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)copyFileFromPackage:(CDVInvokedUrlCommand *)command
{
    CDVPluginResult* pluginResult = nil;
    BOOL isError = NO;
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSLog(@"dest: %@", documentsDirectory);
    NSString *sourcePath;
    NSString *filePath;
    NSString *fileName;
    NSError *error;
    BOOL forceCopy;
    for (NSDictionary *fileDesc in command.arguments){
        
        
        forceCopy= [[fileDesc objectForKey:@"isForceCopyRequired"] intValue];
        fileName = [fileDesc objectForKey:@"fileName"];
        sourcePath = [[NSBundle mainBundle] pathForResource:fileName ofType: @""];
        
        // check file is in subdirectory
        NSArray *array = [fileName componentsSeparatedByString:@"/"];
        if (array.count > 1)
        {
            fileName = [fileName lastPathComponent];
        }
        
        filePath = [documentsDirectory stringByAppendingPathComponent:fileName];
        NSLog(@"sour: %@ -- dest: %@", sourcePath, filePath);
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSLog(@"exists?: %d", [fileManager fileExistsAtPath:filePath]);
        NSLog(@"forceCopy?: %d", forceCopy);
        if([fileManager fileExistsAtPath:filePath] && forceCopy)
        {
            NSLog(@"deleting file");
            [fileManager removeItemAtPath:filePath error:&error];
        }
        
        if(![fileManager fileExistsAtPath:filePath] && ![fileManager copyItemAtPath:sourcePath toPath:filePath error:&error])
        {
            isError = YES;
            NSLog(@"Error description-%@ \n", [error localizedDescription]);
            NSLog(@"Error reason-%@", [error localizedFailureReason]);
            break;
        }
        
        
    }
    
    
    if (isError) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Error while copying files"];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Files Copied Successfully"];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getApplicationPath:(CDVInvokedUrlCommand *)command
{
    
    CDVPluginResult* pluginResult = nil;
    BOOL isError = NO;
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSLog(@"dest: %@", documentsDirectory);
    if (isError)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Error while getting path"];
    }
    else
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:documentsDirectory];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
- (void)deleteFile:(CDVInvokedUrlCommand *)command
{
    CDVPluginResult* pluginResult = nil;
    BOOL isError = NO;
    
    NSString *directory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSDictionary *fileDesc = command.arguments[0];
    NSString *filePath =  [fileDesc objectForKey:@"filePath"];
    NSString *fileName;
	if(filePath != nil && [filePath length] != 0){
		NSString *tempFileName =  [directory stringByAppendingPathComponent:filePath];
		fileName = [tempFileName stringByAppendingPathComponent:[fileDesc objectForKey:@"fileName"]];
		
	}else{
		fileName =  [directory stringByAppendingPathComponent:[fileDesc objectForKey:@"fileName"]];
	}
	BOOL isErrorIfFileNotExists;
    if ( [fileDesc objectForKey:@"isErrorIfFileNotExists"] != nil) {
		isErrorIfFileNotExists = [[fileDesc objectForKey:@"isErrorIfFileNotExists"] intValue];
	} else {
		isErrorIfFileNotExists = YES;
	}
    if([[NSFileManager defaultManager] fileExistsAtPath:fileName]){
        
        if(![[NSFileManager defaultManager] removeItemAtPath:fileName error:nil]){
            
            isError = YES;
        }
    }
    else {
    	if (isErrorIfFileNotExists) {
        	isError = YES;
        }
    }
    
    if (isError)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[NSString stringWithFormat:@"File delete failed for file:%@",command.arguments[0]]];
    }
    else
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"success"];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
}
- (void)isFileExists:(CDVInvokedUrlCommand *)command{
    
    CDVPluginResult* pluginResult = nil;
    NSString *fileName = command.arguments[0];
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fullFilePath = [documentsPath stringByAppendingPathComponent:fileName];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:fullFilePath];
    
    if (fileExists) {
        
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"File exists"];
    }
    else
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"File not found"];
        
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
}
- (void)renameFile:(CDVInvokedUrlCommand *)command
{
    CDVPluginResult* pluginResult = nil;
    BOOL isError = NO;
    NSError *err = NULL;
    NSString *errorMessage = @"test";
    
    NSString *directory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSDictionary *fileDesc = command.arguments[0];
    NSString *oldFileName =  [directory stringByAppendingPathComponent:[fileDesc objectForKey:@"oldFileName"]];
    NSString *newFileName = [directory stringByAppendingPathComponent:[fileDesc objectForKey:@"newFileName"]];
    BOOL isErrorIfSourceFileNotExists;
    if ( [fileDesc objectForKey:@"isErrorIfSourceFileNotExists"] != nil) {
	    isErrorIfSourceFileNotExists = [[fileDesc objectForKey:@"isErrorIfSourceFileNotExists"] intValue];
	} else {
		isErrorIfSourceFileNotExists = YES;
	}
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:oldFileName]){
    
    	if([[NSFileManager defaultManager] fileExistsAtPath:newFileName]){
    		if(![fm removeItemAtPath:newFileName error:nil]){
    			NSLog(@"Error: %@", err);
            	isError = YES;
            	errorMessage =	[NSString stringWithFormat:@"Failed to remove existing destination file:%@",command.arguments[0]];
        	} else {
        		if(![fm moveItemAtPath:oldFileName toPath:newFileName error:&err]){
        			NSLog(@"Error: %@", err);
        			errorMessage = [NSString stringWithFormat:@"Failed to rename file:%@",command.arguments[0]];
        			isError = YES;
        		}        		
        	}
    	} else {
    		if(![fm moveItemAtPath:oldFileName toPath:newFileName error:&err]){
    			NSLog(@"Error: %@", err);
    			errorMessage = [NSString stringWithFormat:@"Failed to rename file:%@",command.arguments[0]];
    			isError = YES;
    		}
    	}
    } else {
    	if (isErrorIfSourceFileNotExists){
    		isError = YES;
        	errorMessage = [NSString stringWithFormat:@"Source file does not exists:%@",command.arguments[0]];
    	}
        
    }
    //[fm release];
    if (isError)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:errorMessage];
    }
    else
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"success"];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
}
- (void)copyFile:(CDVInvokedUrlCommand *)command
{
    CDVPluginResult* pluginResult = nil;
    BOOL isError = NO;
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSLog(@"dest: %@", documentsDirectory);
    
    NSDictionary *fileDesc = command.arguments[0];
    NSString *fileName = [fileDesc objectForKey:@"fileName"];
    //NSString *sourcePath = [[NSBundle mainBundle] pathForResource:fileName ofType: @""];
    NSString *srcPath = [fileDesc objectForKey:@"srcPath"];
    NSString *sourcePath = [srcPath stringByAppendingPathComponent:fileName];
    NSString *newName = [fileDesc objectForKey:@"newName"];
    NSString *destFolder = [fileDesc objectForKey:@"folderName"];
    NSError *error;
    
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:destFolder];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    
    NSString *destinationPath = [dataPath stringByAppendingPathComponent:newName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager copyItemAtPath:sourcePath toPath:destinationPath error:&error]) {
        isError = YES;
        NSLog(@"Error description-%@ \n", [error localizedDescription]);
        NSLog(@"Error reason-%@", [error localizedFailureReason]);
    }
    
    if (isError) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Error while copying files"];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Files Copied Successfully"];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
}

- (void)writeBase64AsFile:(CDVInvokedUrlCommand *)command
{
    CDVPluginResult* pluginResult = nil;
    NSDictionary *fileDesc = command.arguments[0];
    NSString *base64String = [[fileDesc objectForKey:@"base64String"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *folderName = [fileDesc objectForKey:@"destFolder"];
    NSError* error = nil;
    NSData *data = [NSData cdv_dataFromBase64String:base64String];
    
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    } else {
        NSLog(@"Data has loaded successfully.");
    }
    
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [pathArray objectAtIndex:0];
    NSString *fileName = [fileDesc objectForKey:@"fileName"];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:folderName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    
    NSString *filePath = [dataPath stringByAppendingPathComponent:fileName];
    BOOL isSuccess = [data writeToFile:filePath atomically:YES];
    
    if (isSuccess) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"File Write Successful"];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Error while writing file"];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)readFileAsBase64:(CDVInvokedUrlCommand *)command
{
    CDVPluginResult *pluginResult;
    NSDictionary *fileDesc = command.arguments[0];
    NSString *fileName = [fileDesc objectForKey:@"fileName"];
    
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [pathArray objectAtIndex:0];
    NSString *destFolder = [fileDesc objectForKey:@"destFolder"];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:destFolder];
    NSString *filePath = [dataPath stringByAppendingPathComponent:fileName];
    
    NSData * dataFromFile = [NSData dataWithContentsOfFile:filePath];
    NSString *base64 =[dataFromFile base64Encoding];
    
    if (base64) {
        NSLog(@"Base 64 %@", base64);
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:base64];
    } else{
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Error while reading file"];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
