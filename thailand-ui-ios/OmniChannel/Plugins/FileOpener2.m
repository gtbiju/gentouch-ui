/*
 The MIT License (MIT)
 
 Copyright (c) 2013 pwlin - pwlin05@gmail.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#import "FileOpener2.h"
#import <Cordova/CDV.h>

#import <QuartzCore/QuartzCore.h>
#import <MobileCoreServices/MobileCoreServices.h>

@implementation FileOpener2


- (void) open: (CDVInvokedUrlCommand*)command {
    
    NSString *base64String = [[command.arguments objectAtIndex:0] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    CDVViewController* cont = (CDVViewController*)[ super viewController ];
    
    //NSURL *url = [NSURL URLWithString:base64String];
    //NSData *pdfData = [NSData dataWithContentsOfURL:url];
    //NSString *originalString = [NSString stringWithFormat:@"test"];
   
    //NSLog([data base64EncodedString]);
    
    NSError* error = nil;
   // NSData *pdfData = [NSData dataWithContentsOfURL:base64String options:NSDataReadingUncached error:&error];
     NSData *data = [NSData cdv_dataFromBase64String:base64String];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
        //[error release];
    } else {
        NSLog(@"Data has loaded successfully.");
    }
    
   
    
    
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [pathArray objectAtIndex:0];
    NSString *filePath = [documentsDirectory  stringByAppendingPathComponent:@"myPDF.pdf"];
    NSLog(@"File Path: %@",filePath);
    BOOL isSuccess = [data writeToFile:filePath atomically:YES];
    if(isSuccess)
    {
        
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"myPDF.pdf"];
        NSURL *fileURL = [NSURL fileURLWithPath:filePath];
        
        self.controller = [UIDocumentInteractionController  interactionControllerWithURL:fileURL];
        self.controller.delegate = self;
        
        CGRect rect = CGRectMake(0, 0, 1000.0f, 150.0f);
        CDVPluginResult* pluginResult = nil;
        BOOL wasOpened = [self.controller presentOptionsMenuFromRect:rect inView:cont.view animated:NO];
        //presentOptionsMenuFromRect
        //presentOpenInMenuFromRect
        
        if(wasOpened) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: @""];
            //NSLog(@"Success");
        } else {
            NSDictionary *jsonObj = [ [NSDictionary alloc]
                                     initWithObjectsAndKeys :
                                     @"9", @"status",
                                     @"Could not handle UTI", @"message",
                                     nil
                                     ];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:jsonObj];
            //NSLog(@"Could not handle UTI");
        }
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
    }
}


@end
