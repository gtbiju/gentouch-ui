//
//  VideoPlayerViewController.m
//  VideoPlayer
//
//  Created by LifeEngage on 06/06/14.
//  Copyright (c) 2014 LifeEngage. All rights reserved.
//

#import "VideoPlayerViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import<AVFoundation/AVFoundation.h>


@interface VideoPlayerViewController ()

@end

@implementation VideoPlayerViewController
@synthesize source;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
   
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDirectory = [paths objectAtIndex:0];
    NSString *filePath = [docDirectory stringByAppendingPathComponent:source];
    
    if(!filePath){
        return;
    }
 
    NSURL *fileURL    =   [NSURL fileURLWithPath:filePath];
    
    
    MPMoviePlayerController *videoPlayer = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
    videoPlayer.controlStyle = MPMovieControlStyleFullscreen;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(videoFinished:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
    
    [videoPlayer.view setFrame: self.view.bounds];
    [self.view addSubview:videoPlayer.view];
    [videoPlayer play];
//    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
//    playerViewController.player = [AVPlayer playerWithURL:fileURL];
//    
//        [playerViewController.view setFrame: self.view.bounds];
//        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(videoFinished:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerViewController.player.currentItem];
//    
//        [self.view addSubview:playerViewController.view];
//    
//        [playerViewController.player play];
    
    
    //self.avPlayerController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    //[self presentViewController:self.avPlayerController animated:YES completion:nil];
    
    
        
       
   // }
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)doneButtonPressed{
    
     [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)videoFinished:(NSNotification*)aNotification{
    //int value = [[aNotification.userInfo valueForKey: MPMoviePlayerPlaybackDidFinishReasonUserInfoKey] intValue];
    //if (value == MPMovieFinishReasonUserExited) {
        [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
