//
//  SystemCheck.h
//  
//
//  Created by Sajith M on 19/01/15.
//
//

#import <Cordova/CDVPlugin.h>

@interface SystemCheck : CDVPlugin

- (void)RootCheck:(CDVInvokedUrlCommand*)command;
- (void)DeleteCheck:(CDVInvokedUrlCommand*)command;
@end
