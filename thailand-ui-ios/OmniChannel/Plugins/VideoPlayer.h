//
//  VideoPlayer.h
//  iSales
//
//  Created by LifeEngage on 06/06/14.
//
//

#import <Cordova/CDVPlugin.h>

@interface VideoPlayer : CDVPlugin

- (void)playVideo:(CDVInvokedUrlCommand*)command;

@end
