//
//  VideoPlayerViewController.h
//  VideoPlayer
//
//  Created by LifeEngage on 06/06/14.
//  Copyright (c) 2014 LifeEngage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoPlayerViewController : UIViewController
@property (nonatomic, strong) NSString* source;

@end
