


//
//  VideoPlayer.m
//  iSales
//
//  Created by LifeEngage on 06/06/14.
//
//

#import "VideoPlayer.h"
#import "VideoPlayerViewController.h"
#import <AVKit/AVKit.h>
#import<AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@implementation VideoPlayer

- (void)playVideo:(CDVInvokedUrlCommand*)command {
    if (command.arguments)
    {
        if (command.arguments.count > 0)
        {
            NSString* videoPath=(NSString *)[command.arguments objectAtIndex:0];
            NSString* theFileName = @"";
            theFileName = [videoPath lastPathComponent];
            //check the file path for assets
            if([videoPath rangeOfString:@"file:///android_asset"].location != NSNotFound)
            {
                NSError *error;
                NSString* bundlePath = [NSString stringWithFormat:@"www/media/%@",theFileName] ;
                NSString *file = [[NSBundle mainBundle] pathForResource:bundlePath ofType:nil];

                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *docDirectory = [paths objectAtIndex:0];
                NSString *filePath = [docDirectory stringByAppendingPathComponent:theFileName];
                
                
                if([[NSFileManager defaultManager] copyItemAtPath:file toPath:filePath error:&error]){
                    NSLog(@"File successfully copied");
                }
            }
            
            
            //Code added by Pratheesh
            
          
           // float deviceVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *docDirectory = [paths objectAtIndex:0];
            NSString *filePath = [docDirectory stringByAppendingPathComponent:theFileName];
            
            if(!filePath){
                return;
            }
            
            NSURL *fileURL    =   [NSURL fileURLWithPath:filePath];
            
           // if(deviceVersion >= 8.0){
            
            AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
            playerViewController.player = [AVPlayer playerWithURL:fileURL];
            
            [self.viewController presentViewController:playerViewController animated:YES completion:nil];
            [playerViewController.player play];
            
           // }
//            else{
//            //MVplayer
//                
//                VideoPlayerViewController *player = [[VideoPlayerViewController alloc] initWithNibName:@"VideoPlayerViewController"
//                                                     
//                                                                                                bundle:nil];
//                player.source = theFileName;
//                [self.viewController presentViewController:player animated:YES completion:nil];
//                
//                
//                
//            
//          
//            }
            
            
//            VideoPlayerViewController *player = [[VideoPlayerViewController alloc] initWithNibName:@"VideoPlayerViewController"
//               
//                                                                                            bundle:nil];
//            player.source = theFileName;
//            [self.viewController presentViewController:player animated:YES completion:nil];
            
            //[player release];

        }
        
    }
    else{
        return;
    }
}

-(void)videoFinished:(NSNotification*)aNotification{
    
}


@end
