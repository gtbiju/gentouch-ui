function GenProLife_Illustrations(inoutMap, successCB) {
  var protectionPeriodFinal, premiumPeriodFinal, sumInsuredFinal, basePremiumFinal, riderPremiumFinal, WPSumAssuredCover, CNCSumAssuredCover, ADBSumAssuredCover, ADBRCCSumAssuredCover, ADDSumAssuredCover, ADDRCCSumAssuredCover, AISumAssuredCover, AIRCCSumAssuredCover, PBSumAssuredCover, DDSumAssuredCover, HBSumAssuredCover, HSSumAssuredCover, PremiumWPCover, CNCPremiumCover, ADBPremiumCover, ADBRCCPremiumCover, ADDPremiumCover, ADDRCCPremiumCover, AIPremiumCover, AIRCCPremiumCover, PBPremiumCover, DDPremiumCover, HSPremiumCover, HBPremiumCover, totalPremiumFinal, totalLivingBenefit, totalTaxationBenefit, totalPremiumPaid, grandTotalBenefit, totalRatePercent, ADBSumAssured200Percent, ADDSumAssured60Percent, ADDSumAssured25Percent, ADDSumAssured200Percent, OPDSumAssuredCover, OPDPremiumCover, AISumAssured60Percent, AISumAssured50Percent, AISumAssured6Percent, AISumAssured2Percent, AISumAssured10Percent, AISumAssured3Percent, AISumAssured200Percent, secondrow, PremiumWPCover1, benefitTableData, illustrationTableData, riderPremium, uniqueRiderName, selectedRiderTbl, planCode, planName, marketableName, shortName, riderName, summaryTableData, col1, col2, col3, col4, col5, i, inoutMap, err;
  benefitTableData = [];
  illustrationTableData = [];
  riderPremium = [];
  uniqueRiderName = [];
  selectedRiderTbl = [];
  planCode = [];
  planName = [];
  marketableName = [];
  shortName = [];
  riderName = [];
  summaryTableData = [];
  col1 = [];
  col2 = [];
  col3 = [];
  col4 = [];
  col5 = [];
  GenProLifeRuleSet(inoutMap, function (arguments, _$param0, _$param1) {
    inoutMap = _$param0;
    err = _$param1;
    if (inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
      protectionPeriodFinal = sub(85, inoutMap.InsuredDetails.age);
      premiumPeriodFinal = inoutMap.PolicyDetails.premiumPayingTerm;
      startDuration = Number(85);
    } else {
      protectionPeriodFinal = sub(90, inoutMap.InsuredDetails.age);
      premiumPeriodFinal = inoutMap.PolicyDetails.premiumPayingTerm;
      startDuration = Number(90);
    }
    sumInsuredFinal = inoutMap.PolicyDetails.sumAssured;
    basePremiumFinal = inoutMap.modalPremium;
    riderPremiumFinal = inoutMap.totalRiderPremium;
    WPSumAssuredCover = inoutMap.WPRiderSumAssured;
    inoutMap.WPRider.SumAssured = inoutMap.WPRiderSumAssured;
	CNCSumAssuredCover = inoutMap.CNCRider.sumAssured;
    ADBSumAssuredCover = inoutMap.ADBRider.sumAssured;
    ADBRCCSumAssuredCover = inoutMap.ADBRider.sumAssured;
    ADDSumAssuredCover = inoutMap.ADDRiders.sumAssured;
    ADDRCCSumAssuredCover = inoutMap.ADDRiders.sumAssured;
    AISumAssuredCover = inoutMap.AIRider.sumAssured;
    AIRCCSumAssuredCover = inoutMap.AIRider.sumAssured;
    PBSumAssuredCover = inoutMap.PBRider.sumAssured;
    DDSumAssuredCover = inoutMap.DDRider.sumAssured;
    HBSumAssuredCover = inoutMap.HBRiders.sumAssured;
    HSSumAssuredCover = inoutMap.HSRiders.sumAssured;
    OPDSumAssuredCover = inoutMap.OPDRiders.sumAssured;
    PremiumWPCover = inoutMap.PremiumWP;
	CNCPremiumCover = inoutMap.CNCPremium;
    ADBPremiumCover = inoutMap.ADBPremium;
    ADBRCCPremiumCover = inoutMap.ADBRCCPremium;
    ADDPremiumCover = inoutMap.ADDPremium;
    ADDRCCPremiumCover = inoutMap.ADDRCCPremium;
    AIPremiumCover = inoutMap.AIPremium;
    AIRCCPremiumCover = inoutMap.AIRCCPremium;
    PBPremiumCover = inoutMap.PBPremium;
    DDPremiumCover = inoutMap.DDPremium;
    HSPremiumCover = inoutMap.HSPremium;
    OPDPremiumCover = inoutMap.OPDPremium;
    HBPremiumCover = inoutMap.HBPremium;
    totalPremiumFinal = inoutMap.totalPolicyPremium;
    Counter = Number(0);
    if (inoutMap.CNCRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
	if (inoutMap.WPRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.PBRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.ADBRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.RCCADBRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.ADDRiders.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.RCCADDRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.AIRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.RCCAIRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.HBRiders.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.HSRiders.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.OPDRiders.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.DDRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (PremiumWPCover == Number(0) && Number(inoutMap.PolicyDetails.sumAssured) <= Number(4000000)) {
      PremiumWPCover1 = PremiumWPCover;
    } else {
      PremiumWPCover1 = PremiumWPCover;
    }
    i = 0;
    while (i <= sub(Counter, 1)) {	  
	  if (inoutMap.CNCRider.isRequired == 'Yes') {
        riderPremium[i] = CNCPremiumCover;
        uniqueRiderName[i] = inoutMap.CNCRider.uniqueRiderName;
        planCode[i] = '9415';
        planName[i] = 'Cancer Rider';
        marketableName[i] = 'Cancer Rider';
        shortName[i] = 'Cancer Rider';
        col1[i] = 'CNC Illustration page';
        col4[i] = inoutMap.CNCRider.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.WPRider.isRequired == 'Yes') {
        riderPremium[i] = PremiumWPCover;
        uniqueRiderName[i] = inoutMap.WPRider.uniqueRiderName;
        planCode[i] = '9440';
        planName[i] = 'WP';
        marketableName[i] = 'WP';
        shortName[i] = 'WP';
        col1[i] = 'WP Illustration page';
        col4[i] = inoutMap.WPRider.SumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.PBRider.isRequired == 'Yes') {
        riderPremium[i] = PBPremiumCover;
        uniqueRiderName[i] = inoutMap.PBRider.uniqueRiderName;
        planCode[i] = '9430';
        planName[i] = 'PB';
        marketableName[i] = 'PB';
        shortName[i] = 'PB';
        col1[i] = 'PB Illustration page';
        col4[i] = inoutMap.PBRider.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.ADBRider.isRequired == 'Yes') {
        riderPremium[i] = ADBPremiumCover;
        uniqueRiderName[i] = inoutMap.ADBRider.uniqueRiderName;
        planCode[i] = '9910';
        planName[i] = 'ADB';
        marketableName[i] = 'ADB';
        shortName[i] = 'ADB';
        col1[i] = 'ADB Illustration page';
        col4[i] = inoutMap.ADBRider.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.RCCADBRider.isRequired == 'Yes') {
        riderPremium[i] = ADBRCCPremiumCover;
        uniqueRiderName[i] = inoutMap.RCCADBRider.uniqueRiderName;
        planCode[i] = '9911';
        planName[i] = 'RCC ADB';
        marketableName[i] = 'RCC ADB';
        shortName[i] = 'RCC attached with ADB';
        col1[i] = 'ADBRCC Illustration page';
        col4[i] = inoutMap.ADBRider.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.ADDRiders.isRequired == 'Yes') {
        riderPremium[i] = ADDPremiumCover;
        uniqueRiderName[i] = inoutMap.ADDRiders.uniqueRiderName;
        planCode[i] = '9920';
        planName[i] = 'ADD';
        marketableName[i] = 'ADD';
        shortName[i] = 'ADD';
        col1[i] = 'ADD Illustration page';
        col4[i] = inoutMap.ADDRiders.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.RCCADDRider.isRequired == 'Yes') {
        riderPremium[i] = ADDRCCPremiumCover;
        uniqueRiderName[i] = inoutMap.RCCADDRider.uniqueRiderName;
        planCode[i] = '9921';
        planName[i] = 'RCC ADD';
        marketableName[i] = 'RCC ADD';
        shortName[i] = 'RCC attached with ADD';
        col1[i] = 'ADDRCC Illustration page';
        col4[i] = inoutMap.ADDRiders.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.AIRider.isRequired == 'Yes') {
        riderPremium[i] = AIPremiumCover;
        uniqueRiderName[i] = inoutMap.AIRider.uniqueRiderName;
        planCode[i] = '9930';
        planName[i] = 'AI';
        marketableName[i] = 'AI';
        shortName[i] = 'AI';
        col1[i] = 'AI Illustration page';
        col4[i] = inoutMap.AIRider.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.RCCAIRider.isRequired == 'Yes') {
        riderPremium[i] = AIRCCPremiumCover;
        uniqueRiderName[i] = inoutMap.RCCAIRider.uniqueRiderName;
        planCode[i] = '9931';
        planName[i] = 'RCC AI';
        marketableName[i] = 'RCC AI';
        shortName[i] = 'RCC attached with AI';
        col1[i] = 'AIRCC Illustration page';
        col4[i] = inoutMap.AIRider.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.HBRiders.isRequired == 'Yes') {
        riderPremium[i] = HBPremiumCover;
        uniqueRiderName[i] = inoutMap.HBRiders.uniqueRiderName;
        planCode[i] = '9941';
        planName[i] = 'HB(A)';
        marketableName[i] = 'HB(A)';
        shortName[i] = 'HB(A)';
        col1[i] = 'HB Illustration page';
        col4[i] = inoutMap.HBRiders.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.HSRiders.isRequired == 'Yes') {
        riderPremium[i] = HSPremiumCover;
        uniqueRiderName[i] = inoutMap.HSRiders.uniqueRiderName;
        planCode[i] = '9955';
        planName[i] = 'HS Extra';
        marketableName[i] = 'HS Extra';
        shortName[i] = 'HS Extra';
        col1[i] = 'HS Illustration page';
        col4[i] = inoutMap.HSRiders.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.DDRider.isRequired == 'Yes') {
        riderPremium[i] = DDPremiumCover;
        uniqueRiderName[i] = inoutMap.DDRider.uniqueRiderName;
        planCode[i] = '9412';
        planName[i] = 'DD_2551';
        marketableName[i] = 'DD_2551';
        shortName[i] = 'Dread Disease TMO2551';
        col1[i] = 'DD Illustration page';
        col4[i] = inoutMap.DDRider.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.OPDRiders.isRequired == 'Yes') {
        riderPremium[i] = OPDPremiumCover;
        uniqueRiderName[i] = inoutMap.OPDRiders.uniqueRiderName;
        planCode[i] = '9960';
        planName[i] = 'OPD';
        marketableName[i] = 'OPD';
        shortName[i] = 'OPD';
        col1[i] = 'OPD Illustration page';
        col4[i] = inoutMap.OPDRiders.sumAssured;
        i = sum(i, 1);
      }
      i = i + 1;
    }
    if (premiumPeriodFinal == Number(25)) {
      secondrow = 'GenProLIfe 25';
    }
    if (premiumPeriodFinal == Number(20)) {
      secondrow = 'GenProLIfe 20';
    }
    i = 0;
    while (i <= sub(Counter, 1)) {
      rowRiderData = {};
      rowRiderData.uniqueRiderName = uniqueRiderName[i];
      rowRiderData.riderNameForPdf = col1[i];
      rowRiderData.riderSumAssured = col4[i];
      rowRiderData.riderPremium = riderPremium[i];
      rowRiderData.planCode = planCode[i];
      rowRiderData.planName = planName[i];
      rowRiderData.marketableName = marketableName[i];
      rowRiderData.shortName = shortName[i];
      selectedRiderTbl[i] = rowRiderData;
      i = i + 1;
    }
    i = 0;
    while (i <= sum(Counter, 3)) {
      if (i == Number(0)) {
        rowSummaryTableData = {};
        rowSummaryTableData.col1 = 'Insurance';
        rowSummaryTableData.col2 = '';
        rowSummaryTableData.col3 = '';
        rowSummaryTableData.col4 = '';
        rowSummaryTableData.col5 = '';
        summaryTableData[i] = rowSummaryTableData;
      }
      if (i == Number(1)) {
        rowSummaryTableData = {};
        rowSummaryTableData.col1 = secondrow;
        rowSummaryTableData.col2 = protectionPeriodFinal;
        rowSummaryTableData.col3 = premiumPeriodFinal;
        rowSummaryTableData.col4 = inoutMap.PolicyDetails.sumAssured;
        rowSummaryTableData.col5 = inoutMap.PolicyDetails.premium;
        summaryTableData[i] = rowSummaryTableData;
      }
      if (i > Number(2)) {
        rowSummaryTableData = {};
        rowSummaryTableData.col1 = col1[i - 3];
        rowSummaryTableData.col2 = '';
        rowSummaryTableData.col3 = '';
        rowSummaryTableData.col4 = col4[i - 3];
        rowSummaryTableData.col5 = riderPremium[i - 3];
        summaryTableData[i] = rowSummaryTableData;
      }
      if (i == Number(2)) {
        rowSummaryTableData = {};
        rowSummaryTableData.col1 = 'Additional contract';
        rowSummaryTableData.col2 = '';
        rowSummaryTableData.col3 = '';
        rowSummaryTableData.col4 = '';
        rowSummaryTableData.col5 = '';
        summaryTableData[i] = rowSummaryTableData;
      }
      if (i == sum(Counter, 3)) {
        rowSummaryTableData = {};
        rowSummaryTableData.col1 = 'Total premiums';
        rowSummaryTableData.col2 = '';
        rowSummaryTableData.col3 = '';
        rowSummaryTableData.col4 = '';
        rowSummaryTableData.col5 = inoutMap.totalPolicyPremium;
        summaryTableData[i] = rowSummaryTableData;
      }
      if (i > Number(2) && col1[i - 3] == 'WP Illustration page') {
        rowSummaryTableData = {};
        rowSummaryTableData.col1 = col1[i - 3];
        rowSummaryTableData.col2 = '';
        rowSummaryTableData.col3 = '';
        rowSummaryTableData.col4 = col4[i - 3];
        rowSummaryTableData.col5 = PremiumWPCover1;
        summaryTableData[i] = rowSummaryTableData;
      }
      i = i + 1;
    }
    i = 0;
    while (i <= sub(startDuration, inoutMap.InsuredDetails.age, 1)) {
      rowData = {};
      rowData.policyYear = inoutMap.benefitYear[i];
      rowData.age = inoutMap.benefitAge[i];
      rowData.cashValue = inoutMap.cashValue[i];
      rowData.rpuCash = inoutMap.rpuCash[i];
      rowData.rpuSA = inoutMap.rpuSA[i];
      rowData.extPeriodYear = inoutMap.extPeriodYear[i];
      rowData.extPeriodDay = inoutMap.extPeriodDay[i];
      rowData.etiCash = inoutMap.etiCash[i];
      rowData.etiSA = inoutMap.etiSA[i];
      illustrationTableData[i] = rowData;
      i = i + 1;
    }
    i = 0;
    while (i <= sub(startDuration, inoutMap.InsuredDetails.age)) {
      rowBenefitData = {};
      if (i < sub(startDuration, inoutMap.InsuredDetails.age)) {
        rowBenefitData.policyYear = inoutMap.benefitYear[i];
        rowBenefitData.age = inoutMap.benefitAge[i];
        rowBenefitData.annualisedPremium = inoutMap.benefitPremium[i];
        rowBenefitData.benefitRate = inoutMap.benefitCashRate[i];
        rowBenefitData.benefitAmount = inoutMap.benefitCashAmount[i];
        rowBenefitData.benefitTaxAmount = inoutMap.benefitTaxAmount[i];
        rowBenefitData.benefitLifeRate = inoutMap.benefitLifeRate[i];
        rowBenefitData.benefitLifeAmount = inoutMap.benefitLifeAmount[i];
      } else {
        rowBenefitData.policyYear = 'Included';
        rowBenefitData.age = '';
        rowBenefitData.annualisedPremium = inoutMap.sumOfPremium;
        rowBenefitData.benefitRate = inoutMap.sumOfRatePercent;
        rowBenefitData.benefitAmount = inoutMap.sumOfLivingBenefit;
        rowBenefitData.benefitTaxAmount = inoutMap.sumOfTaxBenefit;
        rowBenefitData.benefitLifeRate = '';
        rowBenefitData.benefitLifeAmount = '';
      }
      benefitTableData[i] = rowBenefitData;
      i = i + 1;
    }
    totalLivingBenefit = inoutMap.sumOfLivingBenefit;
    totalTaxationBenefit = inoutMap.sumOfTaxBenefit;
    totalPremiumPaid = inoutMap.sumOfPremium;
    grandTotalBenefit = inoutMap.totalBenefit;
    totalRatePercent = inoutMap.sumOfRatePercent;
    ADBSumAssured200Percent = formatDecimal(mul(Number(inoutMap.ADBRider.sumAssured), 2), 0);
    ADDSumAssured60Percent = formatDecimal(mul(Number(inoutMap.ADDRiders.sumAssured), 0.6), 0);
    ADDSumAssured25Percent = formatDecimal(mul(Number(inoutMap.ADDRiders.sumAssured), 0.25), 0);
    ADDSumAssured200Percent = formatDecimal(mul(Number(inoutMap.ADDRiders.sumAssured), 2), 0);
    AISumAssured60Percent = formatDecimal(mul(Number(inoutMap.AIRider.sumAssured), 0.6), 0);
    AISumAssured50Percent = formatDecimal(mul(Number(inoutMap.AIRider.sumAssured), 0.25), 0);
    AISumAssured6Percent = formatDecimal(mul(Number(inoutMap.AIRider.sumAssured), 0.006), 0);
    AISumAssured2Percent = formatDecimal(mul(Number(inoutMap.AIRider.sumAssured), 0.002), 0);
    AISumAssured10Percent = formatDecimal(mul(Number(inoutMap.AIRider.sumAssured), 0.1), 0);
    AISumAssured3Percent = formatDecimal(mul(Number(inoutMap.AIRider.sumAssured), 0.003), 0);
    AISumAssured200Percent = formatDecimal(mul(Number(inoutMap.AIRider.sumAssured), 2), 0);
    Gen_ProLife_BIGraph(inoutMap, function (arguments, _$param2, _$param3) {
      inoutMap = _$param2;
      err = _$param3;
      updateJSON(inoutMap, 'protectionPeriodFinal', protectionPeriodFinal);
      updateJSON(inoutMap, 'premiumPeriodFinal', premiumPeriodFinal);
      updateJSON(inoutMap, 'sumInsuredFinal', sumInsuredFinal);
      updateJSON(inoutMap, 'basePremiumFinal', basePremiumFinal);
      updateJSON(inoutMap, 'riderPremiumFinal', riderPremiumFinal);
      updateJSON(inoutMap, 'WPSumAssuredCover', WPSumAssuredCover);
	  updateJSON(inoutMap, 'CNCSumAssuredCover', CNCSumAssuredCover);
      updateJSON(inoutMap, 'ADBSumAssuredCover', ADBSumAssuredCover);
      updateJSON(inoutMap, 'ADBRCCSumAssuredCover', ADBRCCSumAssuredCover);
      updateJSON(inoutMap, 'ADDSumAssuredCover', ADDSumAssuredCover);
      updateJSON(inoutMap, 'ADDRCCSumAssuredCover', ADDRCCSumAssuredCover);
      updateJSON(inoutMap, 'AISumAssuredCover', AISumAssuredCover);
      updateJSON(inoutMap, 'AIRCCSumAssuredCover', AIRCCSumAssuredCover);
      updateJSON(inoutMap, 'PBSumAssuredCover', PBSumAssuredCover);
      updateJSON(inoutMap, 'DDSumAssuredCover', DDSumAssuredCover);
      updateJSON(inoutMap, 'HBSumAssuredCover', HBSumAssuredCover);
      updateJSON(inoutMap, 'HSSumAssuredCover', HSSumAssuredCover);
      updateJSON(inoutMap, 'PremiumWPCover', PremiumWPCover);
	  updateJSON(inoutMap, 'CNCPremiumCover', CNCPremiumCover);
      updateJSON(inoutMap, 'ADBPremiumCover', ADBPremiumCover);
      updateJSON(inoutMap, 'ADBRCCPremiumCover', ADBRCCPremiumCover);
      updateJSON(inoutMap, 'ADDPremiumCover', ADDPremiumCover);
      updateJSON(inoutMap, 'ADDRCCPremiumCover', ADDRCCPremiumCover);
      updateJSON(inoutMap, 'AIPremiumCover', AIPremiumCover);
      updateJSON(inoutMap, 'AIRCCPremiumCover', AIRCCPremiumCover);
      updateJSON(inoutMap, 'PBPremiumCover', PBPremiumCover);
      updateJSON(inoutMap, 'DDPremiumCover', DDPremiumCover);
      updateJSON(inoutMap, 'HSPremiumCover', HSPremiumCover);
      updateJSON(inoutMap, 'HBPremiumCover', HBPremiumCover);
      updateJSON(inoutMap, 'totalPremiumFinal', totalPremiumFinal);
      updateJSON(inoutMap, 'totalLivingBenefit', totalLivingBenefit);
      updateJSON(inoutMap, 'totalTaxationBenefit', totalTaxationBenefit);
      updateJSON(inoutMap, 'totalPremiumPaid', totalPremiumPaid);
      updateJSON(inoutMap, 'grandTotalBenefit', grandTotalBenefit);
      updateJSON(inoutMap, 'totalRatePercent', totalRatePercent);
      updateJSON(inoutMap, 'ADBSumAssured200Percent', ADBSumAssured200Percent);
      updateJSON(inoutMap, 'ADDSumAssured60Percent', ADDSumAssured60Percent);
      updateJSON(inoutMap, 'ADDSumAssured25Percent', ADDSumAssured25Percent);
      updateJSON(inoutMap, 'ADDSumAssured200Percent', ADDSumAssured200Percent);
      updateJSON(inoutMap, 'OPDSumAssuredCover', OPDSumAssuredCover);
      updateJSON(inoutMap, 'OPDPremiumCover', OPDPremiumCover);
      updateJSON(inoutMap, 'AISumAssured60Percent', AISumAssured60Percent);
      updateJSON(inoutMap, 'AISumAssured50Percent', AISumAssured50Percent);
      updateJSON(inoutMap, 'AISumAssured6Percent', AISumAssured6Percent);
      updateJSON(inoutMap, 'AISumAssured2Percent', AISumAssured2Percent);
      updateJSON(inoutMap, 'AISumAssured10Percent', AISumAssured10Percent);
      updateJSON(inoutMap, 'AISumAssured3Percent', AISumAssured3Percent);
      updateJSON(inoutMap, 'AISumAssured200Percent', AISumAssured200Percent);
      updateJSON(inoutMap, 'secondrow', secondrow);
      updateJSON(inoutMap, 'PremiumWPCover1', PremiumWPCover1);
      updateJSON(inoutMap, 'benefitTableData', benefitTableData);
      updateJSON(inoutMap, 'illustrationTableData', illustrationTableData);
      updateJSON(inoutMap, 'riderPremium', riderPremium);
      updateJSON(inoutMap, 'uniqueRiderName', uniqueRiderName);
      updateJSON(inoutMap, 'selectedRiderTbl', selectedRiderTbl);
      updateJSON(inoutMap, 'planCode', planCode);
      updateJSON(inoutMap, 'planName', planName);
      updateJSON(inoutMap, 'marketableName', marketableName);
      updateJSON(inoutMap, 'shortName', shortName);
      updateJSON(inoutMap, 'riderName', riderName);
      updateJSON(inoutMap, 'summaryTableData', summaryTableData);
      updateJSON(inoutMap, 'col1', col1);
      updateJSON(inoutMap, 'col2', col2);
      updateJSON(inoutMap, 'col3', col3);
      updateJSON(inoutMap, 'col4', col4);
      updateJSON(inoutMap, 'col5', col5);
      successCB(inoutMap);
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function GenProLifeRuleSet(inoutMap, successCB) {
    processRuleSet(inoutMap, ["Gen_Pro_Life_Premium_02", "Gen_Pro_Life_BFTable_CVTable_03", successCB]);
}
function Gen_ProLife_BIGraph(inoutMap, successCB) {
    var sumAssuredField;
    var premiumModeField;
    var insuredAgeField;
    var planCodeField;
    var planShortNameField;
    var insuredAge1Field;
    var insuredAge5Field;
    var insuredAge10Field;
    var insuredAge15Field;
    var insuredAge20Field;
    var insuredAge25Field;
    var sumAssured4PerField;
    var sumAssured8PerField;
    var sumAssured13PerField;
    var sumAssured16PerField;
    var sumAssured30PerField;
    var sumAssured130PerField;
    var sumAssured150PerField;
    var insuredAge0Field;
    sumAssuredField = inoutMap.PolicyDetails.sumAssured;
    premiumModeField = inoutMap.totalPolicyPremium;
    insuredAge0Field = inoutMap.InsuredDetails.age;
    insuredAge1Field = sum(inoutMap.InsuredDetails.age, 1);
    insuredAge5Field = sum(inoutMap.InsuredDetails.age, 5);
    insuredAge10Field = sum(inoutMap.InsuredDetails.age, 10);
    insuredAge15Field = sum(inoutMap.InsuredDetails.age, 15);
    insuredAge20Field = sum(inoutMap.InsuredDetails.age, 20);
    insuredAge25Field = sum(inoutMap.InsuredDetails.age, 25);
    sumAssured4PerField = formatDecimal(mul(inoutMap.PolicyDetails.sumAssured, 0.004), 0);
    sumAssured8PerField = formatDecimal(mul(inoutMap.PolicyDetails.sumAssured, 0.008), 0);
    sumAssured13PerField = formatDecimal(mul(inoutMap.PolicyDetails.sumAssured, 0.013), 0);
    sumAssured16PerField = formatDecimal(mul(inoutMap.PolicyDetails.sumAssured, 0.016), 0);
    sumAssured30PerField = formatDecimal(mul(inoutMap.PolicyDetails.sumAssured, 0.30), 0);
    sumAssured130PerField = formatDecimal(mul(inoutMap.PolicyDetails.sumAssured, 1.30), 0);
    sumAssured150PerField = formatDecimal(mul(inoutMap.PolicyDetails.sumAssured, 1.50), 0);
    if (inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
        planCodeField = "1620";
        planShortNameField = "GPL20";
    }
    if (inoutMap.PolicyDetails.premiumPayingTerm == Number(25)) {
        planCodeField = "1626";
        planShortNameField = "GPL25";
		if (Number(inoutMap.PolicyDetails.sumAssured) >= Number(200000) && Number(inoutMap.HSRiders.sumAssured) >= Number(2000) && inoutMap.PolicyDetails.premiumMode == 6001) {
		     planCodeField = "1627";
		}
    }
    updateJSON(inoutMap, 'sumAssuredField', sumAssuredField);
    updateJSON(inoutMap, 'premiumModeField', premiumModeField);
    updateJSON(inoutMap, 'insuredAgeField', insuredAgeField);
    updateJSON(inoutMap, 'planCodeField', planCodeField);
    updateJSON(inoutMap, 'planShortNameField', planShortNameField);
    updateJSON(inoutMap, 'insuredAge1Field', insuredAge1Field);
    updateJSON(inoutMap, 'insuredAge5Field', insuredAge5Field);
    updateJSON(inoutMap, 'insuredAge10Field', insuredAge10Field);
    updateJSON(inoutMap, 'insuredAge15Field', insuredAge15Field);
    updateJSON(inoutMap, 'insuredAge20Field', insuredAge20Field);
    updateJSON(inoutMap, 'insuredAge25Field', insuredAge25Field);
    updateJSON(inoutMap, 'sumAssured4PerField', sumAssured4PerField);
    updateJSON(inoutMap, 'sumAssured8PerField', sumAssured8PerField);
    updateJSON(inoutMap, 'sumAssured13PerField', sumAssured13PerField);
    updateJSON(inoutMap, 'sumAssured16PerField', sumAssured16PerField);
    updateJSON(inoutMap, 'sumAssured30PerField', sumAssured30PerField);
    updateJSON(inoutMap, 'sumAssured130PerField', sumAssured130PerField);
    updateJSON(inoutMap, 'sumAssured150PerField', sumAssured150PerField);
    updateJSON(inoutMap, 'insuredAge0Field', insuredAge0Field);
    successCB(inoutMap);
}
function Gen_Pro_Life_Premium_02(inoutMap, successCB) {
  var annPremium, monthPremium, quarPremium, semAnnPremium, annualisedPremium, CNCPremium, ADBPremium, ADBRCCPremium, ADDPremium, ADDRCCPremium, AIPremium, AIRCCPremium, PBPremium, PBSumAssured, DDPremium, HSPremium, HBPremium, DDPaymentPeriod, DDPremRate, HBPremRate, HSPremRate, totalRiderPremium, totalPolicyPremium, modalPremium, WPRiderSumAssured, PremiumWP, quarVar, OPDPremium, OPDPremRate, HSDetails, OPDDetails, i, inoutMap, err, PBPremRate;
  HSDetails = [];
  OPDDetails = [];
  ADBPremium = Number(0);
  ADBRCCPremium = Number(0);
  ADDPremium = Number(0);
  ADDRCCPremium = Number(0);
  AIPremium = Number(0);
  AIRCCPremium = Number(0);
  PBPremium = Number(0);
  PBSumAssured = Number(0);
  DDPremium = Number(0);
  HSPremium = Number(0);
  OPDPremium = Number(0);
  HBPremium = Number(0);
  CNCPremium = Number(0);
  totalRiderPremium = Number(0);
  totalPolicyPremium = Number(0);
  OccupationaLRuleUpdated1ProLife(inoutMap, function (arguments, _$param0, _$param1) {
    inoutMap = _$param0;
    err = _$param1;
    Gen_Pro_Life_LookUp_01(inoutMap, function (arguments, _$param2, _$param3) {
      inoutMap = _$param2;
      err = _$param3;
      monthVar = formatDecimal(mul(inoutMap.premRate, inoutMap.monthRate), 2);
      quarVar = formatDecimal(mul(inoutMap.premRate, inoutMap.quarRate), 2);
      semAnnVar = formatDecimal(mul(inoutMap.premRate, inoutMap.semiAnnRate), 2);
	  monthVarCNC = formatDecimal(mul(inoutMap.CNCPremRate, inoutMap.CNCMonthRate), 2);
	  quarVarCNC = formatDecimal(mul(inoutMap.CNCPremRate, inoutMap.CNCQuarRate), 2);
      semAnnVarCNC = formatDecimal(mul(inoutMap.CNCPremRate, inoutMap.CNCSemiAnnRate), 2);
      if (inoutMap.PolicyDetails.isSumAssured == 'Yes') {
        annPremium = formatDecimal(mul(inoutMap.premRate, inoutMap.PolicyDetails.sumAssured), 2);
        annPremium = formatDecimal(div(annPremium, 1000), 0);
        monthPremium = formatDecimal(mul(monthVar, inoutMap.PolicyDetails.sumAssured), 2);
        monthPremium = formatDecimal(div(monthPremium, 1000), 0);
        quarPremium = formatDecimal(mul(quarVar, inoutMap.PolicyDetails.sumAssured), 2);
        quarPremium = formatDecimal(div(quarPremium, 1000), 0);
        semAnnPremium = formatDecimal(mul(semAnnVar, inoutMap.PolicyDetails.sumAssured), 2);
        semAnnPremium = formatDecimal(div(semAnnPremium, 1000), 0);
        annualisedPremium = annPremium;
        modalPremium = annPremium;
        if (inoutMap.PolicyDetails.premiumMode == 6002) {
          annualisedPremium = mul(semAnnPremium, 2);
          modalPremium = semAnnPremium;
        }
        if (inoutMap.PolicyDetails.premiumMode == 6003) {
          annualisedPremium = mul(quarPremium, 4);
          modalPremium = quarPremium;
        }
        if (inoutMap.PolicyDetails.premiumMode == 6004) {
          annualisedPremium = mul(monthPremium, 12);
          modalPremium = monthPremium;
        }
        inoutMap.PolicyDetails.premium = modalPremium;
      } else {
        if (inoutMap.PolicyDetails.isPremium == 'Yes') {
          inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), inoutMap.premRate), 0);
          annualisedPremium = inoutMap.PolicyDetails.premium;
          modalPremium = inoutMap.PolicyDetails.premium;
          if (inoutMap.PolicyDetails.premiumMode == 6002) {
            inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), semAnnVar), 0);
            annualisedPremium = mul(inoutMap.PolicyDetails.premium, 2);
          }
          if (inoutMap.PolicyDetails.premiumMode == 6003) {
            inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), quarVar), 0);
            annualisedPremium = mul(inoutMap.PolicyDetails.premium, 4);
          }
          if (inoutMap.PolicyDetails.premiumMode == 6004) {
            inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), monthVar), 0);
            annualisedPremium = mul(inoutMap.PolicyDetails.premium, 12);
          }
        }
      }
      (function (_$cont) {
        if (inoutMap.WPRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age <= Number(55) && inoutMap.InsuredDetails.age >= Number(16)) {
          Gen_Pro_Life_WPPremium_05(inoutMap, function (arguments, _$param4, _$param5) {
            inoutMap = _$param4;
            err = _$param5;
            PremiumWP = inoutMap.PremiumWP1;
            WPRiderSumAssured = inoutMap.WPRiderSumAssured1;
            _$cont();
          }.bind(this, arguments));
        } else {
          PremiumWP = Number(0);
          WPRiderSumAssured = Number(0);
          _$cont();
        }
      }.bind(this)(function (_$err) {
        if (_$err !== undefined)
          return _$cont(_$err);
        if (inoutMap.ADBRider.isRequired == 'Yes') {
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
            ADBPremium = formatDecimal(mul(inoutMap.ADBRider.sumAssured, inoutMap.ADBPremRate), 2);
            ADBPremium = formatDecimal(div(ADBPremium, 1000), 0);
          } else {
            if (inoutMap.InsuredDetails.age <= Number(64) && inoutMap.PolicyDetails.premiumPayingTerm == Number(25)) {
              ADBPremium = formatDecimal(mul(inoutMap.ADBRider.sumAssured, inoutMap.ADBPremRate), 2);
              ADBPremium = formatDecimal(div(ADBPremium, 1000), 0);
            }
          }
        } else {
          ADBPremium = Number(0);
        }
        if (inoutMap.RCCADBRider.isRequired == 'Yes') {
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
            ADBRCCPremium = formatDecimal(mul(inoutMap.ADBRider.sumAssured, inoutMap.ADBRCCPremRate), 2);
            ADBRCCPremium = formatDecimal(div(ADBRCCPremium, 1000), 0);
          } else {
            if (inoutMap.InsuredDetails.age <= Number(64) && inoutMap.PolicyDetails.premiumPayingTerm == Number(25)) {
              ADBRCCPremium = formatDecimal(mul(inoutMap.ADBRider.sumAssured, inoutMap.ADBRCCPremRate), 2);
              ADBRCCPremium = formatDecimal(div(ADBRCCPremium, 1000), 0);
            }
          }
        } else {
          ADBRCCPremium = Number(0);
        }
        if (inoutMap.ADDRiders.isRequired == 'Yes') {
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
            ADDPremium = formatDecimal(mul(inoutMap.ADDRiders.sumAssured, inoutMap.ADDPremRate), 2);
            ADDPremium = formatDecimal(div(ADDPremium, 1000), 0);
          } else {
            if (inoutMap.InsuredDetails.age <= Number(64) && inoutMap.PolicyDetails.premiumPayingTerm == Number(25)) {
              ADDPremium = formatDecimal(mul(inoutMap.ADDRiders.sumAssured, inoutMap.ADDPremRate), 2);
              ADDPremium = formatDecimal(div(ADDPremium, 1000), 0);
            }
          }
        } else {
          ADDPremium = Number(0);
        }
        if (inoutMap.RCCADDRider.isRequired == 'Yes') {
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
            ADDRCCPremium = formatDecimal(mul(inoutMap.ADDRiders.sumAssured, inoutMap.ADDRCCPremRate), 2);
            ADDRCCPremium = formatDecimal(div(ADDRCCPremium, 1000), 0);
          } else {
            if (inoutMap.InsuredDetails.age <= Number(64) && inoutMap.PolicyDetails.premiumPayingTerm == Number(25)) {
              ADDRCCPremium = formatDecimal(mul(inoutMap.ADDRiders.sumAssured, inoutMap.ADDRCCPremRate), 2);
              ADDRCCPremium = formatDecimal(div(ADDRCCPremium, 1000), 0);
            }
          }
        } else {
          ADDRCCPremium = Number(0);
        }
        if (inoutMap.AIRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age >= Number(16)) {
          AIPremium = formatDecimal(mul(inoutMap.AIRider.sumAssured, inoutMap.AIPremRate), 2);
          AIPremium = formatDecimal(div(AIPremium, 1000), 0);
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
            AIPremium = formatDecimal(mul(inoutMap.AIRider.sumAssured, inoutMap.AIPremRate), 2);
            AIPremium = formatDecimal(div(AIPremium, 1000), 0);
          } else {
            if (inoutMap.InsuredDetails.age <= Number(64) && inoutMap.PolicyDetails.premiumPayingTerm == Number(25)) {
              AIPremium = formatDecimal(mul(inoutMap.AIRider.sumAssured, inoutMap.AIPremRate), 2);
              AIPremium = formatDecimal(div(AIPremium, 1000), 0);
            }
          }
        } else {
          AIPremium = Number(0);
        }
        if (inoutMap.RCCAIRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age >= Number(16)) {
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
            AIRCCPremium = formatDecimal(mul(inoutMap.AIRider.sumAssured, inoutMap.AIRCCPremRate), 2);
            AIRCCPremium = formatDecimal(div(AIRCCPremium, 1000), 0);
          } else {
            if (inoutMap.InsuredDetails.age <= Number(64) && inoutMap.PolicyDetails.premiumPayingTerm == Number(25)) {
              AIRCCPremium = formatDecimal(mul(inoutMap.AIRider.sumAssured, inoutMap.AIRCCPremRate), 2);
              AIRCCPremium = formatDecimal(div(AIRCCPremium, 1000), 0);
            }
          }
        } else {
          AIRCCPremium = Number(0);
        }
		if (inoutMap.CNCRider.isRequired == "Yes" && inoutMap.InsuredDetails.age <= 65) {			
			CNCPremium = formatDecimal(mul(inoutMap.CNCRider.sumAssured, inoutMap.CNCPremRate), 2);
			CNCPremium = formatDecimal(div(CNCPremium, 1000), 0);
			if (inoutMap.PolicyDetails.premiumMode == "6002") {
				CNCPremium = formatDecimal(mul(inoutMap.CNCRider.sumAssured, semAnnVarCNC), 2);
				CNCPremium = formatDecimal(div(CNCPremium, 1000), 0);
			}
			if (inoutMap.PolicyDetails.premiumMode == "6003") {
				CNCPremium = formatDecimal(mul(inoutMap.CNCRider.sumAssured, quarVarCNC), 2);
				CNCPremium = formatDecimal(div(CNCPremium, 1000), 0);
			}
			if (inoutMap.PolicyDetails.premiumMode == "6004") {
				CNCPremium = formatDecimal(mul(inoutMap.CNCRider.sumAssured, monthVarCNC), 2);
				CNCPremium = formatDecimal(div(CNCPremium, 1000), 0);
			}
		} else {
			CNCPremium = Number(0);
		}		
        (function (_$cont) {
          if (inoutMap.DDRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age <= Number(55) && inoutMap.InsuredDetails.age >= Number(17)) {
            DDPaymentPeriod = minOfNValues(inoutMap.PolicyDetails.premiumPayingTerm, sub(Number(60), inoutMap.InsuredDetails.age), Number(19));
            DDPremRate = 0;
            lookupAsJson('RATE_FACTOR', 'EXTN_RIDER_DD_PREM_TBL', 'PROD_ID=1004', 'INSURED_AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'INSURED_SEX=\'' + inoutMap.InsuredDetails.sex + '\'', 'PAYMENT_PERIOD=\'' + DDPaymentPeriod + '\'', function (arguments, _$param6, _$param7) {
              DDPremRate = _$param6;
              err = _$param7;
              DDPremium = formatDecimal(mul(inoutMap.DDRider.sumAssured, DDPremRate), 2);
              DDPremium = formatDecimal(div(DDPremium, 1000), 0);
              if (inoutMap.PolicyDetails.premiumMode == 6002) {
                DDRate = formatDecimal(mul(DDPremRate, inoutMap.semiAnnRate), 2);
                DDPremium = formatDecimal(mul(inoutMap.DDRider.sumAssured, DDRate), 2);
                DDPremium = formatDecimal(div(DDPremium, 1000), 0);
              }
              if (inoutMap.PolicyDetails.premiumMode == 6003) {
                DDRate = formatDecimal(mul(DDPremRate, inoutMap.quarRate), 2);
                DDPremium = formatDecimal(mul(inoutMap.DDRider.sumAssured, DDRate), 2);
                DDPremium = formatDecimal(div(DDPremium, 1000), 0);
              }
              if (inoutMap.PolicyDetails.premiumMode == 6004) {
                DDRate = formatDecimal(mul(DDPremRate, inoutMap.monthRate), 2);
                DDPremium = formatDecimal(mul(inoutMap.DDRider.sumAssured, DDRate), 2);
                DDPremium = formatDecimal(div(DDPremium, 1000), 0);
              }
              _$cont();
            }.bind(this, arguments));
          } else {
            DDPremium = Number(0);
            _$cont();
          }
        }.bind(this)(function (_$err) {
          if (_$err !== undefined)
            return _$cont(_$err);
          if (inoutMap.HBOccCode == 'IC') {
            HBOccCodeTableFetch = Number(4);
          } else {
            HBOccCodeTableFetch = inoutMap.HBOccCode;
          }
          if (inoutMap.HSOccCode == 'IC') {
            HSOccCodeTableFetch = Number(4);
          } else {
            HSOccCodeTableFetch = inoutMap.HSOccCode;
          }
          (function (_$cont) {
            if (inoutMap.HBRiders.isRequired == 'Yes' && inoutMap.InsuredDetails.age <= Number(59) && inoutMap.InsuredDetails.age >= Number(6)) {
              HBPremRate = 0;
              lookupAsJson('PREMIUM_RATE', 'EXTN_RIDER_HB_PREM_TBL', 'PROD_ID=1004', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + HBOccCodeTableFetch + '\'', 'START_AGE<=\'' + inoutMap.InsuredDetails.age + '\'', 'END_AGE>=\'' + inoutMap.InsuredDetails.age + '\'', 'PLAN_NUM=\'' + inoutMap.HBRiders.sumAssured + '\'', function (arguments, _$param8, _$param9) {
                HBPremRate = _$param8;
                err = _$param9;
                HBPremium = formatDecimal(Number(HBPremRate), 0);
                _$cont();
              }.bind(this, arguments));
            } else {
              HBPremium = Number(0);
              _$cont();
            }
          }.bind(this)(function (_$err) {
            if (_$err !== undefined)
              return _$cont(_$err);
            (function (_$cont) {
              if (inoutMap.HSRiders.isRequired == 'Yes') {
                HSPremRate = 0;
                lookupAsJson('PREMIUM_RATE', 'EXTN_RIDER_HS_PREM_TBL', 'PROD_ID=1004', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + HSOccCodeTableFetch + '\'', 'START_AGE<=\'' + inoutMap.InsuredDetails.age + '\'', 'END_AGE>=\'' + inoutMap.InsuredDetails.age + '\'', 'PLAN_NUM=\'' + inoutMap.HSRiders.sumAssured + '\'', 'SEX=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param10, _$param11) {
                  HSPremRate = _$param10;
                  err = _$param11;
                  HSPremium = formatDecimal(Number(HSPremRate), 0);
                  _$cont();
                }.bind(this, arguments));
              } else {
                HSPremium = Number(0);
                _$cont();
              }
            }.bind(this)(function (_$err) {
              if (_$err !== undefined)
                return _$cont(_$err);
              (function (_$cont) {
                if (inoutMap.OPDRiders.isRequired == 'Yes') {
                  OPDPremRate = 0;
                  lookupAsJson('PREMIUM_RATE', 'EXTN_RIDER_OPD_PREM_TBL', 'PROD_ID=1004', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + HSOccCodeTableFetch + '\'', 'START_AGE<=\'' + inoutMap.InsuredDetails.age + '\'', 'END_AGE>=\'' + inoutMap.InsuredDetails.age + '\'', 'PLAN_NUM=\'' + inoutMap.OPDRiders.sumAssured + '\'', 'SEX=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param12, _$param13) {
                    OPDPremRate = _$param12;
                    err = _$param13;
                    OPDPremium = formatDecimal(Number(OPDPremRate), 0);
                    _$cont();
                  }.bind(this, arguments));
                } else {
                  OPDPremium = Number(0);
                  _$cont();
                }
              }.bind(this)(function (_$err) {
                  if (_$err !== undefined)
                    return _$cont(_$err);
				(function (_$cont) {
                  if (inoutMap.PBRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age <= Number(15) && inoutMap.PayorDetails.age >= Number(20) && inoutMap.PayorDetails.age <= Number(55)) {
                    PBSumAssured = sum(Number(modalPremium), Number(PremiumWP), Number(ADBPremium), Number(ADDPremium), Number(AIPremium), Number(ADBRCCPremium), Number(ADDRCCPremium), Number(AIRCCPremium), Number(DDPremium), Number(HBPremium), Number(HSPremium), Number(OPDPremium), Number(CNCPremium));
                    inoutMap.PBRider.sumAssured = PBSumAssured;
                    PBPaymentPeriod = minOfNValues(inoutMap.PolicyDetails.premiumPayingTerm, sub(Number(25), inoutMap.InsuredDetails.age), sub(Number(60), inoutMap.PayorDetails.age));
                    PBPremRate = 0;
                    lookupAsJson('RATE_FACTOR', 'EXTN_RIDER_PB_PREM_TBL', 'PROD_ID=1004', 'INSURED_AGE=\'' + inoutMap.PayorDetails.age + '\'', 'INSURED_SEX=\'' + inoutMap.PayorDetails.sex + '\'', 'PAYMENT_PERIOD=\'' + PBPaymentPeriod + '\'', function (arguments, _$param14, _$param15) {
                      PBPremRate = _$param14;
                      err = _$param15;
                      PBPremium = formatDecimal(mul(PBSumAssured, PBPremRate), 2);
                      PBPremium = formatDecimal(div(PBPremium, 100), 0);
                      _$cont();
                    }.bind(this, arguments));
                  } else {
                    PBPremium = Number(0);
                    _$cont();
                  }
                }.bind(this)(function (_$err) {
                  if (_$err !== undefined)
                    return _$cont(_$err);	
				
                  totalRiderPremium = sum(Number(CNCPremium), Number(ADBPremium), Number(ADDPremium), Number(AIPremium), Number(ADBRCCPremium), Number(ADDRCCPremium), Number(AIRCCPremium), Number(DDPremium), Number(HBPremium), Number(HSPremium), Number(PremiumWP), PBPremium, Number(OPDPremium));
                  totalPolicyPremium = sum(totalRiderPremium, Number(modalPremium));
                  (function (_$cont) {
                    if (inoutMap.HSRiders.isRequired == 'Yes') {
                      if (inoutMap.HSRiders.sumAssured < Number(1000)) {
                        inoutMap.HSRiders.sumAssured = Number(1000);
                      }
                      HSDetails = 0;
                      lookupAsJson('HS_DETAILS', 'EXTN_RIDER_HS_PDF_TBL', 'PROD_ID=1004', 'PLAN_NUM=\'' + inoutMap.HSRiders.sumAssured + '\'', function (arguments, _$param16, _$param17) {
                        HSDetails = _$param16;
                        err = _$param17;
                        _$cont();
                      }.bind(this, arguments));
                    } else {
                      if (inoutMap.HSRiders.isRequired == 'No') {
                        i = 0;
                        while (i <= 8) {
                          HSDetails[i] = Number(0);
                          i = i + 1;
                        }
                      }
                      _$cont();
                    }
                  }.bind(this)(function (_$err) {
                    if (_$err !== undefined)
                      return _$cont(_$err);
                    (function (_$cont) {
                      if (inoutMap.OPDRiders.isRequired == 'Yes') {
                        if (inoutMap.OPDRiders.sumAssured < Number(1000)) {
                          inoutMap.OPDRiders.sumAssured = Number(1000);
                        }
                        OPDDetails = 0;
                        lookupAsJson('HS_DETAILS', 'EXTN_RIDER_HS_PDF_TBL', 'PROD_ID=1004', 'PLAN_NUM=\'' + inoutMap.OPDRiders.sumAssured + '\'', function (arguments, _$param18, _$param19) {
                          OPDDetails = _$param18;
                          err = _$param19;
                          _$cont();
                        }.bind(this, arguments));
                      } else {
                        i = 0;
                        while (i <= 2) {
                          OPDDetails[i] = Number(0);
                          i = i + 1;
                        }
                        _$cont();
                      }
                    }.bind(this)(function (_$err) {
						if (_$err !== undefined)
                          return _$cont(_$err);						
						updateJSON(inoutMap, 'annPremium', annPremium);
						updateJSON(inoutMap, 'monthPremium', monthPremium);
						updateJSON(inoutMap, 'quarPremium', quarPremium);
						updateJSON(inoutMap, 'semAnnPremium', semAnnPremium);
						updateJSON(inoutMap, 'annualisedPremium', annualisedPremium);
						updateJSON(inoutMap, 'ADBPremium', ADBPremium);
						updateJSON(inoutMap, 'ADBRCCPremium', ADBRCCPremium);
						updateJSON(inoutMap, 'ADDPremium', ADDPremium);
						updateJSON(inoutMap, 'ADDRCCPremium', ADDRCCPremium);
						updateJSON(inoutMap, 'AIPremium', AIPremium);
						updateJSON(inoutMap, 'AIRCCPremium', AIRCCPremium);
						updateJSON(inoutMap, 'PBPremium', PBPremium);
						updateJSON(inoutMap, 'PBSumAssured', PBSumAssured);
						updateJSON(inoutMap, 'DDPremium', DDPremium);
						updateJSON(inoutMap, 'HSPremium', HSPremium);
						updateJSON(inoutMap, 'HBPremium', HBPremium);
						updateJSON(inoutMap, 'DDPaymentPeriod', DDPaymentPeriod);
						updateJSON(inoutMap, 'DDPremRate', DDPremRate);
						updateJSON(inoutMap, 'HBPremRate', HBPremRate);
						updateJSON(inoutMap, 'HSPremRate', HSPremRate);
						updateJSON(inoutMap, 'totalRiderPremium', totalRiderPremium);
						updateJSON(inoutMap, 'totalPolicyPremium', totalPolicyPremium);
						updateJSON(inoutMap, 'modalPremium', modalPremium);
						updateJSON(inoutMap, 'WPRiderSumAssured', WPRiderSumAssured);
						updateJSON(inoutMap, 'PremiumWP', PremiumWP);
						updateJSON(inoutMap, 'quarVar', quarVar);
						updateJSON(inoutMap, 'CNCPremium', CNCPremium);
						updateJSON(inoutMap, 'OPDPremium', OPDPremium);								
						updateJSON(inoutMap, 'OPDPremRate', OPDPremRate);
						updateJSON(inoutMap, 'HSDetails', HSDetails);
						updateJSON(inoutMap, 'OPDDetails', OPDDetails);
						successCB(inoutMap);
					}.bind(this)));
				  }.bind(this)));
                }.bind(this)));
              }.bind(this)));
            }.bind(this)));            
          }.bind(this)));
        }.bind(this)));
      }.bind(this)));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function Gen_Pro_Life_BFTable_CVTable_03(inoutMap, successCB) {
    var sumOfLivingBenefit;
    var sumOfTaxBenefit;
    var sumOfPremium;
    var sumOfGrandBenefit;
    var sumOfRatePercent;
    var totalBenefit;
    var benefitYear = [];
    var benefitAge = [];
    var benefitPremium = [];
    var benefitTaxAmount = [];
    var benefitTaxDeduct = [];
    var benefitCashRate = [];
    var benefitCashAmount = [];
    var benefitLifeRate = [];
    var benefitLifeAmount = [];
    var cashValue = [];
    var rpuCash = [];
    var rpuSA = [];
    var extPeriodYear = [];
    var extPeriodDay = [];
    var etiCash = [];
    var etiSA = [];
    varMultiple = Number(1.1);
    sumOfLivingBenefit = Number(0);
    sumOfTaxBenefit = Number(0);
    sumOfPremium = Number(0);
    sumOfGrandBenefit = Number(0);
    sumOfRatePercent = Number(0);
    if (inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
        startDuration = Number(85);
    } else {
        startDuration = Number(90);
    }
    for (i = 0; i < sub(startDuration, inoutMap.InsuredDetails.age); i++) {
        benefitYear[i] = sum(i, 1);
        benefitAge[i] = sum(inoutMap.InsuredDetails.age, i, 1);
        if (benefitAge[i] < sum(inoutMap.InsuredDetails.age, inoutMap.PolicyDetails.premiumPayingTerm, 1)) {
            benefitPremium[i] = formatDecimal(Number(inoutMap.annualisedPremium), 0);
        } else {
            benefitPremium[i] = Number(0);
        }
        benefitCashRate[i] = formatDecimal(Number(inoutMap.PERate[i + 1]), 2) + "%";
        benefitCashAmount[i] = formatDecimal(div(inoutMap.PERate[i + 1], 100), 4);
        benefitCashAmount[i] = formatDecimal(mul(benefitCashAmount[i], inoutMap.PolicyDetails.sumAssured), 0);
        benefitLifeRate[i] = formatDecimal(Number(inoutMap.DBRate[i]), 2);
        benefitLifeRate[i] = benefitLifeRate[i] + "%";
        benefitLifeAmount[i] = formatDecimal(mul(div(inoutMap.DBRate[i], 100), inoutMap.PolicyDetails.sumAssured), 0);
        cashValue[i] = formatDecimal(div(mul(inoutMap.TCVXRate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
        rpuCash[i] = formatDecimal(div(mul(inoutMap.RPUCashRate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
        rpuSA[i] = formatDecimal(div(mul(inoutMap.RPURate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
        extPeriodYear[i] = inoutMap.ETIYR[i + 1];
        extPeriodDay[i] = inoutMap.ETIDAY[i + 1];
        etiCash[i] = formatDecimal(div(mul(inoutMap.ETICashRate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
        etiSA[i] = formatDecimal(div(mul(inoutMap.ETISARate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
        if (benefitAge[i] < sum(inoutMap.InsuredDetails.age, inoutMap.PolicyDetails.premiumPayingTerm, 1)) {
            if (inoutMap.annualisedPremium >= Number(100000)) {
                benefitTaxAmount[i] = formatDecimal(mul(div(inoutMap.PolicyDetails.taxRate, 100), Number(100000)), 0);
            } else {
                benefitTaxAmount[i] = formatDecimal(mul(div(inoutMap.PolicyDetails.taxRate, 100), benefitPremium[i]), 0);
            }
        } else {
            benefitTaxAmount[i] = Number(0);
        }
        sumOfLivingBenefit = formatDecimal(sum(sumOfLivingBenefit, benefitCashAmount[i]), 2);
        sumOfTaxBenefit = formatDecimal(sum(sumOfTaxBenefit, benefitTaxAmount[i]), 2);
        sumOfPremium = formatDecimal(sum(sumOfPremium, benefitPremium[i]), 2);
        sumOfRatePercent = formatDecimal(sum(sumOfRatePercent, benefitCashRate[i]), 2) + "%";
    }
    totalBenefit = formatDecimal(sub(sum(sumOfLivingBenefit, sumOfTaxBenefit), sumOfPremium), 2);
    updateJSON(inoutMap, 'sumOfLivingBenefit', sumOfLivingBenefit);
    updateJSON(inoutMap, 'sumOfTaxBenefit', sumOfTaxBenefit);
    updateJSON(inoutMap, 'sumOfPremium', sumOfPremium);
    updateJSON(inoutMap, 'sumOfGrandBenefit', sumOfGrandBenefit);
    updateJSON(inoutMap, 'sumOfRatePercent', sumOfRatePercent);
    updateJSON(inoutMap, 'totalBenefit', totalBenefit);
    updateJSON(inoutMap, 'benefitYear', benefitYear);
    updateJSON(inoutMap, 'benefitAge', benefitAge);
    updateJSON(inoutMap, 'benefitPremium', benefitPremium);
    updateJSON(inoutMap, 'benefitTaxAmount', benefitTaxAmount);
    updateJSON(inoutMap, 'benefitTaxDeduct', benefitTaxDeduct);
    updateJSON(inoutMap, 'benefitCashRate', benefitCashRate);
    updateJSON(inoutMap, 'benefitCashAmount', benefitCashAmount);
    updateJSON(inoutMap, 'benefitLifeRate', benefitLifeRate);
    updateJSON(inoutMap, 'benefitLifeAmount', benefitLifeAmount);
    updateJSON(inoutMap, 'cashValue', cashValue);
    updateJSON(inoutMap, 'rpuCash', rpuCash);
    updateJSON(inoutMap, 'rpuSA', rpuSA);
    updateJSON(inoutMap, 'extPeriodYear', extPeriodYear);
    updateJSON(inoutMap, 'extPeriodDay', extPeriodDay);
    updateJSON(inoutMap, 'etiCash', etiCash);
    updateJSON(inoutMap, 'etiSA', etiSA);
    successCB(inoutMap);
}
function OccupationaLRuleUpdated1ProLife(inoutMap, successCB) {
  var occupationalCode, BaseOccCode, CIOccCode, DDOccCode, WPOccCode, CNCOccCode, PBOccCode, AIOccCode, HBOccCode, HSOccCode, ADDOccCode, ADBOccCode, ADBRCCOccCode, ADDRCCOccCode, AIRCCOccCode, OccCode1D, OccCode2D, OccCode1IC, OccCode2IC, SumOccCode1, SumOccCode2, SumOccCode1A, SumOccCode2A, Option, OccCode1, OccCode2, inoutMap, err;
  OccCode1 = [];
  OccCode2 = [];
  SumOccCode1 = Number(0);
  SumOccCode2 = Number(0);
  OccCode1D = Number(0);
  OccCode2D = Number(0);
  OccCode1IC = Number(0);
  OccCode2IC = Number(0);
  Option = '';
  inoutMap.ADBOccCode1A = Number(0);
  inoutMap.ADBOccCode2A = Number(0);
  inoutMap.AIOccCode1A = Number(0);
  inoutMap.AIOccCode2A = Number(0);
  inoutMap.PBOccCode1A = Number(0);
  inoutMap.PBOccCode2A = Number(0);
  inoutMap.WPOccCode1A = Number(0);
  inoutMap.WPOccCode2A = Number(0);
  inoutMap.CNCOccCode1A = Number(0);
  inoutMap.CNCOccCode2A = Number(0);
  inoutMap.DDOccCode1A = Number(0);
  inoutMap.DDOccCode2A = Number(0);
  inoutMap.ADDOccCode1A = Number(0);
  inoutMap.ADDOccCode2A = Number(0);
  inoutMap.HBOccCode1A = Number(0);
  inoutMap.HBOccCode2A = Number(0);
  inoutMap.HSOccCode1A = Number(0);
  inoutMap.HSOccCode2A = Number(0);
  inoutMap.CIOccCode1A = Number(0);
  inoutMap.CIOccCode2A = Number(0);
  inoutMap.ADBRCCOccCode1A = Number(0);
  inoutMap.ADBRCCOccCode2A = Number(0);
  inoutMap.ADDRCCOccCode1A = Number(0);
  inoutMap.ADDRCCOccCode2A = Number(0);
  inoutMap.AIRCCOccCode1A = Number(0);
  inoutMap.AIRCCOccCode2A = Number(0);
  OccupationaLRuleBaseCodeProLife(inoutMap, function (arguments, _$param0, _$param1) {
    inoutMap = _$param0;
    err = _$param1;
    OccupationaLRuleHBOccCodeProLife(inoutMap, function (arguments, _$param2, _$param3) {
      inoutMap = _$param2;
      err = _$param3;
      OccupationaLRuleHSOccCodeProLife(inoutMap, function (arguments, _$param4, _$param5) {
        inoutMap = _$param4;
        err = _$param5;
        OccupationaLRuleADDCodeProLife(inoutMap, function (arguments, _$param6, _$param7) {
          inoutMap = _$param6;
          err = _$param7;
          OccupationaLRuleADBCodeProLife(inoutMap, function (arguments, _$param8, _$param9) {
            inoutMap = _$param8;
            err = _$param9;
            OccupationaLRuleAIOccCodeProLife(inoutMap, function (arguments, _$param10, _$param11) {
              inoutMap = _$param10;
              err = _$param11;
              OccupationaLRulePBOccCodeProLife(inoutMap, function (arguments, _$param12, _$param13) {
                inoutMap = _$param12;
                err = _$param13;
                OccupationaLRuleWBOccCodeProLife(inoutMap, function (arguments, _$param14, _$param15) {
                  inoutMap = _$param14;
                  err = _$param15;
                  OccupationaLRuleDDCodeProLife(inoutMap, function (arguments, _$param16, _$param17) {
                    inoutMap = _$param16;
                    err = _$param17;
                    OccupationaLRuleADBRCCCodeProLife(inoutMap, function (arguments, _$param18, _$param19) {
                      inoutMap = _$param18;
                      err = _$param19;
                      OccupationaLRuleADDRCCCodeProLife(inoutMap, function (arguments, _$param20, _$param21) {
                        inoutMap = _$param20;
                        err = _$param21;
                        OccupationaLRuleAIRCCCodeProLife(inoutMap, function (arguments, _$param22, _$param23) {
                          inoutMap = _$param22;
                          err = _$param23;
						  OccupationaLRuleCNCOccCodeProLife(inoutMap, function (arguments, _$param24, _$param25) {
                            inoutMap = _$param24;
                            err = _$param25;						  
							  if (inoutMap.ADBRCCOccCode1A == Number(1)) {
								inoutMap.ADBRCCOccCode1A = inoutMap.ADBOccCode1A;
							  }
							  if (inoutMap.ADBRCCOccCode2A == Number(1)) {
								inoutMap.ADBRCCOccCode2A = inoutMap.ADBOccCode2A;
							  }
							  if (inoutMap.ADDRCCOccCode1A == Number(1)) {
								inoutMap.ADDRCCOccCode1A = inoutMap.ADDOccCode1A;
							  }
							  if (inoutMap.ADDRCCOccCode2A == Number(1)) {
								inoutMap.ADDRCCOccCode2A = inoutMap.ADDOccCode2A;
							  }
							  if (inoutMap.AIRCCOccCode1A == Number(1)) {
								inoutMap.AIRCCOccCode1A = inoutMap.AIOccCode1A;
							  }
							  if (inoutMap.AIRCCOccCode2A == Number(1)) {
								inoutMap.AIRCCOccCode2A = inoutMap.AIOccCode2A;
							  }
							  SumOccCode2 = sum(inoutMap.BaseOccCode2A, inoutMap.CIOccCode2A, inoutMap.HBOccCode2A, inoutMap.HSOccCode2A, inoutMap.ADDOccCode2A, inoutMap.AIOccCode2A, inoutMap.WPOccCode2A, inoutMap.CNCOccCode2A, inoutMap.ADBOccCode2A, inoutMap.PBOccCode2A, inoutMap.DDOccCode2A);
							  SumOccCode1 = sum(inoutMap.BaseOccCode1A, inoutMap.CIOccCode1A, inoutMap.HBOccCode1A, inoutMap.HSOccCode1A, inoutMap.ADDOccCode1A, inoutMap.AIOccCode1A, inoutMap.WPOccCode1A, inoutMap.CNCOccCode1A, inoutMap.ADBOccCode1A, inoutMap.PBOccCode1A, inoutMap.DDOccCode1A);
							  SumOccCode2A = sum(inoutMap.ADBRCCOccCode2A, SumOccCode2, inoutMap.ADDRCCOccCode2A, inoutMap.AIRCCOccCode2A);
							  SumOccCode1A = sum(inoutMap.ADBRCCOccCode1A, SumOccCode1, inoutMap.ADDRCCOccCode1A, inoutMap.AIRCCOccCode1A);
							  OccCode1[1] = inoutMap.CIOccCode1A;
							  OccCode1[2] = inoutMap.DDOccCode1A;
							  OccCode1[3] = inoutMap.WPOccCode1A;							  
							  OccCode1[4] = inoutMap.PBOccCode1A;
							  OccCode1[5] = inoutMap.AIOccCode1A;
							  OccCode1[6] = inoutMap.HBOccCode1A;
							  OccCode1[7] = inoutMap.HSOccCode1A;
							  OccCode1[8] = inoutMap.ADDOccCode1A;
							  OccCode1[9] = inoutMap.ADBOccCode1A;
							  OccCode1[10] = inoutMap.ADBRCCOccCode1A;
							  OccCode1[11] = inoutMap.ADDRCCOccCode1A;
							  OccCode1[12] = inoutMap.AIRCCOccCode1A;
							  OccCode1[13] = inoutMap.CNCOccCode1A;
							  OccCode2[1] = inoutMap.CIOccCode2A;
							  OccCode2[2] = inoutMap.DDOccCode2A;
							  OccCode2[3] = inoutMap.WPOccCode2A;
							  OccCode2[4] = inoutMap.PBOccCode2A;
							  OccCode2[5] = inoutMap.AIOccCode2A;
							  OccCode2[6] = inoutMap.HBOccCode2A;
							  OccCode2[7] = inoutMap.HSOccCode2A;
							  OccCode2[8] = inoutMap.ADDOccCode2A;
							  OccCode2[9] = inoutMap.ADBOccCode2A;
							  OccCode2[10] = inoutMap.ADBRCCOccCode2A;
							  OccCode2[11] = inoutMap.ADDRCCOccCode2A;
							  OccCode2[12] = inoutMap.AIRCCOccCode2A;
							  OccCode1[13] = inoutMap.CNCOccCode2A;
							  if (inoutMap.BaseOccCode1A >= Number(10)) {
								Option = 'A';
							  } else {
								if (inoutMap.BaseOccCode2A >= Number(10)) {
								  Option = 'B';
								}
							  }
							  i = 1;
							  while (i < 13) {
								if (OccCode1[i] == Number(10)) {
								  OccCode1D = sum(OccCode1[i], OccCode1D);
								}
								if (OccCode2[i] == Number(10)) {
								  OccCode2D = sum(OccCode2[i], OccCode2D);
								}
								if (OccCode1[i] == Number(5)) {
								  OccCode1IC = sum(OccCode2[i], OccCode1IC);
								}
								if (OccCode2[i] == Number(5)) {
								  OccCode2IC = sum(OccCode2[i], OccCode2IC);
								}
								i++;
							  }
							  if (Number(SumOccCode1A) == Number(SumOccCode2A) && OccCode1D == OccCode2D && OccCode1IC == OccCode2IC && isEmptyOrNull(Option) == Boolean(true)) {
								if (Number(inoutMap.BaseOccCode1A) >= Number(inoutMap.BaseOccCode2A)) {
								  Option = 'A';
								} else {
								  Option = 'B';
								}
							  }
							  if (isEmptyOrNull(Option) == Boolean(true)) {
								if (OccCode1D > OccCode2D) {
								  Option = 'A';
								} else {
								  if (OccCode1D < OccCode2D) {
									Option = 'B';
								  }
								}
							  }
							  if (isEmptyOrNull(Option) == Boolean(true)) {
								if (OccCode1IC > OccCode2IC) {
								  Option = 'A';
								} else {
								  if (OccCode1IC < OccCode2IC) {
									Option = 'B';
								  }
								}
							  }
							  if (isEmptyOrNull(Option) == Boolean(true)) {
								if (Number(SumOccCode1A) > Number(SumOccCode2A)) {
								  Option = 'A';
								} else {
								  if (Number(SumOccCode1A) < Number(SumOccCode2A)) {
									Option = 'B';
								  }
								}
							  }
							  if (Option == 'A') {
								BaseOccCode = inoutMap.BaseOccCode1;
								CIOccCode = inoutMap.CIOccCode1;
								DDOccCode = inoutMap.DDOccCode1;
								WPOccCode = inoutMap.WPOccCode1;
								CNCOccCode = inoutMap.CNCOccCode1;
								PBOccCode = inoutMap.PBOccCode1;
								AIOccCode = inoutMap.AIOccCode1;
								HBOccCode = inoutMap.HBOccCode1;
								HSOccCode = inoutMap.HSOccCode1;
								ADDOccCode = inoutMap.ADDOccCode1;
								ADBOccCode = inoutMap.ADBOccCode1;
								ADBRCCOccCode = inoutMap.ADBRCCOccCode1;
								ADDRCCOccCode = inoutMap.ADDRCCOccCode1;
								AIRCCOccCode = inoutMap.AIRCCOccCode1;
								occupationalCode = inoutMap.PolicyDetails.occCode1;
							  } else {
								BaseOccCode = inoutMap.BaseOccCode2;
								CIOccCode = inoutMap.CIOccCode2;
								DDOccCode = inoutMap.DDOccCode2;
								WPOccCode = inoutMap.WPOccCode2;
								CNCOccCode = inoutMap.CNCOccCode2;
								PBOccCode = inoutMap.PBOccCode2;
								AIOccCode = inoutMap.AIOccCode2;
								HBOccCode = inoutMap.HBOccCode2;
								HSOccCode = inoutMap.HSOccCode2;
								ADDOccCode = inoutMap.ADDOccCode2;
								ADBOccCode = inoutMap.ADBOccCode2;
								ADBRCCOccCode = inoutMap.ADBRCCOccCode2;
								ADDRCCOccCode = inoutMap.ADDRCCOccCode2;
								AIRCCOccCode = inoutMap.AIRCCOccCode2;
								occupationalCode = inoutMap.PolicyDetails.occCode2;
							  }
							  updateJSON(inoutMap, 'occupationalCode', occupationalCode);
							  updateJSON(inoutMap, 'BaseOccCode', BaseOccCode);
							  updateJSON(inoutMap, 'CIOccCode', CIOccCode);
							  updateJSON(inoutMap, 'DDOccCode', DDOccCode);
							  updateJSON(inoutMap, 'WPOccCode', WPOccCode);
							  updateJSON(inoutMap, 'CNCOccCode', CNCOccCode);
							  updateJSON(inoutMap, 'PBOccCode', PBOccCode);
							  updateJSON(inoutMap, 'AIOccCode', AIOccCode);
							  updateJSON(inoutMap, 'HBOccCode', HBOccCode);
							  updateJSON(inoutMap, 'HSOccCode', HSOccCode);
							  updateJSON(inoutMap, 'ADDOccCode', ADDOccCode);
							  updateJSON(inoutMap, 'ADBOccCode', ADBOccCode);
							  updateJSON(inoutMap, 'ADBRCCOccCode', ADBRCCOccCode);
							  updateJSON(inoutMap, 'ADDRCCOccCode', ADDRCCOccCode);
							  updateJSON(inoutMap, 'AIRCCOccCode', AIRCCOccCode);
							  updateJSON(inoutMap, 'OccCode1D', OccCode1D);
							  updateJSON(inoutMap, 'OccCode2D', OccCode2D);
							  updateJSON(inoutMap, 'OccCode1IC', OccCode1IC);
							  updateJSON(inoutMap, 'OccCode2IC', OccCode2IC);
							  updateJSON(inoutMap, 'SumOccCode1', SumOccCode1);
							  updateJSON(inoutMap, 'SumOccCode2', SumOccCode2);
							  updateJSON(inoutMap, 'SumOccCode1A', SumOccCode1A);
							  updateJSON(inoutMap, 'SumOccCode2A', SumOccCode2A);
							  updateJSON(inoutMap, 'Option', Option);
							  updateJSON(inoutMap, 'OccCode1', OccCode1);
							  updateJSON(inoutMap, 'OccCode2', OccCode2);
							  successCB(inoutMap);
                          }.bind(this, arguments));
						}.bind(this, arguments));
                      }.bind(this, arguments));
                    }.bind(this, arguments));
                  }.bind(this, arguments));
                }.bind(this, arguments));
              }.bind(this, arguments));
            }.bind(this, arguments));
          }.bind(this, arguments));
        }.bind(this, arguments));
      }.bind(this, arguments));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function Gen_Pro_Life_LookUp_01(inoutMap, successCB) {
  var premRate, monthRate, quarRate, semiAnnRate, CNCPremRate, CNCSemiAnnRate, CNCQuarRate, CNCMonthRate, ADBPremRate, ADDPremRate, AIPremRate, ADBRCCPremRate, ADDRCCPremRate, AIRCCPremRate, DBRate, PERate, TCVXRate, RPUCashRate, RPURate, ETIYR, ETIDAY, ETICashRate, ETISARate, err;
  if (inoutMap.ADDOccCode == 'IC') {
    ADDOccCodeTableFetch = Number(4);
  } else {
    ADDOccCodeTableFetch = inoutMap.ADDOccCode;
  }
  if (inoutMap.ADBOccCode == 'IC') {
    ADBOccCodeTableFetch = Number(4);
  } else {
    ADBOccCodeTableFetch = inoutMap.ADBOccCode;
  }
  if (inoutMap.AIOccCode == 'IC') {
    AIOccCodeTableFetch = Number(4);
  } else {
    AIOccCodeTableFetch = inoutMap.AIOccCode;
  }
  monthRate = 0;
  lookupAsJson('PREMIUM_FREQ', 'EXTN_MODAL_PREM_TBL', 'PREMIUM_MODE=6004', 'PROD_ID=1004', function (arguments, _$param0, _$param1) {
    monthRate = _$param0;
    err = _$param1;
    quarRate = 0;
    lookupAsJson('PREMIUM_FREQ', 'EXTN_MODAL_PREM_TBL', 'PREMIUM_MODE=6003', 'PROD_ID=1004', function (arguments, _$param2, _$param3) {
      quarRate = _$param2;
      err = _$param3;
      semiAnnRate = 0;
      lookupAsJson('PREMIUM_FREQ', 'EXTN_MODAL_PREM_TBL', 'PREMIUM_MODE=6002', 'PROD_ID=1004', function (arguments, _$param4, _$param5) {
        semiAnnRate = _$param4;
        err = _$param5;		
		
		CNCPremRate = 0;
        lookupAsJson('CNC_PREM_RT', 'EXTN_CNC_CANCER_PREM_TBL', 'PROD_ID=1004', 'INS_AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'INS_SEX=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param6, _$param7) {
          CNCPremRate = _$param6;
          err = _$param7;
		
		
		CNCSemiAnnRate = 0;
        lookupAsJson('PREMIUM_FREQ', 'EXTN_CNC_MODAL_PREM_TBL', 'PREMIUM_MODE=6002', 'PROD_ID=1004', function (arguments, _$param8, _$param9) {
          CNCSemiAnnRate = _$param8;
          err = _$param9;
		  
		CNCQuarRate = 0;
        lookupAsJson('PREMIUM_FREQ', 'EXTN_CNC_MODAL_PREM_TBL', 'PREMIUM_MODE=6003', 'PROD_ID=1004', function (arguments, _$param10, _$param11) {
          CNCQuarRate = _$param10;
          err = _$param11;
		  
		CNCMonthRate = 0;
        lookupAsJson('PREMIUM_FREQ', 'EXTN_CNC_MODAL_PREM_TBL', 'PREMIUM_MODE=6004', 'PROD_ID=1004', function (arguments, _$param12, _$param13) {
          CNCMonthRate = _$param12;
          err = _$param13;
		
		
		
        ADBPremRate = 0;
        lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_ADB_PREM_TBL', 'PROD_ID=1004', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + ADBOccCodeTableFetch + '\'', function (arguments, _$param14, _$param15) {
          ADBPremRate = _$param14;
          err = _$param15;
          ADDPremRate = 0;
          lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_AI_PREM_TBL', 'PROD_ID=1004', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + ADDOccCodeTableFetch + '\'', function (arguments, _$param16, _$param17) {
            ADDPremRate = _$param16;
            err = _$param17;
            AIPremRate = 0;
            lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_ADD_PREM_TBL', 'PROD_ID=1004', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + AIOccCodeTableFetch + '\'', function (arguments, _$param18, _$param19) {
              AIPremRate = _$param18;
              err = _$param19;
              ADBRCCPremRate = 0;
              lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_RCC_ADB_PREM_TBL', 'PROD_ID=1004', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', function (arguments, _$param20, _$param21) {
                ADBRCCPremRate = _$param20;
                err = _$param21;
                ADDRCCPremRate = 0;
                lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_RCC_ADD_PREM_TBL', 'PROD_ID=1004', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', function (arguments, _$param22, _$param23) {
                  ADDRCCPremRate = _$param22;
                  err = _$param23;
                  AIRCCPremRate = 0;
                  lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_RCC_AI_PREM_TBL', 'PROD_ID=1004', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', function (arguments, _$param24, _$param25) {
                    AIRCCPremRate = _$param24;
                    err = _$param25;
                    (function (_$cont) {
                      if (inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
                        premRate = 0;
                        lookupAsJson('GPX', 'EXTN_TVVALUE_GENPRO_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', 'TERM= 0', function (arguments, _$param26, _$param27) {
                          premRate = _$param26;
                          err = _$param27;
                          PERate = 0;
                          lookupAsJson('PE', 'EXTN_TVVALUE_GENPRO_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param28, _$param29) {
                            PERate = _$param28;
                            err = _$param29;
                            DBRate = 0;
                            lookupAsJson('DB', 'EXTN_TVVALUE_GENPRO_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param30, _$param31) {
                              DBRate = _$param30;
                              err = _$param31;
                              TCVXRate = 0;
                              lookupAsJson('TCVX', 'EXTN_TVVALUE_GENPRO_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param32, _$param33) {
                                TCVXRate = _$param32;
                                err = _$param33;
                                RPUCashRate = 0;
                                lookupAsJson('RPU_CASH', 'EXTN_TVVALUE_GENPRO_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param34, _$param35) {
                                  RPUCashRate = _$param34;
                                  err = _$param35;
                                  RPURate = 0;
                                  lookupAsJson('RPU', 'EXTN_TVVALUE_GENPRO_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param36, _$param37) {
                                    RPURate = _$param36;
                                    err = _$param37;
                                    ETIYR = 0;
                                    lookupAsJson('ETI_YR', 'EXTN_TVVALUE_GENPRO_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param38, _$param39) {
                                      ETIYR = _$param38;
                                      err = _$param39;
                                      ETIDAY = 0;
                                      lookupAsJson('ETI_DAY', 'EXTN_TVVALUE_GENPRO_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param40, _$param41) {
                                        ETIDAY = _$param40;
                                        err = _$param41;
                                        ETICashRate = 0;
                                        lookupAsJson('ETI_CASH', 'EXTN_TVVALUE_GENPRO_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param42, _$param43) {
                                          ETICashRate = _$param42;
                                          err = _$param43;
                                          ETISARate = 0;
                                          lookupAsJson('ETI_SA', 'EXTN_TVVALUE_GENPRO_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param44, _$param45) {
                                            ETISARate = _$param44;
                                            err = _$param45;
                                            _$cont();
                                          }.bind(this, arguments));
                                        }.bind(this, arguments));
                                      }.bind(this, arguments));
                                    }.bind(this, arguments));
                                  }.bind(this, arguments));
                                }.bind(this, arguments));
                              }.bind(this, arguments));
                            }.bind(this, arguments));
                          }.bind(this, arguments));
                        }.bind(this, arguments));
                      } else {
                        premRate = 0;
                        lookupAsJson('GPX', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', 'TERM=\'0\'', function (arguments, _$param46, _$param47) {
                          premRate = _$param46;
                          err = _$param47;
                          PERate = 0;
                          lookupAsJson('PE', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param48, _$param49) {
                            PERate = _$param48;
                            err = _$param49;
                            DBRate = 0;
                            lookupAsJson('DB', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param50, _$param51) {
                              DBRate = _$param50;
                              err = _$param51;
                              TCVXRate = 0;
                              lookupAsJson('TCVX', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param52, _$param53) {
                                TCVXRate = _$param52;
                                err = _$param53;
                                RPUCashRate = 0;
                                lookupAsJson('RPU_CASH', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param54, _$param55) {
                                  RPUCashRate = _$param54;
                                  err = _$param55;
                                  RPURate = 0;
                                  lookupAsJson('RPU', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param56, _$param57) {
                                    RPURate = _$param56;
                                    err = _$param57;
                                    ETIYR = 0;
                                    lookupAsJson('ETI_YR', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param58, _$param59) {
                                      ETIYR = _$param58;
                                      err = _$param59;
                                      ETIDAY = 0;
                                      lookupAsJson('ETI_DAY', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param60, _$param61) {
                                        ETIDAY = _$param60;
                                        err = _$param61;
                                        ETICashRate = 0;
                                        lookupAsJson('ETI_CASH', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param62, _$param63) {
                                          ETICashRate = _$param62;
                                          err = _$param63;
                                          ETISARate = 0;
                                          lookupAsJson('ETI_SA', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param64, _$param65) {
                                            ETISARate = _$param64;
                                            err = _$param65;
                                            _$cont();
                                          }.bind(this, arguments));
                                        }.bind(this, arguments));
                                      }.bind(this, arguments));
                                    }.bind(this, arguments));
                                  }.bind(this, arguments));
                                }.bind(this, arguments));
                              }.bind(this, arguments));
                            }.bind(this, arguments));
                          }.bind(this, arguments));
                        }.bind(this, arguments));
                      }
                    }.bind(this)(function (_$err) {
                      if (_$err !== undefined)
                        return _$cont(_$err);
                      updateJSON(inoutMap, 'premRate', premRate);
                      updateJSON(inoutMap, 'monthRate', monthRate);
                      updateJSON(inoutMap, 'quarRate', quarRate);
                      updateJSON(inoutMap, 'semiAnnRate', semiAnnRate);
					  updateJSON(inoutMap, 'CNCPremRate', CNCPremRate);
					  updateJSON(inoutMap, 'CNCSemiAnnRate', CNCSemiAnnRate);
					  updateJSON(inoutMap, 'CNCQuarRate', CNCQuarRate);
					  updateJSON(inoutMap, 'CNCMonthRate', CNCMonthRate);					  
                      updateJSON(inoutMap, 'ADBPremRate', ADBPremRate);
                      updateJSON(inoutMap, 'ADDPremRate', ADDPremRate);
                      updateJSON(inoutMap, 'AIPremRate', AIPremRate);
                      updateJSON(inoutMap, 'ADBRCCPremRate', ADBRCCPremRate);
                      updateJSON(inoutMap, 'ADDRCCPremRate', ADDRCCPremRate);
                      updateJSON(inoutMap, 'AIRCCPremRate', AIRCCPremRate);
                      updateJSON(inoutMap, 'DBRate', DBRate);
                      updateJSON(inoutMap, 'PERate', PERate);
                      updateJSON(inoutMap, 'TCVXRate', TCVXRate);
                      updateJSON(inoutMap, 'RPUCashRate', RPUCashRate);
                      updateJSON(inoutMap, 'RPURate', RPURate);
                      updateJSON(inoutMap, 'ETIYR', ETIYR);
                      updateJSON(inoutMap, 'ETIDAY', ETIDAY);
                      updateJSON(inoutMap, 'ETICashRate', ETICashRate);
                      updateJSON(inoutMap, 'ETISARate', ETISARate);
                      successCB(inoutMap);
                    }.bind(this)));
                  }.bind(this, arguments));
                }.bind(this, arguments));
              }.bind(this, arguments));
            }.bind(this, arguments));
			}.bind(this, arguments));
          }.bind(this, arguments));
		  }.bind(this, arguments));
			}.bind(this, arguments));
          }.bind(this, arguments));
        }.bind(this, arguments));
      }.bind(this, arguments));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function Gen_Pro_Life_WPPremium_05(inoutMap, successCB) {
  var annPremiumWP, monthPremiumWP, quarPremiumWP, semAnnPremiumWP, PremiumWP1, CalculatedWPRiderSumAssured, WPRiderSumAssured1, monthVar, inoutMap, err, WPRate;
  if (inoutMap.PolicyDetails.sumAssured > Number(4000000)) {
    CalculatedWPRiderSumAssured = sub(inoutMap.WPRider.sumAssured, Number(4000000));
    WPRiderSumAssured1 = inoutMap.WPRider.sumAssured;
  } else {
    CalculatedWPRiderSumAssured = Number(0);
    WPRiderSumAssured1 = inoutMap.PolicyDetails.sumAssured;
  }
  Gen_Pro_Life_LookUp_01(inoutMap, function (arguments, _$param0, _$param1) {
    inoutMap = _$param0;
    err = _$param1;
    annPremiumWP = div(mul(inoutMap.premRate, CalculatedWPRiderSumAssured), 1000);
    monthVar = formatDecimal(mul(inoutMap.premRate, inoutMap.monthRate), 2);
    monthPremiumWP = formatDecimal(mul(monthVar, CalculatedWPRiderSumAssured), 2);
    monthPremiumWP = formatDecimal(div(monthPremiumWP, 1000), 0);
    quarVar = formatDecimal(mul(inoutMap.premRate, inoutMap.quarRate), 2);
    quarPremiumWP = formatDecimal(mul(quarVar, CalculatedWPRiderSumAssured), 2);
    quarPremiumWP = formatDecimal(div(quarPremiumWP, 1000), 0);
    semAnnVar = formatDecimal(mul(inoutMap.premRate, inoutMap.semiAnnRate), 2);
    semAnnPremiumWP = formatDecimal(mul(semAnnVar, CalculatedWPRiderSumAssured), 2);
    semAnnPremiumWP = formatDecimal(div(semAnnPremiumWP, 1000), 0);
    PremiumWP1 = annPremiumWP;
    if (inoutMap.PolicyDetails.premiumMode == 6002) {
      PremiumWP1 = semAnnPremiumWP;
    }
    if (inoutMap.PolicyDetails.premiumMode == 6003) {
      PremiumWP1 = quarPremiumWP;
    }
    if (inoutMap.PolicyDetails.premiumMode == 6004) {
      PremiumWP1 = monthPremiumWP;
    }
    Period = min(inoutMap.PolicyDetails.premiumPayingTerm, sub(60, inoutMap.InsuredDetails.age));
    WPRate = 0;
    lookupAsJson('RATE_FACTOR', 'EXTN_RIDER_WP_PREM_TBL', 'INSURED_AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'INSURED_SEX=\'' + inoutMap.InsuredDetails.sex + '\'', 'PAYMENT_PERIOD=\'' + Period + '\'', 'PROD_ID=1004', function (arguments, _$param2, _$param3) {
      WPRate = _$param2;
      err = _$param3;
      PremiumWP1 = formatDecimal(mul(PremiumWP1, WPRate), 2);
      PremiumWP1 = formatDecimal(div(PremiumWP1, 100), 0);
      updateJSON(inoutMap, 'annPremiumWP', annPremiumWP);
      updateJSON(inoutMap, 'monthPremiumWP', monthPremiumWP);
      updateJSON(inoutMap, 'quarPremiumWP', quarPremiumWP);
      updateJSON(inoutMap, 'semAnnPremiumWP', semAnnPremiumWP);
      updateJSON(inoutMap, 'PremiumWP1', PremiumWP1);
      updateJSON(inoutMap, 'CalculatedWPRiderSumAssured', CalculatedWPRiderSumAssured);
      updateJSON(inoutMap, 'WPRiderSumAssured1', WPRiderSumAssured1);
      updateJSON(inoutMap, 'monthVar', monthVar);
      successCB(inoutMap);
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function OccupationaLRuleBaseCodeProLife(inoutMap, successCB) {
  var BaseOccCode1, BaseOccCode2, BaseOccCode1A, BaseOccCode2A, err;
  BaseOccCode1A = Number(0);
  BaseOccCode1 = '';
  BaseOccCode2A = Number(0);
  BaseOccCode2 = '';
  BaseOccCode1 = 0;
  lookupAsJson('BASE_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param0, _$param1) {
    BaseOccCode1 = _$param0;
    err = _$param1;
    BaseOccCode2 = 0;
    lookupAsJson('BASE_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param2, _$param3) {
      BaseOccCode2 = _$param2;
      err = _$param3;
      if (BaseOccCode1 == 'D') {
        BaseOccCode1A = Number(10);
      }
      if (BaseOccCode1 == 'IC') {
        BaseOccCode1A = Number(5);
      }
      if (BaseOccCode1 == '4') {
        BaseOccCode1A = Number(4);
      }
      if (BaseOccCode1 == '3') {
        BaseOccCode1A = Number(3);
      }
      if (BaseOccCode1 == '2') {
        BaseOccCode1A = Number(2);
      }
      if (BaseOccCode1 == '1') {
        BaseOccCode1A = Number(1);
      }
      if (BaseOccCode2 == 'D') {
        BaseOccCode2A = Number(10);
      }
      if (BaseOccCode2 == 'IC') {
        BaseOccCode2A = Number(5);
      }
      if (BaseOccCode2 == '4') {
        BaseOccCode2A = Number(4);
      }
      if (BaseOccCode2 == '3') {
        BaseOccCode2A = Number(3);
      }
      if (BaseOccCode2 == '2') {
        BaseOccCode2A = Number(2);
      }
      if (BaseOccCode2 == '1') {
        BaseOccCode2A = Number(1);
      }
      updateJSON(inoutMap, 'BaseOccCode1', BaseOccCode1);
      updateJSON(inoutMap, 'BaseOccCode2', BaseOccCode2);
      updateJSON(inoutMap, 'BaseOccCode1A', BaseOccCode1A);
      updateJSON(inoutMap, 'BaseOccCode2A', BaseOccCode2A);
      successCB(inoutMap);
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function OccupationaLRuleHBOccCodeProLife(inoutMap, successCB) {
  var HBOccCode1, HBOccCode2, HBOccCode1A, HBOccCode2A, err;
  HBOccCode1 = '';
  HBOccCode2 = '';
  HBOccCode1A = Number(0);
  HBOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.HBRiders.isRequired == 'Yes') {
      HBOccCode1 = 0;
      lookupAsJson('HB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param0, _$param1) {
        HBOccCode1 = _$param0;
        err = _$param1;
        HBOccCode2 = 0;
        lookupAsJson('HB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param2, _$param3) {
          HBOccCode2 = _$param2;
          err = _$param3;
          if (HBOccCode1 == 'D') {
            HBOccCode1A = Number(10);
          }
          if (HBOccCode1 == 'IC') {
            HBOccCode1A = Number(5);
          }
          if (HBOccCode1 == '4') {
            HBOccCode1A = Number(4);
          }
          if (HBOccCode1 == '3') {
            HBOccCode1A = Number(3);
          }
          if (HBOccCode1 == '2') {
            HBOccCode1A = Number(2);
          }
          if (HBOccCode1 == '1') {
            HBOccCode1A = Number(1);
          }
          if (HBOccCode2 == 'D') {
            HBOccCode2A = Number(10);
          }
          if (HBOccCode2 == 'IC') {
            HBOccCode2A = Number(5);
          }
          if (HBOccCode2 == '4') {
            HBOccCode2A = Number(4);
          }
          if (HBOccCode2 == '3') {
            HBOccCode2A = Number(3);
          }
          if (HBOccCode2 == '2') {
            HBOccCode2A = Number(2);
          }
          if (HBOccCode2 == '1') {
            HBOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'HBOccCode1', HBOccCode1);
    updateJSON(inoutMap, 'HBOccCode2', HBOccCode2);
    updateJSON(inoutMap, 'HBOccCode1A', HBOccCode1A);
    updateJSON(inoutMap, 'HBOccCode2A', HBOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleHSOccCodeProLife(inoutMap, successCB) {
  var HSOccCode1, HSOccCode2, HSOccCode1A, HSOccCode2A, err;
  HSOccCode1 = '';
  HSOccCode2 = '';
  HSOccCode1A = Number(0);
  HSOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.HSRiders.isRequired == 'Yes') {
      HSOccCode1 = 0;
      lookupAsJson('HS_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param0, _$param1) {
        HSOccCode1 = _$param0;
        err = _$param1;
        HSOccCode2 = 0;
        lookupAsJson('HS_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param2, _$param3) {
          HSOccCode2 = _$param2;
          err = _$param3;
          if (HSOccCode1 == 'D') {
            HSOccCode1A = Number(10);
          }
          if (HSOccCode1 == 'IC') {
            HSOccCode1A = Number(5);
          }
          if (HSOccCode1 == '4') {
            HSOccCode1A = Number(4);
          }
          if (HSOccCode1 == '3') {
            HSOccCode1A = Number(3);
          }
          if (HSOccCode1 == '2') {
            HSOccCode1A = Number(2);
          }
          if (HSOccCode1 == '1') {
            HSOccCode1A = Number(1);
          }
          if (HSOccCode2 == 'D') {
            HSOccCode2A = Number(10);
          }
          if (HSOccCode2 == 'IC') {
            HSOccCode2A = Number(5);
          }
          if (HSOccCode2 == '4') {
            HSOccCode2A = Number(4);
          }
          if (HSOccCode2 == '3') {
            HSOccCode2A = Number(3);
          }
          if (HSOccCode2 == '2') {
            HSOccCode2A = Number(2);
          }
          if (HSOccCode2 == '1') {
            HSOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'HSOccCode1', HSOccCode1);
    updateJSON(inoutMap, 'HSOccCode2', HSOccCode2);
    updateJSON(inoutMap, 'HSOccCode1A', HSOccCode1A);
    updateJSON(inoutMap, 'HSOccCode2A', HSOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleADDCodeProLife(inoutMap, successCB) {
  var ADDOccCode1, ADDOccCode2, ADDOccCode1A, ADDOccCode2A, err;
  ADDOccCode1 = '';
  ADDOccCode2 = '';
  ADDOccCode1A = Number(0);
  ADDOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.ADDRiders.isRequired == 'Yes') {
      ADDOccCode1 = 0;
      lookupAsJson('ADD_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param0, _$param1) {
        ADDOccCode1 = _$param0;
        err = _$param1;
        ADDOccCode2 = 0;
        lookupAsJson('ADD_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param2, _$param3) {
          ADDOccCode2 = _$param2;
          err = _$param3;
          if (ADDOccCode1 == 'D') {
            ADDOccCode1A = Number(10);
          }
          if (ADDOccCode1 == 'IC') {
            ADDOccCode1A = Number(5);
          }
          if (ADDOccCode1 == '4') {
            ADDOccCode1A = Number(4);
          }
          if (ADDOccCode1 == '3') {
            ADDOccCode1A = Number(3);
          }
          if (ADDOccCode1 == '2') {
            ADDOccCode1A = Number(2);
          }
          if (ADDOccCode1 == '1') {
            ADDOccCode1A = Number(1);
          }
          if (ADDOccCode2 == 'D') {
            ADDOccCode2A = Number(10);
          }
          if (ADDOccCode2 == 'IC') {
            ADDOccCode2A = Number(5);
          }
          if (ADDOccCode2 == '4') {
            ADDOccCode2A = Number(4);
          }
          if (ADDOccCode2 == '3') {
            ADDOccCode2A = Number(3);
          }
          if (ADDOccCode2 == '2') {
            ADDOccCode2A = Number(2);
          }
          if (ADDOccCode2 == '1') {
            ADDOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'ADDOccCode1', ADDOccCode1);
    updateJSON(inoutMap, 'ADDOccCode2', ADDOccCode2);
    updateJSON(inoutMap, 'ADDOccCode1A', ADDOccCode1A);
    updateJSON(inoutMap, 'ADDOccCode2A', ADDOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleADBCodeProLife(inoutMap, successCB) {
  var ADBOccCode1, ADBOccCode2, ADBOccCode1A, ADBOccCode2A, err;
  ADBOccCode1 = '';
  ADBOccCode2 = '';
  ADBOccCode1A = Number(0);
  ADBOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.ADBRider.isRequired == 'Yes') {
      ADBOccCode1 = 0;
      lookupAsJson('ADB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param0, _$param1) {
        ADBOccCode1 = _$param0;
        err = _$param1;
        ADBOccCode2 = 0;
        lookupAsJson('ADB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param2, _$param3) {
          ADBOccCode2 = _$param2;
          err = _$param3;
          if (ADBOccCode1 == 'D') {
            ADBOccCode1A = Number(10);
          }
          if (ADBOccCode1 == 'IC') {
            ADBOccCode1A = Number(5);
          }
          if (ADBOccCode1 == '4') {
            ADBOccCode1A = Number(4);
          }
          if (ADBOccCode1 == '3') {
            ADBOccCode1A = Number(3);
          }
          if (ADBOccCode1 == '2') {
            ADBOccCode1A = Number(2);
          }
          if (ADBOccCode1 == '1') {
            ADBOccCode1A = Number(1);
          }
          if (ADBOccCode2 == 'D') {
            ADBOccCode2A = Number(10);
          }
          if (ADBOccCode2 == 'IC') {
            ADBOccCode2A = Number(5);
          }
          if (ADBOccCode2 == '4') {
            ADBOccCode2A = Number(4);
          }
          if (ADBOccCode2 == '3') {
            ADBOccCode2A = Number(3);
          }
          if (ADBOccCode2 == '2') {
            ADBOccCode2A = Number(2);
          }
          if (ADBOccCode2 == '1') {
            ADBOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'ADBOccCode1', ADBOccCode1);
    updateJSON(inoutMap, 'ADBOccCode2', ADBOccCode2);
    updateJSON(inoutMap, 'ADBOccCode1A', ADBOccCode1A);
    updateJSON(inoutMap, 'ADBOccCode2A', ADBOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleAIOccCodeProLife(inoutMap, successCB) {
  var AIOccCode1, AIOccCode2, AIOccCode1A, AIOccCode2A, err;
  AIOccCode1 = '';
  AIOccCode2 = '';
  AIOccCode1A = Number(0);
  AIOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.AIRider.isRequired == 'Yes') {
      AIOccCode1 = 0;
      lookupAsJson('AI_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param106, _$param107) {
        AIOccCode1 = _$param106;
        err = _$param107;
        AIOccCode2 = 0;
        lookupAsJson('AI_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param108, _$param109) {
          AIOccCode2 = _$param108;
          err = _$param109;
          if (AIOccCode1 == 'D') {
            AIOccCode1A = Number(10);
          }
          if (AIOccCode1 == 'IC') {
            AIOccCode1A = Number(5);
          }
          if (AIOccCode1 == '4') {
            AIOccCode1A = Number(4);
          }
          if (AIOccCode1 == '3') {
            AIOccCode1A = Number(3);
          }
          if (AIOccCode1 == '2') {
            AIOccCode1A = Number(2);
          }
          if (AIOccCode1 == '1') {
            AIOccCode1A = Number(1);
          }
          if (AIOccCode2 == 'D') {
            AIOccCode2A = Number(10);
          }
          if (AIOccCode2 == 'IC') {
            AIOccCode2A = Number(5);
          }
          if (AIOccCode2 == '4') {
            AIOccCode2A = Number(4);
          }
          if (AIOccCode2 == '3') {
            AIOccCode2A = Number(3);
          }
          if (AIOccCode2 == '2') {
            AIOccCode2A = Number(2);
          }
          if (AIOccCode2 == '1') {
            AIOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'AIOccCode1', AIOccCode1);
    updateJSON(inoutMap, 'AIOccCode2', AIOccCode2);
    updateJSON(inoutMap, 'AIOccCode1A', AIOccCode1A);
    updateJSON(inoutMap, 'AIOccCode2A', AIOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRulePBOccCodeProLife(inoutMap, successCB) {
  var PBOccCode1, PBOccCode2, PBOccCode1A, PBOccCode2A, err;
  PBOccCode1 = '';
  PBOccCode2 = '';
  PBOccCode1A = Number(0);
  PBOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.PBRider.isRequired == 'Yes') {
      PBOccCode1 = 0;
      lookupAsJson('PB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PayorDetails.occCode1 + '\'', function (arguments, _$param94, _$param95) {
        PBOccCode1 = _$param94;
        err = _$param95;
        PBOccCode2 = 0;
        lookupAsJson('PB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PayorDetails.occCode2 + '\'', function (arguments, _$param96, _$param97) {
          PBOccCode2 = _$param96;
          err = _$param97;
          if (PBOccCode1 == 'D') {
            PBOccCode1A = Number(10);
          }
          if (PBOccCode1 == 'IC') {
            PBOccCode1A = Number(5);
          }
          if (PBOccCode1 == '4') {
            PBOccCode1A = Number(4);
          }
          if (PBOccCode1 == '3') {
            PBOccCode1A = Number(3);
          }
          if (PBOccCode1 == '2') {
            PBOccCode1A = Number(2);
          }
          if (PBOccCode1 == '1') {
            PBOccCode1A = Number(1);
          }
          if (PBOccCode2 == 'D') {
            PBOccCode2A = Number(10);
          }
          if (PBOccCode2 == 'IC') {
            PBOccCode2A = Number(5);
          }
          if (PBOccCode2 == '4') {
            PBOccCode2A = Number(4);
          }
          if (PBOccCode2 == '3') {
            PBOccCode2A = Number(3);
          }
          if (PBOccCode2 == '2') {
            PBOccCode2A = Number(2);
          }
          if (PBOccCode2 == '1') {
            PBOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'PBOccCode1', PBOccCode1);
    updateJSON(inoutMap, 'PBOccCode2', PBOccCode2);
    updateJSON(inoutMap, 'PBOccCode1A', PBOccCode1A);
    updateJSON(inoutMap, 'PBOccCode2A', PBOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleWBOccCodeProLife(inoutMap, successCB) {
  var WPOccCode1, WPOccCode2, WPOccCode1A, WPOccCode2A, err;
  WPOccCode1 = '';
  WPOccCode2 = '';
  WPOccCode1A = Number(0);
  WPOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.WPRider.isRequired == 'Yes') {
      WPOccCode1 = 0;
      lookupAsJson('WP_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param82, _$param83) {
        WPOccCode1 = _$param82;
        err = _$param83;
        WPOccCode2 = 0;
        lookupAsJson('WP_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param84, _$param85) {
          WPOccCode2 = _$param84;
          err = _$param85;
          if (WPOccCode1 == 'D') {
            WPOccCode1A = Number(10);
          }
          if (WPOccCode1 == 'IC') {
            WPOccCode1A = Number(5);
          }
          if (WPOccCode1 == '4') {
            WPOccCode1A = Number(4);
          }
          if (WPOccCode1 == '3') {
            WPOccCode1A = Number(3);
          }
          if (WPOccCode1 == '2') {
            WPOccCode1A = Number(2);
          }
          if (WPOccCode1 == '1') {
            WPOccCode1A = Number(1);
          }
          if (WPOccCode2 == 'D') {
            WPOccCode2A = Number(10);
          }
          if (WPOccCode2 == 'IC') {
            WPOccCode2A = Number(5);
          }
          if (WPOccCode2 == '4') {
            WPOccCode2A = Number(4);
          }
          if (WPOccCode2 == '3') {
            WPOccCode2A = Number(3);
          }
          if (WPOccCode2 == '2') {
            WPOccCode2A = Number(2);
          }
          if (WPOccCode2 == '1') {
            WPOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'WPOccCode1', WPOccCode1);
    updateJSON(inoutMap, 'WPOccCode2', WPOccCode2);
    updateJSON(inoutMap, 'WPOccCode1A', WPOccCode1A);
    updateJSON(inoutMap, 'WPOccCode2A', WPOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleCNCOccCodeProLife(inoutMap, successCB) {
  var CNCOccCode1, CNCOccCode2, CNCOccCode1A, CNCOccCode2A, err;
  CNCOccCode1 = '';
  CNCOccCode2 = '';
  CNCOccCode1A = Number(0);
  CNCOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.CNCRider.isRequired == 'Yes') {
      CNCOccCode1 = 0;
      lookupAsJson('CNC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param82, _$param83) {
        CNCOccCode1 = _$param82;
        err = _$param83;
        CNCOccCode2 = 0;
        lookupAsJson('CNC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param84, _$param85) {
          CNCOccCode2 = _$param84;
          err = _$param85;
          if (CNCOccCode1 == 'D') {
            CNCOccCode1A = Number(10);
          }
          if (CNCOccCode1 == 'IC') {
            CNCOccCode1A = Number(5);
          }
          if (CNCOccCode1 == '4') {
            CNCOccCode1A = Number(4);
          }
          if (CNCOccCode1 == '3') {
            CNCOccCode1A = Number(3);
          }
          if (CNCOccCode1 == '2') {
            CNCOccCode1A = Number(2);
          }
          if (CNCOccCode1 == '1') {
            CNCOccCode1A = Number(1);
          }
          if (CNCOccCode2 == 'D') {
            CNCOccCode2A = Number(10);
          }
          if (CNCOccCode2 == 'IC') {
            CNCOccCode2A = Number(5);
          }
          if (CNCOccCode2 == '4') {
            CNCOccCode2A = Number(4);
          }
          if (CNCOccCode2 == '3') {
            CNCOccCode2A = Number(3);
          }
          if (CNCOccCode2 == '2') {
            CNCOccCode2A = Number(2);
          }
          if (CNCOccCode2 == '1') {
            CNCOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'CNCOccCode1', CNCOccCode1);
    updateJSON(inoutMap, 'CNCOccCode2', CNCOccCode2);
    updateJSON(inoutMap, 'CNCOccCode1A', CNCOccCode1A);
    updateJSON(inoutMap, 'CNCOccCode2A', CNCOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleDDCodeProLife(inoutMap, successCB) {
  var DDOccCode1, DDOccCode2, DDOccCode1A, DDOccCode2A, err;
  DDOccCode1 = '';
  DDOccCode2 = '';
  DDOccCode1A = Number(0);
  DDOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.DDRider.isRequired == 'Yes') {
      DDOccCode1 = 0;
      lookupAsJson('DD_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param98, _$param99) {
        DDOccCode1 = _$param98;
        err = _$param99;
        DDOccCode2 = 0;
        lookupAsJson('DD_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param100, _$param101) {
          DDOccCode2 = _$param100;
          err = _$param101;
          if (DDOccCode1 == 'D') {
            DDOccCode1A = Number(10);
          }
          if (DDOccCode1 == 'IC') {
            DDOccCode1A = Number(5);
          }
          if (DDOccCode1 == '4') {
            DDOccCode1A = Number(4);
          }
          if (DDOccCode1 == '3') {
            DDOccCode1A = Number(3);
          }
          if (DDOccCode1 == '2') {
            DDOccCode1A = Number(2);
          }
          if (DDOccCode1 == '1') {
            DDOccCode1A = Number(1);
          }
          if (DDOccCode2 == 'D') {
            DDOccCode2A = Number(10);
          }
          if (DDOccCode2 == 'IC') {
            DDOccCode2A = Number(5);
          }
          if (DDOccCode2 == '4') {
            DDOccCode2A = Number(4);
          }
          if (DDOccCode2 == '3') {
            DDOccCode2A = Number(3);
          }
          if (DDOccCode2 == '2') {
            DDOccCode2A = Number(2);
          }
          if (DDOccCode2 == '1') {
            DDOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'DDOccCode1', DDOccCode1);
    updateJSON(inoutMap, 'DDOccCode2', DDOccCode2);
    updateJSON(inoutMap, 'DDOccCode1A', DDOccCode1A);
    updateJSON(inoutMap, 'DDOccCode2A', DDOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleADBRCCCodeProLife(inoutMap, successCB) {
  var ADBRCCOccCode1, ADBRCCOccCode2, ADBRCCOccCode1A, ADBRCCOccCode2A, err;
  ADBRCCOccCode1 = '';
  ADBRCCOccCode2 = '';
  ADBRCCOccCode1A = Number(0);
  ADBRCCOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.RCCADBRider.isRequired == 'Yes') {
      ADBRCCOccCode1 = 0;
      lookupAsJson('ADBRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param118, _$param119) {
        ADBRCCOccCode1 = _$param118;
        err = _$param119;
        ADBRCCOccCode2 = 0;
        lookupAsJson('ADBRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param120, _$param121) {
          ADBRCCOccCode2 = _$param120;
          err = _$param121;
          if (ADBRCCOccCode1 == 'D') {
            ADBRCCOccCode1A = Number(10);
          }
          if (ADBRCCOccCode1 == 'IC') {
            ADBRCCOccCode1A = Number(5);
          }
          if (ADBRCCOccCode1 == 'Y') {
            ADBRCCOccCode1A = Number(1);
          }
          if (ADBRCCOccCode2 == 'D') {
            ADBRCCOccCode2A = Number(10);
          }
          if (ADBRCCOccCode2 == 'IC') {
            ADBRCCOccCode2A = Number(5);
          }
          if (ADBRCCOccCode2 == 'Y') {
            ADBRCCOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'ADBRCCOccCode1', ADBRCCOccCode1);
    updateJSON(inoutMap, 'ADBRCCOccCode2', ADBRCCOccCode2);
    updateJSON(inoutMap, 'ADBRCCOccCode1A', ADBRCCOccCode1A);
    updateJSON(inoutMap, 'ADBRCCOccCode2A', ADBRCCOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleADDRCCCodeProLife(inoutMap, successCB) {
  var ADDRCCOccCode1, ADDRCCOccCode2, ADDRCCOccCode1A, ADDRCCOccCode2A, err;
  ADDRCCOccCode1 = '';
  ADDRCCOccCode2 = '';
  ADDRCCOccCode1A = Number(0);
  ADDRCCOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.RCCADDRider.isRequired == 'Yes') {
      ADDRCCOccCode1 = 0;
      lookupAsJson('ADDRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param146, _$param147) {
        ADDRCCOccCode1 = _$param146;
        err = _$param147;
        ADDRCCOccCode2 = 0;
        lookupAsJson('ADDRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param148, _$param149) {
          ADDRCCOccCode2 = _$param148;
          err = _$param149;
          if (ADDRCCOccCode1 == 'D') {
            ADDRCCOccCode1A = Number(10);
          }
          if (ADDRCCOccCode1 == 'IC') {
            ADDRCCOccCode1A = Number(5);
          }
          if (ADDRCCOccCode1 == 'Y') {
            ADDRCCOccCode1A = Number(1);
          }
          if (ADDRCCOccCode2 == 'D') {
            ADDRCCOccCode2A = Number(10);
          }
          if (ADDRCCOccCode2 == 'IC') {
            ADDRCCOccCode2A = Number(5);
          }
          if (ADDRCCOccCode2 == 'Y') {
            ADDRCCOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'ADDRCCOccCode1', ADDRCCOccCode1);
    updateJSON(inoutMap, 'ADDRCCOccCode2', ADDRCCOccCode2);
    updateJSON(inoutMap, 'ADDRCCOccCode1A', ADDRCCOccCode1A);
    updateJSON(inoutMap, 'ADDRCCOccCode2A', ADDRCCOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleAIRCCCodeProLife(inoutMap, successCB) {
  var AIRCCOccCode1, AIRCCOccCode2, AIRCCOccCode1A, AIRCCOccCode2A, err;
  AIRCCOccCode1 = '';
  AIRCCOccCode2 = '';
  AIRCCOccCode1A = Number(0);
  AIRCCOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.RCCAIRider.isRequired == 'Yes') {
      AIRCCOccCode1 = 0;
      lookupAsJson('AIRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param150, _$param151) {
        AIRCCOccCode1 = _$param150;
        err = _$param151;
        AIRCCOccCode2 = 0;
        lookupAsJson('AIRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param152, _$param153) {
          AIRCCOccCode2 = _$param152;
          err = _$param153;
          if (AIRCCOccCode1 == 'D') {
            AIRCCOccCode1A = Number(10);
          }
          if (AIRCCOccCode1 == 'IC') {
            AIRCCOccCode1A = Number(5);
          }
          if (AIRCCOccCode1 == 'Y') {
            AIRCCOccCode1A = Number(1);
          }
          if (AIRCCOccCode2 == 'D') {
            AIRCCOccCode2A = Number(10);
          }
          if (AIRCCOccCode2 == 'IC') {
            AIRCCOccCode2A = Number(5);
          }
          if (AIRCCOccCode2 == 'Y') {
            AIRCCOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'AIRCCOccCode1', AIRCCOccCode1);
    updateJSON(inoutMap, 'AIRCCOccCode2', AIRCCOccCode2);
    updateJSON(inoutMap, 'AIRCCOccCode1A', AIRCCOccCode1A);
    updateJSON(inoutMap, 'AIRCCOccCode2A', AIRCCOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
/* Generated by Continuation.js v0.1.7 */