function GenSave20Plus_Guideline(inoutMap, successCB) {
  var annPremium, monthPremium, quarPremium, semAnnPremium, annualisedPremium, ADBPremium, ADBRCCPremium, ADDPremium, ADDRCCPremium, AIPremium, AIRCCPremium, PBPremium, PBSumAssured, DDPremium, HSPremium, HBPremium, DDPaymentPeriod, DDPremRate, HBPremRate, HSPremRate, totalRiderPremium, totalPolicyPremium, modalPremium, WPRiderSumAssured, PremiumWP, quarVar, HSDetails, minSumAssured, maxSumAssured, inoutMap, err;
  GenSave20PlusRuleSet(inoutMap, function (arguments, _$param0, _$param1) {
    inoutMap = _$param0;
    err = _$param1;
    monthVar = formatDecimal(mul(inoutMap.premRate, inoutMap.monthRate), 2);
    quarVar = formatDecimal(mul(inoutMap.premRate, inoutMap.quarRate), 2);
    semAnnVar = formatDecimal(mul(inoutMap.premRate, inoutMap.semiAnnRate), 2);
    if (inoutMap.PolicyDetails.isSumAssured == 'Yes') {
      annPremium = formatDecimal(mul(inoutMap.premRate, inoutMap.PolicyDetails.sumAssured), 2);
      annPremium = formatDecimal(div(annPremium, 1000), 0);
      monthPremium = formatDecimal(mul(monthVar, inoutMap.PolicyDetails.sumAssured), 2);
      monthPremium = formatDecimal(div(monthPremium, 1000), 0);
      quarPremium = formatDecimal(mul(quarVar, inoutMap.PolicyDetails.sumAssured), 2);
      quarPremium = formatDecimal(div(quarPremium, 1000), 0);
      semAnnPremium = formatDecimal(mul(semAnnVar, inoutMap.PolicyDetails.sumAssured), 2);
      semAnnPremium = formatDecimal(div(semAnnPremium, 1000), 0);
      annualisedPremium = annPremium;
      modalPremium = annPremium;
      if (inoutMap.PolicyDetails.premiumMode == 6002) {
        annualisedPremium = mul(semAnnPremium, 2);
        modalPremium = semAnnPremium;
      }
      if (inoutMap.PolicyDetails.premiumMode == 6003) {
        annualisedPremium = mul(quarPremium, 4);
        modalPremium = quarPremium;
      }
      if (inoutMap.PolicyDetails.premiumMode == 6004) {
        annualisedPremium = mul(monthPremium, 12);
        modalPremium = monthPremium;
      }
      inoutMap.PolicyDetails.premium = modalPremium;
    } else {
      if (inoutMap.PolicyDetails.isPremium == 'Yes') {
        inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), inoutMap.premRate), 0);
        annualisedPremium = inoutMap.PolicyDetails.premium;
        modalPremium = inoutMap.PolicyDetails.premium;
        if (inoutMap.PolicyDetails.premiumMode == 6002) {
          inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), semAnnVar), 0);
          annualisedPremium = mul(inoutMap.PolicyDetails.premium, 2);
        }
        if (inoutMap.PolicyDetails.premiumMode == 6003) {
          inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), quarVar), 0);
          annualisedPremium = mul(inoutMap.PolicyDetails.premium, 4);
        }
        if (inoutMap.PolicyDetails.premiumMode == 6004) {
          inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), monthVar), 0);
          annualisedPremium = mul(inoutMap.PolicyDetails.premium, 12);
        }
      }
    }
    minSumAssured = Number(200000);
    maxSumAssured = 'Unlimited';
    updateJSON(inoutMap, 'annPremium', annPremium);
    updateJSON(inoutMap, 'monthPremium', monthPremium);
    updateJSON(inoutMap, 'quarPremium', quarPremium);
    updateJSON(inoutMap, 'semAnnPremium', semAnnPremium);
    updateJSON(inoutMap, 'annualisedPremium', annualisedPremium);
    updateJSON(inoutMap, 'ADBPremium', ADBPremium);
    updateJSON(inoutMap, 'ADBRCCPremium', ADBRCCPremium);
    updateJSON(inoutMap, 'ADDPremium', ADDPremium);
    updateJSON(inoutMap, 'ADDRCCPremium', ADDRCCPremium);
    updateJSON(inoutMap, 'AIPremium', AIPremium);
    updateJSON(inoutMap, 'AIRCCPremium', AIRCCPremium);
    updateJSON(inoutMap, 'PBPremium', PBPremium);
    updateJSON(inoutMap, 'PBSumAssured', PBSumAssured);
    updateJSON(inoutMap, 'DDPremium', DDPremium);
    updateJSON(inoutMap, 'HSPremium', HSPremium);
    updateJSON(inoutMap, 'HBPremium', HBPremium);
    updateJSON(inoutMap, 'DDPaymentPeriod', DDPaymentPeriod);
    updateJSON(inoutMap, 'DDPremRate', DDPremRate);
    updateJSON(inoutMap, 'HBPremRate', HBPremRate);
    updateJSON(inoutMap, 'HSPremRate', HSPremRate);
    updateJSON(inoutMap, 'totalRiderPremium', totalRiderPremium);
    updateJSON(inoutMap, 'totalPolicyPremium', totalPolicyPremium);
    updateJSON(inoutMap, 'modalPremium', modalPremium);
    updateJSON(inoutMap, 'WPRiderSumAssured', WPRiderSumAssured);
    updateJSON(inoutMap, 'PremiumWP', PremiumWP);
    updateJSON(inoutMap, 'quarVar', quarVar);
    updateJSON(inoutMap, 'HSDetails', HSDetails);
    updateJSON(inoutMap, 'minSumAssured', minSumAssured);
    updateJSON(inoutMap, 'maxSumAssured', maxSumAssured);
    successCB(inoutMap);
  }.bind(this, arguments));
}
function GenSave20Plus_Premium_02(inoutMap, successCB) {
  var annPremium, monthPremium, quarPremium, semAnnPremium, annualisedPremium, ADBPremium, ADBRCCPremium, ADDPremium, ADDRCCPremium, AIPremium, AIRCCPremium, PBPremium, PBSumAssured, DDPremium, HSPremium, OPDPremium, HBPremium, DDPaymentPeriod, DDPremRate, HBPremRate, HSPremRate, OPDPremRate, totalRiderPremium, totalPolicyPremium, modalPremium, WPRiderSumAssured, PremiumWP, quarVar, HSOccCodeTableFetch, HSDetails, OPDDetails, i, inoutMap, err, PBPremRate;
  HSDetails = [];
  OPDDetails = [];
  ADBPremium = Number(0);
  ADBRCCPremium = Number(0);
  ADDPremium = Number(0);
  ADDRCCPremium = Number(0);
  AIPremium = Number(0);
  AIRCCPremium = Number(0);
  PBPremium = Number(0);
  PBSumAssured = Number(0);
  DDPremium = Number(0);
  HSPremium = Number(0);
  OPDPremium = Number(0);
  HBPremium = Number(0);
  totalRiderPremium = Number(0);
  totalPolicyPremium = Number(0);
  OccupationaLRuleUpdated1GenSave(inoutMap, function (arguments, _$param2, _$param3) {
    inoutMap = _$param2;
    err = _$param3;
    GenSave20Plus_LookUp_01(inoutMap, function (arguments, _$param4, _$param5) {
      inoutMap = _$param4;
      err = _$param5;
      monthVar = formatDecimal(mul(inoutMap.premRate, inoutMap.monthRate), 2);
      quarVar = formatDecimal(mul(inoutMap.premRate, inoutMap.quarRate), 2);
      semAnnVar = formatDecimal(mul(inoutMap.premRate, inoutMap.semiAnnRate), 2);
      if (inoutMap.PolicyDetails.isSumAssured == 'Yes') {
        annPremium = formatDecimal(mul(inoutMap.premRate, inoutMap.PolicyDetails.sumAssured), 2);
        annPremium = formatDecimal(div(annPremium, 1000), 0);
        monthPremium = formatDecimal(mul(monthVar, inoutMap.PolicyDetails.sumAssured), 2);
        monthPremium = formatDecimal(div(monthPremium, 1000), 0);
        quarPremium = formatDecimal(mul(quarVar, inoutMap.PolicyDetails.sumAssured), 2);
        quarPremium = formatDecimal(div(quarPremium, 1000), 0);
        semAnnPremium = formatDecimal(mul(semAnnVar, inoutMap.PolicyDetails.sumAssured), 2);
        semAnnPremium = formatDecimal(div(semAnnPremium, 1000), 0);
        annualisedPremium = annPremium;
        modalPremium = annPremium;
        if (inoutMap.PolicyDetails.premiumMode == 6002) {
          annualisedPremium = mul(semAnnPremium, 2);
          modalPremium = semAnnPremium;
        }
        if (inoutMap.PolicyDetails.premiumMode == 6003) {
          annualisedPremium = mul(quarPremium, 4);
          modalPremium = quarPremium;
        }
        if (inoutMap.PolicyDetails.premiumMode == 6004) {
          annualisedPremium = mul(monthPremium, 12);
          modalPremium = monthPremium;
        }
        inoutMap.PolicyDetails.premium = modalPremium;
      } else {
        if (inoutMap.PolicyDetails.isPremium == 'Yes') {
          inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), inoutMap.premRate), 0);
          annualisedPremium = inoutMap.PolicyDetails.premium;
          modalPremium = inoutMap.PolicyDetails.premium;
          if (inoutMap.PolicyDetails.premiumMode == 6002) {
            inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), semAnnVar), 0);
            annualisedPremium = mul(inoutMap.PolicyDetails.premium, 2);
          }
          if (inoutMap.PolicyDetails.premiumMode == 6003) {
            inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), quarVar), 0);
            annualisedPremium = mul(inoutMap.PolicyDetails.premium, 4);
          }
          if (inoutMap.PolicyDetails.premiumMode == 6004) {
            inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), monthVar), 0);
            annualisedPremium = mul(inoutMap.PolicyDetails.premium, 12);
          }
        }
      }
      (function (_$cont) {
        if (inoutMap.WPRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age <= Number(55) && inoutMap.InsuredDetails.age >= Number(16)) {
          GenSave20Plus_WPPremium_05(inoutMap, function (arguments, _$param6, _$param7) {
            inoutMap = _$param6;
            err = _$param7;
            PremiumWP = inoutMap.PremiumWP1;
            WPRiderSumAssured = inoutMap.WPRiderSumAssured1;
            _$cont();
          }.bind(this, arguments));
        } else {
          PremiumWP = Number(0);
          WPRiderSumAssured = Number(0);
          _$cont();
        }
      }.bind(this)(function (_$err) {
        if (_$err !== undefined)
          return _$cont(_$err);
        if (inoutMap.ADBRider.isRequired == 'Yes') {
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
            ADBPremium = formatDecimal(mul(inoutMap.ADBRider.sumAssured, inoutMap.ADBPremRate), 2);
            ADBPremium = formatDecimal(div(ADBPremium, 1000), 0);
          } else {
            if (inoutMap.InsuredDetails.age <= Number(64) && inoutMap.PolicyDetails.premiumPayingTerm == Number(25)) {
              ADBPremium = formatDecimal(mul(inoutMap.ADBRider.sumAssured, inoutMap.ADBPremRate), 2);
              ADBPremium = formatDecimal(div(ADBPremium, 1000), 0);
            }
          }
        } else {
          ADBPremium = Number(0);
        }
        if (inoutMap.RCCADBRider.isRequired == 'Yes') {
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
            ADBRCCPremium = formatDecimal(mul(inoutMap.ADBRider.sumAssured, inoutMap.ADBRCCPremRate), 2);
            ADBRCCPremium = formatDecimal(div(ADBRCCPremium, 1000), 0);
          } else {
            if (inoutMap.InsuredDetails.age <= Number(64) && inoutMap.PolicyDetails.premiumPayingTerm == Number(25)) {
              ADBRCCPremium = formatDecimal(mul(inoutMap.ADBRider.sumAssured, inoutMap.ADBRCCPremRate), 2);
              ADBRCCPremium = formatDecimal(div(ADBRCCPremium, 1000), 0);
            }
          }
        } else {
          ADBRCCPremium = Number(0);
        }
        if (inoutMap.ADDRiders.isRequired == 'Yes') {
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
            ADDPremium = formatDecimal(mul(inoutMap.ADDRiders.sumAssured, inoutMap.ADDPremRate), 2);
            ADDPremium = formatDecimal(div(ADDPremium, 1000), 0);
          } else {
            if (inoutMap.InsuredDetails.age <= Number(64) && inoutMap.PolicyDetails.premiumPayingTerm == Number(25)) {
              ADDPremium = formatDecimal(mul(inoutMap.ADDRiders.sumAssured, inoutMap.ADDPremRate), 2);
              ADDPremium = formatDecimal(div(ADDPremium, 1000), 0);
            }
          }
        } else {
          ADDPremium = Number(0);
        }
        if (inoutMap.RCCADDRider.isRequired == 'Yes') {
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
            ADDRCCPremium = formatDecimal(mul(inoutMap.ADDRiders.sumAssured, inoutMap.ADDRCCPremRate), 2);
            ADDRCCPremium = formatDecimal(div(ADDRCCPremium, 1000), 0);
          } else {
            if (inoutMap.InsuredDetails.age <= Number(64) && inoutMap.PolicyDetails.premiumPayingTerm == Number(25)) {
              ADDRCCPremium = formatDecimal(mul(inoutMap.ADDRiders.sumAssured, inoutMap.ADDRCCPremRate), 2);
              ADDRCCPremium = formatDecimal(div(ADDRCCPremium, 1000), 0);
            }
          }
        } else {
          ADDRCCPremium = Number(0);
        }
        if (inoutMap.AIRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age >= Number(16)) {
          AIPremium = formatDecimal(mul(inoutMap.AIRider.sumAssured, inoutMap.AIPremRate), 2);
          AIPremium = formatDecimal(div(AIPremium, 1000), 0);
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
            AIPremium = formatDecimal(mul(inoutMap.AIRider.sumAssured, inoutMap.AIPremRate), 2);
            AIPremium = formatDecimal(div(AIPremium, 1000), 0);
          } else {
            if (inoutMap.InsuredDetails.age <= Number(64) && inoutMap.PolicyDetails.premiumPayingTerm == Number(25)) {
              AIPremium = formatDecimal(mul(inoutMap.AIRider.sumAssured, inoutMap.AIPremRate), 2);
              AIPremium = formatDecimal(div(AIPremium, 1000), 0);
            }
          }
        } else {
          AIPremium = Number(0);
        }
        if (inoutMap.RCCAIRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age >= Number(16)) {
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
            AIRCCPremium = formatDecimal(mul(inoutMap.AIRider.sumAssured, inoutMap.AIRCCPremRate), 2);
            AIRCCPremium = formatDecimal(div(AIRCCPremium, 1000), 0);
          } else {
            if (inoutMap.InsuredDetails.age <= Number(64) && inoutMap.PolicyDetails.premiumPayingTerm == Number(25)) {
              AIRCCPremium = formatDecimal(mul(inoutMap.AIRider.sumAssured, inoutMap.AIRCCPremRate), 2);
              AIRCCPremium = formatDecimal(div(AIRCCPremium, 1000), 0);
            }
          }
        } else {
          AIRCCPremium = Number(0);
        }
        (function (_$cont) {
          if (inoutMap.DDRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age <= Number(55) && inoutMap.InsuredDetails.age >= Number(17)) {
            DDPaymentPeriod = minOfNValues(inoutMap.PolicyDetails.premiumPayingTerm, sub(Number(60), inoutMap.InsuredDetails.age), Number(19));
            DDPremRate = 0;
            lookupAsJson('RATE_FACTOR', 'EXTN_RIDER_DD_PREM_TBL', 'PROD_ID=1006', 'INSURED_AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'INSURED_SEX=\'' + inoutMap.InsuredDetails.sex + '\'', 'PAYMENT_PERIOD=\'' + DDPaymentPeriod + '\'', function (arguments, _$param8, _$param9) {
              DDPremRate = _$param8;
              err = _$param9;
              DDPremium = formatDecimal(mul(inoutMap.DDRider.sumAssured, DDPremRate), 2);
              DDPremium = formatDecimal(div(DDPremium, 1000), 0);
              if (inoutMap.PolicyDetails.premiumMode == 6002) {
                DDRate = formatDecimal(mul(DDPremRate, inoutMap.semiAnnRate), 2);
                DDPremium = formatDecimal(mul(inoutMap.DDRider.sumAssured, DDRate), 2);
                DDPremium = formatDecimal(div(DDPremium, 1000), 0);
              }
              if (inoutMap.PolicyDetails.premiumMode == 6003) {
                DDRate = formatDecimal(mul(DDPremRate, inoutMap.quarRate), 2);
                DDPremium = formatDecimal(mul(inoutMap.DDRider.sumAssured, DDRate), 2);
                DDPremium = formatDecimal(div(DDPremium, 1000), 0);
              }
              if (inoutMap.PolicyDetails.premiumMode == 6004) {
                DDRate = formatDecimal(mul(DDPremRate, inoutMap.monthRate), 2);
                DDPremium = formatDecimal(mul(inoutMap.DDRider.sumAssured, DDRate), 2);
                DDPremium = formatDecimal(div(DDPremium, 1000), 0);
              }
              _$cont();
            }.bind(this, arguments));
          } else {
            DDPremium = Number(0);
            _$cont();
          }
        }.bind(this)(function (_$err) {
          if (_$err !== undefined)
            return _$cont(_$err);
          if (inoutMap.HBOccCode == 'IC') {
            HBOccCodeTableFetch = Number(4);
          } else {
            HBOccCodeTableFetch = inoutMap.HBOccCode;
          }
          if (inoutMap.HSOccCode == 'IC') {
            HSOccCodeTableFetch = Number(4);
          } else {
            HSOccCodeTableFetch = inoutMap.HSOccCode;
          }
          (function (_$cont) {
            if (inoutMap.HBRiders.isRequired == 'Yes' && inoutMap.InsuredDetails.age <= Number(59) && inoutMap.InsuredDetails.age >= Number(6)) {
              HBPremRate = 0;
              lookupAsJson('PREMIUM_RATE', 'EXTN_RIDER_HB_PREM_TBL', 'PROD_ID=1006', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + HBOccCodeTableFetch + '\'', 'START_AGE<=\'' + inoutMap.InsuredDetails.age + '\'', 'END_AGE>=\'' + inoutMap.InsuredDetails.age + '\'', 'PLAN_NUM=\'' + inoutMap.HBRiders.sumAssured + '\'', function (arguments, _$param10, _$param11) {
                HBPremRate = _$param10;
                err = _$param11;
                HBPremium = HBPremRate;
                _$cont();
              }.bind(this, arguments));
            } else {
              HBPremium = Number(0);
              _$cont();
            }
          }.bind(this)(function (_$err) {
            if (_$err !== undefined)
              return _$cont(_$err);
            (function (_$cont) {
              if (inoutMap.HSRiders.isRequired == 'Yes') {
                HSPremRate = 0;
                lookupAsJson('PREMIUM_RATE', 'EXTN_RIDER_HS_PREM_TBL', 'PROD_ID=1006', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + HSOccCodeTableFetch + '\'', 'START_AGE<=\'' + inoutMap.InsuredDetails.age + '\'', 'END_AGE>=\'' + inoutMap.InsuredDetails.age + '\'', 'PLAN_NUM=\'' + inoutMap.HSRiders.sumAssured + '\'', 'SEX=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param12, _$param13) {
                  HSPremRate = _$param12;
                  err = _$param13;
                  HSPremium = HSPremRate;
                  _$cont();
                }.bind(this, arguments));
              } else {
                HSPremium = Number(0);
                _$cont();
              }
            }.bind(this)(function (_$err) {
              if (_$err !== undefined)
                return _$cont(_$err);
              (function (_$cont) {
                if (inoutMap.OPDRiders.isRequired == 'Yes') {
                  OPDPremRate = 0;
                  lookupAsJson('PREMIUM_RATE', 'EXTN_RIDER_OPD_PREM_TBL', 'PROD_ID=1006', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + HSOccCodeTableFetch + '\'', 'START_AGE<=\'' + inoutMap.InsuredDetails.age + '\'', 'END_AGE>=\'' + inoutMap.InsuredDetails.age + '\'', 'PLAN_NUM=\'' + inoutMap.OPDRiders.sumAssured + '\'', 'SEX=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param14, _$param15) {
                    OPDPremRate = _$param14;
                    err = _$param15;
                    OPDPremium = OPDPremRate;
                    _$cont();
                  }.bind(this, arguments));
                } else {
                  OPDPremium = Number(0);
                  _$cont();
                }
              }.bind(this)(function (_$err) {
                if (_$err !== undefined)
                  return _$cont(_$err);
                (function (_$cont) {
                  if (inoutMap.PBRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age <= Number(15) && inoutMap.PayorDetails.age >= Number(20) && inoutMap.PayorDetails.age <= Number(55)) {
                    PBSumAssured = sum(Number(modalPremium), Number(PremiumWP), Number(ADBPremium), Number(ADDPremium), Number(AIPremium), Number(ADBRCCPremium), Number(ADDRCCPremium), Number(AIRCCPremium), Number(DDPremium), Number(HBPremium), Number(HSPremium), Number(OPDPremium));
                    inoutMap.PBRider.sumAssured = PBSumAssured;
                    PBPaymentPeriod = minOfNValues(inoutMap.PolicyDetails.premiumPayingTerm, sub(Number(25), inoutMap.InsuredDetails.age), sub(Number(60), inoutMap.PayorDetails.age));
                    PBPremRate = 0;
                    lookupAsJson('RATE_FACTOR', 'EXTN_RIDER_PB_PREM_TBL', 'PROD_ID=1006', 'INSURED_AGE=\'' + inoutMap.PayorDetails.age + '\'', 'INSURED_SEX=\'' + inoutMap.PayorDetails.sex + '\'', 'PAYMENT_PERIOD=\'' + PBPaymentPeriod + '\'', function (arguments, _$param16, _$param17) {
                      PBPremRate = _$param16;
                      err = _$param17;
                      PBPremium = formatDecimal(mul(PBSumAssured, PBPremRate), 2);
                      PBPremium = formatDecimal(div(PBPremium, 100), 0);
                      _$cont();
                    }.bind(this, arguments));
                  } else {
                    PBPremium = Number(0);
                    _$cont();
                  }
                }.bind(this)(function (_$err) {
                  if (_$err !== undefined)
                    return _$cont(_$err);
                  totalRiderPremium = sum(Number(ADBPremium), Number(ADDPremium), Number(AIPremium), Number(ADBRCCPremium), Number(ADDRCCPremium), Number(AIRCCPremium), Number(DDPremium), Number(HBPremium), Number(HSPremium), Number(OPDPremium), Number(PremiumWP), PBPremium);
                  totalPolicyPremium = sum(totalRiderPremium, Number(modalPremium));
                  (function (_$cont) {
                    if (inoutMap.HSRiders.isRequired == 'Yes') {
                      if (inoutMap.HSRiders.sumAssured < Number(1000)) {
                        inoutMap.HSRiders.sumAssured = Number(1000);
                      }
                      HSDetails = 0;
                      lookupAsJson('HS_DETAILS', 'EXTN_RIDER_HS_PDF_TBL', 'PROD_ID=1006', 'PLAN_NUM=\'' + inoutMap.HSRiders.sumAssured + '\'', function (arguments, _$param18, _$param19) {
                        HSDetails = _$param18;
                        err = _$param19;
                        _$cont();
                      }.bind(this, arguments));
                    } else {
                      if (inoutMap.HSRiders.isRequired == 'No') {
                        i = 0;
                        while (i <= 8) {
                          HSDetails[i] = Number(0);
                          i = i + 1;
                        }
                      }
                      _$cont();
                    }
                  }.bind(this)(function (_$err) {
                    if (_$err !== undefined)
                      return _$cont(_$err);
                    (function (_$cont) {
                      if (inoutMap.OPDRiders.isRequired == 'Yes') {
                        OPDDetails = 0;
                        lookupAsJson('HS_DETAILS', 'EXTN_RIDER_HS_PDF_TBL', 'PROD_ID=1006', 'PLAN_NUM=\'' + inoutMap.OPDRiders.sumAssured + '\'', function (arguments, _$param20, _$param21) {
                          OPDDetails = _$param20;
                          err = _$param21;
                          _$cont();
                        }.bind(this, arguments));
                      } else {
                        i = 0;
                        while (i <= 2) {
                          OPDDetails[i] = Number(0);
                          i = i + 1;
                        }
                        _$cont();
                      }
                    }.bind(this)(function (_$err) {
                      if (_$err !== undefined)
                        return _$cont(_$err);
                      updateJSON(inoutMap, 'annPremium', annPremium);
                      updateJSON(inoutMap, 'monthPremium', monthPremium);
                      updateJSON(inoutMap, 'quarPremium', quarPremium);
                      updateJSON(inoutMap, 'semAnnPremium', semAnnPremium);
                      updateJSON(inoutMap, 'annualisedPremium', annualisedPremium);
                      updateJSON(inoutMap, 'ADBPremium', ADBPremium);
                      updateJSON(inoutMap, 'ADBRCCPremium', ADBRCCPremium);
                      updateJSON(inoutMap, 'ADDPremium', ADDPremium);
                      updateJSON(inoutMap, 'ADDRCCPremium', ADDRCCPremium);
                      updateJSON(inoutMap, 'AIPremium', AIPremium);
                      updateJSON(inoutMap, 'AIRCCPremium', AIRCCPremium);
                      updateJSON(inoutMap, 'PBPremium', PBPremium);
                      updateJSON(inoutMap, 'PBSumAssured', PBSumAssured);
                      updateJSON(inoutMap, 'DDPremium', DDPremium);
                      updateJSON(inoutMap, 'HSPremium', HSPremium);
                      updateJSON(inoutMap, 'OPDPremium', OPDPremium);
                      updateJSON(inoutMap, 'HBPremium', HBPremium);
                      updateJSON(inoutMap, 'DDPaymentPeriod', DDPaymentPeriod);
                      updateJSON(inoutMap, 'DDPremRate', DDPremRate);
                      updateJSON(inoutMap, 'HBPremRate', HBPremRate);
                      updateJSON(inoutMap, 'HSPremRate', HSPremRate);
                      updateJSON(inoutMap, 'OPDPremRate', OPDPremRate);
                      updateJSON(inoutMap, 'totalRiderPremium', totalRiderPremium);
                      updateJSON(inoutMap, 'totalPolicyPremium', totalPolicyPremium);
                      updateJSON(inoutMap, 'modalPremium', modalPremium);
                      updateJSON(inoutMap, 'WPRiderSumAssured', WPRiderSumAssured);
                      updateJSON(inoutMap, 'PremiumWP', PremiumWP);
                      updateJSON(inoutMap, 'quarVar', quarVar);
                      updateJSON(inoutMap, 'HSOccCodeTableFetch', HSOccCodeTableFetch);
                      updateJSON(inoutMap, 'HSDetails', HSDetails);
                      updateJSON(inoutMap, 'OPDDetails', OPDDetails);
                      successCB(inoutMap);
                    }.bind(this)));
                  }.bind(this)));
                }.bind(this)));
              }.bind(this)));
            }.bind(this)));
          }.bind(this)));
        }.bind(this)));
      }.bind(this)));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function GenSave20Plus_BFTable_CVTable_03(inoutMap, successCB) {
  var sumOfLivingBenefit, sumOfTaxBenefit, sumOfPremium, sumOfGrandBenefit, sumOfRatePercent, totalBenefit, benefitYear, benefitAge, benefitPremium, benefitTaxAmount, benefitTaxDeduct, benefitCashRate, benefitCashAmount, benefitLifeRate, benefitLifeAmount, cashValue, rpuCash, rpuSA, extPeriodYear, extPeriodDay, etiCash, etiSA;
  benefitYear = [];
  benefitAge = [];
  benefitPremium = [];
  benefitTaxAmount = [];
  benefitTaxDeduct = [];
  benefitCashRate = [];
  benefitCashAmount = [];
  benefitLifeRate = [];
  benefitLifeAmount = [];
  cashValue = [];
  rpuCash = [];
  rpuSA = [];
  extPeriodYear = [];
  extPeriodDay = [];
  etiCash = [];
  etiSA = [];
  varMultiple = Number(1.1);
  sumOfLivingBenefit = Number(0);
  sumOfTaxBenefit = Number(0);
  sumOfPremium = Number(0);
  sumOfGrandBenefit = Number(0);
  sumOfRatePercent = Number(0);
  if (inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
    startDuration = inoutMap.PolicyDetails.premiumPayingTerm;
  } else {
    startDuration = inoutMap.PolicyDetails.premiumPayingTerm;
  }
  i = 0;
  while (i <= sub(startDuration, 1)) {
    benefitYear[i] = sum(i, 1);
    benefitAge[i] = sum(inoutMap.InsuredDetails.age, i, 1);
    if (benefitAge[i] < sum(inoutMap.InsuredDetails.age, inoutMap.PolicyDetails.premiumPayingTerm, 1)) {
      benefitPremium[i] = formatDecimal(Number(inoutMap.annualisedPremium), 0);
    } else {
      benefitPremium[i] = Number(0);
    }
    benefitCashRate[i] = formatDecimal(mul(Number(inoutMap.PERate[i + 1]), 100), 2) + '%';
    benefitCashAmount[i] = formatDecimal(div(inoutMap.PERate[i + 1], 1), 4);
    benefitCashAmount[i] = formatDecimal(mul(benefitCashAmount[i], inoutMap.PolicyDetails.sumAssured), 0);
    benefitLifeRate[i] = formatDecimal(mul(Number(inoutMap.DBRate[i]), 100), 2);
    benefitLifeRate[i] = benefitLifeRate[i] + '%';
    benefitLifeAmount[i] = formatDecimal(mul(inoutMap.DBRate[i], inoutMap.PolicyDetails.sumAssured, 1), 0);
    cashValue[i] = formatDecimal(div(mul(inoutMap.TCVXRate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    rpuCash[i] = formatDecimal(div(mul(inoutMap.RPUCashRate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    rpuSA[i] = formatDecimal(div(mul(inoutMap.RPURate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    extPeriodYear[i] = inoutMap.ETIYR[i + 1];
    extPeriodDay[i] = inoutMap.ETIDAY[i + 1];
    etiCash[i] = formatDecimal(div(mul(inoutMap.ETICashRate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    etiSA[i] = formatDecimal(div(mul(inoutMap.ETISARate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    if (benefitAge[i] < sum(inoutMap.InsuredDetails.age, inoutMap.PolicyDetails.premiumPayingTerm, 1)) {
      if (inoutMap.annualisedPremium >= Number(100000)) {
        benefitTaxAmount[i] = formatDecimal(mul(div(inoutMap.PolicyDetails.taxRate, 100), Number(100000)), 0);
      } else {
        benefitTaxAmount[i] = formatDecimal(mul(div(inoutMap.PolicyDetails.taxRate, 100), benefitPremium[i]), 0);
      }
    } else {
      benefitTaxAmount[i] = Number(0);
    }
    sumOfLivingBenefit = formatDecimal(sum(sumOfLivingBenefit, benefitCashAmount[i]), 2);
    sumOfTaxBenefit = formatDecimal(sum(sumOfTaxBenefit, benefitTaxAmount[i]), 2);
    sumOfPremium = formatDecimal(sum(sumOfPremium, benefitPremium[i]), 2);
    sumOfRatePercent = formatDecimal(sum(sumOfRatePercent, benefitCashRate[i]), 2) + '%';
    i++;
  }
  totalBenefit = formatDecimal(sub(sum(sumOfLivingBenefit, sumOfTaxBenefit), sumOfPremium), 2);
  updateJSON(inoutMap, 'sumOfLivingBenefit', sumOfLivingBenefit);
  updateJSON(inoutMap, 'sumOfTaxBenefit', sumOfTaxBenefit);
  updateJSON(inoutMap, 'sumOfPremium', sumOfPremium);
  updateJSON(inoutMap, 'sumOfGrandBenefit', sumOfGrandBenefit);
  updateJSON(inoutMap, 'sumOfRatePercent', sumOfRatePercent);
  updateJSON(inoutMap, 'totalBenefit', totalBenefit);
  updateJSON(inoutMap, 'benefitYear', benefitYear);
  updateJSON(inoutMap, 'benefitAge', benefitAge);
  updateJSON(inoutMap, 'benefitPremium', benefitPremium);
  updateJSON(inoutMap, 'benefitTaxAmount', benefitTaxAmount);
  updateJSON(inoutMap, 'benefitTaxDeduct', benefitTaxDeduct);
  updateJSON(inoutMap, 'benefitCashRate', benefitCashRate);
  updateJSON(inoutMap, 'benefitCashAmount', benefitCashAmount);
  updateJSON(inoutMap, 'benefitLifeRate', benefitLifeRate);
  updateJSON(inoutMap, 'benefitLifeAmount', benefitLifeAmount);
  updateJSON(inoutMap, 'cashValue', cashValue);
  updateJSON(inoutMap, 'rpuCash', rpuCash);
  updateJSON(inoutMap, 'rpuSA', rpuSA);
  updateJSON(inoutMap, 'extPeriodYear', extPeriodYear);
  updateJSON(inoutMap, 'extPeriodDay', extPeriodDay);
  updateJSON(inoutMap, 'etiCash', etiCash);
  updateJSON(inoutMap, 'etiSA', etiSA);
  successCB(inoutMap);
}
function GenSave20PlusRuleSet(inoutMap, successCB) {
  processRuleSet(inoutMap, [
    'GenSave20Plus_Premium_02',
    'GenSave20Plus_BFTable_CVTable_03',
    successCB
  ]);
}
function GenSave20Plus_LookUp_01(inoutMap, successCB) {
  var premRate, monthRate, quarRate, semiAnnRate, ADBPremRate, ADDPremRate, AIPremRate, ADBRCCPremRate, ADDRCCPremRate, AIRCCPremRate, PERate, DBRate, TCVXRate, RPUCashRate, RPURate, ETIYR, ETIDAY, ETICashRate, ETISARate, err;
  if (inoutMap.ADDOccCode == 'IC') {
    ADDOccCodeTableFetch = Number(4);
  } else {
    ADDOccCodeTableFetch = inoutMap.ADDOccCode;
  }
  if (inoutMap.ADBOccCode == 'IC') {
    ADBOccCodeTableFetch = Number(4);
  } else {
    ADBOccCodeTableFetch = inoutMap.ADBOccCode;
  }
  if (inoutMap.AIOccCode == 'IC') {
    AIOccCodeTableFetch = Number(4);
  } else {
    AIOccCodeTableFetch = inoutMap.AIOccCode;
  }
  monthRate = 0;
  lookupAsJson('PREMIUM_FREQ', 'EXTN_MODAL_PREM_TBL', 'PREMIUM_MODE=6004', 'PROD_ID=1006', function (arguments, _$param22, _$param23) {
    monthRate = _$param22;
    err = _$param23;
    quarRate = 0;
    lookupAsJson('PREMIUM_FREQ', 'EXTN_MODAL_PREM_TBL', 'PREMIUM_MODE=6003', 'PROD_ID=1006', function (arguments, _$param24, _$param25) {
      quarRate = _$param24;
      err = _$param25;
      semiAnnRate = 0;
      lookupAsJson('PREMIUM_FREQ', 'EXTN_MODAL_PREM_TBL', 'PREMIUM_MODE=6002', 'PROD_ID=1006', function (arguments, _$param26, _$param27) {
        semiAnnRate = _$param26;
        err = _$param27;
        ADBPremRate = 0;
        lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_ADB_PREM_TBL', 'PROD_ID=1006', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + ADBOccCodeTableFetch + '\'', function (arguments, _$param28, _$param29) {
          ADBPremRate = _$param28;
          err = _$param29;
          ADDPremRate = 0;
          lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_AI_PREM_TBL', 'PROD_ID=1006', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + ADDOccCodeTableFetch + '\'', function (arguments, _$param30, _$param31) {
            ADDPremRate = _$param30;
            err = _$param31;
            AIPremRate = 0;
            lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_ADD_PREM_TBL', 'PROD_ID=1006', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + AIOccCodeTableFetch + '\'', function (arguments, _$param32, _$param33) {
              AIPremRate = _$param32;
              err = _$param33;
              ADBRCCPremRate = 0;
              lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_RCC_ADB_PREM_TBL', 'PROD_ID=1006', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', function (arguments, _$param34, _$param35) {
                ADBRCCPremRate = _$param34;
                err = _$param35;
                ADDRCCPremRate = 0;
                lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_RCC_ADD_PREM_TBL', 'PROD_ID=1006', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', function (arguments, _$param36, _$param37) {
                  ADDRCCPremRate = _$param36;
                  err = _$param37;
                  AIRCCPremRate = 0;
                  lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_RCC_AI_PREM_TBL', 'PROD_ID=1006', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', function (arguments, _$param38, _$param39) {
                    AIRCCPremRate = _$param38;
                    err = _$param39;
                    (function (_$cont) {
                      if (inoutMap.PolicyDetails.premiumPayingTerm == Number(20)) {
                        premRate = 0;
                        lookupAsJson('GPX', 'EXTN_TVVALUE_GSP20_TBL', 'PROD_ID=1006', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', 'TERM= 0', function (arguments, _$param40, _$param41) {
                          premRate = _$param40;
                          err = _$param41;
                          PERate = 0;
                          lookupAsJson('PE', 'EXTN_TVVALUE_GSP20_TBL', 'PROD_ID=1006', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY AGE', function (arguments, _$param42, _$param43) {
                            PERate = _$param42;
                            err = _$param43;
                            DBRate = 0;
                            lookupAsJson('DB', 'EXTN_TVVALUE_GSP20_TBL', 'PROD_ID=1006', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param44, _$param45) {
                              DBRate = _$param44;
                              err = _$param45;
                              TCVXRate = 0;
                              lookupAsJson('TCVX_TWO', 'EXTN_TVVALUE_GSP20_TBL', 'PROD_ID=1006', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param46, _$param47) {
                                TCVXRate = _$param46;
                                err = _$param47;
                                RPUCashRate = 0;
                                lookupAsJson('RPU_CASH', 'EXTN_TVVALUE_GSP20_TBL', 'PROD_ID=1006', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param48, _$param49) {
                                  RPUCashRate = _$param48;
                                  err = _$param49;
                                  RPURate = 0;
                                  lookupAsJson('RPU', 'EXTN_TVVALUE_GSP20_TBL', 'PROD_ID=1006', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param50, _$param51) {
                                    RPURate = _$param50;
                                    err = _$param51;
                                    ETIYR = 0;
                                    lookupAsJson('ETI_YR', 'EXTN_TVVALUE_GSP20_TBL', 'PROD_ID=1006', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param52, _$param53) {
                                      ETIYR = _$param52;
                                      err = _$param53;
                                      ETIDAY = 0;
                                      lookupAsJson('ETI_DAY', 'EXTN_TVVALUE_GSP20_TBL', 'PROD_ID=1006', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param54, _$param55) {
                                        ETIDAY = _$param54;
                                        err = _$param55;
                                        ETICashRate = 0;
                                        lookupAsJson('ETI_CASH', 'EXTN_TVVALUE_GSP20_TBL', 'PROD_ID=1006', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param56, _$param57) {
                                          ETICashRate = _$param56;
                                          err = _$param57;
                                          ETISARate = 0;
                                          lookupAsJson('ETI_SA', 'EXTN_TVVALUE_GSP20_TBL', 'PROD_ID=1006', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param58, _$param59) {
                                            ETISARate = _$param58;
                                            err = _$param59;
                                            _$cont();
                                          }.bind(this, arguments));
                                        }.bind(this, arguments));
                                      }.bind(this, arguments));
                                    }.bind(this, arguments));
                                  }.bind(this, arguments));
                                }.bind(this, arguments));
                              }.bind(this, arguments));
                            }.bind(this, arguments));
                          }.bind(this, arguments));
                        }.bind(this, arguments));
                      } else {
                        premRate = 0;
                        lookupAsJson('GPX', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', 'TERM=\'0\'', function (arguments, _$param60, _$param61) {
                          premRate = _$param60;
                          err = _$param61;
                          PERate = 0;
                          lookupAsJson('PE', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY AGE', function (arguments, _$param62, _$param63) {
                            PERate = _$param62;
                            err = _$param63;
                            DBRate = 0;
                            lookupAsJson('DB', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param64, _$param65) {
                              DBRate = _$param64;
                              err = _$param65;
                              TCVXRate = 0;
                              lookupAsJson('TCVX', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param66, _$param67) {
                                TCVXRate = _$param66;
                                err = _$param67;
                                RPUCashRate = 0;
                                lookupAsJson('RPU_CASH', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param68, _$param69) {
                                  RPUCashRate = _$param68;
                                  err = _$param69;
                                  RPURate = 0;
                                  lookupAsJson('RPU', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param70, _$param71) {
                                    RPURate = _$param70;
                                    err = _$param71;
                                    ETIYR = 0;
                                    lookupAsJson('ETI_YR', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param72, _$param73) {
                                      ETIYR = _$param72;
                                      err = _$param73;
                                      ETIDAY = 0;
                                      lookupAsJson('ETI_DAY', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param74, _$param75) {
                                        ETIDAY = _$param74;
                                        err = _$param75;
                                        ETICashRate = 0;
                                        lookupAsJson('ETI_CASH', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param76, _$param77) {
                                          ETICashRate = _$param76;
                                          err = _$param77;
                                          ETISARate = 0;
                                          lookupAsJson('ETI_SA', 'EXTN_TVVALUE_GENPRO25_TBL', 'PROD_ID=1004', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param78, _$param79) {
                                            ETISARate = _$param78;
                                            err = _$param79;
                                            _$cont();
                                          }.bind(this, arguments));
                                        }.bind(this, arguments));
                                      }.bind(this, arguments));
                                    }.bind(this, arguments));
                                  }.bind(this, arguments));
                                }.bind(this, arguments));
                              }.bind(this, arguments));
                            }.bind(this, arguments));
                          }.bind(this, arguments));
                        }.bind(this, arguments));
                      }
                    }.bind(this)(function (_$err) {
                      if (_$err !== undefined)
                        return _$cont(_$err);
                      updateJSON(inoutMap, 'premRate', premRate);
                      updateJSON(inoutMap, 'monthRate', monthRate);
                      updateJSON(inoutMap, 'quarRate', quarRate);
                      updateJSON(inoutMap, 'semiAnnRate', semiAnnRate);
                      updateJSON(inoutMap, 'ADBPremRate', ADBPremRate);
                      updateJSON(inoutMap, 'ADDPremRate', ADDPremRate);
                      updateJSON(inoutMap, 'AIPremRate', AIPremRate);
                      updateJSON(inoutMap, 'ADBRCCPremRate', ADBRCCPremRate);
                      updateJSON(inoutMap, 'ADDRCCPremRate', ADDRCCPremRate);
                      updateJSON(inoutMap, 'AIRCCPremRate', AIRCCPremRate);
                      updateJSON(inoutMap, 'PERate', PERate);
                      updateJSON(inoutMap, 'DBRate', DBRate);
                      updateJSON(inoutMap, 'TCVXRate', TCVXRate);
                      updateJSON(inoutMap, 'RPUCashRate', RPUCashRate);
                      updateJSON(inoutMap, 'RPURate', RPURate);
                      updateJSON(inoutMap, 'ETIYR', ETIYR);
                      updateJSON(inoutMap, 'ETIDAY', ETIDAY);
                      updateJSON(inoutMap, 'ETICashRate', ETICashRate);
                      updateJSON(inoutMap, 'ETISARate', ETISARate);
                      successCB(inoutMap);
                    }.bind(this)));
                  }.bind(this, arguments));
                }.bind(this, arguments));
              }.bind(this, arguments));
            }.bind(this, arguments));
          }.bind(this, arguments));
        }.bind(this, arguments));
      }.bind(this, arguments));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function OccupationaLRuleBaseCodeGenSave(inoutMap, successCB) {
  var BaseOccCode1, BaseOccCode2, BaseOccCode1A, BaseOccCode2A, err;
  BaseOccCode1A = Number(0);
  BaseOccCode1 = '';
  BaseOccCode2A = Number(0);
  BaseOccCode2 = '';
  BaseOccCode1 = 0;
  lookupAsJson('BASE_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param80, _$param81) {
    BaseOccCode1 = _$param80;
    err = _$param81;
    BaseOccCode2 = 0;
    lookupAsJson('BASE_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param82, _$param83) {
      BaseOccCode2 = _$param82;
      err = _$param83;
      if (BaseOccCode1 == 'D') {
        BaseOccCode1A = Number(10);
      }
      if (BaseOccCode1 == 'IC') {
        BaseOccCode1A = Number(5);
      }
      if (BaseOccCode1 == '4') {
        BaseOccCode1A = Number(4);
      }
      if (BaseOccCode1 == '3') {
        BaseOccCode1A = Number(3);
      }
      if (BaseOccCode1 == '2') {
        BaseOccCode1A = Number(2);
      }
      if (BaseOccCode1 == '1') {
        BaseOccCode1A = Number(1);
      }
      if (BaseOccCode2 == 'D') {
        BaseOccCode2A = Number(10);
      }
      if (BaseOccCode2 == 'IC') {
        BaseOccCode2A = Number(5);
      }
      if (BaseOccCode2 == '4') {
        BaseOccCode2A = Number(4);
      }
      if (BaseOccCode2 == '3') {
        BaseOccCode2A = Number(3);
      }
      if (BaseOccCode2 == '2') {
        BaseOccCode2A = Number(2);
      }
      if (BaseOccCode2 == '1') {
        BaseOccCode2A = Number(1);
      }
      updateJSON(inoutMap, 'BaseOccCode1', BaseOccCode1);
      updateJSON(inoutMap, 'BaseOccCode2', BaseOccCode2);
      updateJSON(inoutMap, 'BaseOccCode1A', BaseOccCode1A);
      updateJSON(inoutMap, 'BaseOccCode2A', BaseOccCode2A);
      successCB(inoutMap);
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function OccupationaLRuleHBOccCodeGenSave(inoutMap, successCB) {
  var HBOccCode1, HBOccCode2, HBOccCode1A, HBOccCode2A, err;
  HBOccCode1 = '';
  HBOccCode2 = '';
  HBOccCode1A = Number(0);
  HBOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.HBRiders.isRequired == 'Yes') {
      HBOccCode1 = 0;
      lookupAsJson('HB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param84, _$param85) {
        HBOccCode1 = _$param84;
        err = _$param85;
        HBOccCode2 = 0;
        lookupAsJson('HB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param86, _$param87) {
          HBOccCode2 = _$param86;
          err = _$param87;
          if (HBOccCode1 == 'D') {
            HBOccCode1A = Number(10);
          }
          if (HBOccCode1 == 'IC') {
            HBOccCode1A = Number(5);
          }
          if (HBOccCode1 == '4') {
            HBOccCode1A = Number(4);
          }
          if (HBOccCode1 == '3') {
            HBOccCode1A = Number(3);
          }
          if (HBOccCode1 == '2') {
            HBOccCode1A = Number(2);
          }
          if (HBOccCode1 == '1') {
            HBOccCode1A = Number(1);
          }
          if (HBOccCode2 == 'D') {
            HBOccCode2A = Number(10);
          }
          if (HBOccCode2 == 'IC') {
            HBOccCode2A = Number(5);
          }
          if (HBOccCode2 == '4') {
            HBOccCode2A = Number(4);
          }
          if (HBOccCode2 == '3') {
            HBOccCode2A = Number(3);
          }
          if (HBOccCode2 == '2') {
            HBOccCode2A = Number(2);
          }
          if (HBOccCode2 == '1') {
            HBOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'HBOccCode1', HBOccCode1);
    updateJSON(inoutMap, 'HBOccCode2', HBOccCode2);
    updateJSON(inoutMap, 'HBOccCode1A', HBOccCode1A);
    updateJSON(inoutMap, 'HBOccCode2A', HBOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleHSOccCodeGenSave(inoutMap, successCB) {
  var HSOccCode1, HSOccCode2, HSOccCode1A, HSOccCode2A, err;
  HSOccCode1 = '';
  HSOccCode2 = '';
  HSOccCode1A = Number(0);
  HSOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.HSRiders.isRequired == 'Yes') {
      HSOccCode1 = 0;
      lookupAsJson('HS_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param88, _$param89) {
        HSOccCode1 = _$param88;
        err = _$param89;
        HSOccCode2 = 0;
        lookupAsJson('HS_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param90, _$param91) {
          HSOccCode2 = _$param90;
          err = _$param91;
          if (HSOccCode1 == 'D') {
            HSOccCode1A = Number(10);
          }
          if (HSOccCode1 == 'IC') {
            HSOccCode1A = Number(5);
          }
          if (HSOccCode1 == '4') {
            HSOccCode1A = Number(4);
          }
          if (HSOccCode1 == '3') {
            HSOccCode1A = Number(3);
          }
          if (HSOccCode1 == '2') {
            HSOccCode1A = Number(2);
          }
          if (HSOccCode1 == '1') {
            HSOccCode1A = Number(1);
          }
          if (HSOccCode2 == 'D') {
            HSOccCode2A = Number(10);
          }
          if (HSOccCode2 == 'IC') {
            HSOccCode2A = Number(5);
          }
          if (HSOccCode2 == '4') {
            HSOccCode2A = Number(4);
          }
          if (HSOccCode2 == '3') {
            HSOccCode2A = Number(3);
          }
          if (HSOccCode2 == '2') {
            HSOccCode2A = Number(2);
          }
          if (HSOccCode2 == '1') {
            HSOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'HSOccCode1', HSOccCode1);
    updateJSON(inoutMap, 'HSOccCode2', HSOccCode2);
    updateJSON(inoutMap, 'HSOccCode1A', HSOccCode1A);
    updateJSON(inoutMap, 'HSOccCode2A', HSOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleADDCodeGenSave(inoutMap, successCB) {
  var ADDOccCode1, ADDOccCode2, ADDOccCode1A, ADDOccCode2A, err;
  ADDOccCode1 = '';
  ADDOccCode2 = '';
  ADDOccCode1A = Number(0);
  ADDOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.ADDRiders.isRequired == 'Yes') {
      ADDOccCode1 = 0;
      lookupAsJson('ADD_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param92, _$param93) {
        ADDOccCode1 = _$param92;
        err = _$param93;
        ADDOccCode2 = 0;
        lookupAsJson('ADD_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param94, _$param95) {
          ADDOccCode2 = _$param94;
          err = _$param95;
          if (ADDOccCode1 == 'D') {
            ADDOccCode1A = Number(10);
          }
          if (ADDOccCode1 == 'IC') {
            ADDOccCode1A = Number(5);
          }
          if (ADDOccCode1 == '4') {
            ADDOccCode1A = Number(4);
          }
          if (ADDOccCode1 == '3') {
            ADDOccCode1A = Number(3);
          }
          if (ADDOccCode1 == '2') {
            ADDOccCode1A = Number(2);
          }
          if (ADDOccCode1 == '1') {
            ADDOccCode1A = Number(1);
          }
          if (ADDOccCode2 == 'D') {
            ADDOccCode2A = Number(10);
          }
          if (ADDOccCode2 == 'IC') {
            ADDOccCode2A = Number(5);
          }
          if (ADDOccCode2 == '4') {
            ADDOccCode2A = Number(4);
          }
          if (ADDOccCode2 == '3') {
            ADDOccCode2A = Number(3);
          }
          if (ADDOccCode2 == '2') {
            ADDOccCode2A = Number(2);
          }
          if (ADDOccCode2 == '1') {
            ADDOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'ADDOccCode1', ADDOccCode1);
    updateJSON(inoutMap, 'ADDOccCode2', ADDOccCode2);
    updateJSON(inoutMap, 'ADDOccCode1A', ADDOccCode1A);
    updateJSON(inoutMap, 'ADDOccCode2A', ADDOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleADBCodeGenSave(inoutMap, successCB) {
  var ADBOccCode1, ADBOccCode2, ADBOccCode1A, ADBOccCode2A, err;
  ADBOccCode1 = '';
  ADBOccCode2 = '';
  ADBOccCode1A = Number(0);
  ADBOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.ADBRider.isRequired == 'Yes') {
      ADBOccCode1 = 0;
      lookupAsJson('ADB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param96, _$param97) {
        ADBOccCode1 = _$param96;
        err = _$param97;
        ADBOccCode2 = 0;
        lookupAsJson('ADB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param98, _$param99) {
          ADBOccCode2 = _$param98;
          err = _$param99;
          if (ADBOccCode1 == 'D') {
            ADBOccCode1A = Number(10);
          }
          if (ADBOccCode1 == 'IC') {
            ADBOccCode1A = Number(5);
          }
          if (ADBOccCode1 == '4') {
            ADBOccCode1A = Number(4);
          }
          if (ADBOccCode1 == '3') {
            ADBOccCode1A = Number(3);
          }
          if (ADBOccCode1 == '2') {
            ADBOccCode1A = Number(2);
          }
          if (ADBOccCode1 == '1') {
            ADBOccCode1A = Number(1);
          }
          if (ADBOccCode2 == 'D') {
            ADBOccCode2A = Number(10);
          }
          if (ADBOccCode2 == 'IC') {
            ADBOccCode2A = Number(5);
          }
          if (ADBOccCode2 == '4') {
            ADBOccCode2A = Number(4);
          }
          if (ADBOccCode2 == '3') {
            ADBOccCode2A = Number(3);
          }
          if (ADBOccCode2 == '2') {
            ADBOccCode2A = Number(2);
          }
          if (ADBOccCode2 == '1') {
            ADBOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'ADBOccCode1', ADBOccCode1);
    updateJSON(inoutMap, 'ADBOccCode2', ADBOccCode2);
    updateJSON(inoutMap, 'ADBOccCode1A', ADBOccCode1A);
    updateJSON(inoutMap, 'ADBOccCode2A', ADBOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleAIOccCodeGenSave(inoutMap, successCB) {
  var AIOccCode1, AIOccCode2, AIOccCode1A, AIOccCode2A, err;
  AIOccCode1 = '';
  AIOccCode2 = '';
  AIOccCode1A = Number(0);
  AIOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.AIRider.isRequired == 'Yes') {
      AIOccCode1 = 0;
      lookupAsJson('AI_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param100, _$param101) {
        AIOccCode1 = _$param100;
        err = _$param101;
        AIOccCode2 = 0;
        lookupAsJson('AI_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param102, _$param103) {
          AIOccCode2 = _$param102;
          err = _$param103;
          if (AIOccCode1 == 'D') {
            AIOccCode1A = Number(10);
          }
          if (AIOccCode1 == 'IC') {
            AIOccCode1A = Number(5);
          }
          if (AIOccCode1 == '4') {
            AIOccCode1A = Number(4);
          }
          if (AIOccCode1 == '3') {
            AIOccCode1A = Number(3);
          }
          if (AIOccCode1 == '2') {
            AIOccCode1A = Number(2);
          }
          if (AIOccCode1 == '1') {
            AIOccCode1A = Number(1);
          }
          if (AIOccCode2 == 'D') {
            AIOccCode2A = Number(10);
          }
          if (AIOccCode2 == 'IC') {
            AIOccCode2A = Number(5);
          }
          if (AIOccCode2 == '4') {
            AIOccCode2A = Number(4);
          }
          if (AIOccCode2 == '3') {
            AIOccCode2A = Number(3);
          }
          if (AIOccCode2 == '2') {
            AIOccCode2A = Number(2);
          }
          if (AIOccCode2 == '1') {
            AIOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'AIOccCode1', AIOccCode1);
    updateJSON(inoutMap, 'AIOccCode2', AIOccCode2);
    updateJSON(inoutMap, 'AIOccCode1A', AIOccCode1A);
    updateJSON(inoutMap, 'AIOccCode2A', AIOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRulePBOccCodeGenSave(inoutMap, successCB) {
  var PBOccCode1, PBOccCode2, PBOccCode1A, PBOccCode2A, err;
  PBOccCode1 = '';
  PBOccCode2 = '';
  PBOccCode1A = Number(0);
  PBOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.PBRider.isRequired == 'Yes') {
      PBOccCode1 = 0;
      lookupAsJson('PB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PayorDetails.occCode1 + '\'', function (arguments, _$param104, _$param105) {
        PBOccCode1 = _$param104;
        err = _$param105;
        PBOccCode2 = 0;
        lookupAsJson('PB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PayorDetails.occCode2 + '\'', function (arguments, _$param106, _$param107) {
          PBOccCode2 = _$param106;
          err = _$param107;
          if (PBOccCode1 == 'D') {
            PBOccCode1A = Number(10);
          }
          if (PBOccCode1 == 'IC') {
            PBOccCode1A = Number(5);
          }
          if (PBOccCode1 == '4') {
            PBOccCode1A = Number(4);
          }
          if (PBOccCode1 == '3') {
            PBOccCode1A = Number(3);
          }
          if (PBOccCode1 == '2') {
            PBOccCode1A = Number(2);
          }
          if (PBOccCode1 == '1') {
            PBOccCode1A = Number(1);
          }
          if (PBOccCode2 == 'D') {
            PBOccCode2A = Number(10);
          }
          if (PBOccCode2 == 'IC') {
            PBOccCode2A = Number(5);
          }
          if (PBOccCode2 == '4') {
            PBOccCode2A = Number(4);
          }
          if (PBOccCode2 == '3') {
            PBOccCode2A = Number(3);
          }
          if (PBOccCode2 == '2') {
            PBOccCode2A = Number(2);
          }
          if (PBOccCode2 == '1') {
            PBOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'PBOccCode1', PBOccCode1);
    updateJSON(inoutMap, 'PBOccCode2', PBOccCode2);
    updateJSON(inoutMap, 'PBOccCode1A', PBOccCode1A);
    updateJSON(inoutMap, 'PBOccCode2A', PBOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleWBOccCodeGenSave(inoutMap, successCB) {
  var WPOccCode1, WPOccCode2, WPOccCode1A, WPOccCode2A, err;
  WPOccCode1 = '';
  WPOccCode2 = '';
  WPOccCode1A = Number(0);
  WPOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.WPRider.isRequired == 'Yes') {
      WPOccCode1 = 0;
      lookupAsJson('WP_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param108, _$param109) {
        WPOccCode1 = _$param108;
        err = _$param109;
        WPOccCode2 = 0;
        lookupAsJson('WP_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param110, _$param111) {
          WPOccCode2 = _$param110;
          err = _$param111;
          if (WPOccCode1 == 'D') {
            WPOccCode1A = Number(10);
          }
          if (WPOccCode1 == 'IC') {
            WPOccCode1A = Number(5);
          }
          if (WPOccCode1 == '4') {
            WPOccCode1A = Number(4);
          }
          if (WPOccCode1 == '3') {
            WPOccCode1A = Number(3);
          }
          if (WPOccCode1 == '2') {
            WPOccCode1A = Number(2);
          }
          if (WPOccCode1 == '1') {
            WPOccCode1A = Number(1);
          }
          if (WPOccCode2 == 'D') {
            WPOccCode2A = Number(10);
          }
          if (WPOccCode2 == 'IC') {
            WPOccCode2A = Number(5);
          }
          if (WPOccCode2 == '4') {
            WPOccCode2A = Number(4);
          }
          if (WPOccCode2 == '3') {
            WPOccCode2A = Number(3);
          }
          if (WPOccCode2 == '2') {
            WPOccCode2A = Number(2);
          }
          if (WPOccCode2 == '1') {
            WPOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'WPOccCode1', WPOccCode1);
    updateJSON(inoutMap, 'WPOccCode2', WPOccCode2);
    updateJSON(inoutMap, 'WPOccCode1A', WPOccCode1A);
    updateJSON(inoutMap, 'WPOccCode2A', WPOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleDDCodeGenSave(inoutMap, successCB) {
  var DDOccCode1, DDOccCode2, DDOccCode1A, DDOccCode2A, err;
  DDOccCode1 = '';
  DDOccCode2 = '';
  DDOccCode1A = Number(0);
  DDOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.DDRider.isRequired == 'Yes') {
      DDOccCode1 = 0;
      lookupAsJson('DD_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param112, _$param113) {
        DDOccCode1 = _$param112;
        err = _$param113;
        DDOccCode2 = 0;
        lookupAsJson('DD_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param114, _$param115) {
          DDOccCode2 = _$param114;
          err = _$param115;
          if (DDOccCode1 == 'D') {
            DDOccCode1A = Number(10);
          }
          if (DDOccCode1 == 'IC') {
            DDOccCode1A = Number(5);
          }
          if (DDOccCode1 == '4') {
            DDOccCode1A = Number(4);
          }
          if (DDOccCode1 == '3') {
            DDOccCode1A = Number(3);
          }
          if (DDOccCode1 == '2') {
            DDOccCode1A = Number(2);
          }
          if (DDOccCode1 == '1') {
            DDOccCode1A = Number(1);
          }
          if (DDOccCode2 == 'D') {
            DDOccCode2A = Number(10);
          }
          if (DDOccCode2 == 'IC') {
            DDOccCode2A = Number(5);
          }
          if (DDOccCode2 == '4') {
            DDOccCode2A = Number(4);
          }
          if (DDOccCode2 == '3') {
            DDOccCode2A = Number(3);
          }
          if (DDOccCode2 == '2') {
            DDOccCode2A = Number(2);
          }
          if (DDOccCode2 == '1') {
            DDOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'DDOccCode1', DDOccCode1);
    updateJSON(inoutMap, 'DDOccCode2', DDOccCode2);
    updateJSON(inoutMap, 'DDOccCode1A', DDOccCode1A);
    updateJSON(inoutMap, 'DDOccCode2A', DDOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function GenSave20Plus_WPPremium_05(inoutMap, successCB) {
  var annPremiumWP, monthPremiumWP, quarPremiumWP, semAnnPremiumWP, PremiumWP1, CalculatedWPRiderSumAssured, WPRiderSumAssured1, monthVar, inoutMap, err, WPRate;
  if (inoutMap.PolicyDetails.sumAssured > Number(4000000)) {
    CalculatedWPRiderSumAssured = sub(inoutMap.WPRider.sumAssured, Number(4000000));
    WPRiderSumAssured1 = inoutMap.WPRider.sumAssured;
  } else {
    CalculatedWPRiderSumAssured = Number(0);
    WPRiderSumAssured1 = inoutMap.PolicyDetails.sumAssured;
  }
  GenSave20Plus_LookUp_01(inoutMap, function (arguments, _$param116, _$param117) {
    inoutMap = _$param116;
    err = _$param117;
    annPremiumWP = div(mul(inoutMap.premRate, CalculatedWPRiderSumAssured), 1000);
    monthVar = formatDecimal(mul(inoutMap.premRate, inoutMap.monthRate), 2);
    monthPremiumWP = formatDecimal(mul(monthVar, CalculatedWPRiderSumAssured), 2);
    monthPremiumWP = formatDecimal(div(monthPremiumWP, 1000), 0);
    quarVar = formatDecimal(mul(inoutMap.premRate, inoutMap.quarRate), 2);
    quarPremiumWP = formatDecimal(mul(quarVar, CalculatedWPRiderSumAssured), 2);
    quarPremiumWP = formatDecimal(div(quarPremiumWP, 1000), 0);
    semAnnVar = formatDecimal(mul(inoutMap.premRate, inoutMap.semiAnnRate), 2);
    semAnnPremiumWP = formatDecimal(mul(semAnnVar, CalculatedWPRiderSumAssured), 2);
    semAnnPremiumWP = formatDecimal(div(semAnnPremiumWP, 1000), 0);
    PremiumWP1 = annPremiumWP;
    if (inoutMap.PolicyDetails.premiumMode == 6002) {
      PremiumWP1 = semAnnPremiumWP;
    }
    if (inoutMap.PolicyDetails.premiumMode == 6003) {
      PremiumWP1 = quarPremiumWP;
    }
    if (inoutMap.PolicyDetails.premiumMode == 6004) {
      PremiumWP1 = monthPremiumWP;
    }
    Period = min(inoutMap.PolicyDetails.premiumPayingTerm, sub(60, inoutMap.InsuredDetails.age));
    WPRate = 0;
    lookupAsJson('RATE_FACTOR', 'EXTN_RIDER_WP_PREM_TBL', 'INSURED_AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'INSURED_SEX=\'' + inoutMap.InsuredDetails.sex + '\'', 'PAYMENT_PERIOD=\'' + Period + '\'', 'PROD_ID=1006', function (arguments, _$param118, _$param119) {
      WPRate = _$param118;
      err = _$param119;
      PremiumWP1 = formatDecimal(mul(PremiumWP1, WPRate), 2);
      PremiumWP1 = formatDecimal(div(PremiumWP1, 100), 0);
      updateJSON(inoutMap, 'annPremiumWP', annPremiumWP);
      updateJSON(inoutMap, 'monthPremiumWP', monthPremiumWP);
      updateJSON(inoutMap, 'quarPremiumWP', quarPremiumWP);
      updateJSON(inoutMap, 'semAnnPremiumWP', semAnnPremiumWP);
      updateJSON(inoutMap, 'PremiumWP1', PremiumWP1);
      updateJSON(inoutMap, 'CalculatedWPRiderSumAssured', CalculatedWPRiderSumAssured);
      updateJSON(inoutMap, 'WPRiderSumAssured1', WPRiderSumAssured1);
      updateJSON(inoutMap, 'monthVar', monthVar);
      successCB(inoutMap);
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function OccupationaLRuleADBRCCCodeGenSave(inoutMap, successCB) {
  var ADBRCCOccCode1, ADBRCCOccCode2, ADBRCCOccCode1A, ADBRCCOccCode2A, err;
  ADBRCCOccCode1 = '';
  ADBRCCOccCode2 = '';
  ADBRCCOccCode1A = Number(0);
  ADBRCCOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.RCCADBRider.isRequired == 'Yes') {
      ADBRCCOccCode1 = 0;
      lookupAsJson('ADBRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param120, _$param121) {
        ADBRCCOccCode1 = _$param120;
        err = _$param121;
        ADBRCCOccCode2 = 0;
        lookupAsJson('ADBRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param122, _$param123) {
          ADBRCCOccCode2 = _$param122;
          err = _$param123;
          if (ADBRCCOccCode1 == 'D') {
            ADBRCCOccCode1A = Number(10);
          }
          if (ADBRCCOccCode1 == 'IC') {
            ADBRCCOccCode1A = Number(5);
          }
          if (ADBRCCOccCode1 == 'Y') {
            ADBRCCOccCode1A = Number(1);
          }
          if (ADBRCCOccCode2 == 'D') {
            ADBRCCOccCode2A = Number(10);
          }
          if (ADBRCCOccCode2 == 'IC') {
            ADBRCCOccCode2A = Number(5);
          }
          if (ADBRCCOccCode2 == 'Y') {
            ADBRCCOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'ADBRCCOccCode1', ADBRCCOccCode1);
    updateJSON(inoutMap, 'ADBRCCOccCode2', ADBRCCOccCode2);
    updateJSON(inoutMap, 'ADBRCCOccCode1A', ADBRCCOccCode1A);
    updateJSON(inoutMap, 'ADBRCCOccCode2A', ADBRCCOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleUpdated1GenSave(inoutMap, successCB) {
  var occupationalCode, BaseOccCode, CIOccCode, DDOccCode, WPOccCode, PBOccCode, AIOccCode, HBOccCode, HSOccCode, ADDOccCode, ADBOccCode, ADBRCCOccCode, ADDRCCOccCode, AIRCCOccCode, OccCode1D, OccCode2D, OccCode1IC, OccCode2IC, SumOccCode1, SumOccCode2, Option, SumOccCode2A, SumOccCode1A, OccCode1, OccCode2, inoutMap, err;
  OccCode1 = [];
  OccCode2 = [];
  SumOccCode1 = Number(0);
  SumOccCode2 = Number(0);
  OccCode1D = Number(0);
  OccCode2D = Number(0);
  OccCode1IC = Number(0);
  OccCode2IC = Number(0);
  Option = '';
  inoutMap.ADBOccCode1A = Number(0);
  inoutMap.ADBOccCode2A = Number(0);
  inoutMap.AIOccCode1A = Number(0);
  inoutMap.AIOccCode2A = Number(0);
  inoutMap.PBOccCode1A = Number(0);
  inoutMap.PBOccCode2A = Number(0);
  inoutMap.WPOccCode1A = Number(0);
  inoutMap.WPOccCode2A = Number(0);
  inoutMap.DDOccCode1A = Number(0);
  inoutMap.DDOccCode2A = Number(0);
  inoutMap.ADDOccCode1A = Number(0);
  inoutMap.ADDOccCode2A = Number(0);
  inoutMap.HBOccCode1A = Number(0);
  inoutMap.HBOccCode2A = Number(0);
  inoutMap.HSOccCode1A = Number(0);
  inoutMap.HSOccCode2A = Number(0);
  inoutMap.CIOccCode1A = Number(0);
  inoutMap.CIOccCode2A = Number(0);
  inoutMap.ADBRCCOccCode1A = Number(0);
  inoutMap.ADBRCCCOccCode2A = Number(0);
  inoutMap.ADDRCCOccCode1A = Number(0);
  inoutMap.ADDRCCOccCode2A = Number(0);
  inoutMap.AIRCCOccCode1A = Number(0);
  inoutMap.AIRCCOccCode2A = Number(0);
  OccupationaLRuleBaseCodeGenSave(inoutMap, function (arguments, _$param124, _$param125) {
    inoutMap = _$param124;
    err = _$param125;
    OccupationaLRuleHBOccCodeGenSave(inoutMap, function (arguments, _$param126, _$param127) {
      inoutMap = _$param126;
      err = _$param127;
      OccupationaLRuleHSOccCodeGenSave(inoutMap, function (arguments, _$param128, _$param129) {
        inoutMap = _$param128;
        err = _$param129;
        OccupationaLRuleADDCodeGenSave(inoutMap, function (arguments, _$param130, _$param131) {
          inoutMap = _$param130;
          err = _$param131;
          OccupationaLRuleADBCodeGenSave(inoutMap, function (arguments, _$param132, _$param133) {
            inoutMap = _$param132;
            err = _$param133;
            OccupationaLRuleAIOccCodeGenSave(inoutMap, function (arguments, _$param134, _$param135) {
              inoutMap = _$param134;
              err = _$param135;
              OccupationaLRulePBOccCodeGenSave(inoutMap, function (arguments, _$param136, _$param137) {
                inoutMap = _$param136;
                err = _$param137;
                OccupationaLRuleWBOccCodeGenSave(inoutMap, function (arguments, _$param138, _$param139) {
                  inoutMap = _$param138;
                  err = _$param139;
                  OccupationaLRuleDDCodeGenSave(inoutMap, function (arguments, _$param140, _$param141) {
                    inoutMap = _$param140;
                    err = _$param141;
                    OccupationaLRuleADBRCCCodeGenSave(inoutMap, function (arguments, _$param142, _$param143) {
                      inoutMap = _$param142;
                      err = _$param143;
                      OccupationaLRuleADDRCCCodeGenSave(inoutMap, function (arguments, _$param144, _$param145) {
                        inoutMap = _$param144;
                        err = _$param145;
                        OccupationaLRuleAIRCCCodeGenSave(inoutMap, function (arguments, _$param146, _$param147) {
                          inoutMap = _$param146;
                          err = _$param147;
                          if (inoutMap.ADBRCCOccCode1A == Number(1)) {
                            inoutMap.ADBRCCOccCode1A = inoutMap.ADBOccCode1A;
                          }
                          if (inoutMap.ADBRCCOccCode2A == Number(1)) {
                            inoutMap.ADBRCCOccCode2A = inoutMap.ADBOccCode2A;
                          }
                          if (inoutMap.ADDRCCOccCode1A == Number(1)) {
                            inoutMap.ADDRCCOccCode1A = inoutMap.ADDOccCode1A;
                          }
                          if (inoutMap.ADDRCCOccCode2A == Number(1)) {
                            inoutMap.ADDRCCOccCode2A = inoutMap.ADDOccCode2A;
                          }
                          if (inoutMap.AIRCCOccCode1A == Number(1)) {
                            inoutMap.AIRCCOccCode1A = inoutMap.AIOccCode1A;
                          }
                          if (inoutMap.AIRCCOccCode2A == Number(1)) {
                            inoutMap.AIRCCOccCode2A = inoutMap.AIOccCode2A;
                          }
                          SumOccCode2 = sum(inoutMap.BaseOccCode2A, inoutMap.CIOccCode2A, inoutMap.HBOccCode2A, inoutMap.HSOccCode2A, inoutMap.ADDOccCode2A, inoutMap.AIOccCode2A, inoutMap.WPOccCode2A, inoutMap.ADBOccCode2A, inoutMap.PBOccCode2A, inoutMap.DDOccCode2A);
                          SumOccCode1 = sum(inoutMap.BaseOccCode1A, inoutMap.CIOccCode1A, inoutMap.HBOccCode1A, inoutMap.HSOccCode1A, inoutMap.ADDOccCode1A, inoutMap.AIOccCode1A, inoutMap.WPOccCode1A, inoutMap.ADBOccCode1A, inoutMap.PBOccCode1A, inoutMap.DDOccCode1A);
                          SumOccCode2A = sum(inoutMap.ADBRCCOccCode2A, SumOccCode2, inoutMap.ADDRCCOccCode2A, inoutMap.AIRCCOccCode2A);
                          SumOccCode1A = sum(inoutMap.ADBRCCOccCode1A, SumOccCode1, inoutMap.ADDRCCOccCode1A, inoutMap.AIRCCOccCode1A);
                          OccCode1[1] = inoutMap.CIOccCode1A;
                          OccCode1[2] = inoutMap.DDOccCode1A;
                          OccCode1[3] = inoutMap.WPOccCode1A;
                          OccCode1[4] = inoutMap.PBOccCode1A;
                          OccCode1[5] = inoutMap.AIOccCode1A;
                          OccCode1[6] = inoutMap.HBOccCode1A;
                          OccCode1[7] = inoutMap.HSOccCode1A;
                          OccCode1[8] = inoutMap.ADDOccCode1A;
                          OccCode1[9] = inoutMap.ADBOccCode1A;
                          OccCode1[10] = inoutMap.ADBRCCOccCode1A;
                          OccCode1[11] = inoutMap.ADDRCCOccCode1A;
                          OccCode1[12] = inoutMap.AIRCCOccCode1A;
                          OccCode2[1] = inoutMap.CIOccCode2A;
                          OccCode2[2] = inoutMap.DDOccCode2A;
                          OccCode2[3] = inoutMap.WPOccCode2A;
                          OccCode2[4] = inoutMap.PBOccCode2A;
                          OccCode2[5] = inoutMap.AIOccCode2A;
                          OccCode2[6] = inoutMap.HBOccCode2A;
                          OccCode2[7] = inoutMap.HSOccCode2A;
                          OccCode2[8] = inoutMap.ADDOccCode2A;
                          OccCode2[9] = inoutMap.ADBOccCode2A;
                          OccCode2[10] = inoutMap.ADBRCCOccCode2A;
                          OccCode2[11] = inoutMap.ADDRCCOccCode2A;
                          OccCode2[12] = inoutMap.AIRCCOccCode2A;
                          if (inoutMap.BaseOccCode1A >= Number(10)) {
                            Option = 'A';
                          } else {
                            if (inoutMap.BaseOccCode2A >= Number(10)) {
                              Option = 'B';
                            }
                          }
                          i = 1;
                          while (i < 13) {
                            if (OccCode1[i] == Number(10)) {
                              OccCode1D = sum(OccCode1[i], OccCode1D);
                            }
                            if (OccCode2[i] == Number(10)) {
                              OccCode2D = sum(OccCode2[i], OccCode2D);
                            }
                            if (OccCode1[i] == Number(5)) {
                              OccCode1IC = sum(OccCode2[i], OccCode1IC);
                            }
                            if (OccCode2[i] == Number(5)) {
                              OccCode2IC = sum(OccCode2[i], OccCode2IC);
                            }
                            i++;
                          }
                          if (Number(SumOccCode1) == Number(SumOccCode2) && OccCode1D == OccCode2D && OccCode1IC == OccCode2IC && isEmptyOrNull(Option) == Boolean(true)) {
                            if (Number(inoutMap.BaseOccCode1A) >= Number(inoutMap.BaseOccCode2A)) {
                              Option = 'A';
                            } else {
                              Option = 'B';
                            }
                          }
                          if (isEmptyOrNull(Option) == Boolean(true)) {
                            if (OccCode1D > OccCode2D) {
                              Option = 'A';
                            } else {
                              if (OccCode1D < OccCode2D) {
                                Option = 'B';
                              }
                            }
                          }
                          if (isEmptyOrNull(Option) == Boolean(true)) {
                            if (OccCode1IC > OccCode2IC) {
                              Option = 'A';
                            } else {
                              if (OccCode1IC < OccCode2IC) {
                                Option = 'B';
                              }
                            }
                          }
                          if (isEmptyOrNull(Option) == Boolean(true)) {
                            if (Number(SumOccCode1) > Number(SumOccCode2)) {
                              Option = 'A';
                            } else {
                              if (Number(SumOccCode1) < Number(SumOccCode2)) {
                                Option = 'B';
                              }
                            }
                          }
                          if (Option == 'A') {
                            BaseOccCode = inoutMap.BaseOccCode1;
                            CIOccCode = inoutMap.CIOccCode1;
                            DDOccCode = inoutMap.DDOccCode1;
                            WPOccCode = inoutMap.WPOccCode1;
                            PBOccCode = inoutMap.PBOccCode1;
                            AIOccCode = inoutMap.AIOccCode1;
                            HBOccCode = inoutMap.HBOccCode1;
                            HSOccCode = inoutMap.HSOccCode1;
                            ADDOccCode = inoutMap.ADDOccCode1;
                            ADBOccCode = inoutMap.ADBOccCode1;
                            ADBRCCOccCode = inoutMap.ADBRCCOccCode1;
                            ADDRCCOccCode = inoutMap.ADDRCCOccCode1;
                            AIRCCOccCode = inoutMap.AIRCCOccCode1;
                            occupationalCode = inoutMap.PolicyDetails.occCode1;
                          } else {
                            BaseOccCode = inoutMap.BaseOccCode2;
                            CIOccCode = inoutMap.CIOccCode2;
                            DDOccCode = inoutMap.DDOccCode2;
                            WPOccCode = inoutMap.WPOccCode2;
                            PBOccCode = inoutMap.PBOccCode2;
                            AIOccCode = inoutMap.AIOccCode2;
                            HBOccCode = inoutMap.HBOccCode2;
                            HSOccCode = inoutMap.HSOccCode2;
                            ADDOccCode = inoutMap.ADDOccCode2;
                            ADBOccCode = inoutMap.ADBOccCode2;
                            ADBRCCOccCode = inoutMap.ADBRCCOccCode2;
                            ADDRCCOccCode = inoutMap.ADDRCCOccCode2;
                            AIRCCOccCode = inoutMap.AIRCCOccCode2;
                            occupationalCode = inoutMap.PolicyDetails.occCode2;
                          }
                          updateJSON(inoutMap, 'occupationalCode', occupationalCode);
                          updateJSON(inoutMap, 'BaseOccCode', BaseOccCode);
                          updateJSON(inoutMap, 'CIOccCode', CIOccCode);
                          updateJSON(inoutMap, 'DDOccCode', DDOccCode);
                          updateJSON(inoutMap, 'WPOccCode', WPOccCode);
                          updateJSON(inoutMap, 'PBOccCode', PBOccCode);
                          updateJSON(inoutMap, 'AIOccCode', AIOccCode);
                          updateJSON(inoutMap, 'HBOccCode', HBOccCode);
                          updateJSON(inoutMap, 'HSOccCode', HSOccCode);
                          updateJSON(inoutMap, 'ADDOccCode', ADDOccCode);
                          updateJSON(inoutMap, 'ADBOccCode', ADBOccCode);
                          updateJSON(inoutMap, 'ADBRCCOccCode', ADBRCCOccCode);
                          updateJSON(inoutMap, 'ADDRCCOccCode', ADDRCCOccCode);
                          updateJSON(inoutMap, 'AIRCCOccCode', AIRCCOccCode);
                          updateJSON(inoutMap, 'OccCode1D', OccCode1D);
                          updateJSON(inoutMap, 'OccCode2D', OccCode2D);
                          updateJSON(inoutMap, 'OccCode1IC', OccCode1IC);
                          updateJSON(inoutMap, 'OccCode2IC', OccCode2IC);
                          updateJSON(inoutMap, 'SumOccCode1', SumOccCode1);
                          updateJSON(inoutMap, 'SumOccCode2', SumOccCode2);
                          updateJSON(inoutMap, 'Option', Option);
                          updateJSON(inoutMap, 'SumOccCode2A', SumOccCode2A);
                          updateJSON(inoutMap, 'SumOccCode1A', SumOccCode1A);
                          updateJSON(inoutMap, 'OccCode1', OccCode1);
                          updateJSON(inoutMap, 'OccCode2', OccCode2);
                          successCB(inoutMap);
                        }.bind(this, arguments));
                      }.bind(this, arguments));
                    }.bind(this, arguments));
                  }.bind(this, arguments));
                }.bind(this, arguments));
              }.bind(this, arguments));
            }.bind(this, arguments));
          }.bind(this, arguments));
        }.bind(this, arguments));
      }.bind(this, arguments));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function OccupationaLRuleADDRCCCodeGenSave(inoutMap, successCB) {
  var ADDRCCOccCode1, ADDRCCOccCode2, ADDRCCOccCode1A, ADDRCCOccCode2A, err;
  ADDRCCOccCode1 = '';
  ADDRCCOccCode2 = '';
  ADDRCCOccCode1A = Number(0);
  ADDRCCOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.RCCADDRider.isRequired == 'Yes') {
      ADDRCCOccCode1 = 0;
      lookupAsJson('ADDRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param148, _$param149) {
        ADDRCCOccCode1 = _$param148;
        err = _$param149;
        ADDRCCOccCode2 = 0;
        lookupAsJson('ADDRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param150, _$param151) {
          ADDRCCOccCode2 = _$param150;
          err = _$param151;
          if (ADDRCCOccCode1 == 'D') {
            ADDRCCOccCode1A = Number(10);
          }
          if (ADDRCCOccCode1 == 'IC') {
            ADDRCCOccCode1A = Number(5);
          }
          if (ADDRCCOccCode1 == 'Y') {
            ADDRCCOccCode1A = Number(1);
          }
          if (ADDRCCOccCode2 == 'D') {
            ADDRCCOccCode2A = Number(10);
          }
          if (ADDRCCOccCode2 == 'IC') {
            ADDRCCOccCode2A = Number(5);
          }
          if (ADDRCCOccCode2 == 'Y') {
            ADDRCCOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'ADDRCCOccCode1', ADDRCCOccCode1);
    updateJSON(inoutMap, 'ADDRCCOccCode2', ADDRCCOccCode2);
    updateJSON(inoutMap, 'ADDRCCOccCode1A', ADDRCCOccCode1A);
    updateJSON(inoutMap, 'ADDRCCOccCode2A', ADDRCCOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleAIRCCCodeGenSave(inoutMap, successCB) {
  var AIRCCOccCode1, AIRCCOccCode2, AIRCCOccCode1A, AIRCCOccCode2A, err;
  AIRCCOccCode1 = '';
  AIRCCOccCode2 = '';
  AIRCCOccCode1A = Number(0);
  AIRCCOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.RCCAIRider.isRequired == 'Yes') {
      AIRCCOccCode1 = 0;
      lookupAsJson('AIRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param152, _$param153) {
        AIRCCOccCode1 = _$param152;
        err = _$param153;
        AIRCCOccCode2 = 0;
        lookupAsJson('AIRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param154, _$param155) {
          AIRCCOccCode2 = _$param154;
          err = _$param155;
          if (AIRCCOccCode1 == 'D') {
            AIRCCOccCode1A = Number(10);
          }
          if (AIRCCOccCode1 == 'IC') {
            AIRCCOccCode1A = Number(5);
          }
          if (AIRCCOccCode1 == 'Y') {
            AIRCCOccCode1A = Number(1);
          }
          if (AIRCCOccCode2 == 'D') {
            AIRCCOccCode2A = Number(10);
          }
          if (AIRCCOccCode2 == 'IC') {
            AIRCCOccCode2A = Number(5);
          }
          if (AIRCCOccCode2 == 'Y') {
            AIRCCOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'AIRCCOccCode1', AIRCCOccCode1);
    updateJSON(inoutMap, 'AIRCCOccCode2', AIRCCOccCode2);
    updateJSON(inoutMap, 'AIRCCOccCode1A', AIRCCOccCode1A);
    updateJSON(inoutMap, 'AIRCCOccCode2A', AIRCCOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
/* Generated by Continuation.js v0.1.7 */