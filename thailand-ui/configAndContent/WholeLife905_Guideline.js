function WholeLife905RuleSet(inoutMap, successCB) {
  processRuleSet(inoutMap, [
    'Whole_life_905_Premium_02',
    'Wholelife905_BFTable_CVTable_03',
    successCB
  ]);
}
function WholeLife905_Illustrations(inoutMap, successCB) {
  var protectionPeriodFinal, premiumPeriodFinal, sumInsuredFinal, basePremiumFinal, discountedPremium, riderPremiumFinal, WPSumAssuredCover, ADBSumAssuredCover, ADBRCCSumAssuredCover, ADDSumAssuredCover, ADDRCCSumAssuredCover, AISumAssuredCover, AIRCCSumAssuredCover, PBSumAssuredCover, DDSumAssuredCover, HBSumAssuredCover, HSSumAssuredCover, OPDSumAssuredCover, PremiumWPCover, ADBPremiumCover, ADBRCCPremiumCover, ADDPremiumCover, ADDRCCPremiumCover, AIPremiumCover, AIRCCPremiumCover, PBPremiumCover, DDPremiumCover, HSPremiumCover, OPDPremiumCover, HBPremiumCover, totalPremiumFinal, totalLivingBenefit, totalTaxationBenefit, totalPremiumPaid, grandTotalBenefit, totalRatePercent, ADBSumAssured200Percent, ADDSumAssured60Percent, ADDSumAssured25Percent, ADDSumAssured200Percent, AISumAssured60Percent, AISumAssured50Percent, AISumAssured6Percent, AISumAssured2Percent, AISumAssured10Percent, AISumAssured3Percent, AISumAssured200Percent, secondrow, PremiumWPCover1, benefitTableData, illustrationTableData, riderPremium, uniqueRiderName, selectedRiderTbl, planCode, planName, marketableName, shortName, riderName, summaryTableData, col1, col2, col3, col4, col5, i, inoutMap, err;
  benefitTableData = [];
  illustrationTableData = [];
  riderPremium = [];
  uniqueRiderName = [];
  selectedRiderTbl = [];
  planCode = [];
  planName = [];
  marketableName = [];
  shortName = [];
  riderName = [];
  summaryTableData = [];
  col1 = [];
  col2 = [];
  col3 = [];
  col4 = [];
  col5 = [];
  WholeLife905RuleSet(inoutMap, function (arguments, _$param0, _$param1) {
    inoutMap = _$param0;
    err = _$param1;
    protectionPeriodFinal = sub(90, inoutMap.InsuredDetails.age);
    premiumPeriodFinal = inoutMap.PolicyDetails.premiumPayingTerm;
    startDuration = Number(90);
    discountRate = Number(0);
    if (inoutMap.PolicyDetails.sumAssured < Number(2000000)) {
      discountRate = Number(0);
    } else {
      if (inoutMap.PolicyDetails.sumAssured >= Number(2000000) && inoutMap.PolicyDetails.sumAssured <= Number(4999999)) {
        discountRate = Number(2);
      } else {
        if (inoutMap.PolicyDetails.sumAssured >= Number(5000000) && inoutMap.PolicyDetails.sumAssured <= Number(9999999)) {
          discountRate = Number(3);
        } else {
          if (inoutMap.PolicyDetails.sumAssured >= Number(10000000)) {
            discountRate = Number(4);
          }
        }
      }
    }
    sumInsuredFinal = inoutMap.PolicyDetails.sumAssured;
    basePremiumFinal = inoutMap.modalPremium;
    discountedPremium = inoutMap.modalPremium;
    riderPremiumFinal = inoutMap.totalRiderPremium;
    WPSumAssuredCover = inoutMap.WPRiderSumAssured;
    inoutMap.WPRider.SumAssured = inoutMap.WPRiderSumAssured;
    ADBSumAssuredCover = inoutMap.ADBRider.sumAssured;
    ADBRCCSumAssuredCover = inoutMap.ADBRider.sumAssured;
    ADDSumAssuredCover = inoutMap.ADDRiders.sumAssured;
    ADDRCCSumAssuredCover = inoutMap.ADDRiders.sumAssured;
    AISumAssuredCover = inoutMap.AIRider.sumAssured;
    AIRCCSumAssuredCover = inoutMap.AIRider.sumAssured;
    PBSumAssuredCover = inoutMap.PBRider.sumAssured;
    DDSumAssuredCover = inoutMap.DDRider.sumAssured;
    HBSumAssuredCover = inoutMap.HBRiders.sumAssured;
    HSSumAssuredCover = inoutMap.HSRiders.sumAssured;
    OPDSumAssuredCover = inoutMap.OPDRiders.sumAssured;
    PremiumWPCover = inoutMap.PremiumWP;
    ADBPremiumCover = inoutMap.ADBPremium;
    ADBRCCPremiumCover = inoutMap.ADBRCCPremium;
    ADDPremiumCover = inoutMap.ADDPremium;
    ADDRCCPremiumCover = inoutMap.ADDRCCPremium;
    AIPremiumCover = inoutMap.AIPremium;
    AIRCCPremiumCover = inoutMap.AIRCCPremium;
    PBPremiumCover = inoutMap.PBPremium;
    DDPremiumCover = inoutMap.DDPremium;
    HSPremiumCover = inoutMap.HSPremium;
    OPDPremiumCover = inoutMap.OPDPremium;
    HBPremiumCover = inoutMap.HBPremium;
    totalPremiumFinal = inoutMap.totalPolicyPremium;
    discountedPremium = formatDecimal(sub(inoutMap.premRate, discountRate), 0);
    discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.PolicyDetails.sumAssured), 2);
    discountedPremium = formatDecimal(div(discountedPremium, 1000), 0);
    if (inoutMap.PolicyDetails.premiumMode == 6002) {
      discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.semiAnnRate), 0);
    } else {
      if (inoutMap.PolicyDetails.premiumMode == 6003) {
        discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.quarRate), 0);
      } else {
        if (inoutMap.PolicyDetails.premiumMode == 6004) {
          discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.monthRate), 0);
        }
      }
    }
    Counter = Number(0);
    if (inoutMap.WPRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.PBRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.ADBRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.RCCADBRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.ADDRiders.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.RCCADDRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.AIRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.RCCAIRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.HBRiders.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.HSRiders.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.OPDRiders.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (inoutMap.DDRider.isRequired == 'Yes') {
      Counter = sum(Counter, 1);
    }
    if (PremiumWPCover == Number(0) && Number(inoutMap.PolicyDetails.sumAssured) <= Number(4000000)) {
      PremiumWPCover1 = PremiumWPCover;
    } else {
      PremiumWPCover1 = PremiumWPCover;
    }
    i = 0;
    while (i <= sub(Counter, 1)) {
      if (inoutMap.WPRider.isRequired == 'Yes') {
        riderPremium[i] = PremiumWPCover;
        uniqueRiderName[i] = inoutMap.WPRider.uniqueRiderName;
        planCode[i] = '9440';
        planName[i] = 'WP';
        marketableName[i] = 'WP';
        shortName[i] = 'WP';
        col1[i] = 'WP Illustration page';
        col4[i] = inoutMap.WPRider.SumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.PBRider.isRequired == 'Yes') {
        riderPremium[i] = PBPremiumCover;
        uniqueRiderName[i] = inoutMap.PBRider.uniqueRiderName;
        planCode[i] = '9430';
        planName[i] = 'PB';
        marketableName[i] = 'PB';
        shortName[i] = 'PB';
        col1[i] = 'PB Illustration page';
        col4[i] = inoutMap.PBRider.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.ADBRider.isRequired == 'Yes') {
        riderPremium[i] = ADBPremiumCover;
        uniqueRiderName[i] = inoutMap.ADBRider.uniqueRiderName;
        planCode[i] = '9910';
        planName[i] = 'ADB';
        marketableName[i] = 'ADB';
        shortName[i] = 'ADB';
        col1[i] = 'ADB Illustration page';
        col4[i] = inoutMap.ADBRider.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.RCCADBRider.isRequired == 'Yes') {
        riderPremium[i] = ADBRCCPremiumCover;
        uniqueRiderName[i] = inoutMap.RCCADBRider.uniqueRiderName;
        planCode[i] = '9911';
        planName[i] = 'RCC ADB';
        marketableName[i] = 'RCC ADB';
        shortName[i] = 'RCC attached with ADB';
        col1[i] = 'ADBRCC Illustration page';
        col4[i] = inoutMap.ADBRider.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.ADDRiders.isRequired == 'Yes') {
        riderPremium[i] = ADDPremiumCover;
        uniqueRiderName[i] = inoutMap.ADDRiders.uniqueRiderName;
        planCode[i] = '9920';
        planName[i] = 'ADD';
        marketableName[i] = 'ADD';
        shortName[i] = 'ADD';
        col1[i] = 'ADD Illustration page';
        col4[i] = inoutMap.ADDRiders.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.RCCADDRider.isRequired == 'Yes') {
        riderPremium[i] = ADDRCCPremiumCover;
        uniqueRiderName[i] = inoutMap.RCCADDRider.uniqueRiderName;
        planCode[i] = '9921';
        planName[i] = 'RCC ADD';
        marketableName[i] = 'RCC ADD';
        shortName[i] = 'RCC attached with ADD';
        col1[i] = 'ADDRCC Illustration page';
        col4[i] = inoutMap.ADDRiders.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.AIRider.isRequired == 'Yes') {
        riderPremium[i] = AIPremiumCover;
        uniqueRiderName[i] = inoutMap.AIRider.uniqueRiderName;
        planCode[i] = '9930';
        planName[i] = 'AI';
        marketableName[i] = 'AI';
        shortName[i] = 'AI';
        col1[i] = 'AI Illustration page';
        col4[i] = inoutMap.AIRider.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.RCCAIRider.isRequired == 'Yes') {
        riderPremium[i] = AIRCCPremiumCover;
        uniqueRiderName[i] = inoutMap.RCCAIRider.uniqueRiderName;
        planCode[i] = '9931';
        planName[i] = 'RCC AI';
        marketableName[i] = 'RCC AI';
        shortName[i] = 'RCC attached with AI';
        col1[i] = 'AIRCC Illustration page';
        col4[i] = inoutMap.AIRider.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.HBRiders.isRequired == 'Yes') {
        riderPremium[i] = HBPremiumCover;
        uniqueRiderName[i] = inoutMap.HBRiders.uniqueRiderName;
        planCode[i] = '9941';
        planName[i] = 'HB(A)';
        marketableName[i] = 'HB(A)';
        shortName[i] = 'HB(A)';
        col1[i] = 'HB Illustration page';
        col4[i] = inoutMap.HBRiders.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.HSRiders.isRequired == 'Yes') {
        riderPremium[i] = HSPremiumCover;
        uniqueRiderName[i] = inoutMap.HSRiders.uniqueRiderName;
        planCode[i] = '9955';
        planName[i] = 'HS Extra';
        marketableName[i] = 'HS Extra';
        shortName[i] = 'HS Extra';
        col1[i] = 'HS Illustration page';
        col4[i] = inoutMap.HSRiders.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.DDRider.isRequired == 'Yes') {
        riderPremium[i] = DDPremiumCover;
        uniqueRiderName[i] = inoutMap.DDRider.uniqueRiderName;
        planCode[i] = '9412';
        planName[i] = 'DD_2551';
        marketableName[i] = 'DD_2551';
        shortName[i] = 'Dread Disease TMO2551';
        col1[i] = 'DD Illustration page';
        col4[i] = inoutMap.DDRider.sumAssured;
        i = sum(i, 1);
      }
      if (inoutMap.OPDRiders.isRequired == 'Yes') {
        riderPremium[i] = OPDPremiumCover;
        uniqueRiderName[i] = inoutMap.OPDRiders.uniqueRiderName;
        planCode[i] = '9960';
        planName[i] = 'OPD';
        marketableName[i] = 'OPD';
        shortName[i] = 'OPD';
        col1[i] = 'OPD Illustration page';
        col4[i] = inoutMap.OPDRiders.sumAssured;
        i = sum(i, 1);
      }
      i = i + 1;
    }
    if (premiumPeriodFinal == Number(5)) {
      secondrow = 'Wholelife 90/5';
    }
    i = 0;
    while (i <= sub(Counter, 1)) {
      rowRiderData = {};
      rowRiderData.uniqueRiderName = uniqueRiderName[i];
      rowRiderData.riderNameForPdf = col1[i];
      rowRiderData.riderSumAssured = col4[i];
      rowRiderData.riderPremium = riderPremium[i];
      rowRiderData.planCode = planCode[i];
      rowRiderData.planName = planName[i];
      rowRiderData.marketableName = marketableName[i];
      rowRiderData.shortName = shortName[i];
      selectedRiderTbl[i] = rowRiderData;
      i = i + 1;
    }
    i = 0;
    while (i <= sum(Counter, 3)) {
      if (i == Number(0)) {
        rowSummaryTableData = {};
        rowSummaryTableData.col1 = 'Insurance';
        rowSummaryTableData.col2 = '';
        rowSummaryTableData.col3 = '';
        rowSummaryTableData.col4 = '';
        rowSummaryTableData.col5 = '';
        summaryTableData[i] = rowSummaryTableData;
      }
      if (i == Number(1)) {
        rowSummaryTableData = {};
        rowSummaryTableData.col1 = secondrow;
        rowSummaryTableData.col2 = protectionPeriodFinal;
        rowSummaryTableData.col3 = premiumPeriodFinal;
        rowSummaryTableData.col4 = inoutMap.PolicyDetails.sumAssured;
        rowSummaryTableData.col5 = discountedPremium;
        summaryTableData[i] = rowSummaryTableData;
      }
      if (i > Number(2)) {
        rowSummaryTableData = {};
        rowSummaryTableData.col1 = col1[i - 3];
        rowSummaryTableData.col2 = '';
        rowSummaryTableData.col3 = '';
        rowSummaryTableData.col4 = col4[i - 3];
        rowSummaryTableData.col5 = riderPremium[i - 3];
        summaryTableData[i] = rowSummaryTableData;
      }
      if (i == Number(2)) {
        rowSummaryTableData = {};
        rowSummaryTableData.col1 = 'Additional contract';
        rowSummaryTableData.col2 = '';
        rowSummaryTableData.col3 = '';
        rowSummaryTableData.col4 = '';
        rowSummaryTableData.col5 = '';
        summaryTableData[i] = rowSummaryTableData;
      }
      if (i == sum(Counter, 3)) {
        rowSummaryTableData = {};
        rowSummaryTableData.col1 = 'Total premiums';
        rowSummaryTableData.col2 = '';
        rowSummaryTableData.col3 = '';
        rowSummaryTableData.col4 = '';
        rowSummaryTableData.col5 = totalPremiumFinal;
        summaryTableData[i] = rowSummaryTableData;
      }
      if (i > Number(2) && col1[i - 3] == 'WP Illustration page') {
        rowSummaryTableData = {};
        rowSummaryTableData.col1 = col1[i - 3];
        rowSummaryTableData.col2 = '';
        rowSummaryTableData.col3 = '';
        rowSummaryTableData.col4 = col4[i - 3];
        rowSummaryTableData.col5 = PremiumWPCover1;
        summaryTableData[i] = rowSummaryTableData;
      }
      i = i + 1;
    }
    i = 0;
    while (i <= sub(startDuration, inoutMap.InsuredDetails.age, 1)) {
      rowData = {};
      rowData.policyYear = inoutMap.benefitYear[i];
      rowData.age = inoutMap.benefitAge[i];
      rowData.cashValue = inoutMap.cashValue[i];
      rowData.rpuCash = inoutMap.rpuCash[i];
      rowData.rpuSA = inoutMap.rpuSA[i];
      rowData.extPeriodYear = inoutMap.extPeriodYear[i];
      rowData.extPeriodDay = inoutMap.extPeriodDay[i];
      rowData.etiCash = inoutMap.etiCash[i];
      rowData.etiSA = inoutMap.etiSA[i];
      illustrationTableData[i] = rowData;
      i = i + 1;
    }
    i = 0;
    while (i <= sub(startDuration, inoutMap.InsuredDetails.age)) {
      rowBenefitData = {};
      if (i < sub(startDuration, inoutMap.InsuredDetails.age)) {
        rowBenefitData.policyYear = inoutMap.benefitYear[i];
        rowBenefitData.age = inoutMap.benefitAge[i];
        rowBenefitData.annualisedPremium = inoutMap.benefitPremium[i];
        rowBenefitData.benefitRate = inoutMap.benefitCashRate[i];
        rowBenefitData.benefitAmount = inoutMap.benefitCashAmount[i];
        rowBenefitData.benefitTaxAmount = inoutMap.benefitTaxAmount[i];
        rowBenefitData.benefitLifeRate = inoutMap.benefitLifeRate[i];
        rowBenefitData.benefitLifeAmount = inoutMap.benefitLifeAmount[i];
      } else {
        rowBenefitData.policyYear = 'Included';
        rowBenefitData.age = '';
        rowBenefitData.annualisedPremium = inoutMap.sumOfPremium;
        rowBenefitData.benefitRate = inoutMap.sumOfRatePercent;
        rowBenefitData.benefitAmount = inoutMap.sumOfLivingBenefit;
        rowBenefitData.benefitTaxAmount = inoutMap.sumOfTaxBenefit;
        rowBenefitData.benefitLifeRate = '';
        rowBenefitData.benefitLifeAmount = '';
      }
      benefitTableData[i] = rowBenefitData;
      i = i + 1;
    }
    totalLivingBenefit = inoutMap.sumOfLivingBenefit;
    totalTaxationBenefit = inoutMap.sumOfTaxBenefit;
    totalPremiumPaid = inoutMap.sumOfPremium;
    grandTotalBenefit = inoutMap.totalBenefit;
    totalRatePercent = inoutMap.sumOfRatePercent;
    ADBSumAssured200Percent = formatDecimal(mul(Number(inoutMap.ADBRider.sumAssured), 2), 0);
    ADDSumAssured60Percent = formatDecimal(mul(Number(inoutMap.ADDRiders.sumAssured), 0.6), 0);
    ADDSumAssured25Percent = formatDecimal(mul(Number(inoutMap.ADDRiders.sumAssured), 0.25), 0);
    ADDSumAssured200Percent = formatDecimal(mul(Number(inoutMap.ADDRiders.sumAssured), 2), 0);
    AISumAssured60Percent = formatDecimal(mul(Number(inoutMap.AIRider.sumAssured), 0.6), 0);
    AISumAssured50Percent = formatDecimal(mul(Number(inoutMap.AIRider.sumAssured), 0.25), 0);
    AISumAssured6Percent = formatDecimal(mul(Number(inoutMap.AIRider.sumAssured), 0.006), 0);
    AISumAssured2Percent = formatDecimal(mul(Number(inoutMap.AIRider.sumAssured), 0.002), 0);
    AISumAssured10Percent = formatDecimal(mul(Number(inoutMap.AIRider.sumAssured), 0.1), 0);
    AISumAssured3Percent = formatDecimal(mul(Number(inoutMap.AIRider.sumAssured), 0.003), 0);
    AISumAssured200Percent = formatDecimal(mul(Number(inoutMap.AIRider.sumAssured), 2), 0);
    WholeLife905_BIGraph(inoutMap, function (arguments, _$param2, _$param3) {
      inoutMap = _$param2;
      err = _$param3;
      updateJSON(inoutMap, 'protectionPeriodFinal', protectionPeriodFinal);
      updateJSON(inoutMap, 'premiumPeriodFinal', premiumPeriodFinal);
      updateJSON(inoutMap, 'sumInsuredFinal', sumInsuredFinal);
      updateJSON(inoutMap, 'basePremiumFinal', basePremiumFinal);
      updateJSON(inoutMap, 'discountedPremium', discountedPremium);
      updateJSON(inoutMap, 'riderPremiumFinal', riderPremiumFinal);
      updateJSON(inoutMap, 'WPSumAssuredCover', WPSumAssuredCover);
      updateJSON(inoutMap, 'ADBSumAssuredCover', ADBSumAssuredCover);
      updateJSON(inoutMap, 'ADBRCCSumAssuredCover', ADBRCCSumAssuredCover);
      updateJSON(inoutMap, 'ADDSumAssuredCover', ADDSumAssuredCover);
      updateJSON(inoutMap, 'ADDRCCSumAssuredCover', ADDRCCSumAssuredCover);
      updateJSON(inoutMap, 'AISumAssuredCover', AISumAssuredCover);
      updateJSON(inoutMap, 'AIRCCSumAssuredCover', AIRCCSumAssuredCover);
      updateJSON(inoutMap, 'PBSumAssuredCover', PBSumAssuredCover);
      updateJSON(inoutMap, 'DDSumAssuredCover', DDSumAssuredCover);
      updateJSON(inoutMap, 'HBSumAssuredCover', HBSumAssuredCover);
      updateJSON(inoutMap, 'HSSumAssuredCover', HSSumAssuredCover);
      updateJSON(inoutMap, 'OPDSumAssuredCover', OPDSumAssuredCover);
      updateJSON(inoutMap, 'PremiumWPCover', PremiumWPCover);
      updateJSON(inoutMap, 'ADBPremiumCover', ADBPremiumCover);
      updateJSON(inoutMap, 'ADBRCCPremiumCover', ADBRCCPremiumCover);
      updateJSON(inoutMap, 'ADDPremiumCover', ADDPremiumCover);
      updateJSON(inoutMap, 'ADDRCCPremiumCover', ADDRCCPremiumCover);
      updateJSON(inoutMap, 'AIPremiumCover', AIPremiumCover);
      updateJSON(inoutMap, 'AIRCCPremiumCover', AIRCCPremiumCover);
      updateJSON(inoutMap, 'PBPremiumCover', PBPremiumCover);
      updateJSON(inoutMap, 'DDPremiumCover', DDPremiumCover);
      updateJSON(inoutMap, 'HSPremiumCover', HSPremiumCover);
      updateJSON(inoutMap, 'OPDPremiumCover', OPDPremiumCover);
      updateJSON(inoutMap, 'HBPremiumCover', HBPremiumCover);
      updateJSON(inoutMap, 'totalPremiumFinal', totalPremiumFinal);
      updateJSON(inoutMap, 'totalLivingBenefit', totalLivingBenefit);
      updateJSON(inoutMap, 'totalTaxationBenefit', totalTaxationBenefit);
      updateJSON(inoutMap, 'totalPremiumPaid', totalPremiumPaid);
      updateJSON(inoutMap, 'grandTotalBenefit', grandTotalBenefit);
      updateJSON(inoutMap, 'totalRatePercent', totalRatePercent);
      updateJSON(inoutMap, 'ADBSumAssured200Percent', ADBSumAssured200Percent);
      updateJSON(inoutMap, 'ADDSumAssured60Percent', ADDSumAssured60Percent);
      updateJSON(inoutMap, 'ADDSumAssured25Percent', ADDSumAssured25Percent);
      updateJSON(inoutMap, 'ADDSumAssured200Percent', ADDSumAssured200Percent);
      updateJSON(inoutMap, 'AISumAssured60Percent', AISumAssured60Percent);
      updateJSON(inoutMap, 'AISumAssured50Percent', AISumAssured50Percent);
      updateJSON(inoutMap, 'AISumAssured6Percent', AISumAssured6Percent);
      updateJSON(inoutMap, 'AISumAssured2Percent', AISumAssured2Percent);
      updateJSON(inoutMap, 'AISumAssured10Percent', AISumAssured10Percent);
      updateJSON(inoutMap, 'AISumAssured3Percent', AISumAssured3Percent);
      updateJSON(inoutMap, 'AISumAssured200Percent', AISumAssured200Percent);
      updateJSON(inoutMap, 'secondrow', secondrow);
      updateJSON(inoutMap, 'PremiumWPCover1', PremiumWPCover1);
      updateJSON(inoutMap, 'benefitTableData', benefitTableData);
      updateJSON(inoutMap, 'illustrationTableData', illustrationTableData);
      updateJSON(inoutMap, 'riderPremium', riderPremium);
      updateJSON(inoutMap, 'uniqueRiderName', uniqueRiderName);
      updateJSON(inoutMap, 'selectedRiderTbl', selectedRiderTbl);
      updateJSON(inoutMap, 'planCode', planCode);
      updateJSON(inoutMap, 'planName', planName);
      updateJSON(inoutMap, 'marketableName', marketableName);
      updateJSON(inoutMap, 'shortName', shortName);
      updateJSON(inoutMap, 'riderName', riderName);
      updateJSON(inoutMap, 'summaryTableData', summaryTableData);
      updateJSON(inoutMap, 'col1', col1);
      updateJSON(inoutMap, 'col2', col2);
      updateJSON(inoutMap, 'col3', col3);
      updateJSON(inoutMap, 'col4', col4);
      updateJSON(inoutMap, 'col5', col5);
      successCB(inoutMap);
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function Whole_life_905_Premium_02(inoutMap, successCB) {
  var annPremium, monthPremium, quarPremium, semAnnPremium, annualisedPremium, ADBPremium, ADBRCCPremium, ADDPremium, ADDRCCPremium, AIPremium, AIRCCPremium, PBPremium, PBSumAssured, DDPremium, HSPremium, OPDPremium, HBPremium, DDPaymentPeriod, DDPremRate, HBPremRate, HSPremRate, OPDPremRate, totalRiderPremium, totalPolicyPremium, modalPremium, WPRiderSumAssured, PremiumWP, quarVar, discountedPremium, HSOccCodeTableFetch, HSDetails, i, inoutMap, err, PBPremRate;
  HSDetails = [];
  ADBPremium = Number(0);
  ADBRCCPremium = Number(0);
  ADDPremium = Number(0);
  ADDRCCPremium = Number(0);
  AIPremium = Number(0);
  AIRCCPremium = Number(0);
  PBPremium = Number(0);
  PBSumAssured = Number(0);
  DDPremium = Number(0);
  HSPremium = Number(0);
  OPDPremium = Number(0);
  HBPremium = Number(0);
  totalRiderPremium = Number(0);
  totalPolicyPremium = Number(0);
  discountRate = Number(0);
  discountedPremium = Number(0);
  if (inoutMap.PolicyDetails.isSumAssured == 'Yes' && inoutMap.PolicyDetails.sumAssured < Number(2000000)) {
    discountRate = Number(0);
  } else {
    if (inoutMap.PolicyDetails.isSumAssured == 'Yes' && inoutMap.PolicyDetails.sumAssured >= Number(2000000) && inoutMap.PolicyDetails.sumAssured <= Number(4999999)) {
      discountRate = Number(2);
    } else {
      if (inoutMap.PolicyDetails.isSumAssured == 'Yes' && inoutMap.PolicyDetails.sumAssured >= Number(5000000) && inoutMap.PolicyDetails.sumAssured <= Number(9999999)) {
        discountRate = Number(3);
      } else {
        if (inoutMap.PolicyDetails.isSumAssured == 'Yes' && inoutMap.PolicyDetails.sumAssured >= Number(10000000)) {
          discountRate = Number(4);
        }
      }
    }
  }
  WholeLife905OccupationaLRuleUpdated1ProLifejuly07(inoutMap, function (arguments, _$param4, _$param5) {
    inoutMap = _$param4;
    err = _$param5;
    Wholelife905_Lookup_01(inoutMap, function (arguments, _$param6, _$param7) {
      inoutMap = _$param6;
      err = _$param7;
      monthVar = formatDecimal(mul(inoutMap.premRate, inoutMap.monthRate), 2);
      quarVar = formatDecimal(mul(inoutMap.premRate, inoutMap.quarRate), 2);
      semAnnVar = formatDecimal(mul(inoutMap.premRate, inoutMap.semiAnnRate), 2);
      if (inoutMap.PolicyDetails.isSumAssured == 'Yes') {
        annPremium = formatDecimal(mul(inoutMap.premRate, inoutMap.PolicyDetails.sumAssured), 2);
        annPremium = formatDecimal(div(annPremium, 1000), 0);
        monthPremium = formatDecimal(mul(monthVar, inoutMap.PolicyDetails.sumAssured), 2);
        monthPremium = formatDecimal(div(monthPremium, 1000), 0);
        quarPremium = formatDecimal(mul(quarVar, inoutMap.PolicyDetails.sumAssured), 2);
        quarPremium = formatDecimal(div(quarPremium, 1000), 0);
        semAnnPremium = formatDecimal(mul(semAnnVar, inoutMap.PolicyDetails.sumAssured), 2);
        semAnnPremium = formatDecimal(div(semAnnPremium, 1000), 0);
        discountedPremium = formatDecimal(sub(inoutMap.premRate, discountRate), 2);
        discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.PolicyDetails.sumAssured), 2);
        discountedPremium = formatDecimal(div(discountedPremium, 1000), 2);
        annualisedPremium = annPremium;
        modalPremium = annPremium;
        annualisedPremium = discountedPremium;
        if (inoutMap.PolicyDetails.premiumMode == 6002) {
          annualisedPremium = mul(semAnnPremium, 2);
          modalPremium = semAnnPremium;
          discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.semiAnnRate), 0);
          annualisedPremium = formatDecimal(mul(discountedPremium, 2), 0);
        }
        if (inoutMap.PolicyDetails.premiumMode == 6003) {
          annualisedPremium = mul(quarPremium, 4);
          modalPremium = quarPremium;
          discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.quarRate), 0);
          annualisedPremium = formatDecimal(mul(discountedPremium, 4), 0);
        }
        if (inoutMap.PolicyDetails.premiumMode == 6004) {
          annualisedPremium = mul(monthPremium, 12);
          modalPremium = monthPremium;
          discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.monthRate), 0);
          annualisedPremium = formatDecimal(mul(discountedPremium, 12), 0);
        }
        inoutMap.PolicyDetails.premium = discountedPremium;
      } else {
        if (inoutMap.PolicyDetails.isPremium == 'Yes') {
          inoutMap.PolicyDetails.sumAssured = formatDecimal(mul(Number(1000), inoutMap.PolicyDetails.premium), 2);
          inoutMap.PolicyDetails.sumAssured = formatDecimal(div(inoutMap.PolicyDetails.sumAssured, inoutMap.premRate), 2);
          annualisedPremium = inoutMap.PolicyDetails.premium;
          modalPremium = inoutMap.PolicyDetails.premium;
          discountRate = Number(0);
          if (inoutMap.PolicyDetails.premiumMode == 6002) {
            inoutMap.PolicyDetails.sumAssured = formatDecimal(number, index)(div(mul(Number(1000), inoutMap.PolicyDetails.premium), semAnnVar), 0);
            annualisedPremium = formatDecimal(mul(modalPremium, 2), 0);
          }
          if (inoutMap.PolicyDetails.premiumMode == 6003) {
            inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), quarVar), 0);
            annualisedPremium = formatDecimal(mul(modalPremium, 4), 0);
          }
          if (inoutMap.PolicyDetails.premiumMode == 6004) {
            inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), monthVar), 0);
            annualisedPremium = formatDecimal(mul(modalPremium, 12), 0);
          }
        }
      }
      if (inoutMap.PolicyDetails.isPremium == 'Yes' && inoutMap.PolicyDetails.sumAssured < Number(2000000)) {
        discountRate = Number(0);
      } else {
        if (inoutMap.PolicyDetails.isPremium == 'Yes') {
          if (inoutMap.PolicyDetails.sumAssured >= Number(2000000) && inoutMap.PolicyDetails.sumAssured <= Number(4999999)) {
            discountRate = Number(2);
          }
        } else {
          if (inoutMap.PolicyDetails.isPremium == 'Yes' && inoutMap.PolicyDetails.sumAssured >= Number(5000000) && inoutMap.PolicyDetails.sumAssured <= Number(9999999)) {
            discountRate = Number(3);
          } else {
            if (inoutMap.PolicyDetails.isPremium == 'Yes' && inoutMap.PolicyDetails.sumAssured >= Number(10000000)) {
              discountRate = Number(4);
            }
          }
        }
      }
      if (inoutMap.PolicyDetails.isPremium == 'Yes') {
        discountedPremium = formatDecimal(sub(inoutMap.premRate, discountRate), 2);
        discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.PolicyDetails.sumAssured), 2);
        discountedPremium = formatDecimal(div(discountedPremium, 1000), 2);
        annualisedPremium = discountedPremium;
      }
      if (inoutMap.PolicyDetails.premiumMode == 6002 && inoutMap.PolicyDetails.isPremium == 'Yes') {
        discountedPremium = formatDecimal(number, index)(mul(discountedPremium, inoutMap.semiAnnRate), 2);
        annualisedPremium = formatDecimal(mul(discountedPremium, 2), 2);
      } else {
        if (inoutMap.PolicyDetails.isPremium == 'Yes' && inoutMap.PolicyDetails.premiumMode == 6003) {
          discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.quarRate), 2);
          annualisedPremium = formatDecimal(mul(discountedPremium, 4), 2);
        } else {
          if (inoutMap.PolicyDetails.isPremium == 'Yes' && inoutMap.PolicyDetails.premiumMode == 6004) {
            discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.monthRate), 2);
            annualisedPremium = formatDecimal(mul(discountedPremium, 12), 2);
          }
        }
      }
      (function (_$cont) {
        if (inoutMap.WPRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age <= Number(55) && inoutMap.InsuredDetails.age >= Number(16)) {
          WholeLife905_WPPremium_05(inoutMap, function (arguments, _$param8, _$param9) {
            inoutMap = _$param8;
            err = _$param9;
            PremiumWP = inoutMap.PremiumWP1;
            WPRiderSumAssured = inoutMap.WPRiderSumAssured1;
            _$cont();
          }.bind(this, arguments));
        } else {
          PremiumWP = Number(0);
          WPRiderSumAssured = Number(0);
          _$cont();
        }
      }.bind(this)(function (_$err) {
        if (_$err !== undefined)
          return _$cont(_$err);
        if (inoutMap.ADBRider.isRequired == 'Yes') {
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(5)) {
            ADBPremium = formatDecimal(mul(inoutMap.ADBRider.sumAssured, inoutMap.ADBPremRate), 2);
            ADBPremium = formatDecimal(div(ADBPremium, 1000), 0);
          }
        } else {
          ADBPremium = Number(0);
        }
        if (inoutMap.ADBRider.isRequired == 'Yes') {
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(5)) {
            ADBRCCPremium = formatDecimal(mul(inoutMap.ADBRider.sumAssured, inoutMap.ADBRCCPremRate), 2);
            ADBRCCPremium = formatDecimal(div(ADBRCCPremium, 1000), 0);
          }
        } else {
          ADBRCCPremium = Number(0);
        }
        if (inoutMap.ADDRiders.isRequired == 'Yes') {
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(5)) {
            ADDPremium = formatDecimal(mul(inoutMap.ADDRiders.sumAssured, inoutMap.ADDPremRate), 2);
            ADDPremium = formatDecimal(div(ADDPremium, 1000), 0);
          }
        } else {
          ADDPremium = Number(0);
        }
        if (inoutMap.ADDRiders.isRequired == 'Yes') {
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(5)) {
            ADDRCCPremium = formatDecimal(mul(inoutMap.ADDRiders.sumAssured, inoutMap.ADDRCCPremRate), 2);
            ADDRCCPremium = formatDecimal(div(ADDRCCPremium, 1000), 0);
          }
        } else {
          ADDRCCPremium = Number(0);
        }
        if (inoutMap.AIRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age >= Number(16)) {
          AIPremium = formatDecimal(mul(inoutMap.AIRider.sumAssured, inoutMap.AIPremRate), 2);
          AIPremium = formatDecimal(div(AIPremium, 1000), 0);
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(5)) {
            AIPremium = formatDecimal(mul(inoutMap.AIRider.sumAssured, inoutMap.AIPremRate), 2);
            AIPremium = formatDecimal(div(AIPremium, 1000), 0);
          }
        } else {
          AIPremium = Number(0);
        }
        if (inoutMap.AIRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age >= Number(16)) {
          if (inoutMap.InsuredDetails.age <= Number(60) && inoutMap.PolicyDetails.premiumPayingTerm == Number(5)) {
            AIRCCPremium = formatDecimal(mul(inoutMap.AIRider.sumAssured, inoutMap.AIRCCPremRate), 2);
            AIRCCPremium = formatDecimal(div(AIRCCPremium, 1000), 0);
          }
        } else {
          AIRCCPremium = Number(0);
        }
        (function (_$cont) {
          if (inoutMap.DDRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age <= Number(55) && inoutMap.InsuredDetails.age >= Number(17)) {
            DDPaymentPeriod = minOfNValues(inoutMap.PolicyDetails.premiumPayingTerm, sub(Number(60), inoutMap.InsuredDetails.age), Number(19));
            DDPremRate = 0;
            lookupAsJson('RATE_FACTOR', 'EXTN_RIDER_DD_PREM_TBL', 'PROD_ID=1220', 'INSURED_AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'INSURED_SEX=\'' + inoutMap.InsuredDetails.sex + '\'', 'PAYMENT_PERIOD=\'' + DDPaymentPeriod + '\'', function (arguments, _$param10, _$param11) {
              DDPremRate = _$param10;
              err = _$param11;
              DDPremium = formatDecimal(mul(inoutMap.DDRider.sumAssured, DDPremRate), 2);
              DDPremium = formatDecimal(div(DDPremium, 1000), 0);
              if (inoutMap.PolicyDetails.premiumMode == 6002) {
                DDRate = formatDecimal(mul(DDPremRate, inoutMap.semiAnnRate), 2);
                DDPremium = formatDecimal(mul(inoutMap.DDRider.sumAssured, DDRate), 2);
                DDPremium = formatDecimal(div(DDPremium, 1000), 0);
              }
              if (inoutMap.PolicyDetails.premiumMode == 6003) {
                DDRate = formatDecimal(mul(DDPremRate, inoutMap.quarRate), 2);
                DDPremium = formatDecimal(mul(inoutMap.DDRider.sumAssured, DDRate), 2);
                DDPremium = formatDecimal(div(DDPremium, 1000), 0);
              }
              if (inoutMap.PolicyDetails.premiumMode == 6004) {
                DDRate = formatDecimal(mul(DDPremRate, inoutMap.monthRate), 2);
                DDPremium = formatDecimal(mul(inoutMap.DDRider.sumAssured, DDRate), 2);
                DDPremium = formatDecimal(div(DDPremium, 1000), 0);
              }
              _$cont();
            }.bind(this, arguments));
          } else {
            DDPremium = Number(0);
            _$cont();
          }
        }.bind(this)(function (_$err) {
          if (_$err !== undefined)
            return _$cont(_$err);
          if (inoutMap.HBOccCode == 'IC') {
            HBOccCodeTableFetch = Number(4);
          } else {
            HBOccCodeTableFetch = inoutMap.HBOccCode;
          }
          if (inoutMap.HSOccCode == 'IC') {
            HSOccCodeTableFetch = Number(4);
          } else {
            HSOccCodeTableFetch = inoutMap.HSOccCode;
          }
          (function (_$cont) {
            if (inoutMap.HBRiders.isRequired == 'Yes' && inoutMap.InsuredDetails.age <= Number(59) && inoutMap.InsuredDetails.age >= Number(6)) {
              HBPremRate = 0;
              lookupAsJson('PREMIUM_RATE', 'EXTN_RIDER_HB_PREM_TBL', 'PROD_ID=1220', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + HBOccCodeTableFetch + '\'', 'START_AGE<=\'' + inoutMap.InsuredDetails.age + '\'', 'END_AGE>=\'' + inoutMap.InsuredDetails.age + '\'', 'PLAN_NUM=\'' + inoutMap.HBRiders.sumAssured + '\'', function (arguments, _$param12, _$param13) {
                HBPremRate = _$param12;
                err = _$param13;
                HBPremium = formatDecimal(Number(HBPremRate), 0);
                _$cont();
              }.bind(this, arguments));
            } else {
              HBPremium = Number(0);
              _$cont();
            }
          }.bind(this)(function (_$err) {
            if (_$err !== undefined)
              return _$cont(_$err);
            (function (_$cont) {
              if (inoutMap.HSRiders.isRequired == 'Yes') {
                HSPremRate = 0;
                lookupAsJson('PREMIUM_RATE', 'EXTN_RIDER_HS_PREM_TBL', 'PROD_ID=1220', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + HSOccCodeTableFetch + '\'', 'START_AGE<=\'' + inoutMap.InsuredDetails.age + '\'', 'END_AGE>=\'' + inoutMap.InsuredDetails.age + '\'', 'PLAN_NUM=\'' + inoutMap.HSRiders.sumAssured + '\'', 'SEX=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param14, _$param15) {
                  HSPremRate = _$param14;
                  err = _$param15;
                  HSPremium = formatDecimal(Number(HSPremRate), 0);
                  _$cont();
                }.bind(this, arguments));
              } else {
                HSPremium = Number(0);
                _$cont();
              }
            }.bind(this)(function (_$err) {
              if (_$err !== undefined)
                return _$cont(_$err);
              (function (_$cont) {
                if (inoutMap.OPDRiders.isRequired == 'Yes') {
                  OPDPremRate = 0;
                  lookupAsJson('PREMIUM_RATE', 'EXTN_RIDER_OPD_PREM_TBL', 'PROD_ID=1220', 'SEX=\'' + inoutMap.InsuredDetails.sex + '\'', 'PLAN_NUM=\'' + inoutMap.OPDRiders.sumAssured + '\'', 'END_AGE>=\'' + inoutMap.InsuredDetails.age + '\'', 'START_AGE<=\'' + inoutMap.InsuredDetails.age + '\'', 'OCC_CLASS=\'' + HSOccCodeTableFetch + '\'', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', function (arguments, _$param16, _$param17) {
                    OPDPremRate = _$param16;
                    err = _$param17;
                    OPDPremium = formatDecimal(Number(OPDPremRate), 0);
                    _$cont();
                  }.bind(this, arguments));
                } else {
                  OPDPremium = Number(0);
                  _$cont();
                }
              }.bind(this)(function (_$err) {
                if (_$err !== undefined)
                  return _$cont(_$err);
                (function (_$cont) {
                  if (inoutMap.PBRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age <= Number(15) && inoutMap.PayorDetails.age >= Number(20) && inoutMap.PayorDetails.age <= Number(55)) {
                    PBSumAssured = sum(Number(modalPremium), Number(PremiumWP), Number(ADBPremium), Number(ADDPremium), Number(AIPremium), Number(ADBRCCPremium), Number(ADDRCCPremium), Number(AIRCCPremium), Number(DDPremium), Number(HBPremium), Number(HSPremium), Number(OPDPremium));
                    inoutMap.PBRider.sumAssured = PBSumAssured;
                    PBPaymentPeriod = minOfNValues(inoutMap.PolicyDetails.premiumPayingTerm, sub(Number(25), inoutMap.InsuredDetails.age), sub(Number(60), inoutMap.PayorDetails.age));
                    PBPremRate = 0;
                    lookupAsJson('RATE_FACTOR', 'EXTN_RIDER_PB_PREM_TBL', 'PROD_ID=1220', 'INSURED_AGE=\'' + inoutMap.PayorDetails.age + '\'', 'INSURED_SEX=\'' + inoutMap.PayorDetails.sex + '\'', 'PAYMENT_PERIOD=\'' + PBPaymentPeriod + '\'', function (arguments, _$param18, _$param19) {
                      PBPremRate = _$param18;
                      err = _$param19;
                      PBPremium = formatDecimal(mul(PBSumAssured, PBPremRate), 2);
                      PBPremium = formatDecimal(div(PBPremium, 100), 0);
                      _$cont();
                    }.bind(this, arguments));
                  } else {
                    PBPremium = Number(0);
                    _$cont();
                  }
                }.bind(this)(function (_$err) {
                  if (_$err !== undefined)
                    return _$cont(_$err);
                  totalRiderPremium = sum(Number(ADBPremium), Number(ADDPremium), Number(AIPremium), Number(ADBRCCPremium), Number(ADDRCCPremium), Number(AIRCCPremium), Number(DDPremium), Number(HBPremium), Number(HSPremium), Number(OPDPremium), Number(PremiumWP), PBPremium);
                  totalPolicyPremium = formatDecimal(sum(totalRiderPremium, Number(discountedPremium)), 0);
                  (function (_$cont) {
                    if (inoutMap.HSRiders.isRequired == 'Yes') {
                      if (inoutMap.HSRiders.sumAssured < Number(1000)) {
                        inoutMap.HSRiders.sumAssured = Number(1000);
                      }
                      HSDetails = 0;
                      lookupAsJson('HS_DETAILS', 'EXTN_RIDER_HS_PDF_TBL', 'PROD_ID=1220', 'PLAN_NUM=\'' + inoutMap.HSRiders.sumAssured + '\'', function (arguments, _$param20, _$param21) {
                        HSDetails = _$param20;
                        err = _$param21;
                        _$cont();
                      }.bind(this, arguments));
                    } else {
                      if (inoutMap.HSRiders.isRequired == 'No') {
                        i = 0;
                        while (i <= 8) {
                          HSDetails[i] = Number(0);
                          i = i + 1;
                        }
                      }
                      _$cont();
                    }
                  }.bind(this)(function (_$err) {
                    if (_$err !== undefined)
                      return _$cont(_$err);
                    updateJSON(inoutMap, 'annPremium', annPremium);
                    updateJSON(inoutMap, 'monthPremium', monthPremium);
                    updateJSON(inoutMap, 'quarPremium', quarPremium);
                    updateJSON(inoutMap, 'semAnnPremium', semAnnPremium);
                    updateJSON(inoutMap, 'annualisedPremium', annualisedPremium);
                    updateJSON(inoutMap, 'ADBPremium', ADBPremium);
                    updateJSON(inoutMap, 'ADBRCCPremium', ADBRCCPremium);
                    updateJSON(inoutMap, 'ADDPremium', ADDPremium);
                    updateJSON(inoutMap, 'ADDRCCPremium', ADDRCCPremium);
                    updateJSON(inoutMap, 'AIPremium', AIPremium);
                    updateJSON(inoutMap, 'AIRCCPremium', AIRCCPremium);
                    updateJSON(inoutMap, 'PBPremium', PBPremium);
                    updateJSON(inoutMap, 'PBSumAssured', PBSumAssured);
                    updateJSON(inoutMap, 'DDPremium', DDPremium);
                    updateJSON(inoutMap, 'HSPremium', HSPremium);
                    updateJSON(inoutMap, 'OPDPremium', OPDPremium);
                    updateJSON(inoutMap, 'HBPremium', HBPremium);
                    updateJSON(inoutMap, 'DDPaymentPeriod', DDPaymentPeriod);
                    updateJSON(inoutMap, 'DDPremRate', DDPremRate);
                    updateJSON(inoutMap, 'HBPremRate', HBPremRate);
                    updateJSON(inoutMap, 'HSPremRate', HSPremRate);
                    updateJSON(inoutMap, 'OPDPremRate', OPDPremRate);
                    updateJSON(inoutMap, 'totalRiderPremium', totalRiderPremium);
                    updateJSON(inoutMap, 'totalPolicyPremium', totalPolicyPremium);
                    updateJSON(inoutMap, 'modalPremium', modalPremium);
                    updateJSON(inoutMap, 'WPRiderSumAssured', WPRiderSumAssured);
                    updateJSON(inoutMap, 'PremiumWP', PremiumWP);
                    updateJSON(inoutMap, 'quarVar', quarVar);
                    updateJSON(inoutMap, 'discountedPremium', discountedPremium);
                    updateJSON(inoutMap, 'HSOccCodeTableFetch', HSOccCodeTableFetch);
                    updateJSON(inoutMap, 'HSDetails', HSDetails);
                    successCB(inoutMap);
                  }.bind(this)));
                }.bind(this)));
              }.bind(this)));
            }.bind(this)));
          }.bind(this)));
        }.bind(this)));
      }.bind(this)));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function Wholelife905_BFTable_CVTable_03(inoutMap, successCB) {
  var sumOfLivingBenefit, sumOfTaxBenefit, sumOfPremium, sumOfGrandBenefit, sumOfRatePercent, totalBenefit, benefitYear, benefitAge, benefitPremium, benefitTaxAmount, benefitTaxDeduct, benefitCashRate, benefitCashAmount, benefitLifeRate, benefitLifeAmount, cashValue, rpuCash, rpuSA, extPeriodYear, extPeriodDay, etiCash, etiSA;
  benefitYear = [];
  benefitAge = [];
  benefitPremium = [];
  benefitTaxAmount = [];
  benefitTaxDeduct = [];
  benefitCashRate = [];
  benefitCashAmount = [];
  benefitLifeRate = [];
  benefitLifeAmount = [];
  cashValue = [];
  rpuCash = [];
  rpuSA = [];
  extPeriodYear = [];
  extPeriodDay = [];
  etiCash = [];
  etiSA = [];
  varMultiple = Number(1.1);
  sumOfLivingBenefit = Number(0);
  sumOfTaxBenefit = Number(0);
  sumOfPremium = Number(0);
  sumOfGrandBenefit = Number(0);
  sumOfRatePercent = Number(0);
  startDuration = Number(90);
  i = 0;
  while (i < sub(startDuration, inoutMap.InsuredDetails.age)) {
    benefitYear[i] = sum(i, 1);
    benefitAge[i] = sum(inoutMap.InsuredDetails.age, i, 1);
    if (benefitAge[i] < sum(inoutMap.InsuredDetails.age, inoutMap.PolicyDetails.premiumPayingTerm, 1)) {
      benefitPremium[i] = formatDecimal(Number(inoutMap.annualisedPremium), 0);
    } else {
      benefitPremium[i] = Number(0);
    }
    benefitCashRate[i] = formatDecimal(Number(inoutMap.PERate[i + 1]), 2);
    benefitCashRate[i] = formatDecimal(mul(benefitCashRate[i], Number(100)), 2) + '%';
    benefitCashAmount[i] = formatDecimal(div(inoutMap.PERate[i + 1], 100), 4);
    benefitCashAmount[i] = mul(benefitCashAmount[i], inoutMap.PolicyDetails.sumAssured);
    benefitCashAmount[i] = formatDecimal(mul(benefitCashAmount[i], Number(100)), 0);
    benefitLifeRate[i] = formatDecimal(Number(inoutMap.DBRate[i]), 2);
    benefitLifeRate[i] = formatDecimal(mul(benefitLifeRate[i], Number(100)), 0) + '%';
    benefitLifeAmount[i] = mul(div(inoutMap.DBRate[i], 100), inoutMap.PolicyDetails.sumAssured);
    benefitLifeAmount[i] = mul(benefitLifeAmount[i], Number(100));
    cashValue[i] = formatDecimal(div(mul(inoutMap.TCVXRate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    rpuCash[i] = formatDecimal(div(mul(inoutMap.RPUCashRate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    rpuSA[i] = formatDecimal(div(mul(inoutMap.RPURate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    extPeriodYear[i] = inoutMap.ETIYR[i + 1];
    extPeriodDay[i] = inoutMap.ETIDAY[i + 1];
    etiCash[i] = formatDecimal(div(mul(inoutMap.ETICashRate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    etiSA[i] = formatDecimal(div(mul(inoutMap.ETISARate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    if (benefitAge[i] < sum(inoutMap.InsuredDetails.age, inoutMap.PolicyDetails.premiumPayingTerm, 1)) {
      if (inoutMap.annualisedPremium >= Number(100000)) {
        benefitTaxAmount[i] = formatDecimal(mul(div(inoutMap.PolicyDetails.taxRate, 100), Number(100000)), 0);
      } else {
        benefitTaxAmount[i] = formatDecimal(mul(div(inoutMap.PolicyDetails.taxRate, 100), benefitPremium[i]), 0);
      }
    } else {
      benefitTaxAmount[i] = Number(0);
    }
    sumOfLivingBenefit = formatDecimal(sum(sumOfLivingBenefit, benefitCashAmount[i]), 2);
    sumOfTaxBenefit = formatDecimal(sum(sumOfTaxBenefit, benefitTaxAmount[i]), 2);
    sumOfPremium = formatDecimal(sum(sumOfPremium, benefitPremium[i]), 2);
    sumOfRatePercent = formatDecimal(sum(sumOfRatePercent, benefitCashRate[i]), 2) + '%';
    i++;
  }
  totalBenefit = formatDecimal(sub(sum(sumOfLivingBenefit, sumOfTaxBenefit), sumOfPremium), 2);
  updateJSON(inoutMap, 'sumOfLivingBenefit', sumOfLivingBenefit);
  updateJSON(inoutMap, 'sumOfTaxBenefit', sumOfTaxBenefit);
  updateJSON(inoutMap, 'sumOfPremium', sumOfPremium);
  updateJSON(inoutMap, 'sumOfGrandBenefit', sumOfGrandBenefit);
  updateJSON(inoutMap, 'sumOfRatePercent', sumOfRatePercent);
  updateJSON(inoutMap, 'totalBenefit', totalBenefit);
  updateJSON(inoutMap, 'benefitYear', benefitYear);
  updateJSON(inoutMap, 'benefitAge', benefitAge);
  updateJSON(inoutMap, 'benefitPremium', benefitPremium);
  updateJSON(inoutMap, 'benefitTaxAmount', benefitTaxAmount);
  updateJSON(inoutMap, 'benefitTaxDeduct', benefitTaxDeduct);
  updateJSON(inoutMap, 'benefitCashRate', benefitCashRate);
  updateJSON(inoutMap, 'benefitCashAmount', benefitCashAmount);
  updateJSON(inoutMap, 'benefitLifeRate', benefitLifeRate);
  updateJSON(inoutMap, 'benefitLifeAmount', benefitLifeAmount);
  updateJSON(inoutMap, 'cashValue', cashValue);
  updateJSON(inoutMap, 'rpuCash', rpuCash);
  updateJSON(inoutMap, 'rpuSA', rpuSA);
  updateJSON(inoutMap, 'extPeriodYear', extPeriodYear);
  updateJSON(inoutMap, 'extPeriodDay', extPeriodDay);
  updateJSON(inoutMap, 'etiCash', etiCash);
  updateJSON(inoutMap, 'etiSA', etiSA);
  successCB(inoutMap);
}
function Wholelife905_Lookup_01(inoutMap, successCB) {
  var premRate, monthRate, quarRate, semiAnnRate, ADBPremRate, ADDPremRate, AIPremRate, ADBRCCPremRate, ADDRCCPremRate, AIRCCPremRate, DBRate, PERate, TCVXRate, RPUCashRate, RPURate, ETIYR, ETIDAY, ETICashRate, ETISARate, err;
  if (inoutMap.ADDOccCode == 'IC') {
    ADDOccCodeTableFetch = Number(4);
  } else {
    ADDOccCodeTableFetch = inoutMap.ADDOccCode;
  }
  if (inoutMap.ADBOccCode == 'IC') {
    ADBOccCodeTableFetch = Number(4);
  } else {
    ADBOccCodeTableFetch = inoutMap.ADBOccCode;
  }
  if (inoutMap.AIOccCode == 'IC') {
    AIOccCodeTableFetch = Number(4);
  } else {
    AIOccCodeTableFetch = inoutMap.AIOccCode;
  }
  monthRate = 0;
  lookupAsJson('PREMIUM_FREQ', 'EXTN_MODAL_PREM_TBL', 'PREMIUM_MODE=6004', 'PROD_ID=1220', function (arguments, _$param22, _$param23) {
    monthRate = _$param22;
    err = _$param23;
    quarRate = 0;
    lookupAsJson('PREMIUM_FREQ', 'EXTN_MODAL_PREM_TBL', 'PREMIUM_MODE=6003', 'PROD_ID=1220', function (arguments, _$param24, _$param25) {
      quarRate = _$param24;
      err = _$param25;
      semiAnnRate = 0;
      lookupAsJson('PREMIUM_FREQ', 'EXTN_MODAL_PREM_TBL', 'PREMIUM_MODE=6002', 'PROD_ID=1220', function (arguments, _$param26, _$param27) {
        semiAnnRate = _$param26;
        err = _$param27;
        ADBPremRate = 0;
        lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_ADB_PREM_TBL', 'PROD_ID=1220', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + ADBOccCodeTableFetch + '\'', function (arguments, _$param28, _$param29) {
          ADBPremRate = _$param28;
          err = _$param29;
          ADDPremRate = 0;
          lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_AI_PREM_TBL', 'PROD_ID=1220', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + ADDOccCodeTableFetch + '\'', function (arguments, _$param30, _$param31) {
            ADDPremRate = _$param30;
            err = _$param31;
            AIPremRate = 0;
            lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_ADD_PREM_TBL', 'PROD_ID=1220', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + AIOccCodeTableFetch + '\'', function (arguments, _$param32, _$param33) {
              AIPremRate = _$param32;
              err = _$param33;
              ADBRCCPremRate = 0;
              lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_RCC_ADB_PREM_TBL', 'PROD_ID=1220', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', function (arguments, _$param34, _$param35) {
                ADBRCCPremRate = _$param34;
                err = _$param35;
                ADDRCCPremRate = 0;
                lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_RCC_ADD_PREM_TBL', 'PROD_ID=1220', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', function (arguments, _$param36, _$param37) {
                  ADDRCCPremRate = _$param36;
                  err = _$param37;
                  AIRCCPremRate = 0;
                  lookupAsJson('PREMIUM_FREQ', 'EXTN_RIDER_RCC_AI_PREM_TBL', 'PROD_ID=1220', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', function (arguments, _$param38, _$param39) {
                    AIRCCPremRate = _$param38;
                    err = _$param39;
                    premRate = 0;
                    lookupAsJson('GPX', 'EXTN_TVVALUE_WHOLELIFE_TBL', 'PROD_ID=1220', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', 'TERM= 0', function (arguments, _$param40, _$param41) {
                      premRate = _$param40;
                      err = _$param41;
                      PERate = 0;
                      lookupAsJson('PE', 'EXTN_TVVALUE_WHOLELIFE_TBL', 'PROD_ID=1220', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param42, _$param43) {
                        PERate = _$param42;
                        err = _$param43;
                        DBRate = 0;
                        lookupAsJson('DB', 'EXTN_TVVALUE_WHOLELIFE_TBL', 'PROD_ID=1220', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param44, _$param45) {
                          DBRate = _$param44;
                          err = _$param45;
                          TCVXRate = 0;
                          lookupAsJson('TCVX', 'EXTN_TVVALUE_WHOLELIFE_TBL', 'PROD_ID=1220', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param46, _$param47) {
                            TCVXRate = _$param46;
                            err = _$param47;
                            RPUCashRate = 0;
                            lookupAsJson('RPU_CASH', 'EXTN_TVVALUE_WHOLELIFE_TBL', 'PROD_ID=1220', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param48, _$param49) {
                              RPUCashRate = _$param48;
                              err = _$param49;
                              RPURate = 0;
                              lookupAsJson('RPU', 'EXTN_TVVALUE_WHOLELIFE_TBL', 'PROD_ID=1220', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\' ORDER BY TERM', function (arguments, _$param50, _$param51) {
                                RPURate = _$param50;
                                err = _$param51;
                                ETIYR = 0;
                                lookupAsJson('ETI_YR', 'EXTN_TVVALUE_WHOLELIFE_TBL', 'PROD_ID=1220', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\' ORDER BY TERM', function (arguments, _$param52, _$param53) {
                                  ETIYR = _$param52;
                                  err = _$param53;
                                  ETIDAY = 0;
                                  lookupAsJson('ETI_DAY', 'EXTN_TVVALUE_WHOLELIFE_TBL', 'PROD_ID=1220', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param54, _$param55) {
                                    ETIDAY = _$param54;
                                    err = _$param55;
                                    ETICashRate = 0;
                                    lookupAsJson('ETI_CASH', 'EXTN_TVVALUE_WHOLELIFE_TBL', 'PROD_ID=1220', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param56, _$param57) {
                                      ETICashRate = _$param56;
                                      err = _$param57;
                                      ETISARate = 0;
                                      lookupAsJson('ETI_SA', 'EXTN_TVVALUE_WHOLELIFE_TBL', 'PROD_ID=1220', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY TERM', function (arguments, _$param58, _$param59) {
                                        ETISARate = _$param58;
                                        err = _$param59;
                                        updateJSON(inoutMap, 'premRate', premRate);
                                        updateJSON(inoutMap, 'monthRate', monthRate);
                                        updateJSON(inoutMap, 'quarRate', quarRate);
                                        updateJSON(inoutMap, 'semiAnnRate', semiAnnRate);
                                        updateJSON(inoutMap, 'ADBPremRate', ADBPremRate);
                                        updateJSON(inoutMap, 'ADDPremRate', ADDPremRate);
                                        updateJSON(inoutMap, 'AIPremRate', AIPremRate);
                                        updateJSON(inoutMap, 'ADBRCCPremRate', ADBRCCPremRate);
                                        updateJSON(inoutMap, 'ADDRCCPremRate', ADDRCCPremRate);
                                        updateJSON(inoutMap, 'AIRCCPremRate', AIRCCPremRate);
                                        updateJSON(inoutMap, 'DBRate', DBRate);
                                        updateJSON(inoutMap, 'PERate', PERate);
                                        updateJSON(inoutMap, 'TCVXRate', TCVXRate);
                                        updateJSON(inoutMap, 'RPUCashRate', RPUCashRate);
                                        updateJSON(inoutMap, 'RPURate', RPURate);
                                        updateJSON(inoutMap, 'ETIYR', ETIYR);
                                        updateJSON(inoutMap, 'ETIDAY', ETIDAY);
                                        updateJSON(inoutMap, 'ETICashRate', ETICashRate);
                                        updateJSON(inoutMap, 'ETISARate', ETISARate);
                                        successCB(inoutMap);
                                      }.bind(this, arguments));
                                    }.bind(this, arguments));
                                  }.bind(this, arguments));
                                }.bind(this, arguments));
                              }.bind(this, arguments));
                            }.bind(this, arguments));
                          }.bind(this, arguments));
                        }.bind(this, arguments));
                      }.bind(this, arguments));
                    }.bind(this, arguments));
                  }.bind(this, arguments));
                }.bind(this, arguments));
              }.bind(this, arguments));
            }.bind(this, arguments));
          }.bind(this, arguments));
        }.bind(this, arguments));
      }.bind(this, arguments));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function WholeLife905OccupationaLRuleUpdated1ProLifejuly07(inoutMap, successCB) {
  var occupationalCode, BaseOccCode, CIOccCode, DDOccCode, WPOccCode, PBOccCode, AIOccCode, HBOccCode, HSOccCode, ADDOccCode, ADBOccCode, OccCode1D, OccCode2D, OccCode1IC, OccCode2IC, SumOccCode1, SumOccCode2, Option, OccCode1, OccCode2, inoutMap, err;
  OccCode1 = [];
  OccCode2 = [];
  SumOccCode1 = Number(0);
  SumOccCode2 = Number(0);
  OccCode1D = Number(0);
  OccCode2D = Number(0);
  OccCode1IC = Number(0);
  OccCode2IC = Number(0);
  Option = '';
  inoutMap.ADBOccCode1A = Number(0);
  inoutMap.ADBOccCode2A = Number(0);
  inoutMap.AIOccCode1A = Number(0);
  inoutMap.AIOccCode2A = Number(0);
  inoutMap.PBOccCode1A = Number(0);
  inoutMap.PBOccCode2A = Number(0);
  inoutMap.WPOccCode1A = Number(0);
  inoutMap.WPOccCode2A = Number(0);
  inoutMap.DDOccCode1A = Number(0);
  inoutMap.DDOccCode2A = Number(0);
  inoutMap.ADDOccCode1A = Number(0);
  inoutMap.ADDOccCode2A = Number(0);
  inoutMap.HBOccCode1A = Number(0);
  inoutMap.HBOccCode2A = Number(0);
  inoutMap.HSOccCode1A = Number(0);
  inoutMap.HSOccCode2A = Number(0);
  inoutMap.CIOccCode1A = Number(0);
  inoutMap.CIOccCode2A = Number(0);
  OccupationaLRuleBaseCodeWholeLife(inoutMap, function (arguments, _$param60, _$param61) {
    inoutMap = _$param60;
    err = _$param61;
    OccupationaLRuleHBOccCodeWholeLife(inoutMap, function (arguments, _$param62, _$param63) {
      inoutMap = _$param62;
      err = _$param63;
      OccupationaLRuleHSOccCodeWholeLife(inoutMap, function (arguments, _$param64, _$param65) {
        inoutMap = _$param64;
        err = _$param65;
        OccupationaLRuleADDCodeWholeLife(inoutMap, function (arguments, _$param66, _$param67) {
          inoutMap = _$param66;
          err = _$param67;
          OccupationaLRuleADBCodeWholeLife(inoutMap, function (arguments, _$param68, _$param69) {
            inoutMap = _$param68;
            err = _$param69;
            OccupationaLRuleAIOccCodeWholeLife(inoutMap, function (arguments, _$param70, _$param71) {
              inoutMap = _$param70;
              err = _$param71;
              OccupationaLRulePBOccCodeWholeLife(inoutMap, function (arguments, _$param72, _$param73) {
                inoutMap = _$param72;
                err = _$param73;
                OccupationaLRuleWBOccCodeWholeLife(inoutMap, function (arguments, _$param74, _$param75) {
                  inoutMap = _$param74;
                  err = _$param75;
                  OccupationaLRuleDDCodeWholeLife(inoutMap, function (arguments, _$param76, _$param77) {
                    inoutMap = _$param76;
                    err = _$param77;
                    SumOccCode2 = sum(inoutMap.BaseOccCode2A, inoutMap.CIOccCode2A, inoutMap.HBOccCode2A, inoutMap.HSOccCode2A, inoutMap.ADDOccCode2A, inoutMap.AIOccCode2A, inoutMap.WPOccCode2A, inoutMap.ADBOccCode2A, inoutMap.PBOccCode2A, inoutMap.DDOccCode2A);
                    SumOccCode1 = sum(inoutMap.BaseOccCode1A, inoutMap.CIOccCode1A, inoutMap.HBOccCode1A, inoutMap.HSOccCode1A, inoutMap.ADDOccCode1A, inoutMap.AIOccCode1A, inoutMap.WPOccCode1A, inoutMap.ADBOccCode1A, inoutMap.PBOccCode1A, inoutMap.DDOccCode1A);
                    OccCode1[1] = inoutMap.CIOccCode1A;
                    OccCode1[2] = inoutMap.DDOccCode1A;
                    OccCode1[3] = inoutMap.WPOccCode1A;
                    OccCode1[4] = inoutMap.PBOccCode1A;
                    OccCode1[5] = inoutMap.AIOccCode1A;
                    OccCode1[6] = inoutMap.HBOccCode1A;
                    OccCode1[7] = inoutMap.HSOccCode1A;
                    OccCode1[8] = inoutMap.ADDOccCode1A;
                    OccCode1[9] = inoutMap.ADBOccCode1A;
                    OccCode2[1] = inoutMap.CIOccCode2A;
                    OccCode2[2] = inoutMap.DDOccCode2A;
                    OccCode2[3] = inoutMap.WPOccCode2A;
                    OccCode2[4] = inoutMap.PBOccCode2A;
                    OccCode2[5] = inoutMap.AIOccCode2A;
                    OccCode2[6] = inoutMap.HBOccCode2A;
                    OccCode2[7] = inoutMap.HSOccCode2A;
                    OccCode2[8] = inoutMap.ADDOccCode2A;
                    OccCode2[9] = inoutMap.ADBOccCode2A;
                    if (inoutMap.BaseOccCode1A >= Number(10)) {
                      Option = 'A';
                    } else {
                      if (inoutMap.BaseOccCode2A >= Number(10)) {
                        Option = 'B';
                      }
                    }
                    i = 1;
                    while (i < 10) {
                      if (OccCode1[i] == Number(10)) {
                        OccCode1D = sum(OccCode1[i], OccCode1D);
                      }
                      if (OccCode2[i] == Number(10)) {
                        OccCode2D = sum(OccCode2[i], OccCode2D);
                      }
                      if (OccCode1[i] == Number(5)) {
                        OccCode1IC = sum(OccCode2[i], OccCode1IC);
                      }
                      if (OccCode2[i] == Number(5)) {
                        OccCode2IC = sum(OccCode2[i], OccCode2IC);
                      }
                      i++;
                    }
                    if (Number(SumOccCode1) == Number(SumOccCode2) && OccCode1D == OccCode2D && OccCode1IC == OccCode2IC && Option >= '') {
                      if (Number(inoutMap.BaseOccCode1A) >= Number(inoutMap.BaseOccCode2A)) {
                        Option = 'A';
                      } else {
                        Option = 'B';
                      }
                    }
                    if (Option >= '') {
                      if (OccCode1D > OccCode2D) {
                        Option = 'A';
                      } else {
                        if (OccCode1D < OccCode2D) {
                          Option = 'B';
                        }
                      }
                    }
                    if (Option >= '') {
                      if (OccCode1IC > OccCode2IC) {
                        Option = 'A';
                      } else {
                        if (OccCode1IC < OccCode2IC) {
                          Option = 'B';
                        }
                      }
                    }
                    if (Option >= '') {
                      if (Number(SumOccCode1) > Number(SumOccCode2)) {
                        Option = 'A';
                      } else {
                        if (Number(SumOccCode1) < Number(SumOccCode2)) {
                          Option = 'B';
                        }
                      }
                    }
                    if (Option == 'A') {
                      BaseOccCode = inoutMap.BaseOccCode1;
                      CIOccCode = inoutMap.CIOccCode1;
                      DDOccCode = inoutMap.DDOccCode1;
                      WPOccCode = inoutMap.WPOccCode1;
                      PBOccCode = inoutMap.PBOccCode1;
                      AIOccCode = inoutMap.AIOccCode1;
                      HBOccCode = inoutMap.HBOccCode1;
                      HSOccCode = inoutMap.HSOccCode1;
                      ADDOccCode = inoutMap.ADDOccCode1;
                      ADBOccCode = inoutMap.ADBOccCode1;
                      occupationalCode = inoutMap.PolicyDetails.occCode1;
                    } else {
                      BaseOccCode = inoutMap.BaseOccCode2;
                      CIOccCode = inoutMap.CIOccCode2;
                      DDOccCode = inoutMap.DDOccCode2;
                      WPOccCode = inoutMap.WPOccCode2;
                      PBOccCode = inoutMap.PBOccCode2;
                      AIOccCode = inoutMap.AIOccCode2;
                      HBOccCode = inoutMap.HBOccCode2;
                      HSOccCode = inoutMap.HSOccCode2;
                      ADDOccCode = inoutMap.ADDOccCode2;
                      ADBOccCode = inoutMap.ADBOccCode2;
                      occupationalCode = inoutMap.PolicyDetails.occCode2;
                    }
                    updateJSON(inoutMap, 'occupationalCode', occupationalCode);
                    updateJSON(inoutMap, 'BaseOccCode', BaseOccCode);
                    updateJSON(inoutMap, 'CIOccCode', CIOccCode);
                    updateJSON(inoutMap, 'DDOccCode', DDOccCode);
                    updateJSON(inoutMap, 'WPOccCode', WPOccCode);
                    updateJSON(inoutMap, 'PBOccCode', PBOccCode);
                    updateJSON(inoutMap, 'AIOccCode', AIOccCode);
                    updateJSON(inoutMap, 'HBOccCode', HBOccCode);
                    updateJSON(inoutMap, 'HSOccCode', HSOccCode);
                    updateJSON(inoutMap, 'ADDOccCode', ADDOccCode);
                    updateJSON(inoutMap, 'ADBOccCode', ADBOccCode);
                    updateJSON(inoutMap, 'OccCode1D', OccCode1D);
                    updateJSON(inoutMap, 'OccCode2D', OccCode2D);
                    updateJSON(inoutMap, 'OccCode1IC', OccCode1IC);
                    updateJSON(inoutMap, 'OccCode2IC', OccCode2IC);
                    updateJSON(inoutMap, 'SumOccCode1', SumOccCode1);
                    updateJSON(inoutMap, 'SumOccCode2', SumOccCode2);
                    updateJSON(inoutMap, 'Option', Option);
                    updateJSON(inoutMap, 'OccCode1', OccCode1);
                    updateJSON(inoutMap, 'OccCode2', OccCode2);
                    successCB(inoutMap);
                  }.bind(this, arguments));
                }.bind(this, arguments));
              }.bind(this, arguments));
            }.bind(this, arguments));
          }.bind(this, arguments));
        }.bind(this, arguments));
      }.bind(this, arguments));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function WholeLife905_WPPremium_05(inoutMap, successCB) {
  var annPremiumWP, monthPremiumWP, quarPremiumWP, semAnnPremiumWP, PremiumWP1, CalculatedWPRiderSumAssured, WPRiderSumAssured1, monthVar, inoutMap, err, WPRate;
  if (inoutMap.PolicyDetails.sumAssured > Number(4000000)) {
    CalculatedWPRiderSumAssured = sub(inoutMap.WPRider.sumAssured, Number(4000000));
    WPRiderSumAssured1 = inoutMap.WPRider.sumAssured;
  } else {
    CalculatedWPRiderSumAssured = Number(0);
    WPRiderSumAssured1 = inoutMap.PolicyDetails.sumAssured;
  }
  Wholelife905_Lookup_01(inoutMap, function (arguments, _$param78, _$param79) {
    inoutMap = _$param78;
    err = _$param79;
    annPremiumWP = div(mul(inoutMap.premRate, CalculatedWPRiderSumAssured), 1000);
    monthVar = formatDecimal(mul(inoutMap.premRate, inoutMap.monthRate), 2);
    monthPremiumWP = formatDecimal(mul(monthVar, CalculatedWPRiderSumAssured), 2);
    monthPremiumWP = formatDecimal(div(monthPremiumWP, 1000), 0);
    quarVar = formatDecimal(mul(inoutMap.premRate, inoutMap.quarRate), 2);
    quarPremiumWP = formatDecimal(mul(quarVar, CalculatedWPRiderSumAssured), 2);
    quarPremiumWP = formatDecimal(div(quarPremiumWP, 1000), 0);
    semAnnVar = formatDecimal(mul(inoutMap.premRate, inoutMap.semiAnnRate), 2);
    semAnnPremiumWP = formatDecimal(mul(semAnnVar, CalculatedWPRiderSumAssured), 2);
    semAnnPremiumWP = formatDecimal(div(semAnnPremiumWP, 1000), 0);
    PremiumWP1 = annPremiumWP;
    if (inoutMap.PolicyDetails.premiumMode == 6002) {
      PremiumWP1 = semAnnPremiumWP;
    }
    if (inoutMap.PolicyDetails.premiumMode == 6003) {
      PremiumWP1 = quarPremiumWP;
    }
    if (inoutMap.PolicyDetails.premiumMode == 6004) {
      PremiumWP1 = monthPremiumWP;
    }
    Period = min(inoutMap.PolicyDetails.premiumPayingTerm, sub(60, inoutMap.InsuredDetails.age));
    WPRate = 0;
    lookupAsJson('RATE_FACTOR', 'EXTN_RIDER_WP_PREM_TBL', 'INSURED_AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'INSURED_SEX=\'' + inoutMap.InsuredDetails.sex + '\'', 'PAYMENT_PERIOD=\'' + Period + '\'', 'PROD_ID=1220', function (arguments, _$param80, _$param81) {
      WPRate = _$param80;
      err = _$param81;
      PremiumWP1 = formatDecimal(mul(PremiumWP1, WPRate), 2);
      PremiumWP1 = formatDecimal(div(PremiumWP1, 100), 0);
      updateJSON(inoutMap, 'annPremiumWP', annPremiumWP);
      updateJSON(inoutMap, 'monthPremiumWP', monthPremiumWP);
      updateJSON(inoutMap, 'quarPremiumWP', quarPremiumWP);
      updateJSON(inoutMap, 'semAnnPremiumWP', semAnnPremiumWP);
      updateJSON(inoutMap, 'PremiumWP1', PremiumWP1);
      updateJSON(inoutMap, 'CalculatedWPRiderSumAssured', CalculatedWPRiderSumAssured);
      updateJSON(inoutMap, 'WPRiderSumAssured1', WPRiderSumAssured1);
      updateJSON(inoutMap, 'monthVar', monthVar);
      successCB(inoutMap);
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function OccupationaLRuleBaseCodeWholeLife(inoutMap, successCB) {
  var BaseOccCode1, BaseOccCode2, BaseOccCode1A, BaseOccCode2A, err;
  BaseOccCode1A = Number(0);
  BaseOccCode1 = '';
  BaseOccCode2A = Number(0);
  BaseOccCode2 = '';
  BaseOccCode1 = 0;
  lookupAsJson('BASE_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param82, _$param83) {
    BaseOccCode1 = _$param82;
    err = _$param83;
    BaseOccCode2 = 0;
    lookupAsJson('BASE_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param84, _$param85) {
      BaseOccCode2 = _$param84;
      err = _$param85;
      if (BaseOccCode1 == 'D') {
        BaseOccCode1A = Number(10);
      }
      if (BaseOccCode1 == 'IC') {
        BaseOccCode1A = Number(5);
      }
      if (BaseOccCode1 == '4') {
        BaseOccCode1A = Number(4);
      }
      if (BaseOccCode1 == '3') {
        BaseOccCode1A = Number(3);
      }
      if (BaseOccCode1 == '2') {
        BaseOccCode1A = Number(2);
      }
      if (BaseOccCode1 == '1') {
        BaseOccCode1A = Number(1);
      }
      if (BaseOccCode2 == 'D') {
        BaseOccCode2A = Number(10);
      }
      if (BaseOccCode2 == 'IC') {
        BaseOccCode2A = Number(5);
      }
      if (BaseOccCode2 == '4') {
        BaseOccCode2A = Number(4);
      }
      if (BaseOccCode2 == '3') {
        BaseOccCode2A = Number(3);
      }
      if (BaseOccCode2 == '2') {
        BaseOccCode2A = Number(2);
      }
      if (BaseOccCode2 == '1') {
        BaseOccCode2A = Number(1);
      }
      updateJSON(inoutMap, 'BaseOccCode1', BaseOccCode1);
      updateJSON(inoutMap, 'BaseOccCode2', BaseOccCode2);
      updateJSON(inoutMap, 'BaseOccCode1A', BaseOccCode1A);
      updateJSON(inoutMap, 'BaseOccCode2A', BaseOccCode2A);
      successCB(inoutMap);
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function OccupationaLRuleHBOccCodeWholeLife(inoutMap, successCB) {
  var HBOccCode1, HBOccCode2, HBOccCode1A, HBOccCode2A, err;
  HBOccCode1 = '';
  HBOccCode2 = '';
  HBOccCode1A = Number(0);
  HBOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.HBRiders.isRequired == 'Yes') {
      HBOccCode1 = 0;
      lookupAsJson('HB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param86, _$param87) {
        HBOccCode1 = _$param86;
        err = _$param87;
        HBOccCode2 = 0;
        lookupAsJson('HB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param88, _$param89) {
          HBOccCode2 = _$param88;
          err = _$param89;
          if (HBOccCode1 == 'D') {
            HBOccCode1A = Number(10);
          }
          if (HBOccCode1 == 'IC') {
            HBOccCode1A = Number(5);
          }
          if (HBOccCode1 == '4') {
            HBOccCode1A = Number(4);
          }
          if (HBOccCode1 == '3') {
            HBOccCode1A = Number(3);
          }
          if (HBOccCode1 == '2') {
            HBOccCode1A = Number(2);
          }
          if (HBOccCode1 == '1') {
            HBOccCode1A = Number(1);
          }
          if (HBOccCode2 == 'D') {
            HBOccCode2A = Number(10);
          }
          if (HBOccCode2 == 'IC') {
            HBOccCode2A = Number(5);
          }
          if (HBOccCode2 == '4') {
            HBOccCode2A = Number(4);
          }
          if (HBOccCode2 == '3') {
            HBOccCode2A = Number(3);
          }
          if (HBOccCode2 == '2') {
            HBOccCode2A = Number(2);
          }
          if (HBOccCode2 == '1') {
            HBOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'HBOccCode1', HBOccCode1);
    updateJSON(inoutMap, 'HBOccCode2', HBOccCode2);
    updateJSON(inoutMap, 'HBOccCode1A', HBOccCode1A);
    updateJSON(inoutMap, 'HBOccCode2A', HBOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleHSOccCodeWholeLife(inoutMap, successCB) {
  var HSOccCode1, HSOccCode2, HSOccCode1A, HSOccCode2A, err;
  HSOccCode1 = '';
  HSOccCode2 = '';
  HSOccCode1A = Number(0);
  HSOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.HSRiders.isRequired == 'Yes') {
      HSOccCode1 = 0;
      lookupAsJson('HS_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param90, _$param91) {
        HSOccCode1 = _$param90;
        err = _$param91;
        HSOccCode2 = 0;
        lookupAsJson('HS_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param92, _$param93) {
          HSOccCode2 = _$param92;
          err = _$param93;
          if (HSOccCode1 == 'D') {
            HSOccCode1A = Number(10);
          }
          if (HSOccCode1 == 'IC') {
            HSOccCode1A = Number(5);
          }
          if (HSOccCode1 == '4') {
            HSOccCode1A = Number(4);
          }
          if (HSOccCode1 == '3') {
            HSOccCode1A = Number(3);
          }
          if (HSOccCode1 == '2') {
            HSOccCode1A = Number(2);
          }
          if (HSOccCode1 == '1') {
            HSOccCode1A = Number(1);
          }
          if (HSOccCode2 == 'D') {
            HSOccCode2A = Number(10);
          }
          if (HSOccCode2 == 'IC') {
            HSOccCode2A = Number(5);
          }
          if (HSOccCode2 == '4') {
            HSOccCode2A = Number(4);
          }
          if (HSOccCode2 == '3') {
            HSOccCode2A = Number(3);
          }
          if (HSOccCode2 == '2') {
            HSOccCode2A = Number(2);
          }
          if (HSOccCode2 == '1') {
            HSOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'HSOccCode1', HSOccCode1);
    updateJSON(inoutMap, 'HSOccCode2', HSOccCode2);
    updateJSON(inoutMap, 'HSOccCode1A', HSOccCode1A);
    updateJSON(inoutMap, 'HSOccCode2A', HSOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleADDCodeWholeLife(inoutMap, successCB) {
  var ADDOccCode1, ADDOccCode2, ADDOccCode1A, ADDOccCode2A, err;
  ADDOccCode1 = '';
  ADDOccCode2 = '';
  ADDOccCode1A = Number(0);
  ADDOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.ADDRiders.isRequired == 'Yes') {
      ADDOccCode1 = 0;
      lookupAsJson('ADD_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param94, _$param95) {
        ADDOccCode1 = _$param94;
        err = _$param95;
        ADDOccCode2 = 0;
        lookupAsJson('ADD_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param96, _$param97) {
          ADDOccCode2 = _$param96;
          err = _$param97;
          if (ADDOccCode1 == 'D') {
            ADDOccCode1A = Number(10);
          }
          if (ADDOccCode1 == 'IC') {
            ADDOccCode1A = Number(5);
          }
          if (ADDOccCode1 == '4') {
            ADDOccCode1A = Number(4);
          }
          if (ADDOccCode1 == '3') {
            ADDOccCode1A = Number(3);
          }
          if (ADDOccCode1 == '2') {
            ADDOccCode1A = Number(2);
          }
          if (ADDOccCode1 == '1') {
            ADDOccCode1A = Number(1);
          }
          if (ADDOccCode2 == 'D') {
            ADDOccCode2A = Number(10);
          }
          if (ADDOccCode2 == 'IC') {
            ADDOccCode2A = Number(5);
          }
          if (ADDOccCode2 == '4') {
            ADDOccCode2A = Number(4);
          }
          if (ADDOccCode2 == '3') {
            ADDOccCode2A = Number(3);
          }
          if (ADDOccCode2 == '2') {
            ADDOccCode2A = Number(2);
          }
          if (ADDOccCode2 == '1') {
            ADDOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'ADDOccCode1', ADDOccCode1);
    updateJSON(inoutMap, 'ADDOccCode2', ADDOccCode2);
    updateJSON(inoutMap, 'ADDOccCode1A', ADDOccCode1A);
    updateJSON(inoutMap, 'ADDOccCode2A', ADDOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleADBCodeWholeLife(inoutMap, successCB) {
  var ADBOccCode1, ADBOccCode2, ADBOccCode1A, ADBOccCode2A, err;
  ADBOccCode1 = '';
  ADBOccCode2 = '';
  ADBOccCode1A = Number(0);
  ADBOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.ADBRider.isRequired == 'Yes') {
      ADBOccCode1 = 0;
      lookupAsJson('ADB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param98, _$param99) {
        ADBOccCode1 = _$param98;
        err = _$param99;
        ADBOccCode2 = 0;
        lookupAsJson('ADB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param100, _$param101) {
          ADBOccCode2 = _$param100;
          err = _$param101;
          if (ADBOccCode1 == 'D') {
            ADBOccCode1A = Number(10);
          }
          if (ADBOccCode1 == 'IC') {
            ADBOccCode1A = Number(5);
          }
          if (ADBOccCode1 == '4') {
            ADBOccCode1A = Number(4);
          }
          if (ADBOccCode1 == '3') {
            ADBOccCode1A = Number(3);
          }
          if (ADBOccCode1 == '2') {
            ADBOccCode1A = Number(2);
          }
          if (ADBOccCode1 == '1') {
            ADBOccCode1A = Number(1);
          }
          if (ADBOccCode2 == 'D') {
            ADBOccCode2A = Number(10);
          }
          if (ADBOccCode2 == 'IC') {
            ADBOccCode2A = Number(5);
          }
          if (ADBOccCode2 == '4') {
            ADBOccCode2A = Number(4);
          }
          if (ADBOccCode2 == '3') {
            ADBOccCode2A = Number(3);
          }
          if (ADBOccCode2 == '2') {
            ADBOccCode2A = Number(2);
          }
          if (ADBOccCode2 == '1') {
            ADBOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'ADBOccCode1', ADBOccCode1);
    updateJSON(inoutMap, 'ADBOccCode2', ADBOccCode2);
    updateJSON(inoutMap, 'ADBOccCode1A', ADBOccCode1A);
    updateJSON(inoutMap, 'ADBOccCode2A', ADBOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleAIOccCodeWholeLife(inoutMap, successCB) {
  var AIOccCode1, AIOccCode2, AIOccCode1A, AIOccCode2A, err;
  AIOccCode1 = '';
  AIOccCode2 = '';
  AIOccCode1A = Number(0);
  AIOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.AIRider.isRequired == 'Yes') {
      AIOccCode1 = 0;
      lookupAsJson('AI_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param102, _$param103) {
        AIOccCode1 = _$param102;
        err = _$param103;
        AIOccCode2 = 0;
        lookupAsJson('AI_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param104, _$param105) {
          AIOccCode2 = _$param104;
          err = _$param105;
          if (AIOccCode1 == 'D') {
            AIOccCode1A = Number(10);
          }
          if (AIOccCode1 == 'IC') {
            AIOccCode1A = Number(5);
          }
          if (AIOccCode1 == '4') {
            AIOccCode1A = Number(4);
          }
          if (AIOccCode1 == '3') {
            AIOccCode1A = Number(3);
          }
          if (AIOccCode1 == '2') {
            AIOccCode1A = Number(2);
          }
          if (AIOccCode1 == '1') {
            AIOccCode1A = Number(1);
          }
          if (AIOccCode2 == 'D') {
            AIOccCode2A = Number(10);
          }
          if (AIOccCode2 == 'IC') {
            AIOccCode2A = Number(5);
          }
          if (AIOccCode2 == '4') {
            AIOccCode2A = Number(4);
          }
          if (AIOccCode2 == '3') {
            AIOccCode2A = Number(3);
          }
          if (AIOccCode2 == '2') {
            AIOccCode2A = Number(2);
          }
          if (AIOccCode2 == '1') {
            AIOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'AIOccCode1', AIOccCode1);
    updateJSON(inoutMap, 'AIOccCode2', AIOccCode2);
    updateJSON(inoutMap, 'AIOccCode1A', AIOccCode1A);
    updateJSON(inoutMap, 'AIOccCode2A', AIOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRulePBOccCodeWholeLife(inoutMap, successCB) {
  var PBOccCode1, PBOccCode2, PBOccCode1A, PBOccCode2A, err;
  PBOccCode1 = '';
  PBOccCode2 = '';
  PBOccCode1A = Number(0);
  PBOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.PBRider.isRequired == 'Yes') {
      PBOccCode1 = 0;
      lookupAsJson('PB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PayorDetails.occCode1 + '\'', function (arguments, _$param106, _$param107) {
        PBOccCode1 = _$param106;
        err = _$param107;
        PBOccCode2 = 0;
        lookupAsJson('PB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PayorDetails.occCode2 + '\'', function (arguments, _$param108, _$param109) {
          PBOccCode2 = _$param108;
          err = _$param109;
          if (PBOccCode1 == 'D') {
            PBOccCode1A = Number(10);
          }
          if (PBOccCode1 == 'IC') {
            PBOccCode1A = Number(5);
          }
          if (PBOccCode1 == '4') {
            PBOccCode1A = Number(4);
          }
          if (PBOccCode1 == '3') {
            PBOccCode1A = Number(3);
          }
          if (PBOccCode1 == '2') {
            PBOccCode1A = Number(2);
          }
          if (PBOccCode1 == '1') {
            PBOccCode1A = Number(1);
          }
          if (PBOccCode2 == 'D') {
            PBOccCode2A = Number(10);
          }
          if (PBOccCode2 == 'IC') {
            PBOccCode2A = Number(5);
          }
          if (PBOccCode2 == '4') {
            PBOccCode2A = Number(4);
          }
          if (PBOccCode2 == '3') {
            PBOccCode2A = Number(3);
          }
          if (PBOccCode2 == '2') {
            PBOccCode2A = Number(2);
          }
          if (PBOccCode2 == '1') {
            PBOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'PBOccCode1', PBOccCode1);
    updateJSON(inoutMap, 'PBOccCode2', PBOccCode2);
    updateJSON(inoutMap, 'PBOccCode1A', PBOccCode1A);
    updateJSON(inoutMap, 'PBOccCode2A', PBOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleWBOccCodeWholeLife(inoutMap, successCB) {
  var WPOccCode1, WPOccCode2, WPOccCode1A, WPOccCode2A, err;
  WPOccCode1 = '';
  WPOccCode2 = '';
  WPOccCode1A = Number(0);
  WPOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.WPRider.isRequired == 'Yes') {
      WPOccCode1 = 0;
      lookupAsJson('WP_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param110, _$param111) {
        WPOccCode1 = _$param110;
        err = _$param111;
        WPOccCode2 = 0;
        lookupAsJson('WP_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param112, _$param113) {
          WPOccCode2 = _$param112;
          err = _$param113;
          if (WPOccCode1 == 'D') {
            WPOccCode1A = Number(10);
          }
          if (WPOccCode1 == 'IC') {
            WPOccCode1A = Number(5);
          }
          if (WPOccCode1 == '4') {
            WPOccCode1A = Number(4);
          }
          if (WPOccCode1 == '3') {
            WPOccCode1A = Number(3);
          }
          if (WPOccCode1 == '2') {
            WPOccCode1A = Number(2);
          }
          if (WPOccCode1 == '1') {
            WPOccCode1A = Number(1);
          }
          if (WPOccCode2 == 'D') {
            WPOccCode2A = Number(10);
          }
          if (WPOccCode2 == 'IC') {
            WPOccCode2A = Number(5);
          }
          if (WPOccCode2 == '4') {
            WPOccCode2A = Number(4);
          }
          if (WPOccCode2 == '3') {
            WPOccCode2A = Number(3);
          }
          if (WPOccCode2 == '2') {
            WPOccCode2A = Number(2);
          }
          if (WPOccCode2 == '1') {
            WPOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'WPOccCode1', WPOccCode1);
    updateJSON(inoutMap, 'WPOccCode2', WPOccCode2);
    updateJSON(inoutMap, 'WPOccCode1A', WPOccCode1A);
    updateJSON(inoutMap, 'WPOccCode2A', WPOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleDDCodeWholeLife(inoutMap, successCB) {
  var DDOccCode1, DDOccCode2, DDOccCode1A, DDOccCode2A, err;
  DDOccCode1 = '';
  DDOccCode2 = '';
  DDOccCode1A = Number(0);
  DDOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.DDRider.isRequired == 'Yes') {
      DDOccCode1 = 0;
      lookupAsJson('DD_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param114, _$param115) {
        DDOccCode1 = _$param114;
        err = _$param115;
        DDOccCode2 = 0;
        lookupAsJson('DD_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param116, _$param117) {
          DDOccCode2 = _$param116;
          err = _$param117;
          if (DDOccCode1 == 'D') {
            DDOccCode1A = Number(10);
          }
          if (DDOccCode1 == 'IC') {
            DDOccCode1A = Number(5);
          }
          if (DDOccCode1 == '4') {
            DDOccCode1A = Number(4);
          }
          if (DDOccCode1 == '3') {
            DDOccCode1A = Number(3);
          }
          if (DDOccCode1 == '2') {
            DDOccCode1A = Number(2);
          }
          if (DDOccCode1 == '1') {
            DDOccCode1A = Number(1);
          }
          if (DDOccCode2 == 'D') {
            DDOccCode2A = Number(10);
          }
          if (DDOccCode2 == 'IC') {
            DDOccCode2A = Number(5);
          }
          if (DDOccCode2 == '4') {
            DDOccCode2A = Number(4);
          }
          if (DDOccCode2 == '3') {
            DDOccCode2A = Number(3);
          }
          if (DDOccCode2 == '2') {
            DDOccCode2A = Number(2);
          }
          if (DDOccCode2 == '1') {
            DDOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'DDOccCode1', DDOccCode1);
    updateJSON(inoutMap, 'DDOccCode2', DDOccCode2);
    updateJSON(inoutMap, 'DDOccCode1A', DDOccCode1A);
    updateJSON(inoutMap, 'DDOccCode2A', DDOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function WholeLife905_Validations(inoutMap, successCB) {
  var minSA, minAgegen20, maxAge, maxAgegen20, minAgegen201, minAgegen202, MaxAgegen201, MaxAgegen202, MaxAgegen203, MaxAgegen204, MinimumPayorage, MaxPayorage, minRiderSA, maxRiderSA1000, maxRiderSA500K, maxRiderSA4MB, maxRiderSA6MB, totalSumInsured, MinSA2L, MaxSA, HSSumAssured, OPDSumAssured, FlagADBRCC, FlagADDRCC, FlagAIRCC, ValidationResults, isSuccess, isSumInsuredFail, err, HSSA1000, HSSA2000, HSSA3000, HSSA5000, HSSA10000, HSSA20000, HSSA30000, HSSA50000, inoutMap;
  ValidationResults = [];
  isSuccess = [];
  isSumInsuredFail = [];
  minSA = 0;
  lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211008', function (arguments, _$param118, _$param119) {
    minSA = _$param118;
    err = _$param119;
    minAgegen20 = 0;
    lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211017', function (arguments, _$param120, _$param121) {
      minAgegen20 = _$param120;
      err = _$param121;
      maxAge = 0;
      lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211007', function (arguments, _$param122, _$param123) {
        maxAge = _$param122;
        err = _$param123;
        maxAgegen20 = 0;
        lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211018', function (arguments, _$param124, _$param125) {
          maxAgegen20 = _$param124;
          err = _$param125;
          minAgegen201 = 0;
          lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211020', function (arguments, _$param126, _$param127) {
            minAgegen201 = _$param126;
            err = _$param127;
            minAgegen202 = 0;
            lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211021', function (arguments, _$param128, _$param129) {
              minAgegen202 = _$param128;
              err = _$param129;
              MaxAgegen201 = 0;
              lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211022', function (arguments, _$param130, _$param131) {
                MaxAgegen201 = _$param130;
                err = _$param131;
                MaxAgegen202 = 0;
                lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211023', function (arguments, _$param132, _$param133) {
                  MaxAgegen202 = _$param132;
                  err = _$param133;
                  MaxAgegen203 = 0;
                  lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211024', function (arguments, _$param134, _$param135) {
                    MaxAgegen203 = _$param134;
                    err = _$param135;
                    MaxAgegen204 = 0;
                    lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211025', function (arguments, _$param136, _$param137) {
                      MaxAgegen204 = _$param136;
                      err = _$param137;
                      MaxPayorage = 0;
                      lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211027', function (arguments, _$param138, _$param139) {
                        MaxPayorage = _$param138;
                        err = _$param139;
                        minRiderSA = 0;
                        lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211040', function (arguments, _$param140, _$param141) {
                          minRiderSA = _$param140;
                          err = _$param141;
                          maxRiderSA1000 = 0;
                          lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211036', function (arguments, _$param142, _$param143) {
                            maxRiderSA1000 = _$param142;
                            err = _$param143;
                            maxRiderSA500K = 0;
                            lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211037', function (arguments, _$param144, _$param145) {
                              maxRiderSA500K = _$param144;
                              err = _$param145;
                              maxRiderSA4MB = 0;
                              lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211038', function (arguments, _$param146, _$param147) {
                                maxRiderSA4MB = _$param146;
                                err = _$param147;
                                maxRiderSA6MB = 0;
                                lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211039', function (arguments, _$param148, _$param149) {
                                  maxRiderSA6MB = _$param148;
                                  err = _$param149;
                                  MinimumPayorage = 0;
                                  lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211042', function (arguments, _$param150, _$param151) {
                                    MinimumPayorage = _$param150;
                                    err = _$param151;
                                    MinSA2L = 0;
                                    lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211059', function (arguments, _$param152, _$param153) {
                                      MinSA2L = _$param152;
                                      err = _$param153;
                                      MaxSA = 0;
                                      lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211061', function (arguments, _$param154, _$param155) {
                                        MaxSA = _$param154;
                                        err = _$param155;
                                        HSSA1000 = 0;
                                        lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211028', function (arguments, _$param156, _$param157) {
                                          HSSA1000 = _$param156;
                                          err = _$param157;
                                          HSSA2000 = 0;
                                          lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211041', function (arguments, _$param158, _$param159) {
                                            HSSA2000 = _$param158;
                                            err = _$param159;
                                            HSSA3000 = 0;
                                            lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211030', function (arguments, _$param160, _$param161) {
                                              HSSA3000 = _$param160;
                                              err = _$param161;
                                              HSSA5000 = 0;
                                              lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211031', function (arguments, _$param162, _$param163) {
                                                HSSA5000 = _$param162;
                                                err = _$param163;
                                                HSSA10000 = 0;
                                                lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211032', function (arguments, _$param164, _$param165) {
                                                  HSSA10000 = _$param164;
                                                  err = _$param165;
                                                  HSSA20000 = 0;
                                                  lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211033', function (arguments, _$param166, _$param167) {
                                                    HSSA20000 = _$param166;
                                                    err = _$param167;
                                                    HSSA30000 = 0;
                                                    lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211034', function (arguments, _$param168, _$param169) {
                                                      HSSA30000 = _$param168;
                                                      err = _$param169;
                                                      HSSA50000 = 0;
                                                      lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=1220', 'propertyType_ID=1211035', function (arguments, _$param170, _$param171) {
                                                        HSSA50000 = _$param170;
                                                        err = _$param171;
                                                        ValidationResults = {};
                                                        isSuccess = Boolean(true);
                                                        isSumInsuredFail = Boolean(true);
                                                        FlagADBRCC = Boolean(true);
                                                        FlagADDRCC = Boolean(true);
                                                        FlagAIRCC = Boolean(true);
                                                        if (Number(inoutMap.InsuredDetails.age) < Number(minAgegen20)) {
                                                          ValidationResults.WhLife90_01 = 'Minimum age for Whole Life 90  is # ' + Number(minAgegen20) + '';
                                                          isSuccess = Boolean(false);
                                                        }
                                                        if (Number(inoutMap.InsuredDetails.age) > Number(maxAgegen20)) {
                                                          ValidationResults.WhLife90_02 = 'Maximum age for Whole life 90 is # ' + Number(maxAgegen20) + '';
                                                          isSuccess = Boolean(false);
                                                        }
                                                        if (Number(inoutMap.PolicyDetails.sumAssured) < Number(minSA) && Number(inoutMap.PolicyDetails.premiumMode) == Number(6001)) {
                                                          ValidationResults.WhLife90_04 = 'Minimum Sum Assured for Whole Life 90 is #' + Number(minSA) + '';
                                                          isSuccess = Boolean(false);
                                                          isSumInsuredFail = Boolean(false);
                                                        }
                                                        if (Number(inoutMap.PolicyDetails.sumAssured) < Number(MinSA2L) && Number(inoutMap.PolicyDetails.premiumMode) != Number(6001)) {
                                                          ValidationResults.WhLife90_64 = 'Minimum Sum Assured for Whole Life 90  is #' + Number(MinSA2L) + '';
                                                          isSuccess = Boolean(false);
                                                          isSumInsuredFail = Boolean(false);
                                                        }
                                                        if (Number(inoutMap.PolicyDetails.sumAssured) > Number(MaxSA)) {
                                                          ValidationResults.WhLife90_65 = 'Maximum Sum Assured for Whole Life 90  is #' + Number(MaxSA) + '';
                                                          isSuccess = Boolean(false);
                                                          isSumInsuredFail = Boolean(false);
                                                        }
                                                        if (inoutMap.AIRider.isRequired == 'Yes' && isSumInsuredFail == Boolean(true)) {
                                                          if (Number(inoutMap.InsuredDetails.age) < Number(minAgegen201)) {
                                                            ValidationResults.WhLife90_05 = 'Minimum age for AIRider  is # ' + Number(minAgegen201) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                          if (Number(inoutMap.InsuredDetails.age) > Number(MaxAgegen201) && Number(inoutMap.PolicyDetails.premiumPayingTerm) == Number(5)) {
                                                            ValidationResults.WhLife90_06 = 'Maximum age for AIRider  is # ' + Number(MaxAgegen201) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                        }
                                                        if (inoutMap.AIRider.isRequired == 'Yes' && Number(inoutMap.InsuredDetails.age) > Number(15) && isSumInsuredFail == Boolean(true)) {
                                                          if (Number(maxRiderSA6MB) > mul(inoutMap.PolicyDetails.sumAssured, Number(3))) {
                                                            DisaplaySAAI = mul(inoutMap.PolicyDetails.sumAssured, Number(3));
                                                          }
                                                          if (Number(maxRiderSA6MB) <= mul(inoutMap.PolicyDetails.sumAssured, Number(3))) {
                                                            DisaplaySAAI = Number(maxRiderSA6MB);
                                                          }
                                                          if (inoutMap.AIRider.sumAssured > mul(inoutMap.PolicyDetails.sumAssured, Number(3)) || inoutMap.AIRider.sumAssured > Number(maxRiderSA6MB) || inoutMap.AIRider.sumAssured < Number(minRiderSA)) {
                                                            ValidationResults.WhLife90_07 = 'Minimum SA for AI Rider is #' + Number(minRiderSA) + '#' + Number(DisaplaySAAI) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                        }
                                                        if (inoutMap.WPRider.isRequired == 'Yes' && isSumInsuredFail == Boolean(true)) {
                                                          if (Number(inoutMap.InsuredDetails.age) < Number(minAgegen201)) {
                                                            ValidationResults.WhLife90_08 = 'Minimum age for WPRider  is # ' + Number(minAgegen201) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                          if (Number(inoutMap.InsuredDetails.age) > Number(MaxAgegen202)) {
                                                            ValidationResults.WhLife90_09 = 'Maximum age for WPRider  is # ' + Number(MaxAgegen202) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                          if (inoutMap.WPRider.sumAssured != inoutMap.PolicyDetails.sumAssured && inoutMap.PolicyDetails.sumAssured <= Number(4000000)) {
                                                            ValidationResults.WhLife90_62 = 'WP Rider Sum Assured should be equal to the Base Sum Assured when the Policy Base SA <= 4,000,000';
                                                            isSuccess = Boolean(false);
                                                          }
                                                          if ((inoutMap.WPRider.sumAssured > inoutMap.PolicyDetails.sumAssured || inoutMap.WPRider.sumAssured < Number(4000000)) && inoutMap.PolicyDetails.sumAssured > Number(4000000)) {
                                                            ValidationResults.WhLife90_63 = 'Minimum and Maximum SA for WP Rider is #' + Number(4000000) + '#' + Number(inoutMap.PolicyDetails.sumAssured) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                        }
                                                        if (inoutMap.DDRider.isRequired == 'Yes' && isSumInsuredFail == Boolean(true)) {
                                                          if (Number(inoutMap.InsuredDetails.age) < Number(minAgegen202)) {
                                                            ValidationResults.WhLife90_10 = 'Minimum age for DDRider  is # ' + Number(minAgegen202) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                          if (Number(inoutMap.InsuredDetails.age) > Number(MaxAgegen202)) {
                                                            ValidationResults.WhLife90_11 = 'Maximum age for DDRider  is # ' + Number(MaxAgegen202) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                          if (Number(maxRiderSA4MB) > mul(inoutMap.PolicyDetails.sumAssured, Number(5))) {
                                                            DisaplaySADD = mul(inoutMap.PolicyDetails.sumAssured, Number(5));
                                                          }
                                                          if (Number(maxRiderSA4MB) <= mul(inoutMap.PolicyDetails.sumAssured, Number(5))) {
                                                            DisaplaySADD = Number(maxRiderSA4MB);
                                                          }
                                                          if (inoutMap.DDRider.sumAssured > mul(inoutMap.PolicyDetails.sumAssured, Number(5)) || inoutMap.DDRider.sumAssured > Number(maxRiderSA4MB) || inoutMap.DDRider.sumAssured < Number(minRiderSA)) {
                                                            ValidationResults.WhLife90_12 = 'Minimum SA for DD Rider is #' + Number(minRiderSA) + '#' + Number(DisaplaySADD) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                        }
                                                        if (inoutMap.ADBRider.isRequired == 'Yes' && isSumInsuredFail == Boolean(true)) {
                                                          if (Number(inoutMap.PolicyDetails.premiumPayingTerm) == Number(5) && Number(inoutMap.InsuredDetails.age) > Number(MaxAgegen201)) {
                                                            ValidationResults.WhLife90_13 = 'Maximum age for ADBRider  is # ' + Number(MaxAgegen201) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                        }
                                                        if (inoutMap.ADBRider.isRequired == 'Yes' && Number(inoutMap.InsuredDetails.age) <= Number(15) && isSumInsuredFail == Boolean(true)) {
                                                          if (Number(maxRiderSA500K) > mul(inoutMap.PolicyDetails.sumAssured, Number(3))) {
                                                            DisaplaySAADB = mul(inoutMap.PolicyDetails.sumAssured, Number(3));
                                                          }
                                                          if (Number(maxRiderSA500K) <= mul(inoutMap.PolicyDetails.sumAssured, Number(3))) {
                                                            DisaplaySAADB = Number(maxRiderSA500K);
                                                          }
                                                          if (inoutMap.ADBRider.sumAssured > mul(inoutMap.PolicyDetails.sumAssured, Number(3)) || inoutMap.ADBRider.sumAssured > Number(maxRiderSA500K) || inoutMap.ADBRider.sumAssured < Number(minRiderSA)) {
                                                            ValidationResults.WhLife90_14 = 'Minimum SA for ADB Rider is #' + Number(minRiderSA) + '#' + Number(DisaplaySAADB) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                        }
                                                        if (inoutMap.ADBRider.isRequired == 'Yes' && Number(inoutMap.InsuredDetails.age) > Number(15) && isSumInsuredFail == Boolean(true)) {
                                                          if (Number(maxRiderSA6MB) > mul(inoutMap.PolicyDetails.sumAssured, Number(3))) {
                                                            DisaplaySAADB = mul(inoutMap.PolicyDetails.sumAssured, Number(3));
                                                          }
                                                          if (Number(maxRiderSA6MB) <= mul(inoutMap.PolicyDetails.sumAssured, Number(3))) {
                                                            DisaplaySAADB = Number(maxRiderSA6MB);
                                                          }
                                                          if (inoutMap.ADBRider.sumAssured > mul(inoutMap.PolicyDetails.sumAssured, Number(3)) || inoutMap.ADBRider.sumAssured > Number(maxRiderSA6MB) || inoutMap.ADBRider.sumAssured < Number(minRiderSA)) {
                                                            ValidationResults.WhLife90_15 = 'Minimum SA for ADB Rider is #' + Number(minRiderSA) + '#' + Number(DisaplaySAADB) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                        }
                                                        if (inoutMap.ADDRiders.isRequired == 'Yes') {
                                                          if (Number(inoutMap.PolicyDetails.premiumPayingTerm) == Number(5) && Number(inoutMap.InsuredDetails.age) > Number(MaxAgegen201)) {
                                                            ValidationResults.WhLife90_16 = 'Maximum age for ADDRider  is # ' + Number(MaxAgegen201) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                        }
                                                        if (inoutMap.ADDRiders.isRequired == 'Yes' && Number(inoutMap.InsuredDetails.age) <= Number(15) && isSumInsuredFail == Boolean(true)) {
                                                          if (Number(maxRiderSA500K) > mul(inoutMap.PolicyDetails.sumAssured, Number(3))) {
                                                            DisaplaySAADD = mul(inoutMap.PolicyDetails.sumAssured, Number(3));
                                                          }
                                                          if (Number(maxRiderSA500K) <= mul(inoutMap.PolicyDetails.sumAssured, Number(3))) {
                                                            DisaplaySAADD = Number(maxRiderSA500K);
                                                          }
                                                          if (inoutMap.ADDRiders.sumAssured > mul(inoutMap.PolicyDetails.sumAssured, Number(3)) || inoutMap.ADDRiders.sumAssured > Number(maxRiderSA500K) || inoutMap.ADDRiders.sumAssured < Number(minRiderSA)) {
                                                            ValidationResults.WhLife90_17 = 'Minimum SA for ADD Rider is #' + Number(minRiderSA) + '#' + Number(DisaplaySAADD) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                        }
                                                        if (inoutMap.ADDRiders.isRequired == 'Yes' && Number(inoutMap.InsuredDetails.age) > Number(15) && isSumInsuredFail == Boolean(true)) {
                                                          if (Number(maxRiderSA6MB) > mul(inoutMap.PolicyDetails.sumAssured, Number(3))) {
                                                            DisaplaySAADD = mul(inoutMap.PolicyDetails.sumAssured, Number(3));
                                                          }
                                                          if (Number(maxRiderSA6MB) <= mul(inoutMap.PolicyDetails.sumAssured, Number(3))) {
                                                            DisaplaySAADD = Number(maxRiderSA6MB);
                                                          }
                                                          if (inoutMap.ADDRiders.sumAssured > mul(inoutMap.PolicyDetails.sumAssured, Number(3)) || inoutMap.ADDRiders.sumAssured > Number(maxRiderSA6MB) || inoutMap.ADDRiders.sumAssured < Number(minRiderSA)) {
                                                            ValidationResults.WhLife90_18 = 'Minimum SA for ADD Rider is #' + Number(minRiderSA) + '#' + Number(DisaplaySAADD) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                        }
                                                        if (inoutMap.PBRider.isRequired == 'Yes' && isSumInsuredFail == Boolean(true)) {
                                                          if (Number(inoutMap.InsuredDetails.age) > Number(MaxAgegen203)) {
                                                            ValidationResults.WhLife90_19 = 'Maximum age for PBRider  is # ' + Number(MaxAgegen203) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                          if (Number(inoutMap.PayorDetails.age) < Number(MinimumPayorage)) {
                                                            ValidationResults.WhLife90_20 = 'Minimum payor age for PBRider  is # ' + Number(MinimumPayorage) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                          if (Number(inoutMap.PayorDetails.age) > Number(MaxPayorage)) {
                                                            ValidationResults.WhLife90_21 = 'Maximum payor age for PBRider  is # ' + Number(MaxPayorage) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                        }
                                                        if (inoutMap.HBRiders.isRequired == 'Yes' && isSumInsuredFail == Boolean(true)) {
                                                          if (Number(inoutMap.InsuredDetails.age) > Number(MaxAgegen204)) {
                                                            ValidationResults.WhLife90_22 = 'Maximum age for HBRider  is # ' + Number(MaxAgegen204) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                          if (Number(inoutMap.InsuredDetails.age) <= Number(15) && inoutMap.HBRiders.sumAssured > Number(maxRiderSA1000)) {
                                                            ValidationResults.WhLife90_23 = 'Minimum and Maximum HBRider sum Assured is #' + Number(800) + '#' + Number(maxRiderSA1000) + '';
                                                            isSuccess = Boolean(false);
                                                          }
                                                          if (Number(inoutMap.InsuredDetails.age) < Number(11) && Number(inoutMap.PolicyDetails.premiumPayingTerm) == Number(5)) {
                                                            ValidationResults.WhLife90_24 = 'HBRider Minimum Age should not be less than # ' + Number(6) + '# for Term 5';
                                                            isSuccess = Boolean(false);
                                                          }
                                                        }
                                                        (function (_$cont) {
                                                          if (inoutMap.HSRiders.isRequired == 'Yes' && Number(inoutMap.PolicyDetails.premiumPayingTerm) == Number(5) && isSumInsuredFail == Boolean(true)) {
                                                            HSRiderValidationRuleWholeLife(inoutMap, function (arguments, _$param172, _$param173) {
                                                              inoutMap = _$param172;
                                                              err = _$param173;
                                                              if (isEmptyOrNull(inoutMap.Message26) == Boolean(false)) {
                                                                ValidationResults.WhLife90_26 = inoutMap.Message26;
                                                                isSuccess = inoutMap.isSuccess1;
                                                              }
                                                              if (isEmptyOrNull(inoutMap.Message27) == Boolean(false)) {
                                                                ValidationResults.WhLife90_27 = inoutMap.Message27;
                                                                isSuccess = inoutMap.isSuccess1;
                                                              }
                                                              if (isEmptyOrNull(inoutMap.Message28) == Boolean(false)) {
                                                                ValidationResults.WhLife90_28 = inoutMap.Message28;
                                                                isSuccess = inoutMap.isSuccess1;
                                                              }
                                                              if (isEmptyOrNull(inoutMap.Message29) == Boolean(false)) {
                                                                ValidationResults.WhLife90_29 = inoutMap.Message29;
                                                                isSuccess = inoutMap.isSuccess1;
                                                              }
                                                              if (isEmptyOrNull(inoutMap.Message30) == Boolean(false)) {
                                                                ValidationResults.WhLife90_30 = inoutMap.Message30;
                                                                isSuccess = inoutMap.isSuccess1;
                                                              }
                                                              if (isEmptyOrNull(inoutMap.Message31) == Boolean(false)) {
                                                                ValidationResults.WhLife90_31 = inoutMap.Message31;
                                                                isSuccess = inoutMap.isSuccess1;
                                                              }
                                                              if (isEmptyOrNull(inoutMap.Message32) == Boolean(false)) {
                                                                ValidationResults.WhLife90_32 = inoutMap.Message32;
                                                                isSuccess = inoutMap.isSuccess1;
                                                              }
                                                              if (isEmptyOrNull(inoutMap.Message33) == Boolean(false)) {
                                                                ValidationResults.WhLife90_33 = inoutMap.Message33;
                                                                isSuccess = inoutMap.isSuccess1;
                                                              }
                                                              if (isEmptyOrNull(inoutMap.Message34) == Boolean(false)) {
                                                                ValidationResults.WhLife90_34 = inoutMap.Message34;
                                                                isSuccess = inoutMap.isSuccess1;
                                                              }
                                                              if (Number(inoutMap.InsuredDetails.age) < Number(11)) {
                                                                ValidationResults.WhLife90_90 = 'HSRider Minimum Age should not be less than # ' + Number(11) + '';
                                                                isSuccess = Boolean(false);
                                                              }
                                                              _$cont();
                                                            }.bind(this, arguments));
                                                          } else {
                                                            _$cont();
                                                          }
                                                        }.bind(this)(function (_$err) {
                                                          if (_$err !== undefined)
                                                            return _$cont(_$err);
                                                          (function (_$cont) {
                                                            if (inoutMap.OPDRiders.isRequired == 'Yes' && isSumInsuredFail == Boolean(true)) {
                                                              OPDRiderValidationRuleWholeLife(inoutMap, function (arguments, _$param174, _$param175) {
                                                                inoutMap = _$param174;
                                                                err = _$param175;
                                                                if (isEmptyOrNull(inoutMap.Message225) == Boolean(false)) {
                                                                  ValidationResults.WhLife90_225 = inoutMap.Message225;
                                                                  isSuccess = inoutMap.isSuccess1;
                                                                }
                                                                if (isEmptyOrNull(inoutMap.Message226) == Boolean(false)) {
                                                                  ValidationResults.WhLife90_226 = inoutMap.Message225;
                                                                  isSuccess = inoutMap.isSuccess1;
                                                                }
                                                                if (isEmptyOrNull(inoutMap.Message227) == Boolean(false)) {
                                                                  ValidationResults.WhLife90_227 = inoutMap.Message227;
                                                                  isSuccess = inoutMap.isSuccess1;
                                                                }
                                                                if (inoutMap.HSRiders.isRequired != 'Yes') {
                                                                  ValidationResults.WhLife90_228 = 'OPD rider should be bought with HS rider';
                                                                  isSuccess = Boolean(false);
                                                                }
                                                                _$cont();
                                                              }.bind(this, arguments));
                                                            } else {
                                                              _$cont();
                                                            }
                                                          }.bind(this)(function (_$err) {
                                                            if (_$err !== undefined)
                                                              return _$cont(_$err);
                                                            (function (_$cont) {
                                                              if (isSumInsuredFail == Boolean(true)) {
                                                                OccupationaLRuleUpdated1WholeLife(inoutMap, function (arguments, _$param176, _$param177) {
                                                                  inoutMap = _$param176;
                                                                  err = _$param177;
                                                                  _$cont();
                                                                }.bind(this, arguments));
                                                              } else {
                                                                _$cont();
                                                              }
                                                            }.bind(this)(function (_$err) {
                                                              if (_$err !== undefined)
                                                                return _$cont(_$err);
                                                              totalSumInsured = Number(0);
                                                              if (Number(inoutMap.InsuredDetails.age) < Number(16)) {
                                                                if (inoutMap.ADBRider.isRequired == 'Yes') {
                                                                  totalSumInsured = sum(inoutMap.ADBRider.sumAssured, totalSumInsured);
                                                                }
                                                                if (inoutMap.ADDRiders.isRequired == 'Yes') {
                                                                  totalSumInsured = sum(inoutMap.ADDRiders.sumAssured, totalSumInsured);
                                                                }
                                                                if (inoutMap.AIRider.isRequired == 'Yes') {
                                                                  totalSumInsured = sum(inoutMap.AIRider.sumAssured, totalSumInsured);
                                                                }
                                                              }
                                                              if (Number(inoutMap.InsuredDetails.age) > Number(15)) {
                                                                if (inoutMap.ADBRider.isRequired == 'Yes') {
                                                                  totalSumInsured = sum(inoutMap.ADBRider.sumAssured, totalSumInsured);
                                                                }
                                                                if (inoutMap.ADDRiders.isRequired == 'Yes') {
                                                                  totalSumInsured = sum(inoutMap.ADDRiders.sumAssured, totalSumInsured);
                                                                }
                                                                if (inoutMap.AIRider.isRequired == 'Yes') {
                                                                  totalSumInsured = sum(inoutMap.AIRider.sumAssured, totalSumInsured);
                                                                }
                                                                if (totalSumInsured > Number(6000000)) {
                                                                  ValidationResults.WhLife90_52 = 'Total Rider (ADB,ADD, AI) Sum Insured should not be greater then 6,000,000';
                                                                  isSuccess = Boolean(false);
                                                                }
                                                              }
                                                              if (inoutMap.BaseOccCode == 'D') {
                                                                ValidationResults.WhLife90_53 = 'Product Gen Pro Life is declined for this Occupation';
                                                                isSuccess = Boolean(false);
                                                              }
                                                              if (inoutMap.DDOccCode == 'D' && inoutMap.DDRider.isRequired == 'Yes') {
                                                                ValidationResults.WhLife90_54 = 'Rider DD is declined for this Occupation';
                                                                isSuccess = Boolean(false);
                                                              }
                                                              if (inoutMap.HBOccCode == 'D' && inoutMap.HBRiders.isRequired == 'Yes') {
                                                                ValidationResults.WhLife90_55 = 'Rider HB is declined for this Occupation';
                                                                isSuccess = Boolean(false);
                                                              }
                                                              if (inoutMap.HSOccCode == 'D' && inoutMap.HSRiders.isRequired == 'Yes') {
                                                                ValidationResults.WhLife90_56 = 'Rider HS is declined for this Occupation';
                                                                isSuccess = Boolean(false);
                                                              }
                                                              if (inoutMap.ADDOccCode == 'D' && inoutMap.ADDRiders.isRequired == 'Yes') {
                                                                ValidationResults.WhLife90_57 = 'Rider ADD is declined for this Occupation';
                                                                isSuccess = Boolean(false);
                                                              }
                                                              if (inoutMap.ADBOccCode == 'D' && inoutMap.ADBRider.isRequired == 'Yes') {
                                                                ValidationResults.WhLife90_58 = 'Rider ADB is declined for this Occupation';
                                                                isSuccess = Boolean(false);
                                                              }
                                                              if (inoutMap.AIOccCode == 'D' && inoutMap.AIRider.isRequired == 'Yes') {
                                                                ValidationResults.WhLife90_59 = 'Rider AI is declined for this Occupation';
                                                                isSuccess = Boolean(false);
                                                              }
                                                              if (inoutMap.PBOccCode == 'D' && inoutMap.PBRider.isRequired == 'Yes') {
                                                                ValidationResults.WhLife90_60 = 'Rider PB is declined for this Occupation';
                                                                isSuccess = Boolean(false);
                                                              }
                                                              if (inoutMap.WPOccCode == 'D' && inoutMap.WPRider.isRequired == 'Yes') {
                                                                ValidationResults.WhLife90_61 = 'Rider WP is declined for this Occupation';
                                                                isSuccess = Boolean(false);
                                                              }
                                                              if (inoutMap.ADBRCCOccCode == 'D' && inoutMap.RCCADBRider.isRequired == 'Yes') {
                                                                ValidationResults.WhLife90_66 = 'Rider ADBRCC is declined for this Occupation';
                                                                isSuccess = Boolean(false);
                                                                FlagADBRCC = Boolean(false);
                                                              }
                                                              if (inoutMap.ADDRCCOccCode == 'D' && inoutMap.RCCADDRider.isRequired == 'Yes') {
                                                                ValidationResults.WhLife90_67 = 'Rider ADDRCC is declined for this Occupation';
                                                                isSuccess = Boolean(false);
                                                                FlagADDRCC = Boolean(false);
                                                              }
                                                              if (inoutMap.AIRCCOccCode == 'D' && inoutMap.RCCAIRider.isRequired == 'Yes') {
                                                                ValidationResults.WhLife90_68 = 'Rider AIRCC is declined for this Occupation';
                                                                isSuccess = Boolean(false);
                                                                FlagAIRCC = Boolean(false);
                                                              }
                                                              (function (_$cont) {
                                                                if (isSuccess == Boolean(true)) {
                                                                  WholeLife905_Illustrations(inoutMap, function (arguments, _$param178, _$param179) {
                                                                    inoutMap = _$param178;
                                                                    err = _$param179;
                                                                    _$cont();
                                                                  }.bind(this, arguments));
                                                                } else {
                                                                  _$cont();
                                                                }
                                                              }.bind(this)(function (_$err) {
                                                                if (_$err !== undefined)
                                                                  return _$cont(_$err);
                                                                updateJSON(inoutMap, 'minSA', minSA);
                                                                updateJSON(inoutMap, 'minAgegen20', minAgegen20);
                                                                updateJSON(inoutMap, 'maxAge', maxAge);
                                                                updateJSON(inoutMap, 'maxAgegen20', maxAgegen20);
                                                                updateJSON(inoutMap, 'minAgegen201', minAgegen201);
                                                                updateJSON(inoutMap, 'minAgegen202', minAgegen202);
                                                                updateJSON(inoutMap, 'MaxAgegen201', MaxAgegen201);
                                                                updateJSON(inoutMap, 'MaxAgegen202', MaxAgegen202);
                                                                updateJSON(inoutMap, 'MaxAgegen203', MaxAgegen203);
                                                                updateJSON(inoutMap, 'MaxAgegen204', MaxAgegen204);
                                                                updateJSON(inoutMap, 'MinimumPayorage', MinimumPayorage);
                                                                updateJSON(inoutMap, 'MaxPayorage', MaxPayorage);
                                                                updateJSON(inoutMap, 'minRiderSA', minRiderSA);
                                                                updateJSON(inoutMap, 'maxRiderSA1000', maxRiderSA1000);
                                                                updateJSON(inoutMap, 'maxRiderSA500K', maxRiderSA500K);
                                                                updateJSON(inoutMap, 'maxRiderSA4MB', maxRiderSA4MB);
                                                                updateJSON(inoutMap, 'maxRiderSA6MB', maxRiderSA6MB);
                                                                updateJSON(inoutMap, 'totalSumInsured', totalSumInsured);
                                                                updateJSON(inoutMap, 'MinSA2L', MinSA2L);
                                                                updateJSON(inoutMap, 'MaxSA', MaxSA);
                                                                updateJSON(inoutMap, 'HSSumAssured', HSSumAssured);
                                                                updateJSON(inoutMap, 'OPDSumAssured', OPDSumAssured);
                                                                updateJSON(inoutMap, 'FlagADBRCC', FlagADBRCC);
                                                                updateJSON(inoutMap, 'FlagADDRCC', FlagADDRCC);
                                                                updateJSON(inoutMap, 'FlagAIRCC', FlagAIRCC);
                                                                updateJSON(inoutMap, 'ValidationResults', ValidationResults);
                                                                updateJSON(inoutMap, 'isSuccess', isSuccess);
                                                                updateJSON(inoutMap, 'isSumInsuredFail', isSumInsuredFail);
                                                                successCB(inoutMap);
                                                              }.bind(this)));
                                                            }.bind(this)));
                                                          }.bind(this)));
                                                        }.bind(this)));
                                                      }.bind(this, arguments));
                                                    }.bind(this, arguments));
                                                  }.bind(this, arguments));
                                                }.bind(this, arguments));
                                              }.bind(this, arguments));
                                            }.bind(this, arguments));
                                          }.bind(this, arguments));
                                        }.bind(this, arguments));
                                      }.bind(this, arguments));
                                    }.bind(this, arguments));
                                  }.bind(this, arguments));
                                }.bind(this, arguments));
                              }.bind(this, arguments));
                            }.bind(this, arguments));
                          }.bind(this, arguments));
                        }.bind(this, arguments));
                      }.bind(this, arguments));
                    }.bind(this, arguments));
                  }.bind(this, arguments));
                }.bind(this, arguments));
              }.bind(this, arguments));
            }.bind(this, arguments));
          }.bind(this, arguments));
        }.bind(this, arguments));
      }.bind(this, arguments));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function WholeLife905_Guideline(inoutMap, successCB) {
  var annPremium, monthPremium, quarPremium, semAnnPremium, annualisedPremium, ADBPremium, ADBRCCPremium, ADDPremium, ADDRCCPremium, AIPremium, AIRCCPremium, PBPremium, PBSumAssured, DDPremium, HSPremium, HBPremium, DDPaymentPeriod, DDPremRate, HBPremRate, HSPremRate, totalRiderPremium, totalPolicyPremium, modalPremium, WPRiderSumAssured, PremiumWP, quarVar, HSDetails, minSumAssured, maxSumAssured, discountedPremium, inoutMap, err;
  WholeLife905RuleSet(inoutMap, function (arguments, _$param180, _$param181) {
    inoutMap = _$param180;
    err = _$param181;
    monthVar = formatDecimal(mul(inoutMap.premRate, inoutMap.monthRate), 2);
    quarVar = formatDecimal(mul(inoutMap.premRate, inoutMap.quarRate), 2);
    semAnnVar = formatDecimal(mul(inoutMap.premRate, inoutMap.semiAnnRate), 2);
    sumAssured = Number(0);
    discountRate = Number(0);
    if (inoutMap.PolicyDetails.isSumAssured == 'Yes' && inoutMap.PolicyDetails.sumAssured < Number(2000000)) {
      discountRate = Number(0);
    } else {
      if (inoutMap.PolicyDetails.isSumAssured == 'Yes' && inoutMap.PolicyDetails.sumAssured >= Number(2000000) && inoutMap.PolicyDetails.sumAssured <= Number(4999999)) {
        discountRate = Number(2);
      } else {
        if (inoutMap.PolicyDetails.isSumAssured == 'Yes' && inoutMap.PolicyDetails.sumAssured >= Number(5000000) && inoutMap.PolicyDetails.sumAssured <= Number(9999999)) {
          discountRate = Number(3);
        } else {
          if (inoutMap.PolicyDetails.isSumAssured == 'Yes' && inoutMap.PolicyDetails.sumAssured >= Number(10000000)) {
            discountRate = Number(4);
          }
        }
      }
    }
    if (inoutMap.PolicyDetails.isSumAssured == 'Yes') {
      annPremium = formatDecimal(mul(inoutMap.premRate, inoutMap.PolicyDetails.sumAssured), 2);
      annPremium = formatDecimal(div(annPremium, 1000), 0);
      monthPremium = formatDecimal(mul(monthVar, inoutMap.PolicyDetails.sumAssured), 2);
      monthPremium = formatDecimal(div(monthPremium, 1000), 0);
      quarPremium = formatDecimal(mul(quarVar, inoutMap.PolicyDetails.sumAssured), 2);
      quarPremium = formatDecimal(div(quarPremium, 1000), 0);
      semAnnPremium = formatDecimal(mul(semAnnVar, inoutMap.PolicyDetails.sumAssured), 2);
      semAnnPremium = formatDecimal(div(semAnnPremium, 1000), 0);
      discountedPremium = formatDecimal(sub(inoutMap.premRate, discountRate), 0);
      discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.PolicyDetails.sumAssured), 2);
      discountedPremium = formatDecimal(div(discountedPremium, 1000), 0);
      annualisedPremium = annPremium;
      modalPremium = annPremium;
      if (inoutMap.PolicyDetails.premiumMode == 6002) {
        annualisedPremium = mul(semAnnPremium, 2);
        modalPremium = semAnnPremium;
        discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.semiAnnRate), 0);
      }
      if (inoutMap.PolicyDetails.premiumMode == 6003) {
        annualisedPremium = mul(quarPremium, 4);
        modalPremium = quarPremium;
        discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.quarRate), 0);
      }
      if (inoutMap.PolicyDetails.premiumMode == 6004) {
        annualisedPremium = mul(monthPremium, 12);
        modalPremium = monthPremium;
        discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.monthRate), 0);
      }
      inoutMap.PolicyDetails.premium = modalPremium;
    } else {
      if (inoutMap.PolicyDetails.isPremium == 'Yes') {
        inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), inoutMap.premRate), 0);
        annualisedPremium = inoutMap.PolicyDetails.premium;
        modalPremium = inoutMap.PolicyDetails.premium;
        discountRate = Number(0);
        if (inoutMap.PolicyDetails.premiumMode == 6002) {
          inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), semAnnVar), 0);
          annualisedPremium = mul(inoutMap.PolicyDetails.premium, 2);
        }
        if (inoutMap.PolicyDetails.premiumMode == 6003) {
          inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), quarVar), 0);
          annualisedPremium = mul(inoutMap.PolicyDetails.premium, 4);
        }
        if (inoutMap.PolicyDetails.premiumMode == 6004) {
          inoutMap.PolicyDetails.sumAssured = formatDecimal(div(mul(Number(1000), inoutMap.PolicyDetails.premium), monthVar), 0);
          annualisedPremium = mul(inoutMap.PolicyDetails.premium, 12);
        }
        if (inoutMap.PolicyDetails.sumAssured < Number(2000000)) {
          discountRate = Number(0);
        } else {
          if (inoutMap.PolicyDetails.sumAssured >= Number(2000000) && inoutMap.PolicyDetails.sumAssured <= Number(4999999)) {
            discountRate = Number(2);
          } else {
            if (inoutMap.PolicyDetails.sumAssured >= Number(5000000) && inoutMap.PolicyDetails.sumAssured <= Number(9999999)) {
              discountRate = Number(3);
            } else {
              if (inoutMap.PolicyDetails.sumAssured >= Number(10000000)) {
                discountRate = Number(4);
              }
            }
          }
        }
        discountedPremium = formatDecimal(sub(inoutMap.premRate, discountRate), 0);
        discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.PolicyDetails.sumAssured), 2);
        discountedPremium = formatDecimal(div(discountedPremium, 1000), 0);
        if (inoutMap.PolicyDetails.premiumMode == 6002) {
          discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.semiAnnRate), 0);
        } else {
          if (inoutMap.PolicyDetails.premiumMode == 6003) {
            discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.quarRate), 0);
          } else {
            if (inoutMap.PolicyDetails.premiumMode == 6004) {
              discountedPremium = formatDecimal(mul(discountedPremium, inoutMap.monthRate), 0);
            }
          }
        }
      }
    }
    minSumAssured = Number(150000);
    maxSumAssured = 'Unlimited';
    discountedPremium = discountedPremium;
    updateJSON(inoutMap, 'annPremium', annPremium);
    updateJSON(inoutMap, 'monthPremium', monthPremium);
    updateJSON(inoutMap, 'quarPremium', quarPremium);
    updateJSON(inoutMap, 'semAnnPremium', semAnnPremium);
    updateJSON(inoutMap, 'annualisedPremium', annualisedPremium);
    updateJSON(inoutMap, 'ADBPremium', ADBPremium);
    updateJSON(inoutMap, 'ADBRCCPremium', ADBRCCPremium);
    updateJSON(inoutMap, 'ADDPremium', ADDPremium);
    updateJSON(inoutMap, 'ADDRCCPremium', ADDRCCPremium);
    updateJSON(inoutMap, 'AIPremium', AIPremium);
    updateJSON(inoutMap, 'AIRCCPremium', AIRCCPremium);
    updateJSON(inoutMap, 'PBPremium', PBPremium);
    updateJSON(inoutMap, 'PBSumAssured', PBSumAssured);
    updateJSON(inoutMap, 'DDPremium', DDPremium);
    updateJSON(inoutMap, 'HSPremium', HSPremium);
    updateJSON(inoutMap, 'HBPremium', HBPremium);
    updateJSON(inoutMap, 'DDPaymentPeriod', DDPaymentPeriod);
    updateJSON(inoutMap, 'DDPremRate', DDPremRate);
    updateJSON(inoutMap, 'HBPremRate', HBPremRate);
    updateJSON(inoutMap, 'HSPremRate', HSPremRate);
    updateJSON(inoutMap, 'totalRiderPremium', totalRiderPremium);
    updateJSON(inoutMap, 'totalPolicyPremium', totalPolicyPremium);
    updateJSON(inoutMap, 'modalPremium', modalPremium);
    updateJSON(inoutMap, 'WPRiderSumAssured', WPRiderSumAssured);
    updateJSON(inoutMap, 'PremiumWP', PremiumWP);
    updateJSON(inoutMap, 'quarVar', quarVar);
    updateJSON(inoutMap, 'HSDetails', HSDetails);
    updateJSON(inoutMap, 'minSumAssured', minSumAssured);
    updateJSON(inoutMap, 'maxSumAssured', maxSumAssured);
    updateJSON(inoutMap, 'discountedPremium', discountedPremium);
    successCB(inoutMap);
  }.bind(this, arguments));
}
function WholeLife905_BIGraph(inoutMap, successCB) {
  var sumAssuredField, premiumModeField, insuredAgeField, planCodeField, planShortNameField, insuredAge1Field, insuredAge5Field, insuredAge10Field, insuredAge15Field, insuredAge20Field, insuredAge25Field, sumAssured4PerField, sumAssured8PerField, sumAssured13PerField, sumAssured16PerField, sumAssured30PerField, sumAssured130PerField, sumAssured150PerField, insuredAge0Field;
  sumAssuredField = inoutMap.PolicyDetails.sumAssured;
  premiumModeField = inoutMap.totalPolicyPremium;
  insuredAge0Field = inoutMap.InsuredDetails.age;
  insuredAge1Field = sum(inoutMap.InsuredDetails.age, 1);
  insuredAge5Field = sum(inoutMap.InsuredDetails.age, 5);
  insuredAge10Field = sum(inoutMap.InsuredDetails.age, 10);
  insuredAge15Field = sum(inoutMap.InsuredDetails.age, 15);
  insuredAge20Field = sum(inoutMap.InsuredDetails.age, 20);
  insuredAge25Field = sum(inoutMap.InsuredDetails.age, 25);
  sumAssured4PerField = formatDecimal(mul(inoutMap.PolicyDetails.sumAssured, 0.004), 0);
  sumAssured8PerField = formatDecimal(mul(inoutMap.PolicyDetails.sumAssured, 0.008), 0);
  sumAssured13PerField = formatDecimal(mul(inoutMap.PolicyDetails.sumAssured, 0.013), 0);
  sumAssured16PerField = formatDecimal(mul(inoutMap.PolicyDetails.sumAssured, 0.016), 0);
  sumAssured30PerField = formatDecimal(mul(inoutMap.PolicyDetails.sumAssured, 0.3), 0);
  sumAssured130PerField = formatDecimal(mul(inoutMap.PolicyDetails.sumAssured, 1.3), 0);
  sumAssured150PerField = formatDecimal(mul(inoutMap.PolicyDetails.sumAssured, 1.5), 0);
  planCodeField = '1P05';
  planShortNameField = 'W05A';
  updateJSON(inoutMap, 'sumAssuredField', sumAssuredField);
  updateJSON(inoutMap, 'premiumModeField', premiumModeField);
  updateJSON(inoutMap, 'insuredAgeField', insuredAgeField);
  updateJSON(inoutMap, 'planCodeField', planCodeField);
  updateJSON(inoutMap, 'planShortNameField', planShortNameField);
  updateJSON(inoutMap, 'insuredAge1Field', insuredAge1Field);
  updateJSON(inoutMap, 'insuredAge5Field', insuredAge5Field);
  updateJSON(inoutMap, 'insuredAge10Field', insuredAge10Field);
  updateJSON(inoutMap, 'insuredAge15Field', insuredAge15Field);
  updateJSON(inoutMap, 'insuredAge20Field', insuredAge20Field);
  updateJSON(inoutMap, 'insuredAge25Field', insuredAge25Field);
  updateJSON(inoutMap, 'sumAssured4PerField', sumAssured4PerField);
  updateJSON(inoutMap, 'sumAssured8PerField', sumAssured8PerField);
  updateJSON(inoutMap, 'sumAssured13PerField', sumAssured13PerField);
  updateJSON(inoutMap, 'sumAssured16PerField', sumAssured16PerField);
  updateJSON(inoutMap, 'sumAssured30PerField', sumAssured30PerField);
  updateJSON(inoutMap, 'sumAssured130PerField', sumAssured130PerField);
  updateJSON(inoutMap, 'sumAssured150PerField', sumAssured150PerField);
  updateJSON(inoutMap, 'insuredAge0Field', insuredAge0Field);
  successCB(inoutMap);
}
function OccupationaLRuleUpdated1WholeLife(inoutMap, successCB) {
  var occupationalCode, BaseOccCode, CIOccCode, DDOccCode, WPOccCode, PBOccCode, AIOccCode, HBOccCode, HSOccCode, ADDOccCode, ADBOccCode, ADBRCCOccCode, ADDRCCOccCode, AIRCCOccCode, OccCode1D, OccCode2D, OccCode1IC, OccCode2IC, SumOccCode1, SumOccCode2, SumOccCode1A, SumOccCode2A, Option, OccCode1, OccCode2, inoutMap, err;
  OccCode1 = [];
  OccCode2 = [];
  SumOccCode1 = Number(0);
  SumOccCode2 = Number(0);
  OccCode1D = Number(0);
  OccCode2D = Number(0);
  OccCode1IC = Number(0);
  OccCode2IC = Number(0);
  Option = '';
  inoutMap.ADBOccCode1A = Number(0);
  inoutMap.ADBOccCode2A = Number(0);
  inoutMap.AIOccCode1A = Number(0);
  inoutMap.AIOccCode2A = Number(0);
  inoutMap.PBOccCode1A = Number(0);
  inoutMap.PBOccCode2A = Number(0);
  inoutMap.WPOccCode1A = Number(0);
  inoutMap.WPOccCode2A = Number(0);
  inoutMap.DDOccCode1A = Number(0);
  inoutMap.DDOccCode2A = Number(0);
  inoutMap.ADDOccCode1A = Number(0);
  inoutMap.ADDOccCode2A = Number(0);
  inoutMap.HBOccCode1A = Number(0);
  inoutMap.HBOccCode2A = Number(0);
  inoutMap.HSOccCode1A = Number(0);
  inoutMap.HSOccCode2A = Number(0);
  inoutMap.CIOccCode1A = Number(0);
  inoutMap.CIOccCode2A = Number(0);
  inoutMap.ADBRCCOccCode1A = Number(0);
  inoutMap.ADBRCCOccCode2A = Number(0);
  inoutMap.ADDRCCOccCode1A = Number(0);
  inoutMap.ADDRCCOccCode2A = Number(0);
  inoutMap.AIRCCOccCode1A = Number(0);
  inoutMap.AIRCCOccCode2A = Number(0);
  OccupationaLRuleBaseCodeWholeLife(inoutMap, function (arguments, _$param182, _$param183) {
    inoutMap = _$param182;
    err = _$param183;
    OccupationaLRuleHBOccCodeWholeLife(inoutMap, function (arguments, _$param184, _$param185) {
      inoutMap = _$param184;
      err = _$param185;
      OccupationaLRuleHSOccCodeWholeLife(inoutMap, function (arguments, _$param186, _$param187) {
        inoutMap = _$param186;
        err = _$param187;
        OccupationaLRuleADDCodeWholeLife(inoutMap, function (arguments, _$param188, _$param189) {
          inoutMap = _$param188;
          err = _$param189;
          OccupationaLRuleADBCodeWholeLife(inoutMap, function (arguments, _$param190, _$param191) {
            inoutMap = _$param190;
            err = _$param191;
            OccupationaLRuleAIOccCodeWholeLife(inoutMap, function (arguments, _$param192, _$param193) {
              inoutMap = _$param192;
              err = _$param193;
              OccupationaLRulePBOccCodeWholeLife(inoutMap, function (arguments, _$param194, _$param195) {
                inoutMap = _$param194;
                err = _$param195;
                OccupationaLRuleWBOccCodeWholeLife(inoutMap, function (arguments, _$param196, _$param197) {
                  inoutMap = _$param196;
                  err = _$param197;
                  OccupationaLRuleDDCodeWholeLife(inoutMap, function (arguments, _$param198, _$param199) {
                    inoutMap = _$param198;
                    err = _$param199;
                    OccupationaLRuleADBRCCCodeWholeLife(inoutMap, function (arguments, _$param200, _$param201) {
                      inoutMap = _$param200;
                      err = _$param201;
                      OccupationaLRuleADDRCCCodeWholeLife(inoutMap, function (arguments, _$param202, _$param203) {
                        inoutMap = _$param202;
                        err = _$param203;
                        OccupationaLRuleAIRCCCodeWholeLife(inoutMap, function (arguments, _$param204, _$param205) {
                          inoutMap = _$param204;
                          err = _$param205;
                          if (inoutMap.ADBRCCOccCode1A == Number(1)) {
                            inoutMap.ADBRCCOccCode1A = inoutMap.ADBOccCode1A;
                          }
                          if (inoutMap.ADBRCCOccCode2A == Number(1)) {
                            inoutMap.ADBRCCOccCode2A = inoutMap.ADBOccCode2A;
                          }
                          if (inoutMap.ADDRCCOccCode1A == Number(1)) {
                            inoutMap.ADDRCCOccCode1A = inoutMap.ADDOccCode1A;
                          }
                          if (inoutMap.ADDRCCOccCode2A == Number(1)) {
                            inoutMap.ADDRCCOccCode2A = inoutMap.ADDOccCode2A;
                          }
                          if (inoutMap.AIRCCOccCode1A == Number(1)) {
                            inoutMap.AIRCCOccCode1A = inoutMap.AIOccCode1A;
                          }
                          if (inoutMap.AIRCCOccCode2A == Number(1)) {
                            inoutMap.AIRCCOccCode2A = inoutMap.AIOccCode2A;
                          }
                          SumOccCode2 = sum(inoutMap.BaseOccCode2A, inoutMap.CIOccCode2A, inoutMap.HBOccCode2A, inoutMap.HSOccCode2A, inoutMap.ADDOccCode2A, inoutMap.AIOccCode2A, inoutMap.WPOccCode2A, inoutMap.ADBOccCode2A, inoutMap.PBOccCode2A, inoutMap.DDOccCode2A);
                          SumOccCode1 = sum(inoutMap.BaseOccCode1A, inoutMap.CIOccCode1A, inoutMap.HBOccCode1A, inoutMap.HSOccCode1A, inoutMap.ADDOccCode1A, inoutMap.AIOccCode1A, inoutMap.WPOccCode1A, inoutMap.ADBOccCode1A, inoutMap.PBOccCode1A, inoutMap.DDOccCode1A);
                          SumOccCode2A = sum(inoutMap.ADBRCCOccCode2A, SumOccCode2, inoutMap.ADDRCCOccCode2A, inoutMap.AIRCCOccCode2A);
                          SumOccCode1A = sum(inoutMap.ADBRCCOccCode1A, SumOccCode1, inoutMap.ADDRCCOccCode1A, inoutMap.AIRCCOccCode1A);
                          OccCode1[1] = inoutMap.CIOccCode1A;
                          OccCode1[2] = inoutMap.DDOccCode1A;
                          OccCode1[3] = inoutMap.WPOccCode1A;
                          OccCode1[4] = inoutMap.PBOccCode1A;
                          OccCode1[5] = inoutMap.AIOccCode1A;
                          OccCode1[6] = inoutMap.HBOccCode1A;
                          OccCode1[7] = inoutMap.HSOccCode1A;
                          OccCode1[8] = inoutMap.ADDOccCode1A;
                          OccCode1[9] = inoutMap.ADBOccCode1A;
                          OccCode1[10] = inoutMap.ADBRCCOccCode1A;
                          OccCode1[11] = inoutMap.ADDRCCOccCode1A;
                          OccCode1[12] = inoutMap.AIRCCOccCode1A;
                          OccCode2[1] = inoutMap.CIOccCode2A;
                          OccCode2[2] = inoutMap.DDOccCode2A;
                          OccCode2[3] = inoutMap.WPOccCode2A;
                          OccCode2[4] = inoutMap.PBOccCode2A;
                          OccCode2[5] = inoutMap.AIOccCode2A;
                          OccCode2[6] = inoutMap.HBOccCode2A;
                          OccCode2[7] = inoutMap.HSOccCode2A;
                          OccCode2[8] = inoutMap.ADDOccCode2A;
                          OccCode2[9] = inoutMap.ADBOccCode2A;
                          OccCode2[10] = inoutMap.ADBRCCOccCode2A;
                          OccCode2[11] = inoutMap.ADDRCCOccCode2A;
                          OccCode2[12] = inoutMap.AIRCCOccCode2A;
                          if (inoutMap.BaseOccCode1A >= Number(10)) {
                            Option = 'A';
                          } else {
                            if (inoutMap.BaseOccCode2A >= Number(10)) {
                              Option = 'B';
                            }
                          }
                          i = 1;
                          while (i < 13) {
                            if (OccCode1[i] == Number(10)) {
                              OccCode1D = sum(OccCode1[i], OccCode1D);
                            }
                            if (OccCode2[i] == Number(10)) {
                              OccCode2D = sum(OccCode2[i], OccCode2D);
                            }
                            if (OccCode1[i] == Number(5)) {
                              OccCode1IC = sum(OccCode2[i], OccCode1IC);
                            }
                            if (OccCode2[i] == Number(5)) {
                              OccCode2IC = sum(OccCode2[i], OccCode2IC);
                            }
                            i++;
                          }
                          if (Number(SumOccCode1A) == Number(SumOccCode2A) && OccCode1D == OccCode2D && OccCode1IC == OccCode2IC && isEmptyOrNull(Option) == Boolean(true)) {
                            if (Number(inoutMap.BaseOccCode1A) >= Number(inoutMap.BaseOccCode2A)) {
                              Option = 'A';
                            } else {
                              Option = 'B';
                            }
                          }
                          if (isEmptyOrNull(Option) == Boolean(true)) {
                            if (OccCode1D > OccCode2D) {
                              Option = 'A';
                            } else {
                              if (OccCode1D < OccCode2D) {
                                Option = 'B';
                              }
                            }
                          }
                          if (isEmptyOrNull(Option) == Boolean(true)) {
                            if (OccCode1IC > OccCode2IC) {
                              Option = 'A';
                            } else {
                              if (OccCode1IC < OccCode2IC) {
                                Option = 'B';
                              }
                            }
                          }
                          if (isEmptyOrNull(Option) == Boolean(true)) {
                            if (Number(SumOccCode1A) > Number(SumOccCode2A)) {
                              Option = 'A';
                            } else {
                              if (Number(SumOccCode1A) < Number(SumOccCode2A)) {
                                Option = 'B';
                              }
                            }
                          }
                          if (Option == 'A') {
                            BaseOccCode = inoutMap.BaseOccCode1;
                            CIOccCode = inoutMap.CIOccCode1;
                            DDOccCode = inoutMap.DDOccCode1;
                            WPOccCode = inoutMap.WPOccCode1;
                            PBOccCode = inoutMap.PBOccCode1;
                            AIOccCode = inoutMap.AIOccCode1;
                            HBOccCode = inoutMap.HBOccCode1;
                            HSOccCode = inoutMap.HSOccCode1;
                            ADDOccCode = inoutMap.ADDOccCode1;
                            ADBOccCode = inoutMap.ADBOccCode1;
                            ADBRCCOccCode = inoutMap.ADBRCCOccCode1;
                            ADDRCCOccCode = inoutMap.ADDRCCOccCode1;
                            AIRCCOccCode = inoutMap.AIRCCOccCode1;
                            occupationalCode = inoutMap.PolicyDetails.occCode1;
                          } else {
                            BaseOccCode = inoutMap.BaseOccCode2;
                            CIOccCode = inoutMap.CIOccCode2;
                            DDOccCode = inoutMap.DDOccCode2;
                            WPOccCode = inoutMap.WPOccCode2;
                            PBOccCode = inoutMap.PBOccCode2;
                            AIOccCode = inoutMap.AIOccCode2;
                            HBOccCode = inoutMap.HBOccCode2;
                            HSOccCode = inoutMap.HSOccCode2;
                            ADDOccCode = inoutMap.ADDOccCode2;
                            ADBOccCode = inoutMap.ADBOccCode2;
                            ADBRCCOccCode = inoutMap.ADBRCCOccCode2;
                            ADDRCCOccCode = inoutMap.ADDRCCOccCode2;
                            AIRCCOccCode = inoutMap.AIRCCOccCode2;
                            occupationalCode = inoutMap.PolicyDetails.occCode2;
                          }
                          updateJSON(inoutMap, 'occupationalCode', occupationalCode);
                          updateJSON(inoutMap, 'BaseOccCode', BaseOccCode);
                          updateJSON(inoutMap, 'CIOccCode', CIOccCode);
                          updateJSON(inoutMap, 'DDOccCode', DDOccCode);
                          updateJSON(inoutMap, 'WPOccCode', WPOccCode);
                          updateJSON(inoutMap, 'PBOccCode', PBOccCode);
                          updateJSON(inoutMap, 'AIOccCode', AIOccCode);
                          updateJSON(inoutMap, 'HBOccCode', HBOccCode);
                          updateJSON(inoutMap, 'HSOccCode', HSOccCode);
                          updateJSON(inoutMap, 'ADDOccCode', ADDOccCode);
                          updateJSON(inoutMap, 'ADBOccCode', ADBOccCode);
                          updateJSON(inoutMap, 'ADBRCCOccCode', ADBRCCOccCode);
                          updateJSON(inoutMap, 'ADDRCCOccCode', ADDRCCOccCode);
                          updateJSON(inoutMap, 'AIRCCOccCode', AIRCCOccCode);
                          updateJSON(inoutMap, 'OccCode1D', OccCode1D);
                          updateJSON(inoutMap, 'OccCode2D', OccCode2D);
                          updateJSON(inoutMap, 'OccCode1IC', OccCode1IC);
                          updateJSON(inoutMap, 'OccCode2IC', OccCode2IC);
                          updateJSON(inoutMap, 'SumOccCode1', SumOccCode1);
                          updateJSON(inoutMap, 'SumOccCode2', SumOccCode2);
                          updateJSON(inoutMap, 'SumOccCode1A', SumOccCode1A);
                          updateJSON(inoutMap, 'SumOccCode2A', SumOccCode2A);
                          updateJSON(inoutMap, 'Option', Option);
                          updateJSON(inoutMap, 'OccCode1', OccCode1);
                          updateJSON(inoutMap, 'OccCode2', OccCode2);
                          successCB(inoutMap);
                        }.bind(this, arguments));
                      }.bind(this, arguments));
                    }.bind(this, arguments));
                  }.bind(this, arguments));
                }.bind(this, arguments));
              }.bind(this, arguments));
            }.bind(this, arguments));
          }.bind(this, arguments));
        }.bind(this, arguments));
      }.bind(this, arguments));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function OccupationaLRuleAIRCCCodeWholeLife(inoutMap, successCB) {
  var AIRCCOccCode1, AIRCCOccCode2, AIRCCOccCode1A, AIRCCOccCode2A, err;
  AIRCCOccCode1 = '';
  AIRCCOccCode2 = '';
  AIRCCOccCode1A = Number(0);
  AIRCCOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.RCCAIRider.isRequired == 'Yes') {
      AIRCCOccCode1 = 0;
      lookupAsJson('AIRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param206, _$param207) {
        AIRCCOccCode1 = _$param206;
        err = _$param207;
        AIRCCOccCode2 = 0;
        lookupAsJson('AIRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param208, _$param209) {
          AIRCCOccCode2 = _$param208;
          err = _$param209;
          if (AIRCCOccCode1 == 'D') {
            AIRCCOccCode1A = Number(10);
          }
          if (AIRCCOccCode1 == 'IC') {
            AIRCCOccCode1A = Number(5);
          }
          if (AIRCCOccCode1 == 'Y') {
            AIRCCOccCode1A = Number(1);
          }
          if (AIRCCOccCode2 == 'D') {
            AIRCCOccCode2A = Number(10);
          }
          if (AIRCCOccCode2 == 'IC') {
            AIRCCOccCode2A = Number(5);
          }
          if (AIRCCOccCode2 == 'Y') {
            AIRCCOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'AIRCCOccCode1', AIRCCOccCode1);
    updateJSON(inoutMap, 'AIRCCOccCode2', AIRCCOccCode2);
    updateJSON(inoutMap, 'AIRCCOccCode1A', AIRCCOccCode1A);
    updateJSON(inoutMap, 'AIRCCOccCode2A', AIRCCOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleADDRCCCodeWholeLife(inoutMap, successCB) {
  var ADDRCCOccCode1, ADDRCCOccCode2, ADDRCCOccCode1A, ADDRCCOccCode2A, err;
  ADDRCCOccCode1 = '';
  ADDRCCOccCode2 = '';
  ADDRCCOccCode1A = Number(0);
  ADDRCCOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.RCCADDRider.isRequired == 'Yes') {
      ADDRCCOccCode1 = 0;
      lookupAsJson('ADDRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param210, _$param211) {
        ADDRCCOccCode1 = _$param210;
        err = _$param211;
        ADDRCCOccCode2 = 0;
        lookupAsJson('ADDRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param212, _$param213) {
          ADDRCCOccCode2 = _$param212;
          err = _$param213;
          if (ADDRCCOccCode1 == 'D') {
            ADDRCCOccCode1A = Number(10);
          }
          if (ADDRCCOccCode1 == 'IC') {
            ADDRCCOccCode1A = Number(5);
          }
          if (ADDRCCOccCode1 == 'Y') {
            ADDRCCOccCode1A = Number(1);
          }
          if (ADDRCCOccCode2 == 'D') {
            ADDRCCOccCode2A = Number(10);
          }
          if (ADDRCCOccCode2 == 'IC') {
            ADDRCCOccCode2A = Number(5);
          }
          if (ADDRCCOccCode2 == 'Y') {
            ADDRCCOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'ADDRCCOccCode1', ADDRCCOccCode1);
    updateJSON(inoutMap, 'ADDRCCOccCode2', ADDRCCOccCode2);
    updateJSON(inoutMap, 'ADDRCCOccCode1A', ADDRCCOccCode1A);
    updateJSON(inoutMap, 'ADDRCCOccCode2A', ADDRCCOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function OccupationaLRuleADBRCCCodeWholeLife(inoutMap, successCB) {
  var ADBRCCOccCode1, ADBRCCOccCode2, ADBRCCOccCode1A, ADBRCCOccCode2A, err;
  ADBRCCOccCode1 = '';
  ADBRCCOccCode2 = '';
  ADBRCCOccCode1A = Number(0);
  ADBRCCOccCode2A = Number(0);
  (function (_$cont) {
    if (inoutMap.RCCADBRider.isRequired == 'Yes') {
      ADBRCCOccCode1 = 0;
      lookupAsJson('ADBRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param214, _$param215) {
        ADBRCCOccCode1 = _$param214;
        err = _$param215;
        ADBRCCOccCode2 = 0;
        lookupAsJson('ADBRCC_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param216, _$param217) {
          ADBRCCOccCode2 = _$param216;
          err = _$param217;
          if (ADBRCCOccCode1 == 'D') {
            ADBRCCOccCode1A = Number(10);
          }
          if (ADBRCCOccCode1 == 'IC') {
            ADBRCCOccCode1A = Number(5);
          }
          if (ADBRCCOccCode1 == 'Y') {
            ADBRCCOccCode1A = Number(1);
          }
          if (ADBRCCOccCode2 == 'D') {
            ADBRCCOccCode2A = Number(10);
          }
          if (ADBRCCOccCode2 == 'IC') {
            ADBRCCOccCode2A = Number(5);
          }
          if (ADBRCCOccCode2 == 'Y') {
            ADBRCCOccCode2A = Number(1);
          }
          _$cont();
        }.bind(this, arguments));
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    updateJSON(inoutMap, 'ADBRCCOccCode1', ADBRCCOccCode1);
    updateJSON(inoutMap, 'ADBRCCOccCode2', ADBRCCOccCode2);
    updateJSON(inoutMap, 'ADBRCCOccCode1A', ADBRCCOccCode1A);
    updateJSON(inoutMap, 'ADBRCCOccCode2A', ADBRCCOccCode2A);
    successCB(inoutMap);
  }.bind(this)));
}
function HSRiderValidationRuleWholeLife(inoutMap, successCB) {
  var HSSA1000, HSSA2000, HSSA3000, HSSA5000, HSSA10000, HSSA20000, HSSA30000, HSSA50000, Message26, Message27, Message28, Message29, Message30, Message31, Message32, Message33, Message34, ValidationResults1, isSuccess1, err;
  ValidationResults1 = [];
  isSuccess1 = [];
  HSSA1000 = 0;
  lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211028', function (arguments, _$param218, _$param219) {
    HSSA1000 = _$param218;
    err = _$param219;
    HSSA2000 = 0;
    lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211041', function (arguments, _$param220, _$param221) {
      HSSA2000 = _$param220;
      err = _$param221;
      HSSA3000 = 0;
      lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211030', function (arguments, _$param222, _$param223) {
        HSSA3000 = _$param222;
        err = _$param223;
        HSSA5000 = 0;
        lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211031', function (arguments, _$param224, _$param225) {
          HSSA5000 = _$param224;
          err = _$param225;
          HSSA10000 = 0;
          lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211032', function (arguments, _$param226, _$param227) {
            HSSA10000 = _$param226;
            err = _$param227;
            HSSA20000 = 0;
            lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211033', function (arguments, _$param228, _$param229) {
              HSSA20000 = _$param228;
              err = _$param229;
              HSSA30000 = 0;
              lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211034', function (arguments, _$param230, _$param231) {
                HSSA30000 = _$param230;
                err = _$param231;
                HSSA50000 = 0;
                lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211035', function (arguments, _$param232, _$param233) {
                  HSSA50000 = _$param232;
                  err = _$param233;
                  ValidationResults1 = {};
                  isSuccess1 = Boolean(true);
                  if (Number(inoutMap.InsuredDetails.age) >= Number(10) && Number(inoutMap.InsuredDetails.age) <= Number(15)) {
                    if (Number(inoutMap.PolicyDetails.sumAssured) >= Number(100000) && Number(inoutMap.PolicyDetails.sumAssured) < Number(500000) && Number(inoutMap.HSRiders.sumAssured) > Number(HSSA3000)) {
                      Message26 = 'Minimum and Maximum SA for HS Rider is #' + Number(HSSA1000) + '#' + Number(HSSA3000) + '';
                      isSuccess1 = Boolean(false);
                    }
                    if (Number(inoutMap.PolicyDetails.sumAssured) >= Number(500000) && Number(inoutMap.PolicyDetails.sumAssured) < Number(3000000) && Number(inoutMap.HSRiders.sumAssured) > Number(HSSA5000)) {
                      Message27 = 'Minimum and Maximum SA for HS Rider is #' + Number(HSSA1000) + '#' + Number(HSSA5000) + '';
                      isSuccess1 = Boolean(false);
                    }
                    if (Number(inoutMap.PolicyDetails.sumAssured) >= Number(3000000) && Number(inoutMap.HSRiders.sumAssured) > Number(HSSA10000)) {
                      Message28 = 'Minimum and Maximum SA for HS Rider is #' + Number(HSSA1000) + '#' + Number(HSSA10000) + '';
                      isSuccess1 = Boolean(false);
                    }
                  }
                  if (Number(inoutMap.InsuredDetails.age) >= Number(16) && Number(inoutMap.InsuredDetails.age) <= Number(60)) {
                    if (Number(inoutMap.PolicyDetails.sumAssured) >= Number(100000) && Number(inoutMap.PolicyDetails.sumAssured) < Number(500000) && Number(inoutMap.HSRiders.sumAssured) > Number(HSSA3000)) {
                      Message29 = 'Minimum and Maximum SA for HS Rider is #' + Number(HSSA1000) + '#' + Number(HSSA3000) + '';
                      isSuccess1 = Boolean(false);
                    }
                    if (Number(inoutMap.PolicyDetails.sumAssured) >= Number(500000) && Number(inoutMap.PolicyDetails.sumAssured) < Number(3000000) && Number(inoutMap.HSRiders.sumAssured) > Number(HSSA5000)) {
                      Message30 = 'Minimum and Maximum SA for HS Rider is #' + Number(HSSA1000) + '#' + Number(HSSA5000) + '';
                      isSuccess1 = Boolean(false);
                    }
                    if (Number(inoutMap.PolicyDetails.sumAssured) >= Number(3000000) && Number(inoutMap.PolicyDetails.sumAssured) < Number(5000000) && Number(inoutMap.HSRiders.sumAssured) > Number(HSSA10000)) {
                      Message31 = 'Minimum SA for HS Rider is #' + Number(HSSA1000) + '# and Maximum SA is #' + Number(HSSA10000) + '';
                      isSuccess1 = Boolean(false);
                    }
                    if (Number(inoutMap.PolicyDetails.sumAssured) >= Number(5000000) && Number(inoutMap.PolicyDetails.sumAssured) < Number(10000000) && Number(inoutMap.HSRiders.sumAssured) > Number(HSSA20000)) {
                      Message32 = 'Minimum and Maximum SA for HS Rider is #' + Number(HSSA1000) + '#' + Number(HSSA20000) + '';
                      isSuccess1 = Boolean(false);
                    }
                    if (Number(inoutMap.PolicyDetails.sumAssured) >= Number(10000000) && Number(inoutMap.PolicyDetails.sumAssured) < Number(20000000) && Number(inoutMap.HSRiders.sumAssured) > Number(HSSA30000)) {
                      Message33 = 'Minimum and Maximum SA for HS Rider is #' + Number(HSSA1000) + '#' + Number(HSSA30000) + '';
                      isSuccess1 = Boolean(false);
                    }
                    if (Number(inoutMap.PolicyDetails.sumAssured) >= Number(20000000) && Number(inoutMap.HSRiders.sumAssured) > Number(HSSA50000)) {
                      Message34 = 'Minimum and Maximum SA for HS Rider is #' + Number(HSSA1000) + '#' + Number(HSSA50000) + '';
                      isSuccess1 = Boolean(false);
                    }
                  }
                  updateJSON(inoutMap, 'HSSA1000', HSSA1000);
                  updateJSON(inoutMap, 'HSSA2000', HSSA2000);
                  updateJSON(inoutMap, 'HSSA3000', HSSA3000);
                  updateJSON(inoutMap, 'HSSA5000', HSSA5000);
                  updateJSON(inoutMap, 'HSSA10000', HSSA10000);
                  updateJSON(inoutMap, 'HSSA20000', HSSA20000);
                  updateJSON(inoutMap, 'HSSA30000', HSSA30000);
                  updateJSON(inoutMap, 'HSSA50000', HSSA50000);
                  updateJSON(inoutMap, 'Message26', Message26);
                  updateJSON(inoutMap, 'Message27', Message27);
                  updateJSON(inoutMap, 'Message28', Message28);
                  updateJSON(inoutMap, 'Message29', Message29);
                  updateJSON(inoutMap, 'Message30', Message30);
                  updateJSON(inoutMap, 'Message31', Message31);
                  updateJSON(inoutMap, 'Message32', Message32);
                  updateJSON(inoutMap, 'Message33', Message33);
                  updateJSON(inoutMap, 'Message34', Message34);
                  updateJSON(inoutMap, 'ValidationResults1', ValidationResults1);
                  updateJSON(inoutMap, 'isSuccess1', isSuccess1);
                  successCB(inoutMap);
                }.bind(this, arguments));
              }.bind(this, arguments));
            }.bind(this, arguments));
          }.bind(this, arguments));
        }.bind(this, arguments));
      }.bind(this, arguments));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function OPDRiderValidationRuleWholeLife(inoutMap, successCB) {
  var HSSA1000, HSSA2000, HSSA3000, HSSA5000, HSSA10000, HSSA20000, HSSA30000, HSSA50000, Message26, Message27, Message28, Message29, Message30, Message31, Message32, Message33, Message34, ValidationResults1, isSuccess1, err, OPDSA1500;
  ValidationResults1 = [];
  isSuccess1 = [];
  HSSA1000 = 0;
  lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211028', function (arguments, _$param234, _$param235) {
    HSSA1000 = _$param234;
    err = _$param235;
    OPDSA1500 = 0;
    lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1911043', function (arguments, _$param236, _$param237) {
      OPDSA1500 = _$param236;
      err = _$param237;
      ValidationResults1 = {};
      isSuccess1 = Boolean(true);
      if (Number(inoutMap.InsuredDetails.age) <= Number(10)) {
        if (Number(inoutMap.HSRiders.sumAssured) <= Number(2000) && Number(inoutMap.OPDRiders.sumAssured) > HSSA1000) {
          Message225 = 'Maximum SA for OPD Rider for this selection is #' + Number(HSSA1000);
          isSuccess1 = Boolean(false);
        }
      }
      if (Number(inoutMap.InsuredDetails.age) >= Number(11) && Number(inoutMap.InsuredDetails.age) <= Number(64)) {
        if (Number(inoutMap.HSRiders.sumAssured) < Number(5000) && Number(inoutMap.OPDRiders.sumAssured) > Number(1000)) {
          Message226 = 'Maximum SA for OPD Rider for this selection is #' + Number(HSSA1000);
          isSuccess1 = Boolean(false);
        }
        if (Number(inoutMap.HSRiders.sumAssured) >= Number(5000) && Number(inoutMap.OPDRiders.sumAssured) > OPDSA1500) {
          Message227 = 'Maximum SA for OPD Rider for this selection of age and HS rider SA is #' + Number(OPDSA1500);
          isSuccess1 = Boolean(false);
        }
      }
      updateJSON(inoutMap, 'HSSA1000', HSSA1000);
      updateJSON(inoutMap, 'HSSA2000', HSSA2000);
      updateJSON(inoutMap, 'HSSA3000', HSSA3000);
      updateJSON(inoutMap, 'HSSA5000', HSSA5000);
      updateJSON(inoutMap, 'HSSA10000', HSSA10000);
      updateJSON(inoutMap, 'HSSA20000', HSSA20000);
      updateJSON(inoutMap, 'HSSA30000', HSSA30000);
      updateJSON(inoutMap, 'HSSA50000', HSSA50000);
      updateJSON(inoutMap, 'Message26', Message26);
      updateJSON(inoutMap, 'Message27', Message27);
      updateJSON(inoutMap, 'Message28', Message28);
      updateJSON(inoutMap, 'Message29', Message29);
      updateJSON(inoutMap, 'Message30', Message30);
      updateJSON(inoutMap, 'Message31', Message31);
      updateJSON(inoutMap, 'Message32', Message32);
      updateJSON(inoutMap, 'Message33', Message33);
      updateJSON(inoutMap, 'Message34', Message34);
      updateJSON(inoutMap, 'ValidationResults1', ValidationResults1);
      updateJSON(inoutMap, 'isSuccess1', isSuccess1);
      successCB(inoutMap);
    }.bind(this, arguments));
  }.bind(this, arguments));
}
/* Generated by Continuation.js v0.1.7 */