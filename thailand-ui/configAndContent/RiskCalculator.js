/*
 * Copyright 2015, LifeEngage 
 */
function RiskCalculator(inoutMap, successCB) {
	var riskScore = 0;
	for ( var i = 0; i <inoutMap.UserInput.length ; i++) {
		if (inoutMap.UserInput[i].score && inoutMap.UserInput[i].score !="") {
			var score = inoutMap.UserInput[i].score;
			riskScore = riskScore + score;
		}
	}
	if (riskScore < 10) {
		updateJSON(inoutMap, "risk", "Conservative");
	} else if (riskScore >=10 && riskScore <=20) {
		updateJSON(inoutMap, "risk", "Moderate");
	} else {
		updateJSON(inoutMap, "risk", "Aggressive");
	}
	successCB(inoutMap);
}