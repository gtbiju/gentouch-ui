function OccupationaLRule(inoutMap, successCB) {
  var BaseOccCode1, CIOccCode1, DDOccCode1, WPOccCode1, PBOccCode1, AIOccCode1, HBOccCode1, HSOccCode1, ADDOccCode1, ADBOccCode1, BaseOccCode2, CIOccCode2, DDOccCode2, WPOccCode2, PBOccCode2, AIOccCode2, HBOccCode2, HSOccCode2, ADDOccCode2, ADBOccCode2, BaseOccCode, CIOccCode, DDOccCode, WPOccCode, PBOccCode, AIOccCode, HBOccCode, HSOccCode, ADDOccCode, ADBOccCode, err;
  BaseOccCode1 = 0;
  lookupAsJson('BASE_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param0, _$param1) {
    BaseOccCode1 = _$param0;
    err = _$param1;
    BaseOccCode2 = 0;
    lookupAsJson('BASE_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param2, _$param3) {
      BaseOccCode2 = _$param2;
      err = _$param3;
      CIOccCode1 = 0;
      lookupAsJson('CI_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param4, _$param5) {
        CIOccCode1 = _$param4;
        err = _$param5;
        CIOccCode2 = 0;
        lookupAsJson('CI_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param6, _$param7) {
          CIOccCode2 = _$param6;
          err = _$param7;
          HBOccCode1 = 0;
          lookupAsJson('HB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param8, _$param9) {
            HBOccCode1 = _$param8;
            err = _$param9;
            HBOccCode2 = 0;
            lookupAsJson('HB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param10, _$param11) {
              HBOccCode2 = _$param10;
              err = _$param11;
              HSOccCode1 = 0;
              lookupAsJson('HS_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param12, _$param13) {
                HSOccCode1 = _$param12;
                err = _$param13;
                HSOccCode2 = 0;
                lookupAsJson('HS_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param14, _$param15) {
                  HSOccCode2 = _$param14;
                  err = _$param15;
                  ADDOccCode1 = 0;
                  lookupAsJson('ADD_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param16, _$param17) {
                    ADDOccCode1 = _$param16;
                    err = _$param17;
                    ADDOccCode2 = 0;
                    lookupAsJson('ADD_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param18, _$param19) {
                      ADDOccCode2 = _$param18;
                      err = _$param19;
                      if (BaseOccCode1 == '1' || BaseOccCode2 == '1') {
                        BaseOccCode = Number(1);
                      }
                      if (CIOccCode1 == '1' || CIOccCode2 == '1') {
                        CIOccCode = Number(1);
                      }
                      if (HBOccCode1 == '1' || HBOccCode2 == '1') {
                        HBOccCode = Number(1);
                      }
                      if (HSOccCode1 == '1' || HSOccCode2 == '1') {
                        HSOccCode = Number(1);
                      }
                      if (ADDOccCode1 == '1' || ADDOccCode2 == '1') {
                        ADDOccCode = Number(1);
                      }
                      if (BaseOccCode1 == '2' || BaseOccCode2 == '2') {
                        BaseOccCode = Number(2);
                      }
                      if (CIOccCode1 == '2' || CIOccCode2 == '2') {
                        CIOccCode = Number(2);
                      }
                      if (HBOccCode1 == '2' || HBOccCode2 == '2') {
                        HBOccCode = Number(2);
                      }
                      if (HSOccCode1 == '2' || HSOccCode2 == '2') {
                        HSOccCode = Number(2);
                      }
                      if (ADDOccCode1 == '2' || ADDOccCode2 == '2') {
                        ADDOccCode = Number(2);
                      }
                      if (BaseOccCode1 == '3' || BaseOccCode2 == '3') {
                        BaseOccCode = Number(3);
                      }
                      if (CIOccCode1 == '3' || CIOccCode2 == '3') {
                        CIOccCode = Number(3);
                      }
                      if (HSOccCode1 == '3' || HSOccCode2 == '3') {
                        HSOccCode = Number(3);
                      }
                      if (HBOccCode1 == '3' || HBOccCode2 == '3') {
                        HBOccCode = Number(3);
                      }
                      if (ADDOccCode1 == '3' || ADDOccCode2 == '3') {
                        ADDOccCode = Number(3);
                      }
                      if (BaseOccCode1 == 'IC' || BaseOccCode2 == 'IC' || BaseOccCode1 == '4' || BaseOccCode2 == '4') {
                        BaseOccCode = Number(4);
                      }
                      if (CIOccCode1 == 'IC' || CIOccCode2 == 'IC' || CIOccCode1 == '4' || CIOccCode2 == '4') {
                        CIOccCode = Number(4);
                      }
                      if (HSOccCode1 == 'IC' || HSOccCode2 == 'IC' || HSOccCode1 == '4' || HSOccCode2 == '4') {
                        HSOccCode = Number(4);
                      }
                      if (HBOccCode1 == 'IC' || HBOccCode2 == 'IC' || HBOccCode1 == '4' || HBOccCode2 == '4') {
                        HBOccCode = Number(4);
                      }
                      if (ADDOccCode1 == 'IC' || ADDOccCode2 == 'IC' || ADDOccCode1 == '4' || ADDOccCode2 == '4') {
                        ADDOccCode = Number(4);
                      }
                      if (BaseOccCode1 == 'D' || BaseOccCode2 == 'D') {
                        BaseOccCode = 'D';
                      }
                      if (CIOccCode1 == 'D' || CIOccCode2 == 'D') {
                        CIOccCode = 'D';
                      }
                      if (HSOccCode1 == 'D' || HSOccCode2 == 'D') {
                        HSOccCode = 'D';
                      }
                      if (HBOccCode2 == 'D' || HBOccCode2 == 'D') {
                        HBOccCode = 'D';
                      }
                      if (ADDOccCode1 == 'D' || ADDOccCode2 == 'D') {
                        ADDOccCode = 'D';
                      }
                      updateJSON(inoutMap, 'BaseOccCode1', BaseOccCode1);
                      updateJSON(inoutMap, 'CIOccCode1', CIOccCode1);
                      updateJSON(inoutMap, 'DDOccCode1', DDOccCode1);
                      updateJSON(inoutMap, 'WPOccCode1', WPOccCode1);
                      updateJSON(inoutMap, 'PBOccCode1', PBOccCode1);
                      updateJSON(inoutMap, 'AIOccCode1', AIOccCode1);
                      updateJSON(inoutMap, 'HBOccCode1', HBOccCode1);
                      updateJSON(inoutMap, 'HSOccCode1', HSOccCode1);
                      updateJSON(inoutMap, 'ADDOccCode1', ADDOccCode1);
                      updateJSON(inoutMap, 'ADBOccCode1', ADBOccCode1);
                      updateJSON(inoutMap, 'BaseOccCode2', BaseOccCode2);
                      updateJSON(inoutMap, 'CIOccCode2', CIOccCode2);
                      updateJSON(inoutMap, 'DDOccCode2', DDOccCode2);
                      updateJSON(inoutMap, 'WPOccCode2', WPOccCode2);
                      updateJSON(inoutMap, 'PBOccCode2', PBOccCode2);
                      updateJSON(inoutMap, 'AIOccCode2', AIOccCode2);
                      updateJSON(inoutMap, 'HBOccCode2', HBOccCode2);
                      updateJSON(inoutMap, 'HSOccCode2', HSOccCode2);
                      updateJSON(inoutMap, 'ADDOccCode2', ADDOccCode2);
                      updateJSON(inoutMap, 'ADBOccCode2', ADBOccCode2);
                      updateJSON(inoutMap, 'BaseOccCode', BaseOccCode);
                      updateJSON(inoutMap, 'CIOccCode', CIOccCode);
                      updateJSON(inoutMap, 'DDOccCode', DDOccCode);
                      updateJSON(inoutMap, 'WPOccCode', WPOccCode);
                      updateJSON(inoutMap, 'PBOccCode', PBOccCode);
                      updateJSON(inoutMap, 'AIOccCode', AIOccCode);
                      updateJSON(inoutMap, 'HBOccCode', HBOccCode);
                      updateJSON(inoutMap, 'HSOccCode', HSOccCode);
                      updateJSON(inoutMap, 'ADDOccCode', ADDOccCode);
                      updateJSON(inoutMap, 'ADBOccCode', ADBOccCode);
                      successCB(inoutMap);
                    }.bind(this, arguments));
                  }.bind(this, arguments));
                }.bind(this, arguments));
              }.bind(this, arguments));
            }.bind(this, arguments));
          }.bind(this, arguments));
        }.bind(this, arguments));
      }.bind(this, arguments));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function Gen_Hlth_LookUp_01(inoutMap, successCB) {
  var premRate, monthRate, quarRate, semiAnnRate, ADDPremRate, CIPremRate, HBPremRate, HSPremRate, Package, PERate, DBRate, TCVXRate, RPUCashRate, RPURate, ETIYR, ETIDAY, ETICashRate, ETISARate, err;
  if (inoutMap.PolicyDetails.packageName == 'A') {
    Package = 'PLAN2000';
  } else {
    Package = 'PLAN5000';
  }
  if (inoutMap.CIOccCode == 'IC') {
    CIOccCodeTableFetch = Number(4);
  } else {
    CIOccCodeTableFetch = inoutMap.CIOccCode;
  }
  if (inoutMap.HBOccCode == 'IC') {
    HBOccCodeTableFetch = Number(4);
  } else {
    HBOccCodeTableFetch = inoutMap.HBOccCode;
  }
  if (inoutMap.HSOccCode == 'IC') {
    HSOccCodeTableFetch = Number(4);
  } else {
    HSOccCodeTableFetch = inoutMap.HSOccCode;
  }
  if (inoutMap.ADDOccCode == 'IC') {
    ADDOccCodeTableFetch = Number(4);
  } else {
    ADDOccCodeTableFetch = inoutMap.ADDOccCode;
  }
  monthRate = 0;
  lookupAsJson('PREMIUM_FREQ', 'EXTN_MODAL_PREM_TBL', 'PREMIUM_MODE=6004', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', function (arguments, _$param20, _$param21) {
    monthRate = _$param20;
    err = _$param21;
    quarRate = 0;
    lookupAsJson('PREMIUM_FREQ', 'EXTN_MODAL_PREM_TBL', 'PREMIUM_MODE=6003', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', function (arguments, _$param22, _$param23) {
      quarRate = _$param22;
      err = _$param23;
      semiAnnRate = 0;
      lookupAsJson('PREMIUM_FREQ', 'EXTN_MODAL_PREM_TBL', 'PREMIUM_MODE=6002', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', function (arguments, _$param24, _$param25) {
        semiAnnRate = _$param24;
        err = _$param25;
        premRate = 0;
        lookupAsJson('CI_PREM_RT', 'EXTN_HLTH_PREM_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'INS_AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'INS_SEX=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param26, _$param27) {
          premRate = _$param26;
          err = _$param27;
          ADDPremRate = 0;
          lookupAsJson('ADD_PREM_RT', 'EXTN_HLTH_RIDER_ADD_PREM_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + ADDOccCodeTableFetch + '\'', 'INS_AGE=\'' + inoutMap.InsuredDetails.age + '\'', function (arguments, _$param28, _$param29) {
            ADDPremRate = _$param28;
            err = _$param29;
            CIPremRate = 0;
            lookupAsJson('CI_PREM_RT', 'EXTN_HLTH_RIDER_CI_PREM_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'INS_AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'INS_SEX=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param30, _$param31) {
              CIPremRate = _$param30;
              err = _$param31;
              HBPremRate = 0;
              lookupAsJson('HB_PREM_RT', 'EXTN_HLTH_RIDER_HB_PREM_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + HBOccCodeTableFetch + '\'', 'INS_AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'PLAN_NUM=\'' + Package + '\'', function (arguments, _$param32, _$param33) {
                HBPremRate = _$param32;
                err = _$param33;
                HSPremRate = 0;
                lookupAsJson('HS_PREM_RT', 'EXTN_HLTH_RIDER_HS_PREM_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + HSOccCodeTableFetch + '\'', 'INS_AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'PLAN_NUM=\'' + Package + '\'', 'INS_SEX=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param34, _$param35) {
                  HSPremRate = _$param34;
                  err = _$param35;
                  PERate = 0;
                  lookupAsJson('PE', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param36, _$param37) {
                    PERate = _$param36;
                    err = _$param37;
                    DBRate = 0;
                    lookupAsJson('DB', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param38, _$param39) {
                      DBRate = _$param38;
                      err = _$param39;
                      TCVXRate = 0;
                      lookupAsJson('TCVX', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param40, _$param41) {
                        TCVXRate = _$param40;
                        err = _$param41;
                        RPUCashRate = 0;
                        lookupAsJson('RPU_CASH', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param42, _$param43) {
                          RPUCashRate = _$param42;
                          err = _$param43;
                          RPURate = 0;
                          lookupAsJson('RPU', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY AGE', function (arguments, _$param44, _$param45) {
                            RPURate = _$param44;
                            err = _$param45;
                            ETIYR = 0;
                            lookupAsJson('ETI_YR', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param46, _$param47) {
                              ETIYR = _$param46;
                              err = _$param47;
                              ETIDAY = 0;
                              lookupAsJson('ETI_DAY', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param48, _$param49) {
                                ETIDAY = _$param48;
                                err = _$param49;
                                ETICashRate = 0;
                                lookupAsJson('ETI_CASH', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param50, _$param51) {
                                  ETICashRate = _$param50;
                                  err = _$param51;
                                  ETISARate = 0;
                                  lookupAsJson('ETI_SA', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param52, _$param53) {
                                    ETISARate = _$param52;
                                    err = _$param53;
                                    updateJSON(inoutMap, 'premRate', premRate);
                                    updateJSON(inoutMap, 'monthRate', monthRate);
                                    updateJSON(inoutMap, 'quarRate', quarRate);
                                    updateJSON(inoutMap, 'semiAnnRate', semiAnnRate);
                                    updateJSON(inoutMap, 'ADDPremRate', ADDPremRate);
                                    updateJSON(inoutMap, 'CIPremRate', CIPremRate);
                                    updateJSON(inoutMap, 'HBPremRate', HBPremRate);
                                    updateJSON(inoutMap, 'HSPremRate', HSPremRate);
                                    updateJSON(inoutMap, 'Package', Package);
                                    updateJSON(inoutMap, 'PERate', PERate);
                                    updateJSON(inoutMap, 'DBRate', DBRate);
                                    updateJSON(inoutMap, 'TCVXRate', TCVXRate);
                                    updateJSON(inoutMap, 'RPUCashRate', RPUCashRate);
                                    updateJSON(inoutMap, 'RPURate', RPURate);
                                    updateJSON(inoutMap, 'ETIYR', ETIYR);
                                    updateJSON(inoutMap, 'ETIDAY', ETIDAY);
                                    updateJSON(inoutMap, 'ETICashRate', ETICashRate);
                                    updateJSON(inoutMap, 'ETISARate', ETISARate);
                                    successCB(inoutMap);
                                  }.bind(this, arguments));
                                }.bind(this, arguments));
                              }.bind(this, arguments));
                            }.bind(this, arguments));
                          }.bind(this, arguments));
                        }.bind(this, arguments));
                      }.bind(this, arguments));
                    }.bind(this, arguments));
                  }.bind(this, arguments));
                }.bind(this, arguments));
              }.bind(this, arguments));
            }.bind(this, arguments));
          }.bind(this, arguments));
        }.bind(this, arguments));
      }.bind(this, arguments));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function Gen_Hlth_Premium_02(inoutMap, successCB) {
  var annPremium, monthPremium, quarPremium, semAnnPremium, annualisedPremium, ADDPremium, CIPremium, HSPremium, HBPremium, totalRiderPremium, totalPolicyPremium, modalPremium, quarVar, Package, HSDetails, inoutMap, err;
  HSDetails = [];
  OccupationaLRule(inoutMap, function (arguments, _$param54, _$param55) {
    inoutMap = _$param54;
    err = _$param55;
    Gen_Hlth_LookUp_01(inoutMap, function (arguments, _$param56, _$param57) {
      inoutMap = _$param56;
      err = _$param57;
      monthVar = formatDecimal(mul(inoutMap.premRate, inoutMap.monthRate), 2);
      quarVar = formatDecimal(mul(inoutMap.premRate, inoutMap.quarRate), 2);
      semAnnVar = formatDecimal(mul(inoutMap.premRate, inoutMap.semiAnnRate), 2);
      if (inoutMap.PolicyDetails.packageName == 'A' || inoutMap.PolicyDetails.packageName == 'B') {
        annPremium = formatDecimal(mul(inoutMap.premRate, inoutMap.PolicyDetails.sumAssured), 2);
        annPremium = formatDecimal(div(annPremium, 1000), 0);
        monthPremium = formatDecimal(mul(monthVar, inoutMap.PolicyDetails.sumAssured), 2);
        monthPremium = formatDecimal(div(monthPremium, 1000), 0);
        quarPremium = formatDecimal(mul(quarVar, inoutMap.PolicyDetails.sumAssured), 2);
        quarPremium = formatDecimal(div(quarPremium, 1000), 0);
        semAnnPremium = formatDecimal(mul(semAnnVar, inoutMap.PolicyDetails.sumAssured), 2);
        semAnnPremium = formatDecimal(div(semAnnPremium, 1000), 0);
        annualisedPremium = annPremium;
        modalPremium = annPremium;
        if (inoutMap.PolicyDetails.premiumMode == 6002) {
          annualisedPremium = mul(semAnnPremium, 2);
          modalPremium = semAnnPremium;
        }
        if (inoutMap.PolicyDetails.premiumMode == 6003) {
          annualisedPremium = mul(quarPremium, 4);
          modalPremium = quarPremium;
        }
        if (inoutMap.PolicyDetails.premiumMode == 6004) {
          annualisedPremium = mul(monthPremium, 12);
          modalPremium = monthPremium;
        }
      }
      if (inoutMap.ADDRider.isRequired == 'Yes') {
        if (inoutMap.InsuredDetails.age <= Number(70)) {
          ADDPremium = formatDecimal(mul(inoutMap.ADDRider.sumAssured, inoutMap.ADDPremRate), 2);
          ADDPremium = formatDecimal(div(ADDPremium, 1000), 0);
        }
      } else {
        ADDPremium = Number(0);
      }
      if (inoutMap.CIRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age <= Number(70)) {
        CIPremium = formatDecimal(mul(inoutMap.CIRider.sumAssured, inoutMap.CIPremRate), 2);
        CIPremium = formatDecimal(div(CIPremium, 1000), 0);
      } else {
        CIPremium = Number(0);
      }
      if (inoutMap.HBRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age <= Number(70)) {
        HBPremium = Number(inoutMap.HBPremRate);
      } else {
        HBPremium = Number(0);
      }
      if (inoutMap.HSRider.isRequired == 'Yes') {
        HSPremium = Number(inoutMap.HSPremRate);
      }
      totalRiderPremium = sum(Number(ADDPremium), Number(CIPremium), Number(HBPremium), Number(HSPremium));
      totalPolicyPremium = sum(totalRiderPremium, Number(modalPremium));
      ADDSumAssured60Percent = mul(Number(inoutMap.ADDRider.sumAssured), 0.6);
      ADDSumAssured25Percent = mul(Number(inoutMap.ADDRider.sumAssured), 0.25);
      ADDSumAssured200Percent = mul(Number(inoutMap.ADDRider.sumAssured), 2);
      i = 0;
      function _$loop_0(_$loop_0__$cont) {
        if (i < 9) {
          if (inoutMap.PolicyDetails.packageName == 'A') {
            Package = 'PLAN2000';
          } else {
            Package = 'PLAN5000';
          }
          HSDetails = 0;
          lookupAsJson('HS_DETAILS', 'EXTN_HLTH_RIDER_HS_PDF_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'PLAN_NUM=\'' + Package + '\'', function (arguments, _$param58, _$param59) {
            HSDetails = _$param58;
            err = _$param59;
            i++;
            _$loop_0(_$loop_0__$cont);
          }.bind(this, arguments));
        } else {
          _$loop_0__$cont();
        }
      }
      _$loop_0(function () {
        updateJSON(inoutMap, 'annPremium', annPremium);
        updateJSON(inoutMap, 'monthPremium', monthPremium);
        updateJSON(inoutMap, 'quarPremium', quarPremium);
        updateJSON(inoutMap, 'semAnnPremium', semAnnPremium);
        updateJSON(inoutMap, 'annualisedPremium', annualisedPremium);
        updateJSON(inoutMap, 'ADDPremium', ADDPremium);
        updateJSON(inoutMap, 'CIPremium', CIPremium);
        updateJSON(inoutMap, 'HSPremium', HSPremium);
        updateJSON(inoutMap, 'HBPremium', HBPremium);
        updateJSON(inoutMap, 'totalRiderPremium', totalRiderPremium);
        updateJSON(inoutMap, 'totalPolicyPremium', totalPolicyPremium);
        updateJSON(inoutMap, 'modalPremium', modalPremium);
        updateJSON(inoutMap, 'quarVar', quarVar);
        updateJSON(inoutMap, 'Package', Package);
        updateJSON(inoutMap, 'HSDetails', HSDetails);
        successCB(inoutMap);
      });
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function Gen_Hlth_BFTable_CVTable_03(inoutMap, successCB) {
  var sumOfLivingBenefit, sumOfTaxBenefit, sumOfPremium, sumOfGrandBenefit, sumOfRatePercent, totalBenefit, benefitYear, benefitAge, benefitPremium, benefitTaxAmount, benefitTaxDeduct, benefitCashRate, benefitCashAmount, benefitLifeRate, benefitLifeAmount, cashValue, rpuCash, rpuSA, extPeriodYear, extPeriodDay, etiCash, etiSA;
  benefitYear = [];
  benefitAge = [];
  benefitPremium = [];
  benefitTaxAmount = [];
  benefitTaxDeduct = [];
  benefitCashRate = [];
  benefitCashAmount = [];
  benefitLifeRate = [];
  benefitLifeAmount = [];
  cashValue = [];
  rpuCash = [];
  rpuSA = [];
  extPeriodYear = [];
  extPeriodDay = [];
  etiCash = [];
  etiSA = [];
  varMultiple = Number(1.1);
  sumOfLivingBenefit = Number(0);
  sumOfTaxBenefit = Number(0);
  sumOfPremium = Number(0);
  sumOfGrandBenefit = Number(0);
  sumOfRatePercent = Number(0);
  i = 0;
  while (i < sub(Number(80), inoutMap.InsuredDetails.age)) {
    benefitYear[i] = sum(i, 1);
    benefitAge[i] = sum(inoutMap.InsuredDetails.age, i, 1);
    if (benefitAge[i] <= Number(80)) {
      benefitPremium[i] = formatDecimal(Number(inoutMap.annualisedPremium), 0);
    } else {
      benefitPremium[i] = Number(0);
    }
    benefitCashRate[i] = formatDecimal(inoutMap.PERate[i + 1], 2) + '%';
    benefitCashAmount[i] = formatDecimal(div(inoutMap.PERate[i + 1], 100), 4);
    benefitCashAmount[i] = formatDecimal(mul(benefitCashAmount[i], inoutMap.PolicyDetails.sumAssured), 2);
    benefitLifeRate[i] = formatDecimal(inoutMap.DBRate[i], 2) + '%';
    benefitLifeAmount[i] = formatDecimal(mul(div(inoutMap.DBRate[i], 100), inoutMap.PolicyDetails.sumAssured), 0);
    cashValue[i] = formatDecimal(div(mul(inoutMap.TCVXRate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    rpuCash[i] = formatDecimal(div(mul(inoutMap.RPUCashRate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    rpuSA[i] = formatDecimal(div(mul(inoutMap.RPURate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    extPeriodYear[i] = inoutMap.ETIYR[i + 1];
    extPeriodDay[i] = inoutMap.ETIDAY[i + 1];
    etiCash[i] = formatDecimal(div(mul(inoutMap.ETICashRate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    etiSA[i] = formatDecimal(div(mul(inoutMap.ETISARate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    benefitTaxAmount[i] = formatDecimal(mul(div(inoutMap.taxRate, 100), benefitPremium[i]), 2);
    sumOfLivingBenefit = formatDecimal(sum(sumOfLivingBenefit, benefitCashAmount[i]), 2);
    sumOfTaxBenefit = formatDecimal(sum(sumOfTaxBenefit, benefitTaxAmount[i]), 2);
    sumOfPremium = formatDecimal(sum(sumOfPremium, benefitPremium[i]), 2);
    sumOfRatePercent = formatDecimal(sum(sumOfRatePercent, benefitCashRate[i]), 2) + '%';
    i++;
  }
  totalBenefit = formatDecimal(sub(sum(sumOfLivingBenefit, sumOfTaxBenefit), sumOfPremium), 2);
  updateJSON(inoutMap, 'sumOfLivingBenefit', sumOfLivingBenefit);
  updateJSON(inoutMap, 'sumOfTaxBenefit', sumOfTaxBenefit);
  updateJSON(inoutMap, 'sumOfPremium', sumOfPremium);
  updateJSON(inoutMap, 'sumOfGrandBenefit', sumOfGrandBenefit);
  updateJSON(inoutMap, 'sumOfRatePercent', sumOfRatePercent);
  updateJSON(inoutMap, 'totalBenefit', totalBenefit);
  updateJSON(inoutMap, 'benefitYear', benefitYear);
  updateJSON(inoutMap, 'benefitAge', benefitAge);
  updateJSON(inoutMap, 'benefitPremium', benefitPremium);
  updateJSON(inoutMap, 'benefitTaxAmount', benefitTaxAmount);
  updateJSON(inoutMap, 'benefitTaxDeduct', benefitTaxDeduct);
  updateJSON(inoutMap, 'benefitCashRate', benefitCashRate);
  updateJSON(inoutMap, 'benefitCashAmount', benefitCashAmount);
  updateJSON(inoutMap, 'benefitLifeRate', benefitLifeRate);
  updateJSON(inoutMap, 'benefitLifeAmount', benefitLifeAmount);
  updateJSON(inoutMap, 'cashValue', cashValue);
  updateJSON(inoutMap, 'rpuCash', rpuCash);
  updateJSON(inoutMap, 'rpuSA', rpuSA);
  updateJSON(inoutMap, 'extPeriodYear', extPeriodYear);
  updateJSON(inoutMap, 'extPeriodDay', extPeriodDay);
  updateJSON(inoutMap, 'etiCash', etiCash);
  updateJSON(inoutMap, 'etiSA', etiSA);
  successCB(inoutMap);
}
function Gen_Hlth_Rule_Set(inoutMap, successCB) {
  processRuleSet(inoutMap, [
    'Gen_Hlth_Premium_02',
    'Gen_Hlth_BFTable_CVTable_03',
    successCB
  ]);
}
function Gen_Hlth_Illustrations(inoutMap, successCB) {
  var protectionPeriodFinal, premiumPeriodFinal, sumInsuredFinal, basePremiumFinal, riderPremiumFinal, WPSumAssuredCover, ADBSumAssuredCover, ADBRCCSumAssuredCover, ADDSumAssuredCover, ADDRCCSumAssuredCover, AISumAssuredCover, AIRCCSumAssuredCover, PBSumAssuredCover, DDSumAssuredCover, HBSumAssuredCover, HSSumAssuredCover, PremiumWPCover, ADBPremiumCover, ADBRCCPremiumCover, ADDPremiumCover, ADDRCCPremiumCover, AIPremiumCover, AIRCCPremiumCover, PBPremiumCover, DDPremiumCover, HSPremiumCover, HBPremiumCover, totalPremiumFinal, totalLivingBenefit, totalTaxationBenefit, totalPremiumPaid, grandTotalBenefit, totalRatePercent, BaseOccCodeCover, CIOccCodeCover, DDOccCodeCover, WPOccCodeCover, PBOccCodeCover, AIOccCodeCover, HBOccCodeCover, HSOccCodeCover, ADDOccCodeCover, ADBOccCodeCover, benefitTableData, illustrationTableData, i, inoutMap, err;
  benefitTableData = [];
  illustrationTableData = [];
  Gen_Hlth_Rule_Set(inoutMap, function (arguments, _$param60, _$param61) {
    inoutMap = _$param60;
    err = _$param61;
    if (inoutMap.PolicyDetails.packageName == 'A') {
      protectionPeriodFinal = Number(0);
      premiumPeriodFinal = Number(0);
    } else {
      protectionPeriodFinal = Number(0);
      premiumPeriodFinal = Number(0);
    }
    sumInsuredFinal = inoutMap.PolicyDetails.sumAssured;
    basePremiumFinal = inoutMap.modalPremium;
    riderPremiumFinal = inoutMap.totalRiderPremium;
    WPSumAssuredCover = Number(0);
    ADBSumAssuredCover = Number(0);
    ADBRCCSumAssuredCover = Number(0);
    ADDSumAssuredCover = inoutMap.ADDRider.sumAssured;
    ADDRCCSumAssuredCover = Number(0);
    CISumAssuredCover = inoutMap.CIRider.sumAssured;
    AIRCCSumAssuredCover = Number(0);
    PBSumAssuredCover = Number(0);
    DDSumAssuredCover = Number(0);
    HBSumAssuredCover = inoutMap.HBRider.sumAssured;
    HSSumAssuredCover = inoutMap.HSRider.sumAssured;
    PremiumWPCover = Number(0);
    ADBPremiumCover = Number(0);
    ADBRCCPremiumCover = Number(0);
    ADDPremiumCover = inoutMap.ADDPremium;
    ADDRCCPremiumCover = Number(0);
    AIPremiumCover = inoutMap.CIPremium;
    AIRCCPremiumCover = Number(0);
    PBPremiumCover = Number(0);
    DDPremiumCover = Number(0);
    HSPremiumCover = inoutMap.HSPremium;
    HBPremiumCover = inoutMap.HBPremium;
    totalPremiumFinal = inoutMap.totalPolicyPremium;
    BaseOccCodeCover = inoutMap.BaseOccCode;
    CIOccCodeCover = inoutMap.CIOccCode;
    DDOccCodeCover = inoutMap.DDOccCode;
    WPOccCodeCover = inoutMap.WPOccCode;
    PBOccCodeCover = inoutMap.PBOccCode;
    AIOccCodeCover = inoutMap.AIOccCode;
    HBOccCodeCover = inoutMap.HBOccCode;
    HSOccCodeCover = inoutMap.HSOccCode;
    ADDOccCodeCover = inoutMap.ADDOccCode;
    ADBOccCodeCover = inoutMap.ADBOccCode;
    i = 0;
    while (i <= sub(Number(80), inoutMap.InsuredDetails.age, 1)) {
      rowData = {};
      rowData.policyYear = inoutMap.benefitYear[i];
      rowData.age = inoutMap.benefitAge[i];
      rowData.cashValue = inoutMap.cashValue[i];
      rowData.rpuCash = inoutMap.rpuCash[i];
      rowData.rpuSA = inoutMap.rpuSA[i];
      rowData.extPeriodYear = inoutMap.extPeriodYear[i];
      rowData.extPeriodDay = inoutMap.extPeriodDay[i];
      rowData.etiCash = inoutMap.etiCash[i];
      rowData.etiSA = inoutMap.etiSA[i];
      illustrationTableData[i] = rowData;
      i = i + 1;
    }
    i = 0;
    while (i <= sub(Number(80), inoutMap.InsuredDetails.age, 1)) {
      rowBenefitData = {};
      rowBenefitData.policyYear = inoutMap.benefitYear[i];
      rowBenefitData.age = inoutMap.benefitAge[i];
      rowBenefitData.annualisedPremium = inoutMap.benefitPremium[i];
      rowBenefitData.benefitRate = inoutMap.benefitCashRate[i];
      rowBenefitData.benefitAmount = inoutMap.benefitCashAmount[i];
      rowBenefitData.benefitTaxAmount = inoutMap.benefitTaxAmount[i];
      rowBenefitData.benefitLifeRate = inoutMap.benefitLifeRate[i];
      rowBenefitData.benefitLifeAmount = inoutMap.benefitLifeAmount[i];
      benefitTableData[i] = rowBenefitData;
      i = i + 1;
    }
    totalLivingBenefit = inoutMap.sumOfLivingBenefit;
    totalTaxationBenefit = inoutMap.sumOfTaxBenefit;
    totalPremiumPaid = inoutMap.sumOfPremium;
    grandTotalBenefit = inoutMap.totalBenefit;
    totalRatePercent = inoutMap.sumOfRatePercent;
    updateJSON(inoutMap, 'protectionPeriodFinal', protectionPeriodFinal);
    updateJSON(inoutMap, 'premiumPeriodFinal', premiumPeriodFinal);
    updateJSON(inoutMap, 'sumInsuredFinal', sumInsuredFinal);
    updateJSON(inoutMap, 'basePremiumFinal', basePremiumFinal);
    updateJSON(inoutMap, 'riderPremiumFinal', riderPremiumFinal);
    updateJSON(inoutMap, 'WPSumAssuredCover', WPSumAssuredCover);
    updateJSON(inoutMap, 'ADBSumAssuredCover', ADBSumAssuredCover);
    updateJSON(inoutMap, 'ADBRCCSumAssuredCover', ADBRCCSumAssuredCover);
    updateJSON(inoutMap, 'ADDSumAssuredCover', ADDSumAssuredCover);
    updateJSON(inoutMap, 'ADDRCCSumAssuredCover', ADDRCCSumAssuredCover);
    updateJSON(inoutMap, 'AISumAssuredCover', AISumAssuredCover);
    updateJSON(inoutMap, 'AIRCCSumAssuredCover', AIRCCSumAssuredCover);
    updateJSON(inoutMap, 'PBSumAssuredCover', PBSumAssuredCover);
    updateJSON(inoutMap, 'DDSumAssuredCover', DDSumAssuredCover);
    updateJSON(inoutMap, 'HBSumAssuredCover', HBSumAssuredCover);
    updateJSON(inoutMap, 'HSSumAssuredCover', HSSumAssuredCover);
    updateJSON(inoutMap, 'PremiumWPCover', PremiumWPCover);
    updateJSON(inoutMap, 'ADBPremiumCover', ADBPremiumCover);
    updateJSON(inoutMap, 'ADBRCCPremiumCover', ADBRCCPremiumCover);
    updateJSON(inoutMap, 'ADDPremiumCover', ADDPremiumCover);
    updateJSON(inoutMap, 'ADDRCCPremiumCover', ADDRCCPremiumCover);
    updateJSON(inoutMap, 'AIPremiumCover', AIPremiumCover);
    updateJSON(inoutMap, 'AIRCCPremiumCover', AIRCCPremiumCover);
    updateJSON(inoutMap, 'PBPremiumCover', PBPremiumCover);
    updateJSON(inoutMap, 'DDPremiumCover', DDPremiumCover);
    updateJSON(inoutMap, 'HSPremiumCover', HSPremiumCover);
    updateJSON(inoutMap, 'HBPremiumCover', HBPremiumCover);
    updateJSON(inoutMap, 'totalPremiumFinal', totalPremiumFinal);
    updateJSON(inoutMap, 'totalLivingBenefit', totalLivingBenefit);
    updateJSON(inoutMap, 'totalTaxationBenefit', totalTaxationBenefit);
    updateJSON(inoutMap, 'totalPremiumPaid', totalPremiumPaid);
    updateJSON(inoutMap, 'grandTotalBenefit', grandTotalBenefit);
    updateJSON(inoutMap, 'totalRatePercent', totalRatePercent);
    updateJSON(inoutMap, 'BaseOccCodeCover', BaseOccCodeCover);
    updateJSON(inoutMap, 'CIOccCodeCover', CIOccCodeCover);
    updateJSON(inoutMap, 'DDOccCodeCover', DDOccCodeCover);
    updateJSON(inoutMap, 'WPOccCodeCover', WPOccCodeCover);
    updateJSON(inoutMap, 'PBOccCodeCover', PBOccCodeCover);
    updateJSON(inoutMap, 'AIOccCodeCover', AIOccCodeCover);
    updateJSON(inoutMap, 'HBOccCodeCover', HBOccCodeCover);
    updateJSON(inoutMap, 'HSOccCodeCover', HSOccCodeCover);
    updateJSON(inoutMap, 'ADDOccCodeCover', ADDOccCodeCover);
    updateJSON(inoutMap, 'ADBOccCodeCover', ADBOccCodeCover);
    updateJSON(inoutMap, 'benefitTableData', benefitTableData);
    updateJSON(inoutMap, 'illustrationTableData', illustrationTableData);
    successCB(inoutMap);
  }.bind(this, arguments));
}
/* Generated by Continuation.js v0.1.4 */