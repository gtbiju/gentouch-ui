/*
 * Copyright 2015, LifeEngage 
 */



function DisabilityCalculations(inoutMap, successCB) {
	
	var wealthTransferAssetTransferSlider = div(inoutMap.wealthTransferAssetTransferSlider,100)
	var infaltionRateSlider = div(inoutMap.infaltionRateSlider,100)
	var investmentRateSlider = div(inoutMap.investmentRateSlider,100)
	var savingsRateSlider = div(inoutMap.savingsRateSlider,100) 
	var nominalAmountOfAssetTransfer = mul(Number(inoutMap.wealthTransferAmountOfAssetSlider),Number(wealthTransferAssetTransferSlider))
	var totalGap = Math.round(mul(Number(nominalAmountOfAssetTransfer),pow(sum(1,Number(infaltionRateSlider)),Number(inoutMap.wealthTransferTimeforAssetSlider))))
	var lumpsum = inoutMap.wealthTransferAvailableLumpsum;
	var futureSavings = Math.round(mul(lumpsum,pow(sum(1,Number(savingsRateSlider)),Number(inoutMap.wealthTransferTimeforAssetSlider))))
	var needGap = Math.round(sub(totalGap,futureSavings))
	var needGapfnaReport = abs(Math.round(sub(totalGap,futureSavings)))
	var needGapunit = Math.round(div(needGapfnaReport,inoutMap.defaultunit))
	if(needGap > 0){
		var montlySavings = Math.round(div(mul(needGap,div(div(Number(investmentRateSlider),12),sub(pow(sum(div(Number(investmentRateSlider),12),1),mul(Number(inoutMap.wealthTransferTimeforAssetSlider),12)),1))),sum(div(Number(investmentRateSlider),12),1)))
		var montlySavingsUnit = Math.round(div(montlySavings,inoutMap.defaultunit))
	}
	var xAxisarr1 = [];
	var montlySavingsArr = [];
	for(var i=0;i<=10;i++){
		if(needGap <= 0){
			needGapNew = 0;	
		}else{
			needGapNew = needGap;
		}
		if(needGapNew == 0){
			var montlySavingsVar = 0;
		}else{
			var diffVal = sub(Number(inoutMap.wealthTransferTimeforAssetSlider),Number(i));
			if(diffVal > 0){
				var diffValNew = diffVal;
			}else{
				var diffValNew = 0;
			}
			var montlySavingsVar = Math.round(div(mul(needGapNew,div(div(Number(investmentRateSlider),12),sub(pow(sum(div(Number(investmentRateSlider),12),1),mul(diffValNew,12)),1))),sum(div(Number(investmentRateSlider),12),1)))
		}
		if(montlySavingsVar >= 0 && montlySavingsVar!="Infinity"){
			montlySavingsArr[i] = montlySavingsVar;
			xAxisarr1[i] = i;
		}
	}
		
	var ruleExecutionOutput = [];
	var obj = {};
	ruleExecutionOutput.push(obj);
	updateJSON(inoutMap, 'ruleExecutionOutput', ruleExecutionOutput);
	updateJSON(inoutMap, 'montlySavingsArr', montlySavingsArr);
	updateJSON(inoutMap, 'xAxisarr1', xAxisarr1);
	
	//updateJSON(inoutMap, 'needGapfnaReport', needGapfnaReport);
	//inoutMap.ruleExecutionOutput[0].needGapfnaReport = needGapfnaReport;
	/*if(needGap > 0){
		inoutMap.ruleExecutionOutput[0].monthlySavingsRequiredUnit = montlySavingsUnit;
		updateJSON(inoutMap, 'montlySavings', montlySavings);
		//inoutMap.ruleExecutionOutput[0].montlySavings = montlySavings;
	}*/
	if(needGap <0){
		inoutMap.ruleExecutionOutput[0].showSurplus = needGapunit;
	}else{
		inoutMap.ruleExecutionOutput[0].futureNeedGapunit = needGapunit;
		inoutMap.ruleExecutionOutput[0].monthlySavingsRequiredUnit = montlySavingsUnit;
		updateJSON(inoutMap, 'montlySavings', montlySavings);
	}
	
	//extra fields for fna report
	updateJSON(inoutMap, 'futureSavings', futureSavings);
	updateJSON(inoutMap, 'totalGap', totalGap);
	updateJSON(inoutMap, 'needGap', needGap);
	updateJSON(inoutMap, 'needGapfnaReport', needGapfnaReport);
	
	
	var labels = "fna.needGap,fna.futureSavings,fna.totalGap";
	updateJSON(inoutMap, 'labels', labels);
	var xAxisLabels = [{'0':[{'key':'totalGap','value':'Total Gap'},{'key':'futureSavings','value':'Future Savings'},{'key':'needGap','value':'Need Gap'}]}];
	updateJSON(inoutMap, 'xAxisLabels', xAxisLabels);
	updateJSON(inoutMap, 'needGapForSummary', needGap);
	
	successCB(inoutMap);
}
function getAge(inoutMap, successCB) {

		var dob = inoutMap.myselfDob;
		if (dob != "" && dob != undefined && dob != null) {
			var dobDate = new Date(dob);
			/*
				* In I.E it will accept date format in '/' separator only
			*/
				if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
				dobDate = new Date(dob.split("-").join("/"));
				}
				var todayDate = new Date();
				var yyyy = todayDate.getFullYear().toString();
				var mm = (todayDate.getMonth() + 1).toString();
				// getMonth() is
				// zero-based
				var dd = todayDate.getDate().toString();
				var formatedDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
				if (dobDate > todayDate) {
					age = 0;
				}
				else {
					var age = todayDate.getFullYear() - dobDate.getFullYear();
					if (todayDate.getMonth() < dobDate.getMonth() - 1 || (dobDate.getMonth() - 1 === todayDate.getMonth() && todayDate.getMonth() < dobDate.getDate())) {
						age--;
					}
					if (dob == undefined || dob == "") {
						age = "-";
					}
					//angular.element('#' + id + 'Summary').text(age);
				}
								
			}
			// inoutMap.ruleExecutionOutput[0].currentAge = age;
			//inoutMap.ruleExecutionOutput[0].currentAge = age;
			successCB(inoutMap);
}

function DisabilityGoalCalculator(inoutMap, successCB) {
	processRuleSet(inoutMap, [ 'DisabilityCalculations', 'getAge',
			successCB ]);
}