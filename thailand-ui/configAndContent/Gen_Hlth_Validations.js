function Gen_Hlth_LookUp_01(inoutMap, successCB) {
  var premRate, monthRate, quarRate, semiAnnRate, ADDPremRate, CIPremRate, HBPremRate, HSPremRate, OPDPremRate, Package, PERate, MBRate, DBRate, TCVXRate, RPUCashRate, RPURate, ETIYR, ETIDAY, ETICashRate, ETISARate, HSOccCodeTableFetch, err;
  if (inoutMap.PolicyDetails.packageName == 'A' || inoutMap.PolicyDetails.packageName == 'C') {
    Package = 'PLAN2000';
  } else {
    Package = 'PLAN5000';
  }
  if (inoutMap.CIOccCode == 'IC') {
    CIOccCodeTableFetch = Number(4);
  } else {
    CIOccCodeTableFetch = inoutMap.CIOccCode;
  }
  if (inoutMap.HBOccCode == 'IC') {
    HBOccCodeTableFetch = Number(4);
  } else {
    HBOccCodeTableFetch = inoutMap.HBOccCode;
  }
  if (inoutMap.HSOccCode == 'IC') {
    HSOccCodeTableFetch = Number(4);
  } else {
    HSOccCodeTableFetch = inoutMap.HSOccCode;
  }
  if (inoutMap.ADDOccCode == 'IC') {
    ADDOccCodeTableFetch = Number(4);
  } else {
    ADDOccCodeTableFetch = inoutMap.ADDOccCode;
  }
  monthRate = 0;
  lookupAsJson('PREMIUM_FREQ', 'EXTN_MODAL_PREM_TBL', 'PREMIUM_MODE=6004', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', function (arguments, _$param0, _$param1) {
    monthRate = _$param0;
    err = _$param1;
    quarRate = 0;
    lookupAsJson('PREMIUM_FREQ', 'EXTN_MODAL_PREM_TBL', 'PREMIUM_MODE=6003', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', function (arguments, _$param2, _$param3) {
      quarRate = _$param2;
      err = _$param3;
      semiAnnRate = 0;
      lookupAsJson('PREMIUM_FREQ', 'EXTN_MODAL_PREM_TBL', 'PREMIUM_MODE=6002', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', function (arguments, _$param4, _$param5) {
        semiAnnRate = _$param4;
        err = _$param5;
        premRate = 0;
        lookupAsJson('CI_PREM_RT', 'EXTN_HLTH_PREM_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'INS_AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'INS_SEX=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param6, _$param7) {
          premRate = _$param6;
          err = _$param7;
          ADDPremRate = 0;
          lookupAsJson('ADD_PREM_RT', 'EXTN_HLTH_RIDER_ADD_PREM_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + ADDOccCodeTableFetch + '\'', 'INS_AGE=\'' + inoutMap.InsuredDetails.age + '\'', function (arguments, _$param8, _$param9) {
            ADDPremRate = _$param8;
            err = _$param9;
            CIPremRate = 0;
            lookupAsJson('CI_PREM_RT', 'EXTN_HLTH_RIDER_CI_PREM_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'INS_AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'INS_SEX=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param10, _$param11) {
              CIPremRate = _$param10;
              err = _$param11;
              HBPremRate = 0;
              lookupAsJson('HB_PREM_RT', 'EXTN_HLTH_RIDER_HB_PREM_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + HBOccCodeTableFetch + '\'', 'INS_AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'PLAN_NUM=\'' + Package + '\'', function (arguments, _$param12, _$param13) {
                HBPremRate = _$param12;
                err = _$param13;
                HSPremRate = 0;
                lookupAsJson('HS_PREM_RT', 'EXTN_HLTH_RIDER_HS_PREM_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + HSOccCodeTableFetch + '\'', 'INS_AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'PLAN_NUM=\'' + Package + '\'', 'INS_SEX=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param14, _$param15) {
                  HSPremRate = _$param14;
                  err = _$param15;
                  OPDPremRate = 0;
                  lookupAsJson('OPD_PREM_RT', 'EXTN_HLTH_RIDER_OPD_PREM_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'PREMIUM_MODE=\'' + inoutMap.PolicyDetails.premiumMode + '\'', 'OCC_CLASS=\'' + HSOccCodeTableFetch + '\'', 'INS_AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'PLAN_NUM=\'' + inoutMap.OPDRiders.sumAssured + '\'', 'INS_SEX=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param16, _$param17) {
                    OPDPremRate = _$param16;
                    err = _$param17;
                    PERate = 0;
                    lookupAsJson('PE', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param18, _$param19) {
                      PERate = _$param18;
                      err = _$param19;
                      MBRate = 0;
                      lookupAsJson('MAT_BONUS', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param20, _$param21) {
                        MBRate = _$param20;
                        err = _$param21;
                        DBRate = 0;
                        lookupAsJson('DB', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param22, _$param23) {
                          DBRate = _$param22;
                          err = _$param23;
                          TCVXRate = 0;
                          lookupAsJson('TCVX', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param24, _$param25) {
                            TCVXRate = _$param24;
                            err = _$param25;
                            RPUCashRate = 0;
                            lookupAsJson('RPU_CASH', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param26, _$param27) {
                              RPUCashRate = _$param26;
                              err = _$param27;
                              RPURate = 0;
                              lookupAsJson('RPU', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\' ORDER BY AGE', function (arguments, _$param28, _$param29) {
                                RPURate = _$param28;
                                err = _$param29;
                                ETIYR = 0;
                                lookupAsJson('ETI_YR', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param30, _$param31) {
                                  ETIYR = _$param30;
                                  err = _$param31;
                                  ETIDAY = 0;
                                  lookupAsJson('ETI_DAY', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param32, _$param33) {
                                    ETIDAY = _$param32;
                                    err = _$param33;
                                    ETICashRate = 0;
                                    lookupAsJson('ETI_CASH', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param34, _$param35) {
                                      ETICashRate = _$param34;
                                      err = _$param35;
                                      ETISARate = 0;
                                      lookupAsJson('ETI_SA', 'EXTN_TVVALUE_HLTH_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'AGE=\'' + inoutMap.InsuredDetails.age + '\'', 'GENDER=\'' + inoutMap.InsuredDetails.sex + '\'', function (arguments, _$param36, _$param37) {
                                        ETISARate = _$param36;
                                        err = _$param37;
                                        updateJSON(inoutMap, 'premRate', premRate);
                                        updateJSON(inoutMap, 'monthRate', monthRate);
                                        updateJSON(inoutMap, 'quarRate', quarRate);
                                        updateJSON(inoutMap, 'semiAnnRate', semiAnnRate);
                                        updateJSON(inoutMap, 'ADDPremRate', ADDPremRate);
                                        updateJSON(inoutMap, 'CIPremRate', CIPremRate);
                                        updateJSON(inoutMap, 'HBPremRate', HBPremRate);
                                        updateJSON(inoutMap, 'HSPremRate', HSPremRate);
                                        updateJSON(inoutMap, 'OPDPremRate', OPDPremRate);
                                        updateJSON(inoutMap, 'Package', Package);
                                        updateJSON(inoutMap, 'PERate', PERate);
                                        updateJSON(inoutMap, 'MBRate', MBRate);
                                        updateJSON(inoutMap, 'DBRate', DBRate);
                                        updateJSON(inoutMap, 'TCVXRate', TCVXRate);
                                        updateJSON(inoutMap, 'RPUCashRate', RPUCashRate);
                                        updateJSON(inoutMap, 'RPURate', RPURate);
                                        updateJSON(inoutMap, 'ETIYR', ETIYR);
                                        updateJSON(inoutMap, 'ETIDAY', ETIDAY);
                                        updateJSON(inoutMap, 'ETICashRate', ETICashRate);
                                        updateJSON(inoutMap, 'ETISARate', ETISARate);
                                        updateJSON(inoutMap, 'HSOccCodeTableFetch', HSOccCodeTableFetch);
                                        successCB(inoutMap);
                                      }.bind(this, arguments));
                                    }.bind(this, arguments));
                                  }.bind(this, arguments));
                                }.bind(this, arguments));
                              }.bind(this, arguments));
                            }.bind(this, arguments));
                          }.bind(this, arguments));
                        }.bind(this, arguments));
                      }.bind(this, arguments));
                    }.bind(this, arguments));
                  }.bind(this, arguments));
                }.bind(this, arguments));
              }.bind(this, arguments));
            }.bind(this, arguments));
          }.bind(this, arguments));
        }.bind(this, arguments));
      }.bind(this, arguments));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function Gen_Hlth_Premium_02(inoutMap, successCB) {
  var annPremium, monthPremium, quarPremium, semAnnPremium, annualisedPremium, ADDPremium, CIPremium, HSPremium, OPDPremium, HBPremium, totalRiderPremium, totalPolicyPremium, ADDSumAssured60Percent, ADDSumAssured25Percent, ADDSumAssured200Percent, modalPremium, quarVar, Package, HSDetails, OPDDetails, inoutMap, err;
  HSDetails = [];
  OPDDetails = [];
  OccupationaLRuleUpdated1(inoutMap, function (arguments, _$param38, _$param39) {
    inoutMap = _$param38;
    err = _$param39;
    Gen_Hlth_LookUp_01(inoutMap, function (arguments, _$param40, _$param41) {
      inoutMap = _$param40;
      err = _$param41;
      monthVar = formatDecimal(mul(inoutMap.premRate, inoutMap.monthRate), 2);
      quarVar = formatDecimal(mul(inoutMap.premRate, inoutMap.quarRate), 2);
      semAnnVar = formatDecimal(mul(inoutMap.premRate, inoutMap.semiAnnRate), 2);
      monthVarCI = formatDecimal(mul(inoutMap.CIPremRate, inoutMap.monthRate), 2);
      quarVarCI = formatDecimal(mul(inoutMap.CIPremRate, inoutMap.quarRate), 2);
      semAnnVarCI = formatDecimal(mul(inoutMap.CIPremRate, inoutMap.semiAnnRate), 2);
	if (inoutMap.PolicyDetails.packageName == 'A' || inoutMap.PolicyDetails.packageName == 'B' || inoutMap.PolicyDetails.packageName == 'C' || inoutMap.PolicyDetails.packageName == 'D') {
        annPremium = formatDecimal(mul(inoutMap.premRate, inoutMap.PolicyDetails.sumAssured), 2);
        annPremium = formatDecimal(div(annPremium, 1000), 0);
        monthPremium = formatDecimal(mul(monthVar, inoutMap.PolicyDetails.sumAssured), 2);
        monthPremium = formatDecimal(div(monthPremium, 1000), 0);
        quarPremium = formatDecimal(mul(quarVar, inoutMap.PolicyDetails.sumAssured), 2);
        quarPremium = formatDecimal(div(quarPremium, 1000), 0);
        semAnnPremium = formatDecimal(mul(semAnnVar, inoutMap.PolicyDetails.sumAssured), 2);
        semAnnPremium = formatDecimal(div(semAnnPremium, 1000), 0);
        annualisedPremium = annPremium;
        modalPremium = annPremium;
        if (inoutMap.PolicyDetails.premiumMode == 6002) {
          annualisedPremium = mul(semAnnPremium, 2);
          modalPremium = semAnnPremium;
        }
        if (inoutMap.PolicyDetails.premiumMode == 6003) {
          annualisedPremium = mul(quarPremium, 4);
          modalPremium = quarPremium;
        }
        if (inoutMap.PolicyDetails.premiumMode == 6004) {
          annualisedPremium = mul(monthPremium, 12);
          modalPremium = monthPremium;
        }
      }
      if (inoutMap.ADDRider.isRequired == 'Yes') {
        if (inoutMap.InsuredDetails.age <= Number(70)) {
          ADDPremium = formatDecimal(mul(inoutMap.ADDRider.sumAssured, inoutMap.ADDPremRate), 2);
          ADDPremium = formatDecimal(div(ADDPremium, 1000), 0);
        }
      } else {
        ADDPremium = Number(0);
      }
      if (inoutMap.CIRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age <= Number(70)) {
        CIPremium = formatDecimal(mul(inoutMap.CIRider.sumAssured, inoutMap.CIPremRate), 2);
        CIPremium = formatDecimal(div(CIPremium, 1000), 0);
        if (inoutMap.PolicyDetails.premiumMode == '6002') {
          CIPremium = formatDecimal(mul(inoutMap.CIRider.sumAssured, semAnnVarCI), 2);
          CIPremium = formatDecimal(div(CIPremium, 1000), 0);
        }
        if (inoutMap.PolicyDetails.premiumMode == '6003') {
          CIPremium = formatDecimal(mul(inoutMap.CIRider.sumAssured, quarVarCI), 2);
          CIPremium = formatDecimal(div(CIPremium, 1000), 0);
        }
        if (inoutMap.PolicyDetails.premiumMode == '6004') {
          CIPremium = formatDecimal(mul(inoutMap.CIRider.sumAssured, monthVarCI), 2);
          CIPremium = formatDecimal(div(CIPremium, 1000), 0);
        }
      } else {
        CIPremium = Number(0);
      }
      if (inoutMap.HBRider.isRequired == 'Yes' && inoutMap.InsuredDetails.age <= Number(70) && inoutMap.InsuredDetails.age >= Number(6)) {
        HBPremium = formatDecimal(Number(inoutMap.HBPremRate), 0);
      } else {
        HBPremium = 0;
      }
      if (inoutMap.PolicyDetails.packageName == 'A' && inoutMap.InsuredDetails.age <= Number(5)) {
        inoutMap.HBRider.sumAssured = Number(0);
      }
      if (inoutMap.HSRider.isRequired == 'Yes') {
        HSPremium = formatDecimal(Number(inoutMap.HSPremRate), 0);
      } else {
		HSPremium = 0;  
	  }
      if (inoutMap.OPDRiders.isRequired == 'Yes') {
        OPDPremium = formatDecimal(Number(inoutMap.OPDPremRate), 0);
      } else {
		OPDPremium = "";  
	  }
      totalRiderPremium = formatDecimal(sum(Number(ADDPremium), Number(CIPremium), Number(HBPremium), Number(HSPremium), Number(OPDPremium)), 0);
      totalPolicyPremium = formatDecimal(sum(totalRiderPremium, Number(modalPremium)), 0);
      ADDSumAssured60Percent = formatDecimal(mul(Number(inoutMap.ADDRider.sumAssured), 0.6), 0);
      ADDSumAssured25Percent = formatDecimal(mul(Number(inoutMap.ADDRider.sumAssured), 0.25), 0);
      ADDSumAssured200Percent = formatDecimal(mul(Number(inoutMap.ADDRider.sumAssured), 2), 0);
      i = 0;
      function _$loop_0(_$loop_0__$cont) {
        if (i < 9) {
          if (inoutMap.PolicyDetails.packageName == 'A' || inoutMap.PolicyDetails.packageName == 'C') {
            Package = 'PLAN2000';
          } else {
            Package = 'PLAN5000';
          }
          HSDetails = 0;
          lookupAsJson('HS_DETAILS', 'EXTN_HLTH_RIDER_HS_PDF_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'PLAN_NUM=\'' + Package + '\'', function (arguments, _$param42, _$param43) {
            HSDetails = _$param42;
            err = _$param43;
            OPDDetails = 0;
            lookupAsJson('OPD_DETAILS', 'EXTN_HLTH_RIDER_OPD_PDF_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'PLAN_NUM=\'' + Package + '\'', function (arguments, _$param44, _$param45) {
              OPDDetails = _$param44;
              err = _$param45;
              i++;
              _$loop_0(_$loop_0__$cont);
            }.bind(this, arguments));
          }.bind(this, arguments));
        } else {
          _$loop_0__$cont();
        }
      }
      _$loop_0 = _$loop_0.bind(this);
      _$loop_0(function () {
        updateJSON(inoutMap, 'annPremium', annPremium);
        updateJSON(inoutMap, 'monthPremium', monthPremium);
        updateJSON(inoutMap, 'quarPremium', quarPremium);
        updateJSON(inoutMap, 'semAnnPremium', semAnnPremium);
        updateJSON(inoutMap, 'annualisedPremium', annualisedPremium);
        updateJSON(inoutMap, 'ADDPremium', ADDPremium);
        updateJSON(inoutMap, 'CIPremium', CIPremium);
        updateJSON(inoutMap, 'HSPremium', HSPremium);
        updateJSON(inoutMap, 'OPDPremium', OPDPremium);
        updateJSON(inoutMap, 'HBPremium', HBPremium);
        updateJSON(inoutMap, 'totalRiderPremium', totalRiderPremium);
        updateJSON(inoutMap, 'totalPolicyPremium', totalPolicyPremium);
        updateJSON(inoutMap, 'ADDSumAssured60Percent', ADDSumAssured60Percent);
        updateJSON(inoutMap, 'ADDSumAssured25Percent', ADDSumAssured25Percent);
        updateJSON(inoutMap, 'ADDSumAssured200Percent', ADDSumAssured200Percent);
        updateJSON(inoutMap, 'modalPremium', modalPremium);
        updateJSON(inoutMap, 'quarVar', quarVar);
        updateJSON(inoutMap, 'Package', Package);
        updateJSON(inoutMap, 'HSDetails', HSDetails);
        updateJSON(inoutMap, 'OPDDetails', OPDDetails);
        successCB(inoutMap);
      });
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function Gen_Hlth_BFTable_CVTable_03(inoutMap, successCB) {
  var sumOfLivingBenefit, sumOfTaxBenefit, sumOfPremium, sumOfGrandBenefit, sumOfRatePercent, totalBenefit, sumOfRatePercentWithoutPer, benefitYear, benefitAge, benefitPremium, benefitTaxAmount, benefitTaxDeduct, benefitCashRate, benefitCashAmount, benefitLifeRate, benefitLifeAmount, cashValue, rpuCash, rpuSA, extPeriodYear, extPeriodDay, etiCash, etiSA;
  benefitYear = [];
  benefitAge = [];
  benefitPremium = [];
  benefitTaxAmount = [];
  benefitTaxDeduct = [];
  benefitCashRate = [];
  benefitCashAmount = [];
  benefitLifeRate = [];
  benefitLifeAmount = [];
  cashValue = [];
  rpuCash = [];
  rpuSA = [];
  extPeriodYear = [];
  extPeriodDay = [];
  etiCash = [];
  etiSA = [];
  varMultiple = Number(1.1);
  sumOfLivingBenefit = Number(0);
  sumOfTaxBenefit = Number(0);
  sumOfPremium = Number(0);
  sumOfGrandBenefit = Number(0);
  sumOfRatePercent = Number(0);
  i = 0;
  while (i < sub(Number(80), inoutMap.InsuredDetails.age)) {
    benefitYear[i] = sum(i, 1);
    benefitAge[i] = sum(inoutMap.InsuredDetails.age, i, 1);
    if (benefitAge[i] <= Number(80)) {
      benefitPremium[i] = formatDecimal(Number(inoutMap.annualisedPremium), 0);
    } else {
      benefitPremium[i] = Number(0);
    }
    benefitCashRate[i] = formatDecimal(Number(inoutMap.MBRate[i + 1]), 2) + '%';
    benefitCashAmount[i] = formatDecimal(div(inoutMap.MBRate[i + 1], 100), 4);
    benefitCashAmount[i] = formatDecimal(mul(benefitCashAmount[i], inoutMap.PolicyDetails.sumAssured), 0);
    benefitLifeRate[i] = formatDecimal(inoutMap.DBRate[i], 2) + '%';
    benefitLifeAmount[i] = formatDecimal(mul(div(inoutMap.DBRate[i], 100), inoutMap.PolicyDetails.sumAssured), 0);
    cashValue[i] = formatDecimal(div(mul(inoutMap.TCVXRate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    rpuCash[i] = formatDecimal(div(mul(inoutMap.RPUCashRate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    rpuSA[i] = formatDecimal(div(mul(inoutMap.RPURate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    extPeriodYear[i] = inoutMap.ETIYR[i + 1];
    extPeriodDay[i] = inoutMap.ETIDAY[i + 1];
    etiCash[i] = formatDecimal(div(mul(inoutMap.ETICashRate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    etiSA[i] = formatDecimal(div(mul(inoutMap.ETISARate[i + 1], inoutMap.PolicyDetails.sumAssured), 1000), 0);
    benefitTaxAmount[i] = formatDecimal(mul(div(inoutMap.PolicyDetails.taxRate, 100), benefitPremium[i]), 0);
    sumOfLivingBenefit = formatDecimal(sum(sumOfLivingBenefit, benefitCashAmount[i]), 2);
    sumOfTaxBenefit = formatDecimal(sum(sumOfTaxBenefit, benefitTaxAmount[i]), 2);
    sumOfPremium = formatDecimal(sum(sumOfPremium, benefitPremium[i]), 2);
    sumOfRatePercent = formatDecimal(sum(sumOfRatePercent, benefitCashRate[i]), 2) + '%';
    sumOfRatePercentWithoutPer = formatDecimal(sum(sumOfRatePercent, benefitCashRate[i]), 2);
    i++;
  }
  totalBenefit = formatDecimal(sub(sum(sumOfLivingBenefit, sumOfTaxBenefit), sumOfPremium), 2);
  updateJSON(inoutMap, 'sumOfLivingBenefit', sumOfLivingBenefit);
  updateJSON(inoutMap, 'sumOfTaxBenefit', sumOfTaxBenefit);
  updateJSON(inoutMap, 'sumOfPremium', sumOfPremium);
  updateJSON(inoutMap, 'sumOfGrandBenefit', sumOfGrandBenefit);
  updateJSON(inoutMap, 'sumOfRatePercent', sumOfRatePercent);
  updateJSON(inoutMap, 'totalBenefit', totalBenefit);
  updateJSON(inoutMap, 'sumOfRatePercentWithoutPer', sumOfRatePercentWithoutPer);
  updateJSON(inoutMap, 'benefitYear', benefitYear);
  updateJSON(inoutMap, 'benefitAge', benefitAge);
  updateJSON(inoutMap, 'benefitPremium', benefitPremium);
  updateJSON(inoutMap, 'benefitTaxAmount', benefitTaxAmount);
  updateJSON(inoutMap, 'benefitTaxDeduct', benefitTaxDeduct);
  updateJSON(inoutMap, 'benefitCashRate', benefitCashRate);
  updateJSON(inoutMap, 'benefitCashAmount', benefitCashAmount);
  updateJSON(inoutMap, 'benefitLifeRate', benefitLifeRate);
  updateJSON(inoutMap, 'benefitLifeAmount', benefitLifeAmount);
  updateJSON(inoutMap, 'cashValue', cashValue);
  updateJSON(inoutMap, 'rpuCash', rpuCash);
  updateJSON(inoutMap, 'rpuSA', rpuSA);
  updateJSON(inoutMap, 'extPeriodYear', extPeriodYear);
  updateJSON(inoutMap, 'extPeriodDay', extPeriodDay);
  updateJSON(inoutMap, 'etiCash', etiCash);
  updateJSON(inoutMap, 'etiSA', etiSA);
  successCB(inoutMap);
}
function Gen_Hlth_Rule_Set(inoutMap, successCB) {
  processRuleSet(inoutMap, [
    'Gen_Hlth_Premium_02',
    'Gen_Hlth_BFTable_CVTable_03',
    successCB
  ]);
}
function OccupationaLRuleCIOccCode(inoutMap, successCB) {
  var CIOccCode1, CIOccCode2, CIOccCode1A, CIOccCode2A, err;
  CIOccCode1A = Number(0);
  CIOccCode1 = '';
  CIOccCode2A = Number(0);
  CIOccCode2 = '';
  CIOccCode1 = 0;
  lookupAsJson('CI_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param46, _$param47) {
    CIOccCode1 = _$param46;
    err = _$param47;
    CIOccCode2 = 0;
    lookupAsJson('CI_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param48, _$param49) {
      CIOccCode2 = _$param48;
      err = _$param49;
      if (CIOccCode1 == 'D') {
        CIOccCode1A = Number(10);
      }
      if (CIOccCode1 == 'IC') {
        CIOccCode1A = Number(5);
      }
      if (CIOccCode1 == '4') {
        CIOccCode1A = Number(4);
      }
      if (CIOccCode1 == '3') {
        CIOccCode1A = Number(3);
      }
      if (CIOccCode1 == '2') {
        CIOccCode1A = Number(2);
      }
      if (CIOccCode1 == '1') {
        CIOccCode1A = Number(1);
      }
      if (CIOccCode2 == 'D') {
        CIOccCode2A = Number(10);
      }
      if (CIOccCode2 == 'IC') {
        CIOccCode2A = Number(5);
      }
      if (CIOccCode2 == '4') {
        CIOccCode2A = Number(4);
      }
      if (CIOccCode2 == '3') {
        CIOccCode2A = Number(3);
      }
      if (CIOccCode2 == '2') {
        CIOccCode2A = Number(2);
      }
      if (CIOccCode2 == '1') {
        CIOccCode2A = Number(1);
      }
      updateJSON(inoutMap, 'CIOccCode1', CIOccCode1);
      updateJSON(inoutMap, 'CIOccCode2', CIOccCode2);
      updateJSON(inoutMap, 'CIOccCode1A', CIOccCode1A);
      updateJSON(inoutMap, 'CIOccCode2A', CIOccCode2A);
      successCB(inoutMap);
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function Gen_Hlth_Validations(inoutMap, successCB) {
  var isSuccess, ValidationResults, minSAPackA, err, maxAge, minADDRiderSAPackA, minCIRiderSAPackA, minHBRiderSAPackA, minHSRiderSAPackA, OPDSA1000, OPDSA1500, minSAPackB, minADDRiderSAPackB, minCIRiderSAPackB, minHBRiderSAPackB, minHSRiderSAPackB, inoutMap;
  ValidationResults = [];
  ValidationResults = {};
  isSuccess = Boolean(true);
  if (inoutMap.PolicyDetails.packageName == 'A' || inoutMap.PolicyDetails.packageName == 'B' || inoutMap.PolicyDetails.packageName == 'C' || inoutMap.PolicyDetails.packageName == 'D') {
    if (inoutMap.CIRider.isRequired == 'No') {
      ValidationResults.GenHlth_01 = 'CIRider is not selected';
      isSuccess = Boolean(false);
    }
    if (inoutMap.ADDRider.isRequired == 'No') {
      ValidationResults.GenHlth_02 = 'ADDRider is not selected';
      isSuccess = Boolean(false);
    }
    if (inoutMap.HSRider.isRequired == 'No') {
      ValidationResults.GenHlth_04 = 'HSRider is not selected';
      isSuccess = Boolean(false);
    }
    if (inoutMap.PolicyDetails.packageName == "C" || inoutMap.PolicyDetails.packageName == "D") {
		if (inoutMap.OPDRiders.isRequired == "No") {
			ValidationResults.GenHlth_135 = "OPDRider is not selected";
			isSuccess = Boolean(false);
		} else if (inoutMap.OPDRiders.isRequired == "Yes" && Number(inoutMap.PolicyDetails.premiumMode) != Number(6001)) {
			ValidationResults.GenHlth_140 = "Only Annual Payment is available for OPD Rider";
            isSuccess = Boolean(false);
        }
	}
  }
  if (inoutMap.PolicyDetails.packageName == 'A' && inoutMap.InsuredDetails.age <= Number(5)) {
    if (inoutMap.HBRider.isRequired == 'Yes') {
      ValidationResults.GenHlth_25 = 'HBRider is not allowed';
      isSuccess = Boolean(false);
    }
  }
  if ((inoutMap.PolicyDetails.packageName == 'A' || inoutMap.PolicyDetails.packageName == 'B') && inoutMap.InsuredDetails.age >= Number(6)) {
    if (inoutMap.HBRider.isRequired == 'No') {
      ValidationResults.GenHlth_03 = 'HBRider is not selected';
      isSuccess = Boolean(false);
    }
  }
  minSAPackA = 0;
  lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211049', function (arguments, _$param50, _$param51) {
    minSAPackA = _$param50;
    err = _$param51;
    maxAge = 0;
    lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211048', function (arguments, _$param52, _$param53) {
      maxAge = _$param52;
      err = _$param53;
      minADDRiderSAPackA = 0;
      lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211049', function (arguments, _$param54, _$param55) {
        minADDRiderSAPackA = _$param54;
        err = _$param55;
        minCIRiderSAPackA = 0;
        lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211049', function (arguments, _$param56, _$param57) {
          minCIRiderSAPackA = _$param56;
          err = _$param57;
          minHBRiderSAPackA = 0;
          lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211050', function (arguments, _$param58, _$param59) {
            minHBRiderSAPackA = _$param58;
            err = _$param59;
            minHSRiderSAPackA = 0;
            lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211050', function (arguments, _$param60, _$param61) {
              minHSRiderSAPackA = _$param60;
              err = _$param61;
              OPDSA1000 = 0;
              lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1911043', function (arguments, _$param62, _$param63) {
                OPDSA1000 = _$param62;
                err = _$param63;
                OPDSA1500 = 0;
                lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1911042', function (arguments, _$param64, _$param65) {
                  OPDSA1500 = _$param64;
                  err = _$param65;
                  minSAPackB = 0;
                  lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211051', function (arguments, _$param66, _$param67) {
                    minSAPackB = _$param66;
                    err = _$param67;
                    minADDRiderSAPackB = 0;
                    lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211051', function (arguments, _$param68, _$param69) {
                      minADDRiderSAPackB = _$param68;
                      err = _$param69;
                      minCIRiderSAPackB = 0;
                      lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211051', function (arguments, _$param70, _$param71) {
                        minCIRiderSAPackB = _$param70;
                        err = _$param71;
                        minHBRiderSAPackB = 0;
                        lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211052', function (arguments, _$param72, _$param73) {
                          minHBRiderSAPackB = _$param72;
                          err = _$param73;
                          minHSRiderSAPackB = 0;
                          lookupAsJson('DEFAULT_VALUE', 'CORE_PROD_PROPERTIES', 'PRODUCT_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'propertyType_ID=1211052', function (arguments, _$param74, _$param75) {
                            minHSRiderSAPackB = _$param74;
                            err = _$param75;
                            if (Number(inoutMap.InsuredDetails.age) > Number(maxAge)) {
                              ValidationResults.GenHlth_05 = 'Maximum age  is # ' + Number(maxAge);
                              isSuccess = Boolean(false);
                            }
                            if (inoutMap.PolicyDetails.packageName == 'A' && Number(inoutMap.PolicyDetails.sumAssured) != Number(minSAPackA)) {
                              ValidationResults.GenHlth_06 = 'Base Sum Assured for Package A is # ' + Number(minSAPackA);
                              isSuccess = Boolean(false);
                            }
                            if (inoutMap.PolicyDetails.packageName == 'B' && Number(inoutMap.PolicyDetails.sumAssured) != Number(minSAPackB)) {
                              ValidationResults.GenHlth_07 = 'Base Sum Assured for Package B is # ' + Number(minSAPackB);
                              isSuccess = Boolean(false);
                            }
                            if (inoutMap.CIRider.isRequired == 'Yes') {
                              if (inoutMap.PolicyDetails.packageName == 'A' && Number(inoutMap.CIRider.sumAssured) != Number(minCIRiderSAPackA)) {
                                ValidationResults.GenHlth_08 = 'Minimum CIRider Sum Assured for Package A is # ' + Number(minCIRiderSAPackA);
                                isSuccess = Boolean(false);
                              } else {
                                if (inoutMap.PolicyDetails.packageName == 'B' && Number(inoutMap.CIRider.sumAssured) != Number(minCIRiderSAPackB)) {
                                  ValidationResults.GenHlth_09 = 'Minimum CIRider Sum Assured for Package B is # ' + Number(minCIRiderSAPackB);
                                  isSuccess = Boolean(false);
                                }
                              }
                            }
                            if (inoutMap.ADDRider.isRequired == 'Yes') {
                              if (inoutMap.PolicyDetails.packageName == 'A' && Number(inoutMap.ADDRider.sumAssured) != Number(minADDRiderSAPackA)) {
                                ValidationResults.GenHlth_10 = 'Minimum ADDRider Sum Assured for Package A is # ' + Number(minADDRiderSAPackA);
                                isSuccess = Boolean(false);
                              }
                              if (inoutMap.PolicyDetails.packageName == 'B' && Number(inoutMap.ADDRider.sumAssured) != Number(minADDRiderSAPackB)) {
                                ValidationResults.GenHlth_11 = 'Minimum ADDRider Sum Assured for Package B is # ' + Number(minADDRiderSAPackB);
                                isSuccess = Boolean(false);
                              }
                            }
                            if (inoutMap.HBRider.isRequired == 'Yes') {
                              if (inoutMap.PolicyDetails.packageName == 'A' && Number(inoutMap.HBRider.sumAssured) != Number(minHBRiderSAPackA) && Number(inoutMap.InsuredDetails.age) >= Number(6)) {
                                ValidationResults.GenHlth_12 = 'Minimum HBRider Sum Assured for Package A is # ' + Number(minHBRiderSAPackA);
                                isSuccess = Boolean(false);
                              }
                              if (inoutMap.PolicyDetails.packageName == 'B' && Number(inoutMap.HBRider.sumAssured) != Number(minHBRiderSAPackB)) {
                                ValidationResults.GenHlth_13 = 'Minimum HBRider Sum Assured for Package B is # ' + Number(minHBRiderSAPackB);
                                isSuccess = Boolean(false);
                              }
                            }
                            if (inoutMap.HSRider.isRequired == 'Yes') {
                              if (inoutMap.PolicyDetails.packageName == 'A' && Number(inoutMap.HSRider.sumAssured) != Number(minHSRiderSAPackA)) {
                                ValidationResults.GenHlth_14 = 'Minimum HSRider Sum Assured for Package A is # ' + Number(minHSRiderSAPackA);
                                isSuccess = Boolean(false);
                              }
                              if (inoutMap.PolicyDetails.packageName == 'B' && Number(inoutMap.HSRider.sumAssured) != Number(minHSRiderSAPackB)) {
                                ValidationResults.GenHlth_15 = 'Minimum HSRider Sum Assured for Package B is # ' + Number(minHSRiderSAPackB);
                                isSuccess = Boolean(false);
                              }
                            }
                            if (inoutMap.OPDRiders.isRequired == 'Yes') {
                              if (Number(inoutMap.InsuredDetails.age) <= Number(10) && Number(inoutMap.OPDRiders.sumAssured) > Number(OPDSA1000)) {
                                ValidationResults.GenHlth_136 = 'Maximum OPD rider SA for the insured age is # ' + Number(OPDSA1000);
                                isSuccess = Boolean(false);
                              }
                              if (Number(inoutMap.InsuredDetails.age) > Number(10) && Number(inoutMap.InsuredDetails.age) <= Number(15) && Number(inoutMap.OPDRiders.sumAssured) > Number(OPDSA1000)) {
                                ValidationResults.GenHlth_137 = 'Maximum OPD rider SA for the insured age is # ' + Number(OPDSA1000);
                                isSuccess = Boolean(false);
                              }
                              if (Number(inoutMap.InsuredDetails.age) > Number(15) && Number(inoutMap.InsuredDetails.age) <= Number(64) && Number(inoutMap.HSRider.sumAssured) < Number(5000) && Number(inoutMap.OPDRiders.sumAssured) > Number(OPDSA1000)) {
                                ValidationResults.GenHlth_138 = 'Maximum OPD rider SA for the insured age and selected HS SA is # ' + Number(OPDSA1000);
                                isSuccess = Boolean(false);
                              }
                              if (Number(inoutMap.InsuredDetails.age) > Number(15) && Number(inoutMap.InsuredDetails.age) <= Number(64) && Number(inoutMap.HSRider.sumAssured) >= Number(5000) && Number(inoutMap.OPDRiders.sumAssured) > Number(OPDSA1500)) {
                                ValidationResults.GenHlth_139 = 'Maximum OPD rider SA for the insured age and selected HS SA is # ' + Number(OPDSA1500);
                                isSuccess = Boolean(false);
                              }
                            }
                            if (inoutMap.PolicyDetails.packageName != 'A' && inoutMap.PolicyDetails.packageName != 'B' && inoutMap.PolicyDetails.packageName != 'C' && inoutMap.PolicyDetails.packageName != 'D') {
                              ValidationResults.GenHlth_16 = 'Please select the Package A or Package B or Package C or Package D';
                              isSuccess = Boolean(false);
                            }
                            if (inoutMap.PolicyDetails.occCode1 <= '') {
                              ValidationResults.GenHlth_17 = 'Please select Insured Occupational Code 1';
                              isSuccess = Boolean(false);
                            }
                            OccupationaLRuleUpdated1(inoutMap, function (arguments, _$param76, _$param77) {
                              inoutMap = _$param76;
                              err = _$param77;
                              if (inoutMap.BaseOccCode1 <= '') {
                                ValidationResults.GenHlth_19 = 'Please select valid Insured Occupational Code 1';
                                isSuccess = Boolean(false);
                              }
                              (function (_$cont) {
                                if (isSuccess == Boolean(true)) {
                                  OccupationaLRuleUpdated1(inoutMap, function (arguments, _$param78, _$param79) {
                                    inoutMap = _$param78;
                                    err = _$param79;
                                    _$cont();
                                  }.bind(this, arguments));
                                } else {
                                  _$cont();
                                }
                              }.bind(this)(function (_$err) {
                                if (_$err !== undefined)
                                  return _$cont(_$err);
                                if (inoutMap.BaseOccCode == 'D') {
                                  ValidationResults.GenHlth_20 = 'Product is declined for this Occupation';
                                  isSuccess = Boolean(false);
                                }
                                if (inoutMap.CIOccCode == 'D') {
                                  ValidationResults.GenHlth_21 = 'Rider CI is declined for this Occupation';
                                  isSuccess = Boolean(false);
                                }
                                if (inoutMap.HBOccCode == 'D' && Number(inoutMap.InsuredDetails.age) >= Number(6)) {
                                  ValidationResults.GenHlth_22 = 'Rider HB is declined for this Occupation';
                                  isSuccess = Boolean(false);
                                }
                                if (inoutMap.HSOccCode == 'D') {
                                  ValidationResults.GenHlth_23 = 'Rider HS is declined for this Occupation';
                                  isSuccess = Boolean(false);
                                }
                                if (inoutMap.ADDOccCode == 'D') {
                                  ValidationResults.GenHlth_24 = 'Rider ADD is declined for this Occupation';
                                  isSuccess = Boolean(false);
                                }
                                (function (_$cont) {
                                  if (isSuccess == Boolean(true)) {
                                    Gen_Hlth_Illustrations(inoutMap, function (arguments, _$param80, _$param81) {
                                      inoutMap = _$param80;
                                      err = _$param81;
                                      _$cont();
                                    }.bind(this, arguments));
                                  } else {
                                    _$cont();
                                  }
                                }.bind(this)(function (_$err) {
                                  if (_$err !== undefined)
                                    return _$cont(_$err);
                                  updateJSON(inoutMap, 'isSuccess', isSuccess);
                                  updateJSON(inoutMap, 'ValidationResults', ValidationResults);
                                  successCB(inoutMap);
                                }.bind(this)));
                              }.bind(this)));
                            }.bind(this, arguments));
                          }.bind(this, arguments));
                        }.bind(this, arguments));
                      }.bind(this, arguments));
                    }.bind(this, arguments));
                  }.bind(this, arguments));
                }.bind(this, arguments));
              }.bind(this, arguments));
            }.bind(this, arguments));
          }.bind(this, arguments));
        }.bind(this, arguments));
      }.bind(this, arguments));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function Gen_Hlth_Illustrations(inoutMap, successCB) {
  var protectionPeriodFinal, premiumPeriodFinal, sumInsuredFinal, basePremiumFinal, riderPremiumFinal, WPSumAssuredCover, ADBSumAssuredCover, ADBRCCSumAssuredCover, ADDSumAssuredCover, ADDRCCSumAssuredCover, CISumAssuredCover, AIRCCSumAssuredCover, PBSumAssuredCover, DDSumAssuredCover, HBSumAssuredCover, HSSumAssuredCover, OPDSumAssuredCover, PremiumWPCover, ADBPremiumCover, ADBRCCPremiumCover, ADDPremiumCover, ADDRCCPremiumCover, CIPremiumCover, AIRCCPremiumCover, PBPremiumCover, DDPremiumCover, HSPremiumCover, OPDPremiumCover, HBPremiumCover, totalPremiumFinal, totalLivingBenefit, totalTaxationBenefit, totalPremiumPaid, grandTotalBenefit, totalRatePercent, BaseOccCodeCover, CIOccCodeCover, DDOccCodeCover, WPOccCodeCover, PBOccCodeCover, AIOccCodeCover, HBOccCodeCover, HSOccCodeCover, OPDOccCodeCover, ADDOccCodeCover, ADBOccCodeCover, benefitTableData, illustrationTableData, i, inoutMap, err;
  benefitTableData = [];
  illustrationTableData = [];
  Gen_Hlth_Rule_Set(inoutMap, function (arguments, _$param82, _$param83) {
    inoutMap = _$param82;
    err = _$param83;
    if (inoutMap.PolicyDetails.packageName == 'A') {
      protectionPeriodFinal = sub(Number(80), inoutMap.InsuredDetails.age);
      premiumPeriodFinal = sub(Number(80), inoutMap.InsuredDetails.age);
    } else {
      protectionPeriodFinal = sub(Number(80), inoutMap.InsuredDetails.age);
      premiumPeriodFinal = sub(Number(80), inoutMap.InsuredDetails.age);
    }
    sumInsuredFinal = inoutMap.PolicyDetails.sumAssured;
    basePremiumFinal = inoutMap.modalPremium;
    riderPremiumFinal = inoutMap.totalRiderPremium;
    WPSumAssuredCover = Number(0);
    ADBSumAssuredCover = Number(0);
    ADBRCCSumAssuredCover = Number(0);
    ADDSumAssuredCover = inoutMap.ADDRider.sumAssured;
    ADDRCCSumAssuredCover = Number(0);
    CISumAssuredCover = inoutMap.CIRider.sumAssured;
    AIRCCSumAssuredCover = Number(0);
    PBSumAssuredCover = Number(0);
    DDSumAssuredCover = Number(0);
    HBSumAssuredCover = inoutMap.HBRider.sumAssured;
    HSSumAssuredCover = inoutMap.HSRider.sumAssured;
    OPDSumAssuredCover = inoutMap.OPDRiders.sumAssured;
    PremiumWPCover = Number(0);
    ADBPremiumCover = Number(0);
    ADBRCCPremiumCover = Number(0);
    ADDPremiumCover = inoutMap.ADDPremium;
    ADDRCCPremiumCover = Number(0);
    CIPremiumCover = inoutMap.CIPremium;
    AIRCCPremiumCover = Number(0);
    PBPremiumCover = Number(0);
    DDPremiumCover = Number(0);
    HSPremiumCover = inoutMap.HSPremium;
    OPDPremiumCover = inoutMap.OPDPremium;
    HBPremiumCover = inoutMap.HBPremium;
    totalPremiumFinal = inoutMap.totalPolicyPremium;
    BaseOccCodeCover = inoutMap.BaseOccCode;
    CIOccCodeCover = inoutMap.CIOccCode;
    DDOccCodeCover = inoutMap.DDOccCode;
    WPOccCodeCover = inoutMap.WPOccCode;
    PBOccCodeCover = inoutMap.PBOccCode;
    AIOccCodeCover = inoutMap.AIOccCode;
    HBOccCodeCover = inoutMap.HBOccCode;
    HSOccCodeCover = inoutMap.HSOccCode;
    OPDOccCodeCover = HSOccCodeCover;
    ADDOccCodeCover = inoutMap.ADDOccCode;
    ADBOccCodeCover = inoutMap.ADBOccCode;
    i = 0;
    while (i <= sub(Number(80), inoutMap.InsuredDetails.age, 1)) {
      rowData = {};
      rowData.policyYear = inoutMap.benefitYear[i];
      rowData.age = inoutMap.benefitAge[i];
      rowData.cashValue = inoutMap.cashValue[i];
      rowData.rpuCash = inoutMap.rpuCash[i];
      rowData.rpuSA = inoutMap.rpuSA[i];
      rowData.extPeriodYear = inoutMap.extPeriodYear[i];
      rowData.extPeriodDay = inoutMap.extPeriodDay[i];
      rowData.etiCash = inoutMap.etiCash[i];
      rowData.etiSA = inoutMap.etiSA[i];
      illustrationTableData[i] = rowData;
      i = i + 1;
    }
    i = 0;
    while (i <= sub(Number(80), inoutMap.InsuredDetails.age)) {
      rowBenefitData = {};
      if (i < sub(Number(80), inoutMap.InsuredDetails.age)) {
        rowBenefitData.policyYear = inoutMap.benefitYear[i];
        rowBenefitData.age = inoutMap.benefitAge[i];
        rowBenefitData.annualisedPremium = inoutMap.benefitPremium[i];
        rowBenefitData.benefitRate = inoutMap.benefitCashRate[i];
        rowBenefitData.benefitAmount = inoutMap.benefitCashAmount[i];
        rowBenefitData.benefitTaxAmount = inoutMap.benefitTaxAmount[i];
        rowBenefitData.benefitLifeRate = inoutMap.benefitLifeRate[i];
        rowBenefitData.benefitLifeAmount = inoutMap.benefitLifeAmount[i];
      } else {
        rowBenefitData.policyYear = 'Included';
        rowBenefitData.age = '';
        rowBenefitData.annualisedPremium = inoutMap.sumOfPremium;
        rowBenefitData.benefitRate = inoutMap.sumOfRatePercent;
        rowBenefitData.benefitAmount = inoutMap.sumOfLivingBenefit;
        rowBenefitData.benefitTaxAmount = inoutMap.sumOfTaxBenefit;
        rowBenefitData.benefitLifeRate = '';
        rowBenefitData.benefitLifeAmount = '';
      }
      benefitTableData[i] = rowBenefitData;
      i = i + 1;
    }
    totalLivingBenefit = inoutMap.sumOfLivingBenefit;
    totalTaxationBenefit = inoutMap.sumOfTaxBenefit;
    totalPremiumPaid = inoutMap.sumOfPremium;
    grandTotalBenefit = inoutMap.totalBenefit;
    totalRatePercent = inoutMap.sumOfRatePercent;
    Gen_Hlth_BIGraph(inoutMap, function (arguments, _$param84, _$param85) {
      inoutMap = _$param84;
      err = _$param85;
      updateJSON(inoutMap, 'protectionPeriodFinal', protectionPeriodFinal);
      updateJSON(inoutMap, 'premiumPeriodFinal', premiumPeriodFinal);
      updateJSON(inoutMap, 'sumInsuredFinal', sumInsuredFinal);
      updateJSON(inoutMap, 'basePremiumFinal', basePremiumFinal);
      updateJSON(inoutMap, 'riderPremiumFinal', riderPremiumFinal);
      updateJSON(inoutMap, 'WPSumAssuredCover', WPSumAssuredCover);
      updateJSON(inoutMap, 'ADBSumAssuredCover', ADBSumAssuredCover);
      updateJSON(inoutMap, 'ADBRCCSumAssuredCover', ADBRCCSumAssuredCover);
      updateJSON(inoutMap, 'ADDSumAssuredCover', ADDSumAssuredCover);
      updateJSON(inoutMap, 'ADDRCCSumAssuredCover', ADDRCCSumAssuredCover);
      updateJSON(inoutMap, 'CISumAssuredCover', CISumAssuredCover);
      updateJSON(inoutMap, 'AIRCCSumAssuredCover', AIRCCSumAssuredCover);
      updateJSON(inoutMap, 'PBSumAssuredCover', PBSumAssuredCover);
      updateJSON(inoutMap, 'DDSumAssuredCover', DDSumAssuredCover);
      updateJSON(inoutMap, 'HBSumAssuredCover', HBSumAssuredCover);
      updateJSON(inoutMap, 'HSSumAssuredCover', HSSumAssuredCover);
      updateJSON(inoutMap, 'OPDSumAssuredCover', OPDSumAssuredCover);
      updateJSON(inoutMap, 'PremiumWPCover', PremiumWPCover);
      updateJSON(inoutMap, 'ADBPremiumCover', ADBPremiumCover);
      updateJSON(inoutMap, 'ADBRCCPremiumCover', ADBRCCPremiumCover);
      updateJSON(inoutMap, 'ADDPremiumCover', ADDPremiumCover);
      updateJSON(inoutMap, 'ADDRCCPremiumCover', ADDRCCPremiumCover);
      updateJSON(inoutMap, 'CIPremiumCover', CIPremiumCover);
      updateJSON(inoutMap, 'AIRCCPremiumCover', AIRCCPremiumCover);
      updateJSON(inoutMap, 'PBPremiumCover', PBPremiumCover);
      updateJSON(inoutMap, 'DDPremiumCover', DDPremiumCover);
      updateJSON(inoutMap, 'HSPremiumCover', HSPremiumCover);
      updateJSON(inoutMap, 'OPDPremiumCover', OPDPremiumCover);
      updateJSON(inoutMap, 'HBPremiumCover', HBPremiumCover);
      updateJSON(inoutMap, 'totalPremiumFinal', totalPremiumFinal);
      updateJSON(inoutMap, 'totalLivingBenefit', totalLivingBenefit);
      updateJSON(inoutMap, 'totalTaxationBenefit', totalTaxationBenefit);
      updateJSON(inoutMap, 'totalPremiumPaid', totalPremiumPaid);
      updateJSON(inoutMap, 'grandTotalBenefit', grandTotalBenefit);
      updateJSON(inoutMap, 'totalRatePercent', totalRatePercent);
      updateJSON(inoutMap, 'BaseOccCodeCover', BaseOccCodeCover);
      updateJSON(inoutMap, 'CIOccCodeCover', CIOccCodeCover);
      updateJSON(inoutMap, 'DDOccCodeCover', DDOccCodeCover);
      updateJSON(inoutMap, 'WPOccCodeCover', WPOccCodeCover);
      updateJSON(inoutMap, 'PBOccCodeCover', PBOccCodeCover);
      updateJSON(inoutMap, 'AIOccCodeCover', AIOccCodeCover);
      updateJSON(inoutMap, 'HBOccCodeCover', HBOccCodeCover);
      updateJSON(inoutMap, 'HSOccCodeCover', HSOccCodeCover);
      updateJSON(inoutMap, 'OPDOccCodeCover', OPDOccCodeCover);
      updateJSON(inoutMap, 'ADDOccCodeCover', ADDOccCodeCover);
      updateJSON(inoutMap, 'ADBOccCodeCover', ADBOccCodeCover);
      updateJSON(inoutMap, 'benefitTableData', benefitTableData);
      updateJSON(inoutMap, 'illustrationTableData', illustrationTableData);
      successCB(inoutMap);
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function OccupationaLRuleUpdated1(inoutMap, successCB) {
  var occupationalCode, BaseOccCode, CIOccCode, DDOccCode, WPOccCode, PBOccCode, AIOccCode, HBOccCode, HSOccCode, ADDOccCode, ADBOccCode, OccCode1D, OccCode2D, OccCode1IC, OccCode2IC, SumOccCode1, SumOccCode2, Option, OccCode1, OccCode2, inoutMap, err;
  OccCode1 = [];
  OccCode2 = [];
  OccupationaLRuleCIOccCode(inoutMap, function (arguments, _$param86, _$param87) {
    inoutMap = _$param86;
    err = _$param87;
    OccupationaLRuleHBOccCode(inoutMap, function (arguments, _$param88, _$param89) {
      inoutMap = _$param88;
      err = _$param89;
      OccupationaLRuleHSOccCode(inoutMap, function (arguments, _$param90, _$param91) {
        inoutMap = _$param90;
        err = _$param91;
        OccupationaLRuleADDCode(inoutMap, function (arguments, _$param92, _$param93) {
          inoutMap = _$param92;
          err = _$param93;
          OccupationaLRuleBaseCode(inoutMap, function (arguments, _$param94, _$param95) {
            inoutMap = _$param94;
            err = _$param95;
            SumOccCode1 = Number(0);
            SumOccCode2 = Number(0);
            OccCode1D = Number(0);
            OccCode2D = Number(0);
            OccCode1IC = Number(0);
            OccCode2IC = Number(0);
            Option = '';
            inoutMap.ADBOccCode1A = Number(0);
            inoutMap.ADBOccCode2A = Number(0);
            inoutMap.AIOccCode1A = Number(0);
            inoutMap.AIOccCode2A = Number(0);
            inoutMap.PBOccCode1A = Number(0);
            inoutMap.PBOccCode2A = Number(0);
            inoutMap.WPOccCode1A = Number(0);
            inoutMap.WPOccCode2A = Number(0);
            inoutMap.DDOccCode1A = Number(0);
            inoutMap.DDOccCode2A = Number(0);
            if (inoutMap.CIRider.isRequired == 'No') {
              inoutMap.CIOccCode1A = Number(0);
              inoutMap.CIOccCode2A = Number(0);
            }
            if (inoutMap.HBRider.isRequired == 'No') {
              inoutMap.HBOccCode1A = Number(0);
              inoutMap.HBOccCode2A = Number(0);
            }
            if (inoutMap.HSRider.isRequired == 'No') {
              inoutMap.HSOccCode1A = Number(0);
              inoutMap.HSOccCode2A = Number(0);
            }
            if (inoutMap.ADDRider.isRequired == 'No') {
              inoutMap.ADDOccCode1A = Number(0);
              inoutMap.ADDOccCode2A = Number(0);
            }
            if (inoutMap.PolicyDetails.occCode1 <= '') {
              inoutMap.CIOccCode1A = Number(0);
              inoutMap.HBOccCode1A = Number(0);
              inoutMap.HSOccCode1A = Number(0);
              inoutMap.ADDOccCode1A = Number(0);
            }
            if (inoutMap.PolicyDetails.occCode2 <= '') {
              inoutMap.CIOccCode2A = Number(0);
              inoutMap.HBOccCode2A = Number(0);
              inoutMap.HSOccCode2A = Number(0);
              inoutMap.ADDOccCode2A = Number(0);
            }
            SumOccCode2 = sum(inoutMap.BaseOccCode2A, inoutMap.CIOccCode2A, inoutMap.HBOccCode2A, inoutMap.HSOccCode2A, inoutMap.ADDOccCode2A, inoutMap.AIOccCode2A, inoutMap.WPOccCode2A, inoutMap.ADBOccCode2A, inoutMap.PBOccCode2A, inoutMap.DDOccCode2A);
            SumOccCode1 = sum(inoutMap.BaseOccCode1A, inoutMap.CIOccCode1A, inoutMap.HBOccCode1A, inoutMap.HSOccCode1A, inoutMap.ADDOccCode1A, inoutMap.AIOccCode1A, inoutMap.WPOccCode1A, inoutMap.ADBOccCode1A, inoutMap.PBOccCode1A, inoutMap.DDOccCode1A);
            OccCode1[1] = inoutMap.CIOccCode1A;
            OccCode1[2] = inoutMap.DDOccCode1A;
            OccCode1[3] = inoutMap.WPOccCode1A;
            OccCode1[4] = inoutMap.PBOccCode1A;
            OccCode1[5] = inoutMap.AIOccCode1A;
            OccCode1[6] = inoutMap.HBOccCode1A;
            OccCode1[7] = inoutMap.HSOccCode1A;
            OccCode1[8] = inoutMap.ADDOccCode1A;
            OccCode1[9] = inoutMap.ADBOccCode1A;
            OccCode2[1] = inoutMap.CIOccCode2A;
            OccCode2[2] = inoutMap.DDOccCode2A;
            OccCode2[3] = inoutMap.WPOccCode2A;
            OccCode2[4] = inoutMap.PBOccCode2A;
            OccCode2[5] = inoutMap.AIOccCode2A;
            OccCode2[6] = inoutMap.HBOccCode2A;
            OccCode2[7] = inoutMap.HSOccCode2A;
            OccCode2[8] = inoutMap.ADDOccCode2A;
            OccCode2[9] = inoutMap.ADBOccCode2A;
            if (inoutMap.BaseOccCode1A >= Number(10)) {
              Option = 'A';
            } else {
              if (inoutMap.BaseOccCode2A >= Number(10)) {
                Option = 'B';
              }
            }
            i = 1;
            while (i < 10) {
              if (OccCode1[i] == Number(10)) {
                OccCode1D = sum(OccCode1[i], OccCode1D);
              }
              if (OccCode2[i] == Number(10)) {
                OccCode2D = sum(OccCode2[i], OccCode2D);
              }
              if (OccCode1[i] == Number(5)) {
                OccCode1IC = sum(OccCode2[i], OccCode1IC);
              }
              if (OccCode2[i] == Number(5)) {
                OccCode2IC = sum(OccCode2[i], OccCode2IC);
              }
              i++;
            }
            if (Number(SumOccCode1) == Number(SumOccCode2) && OccCode1D == OccCode2D && OccCode1IC == OccCode2IC && Option >= '') {
              if (Number(inoutMap.BaseOccCode1A) >= Number(inoutMap.BaseOccCode2A)) {
                Option = 'A';
              } else {
                Option = 'B';
              }
            }
            if (Option >= '') {
              if (OccCode1D > OccCode2D) {
                Option = 'A';
              } else {
                if (OccCode1D < OccCode2D) {
                  Option = 'B';
                }
              }
            }
            if (Option >= '') {
              if (OccCode1IC > OccCode2IC) {
                Option = 'A';
              } else {
                if (OccCode1IC < OccCode2IC) {
                  Option = 'B';
                }
              }
            }
            if (Option >= '') {
              if (Number(SumOccCode1) > Number(SumOccCode2)) {
                Option = 'A';
              } else {
                if (Number(SumOccCode1) < Number(SumOccCode2)) {
                  Option = 'B';
                }
              }
            }
            if (Option == 'A') {
              BaseOccCode = inoutMap.BaseOccCode1;
              CIOccCode = inoutMap.CIOccCode1;
              DDOccCode = inoutMap.DDOccCode1;
              WPOccCode = inoutMap.WPOccCode1;
              PBOccCode = inoutMap.PBOccCode1;
              AIOccCode = inoutMap.AIOccCode1;
              HBOccCode = inoutMap.HBOccCode1;
              HSOccCode = inoutMap.HSOccCode1;
              ADDOccCode = inoutMap.ADDOccCode1;
              ADBOccCode = inoutMap.ADBOccCode1;
              occupationalCode = inoutMap.PolicyDetails.occCode1;
            } else {
              BaseOccCode = inoutMap.BaseOccCode2;
              CIOccCode = inoutMap.CIOccCode2;
              DDOccCode = inoutMap.DDOccCode2;
              WPOccCode = inoutMap.WPOccCode2;
              PBOccCode = inoutMap.PBOccCode2;
              AIOccCode = inoutMap.AIOccCode2;
              HBOccCode = inoutMap.HBOccCode2;
              HSOccCode = inoutMap.HSOccCode2;
              ADDOccCode = inoutMap.ADDOccCode2;
              ADBOccCode = inoutMap.ADBOccCode2;
              occupationalCode = inoutMap.PolicyDetails.occCode2;
            }
            updateJSON(inoutMap, 'occupationalCode', occupationalCode);
            updateJSON(inoutMap, 'BaseOccCode', BaseOccCode);
            updateJSON(inoutMap, 'CIOccCode', CIOccCode);
            updateJSON(inoutMap, 'DDOccCode', DDOccCode);
            updateJSON(inoutMap, 'WPOccCode', WPOccCode);
            updateJSON(inoutMap, 'PBOccCode', PBOccCode);
            updateJSON(inoutMap, 'AIOccCode', AIOccCode);
            updateJSON(inoutMap, 'HBOccCode', HBOccCode);
            updateJSON(inoutMap, 'HSOccCode', HSOccCode);
            updateJSON(inoutMap, 'ADDOccCode', ADDOccCode);
            updateJSON(inoutMap, 'ADBOccCode', ADBOccCode);
            updateJSON(inoutMap, 'OccCode1D', OccCode1D);
            updateJSON(inoutMap, 'OccCode2D', OccCode2D);
            updateJSON(inoutMap, 'OccCode1IC', OccCode1IC);
            updateJSON(inoutMap, 'OccCode2IC', OccCode2IC);
            updateJSON(inoutMap, 'SumOccCode1', SumOccCode1);
            updateJSON(inoutMap, 'SumOccCode2', SumOccCode2);
            updateJSON(inoutMap, 'Option', Option);
            updateJSON(inoutMap, 'OccCode1', OccCode1);
            updateJSON(inoutMap, 'OccCode2', OccCode2);
            successCB(inoutMap);
          }.bind(this, arguments));
        }.bind(this, arguments));
      }.bind(this, arguments));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function OccupationaLRuleHBOccCode(inoutMap, successCB) {
  var HBOccCode1, HBOccCode2, HBOccCode1A, HBOccCode2A, err;
  HBOccCode1 = 0;
  lookupAsJson('HB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param96, _$param97) {
    HBOccCode1 = _$param96;
    err = _$param97;
    HBOccCode2 = 0;
    lookupAsJson('HB_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param98, _$param99) {
      HBOccCode2 = _$param98;
      err = _$param99;
      if (HBOccCode1 == 'D') {
        HBOccCode1A = Number(10);
      }
      if (HBOccCode1 == 'IC') {
        HBOccCode1A = Number(5);
      }
      if (HBOccCode1 == '4') {
        HBOccCode1A = Number(4);
      }
      if (HBOccCode1 == '3') {
        HBOccCode1A = Number(3);
      }
      if (HBOccCode1 == '2') {
        HBOccCode1A = Number(2);
      }
      if (HBOccCode1 == '1') {
        HBOccCode1A = Number(1);
      }
      if (HBOccCode2 == 'D') {
        HBOccCode2A = Number(10);
      }
      if (HBOccCode2 == 'IC') {
        HBOccCode2A = Number(5);
      }
      if (HBOccCode2 == '4') {
        HBOccCode2A = Number(4);
      }
      if (HBOccCode2 == '3') {
        HBOccCode2A = Number(3);
      }
      if (HBOccCode2 == '2') {
        HBOccCode2A = Number(2);
      }
      if (HBOccCode2 == '1') {
        HBOccCode2A = Number(1);
      }
      updateJSON(inoutMap, 'HBOccCode1', HBOccCode1);
      updateJSON(inoutMap, 'HBOccCode2', HBOccCode2);
      updateJSON(inoutMap, 'HBOccCode1A', HBOccCode1A);
      updateJSON(inoutMap, 'HBOccCode2A', HBOccCode2A);
      successCB(inoutMap);
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function OccupationaLRuleHSOccCode(inoutMap, successCB) {
  var HSOccCode1, HSOccCode2, HSOccCode1A, HSOccCode2A, err;
  HSOccCode1 = 0;
  lookupAsJson('HS_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param100, _$param101) {
    HSOccCode1 = _$param100;
    err = _$param101;
    HSOccCode2 = 0;
    lookupAsJson('HS_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param102, _$param103) {
      HSOccCode2 = _$param102;
      err = _$param103;
      if (HSOccCode1 == 'D') {
        HSOccCode1A = Number(10);
      }
      if (HSOccCode1 == 'IC') {
        HSOccCode1A = Number(5);
      }
      if (HSOccCode1 == '4') {
        HSOccCode1A = Number(4);
      }
      if (HSOccCode1 == '3') {
        HSOccCode1A = Number(3);
      }
      if (HSOccCode1 == '2') {
        HSOccCode1A = Number(2);
      }
      if (HSOccCode1 == '1') {
        HSOccCode1A = Number(1);
      }
      if (HSOccCode2 == 'D') {
        HSOccCode2A = Number(10);
      }
      if (HSOccCode2 == 'IC') {
        HSOccCode2A = Number(5);
      }
      if (HSOccCode2 == '4') {
        HSOccCode2A = Number(4);
      }
      if (HSOccCode2 == '3') {
        HSOccCode2A = Number(3);
      }
      if (HSOccCode2 == '2') {
        HSOccCode2A = Number(2);
      }
      if (HSOccCode2 == '1') {
        HSOccCode2A = Number(1);
      }
      updateJSON(inoutMap, 'HSOccCode1', HSOccCode1);
      updateJSON(inoutMap, 'HSOccCode2', HSOccCode2);
      updateJSON(inoutMap, 'HSOccCode1A', HSOccCode1A);
      updateJSON(inoutMap, 'HSOccCode2A', HSOccCode2A);
      successCB(inoutMap);
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function OccupationaLRuleADDCode(inoutMap, successCB) {
  var ADDOccCode1, ADDOccCode2, ADDOccCode1A, ADDOccCode2A, err;
  ADDOccCode1 = 0;
  lookupAsJson('ADD_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param104, _$param105) {
    ADDOccCode1 = _$param104;
    err = _$param105;
    ADDOccCode2 = 0;
    lookupAsJson('ADD_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param106, _$param107) {
      ADDOccCode2 = _$param106;
      err = _$param107;
      if (ADDOccCode1 == 'D') {
        ADDOccCode1A = Number(10);
      }
      if (ADDOccCode1 == 'IC') {
        ADDOccCode1A = Number(5);
      }
      if (ADDOccCode1 == '4') {
        ADDOccCode1A = Number(4);
      }
      if (ADDOccCode1 == '3') {
        ADDOccCode1A = Number(3);
      }
      if (ADDOccCode1 == '2') {
        ADDOccCode1A = Number(2);
      }
      if (ADDOccCode1 == '1') {
        ADDOccCode1A = Number(1);
      }
      if (ADDOccCode2 == 'D') {
        ADDOccCode2A = Number(10);
      }
      if (ADDOccCode2 == 'IC') {
        ADDOccCode2A = Number(5);
      }
      if (ADDOccCode2 == '4') {
        ADDOccCode2A = Number(4);
      }
      if (ADDOccCode2 == '3') {
        ADDOccCode2A = Number(3);
      }
      if (ADDOccCode2 == '2') {
        ADDOccCode2A = Number(2);
      }
      if (ADDOccCode2 == '1') {
        ADDOccCode2A = Number(1);
      }
      updateJSON(inoutMap, 'ADDOccCode1', ADDOccCode1);
      updateJSON(inoutMap, 'ADDOccCode2', ADDOccCode2);
      updateJSON(inoutMap, 'ADDOccCode1A', ADDOccCode1A);
      updateJSON(inoutMap, 'ADDOccCode2A', ADDOccCode2A);
      successCB(inoutMap);
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function OccupationaLRuleBaseCode(inoutMap, successCB) {
  var BaseOccCode1, BaseOccCode2, BaseOccCode1A, BaseOccCode2A, err;
  BaseOccCode1A = Number(0);
  BaseOccCode1 = '';
  BaseOccCode2A = Number(0);
  BaseOccCode2 = '';
  BaseOccCode1 = 0;
  lookupAsJson('BASE_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode1 + '\'', function (arguments, _$param108, _$param109) {
    BaseOccCode1 = _$param108;
    err = _$param109;
    BaseOccCode2 = 0;
    lookupAsJson('BASE_OCC_CD', 'EXTN_HLTH_OCC_TBL', 'PROD_ID=\'' + inoutMap.PolicyDetails.productCode + '\'', 'OCC_CD=\'' + inoutMap.PolicyDetails.occCode2 + '\'', function (arguments, _$param110, _$param111) {
      BaseOccCode2 = _$param110;
      err = _$param111;
      if (BaseOccCode1 == 'D') {
        BaseOccCode1A = Number(10);
      }
      if (BaseOccCode1 == 'IC') {
        BaseOccCode1A = Number(5);
      }
      if (BaseOccCode1 == '4') {
        BaseOccCode1A = Number(4);
      }
      if (BaseOccCode1 == '3') {
        BaseOccCode1A = Number(3);
      }
      if (BaseOccCode1 == '2') {
        BaseOccCode1A = Number(2);
      }
      if (BaseOccCode1 == '1') {
        BaseOccCode1A = Number(1);
      }
      if (BaseOccCode2 == 'D') {
        BaseOccCode2A = Number(10);
      }
      if (BaseOccCode2 == 'IC') {
        BaseOccCode2A = Number(5);
      }
      if (BaseOccCode2 == '4') {
        BaseOccCode2A = Number(4);
      }
      if (BaseOccCode2 == '3') {
        BaseOccCode2A = Number(3);
      }
      if (BaseOccCode2 == '2') {
        BaseOccCode2A = Number(2);
      }
      if (BaseOccCode2 == '1') {
        BaseOccCode2A = Number(1);
      }
      updateJSON(inoutMap, 'BaseOccCode1', BaseOccCode1);
      updateJSON(inoutMap, 'BaseOccCode2', BaseOccCode2);
      updateJSON(inoutMap, 'BaseOccCode1A', BaseOccCode1A);
      updateJSON(inoutMap, 'BaseOccCode2A', BaseOccCode2A);
      successCB(inoutMap);
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function Gen_Hlth_BIGraph(inoutMap, successCB) {
  var sumAssuredField, premiumModeField, packageNameField, sumAssuredCIField, sumAssuredHBField, sumAssuredHSField, sumAssuredADDField, insuredAgeField, planCodeField, planShortNameField;
  sumAssuredField = inoutMap.PolicyDetails.sumAssured;
  premiumModeField = inoutMap.totalPolicyPremium;
  packageNameField = inoutMap.PolicyDetails.packageName;
  sumAssuredCIField = inoutMap.CISumAssuredCover;
  sumAssuredHBField = inoutMap.HBSumAssuredCover;
  sumAssuredHSField = inoutMap.HSSumAssuredCover;
  sumAssuredOPDField = inoutMap.OPDSumAssuredCover;
  sumAssuredADDField = inoutMap.ADDSumAssuredCover;
  insuredAgeField = inoutMap.InsuredDetails.age;
  planCodeField = '4M80';
  planShortNameField = 'CHSA80';
  if (inoutMap.PolicyDetails.packageName == 'A' && inoutMap.InsuredDetails.age <= Number(5)) {
    sumAssuredHBField = Number(0);
  }
  updateJSON(inoutMap, 'sumAssuredField', sumAssuredField);
  updateJSON(inoutMap, 'premiumModeField', premiumModeField);
  updateJSON(inoutMap, 'packageNameField', packageNameField);
  updateJSON(inoutMap, 'sumAssuredCIField', sumAssuredCIField);
  updateJSON(inoutMap, 'sumAssuredHBField', sumAssuredHBField);
  updateJSON(inoutMap, 'sumAssuredHSField', sumAssuredHSField);
  updateJSON(inoutMap, 'sumAssuredADDField', sumAssuredADDField);
  updateJSON(inoutMap, 'insuredAgeField', insuredAgeField);
  updateJSON(inoutMap, 'planCodeField', planCodeField);
  updateJSON(inoutMap, 'planShortNameField', planShortNameField);
  successCB(inoutMap);
}
/* Generated by Continuation.js v0.1.7 */