function eApp_Doc_Requirement_Master(inoutMap, successCB) {
  var Requirements, inoutMap, err;
  Requirements = [];
  eApp_Insured_DocTile(inoutMap, function (arguments, _$param0, _$param1) {
    inoutMap = _$param0;
    err = _$param1;
    Requirements = pushArray(Requirements, inoutMap.InsuredRequirements);
    eApp_Payer_DocTile(inoutMap, function (arguments, _$param2, _$param3) {
      inoutMap = _$param2;
      err = _$param3;
      Requirements = pushArray(Requirements, inoutMap.PayerRequirements);
      eApp_Others_DocTile(inoutMap, function (arguments, _$param4, _$param5) {
        inoutMap = _$param4;
        err = _$param5;
        Requirements = pushArray(Requirements, inoutMap.OtherRequirements);
        eApp_New_Others_DocTile(inoutMap, function (arguments, _$param6, _$param7) {
          inoutMap = _$param6;
          err = _$param7;
          Requirements = pushArray(Requirements, inoutMap.NORequirements);
          updateJSON(inoutMap, 'Requirements', Requirements);
          successCB(inoutMap);
        }.bind(this, arguments));
      }.bind(this, arguments));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function eApp_Insured_DocTile(inoutMap, successCB) {
  var InsuredRequirements, inoutMap, err;
  InsuredRequirements = [];
  eApp_Insured_IdentityProof_DocTile(inoutMap, function (arguments, _$param8, _$param9) {
    inoutMap = _$param8;
    err = _$param9;
    InsuredRequirements = pushArray(InsuredRequirements, inoutMap.IIDRequirements);
    eApp_Insured_HealthRecord_DocTile(inoutMap, function (arguments, _$param10, _$param11) {
      inoutMap = _$param10;
      err = _$param11;
      InsuredRequirements = pushArray(InsuredRequirements, inoutMap.HRRequirements);
      eApp_Insured_ApplicationForm_DocTile(inoutMap, function (arguments, _$param12, _$param13) {
        inoutMap = _$param12;
        err = _$param13;
        InsuredRequirements = pushArray(InsuredRequirements, inoutMap.PAFRequirements);
        eApp_Insured_PrevPolicyDoc_DocTile(inoutMap, function (arguments, _$param14, _$param15) {
          inoutMap = _$param14;
          err = _$param15;
          InsuredRequirements = pushArray(InsuredRequirements, inoutMap.PPDRequirements);
          eApp_Insured_ConcentLetter_DocTile(inoutMap, function (arguments, _$param16, _$param17) {
            inoutMap = _$param16;
            err = _$param17;
            InsuredRequirements = pushArray(InsuredRequirements, inoutMap.CLRequirements);
            eApp_Insured_FATCA_DocTile(inoutMap, function (arguments, _$param18, _$param19) {
              inoutMap = _$param18;
              err = _$param19;
              InsuredRequirements = pushArray(InsuredRequirements, inoutMap.FRequirements);
              eApp_Insured_FinancialRecords_DocTile(inoutMap, function (arguments, _$param20, _$param21) {
                inoutMap = _$param20;
                err = _$param21;
                InsuredRequirements = pushArray(InsuredRequirements, inoutMap.FRRequirements);
                eApp_Insured_ForeignerQues_DocTile(inoutMap, function (arguments, _$param22, _$param23) {
                  inoutMap = _$param22;
                  err = _$param23;
                  InsuredRequirements = pushArray(InsuredRequirements, inoutMap.FQRequirements);
                  updateJSON(inoutMap, 'InsuredRequirements', InsuredRequirements);
                  successCB(inoutMap);
                }.bind(this, arguments));
              }.bind(this, arguments));
            }.bind(this, arguments));
          }.bind(this, arguments));
        }.bind(this, arguments));
      }.bind(this, arguments));
    }.bind(this, arguments));
  }.bind(this, arguments));
}
function eApp_Insured_HealthRecord_DocTile(inoutMap, successCB) {
  var HRRequirements, pages, documents, i;
  HRRequirements = [];
  pages = [];
  documents = [];
  pageArrayIndex = Number(0);
  PageIDDocs = [];
  docArrayIndex = Number(0);
  PayerIDDocs = [];
  arrayIndex = Number(0);
  Requirements = [];
  flag = Number(0);
  mandatoryFlag = Boolean(false);
  pageRowData = {};
  pageRowData.fileName = '';
  pageRowData.contentType = '';
  pageRowData.documentStatus = '';
  PageIDDocs[pageArrayIndex] = pageRowData;
  pages = pushArray(pages, PageIDDocs);
  i = 0;
  while (i <= 0) {
    docRowData = {};
    docRowData.DocType = '';
    if (i == Number(0) && (inoutMap.InsuredDetails.age >= 0 && inoutMap.InsuredDetails.age <= 1)) {
      docRowData.documentType = 'HealthRecord';
      docRowData.documentProofSubmitted = 'D13';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      flag = Number(1);
      docRowData.pages = pages;
      docRowData.documentOptions = ['D13'];
      mandatoryFlag = Boolean(true);
    }
    if (flag != Number(1)) {
      docRowData.documentType = '';
      docRowData.documentProofSubmitted = '';
      docRowData.isMandatory = '';
      docRowData.pages = pages;
      docRowData.documentOptions = [''];
    }
    docRowData.documentName = '';
    docRowData.documentUploadStatus = '';
    docRowData.FormID = '';
    docRowData.documentStatus = '';
    PayerIDDocs[i] = docRowData;
    i = i + 1;
  }
  rowData = {};
  rowData.requirementName = 'Insured_HealthRecords';
  rowData.isMandatory = mandatoryFlag;
  rowData.SerialNumber = '2';
  documents = pushArray(documents, PayerIDDocs);
  rowData.Documents = documents;
  rowData.requirementType = 'HEALTHRECORD';
  rowData.requirementSubType = 'healthRecord';
  rowData.partyIdentifier = 'Insured';
  if (mandatoryFlag == Boolean(true)) {
    HRRequirements[arrayIndex] = rowData;
  }
  updateJSON(inoutMap, 'HRRequirements', HRRequirements);
  updateJSON(inoutMap, 'pages', pages);
  updateJSON(inoutMap, 'documents', documents);
  successCB(inoutMap);
}
function eApp_Payer_DocTile(inoutMap, successCB) {
  var PayerRequirements, inoutMap, err;
  PayerRequirements = [];
  (function (_$cont) {
    if (inoutMap.InsuredDetails.isInsuredSameAsPayer != 'Yes') {
      eApp_Payer_IdentityProof_DocTile(inoutMap, function (arguments, _$param24, _$param25) {
        inoutMap = _$param24;
        err = _$param25;
        PayerRequirements = pushArray(PayerRequirements, inoutMap.IDRequirements);
        _$cont();
      }.bind(this, arguments));
    } else {
      _$cont();
    }
  }.bind(this)(function (_$err) {
    if (_$err !== undefined)
      return _$cont(_$err);
    eApp_Payer_PaymentRecords_DocTile(inoutMap, function (arguments, _$param26, _$param27) {
      inoutMap = _$param26;
      err = _$param27;
      PayerRequirements = pushArray(PayerRequirements, inoutMap.PRRequirements);
      updateJSON(inoutMap, 'PayerRequirements', PayerRequirements);
      successCB(inoutMap);
    }.bind(this, arguments));
  }.bind(this)));
}
function eApp_Payer_IdentityProof_DocTile(inoutMap, successCB) {
  var IDRequirements, PayerIDDocs, documents, pages, i;
  IDRequirements = [];
  PayerIDDocs = [];
  documents = [];
  pages = [];
  pageArrayIndex = Number(0);
  PageIDDocs = [];
  docArrayIndex = Number(0);
  PayerIDDocs = [];
  arrayIndex = Number(0);
  Requirements = [];
  pageRowData = {};
  pageRowData.fileName = '';
  pageRowData.contentType = '';
  pageRowData.documentStatus = '';
  PageIDDocs[pageArrayIndex] = pageRowData;
  pages = pushArray(pages, PageIDDocs);
  i = 0;
  while (i <= 4) {
    docRowData = {};
    docRowData.DocType = '';
    if (i == Number(0) && inoutMap.InsuredDetails.insuredNationality == 'THA') {
      docRowData.documentType = 'NationalID';
      docRowData.documentProofSubmitted = 'IDP';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['IDP'];
    }
    if (i == Number(1)) {
      docRowData.documentType = 'DrivingLicense';
      docRowData.documentProofSubmitted = 'P005';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['P005'];
    }
    if (i == Number(2) && inoutMap.InsuredDetails.insuredNationality == 'THA') {
      docRowData.documentType = 'Passport';
      docRowData.documentProofSubmitted = 'P006';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['P006'];
    }
    if (i == Number(3) && inoutMap.InsuredDetails.insuredNationality == 'THA') {
      docRowData.documentType = 'WorkPermit';
      docRowData.documentProofSubmitted = 'P007';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['P007'];
    }
    if (i == Number(4)) {
      docRowData.documentType = 'HouseholdRegistration';
      docRowData.documentProofSubmitted = 'P008';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['P008'];
    }
    if (i == Number(0) && inoutMap.InsuredDetails.insuredNationality != 'THA') {
      docRowData.documentType = 'NationalID';
      docRowData.documentProofSubmitted = 'IDP';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['IDP'];
    }
    if (i == Number(2) && inoutMap.InsuredDetails.insuredNationality != 'THA') {
      docRowData.documentType = 'Passport';
      docRowData.documentProofSubmitted = 'P006';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['P006'];
    }
    if (i == Number(3) && inoutMap.InsuredDetails.insuredNationality != 'THA') {
      docRowData.documentType = 'WorkPermit';
      docRowData.documentProofSubmitted = 'P007';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['P007'];
    }
    docRowData.documentName = '';
    docRowData.documentUploadStatus = '';
    docRowData.FormID = '';
    docRowData.documentStatus = '';
    PayerIDDocs[i] = docRowData;
    i = i + 1;
  }
  rowData = {};
  rowData.requirementName = 'Payer_IdentityProof';
  rowData.isMandatory = true;
  rowData.SerialNumber = '1';
  documents = pushArray(documents, PayerIDDocs);
  rowData.Documents = documents;
  rowData.requirementType = 'DOCUMENT_IDENTITY_PAYER';
  rowData.requirementSubType = 'documentTypePayer';
  rowData.partyIdentifier = 'Payer';
  IDRequirements[arrayIndex] = rowData;
  updateJSON(inoutMap, 'IDRequirements', IDRequirements);
  updateJSON(inoutMap, 'PayerIDDocs', PayerIDDocs);
  updateJSON(inoutMap, 'documents', documents);
  updateJSON(inoutMap, 'pages', pages);
  successCB(inoutMap);
}
function eApp_Insured_PrevPolicyDoc_DocTile(inoutMap, successCB) {
  var PPDRequirements, pages, documents, i;
  PPDRequirements = [];
  pages = [];
  documents = [];
  pageArrayIndex = Number(0);
  PageIDDocs = [];
  docArrayIndex = Number(0);
  PayerIDDocs = [];
  arrayIndex = Number(0);
  Requirements = [];
  flag = Number(0);
  mandatoryFlag = Boolean(false);
  pageRowData = {};
  pageRowData.fileName = '';
  pageRowData.contentType = '';
  pageRowData.documentStatus = '';
  PageIDDocs[pageArrayIndex] = pageRowData;
  pages = pushArray(pages, PageIDDocs);
  i = 0;
  while (i <= 0) {
    docRowData = {};
    docRowData.DocType = '';
    if (i == Number(0) && inoutMap.ProductDetails.PhysicalApplicationNumber == 'Yes') {
      docRowData.documentType = 'PreviousPolicyDocument';
      docRowData.documentProofSubmitted = 'APQ';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['APQ'];
      mandatoryFlag = Boolean(true);
    }
    if (i == Number(0) && inoutMap.ProductDetails.PhysicalApplicationNumber != 'Yes') {
      docRowData.documentType = 'PreviousPolicyDocument';
      docRowData.documentProofSubmitted = 'APQ';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['APQ'];
    }
    docRowData.documentName = '';
    docRowData.documentUploadStatus = '';
    docRowData.FormID = '';
    docRowData.documentStatus = '';
    PayerIDDocs[i] = docRowData;
    i = i + 1;
  }
  rowData = {};
  rowData.requirementName = 'Insured_PreviousPolicyDocuments';
  rowData.isMandatory = mandatoryFlag;
  rowData.SerialNumber = '4';
  documents = pushArray(documents, PayerIDDocs);
  rowData.Documents = documents;
  rowData.requirementType = 'PREVIOUSPOLICY';
  rowData.requirementSubType = 'previousPolicy';
  rowData.partyIdentifier = 'Insured';
  PPDRequirements[arrayIndex] = rowData;
  updateJSON(inoutMap, 'PPDRequirements', PPDRequirements);
  updateJSON(inoutMap, 'pages', pages);
  updateJSON(inoutMap, 'documents', documents);
  successCB(inoutMap);
}
function eApp_Insured_ConcentLetter_DocTile(inoutMap, successCB) {
  var CLRequirements, pages, documents, i;
  CLRequirements = [];
  pages = [];
  documents = [];
  pageArrayIndex = Number(0);
  PageIDDocs = [];
  docArrayIndex = Number(0);
  PayerIDDocs = [];
  arrayIndex = Number(0);
  Requirements = [];
  flag = Number(0);
  pageRowData = {};
  pageRowData.fileName = '';
  pageRowData.contentType = '';
  pageRowData.documentStatus = '';
  PageIDDocs[pageArrayIndex] = pageRowData;
  pages = pushArray(pages, PageIDDocs);
  i = 0;
  while (i <= 0) {
    docRowData = {};
    docRowData.DocType = '';
    docRowData.documentType = 'ConcentLetter';
    docRowData.documentProofSubmitted = 'CSF';
    docRowData.isMandatory = false;
    docRowData.isDisabled = false;
    docRowData.pages = pages;
    docRowData.documentOptions = ['CSF'];
    docRowData.documentName = '';
    docRowData.documentUploadStatus = '';
    docRowData.FormID = '';
    docRowData.documentStatus = '';
    PayerIDDocs[i] = docRowData;
    i = i + 1;
  }
  rowData = {};
  rowData.requirementName = 'Insured_ConcentLetter';
  rowData.isMandatory = false;
  rowData.SerialNumber = '5';
  documents = pushArray(documents, PayerIDDocs);
  rowData.Documents = documents;
  rowData.requirementType = 'CONSENTLETTER';
  rowData.requirementSubType = 'consentLetter';
  rowData.partyIdentifier = 'Insured';
  CLRequirements[arrayIndex] = rowData;
  updateJSON(inoutMap, 'CLRequirements', CLRequirements);
  updateJSON(inoutMap, 'pages', pages);
  updateJSON(inoutMap, 'documents', documents);
  successCB(inoutMap);
}
function eApp_Insured_FATCA_DocTile(inoutMap, successCB) {
  var FRequirements, pages, documents, i;
  FRequirements = [];
  pages = [];
  documents = [];
  pageArrayIndex = Number(0);
  PageIDDocs = [];
  docArrayIndex = Number(0);
  PayerIDDocs = [];
  arrayIndex = Number(0);
  Requirements = [];
  flag = Number(0);
  innerFlag = Number(0);
  innerIndex = sub(Number(0), Number(1));
  pageRowData = {};
  pageRowData.fileName = '';
  pageRowData.contentType = '';
  pageRowData.documentStatus = '';
  PageIDDocs[pageArrayIndex] = pageRowData;
  pages = pushArray(pages, PageIDDocs);
  i = 0;
  while (i <= 4) {
    docRowData = {};
    docRowData.DocType = '';
    if (i == Number(0) && (inoutMap.QuestionDetails.FatcaQuestions1Ans == 'Yes' && inoutMap.QuestionDetails.FatcaQuestions12Ans == 'Yes') || i == Number(0) && inoutMap.QuestionDetails.FatcaQuestions2Ans == 'NoLonger') {
      docRowData.documentType = 'FormW8';
      docRowData.documentProofSubmitted = 'W8Q';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      flag = Number(1);
      innerFlag = Number(1);
      docRowData.pages = pages;
      docRowData.documentOptions = ['W8Q'];
    }
    if (i == Number(1) && inoutMap.QuestionDetails.FatcaQuestions1Ans == 'Yes' && (inoutMap.QuestionDetails.FatcaQuestions11Ans == 'Yes' || inoutMap.QuestionDetails.FatcaQuestions13Ans == 'Yes') || i == Number(1) && inoutMap.QuestionDetails.FatcaQuestions2Ans == 'Yes' || i == Number(1) && inoutMap.QuestionDetails.FatcaQuestions3Ans == 'Yes' || i == Number(1) && inoutMap.QuestionDetails.FatcaQuestions4Ans == 'Yes') {
      docRowData.documentType = 'FormW9';
      docRowData.documentProofSubmitted = 'W9Q';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      flag = Number(1);
      innerFlag = Number(1);
      docRowData.pages = pages;
      docRowData.documentOptions = ['W9Q'];
    }
    docRowData.documentName = '';
    docRowData.documentUploadStatus = '';
    docRowData.FormID = '';
    docRowData.documentStatus = '';
    if (flag == Number(1)) {
      flag = Number(0);
      innerIndex = sum(innerIndex, 1);
      PayerIDDocs[innerIndex] = docRowData;
    }
    i = i + 1;
  }
  if (innerFlag == Number(0)) {
    docRowData = {};
    docRowData.DocType = '';
    docRowData.documentType = '';
    docRowData.documentProofSubmitted = '';
    docRowData.isMandatory = '';
    docRowData.pages = pages;
    docRowData.documentOptions = [''];
    docRowData.documentName = '';
    docRowData.documentUploadStatus = '';
    docRowData.FormID = '';
    docRowData.documentStatus = '';
    PayerIDDocs[0] = docRowData;
  }
  rowData = {};
  rowData.requirementName = 'FATCA';
  rowData.isMandatory = false;
  rowData.SerialNumber = '6';
  documents = pushArray(documents, PayerIDDocs);
  rowData.Documents = documents;
  rowData.requirementType = 'FATCA';
  rowData.requirementSubType = 'fatcaDocument';
  rowData.partyIdentifier = 'Insured';
  if (innerFlag == Number(1)) {
    FRequirements[arrayIndex] = rowData;
  }
  updateJSON(inoutMap, 'FRequirements', FRequirements);
  updateJSON(inoutMap, 'pages', pages);
  updateJSON(inoutMap, 'documents', documents);
  successCB(inoutMap);
}
function eApp_Insured_ForeignerQues_DocTile(inoutMap, successCB) {
  var FQRequirements, pages, documents, i;
  FQRequirements = [];
  pages = [];
  documents = [];
  pageArrayIndex = Number(0);
  PageIDDocs = [];
  docArrayIndex = Number(0);
  PayerIDDocs = [];
  arrayIndex = Number(0);
  Requirements = [];
  flag = Number(0);
  pageRowData = {};
  pageRowData.fileName = '';
  pageRowData.contentType = '';
  pageRowData.documentStatus = '';
  PageIDDocs[pageArrayIndex] = pageRowData;
  pages = pushArray(pages, PageIDDocs);
  i = 0;
  while (i <= 0) {
    docRowData = {};
    docRowData.DocType = '';
    if (i == Number(0) && inoutMap.InsuredDetails.insuredNationality != 'THA') {
      docRowData.documentType = 'ForeignerQuestionnaire';
      docRowData.documentProofSubmitted = 'FRQ';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      flag = Number(1);
      docRowData.pages = pages;
      docRowData.documentOptions = ['FRQ'];
    }
    if (flag != Number(1)) {
      docRowData.documentType = '';
      docRowData.documentProofSubmitted = '';
      docRowData.isMandatory = '';
      docRowData.pages = pages;
      docRowData.documentOptions = [''];
    }
    docRowData.documentName = '';
    docRowData.documentUploadStatus = '';
    docRowData.FormID = '';
    docRowData.documentStatus = '';
    PayerIDDocs[i] = docRowData;
    i = i + 1;
  }
  rowData = {};
  rowData.requirementName = 'Insured_ForeignerQuestionnaire';
  rowData.isMandatory = false;
  rowData.SerialNumber = '8';
  documents = pushArray(documents, PayerIDDocs);
  rowData.Documents = documents;
  rowData.requirementType = 'FORIGNERQUESTIONNAIRE';
  rowData.requirementSubType = 'forignQuestionnaire';
  rowData.partyIdentifier = 'Insured';
  if (flag == Number(1)) {
    FQRequirements[arrayIndex] = rowData;
  }
  updateJSON(inoutMap, 'FQRequirements', FQRequirements);
  updateJSON(inoutMap, 'pages', pages);
  updateJSON(inoutMap, 'documents', documents);
  successCB(inoutMap);
}
function eApp_Insured_FinancialRecords_DocTile(inoutMap, successCB) {
  var FRRequirements, pages, documents, i;
  FRRequirements = [];
  pages = [];
  documents = [];
  pageArrayIndex = Number(0);
  PageIDDocs = [];
  docArrayIndex = Number(0);
  PayerIDDocs = [];
  arrayIndex = Number(0);
  Requirements = [];
  flag = Number(0);
  pageRowData = {};
  pageRowData.fileName = '';
  pageRowData.contentType = '';
  pageRowData.documentStatus = '';
  PageIDDocs[pageArrayIndex] = pageRowData;
  pages = pushArray(pages, PageIDDocs);
  i = 0;
  while (i <= 0) {
    docRowData = {};
    docRowData.DocType = '';
    if (i == Number(0) && inoutMap.ProductDetails.sumAssured > Number(5000000)) {
      docRowData.documentType = 'LAQ';
      docRowData.documentProofSubmitted = 'LAQ';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      flag = Number(1);
      docRowData.pages = pages;
      docRowData.documentOptions = ['LAQ'];
    }
    if (flag != Number(1)) {
      docRowData.documentType = '';
      docRowData.documentProofSubmitted = '';
      docRowData.isMandatory = '';
      docRowData.pages = pages;
      docRowData.documentOptions = [''];
    }
    docRowData.documentName = '';
    docRowData.documentUploadStatus = '';
    docRowData.FormID = '';
    docRowData.documentStatus = '';
    PayerIDDocs[i] = docRowData;
    i = i + 1;
  }
  rowData = {};
  rowData.requirementName = 'Insured_FinancialRecords';
  rowData.isMandatory = false;
  rowData.SerialNumber = '7';
  documents = pushArray(documents, PayerIDDocs);
  rowData.Documents = documents;
  rowData.requirementType = 'FINANCIALRECORDS';
  rowData.requirementSubType = 'financialRecords';
  rowData.partyIdentifier = 'Insured';
  if (flag == Number(1)) {
    FRRequirements[arrayIndex] = rowData;
  }
  updateJSON(inoutMap, 'FRRequirements', FRRequirements);
  updateJSON(inoutMap, 'pages', pages);
  updateJSON(inoutMap, 'documents', documents);
  successCB(inoutMap);
}
function eApp_Insured_ApplicationForm_DocTile(inoutMap, successCB) {
  var PAFRequirements, pages, documents, i;
  PAFRequirements = [];
  pages = [];
  documents = [];
  pageArrayIndex = Number(0);
  PageIDDocs = [];
  docArrayIndex = Number(0);
  PayerIDDocs = [];
  arrayIndex = Number(0);
  Requirements = [];
  flag = Number(0);
  mandatoryFlag = Boolean(false);
  pageRowData = {};
  pageRowData.fileName = '';
  pageRowData.contentType = '';
  pageRowData.documentStatus = '';
  PageIDDocs[pageArrayIndex] = pageRowData;
  pages = pushArray(pages, PageIDDocs);
  i = 0;
  while (i <= 1) {
    docRowData = {};
    docRowData.DocType = '';
    if (i == Number(0) && inoutMap.ProductDetails.PhysicalApplicationNumber == 'Yes') {
      docRowData.documentType = 'ApplicationForm';
      docRowData.documentProofSubmitted = 'PAF';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      flag = Number(1);
      docRowData.pages = pages;
      docRowData.documentOptions = ['PAF'];
      mandatoryFlag = Boolean(true);
    }
    if (i == Number(1) && inoutMap.ProductDetails.PhysicalApplicationNumber == 'Yes') {
      docRowData.documentType = 'Illustration PDF';
      docRowData.documentProofSubmitted = 'IPDF';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      flag = Number(2);
      docRowData.pages = pages;
      docRowData.documentOptions = ['IPDF'];
      mandatoryFlag = Boolean(true);
    }
    if (flag != Number(1) && flag != Number(2)) {
      docRowData.documentType = '';
      docRowData.documentProofSubmitted = '';
      docRowData.isMandatory = '';
      docRowData.pages = pages;
      docRowData.documentOptions = [''];
    }
    docRowData.documentName = '';
    docRowData.documentUploadStatus = '';
    docRowData.FormID = '';
    docRowData.documentStatus = '';
    PayerIDDocs[i] = docRowData;
    i = i + 1;
  }
  rowData = {};
  rowData.requirementName = 'Insured_PhysicalApplicationForm';
  rowData.isMandatory = mandatoryFlag;
  rowData.SerialNumber = '3';
  documents = pushArray(documents, PayerIDDocs);
  rowData.Documents = documents;
  rowData.requirementType = 'PHYSICALAPPFORMS';
  rowData.requirementSubType = 'physicalAppForms';
  rowData.partyIdentifier = 'Insured';
  if (mandatoryFlag == Boolean(true)) {
    PAFRequirements[arrayIndex] = rowData;
  }
  updateJSON(inoutMap, 'PAFRequirements', PAFRequirements);
  updateJSON(inoutMap, 'pages', pages);
  updateJSON(inoutMap, 'documents', documents);
  successCB(inoutMap);
}
function eApp_Others_DocTile(inoutMap, successCB) {
  var OtherRequirements, page, documents;
  OtherRequirements = [];
  page = [];
  documents = [];
  pageArrayIndex = Number(0);
  PageIDDocs = [];
  docArrayIndex = Number(0);
  PayerIDDocs = [];
  arrayIndex = Number(0);
  Requirements = [];
  flag = Number(0);
  mandatoryFlag = Boolean(false);
  arrayFlag = Boolean(false);
  pageRowData = {};
  pageRowData.fileName = '';
  pageRowData.contentType = '';
  pageRowData.documentStatus = '';
  PageIDDocs[pageArrayIndex] = pageRowData;
  page = pushArray(page, PageIDDocs);
  docRowData = {};
  docRowData.DocType = '';
  docRowData.documentType = 'GuardianDocument';
  docRowData.documentProofSubmitted = '3GD';
  if (inoutMap.InsuredDetails.age < Number(20) && inoutMap.InsuredDetails.isInsuredSameAsPayer == 'No') {
    if (inoutMap.PayerDetails.payerRelationshipWithInsured != 'Parent') {
      docRowData.isMandatory = true;
      mandatoryFlag = Boolean(true);
      arrayFlag = Boolean(true);
    }
  }
  docRowData.pages = page;
  docRowData.documentOptions = ['3GD'];
  docRowData.documentName = '';
  docRowData.documentUploadStatus = '';
  docRowData.FormID = '';
  docRowData.documentStatus = '';
  PayerIDDocs[0] = docRowData;
  rowData = {};
  rowData.requirementName = 'Others';
  rowData.isMandatory = mandatoryFlag;
  rowData.SerialNumber = '1';
  documents = pushArray(documents, PayerIDDocs);
  rowData.Documents = documents;
  rowData.requirementType = 'OTHERS';
  rowData.requirementSubType = 'otherDocument';
  rowData.partyIdentifier = 'Others';
  if (arrayFlag == Boolean(true)) {
    OtherRequirements[arrayIndex] = rowData;
  }
  updateJSON(inoutMap, 'OtherRequirements', OtherRequirements);
  updateJSON(inoutMap, 'page', page);
  updateJSON(inoutMap, 'documents', documents);
  successCB(inoutMap);
}
function eApp_New_Others_DocTile(inoutMap, successCB) {
  var NORequirements, PayerIDDocs, documents, pages, i;
  NORequirements = [];
  PayerIDDocs = [];
  documents = [];
  pages = [];
  pageArrayIndex = Number(0);
  PageIDDocs = [];
  docArrayIndex = Number(0);
  PayerIDDocs = [];
  arrayIndex = Number(0);
  Requirements = [];
  pageRowData = {};
  pageRowData.fileName = '';
  pageRowData.contentType = '';
  pageRowData.documentStatus = '';
  PageIDDocs[pageArrayIndex] = pageRowData;
  pages = pushArray(pages, PageIDDocs);
  i = 0;
  while (i <= 4) {
    docRowData = {};
    docRowData.DocType = '';
    docRowData.documentType = '';
    docRowData.documentProofSubmitted = '';
    docRowData.isMandatory = '';
    docRowData.isDisabled = '';
    docRowData.pages = pages;
    docRowData.documentOptions = [''];
    docRowData.documentName = '';
    docRowData.documentUploadStatus = '';
    docRowData.FormID = '';
    docRowData.documentStatus = '';
    PayerIDDocs[i] = docRowData;
    i = i + 1;
  }
  rowData = {};
  rowData.requirementName = '';
  rowData.isMandatory = '';
  rowData.SerialNumber = '';
  documents = pushArray(documents, PayerIDDocs);
  rowData.Documents = documents;
  rowData.requirementType = '';
  rowData.requirementSubType = '';
  rowData.partyIdentifier = 'Others';
  NORequirements[arrayIndex] = rowData;
  updateJSON(inoutMap, 'NORequirements', NORequirements);
  updateJSON(inoutMap, 'PayerIDDocs', PayerIDDocs);
  updateJSON(inoutMap, 'documents', documents);
  updateJSON(inoutMap, 'pages', pages);
  successCB(inoutMap);
}
function eApp_Payer_PaymentRecords_DocTile(inoutMap, successCB) {
  var answer, PRRequirements, PayerIDDocs, documents, pages, i;
  PRRequirements = [];
  PayerIDDocs = [];
  documents = [];
  pages = [];
  pageArrayIndex = Number(0);
  PageIDDocs = [];
  docArrayIndex = Number(0);
  PayerIDDocs = [];
  arrayIndex = Number(0);
  indexVal = sub(Number(0), Number(1));
  Requirements = [];
  flag = Number(0);
  mandatoryFlag = Boolean(false);
  splitFlag = Boolean(false);
  splitFlag2 = Boolean(false);
  Counter = sub(Number(0), Number(1));
  pageRowData = {};
  pageRowData.fileName = '';
  pageRowData.contentType = '';
  pageRowData.documentStatus = '';
  PageIDDocs[pageArrayIndex] = pageRowData;
  pages = pushArray(pages, PageIDDocs);
  i = 0;
  while (i <= 6) {
    docRowData = {};
    docRowData.DocType = '';
    if (i == Number(0) && inoutMap.BankDetails.ETRNumber == 'Manual') {
      docRowData.documentType = 'TemporaryReceipt';
      docRowData.documentProofSubmitted = 'P001';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      flag = Number(1);
      docRowData.pages = pages;
      docRowData.documentOptions = ['P001'];
      mandatoryFlag = Boolean(true);
      Counter = sum(Counter, Number(1));
      splitFlag2 = Boolean(true);
    }
    if (i == Number(1) && inoutMap.BankDetails.ETRNumber2 == 'Manual') {
      docRowData.documentType = 'TemporaryReceipt';
      docRowData.documentProofSubmitted = 'P0011';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      flag = Number(1);
      docRowData.pages = pages;
      docRowData.documentOptions = ['P011'];
      mandatoryFlag = Boolean(true);
      Counter = sum(Counter, Number(1));
      splitFlag2 = Boolean(true);
    }
    if (i == Number(2) && (inoutMap.BankDetails.modeOfPayment == 'Cash' || inoutMap.BankDetails.modeOfPayment == 'PersonalCheck')) {
      docRowData.documentType = 'BankPayInSlip';
      docRowData.documentProofSubmitted = 'P002';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      flag = Number(1);
      docRowData.pages = pages;
      docRowData.documentOptions = ['P002'];
      mandatoryFlag = Boolean(true);
      Counter = sum(Counter, Number(1));
      splitFlag = Boolean(true);
    }
    if (i == Number(3) && (inoutMap.BankDetails.modeOfPayment2 == 'Cash' || inoutMap.BankDetails.modeOfPayment2 == 'PersonalCheck')) {
      docRowData.documentType = 'BankPayInSlip';
      docRowData.documentProofSubmitted = 'P022';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      flag = Number(1);
      docRowData.pages = pages;
      docRowData.documentOptions = ['P022'];
      mandatoryFlag = Boolean(true);
      Counter = sum(Counter, Number(1));
      splitFlag = Boolean(true);
    }
    if (i == Number(4) && inoutMap.BankDetails.renewalMethod == 'CreditCard') {
      docRowData.documentType = 'CreditCardConcent';
      docRowData.documentProofSubmitted = 'P003';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      flag = Number(1);
      docRowData.pages = pages;
      docRowData.documentOptions = ['P003'];
      mandatoryFlag = Boolean(true);
      Counter = sum(Counter, Number(1));
    } else {
      if (i == Number(4) && inoutMap.BankDetails.renewalMethod != 'CreditCard') {
        docRowData.documentType = 'CreditCardConcent';
        docRowData.documentProofSubmitted = 'P003';
        docRowData.isMandatory = false;
        docRowData.isDisabled = true;
        flag = Number(1);
        docRowData.pages = pages;
        docRowData.documentOptions = ['P003'];
        Counter = sum(Counter, Number(1));
      }
    }
    if (i == Number(5) && inoutMap.BankDetails.renewalMethod == 'DebitAccount') {
      docRowData.documentType = 'DebitCardConcent';
      docRowData.documentProofSubmitted = 'DAF';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      flag = Number(1);
      docRowData.pages = pages;
      docRowData.documentOptions = ['DAF'];
      mandatoryFlag = Boolean(true);
      Counter = sum(Counter, Number(1));
    } else {
      if (i == Number(5) && inoutMap.BankDetails.renewalMethod != 'DebitAccount') {
        docRowData.documentType = 'DebitCardConcent';
        docRowData.documentProofSubmitted = 'DAF';
        docRowData.isMandatory = false;
        docRowData.isDisabled = true;
        flag = Number(1);
        docRowData.pages = pages;
        docRowData.documentOptions = ['DAF'];
        Counter = sum(Counter, Number(1));
      }
    }
    if (i == Number(6) && inoutMap.BankDetails.renewalMethod == 'DebitAccount') {
      docRowData.documentType = 'CopyofBankBook';
      docRowData.documentProofSubmitted = 'P004';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      flag = Number(1);
      docRowData.pages = pages;
      docRowData.documentOptions = ['P004'];
      mandatoryFlag = Boolean(true);
      Counter = sum(Counter, Number(1));
    }
    docRowData.documentName = '';
    docRowData.documentUploadStatus = '';
    docRowData.FormID = '';
    docRowData.documentStatus = '';
    if (flag == Number(1)) {
      PayerIDDocs[Counter] = docRowData;
      flag = Number(0);
    }
    if (i == Number(3) && splitFlag == Boolean(false)) {
      docRowData = {};
      docRowData.DocType = '';
      docRowData.documentType = 'BankPayInSlip';
      docRowData.documentProofSubmitted = 'P002';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['P002'];
      docRowData.documentName = '';
      docRowData.documentUploadStatus = '';
      docRowData.FormID = '';
      docRowData.documentStatus = '';
      Counter = sum(Counter, Number(1));
      PayerIDDocs[Counter] = docRowData;
    }
    if (i == Number(1) && splitFlag2 == Boolean(false)) {
      docRowData = {};
      docRowData.DocType = '';
      docRowData.documentType = 'TemporaryReceipt';
      docRowData.documentProofSubmitted = 'P001';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['P001'];
      docRowData.documentName = '';
      docRowData.documentUploadStatus = '';
      docRowData.FormID = '';
      docRowData.documentStatus = '';
      Counter = sum(Counter, Number(1));
      PayerIDDocs[Counter] = docRowData;
    }
    i = i + 1;
  }
  rowData = {};
  rowData.requirementName = 'Payer_PaymentRecords';
  rowData.isMandatory = mandatoryFlag;
  rowData.SerialNumber = '2';
  documents = pushArray(documents, PayerIDDocs);
  rowData.Documents = documents;
  rowData.requirementType = 'PAYMENTRECORDS';
  rowData.requirementSubType = 'paymentRecords';
  rowData.partyIdentifier = 'Payer';
  PRRequirements[arrayIndex] = rowData;
  updateJSON(inoutMap, 'answer', answer);
  updateJSON(inoutMap, 'PRRequirements', PRRequirements);
  updateJSON(inoutMap, 'PayerIDDocs', PayerIDDocs);
  updateJSON(inoutMap, 'documents', documents);
  updateJSON(inoutMap, 'pages', pages);
  successCB(inoutMap);
}
function eApp_Insured_IdentityProof_DocTile(inoutMap, successCB) {
  var IIDRequirements, InsuredIDDocs, documents, pages, i;
  IIDRequirements = [];
  InsuredIDDocs = [];
  documents = [];
  pages = [];
  pageArrayIndex = Number(0);
  PageIDDocs = [];
  docArrayIndex = Number(0);
  InsuredIDDocs = [];
  arrayIndex = Number(0);
  Requirements = [];
  flag = Number(0);
  mandatoryFlag = Boolean(false);
  pageRowData = {};
  pageRowData.fileName = '';
  pageRowData.contentType = '';
  pageRowData.documentStatus = '';
  PageIDDocs[pageArrayIndex] = pageRowData;
  pages = pushArray(pages, PageIDDocs);
  i = 0;
  while (i <= 4) {
    docRowData = {};
    docRowData.DocType = '';
    if (i == Number(0) && inoutMap.InsuredDetails.insuredNationality == 'THA' && inoutMap.InsuredDetails.age > Number(15)) {
      docRowData.documentType = 'NationalID';
      docRowData.documentProofSubmitted = 'IDA';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['IDA'];
      mandatoryFlag = Boolean(true);
    }
    if (i == Number(0) && inoutMap.InsuredDetails.insuredNationality != 'THA' && inoutMap.InsuredDetails.age > Number(15)) {
      docRowData.documentType = 'NationalID';
      docRowData.documentProofSubmitted = 'IDA';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['IDA'];
    }
    if (i == Number(1)) {
      docRowData.documentType = 'DrivingLicense';
      docRowData.documentProofSubmitted = 'D001';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['D001'];
    }
    if (i == Number(2) && inoutMap.InsuredDetails.insuredNationality != 'THA') {
      docRowData.documentType = 'Passport';
      docRowData.documentProofSubmitted = 'D15';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['D15'];
      mandatoryFlag = Boolean(true);
    }
    if (i == Number(2) && inoutMap.InsuredDetails.insuredNationality == 'THA') {
      docRowData.documentType = 'Passport';
      docRowData.documentProofSubmitted = 'D15';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['D15'];
    }
    if (i == Number(3) && inoutMap.InsuredDetails.insuredNationality != 'THA') {
      docRowData.documentType = 'WorkPermit';
      docRowData.documentProofSubmitted = 'D14';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['D14'];
      mandatoryFlag = Boolean(true);
    }
    if (i == Number(3) && inoutMap.InsuredDetails.insuredNationality == 'THA') {
      docRowData.documentType = 'WorkPermit';
      docRowData.documentProofSubmitted = 'D14';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['D14'];
    }
    if (i == Number(0) && inoutMap.InsuredDetails.age <= Number(15)) {
      docRowData.documentType = 'BirthCertificate';
      docRowData.documentProofSubmitted = 'BIR';
      docRowData.isMandatory = true;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['BIR'];
      mandatoryFlag = Boolean(true);
    }
    if (i == Number(4)) {
      docRowData.documentType = 'HouseholdRegistration';
      docRowData.documentProofSubmitted = 'HRA';
      docRowData.isMandatory = false;
      docRowData.isDisabled = false;
      docRowData.pages = pages;
      docRowData.documentOptions = ['HRA'];
    }
    docRowData.documentName = '';
    docRowData.documentUploadStatus = '';
    docRowData.FormID = '';
    docRowData.documentStatus = '';
    InsuredIDDocs[i] = docRowData;
    i = i + 1;
  }
  rowData = {};
  rowData.requirementName = 'Insured_IdentityProof';
  rowData.isMandatory = mandatoryFlag;
  rowData.SerialNumber = '1';
  documents = pushArray(documents, InsuredIDDocs);
  rowData.Documents = documents;
  rowData.requirementType = 'DOCUMENT_IDENTITY';
  rowData.requirementSubType = 'documentType';
  rowData.partyIdentifier = 'Insured';
  IIDRequirements[arrayIndex] = rowData;
  updateJSON(inoutMap, 'IIDRequirements', IIDRequirements);
  updateJSON(inoutMap, 'InsuredIDDocs', InsuredIDDocs);
  updateJSON(inoutMap, 'documents', documents);
  updateJSON(inoutMap, 'pages', pages);
  successCB(inoutMap);
}
/* Generated by Continuation.js v0.1.7 */