/**
*Functions that are called on field change
**/
var premiumFrequency;
/**
 * This function will take the rider name as input and will return the index for the rider.
 * @param riderNameWithInsured
 * @param riderDetails
 * @param index
 * @returns {Array}
 */
function getRiderIndex(riderNameWithInsured,riderDetails,index) {
	if(typeof index!= 'undefined' && index!= ''){
		var newIndex= Number(index)+1;
		riderNameWithInsured= riderNameWithInsured.replace(index.toString(),newIndex.toString());
	}
	var riderArr = riderDetails;
	var riderIndex;
	for (k = 0; k < riderArr.length; k++) {
		if (riderArr[k].uniqueRiderName == riderNameWithInsured) {
			riderIndex = k;
			break;
		}
	}
	return [ riderIndex ];
 }
/**
 * 
 * @param value1
 * @param value2
 * @returns lower(value1,value2)
 */
function returnLower(value1,value2){
	if(value1>value2){
		return value2;
	}
	else{
		return value1;
	}
}
/**
 * 
 * @param value1
 * @returns half(value1)
 */
function returnHalfValue(value1,max) {
	var halfValue = value1 / 2;
	if(halfValue<max){
		return halfValue;
	}
	else{
		return max;
	}
}

/**
 * sub function will be called from base function on change of main suminsured (ui input field).This is for ADB rider
 * @param illustration_details
 * @param insuredType
 */
function onSumInsuredChangeADBPlan(illustration_details,insuredType){
	if(illustration_details.Product.ProductDetails.productCode==20){
		if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Child')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),500000000));
		}
		else if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Adult')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),4000000000));
		}
	}
	else if(illustration_details.Product.ProductDetails.productCode==5) {
		if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Child')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.singleBasicPremium*1.5),500000000));
		}
		else if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Adult')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.singleBasicPremium*1.5),4000000000));
		}
	} 
	else if(illustration_details.Product.ProductDetails.productCode==9){
		if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Child')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((1.5*illustration_details.Product.policyDetails.sumInsured),500000000));
		}
		else if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Adult')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((1.5*illustration_details.Product.policyDetails.sumInsured),4000000000));
		}
	}
}	
/**
 * sub function will be called from base function on change of main suminsured (ui input field).This is for ADD rider
 * @param illustration_details
 * @param insuredType
 */
function onSumInsuredChangeADDPlan(illustration_details,insuredType){
	if(illustration_details.Product.ProductDetails.productCode==20){
		if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Child')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),500000000));
		}
		else if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Adult')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),4000000000));
		}
	}
	else if(illustration_details.Product.ProductDetails.productCode==5){
		if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Child')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.singleBasicPremium),500000000));
		}
		else if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Adult')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.singleBasicPremium),4000000000));
		}
	} 
}
/**
 * sub function will be called from base function on change of main suminsured (ui input field).For CIACC rider
 * @param illustration_details
 * @param insuredType
 */
function onSumInsuredChangeCIACCPlan(illustration_details,insuredType){
	if(illustration_details.Product.ProductDetails.productCode==20){
		if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Child')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),1000000000));
		}
		else if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Adult')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),1500000000));
		} 
	}
}
/**
 *  sub function will be called from base function on change of main suminsured (ui input field).For CIADD rider
 * @param illustration_details
 * @param insuredType
 */
function onSumInsuredChangeCIADDPlan(illustration_details,insuredType){
	if(illustration_details.Product.ProductDetails.productCode==20){
		if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Child')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),1000000000));
		}
		else if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Adult')>-1){
			if(insuredType == 'MaininsuredCIAddRider' || insuredType == 'SpouseAdditionalInsuredCIAddPlan'){
				illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),1500000000));
			}
			else{
				illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnHalfValue((illustration_details.Product.policyDetails.sumInsured),1000000000));
			}
		} 
	}
	else if(illustration_details.Product.ProductDetails.productCode==9) {
		if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Child')>-1) {
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),1000000000));
		}
		else if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Adult')>-1) {
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),1500000000));
		} 
	}
	else if(illustration_details.Product.ProductDetails.productCode==5){
		if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Child')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.singleBasicPremium),1000000000));
		}
		else if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Adult')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.singleBasicPremium),1500000000));
		}
	}
}
/**
 * sub function will be called from base function on change of main suminsured (ui input field).For MCI rider
 * @param illustration_details
 * @param insuredType
 */
function onSumInsuredChangeMCIPlan(illustration_details,insuredType){
	if(illustration_details.Product.ProductDetails.productCode==20){
		if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Child')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),1000000000));
		}
		else if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Adult')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),1500000000));
		} 
	}
	else if(illustration_details.Product.ProductDetails.productCode==9) {
		if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Child')>-1) {
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),1000000000));
		}
		else if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Adult')>-1) {
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),1500000000));
		} 
	}
}
/**
 * sub function will be called from base function on change of main suminsured (ui input field).For TERM LIFE rider
 * @param illustration_details
 * @param insuredType
 */
function onSumInsuredChangeTermLifePlan(illustration_details,insuredType){
	if(illustration_details.Product.ProductDetails.productCode==20){
		if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Child')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(illustration_details.Product.policyDetails.sumInsured);
		}
		else if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Adult')>-1){
			if(insuredType == 'SpouseAdditionalInsuredTermLifePlan' || insuredType == 'Parent1AdditionalInsuredTermLifePlan'  || insuredType == 'Parent2AdditionalInsuredTermLifePlan'){
				illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(illustration_details.Product.policyDetails.sumInsured);
			}
			else{
				illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnHalfValue((illustration_details.Product.policyDetails.sumInsured),5000000000));
			}
		} 
	}
}
/**
 * sub function will be called from base function on change of main suminsured (ui input field).For TPD rider
 * @param illustration_details
 * @param insuredType
 */
function onSumInsuredChangeTPDPlan(illustration_details,insuredType){
	if(illustration_details.Product.ProductDetails.productCode==20){
		if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Child')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),1000000000));
		}
		else if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Adult')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),5000000000));
		} 
	}
	else if(illustration_details.Product.ProductDetails.productCode==9) {
		if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Child')>-1) {
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),1000000000));
		}
		else if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Adult')>-1) {
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured),5000000000));
		} 
	}
}

/**
 * sub function will be called from base function on change of main suminsured (ui input field).For CI Buy Back rider
 * @param illustration_details
 * @param insuredType
 */
function onSumInsuredChangeCIBuyBackRider(illustration_details,insuredType){
	if(illustration_details.Product.ProductDetails.productCode==20){
		if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Child')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured*0.9),3000000000));
		}
		else if((illustration_details.Insured.BasicDetails.mainInsuredType).indexOf('Adult')>-1){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(returnLower((illustration_details.Product.policyDetails.sumInsured*0.9),5000000000));
		} 																																																		    
	}	
}

/**
 * Master function for sum insured change,Which wil again call the sub functions mentioned above
 * @param illustration_details
 * @param successCB
 */
function onSumInsuredChange(illustration_details,successCB){
	if(illustration_details.Product.ProductDetails.productCode==20){
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeADBPlan(illustration_details,'MaininsuredADBRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeADDPlan(illustration_details,'MaininsuredADDRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeADDPlan(illustration_details,'MaininsuredCIAccRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAddRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAddRider',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeCIADDPlan(illustration_details,'MaininsuredCIAddRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('SpouseAdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('SpouseAdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeCIADDPlan(illustration_details,'SpouseAdditionalInsuredCIAddPlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredMCIRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredMCIRider',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeMCIPlan(illustration_details,'MaininsuredMCIRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredTPDRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredTPDRider',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeTPDPlan(illustration_details,'MaininsuredTPDRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Child1AdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('Child1AdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeCIADDPlan(illustration_details,'Child1AdditionalInsuredCIAddPlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Child2AdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('Child2AdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeCIADDPlan(illustration_details,'Child2AdditionalInsuredCIAddPlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Child3AdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('Child3AdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeCIADDPlan(illustration_details,'Child3AdditionalInsuredCIAddPlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('SpouseAdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('SpouseAdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeTermLifePlan(illustration_details,'SpouseAdditionalInsuredTermLifePlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Child1AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('Child1AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeTermLifePlan(illustration_details,'Child1AdditionalInsuredTermLifePlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Child2AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('Child2AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeTermLifePlan(illustration_details,'Child2AdditionalInsuredTermLifePlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Child3AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('Child3AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeTermLifePlan(illustration_details,'Child3AdditionalInsuredTermLifePlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeTermLifePlan(illustration_details,'Parent1AdditionalInsuredTermLifePlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeTermLifePlan(illustration_details,'Parent2AdditionalInsuredTermLifePlan');
		}
	}else if(illustration_details.Product.ProductDetails.productCode==9){
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]]){
			onSumInsuredChangeADBPlan(illustration_details,'MaininsuredADBRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAddRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAddRider',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeCIADDPlan(illustration_details,'MaininsuredCIAddRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredMCIRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredMCIRider',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeMCIPlan(illustration_details,'MaininsuredMCIRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredTPDRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredTPDRider',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeTPDPlan(illustration_details,'MaininsuredTPDRider');
		}
	}
	else if(illustration_details.Product.ProductDetails.productCode==5){
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]]){
			onSumInsuredChangeADBPlan(illustration_details,'MaininsuredADBRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAddRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAddRider',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeCIADDPlan(illustration_details,'MaininsuredCIAddRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]].isRequired == 'No'){
			onSumInsuredChangeADDPlan(illustration_details,'MaininsuredADDRider');
		}
	}
	else{
		onSumInsuredChangeADBPlan(illustration_details,'MaininsuredADBRider');
		onSumInsuredChangeADDPlan(illustration_details,'MaininsuredADDRider');
		onSumInsuredChangeCIACCPlan(illustration_details,'MaininsuredCIAccRider');
		onSumInsuredChangeCIADDPlan(illustration_details,'MaininsuredCIAddRider');
	}
	successCB(illustration_details);
}
/**
 * 
 * New function added separate for iplan riders,adb,add,ciadd,ciacc riders
 * @param illustration_details
 * @param successCB
 */
function commonSumInsuredDataPopulation(illustration_details,successCB){
	onSumInsuredChangeADBPlan(illustration_details,'MaininsuredADBRider');
	onSumInsuredChangeADDPlan(illustration_details,'MaininsuredADDRider');
	onSumInsuredChangeCIACCPlan(illustration_details,'MaininsuredCIAccRider');
	onSumInsuredChangeCIADDPlan(illustration_details,'MaininsuredCIAddRider');
	
	if(illustration_details.Product.ProductDetails.productCode==20){
		onSumInsuredChangeCIADDPlan(illustration_details,'SpouseAdditionalInsuredCIAddPlan');
		onSumInsuredChangeCIADDPlan(illustration_details,'Child1AdditionalInsuredCIAddPlan');
		onSumInsuredChangeCIADDPlan(illustration_details,'Child2AdditionalInsuredCIAddPlan');
		onSumInsuredChangeCIADDPlan(illustration_details,'Child3AdditionalInsuredCIAddPlan');
		onSumInsuredChangeTermLifePlan(illustration_details,'SpouseAdditionalInsuredTermLifePlan');
		onSumInsuredChangeTermLifePlan(illustration_details,'Child1AdditionalInsuredTermLifePlan');
		onSumInsuredChangeTermLifePlan(illustration_details,'Child2AdditionalInsuredTermLifePlan');
		onSumInsuredChangeTermLifePlan(illustration_details,'Child3AdditionalInsuredTermLifePlan');
		onSumInsuredChangeTermLifePlan(illustration_details,'Parent1AdditionalInsuredTermLifePlan');
		onSumInsuredChangeTermLifePlan(illustration_details,'Parent2AdditionalInsuredTermLifePlan');
		onSumInsuredChangeMCIPlan(illustration_details,'MaininsuredMCIRider');
		onSumInsuredChangeTPDPlan(illustration_details,'MaininsuredTPDRider');
		onSumInsuredChangeCIBuyBackRider(illustration_details,'MaininsuredCIBuyBackRider');
	}
	
	if(illustration_details.Product.ProductDetails.productCode==5){
		onSumInsuredChangeADBPlan(illustration_details,'MaininsuredADBRider');
		onSumInsuredChangeADDPlan(illustration_details,'MaininsuredADDRider');
		onSumInsuredChangeCIADDPlan(illustration_details,'MaininsuredCIAddRider');
	}
	
	if(illustration_details.Product.ProductDetails.productCode==9){
		onSumInsuredChangeMCIPlan(illustration_details,'MaininsuredMCIRider');
		onSumInsuredChangeTPDPlan(illustration_details,'MaininsuredTPDRider');
	}
	successCB(illustration_details);
}
/**
 * Master function for sum insured change
 * @param illustration_details
 * @param successCB
 */
function onSumInsuredChangeiPlan(illustration_details,successCB){
	if(illustration_details.Product.ProductDetails.productCode==20){
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]] &&illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeADBPlan(illustration_details,'MaininsuredADBRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeADDPlan(illustration_details,'MaininsuredADDRider');
	
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeCIACCPlan(illustration_details,'MaininsuredCIAccRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAddRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAddRider',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAddRider',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeCIADDPlan(illustration_details,'MaininsuredCIAddRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredMCIRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredMCIRider',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredMCIRider',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeMCIPlan(illustration_details,'MaininsuredMCIRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredTPDRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredTPDRider',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredTPDRider',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeTPDPlan(illustration_details,'MaininsuredTPDRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('SpouseAdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('SpouseAdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('SpouseAdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeCIADDPlan(illustration_details,'SpouseAdditionalInsuredCIAddPlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Child1AdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('Child1AdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('Child1AdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeCIADDPlan(illustration_details,'Child1AdditionalInsuredCIAddPlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Child2AdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('Child2AdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('Child2AdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeCIADDPlan(illustration_details,'Child2AdditionalInsuredCIAddPlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Child3AdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('Child3AdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('Child3AdditionalInsuredCIAddPlan',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeCIADDPlan(illustration_details,'Child3AdditionalInsuredCIAddPlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('SpouseAdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('SpouseAdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('SpouseAdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeTermLifePlan(illustration_details,'SpouseAdditionalInsuredTermLifePlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Child1AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('Child1AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('Child1AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeTermLifePlan(illustration_details,'Child1AdditionalInsuredTermLifePlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Child2AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('Child2AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('Child2AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeTermLifePlan(illustration_details,'Child2AdditionalInsuredTermLifePlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Child3AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('Child3AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('Child3AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeTermLifePlan(illustration_details,'Child3AdditionalInsuredTermLifePlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeTermLifePlan(illustration_details,'Parent1AdditionalInsuredTermLifePlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredTermLifePlan',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeTermLifePlan(illustration_details,'Parent2AdditionalInsuredTermLifePlan');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIBuyBackRider',illustration_details.Product.RiderDetails)]] &&illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIBuyBackRider',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIBuyBackRider',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeCIBuyBackRider(illustration_details,'MaininsuredCIBuyBackRider');
		}
	}
	else if(illustration_details.Product.ProductDetails.productCode==9){
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAddRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAddRider',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAddRider',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeCIADDPlan(illustration_details,'MaininsuredCIAddRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredMCIRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredMCIRider',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredMCIRider',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeMCIPlan(illustration_details,'MaininsuredMCIRider');
		}
		if(illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredTPDRider',illustration_details.Product.RiderDetails)]] && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredTPDRider',illustration_details.Product.RiderDetails)]].isRequired=='Yes' && illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredTPDRider',illustration_details.Product.RiderDetails)]].sumInsured == ""){
			onSumInsuredChangeTPDPlan(illustration_details,'MaininsuredTPDRider');
		}
	}
	else if(illustration_details.Product.ProductDetails.productCode==5){
		onSumInsuredChangeADBPlan(illustration_details,'MaininsuredADBRider');
		onSumInsuredChangeADDPlan(illustration_details,'MaininsuredADDRider');
		onSumInsuredChangeCIADDPlan(illustration_details,'MaininsuredCIAddRider');
	}
	successCB(illustration_details);
}

/**
 * Master function for regular premium change
 * @param illustration_details
 * @param successCB
 */
function onRegularPremiumChange(illustration_details,successCB){
	var premiumFrequency=returnPremiumFrequency(illustration_details.Product.policyDetails.premiumFrequency);
	onWOPBasicPlan(illustration_details,'MaininsuredWOPRider',premiumFrequency);
	onWOPFullPlan(illustration_details,'MaininsuredWOPRider',premiumFrequency);
	
	onWOPBasicPlan(illustration_details,'SpouseAdditionalInsuredWaiverPlan',premiumFrequency);
	onWOPFullPlan(illustration_details,'SpouseAdditionalInsuredWaiverPlan',premiumFrequency);
	
	onWOPBasicPlan(illustration_details,'SpouseAdditionalInsuredSurvivorPlan',premiumFrequency);
	onWOPFullPlan(illustration_details,'SpouseAdditionalInsuredSurvivorPlan',premiumFrequency);
	
	onParentSurvivor(illustration_details,'Parent1AdditionalInsuredSurvivorPlan',premiumFrequency);
	onParentSurvivor(illustration_details,'Parent2AdditionalInsuredSurvivorPlan',premiumFrequency);
	onParentwaivor(illustration_details,'Parent1AdditionalInsuredWaiverPlan',premiumFrequency);
	onParentwaivor(illustration_details,'Parent2AdditionalInsuredWaiverPlan',premiumFrequency);
	successCB(illustration_details);
}
/**
 * MAster function for handling health/plantype change
 * @param illustration_details
 * @param successCB
 */
function onPlanTypeSurvivorChangeSpouse(illustration_details,successCB){
	var premiumFrequency=returnPremiumFrequency(illustration_details.Product.policyDetails.premiumFrequency);
	onWOPFullPlan(illustration_details,'SpouseAdditionalInsuredSurvivorPlan',premiumFrequency);
	onWOPBasicPlan(illustration_details,'SpouseAdditionalInsuredSurvivorPlan',premiumFrequency);
	successCB(illustration_details);
}
function onPlanTypeWaivorChangeSpouse(illustration_details,successCB){
	var premiumFrequency=returnPremiumFrequency(illustration_details.Product.policyDetails.premiumFrequency);
	onWOPFullPlan(illustration_details,'SpouseAdditionalInsuredWaiverPlan',premiumFrequency);
	onWOPBasicPlan(illustration_details,'SpouseAdditionalInsuredWaiverPlan',premiumFrequency);
	successCB(illustration_details);
}

function onPlanTypeWaivorChangeMain(illustration_details,successCB){
	var premiumFrequency=returnPremiumFrequency(illustration_details.Product.policyDetails.premiumFrequency);
	onWOPFullPlan(illustration_details,'MaininsuredWOPRider',premiumFrequency);
	onWOPBasicPlan(illustration_details,'MaininsuredWOPRider',premiumFrequency);
	successCB(illustration_details);
}
/**
 * the below 4 functions are written for parent1 and parent2(survivor and waivor) suminsured and maxcoverage age prepopulation
 * @param illustration_details
 * @param successCB
 */
function onPlanTypeWaivorChangeParent0(illustration_details,successCB){
	if(illustration_details.Product.ProductDetails.productCode==20){
		var premiumFrequency=returnPremiumFrequency(illustration_details.Product.policyDetails.premiumFrequency);
		illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].sumInsured=(Number(illustration_details.Product.policyDetails.regularPremium)+Number(illustration_details.Product.policyDetails.regularTopUp))*premiumFrequency;
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Plan 18'){
			if((70-illustration_details.AdditionalInsured[0].BasicDetails.age) >(18-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].maxCoverage=(18-illustration_details.Insured.BasicDetails.age)+illustration_details.AdditionalInsured[0].BasicDetails.age;
			}
			else if((70-illustration_details.AdditionalInsured[0].BasicDetails.age) <(18-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].maxCoverage=70;
			}
		}
		else if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Plan 25'){
			if((70-illustration_details.AdditionalInsured[0].BasicDetails.age) >(25-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].maxCoverage=(25-illustration_details.Insured.BasicDetails.age)+illustration_details.AdditionalInsured[0].BasicDetails.age;
			}
			else if((70-illustration_details.AdditionalInsured[0].BasicDetails.age) <(25-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].maxCoverage=70;
			}
		}
	}
	else if(illustration_details.Product.ProductDetails.productCode==9){
		var premiumFrequency=returnPremiumFrequency(illustration_details.Product.policyDetails.premiumFrequency);
		if(illustration_details.Product.policyDetails.regularTopUp==undefined){
			illustration_details.Product.policyDetails.regularTopUp='';
		}
		illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].sumInsured=(Number(illustration_details.Product.policyDetails.regularPremium)+Number(illustration_details.Product.policyDetails.regularTopUp))*premiumFrequency;
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Plan 18'){
			if((70-illustration_details.AdditionalInsured[0].BasicDetails.age) >(18-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].maxCoverage=(18-illustration_details.Insured.BasicDetails.age)+illustration_details.AdditionalInsured[0].BasicDetails.age;
			}
			else if((70-illustration_details.AdditionalInsured[0].BasicDetails.age) <(18-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].maxCoverage=70;
			}
		}
		else if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Plan 25'){
			if((70-illustration_details.AdditionalInsured[0].BasicDetails.age) >(25-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].maxCoverage=(25-illustration_details.Insured.BasicDetails.age)+illustration_details.AdditionalInsured[0].BasicDetails.age;
			}
			else if((70-illustration_details.AdditionalInsured[0].BasicDetails.age) <(25-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].maxCoverage=70;
			}
		}
	}
	successCB(illustration_details);
}
function onPlanTypeWaivorChangeParent1(illustration_details,successCB){
	if(illustration_details.Product.ProductDetails.productCode==20){
		var premiumFrequency=returnPremiumFrequency(illustration_details.Product.policyDetails.premiumFrequency);
		illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].sumInsured=(Number(illustration_details.Product.policyDetails.regularPremium)+Number(illustration_details.Product.policyDetails.regularTopUp))*premiumFrequency;
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Plan 18'){
			if((70-illustration_details.AdditionalInsured[1].BasicDetails.age) >(18-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].maxCoverage=(18-illustration_details.Insured.BasicDetails.age)+illustration_details.AdditionalInsured[1].BasicDetails.age;
			}
			else if((70-illustration_details.AdditionalInsured[1].BasicDetails.age) <(18-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].maxCoverage=70;
			}
		}
		else if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Plan 25'){
			if((70-illustration_details.AdditionalInsured[1].BasicDetails.age) >(25-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].maxCoverage=(25-illustration_details.Insured.BasicDetails.age)+illustration_details.AdditionalInsured[1].BasicDetails.age;
			}
			else if((70-illustration_details.AdditionalInsured[1].BasicDetails.age) <(25-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].maxCoverage=70;
			}
		}
	}
	else if(illustration_details.Product.ProductDetails.productCode==9){
		if(illustration_details.Product.policyDetails.regularTopUp==undefined){
			illustration_details.Product.policyDetails.regularTopUp='';
		}
		var premiumFrequency=returnPremiumFrequency(illustration_details.Product.policyDetails.premiumFrequency);
		illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].sumInsured=(Number(illustration_details.Product.policyDetails.regularPremium)+Number(illustration_details.Product.policyDetails.regularTopUp))*premiumFrequency;
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Plan 18'){
			if((70-illustration_details.AdditionalInsured[1].BasicDetails.age) >(18-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].maxCoverage=(18-illustration_details.Insured.BasicDetails.age)+illustration_details.AdditionalInsured[1].BasicDetails.age;
			}
			else if((70-illustration_details.AdditionalInsured[1].BasicDetails.age) <(18-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].maxCoverage=70;
			}
		}
		else if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Plan 25'){
			if((70-illustration_details.AdditionalInsured[1].BasicDetails.age) >(25-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].maxCoverage=(25-illustration_details.Insured.BasicDetails.age)+illustration_details.AdditionalInsured[1].BasicDetails.age;
			}
			else if((70-illustration_details.AdditionalInsured[1].BasicDetails.age) <(25-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredWaiverPlan',illustration_details.Product.RiderDetails)]].maxCoverage=70;
			}
		}
	}
	successCB(illustration_details);
}
function onPlanTypeSurvivorChangeParent0(illustration_details,successCB){
	if(illustration_details.Product.ProductDetails.productCode==20){
		var premiumFrequency=returnPremiumFrequency(illustration_details.Product.policyDetails.premiumFrequency);
		illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].sumInsured=(Number(illustration_details.Product.policyDetails.regularPremium)+Number(illustration_details.Product.policyDetails.regularTopUp))*premiumFrequency;
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Plan 18'){
			if((70-illustration_details.AdditionalInsured[0].BasicDetails.age) >(18-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].maxCoverage=(18-illustration_details.Insured.BasicDetails.age)+illustration_details.AdditionalInsured[0].BasicDetails.age;
			}
			else if((70-illustration_details.AdditionalInsured[0].BasicDetails.age) <(18-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].maxCoverage=70;
			}
		}
		else if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Plan 25'){
			if((70-illustration_details.AdditionalInsured[0].BasicDetails.age) >(25-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].maxCoverage=(25-illustration_details.Insured.BasicDetails.age)+illustration_details.AdditionalInsured[0].BasicDetails.age;
			}
			else if((70-illustration_details.AdditionalInsured[0].BasicDetails.age) <(25-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].maxCoverage=70;
			}
		}
	}
	else if(illustration_details.Product.ProductDetails.productCode==9) {
		if(illustration_details.Product.policyDetails.regularTopUp==undefined){
			illustration_details.Product.policyDetails.regularTopUp='';
		}
		var premiumFrequency=returnPremiumFrequency(illustration_details.Product.policyDetails.premiumFrequency);
		illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].sumInsured=(Number(illustration_details.Product.policyDetails.regularPremium)+Number(illustration_details.Product.policyDetails.regularTopUp))*premiumFrequency;
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Plan 18'){
			if((70-illustration_details.AdditionalInsured[0].BasicDetails.age) >(18-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].maxCoverage=(18-illustration_details.Insured.BasicDetails.age)+illustration_details.AdditionalInsured[0].BasicDetails.age;
			}
			else if((70-illustration_details.AdditionalInsured[0].BasicDetails.age) <(18-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].maxCoverage=70;
			}
		}
		else if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Plan 25'){
			if((70-illustration_details.AdditionalInsured[0].BasicDetails.age) >(25-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].maxCoverage=(25-illustration_details.Insured.BasicDetails.age)+illustration_details.AdditionalInsured[0].BasicDetails.age;
			}
			else if((70-illustration_details.AdditionalInsured[0].BasicDetails.age) <(25-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent1AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].maxCoverage=70;
			}
		}
	}
	successCB(illustration_details);
}
function onPlanTypeSurvivorChangeParent1(illustration_details,successCB){
	if(illustration_details.Product.ProductDetails.productCode==20){
		var premiumFrequency=returnPremiumFrequency(illustration_details.Product.policyDetails.premiumFrequency);
		illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].sumInsured=(Number(illustration_details.Product.policyDetails.regularPremium)+Number(illustration_details.Product.policyDetails.regularTopUp))*premiumFrequency;
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Plan 18'){
			if((70-illustration_details.AdditionalInsured[1].BasicDetails.age) >(18-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].maxCoverage=(18-illustration_details.Insured.BasicDetails.age)+illustration_details.AdditionalInsured[1].BasicDetails.age;
			}
			else if((70-illustration_details.AdditionalInsured[1].BasicDetails.age) <(18-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].maxCoverage=70;
			}
		}
		else if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Plan 25'){
			if((70-illustration_details.AdditionalInsured[1].BasicDetails.age) >(25-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].maxCoverage=(25-illustration_details.Insured.BasicDetails.age)+illustration_details.AdditionalInsured[1].BasicDetails.age;
			}
			else if((70-illustration_details.AdditionalInsured[1].BasicDetails.age) <(25-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].maxCoverage=70;
			}
		}
	}
	else if(illustration_details.Product.ProductDetails.productCode==9){
		if(illustration_details.Product.policyDetails.regularTopUp==undefined){
			illustration_details.Product.policyDetails.regularTopUp='';
		}
		var premiumFrequency=returnPremiumFrequency(illustration_details.Product.policyDetails.premiumFrequency);
		illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].sumInsured=(Number(illustration_details.Product.policyDetails.regularPremium)+Number(illustration_details.Product.policyDetails.regularTopUp))*premiumFrequency;
		if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Plan 18'){
			if((70-illustration_details.AdditionalInsured[1].BasicDetails.age) >(18-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].maxCoverage=(18-illustration_details.Insured.BasicDetails.age)+illustration_details.AdditionalInsured[1].BasicDetails.age;
			}
			else if((70-illustration_details.AdditionalInsured[1].BasicDetails.age) <(18-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].maxCoverage=70;
			}
		}
		else if(illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Plan 25'){
			if((70-illustration_details.AdditionalInsured[1].BasicDetails.age) >(25-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].maxCoverage=(25-illustration_details.Insured.BasicDetails.age)+illustration_details.AdditionalInsured[1].BasicDetails.age;
			}
			else if((70-illustration_details.AdditionalInsured[1].BasicDetails.age) <(25-illustration_details.Insured.BasicDetails.age)){
				illustration_details.Product.RiderDetails[[getRiderIndex('Parent2AdditionalInsuredSurvivorPlan',illustration_details.Product.RiderDetails)]].maxCoverage=70;
			}
		}
	}
	successCB(illustration_details);
}

function onParentSurvivor(illustration_details,insuredType,premiumFrequency){
	if(illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].isRequired=="Yes"){
		if(illustration_details.Product.ProductDetails.productCode==20){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=(Number(illustration_details.Product.policyDetails.regularPremium)+Number(illustration_details.Product.policyDetails.regularTopUp))*premiumFrequency;
		}
		else if(illustration_details.Product.ProductDetails.productCode==9){
			if(illustration_details.Product.policyDetails.regularTopUp==undefined){
				illustration_details.Product.policyDetails.regularTopUp='';
			}
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=(Number(illustration_details.Product.policyDetails.regularPremium)+Number(illustration_details.Product.policyDetails.regularTopUp))*premiumFrequency;
		}
	}
}
function onParentwaivor(illustration_details,insuredType,premiumFrequency){
	if(illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].isRequired=="Yes"){
		if(illustration_details.Product.ProductDetails.productCode==20){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=(Number(illustration_details.Product.policyDetails.regularPremium)+Number(illustration_details.Product.policyDetails.regularTopUp))*premiumFrequency;
		}
		if(illustration_details.Product.ProductDetails.productCode==9){
			if(illustration_details.Product.policyDetails.regularTopUp==undefined){
				illustration_details.Product.policyDetails.regularTopUp='';
			}
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=(Number(illustration_details.Product.policyDetails.regularPremium)+Number(illustration_details.Product.policyDetails.regularTopUp))*premiumFrequency;
		}
	}
}
function onWOPBasicPlan(illustration_details,insuredType,premiumFrequency){
	if(illustration_details.Product.ProductDetails.productCode==20){
		if(illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Basic'){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(illustration_details.Product.policyDetails.regularPremium)*premiumFrequency;
		}
	}
	else if(illustration_details.Product.ProductDetails.productCode==9){
		if(illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Basic'){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=Number(illustration_details.Product.policyDetails.regularPremium)*premiumFrequency;
		}
	}
}
function onWOPFullPlan(illustration_details,insuredType,premiumFrequency){
	if(illustration_details.Product.ProductDetails.productCode==20){
		if(illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Full'){
			illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=(Number(illustration_details.Product.policyDetails.regularPremium)+Number(illustration_details.Product.policyDetails.regularTopUp))*premiumFrequency;
		}
	}
	else if(illustration_details.Product.ProductDetails.productCode==9){
		if(illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].healthUnitsOrPlanType=='Full'){
			if(illustration_details.Product.policyDetails.regularTopUp != undefined){
				illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=(Number(illustration_details.Product.policyDetails.regularPremium)+Number(illustration_details.Product.policyDetails.regularTopUp))*premiumFrequency;
			}
			else{
				illustration_details.Product.policyDetails.regularTopUp = 0;
				illustration_details.Product.RiderDetails[[getRiderIndex(insuredType,illustration_details.Product.RiderDetails)]].sumInsured=(Number(illustration_details.Product.policyDetails.regularPremium)+Number(illustration_details.Product.policyDetails.regularTopUp))*premiumFrequency;
			}
		}
	}
}

function onPackageOptionChange(illustration_details,successCB){
	if(illustration_details.Product.ProductDetails.productCode==1){
		if((illustration_details.Product.policyDetails.packageOption).indexOf('Package I -')>-1){
			if(illustration_details.Insured.BasicDetails.age>=0 && illustration_details.Insured.BasicDetails.age<=40 ){
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]].sumInsured=25000000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]].sumInsured=20000000;
			}
			else if(illustration_details.Insured.BasicDetails.age>=41 && illustration_details.Insured.BasicDetails.age<=50){
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]].sumInsured=15000000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]].sumInsured=10000000;
			}
			else if(illustration_details.Insured.BasicDetails.age>=51 && illustration_details.Insured.BasicDetails.age<=60){
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]].sumInsured=10000000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]].sumInsured=7500000;
			}
		}
		else if((illustration_details.Product.policyDetails.packageOption).indexOf('Package II -')>-1){
			if(illustration_details.Insured.BasicDetails.age>=0 && illustration_details.Insured.BasicDetails.age<=40){
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]].sumInsured=35000000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]].sumInsured=30000000;
			}
			else if(illustration_details.Insured.BasicDetails.age>=41 && illustration_details.Insured.BasicDetails.age<=50){
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]].sumInsured=20000000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]].sumInsured=15000000;
			}
			else if(illustration_details.Insured.BasicDetails.age>=51 && illustration_details.Insured.BasicDetails.age<=60){
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]].sumInsured=15000000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]].sumInsured=10000000;
			}
		}
		else if((illustration_details.Product.policyDetails.packageOption).indexOf('Package III -')>-1){
			if(illustration_details.Insured.BasicDetails.age>=0 && illustration_details.Insured.BasicDetails.age<=40){
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]].sumInsured=50000000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]].sumInsured=40000000;
			}
			else if(illustration_details.Insured.BasicDetails.age>=41 && illustration_details.Insured.BasicDetails.age<=50){
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]].sumInsured=30000000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]].sumInsured=20000000;
			}
			else if(illustration_details.Insured.BasicDetails.age>=51 && illustration_details.Insured.BasicDetails.age<=60){
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]].sumInsured=20000000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]].sumInsured=15000000;
			}
		}
		else if((illustration_details.Product.policyDetails.packageOption).indexOf('Package IV -')>-1){
			if(illustration_details.Insured.BasicDetails.age>=0 && illustration_details.Insured.BasicDetails.age<=40){
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]].sumInsured=65000000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]].sumInsured=60000000;
			}
			else if(illustration_details.Insured.BasicDetails.age>=41 && illustration_details.Insured.BasicDetails.age<=50){
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]].sumInsured=35000000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]].sumInsured=25000000;
			}
			else if(illustration_details.Insured.BasicDetails.age>=51 && illustration_details.Insured.BasicDetails.age<=60){
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADBRider',illustration_details.Product.RiderDetails)]].sumInsured=25000000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIAccRider',illustration_details.Product.RiderDetails)]].sumInsured=20000000;
			}
		}
	}
	successCB(illustration_details);
}
function onWOPPlanChange(selectedValue,successCB){
	var output={"maxCoverageAge":"","sumInsured":""};
	if(selectedValue=="Plan 55"){
		output.maxCoverageAge=55;
	}
	else if(selectedValue=="Plan 65"){
		output.maxCoverageAge=65;
	}
	else if(selectedValue=="Plan 75"){
		output.maxCoverageAge=75;
	}
	else{
		output.maxCoverageAge='';
	}
	successCB(output);
}

function onSurvivorPlanChange(selectedValue,successCB){
	var output={"maxCoverageAge":"","sumInsured":""};
	if(selectedValue=="Plan 55"){
		output.maxCoverageAge=55;
	}
	else if(selectedValue=="Plan 65"){
		output.maxCoverageAge=65;
	}
	else if(selectedValue=="Plan 75"){
		output.maxCoverageAge=75;
	}
	else if(selectedValue=="Plan 18"){
		output.maxCoverageAge=18;
	}
	else if(selectedValue=="Plan 25"){
		output.maxCoverageAge=25;
	}
	else{
		output.maxCoverageAge='';
	}
	successCB(output);
}

function onWaivorPlanChange(selectedValue,successCB){
	var output={"maxCoverageAge":"","sumInsured":""};
	if(selectedValue=="Plan 55"){
		output.maxCoverageAge=55;
	}
	else if(selectedValue=="Plan 65"){
		output.maxCoverageAge=65;
	}
	else if(selectedValue=="Plan 75"){
		output.maxCoverageAge=75;
	}
	else if(selectedValue=="Plan 18"){
		output.maxCoverageAge=18;
	}
	else if(selectedValue=="Plan 25"){
		output.maxCoverageAge=25;
	}
	else{
		output.maxCoverageAge='';
	}
	successCB(output);
}

function onMedicalPlanChange(selectedValue,successCB){
	var output={"maxCoverageAge":"","sumInsured":""};
	if(selectedValue=="Plan1"){
		output.sumInsured=250000;
	}
	else if(selectedValue=="Plan2"){
		output.sumInsured=500000;
	}
	else if(selectedValue=="Plan3"){
		output.sumInsured=750000;
	}
	else if(selectedValue=="Plan4"){
		output.sumInsured=1000000;
	}
	else if(selectedValue=="Plan5"){
		output.sumInsured=1500000;
	}
	else if(selectedValue=="Plan6"){
		output.sumInsured=2000000;
	}
	else{
		output.sumInsured='';
	}
	successCB(output);
}

function onHealthPlanChange(selectedValue,successCB){
	var output={"maxCoverageAge":"","sumInsured":""};
	if(selectedValue!=''){
		output.sumInsured=Number(selectedValue)*50000;
	}
	else{
		output.sumInsured='';
	}
	successCB(output);
}

/* Added for Thailand*/
function onPackageChange(illustration_details,successCB){
	if(illustration_details.Product.ProductDetails.productId == 1010 && illustration_details.Product.ProductDetails.productCode == 10){
		if((illustration_details.Product.policyDetails.packages).indexOf('Package A')>-1){
				illustration_details.Product.policyDetails.sumInsured=200000;
            illustration_details.Product.policyDetails.premium=illustration_details.Product.policyDetails.premium;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIRider',illustration_details.Product.RiderDetails)]].sumInsured=200000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]].sumInsured=200000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHSRider',illustration_details.Product.RiderDetails)]].sumInsured=2000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHBRider',illustration_details.Product.RiderDetails)]].sumInsured=2000;
		}
		else if((illustration_details.Product.policyDetails.packages).indexOf('Package B')>-1){
			illustration_details.Product.policyDetails.sumInsured=500000;			
			
			illustration_details.Product.policyDetails.premium=illustration_details.Product.policyDetails.premium;
			illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIRider',illustration_details.Product.RiderDetails)]].sumInsured=500000;
			illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]].sumInsured=500000;
			illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHSRider',illustration_details.Product.RiderDetails)]].sumInsured=5000;
			illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHBRider',illustration_details.Product.RiderDetails)]].sumInsured=5000;
		}
	}
	successCB(illustration_details);
}
/**
 * 
 * @param string_frequency
 * @returns {Number}
 */
function returnPremiumFrequency(string_frequency){
	if(string_frequency=="Annual"){
		return 1;
	}
	else if(string_frequency=="Semi Annual"){
		return 2;
	}
	else if(string_frequency=="Quarterly"){
		return 3;
	}
	else if(string_frequency=="Monthly"){
		return 4;
	}
}

/**
 * Master function for regular premium change
 * @param illustration_details
 * @param successCB
 */
function onRegularFrequencyChange(illustration_details,successCB){
	var premiumFrequency=returnPremiumFrequency(illustration_details.Product.policyDetails.premiumFrequency);
	onPremiumChangeForComplete(illustration_details,premiumFrequency);
	successCB(illustration_details);
}

function onPremiumChangeForComplete(illustration_details, premiumFrequency){
	if(illustration_details.Product.ProductDetails.productId == 1010 && illustration_details.Product.ProductDetails.productCode == 10){
		if(premiumFrequency==1){
			if((illustration_details.Product.policyDetails.packages).indexOf('Package A')>-1){
				illustration_details.Product.policyDetails.sumInsured=200000;
				
				illustration_details.Product.premiumSummary.summaryMinSA = 200000;
				illustration_details.Product.premiumSummary.summaryMaxSA = 500000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIRider',illustration_details.Product.RiderDetails)]].sumInsured=200000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]].sumInsured=200000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHSRider',illustration_details.Product.RiderDetails)]].sumInsured=2000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHBRider',illustration_details.Product.RiderDetails)]].sumInsured=2000;
			} else if((illustration_details.Product.policyDetails.packages).indexOf('Package B')>-1){
				illustration_details.Product.policyDetails.sumInsured=500000;
				
				illustration_details.Product.premiumSummary.summaryMinSA = 200000;
				illustration_details.Product.premiumSummary.summaryMaxSA = 500000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIRider',illustration_details.Product.RiderDetails)]].sumInsured=500000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]].sumInsured=500000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHSRider',illustration_details.Product.RiderDetails)]].sumInsured=5000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHBRider',illustration_details.Product.RiderDetails)]].sumInsured=5000;
			}
		} else if(premiumFrequency==2){
			if((illustration_details.Product.policyDetails.packages).indexOf('Package A')>-1){
				illustration_details.Product.policyDetails.sumInsured=200000;
				illustration_details.Product.policyDetails.premium=2080;
				illustration_details.Product.premiumSummary.summaryMinSA = 200000;
				illustration_details.Product.premiumSummary.summaryMaxSA = 500000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIRider',illustration_details.Product.RiderDetails)]].sumInsured=200000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]].sumInsured=200000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHSRider',illustration_details.Product.RiderDetails)]].sumInsured=2000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHBRider',illustration_details.Product.RiderDetails)]].sumInsured=2000;
			} else if((illustration_details.Product.policyDetails.packages).indexOf('Package B')>-1){
				illustration_details.Product.policyDetails.sumInsured=500000;
				illustration_details.Product.policyDetails.premium=5200;
				illustration_details.Product.premiumSummary.summaryMinSA = 200000;
				illustration_details.Product.premiumSummary.summaryMaxSA = 500000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIRider',illustration_details.Product.RiderDetails)]].sumInsured=500000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]].sumInsured=500000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHSRider',illustration_details.Product.RiderDetails)]].sumInsured=5000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHBRider',illustration_details.Product.RiderDetails)]].sumInsured=5000;
			}
		} else if(premiumFrequency==3){
			if((illustration_details.Product.policyDetails.packages).indexOf('Package A')>-1){
				illustration_details.Product.policyDetails.sumInsured=200000;
				illustration_details.Product.policyDetails.premium=1080;
				illustration_details.Product.premiumSummary.summaryMinSA = 200000;
				illustration_details.Product.premiumSummary.summaryMaxSA = 500000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIRider',illustration_details.Product.RiderDetails)]].sumInsured=200000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]].sumInsured=200000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHSRider',illustration_details.Product.RiderDetails)]].sumInsured=2000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHBRider',illustration_details.Product.RiderDetails)]].sumInsured=2000;
			} else if((illustration_details.Product.policyDetails.packages).indexOf('Package B')>-1){
				illustration_details.Product.policyDetails.sumInsured=500000;
				illustration_details.Product.policyDetails.premium=2700;
				illustration_details.Product.premiumSummary.summaryMinSA = 200000;
				illustration_details.Product.premiumSummary.summaryMaxSA = 500000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIRider',illustration_details.Product.RiderDetails)]].sumInsured=500000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]].sumInsured=500000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHSRider',illustration_details.Product.RiderDetails)]].sumInsured=5000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHBRider',illustration_details.Product.RiderDetails)]].sumInsured=5000;
			}
		} else if(premiumFrequency==4){
			if((illustration_details.Product.policyDetails.packages).indexOf('Package A')>-1){
				illustration_details.Product.policyDetails.sumInsured=200000;
				illustration_details.Product.policyDetails.premium=360;
				illustration_details.Product.premiumSummary.summaryMinSA = 200000;
				illustration_details.Product.premiumSummary.summaryMaxSA = 500000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIRider',illustration_details.Product.RiderDetails)]].sumInsured=200000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]].sumInsured=200000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHSRider',illustration_details.Product.RiderDetails)]].sumInsured=2000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHBRider',illustration_details.Product.RiderDetails)]].sumInsured=2000;
			} else if((illustration_details.Product.policyDetails.packages).indexOf('Package B')>-1){
				illustration_details.Product.policyDetails.sumInsured=500000;
				illustration_details.Product.policyDetails.premium=900;
				illustration_details.Product.premiumSummary.summaryMinSA = 200000;
				illustration_details.Product.premiumSummary.summaryMaxSA = 500000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredCIRider',illustration_details.Product.RiderDetails)]].sumInsured=500000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredADDRider',illustration_details.Product.RiderDetails)]].sumInsured=500000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHSRider',illustration_details.Product.RiderDetails)]].sumInsured=5000;
				illustration_details.Product.RiderDetails[[getRiderIndex('MaininsuredHBRider',illustration_details.Product.RiderDetails)]].sumInsured=5000;
			}
		}
	}

}