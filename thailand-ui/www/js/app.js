/*
 *Copyright 2015, LifeEngage 
 */

var resources;
var storeApp = angular.module('lifeEngage', [ 'smart-table','ngRoute','ngResource','ui.bootstrap',
		'lifeEngage.RuleService','lifeEngage.UserDetailsService', 'lifeEngage.DataService',
		'lifeEngage.DocumentService', 'lifeEngage.LookupService',
		'lifeEngage.blur', 'lifeEngage.payment', 'lifeEngage.filters',
		'pascalprecht.translate', 'pasvaz.bindonce','lifeEngage.ProductService',
		'lifeEngage.AuthService', 'lifeEngage.UtilityService','lifeEngage.SyncService',
		'lifeEngage.FnaVariables', 'lifeEngage.GLI_FnaVariables', 'lifeEngage.LmsVariables',
		'lifeEngage.LmsService', 'lifeEngage.FnaService', 'easypiechart',
		'lifeEngage.observation', 'lifeEngage.UtilityVariables',
		'lifeEngage.directives', 'lifeEngage.debounce','lifeEngage.gliFilter','lifeEngage.glidirective',
		'lifeEngage.IllustratorVariables', 'lifeEngage.IllustratorService',
		'lifeEngage.PersistenceMapping', 'lifeEngage.AutoSave',
		'lifeEngage.GLI_DataService','lifeEngage.GLI_LmsService','lifeEngage.GLI_FnaService',
		'lifeEngage.globalService','lifeEngage.EappVariables','lifeEngage.EappService','lifeEngage.DataLookupService','lifeEngage.LoginService','lifeEngage.GLI_LoginService',
		'lifeEngage.GLI_globalService',
		'lifeEngage.GLI_RuleService',
		'lifeEngage.GLI_EappService',
		'lifeEngage.GLI_EappVariables','lifeEngage.PaymentService',
		'lifeEngage.GLI_IllustratorVariables','lifeEngage.GLI_IllustratorService','lifeEngage.AgentService','lifeEngage.MediaService','ng-iscroll','lifeEngage.RequirementService','lifeEngage.PushNotificationService','anguFixedHeaderTable','lifeEngage.EmailService',
		'lifeEngage.TemplateService','lifeEngage.PlatformInfoService','pascalprecht.translate','ngTouch','lifeEngage.GLI_LmsVariables','lifeEngage.GLI_DataLookupService','lifeEngage.GLI_EvaluateService','ngIOS9UIWebViewPatch','lifeEngage.GeoFenceService', 'lifeEngage.DataWipeService','infinite-scroll','lifeEngage.SslCertificate'], function($interpolateProvider) {
	$interpolateProvider.startSymbol('[[');
	$interpolateProvider.endSymbol(']]');
});

/*
 * routeProvider set the partial routing
 */
storeApp
		.config(
				[
						'$routeProvider',
						'$httpProvider',
						function($routeProvider, $httpProvider) {
							$routeProvider
									.
									// Need to uncomment once the home page is
									// done
									// when('/', { templateUrl:
									// 'modules/eApp/partials/Home.html',
									// controller: HomeController }).
									// remove this routing once the home page is
									// done
									when(
											'/',
											{
												templateUrl : 'modules/login/partials/MyAccount.html',
												controller : 'GLI_MyAccountController'
											})
									.when(
											'/login',
											{
												templateUrl : 'modules/login/partials/login.html',
												controller : 'GLI_LoginController'
											})
                                    .when(
											'/customerregistration',
											{
												templateUrl : 'modules/login/partials/registration.html',
												controller : 'CustomerRegistrationCtrl'
											})                                   
 									.when(
											'/forgotpassword',
											{
												templateUrl : 'modules/login/partials/forgotpassword.html',
												controller : 'ForgotPasswordCtrl'
											})
									.when(
											'/home',
											{
												templateUrl : 'modules/eApp/partials/Home.html',
												controller : 'HomeController'
											})
									.when(
											'/MyAccount/:type',
											{
												templateUrl : 'modules/login/partials/MyAccount.html',
												controller : 'GLI_MyAccountController'
											})
									.when(
											'/MyAccount/:type/:leadId/:fnaId',
											{
												templateUrl : 'modules/login/partials/MyAccount.html',
												controller : 'GLI_MyAccountController'
											})
									.when(
											'/lms',
											{
												templateUrl : 'modules/lms/partials/leadListing.html',
												controller : 'GLI_LeadListingController'
											})
									.when(
											'/lms/clientprofile',
											{
												templateUrl : 'modules/lms/partials/leaddetail.html',
												controller : 'GLI_LeadDetailsController'
											})
									.when(
											'/lms/contactlog',
											{
												templateUrl : 'modules/lms/partials/leaddetail.html',
												controller : 'GLI_CallDetailsController'
											})
									.when(
											'/lms/referral',
											{
												templateUrl : 'modules/lms/partials/leaddetail.html',
												controller : 'GLI_ReferralController'
											})
									.when(
											'/lms/feedback',
											{
												templateUrl : 'modules/lms/partials/leaddetail.html',
												controller : 'GLI_FeedbackController'
											})
									.when(
											'/AboutUs',
											{
												templateUrl : 'modules/eApp/partials/AboutUs.html',
												controller : 'HomeController'
											})
									.when(
											'/Eapp/:productId/:proposalId/:illustrationId/:transactionId/:lmsId/:fnaId/:illustrationNum',
											{
												templateUrl : 'modules/eApp/partials/eAppDetails.html',
												controller : 'GLI_EappNavigationController'
											})
									.when(
											'/fna/:fnaId/:customerId/:agentId',
											{
												templateUrl : 'modules/fna/partials/Master.html',
												controller : 'FnaController'
											})
									.when(
											'/products/:productId/:proposalId/:illustrationId/:transactionId/:isProductListing',
											{
												templateUrl : 'modules/illustrator/partials/Products.html',
												controller : 'GLI_ProductsController'
											})
									.when(
											'/Illustrator/:productId/:proposalId/:illustrationId/:transactionId',
											{
												templateUrl : 'modules/illustrator/partials/PersonalDetails.html',
												controller : 'GLI_PersonalDetailsController'
											})
									.when(
											'/illustratorProduct/:productId/:proposalId/:illustrationId/:transactionId',
											{
												templateUrl : 'modules/illustrator/partials/ProductDetails.html',
												controller : 'GLI_ProductDetailsController'
											})
									.when(
											'/illustration/:productId/:proposalId/:illustrationId/:transactionId',
											{
												templateUrl : 'modules/illustrator/partials/IllustrationDetails.html',
												controller : 'GLI_IllustrationDetailsController'
											})
									
											.when(
												'/consentPage',
												{
													templateUrl : 'modules/illustrator/partials/consent.html',
													controller : 'GLI_IllustrationConsentController'
												})

									.when(
											'/setgoal',
											{
												templateUrl : 'modules/fna/partials/SetGoalPreference.html',
												controller : 'GLI_GoalController'
											})
									.when(
											'/fnaListing/:status',
											{
												templateUrl : 'modules/fna/partials/Listing.html',
												controller : 'GLI_FnaListController'
											})
									.when(
											'/fnaCalculator',
											{
												templateUrl : 'modules/fna/partials/Calculators.html',
												controller : 'GLI_CalculatorController'
											})
									.when(
											'/fnasummary',
											{
												templateUrl : 'modules/fna/partials/Summary.html',
												controller : 'GLI_GoalShortfallController'
											})
									.when(
											'/fnaReport',
											{
												templateUrl : 'modules/fna/partials/Report.html',
												controller : 'GLI_FnaReportController'
											})
									.when(
											'/fnaRecomendedProducts',
											{
												templateUrl : 'modules/fna/partials/RecommendedPdts.html',
												controller : 'GLI_ProductListingController'
											})
									.when(
											'/fnaAllProducts',
											{
												templateUrl : 'modules/fna/partials/AllProducts.html',
												controller : 'GLI_AllProductController'
											})
									.when(
											'/landingPage',
											{
												templateUrl : 'modules/login/partials/LandingPage.html',
												controller : 'GLI_LandingController'
											})
									.when(
											'/fnaLandingPage/:fnaId/:transactionId',
											{
												templateUrl : 'modules/fna/partials/Fna-landingpage.html',
												controller : 'FnaLandingController'
											})
									.when(
											'/MyFamilyDetails',
											{
												templateUrl : 'modules/fna/partials/MyFamilyDetails.html',
												controller : 'GLI_MyFamilyDetailsController'
											})
									.when(
											'/aboutme',
											{
												templateUrl : 'modules/fna/partials/Aboutme.html',
												controller : 'GLI_AboutMeController'
											})
								/* 
								FNA changes by LE Team 
									.when(
											'/questioner',
											{
												templateUrl : 'modules/fna/partials/Questionnaire.html',
												controller : 'GLI_QuestionerController'
											})
								*/
									.when(
											'/aboutCompany/:fnaId/:transactionId/:isFromLMS',
											{
												templateUrl : 'modules/fna/partials/AboutCompany.html',
												controller : 'GLI_AboutCompanyController'
											})
									.when(
											'/taskList/:tabName',
											{
												templateUrl : 'modules/taskList/partials/taskList.html',
												controller : 'TaskListController'
											})
									.when(
											'/selfTools/:subTab',
											{
												templateUrl : 'modules/login/partials/selfTools.html',
												controller : 'GLI_MyAccountController'
											})
									.when(
											'/feedback',
											{
												templateUrl : 'modules/feedback/partials/feedback.html',
												controller : 'FeedbackIssueController'
											})
									.when(
											'/fnaChooseParties/:illustrationTransId/:productId/:illustrationNumber' ,
											{
												templateUrl : 'modules/fna/partials/fna-choose-parties.html',
												controller : 'GLI_ChoosePartiesController'
											})
									.when(
											'/salesactivity',
											{
												templateUrl : 'modules/salesactivity/partials/salesActivity.html',
												controller : 'GLI_salesActivityController'
											})
									.when('/memo',
											{
												templateUrl : 'modules/eApp/partials/MemoDetails.html',
												controller : 'MemoDetailsController'
											})
									.when('/gaoDocs',
											{
												templateUrl : 'modules/eApp/partials/GAODocs.html',
												controller : 'GAODocsController'
											}).otherwise({
										redirectTo : '/login'
									});

							var interceptor = [
									'$location',
									'$q',
									function($location, $q) {
										function success(response) {
											return response;
										}
										function error(response) {
											if (response.status === 401) {
												$location.path('/login');
												return $q.reject(response);
											} else {
												return $q.reject(response);
											}
										}
										return function(promise) {
											return promise.then(success, error);
										};
									} ];
						
//							var httpInterceptor =['$q', function () {
//				                return {
//				                	request: function(config) {
//				                		var UserDetailsModel = UserDetailsService.getUserDetailsModel();
//				                		var headers = $.extend({}, config.headers, UserDetailsModel.options.headers);
//				                		if (authToken) {
//				                			config.headers = headers;
//				                		}
//				                		return config;
//				                	}
//				                }
//							}];

							$httpProvider.interceptors
									.push(interceptor);
							
							if ((rootConfig.isDeviceMobile)) {
								$httpProvider.defaults.headers.post['source'] = "200";
							} else {
								$httpProvider.defaults.headers.post['source'] = "100";
							}
							//$httpProvider.responseInterceptors.push(httpInterceptor);

						} ])
		.run(
				[
						'$rootScope',
						'$location',
						'AuthService',
						'$translate',
						function($rootScope, $location, AuthService, $translate) {

							$rootScope.$on("$routeChangeStart", function(event,
									next, current) {

								if (!AuthService.isLoggedIn($rootScope)) {
									if(next.$$route.originalPath.indexOf("gaoDocs")<0)
									$location.path('/login');
								}

							});

							//$rootScope.moduleVariable = translateMessages(
							//		$translate, "fna.landingPageHeading");

						} ]);

storeApp.config( [
    '$compileProvider',
    function( $compileProvider )
    {   
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|sms|tel|chrome-extension):/);
    }
]);	
					
/*  translateProvider
 * register supporting languages
 */

storeApp
		.config([
				'$translateProvider',
				function($translateProvider) {
					/*// register english translation table
					$translateProvider.translations('en_US', resources.en_US);
					// register thai translation table
					$translateProvider.translations('th_TH', resources.th_TH);
					// register chineese-Simpified translation table
					$translateProvider.translations('zh_CN', resources.zh_CN);
					// register chineese-Traditional translation table
					$translateProvider.translations('td_CN', resources.td_CN);
					// register Japanese translation table
					$translateProvider.translations('ja_JA', resources.ja_JA);
					// register Bahasa-Indonesian translation table
    				$translateProvider.translations('in_ID', resources.in_ID);
					// register Portuguese translation table
					$translateProvider.translations('pt_PT', resources.pt_PT);
					//Default set to english*/
					//call to the asyncLoader factory function
                    $translateProvider.useLoader('asyncLoader');

					if ((rootConfig.isDeviceMobile)) {

						if (localStorage["locale"]
								&& localStorage["locale"]
										.indexOf('zh_') > -1) {
							$translateProvider.preferredLanguage("zh_CN");
						} else if (localStorage["locale"]
								&& localStorage["locale"]
										.indexOf('th') == 0) {
							$translateProvider.preferredLanguage("th_TH");
						} else if (localStorage["locale"]
								&& localStorage["locale"]
										.indexOf('ja') == 0) {
							$translateProvider.preferredLanguage("ja_JA");
						} else if (localStorage["locale"]
								&& localStorage["locale"]
										.indexOf('td') == 0) {
							$translateProvider.preferredLanguage("td_CN");
						} else  if(localStorage["locale"] && localStorage["locale"].indexOf('in') == 0 ){
           					 $translateProvider.preferredLanguage("in_ID");
        				} 
						else if (localStorage["locale"]
								&& localStorage["locale"]
										.indexOf('pt') == 0) {
							$translateProvider.preferredLanguage("pt_PT");
						} else  if(localStorage["locale"] && localStorage["locale"].indexOf('vt') == 0 ){
           					 $translateProvider.preferredLanguage("vt_VT");
						}else {
							$translateProvider.preferredLanguage(rootConfig.locale);
						}

					} else {
						if (localStorage["locale"] != undefined) {
							$translateProvider
									.preferredLanguage(localStorage["locale"]);
						} else {
							$translateProvider
									.preferredLanguage(rootConfig.locale);
						}

					}

				} ]);
				
storeApp.factory('asyncLoader', function ($q, $timeout,$resource) {
	function setTranslation(options,translations,deferred){
	    if (options.key === 'en_US') {
	      translations = resources.en_US;
	    }else if(options.key === 'th_TH'){
	    	translations = resources.th_TH;
	    }else if(options.key === 'zh_CN'){
	    	translations = resources.zh_CN;
	    }else if(options.key === 'td_CN'){
	    	translations = resources.td_CN;
	    }else if(options.key === 'ja_JA'){
	    	translations = resources.ja_JA;
	    }else if(options.key === 'in_ID'){
	    	translations = resources.in_ID;
	    }else if(options.key === 'pt_PT'){
	    	translations = resources.pt_PT;
	    }else if(options.key === 'vt_VT'){
	    	translations = resources.vt_VT;
		}else {
	      translations = resources.en_US;
	    }

	    $timeout(function () {
	      deferred.resolve(translations);
	    }, 100);
	}

	  return function (options) {
	    var deferred = $q.defer(),
	        translations;
	    if(!resources){
	    	$resource('config/resources.json').get(function (data) {
	    		resources = data;
	    		setTranslation(options,translations,deferred);
	            }, function () {
	                console.log('Unable to download resources.json!');
	            });
	    } else {
	    	setTranslation(options,translations,deferred);
	    	
	    }
	    

	    return deferred.promise;
	  };
	});
