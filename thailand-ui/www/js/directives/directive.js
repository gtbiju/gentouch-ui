/*
 *Copyright 2015, LifeEngage 
 */



var lifeEngagePayment = angular.module('lifeEngage.payment', []);
var observation = angular.module('lifeEngage.observation', []);
var lifeEngagedirectives = angular.module('lifeEngage.directives', []);
var piechart = null;
var linechart = null;
var comparisonChart = null;
var chartbinddata = null;

lifeEngagedirectives.directive('dynamic', function() {
	return {
		require : 'ngModel',
		link : function(scope, elem, attr, ngModel) {
			var validator=scope[attr.dynamic];
			function validate(value) {
				if(validator){
					var validationResult = validator(value);
					ngModel.$setValidity('dynamic', validationResult.status);
					ngModel.$error.message=validationResult.message;
					return value;
				}
			}
			ngModel.$parsers.unshift(function(value) {
				 return validate(value);
			});
			ngModel.$formatters.unshift(function(value) {
			return validate(value);
			});
		}
	};
});
lifeEngagedirectives.directive('ngIf', function() {
	return {
		link : function(scope, element, attrs) {
			if (scope.$eval(attrs.ngIf)) {

				element.replaceWith(element.children());
			} else {
				element.replaceWith(' ');
			}
			scope.$on('$destroy', function() {
				element.remove();
			 });
		}
	};
});
lifeEngagedirectives.directive('scopeDir', function() {
	return {
		restrict : "EA",
		scope: true,
		link : function(scope, element, attrs) {
			scope.goalIndex = attrs.scopeDir;
		}
	}
});
			
			
lifeEngagedirectives.directive('bindOnce', function() {
	
    return {
        scope: true,
        link: function( $scope, $element ) {
            setTimeout(function() {
                $scope.$destroy();
                $element.removeClass('ng-binding ng-scope');
            }, 0);
        }
    }
});

lifeEngagedirectives
		.directive(
				'lepopup',
				function() {
					return {
						restrict : 'E',
						templateUrl : 'templates/lePopUp.html',
						scope : {
							control : '='
						},
						replace : true, // Replace with the template below
						// transclude: true, // we want to insert custom content
						// inside the directive
						link : function(scope, element, attrs) {
							
							scope.control.message = "";
							scope.control.show = false;
							scope.control.showWarning = function(header,
									message, button1Name, button1Function,
									button2Name, button2Function, closeFunction) {
								scope.control.icon = "warning";
								scope.control.setProperties(header, message,
										button1Name, button1Function,
										closeFunction);
								scope.control.setButton2Properties(button2Name,
										button2Function);
								scope.control.button3Show = false;
								scope.control.textAreaShow = false;
								scope.refresh();
							};

							scope.control.showQuestion = function(header,
									message, button1Name, button1Function,
									button2Name, button2Function, button3Name,
									button3Function, closeFunction) {
								scope.control.textAreaShow = false;
								scope.control.icon = "question";
								scope.control.setProperties(header, message,
										button1Name, button1Function,
										closeFunction);
								scope.control.setButton2Properties(button2Name,
										button2Function);
								scope.control.setButton3Properties(button3Name,
										button3Function);
								scope.refresh();

							};
                            
                            scope.control.paymentSuccess = function(header,
									message, button1Name, button1Function,
									button2Name, button2Function, button3Name,
									button3Function, closeFunction) {
								scope.control.textAreaShow = false;
								scope.control.icon = "paymentSuccess";
								scope.control.setProperties(header, message,
										button1Name, button1Function,
										closeFunction);
								scope.control.setButton2Properties(button2Name,
										button2Function);
								scope.control.setButton3Properties(button3Name,
										button3Function);
								scope.refresh();

							}
                            
							scope.control.showError = function(header, message,
									button1Name, button1Function, closeFunction) {
								scope.control.icon = "error";
								scope.control.setProperties(header, message,
										button1Name, button1Function,
										closeFunction);
								scope.control.button2Show = false;
								scope.control.button3Show = false;
								scope.control.textAreaShow = false;
								scope.refresh();

							};
							scope.control.showInformation = function(header,
									message, button1Name, button1Function,
									button2Name, button2Function, closeFunction) {
								scope.control.icon = "information";
								scope.control.setProperties(header, message,
										button1Name, button1Function,
										closeFunction);
								if (typeof button2Name == 'undefined') {
									scope.control.button2Show = false;
									scope.control.button3Show = false;
									scope.control.textAreaShow = false;
								} else {
									scope.control.textAreaShow = false;
									scope.control.setButton3Properties(
											button2Name, button2Function);
								}
								scope.refresh();

							};
							scope.control.showInfo = function(header,
									message, button1Name, button1Function,
									button2Name, button2Function, button3Name, button3Function,closeFunction) {								
									scope.control.icon = "information";									
									scope.control.setProperties(header, message,button1Name, button1Function, closeFunction);	
									scope.control.setButton2Properties(button2Name,button2Function);								
									scope.control.setButtonThreeProperties(button3Name,button3Function);
									scope.refresh();

							};
							scope.control.showSuccess = function(header,
									message, button1Name, button1Function,
									button2Name, button2Function, closeFunction) {
								scope.control.icon = "success";
								scope.control.setProperties(header, message,
										button1Name, button1Function,
										closeFunction);
								if (typeof button2Name == 'undefined') {
									scope.control.button2Show = false;
									scope.control.button3Show = false;
									scope.control.textAreaShow = false;
								} else {
									scope.control.textAreaShow = false;
									scope.control.setButton3Properties(
											button2Name, button2Function);
								}

								scope.refresh();

							};
							scope.control.showBlockingPopUp = function(header,
									message) {
								scope.control.icon = "error";
								scope.control.blockingPopup = true;
								scope.control.setProperties(header, message);
								scope.control.button2Show = false;
								scope.control.button3Show = false;
								scope.control.textAreaShow = false;
								scope.refresh();

							};
							scope.control.setProperties = function(header,
									message, button1Name, button1Function,
									closeFunction) {
								if (typeof button1Name == 'undefined') {
									button1Name = "OK";
								}
								if (typeof button1Function == 'undefined') {
									button1Function = scope.control.hideModal;
								}
								if (typeof closeFunction == 'undefined') {
									closeFunction = button1Function;
								}
								scope.control.listDetails = [];
								scope.control.isDisplayList = false;
								scope.control.header = header;
								if (message instanceof Array) {// If an array
																// came as
																// argument,
																// First item
																// will be taken
																// as mesage and
																// others will
																// be listed
																// below.
									scope.control.message = message[0];
									message.shift();
									if (message.length > 0) {
										scope.control.listDetails = message;
										scope.control.isDisplayList = true;
									}
								} else {
									scope.control.message = message;
								}
								scope.control.button1Label = button1Name;
								scope.control.show = true;

								scope.control.callBackPositive = function() {
									scope.control.show = false;
									button1Function();
									
								};
								scope.control.closeFunction = function() {
									scope.control.show = false;
									closeFunction();
									
								};

							};

							scope.control.setButton2Properties = function(
									button2Name, button2Function) {
								if (typeof button2Name == 'undefined') {
									scope.control.button2Show = false;
								} else {
									scope.control.button2Label = button2Name;
									scope.control.button2Show = true;
									if (typeof button2Function == 'undefined') {
										scope.control.callBackNegative = scope.control.hideModal;
									} else {
										scope.control.callBackNegative = function() {
											scope.control.show = false;
											button2Function();
										};
									}
								}
							};
							scope.control.setButtonThreeProperties = function(
									button3Name, button3Function) {
								if (typeof button3Name == 'undefined') {
									scope.control.button3Show = false;
								} else {
									scope.control.button3Label = button3Name;
									scope.control.button3Show = true;
									if (typeof button3Function == 'undefined') {
										scope.control.showDetails = scope.control.hideModal;
									} else {
										scope.control.showDetails = function() {
											scope.control.show = false;
											button3Function();
										};
									}
								}
							};
							scope.control.setButton3Properties = function(
									button3Name, button3Function) {
								if (typeof button3Name == 'undefined' || button3Name == '') {
									scope.control.button3Show = false;
								} else {
									scope.control.button3Label = button3Name;
									scope.control.button3Show = true;
									if (typeof button3Function == 'undefined' || button3Name == '') {
										scope.control.showDetails = scope.control.hideModal;
									} else {
										scope.control.showDetails = function() {

											scope.control.details = button3Function();
											if (scope.control.textAreaShow) {
												scope.control.textAreaShow = false;
											} else {
												scope.control.textAreaShow = true;
											}

										};
									}
								}
							};

							scope.control.hideModal = function() {
								scope.control.show = false;
							};
						     
							scope.$on('$destroy', function() {
								element.remove();
							 });
						}
					}
				});

/* custom popup for LE Action popup - updated by Pratheesh */  

lifeEngagedirectives.directive('actionpopup', function ($compile) {
    return {
        restrict: 'E',
        // include smart table controller to use its API if needed
        //require: '^smartTable',
		scope:true,		
        template: '<span class="SmsAndCall_tab"><a class="call-action"></a></span>',
        replace: true,
        link: function (scope, element, attrs, ctrl) {
    
        	var mobNumber;
        	 attrs.$observe('number', function(value) {         		  
     			mobNumber = value;
     			
     			if(mobNumber == undefined){
     				
     				  // can use scope.dataRow, scope.column, scope.formatedValue, and
     				// ctrl API
    				scope.mobNumber = scope.dataRow.contactNumber;
    				scope.emailId = scope.dataRow.emailId;
    			}
    			else{
    				scope.mobNumber = mobNumber; 
    			}
     			
    			scope.showPopup = function(){
    				// var thisObj = $(this);
    				var individualOffset = findOffset(element);
    				var topVal = individualOffset[0];
    				var leftval = individualOffset[1];
					var arrowClass = individualOffset[2];
    				showPopupModal(".common_model_container",".action_popup",topVal,leftval,arrowClass);
    			}
    			
    			function findOffset(thisObj){
					var x =30;
					var y =  -95;
					var arrowClass="arrow";
    				var cordinateVal = $(thisObj).offset();
    				var scrollTopVal = $('body').scrollTop();
    				var scrollLeftVal = $('body').scrollLeft();
					var fixedTop = (cordinateVal.top - scrollTopVal);
					var fixedLeft = (cordinateVal.left - scrollLeftVal );
					if(window.innerHeight/2<fixedTop){
						x = -95;
						arrowClass="arrow-bottom ";
					}
					if(window.innerWidth/2>fixedLeft){
						y = -10;
						arrowClass=arrowClass+" arrow-left";
					}
					else{
						arrowClass=arrowClass+" arrow-right";
					}

    			   var topVal = fixedTop + x + "px";
    				var leftVal = fixedLeft + y  + "px";
    				return [topVal,leftVal,arrowClass];
    			}

    			function showPopupModal(modalPopupOverlay,modalCOntainer,top,left,arrowClass){
    			
    			var html = "<div class='"+arrowClass+"'></div>" +
    							"<ul>"+
    							  "<li><a class='sms' target='_self' href='sms:"+scope.mobNumber+"'> SMS </a></li>"+
    							  "<li class='mob-tab-only'><a class='call ' target='_self' href='tel:"+scope.mobNumber+"'> Call </a></li>"+
    							  
    							 							  
    							"</ul>";
    				var disp = $(modalPopupOverlay).css("display"); 
    				if(disp == "none"){
    					$(modalPopupOverlay).css("display","block");
    					$(modalCOntainer).css(
    							   {
    								   "display": "block",
    								   "top" : top,
    								   "left" : left
    							   });
    						$compile(html)(scope);					 
    						$(modalCOntainer).html("");
    						$(modalCOntainer).append(html);
     						
    					}
    					
    				$(modalCOntainer).click(function(){
    					if (navigator.userAgent.indexOf("Android") > 0 )
				    {
    					$(this).css("display","none");
    					$(modalPopupOverlay).css("display","none");
				    }
    				});
    				 $(modalPopupOverlay).click(function(){
    					$(this).css("display","none");
    					$(modalCOntainer).css("display","none");
    				});
    			}
         	  });
          
        	 scope.$on('$destroy', function() {
        		 element.remove();
				 });
        }
    };
});

lifeEngagedirectives.directive('lookupDdlDirective', function() {
	return {
		restrict : 'A',
		require : 'ngModel',
		link : function(scope, element, attrs) {

			/*
			 * scope.$watch(attrs.ngModel, function(value) {
			 * scope.populateDependentLookupDate(attrs.childType,attrs.childName,scope.getValue(attrs.ngModel));
		
			 */
			attrs.$observe('observeattr', function(value) {
				scope.populateDependentLookupDate(attrs.childType,
						attrs.childName, scope.getValue(attrs.ngModel),
						attrs.grandChildren,attrs.cacheable);
			});
			// scope.$on('$destroy', function() {
        		 //element.remove();
				 //});

			 attrs.$observe('observetype', function(value) {
				scope.populateIndependentLookupDate(attrs.observetype,attrs.observearrayname,attrs.cacheable,attrs.filterobj);
			});
		},
		controller : function($scope, DataLookupService) {
			$scope.populateIndependentLookupDate = function(type, fieldName,cacheable,filterObj) {
				if (!cacheable){
					cacheable = false;
				}
				if (!$scope[fieldName] || $scope[fieldName].length == 0) {
					$scope[fieldName] = [ {
						"key" : "",
						"value" : "loading ...."
					} ];
					var options = {};
					options.type = type;
					DataLookupService.getLookUpData(options, function(data) {
						$scope[fieldName] = data;
						if(filterObj){
							$scope[fieldName] = $scope.getFilterResult(data,filterObj);
						}
						$scope.refresh();
					}, function(errorData) {
						$scope[fieldName] = [];
						
					},cacheable);
				}
			},

			$scope.populateDependentLookupDate = function(type, fieldName,
					parentCode, grandChildren, cacheable) {
				if (!cacheable){
					cacheable = false;
				}
				if (parentCode && parentCode.trim() != "") {
					$scope[fieldName] = [ {
						"key" : "",
						"value" : "loading ...."
					} ];
					var options = {};
					options.type = type;
					options.code = parentCode;
					DataLookupService.getLookUpData(options, function(data) {
						$scope[fieldName] = data;
						$scope.refresh();
						/*
						 * if (grantChildrens && grantChildrens.length > 0) {
						 * for (i=0; i< grantChildrens.length; i++) {
						 * $scope[grantChildrens[i]] = []; } }
						 */
					}, function(errorData) {
						$scope[fieldName] = [];
						
					},cacheable)
					if (grandChildren && grandChildren != "") {
						var grandChildrens = grandChildren.split(",");
						for (var i = 0; i < grandChildrens.length; i++) {
							$scope[grandChildrens[i]] = [];
						}
					}
				}
					else{
                	$scope[fieldName] = [];
                	if (grandChildren && grandChildren != "") {
                		var grandChildrens = grandChildren.split(",");
                			for (var i = 0; i < grandChildrens.length; i++) {
                				$scope[grandChildrens[i]] = [];
                			}
                		}

                	}
			}, $scope.getValue = function(model) {
				if ($scope.isVariableExists(model)) {
					return eval('$scope.' + model)
				}
			}, $scope.isVariableExists = function(variable) {
				variable = variable.split('.')
				var obj = $scope[variable.shift()];
				while (obj && variable.length)
					obj = obj[variable.shift()];
				return obj;
			},
			$scope.getFilterResult = function(data,filterObj) {
				 
				 var filterData=[];
				 var  filterArray = $scope.$eval(filterObj);
				 if(typeof filterArray == 'undefined' || filterArray.length==0){
					filterData =  data;
				 }
				 else{
					 for(var i=0; i<data.length;i++){
						if($.inArray(data[i].code, filterArray)!=-1){
							filterData.push(data[i]);
						}
					 }
				 }
				return filterData;
			
			}
		}
	}
});

lifeEngagedirectives
		.directive(
				'showScrap',
				function() {

					return {

						link : function(scope, element, attrs) {

							function findOffsetForScrap(thisObj, x, y) {
								var cordinateVal = $(thisObj).offset();
								var topVal = cordinateVal.top + x + "px";
								var leftVal = cordinateVal.left + y + "px";
								return [ topVal, leftVal ];
							}

							;

							$(element)
									.click(
											function(event) {
												scope.$parent.thisObjDeletion = $(element);
												scope.$parent.benef_id = $(
														element).parent().attr(
														"id");
												scope.$parent.currentlyRemovedId = $(
														element).children()
														.attr("id");
												var childrenLength = $(element)
														.children().length;
												var individualOffset = findOffsetForScrap(
														scope.$parent.thisObjDeletion,
														-20, 60);
												var topVal = individualOffset[0];
												var leftval = individualOffset[1];
												var chk = $('.cp_scrapButton')
														.css("display");
												if (chk == "none"
														&& childrenLength > 0) {
													$('.cp_scrapButton')
															.fadeIn();
													$('.cp_scrapButton').css({
														"display" : "block",
														"top" : topVal,
														"left" : leftval
													});
												}else{
													$('.cp_scrapButton').hide();
												}
											});
							 scope.$on('$destroy', function() {
				        		 element.remove();
								 });
						}
					};

				});

lifeEngagedirectives.directive('circularWidgetFunc', function() {
	return {
		link : function(scope, element, attrs) {
			/*
			 * The plugin should be initialized only after the ng-repeat has
			 * finished its rendering. ( ie compile phase is complete )
			 */
			if (scope.$last) {
				setTimeout(function() {
					$('.arc_container_wrapper .arc_left').radialOptions({
						animationTime : 100, // set the animation duration
						selectedAction : "switch" // Two options -
					// switch/selectOnly
					});
				}, 1);
			}
			 scope.$on('$destroy', function() {
        		 element.remove();
				 });
		}
	}
});
lifeEngagedirectives
		.directive(
				'dateInput',
				function(dateFilter, $filter) {
					return {
						require : 'ngModel',

						replace : true,
						link : function(scope, elm, attrs, ngModelCtrl) {
							if (!((rootConfig.isDeviceMobile)) || ((rootConfig.isOfflineDesktop))) {
								/* var elmType = '#' + elm[0].id;
								$(elmType).attr('type', 'text'); */
								elm.attr('type','text');
								$('.custdatepicker').datepicker({
									format : 'yyyy-mm-dd',
									autoclose : true
								});

								$('.custdatepicker')
										.on(
												'changeDate',
												function(ev) {
													var id_elmnt = $(this)
															.attr('id');
													var id_elmnt_Model = $(this)
															.attr('ng-model');
													var selected_date = $(
															'#' + id_elmnt)
															.val();
													var modelObject = id_elmnt_Model
															.substring(
																	0,
																	id_elmnt_Model
																			.lastIndexOf('.'));
													var modelVariable = id_elmnt_Model
															.substring(id_elmnt_Model
																	.lastIndexOf('.') + 1);
													if (modelObject.length > 0) {
														modelObject = "."
																+ modelObject;
													}
													eval('scope' + modelObject)[modelVariable] = selected_date;
													scope.refresh();
												});
							}
							/**
							 * Commented due to problem caused in safari problem
							 * is model is not getting due to this*
							 */

							 scope.$on('$destroy', function() {
								 elm.remove();
							});
						}
					};
				});

lifeEngagedirectives
		.directive(
				'riderSlider',
				function() {
					return {
						require : 'ngModel',
						replace : true,
						link : function(scope, elm, attrs, ngModelCtrl) {

							try {
								var riderDisabled = false;
								var sliderElement = $(elm[0]);
								var $scope = scope;
								var index = $scope.riderIndex;
								var ngString = "$scope.Illustration.Product.RiderDetails["
										+ index + "].value";
								var disabled = "$scope.Illustration.Product.RiderDetails["
										+ index + "].isRequired";
								var value = 1000;
								var scopeStr = "$scope.Illustration.Product.RiderDetails["
										+ index + "].value";

								var val = $scope.Illustration.Product.RiderDetails[index].value;
								var val1 = $scope.Illustration.Product.RiderDetails[index].isRequired;

								if (eval(scopeStr) != null) {
									eval("val=" + scopeStr);
								} else {
									val = value;
									eval(scopeStr + "=value");
								}

								var sliderValue = Number(val);
								var ArrSliderValue = sliderValue;
								var RiderDetailsWatch;
								RiderDetailsWatch	= $scope.$watch(
												'Illustration.Product.RiderDetails',
												function() {
													if ($scope.Illustration.Product.RiderDetails[index].isRequired == 'Yes') {
														sliderElement.slider({
															disabled : false
														});
													} else {
														sliderElement
																.slider({
																	disabled : true,
																	value : sliderElement
																			.slider(
																					"option",
																					"min")
																});
														if ((sliderElement
																.attr('id') == "PayorRiderSlider")
																|| (sliderElement
																		.attr('id') == "PartnerCareRiderSlider")) {
															sliderElement
																	.children()
																	.eq(1)
																	.children()
																	.html(
																			sliderElement
																					.slider(
																							"option",
																							"min"));
														} else {
															sliderElement
																	.children()
																	.eq(1)
																	.children()
																	.html(
																			sliderElement
																					.slider(
																							"option",
																							"min")
																					+ "%");
														}
													}

												}, true);

								sliderElement
										.slider({
											range : "min",
											min : attrs.min,
											max : attrs.max,
											value : ArrSliderValue,
											values : null,
											step : null,
											disabled : !($scope.Illustration.Product.RiderDetails[index].isRequired),
											noOfDivisions : 10,
											multipleOf : 10,
											noDivider : true,
											isLargeValue : true,
											isPercentageSlider : false,
											isYearSlider : false,
											isString : false,
											slide : function(event, ui) {
												$(this).children().eq(1)
														.children().html(
																ui.value);
												scope.$apply(function() {
													eval(scopeStr
															+ "= ui.value");
												});
											}
										});

								sliderElement.children().eq(1).children().html(
										sliderValue);
							} catch (ex) {
								alert("error while init slider");
							}
							 scope.$on('$destroy', function() {
								 elm.remove();
								 RiderDetailsWatch();
							});
						}
					};
				});

lifeEngagedirectives
		.directive(
				'numericValidation',
				function() {
					return {
						restrict : 'A',
						require : 'ngModel',
						link : function(scope, elem, attr, ngModel) {
							var numericReqExp = "^[0-9]*$";
							numericReqExp = new RegExp(numericReqExp);
							var min = attr.min;
							var max = attr.max;
							var unit = attr.unit;
							var unitModel = ((unit).replace(/FNAObject/g,
									"scope.FNAObject"));
							var validate = function(inputText) {
								var valid = true;
								if (!isNaN(inputText)) {
									inputText = parseInt(inputText);
									var valid = numericReqExp.test(inputText);
									if (min
											&& ((inputText < parseInt(min) || (eval(unitModel) == "NA")
													&& inputText < 0))) {
										valid = false;
									}									
									if (max && (inputText > parseInt(max))) {
										valid = false;
									}
								} else {
									valid = false;
								}
								ngModel
										.$setValidity('numericValidation',
												valid);
								scope.refresh();
								scope.updateErrorCount(scope.viewName);

								return valid ? inputText : undefined;
							};
							var numericValidationWatch;
							numericValidationWatch	= scope
									.$watch(
											attr.ngModel,
											function(inputText) {
												var unitModel = ((unit)
														.replace(/FNAObject/g,
																"scope.FNAObject"));
												if (eval(unitModel) != "NA"
														&& !isNaN(parseInt(inputText))) {													
													inputText = Number(inputText)
															* Number(scope.unitList[eval(unitModel)]);
												}
												if (inputText != undefined) {
													validate(inputText);
												}

											});

							scope.$on('$destroy', function() {
								elem.remove();
								numericValidationWatch();
							 });
						}
					};
				});

lifeEngagedirectives
		.directive(
				'jqslider',
				function(UtilityService) {
					'use strict';
					return {
						restrict : 'E',
						transclude : true,
						replace : true,
						template : '<div></div>',
						link : function(scope, element, attrs) {

							var newModel = ((attrs.val).replace(/FNAObject/g,
									"scope.FNAObject")).substring(0,
									(attrs.val).replace(/FNAObject/g,
											"scope.FNAObject")
											.indexOf('.value'));
							if (eval(newModel) == undefined) {
								eval(newModel + "={}");

								eval(newModel + ".name =''+"
										+ angular.toJson(attrs.id));
								eval(newModel + ".unit =''+"
										+ angular.toJson(attrs.defaultunit));								
								if (eval(newModel + ".unit") != "NA") {
									eval(newModel + ".textBoxValue =Number("
											+ attrs.defaultvalue
											+ ")/Number(scope.unitList["
											+ newModel + ".unit])");
								} else {
									eval(newModel + ".textBoxValue =Number("
											+ attrs.defaultvalue + ")");
								}
								eval((attrs.val).replace(/FNAObject/g,
										"scope.FNAObject")
										+ "=" + attrs.defaultvalue);

							}

							$('#' + attrs.id)
									.slider(
											{
												orientation : "horizontal",
												range : "min",
												value : eval((attrs.val)
														.replace(/FNAObject/g,
																"scope.FNAObject")),
												min : attrs.min,
												step : 1,
												max : attrs.max,
												noDivider : true,
												hasValueBox : false,
												change : function(event, ui) {
													UtilityService.disableProgressTab = true;
													var sliderVal = parseInt(
															ui.value, 10);
													var model = (attrs.val)
															.replace(
																	/FNAObject/g,
																	"scope.FNAObject");
													eval(model + "="
															+ sliderVal);
													scope.slideValue = sliderVal;
													newModel = ((attrs.val)
															.replace(
																	/FNAObject/g,
																	"scope.FNAObject"))
															.substring(
																	0,
																	(attrs.val)
																			.replace(
																					/FNAObject/g,
																					"scope.FNAObject")
																			.indexOf(
																					'.parameters'));
													delete eval(newModel).result;
													newModel = ((attrs.val)
															.replace(
																	/FNAObject/g,
																	"scope.FNAObject"))
															.substring(
																	0,
																	(attrs.val)
																			.replace(
																					/FNAObject/g,
																					"scope.FNAObject")
																			.indexOf(
																					'.value'));
													if (eval(newModel + ".unit") != "NA") {
														if (!isNaN(parseInt(sliderVal))) {
															eval(newModel
																	+ ".textBoxValue =Number("
																	+ sliderVal
																	+ ")/Number(scope.unitList["
																	+ newModel
																	+ ".unit])");
														}
													} else {
														if (!isNaN(parseInt(sliderVal))) {
															eval(newModel
																	+ ".textBoxValue =Number("
																	+ sliderVal
																	+ ")");
														}
													}													
													scope.goalruleResults = [];
													scope.showCalculations = false;
													scope
															.calculatePendingGoals();

													scope.refresh();

												}
											});
							scope.$on('$destroy', function() {
								element.remove();
							 });
						}
					};
				});

lifeEngagedirectives
		.directive(
				'uiChart',
				function() {
					return {
						restrict : 'E',
						replace : true,
						link : function(scope, elem, attrs) {
							/*
							 * Function to change X Axis Data and Label while
							 * changing value in select box
							 */
							scope.updateXaxis = function(value, xAxisLabels) {
								attrs.xaxis = value;
								for (var i = 0; i < xAxisLabels.length; i++) {
									if (xAxisLabels[i].value == value) {
										attrs.xaxisdata = xAxisLabels[i].key;
										break;
									}
								}
								renderChart();
							};
							/*
							 * Function to change Y Axis Data and Label while
							 * changing value in select box
							 */
							scope.updateYaxis = function(value, yAxisLabels) {
								attrs.yaxis = value;
								for (var i = 0; i < yAxisLabels.length; i++) {
									if (yAxisLabels[i].value == value) {
										attrs.yaxisdata = yAxisLabels[i].key;
										break;
									}
								}
								renderChart();
							};

							var renderChart = function() {

								var tableData = scope[attrs.ngModel];
								var data = tableData;
								var legend_array = scope[attrs.label];
								var chartId = attrs.id;
								var type = attrs.type;
								var xaxisLabel = attrs.xaxis;
								var yaxisLabel = attrs.yaxis;
								if (attrs.xaxisdata && type != "barChart") {
									data = [];
									var xaxisdata = attrs.xaxisdata;
									var yaxisdata = attrs.yaxisdata;
									var tables = JSON.parse(attrs.ngModel1
											.split("'").join('"'));
									for (var i = 0; i < tables.length; i++) {
										var tableData = scope.$eval(tables[i]);
										var chartArr = [];
										for (var j = 0; j < tableData.length; j++) {
											var chartObj = tableData[j];
											chartArr.push([
													chartObj[xaxisdata],
													chartObj[yaxisdata] ]);
										}
										data.push(chartArr);
									}
								}
								if (type == "pieChart") {

									piechart = jQuery
											.jqplot(
													"#" + chartId,
													[ data ],
													{
														title : ' ',
														seriesDefaults : {
															shadow : true,
															renderer : jQuery.jqplot.PieRenderer,
															rendererOptions : {
																showDataLabels : true
															}
														},
														legend : {
															show : true,
															location : 's',
															placement : 'outside'
														}
													});
								} else if (type == "linechart") {

									$("#" + chartId).html('');
									linechart = $
											.jqplot(
													chartId,
													data,
													{

														title : "",
														axes : {
															xaxis : {
																label : xaxisLabel,
																labelRenderer : $.jqplot.CanvasAxisLabelRenderer
															},
															yaxis : {
																label : yaxisLabel,
																labelRenderer : $.jqplot.CanvasAxisLabelRenderer
															}
														},
														seriesDefaults : {
															rendererOptions : {
																smooth : true
															}
														},
														series : [ {
															lineWidth : 1,
															markerOptions : {
																style : "filledCircle"
															}
														} ],
														legend : {
															show : true,
															location : 'nw',
															placement : 'inside',
															labels : legend_array
														}
													});
								} else if (type == "comparisonChart") {
									comparisonChart = $.jqplot("#" + chartId,
											data, {
												title : '',
												seriesDefaults : {
													showMarker : false,
													pointLabels : {
														show : true,
														edgeTolerance : 5
													}
												},
												axesDefaults : {
													showTicks : true,
													showTickMarks : true
												},
												legend : {
													show : true,
													location : 's',
													placement : 'outside'
												}
											});
								} else if (type == "barChart") {
									var tablesPlottingValues = attrs.xaxislabels;

									tablesPlottingValues = JSON
											.parse(tablesPlottingValues.split(
													"'").join('"'));
									var tables = JSON.parse(attrs.ngModel1
											.split("'").join('"'));
									for (var i = 0; i < tables.length; i++) {
										var ticks1 = [];
										var dataArray = [];
										var title = tables[i].title;
										var tableData = scope
												.$eval(tables[i].table);
										var tableAttributes = tablesPlottingValues[i];
										for (var j = 0; j < tableAttributes[i].length; j++) {
											dataArray
													.push(tableData[tableAttributes[i][j].key]);
											ticks1
													.push(tableAttributes[i][j].value);
										}
										charElem2 = $("#" + chartId).clone();
										var id = new Date().getTime();
										$(charElem2).attr("id", id);
										$("#" + chartId).after(charElem2);
										var data = [];
										data.push(dataArray);
										var test = $
												.jqplot(
														id,
														data,
														{
															// Only animate if
															// we're not using
															// excanvas (not in
															// IE 7 or IE 8)..
															title : title,
															animate : !$.jqplot.use_excanvas,
															seriesColors : [ '#e67016' ],
															seriesDefaults : {
																renderer : $.jqplot.BarRenderer,
																pointLabels : {
																	show : true
																}
															},
															axesDefaults : {
																tickRenderer : $.jqplot.CanvasAxisTickRenderer
															},
															axes : {
																xaxis : {
																	renderer : $.jqplot.CategoryAxisRenderer,
																	tickOptions : {
																		angle : -60
																	},
																	ticks : ticks1
																}
															},
															highlighter : {
																show : false
															}
														});
									}
									$("#" + chartId).remove();
								}
								$("#" + chartId)
										.bind(
												"jqplotClick",
												function(ev, gridpos, datapos,
														neighbor) {
													if (neighbor) {
														alert('x:'
																+ neighbor.data[0]
																+ ' y:'
																+ neighbor.data[1]);
													}
												});
							};

							chartbinddata = scope.$watch(attrs.ngModel,
									function() {
										renderChart();
									}, true);

							scope.$on('$destroy', function() {
								elem.remove();
								chartbinddata();
							 });
						}
					};
				});

lifeEngagedirectives.directive('numberMask', function() {

	return function(scope, element, attrs) {

		var keyCode = [ 8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57 ];
		element.bind("keypress", function(event) {
			if ($.inArray(event.which, keyCode) == -1) {
				scope.$apply(function() {
					scope.$eval(attrs.numberMask);
					event.preventDefault();
				});
				event.preventDefault();
			}

		});
	};
});

lifeEngagePayment.directive('payment', function(UtilityService) {
	return {
		restrict : "A",
		scope : false,
		templateUrl : 'modules/eApp/partials/Payment.html',
		replace : false,
		link : function(scope, element, attrs) {
			$('.sel_pay_method .online_pay').on(
					"click",
					function() {
						var checkFlag = $('.online_detls_holder')
								.css("display");
						if (checkFlag == "none") {
							$(".online_detls_holder").css("display", "block")
									.hide().animate({
										height : "toggle"
									}, 300);
						}
						if (checkFlag == "block") {
							$(".online_detls_holder").animate({
								height : "toggle"
							}, 300);
						}
					});
			$('.sel_pay_method .cash_dd').on(
					"click",
					function() {
						var checkFlag = $('.cash_dd_detls_holder').css(
								"display");
						if (checkFlag == "none") {
							$(".cash_dd_detls_holder").css("display", "block")
									.hide().animate({
										height : "toggle"
									}, 300);
						}
						if (checkFlag == "block") {
							$(".cash_dd_detls_holder").animate({
								height : "toggle"
							}, 300);
						}
					});
			$('input.star').rating();
			var formattedDate = UtilityService.getFormattedDate();
			if (Modernizr.inputtypes && !Modernizr.inputtypes['date']) {
				$('.custdatepicker').datepicker('setEndDate', formattedDate)
						.on('changeDate', function() {
							$('.custdatepicker').datepicker('hide');
						});
			}
			initializePayment();

			scope.$on('$destroy', function() {
				element.remove();
			 });
		}
	};
});

var lifeEngageinclude = angular.module('lifeEngage.ln-include', []);

lifeEngageinclude.directive('ln-include', function(templateUrl) {
	return {
		restrict : "A",
		scope : false,
		templateUrl : templateUrl,
		replace : true,
		link : function(scope, element, attrs) {
			scope.$on('$destroy', function() {
				element.remove();
			 });
		}
	};
});

var lifeEngageBlur = angular.module('lifeEngage.blur', []);
lifeEngageBlur.directive('blur', function() {
	return function(scope, elem, attrs) {
		elem.bind('blur', function() {
			scope.$apply(attrs.blur);
		});
	};
});

// Observation

observation.directive('clickstream', function(ObservationService,
		UtilityVariables, $location, UtilityService) {
	'use strict';
	return {
		restrict : 'E',
		link : function(scope, element, attrs) {
			/*
			 * Observation is not saved if user comes through QuickLinks button
			 * in home page
			 */

			var formattedDate = UtilityService.getFormattedDate();
			ObservationService.store({
				context : UtilityVariables.context,
				pageDesc : scope.selectedpage,
				path : $location.path(),
				creationDate : formattedDate,
				customerName : "",
				customerId : "",
				customerDbId : "",
				email : "test@cognizant.com",
				agent : "MaxLife Agent",
				customerDeviceId : "device001"
			}, scope);
			
			scope.$on('$destroy', function() {
				element.remove();
			 });
		}
	};
});

// Observation

lifeEngagedirectives.directive('filelistBind', function() {
	return function(scope, elm, attrs) {
		elm.bind('change', function(evt) {
			var isImage = eval(evt.target.attributes.isimage.value);
			var isPhoto = eval(evt.target.attributes.isimage.value);
			var isInsured = false;
			if (attrs.isinsured == "true") {
				isInsured = true;
			}
			scope.$apply(function() {
				scope.browseFile(isImage, isPhoto, evt.target.files[0],
						isInsured);
			});
		});

		elm.bind('click', function(evt) {
			evt.target.value = '';
		});
	};

});

lifeEngagedirectives.directive('dragInitialize', function() {
	return {
		restrict : "A",
		scope : true,
		replace : true,
		link : function(scope, element, attrs) {
			$(element).draggable({
				revert : false,
				helper : "clone"
			});
			
			scope.$on('$destroy', function() {
				element.remove();
			 });
		}
	};
});

lifeEngagedirectives.directive('onKeyupFn', function() {
	return function(scope, elm, attrs) {
		elm.bind('keyup', function(evt) {
			scope.$apply(function() {
				scope.handleKeypress(evt.which);
			});
		});
	};
});

lifeEngagedirectives
		.directive(
				'customTable',
				function() {
					return {
						restrict : 'A',
						replace : true,
						templateUrl : 'templates/tableDirective.html',
						controller : function($scope, $http) {
							$scope.headerOrder = "order";
							$scope.subHeaderOrder = "order";
							$scope.subHeaderPredicate = '';
							$scope.subHeaderReverse = false;
							$scope.headerPredicate = '';
							$scope.headerReverse = false;
							$scope.subHeaderPaginatorPredicate = '';
							$scope.subHeaderPaginatorReverse = false;
							$scope.headerPaginatorPredicate = '';
							$scope.headerPaginatorReverse = false;

							$scope.currentPageNumber = 0;
							$scope.paginatorColSpan;

							$scope.getValuesOrdered = function(key,
									clickedValue, e) {
								if (clickedValue == 'subHeader') {
									if ($scope.paginator) {
										$scope.subHeaderPaginatorPredicate = key;
										$scope.subHeaderPaginatorReverse = !($scope.subHeaderPaginatorReverse);
									} else {
										$scope.subHeaderPredicate = key;
										$scope.subHeaderReverse = !($scope.subHeaderReverse);
									}
								} else if (clickedValue == 'header') {
									if ($scope.paginator) {
										$scope.headerPaginatorPredicate = key;
										$scope.headerPaginatorReverse = !($scope.headerPaginatorReverse);
									} else {
										$scope.headerPredicate = key;
										$scope.headerReverse = !($scope.headerReverse);
									}
								}

								if ($(e.target).hasClass("sorter_arrow_dwn")) {
									$(e.target).toggleClass(
											"sorter_arrow_dwn sorter_arrow_up");
								} else {
									$(e.target).toggleClass(
											"sorter_arrow_up sorter_arrow_dwn");
								}
							};

							$scope.getValueFormatted = function(val) {
								if (isNaN(val)) {
									return val;
								} else {
									return $scope.checkDecimal(val);
								}
							};

							$scope.getValue = function(row, key) {
								value = row[key.map];
								if (isNaN(value)) {
									return value;
								} else {
									return $scope.checkDecimal(value);
								}
							};
							$scope.checkDecimal = function(number) {
								if (typeof number != "undefined"
										&& number != null
										&& number.toString().indexOf('.') > 0) {

									number = parseInt(number);
								} else if (isNaN(number)) {
									number = "";
								}
								return number;
							};
							$scope.clickedPage = function(pageNumber) {
								$scope.currentPageNumber = (pageNumber - 1);
							};
						},
						scope : {
							ngshow : '=',
							caption : '@',
							oddRowClass : '@',
							evenRowClass : '@',
							headerNeed : '=',
							subHeaderNeed : '=',
							sortable : '=',
							pageSize : '=',
							paginator : '=',
							ngModel : '=',
							tableName : '@'
						},
						link : function(scope, elem, attrs) {
							scope.tableValueRows = "";
							eval("scope.tableValueRows = "
									+ attrs.tableValues.split("'").join('"')
											.split("ngModel").join(
													"scope.ngModel"));

							if (attrs.headerColumns != "") {
								eval("scope.headerColumns = "
										+ attrs.headerColumns.split("'").join(
												'"'));
							}
							if (attrs.subHeaderColumns != "") {
								eval("scope.subHeaderColumns = "
										+ attrs.subHeaderColumns.split("'")
												.join('"'));
							}
							if (scope.subHeaderColumns != undefined) {
								scope.paginatorColSpan = scope.subHeaderColumns.length;
							} else if (scope.headerColumns != undefined) {
								scope.paginatorColSpan = scope.headerColumns.length;
							}
							scope.pageList = [];
							var noOfPages = scope.tableValueRows.length
									/ scope.pageSize;
							for (var i = 0; i < noOfPages; i++) {
								scope.pageList[i] = (i + 1);
							}
							var updateTable = function() {
							};
							var tableValueRows;
							tableValueRows	= scope.$watch(scope.tableValueRows, function() {
								updateTable();
							}, true);
							
							scope.$on('$destroy', function() {
								elem.remove();
								tableValueRows();
							 });
						}
					};
				});

/* STAR RATING */

lifeEngagedirectives
		.directive(
				'starRating',
				function($parse) {
					return {
						restrict : "A",
						scope : true,
						link : function(scope, element, attrs) {

							if (angular.isDefined(attrs.starRating)
									&& (scope.LmsModel.Feedback.questions[$parse(
											attrs.starRating)(scope) - 1].option == scope.$index + 1)) {

								element.siblings('li').removeClass('selected');
								element.addClass('selected');
								element.prevAll('li').addClass('selected');

							}
							element
									.on(
											'click',
											function() {
												scope.LmsModel.Feedback.questions[$parse(
														attrs.starRating)
														(scope) - 1].option = (scope.$index + 1)
														.toString();
												element
														.siblings('li')
														.removeClass('selected');
												element.addClass('selected');
												element.prevAll('li').addClass(
														'selected');
												scope.refresh();
											});
							scope.$on('$destroy', function() {
								element.remove();
							 });
						}
					};
				});

/* STAR RATING */

/** Begin -- directives of Hari for FNA Module Optimisation -- Begin * */

lifeEngagedirectives.directive('circularwidgetproduct', function() {
	return {
		restrict : "A",
		scope : {},
		controller : "@",
		name : "controllerName",
		templateUrl : 'modules/fna/partials/circularwidgetproduct.html',
		replace : true
	}
});

lifeEngagedirectives.directive('circularwidgetcalc', function() {
	return {
		restrict : "A",
		scope : {},
		controller : "@",
		name : "controllerName",
		templateUrl : 'modules/fna/partials/circularwidgetcalc.html',
		replace : true
	}
});

/** Product Recomendation Page - Begin * */

lifeEngagedirectives.directive('carouselWidget', function() {
	return {
		restrict : "EA",
		replace : true,
		templateUrl : 'partials/elastiside-video-carousel.html',
		link : function(scope, element, attrs) {
			scope.currentid = attrs["id"];
			$("#" + scope.currentid).elastislide({
				minItems : 2
			});
			scope.$on('$destroy', function() {
				element.remove();
			 });
		}
	}
});

lifeEngagedirectives.directive('pdtcompare', function() {
	return {
		link : function(scope, element, attrs) {
			if (scope.$last) {
				setTimeout(function() {

					var owl = $("#owl-demo");

					owl.owlCarousel({
						items : 5, // 10 items above 1000px browser width
						itemsDesktop : [ 1000, 5 ], // 5 items between 1000px
						// and 901px
						itemsDesktopSmall : [ 900, 3 ], // betweem 900px and
						// 601px
						itemsTablet : [ 600, 2 ], // 2 items between 600 and 0
						itemsMobile : false,// itemsMobile disabled - inherit
						// from itemsTablet option
						pagination : false,
						mouseDrag : false,
						touchDrag : false

					});

					// Custom Navigation Events
					$(".next").click(function() {
						owl.trigger('owl.next');
					});
					$(".prev").click(function() {
						owl.trigger('owl.prev');
					});
					$(".play").click(function() {
						owl.trigger('owl.play', 1000); // owl.play event accept
						// autoPlay speed as
						// second parameter
					});
					$(".stop").click(function() {
						owl.trigger('owl.stop');
					});

					$(".pdtcompare").draggable({
						revert : true,
						helper : "clone",
						revertDuration : 100
					});

				}, 1);

			}
			
			scope.$on('$destroy', function() {
				element.remove();
			 });
		}
	}
});

/* FNA MY FAMILY DRAG & DROP */
lifeEngagedirectives.directive('myfamilydragdropwidget', function() {
	return {
		restrict : "EA",
		link : function(scope, element, attrs) {
			if (scope.$last) {
				setTimeout(function() {
					// Logic, initialization for drag & drop in controller

					scope.myFamilyDragDrop();
					scope.initialiseFamilyDetails();

					// Owl carousel initialization
					// CUSTOM OWL SLIDER - the array sets the number of items to
					// be shown corresponding to the screen width
					// For a screen width of 1200 and above, show 6 items, for
					// 1000 px upto 700 px, show 5

					var owl = $("#owl-demo");
					owl.owlCarousel({
						itemsCustom : [ [ 0, 2 ], [ 450, 3 ], [ 600, 4 ],
								[ 700, 5 ], [ 1000, 5 ], [ 1200, 6 ],
								[ 1400, 6 ], [ 1600, 6 ] ],
						navigation : true,
						pagination : false,
						rewindNav : false,
						startDragging : hideScrapButton
					});
					function hideScrapButton() {
						$('.scrapButton').css("display", "none");
					}
					;

				}, 1);
			}
			
			scope.$on('$destroy', function() {
				element.remove();
			 });
		}
	}
});

/* FNA SET GOALS DRAG & DROP */
lifeEngagedirectives.directive('infoIconVideoPopup', function() {
	return {
		link : function(scope, element, attrs) {
			element.on("click", function() {

				var videoPopOverSelectGoal = function(element) {
					/* CLICK OF INFORMATION ICON ON THE SET PREFERENCES PAGE */
					$('.videopopover').children().last().removeClass()
							.addClass("attacharrow");
					var thisObj = element;

					var individualOffset = findOffset(thisObj, -145, -210);
					var topVal = individualOffset[0];
					var leftval = individualOffset[1];

					var chk = $('.videopopover').css("display");
					if (chk == "none") {
						$('.videopopover').fadeIn();
						$('.videopopover').css({
							"display" : "block",
							"top" : topVal,
							"left" : leftval
						});
					}
					if (chk == "block") {
						$('.videopopover').fadeOut();
						$('.videopopover').css("display", "none");
					}
				}
				videoPopOverSelectGoal(element);
			});
			scope.$on('$destroy', function() {
				element.remove();
			 });
		}
	}
});

lifeEngagedirectives
		.directive(
				'setGoalPref',
				function() {
					return {
						link : function(scope, element, attrs) {

							var setGoalPrefDragDrop = function() {
								$(".goal").draggable({
									revert : true,
									helper : "clone",
									revertDuration : 0
								});

								$(".priorityorder")
										.droppable(
												{
													drop : function(event, ui) {
														// logic in utility.js
														var currentlyDragged = $(
																ui.draggable)
																.attr("class")
																.split(' ')[1]; // retire
														var chkFlag = $(this)
																.attr("class")
																.split(' ')[0]; // priorityorder
														var priority = $(this)
																.attr("id");
														if (chkFlag == "priorityorder") {
															scope
																	.setGoal(
																			scope,
																			priority,
																			currentlyDragged);
															var chk2 = $(this)
																	.hasClass(
																			"priorityorder");
															if (chk2) {
																$(this)
																		.removeClass()
																		.addClass(
																				'priorityset '
																						+ currentlyDragged
																						+ ' ');
																/*
																 * $(this).removeClass("priorityorder");
																 * $(this).children().remove();
																 */
																$(this)
																		.children(
																				".priority_number,.priority_txt")
																		.css(
																				"display",
																				"none");
															}
														}
													}
												});

								/*
								 * ON CLICK OF THE DROPPED ITEM, IT SHOULD SHOW
								 * THE SCRAP POP UP
								 */
								$(document.body).on(
										'click',
										'.item .priorityset',
										function(event) {
											thisObjDeletion = $(this);
											var individualOffset = findOffset(
													thisObjDeletion, -10, 80);
											var topVal = individualOffset[0];
											var leftval = individualOffset[1];
											// alert(topVal+"--"+leftval);
											var chk = $('.scrapButton').css(
													"display");
											if (chk == "none") {
												$('.scrapButton').fadeIn();
												$('.scrapButton').css({
													"display" : "block",
													"top" : topVal,
													"left" : leftval
												});
											} else if (chk == "block") {
												$('.scrapButton').css({
													"top" : topVal,
													"left" : leftval
												});
											}
											event.preventDefault();
										});

								/*
								 * CLICKING ANYWHERE OUTSIDE THE SCRAP BUTTON
								 * SHOULD HIDE IT
								 */
								$(document)
										.mouseup(
												function(e) {
													var container = $(".scrapButton");
													if (!container.is(e.target)
															&& container
																	.has(e.target).length === 0) {
														container.hide();
													}
												});

							};
							if (scope.$last) {
								setTimeout(function() {
									setGoalPrefDragDrop();

									var owl = $("#owl-demo");
									owl.owlCarousel({
										itemsCustom : [ [ 0, 2 ], [ 450, 3 ],
												[ 600, 4 ], [ 700, 4 ],
												[ 1000, 5 ], [ 1200, 5 ],
												[ 1400, 5 ], [ 1600, 5 ] ],
										navigation : true,
										pagination : false,
										startDragging : hideScrapButtonGoal
									});
									function hideScrapButtonGoal() {
										$('.scrapButton')
												.css("display", "none");
									}
									;

								}, 1);
							}
							
							
							scope.$on('$destroy', function() {
								element.remove();
							 });
						}
					}
				})

lifeEngagedirectives.directive('videoclosebtn', function() {
	return {
		link : function(scope, element, attrs) {
			element.on("click", function($event) {
				var chk = $('.videopopover').css("display");
				if (chk == "block") {
					$('.videopopover').fadeOut();
					$('.videopopover').css("display", "none");
				}
				$event.stopPropagation();
			});
			scope.$on('$destroy', function() {
				element.remove();
			 });
		}
	}
});

lifeEngagedirectives.directive('closeoverlaybtn', function() {
	return {
		link : function(scope, element, attrs) {
			element.on("click", function() {
				hideOverlay();
			});
			scope.$on('$destroy', function() {
				element.remove();
			 });
		}
	}
});

lifeEngagedirectives
		.directive(
				'deletegoals',
				function() {
					return {
						link : function(scope, element, attrs) {
							var onDelete = function() {
								var chkFlag = $('.scrapButton').css("display");
								if (chkFlag == "block") {
									$(thisObjDeletion).removeClass().addClass(
											"priorityorder ui-droppable");
									$(thisObjDeletion).children(
											".priority_number,.priority_txt")
											.css("display", "inline-block");
								}
								var priority = thisObjDeletion.attr("id");
								scope.removeGoal(scope, priority);
								initializeDrop();
								$('.scrapButton').css("display", "none");
							}

							var initializeDrop = function() {
								$(".priorityorder")
										.droppable(
												{
													drop : function(event, ui) {
														// logic in utility.js
														var currentlyDragged = $(
																ui.draggable)
																.attr("class")
																.split(' ')[1]; // retire
														var chkFlag = $(this)
																.attr("class")
																.split(' ')[0]; // priorityorder
														var priority = $(this)
																.attr("id");
														if (chkFlag == "priorityorder") {
															scope
																	.setGoal(
																			scope,
																			priority,
																			currentlyDragged);
															var chk2 = $(this)
																	.hasClass(
																			"priorityorder");
															if (chk2) {
																$(this)
																		.removeClass()
																		.addClass(
																				'priorityset '
																						+ currentlyDragged
																						+ ' ');
																/*
																 * $(this).removeClass("priorityorder");
																 * $(this).children().remove();
																 */
																$(this)
																		.children(
																				".priority_number,.priority_txt")
																		.css(
																				"display",
																				"none");
															}
														}
													}
												});
							};

							element.on("click", function() {
								onDelete();

							});
							
							scope.$on('$destroy', function() {
								element.remove();
							 });
						}
					}
				});

var changeArrowPosn = function(cordinateVal) {
	var modifiedVal = cordinateVal - 420 + "px";
	var flag = $('.videopopover').children().last().attr("class");
	if (flag == "attacharrow") {
		$('.videopopover').children().last().removeClass().addClass(
				"attacharrow_right");
	}
	return modifiedVal;
};

var findOffset = function(thisObj, x, y) {
	var cordinateVal = $(thisObj).offset();

	var topVal = cordinateVal.top + x + "px";
	
	if($(thisObj).hasClass('priorityset')){
		var leftVal = cordinateVal.left + y + "px";
	}
	else{
		var leftVal = (cordinateVal.left > 800) ? changeArrowPosn(cordinateVal.left)
				: cordinateVal.left + y + "px";
	}
	
	return [ topVal, leftVal ];
};

/* FNA SUMMARY */
lifeEngagedirectives.directive('circularchartwidget', function() {
	return {
		restrict : "A",
		scope : {},
		controller : "@",
		name : "controllerName",
		templateUrl : 'modules/fna/partials/circularchartwidget.html',
		replace : true
	}
});
lifeEngagedirectives.directive('chartwdg', function() {
	return {
		link : function(scope, element, attrs) {
			/*
			 * The plugin should be initialized only after the ng-repeat has
			 * finished its rendering. ( ie compile phase is complete )
			 */
			if (scope.$last) {
				setTimeout(function() {
					$('.arc_container_wrapper .arc_left').radialOptions({
						animationTime : 400, // set the animation duration
						selectedAction : "switch" // Two options -
					// switch/selectOnly
					});
				}, 1);
			}
			scope.$on('$destroy', function() {
				element.remove();
			 });
		}
	}
});

/* FNA CHOOSE PARTIES */
lifeEngagedirectives.directive('myselfchoosepartycomponent', function() {
	return {
		restrict : "EA",
		link : function(scope, element, attrs) {
			setTimeout(function() {
				scope.myselfChoosePartyDragDrop();
			}, 1);
			
			scope.$on('$destroy', function() {
				element.remove();
			 });
		}
	}
});
lifeEngagedirectives.directive('choosepartycomponent', function() {
	return {
		restrict : "EA",
		link : function(scope, element, attrs) {
			if (scope.$last) {
				setTimeout(function() {
					// Logic in utility.js
					scope.dependantChoosePartyDragDrop();
				}, 1);
			}
			scope.$on('$destroy', function() {
				element.remove();
			 });
		}
	}
});
/** Begin -- directives of Hari for FNA Module Optimisation -- End * */
storeApp.directive('onFinishRender', function ($timeout,$rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
        	var timer = null;
            if (scope.$last === true) {
                $timeout(function () {
                    $rootScope.$broadcast('ngRepeatFinished');
                });
            }
            
            scope.$on('$destroy', function() {
        	//element.remove();        	
            });
        }
    }
});
storeApp.directive('leCurrency', function ($filter, $locale, $timeout) {
		$locale.NUMBER_FORMATS.CURRENCY_SYM="";
		var decimalSep = $locale.NUMBER_FORMATS.DECIMAL_SEP;
		var toNumberRegex = new RegExp('[^0-9\\' + decimalSep + ']', 'g');
		var trailingZerosRegex = new RegExp('\\' + decimalSep + '0+$');
		var maxlengthModified = false;
		var initialMaxlength = 0;
		var filterFunc = function (value) {
		//return value ? (''+value).replace(/(\d)(?=(\d\d)+\d$)/g, "$1,"):'';
		return value ? (''+value).replace(/(.)(?=(.{3})+$)/g, "$1,"):'';
	}
	function toNumber(currencyStr) {
        return  currencyStr && isNaN(parseFloat(currencyStr.replace(toNumberRegex, ''))) ? '' :parseFloat(currencyStr.replace(toNumberRegex, ''));
    }

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function postLink(scope, elem, attrs, modelCtrl) {					
            modelCtrl.$formatters.push(filterFunc);
            modelCtrl.$parsers.push(function (newViewValue) {
                var oldModelValue = modelCtrl.$modelValue;
                var newModelValue = toNumber(newViewValue);
                modelCtrl.$viewValue = filterFunc(newModelValue);			    
				var commaSeparatedString = (modelCtrl.$viewValue).toString();
				var commaSeparated  = commaSeparatedString.split(',');
				var decimalSeparated =    commaSeparatedString.split('.');
				if(elem.attr('maxlength') && elem.attr('maxlength') > 0){
					var modifiedMaxLength = 0;				
					if(!maxlengthModified){						
						initialMaxlength = parseInt(elem.attr('maxlength'));
					}
					maxlengthModified = true;
					if(commaSeparated.length>1){
					    modifiedMaxLength = initialMaxlength+(commaSeparated.length-1);
				    }
					if(decimalSeparated.length>1){
						if(parseInt(modifiedMaxLength)>0)
							modifiedMaxLength = parseInt(modifiedMaxLength)+1;
						else
							modifiedMaxLength = parseInt(initialMaxlength)+1;
					}
					if(modifiedMaxLength==0){
						modifiedMaxLength = initialMaxlength;
					}
					elem.attr('maxlength',modifiedMaxLength);
						
				}							
				elem.val(modelCtrl.$viewValue);
                return newModelValue;
            });
            elem.on('keydown', function(){
                $timeout(function() {
                    elem[0].selectionStart = elem[0].selectionEnd = 10000;
                },0);

            });

        }
    };
});


storeApp.directive('leNumber', function ($filter, $locale) {
    $locale.NUMBER_FORMATS.CURRENCY_SYM="";
    var decimalSep = $locale.NUMBER_FORMATS.DECIMAL_SEP;
    var toNumberRegex = new RegExp('[^0-9\\' + decimalSep + ']', 'g');
  //  var trailingZerosRegex = new RegExp('\\' + decimalSep + '0+$');
    var filterFunc = function (value) {
        return $filter('number')(value);
    };



    function toNumber(currencyStr) {
        return parseFloat(currencyStr.replace(toNumberRegex, ''));
    }

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function postLink(scope, elem, attrs, modelCtrl) {

            modelCtrl.$formatters.push(filterFunc);
            modelCtrl.$parsers.push(function (newViewValue) {
                var oldModelValue = modelCtrl.$modelValue;
                var newModelValue = toNumber(newViewValue);
                modelCtrl.$viewValue = filterFunc(newModelValue);

                elem.val(modelCtrl.$viewValue);

                return newModelValue;
            });
        }
    };
});


function initializePayment() {

	var currentString = '';
	var a;
	var conf_text = '';

	$(".confirmtext").attr('readonly', true);
	randomCaptcha();
	a = $.trim($('#captcha').text().toLowerCase());

	$('.confirmtext').keyup(function(eventObj) {
		/* display the key and character code for the key you pressed */
		var key_local = getKey(eventObj);
		currentString = currentString + key_local;
		conf_text = $.trim($('.confirmtext').text());
		if (a == conf_text) {
			$('.confirmtext').css('background-color', 'green');
			$('.confirmtext').css('color', 'white');
			$('.captchabtn').removeClass("disabledbtn");
			$('.captchabtn').removeAttr("disabled");
		} else {
			$('.confirmtext').css('background-color', 'white');
			$('.confirmtext').css('color', '#000000');
			$('.captchabtn').addClass("disabledbtn");
			$('.captchabtn').prop('disabled', true);
		}

	});

	$('.captchabtn').click(function() {

	});

	$('.refresh_img').click(function() {
		$('.confirmtext').css('background-color', 'white');
		$('.confirmtext').css('color', '#000000');
		$('.captchabtn').prop('disabled', true);
		$('.captchabtn').addClass("disabledbtn");
		$('.confirmtext').text('');

		currentString = '';
		randomCaptcha();
		a = $.trim($('#captcha').text().toLowerCase());
	});
	function randomCaptcha() {
		var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
		var string_length = 6;
		var random_number = "", pos;
		while (string_length--) {

			pos = Math.floor(Math.random() * chars.length);
			// add the character from the base string to the array
			var rand = chars.substr(pos, 1);
			random_number = random_number + rand;
		}
		$('#captcha').text(random_number);
	};

	/* get key code */
	function getKeyCode(key) {
		// return the key code
		return (key == null) ? event.keyCode : key.keyCode;
	};

	function getKey(key) {
		// return the key
		return String.fromCharCode(getKeyCode(key)).toLowerCase();
	};

}