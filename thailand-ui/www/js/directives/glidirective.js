/*
 *Copyright 2015, LifeEngage 
 */



var gliLifeEngagedirectives = angular.module('lifeEngage.glidirective', []);
var plot = null;
var plot2 = null;
var plot3 = null;
var plot4 = null;
var plot5 = null;
var plot6 = null;
var plot7 = null;
gliLifeEngagedirectives.directive('phonenumberactionpopup', function ($compile, $parse) {
    return {
        restrict: 'C',
        scope: true,

        link: function (scope, element, attrs, ctrl) {
            element.bind('click', function () {
                var phoneNumber = element.context.innerText;
                scope.mobNumber = phoneNumber;
                if (phoneNumber) {
                    showPopup = function () {
                        // var thisObj = $(this);
                        var individualOffset = findOffset(element);
                        var topVal = individualOffset[0];
                        var leftval = individualOffset[1];
                        var arrowClass = individualOffset[2];
                        showPopupModal(".common_model_container", ".action_popup", topVal, leftval, arrowClass);
                    }

                    function findOffset(thisObj) {
                        var x = 30;
                        var y = -95;
                        var arrowClass = "arrow";
                        var cordinateVal = $(thisObj).offset();
                        var scrollTopVal = $('body').scrollTop();
                        var scrollLeftVal = $('body').scrollLeft();
                        var fixedTop = (cordinateVal.top - scrollTopVal);
                        var fixedLeft = (cordinateVal.left - scrollLeftVal);
                        if (window.innerHeight / 2 < fixedTop) {
                            x = -95;
                            arrowClass = "arrow-bottom ";
                        }
                        if (window.innerWidth / 2 > fixedLeft) {
                            y = -10;
                            arrowClass = arrowClass + " arrow-left";
                        } else {
                            arrowClass = arrowClass + " arrow-right";
                        }

                        var topVal = fixedTop + x + "px";
                        var leftVal = fixedLeft + y + "px";
                        return [topVal, leftVal, arrowClass];
                    }

                    function showPopupModal(modalPopupOverlay, modalCOntainer, top, left, arrowClass) {

                        var html = "<div class='" + arrowClass + "'></div>" +
                            "<ul>" +
                            "<li><a class='sms' target='_self' href='sms:" + scope.mobNumber + "'> SMS </a></li>" +
                            "<li class='mob-tab-only'><a class='call ' target='_self' href='tel:+" + scope.mobNumber + "'> Call </a></li>" +
                            "<li><a href='whatsapp://send?text=TextHere'> Whatsapp </a></li>" +
                            "<li><a href='line://msg/text/TextHere'> line </a></li>" +

                            "</ul>";
                        var disp = $(modalPopupOverlay).css("display");
                        if (disp == "none") {
                            $(modalPopupOverlay).css("display", "block");
                            $(modalCOntainer).css({
                                "display": "block",
                                "top": top,
                                "left": left
                            });
                            $compile(html)(scope);
                            $(modalCOntainer).html("");
                            $(modalCOntainer).append(html);

                        }

                        $(modalCOntainer).click(function () {
                            if (navigator.userAgent.indexOf("Android") > 0) {
                                $(this).css("display", "none");
                                $(modalPopupOverlay).css("display", "none");
                            }
                        });
                        $(modalPopupOverlay).click(function () {
                            $(this).css("display", "none");
                            $(modalCOntainer).css("display", "none");
                        });
                    }
                    showPopup();
                }



            });
            scope.$on('$destroy', function () {
                element.remove();
            });
        }
    };
});
gliLifeEngagedirectives
    .directive(
        'glideletegoals',
        function () {
            return {
                link: function (scope, element, attrs, $translate) {
                    var onDelete = function () {
                        var chkFlag = $('.scrapButton').css("display");
                        var priority = thisObjDeletion.attr("id");
                        if (chkFlag == "block") {
                            $(thisObjDeletion).removeClass().addClass("priorityorder ui-droppable");
                            scope.goalpref[priority - 1].translate = '';
                            scope.refresh();
                            $(thisObjDeletion).parent('.priorityGoal-holder').removeClass('item-dropped');
                        }

                        scope.removeGoal(scope, priority);
                        initializeDrop();
                        $('.scrapButton').css("display", "none");
                    }

                    var initializeDrop = function () {
                        $(".priorityorder")
                            .droppable({
                                drop: function (event, ui) {
                                    // logic in utility.js
                                    var currentlyDragged = $(
                                            ui.draggable)
                                        .attr("class")
                                        .split(' ')[1]; // retire
                                    var currentlyDraggedText = $(ui.draggable).parent().parent().find('.txtgoals').attr('translate');
                                    var chkFlag = $(this)
                                        .attr("class")
                                        .split(' ')[0]; // priorityorder
                                    var priority = $(this)
                                        .attr("id");
                                    if (chkFlag == "priorityorder") {
                                        scope
                                            .setGoal(
                                                scope,
                                                priority,
                                                currentlyDragged);
                                        var chk2 = $(this)
                                            .hasClass(
                                                "priorityorder");
                                        if (chk2) {
                                            $(this).removeClass().addClass('priorityset ' + currentlyDragged + ' ');
                                            scope.goalpref[priority - 1].translate = currentlyDraggedText;
                                            scope.refresh();
                                            $(this).parent('.priorityGoal-holder').addClass('item-dropped');
                                        }
                                    }
                                }
                            });
                    };

                    element.on("click", function () {
                        onDelete();

                    });

                    scope.$on('$destroy', function () {
                        element.remove();
                    });
                }
            }
        });
gliLifeEngagedirectives.directive(
    'gliSetGoalPref',
    function () {
        return {
            link: function (scope, element, attrs, $translate) {

                var setGoalPrefDragDrop = function () {
                    $(".goal").draggable({
                        revert: true,
                        helper: "clone",
                        revertDuration: 0
                    });

                    $(".priorityorder")
                        .droppable({
                            drop: function (event, ui) {
                                // logic in utility.js
                                var currentlyDragged = $(
                                        ui.draggable)
                                    .attr("class")
                                    .split(' ')[1]; // retire
                                var currentlyDraggedText = $(ui.draggable).parent().parent().find('.txtgoals').attr('translate');
                                var chkFlag = $(this)
                                    .attr("class")
                                    .split(' ')[0]; // priorityorder
                                var priority = $(this)
                                    .attr("id");
                                if (chkFlag == "priorityorder") {
                                    scope.setGoal(scope, priority, currentlyDragged);
                                    var chk2 = $(this).hasClass("priorityorder");
                                    if (chk2) {
                                        $(this).removeClass().addClass('priorityset ' + currentlyDragged + ' ');
                                        scope.goalpref[priority - 1].translate = currentlyDraggedText;
                                        scope.refresh();
                                        $(this).parent('.priorityGoal-holder').addClass('item-dropped');
                                    }
                                }
                            }
                        });

                    /*
                     * ON CLICK OF THE DROPPED ITEM, IT SHOULD SHOW
                     * THE SCRAP POP UP
                     */
                    $(document.body).on(
                        'click',
                        '.item .priorityset',
                        function (event) {
                            thisObjDeletion = $(this);
                            var individualOffset;
                            if ((rootConfig.isDeviceMobile)) {
                                individualOffset = findOffset(thisObjDeletion, -58, 0);
                            } else {
                                individualOffset = findOffset(thisObjDeletion, -110, 0);
                            }
                            var topVal = individualOffset[0];
                            var leftval = individualOffset[1];
                            // alert(topVal+"--"+leftval);
                            var chk = $('.scrapButton').css(
                                "display");
                            if (chk == "none") {
                                $('.scrapButton').fadeIn();
                                $('.scrapButton').css({
                                    "display": "block",
                                    "top": topVal,
                                    "left": leftval
                                });
                            } else if (chk == "block") {
                                $('.scrapButton').css({
                                    "top": topVal,
                                    "left": leftval
                                });
                            }
                            event.preventDefault();
                        });

                    /*
                     * CLICKING ANYWHERE OUTSIDE THE SCRAP BUTTON
                     * SHOULD HIDE IT
                     */
                    $('body').bind("touchend", function (e) {
                        var container = $(".scrapButton");
                        if (!container.is(e.target) &&
                            container
                            .has(e.target).length === 0) {
                            container.hide();
                        }
                        var scrapButtonCP = $('.cp_scrapButton ');
                        if (!scrapButtonCP.is(e.target) &&
                            container
                            .has(e.target).length === 0) {
                            scrapButtonCP.hide();
                        }
                    });

                };
                if (scope.$last) {
                    setTimeout(function () {
                        setGoalPrefDragDrop();

                        var owl = $("#owl-demo");
                        owl.owlCarousel({
                            itemsCustom: [[0, 2], [450, 3],
									[600, 4], [700, 4],
									[1000, 5], [1200, 5],
									[1400, 5], [1600, 5]],
                            navigation: true,
                            pagination: false,
                            startDragging: hideScrapButtonGoal
                        });

                        function hideScrapButtonGoal() {
                            $('.scrapButton')
                                .css("display", "none");
                        };

                    }, 1);
                }


                scope.$on('$destroy', function () {
                    element.remove();
                });
            }
        }
    });
gliLifeEngagedirectives.directive('carousalInitial', function () {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            var target = '#' + attrs.id;

            angular.element(target).on('shown', function () {
                angular.element(target).find('.item').removeClass('active');
                angular.element(target).find('.item:first-child').addClass('active');
            })
        }
    }
});
gliLifeEngagedirectives.directive("leadManagementScroll", function ($window) {
    return function (scope, element, attrs) {
        scope.mgmtType = "lms.activityManagement";
        angular.element(".agent_details_tbl .table-responsive").bind("scroll", function () {
            var scroll_pos = $(this).find("tbody tr:first-child th:nth-child(2)").offset().left;
            var act_width = 0;
            $(this).find(".act-col").each(function () {
                act_width += $(this).outerWidth();
            });
            if ((scroll_pos * -1) >= act_width) {
                scope.mgmtType = "lms.leadManagement";
            } else {
                scope.mgmtType = "lms.activityManagement";
            }
            scope.$apply();
        });
    };
});



gliLifeEngagedirectives.directive('scrollVertical', function () {
    return {
        restrict: 'A',
        controller: function ($scope) {
            /* scroll.bind(); */
            $(".agent_details_table tbody").on('scroll', function (event) {
                $(this).find("tr th.fixed-title").css("margin-top", (-1 * event.target.scrollTop));
                $(this).find("tr td.pop-links").css("margin-top", (-1 * event.target.scrollTop));
            });
        }
    };
});



gliLifeEngagedirectives.directive('imageMapModal', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            var target = '#' + attrs.id;

            angular.element(target).on('shown', function () {
                scope.imagePath = "css/generali-theme/img/";
                scope.imageChange = "maps.png";
                scope.refresh();
                angular.element(target).find('img[usemap]').rwdImageMaps();
                angular.element(target).find('area').on('click', function (e) {
                    var argg = e.currentTarget.attributes.name.nodeValue;
                    if (argg == "United States") {
                        scope.imageChange = "maps_unitedStates.jpg";
                    } else if (argg == "United Kingdom") {
                        scope.imageChange = "maps_unitedKingdom.jpg";
                    } else if (argg == "Singapore") {
                        scope.imageChange = "maps_singapore.jpg";
                    } else if (argg == "Indonesia") {
                        scope.imageChange = "maps_indonesia.jpg";
                    } else if (argg == "Australia") {
                        scope.imageChange = "maps_australia.jpg";
                    }
                    scope.refresh();

                });
            });
        }
    }
});
gliLifeEngagedirectives.directive('canvasResize', function () {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            el.attr('width', el.parent()[0].offsetWidth)
        }
    }
});
gliLifeEngagedirectives.directive('customSigPad', function () {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {

            /* var wrapper = el.parent(); */
            /* var canvas = wrapper.querySelector("canvas"); */
            scope.$parent.signaturePad = new SignaturePad(el[0]);

        }
    }
});
gliLifeEngagedirectives.directive('signWidth', function () {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
			var width=window.innerWidth;
			var height=window.innerHeight;
			//attrs.width=(window.innerWidth - 47) + "px";
			//attrs.height=(window.innerHeight - 47) + "px";
			var widthSignPad=(el.parent()[0].offsetWidth) + "px";
			el.attr('width',widthSignPad);
			scope.refresh();
        }
    }
});
gliLifeEngagedirectives.directive('syncPopover', function ($rootScope) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            attrs.$observe('errorpopup', function (value) {
                scope.errorpopup = scope.translateErrorMesgs(attrs.errorpopup);
                scope.showPopover = function () {
                    if (scope.errorpopup != "") {
                        angular.element(event.currentTarget).find('.popover-content').text(scope.errorpopup);
                        angular.element(event.currentTarget).find('.popover').show();
                        scope.OverlayPopoverFunction();
                    }
                };
            });
            $rootScope.hideOverlay = function () {
                scope.alertObj.showPopup = false;
                angular.element('.popover').hide();
                scope.closeOverlay();
            }
            scope.$on('$destroy', function () {
                el.remove();
            });
        }
    }
});

gliLifeEngagedirectives.directive('customPopover', function ($rootScope) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            attrs.$observe('phone', function (value) {
                scope.phone = attrs.phone;
                var popoverContentTmpl = "<table class='contact-panel'>" +
                    "<tr>" +
                    "<td width='80'class='text-center tooltip_Height'><a href='tel:+" + scope.phone + "'><img src='css/generali-theme/img/icon/call.png' alt='call' width='30' ></a></td>" +
                    
                    "<td width='80' class='text-center tooltip_Height'><a href='whatsapp://send?text=TextHere'><img src='css/generali-theme/img/icon/whatsapp.png' alt='whatsapp' width='30' ></a></td>" +
                    "<td width='80' class='text-center tooltip_Height'><a href='line://msg/text/TextHere'><img src='css/generali-theme/img/icon/line.png' alt='line' width='30' ></a></td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td class='text-center tooltip_Height'><span class=''>Call</span></td>" +
                    
                    "<td class='text-center tooltip_Height'><span class=''>WhatsApp</span></td>" +
                    "<td class='text-center tooltip_Height'><span class=''>LINE</span></td>" +
                    "</tr>" +
                    "</table>";
                scope.showPhonePopover = function () {
                    if ((rootConfig.isDeviceMobile)) {
                        if (scope.phone != "") {
                            if (event.target.nodeName == "IMG") {
                                scope.alertObj.showPopup = false;
                                angular.element('.popover').hide();
                                scope.overflowClass = false;
                                scope.closeOverlay();

                            } else {
                                angular.element(event.currentTarget).find('.popover-content').html(popoverContentTmpl);
                                angular.element(event.currentTarget).find('.popover').show();

                                scope.OverlayPopoverFunction();

                            }
                        }
                    }
                };

                $rootScope.hideOverlay = function () {
                    scope.alertObj.showPopup = false;
                    angular.element('.popover').hide();
                    scope.closeOverlay();
                }
            });

            scope.$on('$destroy', function () {
                el.remove();
            });
        }
    };
});
gliLifeEngagedirectives.directive('showMobilePopup', function ($rootScope, $compile, $templateRequest) {
    return {
        restrict: 'A',
        scope: {
            'userType': '=',
            'currentItem': '=',
            'editClick': '&',
            'closeClick': '&'
        },
        link: function (scope, el, attrs) {


            scope.closeClick = function () {
                $rootScope.alertObj.showPopup = false;
                angular.element('.popover-mobile').hide();
            }
            scope.editNavigateClick = function (id, item, statusItem) {
                scope.editClick()(id.item1, item.item2, statusItem.item3);
            }

            if (rootConfig.isDeviceMobile && !rootConfig.isOfflineDesktop) {
                scope.showMapIcon = true;
            } else {
                scope.showMapIcon = false;
            }

            scope.showRouteMap = function (currentItem) {
                scope.$parent.$parent.showRouteMap(currentItem.TransTrackingID, currentItem.Type, currentItem.Id);
                scope.$parent.$parent.showMap = true;
                scope.$parent.$parent.$apply();
            }
            scope.closeMap = function () {
                scope.$parent.$parent.showMap = false;
                scope.$parent.$parent.$apply();
            }

            el.find('td.show-pop-up:not(".no-click")').bind('click', function (e) {

                var popoverContentTmplP;
                $templateRequest("templates/mobilePopupTemplate.html").then(function (html) {
                    popoverContentTmplP = html;
                    popoverContentTmplP = $compile(popoverContentTmplP)(scope);
                    angular.element(e.currentTarget).parent().parent().parent().parent().parent().find('.popover-content-mobile').html(popoverContentTmplP);
                    angular.element(e.currentTarget).parent().parent().parent().parent().parent().find('.popover-mobile').show();
                    $rootScope.alertObj.showPopup = true;
                    $rootScope.refresh();
                });


            });

            function closeMobileOverlay() {
                scope.alertObj.showPopup = false;
                angular.element('.popover-mobile').hide();
            };

        }
    }
});
lifeEngagedirectives.directive('gliDefLookupDdlDirective', ['GLI_EvaluateService',function (GLI_EvaluateService) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs) {

            /*
                  * scope.$watch(attrs.ngModel, function(value) {
                  * scope.populateDependentLookupDate(attrs.childType,attrs.childName,scope.getValue(attrs.ngModel));
                
                  */
            attrs.$observe('observeattr', function (value) {
                /*if(attrs.childFilter){                       
                  			scope.populateDependentLookupDate(attrs.childType,
                                      attrs.childName,scope.getValue(attrs.childFilter),
                                      attrs.grandChildren,attrs.childModel);
                     	}
                     else{ */
               /* scope.populateDependentLookupDate(attrs.childType,
                    attrs.childName, scope.getValue(attrs.ngModel),
                    attrs.grandChildren, attrs.childModel,attrs.cacheable); */

                scope.populateDependentLookupDate(attrs.childType,
                    attrs.childName, GLI_EvaluateService.evaluateModel(scope, attrs.ngModel),
                    attrs.grandChildren, attrs.childModel);
                // } 
            });
            
           /* $(element).keydown(function () {  
                var id=event.target || event.srcElement;
			    var key = event.keyCode;
      	         if(id.id.indexOf('Title')>0){
		              if ((key >= 48 && key <= 57) || (key >= 96 && key <= 105)) {
      		            event.preventDefault();        
		          }
                 }
    		});
            
            $('#insuredTitle').on('keydown', function (event) {
                var id=event.target || event.srcElement;
			    var key = event.keyCode;
      	         if(id.id.indexOf('Title')>0){
		              if ((key >= 48 && key <= 57) || (key >= 96 && key <= 105)) {
      		            event.preventDefault();        
		          }
                 }
                });
            
            $('#payorTitle').on('keydown', function (event) {
                var id=event.target || event.srcElement;
			    var key = event.keyCode;
      	         if(id.id.indexOf('Title')>0){
		              if ((key >= 48 && key <= 57) || (key >= 96 && key <= 105)) {
      		            event.preventDefault();        
		          }
                 }
                }); */
            
            scope.$on('$destroy', function () {
                element.remove();
            });
        },
        controller: function ($scope, DataLookupService, $filter, $timeout) {

            $scope.updateOnBlur = function (model, modelString, list, arg) {
                var filteredList = $filter('filter')(list, model);
                var hasValue;
                if (filteredList != "undefined") {
                    if (filteredList.length > 0) {
                        if (model == undefined || model == "") {
                           // $scope.setValuetoScope(modelString, '');
                           GLI_EvaluateService.bindModel($scope, "$scope." + modelString, '');
                        } else {
                            for (var i = 0; i < filteredList.length; i++) {
                                if (filteredList[i].code === model) {
                                    /* $scope.setValuetoScope(modelString,'');
                                    $timeout(function(){ */
                                   // $scope.setValuetoScope(modelString, filteredList[i].code);
                                   GLI_EvaluateService.bindModel($scope, "$scope." + modelString, filteredList[i].code);
                                    /* },0); */
                                    hasValue = true;
                                    break;
                                }
                            }

                            if (!hasValue) {
                               // $scope.setValuetoScope(modelString, filteredList[0].code);
                               GLI_EvaluateService.bindModel($scope, "$scope." + modelString, filteredList[0].code);
                            }
                        }
                    } else {
                        //$scope.setValuetoScope(modelString, '');
                        GLI_EvaluateService.bindModel($scope, "$scope." + modelString, '');
                    }
                }
                $timeout(function () {
                    $scope.updateErrorCount(arg);

                }, 10);
            }
            $scope.populateIndependentLookupDate = function (type, fieldName, model, setDefault, field, currentTab,cacheable) {
					if (!cacheable){
						cacheable = false;
					}
                    if (!$scope[fieldName] || $scope[fieldName].length == 0) {
                        /*Changes done by LETeam Start - Bug 4178*/
                        if($scope.defaultBlankForDropDown){
                            $scope[fieldName] = [{
                            "key": "",
                            "value": ""
                               }];
                        } else {
                            $scope[fieldName] = [{
                            "key": "",
                            "value": "loading ...."
                               }];
                        } 
                        /*Changes done by LETeam End - Bug 4178*/  
                        var options = {};
                        options.type = type;
                        DataLookupService.getLookUpData(options, function (data) {
                            $scope[fieldName] = data;
                            if (setDefault) {
                                for (var i = 0; i < data.length; i++) {
                                    if (data[i].isDefault == 1) {
                                       // $scope.setValuetoScope(model, data[i].value);
                                       GLI_EvaluateService.bindModel($scope,model,data[i].value);
                                        break;
                                    }
                                }
                            } else {
								if(model)
									/*var n = "$scope." + model;
									var m = eval(n);
									if (m) {
										$scope.setValuetoScope(model, m);
									}*/
                                    var m = GLI_EvaluateService.evaluateModel($scope,model);
                                    if(typeof m != "undefined"){
                                        GLI_EvaluateService.bindModel($scope,model,m);
                                    }
								}
                            if (typeof field != "undefined" &&  field != '' && typeof currentTab != "undefined" && currentTab != "") {
                                setTimeout(function () {
									var dataEvaluvated = eval('$scope.' + currentTab + '.' + field);
									if (typeof dataEvaluvated != "undefined") {
										eval('$scope.' + currentTab + '.' + field).$render();
									}
                                }, 0);
                            }
                            if (typeof $scope.lmsSectionId != "undefined") {
                                /*if (eval('$scope.' + $scope.lmsSectionId + '.' + fieldName)) {
                                    eval('$scope.' + $scope.lmsSectionId + '.' + fieldName).$render();
                                }*/
                                if ($scope[$scope.lmsSectionId][fieldName]) {
                                    ($scope[$scope.lmsSectionId][fieldName]).$render();
                                }
                            }


                            /* if($scope.viewToBeCustomized) {

                					if($scope.viewToBeCustomized != undefined) {

                						 setTimeout(function(){
	                             		  	if(eval('$scope.'+$scope.viewToBeCustomized+'.'+fieldName)){
	                                 		  eval('$scope.'+$scope.viewToBeCustomized+'.'+fieldName).$render();
	                                 	  	}
                						 },0);
									  }*/

                            $scope.refresh();
                        }, function (errorData) {
                            $scope[fieldName] = [];

                        },cacheable);
                    }
                },

                $scope.populateDependentLookupDate = function (type, fieldName,
                    parentCode, grandChildren, childNgModel, cacheable) {
                    if (!cacheable){
                        cacheable = false;
                    }
                    if (parentCode && parentCode.trim() != "") {
                        $scope[fieldName] = [{
                            "key": "",
                            "value": "loading ...."
                               }];
                        var options = {};
                        options.type = type;
                        options.code = parentCode;
                        DataLookupService.getLookUpData(options, function (data) {
                            $scope[fieldName] = data;
                            if (childNgModel) {
                                if (data.length == 1 && data[0].isDefault == 1) {
                                   // $scope.setValuetoScope(childNgModel, data[0].value);
                                   GLI_EvaluateService.bindModel($scope, "$scope." + childNgModel, data[0].value);
                                    if ($scope.viewToBeCustomized) {
                                        /*if (eval('$scope.' + $scope.viewToBeCustomized + '.' + fieldName)) {
                                            eval('$scope.' + $scope.viewToBeCustomized + '.' + fieldName).$render();
                                        }*/
                                        if ($scope[$scope.viewToBeCustomized][fieldName]) {
                                            ($scope[$scope.viewToBeCustomized][fieldName]).$render();
                                        }
                                    }
                                }
                                /*else if(data.length==1 && data[0].code == "ALL_INDUSTRY"){
	                                      		var newoptions = {};
                               					newoptions.type = type;
                              					newoptions.code = "ALL_OCCUPATION";
		                              			DataLookupService.getLookUpData(newoptions, function(newdata) {		                                     
		                                    			 $scope[fieldName] = newdata;
		                                    			 
														if(newdata.length > 0){
				                                    	  for ( var i = 0; i < newdata.length; i++) {
			                                                  if(newdata[i].isDefault==1){
			                                                         $scope.setValuetoScope(childNgModel,newdata[i].value);
			                                                         break;
			                                                  }

				                                    	  }	
				                                    	}

				                                    	for ( var i = 0; i < newdata.length; i++) {
			                                                  if(newdata[i].isDefault==1){
			                                                         $scope.setValuetoScope(childNgModel,newdata[i].value);break;
			                                                  }
				                                    	}
				                                    
		                                      			
		                                    
		                               			},
		                               			 function(errorData) {
		                                     			 $scope[fieldName] = [];
		                                  
		  										})


	                                      } */
                                else if (data.length > 0) {
                                    for (var i = 0; i < data.length; i++) {
                                        if (data[i].isDefault == 1) {
                                           // $scope.setValuetoScope(childNgModel, data[i].value);
                                            GLI_EvaluateService.bindModel($scope, "$scope." + childNgModel, data[i].value);
                                            break;
                                        }
                                    }
                                    if ($scope.viewToBeCustomized) {
                                        /*if (eval('$scope.' + $scope.viewToBeCustomized + '.' + fieldName)) {
                                            eval('$scope.' + $scope.viewToBeCustomized + '.' + fieldName).$render();
                                        }*/
                                        if ($scope[$scope.viewToBeCustomized][fieldName]) {
                                            ($scope[$scope.viewToBeCustomized][fieldName]).$render();
                                        }
                                    }
                                    if (typeof $scope.lmsSectionId != "undefined") {
                                        /*if (eval('$scope.' + $scope.lmsSectionId + '.' + fieldName)) {
                                            eval('$scope.' + $scope.lmsSectionId + '.' + fieldName).$render();
                                        }*/
                                        if ($scope[$scope.lmsSectionId][fieldName]) {
                                            ($scope[$scope.lmsSectionId][fieldName]).$render();
                                        }
                                    }
                                }
                            }
                            $scope.refresh();
                            /*
                             * if (grantChildrens && grantChildrens.length > 0) {
                             * for (i=0; i< grantChildrens.length; i++) {
                             * $scope[grantChildrens[i]] = []; } }
                             */
                        }, function (errorData) {
                            $scope[fieldName] = [];

                        }, cacheable)
                        if (grandChildren && grandChildren != "") {
                            var grandChildrens = grandChildren.split(",");
                            for (var i = 0; i < grandChildrens.length; i++) {
                                $scope[grandChildrens[i]] = [];
                            }
                        }
                    } else {
                        $scope[fieldName] = [];
                        if (grandChildren != "" && typeof grandChildren != "undefined") {
                            var grandChildrens = grandChildren.split(",");
                            for (var i = 0; i < grandChildrens.length; i++) {
                                $scope[grandChildrens[i]] = [];
                            }
                        }

                    }
                } /*,$scope.setValuetoScope = function (model, value) {
                    var _lastIndex = model.lastIndexOf('.');
                    if (_lastIndex > 0) {
                        var parentModel = model.substring(0, _lastIndex);
                        var scopeVar = $scope.getValue(parentModel);
                        var remain_parentModel = model.substring(_lastIndex + 1, model.length);
                        scopeVar[remain_parentModel] = value;
                    } else {
                        $scope[model] = value;
                    }
                }, $scope.getValue = function (model) {
                    if ($scope.isVariableExists(model)) {
                        return eval('$scope.' + model)
                    }
                }, $scope.isVariableExists = function (variable) {
                    var modelData = variable;
                    variable = variable.split('.');
                    var obj = $scope[variable.shift()];
                    for (a in variable) {
                        if (variable[a].indexOf('[') >= 0) {
                            var length = variable[a].indexOf('[');
                            if (variable[a].indexOf(']') == (length + 2) || variable[a].indexOf(']') == (length + 3)) {
                                var flag = true;
                                break;
                            }
                        }
                    }
                    while (obj && variable.length)
                        obj = obj[variable.shift()];

                    if (flag == true)
                        return modelData;
                    else
                        return obj;
                }*/
        }
    }
}]);

gliLifeEngagedirectives.directive('gliLookupDdlDirective', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs) {
            attrs.$observe('observeattr', function (value) {
                scope.populateDependentLookupDate(attrs.childType, attrs.childName, scope.getValue(attrs.ngModel), attrs.grandChildren, attrs.childModel, attrs.grandChildType, attrs.successCall,attrs.cacheable);
            });
            scope.$on('$destroy', function () {
                element.remove();
            });
        },
        controller: function ($scope, DataLookupService) {
            $scope.populateIndependentLookupDate = function (type, fieldName, model,cacheable) {
				if (!cacheable){
					cacheable = false;
				}
                    if (!$scope[fieldName] || $scope[fieldName].length == 0) {
                        $scope[fieldName] = [{
                            "key": "",
                            "value": "loading ...."
                        }];
                        var options = {};
                        options.type = type;
                        DataLookupService.getLookUpData(options, function (data) {
                            $scope[fieldName] = data;
                            if ($scope.editFlowPackage == true) {
                                for (var i = 0; i < data.length; i++) {
                                    if (data[i].isDefault == 1) {
                                        $scope.setValuetoScope(model, data[i].code);
                                        break;
                                    }
                                }

                            } else {
                                var n = "$scope." + model;
                                var m = eval(n);
                                /*if($scope.viewToBeCustomized){
                                					if(eval($scope.viewToBeCustomized+'.'+fieldName)){
                                						if(fieldName.indexOf(',')>-1){
                                							var firstFieldName=fieldName.substring(0,(fieldName.indexOf(',')));
                                							eval($scope.viewToBeCustomized+'.'+firstFieldName).$render();
                                							var secondFieldName = fieldName.substring((fieldName.indexOf(',')+1),(fieldName.length));
                                							eval($scope.viewToBeCustomized+'.'+secondFieldName).$render();
                                						}
                                						else{
                                							eval($scope.viewToBeCustomized+'.'+fieldName).$render();
                                						}
                                						
                                					}
                                				}*/
                                $scope.setValuetoScope(model, m);
                            }
                        }, function (errorData) {
                            $scope[fieldName] = [];

                        }, cacheable);
                    }
                },

                $scope.populateDependentLookupDate = function (types, fieldName, parentCode, grandChildren, childNgModels, grandChildType, successCall, cacheable) {
                    if (!cacheable){
                        cacheable = false;
                    }
                    var test = "";
                    //var packageValue;
                    $scope.flag = true;

                    if (parentCode && parentCode.trim() != "") {
                        if (types != undefined && types != "") {
                            var type = types.split(",");
                            $scope.len = type.length;
                            var childNgModelArr = [];
                            if (childNgModels && childNgModels != "") {
                                childNgModelArr = childNgModels.split(",");
                            }

                            var j = 0;

                            function getLookUpValue() {


                                $scope[fieldName] = [{
                                    "key": "",
                                    "value": "loading ...."
                                }];
                                var options = {};
                                options.type = type[j];
                                options.code = parentCode;
                                DataLookupService.getLookUpData(options, function (data) {
                                    if (childNgModelArr.length > 0) {
                                        $scope[fieldName] = data;
                                        $scope[childNgModelArr[j]] = data[0].value;
                                        if ($scope.editFlowPackage == true) {
                                            for (var i = 0; i < data.length; i++) {
                                                if ($scope.flag == true) {
                                                    $scope.flag = false;
                                                    if (childNgModelArr[i]) {
                                                        $scope.setValuetoScope(childNgModelArr[i], data[0].value);
                                                        break;
                                                    }
                                                } else {
                                                    $scope.flag = true;
                                                    if (childNgModelArr[i + 1]) {
                                                        $scope.setValuetoScope(childNgModelArr[i + 1], data[0].value);
                                                        break;
                                                    }
                                                }
                                            }
                                        } else {
                                            if ($scope.len <= 1) {
                                                var n = "$scope." + childNgModels;
                                                var m = eval(n);
                                                //$scope.setValuetoScope(childNgModelArr[0],m); 
                                            } else {
                                                if ($scope.flag == true) {
                                                    $scope.flag = false;
                                                    //$scope.setValuetoScope(childNgModelArr[0],data[0].value);
                                                } else {
                                                    $scope.flag = true;
                                                    //$scope.setValuetoScope(childNgModelArr[1],data[0].value);
                                                }
                                            }
                                        }
                                    } else {
                                        $scope[fieldName] = data;
                                        for (var k = 0; k < data.length; k++) {
                                            if (data[k].isDefault == 1) {
                                                $scope.setValuetoScope(childNgModelArr[k], data[k].value);
                                                break;
                                            }
                                        }
                                    }
                                    if ($scope.viewToBeCustomized) {
                                        if (eval('$scope.' + $scope.viewToBeCustomized + '.' + fieldName)) {
                                            if (fieldName.indexOf(',') > -1) {
                                                var firstFieldName = fieldName.substring(0, (fieldName.indexOf(',')));
                                                eval('$scope.' + $scope.viewToBeCustomized + '.' + firstFieldName).$render();
                                                var secondFieldName = fieldName.substring((fieldName.indexOf(',') + 1), (fieldName.length));
                                                eval('$scope.' + $scope.viewToBeCustomized + '.' + secondFieldName).$render();
                                            } else {
                                                eval('$scope.' + $scope.viewToBeCustomized + '.' + fieldName).$render();
                                            }

                                        }
                                    }
                                    j++;
                                    if (j < type.length) {
                                        getLookUpValue();
                                    } else {
                                        $scope[successCall]();
                                    }
                                }, function (errorData) {
                                    $scope[fieldName] = [];
                                },cacheable)



                            }
                            if (j == 0) {
                                getLookUpValue();
                            }
                        }
                    }
                },

                $scope.getValue = function (model) {
                    if ($scope.isVariableExists(model)) {
                        return eval('$scope.' + model)
                    }
                }, $scope.setValuetoScope = function (model, value) {
                    var _lastIndex = model.lastIndexOf('.');
                    if (_lastIndex > 0) {
                        var parentModel = model.substring(0, _lastIndex);
                        var scopeVar = $scope.getValue(parentModel);
                        var remain_parentModel = model.substring(_lastIndex + 1, model.length);
                        scopeVar[remain_parentModel] = value;
                    }
                    $scope.refresh();
                },
                $scope.isVariableExists = function (variable) {
                    variable = variable.split('.')
                    var obj = $scope[variable.shift()];
                    while (obj && variable.length) obj = obj[variable.shift()];
                    return obj;
                }
        }
    }
});


gliLifeEngagedirectives.directive('radioCheck', function () {    
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {        
            element.bind('focus', function (event) {                  
                if (element.hasClass('ng-invalid')) {                    
                    //$("label[for='" + element.attr('id') + "'] div").addClass("radio-focus-invalid");
                } else {
                   // $("label[for='" + element.attr('id') + "'] div").addClass("radio-focus-valid");
                }
            });

            element.bind('blur', function (event) {
                $("label[for='" + element.attr('id') + "'] div").removeClass("radio-focus-valid radio-focus-invalid");
            });

            attrs.$observe('observeattr', function (value) {

                if (attrs.value == value) {
                    element.parent().addClass('checked');
                } else {
                    element.parent().removeClass('checked');
                }
            });
            scope.$on('$destroy', function () {
                element.remove();
            });
        }
    }
});
gliLifeEngagedirectives.directive('selectCheckBox', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {            
            element.bind('focus', function (event) {
                $(element.parent('.icheckbox')).addClass("radio-focus-invalid");
            });
            element.bind('blur', function (event) {
                $(element.parent('.icheckbox')).removeClass("radio-focus-invalid");
            });
        }
    }
});
gliLifeEngagedirectives.directive('swipeCarousel', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            angular.element(document.querySelector('#' + attrs.id)).bcSwipe({
                threshold: 5
            });
            angular.element(document.querySelector('#' + attrs.id)).carousel({
                pause: true,
                interval: false,
                wrap: false
            });

            scope.$on('$destroy', function () {
                element.remove();
            });
        }
    }
});
gliLifeEngagedirectives.directive('swipeCarouselLanding', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            attrs.$observe('observeattr', function (value) {
                if (value == "true" || value == "") {
                    //No Swipe for landing page content.
                } else {
                    angular.element(document.querySelector('#' + attrs.id)).bcSwipe({
                        threshold: 5
                    });
                    angular.element(document.querySelector('#' + attrs.id)).carousel({
                        pause: true,
                        interval: false,
                        wrap: false
                    });
                }
            });

            scope.$on('$destroy', function () {
                element.remove();
            });
        }
    }
});
gliLifeEngagedirectives.directive('slowSwipecarousel', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            angular.element(document.querySelector('#' + attrs.id)).bcSwipe({
                threshold: 50
            });
            angular.element(document.querySelector('#' + attrs.id)).carousel({
                pause: true,
                interval: false,
                wrap: false
            });

            scope.$on('$destroy', function () {
                element.remove();
            });
        }
    }
});
gliLifeEngagedirectives
    .directive(
        'timePicker',
        function (UtilityService) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    var androidVersion = UtilityService
                        .getAndroidVersion();
                    if (androidVersion == "5.0.1" ||
                        androidVersion == "5.0.2" || rootConfig.isDeviceMobile == false || rootConfig.isOfflineDesktop == true) {
                        var elmType = '#' + element[0].id;
                        $(elmType).attr('type', 'text');
                        $(elmType).timepicker({
                            showMeridian: false,
                            showSeconds: false,
                            minuteStep: 1,
                            disableFocus: true,
                            format: 'HH:mm'
                        });

                        $(elmType)
                            .on('hide.timepicker',
                                function (ev) {
                                    var id_elmnt = $(this)
                                        .attr('id');
                                    var id_elmnt_Model = $(this)
                                        .attr('ng-model');
                                    var selected_date = $(
                                            '#' + id_elmnt)
                                        .val();
                                    var modelObject = id_elmnt_Model
                                        .substring(
                                            0,
                                            id_elmnt_Model
                                            .lastIndexOf('.'));
                                    var modelVariable = id_elmnt_Model
                                        .substring(id_elmnt_Model
                                            .lastIndexOf('.') + 1);
                                    if (modelObject.length > 0) {
                                        modelObject = "." +
                                            modelObject;
                                    }
                                    eval('scope' + modelObject)[modelVariable] = selected_date;
                                    scope.refresh();
                                });

                    }
                    scope.$on('$destroy', function () {
                        element.remove();
                    });
                }
            }
        });
//for LMS calldetailssection alone
gliLifeEngagedirectives
    .directive(
        'timePickerLms',
        function (UtilityService) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    var androidVersion = UtilityService
                        .getAndroidVersion();
                    if (androidVersion == "5.0.1" ||
                        androidVersion == "5.0.2" || rootConfig.isDeviceMobile == false || rootConfig.isOfflineDesktop == true) {
                        var elmType = '#' + element[0].id;
                        $(elmType).attr('type', 'text');
                        $(elmType).attr('readonly', 'readonly');
                        $(elmType).timepicker({
                            defaultTime: false,
                            showMeridian: false,
                            showSeconds: false,
                            minuteStep: 1,
                            disableFocus: true,
                            format: 'HH:mm'
                        });

                        $(elmType)
                            .on('hide.timepicker',
                                function (ev) {

                                    $(this).removeClass('open');
                                    var id_elmnt = $(this)
                                        .attr('id');
                                    var id_elmnt_Model = $(this)
                                        .attr('ng-model');
                                    var widgetMin, widgetHour;

                                    widgetMin = $('.bootstrap-timepicker-minute').val();
                                    widgetHour = $('.bootstrap-timepicker-hour').val();
                                    if (widgetHour >= 24) {
                                        widgetHour = 0;
                                    }
                                    if (widgetMin >= 60) {
                                        widgetMin = 59;
                                    }
                                    var selected_date;
                                    if (ev.time.minutes != $('.bootstrap-timepicker-minute').val() || ev.time.hours != $('.bootstrap-timepicker-hour').val()) {
                                        selected_date = widgetHour + ':' + widgetMin;
                                    } else {
                                        selected_date = $(
                                                '#' + id_elmnt)
                                            .val();
                                    }

                                    var modelObject = id_elmnt_Model
                                        .substring(
                                            0,
                                            id_elmnt_Model
                                            .lastIndexOf('.'));
                                    var modelVariable = id_elmnt_Model
                                        .substring(id_elmnt_Model
                                            .lastIndexOf('.') + 1);
                                    if (modelObject.length > 0) {
                                        modelObject = "." +
                                            modelObject;
                                    }
                                    eval('scope' + modelObject)[modelVariable] = selected_date;
                                    scope.validateFields('callDetailsSection');
                                    scope.refresh();
                                });
                        $(elmType)
                            .on('show.timepicker',
                                function (ev) {
                                    if (androidVersion == "5.0.1" ||
                                        androidVersion == "5.0.2") {
                                        $('.bootstrap-timepicker-hour').attr('type', 'number');
                                        $('.bootstrap-timepicker-minute').attr('type', 'number');
                                        $('.bootstrap-timepicker-second').attr('type', 'number');
                                    }
                                    $(this).addClass('open');
                                    var id_elmnt = $(this)
                                        .attr('id');
                                    var id_elmnt_Model = $(this)
                                        .attr('ng-model');
                                    var selected_date = $('#' + id_elmnt).val();
                                    if (selected_date == "") {
                                        selected_date = new Date();
                                    }
                                    var modelObject = id_elmnt_Model
                                        .substring(
                                            0,
                                            id_elmnt_Model
                                            .lastIndexOf('.'));
                                    var modelVariable = id_elmnt_Model
                                        .substring(id_elmnt_Model
                                            .lastIndexOf('.') + 1);
                                    if (modelObject.length > 0) {
                                        modelObject = "." +
                                            modelObject;
                                    }
                                    eval('scope' + modelObject)[modelVariable] = selected_date;
                                    $(this).timepicker("setTime", selected_date);
                                    scope.refresh();
                                });
                    }

                    scope.$on('$destroy', function () {
                        element.remove();
                    });
                }
            }
        });

gliLifeEngagedirectives
    .directive(
        'timePickerDecl',
        function (UtilityService) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    var androidVersion = UtilityService
                        .getAndroidVersion();
                    if (androidVersion == "5.0.1" ||
                        androidVersion == "5.0.2" || rootConfig.isDeviceMobile == false || rootConfig.isOfflineDesktop == true) {
                        var elmType = '#' + element[0].id;
                        $('.cusTimepicker').attr('type', 'text');
                        $('.cusTimepicker').attr('readonly', 'readonly');
                        $('.cusTimepicker').timepicker({
                            defaultTime: false,
                            showMeridian: false,
                            showSeconds: false,
                            minuteStep: 1,
                            disableFocus: true,
                            format: 'HH:mm'
                        });

                        $(element)
                            .on('hide.timepicker',
                                function (ev) {

                                    $(this).removeClass('open');
                                    var id_elmnt = $(this)
                                        .attr('id');
                                    var id_elmnt_Model = $(this)
                                        .attr('ng-model');
                                    var widgetMin, widgetHour;

                                    widgetMin = $('.bootstrap-timepicker-minute').val();
                                    widgetHour = $('.bootstrap-timepicker-hour').val();
                                    if (widgetHour >= 24) {
                                        widgetHour = 0;
                                    }
                                    if (widgetMin >= 60) {
                                        widgetMin = 59;
                                    }
                                    var selected_date;
                                    if (ev.time.minutes != $('.bootstrap-timepicker-minute').val() || ev.time.hours != $('.bootstrap-timepicker-hour').val()) {
                                        selected_date = widgetHour + ':' + widgetMin;
                                    } else {
                                        selected_date = $(
                                                '#' + id_elmnt)
                                            .val();
                                    }

                                    var modelObject = id_elmnt_Model
                                        .substring(
                                            0,
                                            id_elmnt_Model
                                            .lastIndexOf('.'));
                                    var modelVariable = id_elmnt_Model
                                        .substring(id_elmnt_Model
                                            .lastIndexOf('.') + 1);
                                    if (modelObject.length > 0) {
                                        modelObject = "." +
                                            modelObject;
                                    }
                                    eval('scope' + modelObject)[modelVariable] = selected_date;
                                    scope.refresh();
                                });
                        $(element)
                            .on('show.timepicker',
                                function (ev) {
                                    if (androidVersion == "5.0.1" ||
                                        androidVersion == "5.0.2") {
                                        $('.bootstrap-timepicker-hour').attr('type', 'number');
                                        $('.bootstrap-timepicker-minute').attr('type', 'number');
                                        $('.bootstrap-timepicker-second').attr('type', 'number');
                                    }
                                    $(this).addClass('open');
                                    var id_elmnt = $(this)
                                        .attr('id');
                                    var id_elmnt_Model = $(this)
                                        .attr('ng-model');
                                    var selected_date = $('#' + id_elmnt).val();
                                    if (selected_date == "") {
                                        selected_date = new Date();
                                    }
                                    var modelObject = id_elmnt_Model
                                        .substring(
                                            0,
                                            id_elmnt_Model
                                            .lastIndexOf('.'));
                                    var modelVariable = id_elmnt_Model
                                        .substring(id_elmnt_Model
                                            .lastIndexOf('.') + 1);
                                    if (modelObject.length > 0) {
                                        modelObject = "." +
                                            modelObject;
                                    }
                                    eval('scope' + modelObject)[modelVariable] = selected_date;
                                    $(this).timepicker("setTime", selected_date);
                                    scope.refresh();
                                });
                    }

                    scope.$on('$destroy', function () {
                        element.remove();
                    });
                }
            }
        });

gliLifeEngagedirectives
    .directive(
        'timePickerNew',
        function (UtilityService) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    var androidVersion = UtilityService
                        .getAndroidVersion();
                    if (androidVersion == "5.0.1" ||
                        androidVersion == "5.0.2" || rootConfig.isDeviceMobile == false || rootConfig.isOfflineDesktop == true) {
                        var elmType = '#' + element[0].id;
                        $(elmType).attr('type', 'text');
                        $(elmType).attr('readonly', 'readonly');
                        $(elmType).timepicker({
                            defaultTime: false,
                            showMeridian: false,
                            showSeconds: false,
                            minuteStep: 1,
                            disableFocus: true,
                            format: 'HH:mm'
                        });

                        $(elmType)
                            .on('hide.timepicker',
                                function (ev) {

                                    $(this).removeClass('open');
                                    var id_elmnt = $(this)
                                        .attr('id');
                                    var id_elmnt_Model = $(this)
                                        .attr('ng-model');
                                    var widgetMin, widgetHour;

                                    widgetMin = $('.bootstrap-timepicker-minute').val();
                                    widgetHour = $('.bootstrap-timepicker-hour').val();
                                    if (widgetHour >= 24) {
                                        widgetHour = 0;
                                    }
                                    if (widgetMin >= 60) {
                                        widgetMin = 59;
                                    }
                                    var selected_date;
                                    if (ev.time.minutes != $('.bootstrap-timepicker-minute').val() || ev.time.hours != $('.bootstrap-timepicker-hour').val()) {
                                        selected_date = widgetHour + ':' + widgetMin;
                                    } else {
                                        selected_date = $(
                                                '#' + id_elmnt)
                                            .val();
                                    }

                                    var modelObject = id_elmnt_Model
                                        .substring(
                                            0,
                                            id_elmnt_Model
                                            .lastIndexOf('.'));
                                    var modelVariable = id_elmnt_Model
                                        .substring(id_elmnt_Model
                                            .lastIndexOf('.') + 1);
                                    if (modelObject.length > 0) {
                                        modelObject = "." +
                                            modelObject;
                                    }
                                    eval('scope' + modelObject)[modelVariable] = selected_date;
                                    scope.validateFields('leadDetailsSection');
                                    scope.refresh();
                                });
                        $(elmType)
                            .on('show.timepicker',
                                function (ev) {
                                    if (androidVersion == "5.0.1" ||
                                        androidVersion == "5.0.2") {
                                        $('.bootstrap-timepicker-hour').attr('type', 'number');
                                        $('.bootstrap-timepicker-minute').attr('type', 'number');
                                        $('.bootstrap-timepicker-second').attr('type', 'number');
                                    }
                                    $(this).addClass('open');
                                    var id_elmnt = $(this)
                                        .attr('id');
                                    var id_elmnt_Model = $(this)
                                        .attr('ng-model');
                                    var selected_date = $('#' + id_elmnt).val();
                                    if (selected_date == "") {
                                        selected_date = new Date();
                                    }
                                    var modelObject = id_elmnt_Model
                                        .substring(
                                            0,
                                            id_elmnt_Model
                                            .lastIndexOf('.'));
                                    var modelVariable = id_elmnt_Model
                                        .substring(id_elmnt_Model
                                            .lastIndexOf('.') + 1);
                                    if (modelObject.length > 0) {
                                        modelObject = "." +
                                            modelObject;
                                    }
                                    eval('scope' + modelObject)[modelVariable] = selected_date;
                                    $(this).timepicker("setTime", selected_date);
                                    scope.refresh();
                                });
                    }

                    scope.$on('$destroy', function () {
                        element.remove();
                    });
                }
            }
        });
gliLifeEngagedirectives.directive('submitHandler', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs, $parse) {
            $(element).keydown(function () {
                if (event.keyCode == 13) {
                    document.activeElement.blur();
                    return false;
                }
            });
            scope.$on('$destroy', function () {
                element.remove();
            });
        }
    }
});

gliLifeEngagedirectives.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {            
            modelCtrl.$parsers.push(function (inputValue) {                
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                //restrict to maxlength explicitly
                var len;
                if (attrs.fieldlength) {
                    len = attrs.fieldlength;
                }

                if (inputValue == undefined) return ''
                
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (inputValue.length > len) {
                    transformedInput = inputValue.substring(0, len);
                }
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});



gliLifeEngagedirectives.directive('numbersWithSlashOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            
            modelCtrl.$parsers.push(function (inputValue) {
                
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                //restrict to maxlength explicitly
                var len;
                if (attrs.fieldlength) {
                    len = attrs.fieldlength;
                }

                if (inputValue == undefined) return ''
                
                var transformedInput = inputValue.replace(/[^0-9\/]/g, '');
                if (inputValue.length > len) {
                    transformedInput = inputValue.substring(0, len);
                }
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});



gliLifeEngagedirectives.directive('houseNo', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                //restrict to maxlength explicitly
                var len;
                if (attrs.fieldlength) {
                    len = attrs.fieldlength;
                }

                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^0-9 \/\-]*/g, '');
                if (inputValue.length > len) {
                    transformedInput = inputValue.substring(0, len);
                }
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});

gliLifeEngagedirectives.directive('alphanumericWithSpecialcharecters', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^a-zA-Z0-9 \.\-\'\,]*$/, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});

gliLifeEngagedirectives.directive('alphanumericOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^a-zA-Z0-9 ]*$/, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});
gliLifeEngagedirectives.directive('alphabetsOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                
                if (attrs.fieldlength) {
                     len = attrs.fieldlength;
                 }
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^a-zA-Z ]*$/, '');
                
                if (inputValue.length > len) {
                     transformedInput = inputValue.substring(0, len);
                 } 
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});

gliLifeEngagedirectives.directive('maxLengthRestriction', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                var transformedInput,len;
                
                if (attrs.fieldlength) {
                     len = attrs.fieldlength;
                 }
                if (inputValue == undefined) return ''                
                
                if (inputValue.length > len) {
                     transformedInput = inputValue.substring(0, len);
                 }else{
                     transformedInput = inputValue;
                 } 
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});
gliLifeEngagedirectives.directive('noSpace', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[ ]/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});

gliLifeEngagedirectives.directive('donutChart', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs, $parse) {
            var chartId = attrs.id;
            var tableData;
            var data = [];
            var donutChartArr = [];
            var type = attrs.type;
            var colors = ["#ef654f", "#efbea8", "#6f7072", "#e9573d"];
            var startPlotting = function () {
                if (typeof (plot) != 'undefined') {
                    plot.destroy();
                }
                plot = $.jqplot(chartId, [donutChartArr], {
                    seriesColors: colors,

                    seriesDefaults: {
                        renderer: $.jqplot.DonutRenderer,
                        rendererOptions: {
                            sliceMargin: 0,
                            shadow: false,
                            startAngle: -90,
                            showDataLabels: false,
                            dataLabels: 'value',
                            thickness: 20,
                            padding: 10,
                        }
                    },
                    grid: {
                        background: 'transparent',
                        borderWidth: 0,
                        shadow: false,
                    }
                });
            }

            if (typeof (attrs.chartData) == 'undefined') {
                donutChartArr = [['d', 1]];
                colors = ["#cccccc"];
                startPlotting();
            }
            attrs.$observe('chartData', function (value) {
                if (attrs.chartData != "") {
                    tableData = angular.fromJson(attrs.chartData);

                    if (tableData.subModes.length != 0 && typeof (tableData.subModes.length) != "undefined" && (tableData.count > 0)) {
                        colors = ["#ef654f", "#efbea8", "#6f7072", "#e9573d"];
                        colors = colors.slice(0, tableData.subModes.length);
                        donutChartArr = [];
                        for (var i = 0; i < tableData.subModes.length; i++) {
                            donutChartArr.push([tableData.subModes[i].mode, parseInt(tableData.subModes[i].count)]);
                        }
                    } else {
                        donutChartArr = [['df', 1]];
                        colors = ["#cccccc"];
                    }
                    startPlotting();
                }

            });

            var plot;
            /*$(window).resize(function() {
            	plot.replot();
              }); */
            scope.$on('$destroy', function () {
                element.remove();

            });
        }
    }
});


gliLifeEngagedirectives.directive('fixedHeader1', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var normalCount = 3;
            var referralCount = 5;
            var limitCount;
            var set_height;
            var max_height = '490';
            var mid_height = '330';
            var mid_height_ipad = '360';
            if (angular.element(element).parent().parent('.min').length == 0) {
                limitCount = referralCount;
                set_height = max_height;
            } else {
                limitCount = normalCount;
                if ($('body').hasClass('ipad')) {
                    set_height = mid_height_ipad;
                } else {
                    set_height = mid_height;
                }
            }
            attrs.$observe('count', function (newVal, oldVal) {
                if (!(angular.element(element).parent().parent().hasClass('fht-table'))) {
                    if (scope.$eval(attrs.count) > limitCount) {
                        element.parent().parent().fixedHeaderTable({
                            height: set_height
                        });

                        setTimeout(function () {
                            var wrapper = angular.element(element).parent().parent().parent().parent().find('.fht-thead').html();
                            angular.element(element).parent().parent().parent().parent().find('.fht-thead').html($compile(wrapper)(scope));
                        }, 0);
                    }
                }
            });
        }
    }
});

gliLifeEngagedirectives.directive('showHideDropdown', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            $(document).bind('click', function (event) {
                if (scope.showDropDown) {
                    var isClickedElementChildOfPopup = element.find(event.target).length > 0 || $(event.target).hasClass('fa') || $(event.target).hasClass('btn-large');

                    if (isClickedElementChildOfPopup)
                        return;

                    scope.$apply(function () {
                        scope.showDropDown = false;
                    });
                }
            });

            scope.$on('$destroy', function () {
                element.remove();
            });
        }
    }
});

gliLifeEngagedirectives.directive('noScroll', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var header_height = $('.login.navbar-fixed-top').height();
            var footer_height = $('.footer').outerHeight();
            var total_height = header_height + footer_height;
            element.height(window.innerHeight - total_height);
            $(window).resize(function () {
                header_height = $('.navbar-fixed-top').height();
                footer_height = $('.footer').outerHeight();
                total_height = header_height + footer_height;
                element.height(window.innerHeight - total_height);
            });
            scope.$on('$destroy', function () {
                element.remove();
            });
        }
    }
});

gliLifeEngagedirectives.directive('gliRiderCheckbox', function () {
    return {
        restrict: 'A',
        controller: function ($scope, $translate) {
            $scope.disablechild = function (parent_name, children, disabling_value, planName) {
                    if (parent_name)
                        var parent_model = $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(parent_name)].isRequired;
                    if (parent_model != 'Yes') {
                        $scope[disabling_value] = true;
                        $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(parent_name)].healthUnitsOrPlanType = '';
                        //$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(parent_name)].sumInsured = '';
                        //	$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(parent_name)].maxCoverage = '';
                        if (children.length > 0) {
                            for (index in $scope.additionalInsuredChildren) {
                                var currentObject = $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex($scope.additionalInsuredChildren[index] + planName)];
                                currentObject.isRequired = 'No';
                                currentObject.healthUnitsOrPlanType = '';
                                currentObject.sumInsured = '';
                                //	currentObject.maxCoverage='';
                            }
                            $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(children)].isRequired = 'No';
                            $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(children)].healthUnitsOrPlanType = '';
                            //$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(children)].sumInsured = '';
                            //$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(children)].maxCoverage = '';
                        }
                    } else {
                        $scope[disabling_value] = false;

                    }
                },
                $scope.disablechildForDirectParent = function (parent_name, child_parent, plan_name) {
                    // parent_name will be index in case of child/parent additional isnured
                    if (!child_parent) {
                        var parent_model = $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(parent_name)].isRequired;
                        if (parent_model != 'Yes') {
                            $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(parent_name)].healthUnitsOrPlanType = '';
                            //$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(parent_name)].sumInsured = '';
                            //$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(parent_name)].maxCoverage = '';
                        }
                    } else {
                        if (parent_name != 'Yes') {
                            $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(model_string)].healthUnitsOrPlanType = '';
                            //$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(model_string)].sumInsured = '';
                            //$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(model_string)].maxCoverage = '';
                        }
                    }
                },
                $scope.getRiderIndex = function (riderNameWithInsured) {
                    var riderIndex;
                    for (k = 0; k < $scope.Illustration.Product.RiderDetails.length; k++) {
                        if ($scope.Illustration.Product.RiderDetails[k].uniqueRiderName == riderNameWithInsured) {
                            riderIndex = k;
                            break;
                        }
                    }
                    $scope.riderIndex = riderIndex;
                    return [riderIndex];

                }
        }
    }
});


//Custom directive for checkbox and text box clearing of GFO risk facilities
gliLifeEngagedirectives.directive('gliCheckboxDirective', function () {
    return {
        restrict: 'A',
        link: function (scope, element) {            
            element.bind('focus', function (event) {                
                $(element.parent('.icheckbox')).addClass("radio-focus-invalid");
            });
            element.bind('blur', function (event) {
                $(element.parent('.icheckbox')).removeClass("radio-focus-invalid");
            });
        },
        controller: function ($scope, $translate) {

            $scope.autoReentryRequired = false;

            $scope.evaluateCheckBoxModel = function (checkboxModel, childModel) {
                    $scope.autoReentryRequired = false;
                    if (childModel != '') {
                        var checkBoxChilds = childModel.split(',');
                    }


                    if ($scope.getValue(checkboxModel) == 'No') {
                        for (var i = 0; i < checkBoxChilds.length; i++) {
                            $scope.setValuetoScope(checkBoxChilds[i], 'No');
                            $scope.setValuetoScope(checkBoxChilds[i] + 'Value', '');
                        }
                    }
                },
                $scope.getRiderIndex = function (riderNameWithInsured) {
                    var riderIndex;
                    for (k = 0; k < $scope.Illustration.Product.RiderDetails.length; k++) {
                        if ($scope.Illustration.Product.RiderDetails[k].uniqueRiderName == riderNameWithInsured) {
                            riderIndex = k;
                            break;
                        }
                    }
                    $scope.riderIndex = riderIndex;
                    return [riderIndex];

                },
                $scope.populateListWithValues = function (parent_model, populationName, type) {
                    if (parent_model) {
                        if ($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(parent_model)]) {
                            var model = $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex(parent_model)].healthUnitsOrPlanType;
                            $scope[populationName] = [];
                            if (model && model != '') {
                                if (type) {
                                    var model = model.replace(/^\D+/g, '');
                                    for (var index = 1; index <= model; index++) {
                                        $scope[populationName].push({
                                            "code": ('Plan' + index),
                                            "value": ('Plan ' + index)
                                        });
                                    }
                                } else {
                                    for (var index = 1; index <= model; index++) {
                                        $scope[populationName].push({
                                            "value": index
                                        });
                                    }
                                }
                            }
                        }
                    }
                },
                $scope.evaluateCheckBoxWithSiblingandChild = function (siblingModel, checkboxModel, childModel, partnerModel) {
                    $scope.autoReentryRequired = false;
                    if (childModel != '' && (childModel + 'Value') != '' && childModel.indexOf(',') > 0 && (childModel + 'Value').indexOf(',') > 0) {
                        var checkBoxChilds = childModel.split(',');
                        var textBoxChilds = childModel.split(',');
                        for (index1 in textBoxChilds) {
                            textBoxChilds[index1] = textBoxChilds[index1] + 'Value';
                        }

                        if ($scope.getValue(checkboxModel) == 'No') {
                            for (var i = 0; i < checkBoxChilds.length; i++) {
                                $scope.setValuetoScope(checkBoxChilds[i], 'No');
                            }
                            if (textBoxChilds) {
                                for (var i = 0; i < textBoxChilds.length; i++) {
                                    $scope.setValuetoScope(textBoxChilds[i], '');
                                }
                            }

                            $scope.setValuetoScope(checkboxModel + 'Value', '');
                            $scope.setValuetoScope(checkboxModel, 'No');
                        } else {
                            $scope.autoReentryRequired = true;
                        }
                    } else {
                        var checkBoxChilds = childModel;
                        var textBoxChilds = childModel + 'Value';
                        if ($scope.getValue(checkboxModel) == 'No') {
                            if ($scope.getValue(partnerModel) == 'No' || !$scope.getValue(partnerModel)) {
                                $scope.setValuetoScope(checkBoxChilds, 'No');
                                $scope.setValuetoScope(textBoxChilds, '');
                            } else {
                                $scope.autoReentryRequired = true;
                            }
                            $scope.setValuetoScope(checkboxModel + 'Value', '');
                            $scope.setValuetoScope(checkboxModel, 'No');
                        } else if (checkBoxChilds != '') {
                            $scope.autoReentryRequired = true;
                        }
                    }

                    if ($scope.getValue(checkboxModel) == 'Yes' && $scope.getValue(siblingModel) == 'Yes') {
                        $scope.lePopupCtrl.showWarning(
                            translateMessages($translate,
                                "lifeEngage"),
                            translateMessages($translate,
                                "illustrator.targetRetuenWarning"),
                            translateMessages($translate,
                                "fna.ok"));
                        $scope.setValuetoScope(checkboxModel, 'No');
                        $scope.setValuetoScope(siblingModel, 'No');
                        $scope.setValuetoScope(siblingModel + 'Value', '');
                        $scope.setValuetoScope(childModel, 'No');
                        $scope.setValuetoScope(childModel + 'Value', '');
                        $scope.autoReentryRequired = false;
                    }
                },

                $scope.evaluateCheckBoxWithnone = function (chckBoxngmodel) {
                    if ($scope.getValue(chckBoxngmodel) == 'No') {
                        $scope.setValuetoScope((chckBoxngmodel + 'Value'), '');
                    }

                },
                $scope.setValuetoScope = function (model, value) {
                    if (model) {
                        var _lastIndex = model.lastIndexOf('.');
                        if (_lastIndex > 0) {
                            var parentModel = model.substring(0, _lastIndex);
                            var scopeVar = $scope.getValue(parentModel);
                            var remain_parentModel = model.substring(_lastIndex + 1, model.length);
                            scopeVar[remain_parentModel] = value;
                        }
                    }

                },
                $scope.getValue = function (model) {
                    if (model) {
                        if ($scope.isVariableExists(model)) {
                            return eval('$scope.' + model)
                        }
                    }

                },
                $scope.isVariableExists = function (variable) {
                    variable = variable.split('.')
                    var obj = $scope[variable.shift()];
                    while (obj && variable.length) obj = obj[variable.shift()];
                    return obj;
                }
        }
    }
});

gliLifeEngagedirectives
    .directive(
        'gliUiChart',
        function () {
            return {
                restrict: 'E',
                replace: true,
                link: function (scope, elem, attrs) {
                    /*
                     * Function to change X Axis Data and Label while
                     * changing value in select box
                     */
                    scope.updateXaxis = function (value, xAxisLabels) {
                        attrs.xaxis = value;
                        for (var i = 0; i < xAxisLabels.length; i++) {
                            if (xAxisLabels[i].value == value) {
                                attrs.xaxisdata = xAxisLabels[i].key;
                                break;
                            }
                        }
                        renderChart();
                    };
                    /*
                     * Function to change Y Axis Data and Label while
                     * changing value in select box
                     */
                    scope.updateYaxis = function (value, yAxisLabels) {
                        attrs.yaxis = value;
                        for (var i = 0; i < yAxisLabels.length; i++) {
                            if (yAxisLabels[i].value == value) {
                                attrs.yaxisdata = yAxisLabels[i].key;
                                break;
                            }
                        }
                        renderChart();
                    };

                    var renderChart = function () {

                        var tableData = scope[attrs.ngModel];
                        var data = tableData;
                        var chartId = attrs.id;
                        var type = attrs.type;
                        var xaxisLabel = scope.translateLabel(attrs.xaxis);
                        var yaxisLabel = scope.translateLabel(attrs.yaxis);
                        var chartTopLabel = scope.translateLabel(attrs.xaxistitletext);
                        var splitLabels = attrs.label.split(',');
                        var labelArray = [];
                        for (var i = 0; i < splitLabels.length; i++) {
                            labelArray.push(scope.translateLabel(splitLabels[i]));
                        }
                        var color = attrs.color.split(',');
                        if (attrs.label != undefined) {
                            var legend_array = attrs.label.split(',');
                            for (var legendArrayInsx in legend_array) {
                                legend_array[legendArrayInsx] = scope.translateLabel(legend_array[legendArrayInsx]);
                            }
                        }
                        if (attrs.series != undefined) {
                            var series_array = attrs.series.split(',');
                            var series = [],
                                xaxisArr = [];
                            for (var i = 0; i < series_array.length; i++) {
                                series.push({
                                    markerOptions: {
                                        style: series_array[i]
                                    }
                                });
                            }
                        }
                        if (attrs.xaxisdata && type != "barChart") {
                            data = [];
                            var xaxisdata = attrs.xaxisdata;
                            var yaxisdata = attrs.yaxisdata.split(',');
                            var maxXaxisData = attrs.xaxismaxvalue;
                            var tables = JSON.parse(attrs.ngModel1
                                .split("'").join('"'));
                            for (var i = 0; i < tables.length; i++) {
                                var tableData = scope.$eval(tables[i]);
                                for (var j = 0; j < yaxisdata.length; j++) {
                                    var dataArr = [];
                                    for (var k = 0; k < tableData.length; k++) {
                                        if (k == maxXaxisData) {
                                            break;
                                        }
                                        var chartObj = tableData[k];
                                        dataArr.push([chartObj[xaxisdata], chartObj[yaxisdata[j]]]);
                                        if (xaxisArr != null)
                                            xaxisArr.push(chartObj[xaxisdata]);
                                    } /*/tableData for loop*/
                                    data.push(dataArr);
                                } /*/yaxisdata for loop*/
                            } /*/table for loop*/
                        } /*/bar chart*/
                        /* $(window).resize(function() {
                                data.replot();
                        }); */
                        $.jqplot.config.enablePlugins = true;
                        if (type == "pieChart") {
                            piechart = jQuery.jqplot("#" + chartId, [data], {
                                title: xaxisLabel,
                                seriesDefaults: {
                                    shadow: true,
                                    renderer: jQuery.jqplot.PieRenderer,
                                    rendererOptions: {
                                        showDataLabels: true
                                    }
                                },
                                legend: {
                                    show: true,
                                    location: 's',
                                    placement: 'outside'
                                }
                            });
                        } else if (type == "linechart") {
                            $("#" + chartId).html('');
                            plot7 = $.jqplot(chartId, data, {

                                title: chartTopLabel,
                                axes: {
                                    xaxis: {

                                        ticks: xaxisArr,
                                        tickOptions: {
                                            formatString: '%d'
                                        },
                                        labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                                        pad: 0
                                    },
                                    yaxis: {

                                        labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                                        pad: 0
                                    }
                                },
                                grid: {
                                    gridLineColor: '#cccccc',
                                    gridLineWidth: 1,
                                    borderColor: "#ffffff",
                                    backgroundColor: "#ffffff",
                                    lineWidth: 2,
                                    xOffset: '54',
                                    shadow: false
                                },
                                seriesColors: color,
                                seriesDefaults: {
                                    lineWidth: 2,
                                    pointLabels: {
                                        show: false
                                    },
                                    rendererOptions: {
                                        smooth: true
                                    },
                                    markerOptions: {
                                        shadow: true,
                                        shadowDepth: 5,
                                        shadowAlpha: 1,
                                        shadowOffset: 0
                                    }
                                },
                                series: series,
                                legend: {
                                    show: true,
                                    location: 's',
                                    placement: 'outside',
                                    labels: legend_array
                                }
                            });
                        } else if (type == 'horizontalBarChart') {
                            // For horizontal bar charts, x an y values must will be "flipped"
                            // from their vertical bar counterpart.

                            var db = eval(attrs.ngModel1);
                            var d1 = db[0].split(".");
                            var graphPlot1 = "scope." + d1[0] + "." + d1[1];
                            var combinePlots = [];

                            combinePlots = eval(graphPlot1);
                            //For additional insured graph in Benefit Summary screen
                            var d1 = [];
                            var d2 = [];
                            var d3 = [];
                            var d4 = [];
                            var d5 = [];
                            var d6 = [];
                            if (Object.keys(combinePlots).length > 0) {
                                var riderNameArray = [];
                                var array = {
                                    'Ridername': '',
                                    'type': '',
                                    'value': ''
                                };
                                for (var index in combinePlots) {
                                    for (var newindex in combinePlots[index]) {
                                        array = JSON.parse(JSON.stringify(array));
                                        array.Ridername = index;
                                        array.type = newindex;
                                        array.value = eval("combinePlots['" + index + "']." + newindex);
                                        riderNameArray.push(array);
                                    }
                                }

                                var d = [];
                                for (i = 0; i < riderNameArray.length; i++) {
                                    arrayTest = [parseInt(riderNameArray[i].value), riderNameArray[i].Ridername];
                                    d.push(arrayTest)
                                }

                                var seriesType = [];
                                for (i = 0; i < riderNameArray.length; i++) {
                                    var arrayTestd1 = [];
                                    var arrayTestd2 = [];
                                    var arrayTestd3 = [];
                                    var arrayTestd4 = [];
                                    var arrayTestd5 = [];
                                    var arrayTestd6 = [];

                                    var selectedLabel = [];

                                    if (riderNameArray[i].type == "spouse" || riderNameArray[i].type == "Spouse" || riderNameArray[i].type == "spouseWOP" || riderNameArray[i].type == "spouseSurvivor" || riderNameArray[i].type == "spouseTermLife") {
                                        arrayTestd1 = [parseInt(riderNameArray[i].value), riderNameArray[i].Ridername];
                                        d1.push(arrayTestd1);
                                        selectedLabel = {
                                            label: labelArray[0]
                                        };
                                        for (d = 0; d < Object.keys(selectedLabel).length; d++) {
                                            if (seriesType.length < 1) {
                                                seriesType.push(selectedLabel);
                                            } else {
                                                if (JSON.stringify(seriesType[d]) != JSON.stringify(selectedLabel)) {
                                                    seriesType.push(selectedLabel);
                                                }
                                            }
                                        }
                                    }
                                    if (riderNameArray[i].type == "child1" || riderNameArray[i].type == "Child1" || riderNameArray[i].type == "child1CIAdd" || riderNameArray[i].type == "child1Term") {
                                        arrayTestd2 = [parseInt(riderNameArray[i].value), riderNameArray[i].Ridername];
                                        d2.push(arrayTestd2);
                                        selectedLabel = {
                                            label: labelArray[1]
                                        };
                                        for (d = 0; d < Object.keys(selectedLabel).length; d++) {
                                            if (seriesType.length < 1) {
                                                seriesType.push(selectedLabel);
                                            } else {
                                                if (JSON.stringify(seriesType[d]) != JSON.stringify(selectedLabel)) {
                                                    seriesType.push(selectedLabel);
                                                }
                                            }
                                        }
                                    }
                                    if (riderNameArray[i].type == "child2" || riderNameArray[i].type == "Child2" || riderNameArray[i].type == "child2CIAdd" || riderNameArray[i].type == "child2Term") {
                                        arrayTestd3 = [parseInt(riderNameArray[i].value), riderNameArray[i].Ridername];
                                        d3.push(arrayTestd3);
                                        selectedLabel = {
                                            label: labelArray[2]
                                        };
                                        for (d = 0; d < Object.keys(selectedLabel).length; d++) {
                                            if (seriesType.length < 1) {
                                                seriesType.push(selectedLabel);
                                            } else {
                                                if (JSON.stringify(seriesType[d]) != JSON.stringify(selectedLabel)) {
                                                    seriesType.push(selectedLabel);
                                                }
                                            }
                                        }
                                    }
                                    if (riderNameArray[i].type == "child3" || riderNameArray[i].type == "Child3" || riderNameArray[i].type == "child3CIAdd" || riderNameArray[i].type == "child3Term") {
                                        arrayTestd4 = [parseInt(riderNameArray[i].value), riderNameArray[i].Ridername];
                                        d4.push(arrayTestd4);
                                        selectedLabel = {
                                            label: labelArray[3]
                                        };
                                        for (d = 0; d < Object.keys(selectedLabel).length; d++) {
                                            if (seriesType.length < 1) {
                                                seriesType.push(selectedLabel);
                                            } else {
                                                if (JSON.stringify(seriesType[d]) != JSON.stringify(selectedLabel)) {
                                                    seriesType.push(selectedLabel);
                                                }
                                            }
                                        }
                                    }
                                    if (riderNameArray[i].type == "parent1" || riderNameArray[i].type == "Father" || riderNameArray[i].type == "parent1WOP" || riderNameArray[i].type == "parent1Survivor" || riderNameArray[i].type == "parent1Term") {
                                        arrayTestd5 = [parseInt(riderNameArray[i].value), riderNameArray[i].Ridername];
                                        d5.push(arrayTestd5);
                                        selectedLabel = {
                                            label: labelArray[4]
                                        };
                                        for (d = 0; d < Object.keys(selectedLabel).length; d++) {
                                            if (seriesType.length < 1) {
                                                seriesType.push(selectedLabel);
                                            } else {
                                                if (JSON.stringify(seriesType[d]) != JSON.stringify(selectedLabel)) {
                                                    seriesType.push(selectedLabel);
                                                }
                                            }
                                        }
                                    }
                                    if (riderNameArray[i].type == "parent2" || riderNameArray[i].type == "Mother" || riderNameArray[i].type == "parent2WOP" || riderNameArray[i].type == "parent2Survivor" || riderNameArray[i].type == "parent2Term") {
                                        arrayTestd6 = [parseInt(riderNameArray[i].value), riderNameArray[i].Ridername];
                                        d6.push(arrayTestd6);
                                        selectedLabel = {
                                            label: labelArray[5]
                                        };
                                        for (d = 0; d < Object.keys(selectedLabel).length; d++) {
                                            if (seriesType.length < 1) {
                                                seriesType.push(selectedLabel);
                                            } else {
                                                if (JSON.stringify(seriesType[d]) != JSON.stringify(selectedLabel)) {
                                                    seriesType.push(selectedLabel);
                                                }
                                            }
                                        }
                                    }
                                }


                                var horizontalChartData = [];
                                if (d1.length != 0) {
                                    horizontalChartData.push(d1);
                                }
                                if (d2.length != 0) {
                                    horizontalChartData.push(d2);
                                }
                                if (d3.length != 0) {
                                    horizontalChartData.push(d3);
                                }
                                if (d4.length != 0) {
                                    horizontalChartData.push(d4);
                                }
                                if (d5.length != 0) {
                                    horizontalChartData.push(d5);
                                }
                                if (d6.length != 0) {
                                    horizontalChartData.push(d6);
                                }

                                //var xaxisLabel = "Thousands in IDR";

                                var last_element = horizontalChartData[horizontalChartData.length - 1];

                                var digitCount = last_element[0][0].toString().length;
                                var maxLimit = Math.pow(10, digitCount);
                                var firstDigit = last_element[0][0].toString()[0];
                                if (firstDigit < 5) {
                                    maxLimit = maxLimit / 2;
                                }
                                var division = maxLimit / 5;
                                var xLabels = [];
                                for (i = 0; i <= maxLimit; i = i + division) {
                                    xLabels.push(i);
                                }

                                var ticks19 = xLabels;

                                var labelsdkb = seriesType;

                                plot2 = $.jqplot(chartId, horizontalChartData, {

                                    seriesDefaults: {
                                        renderer: $.jqplot.BarRenderer,
                                        // Show point labels to the right ('e'ast) of each bar.
                                        // edgeTolerance of -15 allows labels flow outside the grid
                                        // up to 15 pixels.  If they flow out more than that, they 
                                        // will be hidden.
                                        pointLabels: {
                                            show: true,
                                            location: 'e',
                                            edgeTolerance: -15,
                                            xpadding: -50
                                        },
                                        // Rotate the bar shadow as if bar is lit from top right.

                                        // Here's where we tell the chart it is oriented horizontally.
                                        rendererOptions: {
                                            barDirection: 'horizontal',
                                            barWidth: 13,
                                            barMargin: 20,
                                            barPadding: 2
                                        },
                                        shadow: false
                                    },
                                    legend: {
                                        show: true,
                                        placement: 'outsideGrid'
                                    },
                                    grid: {
                                        gridLineColor: '#DEDEDE',
                                        borderColor: "#ffffff",
                                        shadow: false,
                                        background: '#ffffff',
                                    },
                                    series: labelsdkb,
                                    seriesColors: ['#4198AF', '#71588F', '#89A54E', '#DB843D', '#AA4643', '#4572A7'],
                                    axes: {
                                        xaxis: {
                                            label: xaxisLabel,
                                            ticks: ticks19,
                                            labelOptions: {
                                                textColor: '#C21B17'
                                            },

                                            tickOptions: {
                                                formatString: "%'d"
                                            },
                                            labelRenderer: $.jqplot.CanvasAxisLabelRenderer
                                        },
                                        yaxis: {
                                            renderer: $.jqplot.CategoryAxisRenderer,
                                            tickOptions: {
                                                showGridline: false,
                                                formatString: "%'d"
                                            }
                                        }
                                    }
                                });
                            }
                        } else if (type == 'combinedChart') {
                            var graph = eval(attrs.ngModel1);

                            var d1 = graph[0].split(".");
                            var graphPlot1 = "scope." + d1[0] + "." + d1[1];
                            var combinePlots = eval(graphPlot1);

                            var dataArray1 = [];
                            var dataArray2 = [];
                            var dataArray3 = [];
                            for (i = 0; i < combinePlots.length; i++) {
                                var accumlatedArray = [];
                                var totalBenefitArray = [];
                                var sumArray = [];

                                accumlatedArray.push(combinePlots[i].ageOfLifeAssured, combinePlots[i].accumulatedPremium);
                                dataArray1.push(accumlatedArray);
                                totalBenefitArray.push(combinePlots[i].ageOfLifeAssured, combinePlots[i].totalBenefit);
                                dataArray2.push(totalBenefitArray);
                                sumArray.push(combinePlots[i].ageOfLifeAssured, combinePlots[i].sumAssured);
                                dataArray3.push(sumArray);
                            }

                            d1 = dataArray1;
                            d2 = dataArray2;
                            d3 = dataArray3;

                            var last_element = dataArray2[dataArray2.length - 1];

                            var digitCount = last_element[1].toString().length;
                            var maxLimit = Math.pow(10, digitCount);
                            var startValue = 10000;
                            var xLabels = [];
                            for (i = startValue; i <= maxLimit; i = i * 10) {
                                xLabels.push(i);
                            }

                            var ticks19 = xLabels;
                            //var yaxisLabel="Amount in '000 IDR";
                            //var xaxisLabel="Age";

                            plot3 = $.jqplot(chartId, [d1, d3, d2], {
                                stackSeries: false,
                                seriesColors: ['#c32a24', '#9ECA3F', '#36A7C9'],
                                seriesDefaults: {
                                    renderer: $.jqplot.BarRenderer,
                                    rendererOptions: {
                                        barMargin: 10
                                    },
                                    pointLabels: {
                                        show: false,
                                        stackedValue: true
                                    },
                                    shadow: false
                                },
                                grid: {
                                    gridLineColor: '#DEDEDE',
                                    borderColor: "#ffffff",
                                    shadow: false,
                                    background: '#ffffff',
                                },
                                legend: {
                                    show: true,
                                    placement: 'outsideGrid'
                                },
                                series: [{
                                        label: labelArray[0]
                                    },
                                    {
                                        label: labelArray[1],
                                        disableStack: false,
                                        renderer: $.jqplot.LineRenderer,
                                        lineWidth: 1,
                                        pointLabels: {
                                            show: false
                                        },
                                        markerOptions: {
                                            size: 0
                                        }
                                  },
                                    {
                                        label: labelArray[2],
                                        disableStack: false,
                                        renderer: $.jqplot.LineRenderer,
                                        lineWidth: 1,
                                        pointLabels: {
                                            show: false
                                        },
                                        markerOptions: {
                                            size: 0
                                        }
                                  }],

                                axesDefaults: {
                                    tickRenderer: $.jqplot.CanvasAxisTickRenderer
                                },
                                axes: {
                                    xaxis: {
                                        label: xaxisLabel,
                                        renderer: $.jqplot.CategoryAxisRenderer,
                                        tickOptions: {
                                            showGridline: false
                                        }
                                    },
                                    yaxis: {
                                        label: yaxisLabel,
                                        autoscale: true,
                                        renderer: $.jqplot.LogAxisRenderer,
                                        ticks: ticks19,
                                        tickOptions: {
                                            formatString: "%'d"
                                        },
                                        labelRenderer: $.jqplot.CanvasAxisLabelRenderer
                                    }
                                }
                            });
                        } else if (type == "comparisonChart") {
                            plot4 = $.jqplot("#" + chartId,
                                data, {
                                    title: '',
                                    seriesDefaults: {
                                        showMarker: false,
                                        pointLabels: {
                                            show: true,
                                            edgeTolerance: 5
                                        }
                                    },
                                    axesDefaults: {
                                        showTicks: true,
                                        showTickMarks: true
                                    },
                                    legend: {
                                        show: true,
                                        location: 's',
                                        placement: 'outside'
                                    }
                                });
                        } else if (type == "barChart") {
                            var tables = JSON.parse(attrs.ngModel1.split("'").join('"'));
                            if (attrs.barcalc == "true") {
                                var d1 = tables[0].split(".");
                                var graphPlot1 = "scope." + d1[0] + "." + d1[1];
                                var combinePlots = eval(graphPlot1);
                                /* var yaxisLabel="Amount in '000 IDR"; */
                                var ticks1 = [];
                                var dataArray = [];
                                var dkb1 = [];
                                var dkb2 = [];
                                for (var i = 0; i < Object.keys(combinePlots).length; i++) {
                                    dkb1 = [];
                                    ticks1.push(combinePlots[i].Ridername);
                                    dkb1.push(combinePlots[i].Ridername, combinePlots[i].value);
                                    dkb2.push(dkb1);
                                }
                                for (var i = 0; i < dkb2.length; i++) {
                                    dataArray.push(dkb2[i]);
                                }
                            } else {
                                var graphPlot1 = "scope." + eval(tables);
                                dataArray = eval(graphPlot1);
                                /* var yaxisLabel="IDR (Monthly Savings in Thousands)"; */
                            }
                            var xaxispointlabels = false;
                            if (attrs.xaxispointlabels != undefined) {
                                if (attrs.xaxispointlabels == "true") {
                                    xaxispointlabels = true;
                                } else
                                    xaxispointlabels = false;
                            }
                            if (attrs.chartbackground != undefined) {
                                var chartbackground = attrs.chartbackground;
                            }
                            if (attrs.yaxislabelcolor != undefined) {
                                var yaxislabelcolor = attrs.yaxislabelcolor;
                            }
                            var showgridline = false;
                            if (attrs.showgridline != undefined) {
                                if (attrs.showgridline == "true") {
                                    showgridline = true;
                                } else
                                    showgridline = false;
                            }
                            if (attrs.yaxisLabel != undefined) {
                                var yaxisLabel = scope.translateLabel(attrs.yaxisLabel);
                            }

                            var id = attrs.id;
                            var data = dataArray;
                            var barWidthCustom = 0;
                            var dataLength = data.length;
                            if (dataLength <= 4) {
                                barWidthCustom = 30;
                            } else {
                                barWidthCustom = null;
                            }
                            plot5 = $.jqplot(id, [data], {
                                seriesColors: ['#c32a24'],
                                seriesDefaults: {
                                    renderer: $.jqplot.BarRenderer,
                                    pointLabels: {
                                        show: true,
                                        edgeTolerance: -15
                                    },
                                    rendererOptions: {
                                        barMargin: 5,
                                        barWidth: barWidthCustom
                                    },
                                    shadow: false
                                },
                                grid: {
                                    /* gridLineColor: '#DEDEDE',
                                    borderColor: "#eee9e9", */
                                    shadow: false,
                                    background: chartbackground,
                                    drawBorder: false,
                                    borderWidth: 0
                                },
                                axes: {
                                    xaxis: {
                                        renderer: $.jqplot.CategoryAxisRenderer,
                                        rendererOptions: {
                                            tickRenderer: $.jqplot.CanvasAxisTickRenderer
                                        },
                                        tickOptions: {
                                            show: xaxispointlabels,
                                            showGridline: false,
                                            angle: -40
                                        }
                                        /*    showTicks: xaxispointlabels */
                                    },
                                    yaxis: {
                                        label: yaxisLabel,
                                        labelOptions: {
                                            textColor: yaxislabelcolor
                                        },
                                        tickOptions: {
                                            formatString: "%'d",
                                            show: true,
                                            showGridline: showgridline
                                        },
                                        labelRenderer: $.jqplot.CanvasAxisLabelRenderer
                                    }

                                }
                            });
                            $("#" + chartId).bind('jqplotDataClick', function (ev, seriesIndex, pointIndex, dataArray) {
                                scope.graphPopover = true;
                                scope.refresh();
                                var tables = JSON.parse(attrs.ngModel1.split("'").join('"'));
                                var d1 = tables[0].split(".");
                                var graphPlot1 = "scope." + d1[0] + "." + d1[1];
                                var combinePlots = eval(graphPlot1);
                                var dkb1 = [];
                                var dkb2 = [];
                                for (var i = 0; i < Object.keys(combinePlots).length; i++) {
                                    ticks19 = [];
                                    ticks08 = [];
                                    ticks19.push(combinePlots[i].Ridername);
                                    dkb1.push(ticks19);
                                    ticks08.push(combinePlots[i].translatedTooltip);
                                    dkb2.push(ticks08);
                                }

                                $('#graph-popover .heading').html(dkb1[pointIndex]);
                                $('#graph-popover .graph-popover-content').html(dkb2[pointIndex]);

                                var elem = $("#" + chartId + " .jqplot-point-" + pointIndex);
                                var position = elem.position();
                                var positionTop = position.top - $('#graph-popover').height() - 32;
                                var positionLeft = position.left + 16;
                                $('#graph-popover').css({
                                    "top": positionTop,
                                    "left": positionLeft
                                });
                            });

                        } else if (type == "fnaChart") {


                            if (attrs.label != undefined) {
                                if (attrs.label.indexOf("labels") != -1) {
                                    legend_array = scope.$eval(attrs.label);
                                } else {
                                    legend_array = attrs.label;
                                }
                                var legend_array = legend_array.split(',');
                                legend_array.reverse();
                                for (var legendArrayInsx in legend_array) {
                                    legend_array[legendArrayInsx] = scope.translateLabel(legend_array[legendArrayInsx]);
                                }
                            }
                            if (attrs.yaxisLabel != undefined) {
                                var yaxisLabel = scope.translateLabel(attrs.yaxisLabel);
                            }
                            if (attrs.chartbackground != undefined) {
                                var chartbackground = attrs.chartbackground;
                            }
                            var xaxispointlabels = false;
                            if (attrs.xaxispointlabels != undefined) {
                                if (attrs.xaxispointlabels == "true") {
                                    xaxispointlabels = true;
                                } else
                                    xaxispointlabels = false;
                            }
                            if (attrs.yaxislabelcolor != undefined) {
                                var yaxislabelcolor = attrs.yaxislabelcolor;
                            }
                            var showgridline = false;
                            if (attrs.showgridline != undefined) {
                                if (attrs.showgridline == "true") {
                                    showgridline = true;
                                } else
                                    showgridline = false;
                            }
                            if (attrs.chartlocation != undefined) {
                                var chartlocation = attrs.chartlocation;
                            }
                            if (attrs.chartplacement != undefined) {
                                var chartplacement = attrs.chartplacement;
                            }
                            var tablesPlottingValues;
                            if (typeof scope.$eval(attrs.xaxislabels) == "object") {
                                tablesPlottingValues = scope.$eval(attrs.xaxislabels);
                            } else {
                                tablesPlottingValues = attrs.xaxislabels;
                                tablesPlottingValues = JSON.parse(tablesPlottingValues.split(
                                    "'").join('"'));
                            }

                            var tables = JSON.parse(attrs.ngModel1
                                .split("'").join('"'));
                            for (var i = 0; i < tables.length; i++) {
                                var ticks1 = [];
                                var dataArray = [];
                                var title = tables[i].title;
                                var tableData = angular.copy(scope
                                    .$eval(tables[i].table));
                                var tableAttributes = tablesPlottingValues[i];
                                var stackSeriesArray1 = [];
                                var stackSeriesArray2 = [];
                                var stackSeriesArray3 = [];
                                var ticksArray = [];
                                var customLabel = [];
                                //FNA changes by LE Team Start
                                if(tableAttributes[i].length>2){
                                //FNA changes by LE Team End
                                for (var j = 0; j <= tableAttributes[i].length; j++) {
                                    /* dataArray.push(tableData[tableAttributes[i][j].key]); */

                                    if (j == 0) {
                                        if (tableData[tableAttributes[i][j].key] < 0) {
                                            tableData[tableAttributes[i][j].key] = 0;
                                        }
                                        stackSeriesArray1.push((tableData[tableAttributes[i][j].key]) / 1000);
                                        stackSeriesArray2.push(0);
                                        stackSeriesArray3.push(0);
                                        if (tableData[tableAttributes[i][j].key] == 0) {
                                            ticksArray.push("0");
                                            customLabel.push(' ');
                                        } else {
                                            ticksArray.push(Math.round((tableData[tableAttributes[i][j].key]) / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                                            customLabel.push(' ');
                                        }
                                    } else if (j == 1) {
                                        if (tableData[tableAttributes[i][j].key] < 0) {
                                            tableData[tableAttributes[i][j].key] = 0;
                                            ticksArray.push(" ");
                                        }
                                        stackSeriesArray1.push(0);
                                        stackSeriesArray2.push((tableData[tableAttributes[i][j].key]) / 1000);

                                        ticksArray.push(Math.round((tableData[tableAttributes[i][j].key]) / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

                                    } else if (j == 2) {
                                        if (tableData[tableAttributes[i][j].key] < 0) {
                                            tableData[tableAttributes[i][j].key] = 0;
                                            //ticksArray.push(" ");
                                        }
                                        stackSeriesArray3.push((tableData[tableAttributes[i][j].key]) / 1000);
                                        ticksArray.push(" ");
                                        customLabel.push(Math.round((tableData[tableAttributes[i][j].key]) / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                                    } else {
                                        stackSeriesArray1.push(0);
                                        stackSeriesArray2.push(0);
                                        stackSeriesArray3.push(0);
                                        //	ticksArray.push(0);
                                    }
                                    /* ticks1.push(tableAttributes[i][j].value); */
                                }}
                                //FNA changes by LE Team Start
                                else{
                                 for (var j = 0; j <= tableAttributes[i].length; j++) {
                                    /* dataArray.push(tableData[tableAttributes[i][j].key]); */

                                    if (j == 0) {
                                        if (tableData[tableAttributes[i][j].key] < 0) {
                                            tableData[tableAttributes[i][j].key] = 0;
                                        }
                                        stackSeriesArray1.push((tableData[tableAttributes[i][j].key]) / 1000);
                                        stackSeriesArray2.push(0);
                                        stackSeriesArray3.push(0);
                                        if (tableData[tableAttributes[i][j].key] == 0) {
                                            ticksArray.push("0");
                                            customLabel.push(' ');
                                        } else {
                                            ticksArray.push(Math.round((tableData[tableAttributes[i][j].key]) / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                                            customLabel.push(' ');
                                        }
                                    } else if (j == 1) {
                                        if (tableData[tableAttributes[i][j].key] < 0) {
                                            tableData[tableAttributes[i][j].key] = 0;
                                            ticksArray.push(" ");
                                        }
                                        stackSeriesArray1.push(0);
                                        stackSeriesArray2.push((tableData[tableAttributes[i][j].key]) / 1000);

                                        ticksArray.push(Math.round((tableData[tableAttributes[i][j].key]) / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

                                    } else {
                                        stackSeriesArray1.push(0);
                                        stackSeriesArray2.push(0);
                                        stackSeriesArray3.push(0);
                                        //	ticksArray.push(0);
                                    }
                                    /* ticks1.push(tableAttributes[i][j].value); */
                                }   
                                    
                                }
                                //FNA changes by LE Team End
                                var data = [];

                                plot6 = $
                                    .jqplot(
                                        chartId, [stackSeriesArray1, stackSeriesArray2, stackSeriesArray3], {
                                            // Only animate if
                                            // we're not using
                                            // excanvas (not in
                                            // IE 7 or IE 8)..
                                            title: title,
                                            animate: !$.jqplot.use_excanvas,
                                            /* series :ticks1, */
                                            seriesColors: color,
                                            stackSeries: true,
                                            seriesDefaults: {
                                                renderer: $.jqplot.BarRenderer,
                                                pointLabels: {
                                                    show: true,
                                                    stackedValue: true
                                                },
                                                rendererOptions: {
                                                    barMargin: 10,
                                                    barWidth: 60
                                                },
                                                shadow: false
                                            },
                                            grid: {
                                                /* gridLineColor: '#DEDEDE',
                                                borderColor: "#eee9e9", */
                                                shadow: false,
                                                background: chartbackground,
                                                drawBorder: false,
                                                borderWidth: 0
                                            },
                                            axesDefaults: {
                                                tickRenderer: $.jqplot.CanvasAxisTickRenderer
                                            },
                                            series: [
                                                {
                                                    pointLabels: {
                                                        show: false
                                                    }
																	},
                                                {
                                                    pointLabels: {
                                                        show: false
                                                    }
																	},
                                                {
                                                    pointLabels: {
                                                        show: true,
                                                        labels: customLabel,
                                                        ypadding: 5
                                                    }
																	}
																],
                                            axes: {
                                                xaxis: {
                                                    renderer: $.jqplot.CategoryAxisRenderer,
                                                    tickOptions: {
                                                        show: true,
                                                        showGridline: false,
                                                        formatString: "%'d"
                                                    },
                                                    ticks: ticksArray,
                                                    showTicks: xaxispointlabels
                                                },
                                                yaxis: {
                                                    label: yaxisLabel,
                                                    labelOptions: {
                                                        textColor: yaxislabelcolor
                                                    },
                                                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                                                    tickOptions: {
                                                        show: true,
                                                        showGridline: showgridline,
                                                        formatString: "%'d"
                                                    },
                                                    showTicks: true
                                                }

                                            },
                                            legend: {
                                                show: true,
                                                location: chartlocation,
                                                placement: chartplacement,
                                                labels: legend_array
                                            },

                                            highlighter: {
                                                show: false
                                            }
                                        });
                            }
                            var legend = $("#" + chartId).find('table.jqplot-table-legend tbody');
                            legend.children().each(function (i, tr) {
                                legend.prepend(tr)
                            });
                            /* $("#" + chartId).remove(); */
                            for (var i = 0; i < tables.length; i++) {
                                for (var j = 0; j <= tableAttributes[i].length; j++) {
                                    
                                    //FNA changes by LE Team
                                    if(j==2 && tableAttributes[i].length==2){                                        
                                            $($("#" + chartId + " tr.jqplot-table-legend").get(j)).hide();
                                    }
                                    else if (tableData[tableAttributes[i][j].key] == 0) {
                                        if (i == 0 && j == 0) {}
											
                                            // $($("#" + chartId + " tr.jqplot-table-legend").get(j)).hide();
                                    }
                                }

                            }
                        }
                        $("#" + chartId).bind("jqplotClick", function (ev, gridpos, datapos, neighbor) {
                            /*if (neighbor) {
                              alert('x:'
                                  + neighbor.data[0]
                                  + ' y:'
                                  + neighbor.data[1]);
                            }*/
                        });
                    };

                    chartbinddata = scope.$watch(attrs.ngModel,
                        function () {
                            renderChart();
                        }, true);
                    scope.$on('$destroy', function () {
                        elem.remove();
                        if (plot) {
                            plot.destroy();
                        }
                        if (plot2) {
                            plot2.destroy();
                        }
                        if (plot3) {
                            plot3.destroy();
                        }
                        if (plot4) {
                            plot4.destroy();
                        }
                        if (plot5) {
                            plot5.destroy();
                        }
                        if (plot6) {
                            plot6.destroy();
                        }
                        if (plot7) {
                            plot7.destroy();
                        }
                        chartbinddata();
                    });
                }
            };
        });
gliLifeEngagedirectives
    .directive(
        'gliCustomTable',
        function () {
            return {
                restrict: 'A',
                replace: true,
                templateUrl: 'templates/tableDirective.html',
                controller: function ($scope, $http) {
                    $scope.headerOrder = "order";
                    $scope.subHeaderOrder = "order";
                    $scope.subHeaderPredicate = '';
                    $scope.subHeaderReverse = false;
                    $scope.headerPredicate = '';
                    $scope.headerReverse = false;
                    $scope.subHeaderPaginatorPredicate = '';
                    $scope.subHeaderPaginatorReverse = false;
                    $scope.headerPaginatorPredicate = '';
                    $scope.headerPaginatorReverse = false;

                    $scope.currentPageNumber = 0;
                    $scope.paginatorColSpan;

                    $scope.getValuesOrdered = function (key,
                        clickedValue, e) {
                        if (clickedValue == 'subHeader') {
                            if ($scope.paginator) {
                                $scope.subHeaderPaginatorPredicate = key;
                                $scope.subHeaderPaginatorReverse = !($scope.subHeaderPaginatorReverse);
                            } else {
                                $scope.subHeaderPredicate = key;
                                $scope.subHeaderReverse = !($scope.subHeaderReverse);
                            }
                        } else if (clickedValue == 'header') {
                            if ($scope.paginator) {
                                $scope.headerPaginatorPredicate = key;
                                $scope.headerPaginatorReverse = !($scope.headerPaginatorReverse);
                            } else {
                                $scope.headerPredicate = key;
                                $scope.headerReverse = !($scope.headerReverse);
                            }
                        }

                        if ($(e.target).hasClass("sorter_arrow_dwn")) {
                            $(e.target).toggleClass(
                                "sorter_arrow_dwn sorter_arrow_up");
                        } else {
                            $(e.target).toggleClass(
                                "sorter_arrow_up sorter_arrow_dwn");
                        }
                    };

                    $scope.getValueFormatted = function (val) {
                        if (isNaN(val)) {
                            return val;
                        } else {
                            return $scope.checkDecimal(val);
                        }
                    };

                    $scope.getValue = function (row, key) {
                        value = row[key.map];
                        if (isNaN(value)) {
                            return value;
                        } else {
                            return $scope.checkDecimal(value);
                        }
                    };
                    $scope.checkDecimal = function (number) {

                        if (isNaN(number)) {

                            return number;
                        } else if (typeof number != "undefined" &&
                            number != null &&
                            number.toString().indexOf('.') > 0) {
                            number = parseInt(number);
                        }

                        return number;
                    };
                    $scope.clickedPage = function (pageNumber) {
                        $scope.currentPageNumber = (pageNumber - 1);
                    };
                },
                scope: {
                    ngshow: '=',
                    caption: '@',
                    oddRowClass: '@',
                    evenRowClass: '@',
                    headerNeed: '=',
                    subHeaderNeed: '=',
                    mappingOrder: '=',
                    sortable: '=',
                    pageSize: '=',
                    paginator: '=',
                    ngModel: '=',
                    tableName: '@',
                    tableType: '@',
                    columnClass: '@'
                },
                link: function (scope, elem, attrs) {
                    scope.tableValueRows = "";

                    if (attrs.tableValues) {
                        eval("scope.tableValueRows = " +
                            attrs.tableValues.split("'").join('"')
                            .split("ngModel").join(
                                "scope.ngModel"));
                    }

                    if (attrs.headerColumns != "") {
                        eval("scope.headerColumns = " +
                            attrs.headerColumns.split("'").join(
                                '"'));
                    }
                    if (attrs.subHeaderColumns != "") {
                        eval("scope.subHeaderColumns = " +
                            attrs.subHeaderColumns.split("'")
                            .join('"'));
                    }
                    if (scope.subHeaderColumns != undefined) {
                        scope.paginatorColSpan = scope.subHeaderColumns.length;
                    } else if (scope.headerColumns != undefined) {
                        scope.paginatorColSpan = scope.headerColumns.length;
                    }
                    scope.pageList = [];
                    if (scope.tableValueRows) {
                        var noOfPages = scope.tableValueRows.length /
                            scope.pageSize;
                    }

                    for (var i = 0; i < noOfPages; i++) {
                        scope.pageList[i] = (i + 1);
                    }
                    var updateTable = function () {};
                    var tablevalueRows;
                    tablevalueRows = scope.$watch(scope.tableValueRows, function () {
                        updateTable();
                    }, true);

                    scope.$on('$destroy', function () {
                        elem.remove();
                        tablevalueRows();
                    });
                }
            };
        });

/* STAR RATING */
/** Begin -- directives of Hari for FNA Module Optimisation -- End * */
storeApp.directive('onFinishRender', function ($timeout, $rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            var timer = null;
            if (scope.$last === true) {
                $timeout(function () {
                    $rootScope.$broadcast('ngRepeatFinished');
                });
            }

            scope.$on('$destroy', function () {
                //element.remove();        	
            });
        }
    }
});
gliLifeEngagedirectives
    .directive(
        'gliStarRating',
        function ($parse) {
            return {
                restrict: "A",
                scope: true,
                link: function (scope, element, attrs) {

                    if (angular.isDefined(attrs.gliStarRating) &&
                        (scope.LmsModel.Feedback.questions[$parse(
                            attrs.gliStarRating)(scope) - 1].option == scope.$index + 1)) {

                        element.siblings('i').removeClass('selected');
                        element.addClass('selected');
                        element.prevAll('i').addClass('selected');

                    }
                    element
                        .on(
                            'click',
                            function () {
                                scope.LmsModel.Feedback.questions[$parse(
                                            attrs.gliStarRating)
                                        (scope) - 1].option = (scope.$index + 1)
                                    .toString();
                                element
                                    .siblings('i')
                                    .removeClass('selected');
                                element.addClass('selected');
                                element.prevAll('i').addClass(
                                    'selected');
                                scope.refresh();
                            });
                }
            };
        });
gliLifeEngagedirectives
    .directive(
        'gliJqslider',
        function (UtilityService, $translate) {
            'use strict';
            return {
                restrict: 'E',
                transclude: true,
                replace: true,
                template: '<div></div>',
                link: function (scope, element, attrs) {

                    var newModel = ((attrs.val).replace(/FNAObject/g,
                        "scope.FNAObject")).substring(0,
                        (attrs.val).replace(/FNAObject/g,
                            "scope.FNAObject")
                        .indexOf('.value'));


                    if (eval(newModel) == undefined) {
                        eval(newModel + "={}");

                        eval(newModel + ".name =''+" +
                            angular.toJson(attrs.id));
                        eval(newModel + ".unit =''+" +
                            angular.toJson(attrs.defaultunit));
                        if ((attrs.defaultvaluemale && attrs.defaultvaluemale != "") || (attrs.defaultvaluefemale && attrs.defaultvaluefemale != ""))
                            if (scope.FNAMyself.BasicDetails && scope.FNAMyself.BasicDetails.gender) {
                                //FNA changes by LE Team
                                var gender = translateMessages($translate, "general.partyGenderOptionFeMale");
                                //var gender = 'Female';
                                if (scope.FNAMyself.BasicDetails.gender == gender) {
                                    attrs.defaultvalue = attrs.defaultvaluefemale;
                                } else {
                                    attrs.defaultvalue = attrs.defaultvaluemale;
                                }
                            }
                        // FNA Changes by LE Team Start
                        if (isNaN(attrs.defaultvalue))
                            attrs.defaultvalue = eval("scope." + attrs.defaultvalue);
                        // FNA Changes by LE Team End
                        if (eval(newModel + ".unit") != "NA") {
                            eval(newModel + ".textBoxValue =Number(" +
                                attrs.defaultvalue +
                                ")/Number(scope.unitList[" +
                                newModel + ".unit])");
                        } else {
                            eval(newModel + ".textBoxValue =Number(" +
                                attrs.defaultvalue + ")");
                        }

                        eval((attrs.val).replace(/FNAObject/g,
                                "scope.FNAObject") +
                            "=" + attrs.defaultvalue);

                    }

                    if (isNaN(attrs.min))
                        attrs.min = eval("scope." + attrs.min);

                    $('#' + attrs.id)
                        .slider({
                            orientation: "horizontal",
                            range: "min",
                            value: eval((attrs.val)
                                .replace(/FNAObject/g,
                                    "scope.FNAObject")),
                            min: Number(attrs.min),
                            step: Number(attrs.step),
                            max: Number(attrs.max),
                            noDivider: true,
                            hasValueBox: false,
                            change: function (event, ui) {
                                UtilityService.disableProgressTab = true;
                                //ui.value.
                                //var sliderVal = parseInt(
                                //ui.value, 10);
                                var sliderVal = ui.value;
                                var model = (attrs.val)
                                    .replace(
                                        /FNAObject/g,
                                        "scope.FNAObject");
                                eval(model + "=" +
                                    sliderVal);
                                scope.slideValue = sliderVal;
                                newModel = ((attrs.val)
                                        .replace(
                                            /FNAObject/g,
                                            "scope.FNAObject"))
                                    .substring(
                                        0,
                                        (attrs.val)
                                        .replace(
                                            /FNAObject/g,
                                            "scope.FNAObject")
                                        .indexOf(
                                            '.parameters'));
                                delete eval(newModel).result;
                                newModel = ((attrs.val)
                                        .replace(
                                            /FNAObject/g,
                                            "scope.FNAObject"))
                                    .substring(
                                        0,
                                        (attrs.val)
                                        .replace(
                                            /FNAObject/g,
                                            "scope.FNAObject")
                                        .indexOf(
                                            '.value'));
                                if (eval(newModel + ".unit") != "NA") {
                                    if (!isNaN(parseInt(sliderVal))) {


                                        var txtVal = Number(sliderVal) / Number(scope.unitList[eval(newModel + ".unit")]);
                                        if (typeof txtVal == "string")
                                            txtVal = parseFloat(txtVal);
                                        else
                                            txtVal = txtVal;


                                        eval(newModel + ".textBoxValue =  Math.round(txtVal)");

                                        /*eval(newModel
                                        		+ ".textBoxValue =Number("
                                        		+ sliderVal
                                        		+ ")/Number(scope.unitList["
                                        		+ newModel
                                        		+ ".unit])");
                                        */


                                        /*if(sliderVal > attrs.min && sliderVal < attrs.max)
                                        	eval(newModel+ ".textBoxValue =Math.round(Number("+ sliderVal+ ")/Number(scope.unitList["+ newModel+ ".unit]))");
													
                                        else if(Number(eval(newModel+ ".textBoxValue"))>=attrs.min && Number(eval(newModel+ ".textBoxValue"))<= attrs.max){
                                        	if(eval(newModel+ ".textBoxValue") != ""){
                                        		eval(newModel
                                        				+ ".textBoxValue =Number("
                                        				+ sliderVal
                                        				+ ")/Number(scope.unitList["
                                        				+ newModel
                                        				+ ".unit])");
                                        	}
                                        	
                                        	
                                        }*/
                                    }
                                } else {
                                    if (!isNaN(parseInt(sliderVal))) {

                                        eval(newModel +
                                            ".textBoxValue =Number(" +
                                            sliderVal +
                                            ")");

                                        /*if(sliderVal > attrs.min && sliderVal < attrs.max)
                                        	eval(newModel
                                        			+ ".textBoxValue =Math.round(Number("
                                        			+ sliderVal
                                        			+ "))");
                                        else if(Number(eval(newModel+ ".textBoxValue"))>=attrs.min && Number(eval(newModel+ ".textBoxValue"))<= attrs.max){
                                        	if(eval(newModel+ ".textBoxValue") != ""){
                                        		eval(newModel
                                        				+ ".textBoxValue =Number("
                                        				+ sliderVal
                                        				+ ")");
                                        	}
                                        }*/
                                    }
                                }
                                scope.goalruleResults = [];
                                scope.showCalculations = false;
                                scope
                                    .calculatePendingGoals();

                                scope.refresh();

                            }
                        });
                    scope.$on('$destroy', function () {
                        element.remove();
                    });
                }
            };
        });

gliLifeEngagedirectives
    .directive(
        'gliNumericValidation',
        function () {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, elem, attr, ngModel) {
                    var numericReqExp = "";
                    var min = attr.min;
                    if (isNaN(min))
                        min = eval("scope." + min);
                    var max = attr.max;
                    var unit = attr.unit;
                    var unitModel = ((unit).replace(/FNAObject/g,
                        "scope.FNAObject"));

                    if (eval(unitModel) == "NA") {
                        numericReqExp = new RegExp(/^[0-9]*$/);
                        //numericReqExp = "^[0-9]*$";
                    } else {
                        //numericReqExp = new RegExp(/^([0-9]{1,16})(\.\d{1,4})?$/);//correct

                        var additional = 3;

                        if (eval(unitModel) == "K")
                            additional = parseInt(attr.maxlength) + 3;
                        else if (eval(unitModel) == "M")
                            additional = parseInt(attr.maxlength) + 6;
                        else if (eval(unitModel) == "B")
                            additional = parseInt(attr.maxlength) + 9;
                        //numericReqExp = "^([0-9]{1," + additional +"})(\.\d{1,4})?$";

                        numericReqExp = "^([0-9]{1," + additional + "})(\\.\\d{1,4})?$";
                        numericReqExp = new RegExp(numericReqExp);

                    }




                    //numericReqExp = new RegExp(numericReqExp);
                    var validate = function (inputText) {
                        var valid = true;
                        if (!isNaN(inputText)) {
                            inputText = parseInt(inputText);
                            var valid = numericReqExp.test(inputText);
                            if (min &&
                                ((inputText < parseInt(min) || (eval(unitModel) == "NA") &&
                                    inputText < 0))) {
                                valid = false;
                            }
                            if (max && (inputText > parseInt(max))) {
                                valid = false;
                            }
                        } else {
                            valid = false;
                        }
                        ngModel.$setValidity('gliNumericValidation',
                            valid);
                        scope.refresh();
                        setTimeout(function () {
                            scope.updateErrorCount(scope.viewName);
                        }, 0);

                        return valid ? inputText : undefined;
                    };
                    scope
                        .$watch(
                            attr.ngModel,
                            function (inputText) {
                                var unitModel = ((unit)
                                    .replace(/FNAObject/g,
                                        "scope.FNAObject"));
                                if (eval(unitModel) != "NA" &&
                                    !isNaN(parseInt(inputText))) {
                                    inputText = Number(inputText) *
                                        Number(scope.unitList[eval(unitModel)]);
                                }
                                if (inputText != undefined) {
                                    validate(inputText);
                                }

                            });

                    scope.$on('$destroy', function () {
                        elem.remove();
                    });
                }
            };
        });
/* STAR RATING */

gliLifeEngagedirectives.directive('gliDate', function () {
    return {
        template: '<span>{{gliFormatedDate}}</span>',
        scope: {
            gliDate: '=value',
            gliDateFormat: '=dateformat'
        },
        controller: ['$scope', '$filter', function ($scope, $filter) {
            if ($scope.gliDate && $scope.gliDate.trim()) {
                var d;
                var t = $scope.gliDate.split(/[- :]/);
                if (t.length == 3) {
                    var d = new Date(t[0], t[1] - 1, t[2]); // yyyy-MM-dd
                } else if (t.length == 5) {
                    var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4]); //yyyy-MM-dd HH:mm
                } else if (t.length == 6) {
                    var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]); //yyyy-MM-dd HH:mm:ss
                }
                $scope.gliFormatedDate = $filter('date')(new Date(d), $scope.gliDateFormat);
                $scope.refresh();
            }


	       }]

    };
});

gliLifeEngagedirectives.directive('gliDateCalc', function () {
   return {
        template: '<span>{{gliFormatedDate}}</span>',
        scope: {
            gliDateCalc: '=value',
            gliDateFormat: '=dateformat'
        },
        controller: ['$scope', '$filter', function ($scope, $filter) {
            if ($scope.gliDateCalc && $scope.gliDateCalc.trim()) {
                var d;
                var t = $scope.gliDateCalc.split(/[- :]/);
                var dateVal = 543;
                if (t.length == 3) {
                    var d = new Date(parseInt(t[0]) + dateVal, t[1] - 1, t[2]); // yyyy-MM-dd
                } else if (t.length == 5) {
                    var d = new Date(parseInt(t[0]) + dateVal, t[1] - 1, t[2], t[3], t[4]); //yyyy-MM-dd HH:mm
                } else if (t.length == 6) {
                    var d = new Date(parseInt(t[0]) + dateVal, t[1] - 1, t[2], t[3], t[4], t[5]); //yyyy-MM-dd HH:mm:ss
                }
                $scope.gliFormatedDate = $filter('date')(new Date(d), $scope.gliDateFormat);
                $scope.refresh();
            }


           }]

    };
});

/* STAR RATING */

gliLifeEngagedirectives.directive('gliDateEapp', function () {
    return {
        template: '<span>{{gliFormatedDate}}</span>',
        scope: {
            gliDate: '=value',
            gliDateFormat: '=dateformat'
        },
        controller: ['$scope', '$filter', function ($scope, $filter) {
            if ($scope.gliDate && $scope.gliDate.trim()) {
                var d;
                var t = $scope.gliDate.split(/[- :]/);

                var d = new Date(t[0], t[1] - 1, t[2]); // yyyy-MM-dd

                $scope.gliFormatedDate = $filter('date')(new Date(d), "dd-MM-yyyy");
                $scope.refresh();
            }


	       }]

    };
});

//directive for adding vertical carousal in fna
gliLifeEngagedirectives.directive(
    'gliVerticalCarousal',
    function () {
        return {
            restrict: 'A',
            link: function (scope, elem, attr, ngModel) {
                var index = scope.$eval(attr.indexAttr);
                if (scope.$last) {
                    $(elem).parent().bxSlider({
                        mode: 'vertical',
                        slideWidth: 260,
                        minSlides: 3,
                        slideMargin: 10,
                        infiniteLoop: false,
                        preventDefaultSwipeY: true,
                        pager: false,
                        hideControlOnEnd: true
                    });
                }

                var count = scope.$eval(attr.count);
                var currentPage = scope.$root.selectedPage;
                $(elem).children('.individual_option').click(function (e) {
                    $(this).parent().parent().find('.selected').removeClass('selected');
                    $(this).addClass('selected');
                    var myClass;
                    if (currentPage == 'ProductListing') {
                        myClass = '.product-rec-container .graph_wrapper';
                    } else {
                        myClass = '.cal-chart-container .graph_wrapper';
                    }

                    if ((e.pageY < 350) && (e.pageY > 250)) {
                        $(myClass).removeClass('position1 position2 position3').addClass('position' + 1);
                    } else if ((e.pageY < 480) && (e.pageY > 380)) {
                        $(myClass).removeClass('position1 position2 position3').addClass('position' + 2);
                    } else if ((e.pageY < 600) && (e.pageY > 500)) {
                        $(myClass).removeClass('position1 position2 position3').addClass('position' + 3);
                    }

                });
            }
        };
    });


lifeEngagedirectives
    .directive(
        'dateInput',
        function (dateFilter, $filter) {
            return {
                require: 'ngModel',

                replace: true,
                link: function (scope, elm, attrs, ngModelCtrl) {
                    var flagChange = $(elm).attr('futuredateflag');
                    if (!((rootConfig.isDeviceMobile)) || ((rootConfig.isOfflineDesktop))) {
                        /* var elmType = '#' + elm[0].id;
                        $(elmType).attr('type', 'text'); */
                        elm.attr('type', 'text');
                        $('.custdatepicker').datepicker({
                             format: 'dd/mm/yyyy',
                            autoclose: true
                        });

                        $('.custdatepicker')
                            .on(
                                'changeDate',
                                function (ev) {
                                    var id_elmnt = $(this)
                                        .attr('id');
                                    var id_elmnt_Model = $(this)
                                        .attr('ng-model');
                                    var selected_date = $(
                                            '#' + id_elmnt)
                                        .val();

                                    var modelObject = id_elmnt_Model
                                        .substring(
                                            0,
                                            id_elmnt_Model
                                            .lastIndexOf('.'));
                                    var modelVariable = id_elmnt_Model
                                        .substring(id_elmnt_Model
                                            .lastIndexOf('.') + 1);
                                    if (modelObject.length > 0) {
                                        modelObject = "." +
                                            modelObject;
                                    }
                                    eval('scope' + modelObject)[modelVariable] = selected_date;
                                    scope.refresh();
                                });
                    } else {
                        if (flagChange) {
                            var today = new Date().toISOString().split('T')[0];
                            document.getElementById(elm[0].id).setAttribute('max', today);
                        }
                    }
                    /**
                     * Commented due to problem caused in safari problem
                     * is model is not getting due to this*
                     */

                    scope.$on('$destroy', function () {
                        elm.remove();
                    });
                }
            };
        });


gliLifeEngagedirectives
    .directive(
        'gliDateInput',
        function (dateFilter, $filter, $parse, $translate) {
            return {
                require: 'ngModel',
                replace: true,
                link: function (scope, elm, attrs, ngModelCtrl) {
                    var flagChange = $(elm).attr('futuredateflag');
                    var ngModelVal = $parse(attrs.ngModel);
                    var id_elmnt = attrs['id'];
                    var customBlur;
                    var languageCheck = '';
                    if (attrs.customBlur) {
                        customBlur = attrs.customBlur;
                    }
                   // if (!((rootConfig.isDeviceMobile)) ||  ((rootConfig.isOfflineDesktop))) {
                        elm.attr('type', 'text');

                        if ($translate.use() == 'vt_VT') {
                            languageCheck = 'vi';
                        } else {
                            languageCheck = 'en';
                        }
                        $('.custdatepicker').datepicker({
                            format: 'dd/mm/yyyy',
                            autoclose: true,
                            language: languageCheck
                        });

                        $('.custdatepicker')
                            .on(
                                'changeDate',
                                function (ev) {
                                    var id_elmnt_Model = attrs['ngModel'];
                                    var selected_date = $(
                                            '#' + id_elmnt)
                                        .val();

                                    var modelObject = id_elmnt_Model
                                        .substring(
                                            0,
                                            id_elmnt_Model
                                            .lastIndexOf('.'));
                                    var modelVariable = id_elmnt_Model
                                        .substring(id_elmnt_Model
                                            .lastIndexOf('.') + 1);
                                    if (modelObject.length > 0) {
                                        modelObject = "." +
                                            modelObject;
                                    }
                                    if (flagChange) {
                                        if (new Date(
                                                selected_date) > new Date()) {
                                            selected_date = "";
                                            $('#' + id_elmnt)
                                                .val('');
                                        }
                                    }
                                    eval('scope' + modelObject)[modelVariable] = selected_date;

                                    /*if (attrs.customBlur) {
                                    	eval('scope.'
                                    			+ customBlur);
                                    }*/
                                    scope.refresh();
                                });
                    //} else {
                        /* if(flagChange){
                                var today = new Date().toISOString().split('T')[0];
                                document.getElementById(elm[0].id).setAttribute('max', today);
                        } */
                   // }
                    /**
                     * Commented due to problem caused in safari problem
                     * is model is not getting due to this*
                     */
                    if (ngModelVal) {
                        ngModelCtrl.$formatters.push(function (val) {
                            var returnValue = new Date(val);

                            if (flagChange) {
                                if (returnValue > new Date()) {
                                    returnValue = "";
                                }
                            }
                            return returnValue;
                        });
                        ngModelCtrl.$parsers.push(function (val) {
                            if (attrs.customBlur) {
                                setTimeout(function () {
                                    eval('scope.' +
                                        customBlur);
                                    scope.refresh();
                                }, 0);
                            }
                            if (val != null &&
                                typeof val != "undefined") {
                                if (flagChange) {
                                    if (val > new Date()) {
                                        $('#' + id_elmnt).val('');
                                        return "";
                                    } else {
                                        var now, month, day;
                                        now = LEDate(val);
                                        year = "" + now.getFullYear();
                                        month = "" +
                                            (now.getMonth() + 1);
                                        if (month.length == 1) {
                                            month = "0" + month;
                                        }
                                        day = "" + now.getDate();
                                        if (day.length == 1) {
                                            day = "0" + day;
                                        }
                                        return year + "-" + month + "-" +
                                            day;
                                    }
                                } else {
                                    var now, month, day;
                                    now = LEDate(val);
                                    year = "" + now.getFullYear();
                                    month = "" + (now.getMonth() + 1);
                                    if (month.length == 1) {
                                        month = "0" + month;
                                    }
                                    day = "" + now.getDate();
                                    if (day.length == 1) {
                                        day = "0" + day;
                                    }
                                    return year + "-" + month + "-" +
                                        day;
                                }
                            } else {
                                return "";
                            }

                        });
                    }
                    scope.$on('$destroy', function () {
                        elm.remove();
                    });
                }
            };
        });

gliLifeEngagedirectives
    .directive(
        'gliDateInputCustom',
        function (dateFilter, $filter, $parse, $translate) {
            return {
                require: 'ngModel',
                replace: true,
                link: function (scope, elm, attrs, ngModelCtrl) {
                    var flagChange = $(elm).attr('futuredateflag');
                    var ngModelVal = $parse(attrs.ngModel);
                    var id_elmnt = attrs['id'];
                    var customBlur;
                    var languageCheck = '';
                    if (attrs.customBlur) {
                        customBlur = attrs.customBlur;
                    }
                   /* if (!((rootConfig.isDeviceMobile)) ||
                        ((rootConfig.isOfflineDesktop))) {
                   */
                        elm.attr('type', 'text');

                        /*if ($translate.use() == 'th_TH') {
                            languageCheck = 'th-th';
                        } else {
                            languageCheck = 'en';
                        }*/
                        languageCheck = 'th-th';
                        $('.custdatepicker').datepicker({
                            format: 'dd/mm/yyyy',
                            autoclose: true,
                            language: languageCheck
                        });

                        $('.custdatepicker')
                            .on(
                                'changeDate',
                                function (ev) {
                                    var id_elmnt_Model = attrs['ngModel'];
                                    var selected_date = $(
                                            '#' + id_elmnt)
                                        .val();

                                    var modelObject = id_elmnt_Model
                                        .substring(
                                            0,
                                            id_elmnt_Model
                                            .lastIndexOf('.'));
                                    var modelVariable = id_elmnt_Model
                                        .substring(id_elmnt_Model
                                            .lastIndexOf('.') + 1);
                                    if (modelObject.length > 0) {
                                        modelObject = "." +
                                            modelObject;
                                    }
                                    if (flagChange) {
										var splitDate = [];
										splitDate = selected_date.split("/");
										if(languageCheck=='th-th'){
											splitDate[2] = splitDate[2] - rootConfig.thaiYearDifference;
										}
										var desiredDateFmt = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];
                                        if (new Date(desiredDateFmt) > new Date()) {
                                            selected_date = "";
                                            $('#' + id_elmnt).val('');
                                        } else {
											/* splitDate = selected_date.split("-");
											if(languageCheck=='th-th'){
												splitDate[0] = Number(splitDate[0]) + Number(rootConfig.thaiYearDifference);
												splitDate[0] = splitDate[0].toString();
											}
											selected_date = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0]; */
										}
                                    }
                                   eval('scope' + modelObject)[modelVariable] = selected_date;

                                    if (attrs.customBlur) {
                                    	eval('scope.' + customBlur);
                                    }
                                    //scope.refresh();
                                });
                    //} else {
                        /* if(flagChange){
                                var today = new Date().toISOString().split('T')[0];
                                document.getElementById(elm[0].id).setAttribute('max', today);
                        } */
                   // }
                    /**
                     * Commented due to problem caused in safari problem
                     * is model is not getting due to this*
                     */
                    if (ngModelVal) {
                        ngModelCtrl.$formatters.push(function (val) {
                            var returnValue = new Date();
                            if (val == "") {
                                returnValue = "";
                            } else {
								var dateformatted=getFormattedDateYYYYMMDD(val);
                                returnValue = new Date(dateformatted);

                                if (returnValue == "Invalid Date" || val.length < 10) {
                                    returnValue = val;
                                }
                            }
                            if (flagChange) {
                                if (returnValue > new Date()) {
                                    //returnValue = "";
                                }
                            }
                            return returnValue;
                        });
                        ngModelCtrl.$parsers.push(function (val) {
                           
                          
                            /* if (attrs.customBlur) {
                                setTimeout(function () {
                                    eval('scope.' +
                                        customBlur);
                                    scope.refresh();
                                }, 0);
                            } */
                            if (val != null &&
                                typeof val != "undefined") {
                                if (flagChange) {
                                    if (val > new Date()) {
                                        $('#' + id_elmnt).val('');
                                        return "";
                                    } else {
                                        var now, month, day;
                                        now = LEDate(val);
                                        year = "" + now.getFullYear();
                                        month = "" +
                                            (now.getMonth() + 1);
                                        if (month.length == 1) {
                                            month = "0" + month;
                                        }
                                        day = "" + now.getDate();
                                        if (day.length == 1) {
                                            day = "0" + day;
                                        }
                                        return day + "/" + month + "/" +
                                            year;
                                    }
                                } else {
                                    var now, month, day;
                                    now = LEDate(val);
                                    year = "" + now.getFullYear();
                                    month = "" + (now.getMonth() + 1);
                                    if (month.length == 1) {
                                        month = "0" + month;
                                    }
                                    day = "" + now.getDate();
                                    if (day.length == 1) {
                                        day = "0" + day;
                                    }
                                    return day + "/" + month + "/" +
                                        year;
                                }
                            } else {
                                return "";
                            }

                        

                        });
                    }
                    scope.$on('$destroy', function () {
                        elm.remove();
                    });
                }
            };
        });

gliLifeEngagedirectives.directive('thaiDate',['dateFilter', '$filter', '$parse','UtilityService','$translate',function(dateFilter, $filter, $parse,UtilityService,$translate) {
	return {
		require : 'ngModel',
		replace : true,
		link : function(scope, elm, attrs, ngModelCtrl) {
			var flagChange = $(elm).attr('futuredateflag');
			var futureDate = $(elm).attr('enableFutureDate')
			var ngModelVal = $parse(attrs.ngModel);
			var id_elmnt = attrs['id'];
			var elmType = '#' + elm[0].id;
			var customBlur;
			if (attrs.customBlur) {
				customBlur = attrs.customBlur;
			}
			var localeLang = 'th-th';
			
			$(elm).datepicker({
                    format : 'dd/mm/yyyy',
                    autoclose : true,
                    language: localeLang
            });
			
			$('.custdatepicker').on('changeDate blur',function (ev) { 
				if (attrs.customBlur) {
					eval('scope.' + customBlur);
				}
				scope.refresh();
			});
			
			ngModelCtrl.$formatters.push(function(val) {
				if (angular.isUndefined(val) || val == '') {
					return "";
				} else {
					var splitDate = [];
					var stdYear="";
					if(val.indexOf('-')>-1){
						splitDate = val.split("-");
						if(localeLang=='th-th'){
							stdYear=splitDate[0];
							splitDate[0] = parseInt(splitDate[0]) + rootConfig.thaiYearDifference;
						}
						var desiredDateFmt = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
						var desiredDateFmtStd = splitDate[2] + "/" + splitDate[1] + "/" + stdYear;
						var desiredDateFmtStdMMDDYY = splitDate[1] + "/" + splitDate[2] + "/" + stdYear; //MMDDYY format
						if(flagChange){
							if(new Date(desiredDateFmtStdMMDDYY) > new Date()){
								desiredDateFmt='';
							}
						}
						return desiredDateFmt;
					}else{
						return val;
					}
					
				}
			});
			ngModelCtrl.$parsers.push(function(val) {
				if ((ngModelCtrl.$invalid && angular.isUndefined(val)) || val === '') {
					return null;
				} else {
					var splitDate = [];
					splitDate = val.split("/");
					if(localeLang=='th-th'){
						splitDate[2] = parseInt(splitDate[2]) - rootConfig.thaiYearDifference;
					}
					if(splitDate[2].toString()=="NaN") {
					return null;
					}else {
					var desiredDateFmt = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];}
					
					if(flagChange){
						if(new Date(desiredDateFmt) > new Date()){
							desiredDateFmt='';
							$('#' + id_elmnt).val('');
						}
					}
					return desiredDateFmt;
				}
			}); 
			scope.$on('$destroy', function() {
				elm.remove();
			});
		}
	};
}]);
	
	
		
gliLifeEngagedirectives.directive('positionModal', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.on("click", function (e) {
                //var topVal=e.screenY-$('.vitalModal').height();;
                //var leftVal=e.screenX+27;
                var topVal = e.pageY - 291;
                var leftVal = e.pageX - 32; 
                var zIndex = '9999';
                $('#riderModal').css('top', topVal);
                $('#riderModal').css('left', leftVal);
                $('#riderModal').css('z-index', zIndex);     
                $('#riderModal').css('font-size', '19px');     
                $('#riderModal').css('font-family', 'DBHelvethaicaRegular');     
            });
        }

    };
});


gliLifeEngagedirectives.directive('disabledCurrency', function ($filter, $locale, $timeout) {
        var formatter = function (num) {
			var str="";
			if(parseInt(num)>=0 && num!=="")
			{
			  if(parseInt(num)>=0)
				{
				  num = parseInt(num) || 0;
				  return $filter('number')(parseInt(num));
				}
				else
				{
					return str;
				}
			}
			else
			{
				return str;
			}
        };

        var unformatter = function (str) {
          return parseInt(str.replace(/[^0-9\.]/g, ''));
        };

        return {
          restrict: 'A',
          require: 'ngModel',
          link: function (scope, element, attr, ngModel) {
            ngModel.$formatters.unshift(formatter);
            ngModel.$parsers.unshift(unformatter);

            element.bind('blur', function () {
				if(parseInt(ngModel.$modelValue)>=0)
				{
				  element.val(formatter(ngModel.$modelValue))
				}
				else
				{
					element.val('');
                    ngModel.$setViewValue('');
				}
            });

            element.bind('focus', function () {
                if(isNaN(ngModel.$modelValue)){
                    ngModel.$modelValue="";
                }
                element.val(ngModel.$modelValue);                
            });
          }
        };
});



gliLifeEngagedirectives.directive('eappCurrency', function ($filter, $locale, $timeout) {
		$locale.NUMBER_FORMATS.CURRENCY_SYM="";
		var decimalSep = $locale.NUMBER_FORMATS.DECIMAL_SEP;
		var toNumberRegex = new RegExp('[^0-9\\' + decimalSep + ']', 'g');
		var trailingZerosRegex = new RegExp('\\' + decimalSep + '0+$');
		var maxlengthModified = false;
		var initialMaxlength = 0;
		var filterFunc = function (value) {
		if(value==0)
        {
            return (''+value).replace(/(.)(?=(.{3})+$)/g, "$1,");
        }
        else
        {
		return value ? (''+value).replace(/(.)(?=(.{3})+$)/g, "$1,"):'';
        }
	}
	function toNumber(currencyStr) {
        return  currencyStr && isNaN(parseFloat(currencyStr.replace(toNumberRegex, ''))) ? '' :parseFloat(currencyStr.replace(toNumberRegex, ''));
    }

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function postLink(scope, elem, attrs, modelCtrl) {					
            modelCtrl.$formatters.push(filterFunc);
            modelCtrl.$parsers.push(function (newViewValue) {
                var oldModelValue = modelCtrl.$modelValue;
                var newModelValue = toNumber(newViewValue);
                modelCtrl.$viewValue = filterFunc(newModelValue);			    
				var commaSeparatedString = (modelCtrl.$viewValue).toString();
				var commaSeparated  = commaSeparatedString.split(',');
				var decimalSeparated =    commaSeparatedString.split('.');
				if(elem.attr('maxlength') && elem.attr('maxlength') > 0){
					var modifiedMaxLength = 0;				
					if(!maxlengthModified){						
						initialMaxlength = parseInt(elem.attr('maxlength'));
					}
					maxlengthModified = true;
					if(commaSeparated.length>1){
					    modifiedMaxLength = initialMaxlength+(commaSeparated.length-1);
				    }
					if(decimalSeparated.length>1){
						if(parseInt(modifiedMaxLength)>0)
							modifiedMaxLength = parseInt(modifiedMaxLength)+1;
						else
							modifiedMaxLength = parseInt(initialMaxlength)+1;
					}
					if(modifiedMaxLength==0){
						modifiedMaxLength = initialMaxlength;
					}
					elem.attr('maxlength',modifiedMaxLength);
						
				}							
				elem.val(modelCtrl.$viewValue);
                return newModelValue;
            });
            elem.on('keydown', function(){
                $timeout(function() {
                    elem[0].selectionStart = elem[0].selectionEnd = 10000;
                },0);

            });

        }
    };
});

gliLifeEngagedirectives.directive('customGraph', function () {
    return {
		restrict: 'E',
        templateUrl: 'modules/illustrator/partials/graph.html',
    };
});
gliLifeEngagedirectives.directive('customcompleteGraph', function () {
    return {
		restrict: 'E',
        templateUrl: 'modules/illustrator/partials/genCompleteHealth.html',
    };
});
gliLifeEngagedirectives.directive('customcompleteCancer', function () {
    return {
		restrict: 'E',
        templateUrl: 'modules/illustrator/partials/genCancerSuperProtection.html',
    };
});
gliLifeEngagedirectives.directive('customGenprolife25', function () {
    return {
		restrict: 'E',
        templateUrl: 'modules/illustrator/partials/GenProLife25.html',
    };
});
gliLifeEngagedirectives.directive('customGenprolife20', function () {
    return {
		restrict: 'E',
        templateUrl: 'modules/illustrator/partials/GenProLife20.html',
    };
});
gliLifeEngagedirectives.directive('customGensaveplus20', function () {
    return {
		restrict: 'E',
        templateUrl: 'modules/illustrator/partials/genSave20Plus.html',
    };
});
gliLifeEngagedirectives.directive('customGenprotection5', function () {
    return {
		restrict: 'E',
        templateUrl: 'modules/illustrator/partials/genProtection5.html',
    };
});
gliLifeEngagedirectives.directive('customGensaveplus10', function () {
    return {
		restrict: 'E',
        templateUrl: 'modules/illustrator/partials/genSave10Plus.html',
    };
});
gliLifeEngagedirectives.directive('customGensaveplus04', function () {
    return {
		restrict: 'E',
        templateUrl: 'modules/illustrator/partials/genSave04Plus.html',
    };
});
gliLifeEngagedirectives.directive('customStaticaccordion', function () {
    return {
		restrict: 'E',
        templateUrl: 'modules/illustrator/partials/customTabStaticContent.html',
    };
});
gliLifeEngagedirectives.directive('customTabNavigation', function () {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
                                                     
                                             var tabId= attrs.navigationtabid;                                                                                                     
                                                $(el).owlCarousel({
                                                                navigation : false                                                                
                                                });
                                                
                                                $("#next-slide_" + tabId).click(function() {
                                                    
                                                                $(el).trigger('owl.next');
                                                });
                                                $("#prev-slide_" + tabId).click(function() {
                                                    
                                                                $(el).trigger('owl.prev');
                                                });
        }
    }
});

/* FNA changes by LE team starts */
gliLifeEngagedirectives.directive(
				'showDelete',
				function() {
					return {
						link : function(scope, element, attrs) {
							function findOffsetForScrap(thisObj, x, y) {
								var cordinateVal = $(thisObj).offset();
								var topVal = cordinateVal.top + x + "px";
								var leftVal = cordinateVal.left + y + "px";
								return [ topVal, leftVal ];
							};
							$(element)
									.click(
											function(event) {
												scope.$parent.thisObjDeletion = $(element);
												scope.$parent.benef_id = $(
														element).parent().attr(
														"id");
												scope.$parent.currentlyRemovedId = $(
														element).children()
														.attr("id");
												var childrenLength = $(element)
														.children().length;
												var individualOffset = findOffsetForScrap(
														scope.$parent.thisObjDeletion,
														48, 24);
												var topVal = individualOffset[0];
												var leftval = individualOffset[1];
												var chk = $('.cp_scrapButton')
														.css("display");
                                                /* FNA changes by LE Team start */
												if (chk == "none" || chk=="block"
														&& childrenLength > 0) {
													$('.cp_scrapButton')
															.fadeIn();
													$('.cp_scrapButton').css({
														"display" : "block",
														"top" : topVal,
														"left" : leftval
													}); /* FNA changes by LE Team end */
												}else{
													$('.cp_scrapButton').hide();
												}
											});
							 scope.$on('$destroy', function() {
				        		 element.remove();
								 });
						}
					};
				});
/* FNA changes by LE team ends */