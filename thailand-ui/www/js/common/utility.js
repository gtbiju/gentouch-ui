/*
 *Copyright 2015, LifeEngage 
 */


function Request() {
	var updatedDate = new Date();
	var formattedDate = getFormattedDate();
	var createdDate = getFormattedDate();
	return {
		"Request" : {
			"RequestInfo" : {
				"UserName" : "uN@me",
				"CreationDate" : createdDate,
				"CreationTime" : "12:15:54-04:00",
				"SourceInfoName" : "tablet",
				"TransactionId" : "9e59098a45f549e894",
				"RequestorToken" : "",
				"LastSyncDate" : "",
				"FirstTimeSync" : "true"
			},
			"RequestPayload" : {
				"Transactions" : []
			}
		}
	};
}
function getRequest(){
	var req = Request();
	if(!rootConfig.isDeviceMobile && ! rootConfig.isOfflineDesktop){
		req.Request.RequestInfo.SourceInfoName = "web";
	}else if(rootConfig.isDeviceMobile && rootConfig.isOfflineDesktop){
		req.Request.RequestInfo.SourceInfoName = "web-kit";		
	}else if(rootConfig.isDeviceMobile && !rootConfig.isOfflineDesktop){
		if(navigator.userAgent.indexOf("Android") > 0){
			req.Request.RequestInfo.SourceInfoName = "Android";	
		}else if(navigator.userAgent.indexOf("iPhone") > 0
				|| navigator.userAgent.indexOf("iPad") > 0
				|| navigator.userAgent.indexOf("iPod") > 0){
					req.Request.RequestInfo.SourceInfoName = "IOS";
		}
	}
	return req;
}
function Hierarchy() {
	
	return {
	
	"Response": {
		"ResponseInfo": {
			"UserName": "uN@me",
			"CreationDate": "2016-10-20 09:38:48",
			"CreationTime": "12:15:54-04:00",
			"SourceInfoName": "tablet",
			"TransactionId": "9e59098a45f549e894",
			"RequestorToken": "",
			"LastSyncDate": "",
			"FirstTimeSync": "true"
		},
		"ResponsePayload": {
			"Transactions": [{
				"TransactionData": {
					"searchCriteriaResponse": {
						"command": "LeadManagerialLevelRequest",
						"UserDetails": [{
							"userType": "ASM",
							"userId": "BD000099",
							"MTDnew": "23",
							"MTDfollowUp": "1",
							"MTDclosed": "32",
							"MTDActivityRatio": "534",
							"MTDConversionRate": "412",
							"MTDAPE": "234",
							"MTDProductivity": "2345",
							"MTDCaseSize": "25",
							"YTDnew": "24",
							"YTDfollowUp": "345",
							"YTDclosed": "25",
							"YTDActivityRatio": "251",
							"YTDConversionRate": "122",
							"YTDAPE": "3242",
							"YTDProductivity": "1234",
							"YTDCaseSize": "2345",
							"UserDetails": [{
								"userType": "RM",
								"userId": "BI000718",
								"MTDnew": "3",
								"MTDfollowUp": "2",
								"MTDclosed": "4",
								"MTDActivityRatio": "534",
								"MTDConversionRate": "412",
								"MTDAPE": "234",
								"MTDProductivity": "2345",
								"MTDCaseSize": "25",
								"YTDnew": "24",
								"YTDfollowUp": "345",
								"YTDclosed": "25",
								"YTDActivityRatio": "251",
								"YTDConversionRate": "122",
								"YTDAPE": "3242",
								"YTDProductivity": "1234",
								"YTDCaseSize": "2345",
							"selectedAgentIds": [{
								"userId": "BI000681",
								"YTDnew": "3",
								"YTDclosed": "4",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000682",
								"YTDnew": "4",
								"YTDclosed": "23",
								"YTDfollowUp": "6",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000683",
								"YTDnew": "34",
								"YTDclosed": "2",
								"YTDfollowUp": "4",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000684",
								"YTDnew": "34",
								"YTDclosed": "2",
								"YTDfollowUp": "4",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000685",
								"YTDnew": "34",
								"YTDclosed": "2",
								"YTDfollowUp": "4",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000724",
								"YTDnew": "34",
								"YTDclosed": "2",
								"YTDfollowUp": "4",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000726",
								"YTDnew": "34",
								"YTDclosed": "2",
								"YTDfollowUp": "4",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000728",
								"YTDnew": "34",
								"YTDclosed": "2",
								"YTDfollowUp": "4",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000819",
								"YTDnew": "34",
								"YTDclosed": "2",
								"YTDfollowUp": "4",
								"YTDConversionRate": "234",
								"YTDAPE": "24"

							}],
							"selectedActiveAgentIds": ["BC0009","Bs2142","sasdfag","sgasg"]
							},
							{
								"userType": "RM",
								"userId": "BI000138",
								"MTDnew": "23",
								"MTDfollowUp": "1",
								"MTDclosed": "4",
								"MTDActivityRatio": "534",
								"MTDConversionRate": "412",
								"MTDAPE": "234",
								"MTDProductivity": "2345",
								"MTDCaseSize": "25",
								"YTDnew": "24",
								"YTDfollowUp": "345",
								"YTDclosed": "25",
								"YTDActivityRatio": "251",
								"YTDConversionRate": "122",
								"YTDAPE": "3242",
								"YTDProductivity": "1234",
								"YTDCaseSize": "2345",
								"selectedAgentIds": [{
								"userId": "BI000681",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000682",
								"YTDnew": "34",
								"YTDclosed": "2",
								"YTDfollowUp": "4",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000683",
							"YTDnew": "34",
								"YTDclosed": "2",
								"YTDfollowUp": "4",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000684",
								"YTDnew": "34",
								"YTDclosed": "2",
								"YTDfollowUp": "4",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000685",
								"YTDnew": "34",
								"YTDclosed": "2",
								"YTDfollowUp": "4",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000724",
								"YTDnew": "34",
								"YTDclosed": "2",
								"YTDfollowUp": "4",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000726",
								"YTDnew": "34",
								"YTDclosed": "2",
								"YTDfollowUp": "4",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000728",
								"YTDnew": "34",
								"YTDclosed": "2",
								"YTDfollowUp": "4",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000819",
								"YTDnew": "34",
								"YTDclosed": "2",
								"YTDfollowUp": "4",
								"YTDConversionRate": "234",
								"YTDAPE": "24"

							}],
							"selectedActiveAgentIds": ["BC0009","Bs2142","sasdfag","sgasg"]
							},
							{
								"userType": "RM",
								"userId": "BI000913",
								"MTDnew": "3423",
								"MTDfollowUp": "2345",
								"MTDclosed": "1241",
								"MTDActivityRatio": "534",
								"MTDConversionRate": "412",
								"MTDAPE": "234",
								"MTDProductivity": "2345",
								"MTDCaseSize": "25",
								"YTDnew": "24",
								"YTDfollowUp": "345",
								"YTDclosed": "25",
								"YTDActivityRatio": "251",
								"YTDConversionRate": "122",
								"YTDAPE": "3242",
								"YTDProductivity": "1234",
								"YTDCaseSize": "2345",
								"selectedAgentIds": [{
								"userId": "BI000681",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000682",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000683",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000684",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000685",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000724",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000726",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000728",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000819",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"

							}],
							"selectedActiveAgentIds": ["BC0009","Bs2142","sasdfag","sgasg"]
							},
							{
								"userType": "RM",
								"userId": "BI000626",
								"MTDnew": "3423",
								"MTDfollowUp": "2345",
								"MTDclosed": "1241",
								"MTDActivityRatio": "534",
								"MTDConversionRate": "412",
								"MTDAPE": "234",
								"MTDProductivity": "2345",
								"MTDCaseSize": "25",
								"YTDnew": "24",
								"YTDfollowUp": "345",
								"YTDclosed": "25",
								"YTDActivityRatio": "251",
								"YTDConversionRate": "122",
								"YTDAPE": "3242",
								"YTDProductivity": "1234",
								"YTDCaseSize": "2345",
								"selectedAgentIds": [{
								"userId": "BI000681",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000682",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000683",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000684",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000685",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000724",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000726",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000728",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000819",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"

							}],
							"selectedActiveAgentIds": ["BC0009","Bs2142","sasdfag","sgasg"]
								
							},
							{
								"userType": "RM",
								"userId": "BI000006",
								"MTDnew": "3423",
								"MTDfollowUp": "2345",
								"MTDclosed": "1241",
								"MTDActivityRatio": "534",
								"MTDConversionRate": "412",
								"MTDAPE": "234",
								"MTDProductivity": "2345",
								"MTDCaseSize": "25",
								"YTDnew": "24",
								"YTDfollowUp": "345",
								"YTDclosed": "25",
								"YTDActivityRatio": "251",
								"YTDConversionRate": "122",
								"YTDAPE": "3242",
								"YTDProductivity": "1234",
								"YTDCaseSize": "2345",
								"selectedAgentIds": [{
								"userId": "BI000681",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000682",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000683",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000684",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000685",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000724",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000726",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000728",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000819",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"

							}],
							"selectedActiveAgentIds": ["BC0009","Bs2142","sasdfag","sgasg"]
								
							},
							{
								"userType": "RM",
								"userId": "BI000892",
								"MTDnew": "3423",
							"MTDfollowUp": "2345",
							"MTDclosed": "1241",
							"MTDActivityRatio": "534",
							"MTDConversionRate": "412",
							"MTDAPE": "234",
							"MTDProductivity": "2345",
							"MTDCaseSize": "25",
							"YTDnew": "24",
							"YTDfollowUp": "345",
							"YTDclosed": "25",
							"YTDActivityRatio": "251",
							"YTDConversionRate": "122",
							"YTDAPE": "3242",
							"YTDProductivity": "1234",
							"YTDCaseSize": "2345",
								"selectedAgentIds": [{
								"userId": "BI000681",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000682",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000683",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000684",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000685",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000724",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000726",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000728",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"
							}, {
								"userId": "BI000819",
								"YTDnew": "234",
								"YTDclosed": "234",
								"YTDfollowUp": "24",
								"YTDConversionRate": "234",
								"YTDAPE": "24"

							}],
							"selectedActiveAgentIds": ["BC0009","Bs2142","sasdfag","sgasg"]
								
							}]
						}]
					}
				},
				"TransTrackingID": "",
				"Type": "",
				"Key1": "",
				"Key2": "",
				"Key3": "",
				"Key4": "",
				"Key5": "",
				"Key6": "",
				"Key7": "",
				"Key8": "",
				"Key9": "",
				"Key10": "",
				"Key11": "BD000099",
				"Key12": "",
				"Key14": "2016-10-20 09:38:48",
				"Key15": "",
				"Key16": "",
				"Key17": "",
				"Key18": "",
				"Key19": "",
				"Key20": "",
				"Key21": "",
				"Key22": "",
				"Key23": "",
				"Key24": "",
				"Key25": ""
			}]
		}
	}

	};
	
}
function RequestPayment() {
	var updatedDate = new Date();
	var formattedDate = getFormattedDate();
	var createdDate = getFormattedDate();
	return {
		"Request" : {
			"RequestInfo" : {
				"UserName" : "uN@me",
				"CreationDate" : createdDate,
				"CreationTime" : "12:15:54-04:00",
				"SourceInfoName" : "tablet",
				"TransactionId" : "9e59098a45f549e894",
				"RequestorToken" : "",
				"LastSyncDate" : "",
				"FirstTimeSync" : "true"
			},
			"RequestPayLoad" : {
				"Transactions" : []
			}
		}
	};
}

var _MS_PER_DAY = 1000 * 60 * 60 * 24;
function dateDiffInDays(a, b) {
	// a = new Date(a);
	// b = new Date(b);
    // Discard the time and time-zone information.
    var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
  }

function addPrefixInDate(dt){
   addedPrefix =  (((dt.toString()).length)>1?'':'0')+dt
   return addedPrefix;
}

function getFormattedDate() {
	now = new Date();
	year = "" + now.getFullYear();
	month = "" + (now.getMonth() + 1);
	if (month.length == 1) {
		month = "0" + month;
	}
	day = "" + now.getDate();
	if (day.length == 1) {
		day = "0" + day;
	}
	hour = "" + now.getHours();
	if (hour.length == 1) {
		hour = "0" + hour;
	}
	minute = "" + now.getMinutes();
	if (minute.length == 1) {
		minute = "0" + minute;
	}
	second = "" + now.getSeconds();
	if (second.length == 1) {
		second = "0" + second;
	}
	return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
}

function getFormattedDateSummary(date) {
	now = new Date(date);
	year = "" + now.getFullYear();
	month = "" + (now.getMonth() + 1);
	if (month.length == 1) {
		month = "0" + month;
	}
	day = "" + now.getDate();
	if (day.length == 1) {
		day = "0" + day;
	}
	hour = "" + now.getHours();
	if (hour.length == 1) {
		hour = "0" + hour;
	}
	minute = "" + now.getMinutes();
	if (minute.length == 1) {
		minute = "0" + minute;
	}
	second = "" + now.getSeconds();
	if (second.length == 1) {
		second = "0" + second;
	}
	return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
}

//getting mm/dd/yyyy H:M Format
function getFormattedDateDDMMYYYYhhmmss() {
	now = new Date();
	
	year = "" + now.getFullYear();
	month = "" + (now.getMonth() + 1);
	if (month.length == 1) {
		month = "0" + month;
	}
	day = "" + now.getDate();
	if (day.length == 1) {
		day = "0" + day;
	}
	hour = "" + now.getHours();
	if (hour.length == 1) {
		hour = "0" + hour;
	}
	
	minute = "" + now.getMinutes();
	if (minute.length == 1) {
		minute = "0" + minute;
	}
	second = "" + now.getSeconds();
	if (second.length == 1) {
		second = "0" + second;
	}
	return  day + "/" + month + "/ "+ year +" " + hour + ":" + minute + ":" + second 
}

function getFormattedDateDDMMYYYY(selectedDate) {
	now = new Date(selectedDate);
	
	year = "" + now.getFullYear();
	month = "" + (now.getMonth() + 1);
	if (month.length == 1) {
		month = "0" + month;
	}
	day = "" + now.getDate();
	if (day.length == 1) {
		day = "0" + day;
	}
	hour = "" + now.getHours();
	if (hour.length == 1) {
		hour = "0" + hour;
	}
	
	minute = "" + now.getMinutes();
	if (minute.length == 1) {
		minute = "0" + minute;
	}
	second = "" + now.getSeconds();
	if (second.length == 1) {
		second = "0" + second;
	}
	return  day + "/" + month + "/"+ year;
}

function getFormattedDateDDMMYYYYThai(selectedDate) {
	now = new Date(selectedDate);
	
	year = parseInt(now.getFullYear())+parseInt(rootConfig.thaiYearDifference);
	month = "" + (now.getMonth() + 1);
	if (month.length == 1) {
		month = "0" + month;
	}
	day = "" + now.getDate();
	if (day.length == 1) {
		day = "0" + day;
	}
	hour = "" + now.getHours();
	if (hour.length == 1) {
		hour = "0" + hour;
	}
	
	minute = "" + now.getMinutes();
	if (minute.length == 1) {
		minute = "0" + minute;
	}
	second = "" + now.getSeconds();
	if (second.length == 1) {
		second = "0" + second;
	}
	return  day + "/" + month + "/"+ year;
}

function getFormattedPaymentDateThai(selectedDate) {
	now = new Date(selectedDate);
	
	year = parseInt(now.getFullYear())+parseInt(rootConfig.thaiYearDifference);
	
	month = "" + (now.getMonth() + 1);
	if (month.length == 1) {
		month = "0" + month;
	}
	day = "" + now.getDate();
	if (day.length == 1) {
		day = "0" + day;
	}
	return  day + "/" + month + "/"+ year;
}

function getFormattedDateYYYYMMDDEapp(selectedDate) {
	var present = new Date(selectedDate);
	
	year = "" + present.getFullYear();
	month = "" + (present.getMonth() + 1);
	if (month.length == 1) {
		month = "0" + month;
	}
	day = "" + present.getDate();
	if (day.length == 1) {
		day = "0" + day;
	}
	hour = "" + present.getHours();
	if (hour.length == 1) {
		hour = "0" + hour;
	}
	
	minute = "" + present.getMinutes();
	if (minute.length == 1) {
		minute = "0" + minute;
	}
	second = "" + present.getSeconds();
	if (second.length == 1) {
		second = "0" + second;
	}
	return  year + "-" + month + "-"+ day; 
}

function ExternalToInternal(selectedDate) {

	//External Date format:  DD/MM/YYYY
	//Internal Date format:  YYYY-MM-DD
	var test = selectedDate.split('/');

	if(rootConfig.locale ="th_TH")
	{
		test[2] = test[2] - rootConfig.thaiYearDifference;
	}

	selectedDate = test[2] + '-' + test[1] + '-' + test[0];
	
	return  selectedDate;
}

// This method always return the value of type Date
function InternalToExternal(selectedDate) {

	//Internal Date format:  YYYY-MM-DD
	//External Date format:  DD/MM/YYYY
	var test = selectedDate.split('-');

	if(rootConfig.locale = "th_TH")
	{
		test[0] = parseInt(test[0]) + rootConfig.thaiYearDifference;
	}

	var selectedDateType = new Date(test[0],parseInt(test[1])-1,parseInt(test[2]));
	
	return selectedDateType;
}


// This method always return the value of type Date
function InternalToExternalSummary(selectedDate) {

	//Internal Date format:  YYYY-MM-DD
	//External Date format:  DD/MM/YYYY
	var test = selectedDate.split('-');

	if(rootConfig.locale = "th_TH")
	{
		test[0] = parseInt(test[0]) + rootConfig.thaiYearDifference;
	}

	var selectedDateType = test[2] + '/' + test[1] + '/' + test[0];
	
	return selectedDateType;
}


function InternalToEtr(selectedDate) {

	//Internal Date format:  YYYY-MM-DD
	//External Date format:  YYYY-MM-DD
	var test = selectedDate.split('-');

	if(rootConfig.locale = "th_TH")
	{
		test[0] = parseInt(test[0]) + rootConfig.thaiYearDifference;
	}

	
	var selectedDateType = test[0] + '-' + test[1] + '-' + test[2];
	return selectedDateType;
}

function InternalToExternalInString(selectedDate) {

	//Internal Date format:  YYYY-MM-DD
	//External Date format:  DD/MM/YYYY
	var test = selectedDate.split('-');

	if(rootConfig.locale = "th_TH")
	{
		test[0] = parseInt(test[0]) + rootConfig.thaiYearDifference;
	}

	var selectedDateType = test[0]+'-'+test[1]+'-'+test[2];
	
	return selectedDateType;
}

function InternalToPaymentString(selectedDate) {

	//Internal Date format:  MM-DD-YYYY
	//External Date format:  DD/MM/YYYY
	var test = selectedDate.split('-');

	if(rootConfig.locale = "th_TH")
	{
		test[0] = parseInt(test[0]) + rootConfig.thaiYearDifference;
	}

    if (test[1].length == 1) {
        test[1] = "0" + test[1];
    }
   
    if (test[2].length == 1) {
        test[2] = "0" + test[2];
    }
    
    
	var selectedDateType = test[2]+'/'+test[1]+'/'+test[0];
	
	return selectedDateType;
}
function getFormattedDateThai(selectedDate) {
	var test = selectedDate.split('/');
	test[2] = test[2] - rootConfig.thaiYearDifference;
	selectedDate = test[2] + '-' + test[1] + '-' + test[0];
	
	now = new Date(selectedDate);
	
	year = "" + now.getFullYear();
	month = "" + (now.getMonth() + 1);
	if (month.length == 1) {
		month = "0" + month;
	}
	day = "" + now.getDate();
	if (day.length == 1) {
		day = "0" + day;
	}
	return  year + "-" + month + "-"+ day;
}

function summaryGetFormattedDateThai(selectedDate) {
	var test = selectedDate.split('/');
	test[0] = test[0] - rootConfig.thaiYearDifference;
	selectedDate = test[0] + '-' + test[1] + '-' + test[2];
	
	now = new Date(selectedDate);
	
	year = "" + now.getFullYear();
	month = "" + (now.getMonth() + 1);
	if (month.length == 1) {
		month = "0" + month;
	}
	day = "" + now.getDate();
	if (day.length == 1) {
		day = "0" + day;
	}
	return  year + "-" + month + "-"+ day;
}


function summaryifonThai(selectedDate) {
	var test = selectedDate.split('/');
	test[2] = test[2];
	selectedDate = test[2] + '-' + test[1] + '-' + test[0];
	
	now = new Date(selectedDate);
	
	year = "" + now.getFullYear();
	month = "" + (now.getMonth() + 1);
	if (month.length == 1) {
		month = "0" + month;
	}
	day = "" + now.getDate();
	if (day.length == 1) {
		day = "0" + day;
	}
	return  year + "-" + month + "-"+ day;
}

function getFormattedDateThaiSignature(selectedDate) {
	var test = selectedDate.split('/');
	test[2] = parseInt(test[2]) + 543;
	selectedDate = test[2] + '-' + test[1] + '-' + test[0]
	now = new Date(selectedDate);
	
	year = "" + now.getFullYear();
	month = "" + (now.getMonth() + 1);
	if (month.length == 1) {
		month = "0" + month;
	}
	day = "" + now.getDate();
	if (day.length == 1) {
		day = "0" + day;
	}
	return  day + "/" + month + "/"+ year;
}

function getFormattedDateEnglish(selectedDate) {
	var test = selectedDate.split('/');
	//test[2] = test[2] - rootConfig.thaiYearDifference;
	selectedDate = test[2] + '-' + test[1] + '-' + test[0];
	
	now = new Date(selectedDate);
	
	year = "" + now.getFullYear();
	month = "" + (now.getMonth() + 1);
	if (month.length == 1) {
		month = "0" + month;
	}
	day = "" + now.getDate();
	if (day.length == 1) {
		day = "0" + day;
	}
	return  year + "-" + month + "-"+ day;
} 

function getFormattedShashDateEnglish(selectedDate) {
	var test = selectedDate.split('/');
	//test[2] = test[2] - rootConfig.thaiYearDifference;
	selectedDate = test[2] + '-' + test[1] + '-' + test[0];
	
	now = new Date(selectedDate);
	
	year = "" + now.getFullYear();
	month = "" + (now.getMonth() + 1);
	if (month.length == 1) {
		month = "0" + month;
	}
	day = "" + now.getDate();
	if (day.length == 1) {
		day = "0" + day;
	}
	return  day + "/" + month + "/"+ year;
} 

function formatForDateControl(date) {

	now = LEDate(date);
	year = "" + now.getFullYear();
	month = "" + (now.getMonth() + 1);
	if (month.length == 1) {
		month = "0" + month;
	}
	day = "" + now.getDate();
	if (day.length == 1) {
		day = "0" + day;
	}
	return year + "-" + month + "-" + day;

}

function formatForDateTimeControl(now) {
	months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
	var formattedDate = now.getDate()+"-"+months[now.getMonth()]+"-"+now.getFullYear()+ ", "+ now.getHours() + ":"+ (now.getMinutes()<10?'0':'') + now.getMinutes() 
	// now.getMinutes(); 
	return formattedDate;
}

function formatForTimeControl(date, forSave) {

	var nowTZ = LEDate(date);
	hour = "" + nowTZ.getHours();
	if (hour.length == 1) {
		hour = "0" + hour;
	}
	minute = "" + nowTZ.getMinutes();
	if (minute.length == 1) {
		minute = "0" + minute;
	}
	if (forSave == true) {
		return hour + ":" + minute + ":" + "00";
	} else if (forSave == false) {
		return hour + ":" + minute;
	} else {
		return hour + ":" + minute + ":" + "00Z";
	}

}

function getNumberOfDays(dob){
	var selectedDate = dob;
	var dsplit = selectedDate.split("-");
	var test4 = dsplit[1] + "/" + dsplit[2]+ "/"+ dsplit[0];
	var date1 = new Date(test4);
	var now = new Date();
	var currentDate = now.getFullYear() + '-' + ('0' + (now.getMonth() + 1)).slice(-2) + '-' + ('0' + now.getDate()).slice(-2);
	var dsplit = currentDate.split("-");
	var test5 = dsplit[1] + "/" + dsplit[2]+ "/"+ dsplit[0];
	var date2 = new Date(test5);
	var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
	return diffDays;
}
function getDaysForAdditionalInsured(dob){
	var selectedDate = dob;
	var dsplit = selectedDate.split("-");
	var test4 = dsplit[1] + "/" + dsplit[2]+ "/"+ dsplit[0];
	var date1 = new Date(test4);
	var now = new Date();
	var currentDate = now.getFullYear() + '-' + ('0' + (now.getMonth() + 1)).slice(-2) + '-' + ('0' + now.getDate()).slice(-2);
	var dsplit = currentDate.split("-");
	var test5 = dsplit[1] + "/" + dsplit[2]+ "/"+ dsplit[0];
	var date2 = new Date(test5);
	var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
	return diffDays;
}

function LEDate(date) {
	if (date != undefined) {
		if ( typeof date != 'object') {
			if(typeof date == 'string' && date.length == 10){
			var dateString = date.split("-").join("/");
				var nowTZ = new Date(Date.parse(dateString));
			}else{
			var dateString = date.replace(' ', 'T');
			if(dateString.substr(dateString.length - 1)!='Z')
			dateString+='Z'
			var now = new Date(Date.parse(dateString));
			var nowTZ = new Date(now.getTime() + now.getTimezoneOffset()
						* 60000);
			}
			
		} else {
			nowTZ = date;
		}
	} else {
		nowTZ = new Date();
	}
	return nowTZ;
}

function getSyncObject(type,transactionObj,successCallback,errorCallback,options,syncType,isSaveBandWidth){
	return {
		"Type" : type,
		"transactionObj" :transactionObj ,
		"successCallback": successCallback,
		"errorCallback" : errorCallback,
		"options" : options,
		"syncType" : syncType,
		"saveBandWidth" :isSaveBandWidth
		} ;

}

/* keypress */

function getCodeBoxElement(index) {
    return document.getElementById('codeBox' + index);
}

function onKeyUpEvent(index, event) {
    const eventCode = event.which || event.keyCode;
    if (getCodeBoxElement(index).value.length === 1) {
      if (index !== 6) {
        getCodeBoxElement(index+ 1).focus();
      } else {
        getCodeBoxElement(index).blur();
        // Submit code
        console.log('submit code ');
      }
    }
    if (eventCode === 8 && index !== 1) {
      getCodeBoxElement(index - 1).focus();
    }
}

function onFocusEvent(index) {
    for (item = 1; item < index; item++) {
      const currentElement = getCodeBoxElement(item);
      if (!currentElement.value) {
          currentElement.focus();
          break;
      }
    }
}

/* Keypress */

// Generic function to handle the anugular translate services
function translateMessages($translate, message, additionalInfo) {
	if (!jQuery.isEmptyObject(message) && !jQuery.isEmptyObject($translate) && !jQuery.isEmptyObject(additionalInfo)) {
		// hack:Need to research on how we can inject data in between the
		// traslated message.
		return $translate.instant(message) + " " + additionalInfo;
	} else if (!jQuery.isEmptyObject(message) && !jQuery.isEmptyObject($translate)) {
		//return $translate(message);
		return $translate.instant(message);
	} else {
		return message;
	}
}

// Show hide the loading images across all ajax requests
/*
* function showHideLoadingImage(isShow, loadingMessage, $translate) { if
* (isShow) { var dynamic_height = $(window).height(); $('#page_loader').show();
* $('#page_loader').find('#loadImage').css("display", "inline");
* $('#page_loader').find('span.loadingMessage').css("display", "inline");
* $('#page_loader').css('height', dynamic_height).fadeIn(300); } else {
* $('#page_loader').hide(); } $('#page_loader
* span.loadingMessage').html(translateMessages($translate, loadingMessage));
* displayPopUp(); }
*
* //Show popups function displayPopUp() { var flag =
* $('#page_loader').css("display"); if (flag == "block") { var top =
* ($(window).height() - $('.modal_container').height()) / 2; var left =
* ($(window).width() - $('.modal_container').width()) / 2;
* $('.modal_container').css({ 'top': top, 'left': left });
* $("body").removeClass("display_modal").addClass("display_modal"); } else {
* $("body").removeClass("display_modal"); } }
*/
// to check internet connection
function checkConnection() {
	if ((rootConfig.isOfflineDesktop)) {
		if (navigator.onLine) {
			return true;
		} else {
			return false;
		}
	} else {
		var networkState = navigator.network.connection.type;
		var states = {};
		states[Connection.UNKNOWN] = 'Unknown connection';
		states[Connection.ETHERNET] = 'Ethernet connection';
		states[Connection.WIFI] = 'WiFi connection';
		states[Connection.CELL_2G] = 'Cell 2G connection';
		states[Connection.CELL_3G] = 'Cell 3G connection';
		states[Connection.CELL_4G] = 'Cell 4G connection';
		states[Connection.NONE] = 'No network connection';
		if (states[networkState] == 'No network connection') {
			return false;
		} else {
			return true;
		}
	}
	// alert('Connection type: ' + states[networkState]);
}

// NotifyMessages
/*
* function NotifyMessages(isError, message, $translate, additionalInfo) { if
* (isError) { $('#notifyIcon').removeClass("success_icon info_icon");
* $('#notifyIcon').addClass("error_icon info_icon");
* $("#notifyMsgContent").removeClass("success_alert_content");
* $("#notifyMsgContent").addClass("error_alert_content"); } else {
* $('#notifyIcon').removeClass("error_icon info_icon");
* $('#notifyIcon').addClass("success_icon info_icon");
* $("#notifyMsgContent").removeClass("error_alert_content");
* $("#notifyMsgContent").addClass("success_alert_content"); }
* $("#notifyMsgContent").html(translateMessages($translate, message,
* additionalInfo)); $('#notifyMsg').show().delay(1000).fadeOut('slow'); }
*/

/*
* function HideNotifyMsg() { // $('#notifyMsg').hide(); }
*/
// will remove once the service is always up
$(document).ready(function() {

		if ((rootConfig.isDeviceMobile)){
	if (/iPhone|iPad/i.test(navigator.userAgent)) {
		$(document).on('focus', 'select,input:not([type="radio"]):not([type="checkbox"])', function() {
				     			$('.navbar-fixed-top').removeClass('animated fadeInCustom');
								$('.navbar-fixed-top').css('position', 'absolute');
                                $('.footer').css('position', 'fixed');
                       			$('.footer').css('bottom', '-20px');
 
		});		
		$(document).on('blur', 'select,input:not([type="radio"]):not([type="checkbox"])', function() {
								$('.navbar-fixed-top').addClass('animated fadeInCustom');
								$('.navbar-fixed-top').css('position', 'fixed');
                       			$('.footer').css('bottom', '0px');
			setTimeout(function() {				
				window.scrollTo(document.body.scrollLeft, document.body.scrollTop);
			}, 0);
		});
		
		$(document).on('focus', 'textarea', function() {
								$('.navbar-fixed-top').removeClass('animated fadeInCustom');
								$('.navbar-fixed-top').css('position', 'absolute');						
		});
		$(document).on('blur', 'textarea', function() {
								$('.navbar-fixed-top').addClass('animated fadeInCustom');
								$('.navbar-fixed-top').css('position', 'fixed');						
						setTimeout(function() {			
				window.scrollTo(document.body.scrollLeft, document.body.scrollTop);
			}, 0);
		});
                  
                  $(document).on('focus', 'select,input[type="date"],input[type="time"]', function() {
                                 $('.footer').css('position', 'absolute');
                                $('.footer').css('bottom', '-20px');
                                 $('.alert-box-holder').css('display', 'none');
                                 $('.footer').removeClass('animated fadeInCustom');
                                 if($('.image-login').length==1)
                                 {
                                    $('footer').css('display','none');
                                 }
                            });
                  $(document).on('blur', 'select,input[type="date"],input[type="time"]', function() {
                                 
                                 
                                 $('.footer').addClass('animated fadeInCustom');
                                 $('.alert-box-holder').css('display', 'block');
                                 $('.footer').css('bottom', '0px');
                                 if($('.image-login').length==1)
                                 {
                                 $('footer').css('display','block');
                                 }
                                 $('.footer').css('position', 'fixed');
                              
                                 
                                 window.scrollTo(document.body.scrollLeft, document.body.scrollTop);
                    });
	
		$(document).on('blur', 'input:not([type="radio"]):not([type="checkbox"]):not([type="date"]):not([type="time"])', function() {
                      
            $('.footer').css('position', 'fixed');
			$('.footer').css('bottom', '0px');
			$('.footer').addClass('animated fadeInCustom');
				$('.alert-box-holder').css('display', 'block');
				if($('.image-login').length==1)
		{
			$('footer').css('display','block');	
		}
			$('.footer').css('position', 'fixed');
			
		
			window.scrollTo(document.body.scrollLeft, document.body.scrollTop);
		});
		$(document).on('focus', 'input:not([type="radio"]):not([type="checkbox"]):not([type="date"]):not([type="time"])', function() {
                   
			$('.footer').css('position', 'absolute');
			$('.footer').css('bottom', '-20px');
			$('.alert-box-holder').css('display', 'none');
			$('.footer').removeClass('animated fadeInCustom');
                if($('.image-login').length==1)
		{
		$('footer').css('display','none');			
		}
		});
		
		$(document).on('focus', 'textarea', function() {
		$('.footer').css('position', 'absolute');
		//$('.footer').css('margin-bottom', '-20px');
		$('.alert-box-holder').css('display', 'none');
		});
		$(document).on('blur', 'textarea', function() {
			setTimeout(function() {
				
				$('.footer').css('position', 'fixed');
				$('.alert-box-holder').css('display', 'block');
			
				window.scrollTo(document.body.scrollLeft, document.body.scrollTop);
			}, 0);
		});
                  }
	}
});

function UserDetailsObject(){
	return{
		"user": {
		       "userId": "",
		       "password": "",
		       "token":"",
			   "lastLoggedInDate": ""
		    },
		 "options" : {
				"headers": {
					"Token": "" ,
					"source" : ""
				}
		}
	}
}

/*Requirement Object model*/

function CreateRequirementFile() {
		return {
		  "RequirementFile":{
				"identifier":"",
		        "requirementName":"",
				"documentName" : "",
				"fileName" : "",
				"FormID" : "",
				"DocType" : "", 
				"status":"",
				"description":"",
				"date":"",
				"base64string" : ""
		      }
		
	};

}


function Requirement() {
		return {
    		"requirementType": "",
    		"requirementName": "",
    		"partyIdentifier": "",
    		"isMandatory": "",
    		"Documents": [{
    				"documentName": "",
    				"documentProofSubmitted": "",
    				"documentStatus": "",
    				"documentUploadStatus" : "",
					"FormID" : "",
					"DocType" : "", 
					"documentOptions": [
						{
							"optionTypeCode": ""
						}
					]
    		}]
		}
}
//Changed for Generali implementation
function RequirementRule() {
	return {
		"InsuredDetails": {
			  "age": "",
			  "partyIdentifier": "",
			  "insuredNationality": "",
			  "isInsuredSameAsPayer" : ""
        },
        "PolicyDetails": {
              "sumAssured" : ""
        },
        "BankDetails": {
        	  "modeOfPayment": "",
        	  "renewalMode": "",
        	  "isSplitPayment": "",
        	  "modeOfPayment2": "",
        	  "renewalMethod": "",
        	  "ETRNumber":"",
        	  "ETRNumber2":""
        },
        "QuestionDetails" : {
			  "FatcaQuestions1Ans" : "",
			  "FatcaQuestions11Ans" : "",
			  "FatcaQuestions12Ans" : "",
			  "FatcaQuestions13Ans" : "",
			  "FatcaQuestions2Ans" : "",
			  "FatcaQuestions3Ans" : "",
			  "FatcaQuestions4Ans" : ""
		},
		"PaymentDetails" :{
			  "ETRNumber" :""
		},
		"PayerDetails" :{
			  "partyIdentifier": "", 
			  "payerRelationshipWithInsured" :""
		},
		"ProductDetails" :{
			  "PhysicalApplicationNumber" :"",
			  "sumAssured" : ""
		}
	};
}

function IllustrationObject() {
	return {
		"Product" : {
			"RiderDetails" : [],
			"ProductDetails" : {
				"productCode" : "",
				"productId" : "",
				"productType" : "",
				"productName" : "",
				"productGroup" : "",
				"familyType" : ""
			},
			"policyDetails" : {
				"premiumType" : "",
				"isPremium" : "",
				"premium" : "",
				"packages" : "",
				"policyTerm" : "",
				"premiumPeriod" : "",
				"premiumFrequency" : "",
				"currency" : "",
				"bpmCode":"",
				"sumInsured":"",
				"isSumAssured" :"",
				"policyNumber" : "",
				"premiumTerm" : "",
				"tax" : "",
				"premiumAmount" : ""
			},
			"premiumSummary" : {},
			"Extension" : {
				"currency" : "",
				"channelID":"",
				"channelName":""
			},
            "templates" : {
                "eAppPdfTemplate" : "",
                "illustrationPdfTemplate" : "",
                "selectedLanguage":""
            }
		},
		"IllustrationOutput" : {
		},
		"PrivacyLaw": {
			"userConsentFlag": "No"
		},
		"Insured" : {
			"BasicDetails" : {
				"firstName" : "",
				"lastName" : "",
				"title" : "",
				"nickName" : "",
				"gender" : "",
				"dob" : "",
				"age":"",
				"fullName": "",
				"isInsuredSameAsPayer" : "",
                "occupationClassCode":""
			},
			"OccupationDetails" : [{
                "id":"",
				"occupationCode":"",
                "description":"",
                "job":"",
                "subJob":"",
                "occupationClass":"",
                "industry":"",
                "occupationCodeValue":"",
                "descriptionValue":"",
                "jobValue":"",
                "subJobValue":"",
                "occupationClassValue":"",
                "industryValue":"",
                "natureofWork":"",
                "natureofWorkValue":""
			}],
			"ContactDetails" :{
				"currentAddress" : {
					"city": "",
					"state": "",
					"houseNo" : "",
					"street" : "",
					"ward" : "",
					"district" : "",
					"country" : ""
				},
				"permanentAddress" :{
					"city": "",
					"state": "",
					"houseNo" : "",
					"street" : "",
					"ward" : "",
					"district" : "",
					"country" : ""
				},
				"emailId":"",
				"mobileNumber1" : "",
				"lineID":""
			},
			"Declaration" :{
				"place" : "",
				"date"	: ""
			}
		},
		"AdditionalInsured":[],		
		"Beneficiaries" : [],
		"Payer" : {
			"CustomerRelationship" : {
				"isPayorDifferentFromInsured" : "",
				"relationShipWithPO" : "",
                "relationWithInsured":""
			},
			"BasicDetails" : {
				"firstName" : "",
				"lastName" : "",
				"title" : "",
				"nickName" : "",
				"gender" : "",
				"dob" : "",
				"age":"",
				"fullName": "",
                "occupationClassCode":""                
			},
			"OccupationDetails" : [{
                "id":"",
				"occupationCode":"",
                "description":"",
                "job":"",
                "subJob":"",
                "occupationClass":"",
                "industry":"",
                "occupationCodeValue":"",
                "descriptionValue":"",
                "jobValue":"",
                "subJobValue":"",
                "occupationClassValue":"",
                "industryValue":""
			}],
			"ContactDetails" :{
				"currentAddress" : {
					"city": "",
					"state": "",
					"houseNo" : "",
					"street" : "",
					"ward" : "",
					"district" : "",
					"country" : ""
				},
				"permanentAddress" :{
					"city": "",
					"state": "",
					"houseNo" : "",
					"street" : "",
					"ward" : "",
					"district" : "",
					"country" : ""
				},
				"emailId":"",
				"mobileNumber1" : "",
				"lineID":""
			},
			"Declaration" :{
				"place" : "",
				"date"	: ""
			}
		},
		"AnnuitantDetails" : {
			"secondAnnuitantName" : "",
			"secondAnnuitantDOB" : "",
			"secondAnnuitantSex" : ""
		},
		"AgentInfo" : {
			"agentCode" : "",
			"isRDSUser":"",
			"agentId" : "",
			"agentName" : "",
			"agencyName" : "",
			"agentHomeNumber" : "",
			"agentMobileNumber" : "",
			"agentEmailID" : ""
		},
		"Requirements" : []
	};
}

function getPartyAge(dob) {
	var dobDate = new Date(dob);
	/* In I.E and safari it will accept date format in '/' separator only */
	if ((dobDate == "NaN" || dobDate == "Invalid Date") && dob != "" && dob != undefined && dob != null) {
		dobDate = new Date(dob.split("-").join("/"));
	}
	var todayDate = new Date();
	var age = todayDate.getFullYear() - dobDate.getFullYear();
	if (todayDate.getMonth() < dobDate.getMonth() - 1 || (dobDate.getMonth() - 1 === todayDate.getMonth() && todayDate.getMonth() < dobDate.getDate())) {
		age--;
	}
	return age;
}

//Function to get gender 
function getGender(gender){
	if(gender=='Male'){
		gender='M';
	}else if(gender=='Female'){
		gender = 'F';
	}
	return gender;
}
//Calculating age  
function ageCalculationsp(date1, date2) {
	var y1 = date1.getFullYear(), m1 = date1.getMonth(), d1 = date1.getDate(),
	y2 = date2.getFullYear(), m2 = date2.getMonth(), d2 = date2.getDate();
/*	if (m1 < m2) {
		var year = y1--;
		var month = m1 += 12;
	} */
	//  if (m1 == m2) {
		//var days = d1 - d2;
		//if(days.toString().indexOf("-") >  -1){
			//days = days * -1;
			//year = y1 - y2;
		//} else {
			//year = y1 -- ;
		//}
	//	if(d1 < d2){
		//	year = (y1 - y2)-1;
	//	} else{
		//	year = y1 - y2;
	//	}
//	}

var year=y1-y2;
if(m2>m1)
year--;
else if(m1==m2){
if(d2>d1){
year--;
}
}
	return year;
}
//Function to get days and age 
function getAgeByDOB(dob){
	var todayDate = new Date();
	var dobDate = new Date(dob);
	if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
		dobDate = new Date(dob.split("-").join("/"));
	}
	var curd = new Date(todayDate.getFullYear(),todayDate.getMonth(),todayDate.getDate());
	var cald = new Date(dobDate.getFullYear(),dobDate.getMonth(),dobDate.getDate());
//	var dife = ageCalculationd(curd,cald);
	var difeViet = ageCalculationsp(curd,cald);
	var age = difeViet;
//	age = dife[0];
	if(age < 1) {
		var datediff = curd.getTime() - cald.getTime();
		var days = (datediff / (24 * 60 * 60 * 1000))/365;
		days = Number(Math.round(days+'e3')+'e-3');
		age = 0;
	}
	return age;
}

//Function to get days and age 
function getAgeDays(dob){
	var todayDate = new Date();
	var dobDate = new Date(dob);
	if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
		dobDate = new Date(dob.split("-").join("/"));
	}
	var curd = new Date(todayDate.getFullYear(),todayDate.getMonth(),todayDate.getDate());
	var cald = new Date(dobDate.getFullYear(),dobDate.getMonth(),dobDate.getDate());
	var dife = ageCalculationd(curd,cald);
	var age = "";
	if(age < 1) {
		var datediff = curd.getTime() - cald.getTime();
		var days = (datediff / (24 * 60 * 60 * 1000))/365;
		days = Number(Math.round(days+'e3')+'e-3');
		age = 0;
	}
	if(dife[1] < 6){
		age = dife[0];
	}else{
		if(dife[2] >0){
			age = dife[0] +1;
		}
		else{
			age = dife[0];
		}
	}
	if (dob == undefined || dob == "") {
		age = "-";
	}
	if(age >= 1){
		return age;
	}else{
		return days;
	}
}

//function to get additional insured's age
/*function getAgeDaysForAddInsured(dob){
	var todayDate = new Date();
	var dobDate = new Date(dob);
	if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
		dobDate = new Date(dob.split("-").join("/"));
	}
	var curd = new Date(todayDate.getFullYear(),todayDate.getMonth(),todayDate.getDate());
	var cald = new Date(dobDate.getFullYear(),dobDate.getMonth(),dobDate.getDate());
	var dife = ageCalculationd(curd,cald);
	var age = "";
	if(age < 1) {
		
		var timeDiff = Math.abs(curd.getTime() - cald.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
		var days = Number(Math.round(diffDays+'e3')+'e-3');
		age = days;
	}
	if (dob == undefined || dob == "") {
		age = "-";
	}
	if(age >= 1){
		var stringAge = age.toString();
		var wholeNoAge = [];
		wholeNoAge = stringAge.split(".");
		var age2 = wholeNoAge[0];
		var ageAfterCalc = parseInt(age2);
		return ageAfterCalc;
	}else{
		return days;
	}
}*/


function isLeapYear(year) {
    var d = new Date(year, 1, 28);
    d.setDate(d.getDate() + 1);
    return d.getMonth() == 1;
}

function getAgeDaysForAddInsured(date) {
	var age = "";
    var d = new Date(date);
    var now = new Date();
    
    if ((d == "NaN" || d == "Invalid Date")) {
		d = new Date(dob.split("-").join("/"));
	}
    
    var years = now.getFullYear() - d.getFullYear();
    d.setFullYear(d.getFullYear() + years);
    if (d > now) {
        years--;
        d.setFullYear(d.getFullYear() - 1);
    }
    var days = (now.getTime() - d.getTime()) / (3600 * 24 * 1000);
    age =  years + days / (isLeapYear(now.getFullYear()) ? 366 : 365);
    if(age >= 1){
		var stringAge = age.toString();
		var wholeNoAge = [];
		wholeNoAge = stringAge.split(".");
		var age2 = wholeNoAge[0];
		var ageAfterCalc = parseInt(age2);
		return ageAfterCalc;
	}else{
		return age;
	}
}


//Function to get days and age 
function getAgeByGivingDOB(dob){
	var todayDate = new Date();
	var dobDate = new Date(dob);
	if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
		dobDate = new Date(dob.split("-").join("/"));
	}
	var curd = new Date(todayDate.getFullYear(),todayDate.getMonth(),todayDate.getDate());
	var cald = new Date(dobDate.getFullYear(),dobDate.getMonth(),dobDate.getDate());
	var dife = ageCalculationd(curd,cald);
	var age = "";
	if(age < 1) {
		var datediff = curd.getTime() - cald.getTime();
		var days = (datediff / (24 * 60 * 60 * 1000))/365;
		days = Number(Math.round(days+'e3')+'e-3');
		age = 0;
	}
	age = dife[0];
	/*if(dife[1] < 6){
		age = dife[0];
	}else{
		if(dife[2] >0){
			age = dife[0] +1;
		}
		else{
			age = dife[0];
		}
	}
	if (dob == undefined || dob == "") {
		age = "-";
	}*/
	if(age >= 1){
		return "age";
	}else{
		return "days";
	}
}

//return age in days
function getAgeInDays(date){
	var age = "";
	var d = new Date(date);
	var now = new Date();
	if ((d == "NaN" || d == "Invalid Date")) {
		d = new Date(dob.split("-").join("/"));
	}
	var years = now.getFullYear() - d.getFullYear();
	d.setFullYear(d.getFullYear() + years);
	if (d > now) {
		years--;
		d.setFullYear(d.getFullYear() - 1);
	}
	var days = (now.getTime() - d.getTime()) / (3600 * 24 * 1000);
	return days;
}

//Calculating age in years, month , days 
function ageCalculationd(date1, date2) {
	var y1 = date1.getFullYear(), m1 = date1.getMonth(), d1 = date1.getDate(),
	y2 = date2.getFullYear(), m2 = date2.getMonth(), d2 = date2.getDate();
	if (m1 < m2) {
		y1--;
		m1 += 12;
	}
	return [y1 - y2, m1 - m2, d1 - d2];
}

/*
 * Function to return relationship of insured entities
 */
function getInsuredRelationship(insured,additionalInsured){
	for(var m=0; m<additionalInsured.length; m++){
		if (insured==additionalInsured[m].BasicDetails.parent){
			insured =  additionalInsured[m].BasicDetails.relationshipWithInsured;
			break;
		}
	}
	return insured;
}
// convert to canonical structure
function convertToCanonicalModel(hierarchicalObj) {
	var canonicalObj = {};
	if (hierarchicalObj.Insured !== null && typeof hierarchicalObj.Insured != "undefined") {
		var insuredDob = hierarchicalObj.Insured.BasicDetails.dob;
		var insuredAge = getPartyAge(insuredDob);
		var insuredObj = {
			"name" : hierarchicalObj.Insured.BasicDetails.firstName,
			"age" : insuredAge,
			"sex" : hierarchicalObj.Insured.BasicDetails.gender,
			"state" : hierarchicalObj.Insured.BasicDetails.state,
			"monthlyIncome" : hierarchicalObj.Insured.BasicDetails.monthlyIncome,
			"vestingAge" : hierarchicalObj.Insured.BasicDetails.vestingAge,
			"emailIdTo" : hierarchicalObj.Insured.BasicDetails.emailIdTo,
			"emailIdCc" : hierarchicalObj.Insured.BasicDetails.emailIdCc
		};
	}

	if (hierarchicalObj.AnnuitantDetails !== null && typeof hierarchicalObj.AnnuitantDetails != "undefined") {
		var annuitantDob = hierarchicalObj.AnnuitantDetails.secondAnnuitantDOB;
		var annuitantAge = getPartyAge(annuitantDob);
		var annuitantDetailsObj = {
			"secondAnnuitantSex" : hierarchicalObj.AnnuitantDetails.secondAnnuitantSex,
			"secondAnnuitantAge" : annuitantAge,
			"secondAnnuitantName" : hierarchicalObj.AnnuitantDetails.secondAnnuitantName,
			"secondAnnuitantDOB" : hierarchicalObj.AnnuitantDetails.secondAnnuitantDOB
		};
	} else {
		var annuitantDetailsObj = {
			"secondAnnuitantSex" : "",
			"secondAnnuitantAge" : "",
			"secondAnnuitantName" : "",
			"secondAnnuitantDOB" : ""
		};
	}

	if (hierarchicalObj.Payer !== null && typeof hierarchicalObj.Payer != "undefined") {
		var payorDob = hierarchicalObj.Payer.BasicDetails.dob;
		var payorAge = getPartyAge(payorDob);
		var payorObj = {
			"name" : hierarchicalObj.Payer.BasicDetails.firstName,
			"age" : payorAge,
			"sex" : hierarchicalObj.Payer.BasicDetails.gender
		};
	}

	if (hierarchicalObj.ChildDetails != null && typeof hierarchicalObj.ChildDetails != "undefined") {
		var childDob = hierarchicalObj.ChildDetails.dob;
		var childAge = getPartyAge(childDob);
		var childObj = {
			"age" : childAge
		};
	}

	if (hierarchicalObj.Product.policyDetails !== null && typeof hierarchicalObj.Product.policyDetails != "undefined") {
		var policyObj = {
			"sumAssured" : hierarchicalObj.Product.policyDetails.faceAmount,
			"bonusOption" : hierarchicalObj.Product.policyDetails.bonusOption,
			"policyTerm" : hierarchicalObj.Product.policyDetails.policyTerm,
			"premiumPayingTerm" : hierarchicalObj.Product.policyDetails.premiumPayingTerm,
			"premiumMode" : hierarchicalObj.Product.policyDetails.premiumFrequency,
			"committedPremium" : hierarchicalObj.Product.policyDetails.committedPremium,
			"benefitPeriod" : hierarchicalObj.Product.policyDetails.benefitPeriod,
			"productCode" : hierarchicalObj.Product.ProductDetails.productCode,
			"annuityOption" : hierarchicalObj.Product.policyDetails.annuityOption,
			"annualIncome" : hierarchicalObj.Product.policyDetails.annualIncome
		};
	}

	var riderArray = hierarchicalObj.Product.RiderDetails;
	for ( i = 0; i < riderArray.length; i++) {
		if (riderArray[i].riderName !== null) {
			var riderDetails = {
				"id" : riderArray[i].id,
				"isRequired" : riderArray[i].isRequired,
				"sumAssured" : riderArray[i].riderSumAssured,
				"term" : riderArray[i].riderTerm,
				"emrRequired" : riderArray[i].riderEmrRequired,
				"emr" : riderArray[i].riderEmr,
				"emrDuration" : riderArray[i].riderEmrDuration,
				"flatExtraRequired" : riderArray[i].riderFlatExtraRequired,
				"flatExtraRate" : riderArray[i].riderFlatExtraRate,
				"flatExtraDuration" : riderArray[i].riderFlatExtraDuration
			};
			var riderName = riderArray[i].shortName;
			canonicalObj[riderName] = riderDetails;
		}
	}

	if (hierarchicalObj.Product.FlatExtraBase !== null && typeof hierarchicalObj.Product.FlatExtraBase != "undefined") {
		var flatExtraObj = {
			"rate" : hierarchicalObj.Product.FlatExtraBase.rate,
			"isRequired" : hierarchicalObj.Product.FlatExtraBase.isRequired,
			"duration" : hierarchicalObj.Product.FlatExtraBase.duration
		};
	}

	if (hierarchicalObj.Product.EMR !== null && typeof hierarchicalObj.Product.EMR != "undefined") {
		var emrObj = {
			"rate" : hierarchicalObj.Product.EMR.rate,
			"isRequired" : hierarchicalObj.Product.EMR.isRequired,
			"duration" : hierarchicalObj.Product.EMR.duration
		};
	}

	if (hierarchicalObj.Product.loanDetails !== null && typeof hierarchicalObj.Product.loanDetails != "undefined") {
		var loanDetailsArray = [];
		var loanRepaymentAmountArray = [];

		for ( i = 0; i < hierarchicalObj.Product.policyDetails.policyTerm; i++) {
			loanDetailsArray[i] = 0;
			loanRepaymentAmountArray[i] = 0;
		}

		var loanObj = {
			"isRequired" : hierarchicalObj.Product.loanDetails[0].isRequired,
			"amount" : loanDetailsArray
		};
		canonicalObj["Loan"] = loanObj;

		if (hierarchicalObj.Product.ProductDetails.productCode == "41") {
			hierarchicalObj.Product.loanDetails[0].LoanRepaymentDetails = {};
			hierarchicalObj.Product.loanDetails[0].LoanRepaymentDetails.isRequired = "No";
			var loanRepaymentObj = {
				"isRequired" : hierarchicalObj.Product.loanDetails[0].LoanRepaymentDetails.isRequired,
				"amount" : loanRepaymentAmountArray
			};

			canonicalObj["LoanRepayment"] = loanRepaymentObj;
		}

	}

	if (hierarchicalObj.Product.OPPB !== null && typeof hierarchicalObj.Product.OPPB != "undefined") {
		var oppbObj = {
			"isRequired" : hierarchicalObj.Product.OPPB.isRequired,
			"premiumValues" : hierarchicalObj.Product.OPPB.premiumValues
		};
	}

	if (hierarchicalObj.Product.policyDetails !== null && typeof hierarchicalObj.Product.policyDetails != "undefined") {
		var surrenderOptionArray = [];
		if (hierarchicalObj.Product.policyDetails.surrenderOption == "Yes") {
			if (hierarchicalObj.Product.withdrawalDetails[0].withdrawalType == "Periodic Withdrawal") {
				var currentYear = new Date().getFullYear();
				var insuredDob = hierarchicalObj.Insured.BasicDetails.dob;
				var insuredAge = getPartyAge(insuredDob);
				var withdrawalTermBegin = hierarchicalObj.Product.withdrawalDetails[0].withdrawalTermBegin;
				var withdrawalTermEnd = hierarchicalObj.Product.withdrawalDetails[0].withdrawalTermEnd;
				var withdrawalFrequency = hierarchicalObj.Product.withdrawalDetails[0].withdrawalFrequency;
				var arrayLength = Number(currentYear) + Number(100 - insuredAge);
				var j = 0;
				for ( i = currentYear; i <= arrayLength; i++) {
					if (i < withdrawalTermBegin || i > withdrawalTermEnd) {
						surrenderOptionArray[j] = 0;
						j++;
					} else {
						for ( k = withdrawalTermBegin; k <= withdrawalTermEnd; k = Number(k) + Number(withdrawalFrequency)) {
							surrenderOptionArray[j] = Number(hierarchicalObj.Product.withdrawalDetails[0].withdrawalAmount);
							j++;
							i++;
						}
					}
				}

			} else if (hierarchicalObj.Product.withdrawalDetails[0].withdrawalType == "Partial Withdrawal") {
				var currentYear = new Date().getFullYear();
				var insuredDob = hierarchicalObj.Insured.BasicDetails.dob;
				var insuredAge = getPartyAge(insuredDob);
				var arrayLength = Number(currentYear) + Number(100 - insuredAge);

				var withdrwalDetails = hierarchicalObj.Product.withdrawalDetails;
				withdrwalDetails.sort(function(obj1, obj2) {
					return obj1.withdrawalTerm - obj2.withdrawalTerm;
				});
				for ( i = currentYear, k = 0, j = 0; i <= arrayLength; i++, k++) {
					if (j < withdrwalDetails.length && withdrwalDetails[j].withdrawalTerm == i) {
						surrenderOptionArray[k] = Number(withdrwalDetails[j].withdrawalAmount);
						j++;
					} else {
						surrenderOptionArray[k] = 0;
					}
				}

			}
		} else {
			for ( i = 0; i < hierarchicalObj.Product.policyDetails.policyTerm; i++) {
				surrenderOptionArray[i] = 0;
			}
		}
		var surrenderOptionObj = {
			"isRequired" : hierarchicalObj.Product.policyDetails.surrenderOption,
			"amount" : surrenderOptionArray
		};
	}

	if (hierarchicalObj.Product.FeeType !== null && typeof hierarchicalObj.Product.FeeType != "undefined") {
		var feeTypeObj = {
			"coverMultiple" : hierarchicalObj.Product.FeeType.coverMultiple,
			"incomeBooster" : hierarchicalObj.Product.FeeType.incomeBooster,
			"maturityBenefitRate" : hierarchicalObj.Product.FeeType.maturityBenefitRate
		};
	}

	if (hierarchicalObj.Product.TopUp !== null && typeof hierarchicalObj.Product.TopUp != "undefined") {
		var topUpObj = {
			"pct" : hierarchicalObj.Product.TopUp.pct,
			"isRequired" : hierarchicalObj.Product.TopUp.isRequired,
			"input" : hierarchicalObj.Product.TopUp.topUpArray
		};
	}

	if (hierarchicalObj.Product.IncomeBooster !== null && typeof hierarchicalObj.Product.IncomeBooster != "undefined") {
		var incomeBoosterObj = {
			"rating1" : hierarchicalObj.Product.IncomeBooster.rating1,
			"rating2" : hierarchicalObj.Product.IncomeBooster.rating2
		};
	}

	if (hierarchicalObj.Product.InvPension !== null && typeof hierarchicalObj.Product.InvPension != "undefined") {
		var invPensionObj = {
			"maximiseOption" : hierarchicalObj.Product.InvPension.maximiseOption,
			"preserveOption" : hierarchicalObj.Product.InvPension.preserveOption
		};
	}

	if (hierarchicalObj.Product.FundInformation !== null && typeof hierarchicalObj.Product.FundInformation != "undefined") {
		// var fundArray=[];
		var fundsObj = {};
		for ( i = 0; i < hierarchicalObj.Product.FundInformation.length; i++) {
			/*
			 * var fundsObj = new Object(); fundsObj.AllocationPercentage =
			 * hierarchicalObj.Product.FundInformation[i].AllocationPercentage,
			 * fundsObj.BonusOrDividend =
			 * hierarchicalObj.Product.FundInformation[i].BonusOrDividend,
			 * fundsObj.PayoutOrSettlement=
			 * hierarchicalObj.Product.FundInformation[i].PayoutOrSettlement,
			 * fundsObj.FundOrPortfolioName=
			 * hierarchicalObj.Product.FundInformation[i].FundOrPortfolioName,
			 * fundArray.push(fundsObj);
			 */
			var fundName = "";
			if (hierarchicalObj.Product.FundInformation[i].FundOrPortfolioName == "Secure Fund") {
				fundName = "secureFund";
			} else if (hierarchicalObj.Product.FundInformation[i].FundOrPortfolioName == "Conservative Fund") {
				fundName = "conservativeFund";
			} else if (hierarchicalObj.Product.FundInformation[i].FundOrPortfolioName == "Balanced Fund") {
				fundName = "balancedFund";
			} else if (hierarchicalObj.Product.FundInformation[i].FundOrPortfolioName == "Growth Fund") {
				fundName = "growthFund";
			} else if (hierarchicalObj.Product.FundInformation[i].FundOrPortfolioName == "Growth Super Fund") {
				fundName = "growthSuperFund";
			}
			if (fundName == "") {
				fundName = hierarchicalObj.Product.FundInformation[i].FundOrPortfolioName;
			}
			if (fundName != "") {
				fundsObj[fundName] = hierarchicalObj.Product.FundInformation[i].AllocationPercentage;
			}

		}
		if (!(fundsObj.hasOwnProperty('balancedFund'))) {
			fundsObj['balancedFund'] = 0;
		}
		if (!(fundsObj.hasOwnProperty('conservativeFund'))) {
			fundsObj['conservativeFund'] = 0;
		}
		if (!(fundsObj.hasOwnProperty('secureFund'))) {
			fundsObj['secureFund'] = 0;
		}
		if (!(fundsObj.hasOwnProperty('growthFund'))) {
			fundsObj['growthFund'] = 0;
		}
		if (!(fundsObj.hasOwnProperty('growthSuperFund'))) {
			fundsObj['growthSuperFund'] = 0;
		}

	}

	if (hierarchicalObj.Product.Extension !== null && typeof hierarchicalObj.Product.Extension != "undefined") {
		var extensionObj = {
			"mliEmpl" : hierarchicalObj.Product.Extension.mliEmpl,
			"rateId" : hierarchicalObj.Product.Extension.rateId,
			"riskClass" : hierarchicalObj.Product.Extension.riskClass,
			"saveMoreTomOptionReqd" : hierarchicalObj.Product.Extension.saveMoreTomOptionReqd,
			"dfaOption" : hierarchicalObj.Product.Extension.dfaOption,
			"stpOption" : hierarchicalObj.Product.Extension.stpOption,
			"indexationOption" : hierarchicalObj.Product.Extension.indexationOption
		};
	}

	canonicalObj.InsuredDetails = insuredObj;
	canonicalObj.PayorDetails = payorObj;
	canonicalObj.ChildDetails = childObj;
	canonicalObj.PolicyDetails = policyObj;
	canonicalObj.FlatExtraBase = flatExtraObj;
	canonicalObj.EMR = emrObj;
	canonicalObj.OPPB = oppbObj;
	canonicalObj.SurrenderOption = surrenderOptionObj;
	canonicalObj.FeeType = feeTypeObj;
	canonicalObj.TopUp = topUpObj;
	canonicalObj.IncomeBooster = incomeBoosterObj;
	canonicalObj.InvPension = invPensionObj;
	canonicalObj.Funds = fundsObj;
	canonicalObj.Extension = extensionObj;
	canonicalObj.AnnuitantDetails = annuitantDetailsObj;

	return canonicalObj;
}

// Observation

function ObservationObject() {
	return {
		"Observation" : {
			"Page" : {
				"Context" : "FNA",
				"PageDesc" : ""
			},
			"CreationDate" : "",
			"Path" : "",
			"CustomerName" : "",
			"CustomerId" : "",
			"CustomerDbId" : "",
			"Email" : "",
			"Agent" : "",
			"CustomerDeviceId" : ""
		}
	};
}

// Observation

function FNAObject() {
	return {
		"FNA" : {
			"lifeStage" : {
				"id" : "",
				"description" : ""
			},
			"parties" : [],
			"goals" : [],
			"pageSelections" : [{
				"pageType" : "Introduction",
				"label" : "fna.introduction",
				"isSelected" : "selected",
				"isDisabled" : ""
			}, {
				"pageType" : "Customers Profile",
				"label" : "fna.customersProfile",
				"isSelected" : "",
				"isDisabled" : "disabled"
			}, {
				"pageType" : "Need Analysis",
				"label" : "fna.needAnalysis",
				"isSelected" : "",
				"isDisabled" : "disabled"
			}, {
				"pageType" : "Product Recommendation",
				"label" : "fna.productRecommendation",
				"isSelected" : "",
				"isDisabled" : "disabled"
			}, {
				"pageType" : "FNA Report",
				"label" : "fna.fnaReport",
				"isSelected" : "",
				"isDisabled" : "disabled"
			}],
			"activeGoalIndex" : 0,
			"questionnaire" : [{
				"questionId" : "question1",
				"answerIndex" : ""
			}, {
				"questionId" : "question2",
				"answerIndex" : ""
			}, {
				"questionId" : "question3",
				"answerIndex" : ""
			},
			{
				"questionId" : "question4",
				"answerIndex" : ""
			}, {
				"questionId" : "question5",
				"answerIndex" : ""
			}, {
				"questionId" : "question6",
				"answerIndex" : ""
			},
			{
				"questionId" : "question7",
				"answerIndex" : ""
			}, {
				"questionId" : "question8",
				"answerIndex" : ""
			}, {
				"questionId" : "question9",
				"answerIndex" : ""
			}],

			"riskProfile" : {
				"score" : "",
				"level" : ""
			},
			"selectedProduct" : {},
			"lastVisitedPage" : "",
			"checkedState" : "No"
		}
	};
}

// Document Object Model
function EappAttachment() {
	return {
		"Upload" : {
			"Photograph" : [{
				"isMandatory" : true,
				"documentType" : "Photograph",
				"documentName" : "",
				"documentStatus" : "Synced",
				"documentDescription" : "",
				"pages" : [{
					"fileName" : "",
					"base64string" : "/9j/4AAQSkZJRgABAQEAYABgAAD/4QBcRXhpZgAATU0AKgAAAAgABAMCAAIAAAAWAAAAPlEQAAEAAAABAQAAAFERAAQAAAABAAALE1ESAAQAAAABAAALEwAAAABQaG90b3Nob3AgSUNDIHByb2ZpbGUA/+IMWElDQ19QUk9GSUxFAAEBAAAMSExpbm8CEAAAbW50clJHQiBYWVogB84AAgAJAAYAMQAAYWNzcE1TRlQAAAAASUVDIHNSR0IAAAAAAAAAAAAAAAEAAPbWAAEAAAAA0y1IUCAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARY3BydAAAAVAAAAAzZGVzYwAAAYQAAABsd3RwdAAAAfAAAAAUYmtwdAAAAgQAAAAUclhZWgAAAhgAAAAUZ1hZWgAAAiwAAAAUYlhZWgAAAkAAAAAUZG1uZAAAAlQAAABwZG1kZAAAAsQAAACIdnVlZAAAA0wAAACGdmlldwAAA9QAAAAkbHVtaQAAA/gAAAAUbWVhcwAABAwAAAAkdGVjaAAABDAAAAAMclRSQwAABDwAAAgMZ1RSQwAABDwAAAgMYlRSQwAABDwAAAgMdGV4dAAAAABDb3B5cmlnaHQgKGMpIDE5OTggSGV3bGV0dC1QYWNrYXJkIENvbXBhbnkAAGRlc2MAAAAAAAAAEnNSR0IgSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAASc1JHQiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAADzUQABAAAAARbMWFlaIAAAAAAAAAAAAAAAAAAAAABYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9kZXNjAAAAAAAAABZJRUMgaHR0cDovL3d3dy5pZWMuY2gAAAAAAAAAAAAAABZJRUMgaHR0cDovL3d3dy5pZWMuY2gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZGVzYwAAAAAAAAAuSUVDIDYxOTY2LTIuMSBEZWZhdWx0IFJHQiBjb2xvdXIgc3BhY2UgLSBzUkdCAAAAAAAAAAAAAAAuSUVDIDYxOTY2LTIuMSBEZWZhdWx0IFJHQiBjb2xvdXIgc3BhY2UgLSBzUkdCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGRlc2MAAAAAAAAALFJlZmVyZW5jZSBWaWV3aW5nIENvbmRpdGlvbiBpbiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAACxSZWZlcmVuY2UgVmlld2luZyBDb25kaXRpb24gaW4gSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB2aWV3AAAAAAATpP4AFF8uABDPFAAD7cwABBMLAANcngAAAAFYWVogAAAAAABMCVYAUAAAAFcf521lYXMAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAKPAAAAAnNpZyAAAAAAQ1JUIGN1cnYAAAAAAAAEAAAAAAUACgAPABQAGQAeACMAKAAtADIANwA7AEAARQBKAE8AVABZAF4AYwBoAG0AcgB3AHwAgQCGAIsAkACVAJoAnwCkAKkArgCyALcAvADBAMYAywDQANUA2wDgAOUA6wDwAPYA+wEBAQcBDQETARkBHwElASsBMgE4AT4BRQFMAVIBWQFgAWcBbgF1AXwBgwGLAZIBmgGhAakBsQG5AcEByQHRAdkB4QHpAfIB+gIDAgwCFAIdAiYCLwI4AkECSwJUAl0CZwJxAnoChAKOApgCogKsArYCwQLLAtUC4ALrAvUDAAMLAxYDIQMtAzgDQwNPA1oDZgNyA34DigOWA6IDrgO6A8cD0wPgA+wD+QQGBBMEIAQtBDsESARVBGMEcQR+BIwEmgSoBLYExATTBOEE8AT+BQ0FHAUrBToFSQVYBWcFdwWGBZYFpgW1BcUF1QXlBfYGBgYWBicGNwZIBlkGagZ7BowGnQavBsAG0QbjBvUHBwcZBysHPQdPB2EHdAeGB5kHrAe/B9IH5Qf4CAsIHwgyCEYIWghuCIIIlgiqCL4I0gjnCPsJEAklCToJTwlkCXkJjwmkCboJzwnlCfsKEQonCj0KVApqCoEKmAquCsUK3ArzCwsLIgs5C1ELaQuAC5gLsAvIC+EL+QwSDCoMQwxcDHUMjgynDMAM2QzzDQ0NJg1ADVoNdA2ODakNww3eDfgOEw4uDkkOZA5/DpsOtg7SDu4PCQ8lD0EPXg96D5YPsw/PD+wQCRAmEEMQYRB+EJsQuRDXEPURExExEU8RbRGMEaoRyRHoEgcSJhJFEmQShBKjEsMS4xMDEyMTQxNjE4MTpBPFE+UUBhQnFEkUahSLFK0UzhTwFRIVNBVWFXgVmxW9FeAWAxYmFkkWbBaPFrIW1hb6Fx0XQRdlF4kXrhfSF/cYGxhAGGUYihivGNUY+hkgGUUZaxmRGbcZ3RoEGioaURp3Gp4axRrsGxQbOxtjG4obshvaHAIcKhxSHHscoxzMHPUdHh1HHXAdmR3DHeweFh5AHmoelB6+HukfEx8+H2kflB+/H+ogFSBBIGwgmCDEIPAhHCFIIXUhoSHOIfsiJyJVIoIiryLdIwojOCNmI5QjwiPwJB8kTSR8JKsk2iUJJTglaCWXJccl9yYnJlcmhya3JugnGCdJJ3onqyfcKA0oPyhxKKIo1CkGKTgpaymdKdAqAio1KmgqmyrPKwIrNitpK50r0SwFLDksbiyiLNctDC1BLXYtqy3hLhYuTC6CLrcu7i8kL1ovkS/HL/4wNTBsMKQw2zESMUoxgjG6MfIyKjJjMpsy1DMNM0YzfzO4M/E0KzRlNJ402DUTNU01hzXCNf02NzZyNq426TckN2A3nDfXOBQ4UDiMOMg5BTlCOX85vDn5OjY6dDqyOu87LTtrO6o76DwnPGU8pDzjPSI9YT2hPeA+ID5gPqA+4D8hP2E/oj/iQCNAZECmQOdBKUFqQaxB7kIwQnJCtUL3QzpDfUPARANER0SKRM5FEkVVRZpF3kYiRmdGq0bwRzVHe0fASAVIS0iRSNdJHUljSalJ8Eo3Sn1KxEsMS1NLmkviTCpMcky6TQJNSk2TTdxOJU5uTrdPAE9JT5NP3VAnUHFQu1EGUVBRm1HmUjFSfFLHUxNTX1OqU/ZUQlSPVNtVKFV1VcJWD1ZcVqlW91dEV5JX4FgvWH1Yy1kaWWlZuFoHWlZaplr1W0VblVvlXDVchlzWXSddeF3JXhpebF69Xw9fYV+zYAVgV2CqYPxhT2GiYfViSWKcYvBjQ2OXY+tkQGSUZOllPWWSZedmPWaSZuhnPWeTZ+loP2iWaOxpQ2maafFqSGqfavdrT2una/9sV2yvbQhtYG25bhJua27Ebx5veG/RcCtwhnDgcTpxlXHwcktypnMBc11zuHQUdHB0zHUodYV14XY+dpt2+HdWd7N4EXhueMx5KnmJeed6RnqlewR7Y3vCfCF8gXzhfUF9oX4BfmJ+wn8jf4R/5YBHgKiBCoFrgc2CMIKSgvSDV4O6hB2EgITjhUeFq4YOhnKG14c7h5+IBIhpiM6JM4mZif6KZIrKizCLlov8jGOMyo0xjZiN/45mjs6PNo+ekAaQbpDWkT+RqJIRknqS45NNk7aUIJSKlPSVX5XJljSWn5cKl3WX4JhMmLiZJJmQmfyaaJrVm0Kbr5wcnImc951kndKeQJ6unx2fi5/6oGmg2KFHobaiJqKWowajdqPmpFakx6U4pammGqaLpv2nbqfgqFKoxKk3qamqHKqPqwKrdavprFys0K1ErbiuLa6hrxavi7AAsHWw6rFgsdayS7LCszizrrQltJy1E7WKtgG2ebbwt2i34LhZuNG5SrnCuju6tbsuu6e8IbybvRW9j74KvoS+/796v/XAcMDswWfB48JfwtvDWMPUxFHEzsVLxcjGRsbDx0HHv8g9yLzJOsm5yjjKt8s2y7bMNcy1zTXNtc42zrbPN8+40DnQutE80b7SP9LB00TTxtRJ1MvVTtXR1lXW2Ndc1+DYZNjo2WzZ8dp22vvbgNwF3IrdEN2W3hzeot8p36/gNuC94UThzOJT4tvjY+Pr5HPk/OWE5g3mlucf56noMui86Ubp0Opb6uXrcOv77IbtEe2c7ijutO9A78zwWPDl8XLx//KM8xnzp/Q09ML1UPXe9m32+/eK+Bn4qPk4+cf6V/rn+3f8B/yY/Sn9uv5L/tz/bf///9sAQwACAQECAQECAgICAgICAgMFAwMDAwMGBAQDBQcGBwcHBgcHCAkLCQgICggHBwoNCgoLDAwMDAcJDg8NDA4LDAwM/9sAQwECAgIDAwMGAwMGDAgHCAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgAeAB4AwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A/fyiiigAooooAKRnCKWYhVUZJPAFJNMltC8kjKkcalmY8BQBya8h8d/EG48VXbxRM8NghwkY4Mn+03r9O360Aeg6n8TtF0uQo12JnHUQqXH5jj9aof8AC6tI3Y8u+I9fLXH/AKFXlNFAHsmmfE/RdTcKLsQOe0ylB+fT9a345FlRWRlZWGQQcg18+VveC/Hl14Su0AZ5rJj+8hJyPqvof50AezUVHaXcd/axTwsHimUOjDuCMipKACiiigAooooAKKKKACiiigDlvi/qjad4QaNCVa7kWI4645J/lj8a8kr0345H/iSWQ9Zz/wCgmvMqACiiigAooooA9Y+DWoteeEjExJNrMyD/AHSA38ya6yuF+BhP9l347CVf5V3VABRRRQAUUUUAFFFFABRRRQBw3xzI/sixGeTM3/oNeaV2nxuuZX8SW0LEiGO3DIO2Sxyf0H5VxdABRRRQAUUUUAek/Asj+ztQGRkSL/I13deV/Ba6li8VSRIWMUsDGQduCMH8zj8a9UoAKKKKACiiigAooooAKKKKAOX+KfhJfEGhtcpkXNirOvH316lf0yP/AK9eR19CEBgQQCD1FeK+O/C7eFfEEsIU/Z5f3kDdip7fUdP/ANdAGLRRRQAUUVreCvDL+K9eitgCIV+eZh/Cg6/ien40AehfCfwkuiaIt7Jzc36Bun3E6gD68E/h6V1lIiLEiqqhVUYAHAApaACiiigAooooAKKKKACiiigArn/idpFtqfhG6knCq9qplifuren49PyrT1zxDZ+HLTzryZYlP3R1Zz6Ad68r8cfES48XP5KKbeyQ5WPOWc+rH+nQe/WgDnKKKKACvYPhZpFtp3hK3mgAaS7G+V+5OSMfQdPzrx+ug8D+P7jwfcFCDPZSHLxZ5B/vL6H+f60AeyUVR0LxHZ+JLXzrOZZVGNy9GQ+hHar1ABRRRQAUUUUAFFVtW1a30OwkurqQRQx9T1P0A7muD1/43O4aPTbYIP8AnrNyfwUf1NAHf32oQaZbNNcTRwRL1Z2CiuI8UfGiOENDpUfmt08+QYUfRep/HH0rg9W1y812fzby4luHHTceF+g6D8KqUAWNT1W51m6ae6mknlb+JjnHsPQewqvRRQAUUUUAFFFFAE+nancaRdrPazSQSp0ZTg/T3HtXfeF/jSjhYdViKN08+IZB+q9vw/KvOqKAPftP1K31W2Wa2mjniboyMCPp9amrwXS9Zu9EuBLaXEtu/cocA/UdD+NdpoHxtli2x6lbCVRx5sPDfip4P4YoA9Goqro2s22v2CXVpIJYX74wQe4I7GigDnPjR/yJ6dsXCfyavKK9X+NBx4PQetwn8mryigAooooAKKKKACiiigAooooAKKKKACiiigD1X4Kf8ijL/wBfLf8AoK0UfBT/AJFGX/r6b/0FaKAOl1rRbfxBp0lrdJvik9Dgg9iD2Ncz/wAKT0jJPnX/AP38X/4miigA/wCFJ6R/z2v/APv4v/xNH/Ck9I/57X//AH8X/wCJoooAP+FJ6R/z2v8A/v4v/wATR/wpPSP+e1//AN/F/wDiaKKAD/hSekf89r//AL+L/wDE0f8ACk9I/wCe1/8A9/F/+JoooAP+FJ6R/wA9r/8A7+L/APE0f8KT0j/ntf8A/fxf/iaKKAD/AIUnpH/Pa/8A+/i//E0f8KT0j/ntf/8Afxf/AImiigA/4UnpH/Pa/wD+/i//ABNH/Ck9I/57X/8A38X/AOJoooA6TQNBtvDWmpa2qMsSkkknLMT1JPrRRRQB/9k="
				}]
			}],
			"Signature" : [{
				"isMandatory" : true,
				"documentType" : "Signature",
				"documentName" : "",
				"documentStatus" : "Saved",
				"documentDescription" : "",
				"pages" : [{
					"fileName" : "",
					"base64string" :"",
					"fileContent" : ""
				}]
			}],
			"Documents" : [{
				"isMandatory" : true,
				"documentType" : "",
				"documentName" : "",
				"documentUploadStatus" : "",
				"documentStatus" : "Saved",
				"documentDescription" : "",
				"isSigned" : "No",
				"pages" : []
			}]
		}
	};

}

// Document Object Model
function IllustrationAttachment() {
	return {
		"Upload" : {
			"Signature" : [{
				"isMandatory" : true,
				"documentType" : "Signature",
				"documentName" : "",
				"documentStatus" : "Saved",
				"documentDescription" : "",
				"pages" : [{
					"fileName" : "",
					"base64string" :"",
					"fileContent" : ""
				},
				{
                	"fileName" : "",
                	"base64string" :"",
                	"fileContent" : ""
                }]
			}]
		}
	};
}

function LmsObject() {
	return {
		"Lead": {
			"AgentInfo": {
				"agentId": ""
			},
			"RMInfo": {
				"RMName": "",
				"RMNationalID": "",
				"RMMobileNumber": "",
				"RMReportingAgentCode":""
			},
			"BasicDetails": {
				"fullName": "",
				"lastName": "",
				"nickName": "",
				"gender": "",
				"dob": "",
				"identityProof" : "",
				"IDcard":"",
				"incomeRange" : "",
				"need" : "",
				"action" : "",
				"nextAction" : "",
				"age": "",
				"nationality": "",
				"IdentityExpDate": "",
				"taxNo": "",
				"estIncome": ""
			},
			"ContactDetails": {
				"homeNumber1": "",
				"homeNumber":"",
				"emailId": "",
				"mobileNumber1": "",
				"lineID":"",
				"birthAddress": {
					"city": "",
					"country": ""
				},
				"currentAddress": {
					"addressLine1": "",
					"street": "",
					"ward": "",
					"country": "",
					"city": "",
					"state": "",
					"zipcode": ""
				}
			},
			"SourceDetails": {
				"source": ""
			},
			"OccupationDetails": [],
			"SeverityDetails": {
				"severity": ""
			},
			"StatusDetails": {
				"disposition": "New",
				"subDisposition": "",
				"reason": "",
				"subReason": ""
			},
			"PaymentDetails": {
				"premiumCollected": ""
			},
			"Dependents": {
				"spouseEstIncome": "",
				"noOfChildren": ""
			},
			"required":{
				"requiredMeetingDate":""
			}
		},
			"Communication":[{
			"metDateAndTime": "",
			"metDate": "",
			"metTime": "",
			"interactionType": "",
			"planedDateAndTime" : "",
			"isJointCall": "yes",
			"jointCallWith": "",
			"remarks": "",
			"followUps": [],
			"status": "New"
			
		},
		{
			"metDateAndTime": "",
			"metDate": "",
			"metTime": "",
			"interactionType": "Call",
			"planedDateAndTime" : "",
			"isJointCall": "yes",
			"jointCallWith": "",
			"remarks": "",
			"followUps": [],
			"status": "New",
			"count":0,
			"communicationStatusCode":""
           
		},
		{
			"metDateAndTime": "",
			"metDate": "",
			"metTime": "",
			"interactionType": "Appointment",
			"planedDateAndTime" : "",
			"isJointCall": "yes",
			"jointCallWith": "",
			"remarks": "",
			"followUps": [],
			"status": "New",
			"count":0,
			
		},
		{
			"metDateAndTime": "",
			"metDate": "",
			"metTime": "",
			"interactionType": "Visit",
			"planedDateAndTime" : "",
			"isJointCall": "yes",
			"jointCallWith": "",
			"remarks": "",
			"followUps": [],
			"status": "New",
			"count":0,
			"communicationStatusCode":""
		}
		],
		"SystemRecordDetails": {
			"loginDate": "",
			"expectedBusiness": "",
			"expSubmissionDate": ""
		},
		"Feedback": {
			"LastUpdatedDetails" : "",
			"questions": [{
				"questionID": "1",
				"option": "0"
			},
			{
				"questionID": "2",
				"option": ""
			},
			{
				"questionID": "3",
				"option": "0"
			},
			{
				"questionID": "4",
				"option": "0"
			},
			{
				"questionID": "5",
				"option": ""
			},
			{
				"questionID": "6",
				"option": ""
			}]
			
		},
		"Payer" : {},
		"Insured" : {},
		"AdditionalInsured" : {},
		"Beneficiaries" : {},
		"Policies": {
			"productSold": [{
				"productCode": "",
				"proposalNumber": "",
				"premium": ""
			}],
			"submissionDate": ""
		},
		"Referrals": {
			"parentLeadId": "",
			"referredByName" : "",
			"referredByContact" : "",
			"referredByBranchName":"",
			"referredByNationalID":""
			
		}
	};
}

//Push Notification Register Request
function PushNotificationRegisterRequest() {

	return {
		"registerDeviceRequest" : {
			"MobileAppBundleID" : "",
			"DeviceType" : "",
			"DeviceUserID" : "",
			"PushMsgRegisteredID" : "",
			"UserGroup" : "",
			"Device_ID" : "",
			"Application_Name" : ""
		}
	};
}
function FeedbackIssueModelObject() { 
	return {
			"templateID": "",
			"composeHeader": "",
			"type": "",
			"moduleTitle": "Module Name",
			"screenTitle": "Screen Name",
			"descriptionTitle": "Description",
			"contactNumberTitle" : "Contact Number",
			"deviceInformation": "Device Information:",
			"deviceUUID": "Device UUID",
			"uuid": "",
			"deviceVersion": "Version",
			"version": "",
			"devicePlatform": "Platform",
			"platform": "",
			"deviceVersionModel": "Model",
			"model": "",
			"deviceManufacturer": "Manufacturer",
			"manufacturer": "",
			"emailFooter": "",
			"attachDeviceTemplate": "No",
			"browserInformation" : "Browser Information",
			"browserName" : "Browser Name",
			"browserVersion" : "Browser Version",
			"language" : "Browser Language",
			"platform" : "Platform",
			"agentDetails" : "Agent Details",
			"agentCodeTitle" : "Agent Code",
			"agentCode" : "",
			"agentNameTitle" : "Agent Name",
			"agentName" : "",
			"deviceInfo" : {
				"uuid" : "",
				"version" : "",
				"platform" : "",
				"model" : ""
			},
			"browserInfo" : {
				"browserName"	: "",
				"majorVersion" : "",
				"fullVersion"	: "",
				"language" : "",
				"platform" : ""
			}
			
			
				
	};
}

function EmailObject() { 
	return {
			"toMailIds": [],
			"ccMailIds": [],
			"bccMailIds": [],
			"mailSubject": "",
			"emailAttachments": [],
			"emailIsHtml": true,
			"openWithApp" : null,
			"emailBody": ""
				
	};
}

function checkinObject(){
	return{
		"TransTrackingID" : "",
		"Type" : "PushNotification",
		"TransactionData" : {
			"CheckInDetails":{
				"CheckedinBranch":"",
				"branchName":"",
				"NotifierID":[],
				"AgentID":"",
				"agentName":"",
				"sendPushnotification":true,
				"type":"agentCheckin",
				"agentType":""
			}
		}
	}
}

function getCheckInDetailsObject(){
	return{
		"TransTrackingID" : "",
		"Type" : "",
		"TransactionData" : {
			"UserDetails":[]
		}
	}
}
function RequirementFile() {
		return {
		  "RequirementFile":{
				"identifier":"",
		        "requirementName":"",
				"documentName" : "",
				"fileName" : "",
				"status":"",
				"description":"",
				"date":"",
				"base64string" : ""
		      }
		
	};

}

function paymentSaveObject() {
	return {
		"PaymentInfo":{
			"paymentMethod" :"",
			"paySplit" :"",
			"paymentType" : "",
			"transDate" : "",
			"paymentDateTime":"",
			"creditCardNo":"",
			"approvalCode":"",
			"bankName":"",
			"bankCode":"",
			"branchName":"",
			"branchCode":"",
			"bankAccNo":"",
			"issBankName":"",
			"issBranchName":"",
			"issBranchCode":"",
			"premiumAmt":"",
			"bankCharge":"",
			"chequeNumber":"",
			"chequeDate":"",
			"etrNumber":"",
			"etrDate":"",
			"cash" : "",
			"receiptNumber" : "",
			"notes" : "",
			"status" : "",
			"merchantId":"",
			"paymentDateTim":"",
			"etrGeneration":"",
			"splitPaymentTst":"",	
			"skipPaymentTst":""
			}
		};
}


function PaymentInfo(){
  return {
   "PaymentInfo": { 	 
	   "payment_no": "",
	   "payment_purpose": "",
	   "first_name": "",
	   "last_name": "",
	   "email": "",
	   "premium": "",
	   "company": "",
	   "grace_due_date": "",
	   "allow_to_pay_shortage": "",
	   "source_id": "",
	   "paymentUrl": "",
	   "isCopyURL": "",
	   "sms_allowed": "",
	   "telephone": ""
    	}
    };
}


function paymentETRTransaction(){
	  return {
	  "agent_code" :"",
	  "source_of_data":"",
	  "payor_name":"",
	  "payor_email":"",
	  "mobile" : "",
	  "payment_date":"",
	  "pay_for":"",
	  "payor_role":"",
	  "product_id":"",
	  "application_no":"",
	  "application_date":"",
	  "policy_holder":"",
	  "DOB_policy_holder":"",
	  "sum_assured":"",
	  "premium":"",
	  "cash_payment":"",
	  "cash_payment":"",
	  "cheques": [],
	  "credit_cards": [],
	  "transfers": []
    };
}



function PaymentCard(){
	  return {
	   "PaymentInfo": { 	 
		   "gateway": "",
		   "method": "",
		   "mode": "",
		   "company": "",
		   "invoice": "",
		   "product_title": "",
		   "company": "",
		   "references": [],
		   "amount": "",
		   "callback": "",
	    	}
	    };
	}


// Moved from controllers to avoid repeatition
 function checkLength(data, successcallback) {
        if (data && data != "") {
            if (JSON.parse(data).selectedArr.length > 0) {
                successcallback(true);
            }
            else {
                successcallback(false);
            }
        }
        else {
            successcallback(false);
        }
    };


/* convert base64string to blob */
var base64toBlob = function(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i=0; i<slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
}