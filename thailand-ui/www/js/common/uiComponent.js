/*
 *Copyright 2015, LifeEngage 
 */


/*Older version of paintui which does not store the html in the local storage- retaining it for reference*/
function paintUILocal(templateName, jsonFile, jsonPortion, divElement,
		callback, $scope, $compile) {

	$.get(templateName, function(template) {
		var source = template;
		var template = Handlebars.compile(source);
		$.ajax({
			type : "GET",
			url : jsonFile,
			datatype : "JSON",
			success : function(json) {
				var json = JSON.parse(json);
				var eSalesForm = $(divElement);
				if (jsonPortion == "") {
					eSalesForm.html((template(json)));
					$compile(eSalesForm)($scope);
				} else {
					$.each(json.Views, function(i, item) {
						if (item.id.toString() == jsonPortion.toString()) {
							eSalesForm.html((template(item)));
						}
					});
					$compile(eSalesForm)($scope);
				}
				$scope.$apply();

				if (callback) {
					$scope.callback();
				}
			},
			error : function(error) {
				alert("error");
				alert(error.message);
			}
		});
	});

}

function paintUIFromArray(templateName, jsonArray, jsonPortion, divElement,
		callback, $scope, $compile) {

	for ( var i = jsonArray.length; i > 0; i--) {
		var isPainted = false;

		$.when(
				paintUI(templateName, jsonArray[i - 1], jsonPortion,
						divElement, callback, $scope, $compile)).done(
				function(data) {
					isPainted = data;

				});
		if (isPainted) {
			break;
		}
	}
}

function paintUI(templateName, jsonFile, jsonPortion, divElement, callback,
		successCallback, $scope, $compile) {

	var dfd = jQuery.Deferred();

	var uniqueKey = 'eAppPage_' + jsonPortion + divElement;
	var uiControls = 'eAppConfig_' + jsonFile;
	if (rootConfig.isDebug) {
		window.localStorage.clear();
		for (key in localStorage) {
			delete localStorage[key];
		}
	}
	if (!localStorage[uiControls]) {
		getFromLocal(jsonFile, false);
	}

	if (!localStorage[uniqueKey]) {

		$
				.get(
						templateName,
						function(template) {
							var compiledTemplate;
							if (rootConfig.isDebug) {

								var source = template;
								if (!localStorage["eAppPrecompiledHandlebarTemplate"]) {
									eval("var template = "
											+ Handlebars.precompile(source));
									localStorage["eAppPrecompiledHandlebarTemplate"] = template;
								}
								if (templateName == "eAppTemplate.html") {
									compiledTemplate = Handlebars
											.compile(source);
								} else {
									eval("var templateFunction = "
											+ localStorage["eAppPrecompiledHandlebarTemplate"]);
									compiledTemplate = Handlebars
											.template(templateFunction);
								}
							} else {
								compiledTemplate = Handlebars.templates['eAppPreCompiledTemplate'];
							}
							var json = JSON.parse(localStorage[uiControls]);
							var eSalesForm = $(divElement);
							if (jsonPortion == "") {
								eSalesForm.html((template(json)));
								$compile(eSalesForm)($scope);
							} else {
								$
										.each(
												json.Views,
												function(i, item) {
													if (item.id.toString() == jsonPortion
															.toString()) {
														localStorage[uniqueKey] = "";
														localStorage[uniqueKey] = compiledTemplate(item);
														eSalesForm
																.html((localStorage[uniqueKey]));
													}
													if (item.templateID == "NavigationTemplate") {
														localStorage["NavigationTabs"] = JSON
																.stringify(item.tabs);
														$scope.tabs = item.tabs;
													}
												});
								$compile(eSalesForm)($scope);
							}
							$scope.refresh();
							if (callback) {
								successCallback(jsonPortion);
							}
							dfd.resolve();

						});
	} else {
		var eSalesForm = $(divElement);
		eSalesForm.html((localStorage[uniqueKey]));
		$compile(eSalesForm)($scope);
		$scope.tabs = JSON.parse(localStorage["NavigationTabs"]);
		$scope.refresh();
		if (callback) {
			successCallback(jsonPortion);
		}
		dfd.resolve();

	}

	return dfd.promise();
}

function getFromRemote(appName, callmode) {
	var uiControls = 'eAppConfig_' + appName;
	var tempLocalStarageKeys = [];
	$.ajax({
		type : "GET",
		url : 'http://174.129.3.17/FNACalculator/KLIeAppNewUI/' + appName,
		dataType : "JSON",
		success : function(json) {
			localStorage[uiControls] = JSON.stringify(json);
			for ( var i = 0; i < localStorage.length; i++) {
				var propertyName = localStorage.key(i);
				if (/KLIeAppPage_/.test(propertyName)) {
					tempLocalStarageKeys.push(propertyName);
				}
			}
			for ( var i = 0; i < tempLocalStarageKeys.length; i++) {
				localStorage.removeItem(tempLocalStarageKeys[i]);
			}
			if (typeof refreshUI == 'function') {
				refreshUI();
			}
		},
		async : callmode,
		error : function(error) {
			alert("Error during Refresh...");
		}
	});
}

function getFromLocal(appName, callmode) {
	var uiControls = 'eAppConfig_' + appName;
	$.ajax({
		type : "GET",
		url : appName,
		dataType : "JSON",
		success : function(json) {
			localStorage[uiControls] = JSON.stringify(json);
		},
		async : callmode,
		error : function(error) {
			alert("Error during Refresh...");
		}
	});
}