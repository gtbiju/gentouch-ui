/*
 *Copyright 2015, LifeEngage 
 */

/* GLI_Filter module - GLI specific filter
 * Filters required for iterations using ng-repeat/ng-options
 */
var filters = angular.module('lifeEngage.gliFilter', []);

/*
 * KMC advance filter
 */
filters
		.filter(
				'KMCfilter',
				function() {
					return function(input, leadName, contactNumber, source,
							nextMeetingDate,status) {
						source = source.replace(/ +/g, "");	
						if(typeof status != "undefined" && status != null){
						status = status.replace(/ +/g, "");
						}
						var out = [];
						var formattedDate = "";
						if(typeof input != "undefined"){
						for ( var i = 0; i < input.length; i++) {
							input[i].Key5 = input[i].Key5.replace(/ +/g, "");
							input[i].Key15 = input[i].Key15.replace(/ +/g, "");
							formattedDate = ((input[i].Key19).split(" "))[0];

							if (((leadName == "" || leadName == undefined) || ((/^[\u0E00-\u0E7Fa-zA-Z' ]*$/.test(leadName))
									&&  (input[i].Key2 != undefined && input[i].Key26 != undefined && input[i].Key2 +' '+ input[i].Key26)
									.search(new RegExp(leadName, "i")) > -1))
									&& ((/^\d+$/.test(contactNumber) && (contactNumber == "" || contactNumber == undefined)) || (input[i].Key4 != undefined && input[i].Key4
											.search(contactNumber) > -1))
									&& ((source == "" || source == undefined) || (input[i].Key5 != undefined && source == input[i].Key5))
									&& ((status == "" || status == undefined) || (input[i].Key15 != undefined && status == input[i].Key15))
									&& ((nextMeetingDate == "" || nextMeetingDate == undefined) || (formattedDate == nextMeetingDate))) {

								out.push(input[i]);
							}
						}
					}
						return out;
					};
				});

filters.filter('leadStatusFilter', function() {
	return function(input, searchLead, searchKey) {
		var out = [];
		if(typeof input != "undefined"){
		for ( var i = 0; i < input.length; i++) {
			if ((searchKey == "" || searchKey == undefined) || (input[i].Key2 != undefined && input[i].Key2.search(new RegExp(searchKey, "i")) > -1) || (input[i].Key4 != undefined && input[i].Key4.search(new RegExp(searchKey, "i")) > -1)|| (input[i].Key26 != undefined && input[i].Key26.search(new RegExp(searchKey, "i")) > -1)) {
				if ((searchLead == "" || searchLead == undefined) || (input[i].Key15 != undefined && input[i].Key15.search(new RegExp(searchLead, "i")) > -1)) {
					out.push(input[i]);
				} else if ((searchLead == "" || searchLead == undefined)|| (input[i].Key15 != undefined && "Open"
								.search(new RegExp(searchLead, "i")) > -1)) {
					var openStatusArr = rootConfig.lmsOpenStatus.split(",");
					for ( var j = 0; j < openStatusArr.length; j++) {
						if (input[i].Key15.search(new RegExp(openStatusArr[j],
								"i")) > -1) {
							out.push(input[i]);
						}
					}
				}
			}
		}
	}
		return out;
	};
});

   filters.filter("localeOrderBy", [function () {
   
        return function (array, sortPredicate, reverseOrder) {
            if (!Array.isArray(array)) return array;
            if (!sortPredicate) return array;

            var isString = function (value) {
                return (typeof value === "string");
            };

            var isNumber = function (value) {
                return (typeof value === "number");
            };

            var isBoolean = function (value) {
                return (typeof value === "boolean");
            };

            var arrayCopy = [];
            angular.forEach(array, function (item) {
                arrayCopy.push(item);
            });
			var alphabet, enumeration, comparator, mixedChars, i, c;

			alphabet = ['a','A','à','À','á','Á','ả','Ả','ã','Ã','ạ','Ạ','ă','Ă','ằ','Ằ','ắ','Ắ','ẳ','Ẳ','ẵ','Ẵ','ặ','Ặ','â','Â','ầ','Ầ','ấ','Ấ','ẩ','Ẩ','ẫ','Ẫ','ậ','Ậ','b','B','c','C','d','D','đ','Đ','e','E','è','È','é','É','ẻ','Ẻ','ẽ','Ẽ','ẹ','Ẹ','ê','Ê','ề','Ề','ế','Ế','ể','Ể','ễ','Ễ','ệ','Ệ','f','F','g','G','h','H','i','I','ì','Ì','í','Í','ỉ','Ỉ','ĩ','Ĩ','ị','Ị','j','J','k','K','l','L','m','M','n','N','o','O','ò','Ò','ó','Ó','ỏ','Ỏ','õ','Õ','ọ','Ọ','ô','Ô','ồ','Ồ','ố','Ố','ổ','Ổ','ỗ','Ỗ','ộ','Ộ','ơ','Ơ','ờ','Ờ','ớ','Ớ','ở','Ở','ỡ','Ỡ','ợ','Ợ','p','P','q','Q','r','R','s','S','t','T','u','U','ù','Ù','ú','Ú','ủ','Ủ','ũ','Ũ','ụ','Ụ','ư','Ư','ừ','Ừ','ứ','Ứ','ử','Ử','ữ','Ữ','ự','Ự','v','V','w','W','x','X','y','Y','ỳ','Ỳ','ý','Ý','ỷ','Ỷ','ỹ','Ỹ','ỵ','Ỵ','z','Z','0','1','2','3','4','5','6','7','8','9','-',':',' ',','];

			enumeration = {};

			for (i = 0; i < alphabet.length; i += 1) {
				c = alphabet[i];
				enumeration[c] = i;
			}

			comparator = function (a, b) {
				var j, k, d, x, y;

				k = Math.min(a.length, b.length);
				for (j = 0; j < k; j += 1) {
					x = a[j];
					y = b[j];
					d = enumeration[x] - enumeration[y];
					if (0 !== d) {
						return d;
					}
				}

				if (j < a.length) {
					return 1;
				}

				if (j < b.length) {
					return -1;
				}

				return 0;
			};


            arrayCopy.sort(function (a, b) {
                var valueA = a[sortPredicate];
                var valueB = b[sortPredicate];

                if (isString(valueA))
                    /* return !reverseOrder ? valueA.localeCompare(valueB) : valueB.localeCompare(valueA); */
                    /* return !reverseOrder ? new Intl.Collator('vi').compare(valueA, valueB) : new Intl.Collator('vi').compare(valueB, valueA); */
                    return !reverseOrder ? comparator(valueA, valueB) : comparator(valueB, valueA);
					

                if (isNumber(valueA) || isBoolean(valueA))
                    return !reverseOrder ? valueA - valueB : valueB - valueA;

                return 0;
            });

            return arrayCopy;
        }
    }]);
	
filters.filter('dashBoardFilter', function() {
	return function(input, searchLead, searchKey) {
		var out = [];
		for ( var i = 0; i < input.length; i++) {
			if ((searchKey == "" || searchKey == undefined) 
					|| (input[i].Key6 != undefined && input[i].Key6
							.search(new RegExp(searchKey, "i")) > -1)
					|| (input[i].Key26 != undefined && input[i].Key26
									.search(new RegExp(searchKey, "i")) > -1)
					|| (input[i].Key4 != undefined && input[i].Key4
							.search(new RegExp(searchKey, "i")) > -1)
					|| (input[i].Key8 != undefined &&  input[i].Key8.search(new RegExp(searchKey, "i")) > -1)){
				if ((searchLead == "" || searchLead == undefined)
						|| (input[i].Key15 != undefined && input[i].Key15
								.search(new RegExp(searchLead, "i")) > -1)) {
					out.push(input[i]);
				}
			}
		}
		return out;
	};
});

filters.filter('gliListingFilter', function(){
    return function(input, searchKey){
        var output = [];
        if(input !=undefined){
        	for(var i = 0; i < input.length; i++){
                if(input[i].Type == 'LMS'){
                if(((searchKey == "" || searchKey == undefined) || (input[i].Key2 != undefined &&  input[i].Key2.search(new RegExp(searchKey,"i")) > -1)) || ((searchKey == "" || searchKey == undefined) || (input[i].Key4 != undefined &&  input[i].Key4.search(new RegExp(searchKey,"i")) > -1))){
                    output.push(input[i]);
                } 
    			}else{
                    //if(((searchKey == "" || searchKey == undefined) || (input[i].Key6 != undefined &&  input[i].Key6.search(new RegExp(searchKey,"i")) > -1)) || ((searchKey == "" || searchKey == undefined) || (input[i].Key8 != undefined &&  input[i].Key8.search(new RegExp(searchKey,"i")) > -1)) || ((searchKey == "" || searchKey == undefined) || (input[i].Key26 != undefined &&  input[i].Key26.search(new RegExp(searchKey,"i")) > -1)) || ((searchKey == "" || searchKey == undefined) || (input[i].Key3 != undefined &&  input[i].Key3.search(new RegExp(searchKey,"i")) > -1))){
    				if(((searchKey == "" || searchKey == undefined) || (( /^[\u0E00-\u0E7Fa-zA-Z' ]*$/.test(searchKey)) &&  input[i].Key6 != undefined &&  input[i].Key6.search(new RegExp(searchKey,"i")) > -1)) || ((searchKey == "" || searchKey == undefined) || (input[i].Key8 != undefined &&  input[i].Key8.search(new RegExp(searchKey,"i")) > -1)) || ((searchKey == "" || searchKey == undefined) || (( /^[\u0E00-\u0E7Fa-zA-Z' ]*$/.test(searchKey)) && input[i].Key26 != undefined &&  input[i].Key26.search(new RegExp(searchKey,"i")) > -1)) || ((searchKey == "" || searchKey == undefined) )){
    				output.push(input[i]);
                }
            }
            
        }
        }
	return output;
	};
});

filters.filter('eappfilter', function($filter){
    return function(input, searchKey){
		function dateconvert(date){
			var d;
			var t = date.split(/[- :]/);
			var dateVal = 543;
			if (t.length == 3) {
				var d = new Date(parseInt(t[0]) + dateVal, t[1] - 1, t[2]); // yyyy-MM-dd
			} else if (t.length == 5) {
				var d = new Date(parseInt(t[0]) + dateVal, t[1] - 1, t[2], t[3], t[4]); //yyyy-MM-dd HH:mm
			} else if (t.length == 6) {
				var d = new Date(parseInt(t[0]) + dateVal, t[1] - 1, t[2], t[3], t[4], t[5]); //yyyy-MM-dd HH:mm:ss
			}
			var newDate = $filter('date')(new Date(d),'dd/MM/yyyy');
			return(newDate);
		}
        var output = [];
        if(typeof input != "undefined"){
        for(var i = 0; i < input.length; i++){
            if(input[i].Type == 'eApp'){
            if(((searchKey == "" || searchKey == undefined) || (input[i].Key6 != undefined &&  input[i].Key6.search(new RegExp(searchKey,"i")) > -1)) || ((searchKey == "" || searchKey == undefined) || (input[i].Key21 != undefined &&  input[i].Key21.search(new RegExp(searchKey,"i")) > -1)) ||((searchKey == "" || searchKey == undefined) || (dateconvert(input[i].Key13) != undefined &&  dateconvert(input[i].Key13).search(new RegExp(searchKey,"i")) > -1)) || ((searchKey == "" || searchKey == undefined) || (input[i].Key38 != undefined &&  input[i].Key38.search(new RegExp(searchKey,"i")) > -1))){
                output.push(input[i]);
			}
			}
		}
	return output;
	};
    }});


filters.filter('potentialFilter', function(){
    return function(input, potential){
        var output = [];
        if(input !=undefined){
        	for(var i = 0; i < input.length; i++){
                if(((potential == "" || potential == undefined) || (input[i].Key18 != undefined &&  input[i].Key18.search(new RegExp(potential,"i")) > -1)) ){
                    output.push(input[i]);
                } 
    			
            
        }
        }
	return output;
	};
});

filters.filter('listingStatusFilter', function(){
    return function(input, statusFilter){
        var output = [];
        if(input !=undefined){
        	for(var i = 0; i < input.length; i++){
        	if ((statusFilter != "" && statusFilter != undefined && "Follow Up"
							.search(new RegExp(statusFilter, "i")) > -1)) {
				var followUpStatusList = rootConfig.followUpStatusList;
				for ( var j = 0; j < followUpStatusList.length; j++) {
					if (input[i].Key15.search(new RegExp(followUpStatusList[j],
							"i")) > -1) {
						output.push(input[i]);
					}
				}
			} else {
        			if(((statusFilter == "" || statusFilter == undefined) || (input[i].Key15 != undefined &&  
        					input[i].Key15.search(new RegExp(statusFilter,"i")) > -1)) ){
        				output.push(input[i]);
        			} 
    		    }
        	}
        }
	return output;
	};
});
/*
 * FNA changes by LE Team >>> added filter logic for new field  - Starts
 */
filters.filter('gliFilterFNAList', function() {
			return function(input, name, dob, creationDate, statusFilter,fnaID) {
				var out = [];
				if(dob){
					dob = formatForDateControl(dob);
				}
				if(creationDate){
					creationDate = formatForDateControl(creationDate);
				}
				if (input) {
						for ( var i = 0; i < input.length; i++) {
                            var createdDate=formatForDateControl(input[i].Key13);
							if(((name == "" || name == undefined) || (input[i].Key6 != undefined && input[i].Key6.search(new RegExp(name, "i")) > -1)) && ((dob == "" || dob == undefined) || (input[i].Key7 != undefined && input[i].Key7 == dob)) && ((creationDate == "" || creationDate == undefined) || (createdDate != undefined && createdDate == creationDate)))
							{
								out.push(input[i]);
							}
						}
				}
				return out;
			};
});

/*
 * FNA changes by LE Team >>> added filter logic for new field  - Ends
 */
/*
 * Filter for adding commas between numbers in Illustration Output Screen
 */

filters.filter('formattedNumber', function($filter) {
			return function(input) {
				if (isNaN(input)) {
					return input;
				} else {
					if(input!="") {
						return $filter('number')(input);
					}
					else {
						return input;
					}
				};
			};
});

filters.filter('formattedNumberIllustration', function($filter) {
			return function(input) {
				if (isNaN(input)) {
					return input;
				} else {
					if(input!="") {
						if(input == 0)
						{
							input = '';
							return input;
						}
						else
						{
							return $filter('number')(input);
						}						
					}
					else {
						if(input == 0)
						{
							input = '';
							return input;
						}
						else
						{
							return input;
						}
					}
				};
			};
});

/*
 * Filter for adding commas between premium amount in Illustration Output Screen
 */
filters.filter('formattedNumberForString', function($filter) {
			return function(input) {
			if(typeof(input) != 'undefined'){
            	input = input.toString();
            }
				if (isNaN(input)) {
					return input;
				} else {

					if(input!="") {;
						return $filter('number')(input);
					}
					else {
						return input;
					}
				};
			};
});

filters.filter('dynamicProposalListingFilter', function(){
    return function(input, searchKey , dynamicSearchKey){
        var output = [];
        if(typeof input != "undefined"){
        for(var i = 0; i < input.length; i++){
        	if(dynamicSearchKey == 'Proposal number'){
        		if((searchKey == "" || searchKey == undefined) || (input[i].Key21 != undefined &&  (input[i].Key21).toString().search(new RegExp(searchKey,"i")) > -1)){
        			output.push(input[i]);
        		}
        	} else if(dynamicSearchKey == 'Policyholder Name'){
        		if((searchKey == "" || searchKey == undefined) || (input[i].Key6 != undefined &&  input[i].Key6.search(new RegExp(searchKey,"i")) > -1)){
        			output.push(input[i]);
        		}
        	} else if(dynamicSearchKey == 'Total Premium'){
        		if((searchKey == "" || searchKey == undefined) || (input[i].Key23 != undefined &&  (input[i].Key23).toString().search(new RegExp(searchKey,"i")) > -1)){
        			output.push(input[i]);
        		}
        	}
    }
    }
	return output;
	};
});

/*
 * Filter for populating others section in eApp
 */
filters.filter('requirementFilterOthers', function(){
	return function(input,key7Val){
		var out = [];
		if (!jQuery.isEmptyObject(input) && input != "undefined" && input && input.length) {
				for ( var i = 0; i < input.length; i++) {
					if ((input[i].partyIdentifier == "") && (input[i].requirementType != "Signature") && ( key7Val !="Pending") && (key7Val !="PendingUploaded")){
					out.push(input[i]);
				}else if((input[i].partyIdentifier == "") && (input[i].requirementType != "Signature") && ( key7Val =="Pending" || key7Val =="PendingUploaded")){
				   if(input[i].isMandatory =="Yes"){
				     input[i].isMandatory ="No";
				   }
				   input[i].uploadStatus = input[i].Documents[0].documentUploadStatus;
				   out.push(input[i]);
				}
				}				
			}
			return out;
	};
});
filters.filter('sliceString',function(){
    return function(input,sliceStartIndex,sliceEndIndex){
         if (!input || !input.length) { return; }
			return input.slice(sliceStartIndex,sliceEndIndex)
    }
}
);

filters
.filter(
		'AdvancedLeadSearch',
		function() {
			return function(input, leadName, lastName, contactNumber, source,
					nextMeetingDate,action,filterPotential,nextAction,referenceNameFilter) {
				if(source != null && source != "" && source != undefined){
				source = source.replace(/ +/g, "");
				}
				var out = [];
				var formattedDate = "";
				if(typeof input != "undefined"){
				for ( var i = 0; i < input.length; i++) {
					input[i].Key5 = input[i].Key5.replace(/ +/g, "");
					formattedDate = ((input[i].Key19).split(" "))[0];

					if (((leadName == "" || leadName == undefined) || ((/^[a-zA-ZÁÀẠẢÃáàạảãẤẦẬẨẪấầậẩẫẮẰẶẲẴắằặẳẵÉÈẸẺẼéèẹẻẽẾỀỆỂỄếềệểễÍÌỊỈĨíìịỉĩÓÒỌỎÕóòọỏõỐỒỘỔỖốồộổỗỚỜỢỞỠớờợởỡÚÙỤỦŨúùụủũỨỪỰỬỮứừựửữÝỲỴỶỸýỳỵỷỹĐđÂĂÊÔƠƯâăêơôư' ]*$/ 
							.test(leadName))
							&& input[i].Key2 != undefined && input[i].Key2
							.search(new RegExp(leadName, "i")) > -1))
							&&  ((lastName == "" || lastName == undefined) || ((/^[a-zA-ZÁÀẠẢÃáàạảãẤẦẬẨẪấầậẩẫẮẰẶẲẴắằặẳẵÉÈẸẺẼéèẹẻẽẾỀỆỂỄếềệểễÍÌỊỈĨíìịỉĩÓÒỌỎÕóòọỏõỐỒỘỔỖốồộổỗỚỜỢỞỠớờợởỡÚÙỤỦŨúùụủũỨỪỰỬỮứừựửữÝỲỴỶỸýỳỵỷỹĐđÂĂÊÔƠƯâăêơôư' ]*$/ 
							.test(lastName))
							&& input[i].Key3 != undefined && input[i].Key3
							.search(new RegExp(lastName, "i")) > -1))
							&& ((/^\d+$/.test(contactNumber) && (contactNumber == "" || contactNumber == undefined)) || (input[i].Key4 != undefined && input[i].Key4
									.search(contactNumber) > -1))
							&& ((source == "" || source == undefined) || (input[i].Key5 != undefined && source == input[i].Key5))
							&& ((nextAction == "" || nextAction == undefined) || (input[i].Key22 != undefined && nextAction == input[i].Key22))
							&& ((action == "" || action == undefined) || (input[i].Key21 != undefined && action == input[i].Key21))
							&& ((filterPotential == "" || filterPotential == undefined) || (input[i].Key18 != undefined && filterPotential == input[i].Key18))
							&& ((nextMeetingDate == "" || nextMeetingDate == undefined) || (formattedDate == nextMeetingDate))
							&& ((referenceNameFilter == "" || referenceNameFilter == undefined) || ((/^[a-zA-ZÁÀẠẢÃáàạảãẤẦẬẨẪấầậẩẫẮẰẶẲẴắằặẳẵÉÈẸẺẼéèẹẻẽẾỀỆỂỄếềệểễÍÌỊỈĨíìịỉĩÓÒỌỎÕóòọỏõỐỒỘỔỖốồộổỗỚỜỢỞỠớờợởỡÚÙỤỦŨúùụủũỨỪỰỬỮứừựửữÝỲỴỶỸýỳỵỷỹĐđÂĂÊÔƠƯâăêơôư' ]*$/ 
									.test(referenceNameFilter))
									&& input[i].Key6 != undefined && input[i].Key6
									.search(new RegExp(referenceNameFilter, "i")) > -1 ))) {

						out.push(input[i]);
					}
				}
			}
				return out;
			};
		});
//filter to filter out the documents		
filters.filter('requirementFilterDoc', function() {
	return function(input, uuid, key7Val) {
		var out = [];
		if (!jQuery.isEmptyObject(input) && input != "undefined" && input && input.length) {
			for ( var i = 0; i < input.length; i++) {
				if(input[i].requirementType == "Memo"){
					if(uuid.indexOf(input[i].partyIdentifier)>-1){
						out.push(input[i]);
					}	
				}else{
					if ((input[i].partyIdentifier == uuid) && (key7Val !="Pending") && (key7Val !="PendingUploaded")) {
						out.push(input[i]);
					}else if((input[i].partyIdentifier == uuid) && ( key7Val =="Pending" || key7Val =="PendingUploaded")){
					   if(input[i].isMandatory =="Yes"){
						 input[i].isMandatory ="No";
					   }
					   //input[i].uploadStatus = input[i].Documents[0].documentUploadStatus;
					   out.push(input[i]);
					}
				}
			}
		}
		return out;
	};
});

//Filter to find the memo eapps alone
filters.filter('memoFilter', function() {
	return function(input, searchStatus) {
		var out = [];
		for ( var i = 0; i < input.length; i++) {
			if ((searchStatus == "" || searchStatus == undefined) || (input[i].Key30 != undefined && parseInt(input[i].Key30)>0)) {
					out.push(input[i]);
			}
		}
		
		
		return out;
	};
});

//Filter to filter the payment option for branch admin
filters.filter('branchPaymentFilter', function() {
	return function(input,checkValue,status) {
		var out = [];
		for ( var i = 0; i < input.length; i++) {
			if(status==true){
				if(checkValue.indexOf(input[i].code)>-1){
					out.push(input[i]);
				}
			}else{
				out.push(input[i]);
			}
		}
		return out;
	};
});

//Filtering eapp based on status and seach from dashboard
filters.filter('DynamicEappfilter', function($filter){
    return function(input,searchValue,searchKey){
        var output = [];
        if(typeof input != "undefined"){
        for(var i = 0; i < input.length; i++){
            if(input[i].Type == 'eApp'){
				if(((searchKey == "" || searchKey == undefined) || (input[i].Key6 != undefined &&  input[i].Key6.search(new RegExp(searchKey,"i")) > -1))){
					if(searchValue==rootConfig.statusCountListeAppMemo){
						if ((searchValue == "" || searchValue == undefined) || (input[i].Key30 != undefined && parseInt(input[i].Key30)>0)) {
							output.push(input[i]);
						}
					}else if(searchValue=="Submitted"){
						if ((searchValue == "" || searchValue == undefined) || (input[i].Key15 != undefined && input[i].Key15!="Pending Submission")) {
							output.push(input[i]);
						}
					}else{
						if ((searchValue == "" || searchValue == undefined) || (input[i].Key15 != undefined && input[i].Key15.search(new RegExp(searchValue, "i")) > -1)) {
							output.push(input[i]);
						}
					}
				}
			}
		}
		return output;
	};
}});
