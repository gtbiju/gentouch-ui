/*
 *Copyright 2015, LifeEngage 
 */



/*  Filtermodule
 * Filters required for iterations using ng-repeat/ng-options
 */
var filters = angular.module('lifeEngage.filters', []);

filters
		.filter(
				'filterSearch',
				function() {
					return function(input, leadName, contactNumber, source,
							subSource, agentName, leadId) {

						var out = [];
						for ( var i = 0; i < input.length; i++) {

							if (((leadName == "" || leadName == undefined) || (input[i].Key2 != undefined && input[i].Key2
									.search(new RegExp(leadName, "i")) > -1))
									&& ((contactNumber == "" || contactNumber == undefined) || (input[i].Key4 != undefined && input[i].Key4
											.search(contactNumber) > -1))
									&& ((source == "" || source == undefined) || (input[i].Key5 != undefined && source == input[i].Key5))
									&& ((subSource == "" || subSource == undefined) || (input[i].Key6 != undefined && subSource == input[i].Key6))
									&& ((agentName == "" || agentName == undefined) || (input[i].Key11 != undefined && input[i].Key11
											.search(new RegExp(agentName, "i")) == 0)) && ((leadId == "" || leadId == undefined) || (input[i].Key1 != undefined && input[i].Key1
											.search(new RegExp(
													leadId, "i")) > -1))) {

								out.push(input[i]);
							}
						}
						return out;
					};
				});

filters
		.filter(
				'illustrationFilter',
				function() {
					return function(input, insuredName, PayerName, prodName,
							prodType, statusOption,illustrationId) {

						var out = [];
						for ( var i = 0; i < input.length; i++) {
							if (((prodType == "" || prodType == undefined) || (input[i].TransactionData.Product.ProductDetails.productType != undefined && prodType == input[i].TransactionData.Product.ProductDetails.productType))
									&& ((prodName == "" || prodName == undefined) || (input[i].TransactionData.Product.ProductDetails.productName != undefined && input[i].TransactionData.Product.ProductDetails.productName
											.search(new RegExp(prodName, "i")) > -1))
									&& ((insuredName == "" || insuredName == undefined) || (input[i].TransactionData.Insured != undefined
											&& input[i].TransactionData.Insured.BasicDetails.firstName != undefined && input[i].TransactionData.Insured.BasicDetails.firstName
											.search(new RegExp(insuredName, "i")) > -1))
									&& ((PayerName == "" || PayerName == undefined) || (input[i].TransactionData.Payer != undefined
											&& input[i].TransactionData.Payer.BasicDetails.firstName != undefined && input[i].TransactionData.Payer.BasicDetails.firstName
											.search(new RegExp(PayerName, "i")) > -1))
									&& ((statusOption == "" || statusOption == undefined) || (input[i].Key15 != undefined && statusOption == input[i].Key15)) && ((illustrationId == "" || illustrationId == undefined) || (input[i].Key3 != undefined && input[i].Key3
											.search(new RegExp(
													illustrationId, "i")) > -1))) {
								out.push(input[i]);
							}
						}
						return out;
					};
				});
filters
		.filter(
				'basicIllustrationFilter',
				function() {
					return function(input, searchIllustrationKey) {

						var out = [];
						for ( var i = 0; i < input.length; i++) {

							if (((searchIllustrationKey == "" || searchIllustrationKey == undefined) || (input[i].TransactionData.Insured != undefined
									&& input[i].TransactionData.Insured.BasicDetails.firstName != undefined && input[i].TransactionData.Insured.BasicDetails.firstName
									.search(new RegExp(searchIllustrationKey,
											"i")) > -1))
									|| ((searchIllustrationKey == "" || searchIllustrationKey == undefined) || (input[i].Key3 != undefined
											&& input[i].Key3 != undefined && input[i].Key3
											.search(new RegExp(
													searchIllustrationKey, "i")) > -1))) {

								out.push(input[i]);
							}
						}
						return out;
					};
				});
filters
		.filter(
				'basicProposalFilter',
				function() {
					return function(input, searchProposalKey) {

						var out = [];
						for ( var i = 0; i < input.length; i++) {

							if (((searchProposalKey == "" || searchProposalKey == undefined) || (input[i].TransactionData.Proposer != undefined
									&& input[i].TransactionData.Proposer.BasicDetails.firstName != undefined && input[i].TransactionData.Proposer.BasicDetails.firstName
									.search(new RegExp(searchProposalKey, "i")) > -1))
									|| ((searchProposalKey == "" || searchProposalKey == undefined) || (input[i].Key4 != undefined
											&& input[i].Key4 != undefined && input[i].Key4
											.search(new RegExp(
													searchProposalKey, "i")) > -1))) {

								out.push(input[i]);
							}
						}
						return out;
					};
				});

filters
		.filter(
				'proposalFilter',
				function() {
					return function(input, insuredName, proposerName, prodName,
							prodType, statusOption,proposalId) {

						
						var out = [];
						for ( var i = 0; i < input.length; i++) {
							if (((prodType == "" || prodType == undefined) || ( input[i].TransactionData.Product.ProductDetails && input[i].TransactionData.Product.ProductDetails.productType != undefined && prodType == input[i].TransactionData.Product.ProductDetails.productType))
									&& ((prodName == "" || prodName == undefined) || ( input[i].TransactionData.Product.ProductDetails && input[i].TransactionData.Product.ProductDetails.productName != undefined && input[i].TransactionData.Product.ProductDetails.productName
											.search(new RegExp(prodName, "i")) > -1))
									&& ((insuredName == "" || insuredName == undefined) || (input[i].TransactionData.Insured != undefined
											&& input[i].TransactionData.Insured.BasicDetails.firstName != undefined && input[i].TransactionData.Insured.BasicDetails.firstName
											.search(new RegExp(insuredName, "i")) > -1))
									&& ((proposerName == "" || proposerName == undefined) || (input[i].TransactionData.Proposer != undefined
											&& input[i].TransactionData.Proposer.BasicDetails.firstName != undefined && input[i].TransactionData.Proposer.BasicDetails.firstName
											.search(new RegExp(proposerName,
													"i")) > -1))
									&& ((statusOption == "" || statusOption == undefined) || (input[i].Key15 != undefined && statusOption == input[i].Key15)) && ((proposalId == "" || proposalId == undefined) || (input[i].Key4 != undefined && input[i].Key4
											.search(new RegExp(
													proposalId, "i")) > -1))) {
								out.push(input[i]);
							}
						}
						return out;
					};
				});

filters
		.filter(
				'basicFilter',
				function() {
					return function(input, searchLead) {

						var out = [];
						for ( var i = 0; i < input.length; i++) {

							if (((input[i].Key2 != "" || input[i].Key2 != undefined) && (input[i].Key2 != undefined && input[i].Key2
									.search(new RegExp(searchLead, "i")) > -1))
									|| ((input[i].Key4 != "" || input[i].Key4 != undefined) && (input[i].Key4 != undefined && input[i].Key4
											.search(searchLead) > -1))
									|| ((input[i].Key1 != "" || input[i].Key1 != undefined) && (input[i].Key1 != undefined && input[i].Key1
											.search(new RegExp(
													searchLead, "i")) > -1))) {

								out.push(input[i]);
							}
						}
						return out;
					};
				});
filters
		.filter(
				'familyFilter',
				function() {
					return function(input) {

						var out = [];
						for ( var i = 0; i < input.length; i++) {

							if (input[i].id != "myself") {

								out.push(input[i]);
							}
						}
						return out;
					};
				});
filters
		.filter(
				'ageFilter',
				function() {
							return function(dob) {

							if (dob != "" && dob != undefined && dob != null) {
								var dobDate = new Date(dob);
								/*
									* In I.E it will accept date format in '/' separator only
								*/
								if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
									dobDate = new Date(dob.split("-").join("/"));
								}
								var todayDate = new Date();
								var yyyy = todayDate.getFullYear().toString();
								var mm = (todayDate.getMonth() + 1).toString();
								// getMonth() is
								// zero-based
								var dd = todayDate.getDate().toString();
								var formatedDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
								if (dobDate > todayDate) {
									age = 0;
								}
								else {
									var age = todayDate.getFullYear() - dobDate.getFullYear();
									if (todayDate.getMonth() < dobDate.getMonth() - 1 || (dobDate.getMonth() - 1 === todayDate.getMonth() && todayDate.getMonth() < dobDate.getDate())) {
									age--;
									}
									if (dob == undefined || dob == "") {
										age = "-";
									}
								//angular.element('#' + id + 'Summary').text(age);
								}
								
							}
							return age;
						};
						
					
				});

filters.filter('referralFilter', function() {
	return function(input, parentLeadId) {

		var out = [];
		for ( var i = 0; i < input.length; i++) {

			if ((input[i].Key8 != "" && input[i].Key8 != undefined)
					&& (input[i].Key8 == parentLeadId)) {

				out.push(input[i]);
			}
		}
		return out;
	};
});

filters.filter('filterDependant', function() {
	return function(input, dependant) {
		var out = [];
		if (!jQuery.isEmptyObject(input) && input != "undefined" && input
				&& input.length) {
			for ( var i = 0; i < input.length; i++) {
				if (input[i].Dependant == dependant) {
					out.push(input[i]);
				}
			}
		}
		return out;
	};
});

filters
		.filter(
				'filterFNAList',
				function() {
					return function(input, name, dob, statusFilter,fnaID) {
						var out = [];
						if (input) {
								for ( var i = 0; i < input.length; i++) {
								
				if (((name == "" || name == undefined) || (input[i].TransactionData.parties[0].BasicDetails.firstName != undefined && input[i].TransactionData.parties[0].BasicDetails.firstName
									.search(new RegExp(name, "i")) > -1))
									&& ((dob == "" || dob == undefined) || (input[i].TransactionData.parties[0].BasicDetails.dob != undefined && input[i].TransactionData.parties[0].BasicDetails.dob == dob))
									&& ((statusFilter == "" || statusFilter == undefined) || (input[i].Key15 != undefined && statusFilter == input[i].Key15))&& ((fnaID == "" || fnaID == undefined) || (input[i].Key2 != undefined && input[i].Key2
											.search(new RegExp(
													fnaID, "i")) > -1))) {
											out.push(input[i]);
											
											}
								}
						}
						return out;
					};
				});
filters
		.filter(
				'basicFNAFilter',
				function() {
					return function(input, searchFNA) {

						var out = [];
						for ( var i = 0; i < input.length; i++) {

							if (((searchFNA == "" || searchFNA == undefined) || (input[i].TransactionData.parties[0] != undefined
									&& input[i].TransactionData.parties[0].BasicDetails.firstName != undefined && input[i].TransactionData.parties[0].BasicDetails.firstName
									.search(new RegExp(searchFNA, "i")) > -1))
									|| ((searchFNA == "" || searchFNA == undefined) || (input[i].Key2 != undefined
											&& input[i].Key2 != undefined && input[i].Key2
											.search(new RegExp(searchFNA, "i")) > -1))) {

								out.push(input[i]);
							}
						}
						return out;
					};
				});

filters.filter('paginate', function() {
	return function(input, currentPage, rowsPerPage) {
		if (!input) {
			return input;
		}
		return input.slice(parseInt(currentPage * rowsPerPage),
				parseInt((currentPage + 1) * rowsPerPage + 1) - 1);
	};
});

filters.filter('listingFilter', function(){
    return function(input, searchKey){
        var output = [];
        if(typeof input != "undefined"){
        for(var i = 0; i < input.length; i++){
            if(input[i].Type == 'LMS'){
            if(((searchKey == "" || searchKey == undefined) || (input[i].Key2 != undefined &&  input[i].Key2.search(new RegExp(searchKey,"i")) > -1)) || ((searchKey == "" || searchKey == undefined) || (input[i].Key4 != undefined &&  input[i].Key4.search(new RegExp(searchKey,"i")) > -1))){
                output.push(input[i]);
            } 
			}else{
                if(((searchKey == "" || searchKey == undefined) || (input[i].Key6 != undefined &&  input[i].Key6.search(new RegExp(searchKey,"i")) > -1)) || ((searchKey == "" || searchKey == undefined) || (input[i].Key8 != undefined &&  input[i].Key8.search(new RegExp(searchKey,"i")) > -1)) || ((searchKey == "" || searchKey == undefined) || (input[i].Key21 != undefined &&  input[i].Key21.search(new RegExp(searchKey,"i")) > -1))){
                output.push(input[i]);
            }
        }
        
    }
    }
	return output;
	};
});

filters.filter('requirementFilter', function() {
	return function(input, uuid, key7Val) {

		var out = [];
		if (!jQuery.isEmptyObject(input) && input != "undefined" && input
				&& input.length) {
			for ( var i = 0; i < input.length; i++) {
				if ((input[i].partyIdentifier == uuid) && (key7Val !="Pending") && (key7Val !="PendingUploaded")) {
					out.push(input[i]);
				}else if((input[i].partyIdentifier == uuid) && ( key7Val =="Pending" || key7Val =="PendingUploaded")){
				   if(input[i].isMandatory =="Yes"){
				     input[i].isMandatory ="No";
				   }
				   //input[i].uploadStatus = input[i].Documents[0].documentUploadStatus;
				   out.push(input[i]);
				}
			}
		}
		return out;
	};
});

filters.filter('requirementFilterBeneficiary', function() {
	return function(input, uuid, key7Val) {

		var out = [];
		if (!jQuery.isEmptyObject(input) && input != "undefined" && input
				&& input.length) {
			for ( var i = 0; i < input.length; i++) {
                for(var j=0;j<uuid.length;j++){
						if ((uuid[j].UUID != "") && (input[i].partyIdentifier == uuid[j].UUID) &&  (key7Val !="Pending") && (key7Val !="PendingUploaded")) {
						  input[i].partyName =  uuid[j].partyName;
						out.push(input[i]);
					}else if((uuid[j].UUID != "") && (input[i].partyIdentifier == uuid[j].UUID) && ( key7Val =="Pending" || key7Val =="PendingUploaded")){
					  input[i].partyName =  uuid[j].partyName;
					   if(input[i].isMandatory =="Yes"){
						 input[i].isMandatory ="No";
					   }
					   input[i].uploadStatus = input[i].Documents[0].documentUploadStatus;
					   out.push(input[i]);
					}				
                }
			}
		}
		return out;
	};
});


filters.filter('documentFilter', function() {
	return function(codelookupCode, document) {

		var out = [];
		if (!jQuery.isEmptyObject(codelookupCode) && codelookupCode != "undefined" && codelookupCode
				&& codelookupCode.length) {
			for ( var i = 0; i < codelookupCode.length; i++) {
			    for(var j=0; j<document.length;j++){
				if(document[j].document != ""){
				if (codelookupCode[i].code == document[j].document && out.lastIndexOf(codelookupCode[i].value)==-1) {
					out.push(codelookupCode[i].value);
					break;
				}
				}else{
				if(out.lastIndexOf(codelookupCode[i].value)==-1){
				out.push(codelookupCode[i].value);
				}
				
				}
				}
				
			}
		}
		return out;
	};
});

filters.filter('documentRequirementFilter', function() {
	return function(input,key7Val) {
		var out = [];		
		if ( input != undefined && input.Documents) {
			for ( var i = 0; i < input.Documents.length; i++) {
				if(((input.Documents[i].documentStatus)!="Deleted")){
					out.push(input.Documents[i]);				
				}
			}
		}	
		return out;
		}

});

filters.filter('filterUniqueDocuments', function() {
	return function(input,filterObj,index) {
		var filterData=[];
		if ( typeof input != 'undefined'  ) {
				for(var j=0; j<input.length;j++){
					var valueExist = false;
					for(var k=0; k<filterObj.length;k++){
						if(input[j].code==filterObj[k].documentProofSubmitted && index!=k){
							valueExist = true;	
						}
					}
					if(!valueExist){
						filterData.push(input[j]);
					}
				}
		}
		return filterData;
		
	
	};
});
