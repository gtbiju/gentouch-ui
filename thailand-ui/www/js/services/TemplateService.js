//TemplateService for compiling HTML templates
angular.module('lifeEngage.TemplateService', []).factory("TemplateService", function($http) {
	
	var templateService = new Object();
		
	templateService.createHtmlTemplate = function(templateName, JSONObject, successCallback, errorCallback) {	
		
			if (rootConfig.isPrecomiledTemplate) {	
				
					var compiledTemplate = Handlebars.templates['lePreCompiledEmailTemplate'];	
					var compiledOutput = compiledTemplate(JSONObject);
					successCallback(compiledOutput);	
	
			} else {			
				$.get( templateName , function(respData) {						
					 var template = Handlebars.compile(respData);
					 
					 if (!localStorage[rootConfig.appName+'_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_lePrecompiledHandlebarEmailTemplate']) {
							eval("var preCompiledEmailTemplate = " + Handlebars.precompile(respData));
							localStorage[rootConfig.appName+'_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_lePrecompiledHandlebarEmailTemplate'] = preCompiledEmailTemplate;
					 	}
					 
					 var compiledOutput = template(JSONObject);									
					 successCallback(compiledOutput);
				 });	

				}				 
	};	
	
	return templateService;	
});