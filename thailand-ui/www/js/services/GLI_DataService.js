/*
 *Copyright 2015, LifeEngage 
 */




//DataService for GLI specific changes

angular
		.module('lifeEngage.GLI_DataService', []).factory(
				"GLI_DataService",['UserDetailsService','$location','$translate','$rootScope','DataService','$http',
				function(UserDetailsService, $location, $translate, $rootScope,DataService, $http) {
					
					//Function to retrieve FNA corresponding to a given LMS
					DataService['getRelatedFNAForLMS'] = function(lmsId, successCallback,
							errorCallback) {

						return DbOperationsUtility.getRelatedFNAForLMS(lmsId,
								successCallback, function(data, status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback(data);
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
					};
					
					DataService['retrieveAgentDetailsOffline'] = function(transactionObj, successCallback, errorCallback) {
				            try {
				        
				        	  return DbOperationsUtility.retrieveAgentDetails(transactionObj,
								function(data){
				        	      			if(data.length>0){
				        	      			    var responseData = {};
				        	      			    responseData['AgentDetails'] = data[0];
				        	      			    responseData['signatureDate'] = "";
				        	      			    responseData['Achievements'] = [];
				        	      			    responseData['spajNos'] = [];	
										//successCallback(responseData);												
				        	      			   DbOperationsUtility.retrieveAchievements(transactionObj,
											function(achievements){												
												if(achievements.length>0){
													 responseData['Achievements'].push(achievements);
												}
												successCallback(responseData);
											},
											function(data, status){
												errorCallback(data);
									      });
									      
				        	      			}
				        	  		}, function(data, status){
				        	  		    if(status=='401'){
				        	  			$location.path('/login');
				        	  			$rootScope.$apply();
				        	  			$rootScope.showHideLoadingImage(false);
				        	  			$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
				        	  			   $rootScope.showHideLoadingImage(false);
								        });
				        	  		    }else{
				        	  			errorCallback(data);
				        	  		    }
				        	  		}, UserDetailsService.getUserDetailsModel().options, $http);				        	  
				        	 
				            } catch (exception) {
				                errorCallback(exception);
				            }
				        },
					
					//function to retrive eApp for BI
					DataService['getRelatedEappForBI'] = function(transTrackingId, successCallback,
							errorCallback) {

						return DbOperationsUtility.getRelatedEappForBI(transTrackingId,
								successCallback, function(data, status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback(data);
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
					};
					
					
					//function to retrive eApp for LMS
					DataService['getRelatedEappForLMS'] = function(transTrackingId, successCallback,
							errorCallback) {

						return DbOperationsUtility.getRelatedEappForLMS(transTrackingId,
								successCallback, function(data, status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback(data);
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
					};
					
					//function to retrive BI for eApp
					//This is doen to disable the fields in EApp which is prepopulated even in eedit flow.
					DataService['getRelatedBIForEapp'] = function(transTrackingId, successCallback,
							errorCallback) {

						return DbOperationsUtility.getRelatedBIForEapp(transTrackingId,
								successCallback, function(data, status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback(data);
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
					};
					
					DataService['getRelatedIllustrationForLMS'] = function(lmsId, successCallback,
							errorCallback) {
					
						return DbOperationsUtility.getRelatedIllustrationForLMS(lmsId,
								successCallback, function(data, status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback(data);
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
						
					};
					
					DataService['retrieveByFilterCount'] = function(transactionObj,
							successCallback, errorCallback) {

						return DbOperationsUtility.retrieveByFilterCount(
								transactionObj, successCallback, function(data, status){
                                                                                                    if(status=='401'){
                                                                                                       $location.path('/login');
                                                                                                           $rootScope.$apply();
                                                                                                              $rootScope.showHideLoadingImage(false);
                                                                                                                  $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
                                                                                                                       $rootScope.showHideLoadingImage(false);
                                                                                                                  });
                                                                                                    }else{
                                                                                                       errorCallback(data);
                                                                                                    }
                                                                                                 }, UserDetailsService.getUserDetailsModel().options,$http);
					};
					
					DataService['getDetailsForListing'] = function(transactionObj,
							successCallback, errorCallback) {
						
						return DbOperationsUtility.getDetailsForListing(transactionObj,
								successCallback, function(data, status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback(data);
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
					};
					DataService['downloadPDF']  = function(transactionObj,
							templateId, successCallback, errorCallback) {
						
						return DbOperationsUtility.downloadPDF(transactionObj,
								templateId, successCallback, function(data, status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback(data);
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
					};
					
					DataService['printPDF']  = function(transactionObj,
							templateId, successCallback, errorCallback) {
						
						return DbOperationsUtility.printPDF(transactionObj,
								templateId, successCallback, function(data, status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback(data);
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
					};

					DataService['getAllDocsToResubmit'] = function(status,transtrackingId,
							successCallback, errorCallback) {
						DbOperationsUtility.getAllDocsToResubmit(status,transtrackingId,
								successCallback, errorCallback);
					};
					
					//save reporting agents
					DataService['saveReportingAgentsDetails'] = function(reportingList,
							successCallback, errorCallback) {
						DbOperationsUtility.saveReportingAgentsDetails(reportingList,
								successCallback, errorCallback);
					};
                    
                    //save Applicable Products
					DataService['saveApplicableProductDetails'] = function(productList,
							successCallback, errorCallback) {
						DbOperationsUtility.saveApplicableProductDetails(productList,
								successCallback, errorCallback);
					};
					//save hierarchy response
					DataService['saveHierarchyResponse'] = function(hierarchyResponse,
							successCallback, errorCallback) {
						DbOperationsUtility.saveHierarchyResponse(hierarchyResponse,
								successCallback, errorCallback);
					};
					
					//save mapped branches
					DataService['saveMappedBranchesDetails'] = function(mappedBranchList,
							successCallback, errorCallback) {
						DbOperationsUtility.saveMappedBranchesDetails(mappedBranchList,
								successCallback, errorCallback);
					};
					
					//save reporting RMs
					DataService['saveReportingRM'] = function(reportingRMList,
							successCallback, errorCallback) {
						DbOperationsUtility.saveReportingRM(reportingRMList,
								successCallback, errorCallback);
					};
					
					//get reporting agents
					DataService['retrieveReportingAgentsDetails'] = function(parentAgentCode,
							successCallback, errorCallback) {
						DbOperationsUtility.retrieveReportingAgentsDetails(parentAgentCode,
								successCallback, errorCallback);
					};
					
					//get applicable products 
					DataService['retrieveApplicableProductDetails'] = function(parentAgentCode,
							successCallback, errorCallback) {
						DbOperationsUtility.retrieveApplicableProductDetails(parentAgentCode,
								successCallback, errorCallback);
					};
					
							
								//get hierarchy response
					DataService['retrieveHierarchyResponse'] = function(AgentCode,
							successCallback, errorCallback) {
						DbOperationsUtility.retrieveHierarchyResponse(AgentCode,
								successCallback, errorCallback);
					};
					
							//get mapping branches
					DataService['retrieveMappedBranchesDetails'] = function(parentAgentCode,
							successCallback, errorCallback) {
						DbOperationsUtility.retrieveMappedBranchesDetails(parentAgentCode,
								successCallback, errorCallback);
					};
					
					//get reporting RM
					DataService['retrieveReportingRM'] = function(parentAgentCode,
							successCallback, errorCallback) {
						DbOperationsUtility.retrieveReportingRM(parentAgentCode,
								successCallback, errorCallback);
					};
					
					//functions for implementing SPAJ logic
					DataService['retrieveSPAJNumbers'] = function(dataObj,
							successCallback, errorCallback) {
						return DbOperationsUtility.retrieveSPAJNumbers(dataObj,
								successCallback, errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
					};					
					DataService['AgentCodeValidation'] = function(dataObj,
							successCallback, errorCallback) {
						return DbOperationsUtility.AgentCodeValidation(dataObj,
								successCallback, errorCallback,UserDetailsService.getUserDetailsModel().options, $http);
					};

					DataService['isSpajExisting'] = function(spajno,
							successCallback, errorCallback) {
						return DbOperationsUtility.isSpajExisting(spajno,
								successCallback, errorCallback,UserDetailsService.getUserDetailsModel().options, $http);
					};
					DataService['saveSPAJDetails'] = function(spajno,agentId,
							successCallback, errorCallback) {
						return DbOperationsUtility.saveSPAJDetails(spajno,agentId,
								successCallback, errorCallback);
					};
					DataService['retrieveSPAJCount'] = function(agentId,
							successCallback, errorCallback) {
						return DbOperationsUtility.retrieveSPAJCount(agentId,
								successCallback, errorCallback);
					};
					DataService['retrieveSPAJNo'] = function(agentId,
							successCallback, errorCallback) {
						return DbOperationsUtility.retrieveSPAJNo(agentId,
								successCallback, errorCallback);
					};
					DataService['getRelatedTransactionsToSync'] = function(lmsId, fnaId, biId,
							successCallback, errorCallback) {
						return DbOperationsUtility.getRelatedTransactionsToSync(lmsId, fnaId, biId,
								successCallback, errorCallback);
					};
					DataService['getRelatedLMS'] = function(leadId, successCallback, errorCallback) {
						return DbOperationsUtility.getRelatedLMS(leadId, successCallback, errorCallback);
					};
					DataService['callToUpdateBPMUpload'] = function(keyToUpdate, isSubmissionPending,
							successCallback, errorCallback) {
						return DbOperationsUtility.callToUpdateBPMUpload(keyToUpdate, isSubmissionPending, successCallback,
										errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
					};
					DataService['generateBPMEmailPDF'] = function(transactionObj, successCallback, errorCallback) {
						return DbOperationsUtility.generateBPMEmailPDF(transactionObj, successCallback,
										errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
					};
					DataService['updateTransactionAfterBPMEmailPDF'] = function(transactionObj, transId, successCallback, errorCallback) {
						return DbOperationsUtility.updateTransactionAfterBPMEmailPDF(transactionObj, transId, successCallback,
										errorCallback);
					};
					DataService['checkInService'] = function(transactionObj, successCallback, errorCallback) {
                    						return DbOperationsUtility.checkInService(transactionObj, successCallback,
                    										errorCallback,UserDetailsService.getUserDetailsModel().options, $http);
					};
					DataService['getCheckInDetails'] = function(transactionObj, successCallback, errorCallback) {
                                        						return DbOperationsUtility.getCheckInDetails(transactionObj, successCallback,
                                        										errorCallback,UserDetailsService.getUserDetailsModel().options, $http);
					};
					DataService['getLAServerLiveStatus'] = function(successCallback, errorCallback) {
						return DbOperationsUtility.getLAServerLiveStatus(successCallback,
										errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
					};


					DataService['getTransactions']  = function(transactionObj, successCallback, errorCallback) {
						return DbOperationsUtility.getTransactions(transactionObj, successCallback, function(data, status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
								});
							}else{
								errorCallback(data);
							}
						},$http);
					};
					
					DataService['retrieveSalesActivityDetails'] = function(transactionObj, successCallback, errorCallback) {
						return DbOperationsUtility.retrieveSalesActivityDetails(transactionObj, successCallback,
                                                errorCallback,UserDetailsService.getUserDetailsModel().options, $http);
					};
					DataService['memoEmailCall'] = function(transactionObj,successCallback, errorCallback) {
						return DbOperationsUtility.memoEmailCall(transactionObj,successCallback,errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
					};
					DataService['memoDownloadPDF'] = function(transactionObj,templateId,successCallback, errorCallback) {
						return DbOperationsUtility.memoDownloadPDF(transactionObj,templateId,successCallback,errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
					};
					DataService['deleteUserDetailsTableForAnAgent'] = function(agentId,successCallback, errorCallback) {
						return DbOperationsUtility.deleteUserDetailsTableForAnAgent(agentId,successCallback,errorCallback);
					};
					DataService['requestGAOSupport'] = function(transactionObj,successCallback, errorCallback) {
						return DbOperationsUtility.requestGAOSupport(transactionObj,successCallback,errorCallback,UserDetailsService.getUserDetailsModel().options, $http);
					};
					DataService['dbInit'] = function(successCallback){
						DbOperationsUtility.dbInit(successCallback);
					}
					//consent changes
					DataService['sendOTP'] = function(transactionObj, successCallback, errorCallback) {
                        return DbOperationsUtility.sendOTP(transactionObj, successCallback, errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
                    };
					DataService['resendOtp'] = function(transactionObj, successCallback, errorCallback) {
                        return DbOperationsUtility.resendOtp(transactionObj, successCallback, errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
                    };
					DataService['validateOTP'] = function(transactionObj, successCallback, errorCallback) {
                        return DbOperationsUtility.validateOTP(transactionObj, successCallback, errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
					};
					DataService['generateOTP'] = function(otpInput, successCallback, errorCallback) {
                        return DbOperationsUtility.generateOTP(otpInput, successCallback, errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
                    };	
					DataService['verifyOTP'] = function(otpVerifyInput, successCallback, errorCallback) {
                        return DbOperationsUtility.verifyOTP(otpVerifyInput, successCallback, errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
                    };					
					//end
					return DataService;
				}]);