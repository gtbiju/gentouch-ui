//EmailService for Sending Mail, Attaching Photo, DeviceInfo
angular.module('lifeEngage.EmailService', []).factory("EmailService", function($http) {
	
	var emailService = new Object();
		
	
	emailService.sendEmail = function(emailObj, successCallback, errorCallback, options) {	
		
		EmailServiceUtility.sendEmail(emailObj, successCallback, errorCallback, options, $http);
	},
	
	emailService.attachPhoto = function(successCallback, errorCallback) {	
		
		EmailServiceUtility.attachPhoto(successCallback, errorCallback);
	};	
	
	return emailService;	
});