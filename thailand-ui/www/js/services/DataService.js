//DataService for save,retrieve, sysn
angular
		.module('lifeEngage.DataService', [])
		.factory(
				"DataService", ['UserDetailsService','$location','$translate','$rootScope', '$http' ,
				function(UserDetailsService, $location, $translate, $rootScope, $http) {
					var lifeEngage = new Object();

					lifeEngage.initializeDb = function(successFileList) {
						DbOperationsUtility.initializeDb(successFileList);
					};
					lifeEngage.getListings = function(transactionObj,
							successCallback, errorCallback) {
						DbOperationsUtility.getListings(transactionObj,
								successCallback, function(data, status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback(data);
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
					};
					lifeEngage.getDetailsForListing = function(transactionObj,
							successCallback, errorCallback) {
						DbOperationsUtility.getDetailsForListing(
								transactionObj, successCallback, function(data, status){
									if(status=='401'){
										$location.path('/login');
										$rootScope.$apply();
										$rootScope.showHideLoadingImage(false);
										$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
											$rootScope.showHideLoadingImage(false);
											});
									}else{
										errorCallback(data);
									}
								}, UserDetailsService.getUserDetailsModel().options, $http);
					};
					lifeEngage.getListingDetail = function(transactionObj,
							successCallback, errorCallback) {
						DbOperationsUtility.getListingDetail(transactionObj,
								successCallback, function(data, status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback(data);
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
					};
					lifeEngage.getCancelledProposals = function(status,
							successCallback, errorCallback) {
						DbOperationsUtility.getCancelledProposals(status,
								successCallback, errorCallback);
					};

					lifeEngage.saveTransactions = function(transactionObj,
							successCallback, errorCallback) {
						DbOperationsUtility.saveTransactions(transactionObj,
								successCallback, function(data, status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback(data);
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
					};
					lifeEngage.deleteRequirementFiles = function(tableName,transtrackingId,fileName,successCallback,errorCallback) {
						DbOperationsUtility.deleteRequirementFiles(tableName,transtrackingId,fileName,successCallback, function(data, status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "Token Validity expired. Please login again"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
								});
							}else{
								errorCallback(data);
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
					};
					lifeEngage.runSTPRule = function(transactionObj,
							successCallback, errorCallback) {
						var options= eval(UserDetailsService.getUserDetailsModel()).options;
						DbOperationsUtility.runSTPRule(transactionObj,
								successCallback, function(data){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback();
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
					};
					
					lifeEngage.runRequirementRule = function(transactionObj,
							successCallback, errorCallback) {
							var options= eval(UserDetailsService.getUserDetailsModel()).options;
							DbOperationsUtility.runRequirementRule(transactionObj,
								successCallback, function(data){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.validToken"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback();
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
					};
					
					lifeEngage.syncTransactions = function(transactionObj,
							callback) {
						DbOperationsUtility.syncTransactions(transactionObj,
								successCallback, errorCallback);
					};
					lifeEngage.deleteTransactions = function(transactionObj,
							successCallback, errorCallback) {
						DbOperationsUtility.deleteTransactions(transactionObj,
								successCallback, function(data, status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback(data);
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
					};

					lifeEngage.saveFileLocally = function(documentData,
							successCallback, errorCallback) {
						DbOperationsUtility.saveFile(documentData,
								successCallback, errorCallback);
					};
					lifeEngage.saveFilesLocally = function(documents,
							successCallback, errorCallback) {
						DbOperationsUtility.saveFilesLocally(documents,
								successCallback, errorCallback);
					};
					lifeEngage.closeDatabase = function(dbName,
							successCallback, errorCallback) {
						DbOperationsUtility.closeDatabase(dbName,
								successCallback, errorCallback);
					};

					lifeEngage.deleteFromTable = function(tableName,
							whereClause, successCallback, errorCallback) {
						DbOperationsUtility.deleteFromTable(tableName,
								whereClause, successCallback, errorCallback);
					};

					lifeEngage.fetchOne = function(dataObj, successCallback,
							errorCallback) {
						DbOperationsUtility.fetchOne(dataObj, successCallback,
								errorCallback);
					};

					lifeEngage.fetchAll = function(dataObj, tblName,
							successCallback, errorCallback) {
						DbOperationsUtility.fetchAll(dataObj, tblName,
								successCallback, errorCallback);
					};

					lifeEngage.getDocumentsForUpload = function(proposalId,
							successCallback, errorCallback) {
						DbOperationsUtility.getDocumentsForUpload(proposalId,
								successCallback, errorCallback);
					};

					lifeEngage.getAllDocsToSync = function(syncStatus,
							recordType, successCallback, errorCallback) {
						DbOperationsUtility.getAllDocsToSync(syncStatus,
								recordType, successCallback, errorCallback);
					};
					lifeEngage.getKey4ForDoc = function(id, keyToUpdate,
							successCallback, errorCallback) {
						DbOperationsUtility.getKey4ForDoc(id, keyToUpdate,
								successCallback, errorCallback);
					};

					lifeEngage.getDocumentsForTransaction = function(
							transactionId, successCallback, errorCallback) {
						DbOperationsUtility.getDocumentsForTransaction(
								transactionId, successCallback, errorCallback);
					};

					lifeEngage.syncData = function(type, successCallback,
							errorCallback,options) {
						DbOperationsUtility.syncData(type, successCallback,
								function(data){
							if(data && data.statusCode=='401'){
								$rootScope.showHideLoadingImage(false);
								$rootScope.startupPopup = false;
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
								}else{
								errorCallback();
							}
						},options);
					};

					lifeEngage.syncDataToServer = function(type,
							successCallback, errorCallback, saveBandwidth,
							options) {
						DbOperationsUtility.syncDataToServer(type,
								successCallback, function(data){
							if(data && data.statusCode=='401'){
								$rootScope.showHideLoadingImage(false);
								$rootScope.startupPopup = false;
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback();
							}
						}, saveBandwidth,
								options);
					};
					lifeEngage.getEmptyTransTrackingID = function(successCallback) {
						DbOperationsUtility.getEmptyTransTrackingID(successCallback);
					};
					lifeEngage.updateTransTrackingID = function(data,transactionData,successCallback) {
						DbOperationsUtility.updateTransTrackingID(data,transactionData,successCallback);
					};
					lifeEngage.updateTransTblStatus = function(tableName,
							status, transactionIds, successCallback,
							errorCallback) {
						DbOperationsUtility.updateTransTblStatus(tableName,
								status, transactionIds, successCallback,
								errorCallback);
					};

					lifeEngage.updateTransStatusAndData = function(data,
							status, transactionIds, successCallback,
							errorCallback) {
						DbOperationsUtility.updateTransStatusAndData(data,
								status, transactionIds, successCallback,
								errorCallback);
					};

					lifeEngage.payment = function(transactionObj,
							successCallback, errorCallback) {

						return DbOperationsUtility.payment(transactionObj,
								successCallback, errorCallback);
					};

					lifeEngage.getDocument = function(document,
							successCallback, errorCallback) {

						return DbOperationsUtility.getDocument(document,
								successCallback, errorCallback);
					};
					lifeEngage.getPages = function(documentName,
							successCallback, errorCallback) {

						return DbOperationsUtility.getPages(documentName,
								successCallback, errorCallback);

					};
					lifeEngage.getFile = function(file, successCallback,
							errorCallback) {

						return DbOperationsUtility.getFile(file,
								successCallback, errorCallback);

					};
					lifeEngage.getTransDataWithStatus = function(tableCol,
									proposalStatus, type, agentId, successCallback,
									errorCallback) {
								return DbOperationsUtility
										.getTransDataWithStatus(tableCol,proposalStatus,
												type, agentId, successCallback,
												errorCallback);
					};
					lifeEngage.getParentDetails = function(parentId, successCallback,
							errorCallback) {

						return DbOperationsUtility.getParentDetails(parentId,
								successCallback, errorCallback);
					};
					lifeEngage.getRelatedFNA = function(fnaId, successCallback,
							errorCallback) {

						return DbOperationsUtility.getRelatedFNA(fnaId,
								successCallback, errorCallback);
					};
					lifeEngage.getModulesSummary = function(transactionObj,
							successCallback, errorCallback) {

						return DbOperationsUtility.getModulesSummary(
								transactionObj, successCallback, errorCallback);
					};
					lifeEngage.getFilteredListing = function(transactionObj,
							successCallback, errorCallback) {
						return DbOperationsUtility.getFilteredListing(
								transactionObj, successCallback, function(data, status){
									if(status=='401'){
										$location.path('/login');
										$rootScope.$apply();
										$rootScope.showHideLoadingImage(false);
										$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
											$rootScope.showHideLoadingImage(false);
											});
									}else{
										errorCallback(data);
								} }, UserDetailsService.getUserDetailsModel().options,$http);
					};
					lifeEngage.retrieveByFilterCount = function(transactionObj,
							successCallback, errorCallback) {

						return DbOperationsUtility.retrieveByFilterCount(
								transactionObj, successCallback, errorCallback, UserDetailsService.getUserDetailsModel().options,$http);
					};
					lifeEngage.getAgentDetails = function(transactionObj,
							successCallback, errorCallback) {

						return DbOperationsUtility.getAgentDetails(
								transactionObj, successCallback, errorCallback);
					};
					lifeEngage.getAchievementDetails = function(transactionObj,
							successCallback, errorCallback) {

						return DbOperationsUtility.getAchievementDetails(
								transactionObj, successCallback, errorCallback);
					};
					lifeEngage.downloadPDF = function(transactionObj,
							templateId, successCallback, errorCallback) {
						return DbOperationsUtility.downloadPDF(transactionObj,
								templateId, successCallback, function(data, status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback(data);
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
					};
                    
                    lifeEngage.downloadBase64 = function(transactionObj, successCallback, errorCallback) {
				        return DbOperationsUtility.downloadBase64(transactionObj,
												successCallback,errorCallback,
												UserDetailsService
														.getUserDetailsModel().options,
												$http);
				   };

					lifeEngage.saveOnline = function(transactionObj,
							successCallback, errorCallback) {
						return DbOperationsUtility.saveOnline(transactionObj,
								successCallback, function(data, status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.showHideLoadingImage(false);
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback(data);
							}
						}, UserDetailsService.getUserDetailsModel().options, $http);
					};

					lifeEngage.isDuplicateLead = function(transactionObj,
							successCallback, errorCallback) {

						return DbOperationsUtility.isDuplicateLead(
								transactionObj, successCallback, errorCallback);
					};
					lifeEngage.getCalenderListings = function(currentDate,
							todoSelected, endDate, viewallDate, transactionObj, successCallback,
							errorCallback) {
						DbOperationsUtility.getCalenderListings(currentDate,
								todoSelected, endDate, viewallDate, transactionObj, successCallback,
								errorCallback);
					};
					lifeEngage.getLMSCount = function(transactionObj, agentId, successCallback,
							errorCallback) {
						DbOperationsUtility.getLMSCount(transactionObj, agentId, successCallback,
								errorCallback);
					};
					lifeEngage.fetchTransactionForIds = function(transactionObj,
							successCallback, errorCallback) {
						DbOperationsUtility
								.fetchTransactionForIds(
										transactionObj,
										successCallback,
										function(data, status) {
											if (status == '401') {
												$location
														.path('/login');
												$rootScope.$apply();
												$rootScope.showHideLoadingImage(false);
												$rootScope.lePopupCtrl
														.showError(
																translateMessages(
																		$translate,
																		"lifeEngage"),
																translateMessages(
																		$translate,
																		"general.tokenValidityErrMsg"),
																translateMessages(
																		$translate,
																		"fna.ok"),
																function() {
																	$rootScope
																			.showHideLoadingImage(false);
																});
											} else {
												errorCallback(data);
											}
										},
										UserDetailsService
												.getUserDetailsModel().options,
										$http);
					};						
					lifeEngage.saveTransactionObjectsMultiple = function(
							transactionObj, successCallback,
							errorCallback) {
						DbOperationsUtility
								.saveTransactionObjectsMultiple(
										transactionObj,
										successCallback,
										function(data, status) {
											if (status == '401') {
												$location
														.path('/login');
												$rootScope.$apply();
												$rootScope.showHideLoadingImage(false);
												$rootScope.lePopupCtrl
														.showError(
																translateMessages(
																		$translate,
																		"lifeEngage"),
																translateMessages(
																		$translate,
																		"general.tokenValidityErrMsg"),
																translateMessages(
																		$translate,
																		"fna.ok"),
																function() {
																	$rootScope
																			.showHideLoadingImage(false);
																});
											} else {
												errorCallback(data);
											}
										},
										UserDetailsService
												.getUserDetailsModel().options,
										$http);
					};
					lifeEngage.saveAgentProfile = function(transactionObj, successCallback, errorCallback) {
						DbOperationsUtility.saveAgentProfile(transactionObj, successCallback, errorCallback);
					};
					
					lifeEngage.retrieveAgentDetails = function(transactionObj, successCallback, errorCallback) {
						DbOperationsUtility.retrieveAgentDetails(transactionObj, successCallback, errorCallback);
					};
					lifeEngage.retrieveAchievements = function(transactionObj, successCallback, errorCallback) {
						DbOperationsUtility.retrieveAchievements(transactionObj, successCallback, errorCallback);
					};
					lifeEngage.updateTransactionAfterSync = function(transactionObj, transactionId, successCallback, errorCallback) {
						DbOperationsUtility.updateTransactionAfterSync(transactionObj, transactionId, successCallback, errorCallback);
					};
					
					lifeEngage.saveRequirementFiles = function(documentData,
							successCallback, errorCallback) {
								DbOperationsUtility.saveRequirementFiles(documentData,
								successCallback, function(data,status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.validToken"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback(data);
							}
						},UserDetailsService.getUserDetailsModel().options,$http);
					};
					
					lifeEngage.updateRequirementFiles = function(data,
							successCallback, errorCallback) {
						DbOperationsUtility.updateRequirementFiles(
								data, successCallback, errorCallback);
					};

					lifeEngage.getRequirementForDelete = function(data,
							requirementName, transtrackingId,
							successCallback, errorCallback) {
						DbOperationsUtility.getRequirementForDelete(
								data, requirementName, transtrackingId,
								successCallback, errorCallback);
					};

					lifeEngage.getDocumentsForRequirement = function(
							transactionObj, successCallback,
							errorCallback) {
						DbOperationsUtility.getDocumentsForRequirement(
								transactionObj, successCallback, function(data,status){
							if(status=='401'){
								$location.path('/login');
								$rootScope.$apply();
								$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.validToken"),translateMessages($translate, "fna.ok"),function(){
									$rootScope.showHideLoadingImage(false);
									});
							}else{
								errorCallback();
							}
						},UserDetailsService.getUserDetailsModel().options,$http);
					};
					
					lifeEngage.getRequirementsForTransId = function(
							transactionObj, transtrackingId,
							successCallback, errorCallback) {
						DbOperationsUtility.getRequirementsForTransId(
								transactionObj, transtrackingId,
								successCallback, errorCallback);
					};

					lifeEngage.getRequirementsUploadedForTransId = function(
							transactionObj, transtrackingId,
							successCallback, errorCallback) {
						DbOperationsUtility
								.getRequirementsUploadedForTransId(
										transactionObj,
										transtrackingId,
										successCallback, errorCallback);
					};

					lifeEngage.getStatusForRequirement = function(
							transactionObj, transtrackingId,
							successCallback, errorCallback) {
						DbOperationsUtility.getStatusForRequirement(
								transactionObj, transtrackingId,
								successCallback, errorCallback);
					};

					lifeEngage.getDocumentsForSavingComment = function(
							data, requirementName, requirementType,
							transtrackingId, successCallback,
							errorCallback) {
						DbOperationsUtility
								.getDocumentsForSavingComment(data,
										requirementName,
										requirementType,
										transtrackingId,
										successCallback, errorCallback);
					};

					lifeEngage.getRequirementsForTransaction = function(
							transtrackingId, successCallback,
							errorCallback) {
						DbOperationsUtility
								.getRequirementsForTransaction(
										transtrackingId,
										successCallback, errorCallback);
					};

					lifeEngage.getSignature = function(transtrackingId,
							signature, successCallback, errorCallback) {
						DbOperationsUtility.getSignature(
								transtrackingId, signature,
								successCallback, errorCallback);
					};

					lifeEngage.getCancelledProposalsForType = function(
							status, type, agentId, successCallback,
							errorCallback) {
						DbOperationsUtility
								.getCancelledProposalsForType(status,
										type, agentId, successCallback,
										errorCallback);
					};

					lifeEngage.updateFileStatusSynced = function(
							curReqFileToSync, successCallback,
							errorCallback) {
						DbOperationsUtility.updateFileStatusSynced(
								curReqFileToSync, successCallback,
								errorCallback);
					};

					lifeEngage.deleteIfFileExists = function(fileObj,
							successCallback, errorCallback) {
						DbOperationsUtility.deleteIfFileExists(fileObj,
								successCallback, errorCallback);
					};

					lifeEngage.checkIfFileExists = function(fileObj,
							successCallback, errorCallback) {
						DbOperationsUtility.checkIfFileExists(fileObj,
								successCallback, errorCallback);
					};

					lifeEngage.getStatusForDocuments = function(
							transactionObj, successCallback, errorCallback) {
                        DbOperationsUtility.getStatusForDocuments(transactionObj, successCallback, errorCallback);
                      };
                      
                   lifeEngage.getDocumentFileForRequirement = function(
  							transactionObj, successCallback, errorCallback) {
  						DbOperationsUtility.getDocumentFileForRequirement(
  								transactionObj, successCallback, errorCallback,UserDetailsService
  														.getUserDetailsModel().options,$http);
  					};
  					lifeEngage.getRequirementsFilesToSync = function(
									transtrackingId, successCallback,
									errorCallback) {
								DbOperationsUtility.getRequirementsFilesToSync(
										transtrackingId, successCallback,
										errorCallback)
							};

                    lifeEngage.getRequirementFilesToDelete = function(
  							transactionId,documentName,successCallback, errorCallback) {
  						DbOperationsUtility.getRequirementFilesToDelete(
  								transactionId,documentName,successCallback, errorCallback,UserDetailsService
  														.getUserDetailsModel().options,$http);
  					};
  					
  				  lifeEngage.getTransactions = function (transactionObj, successCallback, errorCallback) {
  				    var options = UserDetailsService.userDetilsModel.user;
  			        DbOperationsUtility.getTransactions(transactionObj, successCallback, function(data, status, message){
  					    if(status=='401'){
  							errorCallback();
  						} else {
  							errorCallback();
  						}
  					}, UserDetailsService.userDetilsModel.options.headers, $http);       
  			    };
				
				lifeEngage.retrieveSalesActivityDetails = function(transactionObj,
							successCallback, errorCallback) {

						return DbOperationsUtility.retrieveSalesActivityDetails(
								transactionObj, successCallback, errorCallback, UserDetailsService.getUserDetailsModel().options,$http);
					};
					
					return lifeEngage;

				}]);
