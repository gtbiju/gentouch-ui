﻿/*
 *Copyright 2015, LifeEngage 
 */



//DataService for save,retrieve, sysn
angular.module('lifeEngage.AuthService', [])
		.factory(
				"AuthService",['$http',
				function($http) {
					var authService = new Object();

					authService.login = function(user, successCallback,
							errorCallback) {
						authServiceUtility.login(user, successCallback,
								errorCallback, $http);
					};
					authService.logout = function(user, successCallback,
							errorCallback) {
						authServiceUtility.logout(user, successCallback,
								errorCallback);
					};

					authService.isLoggedIn = function(user) {
						return authServiceUtility.isLoggedIn(user);
					};
					authService.authorize = function(accessLevel, role) {
						return authServiceUtility.authorize(accessLevel, role);
					};
					authService.fetchKey = function(user,successCallback,errorCallback) {
						return authServiceUtility.fetchKey(user,successCallback,errorCallback,$http);
					};
					return authService;

				}]);
