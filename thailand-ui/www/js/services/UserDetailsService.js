/*
 *Copyright 2015, LifeEngage 
 */



angular.module('lifeEngage.UserDetailsService',[]).service("UserDetailsService", function ($http) {
	var UserDetailsService= {
			"userDetilsModel" : "",
			"isRDSUser" : "",
			"RDSUserDetailsModel" : "",
			"RMReportingAgents":"",
			"agentMappedBranches":"",
            "applicableProducts":"",
			"getUserDetailsModel" : function() {
				if (!UserDetailsService.userDetilsModel) {
					userDetailsObj = new UserDetailsObject();
					if ((rootConfig.isDeviceMobile)) {
						userDetailsObj.options.headers.source = "200";
					} else {
						userDetailsObj.options.headers.source = "100";
					}
					return userDetailsObj;
				} else {
					var userDetilsModelObj = JSON
							.stringify(UserDetailsService.userDetilsModel);
					return JSON.parse(userDetilsModelObj);
				}
			},
		    "setUserDetailsModel" :function(value) {
		    	UserDetailsService.userDetilsModel = value;
			},
			"getRDSUserDetailsModel" : function() {
				if (!UserDetailsService.RDSUserDetailsModel) {
					RDSUserDetailsModelObj = new UserDetailsObject();
					if ((rootConfig.isDeviceMobile)) {
						RDSUserDetailsModelObj.options.headers.source = "200";
					} else {
						RDSUserDetailsModelObj.options.headers.source = "100";
					}
					return RDSUserDetailsModelObj;
				} else {
					var RDSUserDetailsModelObj = JSON
							.stringify(UserDetailsService.RDSUserDetailsModel);
					return JSON.parse(RDSUserDetailsModelObj);
				}
			},
		    "setRDSUserDetailsModel" :function(value) {
		    	UserDetailsService.RDSUserDetailsModel = value;
			},
			"getRDSUser" : function() {
				return UserDetailsService.isRDSUser;
			},
		    "setRDSUser" :function(value) {
		    	UserDetailsService.isRDSUser = value;
			},
			"setReportingAgents" : function(value){
			UserDetailsService.RMReportingAgents=value;
			},
			"getReportingAgents" : function(value){
			return UserDetailsService.RMReportingAgents;
			},
            "setApplicableProducts" : function(value){
			 UserDetailsService.applicableProducts=value;
			},
			"getApplicableProducts" : function(value){
			return UserDetailsService.applicableProducts;
			},
			"setMappedBranches" : function(value){
			UserDetailsService.agentMappedBranches=value;
			},
			"getMappedBranches" : function(value){
			return UserDetailsService.agentMappedBranches;
			},
			"setHierarchyData" : function(value){
			UserDetailsService.agentHierarchyDataResponse=value;
			},
			"getHierarchyData" : function(value){
			return UserDetailsService.agentHierarchyDataResponse;
			},
			"setAgentRetrieveData" : function(value){
				UserDetailsService.agentRetrieveData=angular.copy(value);
			},
			"getAgentRetrieveData" : function(){
				return UserDetailsService.agentRetrieveData;
			},
			"setCurrentBranch" : function(value){
				UserDetailsService.currentBranch=angular.copy(value);
			},
			"getCurrentBranch" : function(){
				return UserDetailsService.currentBranch;
			},
			"setReportingRM" : function(value){
				UserDetailsService.reportingRM=angular.copy(value);
			},
			"getReportingRM" : function(){
				return UserDetailsService.reportingRM;
			},
			"setAgentForGAO" : function(value){
				UserDetailsService.agentForGAO=angular.copy(value);
			},
			"getAgentForGAO" : function(){
				return UserDetailsService.agentForGAO;
			}
	};
     return UserDetailsService;
    
});