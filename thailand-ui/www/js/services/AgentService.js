﻿
//DataService for agentprofile
angular.module('lifeEngage.AgentService', [])
		.factory(
				"AgentService",['UserDetailsService','$http',
				function(UserDetailsService,$http) {
					var agentService = new Object();
try{
					agentService.retrieveAgentProfile = function(transactionObj, successCallback,
							errorCallback) {
						agentServiceUtility.retrieveAgentProfile(transactionObj, successCallback,
								errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
								
					};
					agentService.retrieveHierarchyAgentProfile = function(transactionObj, successCallback,
						errorCallback) {
					agentServiceUtility.retrieveHierarchyAgentProfile(transactionObj, successCallback,
							errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
							
					};
					agentService.retrieveManagerialLevelProfile = function(transactionObj, successCallback,
						errorCallback) {
					agentServiceUtility.retrieveManagerialLevelProfile(transactionObj, successCallback,
							errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
							
					};
					}catch(e){
					alert(e);
					}
					return agentService;

				}]);
