﻿/*
 *Copyright 2015, LifeEngage 
 */



//documentService for all document related functionalities like upload, browse, capture, delete
angular.module('lifeEngage.LookupService', []).factory(
		"LookupService", ['UserDetailsService','$location','$translate','$rootScope','$http',
		function(UserDetailsService, $location, $translate, $rootScope, $http) {
			var lifeEngage = new Object();

			lifeEngage.lookupCall = function(transactionObj, successCallback,
					errorCallback) {
				DbOperationsUtility.lookupCall(transactionObj, successCallback,
						function(data, status){
					if(status=='401'){
						$location.path('/login');
						$rootScope.$apply();
						$rootScope.showHideLoadingImage(false);
						$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
							$rootScope.showHideLoadingImage(false);
							});
					}else{
						errorCallback(data);
					}
				}, UserDetailsService.getUserDetailsModel().options, $http);
			};
			return lifeEngage;

		}]);
