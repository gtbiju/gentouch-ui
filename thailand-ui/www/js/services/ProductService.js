﻿/*
 *Copyright 2015, LifeEngage 
 */



//DataService for save,retrieve, sysn
angular.module('lifeEngage.ProductService', []).factory(
		"ProductService", ['UserDetailsService','$location','$translate','$rootScope','$http',
		function(UserDetailsService, $location, $translate, $rootScope, $http) {
			var lifeEngage = new Object();

			lifeEngage.getProductDetails = function(transactionObj,
					successCallback, errorCallback) {
				productServiceUtility.getProductDetails(transactionObj,
						successCallback, errorCallback);
			};

			lifeEngage.getAllActiveProducts = function(transactionObj,
					successCallback, errorCallback) {
				productServiceUtility.getAllActiveProducts(transactionObj,
						successCallback, function(data, status){
					if(status=='401'){
						$location.path('/login');
						$rootScope.$apply();
						$rootScope.showHideLoadingImage(false);
						$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
							$rootScope.showHideLoadingImage(false);
							});
					}else{
						errorCallback(data);
					}
				}, UserDetailsService.getUserDetailsModel().options, $http);
			};
			
			lifeEngage.getAllActiveProductsByFilter = function(transactionObj,
					successCallback, errorCallback) {
				productServiceUtility.getAllActiveProductsByFilter(transactionObj,
						successCallback,errorCallback,
						UserDetailsService.getUserDetailsModel().options, $http);
			};
			
			lifeEngage.getProductsByFilter = function(transactionObj,
					successCallback, errorCallback) {
				productServiceUtility.getProductsByFilter(transactionObj,
						successCallback,errorCallback,
						UserDetailsService.getUserDetailsModel().options, $http);
			};
			lifeEngage.getProduct = function(transactionObj, successCallback,
					errorCallback) {
				productServiceUtility.getProduct(transactionObj,
						successCallback, function(data, status){
					if(status=='401'){
						$location.path('/login');
						$rootScope.$apply();
						$rootScope.showHideLoadingImage(false);
						$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
							$rootScope.showHideLoadingImage(false);
							});
					}else{
						errorCallback(data);
					}
				}, UserDetailsService.getUserDetailsModel().options, $http);
			};

			return lifeEngage;

		}]);
