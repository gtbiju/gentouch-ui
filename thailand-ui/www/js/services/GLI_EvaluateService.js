angular.module('lifeEngage.GLI_EvaluateService', [])
		.service("GLI_EvaluateService",['$translate','$rootScope','$http',
		function($translate,$rootScope,$http){
			this.evaluateString = function($scope,value) {
                var val;

                               var variableValue = $scope;
                               if (value.indexOf('.') != -1) {
                                   val = value.split('.');
                                   if (val[0] == '$scope' || val[0] == 'scope') {
                                       val.shift();
                                   }
                                   for (var i in val) {
                                                if(typeof variableValue != 'undefined'){
                                                       if (val[i].indexOf('[') != -1) {
                                                              var indexValue = val[i].substring(val[i].indexOf('[') + 1, val[i].indexOf(']'));
                                                              var arrayObject = val[i].split('[');
                                                              if (variableValue[arrayObject[0]].hasOwnProperty(indexValue)) {
                                                                     variableValue = variableValue[arrayObject[0]][indexValue];
                                                              } else if (!$.isNumeric(value) && typeof $scope[indexValue] != 'undefined') {
                                                                     var indexEval = $scope[indexValue];
                                                                     variableValue = variableValue[arrayObject[0]][indexEval];
                                                              } else {
                                                                     variableValue = variableValue[arrayObject[0]][indexValue];
                                                              }

                                                       } else {
                                                              variableValue = variableValue[val[i]];
                                                       }
                                                }
                                   }
                               } else {
                                   variableValue = value;
                               }
                               return variableValue;
                           };
						   
						   this.valuateString=function(value){
							  var val;
                              var variableValue;
							  if (value.indexOf('.') != -1) {
                                   val = value.split('.');
								    for(var i in val){
										if(i==0){
											variableValue =val[i];
										}
										else{
											variableValue = variableValue[val[i]];
										}
                                    }
                                    return variableValue;
							  
						   }
						   }
						   this.evaluateModelValueFromString = function($scope, modelName, start, end, init) {
                               var variableName = modelName;
                               var evaluatedData;
                               var chk = false;
                               while (modelName.indexOf('[', start) != -1) {
                                   start = modelName.indexOf('[', start);
                                   end = modelName.indexOf(']', end + 1);
                                   var value = modelName.substring(start, end + 1);
                                   var startingCount = (value.match(/\[/g) || []).length;
                                   var closingCount = (value.match(/\]/g) || []).length;
                                   variableName = value;
                                   if (startingCount == closingCount) {
                                       variableName = variableName.substring(1, variableName.length - 1);
                                       returnedData = this.evaluateModelValueFromString($scope, variableName, 0, 0, false);
                                       modelName = modelName.substring(0, start + 1) + String(returnedData) + modelName.substring(end, modelName.length + 1);
                                       variableName = modelName;
                                       start = modelName.indexOf(']', start);
                                       end = start + 1;

                                   } else {
                                       variableName = this.evaluateModelValueFromString($scope, modelName, start, end, init);
                                       evaluatedData = variableName;
                                       chk = true;
                                       break;
                                   }
                               }

                               if (init) {
                                   evaluatedData = variableName;
                               } else {
                                   if (!chk) {
                                       evaluatedData = this.evaluateString($scope,variableName);
                                   }
                               }

                               return evaluatedData;
                           };
                           //Method to evaluate value from model(model name is passed as string)
                           this.evaluateModel = function($scope,model){
                                  var modelArray = model.split('.');
                                  if(modelArray.length == 1 || ((modelArray[0] == '$scope' || modelArray[0] == 'scope') &&  modelArray.shift().length == 1)){
                                         return $scope[modelArray[0]];
                                  } else {
                                         return this.evaluateModelValueFromString($scope,model,0,0,false);
                                  }
                           };
                           //Method to assign value to model (model name is passed as string)
                           this.bindModel = function($scope, model, value) {
                               var variable = this.evaluateModelValueFromString($scope, model, 0, 0, true);
                               var _lastIndex = variable.lastIndexOf('.');
                               var parentModel;
                               var remain_parentModel;
                               if (typeof variable != 'undefined' && _lastIndex > 0) {
                                   parentModel = variable.substring(0, _lastIndex);
                                   parentModel = this.evaluateModelValueFromString($scope, parentModel, 0, 0, false);
                                   remain_parentModel = variable.substring(_lastIndex + 1, variable.length);
                                   if (remain_parentModel.indexOf('[') != -1) {
                                       var indexValue = remain_parentModel.substring(remain_parentModel.indexOf('[') + 1, remain_parentModel.indexOf(']'));
                                       var arrayObject = remain_parentModel.split('[');
                                       if (parentModel[arrayObject[0]].hasOwnProperty(indexValue)) {
                                           parentModel[arrayObject[0]][indexValue] = value;
                                       } else if (!$.isNumeric(value) && typeof $scope[indexValue] != 'undefined') {
                                           parentModel[arrayObject[0]][$scope[indexValue]] = value;
                                       } else {
                                           parentModel[arrayObject[0]][indexValue] = value;
                                       }

                                   } else {
                                       parentModel[remain_parentModel] = value;
                                   }
                               } else {
                                  if(typeof variable != 'undefined' && $scope[variable]){
                                        $scope[variable] = value;
                                  }
                                  
                               }
                           };
                     this.stringValuate = function(value) {
								var val;

                               var variableValue;
                               if (value.indexOf('.') != -1) {
                                   val = value.split('.');
                                   if (val[0] == '$scope' || val[0] == 'scope') {
                                       val.shift();
                                   }
                                   for (var i in val) {
									   if(i==0){
										   variableValue =val[i];
									   }
									   else{
                                                if(typeof variableValue != 'undefined'){
                                                       if (val[i].indexOf('[') != -1) {
                                                              var indexValue = val[i].substring(val[i].indexOf('[') + 1, val[i].indexOf(']'));
                                                              var arrayObject = val[i].split('[');
                                                              if (variableValue[arrayObject[0]].hasOwnProperty(indexValue)) {
                                                                     variableValue = variableValue[arrayObject[0]][indexValue];
                                                              } else if (!$.isNumeric(value) && typeof $scope[indexValue] != 'undefined') {
                                                                     var indexEval = $scope[indexValue];
                                                                     variableValue = variableValue[arrayObject[0]][indexEval];
                                                              } else {
                                                                     variableValue = variableValue[arrayObject[0]][indexValue];
                                                              }

                                                       } else {
                                                              variableValue = variableValue[val[i]];
                                                       }
                                                }
                                   }
                               } 
						   }else {
                                   variableValue = value;
                               }
                               return variableValue;
                           };
                          
        
		
	}]);
		
		