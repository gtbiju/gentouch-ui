//DataService for agentprofile
angular.module('lifeEngage.SslCertificate', [])
.config(['$httpProvider', function($httpProvider) {
    if (rootConfig.sslCheckEnabled && rootConfig.isDeviceMobile) {
     
        $httpProvider.interceptors.push("sslCertificateInterceptor");
    }
}])
.factory("sslCertificateInterceptor", ["$q","$rootScope","$location","$translate", function ($q, $rootScope, $location, $translate) {

    return {
        request: function(config) {
            var deferred = $q.defer();
            
            // For Production
            //var server = "https://gentouch.generali.co.th";
            //var fingerprint = "33 9E 05 10 74 C6 09 DD 7E 86 38 BC 57 98 14 58 D1 4A 72 E9 96 49 6D B8 A4 B1 7E F6 71 CC 46 8B";
            
            // For SIT and UAT                          
            var server = "https://gentouch-sit.generali.co.th";
            var fingerprint = "20 d1 ff 65 e1 86 f4 22 09 64 9f 31 0a 0c d6 13 49 21 dc 79";
            
            function successCallback(message) {
                //console.log(message);

                // Message is always: CONNECTION_SECURE.
                // Now do something with the trusted server.
                deferred.resolve(config);
            }

            function errorCallback(message) {
                //console.log(message);
                
                if (message === "CONNECTION_NOT_SECURE") {
                    // There is likely a man in the middle attack going on, be careful!
                    deferred.reject(config);
                    $rootScope.showHideLoadingImage(false);//please use secure connection
                    $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "you are not allowed" + message),translateMessages($translate, "fna.ok"),function(){
                        $location.path('/login');
                        $rootScope.showHideLoadingImage(false);
                    });
                } else if (message.indexOf("CONNECTION_FAILED") >- 1) {
                    // There was no connection (yet). Internet may be down. Try again (a few times) after a little timeout.
                    deferred.reject(config);
                }
            }

            var url = (config.url).toLowerCase();
            if(url.indexOf("http://") != -1 || url.indexOf("https://") != -1) {
                CDVPLUGIN.SSLCertificateChecker.check(successCallback, errorCallback, config.url, fingerprint);
            }
            else {
                deferred.resolve(config);
            }
            return deferred.promise;
        },

        requestError: function(config) {
            return config;
        },

        response: function(res) {
            return res;
        },

        responseError: function(res) {
            return $q.reject(res);
        }
    }
}])