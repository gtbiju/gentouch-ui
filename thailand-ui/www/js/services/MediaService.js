angular
		.module('lifeEngage.MediaService', [])
		.factory(
				"MediaService",
				[
						'$window',
						function() {
							var lifeEngage = new Object();

							lifeEngage.playVideo = function($scope, fileName) {
								if ((rootConfig.isDeviceMobile)
										&& rootConfig.isOfflineDesktop) {
									$scope.videoSource = rootConfig.configAndContentPath
											+ '/' + fileName;
									$scope.show_hide_popup = true;
									$scope.showPDF = false;
								} else if (!(rootConfig.isDeviceMobile)
										&& !rootConfig.isOfflineDesktop) {
									$scope.videoSource = rootConfig.mediaURL
											+ fileName;
                                    document.getElementById("le_prod_video").load();
									$scope.show_hide_popup = true;
									$scope.showPDF = false;
								} else {
									var fileFullPath = "";
									window.plugins.LEFileUtils
											.getApplicationPath(function(path) {
												var documentsPath = path; // contains
																			// till
																			// the
																			// app
																			// package
																			// name

												fileFullPath = "file://"
														+ documentsPath + "/"
														+ fileName;

												window.plugins.videoPlayer
														.play(fileFullPath);
											}, function(e) {
												console.log(e);
											});

								}
							};

							lifeEngage.openPDF = function($scope, $window,
									fileName) {
								if ((rootConfig.isDeviceMobile)
										&& rootConfig.isOfflineDesktop) {
									var gui = require('nw.gui');
									gui.Shell
											.openExternal(rootConfig.configAndContentPath
													+ '/' + fileName);
								} else if (!(rootConfig.isDeviceMobile)
										&& !rootConfig.isOfflineDesktop) {
									var myPdfUrl = rootConfig.mediaURL
											+ fileName;
									$window.open(myPdfUrl);
								} else {
									var fileFullPath = "";
									window.plugins.LEFileUtils
											.getApplicationPath(function(path) {
												var documentsPath = path; // contains
																			// till
																			// the
																			// app
																			// package
																			// name
												fileFullPath = "file://"
														+ documentsPath + "/"
														+ fileName;
												window.plugins.pdfViewer
														.showPdf(fileFullPath);

											}, function(e) {
												console.log(e)
											});
								}
							};

							return lifeEngage;
						} ]);