﻿/*GeoFenceService for geofence related functionalities
 * Author : Sajith M
 * created date  : 30/11/2016
 */
angular
		.module('lifeEngage.GeoFenceService', [])
		.factory(
				"GeoFenceService",
				[
						'UtilityService',
						'UserDetailsService',

						function(UtilityService,  UserDetailsService) {
							var GeoFenceService = new Object();
							    // Find in which branch the agent entered using his current location
								GeoFenceService.findEnteredBranch = function(currentLocation,successCall,errorCall){
									var mappedBranchList = UserDetailsService.getMappedBranches();
									var enteredBranch = null;
                                    if(typeof mappedBranchList!="undefined"){
                                    	for(var i = 0 ;i < mappedBranchList.length ;i++){
                                    		if(mappedBranchList[i].latitude && mappedBranchList[i].longitude && mappedBranchList[i].radius){
											   var distance =  UtilityService.calculateDistanceBtwTwoLatLong(currentLocation.coords.latitude ,currentLocation.coords.longitude, mappedBranchList[i].latitude,mappedBranchList[i].longitude);
												if(distance <= mappedBranchList[i].radius){
													enteredBranch = mappedBranchList[i];
													successCall(enteredBranch);
												}
											}
                                    	}
                                    	if(enteredBranch==null){
                                    		errorCall();
                                    	}
                                    }else{
                                    	errorCall();
                                    }
								};
							return GeoFenceService;
			            }
			    ]);