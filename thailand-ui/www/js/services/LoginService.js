/*
 *Copyright 2015, LifeEngage 
 */



/**
 * 
 */

angular.module('lifeEngage.LoginService', []).factory("LoginService", function () {
    var lifeEngage = new Object();
    
    lifeEngage.saveToken = function (userdetailsService, successCallback, errorCallback) {
    	loginUtility.saveToken(userdetailsService, successCallback, errorCallback);       
    };
    lifeEngage.fetchToken = function (userdetailsService, successCallback, errorCallback) {
    	loginUtility.fetchToken(userdetailsService, successCallback, errorCallback);
    };
   lifeEngage.checkAgentIdExistingInDb = function (user, successCallback, errorCallback) {
    	loginUtility.checkAgentIdExistingInDb(user, successCallback, errorCallback);
    };
    
    lifeEngage.checkDuplicateAgentInDb = function (user, successCallback, errorCallback) {
    	loginUtility.checkDuplicateAgentInDb(user, successCallback, errorCallback);
    };
    
    return lifeEngage;

});