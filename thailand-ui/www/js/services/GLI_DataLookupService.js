/*
 *Copyright 2015, LifeEngage 
 */




//DataLookupService for GLI specific changes

angular
		.module('lifeEngage.GLI_DataLookupService', []).factory(
				"GLI_DataLookupService",['UserDetailsService','$location','$translate','$rootScope','DataLookupService','$http',
				function(UserDetailsService, $location, $translate, $rootScope,DataLookupService, $http) {
					var lifeEngage = {};
					lifeEngage.dataLookupCache = [];
					DataLookupService['getLookUpData'] = function(transactionObj,
							successCallback, errorCallback, isCacheable) {
						
						if (localStorage["locale"]) { // Specific to GLI locale in localstorage. Not generic as in LE
							locale = localStorage["locale"].split("_");
							transactionObj.language = locale[0];
							transactionObj.country = locale[1]?locale[1]:"";
						} else {
							transactionObj.language = "en";
							transactionObj.country = "US";
						}
						if (isCacheable && lifeEngage.dataLookupCache[transactionObj.type + "_" + transactionObj.country 
              			        + "_" + transactionObj.language + "_" + ((transactionObj.code)?transactionObj.code:"")] && 
              			      lifeEngage.dataLookupCache[transactionObj.type + "_" + transactionObj.country 
              			        + "_" + transactionObj.language + "_" + ((transactionObj.code)?transactionObj.code:"")].length > 0) {
              				successCallback(lifeEngage.dataLookupCache[transactionObj.type + "_" + transactionObj.country + "_" + transactionObj.language + "_" + ((transactionObj.code)?transactionObj.code:"")]);
              			} else {
							dataLookupServiceUtility
								.getLookUpData(
										transactionObj,
										function (dataLookUps){
			 								if(isCacheable) {
			 									lifeEngage.dataLookupCache[transactionObj.type + "_" 
								           		+ transactionObj.country + "_" 
								           		+ transactionObj.language + "_" + ((transactionObj.code)?transactionObj .code:"")] = dataLookUps;
											}
			 								successCallback(dataLookUps);
			 							},
										function(data, status) {
											if (status == '401') {
												$location.path('/login');
												$rootScope.$apply();
												$rootScope.showHideLoadingImage(false);
												$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
													$rootScope.showHideLoadingImage(false);
													});
											} else {
												errorCallback(data);
											}
										}, UserDetailsService.getUserDetailsModel().options, $http);
						}
					};
					DataLookupService['getMultipleLookUpData'] = function(typeNameList, successCallback, errorCallback) {
						var cacheableArray = [];
			        	var typeNameListNew = [];
			        	var result =[];
			        	
						if (localStorage["locale"]) {// Specific to GLI locale in localstorage. Not generic as in LE
							locale = localStorage["locale"].split("_");
							language = locale[0];
							country = locale[1]?locale[1]:"";
						} else {
							language = "en";
							country = "US";
						}
						typeNameList.forEach(function(typeobj) {
			        		typeobj.language = language;
			        		typeobj.country = country;
			        		
			        		if(typeobj.isCacheable) {
								cacheableArray.push(typeobj.type);
								if( lifeEngage.dataLookupCache[typeobj.type + "_" 
				     			        + country + "_" + language + "_" 
				     			        + ""] && lifeEngage.dataLookupCache[typeobj.type + "_" 
				     			        + country + "_" + language + "_" 
				     			        + ""].length > 0) {
									
									typeobj.lookUps = lifeEngage.dataLookupCache[typeobj.type + "_" 
									    + country + "_" + language + "_" + ""]
									result.push(typeobj);
								} else {						
									typeNameListNew.push(typeobj);
								}
							} else {
								typeNameListNew.push(typeobj);
							}
							
			        	});
						
						if( typeNameListNew.length > 0 ){
							dataLookupServiceUtility.getMultipleLookUpData(typeNameListNew, function (dataLookUps){
				        		dataLookUps.forEach(function(dataLookUpObj) {
									if (cacheableArray.indexOf(dataLookUpObj.type) > -1) {
										lifeEngage.dataLookupCache[dataLookUpObj.type + "_" 
										           + dataLookUpObj.country + "_" 
										           + dataLookUpObj.language + "_"] = dataLookUpObj.lookUps;
									}
									result.push(dataLookUpObj);
								});							
								successCallback(result);
				        	},errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
				        }else{
				        	successCallback(result);
				        }
					};
					
					DataLookupService['clearDataLookupCache'] = function() {
						lifeEngage.dataLookupCache =[];
					};
					return DataLookupService;
				}]);