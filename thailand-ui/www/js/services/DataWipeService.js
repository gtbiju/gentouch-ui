//SyncService for all sync related functionalities
angular
		.module('lifeEngage.DataWipeService', [])
		.factory(
				"DataWipeService",
				[
					'UtilityService',
					'DataService',
					'PlatformInfoService',
					'PersistenceMapping',
					'$http',
					function(UtilityService, DataService, PlatformInfoService, PersistenceMapping, $http) {
						var LEDataWipeService = {};

						LEDataWipeService.deleteAllConfigFiles = function(reason, successCallBack, errorCallBack) {
                            if (rootConfig.isDeviceMobile) {
                                DataService.retrieveLoggedInAgentIds(function(userIds){
                                    // Call LE FileUtils method for deleting all config files
                                    window.plugins.LEFileUtils.deleteAllConfigFiles(function() {
                                        if (UtilityService.checkConnection()) {
                                            var dataWipeAuditObj = _dataWipeAuditObj();
                                            if (userIds.length > 0) {
                                                dataWipeAuditObj.UserIds = userIds;
                                                dataWipeAuditObj.Reason = reason;
                                                _getPlatformInfoAndProceedAudit(dataWipeAuditObj, successCallBack, errorCallBack);
                                            } else {
                                                _log("No Users logged in to tablet. So skipping data wipe audit");
                                                successCallBack();
                                            }

                                        } else {
                                            _log("Since no internet connectivity, system is not able to call the datawipe audit service");
                                            successCallBack();
                                        }
                                    }, errorCallBack);
                                }, function (){
                                    _log("Error occurred during retrieval of available agent IDs. So skipping data wipe audit");
                                     window.plugins.LEFileUtils.deleteAllConfigFiles(successCallBack,errorCallBack);

                                });
                            } else {
                                _log("Application is not running in mobile/desktop offline. Still calling the data wipe API : deleteAllConfigFiles");
                                errorCallBack("Application is not running in mobile/desktop offline. Still calling the data wipe API: deleteAllConfigFiles");
                            }
						},
						LEDataWipeService.deleteAgentSpecificConfigFiles = function(agentId, reason, successCallBack, errorCallBack) {
							if (rootConfig.isDeviceMobile) {
                                DataService.checkAgentIdExists(agentId, function(agentIdCount){
                                    if(agentIdCount[0].userCount > 0) {
                                        DataService.retrieveRequirementFileNames(agentId, function(filesList) {
                                            // Call dataservice methods for deleting database details for particular Agent.
                                            DataService.deleteAgentSpecificTransactionData(agentId, function(){
                                                window.plugins.LEFileUtils.deleteUploadedFiles(filesList, function() {

                                                    if (UtilityService.checkConnection()) {
                                                        var dataWipeAuditObj = _dataWipeAuditObj();
                                                        userIdObj = {"userId" : agentId };
                                                        dataWipeAuditObj.UserIds.push(userIdObj);
                                                        dataWipeAuditObj.Reason = reason;
                                                        _getPlatformInfoAndProceedAudit(dataWipeAuditObj, successCallBack, errorCallBack);
                                                    } else {
                                                        _log("Since no internet connectivity, system is not able to call the datawipe audit service");
                                                        successCallBack();
                                                    }
                                                }, function () {
                                                    _log("Error occurred during uploaded file deletion.");
                                                    errorCallBack("Error occurred during uploaded file deletion.");
                                                });
                                            }, function(){
                                                _log("Error occurred during agent specific transaction data removal.");
                                                errorCallBack("Error occurred during agent specific transaction data removal.");
                                            });
                                        }, function() {
                                            _log("Failed to fetch requirement file names for a agent.");
                                            errorCallBack("Failed to fetch requirement file names for a agent.");
                                        });
                                    } else {
                                        _log("No user details to delete");
                                        successCallBack();
                                    }

                                }, function (){
                                    _log("Failed to check agent code existence");
                                    errorCallBack("Failed to check agent code existence");
                                });
                            } else {
                                _log("Application is not running in mobile/desktop offline. Still calling the data wipe API : deleteAgentSpecificConfigFiles");
                                errorCallBack("Application is not running in mobile/desktop offline. Still calling the data wipe API : deleteAgentSpecificConfigFiles");
                            }
						};
						function _log(message){
							if (rootConfig.isDebug) {
							}
						};
						function _dataWipeAuditObj(){
						    return {
                                "DeviceDetails": {
                                    "platform":"",
                                    "version":"",
                                    "model" : "",
                                    "deviceUUID" : ""
                                },
                                "Reason" : "",
                                "UserIds" :[]
                            };
                        };
                        function _getPlatformInfoAndProceedAudit(dataWipeAuditObj, successCallBack, errorCallBack){
                            if (rootConfig.isDeviceMobile && !rootConfig.isOfflineDesktop) {
                                PlatformInfoService.getPlatformInfo(function(platFormInfo) {
                                    dataWipeAuditObj.DeviceDetails.platform =  platFormInfo.platform;
                                    dataWipeAuditObj.DeviceDetails.version =  platFormInfo.version;
                                    dataWipeAuditObj.DeviceDetails.model =  platFormInfo.model;
                                    dataWipeAuditObj.DeviceDetails.deviceUUID =  platFormInfo.uuid;

                                    _processDataWipeAudit(dataWipeAuditObj, successCallBack, errorCallBack);
                                }, function(error){
                                    _log("Failed to Fetch the Devide Details. So calling the data wipe audit without device information");
                                    _processDataWipeAudit(dataWipeAuditObj, successCallBack, errorCallBack);
                                });
                            } else {
                                dataWipeAuditObj.DeviceDetails.platform =  "Offline Desktop";
                                dataWipeAuditObj.DeviceDetails.version =  "";
                                dataWipeAuditObj.DeviceDetails.model =  "";
                                dataWipeAuditObj.DeviceDetails.deviceUUID =  "";

                                _processDataWipeAudit(dataWipeAuditObj, successCallBack, errorCallBack);
                            }
                        };

                        function _processDataWipeAudit(dataWipeAuditObj, successCallBack, errorCallBack) {
                            PersistenceMapping.clearTransactionKeys();
                            var transactionObj = PersistenceMapping.mapScopeToPersistence({});
                            transactionObj.TransactionData=dataWipeAuditObj;
                            transactionObj.Type = 'DataWipeAudit';

                            var options={};

                            DataService.saveDataWipeAudit(transactionObj, function(){
                                successCallBack();
                            }, function (error) {
                                _log("Error Occured during data wipe audit : "+ error);
                                successCallBack();
                            }, options);
                        }
						return LEDataWipeService;
					} ]);