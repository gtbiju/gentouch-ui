/*
 *Copyright 2015, LifeEngage 
 */



/*
 * Services for mapping the keys of the transaction Object from service and to the service
 */
angular
		.module('lifeEngage.PersistenceMapping', [])
		.factory(
				"PersistenceMapping",
				function($http, $rootScope, FnaVariables, IllustratorVariables,
						LmsVariables,UserDetailsService) {

					var PersistenceMapping = {

						"Key1" : "",
						"Key2" : "",
						"Key3" : "",
						"Key4" : "",
						"Key5" : "",
						"Key6" : "",
						"Key7" : "",
						"Key8" : "",
						"Key9" : "",
						"Key10" : "",
						"Key11" : "",
						"Key12" : "",
						"Key13" : "",
						"Key14" : "",
						"Key15" : "",
						"Key16" : "",
						"Key17" : "",
						"Key18" : "",
						"Key19" : "",
						"Key20" : "",
						"Key21" : "",
						"Key22" : "",
						"Key23" : "",
						"Key24" : "",
						"Key25" : "",
						"Key26" : "",
						"Key27" : "",
						"Key28" : "",
						"Key29" : "",
						"Key30" : "",
						"Key31" : "",
						"Key32" : "",
						"Key33" : "",
						"Key34" : "",
						"Key35" : "",
						"Key36" : "",
						"Key37" : "",
						"Key38" : "",
						"Key39" : "",
						"Key40" : "",
						"Type" : "",
						"creationDate" : "",
						"modifiedDate" : "",
						"TransactionId" : "",
						"TransTrackingID" : "",
						"filter" : "",
						"transactionData" : {},
						"dataIdentifyFlag" : true,
						"mapScopeToPersistence" : function(data) {
							var transactionData = angular.copy(data);
							var formattedDate = getFormattedDate();
							var transactionObj = {};
							if ((rootConfig.isDeviceMobile)) {
								transactionObj.Id = $rootScope.transactionId != null
										&& $rootScope.transactionId != 0 ? $rootScope.transactionId
										: null;
								if (PersistenceMapping.Type == "LMS") {
									transactionObj.Id = LmsVariables.lmsKeys.Id;
									if (transactionObj.Id == 0) {
										delete transactionObj.Id;
									}

								}
								if (PersistenceMapping.dataIdentifyFlag) {
									transactionObj.TransactionData = angular
											.toJson(transactionData);
								} else {
									transactionObj.TransactionData = transactionData;
								}
							} else {
								//transactionObj.TransTrackingID = PersistenceMapping.TransTrackingID;
								transactionObj.TransactionData = transactionData;
							}
							transactionObj.TransTrackingID = PersistenceMapping.TransTrackingID;
							transactionObj.Type = PersistenceMapping.Type;
							//Upgrade related modification
							if (typeof transactionObj.TransactionData == "object") {
								if ((rootConfig.isDeviceMobile)) {
									if (PersistenceMapping.dataIdentifyFlag) {
										transactionObj.TransactionData = angular
												.toJson(transactionObj.TransactionData);
									}

								}
							}
								
						    //Upgrade related modification	
							/*transactionObj.Key1 = (LmsVariables.lmsKeys.Key1 != 0) ? LmsVariables.lmsKeys.Key1
									: "";*/
							transactionObj.Key1 = PersistenceMapping.Key1;
							transactionObj.Key2 = PersistenceMapping.Key2;
							transactionObj.Key3 = PersistenceMapping.Key3;
							transactionObj.Key4 = PersistenceMapping.Key4;
							transactionObj.Key5 = PersistenceMapping.Key5;
							transactionObj.Key6 = PersistenceMapping.Key6;
							transactionObj.Key7 = PersistenceMapping.Key7;
							transactionObj.Key8 = PersistenceMapping.Key8;
							transactionObj.Key9 = PersistenceMapping.Key9;
							transactionObj.Key10 = PersistenceMapping.Key10;
							transactionObj.Key11 = $rootScope.username; // AgentId is
							// hardcoded
							transactionObj.Key12 = PersistenceMapping.Key12;

							if (PersistenceMapping.Type == "Observation") {
								transactionObj.Key13 = formattedDate;
								delete transactionObj.Id;
							} else if (PersistenceMapping.Type == "eApp"
									|| PersistenceMapping.Type == "illustration"
									|| PersistenceMapping.Type == "FNA") {
								
									if ($rootScope.transactionId == null
											|| $rootScope.transactionId == 0) {
										transactionObj.creationDate = formattedDate;
										transactionObj.Key13 = formattedDate;
									}							   
									
									if (PersistenceMapping.creationDate != "") {
										transactionObj.creationDate = PersistenceMapping.creationDate;
										transactionObj.Key13 = PersistenceMapping.creationDate;
									}
								
							} else if (PersistenceMapping.Type == "LMS") {
								if ((rootConfig.isDeviceMobile)) {
									if (LmsVariables.lmsKeys.Key1 == null
											|| LmsVariables.lmsKeys.Key1 == 0) {
										transactionObj.creationDate = formattedDate;
										transactionObj.Key13 = formattedDate;
									}
								} else {
									if (PersistenceMapping.creationDate != "") {
										transactionObj.creationDate = PersistenceMapping.creationDate;
										transactionObj.Key13 = PersistenceMapping.creationDate;
									}
								}
							}

							transactionObj.creationDate=PersistenceMapping.creationDate;
							transactionObj.modifiedDate = formattedDate;
							transactionObj.Key13 = PersistenceMapping.Key13;
							transactionObj.Key14 = PersistenceMapping.Key14;
                            transactionObj.Key14 = formattedDate;
							transactionObj.Key15 = PersistenceMapping.Key15;
							transactionObj.Key16 = PersistenceMapping.Key16;
							transactionObj.Key17 = PersistenceMapping.Key17;
							transactionObj.Key18 = PersistenceMapping.Key18;
							transactionObj.Key19 = PersistenceMapping.Key19;
							transactionObj.Key20 = PersistenceMapping.Key20;
							transactionObj.Key21 = PersistenceMapping.Key21;
							transactionObj.Key22 = PersistenceMapping.Key22;
							transactionObj.Key23 = PersistenceMapping.Key23;
							transactionObj.Key24 = PersistenceMapping.Key24;
							transactionObj.Key25 = PersistenceMapping.Key25;
							transactionObj.Key26 = PersistenceMapping.Key26;
							transactionObj.Key27 = PersistenceMapping.Key27;
							transactionObj.Key28 = PersistenceMapping.Key28;
							transactionObj.Key29 = PersistenceMapping.Key29;
							transactionObj.Key30 = PersistenceMapping.Key30;
							transactionObj.Key31 = PersistenceMapping.Key31;
							transactionObj.Key32 = PersistenceMapping.Key32;
							transactionObj.Key33 = PersistenceMapping.Key33;
							transactionObj.Key34 = PersistenceMapping.Key34;
							transactionObj.Key35 = PersistenceMapping.Key35;
							transactionObj.Key36 = PersistenceMapping.Key36;
							transactionObj.Key37 = PersistenceMapping.Key37;
							transactionObj.Key38 = PersistenceMapping.Key38;
							transactionObj.Key39 = PersistenceMapping.Key39;
							transactionObj.Key40 = PersistenceMapping.Key40;
							/*
							 * Commented For Generali Thailand
							 * if(PersistenceMapping.Type == "eApp"){
								transactionObj.Key26 = PersistenceMapping.Key26;
							}*/
							/*
							 * Setting Key11 as reporting Agent, on RM login. Need to change in case of multiple agents
							 */
							var usrDetails = UserDetailsService.getUserDetailsModel();
							if(usrDetails.agentType=="BranchUser"){
								transactionObj.Key25 = $rootScope.username;
							}
							
							return transactionObj;
						},
						"mapPersistenceToScope" : function(dbObject) {
							PersistenceMapping.clearTransactionKeys();
							PersistenceMapping.Key1 = dbObject[0].Key1;
							//FnaVariables.fnaId = dbObject[0].Key2;
							PersistenceMapping.Key2 = dbObject[0].Key2;
							//IllustratorVariables.illustratorId = dbObject[0].Key3;
							PersistenceMapping.Key3 = dbObject[0].Key3;
							PersistenceMapping.Key4 = dbObject[0].Key4;
							PersistenceMapping.Key5 = dbObject[0].Key5;
							PersistenceMapping.Key6 = dbObject[0].Key6;
							PersistenceMapping.Key7 = dbObject[0].Key7;
							PersistenceMapping.Key8 = dbObject[0].Key8;
							PersistenceMapping.Key9 = dbObject[0].Key9;
							PersistenceMapping.Key10 = dbObject[0].Key10;
							PersistenceMapping.Key11 = dbObject[0].Key11;
							PersistenceMapping.Key12 = dbObject[0].Key12;
							PersistenceMapping.Key13 = dbObject[0].Key13;
							PersistenceMapping.Key14 = dbObject[0].Key14;
							PersistenceMapping.Key15 = dbObject[0].Key15;
							PersistenceMapping.creationDate = dbObject[0].creationDate;
							PersistenceMapping.modifiedDate = dbObject[0].modifiedDate;                            
							PersistenceMapping.Key21 = dbObject[0].Key21;
							PersistenceMapping.Key24 = dbObject[0].Key24;
							PersistenceMapping.Key25 = dbObject[0].Key25;
							PersistenceMapping.Key25 = dbObject[0].Key25;
							PersistenceMapping.Key25 = dbObject[0].Key25;
							PersistenceMapping.Key26 = dbObject[0].Key26;
							PersistenceMapping.Key27 = dbObject[0].Key27;
							PersistenceMapping.Key28 = dbObject[0].Key28;
							PersistenceMapping.Key29 = dbObject[0].Key29;
							PersistenceMapping.Key30 = dbObject[0].Key30;
							PersistenceMapping.Key31 = dbObject[0].Key31;
							PersistenceMapping.Key32 = dbObject[0].Key32;
							PersistenceMapping.Key33 = dbObject[0].Key33;
							PersistenceMapping.Key34 = dbObject[0].Key34;
							PersistenceMapping.Key35 = dbObject[0].Key35;
							PersistenceMapping.Key36 = dbObject[0].Key36;
							PersistenceMapping.Key37 = dbObject[0].Key37;
							PersistenceMapping.Key38 = dbObject[0].Key38;
							PersistenceMapping.Key39 = dbObject[0].Key39;
							PersistenceMapping.Key40 = dbObject[0].Key40;
							
							/*
							 * Commented For Generali Thailand
							 * if(dbObject[0].Key26 != null){
									PersistenceMapping.Key26 = dbObject[0].Key26;
								}
							*/
							PersistenceMapping.TransTrackingID = dbObject[0].TransTrackingID;
							if (dbObject[0].Key16 != null
									&& dbObject[0].Key16 == "SUCCESS") {
								PersistenceMapping.Key16 = "Synced";
							}
							PersistenceMapping.Key17 = dbObject[0].Key17;
							PersistenceMapping.transactionData = dbObject[0].TransactionData;
							
							//Upgrade related modification
							if ((rootConfig.isDeviceMobile) && dbObject[0].Id != null) {
								$rootScope.transactionId = dbObject[0].Id;
							}
						},
						"clearTransactionKeys" : function() {

							PersistenceMapping.Key1 = "";
									PersistenceMapping.Key2 = "";
									PersistenceMapping.Key3 = "";
									PersistenceMapping.Key4 = "";
									PersistenceMapping.Key5 = "";
									PersistenceMapping.Key6 = "";
									PersistenceMapping.Key7 = "";
									PersistenceMapping.Key8 = "";
									PersistenceMapping.Key9 = "";
									PersistenceMapping.Key10 = "";
									PersistenceMapping.Key11 = "";
									PersistenceMapping.Key12 = "";
									PersistenceMapping.Key13 = "";
									PersistenceMapping.Key14 = "";
									PersistenceMapping.Key15 = "";
									PersistenceMapping.Key16 = "";
									PersistenceMapping.Key17 = "";
									PersistenceMapping.Key18 = "";
									PersistenceMapping.Key19 = "";
									PersistenceMapping.Key20 = "";
									PersistenceMapping.Key21 = "";
									PersistenceMapping.Key22 = "";
									PersistenceMapping.Key23 = "";
									PersistenceMapping.Key24 = "";
									PersistenceMapping.Key25 = "";
									PersistenceMapping.Key26 = "";
									PersistenceMapping.Key27 = "";
									PersistenceMapping.Key28 = "";
									PersistenceMapping.Key29 = "";
									PersistenceMapping.Key30 = "";
									PersistenceMapping.Key31 = "";
									PersistenceMapping.Key32 = "";
									PersistenceMapping.Key33 = "";
									PersistenceMapping.Key34 = "";
									PersistenceMapping.Key35 = "";
									PersistenceMapping.Key36 = "";
									PersistenceMapping.Key37 = "";
									PersistenceMapping.Key38 = "";
									PersistenceMapping.Key39 = "";
									PersistenceMapping.Key40 = "";
									PersistenceMapping.Type = "";
									PersistenceMapping.creationDate = "";
									PersistenceMapping.modifiedDate = "";
									PersistenceMapping.TransactionId = "";
									PersistenceMapping.TransTrackingID = "";
									PersistenceMapping.filter = "";
									PersistenceMapping.dataIdentifyFlag = true;
						},
						"getFormattedDate" : function() {
							now = new Date();
							year = "" + now.getFullYear();
							month = "" + (now.getMonth() + 1);
							if (month.length == 1) {
								month = "0" + month;
							}
							day = "" + now.getDate();
							if (day.length == 1) {
								day = "0" + day;
							}
							hour = "" + now.getHours();
							if (hour.length == 1) {
								hour = "0" + hour;
							}
							minute = "" + now.getMinutes();
							if (minute.length == 1) {
								minute = "0" + minute;
							}
							second = "" + now.getSeconds();
							if (second.length == 1) {
								second = "0" + second;
							}
							return year + "-" + month + "-" + day + " " + hour
									+ ":" + minute + ":" + second;
						}
					};
					return PersistenceMapping;
				});
