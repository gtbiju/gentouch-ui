/*
 *Copyright 2015, LifeEngage 
 */


angular.module('lifeEngage.AutoSave', []).factory(
		"AutoSave",
		function($http, $rootScope, FnaVariables, IllustratorVariables,LmsService,LmsVariables,EappService) {

			var AutoSave = {
				"setupWatchForScope" : function(model, DataService,
						UtilityService, IllustratorService, $scope, $debounce,
						$translate, $routeParams, FnaService) {
					var saveUpdates = function(newVal, oldVal) {
                        if(!AutoSave.equals(oldVal, newVal)){
                            
                            $scope.Save();
                        }
					
					};
					 
					
				    AutoSave.destroyWatch = $scope.$watch(model,
								$debounce.timeoutMonitor(saveUpdates,
										rootConfig.autoSaveTimeDelay), true);
					
					$scope.$on('$routeChangeStart', function(next, current) {
						AutoSave.destroyWatch();
						$debounce.timeoutFlush();
					});

				},
				"equals" : function(obj1, obj2) {
					function _equals(obj1, obj2) {
						var clone = $.extend(true, {}, obj1), cloneStr = JSON
								.stringify(clone);
						return cloneStr === JSON.stringify($.extend(true,
								clone, obj2));
					}
					return _equals(obj1, obj2) && _equals(obj2, obj1);
				}
			};
			return AutoSave;
		});
