//RequirementService for all requirement related functionalities like upload, retrieve
angular.module('lifeEngage.RequirementService', []).factory(
        "RequirementService", ['UserDetailsService','$translate','$rootScope', '$http',
        function(UserDetailsService, $translate, $rootScope, $http) {
            var lifeEngageRequirementService = new Object();
            lifeEngageRequirementService.uploadDocumentFile = function(transactionObj, successCallback,
                    errorCallback) {
                RequirementUtility.uploadDocumentFile(transactionObj, successCallback, errorCallback,
                UserDetailsService.getUserDetailsModel().options, $http);
            };
            
            lifeEngageRequirementService.readFile = function(fileName, path,successCallback,
                    errorCallback) {
                RequirementUtility.readFile(fileName,path, successCallback,
                        errorCallback);
            };
            
            lifeEngageRequirementService.downLoadFile = function(transactionObj, successCallback, errorCallback) {
                RequirementUtility.downLoadFile(transactionObj, successCallback,
                        errorCallback,UserDetailsService.getUserDetailsModel().options, $http);                
            };
            
            lifeEngageRequirementService.writeFile = function(fileObj, successCallback, errorCallback) {
                RequirementUtility.writeFile(fileObj, successCallback,
                        errorCallback);                
            };
        
        return lifeEngageRequirementService;

        }]);
