//PlatformInfoService for Sending Mail, Attaching Photo, DeviceInfo
angular.module('lifeEngage.PlatformInfoService', []).factory("PlatformInfoService", function($http) {
	
	var platformInfoService = new Object();
		
	
	platformInfoService.getPlatformInfo = function(successCallback, errorCallback) {	
		
		PlatformInfoUtility.getPlatformInfo(successCallback, errorCallback);
	};	
	
	return platformInfoService;	
});