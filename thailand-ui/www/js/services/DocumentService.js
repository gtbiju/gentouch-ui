﻿/*
 *Copyright 2015, LifeEngage 
 */



//documentService for all document related functionalities like upload, browse, capture, delete
angular.module('lifeEngage.DocumentService', []).factory(
		"DocumentService", ['UserDetailsService','$location','$translate','$rootScope', '$http',
		function(UserDetailsService, $location, $translate, $rootScope, $http) {
			var lifeEngage = new Object();

			lifeEngage.browseFile = function(fileDetails, agentId,
					successCallback, errorCallback) {
				FileSelectUtility.browseFile(fileDetails, agentId,
						successCallback, errorCallback);
			},
			
			lifeEngage.copyFile = function(flag, successCallback, errorCallback,data) {
				FileSelectUtility.copyFile(flag, successCallback, errorCallback,data);
			},
			lifeEngage.copyUploadedFile = function(path,successCallback, errorCallback) {
				FileSelectUtility.copyUploadedFile(path,successCallback, errorCallback);
			},
			lifeEngage.base64AsFile = function(fileDetails,
					successCallback, errorCallback) {
				FileSelectUtility.base64AsFile(fileDetails,
						successCallback, errorCallback);
			},
		
			lifeEngage.captureFile = function(fileDetails, agentId,
					successCallback, errorCallback) {
				FileSelectUtility.captureFile(fileDetails, agentId,
						successCallback, errorCallback);
			},

			lifeEngage.openFile = function(documentPath, documentName,
					successCallback, errorCallback) {
				FileSelectUtility.openFile(documentPath, documentName,
						successCallback, errorCallback);
			},

			lifeEngage.deleteFile = function(fileToDelete, successCallback,
					errorCallback) {
				FileSelectUtility.deleteFile(fileToDelete, successCallback,	function(data, status){
					if(status=='401'){
						$location.path('/login');
						$rootScope.$apply();
						$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.validToken"),translateMessages($translate, "fna.ok"),function(){
							$rootScope.showHideLoadingImage(false);
							});
					}else{
						errorCallback(data);
					}
				}, UserDetailsService.getUserDetailsModel().options, $http);
			},

			lifeEngage.deleteTempFile = function() {
				FileSelectUtility.deleteTempFile();
			},

			lifeEngage.uploadDocuments = function(documents, docIndex, successCallback,
					errorCallback) {
				FileSelectUtility.uploadDocuments(documents, docIndex, successCallback, function(data, status){
					if(status=='401'){
						$location.path('/login');
						$rootScope.$apply();
						$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.validToken"),translateMessages($translate, "fna.ok"),function(){
							$rootScope.showHideLoadingImage(false);
							});
					}else{
						errorCallback(data);
					}
				}, UserDetailsService.getUserDetailsModel().options, $http);
			}, 
			
			lifeEngage.downloadDocument = function(docName, proposalId,
					successCallback, errorCallback) {
				FileSelectUtility.downloadDocument(docName, proposalId,
						successCallback, function(data, status){
					if(status=='401'){
						$location.path('/login');
						$rootScope.$apply();
						$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.validToken"),translateMessages($translate, "fna.ok"),function(){
							$rootScope.showHideLoadingImage(false);
							});
					}else{
						errorCallback(data);
					}
				}, UserDetailsService.getUserDetailsModel().options, $http);
			}, 
			
			lifeEngage.startDownloadForEachTransaction = function(index,
					allTransactions, successCallback, errorCallback) {
				FileSelectUtility.startDownloadForEachTransaction(index,
						allTransactions, successCallback, function(data, status){
					if(status=='401'){
						$location.path('/login');
						$rootScope.$apply();
						$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.validToken"),translateMessages($translate, "fna.ok"),function(){
							$rootScope.showHideLoadingImage(false);
							});
					}else{
						errorCallback(data);
					}
				}, UserDetailsService.getUserDetailsModel().options, $http);
			}, lifeEngage.writeFile = function(document, agentId,
					successCallback, errorCallback) {
				FileSelectUtility.writeFile(document, agentId, successCallback,
						errorCallback);
			}, lifeEngage.readFile = function(fileName, successCallback,
					errorCallback) {
				FileSelectUtility.readFile(fileName, successCallback,
						errorCallback);
			},
			
			lifeEngage.saveRequirementOnly = function(requirementObj, successCallback,
					errorCallback) {
				FileSelectUtility.saveRequirementOnly(requirementObj, successCallback, function(data, status){
					if(status=='401'){
						$location.path('/login');
						$rootScope.$apply();
						$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.validToken"),translateMessages($translate, "fna.ok"),function(){
							$rootScope.showHideLoadingImage(false);
							});
					}else{
						errorCallback(data);
					}
				}, UserDetailsService.getUserDetailsModel().options, $http);
			}
			
			return lifeEngage;

		}]);
