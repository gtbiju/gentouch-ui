﻿﻿﻿//SyncService for all sync related functionalities
angular
		.module('lifeEngage.SyncService', [])
		.factory(
				"SyncService",
				[
						'UtilityService',
						'DataService',
						'DocumentService',
						'RequirementService',
						'UserDetailsService',
						'$translate',
						'PersistenceMapping',

						function(UtilityService, DataService, DocumentService,
								RequirementService, UserDetailsService,
								$translate, PersistenceMapping) {
							var LESyncService = new Object();
							var syncToTransactionObj;
							var syncFromTransactionObj;
							var syncLength;
							var MasterSyncObject = [];
							var archiveOptions = {};
							var syncFailed = false;
							var syncWarning = false;
							var syncDocFailed = false;
							var syncDocDownloadFailed = false;
							var docUploadFailedForCurrentTrans = false;
							var docDownloadFailedForCurrentTrans = false;
							var transactionIndex = 0;
							var archieveTypes;
							var currentSyncObject = {};
							var syncToOptions;
							var syncFromOptions;
							var keyToUpdate;
							var isDebugSync = rootConfig.isDebug;
									// Initialize the sync variables
									LESyncService.initialize = function(
											syncConfig, syncCompleteCallBack,
											synprogress,syncFromPerModuleCompleteCallback,syncToPerModuleCompleteCallback,syncProgressDocCallback) {
										currentSyncObject = {};
										syncFailed = false;
										syncWarning = false;
										syncDocFailed = false;
							            syncDocDownloadFailed = false; 
							            docUploadFailedForCurrentTrans = false;
							            docDownloadFailedForCurrentTrans = false;
										LESyncService
												.createCompleteSyncObject(syncConfig);
										LESyncService.syncCompleteCallBack = syncCompleteCallBack;
										LESyncService.syncProgress = synprogress;
										LESyncService.syncToPerModuleCompleteCallback = syncToPerModuleCompleteCallback;
										LESyncService.syncFromPerModuleCompleteCallback = syncFromPerModuleCompleteCallback;
										LESyncService.syncProgressDocCallback = syncProgressDocCallback;
										syncLength = MasterSyncObject.length;
									},
									// Create the Complete sync object which is
									// used for sync
									LESyncService.createCompleteSyncObject = function(
											syncConfig) {
										for ( var i = 0; i < syncConfig.length; i++) {
											var syncObject = [];
											var saveBandWidth;
											var syncSsuccessCallback;
											if (syncConfig[i].operation == "syncTo") {
												saveBandWidth = false;
												syncSsuccessCallback = LESyncService.syncTosuccessCallback;
											} else {
												saveBandWidth = "";
												syncSsuccessCallback = LESyncService.syncFromsuccessCallback;
											}
											syncObject = getSyncObject(
													syncConfig[i].module,
													LESyncService
															.buildTransactionObject(syncConfig[i].module),
													syncSsuccessCallback,
													LESyncService.syncErrorCallback,
													LESyncService
															.getSyncOptions(
																	syncConfig[i].operation,
																	syncConfig[i].selectedIds),
													syncConfig[i].operation,
													saveBandWidth);
											MasterSyncObject.push(syncObject);
										}
									},
									// Set TransTrackingID if empty
									LESyncService.setTransTrackingID = function(
											successCallback) {
										DataService
												.getEmptyTransTrackingID(function(
														data) {
													var len = data.length;
													var transactionData = [];
													for (i = 0; i < len; i++) {
														var trackingID = UtilityService
																.getTransTrackingID();
														transactionData
																.push({
																	"transTrackingID" : trackingID
																});
													}
													DataService
															.updateTransTrackingID(
																	data,
																	transactionData,
																	successCallback);
												});
									},
									// creating Transaction object for sync
									LESyncService.buildTransactionObject = function(
											syncType) {
										PersistenceMapping
												.clearTransactionKeys();
										LESyncService
												.mapKeysforPersistence(syncType);
										syncTransactionObj = PersistenceMapping
												.mapScopeToPersistence({});
										return syncTransactionObj;
									},
									// Creating Sync Options
									LESyncService.getSyncOptions = function(
											operation, idArray) {
										var userDetails = UserDetailsService
												.getUserDetailsModel();
										var selectedIDs = [];
										var syncOptions;
										if (operation == "syncTo") {
											if (idArray && idArray.length)
												selectedIDs = idArray;
											syncOptions = {
												url : rootConfig.serviceBaseUrl
														+ "lifeEngageService/save",
												headers : userDetails.options.headers,
												selectedIds : selectedIDs
											};
											if (!selectedIDs.length) // Remove
																		// if
												// corrected
												// in LESync
												delete syncOptions.selectedIds;
										} else {
											syncOptions = {
												url : rootConfig.serviceBaseUrl
														+ "lifeEngageService/retrieveAll",
												headers : userDetails.options.headers
											};
										}
										return syncOptions;
									},
									LESyncService.mapKeysforPersistence = function(
											syncType, tranKeyToUpdate) {
										PersistenceMapping.Key10 = "FullDetails";
										PersistenceMapping.Type = syncType;
										PersistenceMapping.dataIdentifyFlag = false;
										if (keyToUpdate == "Key4") {
											// transactionObj.Key4 =
											// tranKeyToUpdate;
											PersistenceMapping.Key4 = tranKeyToUpdate;
										} else if (keyToUpdate == "Key3") {
											PersistenceMapping.Key3 = tranKeyToUpdate;
											// transactionObj.Key3 =
											// tranKeyToUpdate;

										} else if (keyToUpdate == "Key2") {
											PersistenceMapping.Key2 = tranKeyToUpdate;
											// transactionObj.Key2 =
											// tranKeyToUpdate;
										}
									},
									// The main thread for module wise sync
									LESyncService.runSyncThread = function() {
										if (currentSyncObject.Type
												&& currentSyncObject.syncType == 'syncFrom') {
											if ($
													.inArray(
															currentSyncObject.Type,
															rootConfig.emailEnabledModules) > -1) {
												LESyncService
														.triggerMail(currentSyncObject.Type);
											}
										}
										if (MasterSyncObject.length) {
											currentSyncObject = MasterSyncObject
													.shift(); // Pop the next
											// module to
											// sync
											if (LESyncService.syncProgress) {
												if (currentSyncObject.Type == "LMS") {
													var syncMessage = translateMessages(
															$translate,
															"general.syncLms");
												} else if (currentSyncObject.Type == "FNA") {
													var syncMessage = translateMessages(
															$translate,
															"general.syncFna");
												} else if (currentSyncObject.Type == "illustration") {
													var syncMessage = translateMessages(
															$translate,
															"general.syncIllustration");
												} else if (currentSyncObject.Type == "eApp") {
													var syncMessage = translateMessages(
															$translate,
															"general.syncEapp");
												} else if (currentSyncObject.Type == "Observation") {
													var syncMessage = translateMessages(
															$translate,
															"general.syncObservation");
												}
												var syncPercentage = Math
														.floor((1 - ((MasterSyncObject.length + 1) / syncLength)) * 100);
												LESyncService.syncProgress(
														syncMessage,
														syncPercentage);
											}
											LESyncService
													.doSync(currentSyncObject);
										} else {
											// Sync is completed. If need to
											// send the progress(100%) also call
											// the LESyncService.syncProgress
											// with 100%
											LESyncService.syncCompleteCallBack(
													syncFailed, syncWarning,
													syncDocFailed,
													syncDocDownloadFailed);
										}
									},
									// Sync one module
									LESyncService.doSync = function(
											currentSyncObject) {
										if (currentSyncObject.syncType == "syncTo") {
											DataService
													.syncDataToServer(
															currentSyncObject.transactionObj,
															eval(currentSyncObject.successCallback),
															eval(currentSyncObject.errorCallback),
															eval(currentSyncObject.saveBandWidth),
															eval(currentSyncObject.options));
										} else {
											DataService
													.syncData(
															currentSyncObject.transactionObj,
															eval(currentSyncObject.successCallback),
															eval(currentSyncObject.errorCallback),
															eval(currentSyncObject.options));
										}
									},
									// Sync Error callback
									LESyncService.syncErrorCallback = function() {
										syncFailed = true;
										LESyncService.runSyncThread(); // a
										// module
										// sync
										// failed
										// ;continue
										// sync
									},
									// If any db call failed
									LESyncService.dbCallError = function() {
										LESyncService.log("DB Call err..!!!!");
										// db call failed, but it did'nt stop
										// the sync thread
									},
									// Success callback after Sync To
									LESyncService.syncTosuccessCallback = function(
											result, type) {
										if (result.statusData
												&& result.statusData.Status == "FAILURE")

											syncWarning = true; // If sync of
										// any record
										// failed
										// Starting Req File Sync Changes
										// ************************

										switch (type) {
										case "LMS":
											keyToUpdate = "Key1";
											break;
										case "FNA":
											keyToUpdate = "Key2";
											break;
										case "illustration":
											keyToUpdate = "Key3";
											break;
										case "eApp":
											keyToUpdate = "Key4";
											break;
										case "Observation":
											keyToUpdate = "Key4";
											break;
										}
										var docIndex = 0;
										var idsToSync=[];
										if(currentSyncObject.options.selectedIds)
											idsToSync=currentSyncObject.options.selectedIds;
										// Get all transaction with 'Pending
										// document upload' status
										DataService
												.getTransDataWithStatus(
														"*",
														"Pending Documents Upload",
														type,UserDetailsService.userDetilsModel.agentCode,

														function(data) {
														//Reinitializing transactionIndex
															transactionIndex = 0;
															if (data&& data.length) {
																LESyncService.uploadAllReqFiles(data,result);
															} else {
																// check for  cancelled  items if any  and delete and resume Sync 
															    LESyncService.deleteCancelledAndContinueSync(type,result);
															}
														},LESyncService.dbCallError);
									},

									// Sync All documents
									LESyncService.syncAllDocuments = function(
											docsArray, docIndex, type, result) {
										LESyncService
												.log(currentSyncObject.Type
														+ " :syncAllDocs : docIndex "
														+ docIndex + " out of "
														+ docsArray.length);
										switch (type) {
										case "LMS":
											keyToUpdate = "Key1";
											break;
										case "FNA":
											keyToUpdate = "Key2";
											break;
										case "illustration":
											keyToUpdate = "Key3";
											break;
										case "eApp":
											keyToUpdate = "Key4";
											break;
										case "Observation":
											keyToUpdate = "Key4";
											break;
										}
										if (docIndex < docsArray.length) { // has
											// docs
											// to
											// sync
											LESyncService.syncDocument(
													docsArray, docIndex, type, result);
										} else {
											// All documents are synced; Delete
											// all the cancelled transactions
											// from offline db
											DataService
													.getCancelledProposals(
															"Cancelled",

															function(id) {
																for ( var i = 0; i < id.length; i++) {
																	DataService
																			.deleteFromTable(
																					"Transactions",
																					"Id='"
																							+ id[i].Id
																							+ "'",

																					function() {
																						// Successcallback
																					},
																					LESyncService.dbCallError);
																}
															},
															LESyncService.dbCallError);
											// Get all transaction with 'Pending
											// document upload' status
											DataService
													.getTransDataWithStatus(
															"ID," + keyToUpdate,
															"Pending Documents Upload",currentSyncObject.Type,UserDetailsService.userDetilsModel.agentCode,

															function(data) {
																// Update All
																// transaction
																// status to
																// 'synced' from
																// 'Pending doc
																// Upload'
																var transactionIds = [];
																for ( var i = 0; i < data.length; i++) {
																	transactionIds
																			.push(data[i].Id);
																}
																DataService
																		.updateTransTblStatus(
																				"",
																				"Synced",
																				transactionIds,

																				function() {
																								if(LESyncService.syncToPerModuleCompleteCallback){
																									LESyncService.syncToPerModuleCompleteCallback(result,type);
																								}else{
																									LESyncService
																												.runSyncThread();// no
																																	// document
																																	// to
																																	// sync
																																	// ;
																																	// continue
																																	// sync
																																	// main
																																	// thread
																								} // continue
																					// sync
																					// next
																					// module
																					// after
																					// updating
																					// the
																					// status
																				},
																				LESyncService.dbCallError);
															},
															LESyncService.dbCallError);
										}
									},

									// Sync a document
									LESyncService.syncDocument = function(
											docsArray, docIndex, type, result) {
										LESyncService
												.log(currentSyncObject.Type
														+ " :syncDocument "
														+ docIndex);
										var curDocToSync = docsArray[docIndex];
										// Get key4 for the document so as to
										// pass to upload doc service
										DataService
												.getKey4ForDoc(
														curDocToSync.ParentId,
														keyToUpdate,

														function(id) {
															if (id != null
																	&& id != "") {
																var docDetails = {
																	Documents : JSON
																			.parse(curDocToSync.DocumentObject)
																};
																PersistenceMapping
																		.clearTransactionKeys();
																LESyncService
																		.mapKeysforPersistence(currentSyncObject.Type);
																var transactionObj = PersistenceMapping
																		.mapScopeToPersistence();
																if (keyToUpdate == "Key4") {
																	transactionObj.Key4 = id;
																} else if (keyToUpdate == "Key3") {
																	transactionObj.Key3 = id;
																} else if (keyToUpdate == "Key2") {
																	transactionObj.Key2 = id;
																}
																transactionObj.TransactionData = docDetails;
																var type = transactionObj.Type;
																var newIndex = docIndex;
																// Upload doc
																// service call
																// to
																// save/update/delete
																// a document
																DocumentService
																		.uploadDocuments(
																				transactionObj,
																				docIndex,

																				function(
																						res,
																						count) {
																					LESyncService
																							.log(currentSyncObject.Type
																									+ " :upload success "
																									+ docIndex);
																					// After
																					// syncing,
																					// delete
																					// the
																					// deleted
																					// doc
																					// record
																					// from
																					// offline
																					// db
																					if (curDocToSync.DocumentStatus == 'Deleted') {
																						DataService
																								.deleteFromTable(
																										"Eapp_Attachments",
																										"DocumentName='"
																												+ curDocToSync.DocumentName
																												+ "'",

																										function() {
																											// current
																											// doc
																											// delete
																											// success
																											// ;
																											// sync
																											// the
																											// next
																											// record
																											newIndex = newIndex + 1;
																											LESyncService
																													.syncAllDocuments(
																															docsArray,
																															newIndex,
																															type,
																															result);
																										},

																										function() {
																											// current
																											// doc
																											// delete
																											// failed
																											// ;
																											// sync
																											// the
																											// next
																											// record
																											newIndex = newIndex + 1;
																											LESyncService
																													.syncAllDocuments(
																															docsArray,
																															newIndex,
																															type,
																															result);
																										});
																					} else {
																						var docObj = {};
																						docObj.id = curDocToSync.Id;
																						docObj.documentStatus = "Synced";
																						var newIndex = docIndex;
																						// After
																						// synching,
																						// update
																						// doc
																						// record
																						// status
																						// as
																						// synced
																						// in
																						// offline
																						// db
																						DataService
																								.saveFileLocally(
																										docObj,

																										function() {
																											LESyncService
																													.log(currentSyncObject.Type
																															+ " : save success");
																											// current
																											// doc
																											// update
																											// success
																											// ;
																											// sync
																											// the
																											// next
																											// record
																											newIndex = newIndex + 1;
																											LESyncService
																													.syncAllDocuments(
																															docsArray,
																															newIndex,
																															type,
																															result);
																										},

																										function() {
																											LESyncService
																													.log(currentSyncObject.Type
																															+ " :save failed");
																											// current
																											// doc
																											// update
																											// failed
																											// ;
																											// sync
																											// the
																											// next
																											// record
																											newIndex = newIndex + 1;
																											LESyncService
																													.syncAllDocuments(
																															docsArray,
																															newIndex,
																															type,
																															result);
																										});
																					}
																				},

																				function() {
																					// upload
																					// doc
																					// failed;
																					// sync
																					// the
																					// next
																					// record
																					LESyncService
																							.log(currentSyncObject.Type
																									+ " :upload failed  "
																									+ docIndex);
																					syncDocFailed = true;
																					newIndex = newIndex + 1;
																					LESyncService
																							.syncAllDocuments(
																									docsArray,
																									newIndex,
																									type,
																									result);
																				});
															} else {
																// we didnt get
																// the id to
																// pass to the
																// upload doc
																// service. so
																// try next doc
																LESyncService
																		.log(currentSyncObject.Type
																				+ " :id to update is null  "
																				+ docIndex);
																syncDocFailed = true;
																var newIndex = docIndex + 1;
																LESyncService
																		.syncAllDocuments(
																				docsArray,
																				newIndex,
																				type,
																				result);
															}
														},
														LESyncService.dbCallError);
									},

									LESyncService.uploadAllReqFiles = function(
											transactions,result) {
										var reqFileIndex = 0;
										docUploadFailedForCurrentTrans = false;
										if (transactionIndex < transactions.length) {
											DataService
													.getRequirementsFilesToSync(
															transactions[transactionIndex].TransTrackingID,

															function(data) {
																if (data.length > 0) {
																	var requirementFilesArray = data;
																	LESyncService
																			.syncRequirementFile(
																					requirementFilesArray,
																					reqFileIndex,
																					transactions,
																					result);
																} else {
																	LESyncService
																			.log(currentSyncObject.Type
																					+ " No RequirementFiles");
																	// Mark eApp
																	// as synced
																	LESyncService
																			.markTransactionAsSynced(
																					transactions,
																					"Synced",result);
																}
															},

															function() {
																// fetch file
																// list for
																// transaction
																// failed -
																// proceed to
																// next
																docUploadFailedForCurrentTrans = true;
																syncDocFailed = true;
																LESyncService
																		.proceedToNextTransactionForFileUpload(transactions, result);

																transactionIndex = transactionIndex + 1;
																LESyncService
																		.uploadAllReqFiles(
																				transactions,
																				transactionIndex,
																				result);
															});
										} else {
											LESyncService
													.deleteCancelledAndContinueSync(currentSyncObject.Type,result);
										}
									},

									LESyncService.markTransactionAsSynced = function(
											transactions, syncStatus, result) {
										DataService
												.updateTransTblStatus(
														"",
														syncStatus,
														transactions[transactionIndex].Id,

														function() {
															transactionIndex = transactionIndex + 1;
															LESyncService
																	.uploadAllReqFiles(transactions, result);
														},

														function() {
															// proceed to next
															// transaction for
															// files sync even
															// status update
															// failed
															LESyncService
																	.dbCallError();
															transactionIndex = transactionIndex + 1;
															LESyncService
																	.uploadAllReqFiles(transactions, result);
														});
									},

									LESyncService.deleteCancelledAndContinueSync = function(
											type, result) {
										DataService
												.getCancelledProposalsForType(
														"Cancelled",
														type,UserDetailsService.userDetilsModel.agentCode,

														function(id) {
															if (id && id.length) {
																for ( var i = 0; i < id.length; i++) {
																	DataService
																			.deleteFromTable(
																					"Transactions",
																					"Id='"
																							+ id[i].Id
																							+ "'",

																					function() {
																						 if(LESyncService.syncToPerModuleCompleteCallback){
																									LESyncService.syncToPerModuleCompleteCallback(result,type);
																								}else{
																									LESyncService
																												.runSyncThread();// no
																																	// document
																																	// to
																																	// sync
																																	// ;
																																	// continue
																																	// sync
																																	// main
																																	// thread
																								}
																					},
																					LESyncService.dbCallError);
																}
															} else {
																 if(LESyncService.syncToPerModuleCompleteCallback){
																									LESyncService.syncToPerModuleCompleteCallback(result,type);
																								}else{
																									LESyncService
																												.runSyncThread();// no
																																	// document
																																	// to
																																	// sync
																																	// ;
																																	// continue
																																	// sync
																																	// main
																																	// thread
																								}
															}
														},
														LESyncService.dbCallError);
									},

									// Sync All requirement Files

									LESyncService.syncRequirementFile = function(
											requirementFilesArray,
											reqFileIndex, transactions, resultFromServer) {
										if (reqFileIndex < requirementFilesArray.length) {
											var curTrans = transactions[transactionIndex];
											if(curTrans.Type=="eApp"){
												if(LESyncService.syncProgressDocCallback){
													LESyncService.syncProgressDocCallback(reqFileIndex+1,requirementFilesArray.length);
												}
											}
											var tranKeyToUpdate = LESyncService
													.getTrasactionKey(curTrans,
															keyToUpdate);
											var curReqFileToSync = requirementFilesArray[reqFileIndex];
											if (tranKeyToUpdate != null
													&& tranKeyToUpdate != "") {
												var requirementFile = {};

												requirementFile.RequirementFile = curReqFileToSync;
												PersistenceMapping
														.clearTransactionKeys();
												LESyncService
														.mapKeysforPersistence(currentSyncObject.Type,tranKeyToUpdate);
												//if(curReqFileToSync.status == 'Saved') {
												RequirementService
														.readFile(
																curReqFileToSync,
																function(
																		base64FileContent) {
																	curReqFileToSync.base64string = base64FileContent;
																	requirementFile.RequirementFile = curReqFileToSync;
																	var transactionObj = PersistenceMapping
					                                                        .mapScopeToPersistence(requirementFile);
																	RequirementService
																	    .uploadDocumentFile(
																		transactionObj,
																		function(result) {
    																		if (result.ResponsePayload.Transactions[0].StatusData
    																			&& result.ResponsePayload.Transactions[0].StatusData.Status == "SUCCESS") {
        																		LESyncService
        																				.log(currentSyncObject.Type
        																						+ " :upload success "
        																						+ reqFileIndex);
        																		if (curReqFileToSync.status == 'Deleted') {
        																			DataService
        																					.deleteFromTable(
        																							"RequirementFiles",
        																							"fileName= '"
        																									+ curReqFileToSync.fileName
        																									+ "'",function(){
        																									//Delete Success - proceed to next
        																							LESyncService.proceedToNextFileUpload
																											(reqFileIndex,requirementFilesArray,transactions, resultFromServer)}
        																						,
        																						//Delete Failed - proceed to next
																								function(){
        																						    LESyncService.proceedToNextFileUpload
																								(reqFileIndex,requirementFilesArray,transactions, resultFromServer)}
        																			);
        																		} else if (curReqFileToSync.status == 'Saved') {
        																			// Update  status  as  Synced
        																			var statusUpdate = {};
        																			statusUpdate.status = 'Synced';
        																			statusUpdate.Id = curReqFileToSync.Id;
        																			DataService
        																					.updateFileStatusSynced(
        																							statusUpdate,function(){
        																							LESyncService.proceedToNextFileUpload
        																							(reqFileIndex,requirementFilesArray,transactions, resultFromServer)},
        																						LESyncService.dbCallError);
        																		} else { // Invalid Status  for current file -  proceed to next																			
        																			LESyncService.proceedToNextFileUpload(reqFileIndex,
        																			        requirementFilesArray,transactions, resultFromServer);
        																		}
    																	} else {
    																		// upload Failed for cur file - proceed to next file
    																		docUploadFailedForCurrentTrans = true;
    																		syncDocFailed = true;
    																		LESyncService.proceedToNextFileUpload(reqFileIndex,
    																		        requirementFilesArray,transactions, resultFromServer);
    																	}
																},
																function() {
																	// upload  Failed for cur  file - proceed to next  file
																    docUploadFailedForCurrentTrans = true;
																    syncDocFailed = true;
																	LESyncService.proceedToNextFileUpload(reqFileIndex,
																	        requirementFilesArray,transactions, resultFromServer);
																});
														 }, function() {                                                               
	                                                            //File Content Fetch Failed, proceed to next file
	                                                            docUploadFailedForCurrentTrans = true;
	                                                            syncDocFailed = true;
	                                                            LESyncService.proceedToNextFileUpload(reqFileIndex,
	                                                                    requirementFilesArray,transactions, resultFromServer);
	                                                    });
																								
											} else {
												// we didnt get the id to pass
												// to the upload doc service.
												syncDocFailed = true;
												docUploadFailedForCurrentTrans = true;
												LESyncService
														.proceedToNextTransactionForFileUpload(transactions, resultFromServer);
											}
										} else {
											var curTrans = transactions[transactionIndex];
											var syncStatus = "Synced";
											if (docUploadFailedForCurrentTrans == true) {
												syncStatus = "SyncError";
											}
											LESyncService
													.markTransactionAsSynced(
															transactions,
															syncStatus,
															resultFromServer);
										}
									},
									LESyncService.getTrasactionKey = function(
											transactionObj, keyToUpdate) {
										if (transactionObj) {
											if (keyToUpdate == "Key1") {
												return transactionObj.Key1;
											} else if (keyToUpdate == "Key2") {
												return transactionObj.Key2;
											} else if (keyToUpdate == "Key3") {
												return transactionObj.Key3;
											} else if (keyToUpdate == "Key4") {
												return transactionObj.Key4;
											}
										}
										return "";
									},

									LESyncService.proceedToNextTransactionForFileUpload = function(
											transactions, result) {
										transactionIndex = transactionIndex + 1;
										LESyncService
												.uploadAllReqFiles(transactions, result);
									},

									LESyncService.proceedToNextFileUpload = function(
											reqFileIndex,
											requirementFilesArray, transactions, result) {
										reqFileIndex = reqFileIndex + 1;
										LESyncService.syncRequirementFile(
												requirementFilesArray,
												reqFileIndex, transactions, result);
										// LESyncService
										// .log(currentSyncObject.Type
										// + " :upload failed "
										// +
										// requirementFilesArray[reqFileIndex].fileName);
									},
									// Success callback for Sync From
									LESyncService.syncFromsuccessCallback = function(
											result) {
										DataService
												.getTransDataWithStatus(
														"*",
														"Pending Documents Download",
														currentSyncObject.Type, UserDetailsService.userDetilsModel.agentCode,
														function(data) {
															$.each(data,
																	function(i,	obj) {
																		var formattedData = JSON
																				.parse(unescape(obj.TransactionData));
																		obj.TransactionData = formattedData;
																	});
															//Resetting Index
															transactionIndex = 0;
															
															if (data
																	&& data.length) {
																LESyncService
																		.downloadAllReqFiles(data);
															} else {
																if(LESyncService.syncFromPerModuleCompleteCallback){
																		LESyncService.syncFromPerModuleCompleteCallback(result,type);
																}else{
																	LESyncService
																			.runSyncThread();// no
																							// document
																							// to
																							// sync
																							// ;
																							// continue
																							// sync
																							// main
																							// thread
																}
															}
														},
														function(err) {
															if(LESyncService.syncFromPerModuleCompleteCallback){
																		LESyncService.syncFromPerModuleCompleteCallback(result,type);
																}else{
																	LESyncService
																			.runSyncThread();// no
																							// document
																							// to
																							// sync
																							// ;
																							// continue
																							// sync
																							// main
																							// thread
																}
														});
									},

									// Downloads all documents
									LESyncService.downloadAllDocuments = function(
											transactionData, transactionIndexIllustration,
											docIndex) {
										LESyncService
												.log(currentSyncObject.Type
														+ " :downloadAllDocuments: transactionIndexIllustration "
														+ transactionIndexIllustration
														+ "--docIndex "
														+ docIndex);
										if (transactionData.length > 0) {
											var dataLength = transactionData.length;
											if (transactionIndexIllustration < dataLength) {
												// If there are more
												// transactions for a
												// type(eApp,illustration or
												// FNA) to be synced
												var currTrans = transactionData[transactionIndexIllustration];
												var currTransId = transactionData[transactionIndexIllustration].Id;
												var docData = [];
												if (currTrans.TransactionData
														&& currTrans.TransactionData.Upload) {
													if (!jQuery
															.isEmptyObject(currTrans.TransactionData.Upload.Documents)) {
														var documentsAdded = angular
																.copy(currTrans.TransactionData.Upload.Documents);
														$
																.each(
																		documentsAdded,

																		function(
																				i,
																				docObj) {
																			docObj = LESyncService
																					.mapDocObj(
																							docObj,
																							currTransId);
																			docData
																					.push(docObj);
																		});
													}
													if (!jQuery
															.isEmptyObject(currTrans.TransactionData.Upload.Photograph)
															&& !jQuery
																	.isEmptyObject(currTrans.TransactionData.Upload.Photograph[0])) {
														var tempPhoto = angular
																.copy(currTrans.TransactionData.Upload.Photograph[0]);
														docData
																.push(LESyncService
																		.mapDocObj(
																				tempPhoto,
																				currTransId));
													}
													if (!jQuery
															.isEmptyObject(currTrans.TransactionData.Upload.Signature)
															&& !jQuery
																	.isEmptyObject(currTrans.TransactionData.Upload.Signature[0])) {
														var tempSignature = angular
																.copy(currTrans.TransactionData.Upload.Signature[0]);
														docData
																.push(LESyncService
																		.mapDocObj(
																				tempSignature,
																				currTransId));
													}
												}
												LESyncService
														.downloadDocumentsForTransaction(
																transactionData,
																transactionIndexIllustration,
																docData,
																docIndex);
											} else {
												if(LESyncService.syncFromPerModuleCompleteCallback){
																		LESyncService.syncFromPerModuleCompleteCallback(result,type);
																}else{
																	LESyncService
																			.runSyncThread();// no
																							// document
																							// to
																							// sync
																							// ;
																							// continue
																							// sync
																							// main
																							// thread
																} // All
												// transaction
												// for
												// a
												// type(eApp,illustration
												// or
												// FNA)
												// is
												// retrieved
												// and
												// synced
											}
										} else {
											if(LESyncService.syncFromPerModuleCompleteCallback){
																		LESyncService.syncFromPerModuleCompleteCallback(result,type);
																}else{
																	LESyncService
																			.runSyncThread();// no
																							// document
																							// to
																							// sync
																							// ;
																							// continue
																							// sync
																							// main
																							// thread
																} // In
											// case,
											// if
											// transaction
											// data
											// for
											// a
											// type
											// retrieved
											// as
											// empty
											// from
											// server;
											// continue
											// sync
											// next
											// module
										}
									},
									// Download all docs for a transaction
									LESyncService.downloadDocumentsForTransaction = function(
											transactionData, transactionIndexIllustration,
											docData, docIndex) {
										LESyncService
												.log(currentSyncObject.Type
														+ " :downloadDocumentsForTransaction "
														+ transactionIndexIllustration
														+ "doc " + docIndex);
										if (docData.length != 0
												&& docIndex < docData.length) {
											// If current transaction has more
											// docs to be synced
											var currTransId = transactionData[transactionIndexIllustration].Id;
											// Delete all the existing documents
											// for the current transaction. So
											// as to handle the document delete
											// flow
											if (docIndex == 0) {
												DataService
														.deleteFromTable(
																"Eapp_Attachments",
																"ParentId='"
																		+ currTransId
																		+ "' AND DocumentStatus='Synced'",

																function() {
																	LESyncService
																			.downloadDocument(
																					docData,
																					docIndex,
																					transactionData,
																					transactionIndexIllustration);
																},
																LESyncService.dbCallError);
											} else {
												LESyncService.downloadDocument(
														docData, docIndex,
														transactionData,
														transactionIndexIllustration);
											}
										} else {
											// If all docs related to the
											// current transaction is synced
											// Remove Upload Object after
											// getting the documents
											if (transactionData
													&& transactionData[transactionIndexIllustration]
													&& transactionData[transactionIndexIllustration].TransactionData)
												delete transactionData[transactionIndexIllustration].TransactionData.Upload;
											// Update the status to Synced and
											// move to next transaction
											var newTransactionIndex = transactionIndexIllustration;
											var docIndex = 0;
											DataService
													.updateTransStatusAndData(
															transactionData[transactionIndexIllustration].TransactionData,
															"Synced",
															transactionData[transactionIndexIllustration].Id,

															function() {
																// continue the
																// sync with new
																// transaction
																newTransactionIndex = newTransactionIndex + 1;
																LESyncService
																		.downloadAllDocuments(
																				transactionData,
																				newTransactionIndex,
																				docIndex);
															},
															LESyncService.dbCallError);
										}
									},
									// Download a document
									LESyncService.downloadDocument = function(
											docData, docIndex, transactionData,
											transactionIndexIllustration) {
										LESyncService
												.log(currentSyncObject.Type
														+ " :download docIndex "
														+ docIndex
														+ " with name "
														+ docData[docIndex].documentName);
										var docDetails = {
											Documents : {
												documentName : docData[docIndex].documentName
											}
										};
										PersistenceMapping
												.clearTransactionKeys();
										LESyncService
												.mapKeysforPersistence(currentSyncObject.Type);
										var transactionObj = PersistenceMapping
												.mapScopeToPersistence();
										if (transactionData[transactionIndexIllustration].Type == "illustration") {
											transactionObj.Key3 = transactionData[transactionIndexIllustration].Key3;
										} else if (transactionData[transactionIndexIllustration].Type == "eApp") {
											transactionObj.Key4 = transactionData[transactionIndexIllustration].Key4;
										} else if (transactionData[transactionIndexIllustration].Type == "FNA") {
											transactionObj.Key2 = transactionData[transactionIndexIllustration].Key4;
										}
										transactionObj.TransactionData = docDetails;
										var newIndex = docIndex;
										DocumentService
												.startDownloadForEachTransaction(
														docIndex,
														transactionObj,

														function(documentData,
																count) {
															if (documentData) {
																// set synced
																// status for
																// the document
																var documentDataToSave = LESyncService
																		.mapDocObj(
																				documentData,
																				transactionData[transactionIndexIllustration].Id);
																documentDataToSave.documentStatus = "Synced";
																DataService
																		.saveFileLocally(
																				documentDataToSave,

																				function() {
																					// current
																					// doc
																					// update
																					// success
																					// ;
																					// sync
																					// the
																					// next
																					// record
																					newIndex = newIndex + 1;
																					LESyncService
																							.downloadDocumentsForTransaction(
																									transactionData,
																									transactionIndexIllustration,
																									docData,
																									newIndex);
																				},

																				function() {
																					// current
																					// doc
																					// update
																					// failed
																					// ;
																					// sync
																					// the
																					// next
																					// record
																					newIndex = newIndex + 1;
																					LESyncService
																							.downloadDocumentsForTransaction(
																									transactionData,
																									transactionIndexIllustration,
																									docData,
																									newIndex);
																				});
															} else {
																// Remove Upload
																// Object after
																// getting the
																// documents
																delete transactionData[transactionIndexIllustration].TransactionData.Upload;
																// set synced
																// status for
																// the
																// transaction
																// after all
																// documents
																// downloaded
																DataService
																		.updateTransStatusAndData(
																				transactionData[transactionIndexIllustration].TransactionData,
																				"Synced",
																				transactionData[transactionIndexIllustration].Id,

																				function() {
																					//current doc update success ; sync the next record
																					newIndex = newIndex + 1;
																					LESyncService
																							.downloadDocumentsForTransaction(
																									transactionData,
																									transactionIndexIllustration,
																									docData,
																									newIndex);
																				},

																				function() {
																					//current doc update failed ; sync the next record
																					newIndex = newIndex + 1;
																					LESyncService
																							.downloadDocumentsForTransaction(
																									transactionData,
																									transactionIndexIllustration,
																									docData,
																									newIndex);
																				});
															}
														},

														function() {
															// continue the sync the next record
															syncDocDownloadFailed = true;
															newIndex = newIndex + 1;
															LESyncService
																	.downloadDocumentsForTransaction(
																			transactionData,
																			transactionIndexIllustration,
																			docData,
																			newIndex);
														});
									},

									//create documnet object
									LESyncService.mapDocObj = function(data,
											transactionId) {
										var docObject = {};
										var updatedDate = new Date();
										var formattedDate = Number(updatedDate
												.getMonth() + 1)
												+ "-"
												+ updatedDate.getDate()
												+ "-"
												+ updatedDate.getFullYear();
										docObject.parentId = transactionId;
										docObject.documentType = data.documentType;
										docObject.documentDescription = data.documentDescription;
										docObject.documentName = data.documentName;
										docObject.documentStatus = data.documentStatus;
										docObject.date = formattedDate;
										docObject.documentObject = data;
										return docObject;
									},

									// Downloads all documents
									LESyncService.downloadAllReqFiles = function(
											transactionData) {
										var dataLength = transactionData.length;
										if (transactionIndex < dataLength) {
											if (transactionData.length > 0) {
												// If there are more
												// transactions for a
												// type(eApp,illustration or
												// FNA) to be synced
												var currTrans = transactionData[transactionIndex];
												var fileArrayToDownload = [];
												if (currTrans.TransactionData
														&& currTrans.TransactionData.Requirements
														&& !jQuery
																.isEmptyObject(currTrans.TransactionData.Requirements)) {
													var reqLength = currTrans.TransactionData.Requirements.length;
													if (reqLength > 0) {
														$
																.each(
																		currTrans.TransactionData.Requirements,
																		function(
																				i,
																				obj) {
																			if (!jQuery
																					.isEmptyObject(obj.Documents)
																					&& obj.Documents.length > 0) {

																				$
																						.each(
																								obj.Documents,
																								function(
																										j,
																										docObj) {
																									if (!jQuery
																											.isEmptyObject(docObj.pages)
																											&& docObj.pages.length > 0) {
																										$
																												.each(
																														docObj.pages,
																														function(
																																k,
																																fileObj) {
																															var fileReqObjForDownload = {};
																															var fileObject = {};
																															fileObject.fileName = fileObj.fileName;
																															fileObject.status = fileObj.documentStatus;
																															fileObject.requirementName = obj.requirementName;
																															fileObject.documentName = docObj.documentName;
																															fileArrayToDownload
																																	.push(fileObject);
																														});
																									}
																								});
																			}
																		});
													}

													// We have the list of files  - check and see  download/delete is needed.
													var fileIndex = 0;
													//Initializing current Transaction download failed flag
													docDownloadFailedForCurrentTrans = false;
													LESyncService
															.downLoadOrDeleteFiles(
																	fileArrayToDownload,
																	fileIndex,
																	transactionData);
												} else {
													LESyncService
															.updateTransStatusAndData(transactionData);
												}
											} else {
												LESyncService
														.updateTransStatusAndData(transactionData);
											}
										} else {
											if(LESyncService.syncFromPerModuleCompleteCallback){
												LESyncService.syncFromPerModuleCompleteCallback(transactionData,currentSyncObject.Type);
											}else{
												LESyncService
														.runSyncThread();// In
																			// case,
																			// if
																			// transaction
																			// data
																			// for
																			// a
																			// type
																			// retrieved
																			// as
																			// empty
																			// from
																			// server;
																			// continue
																			// sync
																			// next
																			// module
												}
										}
									},

									LESyncService.downLoadOrDeleteFiles = function(
											fileArrayToDownload, fileIndex,
											transactionData) {
										var curTrans = transactionData[transactionIndex];
										// var transTrackingId = curTrans.
										if (fileIndex < fileArrayToDownload.length) {
											var fileObj = fileArrayToDownload[fileIndex];
											if (fileObj.status == 'Deleted') {
												DataService
														.deleteIfFileExists(
																fileObj,

																function(data) {
																	// file  deleted -  proceed  to next  file -  Delete  from  device
																	LESyncService
																			.proceedToNextFileDownload(
																					fileIndex,
																					fileArrayToDownload,
																					transactionData);

																},

																function() {
																	// File  delete Error - syncFiles failed - proceed  to next file
																	LESyncService
																			.proceedToNextFileDownload(
																					fileIndex,
																					fileArrayToDownload,
																					transactionData);
																});
											} else {
												DataService
														.checkIfFileExists(
																fileObj,

																function(data) {
																	// if file  not  exists
																	if (data.length == 0) {
																		var keyToupdate;
																		// Call download
																		PersistenceMapping
																				.clearTransactionKeys();

																		if (transactionData[transactionIndex].Type == "illustration") {
																			keyToupdate = transactionData[transactionIndex].Key3;
																		} else if (transactionData[transactionIndex].Type == "eApp") {
																			keyToupdate = transactionData[transactionIndex].Key4;
																		} else if (transactionData[transactionIndex].Type == "FNA") {
																			keyToupdate = transactionData[transactionIndex].Key2;
																		}
																		LESyncService
																				.mapKeysforPersistence(
																						transactionData[transactionIndex].Type,
																						keyToupdate);

																		var requirementFile = {};
																		requirementFile.RequirementFile = fileObj;
																		var transactionObj = PersistenceMapping
																				.mapScopeToPersistence(requirementFile);
																		RequirementService
																				.downLoadFile(
																						transactionObj,

																						function(
																								data) {
																							// Insert into RequirementFiles  table new page.physical save and db save
																							if (data
																									&& data.ResponsePayload.Transactions[0].StatusData.Status == "SUCCESS") {
																								var requirementFileResponse = data.ResponsePayload.Transactions[0].TransactionData.RequirementFile;
																								var pageObj = requirementFileResponse;
																								if (pageObj
																										&& pageObj.base64string) {
																									RequirementService
																											.writeFile(
																													pageObj,

																													function(
																															path) {
																														pageObj.base64string = path;

																														var reqFileObject = {};
																														reqFileObject.Identifier = curTrans.TransTrackingID;
																														reqFileObject.requirementName = pageObj.requirementName;
																														reqFileObject.documentName = pageObj.documentName;
																														reqFileObject.fileName = pageObj.fileName;
																														reqFileObject.description = pageObj.description;
																														reqFileObject.base64string = pageObj.base64string;
																														reqFileObject.status = 'Synced';

																														DataService
																																.saveRequirementFiles(
																																		reqFileObject,

																																		function() {
																																			LESyncService
																																					.proceedToNextFileDownload(
																																							fileIndex,
																																							fileArrayToDownload,
																																							transactionData);
																																		},

																																		function() {
																																			// Requirement File  db  insert Error
																																			LESyncService
																																					.proceedToNextFileDownload(
																																							fileIndex,
																																							fileArrayToDownload,
																																							transactionData);
																																		});
																													},

																													function() {
																														// File  write Error
																														docDownloadFailedForCurrentTrans = true;
																														LESyncService
																																.proceedToNextFileDownload(
																																		fileIndex,
																																		fileArrayToDownload,
																																		transactionData);

																													});
																								} else {
																									// File Content Missing
																									docDownloadFailedForCurrentTrans = true;
																									syncDocDownloadFailed = true;
																									LESyncService
																											.proceedToNextFileDownload(
																													fileIndex,
																													fileArrayToDownload,
																													transactionData);
																								}
																							} else {
																								syncDocDownloadFailed = true;
																								docDownloadFailedForCurrentTrans = true;
																								LESyncService
																										.proceedToNextFileDownload(
																												fileIndex,
																												fileArrayToDownload,
																												transactionData);
																							}
																						},

																						function() {
																							// File  download Error
																							syncDocDownloadFailed = true;
																							docDownloadFailedForCurrentTrans = true;
																							LESyncService
																									.proceedToNextFileDownload(
																											fileIndex,
																											fileArrayToDownload,
																											transactionData);
																						});
																	} else { // File is already there- no download needed
																		LESyncService
																				.proceedToNextFileDownload(
																						fileIndex,
																						fileArrayToDownload,
																						transactionData);
																	}
																},

																function() {
																	LESyncService
																			.proceedToNextFileDownload(
																					fileIndex,
																					fileArrayToDownload,
																					transactionData);
																});
											}
										} else {
											LESyncService
													.updateTransStatusAndData(transactionData);
										}
									},

									LESyncService.updateTransStatusAndData = function(
											transactionData) {
										var syncStatus = "Synced";
										if (docDownloadFailedForCurrentTrans == true) {
											syncStatus = "SyncError";
										}
										DataService
												.updateTransStatusAndData(
														transactionData[transactionIndex].TransactionData,
														syncStatus,
														transactionData[transactionIndex].Id,

														function() {
															transactionIndex = transactionIndex + 1;
															LESyncService
																	.downloadAllReqFiles(
																			transactionData,
																			transactionIndex);
														},
														function() {
															//Status and data update for current Transaction Failed - proceed to next
															transactionIndex = transactionIndex + 1;
															LESyncService
																	.downloadAllReqFiles(
																			transactionData,
																			transactionIndex);
														});
									},

									LESyncService.proceedToNextFileDownload = function(
											fileIndex, fileArrayToDownload,
											transactionData) {
										fileIndex = fileIndex + 1;
										LESyncService.downLoadOrDeleteFiles(
												fileArrayToDownload, fileIndex,
												transactionData);
									}

									LESyncService.triggerMail = function(type) {
									if(type != "illustration"){
										PersistenceMapping
												.clearTransactionKeys();
										var transactionObjForMailReq = PersistenceMapping
												.mapScopeToPersistence({});
										transactionObjForMailReq.Type = type;
										var userDetails = UserDetailsService
												.getUserDetailsModel();
										UtilityService.emailServiceCall(
												userDetails,
												transactionObjForMailReq);
									}
									},

									// Archiving initialzing
									LESyncService.doArchiving = function(successcallback) {
										archieveTypes = rootConfig.archieveTypes;
										LESyncService.archiveCompletedcallback = successcallback;
										LESyncService.runArchiving();
									},
									//Archive a module
									LESyncService.runArchiving = function() {
										if (archieveTypes) {
											if (archieveTypes.length == 0) { //archive for all modules completed
												LESyncService.archiveCompletedcallback(syncFailed,syncWarning,syncDocFailed,syncDocDownloadFailed);
											} else {
												switch (archieveTypes[0]) {
												case "LMS":
													archiveOptions.age = rootConfig.lmsArchieveAge;
													archiveOptions.status = rootConfig.lmsArchieveStatus;
													archiveOptions.type = archieveTypes[0];
													break;
												case "FNA":
													archiveOptions.age = rootConfig.fnaArchieveAge;
													archiveOptions.status = rootConfig.fnaArchieveStatus;
													archiveOptions.type = archieveTypes[0];
													break;
												case "illustration":
													archiveOptions.age = rootConfig.illustrationArchieveAge;
													archiveOptions.status = rootConfig.illustrationArchieveStatus;
													archiveOptions.type = archieveTypes[0];
													break;
												case "eApp":
													archiveOptions.age = rootConfig.eAppArchieveAge;
													archiveOptions.status = rootConfig.eAppArchieveStatus;
													archiveOptions.type = archieveTypes[0];
													break;
												}
												archiveOptions.agentId = UserDetailsService.userDetilsModel.agentCode;
												DBSYNC.archive(archiveOptions,LESyncService.archieveSuccess,LESyncService.archieveError);
											}
										}
									},
									LESyncService.archieveSuccess = function() {
										// Success Call Back ; archive next module
										archieveTypes.splice(0,1);
										LESyncService.runArchiving();
									},
									LESyncService.archieveError = function() {
										// Success Call Back; archive next module
										archieveTypes.splice(0,1);
										LESyncService.runArchiving();
									},
									//Showing Logs
									LESyncService.log = function(message) {
										if (isDebugSync)
											console.log("############# SYNC :" + message);
									}
							return LESyncService;
						} ]);