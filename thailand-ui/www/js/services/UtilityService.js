/*
 *Copyright 2015, LifeEngage 
 */



//DataService for save,retrieve, sysn
angular.module('lifeEngage.UtilityService', []).factory("UtilityService", function($http, PersistenceMapping,$rootScope) {
	var utilityService = new Object();
	var disableProgressTab = false;
	Modernizr.addTest('ipad', function () {
		return !!navigator.userAgent.match(/iPad/i);
	});
Modernizr.addTest('iphone', function () {
		return !!navigator.userAgent.match(/iPhone/i);
	});
	utilityService.getAndroidVersion = function(){
		var ua = (navigator.userAgent).toLowerCase(); 
		var match = ua.match(/android\s([0-9\.]*)/);
		return match ? match[1] : false;
	};
	utilityService.disableAllActionElements = function(){
		$('input[type="text"]').prop("disabled", true);
		$('input[type="date"]').prop("disabled", true);
		$('select').not('.languageSelector').prop("disabled", true);
		$('input[type="radio"]').not('input[name="language"]').prop("disabled", true); 
		$('.iradio').not('[id = "languageColor"]').addClass('disabled');
		$('input[type="checkbox"]').prop("disabled", true);
		$('.icheckbox').addClass('disabled');
		$('button').not('.menu-btn,.modal button,[id = "summaryProceedButton"],[id = "agentSummaryButton"]').attr("disabled", "disabled");
		$('textarea').attr("disabled", "disabled");
		$('.items .tile').addClass('tile-disabled');
		$('.fa-edit').addClass('touch-disabled');
		$('.fa-trash-o').not('.Doc_remove_attachment_icon').addClass('touch-disabled');
        $('#btnbtnSummaryPreview').removeProp("disabled");
	};
	utilityService.removeDisableElements = function(id){
		$('form#'+id+' input[type="text"]').removeProp("disabled");
		$('form#'+id+' input[type="date"]').removeProp("disabled");
		$('form#'+id+' select').removeProp("disabled");
		$('form#'+id+' input[type="radio"]').removeProp("disabled");
		$('form#'+id+' .iradio').removeClass('disabled');
		$('form#'+id+' input[type="checkbox"]').removeProp("disabled");
		$('form#'+id+' .icheckbox').removeClass('disabled');
		$('button').not('.menu-btn,#submitBtn-DocUpload').removeAttr("disabled");
		$('textarea').removeProp("disabled");
		$('.items .tile').removeClass('tile-disabled');
		$('form#'+id+' .fa-edit').removeClass('touch-disabled');
		$('form#'+id+' .fa-trash').removeClass('touch-disabled');

	};
	utilityService.removeDisableSpecificElements = function(id){
		$('form#'+id+' input[type="text"]').not('#agentSectionPlace').removeProp("disabled");
		$('form#'+id+' input[type="text"]').not('#agentSectionPlace').removeAttr("disabled");
		$('form#'+id+' input[type="date"]').removeProp("disabled");
		$('form#'+id+' select').removeProp("disabled");
		$('form#'+id+' input[type="radio"]').removeProp("disabled");
		$('form#'+id+' .iradio').removeClass('disabled');
		$('form#'+id+' input[type="checkbox"]').not('#agentAgree').removeProp("disabled");
		$('form#'+id+' .icheckbox').removeClass('disabled');
		$('button').not('.menu-btn,#submitBtn-DocUpload,#confirmSig,#clearSig,#saveProposalButton,#summaryConfirmButton').removeAttr("disabled");
		$('textarea').removeProp("disabled");
		$('.items .tile').removeClass('tile-disabled');
		$('form#'+id+' .fa-edit').removeClass('touch-disabled');
		$('form#'+id+' .fa-trash').removeClass('touch-disabled');

	};
	utilityService.initiateThemeChange = function(theme){
		switch (theme) {
			case 'MainTheme':
				$(
						'link[href="css/abc-theme/css/styles.css"]')
						.attr('href',
								'css/main-theme/css/styles.css');
				$rootScope.theme = "mainTheme";
				break;
			case 'ABCTHEME':
				$(
						'link[href="css/main-theme/css/styles.css"]')
						.attr('href',
								'css/abc-theme/css/styles.css');
				$rootScope.theme = "abcTheme";
				break;
			default:
				break;
			// Do nothing
			}
	}
	utilityService.mapKeysforPersistence = function(scope) {
		PersistenceMapping.Key4 = (scope.proposalId != null && scope.proposalId != 0) ? scope.proposalId : "";
		PersistenceMapping.Key5 = scope.productId != null ? scope.productId : "1";
		PersistenceMapping.Key10 = "FullDetails";
		PersistenceMapping.Key15 = scope.status != null ? scope.status : "Initial";
		PersistenceMapping.Type = "";
	};

	utilityService.isVariableExists = function($scope, variable) {
		variable = variable.split('.');
		var obj = $scope[variable.shift()];
		while (obj && variable.length) {
			obj = obj[variable.shift()];
		}
		return obj;
	};

	utilityService.DynamicDropdowns = function($scope, $rootScope, LookupService) {
		this.InitializeDynamicDropdownCountry = function() {
			PersistenceMapping.clearTransactionKeys();
			utilityService.mapKeysforPersistence($scope);
			var transactionObj = PersistenceMapping.mapScopeToPersistence({
				"Id" : 0,
				"subDivisionType" : "Country"
			});
			LookupService.lookupCall(transactionObj, $scope.onLookupCallSuccess, $scope.onLookupCallError);
		};
		this.OnChangeDynamicDropdown = function(model, dependent, control, dependant1, dependant2) {
			if (dependant1 != '' && dependant2 != '') {
				ClearVariable($scope, dependant2);
				ClearVariable($scope, dependant1);
			}
			var selectedId = model;
			var selectedType = dependent;
			if (selectedType == "State") {
				PersistenceMapping.clearTransactionKeys();
				utilityService.mapKeysforPersistence($scope);
				var transactionObj = PersistenceMapping.mapScopeToPersistence({
					"Id" : selectedId,
					"subDivisionType" : selectedType
				});
				var stateExists = false;
				for (i in $scope.states) {
					if ($scope.states[i].Dependant == selectedId) {
						stateExists = true;
						break;
					}
				}
				if (!stateExists) {
					LookupService.lookupCall(transactionObj, $scope.onLookupCallSuccess, $scope.onLookupCallError);
				}
			} else if (selectedType == "City") {
				PersistenceMapping.clearTransactionKeys();
				utilityService.mapKeysforPersistence($scope);
				var transactionObj = PersistenceMapping.mapScopeToPersistence({
					"Id" : selectedId,
					"subDivisionType" : selectedType
				});
				var citiesExists = false;
				for (i in $scope.cities) {
					if ($scope.cities[i].Dependant == selectedId) {
						citiesExists = true;
						break;
					}
				}
				if (!citiesExists) {
					LookupService.lookupCall(transactionObj, $scope.onLookupCallSuccess, $scope.onLookupCallError);
				}
			}
		};

		$scope.onLookupCallSuccess = function(callbackId, data) {
			if (callbackId == "LookupService.Country") {
				$scope.countries = data;
			} else if (callbackId == "LookupService.State") {
				if ( typeof $scope.states == 'undefined') {
					$scope.states = data;
				} else {
					allStates = $scope.states;
					$scope.states = $scope.states.concat(data);
				}
				allStates = $scope.states;
			} else if (callbackId == "LookupService.City") {
				if ( typeof $scope.cities == 'undefined') {
					$scope.cities = data;
				} else {
					$scope.cities = $scope.cities.concat(data);
				}
				allCities = $scope.cities;
			}
			if (stateOrCity && filterItems.length > 0) {
				$scope.LoadLookUpItems();
			}
			$scope.refresh();
		};
		var stateOrCity = '';

		$scope.LoadLookUpItems = function() {
			var item = filterItems.splice(0, 1)[0];
			if (item) {
				if (utilityService.isVariableExists($scope, item.model)) {
					if (!jQuery.isEmptyObject(item) && eval('$scope.' + item.model) != "") {
						PersistenceMapping.clearTransactionKeys();
						utilityService.mapKeysforPersistence($scope);
						var transactionObj = PersistenceMapping.mapScopeToPersistence({
							"Id" : eval('$scope.' + item.model),
							"subDivisionType" : item.dependent
						});
						if (item.dependent == "State") {
							var stateExists = false;
							for (i in $scope.states) {
								if ($scope.states[i].Dependant == eval('$scope.' + item.model)) {
									stateExists = true;
									break;
								}
							}
							if (!stateExists) {
								LookupService.lookupCall(transactionObj, $scope.onLookupCallSuccess, $scope.onLookupCallError);
							}
						} else if (item.dependent == "City") {

							var citiesExists = false;
							for (i in $scope.cities) {
								if ($scope.cities[i].Dependant == eval('$scope.' + item.model)) {
									citiesExists = true;
									break;
								}
							}
							if (!citiesExists) {
								LookupService.lookupCall(transactionObj, $scope.onLookupCallSuccess, $scope.onLookupCallError);
							}
						}
						$scope.LoadLookUpItems();
					}
				} else {
					$scope.LoadLookUpItems();
				}
			}
		};

	};
	utilityService.InitializeErrorDisplay = function($scope, $rootScope, $translate) {
		$scope.showErrorCount = true;
		$scope.inFocus=false;
		$scope.updateErrorCountWhileAddOrRemove = function(id) {
			$rootScope.updateErrorCount(id);
		};
		$scope.focusErrorField = function(errorMessage) {
			$scope.isPopupDisplayed = false;
			$scope.alertObj.showPopup=false;
			$('#' + errorMessage).focus();//Generali customization
			$scope.inFocus=false;
		};
		$rootScope.hideOverlay=function(){
			$scope.alertObj.showPopup=false;
			$scope.isPopupDisplayed = false;
			$scope.inFocus=false;
		};
        $rootScope.hideOverlayKeyFeatures=function(){
			$scope.alertObj.showPopup=false;
            $scope.keyFeaturesOverlay=true;
			$scope.isPopupDisplayed = false;
			$scope.inFocus=false;
		};
		$rootScope.showErrorPopup = function(id, e) {
			if ($scope.isPopupDisplayed) {
				$scope.isPopupDisplayed = false;
			} else {
				$scope.errorMessages = [];
				$scope.isPopupDisplayed = true;				
				var i = 0;
				var id1 = Array.prototype.slice.call(document.querySelector('#' + id).querySelectorAll(
														".trueRequired:not([class*='ng-hide']) , .trueDynamic:not([class*='ng-hide']), .truePattern:not([class*='ng-hide']), .trueValid:not([class*='ng-hide'])"));
				id1.forEach(function(elemen) {
					var error = {};
					error.message = translateMessages($translate,elemen.getAttribute('error-msg'));
					error.key = elemen.getAttribute('errorkey');
					$scope.errorMessages.push(error);
				});
				if($scope.dynamicErrorMessages) {
					$scope.dynamicErrorMessages.forEach(function(elemen) {
						$scope.errorMessages.push(elemen);
					});
				}
			}
		};
		
		$rootScope.updateErrorCount = function(id, isExplicitCall) {
			var id1 = Array.prototype.slice.call(document.querySelector('#' + id).querySelectorAll(
							".trueRequired:not([class*='ng-hide']) , .trueDynamic:not([class*='ng-hide']), .truePattern:not([class*='ng-hide']),.trueValid:not([class*='ng-hide'])"));
			if($scope.dynamicErrorCount){
				$scope.errorCount = id1.length + $scope.dynamicErrorCount;
			} else {
				$scope.errorCount = id1.length;
			}
			if ($scope.isPopupDisplayed) {
				if (isExplicitCall) {
					$scope.isPopupDisplayed = $scope.isPopupDisplayed;
				} else {
					$scope.isPopupDisplayed = false;
				}
			}
		};
		
		$rootScope.updateErrorCount = function(id) {
			var id1 = Array.prototype.slice.call(document.querySelector('#' + id).querySelectorAll(
							".trueRequired:not([class*='ng-hide']) , .trueDynamic:not([class*='ng-hide']), .truePattern:not([class*='ng-hide']),.trueValid:not([class*='ng-hide'])"));
			if($scope.dynamicErrorCount){
				$scope.errorCount = id1.length + $scope.dynamicErrorCount;
			} else {
				$scope.errorCount = id1.length;
			}
		} 
	};

	utilityService.getFormattedDate = function() {
		now = new Date();
		year = "" + now.getFullYear();
		month = "" + (now.getMonth() + 1);
		if (month.length == 1) {
			month = "0" + month;
		}
		day = "" + now.getDate();
		if (day.length == 1) {
			day = "0" + day;
		}
		hour = "" + now.getHours();
		if (hour.length == 1) {
			hour = "0" + hour;
		}
		minute = "" + now.getMinutes();
		if (minute.length == 1) {
			minute = "0" + minute;
		}
		second = "" + now.getSeconds();
		if (second.length == 1) {
			second = "0" + second;
		}
		return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
	};
    
    utilityService.getFormattedDateTime = function (val, listing) {
        if (typeof val != "undefined" || val != "") {
            now = LEDate(val);
        }
        else {
            now = new Date();
        }
        year = "" + now.getFullYear();
        month = "" + (now.getMonth() + 1);
        if (month.length == 1) {
            month = "0" + month;
        }
        day = "" + now.getDate();
        if (day.length == 1) {
            day = "0" + day;
        }
        hour = "" + now.getHours();
        if (hour.length == 1) {
            hour = "0" + hour;
        }
        minute = "" + now.getMinutes();
        if (minute.length == 1) {
            minute = "0" + minute;
        }
        second = "" + now.getSeconds();
        if (second.length == 1) {
            second = "0" + second;
        }
        if (typeof listing != "undefined" && listing == true) {
            return year + "-" + month + "-" + day + " " + hour + ":" + minute;
        }
        else {
            return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
        }
    };
	
	utilityService.getTransTrackingID = function() {
		var buffer = new Array(32); 
		uuid.v4(null, buffer, 0);
		var transTrackingID = uuid.unparse(buffer);
		return transTrackingID;
	};
	
	utilityService.getUUID = function() {
		var buffer = new Array(32); 
		uuid.v4(null, buffer, 0);
		var transTrackingID = uuid.unparse(buffer);
		return transTrackingID;
			
	};
	
	utilityService.emailServiceCall = function(userDetails,transactionObjForMailReq, successCallback,errorCallback) {
        var emailServiceUrl = rootConfig.serviceBaseUrl + "emailService/triggerEmail";
        var requestInfo = Request();
        requestInfo.Request.RequestPayload.Transactions.push(transactionObjForMailReq);
        var  headers = userDetails.options.headers;
        var options = {};
        options.headers = userDetails.options.headers;
        callAngularPost(emailServiceUrl,requestInfo,options,successCallback,errorCallback,$http);       
    };
    
    utilityService.sendWhatsApp = function(msg) {
		window.plugins.socialsharing.shareViaWhatsApp(msg, null /* img */, null /* url */,
    						function() {console.log('shared ok')},
    						function(errormsg){ alert(errormsg); })

    };
    
    utilityService.sendLinkApp = function(msg) {
    	var url = "http://line.me";
		window.plugins.socialsharing.share(msg, null /* img */, url,
    						function() { console.log('shared ok') },
    						function(errormsg){alert(errormsg)})

    };

    utilityService.calculateDistanceBtwTwoLatLong = function(orgLat,orgLong ,destLat,destLong) {
            var R = 6378137;// in meters
            var angleOrgLat = convertToRadian( orgLat );
            var angleDestLat = convertToRadian( destLat );
            var diffLat = convertToRadian( destLat - orgLat );
            var diffLong = convertToRadian( destLong - orgLong );
            var a = Math.sin(diffLat / 2) * Math.sin(diffLat / 2) +
            		Math.cos(angleOrgLat) * Math.cos(angleDestLat) *
            		Math.sin(diffLong / 2) * Math.sin(diffLong / 2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            var d = R * c;
            return d;
        };

    utilityService.getFilteredDataBasedOnInput = function (model, list, key, continueLoop) {
		var filteredResult = [],
			i, listItem;
		for (i = 0, len = list.length; i < len; i++) {
			if (key) {
				listItem = list[i][key];
			} else {
				listItem = list[i];
			}
			if (listItem.toLowerCase() == model.toLowerCase()) {
				filteredResult.push(list[i]);
				if (!continueLoop) {
					break;
				}
			}
		}
		return filteredResult;
	};

	return utilityService;
});


/*
 * Generic Method for calling Angular $http service.
 */
function callAngularPost(url, data, options, successCallback, errorCallback,
        $http) {
    var request = {
        method : 'POST',
        url : url,
        headers : {
            'Content-Type' : "application/json; charset=utf-8",
            "Token" : options.headers.Token,
			 'Accept': "application/json; charset=utf-8"	
        },
        data : data
    }

    $http(request).success(function(data, status, headers, config) {
    	if(data.Response){
    		 if (data.Response.ResponsePayload.Transactions)
    	            successCallback(data.Response.ResponsePayload.Transactions);
    	        else
    	            successCallback(data.Response.ResponsePayload);
    	}
    }).error(function(data, status, headers, config) {
        errorCallback(data, status, resources.errorMessage);
    });
}
function convertToRadian (angle){
	return angle * (Math.PI / 180);
}