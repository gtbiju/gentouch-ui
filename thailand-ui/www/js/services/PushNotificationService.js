//Data service for push notification
angular
		.module('lifeEngage.PushNotificationService', [])
		.factory(
				"PushNotificationService",['UserDetailsService','$location','$translate','$rootScope', '$http',
				function(UserDetailsService, $location, $translate, $rootScope, $http) {
					var pushNotificationService = new Object();

					pushNotificationService.registerAndroidDeviceForDeviceToken = function(successCallback, errorCallback) {
						pushNotificationServiceUtility
								.registerAndroidDeviceForDeviceToken(successCallback,errorCallback);
					};
					pushNotificationService.registeriOSDeviceForDeviceToken = function(successCallback, errorCallback) {
						pushNotificationServiceUtility
								.registeriOSDeviceForDeviceToken(successCallback,errorCallback);
					};
					pushNotificationService.saveDeviceToken = function(transactionObj,
							successCallback, errorCallback) {
						pushNotificationServiceUtility
								.saveDeviceToken(
										transactionObj,
										successCallback,errorCallback
									, UserDetailsService.getUserDetailsModel().options, $http);
					};
					return pushNotificationService;

				}]);
				
