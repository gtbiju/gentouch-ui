﻿//DataService for save,retrieve, sysn
angular.module('lifeEngage.RuleService', []).factory(
		"RuleService", ['UserDetailsService','$location','$translate','$rootScope', '$http',
		function(UserDetailsService, $location, $translate, $rootScope, $http) {
			var lifeEngage = new Object();

			lifeEngage.runRule = function(transactionObj, successCallback,
					errorCallback) {
				RuleHelper.run(transactionObj, successCallback, function(data, status){
					if(status=='401'){
						$location.path('/login');
						$rootScope.$apply();
						$rootScope.showHideLoadingImage(false);
						$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
							$rootScope.showHideLoadingImage(false);
							});
					}else{
						errorCallback(data);
					}
				}, UserDetailsService.getUserDetailsModel().options, $http);
			};

			lifeEngage.calculateRisk = function(input, ruleSetObj,
					successCallback, errorCallback) {
				RuleHelper.calculateRisk(input, ruleSetObj, successCallback, function(data, status){
					if(status=='401'){
						$location.path('/login');
						$rootScope.$apply();
						$rootScope.showHideLoadingImage(false);
						$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
							$rootScope.showHideLoadingImage(false);
							});
					}else{
						errorCallback(data);
					}
				}, UserDetailsService.getUserDetailsModel().options, $http);
			};

			lifeEngage.calculateGoal = function(transactionObj, goalObject,
					successCallback, errorCallback) {
				RuleHelper.calculateGoal(transactionObj, goalObject,
						successCallback, function(data, status){
					if(status=='401'){
						$location.path('/login');
						$rootScope.$apply();
						$rootScope.showHideLoadingImage(false);
						$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
							$rootScope.showHideLoadingImage(false);
							});
					}else{
						errorCallback(data);
					}
				}, UserDetailsService.getUserDetailsModel().options, $http);
			};

			lifeEngage.getRecommendedProducts = function(input,
					successCallback, errorCallback) {	
				RuleHelper.getRecommendedProducts(input,
						successCallback, function(data, status){
					if(status=='401'){
						$location.path('/login');
						$rootScope.$apply();
						$rootScope.showHideLoadingImage(false);
						$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
							$rootScope.showHideLoadingImage(false);
							});
					}else{
						errorCallback(data);
					}
				}, UserDetailsService.getUserDetailsModel().options, $http);
			};
			
			lifeEngage.checkValidationFNAtoBI = function(transactionObj, successCallback,
					errorCallback) {
				RuleHelper.checkValidationFNAtoBI(transactionObj, successCallback, function(data, status){
					if(status=='401'){
						$location.path('/login');
						$rootScope.$apply();
						$rootScope.showHideLoadingImage(false);
						$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
							$rootScope.showHideLoadingImage(false);
							});
					}else{
						errorCallback(data);
					}
				}, UserDetailsService.getUserDetailsModel().options, $http);
			};
			
			return lifeEngage;

		}]);
