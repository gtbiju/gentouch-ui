﻿
//DataService for Payment
angular.module('lifeEngage.PaymentService', [])
		.factory(
				"PaymentService",['UserDetailsService','$http',
				function(UserDetailsService,$http) {
					var paymentServiceObj = new Object();
						try{
							paymentServiceObj.makePayment = function(transactionObj, successCallback,
									errorCallback) {
								paymentServiceUtility.makePayment(transactionObj, successCallback,
										errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
										
							};
							
							paymentServiceObj.getStatus = function(transactionRefNo, successCallback,
									errorCallback) {
								paymentServiceUtility.getStatus(transactionRefNo, successCallback,
										errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
										
							};
							
							paymentServiceObj.getETRNumber = function(transactionObj, successCallback,
									errorCallback)  {
								paymentServiceUtility.getETRNumber(transactionObj, successCallback,
										errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
																			
							};
							
							paymentServiceObj.getPaymentStatus = function(transactionRefNo, successCallback,
									errorCallback) {
								paymentServiceUtility.getPaymentStatus(transactionRefNo, successCallback,
										errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
										
							};
							
							
							paymentServiceObj.validateReciptNumber = function(transactionObj, successCallback,
									errorCallback) {
								paymentServiceUtility.validateReciptNumber(transactionObj, successCallback,
										errorCallback, UserDetailsService.getUserDetailsModel().options, $http);
										
							};

							paymentServiceObj.validateOnlinePayment = function(transactionObj, successCallback,
                            									errorCallback) {
                            								paymentServiceUtility.validateOnlinePayment(transactionObj, successCallback,
                            										errorCallback, UserDetailsService.getUserDetailsModel().options, $http);

                            							};
					}catch(e){
					alert(e);
					}
					return paymentServiceObj;

				}]);
