/*
 *Copyright 2015, LifeEngage 
 */




var debounce = angular.module('lifeEngage.debounce', []);
// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
debounce.factory('$debounce', function($timeout, $q) {
	var $debounce = {
		"timeout" : "",
		"timeoutMonitor" : function(func, wait, immediate) {
			var timeout;
			var deferred = $q.defer();
			return function() {
				var context = this, args = arguments;
				var later = function() {
					$debounce.timeout = null;
					if (!immediate) {
						deferred.resolve(func.apply(context, args));
						deferred = $q.defer();
					}
				};
				var callNow = immediate && !$debounce.timeout;
				if ($debounce.timeout) {
					$timeout.cancel($debounce.timeout);
				}
				$debounce.timeout = $timeout(later, wait);
				if (callNow) {
					deferred.resolve(func.apply(context, args));
					deferred = $q.defer();
				}
				return deferred.promise;
			};
		},
		"timeoutFlush" : function() {
			$timeout.cancel($debounce.timeout);
		}
	};
	return $debounce;
});
