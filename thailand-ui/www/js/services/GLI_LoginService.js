/*
 *Copyright 2015, LifeEngage 
 */

//Extended Login Service for generali

angular.module('lifeEngage.GLI_LoginService', []).factory(
		"GLI_LoginService", function (LoginService) {
			
			LoginService.checkDuplicateAgentInDb = function(user, successCallback, errorCallback) {    
				loginUtility.checkDuplicateAgentInDb(user, successCallback, errorCallback);
			};
		        
		    return LoginService;

});