/*
 *Copyright 2015, LifeEngage 
 */
angular
		.module('lifeEngage.GLI_globalService', [])
		.service("GLI_globalService",
				function($http, globalService) {
				
				globalService.additionalInsured = [];
				/*FNA changes by Thailand UI- create FNAGoal Object>> starts */
				globalService.FNAGoal = {};
				/*FNA changes by Thailand UI - create FNAGoal Object>> ends */
				/* Defect -3760 - starts*/
				globalService.CPInsured = {};
				globalService.CPPayer = {};
				globalService.CPPrivacyLaw = {};
				globalService.CPBeneficiaries = [];
				/* Defect -3760 - ends*/
				globalService.party = function() {
					return {
							"BasicDetails" : {
								"isSenior" : "",
								"firstName" : "",
								"dob" : "",
								"lastName" : "",
								"photo" : "",
								"gender" : "",
								"age" : "",
								"currentDate" : "",
								"nickName" : "",
								"nationality" : "",
								"countryofResidence":"",
								"identityProof" : "",
								"incomeRange" : "",
								"fullName": "",
								"riskInformation" : {
									"smoke" : "",
									"rateSmokingClass" : "",
									"rateDrinkingClass" : ""
								},
								"isInsuredSameAsPayer" : ""
							},
							"ContactDetails" : {
								"homeNumber1": "",
								"emailId": "",
								"mobileNumber1": "",
								"birthAddress": {
									"city": "",
									"country": ""
								},
								"currentAddress": {
									"addressLine1": "",
									"city": "",
									"state": "",
									"houseNo" : "",
									"street" : "",
									"zipCode": "",
									"ward" : "",
									"district" : "",
									"country" : ""
								},
								"methodOfCommunication" : {},
								"officeNumber" : {"extension": "",
                                                  "number": ""},
								"permanentAddress" : {},
								"homeNumber1" : ""
							},
							"SourceDetails": {
								"source": ""
							},
							"CustomerRelationship" : {
								"isPayorDifferentFromInsured" : "No",
								"relationShipWithPO" : ""
							},
							"OccupationDetails": [{
                                "id":"",
				                "occupationCode":"",
                                "description":"",
                                "job":"",
                                "subJob":"",
                                "occupationClass":"",
                                "industry":"",
                                "occupationCodeValue":"",
                                "descriptionValue":"",
                                "jobValue":"",
                                "subJobValue":"",
                                "occupationClassValue":"",
                                "industryValue":"",
                                "natureofWork":"",
                				"natureofWorkValue":""
			                 }],
							"Declaration": {
								"place": "",
								"date": ""
							},
							"IdentityDetails" : {
								"idNo" : "",
								"idIssueDate" : "",
								"idIssuePlace" : ""
							},
							"SeverityDetails": {
								"severity": ""
							},
							"IncomeDetails" : {},
							"type" : ""
					};
					
				},
				globalService.riders = [],
				globalService.setRiders = function(value) {
					globalService.riders = value;
				},
				globalService.clearData = function(){
					globalService.additionalInsured = [];
					globalService.riders = [];
					globalService.parties = [];
					globalService.product ={
							"ProductDetails" : {},
							"policyDetails" : {"surrenderOption" : "No"}
					};
				}
				globalService.getRiders = function() {
					return globalService.riders;
				},
				globalService.setAdditionalInsured = function(value) {
					globalService.additionalInsured = value;
				},
				/*FNA changes by LE Team - create FNAGoal Object>> starts */
				globalService.setFNAGoal = function(value) {
					globalService.FNAGoal = value;
				},
				/*FNA changes by LE Team - create FNAGoal Object>> ends */
				/* Defect -3760 - starts*/
				globalService.setCPInsured = function(value) {
					globalService.CPInsured = value;
				},
				globalService.setCPPayer = function(value) {
					globalService.CPPayer = value;
				},
				globalService.setCPPrivacyLaw = function(value) {
					globalService.CPPrivacyLaw = value;
				},
				globalService.setCPBeneficiaries = function(value) {
					globalService.CPBeneficiaries = value;
				},
				/* Defect -3760 - ends*/
				globalService.getProductForEapp = function() {
				
					var newProduct = [];
					var eAppProductDetails = globalService.product;
					var productData={};
					if(eAppProductDetails.RiderDetails){
						for(i=0;i<eAppProductDetails.RiderDetails.length;i++){
							if(eAppProductDetails.RiderDetails[i].isRequired == "Yes"){
								newProduct.push(eAppProductDetails.RiderDetails[i]);
							}
						}
					}
					eAppProductDetails.RiderDetails = newProduct;
					
					productData["ProductDetails"]=eAppProductDetails.ProductDetails;
					productData["policyDetails"]=eAppProductDetails.policyDetails;
					productData["RiderDetails"]=eAppProductDetails.RiderDetails;
					productData["FundInformation"]=eAppProductDetails.FundInformation;
					productData["Risk"]=eAppProductDetails.Risk;
					productData["templates"]=eAppProductDetails.templates;
					productData["premiumSummary"]=eAppProductDetails.premiumSummary;
					if( productData.premiumSummary && productData.premiumSummary["premiumSummaryOutput"]){
						delete productData.premiumSummary["premiumSummaryOutput"];
					}
					//productData["TopUp"]=eAppProductDetails.TopUp;
					//productData["withdrawalDetails"]=eAppProductDetails.withdrawalDetails;
					
					return productData;
				},
				
				globalService.setDefaultParties = function() {
					var newParties = [];
					var existingParties = globalService.parties;
					globalService.additionalInsured = [];
					for (party in existingParties) {
						existingParties[party].isInsured = false;
						existingParties[party].isPayer = false;
						existingParties[party].isBeneficiary = false;
						existingParties[party].isAdditionalInsured = false;
						existingParties[party].fnaBenfPartyId = "";
						existingParties[party].fnaInsuredPartyId = "";
						existingParties[party].fnaPayerPartyId = "";
						newParties.push(existingParties[party]);
					}
					globalService.setParties(newParties);
				},
				globalService.getDefaultParties = function() {
					globalService.setDefaultParties();
					return globalService.parties;
				},
				globalService.getAdditionalInsured = function() {
					var additionalInsured = [];
					var parties = globalService.parties;
					for (party in parties) {
						if (parties[party].isAdditionalInsured &&  parties[party].isAdditionalInsured) {
							var temp = angular.copy(parties[party]);
							additionalInsured.push(temp);
						}
					}
					return additionalInsured;
				},
				/*FNA changes by LE Team - create FNAGoal Object>> starts */
				globalService.getFNAGoal = function() {
					return angular.copy(globalService.FNAGoal);
				},
				/*FNA changes by LE Team - create FNAGoal Object>> ends */
				/* Defect -3760 - starts*/
				globalService.getCPInsured = function() {
					return angular.copy(globalService.CPInsured);
				},
				globalService.getCPPayer = function() {
					return angular.copy(globalService.CPPayer);
				},
				globalService.getCPPrivacyLaw = function() {
					return angular.copy(globalService.CPPrivacyLaw);
				},
				globalService.getCPBeneficiaries = function() {
					return angular.copy(globalService.CPBeneficiaries);
				},
				/* Defect -3760 - ends*/
				globalService.getAdditionalInsuredFromIlln = function() {
					var additionalInsured = [];
					var additionalInsuredArr = globalService.additionalInsured;
					var i=0;
					for (party in additionalInsuredArr) {
							var temp = angular.copy(additionalInsuredArr[party]);
							if(typeof temp.partyId=="undefined"){
								temp.partyId=i+1;
								i=temp.partyId;
							}
							additionalInsured.push(temp);
					}
					return additionalInsured;
				},
				/*Code Added For Generali Vietnam Starts*/
				globalService.getAdditionalInsuredforIllustration = function() {
					var additionalInsured = [];
					var parties = globalService.parties;
					var party = 0;
					if(parties[party] && parties[party].isAdditionalInsured != undefined){
						if(parties[party].isAdditionalInsured && parties[party].isAdditionalInsured==true){
							var temp = angular.copy(parties[party]);
							additionalInsured.push(temp);
						}
					}
					return additionalInsured;
				},
				globalService.clearChoosePartyData = function(){
					globalService.additionalInsured = [];
					globalService.riders = [];
					globalService.product ={
							"ProductDetails" : {},
							"policyDetails" : {"surrenderOption" : "No"}
					};
				},
				/*Code Added For Generali Vietnam Ends*/
				globalService.setAddInsuredFromIllustrn = function(additionalInsured) {
					var parties = globalService.parties;

					for (var addInsured in additionalInsured) {
						for (party in parties) {
							if (parties[party].id == additionalInsured[addInsured].fnaAddInsuredPartyId) {
								parties[party].isAdditionalInsured = true;
								parties[party].fnaAddInsuredPartyId = parties[party].id;
							}
						}
					}
					globalService.setParties(parties);
				},
				globalService.setInsuredFromFNA = function(value) {
					var parties = globalService.parties;
					var flagInsured=false;
					for (party in parties) {
						if (parties[party].id == value.fnaInsuredPartyId) {
							flagInsured=true;
							value.id = parties[party].id;
							value.isInsured = true;
							//value.BasicDetails.photo = parties[party].BasicDetails.photo;
							value.type = parties[party].type;
							parties[party] = value;
							break;
						}
					}
					if(flagInsured==false){
						//globalservice.setPartyFromillustrtn(value,true);
						var parties = globalService.parties;
						value.id = parties[length]-1;
						value.isIllustrationInsured = true;
						parties.push(value);
						globalService.setParties(parties);
					}else{
						globalService.setParties(parties);
					}
					
				},
				globalService.setPayerFromFNA = function(value) {
					var parties = globalService.parties;
					var flagPayer=false;
					for (party in parties) {
						if (parties[party].id == value.fnaPayerPartyId) {
							flagPayer=true;
							value.id = parties[party].id;
							value.isPayer = true;
							value.BasicDetails.photo = parties[party].BasicDetails.photo;
							value.fnaInsuredPartyId = parties[party].fnaInsuredPartyId;
							value.isInsured = parties[party].isInsured;
							value.type = parties[party].type;
							parties[party] = value;
							break;
						}
					}
					if(flagPayer==false){
						//globalService.setPartyFromillustrtn(value, false);
						var parties = globalService.parties;
						value.id = parties[length]-1;
						value.isPayer = true;
						parties.push(value);
						globalService.setParties(parties);
					}else{
						globalService.setParties(parties);
					}
					
				},
                /* FNA changes by LE Team >>>                   
                 * retrieving Beneficiary data from database -- Starts 
                 */
                globalService.getFNABeneficiaries = function() {
                    var beneficiaries = [];
                    var parties = globalService.parties;
                    for (party in parties) {
                        if (parties[party].type != "FNAMyself" && parties[party].type != "Lead") {
                            var temp = angular.copy(parties[party]);
                            beneficiaries.push(temp);
                        }
                    }
                    return beneficiaries;
                },
                /* FNA changes by LE Team >>>                   
                 * retrieving Beneficiary data from database -- ends 
                 */    
				globalService.getSelectdBeneficiariesGLI = function() {
					var beneficiaries = [];
					var parties = globalService.parties;
					var i=0;
					for (party in parties) {
						if (parties[party].isBeneficiary) {
							var temp = angular.copy(parties[party]);
							
							temp.BeneficiaryId=i+1;
							i=temp.BeneficiaryId;
							beneficiaries.push(temp);
						}
					}
					return beneficiaries;
				},
				globalService.getInsuredEapp = function() {
					var parties = globalService.parties;
					var insured = new globalService.party();
					for (party in parties) {
						if (parties[party].isInsured || parties[party].isIllustrationInsured) {
							insured = parties[party];
							
							insured.partyId=0;
							break;
						}
					}
					return insured;
				};

				globalService.pinData = [];
				globalService.setOrGetPincode = function (data) {
					if(data) {
						globalService.pinData = angular.copy(data);
					} else {
						return globalService.pinData;
					}
				}
				
		return globalService;
		});

				