﻿﻿﻿/*
 *Copyright 2015, LifeEngage 
 */



//Extended rule service for generali
angular.module('lifeEngage.GLI_RuleService', []).factory(
		"GLI_RuleService",['UserDetailsService','$location','$translate','$rootScope', '$http','RuleService',
		function(UserDetailsService, $location, $translate, $rootScope, $http, RuleService) {
	
	RuleService['runCustomRule'] = function(transactionObj, successCallback,
			errorCallback, ruleInfo, container) {
		RuleHelperGLI.runCustom(transactionObj, successCallback, function(data, status){
			if(status=='401'){
				$location.path('/login');
				$rootScope.$apply();
				$rootScope.showHideLoadingImage(false);
				$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
					$rootScope.showHideLoadingImage(false);
					});
			}else{
				errorCallback(data);
			}
		}, UserDetailsService.getUserDetailsModel().options,ruleInfo, container, $http);
	};
	
	RuleService['runCommonFunctionRule'] = function(transactionObj, successCallback, errorCallback, fileName,functionName,value,riderName,index,fieldTobePopulated,callLoadSummary) {
		RuleHelperGLI.runCommonFunctionRule(transactionObj, successCallback, function(data, status){
			if(status=='401'){
				$location.path('/login');
				$rootScope.$apply();
				$rootScope.showHideLoadingImage(false);
				$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
					$rootScope.showHideLoadingImage(false);
					});
			}else{
				errorCallback(data);
			}
		}, UserDetailsService.getUserDetailsModel().options, fileName,functionName,value,riderName,index,fieldTobePopulated,callLoadSummary, $http);
	};
		
	RuleService['runInsuranceEngagement'] = function(transactionObj,ruleInfo, successCallback, errorCallback) {
		RuleHelperGLI.runInsuranceEngagement(transactionObj, ruleInfo,successCallback, function(data, status){
			if(status=='401'){
				$location.path('/login');
				$rootScope.$apply();
				$rootScope.showHideLoadingImage(false);
				$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "general.tokenValidityErrMsg"),translateMessages($translate, "fna.ok"),function(){
					$rootScope.showHideLoadingImage(false);
					});
			}else{
				errorCallback(data);
			}
		}, UserDetailsService.getUserDetailsModel().options, $http);	
		
		};
	  return RuleService;
	  
}]);