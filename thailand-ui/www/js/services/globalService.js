/*
 *Copyright 2015, LifeEngage 
 */



//DataService for data transfer across modules
angular
		.module('lifeEngage.globalService', [])
		.service(
				"globalService",
				function($http) {
					var globalService = {

						"party" : function() {
							return {
								"BasicDetails" : {
									"isSenior" : "",
									"firstName" : "",
									"dob" : "",
									"lastName" : "",
									"photo" : "",
									"gender" : "",
									"riskInformation" : {
										"rateSmokingClass" : "",
										"rateDrinkingClass" : ""
									}
								},
								"CustomerRelationship" : {
									"isPayorDifferentFromInsured" : "No"
								},
								"OccupationDetails" : {
									"description" : ""
								
							    },
								"ContactDetails" : {
									"emailId" : "",
									"currentAddress" : {
										"address" : ""
									},
									"methodOfCommunication" : {},
									"officeNumber" : {
										"extension" : "",
										"number" : ""
									},
									"permanentAddress" : {},
									"homeNumber1" : ""
								},
								"IncomeDetails" : {},
								"type" : ""
							};
						},

						"product" : {
							"ProductDetails" : {},
							"policyDetails" : {
								"surrenderOption" : "No"
							}
						},

						"parties" : [],
						"getParties" : function() {
							return globalService.parties;
						},
						"setParties" : function(value) {
							for ( var i = 0; i < value.length; i++) {
								var party = value[i];
							}
							globalService.parties = value;

						},
						"getParty" : function(partyType) {
							var parties = globalService.parties;
							var partyForPartyType = new globalService.party();
							for ( var i = 0; i < parties.length; i++) {
								var party = parties[i];
								partyForPartyType = party;
								break;
							}
							partyForPartyType.type = partyType;
							return partyForPartyType;
						},
						"setParty" : function(value, partyType) {
							var parties = globalService.parties;
							if (parties.length > 0) {
								for ( var i = 0; i < parties.length; i++) {
									var party = parties[i];
									if (party.type == partyType) {
										party = value;
										parties[i] = party;
										break;
									}
								}
							} else {
								parties.push(value);
							}
							globalService.setParties(parties);
						},
						"setPartyFromParty" : function(partyType, fromPartyType) {
							var parties =  globalService.parties;
							var newParties = [];
							var partyForPartyType = new globalService.party();
							for (party in parties) {
								var currentParty = parties[party];
								if (currentParty.type == fromPartyType) {
									partyForPartyType = currentParty;
									partyForPartyType.type = partyType;
									break;
								}
							}
							
							newParties.push(partyForPartyType);
							globalService.setParties(newParties);

						},
						"getFNABeneficiaries" : function() {
							var beneficiaries = [];
							var parties = globalService.parties;
							for (party in parties) {
								if (parties[party].type != "FNAMyself" && parties[party].type != "Lead" && (parties[party].isIllustrationInsured && parties[party].isIllustrationInsured != true)) {
									var temp = angular.copy(parties[party]);
                                    beneficiaries.push(temp);
								}
							}
							return beneficiaries;
						},
						"setFNABeneficiaries" : function(beneficiaries) {
							var newParties = [];
							var existingParties = globalService.parties;
							for (party in existingParties) {
								if (existingParties[party].type == "FNAMyself" || existingParties[party].type == "Lead" || (existingParties[party].isIllustrationInsured && existingParties[party].isIllustrationInsured == true)) {
									newParties.push(existingParties[party]);
								}
							}
							for (beneficiary in beneficiaries) {
								var temp = angular
										.copy(beneficiaries[beneficiary]);
								newParties.push(temp);
							}
							globalService.setParties(newParties);
						},
						"removeBeneficiarySelected" : function() {
							var parties = globalService.parties;
							for (party in parties) {
								if (parties[party].isBeneficiary) {
									parties[party].isBeneficiary = false;
									parties[party].fnaBenfPartyId = "";
								}
							}
							globalService.setParties(parties);
						},
						"getSelectdBeneficiaries" : function() {
							var beneficiaries = [];
							var parties = globalService.parties;
							for (party in parties) {
								if (parties[party].isBeneficiary) {
									var temp = angular.copy(parties[party]);
									beneficiaries.push(temp);
								}
							}
							return beneficiaries;
						},
						"getImageFromFNAParty" : function(partyId) {
							var parties = globalService.parties;
							var photo = "";
							for (party in parties) {
								if (parties[party].id == partyId) {
									photo = parties[party].BasicDetails.photo;
									break;
								}
							}
							return photo;
						},
						"setProduct" : function(value) {
							globalService.product = value;

						},
						"getProduct" : function() {
							globalService.product.policyDetails.surrenderOption = "No";
							return globalService.product;
						},
						"getInsured" : function() {
							var parties = globalService.parties;
							var insured = new globalService.party();
							for (party in parties) {
								if(parties[party] && parties[party].isInsured != undefined) {
									if (parties[party].isInsured) {
										insured = parties[party];
										break;
									}
								}
							}
							return insured;
						},
						"getInsuredIllustration" : function() {
							var parties = globalService.parties;
                            var insured = new globalService.party();
							var insuredIllustration = new globalService.party();
							for (party in parties) {
								if(parties[party]) {
										insured = parties[party];
										break;
								}
							}
							return insured;
						},
						"getPayer" : function() {
							var parties = globalService.parties;
							var payer = new globalService.party();
							for (party in parties) {
								if(parties[party] && parties[party].isPayer != undefined) {
									if (parties[party].isPayer) {
										payer = parties[party];
										break;
									}
								}
							}
							return payer;
						},
						"setInsured" : function(value) {
							var parties = globalService.parties;
							for (party in parties) {
								if (parties[party].id == value.fnaInsuredPartyId) {
									value.id = parties[party].id;
									value.isInsured = true;
									//value.BasicDetails.photo = parties[party].BasicDetails.photo;
									value.type = parties[party].type;
									parties[party] = value;
									break;
								}
							}
							globalService.setParties(parties);

						},
						"setBenfFromIllustrn" : function(beneficiaries) {
							var parties = globalService.parties;

							for (benef in beneficiaries) {
								for (party in parties) {
									if (parties[party].id == beneficiaries[benef].fnaBenfPartyId) {
										parties[party].isBeneficiary = true;
										parties[party].fnaBenfPartyId = parties[party].id;
									}
								}
							}
							globalService.setParties(parties);
						},
						"setPayer" : function(value) {
							var parties = globalService.parties;
							for (party in parties) {
								if (parties[party].id == value.fnaPayerPartyId) {
									value.id = parties[party].id;
									value.isPayer = true;
									value.BasicDetails.photo = parties[party].BasicDetails.photo;
									value.fnaInsuredPartyId = parties[party].fnaInsuredPartyId;
									value.isInsured = parties[party].isInsured;
									value.type = parties[party].type;
									parties[party] = value;
									break;
								}
							}
							globalService.setParties(parties);

						},
						"setFNADefaultParties" : function() {
							var newParties = [];
							var existingParties = globalService.parties;
							for (party in existingParties) {
								existingParties[party].isInsured = false;
								existingParties[party].isPayer = false;
								existingParties[party].isBeneficiary = false;
								existingParties[party].fnaBenfPartyId = "";
								existingParties[party].fnaInsuredPartyId = "";
								existingParties[party].fnaPayerPartyId = "";
								newParties.push(existingParties[party]);
							}
							globalService.setParties(newParties);
						},
						"getFNAParties" : function() {
							globalService.setFNADefaultParties();
							return globalService.parties;
						},
						"RequirementFile": function(){
							return {
								"RequirementFile" : {
									"identifier" : "",
									"requirementName" : "",
									"documentName" : "",
									"fileName" : "",
									"status" : "",
									"description" : "",
									"date" : "",
									"base64string" : ""
								}
							};
						},
						
						"Requirement": function(){
							return {
								"requirementType" : "",
								"requirementSubType":"",
								"requirementName" : "",
								"partyIdentifier" : "",
								"isMandatory" : "",
								"Documents" : [ {
									"documentName" : "",
									"documentProofSubmitted" : "",
									"documentStatus" : "",
									"documentOptions" : [ {
										"optionTypeCode" : ""
									} ],
									"pages" : [ {
										"fileName" : ""
									} ]
								} ]
							};
						},
						
						"RequirementAgent": function(){
							return {
								"requirementType" : "",
								"requirementSubType":"",
								"requirementName" : "",
								"partyIdentifier" : "",
								"isMandatory" : "",
								"Documents" : [ {
									"documentName" : "",
									"documentProofSubmitted" : "",
									"documentStatus" : "",
									"documentOptions" : [ {
										"optionTypeCode" : ""
									} ],
									"pages" : [ {
										"fileName" : ""
									} ]
								} ]
							};
						},
						
						"setPartyFromillustrtn" : function(value, isInsured) {
							var parties = globalService.parties;
							if (isInsured) {
								value.id = value.fnaInsuredPartyId;
								value.isInsured = true;
							} else {
								value.id = value.fnaPayerPartyId;
								value.isPayer = true;
							}
							parties.push(value);
							globalService.setParties(parties);

						}
					};

					return globalService;

				});
