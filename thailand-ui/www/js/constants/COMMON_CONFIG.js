storeApp.constant('commonConfig', function() {
	return {
		"DATA_WIPE_REASON" : {
		    "ROOTED_DEVICE" : "RootedDevice",
		    "UNAUTHORIZED_USER" : "UnAuthorizedUser",
		    "BRUTE_FORCE_LOGIN" : "BruteForceLogin"
		}
	};
});