/*
 *Copyright 2015, LifeEngage 
 */




(function() {

	// Dynamically loading device specific cordova and SQLLite script files.
	// Found the solution from here.
	// http://stackoverflow.com/questions/6783144/issue-with-dynamically-loaded-phonegap-js

	if ((rootConfig.isDeviceMobile)) {
		if (navigator.userAgent.indexOf("Android") > 0
			|| navigator.userAgent.indexOf("iPhone") > 0
			|| navigator.userAgent.indexOf("iPad") > 0
			|| navigator.userAgent.indexOf("iPod") > 0) {
		loadScript('services/dataservice-mobile.js');
		loadScript('services/documentservice-mobile.js');
		loadScript('services/productservice-mobile.js');
		loadScript('services/ruleHelper-mobile.js');
		loadScript('services/gli-ruleHelper-mobile.js');
		loadScript('services/authenticationservice-mobile.js');
		loadScript('services/datalookupservice-mobile.js');
		loadScript('services/loginservice-mobile.js');
		loadScript('services/gli-dataservice-mobile.js');
		loadScript('services/agentservice-mobile.js');
		loadScript('services/paymentservice-mobile.js');
		loadScript('services/emailservice-mobile.js');
		loadScript('services/platforminfoservice-mobile.js');
		loadScript('services/requirementservice-mobile.js');
		loadScript('services/pushnotificationsservice-mobile.js');
		
		//loadScript('services/gli-loginservice-mobile.js');
	    } else {
			loadScript('services/dataservice-desktop-offline.js');
			loadScript('services/productservice-desktop-offline.js');
			loadScript('services/ruleHelper-desktop-offline.js');
			loadScript('services/gli-ruleHelper-desktop-offline.js');
			loadScript('services/authenticationservice-desktop-offline.js');
			loadScript('services/documentservice-desktop-offline.js');
			loadScript('services/datalookupservice-desktop-offline.js');
			loadScript('services/gli-dataservice-desktop-offline.js');
			loadScript('services/loginservice-desktop-offline.js');
			loadScript('services/agentservice-desktop-offline.js');
			loadScript('services/paymentservice-desktop-offline.js');
			loadScript('services/emailservice-desktop-offline.js');
			loadScript('services/requirementservice-desktop-offline.js');
		}
	} else {
			loadScript('services/dataservice-desktop.js');
			loadScript('services/documentservice-desktop.js');
			loadScript('services/productservice-desktop.js');
			loadScript('services/ruleHelper-desktop.js');
			loadScript('services/authenticationservice-desktop.js');
			loadScript('services/datalookupservice-desktop.js');
			loadScript('services/loginservice-desktop.js');
			loadScript('services/agentservice-desktop.js');
			loadScript('services/gli-ruleHelper-desktop.js');
			loadScript('services/gli-dataservice-desktop.js');
			loadScript('services/paymentservice-desktop.js');
			loadScript('services/emailservice-desktop.js');
			loadScript('services/platforminfoservice-desktop.js');
		}

	function loadScript(url) {
		// synchronous load by @Sean Kinsey
		// http://stackoverflow.com/a/2880147/813951
		var xhrObj = new XMLHttpRequest();
		xhrObj.open('GET', url, false);
		xhrObj.send('');
		var scriptTag = document.createElement('script');
		scriptTag.text = xhrObj.responseText;
		document.getElementsByTagName('head')[0].appendChild(scriptTag);
	}

})();