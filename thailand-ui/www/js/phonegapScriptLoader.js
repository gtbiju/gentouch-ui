(function() {

	// Dynamically loading device specific cordova and SQLLite script files.
	// Found the solution from here.
	// http://stackoverflow.com/questions/6783144/issue-with-dynamically-loaded-phonegap-js

	if ((rootConfig.isDeviceMobile)) {
		if (navigator.userAgent.indexOf("Android") > 0) {
			loadScript('lib/vendor/phonegap/cordova-5.3.3.js');
			loadScript('lib/vendor/phonegap/SQLitePlugin.js');
			loadScript('lib/vendor/phonegap/PushNotification.js');

		} else if (navigator.userAgent.indexOf("iPhone") > 0
				|| navigator.userAgent.indexOf("iPad") > 0
				|| navigator.userAgent.indexOf("iPod") > 0) {

			loadScript('lib/vendor/phonegap/cordova-5.3.3_ios.js');
			loadScript('lib/vendor/phonegap/SQLitePlugin_ios.js');
			loadScript('lib/vendor/phonegap/PushNotification_ios.js');
		}

			loadScript('lib/vendor/phonegap/LEEncryption.js');
			loadScript('lib/vendor/phonegap/EmailComposer.js');
			loadScript('lib/vendor/phonegap/DeviceUtil.js');
			loadScript('lib/vendor/phonegap/video.js');
			loadScript('lib/vendor/phonegap/pdf.js');
			loadScript('lib/vendor/phonegap/LEFileUtils.js');
			loadScript('lib/vendor/phonegap/Calendar.js');
			loadScript('lib/vendor/phonegap/mfilechooser.js');
			loadScript('lib/vendor/phonegap/DGGeofencing.js');
			loadScript('lib/vendor/phonegap/localNotification.js');
			loadScript('lib/vendor/phonegap/SocialSharing.js');
			//loadScript('lib/vendor/phonegap/SSLCertificateChecker.js');
	}
	var scriptTag;

	function loadScript(url) {
		// synchronous load by @Sean Kinsey
		// http://stackoverflow.com/a/2880147/813951
		var xhrObj = new XMLHttpRequest();
		xhrObj.open('GET', url, false);
		xhrObj.send('');
		var scriptTag = document.createElement('script');
		scriptTag.text = xhrObj.responseText;
		document.getElementsByTagName('head')[0].appendChild(scriptTag);
	}
})();