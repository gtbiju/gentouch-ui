
var cordovaObj = window.cordova || window.Cordova;
var  channel = cordovaObj.require("cordova/channel");
var devicePlatform
if (navigator.userAgent.indexOf("iPhone") > 0 || navigator.userAgent.indexOf("iPad") > 0 || navigator.userAgent.indexOf("iPod") > 0){
    devicePlatform = 'iOS';
}

/*************
 * INTERFACE *
 *************/

NOTIFICATIONS = ( typeof NOTIFICATIONS == 'undefined' ? {} : NOTIFICATIONS );
NOTIFICATIONS = {

/***********
 * MEMBERS *
 ***********/

// Default values
_defaults : {
    text:  '',
    title: '',
    icon : '',
    sound: 'res://platform_default',
    badge: 0,
    autoClear  : true,
    id:    0,
    data:  undefined,
    every: undefined,
    at:    undefined
},

// listener
_listener : {},

// Registered permission flag
_registered : false,
/**
 * Returns the default settings.
 *
 * @return {Object}
 */
getDefaults : function () {
    return this._defaults;
},

/**
 * Overwrite default settings.
 *
 * @param {Object} defaults
 */
setDefaults : function (newDefaults) {
    var defaults = this.getDefaults();

       for (var key in defaults) {
           if (newDefaults.hasOwnProperty(key)) {
               defaults[key] = newDefaults[key];
           }
       }
},

/**
 * Schedule a new local notification.
 *
 * @param {Object} notifications
 *      The notification properties
 * @param {Function} callback
 *      A function to be called after the notification has been canceled
 * @param {Object?} scope
 *      The scope for the callback function
 * @param {Object?} args
 *      skipPermission:true schedules the notifications immediatly without
 *                          registering or checking for permission
 */
schedule : function (msgs, callback, scope, args) {
    var fn = function(granted) {

           if (!granted) return;

           var notifications = Array.isArray(msgs) ? msgs : [msgs];

           for (var i = 0; i < notifications.length; i++) {
               var notification = notifications[i];

               this.mergeWithDefaults(notification);
               this.convertProperties(notification);
           }

          this.exec('schedule', notifications, callback, scope);
       };

     if (args && args.skipPermission) {
           fn.call(this, true);
      } else {
          this.registerPermission(fn, this);
      }
},

/**
 * Merge custom properties with the default values.
 *
 * @param {Object} options
 *      Set of custom values
 *
 * @retrun {Object}
 *      The merged property list
 */
mergeWithDefaults : function (options) {
    var defaults = this.getDefaults();

    options.at   = this.getValueFor(options, 'at', 'firstAt', 'date');
    options.text = this.getValueFor(options, 'text', 'message');
    options.data = this.getValueFor(options, 'data', 'json');

    if (defaults.hasOwnProperty('autoClear')) {
        options.autoClear = this.getValueFor(options, 'autoClear', 'autoCancel');
    }

    if (options.autoClear !== true && options.ongoing) {
        options.autoClear = false;
    }

    if (options.at === undefined || options.at === null) {
        options.at = new Date();
    }

    for (var key in defaults) {
        if (options[key] === null || options[key] === undefined) {
            if (options.hasOwnProperty(key) && ['data','sound'].indexOf(key) > -1) {
                options[key] = undefined;
            } else {
                options[key] = defaults[key];
            }
        }
    }

    for (key in options) {
        if (!defaults.hasOwnProperty(key)) {
            delete options[key];
            console.warn('Unknown property: ' + key);
        }
    }

    return options;
},
/**
 * First found value for the given keys.
 *
 * @param {Object} options
 *      Object with key-value properties
 * @param {String[]} keys*
 *      Key list
 */
getValueFor : function (options) {
    var keys = Array.apply(null, arguments).slice(1);

    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];

        if (options.hasOwnProperty(key)) {
            return options[key];
        }
    }
},
/**
 * Convert the passed values to their required type.
 *
 * @param {Object} options
 *      Set of custom values
 *
 * @retrun {Object}
 *      The converted property list
 */
convertProperties : function (options) {

    if (options.id) {
        if (isNaN(options.id)) {
            options.id = this.getDefaults().id;
            console.warn('Id is not a number: ' + options.id);
        } else {
            options.id = Number(options.id);
        }
    }

    if (options.title) {
        options.title = options.title.toString();
    }

    if (options.text) {
        options.text  = options.text.toString();
    }

    if (options.badge) {
        if (isNaN(options.badge)) {
            options.badge = this.getDefaults().badge;
            console.warn('Badge number is not a number: ' + options.id);
        } else {
            options.badge = Number(options.badge);
        }
    }

    if (options.at) {
        if (typeof options.at == 'object') {
            options.at = options.at.getTime();
        }

        options.at = Math.round(options.at/1000);
    }

    if (typeof options.data == 'object') {
        options.data = JSON.stringify(options.data);
    }

    if (options.every) {
        if (devicePlatform == 'iOS' && typeof options.every != 'string') {
            options.every = this.getDefaults().every;
            var warning = 'Every option is not a string: ' + options.id;
            warning += '. Expects one of: second, minute, hour, day, week, ';
            warning += 'month, year on iOS.';
            console.warn(warning);
        }
    }

    return options;
},

/**
 * Create callback, which will be executed within a specific scope.
 *
 * @param {Function} callbackFn
 *      The callback function
 * @param {Object} scope
 *      The scope for the function
 *
 * @return {Function}
 *      The new callback function
 */
createCallbackFn : function (callbackFn, scope) {

    if (typeof callbackFn != 'function')
        return;

    return function () {
        callbackFn.apply(scope || this, arguments);
    };
},

/**
 * Update existing notifications specified by IDs in options.
 *
 * @param {Object} notifications
 *      The notification properties to update
 * @param {Function} callback
 *      A function to be called after the notification has been updated
 * @param {Object?} scope
 *      The scope for the callback function
 * @param {Object?} args
 *      skipPermission:true schedules the notifications immediatly without
 *                          registering or checking for permission
 */
update : function (msgs, callback, scope, args) {
    var fn = function(granted) {

        if (!granted) return;

        var notifications = Array.isArray(msgs) ? msgs : [msgs];

        for (var i = 0; i < notifications.length; i++) {
            var notification = notifications[i];

            this.convertProperties(notification);
        }

        this.exec('update', notifications, callback, scope);
    };

    if (args && args.skipPermission) {
        fn.call(this, true);
    } else {
        this.registerPermission(fn, this);
    }
},
/**
 * Clear the specified notification.
 *
 * @param {String} id
 *      The ID of the notification
 * @param {Function} callback
 *      A function to be called after the notification has been cleared
 * @param {Object?} scope
 *      The scope for the callback function
 */
clear : function (ids, callback, scope) {
    ids = Array.isArray(ids) ? ids : [ids];
    ids = this.convertIds(ids);

    this.exec('clear', ids, callback, scope);
},

/**
 * Clear all previously sheduled notifications.
 *
 * @param {Function} callback
 *      A function to be called after all notifications have been cleared
 * @param {Object?} scope
 *      The scope for the callback function
 */
clearAll : function (callback, scope) {
    this.exec('clearAll', null, callback, scope);
},
/**
 * Cancel the specified notifications.
 *
 * @param {String[]} ids
 *      The IDs of the notifications
 * @param {Function} callback
 *      A function to be called after the notifications has been canceled
 * @param {Object?} scope
 *      The scope for the callback function
 */
cancel : function (ids, callback, scope) {
    ids = Array.isArray(ids) ? ids : [ids];
    ids = this.convertIds(ids);

    this.exec('cancel', ids, callback, scope);
},

/**
 * Remove all previously registered notifications.
 *
 * @param {Function} callback
 *      A function to be called after all notifications have been canceled
 * @param {Object?} scope
 *      The scope for the callback function
 */
cancelAll : function (callback, scope) {
    this.exec('cancelAll', null, callback, scope);
},

/**
 * Check if a notification with an ID is present.
 *
 * @param {String} id
 *      The ID of the notification
 * @param {Function} callback
 *      A callback function to be called with the list
 * @param {Object?} scope
 *      The scope for the callback function
 */
isPresent : function (id, callback, scope) {
    this.exec('isPresent', id || 0, callback, scope);
},

/**
 * Check if a notification with an ID is scheduled.
 *
 * @param {String} id
 *      The ID of the notification
 * @param {Function} callback
 *      A callback function to be called with the list
 * @param {Object?} scope
 *      The scope for the callback function
 */
isScheduled : function (id, callback, scope) {
    this.exec('isScheduled', id || 0, callback, scope);
},

/**
 * Check if a notification with an ID was triggered.
 *
 * @param {String} id
 *      The ID of the notification
 * @param {Function} callback
 *      A callback function to be called with the list
 * @param {Object?} scope
 *      The scope for the callback function
 */
isTriggered : function (id, callback, scope) {
    this.exec('isTriggered', id || 0, callback, scope);
},

/**
 * List all local notification IDs.
 *
 * @param {Function} callback
 *      A callback function to be called with the list
 * @param {Object?} scope
 *      The scope for the callback function
 */
getAllIds : function (callback, scope) {
    this.exec('getAllIds', null, callback, scope);
},

/**
 * Alias for `getAllIds`.
 */
getIds : function () {
    this.getAllIds.apply(this, arguments);
},

/**
 * List all scheduled notification IDs.
 *
 * @param {Function} callback
 *      A callback function to be called with the list
 * @param {Object?} scope
 *      The scope for the callback function
 */
getScheduledIds : function (callback, scope) {
    this.exec('getScheduledIds', null, callback, scope);
},

/**
 * List all triggered notification IDs.
 *
 * @param {Function} callback
 *      A callback function to be called with the list
 * @param {Object?} scope
 *      The scope for the callback function
 */
getTriggeredIds : function (callback, scope) {
    this.exec('getTriggeredIds', null, callback, scope);
},

/**
 * Property list for given local notifications.
 * If called without IDs, all notification will be returned.
 *
 * @param {Number[]?} ids
 *      Set of notification IDs
 * @param {Function} callback
 *      A callback function to be called with the list
 * @param {Object?} scope
 *      The scope for the callback function
 */
get : function () {
    var args = Array.apply(null, arguments);

    if (typeof args[0] == 'function') {
        args.unshift([]);
    }

    var ids      = args[0],
        callback = args[1],
        scope    = args[2];

    if (!Array.isArray(ids)) {
        this.exec('getSingle', Number(ids), callback, scope);
        return;
    }

    ids = this.convertIds(ids);

    this.exec('getAll', ids, callback, scope);
},

/**
 * Property list for all local notifications.
 *
 * @param {Function} callback
 *      A callback function to be called with the list
 * @param {Object?} scope
 *      The scope for the callback function
 */
getAll : function (callback, scope) {
    this.exec('getAll', null, callback, scope);
},

/**
 * Property list for given scheduled notifications.
 * If called without IDs, all notification will be returned.
 *
 * @param {Number[]?} ids
 *      Set of notification IDs
 * @param {Function} callback
 *      A callback function to be called with the list
 * @param {Object?} scope
 *      The scope for the callback function
 */
getScheduled : function () {
    var args = Array.apply(null, arguments);

    if (typeof args[0] == 'function') {
        args.unshift([]);
    }

    var ids      = args[0],
        callback = args[1],
        scope    = args[2];

    if (!Array.isArray(ids)) {
        ids = [ids];
    }

    if (!Array.isArray(ids)) {
        this.exec('getSingleScheduled', Number(ids), callback, scope);
        return;
    }

    ids = this.convertIds(ids);

    this.exec('getScheduled', ids, callback, scope);
},

/**
 * Property list for all scheduled notifications.
 *
 * @param {Function} callback
 *      A callback function to be called with the list
 * @param {Object?} scope
 *      The scope for the callback function
 */
getAllScheduled : function (callback, scope) {
    this.exec('getScheduled', null, callback, scope);
},

/**
 * Property list for given triggered notifications.
 * If called without IDs, all notification will be returned.
 *
 * @param {Number[]?} ids
 *      Set of notification IDs
 * @param {Function} callback
 *      A callback function to be called with the list
 * @param {Object?} scope
 *      The scope for the callback function
 */
getTriggered : function () {
    var args = Array.apply(null, arguments);

    if (typeof args[0] == 'function') {
        args.unshift([]);
    }

    var ids      = args[0],
        callback = args[1],
        scope    = args[2];

    if (!Array.isArray(ids)) {
        ids = [ids];
    }

    if (!Array.isArray(ids)) {
        this.exec('getSingleTriggered', Number(ids), callback, scope);
        return;
    }

    ids = this.convertIds(ids);

    this.exec('getTriggered', ids, callback, scope);
},

/**
 * Property list for all triggered notifications.
 *
 * @param {Function} callback
 *      A callback function to be called with the list
 * @param {Object?} scope
 *      The scope for the callback function
 */
getAllTriggered : function (callback, scope) {
    this.exec('getTriggered', null, callback, scope);
},

/**
 * Informs if the app has the permission to show notifications.
 *
 * @param {Function} callback
 *      The function to be exec as the callback
 * @param {Object?} scope
 *      The callback function's scope
 */
hasPermission : function (callback, scope) {
    var fn = this.createCallbackFn(callback, scope);

    if (devicePlatform != 'iOS') {
        fn(true);
        return;
    }

    cordovaObj.exec(fn, null, 'LocalNotification', 'hasPermission', []);
},

/**
 * Register permission to show notifications if not already granted.
 *
 * @param {Function} callback
 *      The function to be exec as the callback
 * @param {Object?} scope
 *      The callback function's scope
 */
registerPermission : function (callback, scope) {

    if (this._registered) {
        return this.hasPermission(callback, scope);
    } else {
        this._registered = true;
    }

    var fn = this.createCallbackFn(callback, scope);

    if (devicePlatform != 'iOS') {
        fn(true);
        return;
    }

    cordovaObj.exec(fn, null, 'LocalNotification', 'registerPermission', []);
},

/**
 * Fire event with given arguments.
 *
 * @param {String} event
 *      The event's name
 * @param {args*}
 *      The callback's arguments
 */
fireEvent : function (event) {
    var args     = Array.apply(null, arguments).slice(1),
        listener = this._listener[event];

    if (!listener)
        return;

    for (var i = 0; i < listener.length; i++) {
        var fn    = listener[i][0],
            scope = listener[i][1];

        fn.apply(scope, args);
    }
},
/**
 * Convert the IDs to numbers.
 *
 * @param {String/Number[]} ids
 *
 * @return Array of Numbers
 */
convertIds : function (ids) {
    var convertedIds = [];

    for (var i = 0; i < ids.length; i++) {
        convertedIds.push(Number(ids[i]));
    }

    return convertedIds;
},

/**
 * Execute the native counterpart.
 *
 * @param {String} action
 *      The name of the action
 * @param args[]
 *      Array of arguments
 * @param {Function} callback
 *      The callback function
 * @param {Object} scope
 *      The scope for the function
 */
exec : function (action, args, callback, scope) {
    var fn = this.createCallbackFn(callback, scope),
        params = [];

    if (Array.isArray(args)) {
        params = args;
    } else if (args) {
        params.push(args);
    }

     cordovaObj.exec(fn, null, 'LocalNotification', action, params);
},


/**********
 * EVENTS *
 **********/

/**
 * Register callback for given event.
 *
 * @param {String} event
 *      The event's name
 * @param {Function} callback
 *      The function to be exec as callback
 * @param {Object?} scope
 *      The callback function's scope
 */
on : function (event, callback, scope) {

    if (typeof callback !== "function")
        return;

    if (!this._listener[event]) {
        this._listener[event] = [];
    }

    var item = [callback, scope || window];

    this._listener[event].push(item);
},


/**
 * Unregister callback for given event.
 *
 * @param {String} event
 *      The event's name
 * @param {Function} callback
 *      The function to be exec as callback
 */
un : function (event, callback) {
    var listener = this._listener[event];
    if (!listener)
        return;

    for (var i = 0; i < listener.length; i++) {
        var fn = listener[i][0];

       if (fn == callback) {
            listener.splice(i, 1);
            break;
        }
    }
}

}