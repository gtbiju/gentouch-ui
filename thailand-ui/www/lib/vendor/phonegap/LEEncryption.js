/*
 * LEEncryption.js
 * version - 2.1.0.0
 */
function LEEncryption() {
	
};

LEEncryption.prototype.encryptDB = function(DBArray, success, error) {
	if (rootConfig && rootConfig.isOfflineDesktop) {
	} else {
	    cordova.exec(success, error, "LEEncryption", "encryptDB", DBArray);
	}
};
LEEncryption.prototype.checkKeyAvailability = function(options, success, error) {
	cordova.exec(success, error, "LEEncryption", "checkKeyAvailability", options);
};
LEEncryption.prototype.insertKey= function(key, success, error){
    cordova.exec(success, error, "LEEncryption", "insertKey", key);
};
LEEncryption.prototype.fetchKey= function(options, success, error) {
	cordova.exec(success, error, "LEEncryption", "fetchKey", options);
};
/**
 * Load Plugin
 */
if (!rootConfig || ( rootConfig && !rootConfig.isOfflineDesktop)) {
	if(!window.plugins) {
	    window.plugins = {};
	}
	if (!window.plugins.LEEncryption) {
	    window.plugins.LEEncryption = new LEEncryption();
	}
}