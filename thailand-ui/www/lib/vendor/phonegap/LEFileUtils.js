/*
 * LEFileUtils.js
 * version - 2.2.0.0
 */
function LEFileUtils() {
	if (rootConfig && rootConfig.isOfflineDesktop) {
		this.fs = require('fs');
	}
};

function _log(message){
	if (rootConfig.isDebug) {
		console.log("LEFileUtils :" + message);
	}	
}
LEFileUtils.prototype.renameFile = function(filesDetails, success, error) {
	_log("Files rename initiated");
	if (rootConfig && rootConfig.isOfflineDesktop) {
	fs= this.fs;
		var obj = filesDetails[0];
		var oldFileName = obj.oldFileName;
		var newFileName = obj.newFileName;
		var isErrorIfSourceFileNotExists = true;
		if (obj.isErrorIfSourceFileNotExists == false) {
			isErrorIfSourceFileNotExists = false;
		}
		var path = rootConfig.configAndContentPath + "/";
		fs.exists( path + oldFileName, function (exists) {
			if (exists) {
				fs.writeFileSync(path  + newFileName , fs.readFileSync(path + oldFileName));
				success("File renamed successfully");
			} else {
				if (isErrorIfSourceFileNotExists) {
					error("File does not exist : " + path + oldFileName)
				} else {
					success("Skipping the operation since file doesn't exists");
				}
			}
		});
	} else {
	    cordova.exec(success, error, "LEFileUtils", "renameFile", filesDetails);
	}
};
LEFileUtils.prototype.copyFiles = function(filesToBeCopied, success, error) {
	_log("Files Copy initiated");
	if (rootConfig && rootConfig.isOfflineDesktop) {
		fs= this.fs;
		function _copyIndividualFiles(index) {
			if(index == filesToBeCopied.length ){
				if (success)success();
			} else {
				var obj = filesToBeCopied[i];
				_log("Copying file : "+ obj.fileName);
				var fileName = obj.fileName;
				var isForceCopyRequired = false
				var filePath = "";
				if (obj.filePath) {
					filePath = obj.filePath + "/";
				} else if (obj.sourcePath) {
					filePath = obj.sourcePath + "/";
				}
				if (obj.isForceCopyRequired) {
					isForceCopyRequired = true;
				}
				_checkFolderExistsAndCopy(fileName, filePath, isForceCopyRequired, function(result){
					if(result)
						_log("Successfully copied file : " + filePath + fileName)
					i++;
					_copyIndividualFiles(i);
				}, function(err){
					_log("LEFileUtils : Failed to copy file : " + filePath + fileName + "(Error : " + JSON.stringify(err) + ")");

					i++;
					_copyIndividualFiles(i);
				});
			}
		}
		function _mkdirRecursive(path, callback, position) {
			osSep = process.platform === 'win32' ? '\\' : '/';
			var parts = require('path').normalize(path).split(osSep);


			position = position || 0;
		  
			if (position >= parts.length) {
			  return callback();
			}
		  
			var directory = parts.slice(0, position + 1).join(osSep) || osSep;
			fs.stat(directory, function(err) {    
			  if (err === null) {			
				_mkdirRecursive(path,  callback, position + 1);
			  } else {
				fs.mkdir(directory,  function (err) {
				  if (err && err.code != 'EEXIST') {
					return callback(err);
				  } else {
					_mkdirRecursive(path, callback, position + 1);
				  }
				});
			  }
			});
		}
		function _checkFolderExistsAndCopy(name, path, isForceCopy, cb, errorCb) {
			
			var documentsPath = "";
			if (rootConfig && rootConfig.configAndContentPath) {
				documentsPath = rootConfig.configAndContentPath + "/";
			}
			destinationFileFullPath = documentsPath + name;
			sourceFullPath = path + name;
			_log("Checking Destination folder exists or not");
			fs.exists(documentsPath, function(isExists){			
				if(!isExists) {
					_log("Destination folder not exist");
					_mkdirRecursive(documentsPath, function(err){
						if (!err) {	
							_log("Successfully created destination folder");
							_copyFile(destinationFileFullPath, sourceFullPath, isForceCopy, cb, errorCb)
						} else {
							_log("Failed to create destination folder");
							errorCb(err);
						}
					})
				} else {
					_log("Destination folder exists");
					_copyFile(destinationFileFullPath, sourceFullPath, isForceCopy, cb, errorCb)
				}
			})	
				
		}
		function _copyFile(destFullPath, sourceFullPath, isForceCopy, cb, errorCb) {		
			fs.exists(destFullPath, function(fileExists){
				if(!fileExists || isForceCopy){
					var cbCalled = false;
					var rd = fs.createReadStream(sourceFullPath);
					rd.on("error", function(err) {
						done(err);
					});
					var wr = fs.createWriteStream(destFullPath);
					wr.on("error", function(err) {
						done(err);
					});
					wr.on("close", function(ex) {
						done();
					});
					rd.pipe(wr);
					function done(err) {
						if (!cbCalled) {
							if(err){
								errorCb(err);
							} else {
								cb(true);
							}
							cbCalled = true;
						}
					}
				} else {
					_log("File already exists. Also force copying is false.");
					cb(false);
				}
			})
			
		}
		var i = 0;
		_copyIndividualFiles(i);
		
	} else {
	    cordova.exec(success, error, "LEFileUtils", "copyFileFromPackage", filesToBeCopied);
	}
};

LEFileUtils.prototype.getApplicationPath = function(success, error) {
	if (rootConfig && rootConfig.isOfflineDesktop) {
		var documentsPath = "";
		if (rootConfig && rootConfig.configAndContentPath) {
			documentsPath = rootConfig.configAndContentPath + "/";
		}
		success(documentsPath);
	} else {
	    var nullArgumentArray = [];
	    cordova.exec(success, error, "LEFileUtils", "getApplicationPath",[]);
	}
};

LEFileUtils.prototype.isFileExists = function(fileName, success, error) {
if(rootConfig && rootConfig.isOfflineDesktop){
	fs = this.fs;
	var filePath = "";
		if (rootConfig && rootConfig.configAndContentPath) {
			filePath = rootConfig.configAndContentPath + "/" + fileName;
		}
		if (fs.existsSync(filePath)) {
    		success();
    	} else {
    		error();
    	}
}else{
	var options = {}
	options.fileName= fileName;
			cordova.exec(success, error, "LEFileUtils", "isFileExists",[fileName]);
}
};

LEFileUtils.prototype.deleteFile = function(fileDesc,success, error) {
	if (rootConfig && rootConfig.isOfflineDesktop) {
		fs= this.fs;
		var obj = fileDesc[0];
		var fileName = obj.fileName;
		var filePath = obj.filePath;
		var isErrorIfFileNotExists = true;
		if (obj.isErrorIfFileNotExists == false) {
			isErrorIfFileNotExists = false;
		}
	    var documentsPath = "";
		if (rootConfig && rootConfig.configAndContentPath) {
			if(filePath && filePath !=""){
				documentsPath = rootConfig.configAndContentPath+ "/"+filePath + "/";
			}else{
				documentsPath = rootConfig.configAndContentPath + "/";
			}
		}
		if (!fs.existsSync(documentsPath + fileName)) {
			if (isErrorIfFileNotExists){
				error("File doesn't exists");
			} else {
				success();
			}			
		} else {
			fs.unlink(documentsPath+fileName, function (err) {
				if (err) {
					_log("failed to copy file : " + documentsPath+fileName);
					if(error) error(err);
				} else {
					_log("successfully deleted file : " + documentsPath+fileName);
					if(success) success();			
				}
			});
		}
	} else {
		cordova.exec(success, error, "LEFileUtils", "deleteFile",fileDesc);
	}
    
};

LEFileUtils.prototype.copyFile = function(fileDesc, success, error){
	if (rootConfig && rootConfig.isOfflineDesktop) {
		try {
		
			fs= this.fs;
			var obj = fileDesc[0];
			window.plugins.LEFileUtils.getApplicationPath(function(path){
				if (!fs.existsSync(path + obj.folderName)) {
				  fs.mkdirSync(path + obj.folderName);
				}
				var inStr = fs.createReadStream(obj.srcPath);
				var outStr = fs.createWriteStream(path + obj.folderName+'/'+obj.newName);
				inStr.pipe(outStr);
				success(outStr);
			},error);
		} catch (e) {
			console.log('Error');
			console.log(e);
		}
	} else {	
		cordova.exec(success, error, "LEFileUtils", "copyFile", fileDesc);
	}
	
};

LEFileUtils.prototype.readFileAsBase64 = function(fileDetails, success, error){
    if(rootConfig && rootConfig.isOfflineDesktop || (!rootConfig.isOfflineDesktop && !rootConfig.isDeviceMobile)){
		var obj = fileDetails[0];
		var img = new Image();
		img.crossOrigin = 'Anonymous';
		img.onload = function(){
			var canvas = document.createElement('CANVAS'),
			ctx = canvas.getContext('2d'), dataURL;
			canvas.height = this.height;
			canvas.width = this.width;
			ctx.drawImage(this, 0, 0);
			dataURL = canvas.toDataURL(obj.outputFormat);
			success(dataURL);
			canvas = null; 
		};
		img.src = obj.fileName;
	}else{
	cordova.exec(success, error, "LEFileUtils", "readFileAsBase64", fileDetails);
	}
};

LEFileUtils.prototype.writeBase64AsFile = function(fileDetails, success, error){
    if(rootConfig && rootConfig.isOfflineDesktop){
		fs= this.fs;
		var obj = fileDetails[0];
		window.plugins.LEFileUtils.getApplicationPath(function(path){
		 if (!fs.existsSync(path + obj.destFolder)) {
		  fs.mkdirSync(path + obj.destFolder);
		 }		  
		var filepath=path + obj.destFolder + obj.fileName;
		fs.writeFile(filepath, obj.base64String.data,error);
		success(filepath);
		},error);
		
	}else{
	cordova.exec(success, error, "LEFileUtils", "writeBase64AsFile", fileDetails);
	}
};

LEFileUtils.prototype.deleteConfigFiles = function(success, error){
	if (rootConfig && rootConfig.isOfflineDesktop) {
		try {
			fs= this.fs;
			window.plugins.LEFileUtils.getApplicationPath(function(path){
				if (path) {
					deleteFolderRecursive(path);
					success();	
				}
			},error);
		} catch (e) {
			console.log('Error');
			console.log(e);
		}
	} else {
		cordova.exec(success,error, "LEFileUtils", "deleteConfigFiles", []);
	}

};
function deleteFolderRecursive(path) {
  if( fs.existsSync(path) ) {
	fs.readdirSync(path).forEach(function(file,index){
      var curPath = path + "/" + file;
	  if(curPath.indexOf('.svn')<0){
		if(fs.lstatSync(curPath).isDirectory()) { // recurse
			deleteFolderRecursive(curPath);
		  } else { // delete file
			fs.unlinkSync(curPath);
		  }
	  }
    });
  }
};
/**
 * Load Plugin
 */

	if(!window.plugins) {
	    window.plugins = {};
	}
	if (!window.plugins.LEFileUtils) {
	    window.plugins.LEFileUtils = new LEFileUtils();
	}
