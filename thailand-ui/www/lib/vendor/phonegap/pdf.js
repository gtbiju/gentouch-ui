
if (navigator.userAgent.indexOf("Android") > 0 || navigator.userAgent.indexOf("iPhone") > 0 || navigator.userAgent.indexOf("iPad") > 0 || navigator.userAgent.indexOf("iPod") > 0) {
cordova.define("cordova/plugins/pdfViewer",
  function(require, exports, module) {
    var exec = require("cordova/exec");
    var PDFViewer = function () {};


    PDFViewer.prototype.showPdf = function(url) {
        exec(null, function(){alert('could not open pdf')}, "PdfViewer", "showPdf", [url]);
    };

    var pdfViewer = new PDFViewer();
    module.exports = pdfViewer;
});

if (!window.plugins) {
    window.plugins = {};
}
if (!window.plugins.pdfViewer) {
    window.plugins.pdfViewer = cordova.require("cordova/plugins/pdfViewer");
}
}