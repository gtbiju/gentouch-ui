/*
 * DeviceUtil.js
 * version - 1.0.0.0
 */
	function DeviceUtil() {
		if (rootConfig && rootConfig.isOfflineDesktop)
		{
			this.fs = require('fs');
		}
	};
	
	DeviceUtil.prototype.getDeviceInfo = function(successCallback,errorCallback){
		
		cordova.exec(getDeviceInfo_success, getDeviceInfo_failure, "Device", "getDeviceInfo", []);
		
		function getDeviceInfo_success(data){	
			var parsedData = JSON.parse(JSON.stringify(data));	
			successCallback(parsedData);
		}
		function getDeviceInfo_failure(data){			
			if (errorCallback && typeof errorCallback == "function") {
				errorCallback(data);
			}		
		}	
		
		
	}

	function _log(message){
		if (rootConfig.isDebug) 
		{
			console.log("DeviceUtil :" + message);
		}	
	}


/**
 * Load Plugin
 */
if (!rootConfig || ( rootConfig && !rootConfig.isOfflineDesktop)) 
{
	if(!window.plugins) 
	{
	    window.plugins = {};
	}
	if (!window.plugins.DeviceUtil) 
	{
	    window.plugins.DeviceUtil = new DeviceUtil();
	}
}