 (function() {

 	var PushNotification, PushNotificationTransactionCB, pushnotifiction_callback_queue, root, pn_id, callback;
 	pushnotifiction_callback_queue = {};
 	root = this;
 	PushNotification = function() {};

 	get_unique_id = function() {
 		var id, id2;
 		id = new Date().getTime();
 		id2 = new Date().getTime();
 		while (id === id2) {
 			id2 = new Date().getTime();
 		}
 		return id2 + "000";
 	};
 	// Call this to register for push notifications. Content of [options] depends on whether we are working with APNS (iOS) or GCM (Android)
 	PushNotification.prototype.register = function(successCallback, errorCallback, options) {
 		this.pn_id = get_unique_id();
 		pushnotifiction_callback_queue[this.pn_id] = new Object();
 		pushnotifiction_callback_queue[this.pn_id]["success"] = successCallback;
 		pushnotifiction_callback_queue[this.pn_id]["error"] = errorCallback;

 		cordova.exec(null, null, "PushPlugin", "register", [options, this.pn_id]);
 	};

 	// Call this to unregister for push notifications- Not implemented 
 	PushNotification.prototype.unregister = function(successCallback, errorCallback) {
 		if (errorCallback == null) {
 			errorCallback = function() {}
 		}

 		if (typeof errorCallback != "function") {
 			console.log("PushNotification.unregister failure: failure parameter not a function");
 			return;
 		}

 		if (typeof successCallback != "function") {
 			console.log("PushNotification.unregister failure: success callback parameter must be a function");
 			return;
 		}

 		cordova.exec(successCallback, errorCallback, "PushPlugin", "unregister", []);
 	};
 	PushNotificationTransactionCB = {};
 	PushNotificationTransactionCB.onNotificationGCM = function(ev, pnId) {
 		switch (ev.event) {
 			case 'registered':
 				if (ev.regid.length > 0) {
 					if (typeof pnId !== "undefined") {
 						if (pnId && pushnotifiction_callback_queue[pnId] && pushnotifiction_callback_queue[pnId]["success"]) {
 							return pushnotifiction_callback_queue[pnId]["success"](ev);
 						}
 					} else {
 						return console.log("PushNotificationTransactionCB.onNotificationGCM ---pn_id = NULL");
 					}
 				}

 				break;

 			case 'message':
 				pushNotificationInitiated = "true";
 				//alert("Message  " +ev.message);  
 				break;

 			case 'error':

 				break;

 			default:

 				break;
 		}


 	};

 	PushNotificationTransactionCB.eroorCB = function() {
 		if (typeof pnId !== "undefined") {
 			if (pnId && pushnotifiction_callback_queue[pnId] && pushnotifiction_callback_queue[pnId]["error"]) {
 				return pushnotifiction_callback_queue[pnId]["error"]();
 			}
 		} else {
 			return console.log("PushNotificationTransactionCB.onNotificationGCM ---pn_id = NULL");
 		}
 	};
 	root.PushNotificationTransactionCB = PushNotificationTransactionCB;

 	if (!window.plugins) {
 		window.plugins = {};
 	}
 	if (!window.plugins.pushNotification) {
 		window.plugins.pushNotification = new PushNotification();
 	}


 })();