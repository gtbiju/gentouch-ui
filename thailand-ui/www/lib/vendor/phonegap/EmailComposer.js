/*
 * EmailComposer.js
 * version - 1.1.0.0
 */
function EmailComposer() {
};

function _log(message){
	if (rootConfig.isDebug) {
		console.log("EmailComposer.js : " + message);
	}	
}

/**
 * Load Plug-in
 */
if (!rootConfig || ( rootConfig && !rootConfig.isOfflineDesktop)) {
	if(!window.plugins) {
	    window.plugins = {};
	}
	if (!window.plugins.email) {
	    window.plugins.email = new EmailComposer();
	}
}

EmailComposer.prototype.open = function(to, cc, bcc, subject, body, attachments, isHtml, app, success, error)
{	
	_log("open - started");
	if (rootConfig && rootConfig.isDeviceMobile) {
		var jsonData = {};		
		jsonData["to"] = to;
		jsonData["cc"] = cc;
		jsonData["bcc"] = bcc;
		jsonData["subject"] = subject;
		jsonData["body"] = body;				
		jsonData["attachments"] = attachments;
		jsonData["isHtml"] = isHtml;
		jsonData["app"] = app;
		
		cordova.exec(success, error, "EmailComposer", "open", [jsonData]);
	}
	_log("open - ended");
};

EmailComposer.prototype.isEmailConfigured = function(success, error)
{
	_log("isEmailConfigured - started");
	if (rootConfig && rootConfig.isDeviceMobile) {		
		cordova.exec(success, error, "EmailComposer", "isAvailable", []);
	}	
	_log("isEmailConfigured - ended");
};
