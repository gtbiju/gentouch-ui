$.fn.radialOptions = function(options) {
	
	var settings = $.extend({
		//Defaults
		animationTime:500,
		selectedAction:"switch"
	},options);
	
	
	/*Variable declaration*/		
				   
	var elem6_first_top,elem6_first_left,elem6_second_top,elem6_second_left,elem6_third_top,
	    elem6_third_left,elem6_fourth_top,elem6_fourth_left,elem6_fifth_top,elem6_fifth_left,
		elem6_sixth_top,elem6_sixth_left,elem5_first_top,elem5_first_left,elem5_second_top,
		elem5_second_left,elem5_third_top,elem5_third_left,elem5_fourth_top,elem5_fourth_left,
		elem5_fifth_top,elem5_fifth_left,elem4_first_top,elem4_first_left,elem4_second_top,
		elem4_second_left,elem4_third_top,elem4_third_left,elem4_fourth_top,elem4_fourth_left,
		elem3_first_top,elem3_first_left,elem3_second_top,elem3_second_left,elem3_third_top,
		elem3_third_left,elem2_first_top,elem2_first_left,elem2_second_top,elem2_second_left;
				   
        return this.each(function() {
      
	            var ht_of_container = $(this).height();				
				var ht_half_container = ht_of_container/2; 				
				/*ORIGIN HEIGHT & WIDTH*/
				var origin_height = $(this).children().eq(0).height();
				var origin_half_height = origin_height/2; 
				var origin_width = $(this).children().eq(0).width();
				
				//Make width in steps of origin_width container
				
				var one_tenth = origin_width/10;
				var one_tenth_m = origin_width * 0.15;
				var two_tenth = origin_width * 0.2;
				var two_tenth_m = origin_width * 0.25;
				var three_tenth = origin_width * 0.3;
				var three_tenth_m = origin_width * 0.35;
				var four_tenth = origin_width * 0.4;
				var four_tenth_m = origin_width * 0.45;
				var one_half = origin_width * 0.5;
				var one_half_m = origin_width * 0.55;
				var six_tenth = origin_width * 0.6;
				var six_tenth_m = origin_width * 0.65;
				var seven_tenth = origin_width * 0.7;
				var seven_tenth_m = origin_width * 0.75;
				var eight_tenth = origin_width * 0.8;
				var eigth_tenth_m = origin_width * 0.85;
				var nine_tenth = origin_width * 0.9;
				var nine_tenth_m = origin_width * 0.95;
				
				var position_of_origin = ht_half_container - origin_half_height;	
				/*POSITION THE ORIGIN*/
				$(this).children().eq(0).css("top",position_of_origin);
				
				if(settings.selectedAction == "switch"){	
				 /*Default Selection of the Option on load, the branches along with first class will be chosen*/
				  $(this).children(".branches.option1")
				         .find(".option_container")
						 .removeClass()
						 .addClass("selected_option_container");	
				}
				
				
				$(this).children('.branches').each(function(){	
				   var first = $(this).hasClass("option1");
				   var second = $(this).hasClass("option2");
				   var third = $(this).hasClass("option3");
				   var fourth = $(this).hasClass("option4");
				   var fifth = $(this).hasClass("option5");
				   var sixth = $(this).hasClass("option6");
				   
				   var chek_number_of_circles = $(this).parent().attr("class").split(" ")[1];	
				   var classPosn = $(this).parent().attr("class").split(" ")[0];
				   
				   //Check if the arc should be on right or left				   
				   if(classPosn == "arc_left"){				   
					    if(chek_number_of_circles == "totalelements1")
						{
						   if(first){
							  var first_elem_ht = $(this).height();
							  var first_elem_ht_half = first_elem_ht/2;
							  var ht_for_first = ht_half_container - first_elem_ht_half   ;
							  $(this).css("top",ht_for_first + one_tenth);
							  $(this).css("left",(origin_width + two_tenth));
						   }
						}
						if(chek_number_of_circles == "totalelements2"){
				     
							if(first){
							  var first_elem_ht = $(this).height();
							  var first_elem_ht_half = first_elem_ht/2;
							  elem2_first_top = ht_half_container - first_elem_ht_half + one_tenth  ;
							  elem2_first_left = (origin_width + two_tenth);
							  $(this).css("top",elem2_first_top);
							  $(this).css("left", elem2_first_left);
						   }
						   if(second){
							  var second_elem_ht = $(this).height();
							  var second_elem_ht_half = second_elem_ht/2;
							  elem2_second_top = ht_half_container - (second_elem_ht_half * 3) - two_tenth;
							  elem2_second_left = (origin_width - six_tenth);
							  $(this).css("top",elem2_second_top );
							  $(this).css("left", elem2_second_left);
						   }	    
							
						}
						if(chek_number_of_circles == "totalelements3"){
				     
							if(first){
							  var first_elem_ht = $(this).height();
							  var first_elem_ht_half = first_elem_ht/2;
							  elem3_first_top = ht_half_container - first_elem_ht_half + one_tenth  ;
							  elem3_first_left = (origin_width + two_tenth);
							  $(this).css("top",elem3_first_top );
							  $(this).css("left",elem3_first_left);
						   }
						   if(second){
							  var second_elem_ht = $(this).height();
							  var second_elem_ht_half = second_elem_ht/2;
							  elem3_second_top = ht_half_container - (second_elem_ht_half * 3) - two_tenth ;
							  elem3_second_left = (origin_width - six_tenth) ;
							  $(this).css("top",elem3_second_top);
							  $(this).css("left", elem3_second_left);
						   }
						   if(third){
							  var third_elem_ht = $(this).height();
							  var third_elem_ht_half = third_elem_ht/2;
							  elem3_third_top = ht_half_container +(third_elem_ht_half) + two_tenth + three_tenth ;
							  elem3_third_left = (origin_width - six_tenth);
							  $(this).css("top",elem3_third_top );
							  $(this).css("left", elem3_third_left);
						   }
						}
						if(chek_number_of_circles == "totalelements4"){
				     
							if(first){
							  var first_elem_ht = $(this).height();
							  var first_elem_ht_half = first_elem_ht/2;
							  elem4_first_top = ht_half_container - first_elem_ht_half   ;
							  elem4_first_left = (origin_width + two_tenth);
							  $(this).css("top",elem4_first_top);
							  $(this).css("left",elem4_first_left);
						   }
						   if(second){
							  var second_elem_ht = $(this).height();
							  var second_elem_ht_half = second_elem_ht/2;
							  elem4_second_top = ht_half_container - (second_elem_ht_half * 3)  ;
							  elem4_second_left = (origin_width - two_tenth);
							  $(this).css("top",elem4_second_top);
							  $(this).css("left", elem4_second_left);
						   }
						   if(third){
							  var third_elem_ht = $(this).height();
							  var third_elem_ht_half = third_elem_ht/2;
							  elem4_third_top = ht_half_container +(third_elem_ht_half) ;
							  elem4_third_left = (origin_width - two_tenth);
							  $(this).css("top",elem4_third_top );
							  $(this).css("left", elem4_third_left);
						   }
						   if(fourth){
							  var fourth_elem_ht = $(this).height();
							  var fourth_elem_ht_half = fourth_elem_ht/2;
							  elem4_fourth_top = ht_half_container -(fourth_elem_ht_half *3) + one_tenth_m - one_half;
							  elem4_fourth_left = (origin_width - fourth_elem_ht_half * 2) - one_tenth;
							  $(this).css("top",elem4_fourth_top );
							  $(this).css("left",elem4_fourth_left);
						   }
						}
						if(chek_number_of_circles == "totalelements5"){
				        	if(first){
							  var first_elem_ht = $(this).height();
							  var first_elem_ht_half = first_elem_ht/2;
							  elem5_first_top = ht_half_container - first_elem_ht_half ;
							  elem5_first_left = origin_width + two_tenth;
							  $(this).css("top",elem5_first_top);
							  $(this).css("left",elem5_first_left);
						   }
						   if(second){
							  var second_elem_ht = $(this).height();
							  var second_elem_ht_half = second_elem_ht/2;
							  elem5_second_top = ht_half_container - (second_elem_ht_half * 3)  ;
							  elem5_second_left = origin_width - two_tenth;
							  $(this).css("top",elem5_second_top);
							  $(this).css("left",elem5_second_left);
						   }
						   if(third){
							  var third_elem_ht = $(this).height();
							  var third_elem_ht_half = third_elem_ht/2;
							  elem5_third_top = ht_half_container +(third_elem_ht_half) ;
							  elem5_third_left = origin_width - two_tenth;
							  $(this).css("top",elem5_third_top);
							  $(this).css("left",elem5_third_left);
						   }
						   if(fourth){
							  var fourth_elem_ht = $(this).height();
							  var fourth_elem_ht_half = fourth_elem_ht/2;
							   elem5_fourth_top = ht_half_container -(fourth_elem_ht_half *3) + one_tenth_m - one_half ;
							   elem5_fourth_left = (origin_width - fourth_elem_ht_half * 2) - one_tenth;
							  $(this).css("top",elem5_fourth_top );
							  $(this).css("left", elem5_fourth_left);
						   }
						   if(fifth){
							  var fifth_elem_ht = $(this).height();
							  var fifth_elem_ht_half = fifth_elem_ht/2;
							  elem5_fifth_top = ht_half_container +(fifth_elem_ht_half *2) + one_tenth ;
							  elem5_fifth_left = (origin_width - fifth_elem_ht_half * 2) - one_tenth;
							  $(this).css("top",elem5_fifth_top);
							  $(this).css("left",elem5_fifth_left);
						   }
						}
						
						if(chek_number_of_circles == "totalelements6"){
				     
							if(first){
							  var first_elem_ht = $(this).height();
							  var first_elem_ht_half = first_elem_ht/2;
							  elem6_first_top = ht_half_container - first_elem_ht_half + one_tenth  ;
							  elem6_first_left = (origin_width + two_tenth);
							  $(this).css("top",elem6_first_top);
							  $(this).css("left",elem6_first_left);
						   }
						   if(second){
							  var second_elem_ht = $(this).height();
							  var second_elem_ht_half = second_elem_ht/2;
							  elem6_second_top = ht_half_container - (second_elem_ht_half * 3)  ;
							  elem6_second_left = (origin_width + two_tenth);
							  $(this).css("top",elem6_second_top );
							  $(this).css("left",elem6_second_left);
						   }
						   if(third){
							  var third_elem_ht = $(this).height();
							  var third_elem_ht_half = third_elem_ht/2;
							  elem6_third_top = ht_half_container +(third_elem_ht_half) + two_tenth ;
							  elem6_third_left = (origin_width + two_tenth);
							  $(this).css("top",elem6_third_top );
							  $(this).css("left", elem6_third_left);
						   }
						   if(fourth){
							  var fourth_elem_ht = $(this).height();
							  var fourth_elem_ht_half = fourth_elem_ht/2;
							/*  elem6_fourth_top = ht_half_container -(fourth_elem_ht_half *3) + one_tenth_m - fourth_elem_ht_half*2 - two_tenth_m ;*/
							    elem6_fourth_top = ht_half_container +(fourth_elem_ht_half *2) + one_tenth + one_half_m ;
							  elem6_fourth_left = (origin_width + two_tenth);
							  $(this).css("top",elem6_fourth_top );
							  $(this).css("left", elem6_fourth_left);
						   }
						   if(fifth){
							  var fifth_elem_ht = $(this).height();
							  var fifth_elem_ht_half = fifth_elem_ht/2;
							/*  elem6_fifth_top = ht_half_container +(fifth_elem_ht_half *2) + one_tenth + one_half_m ;*/
							  elem6_fifth_top = ht_half_container +(fifth_elem_ht_half *2) + one_tenth + fifth_elem_ht_half*2 + one_half_m ;
							  elem6_fifth_left = (origin_width + two_tenth);
							  $(this).css("top",elem6_fifth_top);
							  $(this).css("left", elem6_fifth_left);
						   }
						   if(sixth){
							  var sixth_elem_ht = $(this).height();
							  var sixth_elem_ht_half = sixth_elem_ht/2;
							  /*elem6_sixth_top = ht_half_container +(sixth_elem_ht_half *2) + one_tenth + sixth_elem_ht_half*2 + one_half_m ;*/
							    elem6_sixth_top = ht_half_container +(sixth_elem_ht_half *2) + one_tenth + sixth_elem_ht *2 + one_half_m ;
							  elem6_sixth_left = (origin_width + two_tenth);
							  $(this).css("top",elem6_sixth_top  );
							  $(this).css("left",elem6_sixth_left);
						   }
						}
				    }
			    });
				
				
				/*CLICK OF THE OPTIONS*/
				var sel_obj = $(this).children(".branches");
				
				
				$(sel_obj).on("click",function(){
				 var thisObject = $(this);
				 var thisParentNumber = $(this).parent().attr("class").split(" ")[1];
				 var currentlySelectedClass = $(thisObject).attr("class").split(" ")[1];
				 
				 /*********************FOR THE SWITCHING ACTION************************************/
				 
				 if(settings.selectedAction == "switch"){				
				 
				 if(thisParentNumber == "totalelements6"){		 
				 
					if(currentlySelectedClass == "option1"){
					  //DO NOTHING AS IT IS IN THE SELCTED POSITION ITSELF					    
				     }
					 if(currentlySelectedClass == "option2"){	
						 $(thisObject).animate({top:elem6_first_top,left:elem6_first_left}, settings.animationTime);
						 $(thisObject).siblings(".option1").animate({top:elem6_second_top,left:elem6_second_left}, settings.animationTime);					 
						 $(thisObject).removeClass().addClass("branches option1");						 	
						 $(thisObject).siblings(".option1").removeClass().addClass("branches option2");	
						 selectionFunction(thisObject);
						 
					 } 
					 if(currentlySelectedClass == "option3"){	
						 $(thisObject).animate({top:elem6_first_top,left:elem6_first_left}, settings.animationTime);
						 $(thisObject).siblings(".option1").animate({top:elem6_third_top,left:elem6_third_left}, settings.animationTime);					 
						 $(thisObject).removeClass().addClass("branches option1");
						 $(thisObject).siblings(".option1").removeClass().addClass("branches option3");	
						 selectionFunction(thisObject);
				     }
					 if(currentlySelectedClass == "option4"){	
						 $(thisObject).animate({top:elem6_first_top,left:elem6_first_left}, settings.animationTime);
						 $(thisObject).siblings(".option1").animate({top:elem6_fourth_top,left:elem6_fourth_left}, settings.animationTime);					 
						 $(thisObject).removeClass().addClass("branches option1");
						 $(thisObject).siblings(".option1").removeClass().addClass("branches option4");
						 selectionFunction(thisObject);
					 }
					 if(currentlySelectedClass == "option5"){	
						 $(thisObject).animate({top:elem6_first_top,left:elem6_first_left}, settings.animationTime);
						 $(thisObject).siblings(".option1").animate({top:elem6_fifth_top,left:elem6_fifth_left}, settings.animationTime);					 
						 $(thisObject).removeClass().addClass("branches option1");
						 $(thisObject).siblings(".option1").removeClass().addClass("branches option5");	
						 selectionFunction(thisObject);
					 }
					 if(currentlySelectedClass == "option6"){	
						 $(thisObject).animate({top:elem6_first_top,left:elem6_first_left}, settings.animationTime);
						 $(thisObject).siblings(".option1").animate({top:elem6_sixth_top,left:elem6_sixth_left}, settings.animationTime);					 
						 $(thisObject).removeClass().addClass("branches option1");
						 $(thisObject).siblings(".option1").removeClass().addClass("branches option6");	
						 selectionFunction(thisObject);
					 }
				 }
				 
				 if(thisParentNumber == "totalelements5"){				 
				 
					if(currentlySelectedClass == "option1"){
					  //DO NOTHING AS IT IS IN THE SELCTED POSITION ITSELF					   
				     }
					 if(currentlySelectedClass == "option2"){	
						 $(thisObject).animate({top:elem5_first_top,left:elem5_first_left}, settings.animationTime);
						 $(thisObject).siblings(".option1").animate({top:elem5_second_top,left:elem5_second_left}, settings.animationTime);					 
						 $(thisObject).removeClass().addClass("branches option1");
						 $(thisObject).siblings(".option1").removeClass().addClass("branches option2");
						 selectionFunction(thisObject);						 
					 } 
					 if(currentlySelectedClass == "option3"){	
						 $(thisObject).animate({top:elem5_first_top,left:elem5_first_left}, settings.animationTime);
						 $(thisObject).siblings(".option1").animate({top:elem5_third_top,left:elem5_third_left}, settings.animationTime);					 
						 $(thisObject).removeClass().addClass("branches option1");
						 $(thisObject).siblings(".option1").removeClass().addClass("branches option3");	
						 selectionFunction(thisObject);						 
				     }
					 if(currentlySelectedClass == "option4"){	
						 $(thisObject).animate({top:elem5_first_top,left:elem5_first_left}, settings.animationTime);
						 $(thisObject).siblings(".option1").animate({top:elem5_fourth_top,left:elem5_fourth_left}, settings.animationTime);					 
						 $(thisObject).removeClass().addClass("branches option1");
						 $(thisObject).siblings(".option1").removeClass().addClass("branches option4");	
						 selectionFunction(thisObject);
					 }
					 if(currentlySelectedClass == "option5"){	
						 $(thisObject).animate({top:elem5_first_top,left:elem5_first_left}, settings.animationTime);
						 $(thisObject).siblings(".option1").animate({top:elem5_fifth_top,left:elem5_fifth_left}, settings.animationTime);					 
						 $(thisObject).removeClass().addClass("branches option1");
						 $(thisObject).siblings(".option1").removeClass().addClass("branches option5");	
						 selectionFunction(thisObject);
					 }					 
				 }
				 
				 if(thisParentNumber == "totalelements4"){				 
				 
					if(currentlySelectedClass == "option1"){
					  //DO NOTHING AS IT IS IN THE SELCTED POSITION ITSELF
				     }
					 if(currentlySelectedClass == "option2"){	
						 $(thisObject).animate({top:elem4_first_top,left:elem4_first_left}, settings.animationTime);
						 $(thisObject).siblings(".option1").animate({top:elem4_second_top,left:elem4_second_left}, settings.animationTime);					 
						 $(thisObject).removeClass().addClass("branches option1");
						 $(thisObject).siblings(".option1").removeClass().addClass("branches option2");
						 selectionFunction(thisObject);
					 } 
					 if(currentlySelectedClass == "option3"){	
						 $(thisObject).animate({top:elem4_first_top,left:elem4_first_left}, settings.animationTime);
						 $(thisObject).siblings(".option1").animate({top:elem4_third_top,left:elem4_third_left}, settings.animationTime);					 
						 $(thisObject).removeClass().addClass("branches option1");
						 $(thisObject).siblings(".option1").removeClass().addClass("branches option3");	
						 selectionFunction(thisObject);
				     }
					 if(currentlySelectedClass == "option4"){	
						 $(thisObject).animate({top:elem4_first_top,left:elem4_first_left}, settings.animationTime);
						 $(thisObject).siblings(".option1").animate({top:elem4_fourth_top,left:elem4_fourth_left}, settings.animationTime);					 
						 $(thisObject).removeClass().addClass("branches option1");
						 $(thisObject).siblings(".option1").removeClass().addClass("branches option4");
						 selectionFunction(thisObject);
					 }					 					 
				 }
				 
				 if(thisParentNumber == "totalelements3"){				 
				    
					if(currentlySelectedClass == "option1"){
					  //DO NOTHING AS IT IS IN THE SELCTED POSITION ITSELF
				     }
					 if(currentlySelectedClass == "option2"){
					    
						 $(thisObject).animate({top:elem3_first_top,left:elem3_first_left}, settings.animationTime);
						 $(thisObject).siblings(".option1").animate({top:elem3_second_top,left:elem3_second_left}, settings.animationTime);					 
						 $(thisObject).removeClass().addClass("branches option1");
						 $(thisObject).siblings(".option1").removeClass().addClass("branches option2"); 
						 selectionFunction(thisObject);
					 } 
					 if(currentlySelectedClass == "option3"){	
						 $(thisObject).animate({top:elem3_first_top,left:elem3_first_left}, settings.animationTime);
						 $(thisObject).siblings(".option1").animate({top:elem3_third_top,left:elem3_third_left}, settings.animationTime);					 
						 $(thisObject).removeClass().addClass("branches option1");
						 $(thisObject).siblings(".option1").removeClass().addClass("branches option3");	
						 selectionFunction(thisObject);
				     }					 				 					 
				 }
				 
				 if(thisParentNumber == "totalelements2"){				 
				    
					if(currentlySelectedClass == "option1"){
					  //DO NOTHING AS IT IS IN THE SELCTED POSITION ITSELF
				     }
					 if(currentlySelectedClass == "option2"){
					    
						 $(thisObject).animate({top:elem2_first_top,left:elem2_first_left}, settings.animationTime);
						 $(thisObject).siblings(".option1").animate({top:elem2_second_top,left:elem2_second_left}, settings.animationTime);					 
						 $(thisObject).removeClass().addClass("branches option1");
						 $(thisObject).siblings(".option1").removeClass().addClass("branches option2");
						 selectionFunction(thisObject);
					 }					 				 				 					 
				 }
				 
				 }
				 
				 if(settings.selectedAction == "selectOnly"){		     
					 
					 $(thisObject).parent().children('.branches').children().each(function(){						 
					 var check = $(this).children().eq(0).hasClass("selected_option_container");
					 if(check){
						 $(this).children().eq(0).removeClass("selected_option_container").addClass("option_container");
					 }
					 $(thisObject).children().children().eq(0).removeClass().addClass("selected_option_container");					 
					 
				 });
					 
				 }
				
			});
			
			/******************GENERAL FUNCTION TO REMOVE THE SELECTED OPTION ON CLICK AND ATTACH IT TO THE RIGHT ONE ********/
			function selectionFunction(thisObj){
				/*ON CLICK, REMOVE THE SELECTED STATE FROM ALL OPTIONS*/
				 $(thisObj).parent().children('.branches').children().each(function(){						 
					 var check = $(this).children().eq(0).hasClass("selected_option_container");
					 if(check){
						 $(this).children().eq(0).removeClass("selected_option_container").addClass("option_container");
					 }
				 });
				 /*ONCE ALL SELECTED OPTIONS ARE CLEARED, ADD THE SELECTED OPTION TO THE CLICKED ONE*/
				 $(thisObj).parent().children('.branches.option1').children().children().eq(0)
				 .removeClass().addClass("selected_option_container");				
			};
	  
});
        
};