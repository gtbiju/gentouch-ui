$(document).ready(function(){
	/*CHECKS FOR A CLASS - FASTCLICK AND APPLIES THE SAME ON THAT ELEMENT ONLY*/
	/* var button = document.querySelector(".fastclick");
	new FastClick(button);	 */	
				
	/*BOX SHADOW FOR BUTTON CLICK*/	
    $('.bxbtn').on('click',function(){
			var thisObj = $(this);
			$(thisObj).addClass("btn_box_shadow");
				 setTimeout(function(){ 
					 $(thisObj).removeClass("btn_box_shadow"); 
					 $(thisObj).css("background-color","#044f6e");
		        }, 300);
	});
	
	/***********GENERAL TAB **********************/
	    $('.gen_le_tabset li').eq(0).addClass("active");
	    /* $('.gen_le_tabset').each(function(){
			$(this).children().eq(0).addClass("active");
		}); */
		var thisObjset = "#" + $('.gen_le_tabset li').parent().attr("id") + " li";//#riskProfile_tabs li	
		var thisObjId = "#" + $('.gen_le_tabset li').parent().attr("id");//#riskProfile_tabs
		var containerClass =  "." + $('.gen_le_tabset li').parent().attr("id") + "_container";//.riskProfile_tabs_container	
		var thisObj_link = "#" + $('.gen_le_tabset li').children().attr("rel");	
		toggleTabs(thisObjId,containerClass,thisObj_link);
				
	$('.gen_le_tabset li').on('click',function (e) {
		e.preventDefault();					
		var thisObjset = "#" + $(this).parent().attr("id") + " li";//#riskProfile_tabs li	
		var thisObjId = "#" + $(this).parent().attr("id");//#riskProfile_tabs
		var containerClass =  "." + $(this).parent().attr("id") + "_container";//.riskProfile_tabs_container	
		var thisObj_link = "#" + $(this).children().attr("rel");					
		$(thisObjset).removeClass("active");
		$(this).addClass("active");
		toggleTabs(thisObjId,containerClass,thisObj_link);					
	});	
	
	/**********************FUNCTIONS******************************/
	function toggleTabs(thisObjId,containerClass,thisObj_link){			  
		$(containerClass).children('.customtab').removeClass("active");
	    var rel_id = thisObj_link;
		$(containerClass).children().each(function(){
			var attrid = "#" + $(this).attr("id");	
			if(attrid == rel_id){
				$(attrid).siblings().hide();
				$(attrid).removeClass("active").addClass("active").fadeIn();
			}
	    }); 
	};
});