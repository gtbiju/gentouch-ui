$(document).ready(function(){		 
	var mdl_Class;	 
	/* SET THE DISPLAY OF THE OVERLAY AS NONE INITIALLY ON LOAD */
	$('.general-modal-overlay').css("display","none");
	 		 
	/*HIDE THE OVERLAY UPON CLICKING CLOSE*/
	$('.general-overlay-close-button').click(function(){	
	  hideOverlay();
	});
	$('.general-overlay-hide-button').click(function(){
		hideOverlay();
	})
	$('.general-overlay-cancel-button').click(function(){
		hideOverlay();
	})
	 
});


function displayOverlay(modalClass){	

	mdl_Class = "." + modalClass;	
	$(mdl_Class).css("display","block");
	/*THIS BELOW LINE IS IE8 FIX FOR OPACITY*/
	$('.general-modal-overlay').css('filter', 'alpha(opacity=75)');	 
	
	if(mdl_Class != '.theme_switcher_popup')	{
        $('.general-modal-overlay').not('.themePopup').show();
			$('.general-modal-overlay').not('.themePopup').addClass("open");
	}
	else{
		$(mdl_Class).next('.general-modal-overlay').show();
		$(mdl_Class).next('.general-modal-overlay').addClass("open");
	}
	$(mdl_Class).removeClass("hide_popup");
	var check_open = $(mdl_Class).hasClass("show_popup");
	if(!check_open){
	  $(mdl_Class).addClass("show_popup");	
	  $("body").removeClass("hide_scroll").addClass("hide_scroll");
	  alignModalPopUpOnCenter(mdl_Class);				   
	}		
};

function hideOverlay(){	   
	  /* $('.general-overlay-container').removeClass("show_popup").addClass("hide_popup");
	   $('.general-modal-overlay').css("display","none");
	   $('.general-modal-overlay').removeClass("open");	 
	   $("body").removeClass("hide_scroll"); 
	   $(mdl_Class).css("display","none");*/
}	
	
/*FUNCTION TO OPEN THE OVERLAY AND ITS CONTAINER*/		 
function alignModalPopUpOnCenter(mdl_Class){
    
	var flag = $('.general-modal-overlay').hasClass("open");

	if(flag){
		 var top = ($(window).height() - $(mdl_Class).height()) / 2;
		
		 if(mdl_Class != '.theme_switcher_popup'){	   
			 var left = ($(window).width() - $(mdl_Class).width()) / 2;	
			 $('.general-overlay-container').css({'left': left});  
		 }
		 else{
			 $('.general-overlay-container').css({'left': 'auto'});  
		 }

		/* $('.general-overlay-container').css({'top': top + $(document).scrollTop()});  */	 
		 $('.general-overlay-container').css({'top': top});		
	}
	else{
		/*ELSE LOGIC*/	 
	}	
}


