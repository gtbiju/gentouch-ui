function initFixedTableHeadColHeader(tableWrapper) {
    var tableWrapper = tableWrapper,
    table = tableWrapper.children("table"),
    tableThead = table.children("thead"),
    tableTheadTh = tableThead.children("tr").children("th"),
    tableTbodytd = table.children("tbody").children("tr:first-child").children("td"),
    tableTheadThdiv;
    tableWrapper.scrollTop(0).scrollLeft(0);
	tableThead.css({
			"position": "relative",
			"left": "auto",
			"top": '0px'
		});
	tableWrapper.css({
		"overflow-y": "scroll"
		});
	tableTheadTh.wrapInner('<div/>');
	tableTbodytd.wrapInner('<div/>');
	tableTheadThdiv = tableTheadTh.children("div");
	//var height = tableThead.outerHeight(true) + 5,
	var height = tableThead.outerHeight(true) -1,
		tdHeight = tableTbodytd.outerHeight(true),
		tdOffset = [],//offset values for td
		tdWidth = [], // width for td
		index = [];
	tableWrapper.css("padding-top", height);
	tableTbodytd.each(function (i) {
		var $this = $(this);
		tdOffset[tdOffset.length] = $this.offset().left;
		tdWidth[tdWidth.length] = $this.width();
	});

	tableTheadTh.each(function (i) {
		var $this = $(this),
			offsetLeft = $this.offset().left;
		index[index.length] = $.inArray(offsetLeft, tdOffset);
	});
	// assign the width for th
	tableTheadThdiv.each(function (i) {
		var $this = $(this);
		$this.width(tdWidth[index[i]]);
	});

	tableThead.css("position", "absolute");
	tableTbodytd.children('div').each(function (i) {
		var $this = $(this);
		$this.css('width', tdWidth[i]);
	});
    //bind to scroll event
    tableWrapper.scroll(function () {
        tableThead.css("top", (tableWrapper.scrollTop()));
    });
}