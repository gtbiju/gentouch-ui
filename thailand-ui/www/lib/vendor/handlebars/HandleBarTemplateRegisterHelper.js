(function() {
		Handlebars.registerHelper('ifCond', function (variable,value,options) {
			// Behavior is to render the positive path if the variable and expressions are matching
			if (variable != value) { 
			  return options.inverse(this);
			} else {
			  return options.fn(this);
			}
	    });
	})();