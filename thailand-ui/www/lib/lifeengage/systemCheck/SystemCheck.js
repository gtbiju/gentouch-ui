/**
 * Cordova plugin for System Check 
 * Created by Sajith M
 */
 //The other system checks using native code can be added under System check.
CDVPLUGIN = ( typeof CDVPLUGIN == 'undefined' ? {} : CDVPLUGIN );
var cordova = window.cordova || window.Cordova;
CDVPLUGIN.SystemCheck = {
		RootCheck : function(params, success, fail) {
        return cordova.exec(function(args) {
            success(args);
        }, function(args) {
            fail(args);
        }, 'SystemCheck', 'RootCheck', [params]);
    },
    PinCheck : function(params, success, fail) {
        return cordova.exec(function(args) {
            success(args);
        }, function(args) {
            fail(args);
        }, 'SystemCheck', 'PinCheck', [params]);
    },
    DeleteCheck : function(params, success, fail){
    	return cordova.exec(function(args) {
            success(args);
        }, function(args) {
            fail(args);
        }, 'SystemCheck', 'DeleteCheck', [params]);
    }
}

