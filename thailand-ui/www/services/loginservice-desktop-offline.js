var sqlite3 = require('sqlite3').verbose();
//console.log("SQLite3 is available " + sqlite3);
var loginUtility;
(function() {
	loginUtility = {

		saveToken : function(userDetails, successCallback, errorCallback) {
			try {
				var self = this;
				self.checkAgentIdExistingInDb(userDetails, function(data) {
					if (data.length > 0) {
						sql = _dbOperations._buildUpdateScript("UserDetails",
								userDetails);
						sql += " WHERE 	userId = '" + userDetails["userId"]
								+ "'";
					} else {
						sql = _dbOperations._buildInsertScript("UserDetails",
								userDetails);

					}
					_dbOperations._executeSql(sql, function(data) {
						successCallback(data);
					}, function(error) {
						errorCallback(error);
					});
				});

			} catch (exception) {
				errorCallback(exception);
			}
		},

		fetchToken : function(userId, successCallback, errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				dataObj.userId = userId;
				sql = _dbOperations._buildFetchScript("token,lastLoggedInDate",
						"UserDetails", dataObj);
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},

		checkAgentIdExistingInDb : function(user, successCallback,
				errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				dataObj.userId = user.userId;
				sql = _dbOperations._buildFetchScript("password",
						"UserDetails", dataObj);
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		checkAgentIdExistingInDbAndReturnToken : function(user, successCallback,
				errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				dataObj.userId = user.userId;
				dataObj.password = user.password;
				sql = _dbOperations._buildFetchScriptForLoginCredentialCheck("UserDetails", dataObj);
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		}
	};
	var _dbOperations = {
		_db : null,
		_dbInit : function(sql, successCallback, errorCallback) {
			var self = this;
			self._db = new sqlite3.Database(rootConfig.configAndContentPath
					+ '/LifeEngageData.db');

			// Add functions to db that mimic a WebSQL database
			self._db.transaction = function(callBack) {
				var tx = {};
				callBack(tx);
			};
			var tblUserDetails = "CREATE TABLE IF NOT EXISTS UserDetails (userId TEXT PRIMARY KEY ,password TEXT,token TEXT NOT NULL,lastLoggedInDate TEXT NOT NULL)"
			self._executeSql(tblUserDetails, function() {
				self._executeSql(sql, successCallback, errorCallback);
			}, function(error) {
				alert("Error in creating UserDetails table "
						+ JSON.stringify(error));
			});

		},

		_closeDB : function(dbName, successCallback, errorCallback) {
			try {
				if (self._db != null) {
					self._db.close(dbName);
					if (successCallback && typeof successCallback == "function") {
						successCallback();
					} else {
						self._defaultCallBack();
					}
				}
			} catch (e) {
				//console.log("ERROR: " + e.message);
				errorCallback(e);
			}
		},
		_executeSql : function(sql, successCallback, errorCallback) {

			var self = this;
			try {
				if (self._db == null) {
					self._dbInit(sql, successCallback, errorCallback);
				} else {
					self._db.all(sql, [], function(error, rows) {
						if (error == null) {
							var results = {};
							results.rows = rows;
							results.rows.item = function(i) {
								return this[i];
							};
							if (successCallback
									&& typeof successCallback == "function") {

								successCallback(results);
							} else {

								self._defaultCallBack(sql, results);
							}
						} else {
							if (errorCallback
									&& typeof errorCallback == "function") {
								errorCallback(error);
							} else {
								self._errorHandler(error);
							}
						}
					});
				}

			} catch (e) {
				//console.log("ERROR: " + e.message);
				errorCallback(e);
			}

		},
		_buildInsertScript : function(tableName, trasactionData) {
			var members = this._getAttributesList(trasactionData);
			var values = this._getMembersValue(trasactionData, members);
			if (members.length === 0) {
				throw 'buildInsertSQL : Error, try to insert an empty object in the table '
						+ tableName;
			}
			var sql = "INSERT INTO " + tableName + " (";
			sql += this._arrayToString(members, ',');
			sql += ") VALUES (";
			sql += this._arrayToString(values, ',');
			sql += ")";
			return sql;

		},
		_buildUpdateScript : function(tableName, trasactionData) {
			var self = this;
			var sql = "UPDATE " + tableName + " SET ";
			var members = self._getAttributesList(trasactionData);
			if (members.length === 0) {
				throw 'buildUpdateSQL : Error, try to insert an empty object in the table '
						+ tableName;
			}
			var values = self._getMembersValue(trasactionData, members);

			var memLength = members.length;
			for ( var i = 0; i < memLength; i++) {
				//console.log(members[i] + " = " + values[i]);
				sql += members[i] + " = " + values[i];
				if (i < memLength - 1) {
					sql += ', ';
				}
			}

			return sql;
		},
		_buildFetchScript : function(selectCol, tableName, dataObj) {
			var self = this;
			var sql = "SELECT " + selectCol + " FROM " + tableName;
			if (!jQuery.isEmptyObject(dataObj)) {
				sql += " WHERE ";
				var members = self._getAttributesList(dataObj);
				if (members.length === 0) {
					throw 'buildUpdateSQL : Error, try to insert an empty object in the table '
							+ tableName;
				}
				var values = self._getMembersValue(dataObj, members);
				var memLength = members.length;
				for ( var i = 0; i < memLength; i++) {
					if (members[i] == "filter") {
						continue;
					}
					sql += members[i] + " = " + values[i];
					if (i < memLength - 1) {
						sql += " AND ";
					}
				}
			}
			return sql;
		},
		_buildFetchScriptForLoginCredentialCheck : function(tableName, dataObj) {
			var self = this;
			var sql = "SELECT * FROM (SELECT COUNT(*) AS userCount FROM " + tableName;
			//if(dataObj.filter==null && dataObj.filter!="All"){
			if (!jQuery.isEmptyObject(dataObj)) {
				if(dataObj.userId && dataObj.password){
					sql += " WHERE userId = '" + dataObj.userId + "') left outer join (SELECT * FROM " + tableName
							+ " WHERE userId = '" + dataObj.userId + "' AND password = '"+ dataObj.password +"')";
					//console.log("CredCheck SQL**  "+ sql);
				} else {
					throw '_buildFetchScriptForLoginCredentialCheck : Error, username or password missing '
				}
			}
			return sql;
		},
		_buildDeleteScript : function(tableName, whereClause) {
			var self = this;
			var sql = 'DELETE FROM ' + tableName + ' WHERE ' + whereClause;
			return sql;
		},
		_getRecordsArray : function(results) {
			var resultsArray = [];
			if (results.rows.length == 0) {
				//console.log("No Results returned from DB");
			} else {
				for ( var i = 0; i < results.rows.length; i++) {
					resultsArray.push(results.rows.item(i));
				}
			}
			return resultsArray;
		},
		_getMembersValue : function(obj, members) {
			var valueArray = [];
			for ( var i = 0; i < members.length; i++) {
				if (members[i].toUpperCase() == "ID") {
					valueArray.push(obj[members[i]]);
				} else {
					valueArray.push("'" + obj[members[i]] + "'");
				}
			}
			return valueArray;
		},
		_getAttributesList : function(obj) {
			var memberArray = [];
			for ( var elm in obj) {
				if (typeof this[elm] === 'function' && !obj.hasOwnProperty(elm)) {
					continue;
				}
				memberArray.push(elm);
			}
			return memberArray;
		},
		_arrayToString : function(array, separator) {
			var result = '';
			for ( var i = 0; i < array.length; i++) {
				result += array[i];
				if (i < array.length - 1) {
					result += separator;
				}
			}
			return result;
		},
		_defaultCallBack : function(transaction, results) {
		},
		_errorHandler : function(transaction, error) {
			//console.log('Error : ' + error.message + ' (Code ' + error.code+ ') Transaction.sql = ' + transaction.sql);
					
		}

	};
	return loginUtility;
})();