var PlatformInfoUtility;

(function() {
	PlatformInfoUtility = {
			getPlatformInfo : function(successCallback, errorCallback) {
				var self = this;
				
				try {

					window.plugins.DeviceUtil
							.getDeviceInfo(
									function getDeviceInfo_success(parsedData) {

										 successCallback(parsedData);

									}, null);
			
				   } catch (exception) {
						if (errorCallback && typeof errorCallback == "function") {
							errorCallback(exception);
						} else {
							self._errorHandler(exception);
						}
				}

		},
		
		/*Default failure callback if the error callback is not specified*/
		_errorHandler : function(error) {
			console.log('Error : ' + error.message + ' (Code ' + error.code
					+ ')');
		}

	};

	return PlatformInfoUtility;
})();

