﻿/*
 * Copyright 2015, LifeEngage 
 */
//Implementation for life engage AuthService
var authServiceUtility;
(function() {
	authServiceUtility = {
		login : function(user, successCallback, errorCallback, $http) {
			 
			  	var loginServiceUrl = rootConfig.loginUrl;
	            var username=user.username;
				var password=user.password;	
				var req = {
						 method: 'POST',
						 url: loginServiceUrl,
						 headers: {
						   'Content-Type': "application/x-www-form-urlencoded; charset=utf-8"
						 },
						 transformRequest: function(obj) {
						        var str = [];
						        for(var p in obj)
						        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
						        return str.join("&");
						    },
						 data:{j_username:username , j_password:password , j_auth_token:"on"}
						}
				
						$http(req).success(function(data, status, headers, config){
							successCallback(headers("TOKEN"),status);
						}).error(function(data, status){
							 errorCallback(data, status);
						});
		},
		logout : function(successCallback, errorCallback) {
			//once the authentication service is up, remove the hardcoding
			//            $http.post('/logout').success(function(){
			//                success();
			//            }).error(error);
			successCallback();
		},
		authorize : function(accessLevel, role) {
			return false;
		},
		isLoggedIn : function($rootScope) {
			if ($rootScope.isAuthenticated)
				return true;
			else
				return false;
		}
	};

	return authServiceUtility;
})();