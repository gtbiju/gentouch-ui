var EmailServiceUtility;

(function() {
	EmailServiceUtility = {
			sendEmail : function(emailObject, successCallback, errorCallback, options, $http) {
				/*var self = this;
				
				try {
				      window.plugins.email.open(emailObject.toMailIds, emailObject.ccMailIds, emailObject.bccMailIds, emailObject.mailSubject,
				    		 					emailObject.emailBody, emailObject.emailAttachments, emailObject.emailIsHtml,
				    		 					emailObject.openWithApp, null, null);
				
					  successCallback();
			
				   } catch (exception) {
						if (errorCallback && typeof errorCallback == "function") {
							errorCallback(exception);
						} else {
							self._errorHandler(exception);
						}
				}*/

				var self = this;
				var emailServiceUrl = rootConfig.serviceBaseUrl + "emailService/sendEmailWithAttachment";
								
				try {
															
					 var request = {
						        method : 'POST',
						        url : emailServiceUrl,
						        headers : {
						            'Content-Type' : "application/json; charset=utf-8",
						            "Token" : options.headers.Token
						        },
						        data : emailObject
						    }

						    $http(request).success(function(data, status, headers, config) {
						    		successCallback(data.Response);
						    }).error(function(data, status, headers, config) {
							        errorCallback();
						    });
			
				   } catch (exception) {
						if (errorCallback && typeof errorCallback == "function") {
							errorCallback(exception);
						} else {
							self._errorHandler(exception);
						}
				}

		},
		attachPhoto : function(successCallback, errorCallback) {
			
			navigator.camera.getPicture(onSuccess, onFail, {
				quality : 50,
				destinationType : navigator.camera.DestinationType.DATA_URL,
				sourceType : navigator.camera.PictureSourceType.PHOTOLIBRARY,
				mediaType : navigator.camera.MediaType.PICTURE
			});

			function onSuccess(imageData) {
			
				var d = new Date();
				var n = d.getTime();
				var entryName = "photo_" + n + ".jpg";
				var selectedImage = {id:'table', source: 'data:image/jpeg;base64,' + imageData};
				var attchment = 'base64:'+entryName+'//' + imageData;

				successCallback(imageData, selectedImage, attchment);
			}

			function onFail(message) {
					console.log('Failed because: ' + message);
					if (errorCallback && typeof errorCallback == "function") {
						errorCallback(message);
					} 
			}
			
		},
		
		/*Default failure callback if the error callback is not specified*/
		_errorHandler : function(error) {
			console.log('Error : ' + error.message + ' (Code ' + error.code
					+ ')');
		}

	};

	return EmailServiceUtility;
})();

