var EmailServiceUtility;

(function() {
	EmailServiceUtility = {
			sendEmail : function(emailObject, successCallback, errorCallback, options, $http) {
				
				var self = this;
				var emailServiceUrl = rootConfig.serviceBaseUrl + "emailService/sendEmailWithAttachment";
								
				try {
															
					 var request = {
						        method : 'POST',
						        url : emailServiceUrl,
						        headers : {
						            'Content-Type' : "application/json; charset=utf-8",
						            "Token" : options.headers.Token
						        },
						        data : emailObject
						    }

						    $http(request).success(function(data, status, headers, config) {
						    		successCallback(data.Response);
						    }).error(function(data, status, headers, config) {
							        errorCallback();
						    });
			
				   } catch (exception) {
						if (errorCallback && typeof errorCallback == "function") {
							errorCallback(exception);
						} else {
							self._errorHandler(exception);
						}
				}

		},
		
		/*Default failure callback if the error callback is not specified*/
		_errorHandler : function(error) {
			console.log('Error : ' + error.message + ' (Code ' + error.code
					+ ')');
		}

	};

	return EmailServiceUtility;
})();
