var productServiceUtility;
(function() {
	productServiceUtility = {
		getProductDetails : function(transactionObj, successCallback,
				errorCallback) {

			var productJsonName = "eAppWithTabs.json";
			successCallback(productJsonName);

		},
		getAllActiveProducts : function(transactionObj, successCallback,
				errorCallback, options) {

			var carrierId = 1;
			LEProductService.getAllActiveProducts(carrierId, successCallback,
					errorCallback);

		},
		getAllActiveProductsByFilter : function(transactionObj, successCallback,
				errorCallback, options) {

			var carrierId = 1;
			LEProductService.getAllActiveProductsByFilter(transactionObj.Products, successCallback,
					errorCallback);

		},
		getProductsByFilter : function(transactionObj, successCallback,
				errorCallback, options) {

			var carrierId = 1;
			var productIDList= [transactionObj.id];
			LEProductService.getProductsByFilter(carrierId,productIDList,transactionObj, successCallback,
					errorCallback);

		},

		getProduct : function(transactionObj, successCallback, errorCallback, options) {
			try {
				carrierId = transactionObj.Products.carrierCode;
				productId = transactionObj.Products.id;
				options.apiVersion = 2;
				LEProductService.getProductDetails(carrierId, productId,
						successCallback, errorCallback,options);

			} catch (exception) {
				alert("error in  getProduct: " + angular.toJson(exception)
						+ " :error in  getProduct");
				errorCallback(resources.errorMessage);
			}
		}

	};

	return productServiceUtility;
})();