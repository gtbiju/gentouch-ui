﻿/*
* Copyright 2015, LifeEngage 
 */
//Implementation for life engage service save
var productServiceUtility;
(function() {
	productServiceUtility = {
		getProductDetails : function(transactionObj, successCallback,
				errorCallback) {

			var productJsonName = "eAppWithTabs.json";
			successCallback(productJsonName);
		},

		getAllActiveProducts : function(transactionObj, successCallback,
				errorCallback, options, $http) {
			try {
				var getUrl = rootConfig.serviceBaseUrl
						+ "lifeEngageProductService/getActiveProducts";
				var requestInfo = Request();
				requestInfo.Request.RequestPayload = transactionObj;
				callAngularPost(getUrl, requestInfo, options, successCallback,
						errorCallback, $http);

			} catch (exception) {
				alert("error in  getProduct: " + angular.toJson(exception)
						+ " :error in  getProduct");
				errorCallback(resources.errorMessage);
			}
		},
		
		getAllActiveProductsByFilter : function(transactionObj, successCallback,
				errorCallback, options, $http) {
			try {
				var getUrl = rootConfig.serviceBaseUrl
												+ "lifeEngageProductService/V2/getAllProductsByFilter";
				var requestInfo = Request();
				requestInfo.Request.RequestPayload = transactionObj;
				callAngularPost(getUrl, requestInfo, options, successCallback,
												errorCallback, $http);

			} catch (exception) {
				alert("error in  getAllActiveProductsByFilter: " + angular.toJson(exception)
						+ " :error in  getAllActiveProductsByFilter");
				errorCallback(resources.errorMessage);
			}
		},

		getProduct : function(transactionObj, successCallback, errorCallback, options, $http) {
			try {
				var getUrl = rootConfig.serviceBaseUrl
						+ "lifeEngageProductService/V2/getProduct";
				var requestInfo = Request();
				requestInfo.Request.RequestPayload = transactionObj;
				callAngularPost(getUrl, requestInfo, options,
						function(productsArray) {
							successCallback(productsArray.Products[0]);
						}, errorCallback, $http);

			} catch (exception) {
				alert("error in  getProduct: " + angular.toJson(exception)
						+ " :error in  getProduct");
				errorCallback(resources.errorMessage);
			}
		}
	};

	return productServiceUtility;
})();