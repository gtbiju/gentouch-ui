﻿/*
 * Copyright 2015, LifeEngage 
 */
var dataLookupServiceUtility;
(function () {
    dataLookupServiceUtility = {
        getLookUpData: function (transactionObj, successCallback, errorCallback, options, $http) {
			var dataLookupServiceUrl = rootConfig.serviceBaseUrl + "codeLookUpService/getLookUpData";
			var requestInfo = Request();
			transactionDataObj = {};
			transactionDataObj.typeName = transactionObj.type;
			if (transactionObj.code) {
				transactionDataObj.code = transactionObj.code;
			}
			if (localStorage["locale"]) {
				locale = localStorage["locale"].split("_")
				transactionDataObj.language = locale[0];
				transactionDataObj.country = locale[1];
			} else {
				transactionDataObj.language = "en";
				transactionDataObj.country = "US";
			}
			transactionObj = {};
			transactionObj.TransactionData = transactionDataObj;
			requestInfo.Request.RequestPayload.Transactions.push(transactionObj)
			
			 var request = {
   	    		 method: 'POST',
   	    		 url: dataLookupServiceUrl,
   	    		 headers: {
   	    		   'Content-Type': "application/json; charset=utf-8",
   	    		   "Token": options.headers.Token,
				   'Accept': "application/json; charset=utf-8"				   
   	    		 },
   	    		 data: requestInfo
   	    		}

   	    		$http(request).success(function(data, status, headers, config){
   	    			result = data.Response.ResponsePayload.Transactions[0].TransactionData.lookUps;                   
                    successCallback(result);
   	    			}).error(function(data, status, headers, config){
   	    				errorCallback(data, status);
   	    			});
        },

        getMultipleLookUpData:  function(typeList, successCallback,
				errorCallback, options, $http) {
			var _self= this;
			var result =[];
			var dataLookupServiceUrl = rootConfig.serviceBaseUrl
					+ "codeLookUpService/getMultipleLookUpData";
			// var requestInfo = options.requestObj;
			var requestInfo = Request();
			
			if (Array.isArray(typeList) && typeList.length > 0 ) {
				
				typeList.forEach(function(typeobj) {				
					transactionObj = {};
					transactionObj.TransactionData = typeobj;					
					requestInfo.Request.RequestPayload.Transactions.push(transactionObj);					
				});				
				
				if (requestInfo.Request.RequestPayload.Transactions.length > 0) {
					var request = { 
						method : 'POST',
						url : dataLookupServiceUrl,
						headers : {
							'Content-Type' : "application/json; charset=utf-8",
							"Token" : options.headers.Token
						},
						data : requestInfo
					}
		
					$http(request)
							.success(
									function(data, status, headers, config) {
										transactions = data.Response.ResponsePayload.Transactions;								
										transactions.forEach(function(transactionsRsObj) {											
											result.push(transactionsRsObj.TransactionData);
										});							
										successCallback(result);
									}).error(function(data, status, headers, config) {
								errorCallback(data, status);
							});
				} else {
					successCallback(result);
				}
			} else {
				successCallback(result);
			}
		}
	};
    return dataLookupServiceUtility;
})();