/*
 * Copyright 2015, LifeEngage 
 */

(function() {

	DbOperationsUtility['getDetailsForListing'] = function(transactionObj,
			successCallback, errorCallback, options) {
		try {
			this
					.getListings(
							transactionObj,
							function(data) {
								var requiredData = [];
								for ( var i = 0; i < data.length; i++) {
									if (data[i].TransactionData) {
										if (data[i].Type == "illustration")
											data[i].TransactionData.IllustrationOutput = {};
										// get only the required details to
										// be displayed in the listing
										// screen
										var dataNew = {
											"TransactionData" : {
												"Insured" : {
													"BasicDetails" : {
														"firstName" : "",
														"lastName" : "",
														"dob" : ""
													}
												},
												"Proposer" : {
													"BasicDetails" : {
														"firstName" : "",
														"lastName" : ""
													}
												},
												"Payer" : {
													"BasicDetails" : {
														"firstName" : "",
														"lastName" : ""
													}
												},
												"Product" : {
													"ProductDetails" : {
														"productName" : "",
														"productCode" : "",
														"productType" : ""
													},
													"premiumSummary" : {
														"validatedDate" : ""
													}
												}
											}
										};
										if (data[i].Type == "eApp") {
											if (data[i].TransactionData.Proposer.BasicDetails) {
												dataNew.TransactionData.Proposer.BasicDetails.firstName = data[i].TransactionData.Proposer.BasicDetails.firstName;
												dataNew.TransactionData.Proposer.BasicDetails.lastName = data[i].TransactionData.Proposer.BasicDetails.lastName;
											}
											if (data[i].TransactionData.Insured.BasicDetails) {
												dataNew.TransactionData.Insured.BasicDetails.firstName = data[i].TransactionData.Insured.BasicDetails.firstName;
												dataNew.TransactionData.Insured.BasicDetails.lastName = data[i].TransactionData.Insured.BasicDetails.lastName;
											}
											if (data[i].TransactionData.Product.ProductDetails) {
												dataNew.TransactionData.Product.ProductDetails.productName = data[i].TransactionData.Product.ProductDetails.productName;
												dataNew.TransactionData.Product.ProductDetails.productCode = data[i].TransactionData.Product.ProductDetails.productCode;
												dataNew.TransactionData.Product.ProductDetails.productType = data[i].TransactionData.Product.ProductDetails.productType;
											}
										}
										if (data[i].Type != "FNA") {
											if (data[i].TransactionData.Product.ProductDetails) {
												dataNew.TransactionData.Product.ProductDetails.productName = data[i].TransactionData.Product.ProductDetails.productName;
												dataNew.TransactionData.Product.ProductDetails.productCode = data[i].TransactionData.Product.ProductDetails.productCode;
												dataNew.TransactionData.Product.ProductDetails.productType = data[i].TransactionData.Product.ProductDetails.productType;
											}
											if (data[i].TransactionData.Insured.BasicDetails) {
												dataNew.TransactionData.Insured.BasicDetails.firstName = data[i].TransactionData.Insured.BasicDetails.firstName;
												dataNew.TransactionData.Insured.BasicDetails.lastName = data[i].TransactionData.Insured.BasicDetails.lastName;
												dataNew.TransactionData.Insured.BasicDetails.dob = data[i].TransactionData.Insured.BasicDetails.dob;
											}
											if (data[i].TransactionData.Payer.BasicDetails) {
												dataNew.TransactionData.Payer.BasicDetails.firstName = data[i].TransactionData.Payer.BasicDetails.firstName;
												dataNew.TransactionData.Payer.BasicDetails.lastName = data[i].TransactionData.Payer.BasicDetails.lastName;
											}

										}
										if (data[i].Type == "illustration") {
											if (data[i].TransactionData.Product.premiumSummary) {
												dataNew.TransactionData.Product.premiumSummary.validatedDate = data[i].TransactionData.Product.premiumSummary.validatedDate;
											}
										} else {
											var partiesArray = data[i].TransactionData.parties;
											var basicDetails = {
												"BasicDetails" : {
													"firstName" : "",
													"dob" : ""
												}
											};
											dataNew.TransactionData.parties = [];
											for (party in partiesArray) {
												if (partiesArray[party].type == "FNAMyself") {
													basicDetails.BasicDetails.firstName = partiesArray[party].BasicDetails.firstName
															+ " "
															+ partiesArray[party].BasicDetails.lastName;
													basicDetails.BasicDetails.dob = partiesArray[party].BasicDetails.dob;
													dataNew.TransactionData.parties
															.push(basicDetails);
												}
											}
										}
										dataNew.Key1 = data[i].Key1;
										dataNew.Key2 = data[i].Key2;
										dataNew.Key3 = data[i].Key3;
										dataNew.Key4 = data[i].Key4;
										dataNew.Key5 = data[i].Key5;
										dataNew.Key6 = data[i].Key6;
										dataNew.Key13 = data[i].Key13;
										dataNew.Key15 = data[i].Key15;
										dataNew.Key16 = data[i].Key16;
										dataNew.Key19 = data[i].Key19;
										dataNew.Key21 = data[i].Key21;
										dataNew.Id = data[i].Id;
										delete data[i];
										requiredData.push(dataNew);
									}
								}
								successCallback(requiredData);
							}, function(error) {
								errorCallback(error);
							});
		} catch (exception) {
			errorCallback(exception);
		}

	};
	
	DbOperationsUtility['saveReportingAgentsDetails'] = function(transactionObj, successCallback,
			errorCallback, options, $http) {
		var saveUrl = rootConfig.serviceBaseUrl + "lifeEngageService/save";
		var requestInfo = Request();
		requestInfo.Request.RequestPayload.Transactions
				.push(transactionObj)

		var request = {
			method : 'POST',
			url : saveUrl,
			headers : {
				'Content-Type' : "application/json; charset=utf-8",
				"Token" : options.headers.Token
			},
			data : requestInfo
		}

		$http(request)
				.success(
						function(data, status, headers, config) {
							if (data.Response.ResponsePayload.Transactions[0].StatusData.StatusCode == "100") {
								successCallback(data.Response.ResponsePayload.Transactions[0]);
							} else {
								errorCallback(data.Response.ResponsePayload.Transactions[0].StatusData.StatusMessage);
							}
						}).error(function(data, status, headers, config) {
					errorCallback(data, status, resources.errorMessage);
				});
	},
	DbOperationsUtility['saveHierarchyResponse'] =function(hierarchyResponse, successCallback, errorCallback)  {
			try {
				var self = this;
					sql = _dbOperations._buildInsertScript("Hierarchy",
							hierarchyResponse);
					_dbOperations._runSql(sql, function(data) {
						//var transId = data;
						//self.insertStatusRec(transId, successCallback,
								//errorCallback);
					}, function(error) {
						errorCallback(error);
					});
				

			} catch (exception) {
				errorCallback(exception);
			}
		};
		
		DbOperationsUtility['retrieveHierarchyResponse'] =function(AgentCode,sucesscallback,errorCallback) {
		try {
			var sql = _dbOperations._buildFetchScript("*", "Hierarchy", AgentCode);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				sucesscallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	};
	DbOperationsUtility['saveMappedBranchesDetails'] = function(transactionObj, successCallback,
			errorCallback, options, $http) {
		var saveUrl = rootConfig.serviceBaseUrl + "lifeEngageService/save";
		var requestInfo = Request();
		requestInfo.Request.RequestPayload.Transactions
				.push(transactionObj)

		var request = {
			method : 'POST',
			url : saveUrl,
			headers : {
				'Content-Type' : "application/json; charset=utf-8",
				"Token" : options.headers.Token
			},
			data : requestInfo
		}

		$http(request)
				.success(
						function(data, status, headers, config) {
							if (data.Response.ResponsePayload.Transactions[0].StatusData.StatusCode == "100") {
								successCallback(data.Response.ResponsePayload.Transactions[0]);
							} else {
								errorCallback(data.Response.ResponsePayload.Transactions[0].StatusData.StatusMessage);
							}
						}).error(function(data, status, headers, config) {
					errorCallback(data, status, resources.errorMessage);
				});
	},
	
	DbOperationsUtility['retrieveReportingAgentsDetails'] = function(transactionObj, successCallback, errorCallback,
			options, $http) {
		try {
			var getUrl = rootConfig.serviceBaseUrl
					+ "lifeEngageService/retrieveAll";
			var requestInfo = Request();

			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObj)
			callAngularPost(getUrl, requestInfo, options, successCallback,
					errorCallback, $http);

		} catch (exception) {
			alert("error in  getListings: " + angular.toJson(exception)
					+ " :error in  getListings");
			errorCallback(resources.errorMessage);
		}
	},
	
	DbOperationsUtility['retrieveMappedBranchesDetails'] = function(transactionObj, successCallback, errorCallback,
			options, $http) {
		try {
			var getUrl = rootConfig.serviceBaseUrl
					+ "lifeEngageService/retrieveAll";
			var requestInfo = Request();

			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObj)
			callAngularPost(getUrl, requestInfo, options, successCallback,
					errorCallback, $http);

		} catch (exception) {
			alert("error in  getListings: " + angular.toJson(exception)
					+ " :error in  getListings");
			errorCallback(resources.errorMessage);
		}
	},

		DbOperationsUtility['AgentCodeValidation'] =function(transactionObj, successCallback, errorCallback,options, $http) {
		/*transactionObj.Key16 = "SUCCESS";
		successCallback(transactionObj);*/
		try {
				var getUrl = rootConfig.serviceBaseUrl + "generaliService/validateAgentCode";
				var requestInfo = Request();
				requestInfo.Request.RequestPayload.Transactions.push(transactionObj);
				var request = {
				method : 'POST',
				url : getUrl,
				    headers : {
					  'Content-Type' : "application/json; charset=utf-8",
					  "Token" : options.headers.Token
				    },
				   data : requestInfo
			    }

			$http(request).success(function(data, status, headers, config) {
				if (data.Response.ResponsePayload.Transactions)
					successCallback(data.Response.ResponsePayload.Transactions[0]);
				else
					successCallback(data.Response.ResponsePayload);
			}).error(function(data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
			
			
		} catch (exception) {
			errorCallback(exception);
		}

	},
	DbOperationsUtility['callToUpdateBPMUpload'] = function(keyForBPM, isSubmissionPending, successCallback, errorCallback,
				options, $http) {
		var getUrl = "";
		if(isSubmissionPending){
			getUrl = rootConfig.serviceBaseUrl + "generaliService/resubmit/"+keyForBPM;
		} else{
			getUrl = rootConfig.serviceBaseUrl + "bpmService/submit/"+keyForBPM;
		}
			var request = {
					method : 'GET',
					url : getUrl,
					headers : {
						'Content-Type' : "application/json; charset=utf-8",
						"Token" : options.headers.Token,
						'SOURCE': 100
					}
				}

				$http(request).success(function(data, status, headers, config) {
						successCallback();
				}).error(function(data, status, headers, config) {
					errorCallback();
				});
		};
		
		DbOperationsUtility['generateBPMEmailPDF'] = function(transactionObj, successCallback, errorCallback,
				options, $http) {
			var self = this;
			var generateBPMPdfUrl = rootConfig.serviceBaseUrl + "generaliService/generateBPMEmailPdf";

			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObj);

			var request = {
				method : 'POST',
				url : generateBPMPdfUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request)
					.success(
							function(data, status, headers, config) {
								successCallback(status);
							}).error(
							function(data, status, headers, config) {
								errorCallback(data, status, resources.errorMessage);
							});
		};
		
		 DbOperationsUtility['getTransactions'] = function(transactionObj, successCallback,
		      		errorCallback) {
		      	var getUrl = rootConfig.registrationURL;
		      	var requestInfo = Request();
		      	switch (transactionObj.Type) {
		      	case "UserDetails":
		      		getUrl = getUrl + "userService/register";
		      		break;
				case "ForgetPassword":
		        	getUrl = getUrl + "userService/forgetPassword";
		        	break;
				case "CreatePassword":
		        	getUrl = getUrl + "userService/createPassword";
		        	requestInfo = getRequest();
		        	break;
				case "ResetPassword":
			        	getUrl = getUrl + "userService/resetPassword";
			        	break;
				default:break;
		      	}
		      	requestInfo.Request.RequestPayload.Transactions.push(transactionObj);
		      	jQuery.support.cors = true;
		      	$.ajax({
		      				type : "POST",
		      				crossDomain : true,
		      				url : getUrl,
		      				headers: {
		      					"SOURCE": 100
		      			    },
		      				data : angular.toJson(requestInfo),
		      				contentType : "application/json; charset=utf-8",
		      				dataType : "json",
		      				success : function(data, status) {
		      					try{
		      						if ((data.STATUS)&& (data.STATUS == 'E')) {

		      						    errorCallback("Error in ajax call"+data.STATUS);
		      						}
		      						else
		      						{
		      						    if (data.Response.ResponsePayload.Transactions !== null)
		      						    {
											successCallback(data.Response.ResponsePayload.Transactions);
		      						    }
		      						    else{
											successCallback(data.Response.ResponsePayload);
										}
		      						}
		      					}
		      				catch(error){
		      				var data =
		      				errorCallback("Error in ajax call"+error);
		      				}
		      				},
		      				error : function(data, status) {
		      						errorCallback(data.status);
		      				}
		      			});
		      };
		      
	   DbOperationsUtility['retrieveSalesActivityDetails'] = function(transactionObj, successCallback, errorCallback,
						options,$http) {
					try {
						var getUrl = rootConfig.serviceBaseUrl
								+ "generaliSalesActivityService/retrieveSalesActivityDetails";
						var requestInfo = Request();

						requestInfo.Request.RequestPayload.Transactions
								.push(transactionObj)
						callAngularPost(getUrl, requestInfo, options, successCallback,
								errorCallback, $http);

					} catch (exception) {
						alert("error in  retrieveSalesActivityDetails: " + angular.toJson(exception)
								+ " :error in  retrieveSalesActivityDetails");
						errorCallback(resources.errorMessage);
					}
				};
		
		DbOperationsUtility['updateTransactionAfterBPMEmailPDF'] = function(transactionData, 
				transactionId, successCallback, errorCallback) {
			try {
				var self = this;
				sql = _dbOperations._buildUpdateScript("Transactions",
						transactionData);
				sql += " WHERE 	Id in (" + transactionId + ");";
				_dbOperations._executeSql(sql, function(results) {
						sql = " DELETE FROM new_elem WHERE Id in ("
							+ transactionId + ");";
					_dbOperations._executeSql(sql, function(results) {
						successCallback(results);
					}, function(error) {
						errorCallback(error);
					});
				}, function(error) {
					errorCallback(error);
				});
			} catch (error) {
				errorCallback(error);
			}

		};
	
	DbOperationsUtility['getRelatedIllustrationForLMS'] = function(lmsId,
			successCallback, errorCallback) {
		try {
			var self = this;
			sql = _dbOperations._buildFetchScript("*", "Transactions", {})
					+ " WHERE (Type = 'FNA' AND Key1 = '" + lmsId + "')";
			//console.log(sql);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				$.each(data, function(i, obj) {
					var formattedData = JSON
							.parse(unescape(obj.TransactionData));
					obj.TransactionData = formattedData;
				});
				successCallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}

	};

	DbOperationsUtility['getRelatedFNAForLMS'] = function(lmsId,
			successCallback, errorCallback) {
		try {
			var self = this;
			sql = _dbOperations._buildFetchScript("*", "Transactions", {})
					+ " WHERE (Type = 'FNA' AND Key1 = '" + lmsId + "')";
			//console.log(sql);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				$.each(data, function(i, obj) {
					var formattedData = JSON
							.parse(unescape(obj.TransactionData));
					obj.TransactionData = formattedData;
				});
				successCallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	};
	
	DbOperationsUtility['getRelatedEappForBI'] = function(transTrackingId,
			successCallback, errorCallback) {
		try {
			var self = this;
			sql = _dbOperations._buildFetchScript("*", "Transactions", {})
					+ " WHERE (Type = 'eApp' AND Key3 = '" + transTrackingId + "' AND Key15  <> 'Cancelled')";
			//console.log(sql);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				$.each(data, function(i, obj) {
					var formattedData = JSON
							.parse(unescape(obj.TransactionData));
					obj.TransactionData = formattedData;
				});
				successCallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	};
	
	DbOperationsUtility['getRelatedBIForEapp'] = function(transTrackingId,
			successCallback, errorCallback) {
		try {
			var self = this;
			sql = _dbOperations._buildFetchScript("*", "Transactions", {})
					+ " WHERE (Type = 'illustration' AND TransTrackingID = '" + transTrackingId + "' AND Key15  <> 'Cancelled')";
			//console.log(sql);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				$.each(data, function(i, obj) {
					var formattedData = JSON
							.parse(unescape(obj.TransactionData));
					obj.TransactionData = formattedData;
				});
				successCallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	};

	DbOperationsUtility['downloadPDF'] = function(transactionObj, templateId,
			successCallback, errorCallback, options, $http) {
		try {
			var type = transactionObj.Type;
			var proposalNumber = "";
			if (type == "eApp") {
				proposalNumber = transactionObj.Key4;
			}else if(type == "Memo"){
				proposalNumber = transactionObj.Key21;
			}else {
				proposalNumber = transactionObj.Key3;
			}
			var cacheParamValue = (new Date()).getTime();
			var downloadPDFUrl = rootConfig.serviceBaseUrl
					+ "documentService/getpdf/" + proposalNumber + "/"
					+ type + "/" + templateId + "?cache=" + cacheParamValue;

			if (navigator.userAgent.toLowerCase().indexOf("safari") > 0) {
				window.open(downloadPDFUrl);
			} else {
				$("body").append(
					"<iframe src='" + downloadPDFUrl
							+ "' style='display: none;' ></iframe>");
			}
			successCallback();

		} catch (exception) {
			errorCallback(exception);
		}
	};

	DbOperationsUtility['printPDF'] = function(transactionObj, templateId,
			successCallback, errorCallback, options, $http) {
		transactionObj.TransactionData.Product.templates.illustrationPdfTemplate = templateId;
		var downloadPdfUrl = rootConfig.serviceBaseUrl
				+ "documentService/getbase64stringpdf";

		var requestInfo = Request();
		requestInfo.Request.RequestPayload.Transactions.push(transactionObj);

		var request = {
			method : 'POST',
			url : downloadPdfUrl,
			headers : {
				'Content-Type' : "application/json; charset=utf-8",
				"Token" : options.headers.Token
			},
			data : requestInfo
		}

		$http(request)
				.success(
						function(data, status, headers, config) {
							if (navigator.userAgent.indexOf("iPad") > 0) {
								var base64 = data.Response.ResponsePayload.Transactions[0].base64pdf;
								var base64pdf = "data:application/pdf;base64,"
										+ base64;
								cordova.exec(function() {
									//alert('file opened successfully');
									//console.log('file opened successfully');
								}, function(args) {
									//console.log('Error');
									//console.log(angular.toJson(args));
								}, 'FileOpener2', 'open', [ base64pdf,
										'application/pdf' ]);

							} else {

								var base64pdf = data.Response.ResponsePayload.Transactions[0].base64pdf;
								cordova.exec(function() {
								}, function() {
								}, "PdfViewer", "printPdf", [ base64pdf,
										transactionObj.Key3,
										rootConfig.downloadFolder ]);

							}
							transactionObj.TransactionData.IllustrationOutput.base64PdfString = base64pdf;
							successCallback(transactionObj);
						}).error(
						function(data, status, headers, config) {
							//console.log('error' + JSON.stringify(data)+ 'status' + status);
									
							errorCallback(data, status);
						});
	};

	DbOperationsUtility['isDuplicateLead'] = function(transactionData,
			successCallback, errorCallback) {
		try {
			var dataObj = {};
			var isDuplicate = false;
			var Id = parseInt(transactionData.Id);
			dataObj.Key11 = transactionData.Key11;
			dataObj.Key4 = transactionData.Key4;
			dataObj.filter = "Key15  <> 'Cancelled'";
			var sql = _dbOperations._buildFetchScript("Id", "Transactions",
					dataObj);
			_dbOperations._executeSql(sql, function(results) {

				if (results.rows.length !== 0) {
					var data = _dbOperations._getRecordsArray(results);
					for ( var i = 0; i < data.length; i++) {
						if (Id !== data[i].Id) {
							isDuplicate = true;
						}
					}
				}
				successCallback(isDuplicate);

			}, function(error) {
				errorCallback(error);
			});

		} catch (exception) {
			errorCallback(exception);
		}
	};
	DbOperationsUtility['retrieveSPAJNumbers'] = function(transactionObj, successCallback, errorCallback, options, $http) {
		try {
				var getUrl = rootConfig.serviceBaseUrl + "generaliService/generateSpaj";
				var requestInfo = Request();
				requestInfo.Request.RequestPayload.Transactions
						.push(transactionObj);
			var request = {
				method : 'POST',
				url : getUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request).success(function(data, status, headers, config) {
				if (data.Response.ResponsePayload.Transactions)
					successCallback(data.Response.ResponsePayload.Transactions[0].TransactionData);
				else
					successCallback(data.Response.ResponsePayload);
			}).error(function(data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});

			} catch (exception) {
				alert("error in  retrieving SPAJ: " + angular.toJson(exception)
						+ " :error in  retrieving SPAJ");
				errorCallback(exception);
			}
		};
	 DbOperationsUtility['retrieveData'] =function(selectCol, tableName,additionalClause, dataObj,sucesscallback,errorCallback) {
		try {
			var sql = _dbOperations._buildFetchScript("*", tableName, dataObj);
			if(additionalClause!=''){
				sql =sql+additionalClause;
			}
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				sucesscallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	};
	DbOperationsUtility['saveSPAJDetails'] = function(SPAJNo,agentId, successCallback, errorCallback) {

		try {
			var self = this;
			var data;
			for(i = 0; i< SPAJNo.length ; i++){
				var dataObj = {};
				dataObj.agentId = agentId;
				dataObj.SPAJNo = SPAJNo[i];
				var sql = _dbOperations._buildInsertScript("GeneraliSPAJNo", dataObj);
				_dbOperations._executeSql(sql, function(results) {
					data = _dbOperations._getRecordsArray(results);
					successCallback();
				}, function(error) {
					errorCallback(error);
				});
			}
		} catch (exception) {
			errorCallback(exception);
		}

	};
	DbOperationsUtility['retrieveSPAJCount'] =  function(agentId, successCallback, errorCallback) {
		try {
			var self = this;
			var dataObj = {};
			dataObj.agentId = agentId;
			self.retrieveData("*", 'GeneraliSPAJNo','', dataObj,function(retrieveData){
				successCallback(retrieveData.length);
			},errorCallback);
			
		} catch (exception) {
			errorCallback(exception);
		}
	};
	
	DbOperationsUtility['isSpajExisting'] =function(transactionObj, successCallback, errorCallback,options, $http) {
		/*transactionObj.Key16 = "SUCCESS";
		successCallback(transactionObj);*/
		try {
				var getUrl = rootConfig.serviceBaseUrl + "generaliService/isSpajExisting";
				var requestInfo = Request();
				requestInfo.Request.RequestPayload.Transactions.push(transactionObj);
				var request = {
				method : 'POST',
				url : getUrl,
				    headers : {
					  'Content-Type' : "application/json; charset=utf-8",
					  "Token" : options.headers.Token
				    },
				   data : requestInfo
			    }

			$http(request).success(function(data, status, headers, config) {
				if (data.Response.ResponsePayload.Transactions)
					successCallback(data.Response.ResponsePayload.Transactions[0]);
				else
					successCallback(data.Response.ResponsePayload);
			}).error(function(data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
			
			
		} catch (exception) {
			errorCallback(exception);
		}

	};
	
	DbOperationsUtility['retrieveSPAJNo'] = function(agentId, successCallback, errorCallback) {
		try {
			var self = this;
			var dataObj = {};
			dataObj.agentId = agentId;
			var clause = " ORDER BY id ASC LIMIT 1";
			self.retrieveData("*", 'GeneraliSPAJNo', clause , dataObj,function(retrieveData){
				var whereClause = "id=" + retrieveData[0].id;
                self.deleteFromTable("GeneraliSPAJNo", whereClause,
                    function() {
                		successCallback(retrieveData);
                    }, errorCallback);
			},errorCallback);
		} catch (exception) {
			errorCallback(exception);
		}

	};

	//Function to call the email for memo online
		DbOperationsUtility['memoEmailCall'] = function(transactionObj,successCallback, errorCallback,options, $http) {
			var saveUrl = rootConfig.serviceBaseUrl + "emailService/updateEmailDetails";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions = transactionObj;
			var request = {
				method : 'POST',
				url : saveUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}
			$http(request).success(function(data, status, headers, config) {
				if (data.Response.ResponsePayload.Transactions[0].StatusData.StatusCode == "100") {
					successCallback(data.Response.ResponsePayload.Transactions[0]);
				} else {
					errorCallback(data.Response.ResponsePayload.Transactions[0].StatusData.StatusMessage);
				}
			}).error(function(data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
		};
		//Function to get GAO support 
		DbOperationsUtility['requestGAOSupport'] = function(transactionObj,successCallback, errorCallback,options, $http) {
			var saveUrl = rootConfig.serviceBaseUrl + "generaliService/lockUnlockCase";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions = transactionObj;
			var request = {
				method : 'POST',
				url : saveUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}
			$http(request).success(function(data, status, headers, config) {
				 if (data.Response.ResponsePayload.Transactions[0].StatusData.StatusCode == "100") {
					successCallback(data.Response.ResponsePayload.Transactions[0]);
				} else {
					errorCallback(data.Response.ResponsePayload.Transactions[0].StatusData.StatusMessage);
				}
			}).error(function(data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
		};
		//Privacy Law
		DbOperationsUtility['sendOTP'] = function (transactionObj, successCallback, errorCallback, options, $http) {
			var getUrl = rootConfig.serviceBaseUrl + "otpService/generateOTP";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions.push(transactionObj);
			var request = {
				method: 'POST',
				url: getUrl,
				headers: {
					'Content-Type': "application/json; charset=utf-8",
					"Token": options.headers.Token
				},
				data: requestInfo
			}
			$http(request).success(function (data, status, headers, config) {
				successCallback(data);
			}).error(function (data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
		};
		DbOperationsUtility['resendOtp'] = function (transactionObj, successCallback, errorCallback, options, $http) {
			var getUrl = rootConfig.serviceBaseUrl + "otpService/resendOTP";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions.push(transactionObj);
			var request = {
				method: 'POST',
				url: getUrl,
				headers: {
					'Content-Type': "application/json; charset=utf-8",
					"Token": options.headers.Token
				},
				data: requestInfo
			}
			$http(request).success(function (data, status, headers, config) {
				successCallback(data);
			}).error(function (data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
		};
		DbOperationsUtility['validateOTP'] = function (transactionObj, successCallback, errorCallback, options, $http) {
			var getUrl = rootConfig.serviceBaseUrl + "otpService/validateOTP";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions.push(transactionObj);
			var request = {
				method: 'POST',
				url: getUrl,
				headers: {
					'Content-Type': "application/json; charset=utf-8",
					"Token": options.headers.Token
				},
				data: requestInfo
			}
			$http(request).success(function (data, status, headers, config) {
				successCallback(data);
			}).error(function (data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
		};
		DbOperationsUtility['generateOTP'] = function (otpInput, successCallback, errorCallback, options, $http) {
			var getOTPGenerateUrl = rootConfig.generateOTPURL;
			var username = otpInput.username;
			var password = otpInput.password;
			var msg = otpInput.msg;
			var sender = otpInput.sender;
			var msisdn = otpInput.msisdn;
			var req = {
				method : 'POST',
				crossDomain : true,
				url : getOTPGenerateUrl,
				headers : {
					'Content-Type' : "application/x-www-form-urlencoded; charset=utf-8"
				},
				transformRequest : function(obj) {
					var str = [];
					for ( var p in obj)
						str.push(encodeURIComponent(p) + "="
								+ encodeURIComponent(obj[p]));
					return str.join("&");
				},
				data : {
					username : username,
					password : password,
					msg : msg,
					sender : sender,
					msisdn : msisdn
				}
			}
			
			$http(req).success(function(data, status, headers, config) {
				successCallback(data);
			}).error(function(data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
			
		};
		DbOperationsUtility['verifyOTP'] = function (otpVerifyInput, successCallback, errorCallback, options, $http) {
			var getOTPVerifyUrl = rootConfig.verifyOTPURL;
			var username = otpVerifyInput.username;
			var password = otpVerifyInput.password;
			var otp = otpVerifyInput.otp;
			var ref = otpVerifyInput.ref;
			var msisdn = otpVerifyInput.msisdn;
			var transid = otpVerifyInput.transid;
			var req = {
				method : 'POST',
				crossDomain : true,
				url : getOTPVerifyUrl,
				headers : {
					'Content-Type' : "application/x-www-form-urlencoded; charset=utf-8"
				},
				transformRequest : function(obj) {
					var str = [];
					for ( var p in obj)
						str.push(encodeURIComponent(p) + "="
								+ encodeURIComponent(obj[p]));
					return str.join("&");
				},
				data : {
					username : username,
					password : password,
					otp : otp,
					ref : ref,
					transid : transid,
					msisdn : msisdn
				}
			}
			
			$http(req).success(function(data, status, headers, config) {
				successCallback(data);
			}).error(function(data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
			
		};	
		
		
		//end
	var _dbOperations = {
		_db : null,
		_dbInit : function() {
			var self = this;
			self._db = window.sqlitePlugin.openDatabase("LifeEngageData",
					"1.0", "LifeEngageData", -1);
			var tblTransactionQuery = "CREATE TABLE IF NOT EXISTS Transactions (Id INTEGER PRIMARY KEY ,Key1 TEXT NOT NULL,Key2 TEXT ,Key3 TEXT ,Key4 TEXT,Key5 TEXT NOT NULL,Key6 TEXT,Key7 TEXT,Key8 TEXT,Key9 TEXT,Key10 TEXT,Key11 TEXT NOT NULL,Key12 TEXT,Key13 TEXT,Key14 TEXT,Key15 TEXT,Key16 TEXT,Key17 TEXT,Key18 TEXT,Key19 TEXT,Key20 TEXT,Key21 TEXT,Key22 TEXT,Key23 TEXT,Key24 TEXT,Key25 TEXT,Type TEXT,TransactionData TEXT,TransTrackingID INTEGER)";
			self._executeSql(tblTransactionQuery);
			var tblTransactionMetadataQuery = "PRAGMA table_info(Transactions);";
			self
					._executeSql(
							tblTransactionMetadataQuery,
							function(result) {
								var data = _dbOperations
										._getRecordsArray(result);
								var isColumnFlag = false;
								$.each(data, function(i, obj) {
									if (obj.name == "TransTrackingID") {
										isColumnFlag = true;
									}
								});
								if (!isColumnFlag) {
									var tblTransactionAlterQuery = "ALTER TABLE Transactions ADD TransTrackingID INTEGER";
									self._executeSql(tblTransactionAlterQuery);
								}
							});
			var tblAttachmentsQuery = "CREATE TABLE IF NOT EXISTS Eapp_Attachments (Id INTEGER PRIMARY KEY,ParentId TEXT,DocumentType TEXT,DocumentName TEXT,DocumentStatus TEXT,Date TEXT,DocumentDescription TEXT,DocumentObject TEXT)";
			self._executeSql(tblAttachmentsQuery);
			var tblSyncStatusQuery = "CREATE TABLE IF NOT EXISTS SyncStatus (Id INTEGER PRIMARY KEY,RecordId INTEGER,SynAttemptTS TEXT,SyncResponseStatus TEXT,SyncResponseStatusDesc TEXT,SyncResponseTS TEXT)";
			self._executeSql(tblSyncStatusQuery);
			self._createDBSync();

		},
		_getIdExitingInDB : function(tableName, idName, listIdToCheck,
				successCallback, errorCallback) {
			if (listIdToCheck.length === 0) {
				successCallback([]);
				return;
			}
			var self = this;
			var SQL = 'SELECT ' + idName + ' FROM ' + tableName + ' WHERE '
					+ idName + ' IN ("'
					+ self._arrayToString(listIdToCheck, '","') + '")';
			self._executeSql(SQL, function(ids) {
				var data = _dbOperations._getRecordsArray(ids);
				var idsInDb = [];
				for ( var i = 0; i < data.length; ++i) {
					idsInDb[data[i][idName]] = true;
				}
				successCallback(idsInDb);
			}, errorCallback);
		},
		_keyUpdation : function(table, result, recordType, successCallback,
				errorCallback) {
			var self = this;
			if (result.serverAnswer.Response.ResponseInfo.type != null
					&& result.serverAnswer.Response.ResponseInfo.type != "") {
				var i = 0;
				_dbOperations._updateRelatedKey(result, recordType, i,
						successCallback, errorCallback);
			}
		},
		_updateRelatedKey : function(result, recordType, i, successCallback,
				errorCallback) {
			var self = this;
			var resultLength = result.serverAnswer.Response.ResponsePayload.Transactions.length;
			if (result.serverAnswer.Response.ResponsePayload.Transactions[i].Type == "FNA") {
				var data = result.serverAnswer.Response.ResponsePayload.Transactions[i];
				var transactionDatas = {};
				transactionDatas.Key2 = data.Key2;
				var sql = _dbOperations._buildUpdateScript("Transactions",
						transactionDatas);
				var ID = result.serverAnswer.Response.ResponsePayload.Transactions[i].Id;
				sql += " WHERE Type='illustration' AND Key2 = " + ID + ";";
				self._executeSql(sql, function() {
					i++;
					if (i < resultLength) {
						_dbOperations._updateRelatedKey(result, recordType, i,
								successCallback, errorCallback);
					} else {
						successCallback();
					}
				}, errorCallback);

			} else if (result.serverAnswer.Response.ResponsePayload.Transactions[i].Type == "illustration") {
				var data = result.serverAnswer.Response.ResponsePayload.Transactions[i];
				var transactionDatas = {};
				transactionDatas.Key3 = data.Key3;
				var sql = _dbOperations._buildUpdateScript("Transactions",
						transactionDatas);
				var ID = result.serverAnswer.Response.ResponsePayload.Transactions[i].Id;
				sql += " WHERE Type='eApp' AND Key3 = " + ID + ";";
				self._executeSql(sql, function() {
					i++;
					if (i < resultLength) {
						_dbOperations._updateRelatedKey(result, recordType, i,
								successCallback, errorCallback);
					} else {
						successCallback();
					}
				}, errorCallback);

			} else if (result.serverAnswer.Response.ResponsePayload.Transactions[i].Type == "LMS") {
				var data = result.serverAnswer.Response.ResponsePayload.Transactions[i];
				var transactionDatas = {};
				transactionDatas.Key1 = data.Key1;
				var sql = _dbOperations._buildUpdateScript("Transactions",
						transactionDatas);
				var ID = result.serverAnswer.Response.ResponsePayload.Transactions[i].Id;
				sql += " WHERE Type='FNA' AND Key1 = " + ID + ";";
				self._executeSql(sql, function() {
					i++;
					if (i < resultLength) {
						_dbOperations._updateRelatedKey(result, recordType, i,
								successCallback, errorCallback);
					} else {
						successCallback();
					}
				}, errorCallback);

			}
		},
		_syncNow : function(type, progressIndicator, successCallback,
				errorCallback, saveBandwidth, options) {
			DBSYNC.syncNow(type, progressIndicator, function(result) {
				if (result.syncOK === true) {
					// Synchronized successfully
					//console.log(result);
					successCallback(result);
				}
			}, errorCallback, saveBandwidth, options);
		},
		_refreshNow : function(transObj, progressIndicator, successCallback,
				errorCallback, options) {
			DBSYNC.refreshNow(transObj, progressIndicator, function(response) {

				successCallback(response);
			}, errorCallback, options);
		},
		_createDBSync : function() {
			var self = this;
			var saveUrl = rootConfig.serviceBaseUrl + "lifeEngageService/save";
			TABLES_TO_SYNC = [ {
				tableName : 'Transactions',
				idName : 'Id'
			} ];
			var syncInfo;
			DBSYNC.initSync(TABLES_TO_SYNC, self._db, {
				userEmail : 'test@gmail.com',
				device_uuid : 'UNIQUE_DEVICE_ID_287CHBE873JB',
				lastSyncDate : 0
			}, saveUrl, function() {
				//console.log('db initialisation success');
			});
		},
		_closeDB : function(dbName, successCallback, errorCallback) {
			try {
				var self = this;
				if (self._db != null) {
					self._db.close(dbName);
					if (successCallback && typeof successCallback == "function") {
						successCallback();
					} else {
						self._defaultCallBack();
					}
				}
			} catch (e) {
				//console.log("ERROR: " + e.message);
				errorCallback(e);
			}
		},
		_executeSql : function(sql, successCallback, errorCallback) {
			var self = this;
			try {
				if (self._db == null) {
					self._dbInit();
				}
				self._db.transaction(function(tx) {
					tx.executeSql(sql, [], function(tx, results) {
						if (successCallback
								&& typeof successCallback == "function") {
							successCallback(results);
						} else {
							self._defaultCallBack(tx, results);
						}
					},
							function(tx, e) {
								if (errorCallback
										&& typeof errorCallback == "function") {
									errorCallback(e);
								} else {
									self._errorHandler(tx, results);
								}
							});
				});
			} catch (e) {
				//console.log("ERROR: " + e.message);
				errorCallback(e);
			}

		},
		_buildInsertScript : function(tableName, trasactionData) {
			var members = this._getAttributesList(trasactionData);
			var values = this._getMembersValue(trasactionData, members);
			if (members.length === 0) {
				throw 'buildInsertSQL : Error, try to insert an empty object in the table '
						+ tableName;
			}
			var sql = "INSERT INTO " + tableName + " (";
			sql += this._arrayToString(members, ',');
			sql += ") VALUES (";
			sql += this._arrayToString(values, ',');
			sql += ")";
			return sql;

		},
		_buildUpdateScript : function(tableName, trasactionData) {
			var self = this;
			var sql = "UPDATE " + tableName + " SET ";
			var members = self._getAttributesList(trasactionData);
			if (members.length === 0) {
				throw 'buildUpdateSQL : Error, try to insert an empty object in the table '
						+ tableName;
			}
			var values = self._getMembersValue(trasactionData, members);

			var memLength = members.length;
			for ( var i = 0; i < memLength; i++) {
				//console.log(members[i] + " = " + values[i]);
				sql += members[i] + " = " + values[i];
				if (i < memLength - 1) {
					sql += ', ';
				}
			}

			return sql;
		},
		_buildFetchScript : function(selectCol, tableName, dataObj) {
			var self = this;
			var sql = "SELECT " + selectCol + " FROM " + tableName;
			if (!jQuery.isEmptyObject(dataObj)) {
				sql += " WHERE ";
				var members = self._getAttributesList(dataObj);
				if (members.length === 0) {
					throw 'buildUpdateSQL : Error, try to insert an empty object in the table '
							+ tableName;
				}
				var values = self._getMembersValue(dataObj, members);
				var memLength = members.length;
				for ( var i = 0; i < memLength; i++) {
					if (members[i] == "filter") {
						sql += dataObj.filter;
					} else {
						sql += members[i] + " = " + values[i];
					}

					if (i < memLength - 1) {
						sql += " AND ";
					}
				}

			}
			return sql;
		},
		_buildDeleteScript : function(tableName, whereClause) {
			var self = this;
			var sql = 'DELETE FROM ' + tableName + ' WHERE ' + whereClause;
			return sql;
		},
		_getRecordsArray : function(results) {
			var resultsArray = [];
			if (results.rows.length == 0) {
				//console.log("No Results returned from DB");
			} else {
				for ( var i = 0; i < results.rows.length; i++) {
					resultsArray.push(results.rows.item(i));
				}
			}
			return resultsArray;
		},
		_getMembersValue : function(obj, members) {
			var valueArray = [];
			for ( var i = 0; i < members.length; i++) {
				if (members[i].toUpperCase() == "ID") {
					valueArray.push(obj[members[i]]);
				} else if (members[i].toUpperCase() == "DOCUMENTOBJECT") {
					valueArray
							.push("'" + JSON.stringify(obj[members[i]]) + "'");
				} else if (members[i] == "TransactionData") {
					valueArray.push("'" + escape(obj[members[i]]) + "'");
				} else {
					valueArray.push('"' + obj[members[i]] + '"');
				}
			}
			return valueArray;
		},
		_getAttributesList : function(obj) {
			var memberArray = [];
			for ( var elm in obj) {
				if (typeof this[elm] === 'function' && !obj.hasOwnProperty(elm)) {
					continue;
				}
				memberArray.push(elm);
			}
			return memberArray;
		},
		_arrayToString : function(array, separator) {
			var result = '';
			for ( var i = 0; i < array.length; i++) {
				result += array[i];
				if (i < array.length - 1) {
					result += separator;
				}
			}
			return result;
		},
		_defaultCallBack : function(transaction, results) {
		//	console.log('SQL Query executed. insertId: ' + results.insertId+ ' rows.length ' + results.rows.length);
					
		},
		_errorHandler : function(transaction, error) {
			//console.log('Error : ' + error.message + ' (Code ' + error.code+ ') Transaction.sql = ' + transaction.sql);
					
		}
	};
	
	return DbOperationsUtility;
})();
