﻿﻿﻿/*
 * Copyright 2015, LifeEngage 
 */
//Implementation for Rule Execution
var RuleHelperGLI;
(function() {
	RuleHelperGLI = {
		runCustom : function(transactionObj, successCallback, errorCallback, options, ruleInfo, container, $http) {
            
			
			var saveUrl = rootConfig.serviceBaseUrl
					+ "lifeEngageService/executeRule";
			var requestInfo = Request();
			var canonicalModelInput = {};
			var transactionObjRule = {};
			var RuleName = "";
			var FunctionName = "";
			RuleName = ruleInfo.ruleName + '.js';
			FunctionName = ruleInfo.ruleName;
			
			canonicalModelInput.ruleInput = transactionObj.TransactionData;
			canonicalModelInput.ruleName = FunctionName;
			canonicalModelInput.ruleGroup = ruleInfo.ruleGroup;
			transactionObjRule.TransactionData = canonicalModelInput;
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObjRule);
                        
			jQuery.support.cors = true;
				var request = {
				method : 'POST',
				url : saveUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Accept" : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request).success(function(data, status, headers, config) {
				if(container == "validate"){
					if(data.isSuccess == true || data.isSuccess == "true"){
						successCallback(data);	
					}
					else{
						errorCallback(data);
					}
				}
				else{
					successCallback(data);
				}
			}).error(function(data, status, headers, config) {
				errorCallback(data, status);
			});
		},
		
		runCommonFunctionRule : function(transactionObj, successCallback, errorCallback, options, fileName,functionName,value,riderName,index,fieldTobePopulated,callLoadSummary, $http) {

			
			
			var saveUrl = rootConfig.serviceBaseUrl + "lifeEngageService/executeRule";
			var requestInfo = Request();
			var canonicalModelInput = {};
			var transactionObjRule = {};
			var RuleName = "";
			var FunctionName = "";
			RuleName = fileName + '.js';
			FunctionName = functionName;
			if(typeof value =="object"){
				canonicalModelInput.ruleInput = value;
			}
			else{
				canonicalModelInput.ruleInput = {};
				canonicalModelInput.ruleInput.selectedValue = value;
			}
			canonicalModelInput.ruleName = FunctionName;
			canonicalModelInput.ruleGroup = "7";
			transactionObjRule.TransactionData = canonicalModelInput;
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObjRule);
		                
			jQuery.support.cors = true;
				var request = {
				method : 'POST',
				url : saveUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Accept" : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}
		
			$http(request).success(function(data, status, headers, config) {
				successCallback(data);
			}).error(function(data, status, headers, config) {
				errorCallback(data, status);
			});
			
		},
		//TODO:This rule copy pasted from desktop offline versionThis have to be configured for desktop
		runInsuranceEngagement :function(transactionObj,ruleInfo, successCallback, errorCallback, options, $http){
			var requestInfo = Request();
			var saveUrl = rootConfig.serviceBaseUrl+ "lifeEngageService/executeRule";
			var canonicalModelInput = {};
			var transactionObjRule = {};
			var ruleName = ruleInfo.ruleName
			+ '.js';
			var functionName = ruleInfo.ruleName;
			var databaseName = ruleInfo.offlineDB + '.db';
			
			canonicalModelInput.ruleInput = transactionObj;
			canonicalModelInput.ruleName = functionName;
			canonicalModelInput.ruleGroup = "1009";
			
			transactionObjRule.TransactionData = canonicalModelInput;
			
			requestInfo.Request.RequestPayload.Transactions
			.push(transactionObjRule);
			jQuery.support.cors = true;
			try {
				var request = {
						method : 'POST',
						url : saveUrl,
						headers : {
							'Content-Type' : "application/json; charset=utf-8",
							"Accept" : "application/json; charset=utf-8",
							"Token" : options.headers.Token
						},
						data : requestInfo
					}
				$http(request).success(function(data, status, headers, config) {
						successCallback(data);	
					}).error(function(data, status, headers, config) {
						errorCallback(status);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		}
	}
})();