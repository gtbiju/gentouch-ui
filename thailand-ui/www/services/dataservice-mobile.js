var DbOperationsUtility;
(function() {
	DbOperationsUtility = {
		openDatabase : function(dbName) {

		},
		payment : function(transactionObj, successCallback, errorCallback) {
			return (true);
		},
		lookupCall : function(transactionObj, successCallback, errorCallback,
				options) {
			try {
				var self = this;
				var transObj = JSON.parse(transactionObj.TransactionData);
				var subDivisionType = transObj.subDivisionType;
				var parentId = transObj.Id;
				var dataObj = {};
				dataObj.type = subDivisionType;
				if (parentId != null) {
					dataObj.parentId = parentId;
				}
				sql = _dbOperations._buildFetchScript("*", "COUNTRY_LOOKUP",
						dataObj);
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					//console.log(JSON.stringify(data));
					var dataList = [];
					$.each(data, function(i, obj) {
						dataList.push({
							id : obj.Identifier,
							name : obj.Name,
							Dependant : transObj.Id
						});
					});
					//console.log(JSON.stringify(dataList));
					successCallback("LookupService." + subDivisionType,
							dataList);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		getAgentDetails : function(transactionObj, successCallback,
				errorCallback) {
			try {
				var self = this;
				var agentCode = transactionObj.agentCode;
				var dataObj = {};
				dataObj.agentCode = agentCode;
				sql = _dbOperations._buildFetchScript(
						"experience,businessSourced,servicedCustomers",
						"AGENT", dataObj);
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {

					//console.log(results);
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		getAchievementDetails : function(transactionObj, successCallback,
				errorCallback) {
			try {
				var self = this;
				var agentId = transactionObj.agentId;
				var dataObj = {};
				dataObj.agentId = agentId;
				sql = _dbOperations._buildFetchScript("text", "ACHIEVEMENT",
						dataObj);
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {

					//console.log(results);
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		getCancelledProposals : function(syncStatus, successCallback,
				errorCallback) {
			try {

				var self = this;
				var dataObj = {};
				dataObj.Key16 = syncStatus;
				sql = _dbOperations._buildFetchScript("Id", "Transactions",
						dataObj);
				//console.log(sql);

				_dbOperations._executeSql(sql, function(results) {

					//console.log(results);

					var data = _dbOperations._getRecordsArray(results);

					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},

		getDetailsForListing : function(transactionObj, successCallback,
				errorCallback, options) {
			try {
				this
						.getListings(
								transactionObj,
								function(data) {
									var requiredData = [];
									for (var i = 0; i < data.length; i++) {
										if (data[i].TransactionData) {
											if (data[i].Type == "illustration")
												data[i].TransactionData.IllustrationOutput = {};
											// get only the required details to
											// be displayed in the listing
											// screen
											var dataNew = {
												"TransactionData" : {
													"Insured" : {
														"BasicDetails" : {
															"firstName" : "",
															"lastName" : "",
															"dob" : ""
														}
													},
													"Proposer" : {
														"BasicDetails" : {
															"firstName" : "",
															"lastName" : ""
														}
													},
													"Payer" : {
														"BasicDetails" : {
															"firstName" : "",
															"lastName" : ""
														}
													},
													"Product" : {
														"ProductDetails" : {
															"productName" : "",
															"productCode" : "",
															"productType" : ""
														}
													}
												}
											};
											if (data[i].Type == "eApp") {
												if (data[i].TransactionData.Proposer.BasicDetails) {
													dataNew.TransactionData.Proposer.BasicDetails.firstName = data[i].TransactionData.Proposer.BasicDetails.firstName;
													dataNew.TransactionData.Proposer.BasicDetails.lastName = data[i].TransactionData.Proposer.BasicDetails.lastName;
												}
												if (data[i].TransactionData.Insured.BasicDetails) {
													dataNew.TransactionData.Insured.BasicDetails.firstName = data[i].TransactionData.Insured.BasicDetails.firstName;
													dataNew.TransactionData.Insured.BasicDetails.lastName = data[i].TransactionData.Insured.BasicDetails.lastName;
												}
												if (data[i].TransactionData.Product.ProductDetails) {
													dataNew.TransactionData.Product.ProductDetails.productName = data[i].TransactionData.Product.ProductDetails.productName;
													dataNew.TransactionData.Product.ProductDetails.productCode = data[i].TransactionData.Product.ProductDetails.productCode;
													dataNew.TransactionData.Product.ProductDetails.productType = data[i].TransactionData.Product.ProductDetails.productType;
												}
											}
											if (data[i].Type != "FNA") {
												if (data[i].TransactionData.Product.ProductDetails) {
													dataNew.TransactionData.Product.ProductDetails.productName = data[i].TransactionData.Product.ProductDetails.productName;
													dataNew.TransactionData.Product.ProductDetails.productCode = data[i].TransactionData.Product.ProductDetails.productCode;
													dataNew.TransactionData.Product.ProductDetails.productType = data[i].TransactionData.Product.ProductDetails.productType;
												}
												if (data[i].TransactionData.Insured.BasicDetails) {
													dataNew.TransactionData.Insured.BasicDetails.firstName = data[i].TransactionData.Insured.BasicDetails.firstName;
													dataNew.TransactionData.Insured.BasicDetails.lastName = data[i].TransactionData.Insured.BasicDetails.lastName;
													dataNew.TransactionData.Insured.BasicDetails.dob = data[i].TransactionData.Insured.BasicDetails.dob;
												}
												if (data[i].TransactionData.Payer.BasicDetails) {
													dataNew.TransactionData.Payer.BasicDetails.firstName = data[i].TransactionData.Payer.BasicDetails.firstName;
													dataNew.TransactionData.Payer.BasicDetails.lastName = data[i].TransactionData.Payer.BasicDetails.lastName;
												}

											} else {
												var partiesArray = data[i].TransactionData.parties;
												var basicDetails = {
													"BasicDetails" : {
														"firstName" : "",
														"dob" : ""
													}
												};
												dataNew.TransactionData.parties = [];
												for (party in partiesArray) {
													if (partiesArray[party].type == "FNAMyself") {
														basicDetails.BasicDetails.firstName = partiesArray[party].BasicDetails.firstName
																+ " "
																+ partiesArray[party].BasicDetails.lastName;
														basicDetails.BasicDetails.dob = partiesArray[party].BasicDetails.dob;
														dataNew.TransactionData.parties
																.push(basicDetails);
													}
												}
											}

											dataNew.Key2 = data[i].Key2;
											dataNew.Key3 = data[i].Key3;
											dataNew.Key4 = data[i].Key4;
											dataNew.Key5 = data[i].Key5;
											dataNew.Key6 = data[i].Key6;
											dataNew.Key7 = data[i].Key7;
											dataNew.Key8 = data[i].Key8;
											dataNew.Key13 = data[i].Key13;
											dataNew.Key15 = data[i].Key15;
											dataNew.Key16 = data[i].Key16;
											dataNew.creationDate=data[i].creationDate;
											dataNew.modifiedDate=data[i].modifiedDate;
											dataNew.Id = data[i].Id;
											delete data[i];
											requiredData.push(dataNew);
										}
									}
									successCallback(requiredData);
								}, function(error) {
									errorCallback(error);
								});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		getParentDetails : function(parentId, successCallback, errorCallback) {

			try {
				var self = this;
				var fields = rootConfig.lmsParentDetailsKeys;
				var dataObj = {};
				var sql = _dbOperations._buildFetchScript(fields,
						"Transactions", dataObj);
				sql = sql + " where Type = 'LMS' and Key1=" + parentId;

				_dbOperations._executeSql(sql, function(results) {

					var data = _dbOperations._getRecordsArray(results);

					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}

		},
		getListings : function(transactionObj, successCallback, errorCallback,
				options) {
			try {
				var self = this;
				var dataObj = {};
				if ((transactionObj.Type == "FNA")
						&& transactionObj.Key2 != null
						&& transactionObj.Key2 != "") {
					dataObj.Key2 = transactionObj.Key2;
				} else if (transactionObj.Type == "illustration"
						&& transactionObj.Key3 != null
						&& transactionObj.Key3 != "") {
					dataObj.Key3 = transactionObj.Key3;
				} else if (transactionObj.Type == "eApp"
						&& transactionObj.Key4 != null
						&& transactionObj.Key4 != "") {
					dataObj.Key4 = transactionObj.Key4;
				} else if (transactionObj.Id != null && transactionObj.Id != "") {
					dataObj.Id = transactionObj.Id;
				}else if (transactionObj.TransTrackingID != null && transactionObj.TransTrackingID != "") {
					dataObj.TransTrackingID = transactionObj.TransTrackingID;
				}
				if (transactionObj.Type != null && transactionObj.Type != "") {
					dataObj.Type = transactionObj.Type;
				}
				if (transactionObj.filter != null
						&& transactionObj.filter != "") {
					dataObj.filter = transactionObj.filter;
				}

				var fields = '*';
				switch (transactionObj.Key10) {
				// this wil help to select custom fields
				case "Custom":
					if (transactionObj.fields != null
							&& transactionObj.fields != "") {
						fields = transactionObj.fields;
					} else {
						fields = rootConfig.transactionTableBasicFields;
					}
					break;
				// Selects all feilds except the transaction data
				case "BasicDetails":
					if (transactionObj.Type == "LMS") {
						fields = rootConfig.transactionTableBasicLMSFields;
					} else {
						fields = rootConfig.transactionTableBasicFields;
					}

					break;
				// Selects entire fields
				case "FullDetails":
					fields = " * ";
					break;

				default:
					break;
				// Do nothing
				}
				sql = _dbOperations._buildFetchScript(fields, "Transactions",
						dataObj);
				sql = sql + " ORDER BY Key14 DESC";

				_dbOperations
						._executeSql(
								sql,
								function(results) {
									var data = _dbOperations
											._getRecordsArray(results);

									if (transactionObj.Type == "LMS") {
										// To get parent details of a referral
										// (among listed referrels in lms)
										if (transactionObj.Key10 != "BasicDetails") {
											var dataObj = [];
											if (data && data.length > 0) {
												$
														.each(
																data,
																function(i, obj) {
																	if (obj.Key8 != "") {
																		self
																				.getParentDetails(
																						obj.Key8,
																						function(
																								results) {
																							var transactionTabledata = JSON
																									.parse(unescape(obj.TransactionData));

																							var parentDetails = {};
																							if( results && results.length > 0){
        																							for (var k = 0; k < rootConfig.lmsParentDetailsKeys.length; k++) {
        																								var field = rootConfig.lmsParentDetailsKeys[k];
        																								parentDetails[field] = results[0][field];
        
        																							}
																							}
        
        																					transactionTabledata.Referrals.parentLeadDetails = parentDetails;
																							obj.TransactionData = escape(JSON
																									.stringify(transactionTabledata));
																							dataObj
																									.push(obj);
																							successCallback(dataObj);

																						},
																						function(
																								error) {
																							errorCallback(error);
																						});

																	} else {
																		successCallback(data);
																	}

																});
											} else {
												successCallback(data);
											}
										} else {
											successCallback(data);
										}

									}

									else {
										$
												.each(
														data,
														function(i, obj) {
															var formattedData = JSON
																	.parse(unescape(obj.TransactionData));
															obj.TransactionData = formattedData;
														});
										successCallback(data);
									}

								}, function(error) {
									errorCallback(error);
								});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		getRelatedFNA : function(fnaId, successCallback, errorCallback) {
			try {
				var self = this;
				sql = _dbOperations._buildFetchScript("*", "Transactions", {})
						+ " WHERE Type = 'FNA' AND TransTrackingID = '" + fnaId
						+ "'";
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					$.each(data, function(i, obj) {
						var formattedData = JSON
								.parse(unescape(obj.TransactionData));
						obj.TransactionData = formattedData;
					});
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		getFilteredListing : function(transactionObj, successCallback,
				errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				dataObj.filter = "Key11 = '" + transactionObj.Key11 + "'";
				var searchvalue = transactionObj.TransactionData.searchCriteriaRequest.value;
				if (transactionObj.TransactionData.searchCriteriaRequest.command === 'ListingDashBoard') {
					if (transactionObj.TransactionData.searchCriteriaRequest.modes !== undefined
							&& transactionObj.TransactionData.searchCriteriaRequest.modes.length > 0) {
						if (searchvalue != undefined
								&& searchvalue !== ""
								&& transactionObj.TransactionData.searchCriteriaRequest.modes[0] === 'LMS') {
							dataObj.filter += " AND ((UPPER(Key2) LIKE '%"
									+ searchvalue + "%' or UPPER(Key2)=UPPER('"
									+ searchvalue
									+ "')) or (UPPER(Key3) LIKE '%"
									+ searchvalue + "%' or UPPER(Key3)=UPPER('"
									+ searchvalue
									+ "')) or (UPPER(Key4) LIKE '%"
									+ searchvalue + "%' or UPPER(Key4)=UPPER('"
									+ searchvalue + "')))";
						} else if (searchvalue != undefined
								&& searchvalue !== ""
								&& transactionObj.TransactionData.searchCriteriaRequest.modes[0] === 'FNA') {
							dataObj.filter += " AND ((UPPER(Key6) LIKE '%"
									+ searchvalue + "%' or UPPER(Key6)=UPPER('"
									+ searchvalue
									+ "')) or (UPPER(Key8) LIKE '%"
									+ searchvalue + "%' or UPPER(Key8)=UPPER('"
									+ searchvalue + "')))";
						} else if (searchvalue != undefined
								&& searchvalue !== ""
								&& transactionObj.TransactionData.searchCriteriaRequest.modes[0]
										.toLowerCase() === 'illustration') {
							dataObj.filter += " AND ((UPPER(Key6) LIKE '%"
									+ searchvalue + "%' or UPPER(Key6)=UPPER('"
									+ searchvalue
									+ "')) or (UPPER(Key8) LIKE '%"
									+ searchvalue + "%' or UPPER(Key8)=UPPER('"
									+ searchvalue + "')))";
						} else if (searchvalue != undefined
								&& searchvalue !== ""
								&& transactionObj.TransactionData.searchCriteriaRequest.modes[0]
										.toLowerCase() === 'eapp') {
							dataObj.filter += " AND ((UPPER(Key6) LIKE '%"
									+ searchvalue + "%' or UPPER(Key6)=UPPER('"
									+ searchvalue
									+ "')) or (UPPER(Key8) LIKE '%"
									+ searchvalue + "%' or UPPER(Key8)=UPPER('"
									+ searchvalue + "')))"
						}
					}
				}

				if ((transactionObj.Type == "FNA")
						&& transactionObj.Key2 != null
						&& transactionObj.Key2 != "") {
					dataObj.Key2 = transactionObj.Key2;
				} else if (transactionObj.Type.toLowerCase() == "illustration"
						&& transactionObj.Key3 != null
						&& transactionObj.Key3 != "") {
					dataObj.Key3 = transactionObj.Key3;
				} else if (transactionObj.Type.toLowerCase() == "eapp"
						&& transactionObj.Key4 != null
						&& transactionObj.Key4 != "") {
					dataObj.Key4 = transactionObj.Key4;
				} else if (transactionObj.Id != null && transactionObj.Id != "") {
					dataObj.Id = transactionObj.Id;
				}
				if (transactionObj.Type != null && transactionObj.Type != "") {
					dataObj.Type = transactionObj.Type;
				}
				if (transactionObj.filter != null
						&& transactionObj.filter != "") {
					if (dataObj.filter !== undefined) {
						dataObj.filter += " AND " + transactionObj.filter;
					} else {
						dataObj.filter = transactionObj.filter;
					}

				}

				var fields = '';
				switch (transactionObj.Key10) {
				// this wil help to select custom fields
				case "Custom":
					if (transactionObj.fields != null
							&& transactionObj.fields != "") {
						fields = transactionObj.fields;
					} else {
						if (transactionObj.Type == "LMS") {
							fields = rootConfig.transactionTableBasicLMSFields;
						} else {
							fields = rootConfig.transactionTableBasicFields;
						}
					}

					fields = fields
							+ ",(select count(*) from Transactions T1 where T1.Key1=T.TransTrackingID and Type ='eApp' and Key15 <> 'Cancelled') as eAppCount ,(select count(*) from Transactions T1 where T1.Key1=T.TransTrackingID and Type ='illustration' and Key15 <> 'Cancelled') as illustrationCount,(select count(*) from Transactions T1 where T1.Key1=T.TransTrackingID and Type ='FNA' and Key15 <> 'Cancelled') as fnaCount";

					break;
				// Selects all feilds except the transaction data
				case "BasicDetails":
					if (transactionObj.Type == "LMS") {
						fields = rootConfig.transactionTableBasicLMSFields;
					} else {
						fields = rootConfig.transactionTableBasicFields;
					}
					break;
				// Selects entire fields
				case "FullDetails":
					fields = " * ";
					break;
				default:
					break;
				// Do nothing
				}

				sql = _dbOperations._buildFetchScript(fields, "Transactions T",
						dataObj);
				sql = sql + " ORDER BY modifiedDate DESC";
				//console.log('sql ' + sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);

				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		retrieveByFilterCount : function(transactionObj, successCallback,
				errorCallback) {
			try {
				var self = this;
				var searchData = [];
				var sql = "";
				var searchvalue = transactionObj.TransactionData.searchCriteriaRequest.value;
				if (transactionObj.TransactionData.searchCriteriaRequest.command === 'LandingDashBoardCount') {
					if (transactionObj.TransactionData.searchCriteriaRequest.modes !== undefined
							&& transactionObj.TransactionData.searchCriteriaRequest.modes.length > 0) {

						for (var i = 0; i < transactionObj.TransactionData.searchCriteriaRequest.modes.length; i++) {
							if (transactionObj.TransactionData.searchCriteriaRequest.modes[i] === 'LMS') {
								if (sql === "") {
									sql = "select count(*) as Count,Key15 as Status,Type from Transactions where Type='LMS' and Key15 <> 'Cancelled'";
									if (searchvalue != "") {
										sql += " and Key11 = '"
												+ transactionObj.Key11
												+ "' and  ((UPPER(Key2) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key2)=UPPER('"
												+ searchvalue
												+ "')) or (UPPER(Key3) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key3)=UPPER('"
												+ searchvalue
												+ "')) or (UPPER(Key4) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key4)=UPPER('"
												+ searchvalue + "'))) ";
									}
									sql += " and Key11 = '"
											+ transactionObj.Key11
											+ "'  group by Key15,Type ";
								} else {
									sql += " UNION ALL select count(*) as Count,Key15 as Status,Type from Transactions where Type='LMS' and Key15 <> 'Cancelled'";
									if (searchvalue != "") {
										sql += " and Key11 = '"
												+ transactionObj.Key11
												+ "' and  ((UPPER(Key2) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key2)=UPPER('"
												+ searchvalue
												+ "')) or (UPPER(Key3) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key3)=UPPER('"
												+ searchvalue
												+ "')) or (UPPER(Key4) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key4)=UPPER('"
												+ searchvalue + "'))) ";
									}
									sql += " and Key11 = '"
											+ transactionObj.Key11
											+ "'  group by Key15,Type ";
								}

							} else if (transactionObj.TransactionData.searchCriteriaRequest.modes[i] === 'FNA') {
								if (sql === "") {
									sql = "select count(*) as Count,Key15 as Status,Type from Transactions where Type='FNA'";
									if (searchvalue != "") {
										sql += " and Key11 = '"
												+ transactionObj.Key11
												+ "'  and ((UPPER(Key6) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key6)=UPPER('"
												+ searchvalue
												+ "')) or (UPPER(Key8) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key8)=UPPER('"
												+ searchvalue + "')))";
									}
									sql += " and Key11 = '"
											+ transactionObj.Key11
											+ "'  group by Key15,Type";
								} else {
									sql += " UNION ALL select count(*) as Count,Key15 as Status,Type from Transactions where Type='FNA'";
									if (searchvalue != "") {
										sql += " and Key11 = '"
												+ transactionObj.Key11
												+ "'  and ((UPPER(Key6) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key6)=UPPER('"
												+ searchvalue
												+ "')) or (UPPER(Key8) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key8)=UPPER('"
												+ searchvalue + "')))";
									}
									sql += " and Key11 = '"
											+ transactionObj.Key11
											+ "'  group by Key15,Type";
								}
							} else if (transactionObj.TransactionData.searchCriteriaRequest.modes[i]
									.toLowerCase() === 'illustration') {
								if (sql === "") {
									sql = "select count(*) as Count,Key15 as Status,Type from Transactions where Type='illustration' and Key15 <> 'Cancelled'";
									if (searchvalue != "") {
										sql += " and Key11 = '"
												+ transactionObj.Key11
												+ "'  and ((UPPER(Key6) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key6)=UPPER('"
												+ searchvalue
												+ "')) or (UPPER(Key8) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key8)=UPPER('"
												+ searchvalue + "')))";
									}
									sql += " and Key11 = '"
											+ transactionObj.Key11
											+ "'  group by Key15,Type";
								} else {
									sql += " UNION ALL select count(*) as Count,Key15 as Status,Type from Transactions where Type='illustration' and Key15 <> 'Cancelled'";
									if (searchvalue != "") {
										sql += " and Key11 = '"
												+ transactionObj.Key11
												+ "'  and ((UPPER(Key6) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key6)=UPPER('"
												+ searchvalue
												+ "')) or (UPPER(Key8) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key8)=UPPER('"
												+ searchvalue + "')))";
									}
									sql += " and Key11 = '"
											+ transactionObj.Key11
											+ "'  group by Key15,Type";
								}
							} else if (transactionObj.TransactionData.searchCriteriaRequest.modes[i]
									.toLowerCase() === 'eapp') {
								if (sql === "") {
									sql = "select count(*) as Count,Key15 as Status,Type from Transactions where Type='eApp' and Key15 <> 'Cancelled' and Key7 = ''";
									if (searchvalue != "") {
										sql += " and Key11 = '"
												+ transactionObj.Key11
												+ "'  and  ((UPPER(Key6) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key6)=UPPER('"
												+ searchvalue
												+ "')) or (UPPER(Key8) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key8)=UPPER('"
												+ searchvalue + "')))";
									}
									sql += " and Key11 = '"
											+ transactionObj.Key11
											+ "'  group by Key15,Type";
								} else {
									sql += " UNION ALL select count(*) as Count,Key15 as Status,Type from Transactions where Type='eApp' and Key15 <> 'Cancelled' and Key7 = ''";
									if (searchvalue != "") {
										sql += " and Key11 = '"
												+ transactionObj.Key11
												+ "'  and  ((UPPER(Key6) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key6)=UPPER('"
												+ searchvalue
												+ "')) or (UPPER(Key8) LIKE '%"
												+ searchvalue
												+ "%' or UPPER(Key8)=UPPER('"
												+ searchvalue + "')))";
									}
									sql += " and Key11 = '"
											+ transactionObj.Key11
											+ "'  group by Key15,Type";
								}
							}
						}
					}
					_dbOperations
							._executeSql(
									sql,
									function(results) {

										var data = _dbOperations
												._getRecordsArray(results);
										var eAppmodeCount = 0, lmsmodeCount = 0, fnamodeCount = 0, illustrationmodeCount = 0, flageApp = true, flagLMS = true, flagFNA = true, flagillustration = true, eAppcount = 0, LMScount = 0, FNAcount = 0, illustrationcount = 0;
										var eAppIndex, lmsIndex, fnaIndex, illustrationIndex, commonIndex = 0;

										for (var y = 0; y < data.length; y++) {

											if (data[y].Type.toLowerCase() === "eapp") {
												if (flageApp) {
													eAppIndex = commonIndex;
												}
												if (eAppcount == 0 && flageApp) {
													searchData
															.push({
																"mode" : data[y].Type,
																"count" : eAppmodeCount,
																"subModes" : []
															});
													eAppcount++;
													commonIndex++;
													flageApp = false;
												}
												if (!flageApp) {
													searchData[eAppIndex].subModes
															.push({
																"mode" : data[y].Status,
																"count" : data[y].Count
															});
													searchData[eAppIndex].count += data[y].Count;
												}
											} else if (data[y].Type === "LMS") {
												if (flagLMS) {
													lmsIndex = commonIndex;
												}
												if (LMScount == 0 && flagLMS) {
													searchData.push({
														"mode" : data[y].Type,
														"count" : lmsmodeCount,
														"subModes" : []
													});
													LMScount++;
													commonIndex++;
													flagLMS = false;
												}
												if (!flagLMS) {

													searchData[lmsIndex].subModes
															.push({
																"mode" : data[y].Status,
																"count" : data[y].Count
															});
													searchData[lmsIndex].count += data[y].Count;
												}
											} else if (data[y].Type === "FNA") {
												if (flagFNA) {
													fnaIndex = commonIndex;
												}
												if (FNAcount == 0 && flagFNA) {
													searchData.push({
														"mode" : data[y].Type,
														"count" : fnamodeCount,
														"subModes" : []
													});
													FNAcount++;
													commonIndex++;
													flagFNA = false;
												}
												if (!flagFNA) {

													searchData[fnaIndex].subModes
															.push({
																"mode" : data[y].Status,
																"count" : data[y].Count
															});
													searchData[fnaIndex].count += data[y].Count;
												}
											} else if (data[y].Type
													.toLowerCase() === "illustration") {
												if (flagillustration) {
													illustrationIndex = commonIndex;
												}
												if (illustrationcount == 0
														&& flagillustration) {
													searchData
															.push({
																"mode" : data[y].Type,
																"count" : illustrationmodeCount,
																"subModes" : []
															});
													illustrationcount++;
													commonIndex++;
													flagillustration = false;
												}
												if (!flagillustration) {
													searchData[illustrationIndex].subModes
															.push({
																"mode" : data[y].Status,
																"count" : data[y].Count
															});
													searchData[illustrationIndex].count += data[y].Count;
												}
											}

										}
										successCallback(searchData);
									}, function(error) {
										errorCallback(error);
									});
				}
			} catch (exception) {
				errorCallback(exception);
			}
		},
		deleteRequirementFiles : function(tableName,transtrackingId,fileName,successCallback,errorCallback) {
			try {
				//var whereClause = "fileName='"+fileName+ "' and identifier='"+transtrackingId+"'";
				
				var whereClause =  " identifier IN ("
				+ "'"+transtrackingId+"'"
				+ ") and(requirementName LIKE"
				+ "'%"+fileName+"%'"
				+ ") ";
				sql = _dbOperations._buildDeleteScript(tableName, whereClause);
				_dbOperations._executeSql(sql, function(data) {
						successCallback(data);
					}, function(error) {
						errorCallback(error);
					});
			} catch (error) {
				errorCallback(error);
			}
		},
		getPages : function(docName, successCallback, errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				dataObj.documentName = docName;
				sql = _dbOperations._buildFetchScript("DocumentObject",
						"Eapp_Attachments", dataObj);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		getListingDetail : function(transactionObj, successCallback,
				errorCallback, options) {
			return this.getListings(transactionObj, successCallback,
					errorCallback, options);
		},
		updateTransWithUniqueID : function(transactionObj, successCallback, errorCallback){
			var uniqueKey='';
			var isIdentifierNull = true;
			if (transactionObj.Type=='LMS' && transactionObj.Key1==''){
				uniqueKey= "Key1";
			} else if (transactionObj.Type=='FNA' && transactionObj.Key2==''){
				uniqueKey= "Key2";
			}else if (transactionObj.Type=='illustration' && transactionObj.Key3==''){
				uniqueKey= "Key3";
			}else if (transactionObj.Type=='eApp' && transactionObj.Key4==''){
				uniqueKey= "Key4";
			}else{
				isIdentifierNull= false;
			}
			if (isIdentifierNull){
				sql="select "+ uniqueKey +  " from Transactions where Id='" + transactionObj.Id + "' and Key16='Synced'";
				_dbOperations._executeSql(sql, function(results) {
					if(results && results.rows.length>0){
						var data = _dbOperations._getRecordsArray(results);
						transactionObj[uniqueKey]=data[0][uniqueKey];
					}
					successCallback(transactionObj);
				}, function(){
					errorCallback(error);
				});
			}else{
				successCallback(transactionObj);
			}
		},
		saveTransactions : function(transactionData, successCallback,
				errorCallback, options) {	
			try {
				var self = this;
				if (transactionData.Id != null && transactionData.Id != 0) {
					DbOperationsUtility.updateTransWithUniqueID(transactionData, function(transactionData){
						sql = _dbOperations._buildUpdateScript("Transactions",
								transactionData);
						sql += " WHERE 	Id = " + transactionData["Id"];
						_dbOperations._executeSql(sql, function(data) {
							var transId =  0;
							if (transactionData.Id != null && transactionData.Id != 0) {
								transId =transactionData.Id;
							}else{
								transId =(data.insertId == null) ? transactionData.Id
										: data.insertId;
							}
							//var transId = (data.insertId == null) ? transactionData.Id
							//: data.insertId;
							self.insertStatusRec(transId, successCallback,
									errorCallback);
						}, function(error) {
							errorCallback(error);
						});
					}, function() {
						errorCallback(error);
					});
				} else {
					sql = _dbOperations._buildInsertScript("Transactions",
							transactionData);
					_dbOperations._executeSql(sql, function(data) {
						var transId =  0;
						if (transactionData.Id != null && transactionData.Id != 0) {
							transId =transactionData.Id;
						}else{
							transId =(data.insertId == null) ? transactionData.Id
									: data.insertId;
						}
						//var transId = (data.insertId == null) ? transactionData.Id
						//: data.insertId;
						self.insertStatusRec(transId, successCallback,
								errorCallback);
					}, function(error) {
						errorCallback(error);
					});
				}
				//console.log(sql + "query");

			} catch (exception) {
				errorCallback(exception);
			}
		},
		insertStatusRec : function(transId, successCallback, errorCallback) {
			try {
				var self = this;
				var statusData = {
					RecordId : transId,
					SyncResponseStatus : "Saved"
				};
				_dbOperations._getIdExitingInDB("SyncStatus", "RecordId",
						[ transId ], function(idInStatusTbl) {
							if (idInStatusTbl[transId]) {
								sql = _dbOperations._buildUpdateScript(
										"SyncStatus", statusData);
								sql += " WHERE 	RecordId = " + transId;
							} else {
								sql = _dbOperations._buildInsertScript(
										"SyncStatus", statusData);
							}
							_dbOperations._executeSql(sql, function(data) {
								successCallback(transId);
							}, function(error) {
								errorCallback(error);
							});
						}, errorCallback);
			} catch (exception) {
				errorCallback(exception);
			}
		},
		runSTPRule : function(transactionData, successCallback, errorCallback) {
			try {
				// Call rule engine and execute rule
				var RuleName = "stpRules.js";
				var FunctionName = "runSTPRules";

				var inputJson = '{ "data": {"OfflineDbName": "LifeEngageData.db", "RuleSetId": "1","RuleSetName": "'
						+ RuleName
						+ '", "FunctionName": "'
						+ FunctionName
						+ '","variables": '
						+ JSON.stringify(transactionData)
						+ '} }';

				LERuleEngine.execute(inputJson, function(output) {
					//console.log("RuleOutput : " + output);
					successCallback($.parseJSON(output));
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		saveFile : function(documentData, successCallback, errorCallback) {
			try {
				var self = this;
				var document = jQuery.extend(true, {}, documentData);
				if ("documentDisplayName" in document) {
					delete document.documentDisplayName;
				}
				if (documentData.id != null && documentData.id != 0) {
					sql = _dbOperations._buildUpdateScript("Eapp_Attachments",
							document);
					sql += " WHERE id = " + document["id"];
				} else {
					sql = _dbOperations._buildInsertScript("Eapp_Attachments",
							document);
				}
				//console.log(sql + "query");
				_dbOperations._executeSql(sql, function(results) {
					successCallback(documentData, results);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		saveFilesLocally : function(documentData, successCallback,
				errorCallback) {
			try {
				var self = this;
				var docLength = documentData.length;
				var index = 0;

				function saveEachFile(index, documentData, successCallback,
						errorCallback) {
					self.saveFile(documentData[index], function() {
						var newindex = index + 1;
						if (newindex < docLength) {
							saveEachFile(newindex, documentData,
									successCallback, errorCallback);
						} else {
							successCallback();
						}
					}, errorCallback);
				}

				saveEachFile(0, documentData, successCallback, errorCallback);
			} catch (exception) {
				errorCallback(exception);
			}
		},
		runRequirementRule : function(transactionData, successCallback, errorCallback) {
            try {
                // Call rule engine and execute rule
            	var RuleName = rootConfig.requirementRuleName + '.js';
                var FunctionName = rootConfig.requirementFunctionName;

                var inputJson = '{ "data": {"OfflineDbName": "core-products.db", "RuleSetId": "1","RuleSetName": "'
                        + RuleName
                        + '", "FunctionName": "'
                        + FunctionName
                        + '","variables": '
                        + JSON.stringify(transactionData)
                        + '} }';

                LERuleEngine.execute(inputJson, function(output) {
                   // console.log("RuleOutput : " + output);
                    successCallback($.parseJSON(output));
                });
            } catch (exception) {
                errorCallback(exception);
            }
        },

        saveRequirementFiles : function(requirementData, successCallback, errorCallback) {
            try {
                var self = this;
                
                var document = jQuery.extend(true, {}, requirementData);
                if ("documentDisplayName" in document) {
                    delete document.documentDisplayName;
                }
                
                sql = _dbOperations._buildInsertOrUpdateScript("RequirementFiles", "fileName",
                    document);
                                    
               // console.log(sql + "query");
                _dbOperations._executeSql(sql, function(results) {
                    successCallback(requirementData, results);
                }, function(error) {
                    errorCallback(error);
                });
            } catch (exception) {
                errorCallback(exception);
            }
        },
        getSignature : function(transtrackingId,signature,successCallback,
                errorCallback) {
            try {
                var self = this;
                var dataObj = {};
            sql = _dbOperations._buildFetchScript(
                        rootConfig.requirementTableFetchFields, "RequirementFiles", {})
                        + " WHERE identifier IN ("
                        + "'"+transtrackingId+"'"
                        + ") and(requirementName LIKE"
                        + "'%"+signature+"%'"
                        + ") ";

                console.log(sql);
                _dbOperations._executeSql(sql, function(results) {
                    var data = _dbOperations._getRecordsArray(results);
                    successCallback(data);
                }, function(error) {
                    errorCallback(error);
                });
            } catch (exception) {
                errorCallback(exception);
            }
        },
        updateRequirementFiles : function(data,successCallback, errorCallback) {
            try {
                var self = this;
                var dataObj = {};
                sql = _dbOperations._buildUpdateScript("RequirementFiles",
                        data);
                sql += " WHERE  Id in (" + data.id + ");";
                //console.log(sql);
                _dbOperations._executeSql(sql, function(results) {
                    var data = _dbOperations._getRecordsArray(results);
                    successCallback(data);
                }, function(error) {
                    errorCallback(error);
                });
            } catch (exception) {
                errorCallback(exception);
            }
        },

        getRequirementForDelete : function(data,requirementName,transtrackingId,successCallback,
                errorCallback) {
            try {
                var self = this;
                var dataObj = {};
            sql = _dbOperations._buildFetchScript(
                        rootConfig.requirementTableFetchFields, "RequirementFiles", {})
                        + " WHERE identifier IN ("
                        + "'"+transtrackingId+"'"
                        + ") and base64string IN("+"'"+ data+"'"
                        + ") and(requirementName LIKE"
                        + "'%"+requirementName+"%'"
                        + ") ";

                //console.log(sql);
                _dbOperations._executeSql(sql, function(results) {
                    var data = _dbOperations._getRecordsArray(results);
                    successCallback(data);
                }, function(error) {
                    errorCallback(error);
                });
            } catch (exception) {
                errorCallback(exception);
            }
        },

        getDocumentsForRequirement : function(transactionObj, successCallback,
                errorCallback) {
            try {
                var self = this;
                var dataObj = {};
                sql = _dbOperations._buildFetchScript(
                        rootConfig.requirementTableFetchFields, "RequirementFiles", {})
                        + " WHERE identifier IN ("
                        + "'"+transactionObj.transtrackingId+"'"
                        + ") and requirementName IN("
                        + "'"+transactionObj.requirementName+"'"
                        + ")and documentName IN ("
                        + "'"+transactionObj.documentName+"'"
                        + ") and status NOT IN ('Deleted')";

                //console.log(sql);
                _dbOperations._executeSql(sql, function(results) {
                    var data = _dbOperations._getRecordsArray(results);
                    successCallback(data);
                }, function(error) {
                    errorCallback(error);
                });
            } catch (exception) {
                errorCallback(exception);
            }
        },

        getRequirementsForTransId : function(data,transtrackingId, successCallback,
				errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				sql ="select count(*) as count FROM  RequirementFiles where identifier IN ("
						+ "'"+transtrackingId+"'"
						+ ") and requirementName IN ("
						+ "'"+data.requirementName+"'"
						+ ") and Status NOT IN ('Deleted')";
				
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},

		getRequirementsUploadedForTransId : function(data,transtrackingId, successCallback,
				errorCallback) {
			try {
				var self = this;
				var dataObj = {}; 
				var dataArray=[];
				for(var i=0;i<data.length;i++){
				
				dataArray.push("'"+data[i]+"'");
				}
				
				sql ="select Count(Distinct documentName) as count from RequirementFiles where documentName IN("+dataArray+") and Status NOT IN ('Deleted')";
				
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},

		getStatusForRequirement : function(data,transtrackingId, successCallback,
			errorCallback) {
		try {
			var self = this;
			var dataObj = {}; 
			var dataArray=[];
		sql ="select a.unSyncedCount, b.totalCount  from (select count(*) as unSyncedCount FROM  RequirementFiles where identifier IN ("
					+ "'"+transtrackingId+"'"
					+ ") and documentName IN ("
					+ "'"+data.Documents[0].documentName+"'"
					+ ")and Status NOT IN ('Synced')) as a , (select count(*) as totalCount FROM  RequirementFiles where identifier IN (" + "'"+transtrackingId+"'"
					+ ") and documentName IN ("
					+ "'"+data.Documents[0].documentName+"'"
					+ ")) as b";
			//console.log(sql);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				successCallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	},

    getDocumentsForSavingComment : function(data,requirementName,requirementType,transtrackingId, successCallback,
            errorCallback) {
        try {
            var self = this;
            var dataObj = {};
            sql = _dbOperations._buildFetchScript(
                    rootConfig.requirementTableFetchFields, "RequirementFiles", {})
                    
                    + " WHERE identifier IN ("
                    + "'"+transtrackingId+"'"
                    + ") and (requirementName LIKE"
                    + "'%"+requirementName+"%'"
                    + ")and (documentName LIKE "
                    + "'%"+requirementType+"%'"
                    + ")and base64string IN("+"'"+ data+"'"
                    + ") and Status NOT IN ('Deleted')";

            //console.log(sql);
            _dbOperations._executeSql(sql, function(results) {
                var data = _dbOperations._getRecordsArray(results);
                successCallback(data);
            }, function(error) {
                errorCallback(error);
            });
        } catch (exception) {
            errorCallback(exception);
        }
    },

    getRequirementsForTransaction : function(transtrackingId, successCallback,
            errorCallback) {
        try {
            var self = this;
            var dataObj = {};
            sql = _dbOperations._buildFetchScript(
                    rootConfig.requirementTableFetchFields, "RequirementFiles", {})
                    + " WHERE identifier IN ("
                    + transtrackingId
                    + ") and documentStatus NOT IN ('Deleted') ";

            //console.log(sql);
            _dbOperations._executeSql(sql, function(results) {
                var data = _dbOperations._getRecordsArray(results);
                successCallback(data);
            }, function(error) {
                errorCallback(error);
            });
        } catch (exception) {
            errorCallback(exception);
        }
    },

    getSignature : function(transtrackingId,signature,successCallback,
            errorCallback) {
        try {
            var self = this;
            var dataObj = {};
        sql = _dbOperations._buildFetchScript(
                    rootConfig.requirementTableFetchFields, "RequirementFiles", {})
                    + " WHERE identifier IN ("
                    + "'"+transtrackingId+"'"
                    + ") and(requirementName LIKE"
                    + "'%"+signature+"%'"
                    + ") ";

            //console.log(sql);
            _dbOperations._executeSql(sql, function(results) {
                var data = _dbOperations._getRecordsArray(results);
                successCallback(data);
            }, function(error) {
                errorCallback(error);
            });
        } catch (exception) {
            errorCallback(exception);
        }
    },

	getCancelledProposalsForType : function(syncStatus,type, agentId, successCallback,
            errorCallback) {
        try {

            var self = this;
            var dataObj = {};
            dataObj.Key15 = syncStatus;
            dataObj.type = type;
            dataObj.Key11=agentId;
            sql = _dbOperations._buildFetchScript("Id", "Transactions",
                    dataObj);
            //console.log(sql);

            _dbOperations._executeSql(sql, function(results) {

                //console.log(results);

                var data = _dbOperations._getRecordsArray(results);

                successCallback(data);
            }, function(error) {
                errorCallback(error);
            });
        } catch (exception) {
            errorCallback(exception);
        }
    },

    updateFileStatusSynced : function(curReqFileToSync,successCallback, errorCallback){
        try {
            var self = this;
            var dataObj = {};                                       
            sql = _dbOperations._buildUpdateScript("RequirementFiles",
                curReqFileToSync);
            sql += " WHERE  Id = " + curReqFileToSync.Id;
            _dbOperations._executeSql(sql,              
                function(results) {
                    var data = _dbOperations._getRecordsArray(results);
                    successCallback(data);
                }, function(error) {
                    errorCallback(error);
                });
        } catch (exception) {
            errorCallback(exception);
        }
    },

    deleteIfFileExists : function(fileObj,successCallback, errorCallback){
            try {
                sql = _dbOperations._buildDeleteScript("RequirementFiles",
                    "fileName = '" + fileObj.fileName +"'" );
                _dbOperations._executeSql(sql, function(data) {
                    successCallback(data);
                }, function(error) {
                    errorCallback(error);
                });
            } catch (exception) {
                errorCallback(exception);
            }
    },

    checkIfFileExists : function(fileObj,successCallback, errorCallback){
        try {
            var self = this;
            var dataObj = {};
            dataObj.fileName = fileObj.fileName;
            sql = _dbOperations._buildFetchScript("fileName","RequirementFiles",dataObj);   
            
            _dbOperations._executeSql(sql, function(results) {
                var data = _dbOperations._getRecordsArray(results);
                successCallback(data);
            }, function(error) {
                errorCallback(error);
            });
        } catch (exception) {
            errorCallback(exception);
        }
    },

	getRequirementFilesToDelete : function(trastrackingId,documentName,successCallback,
			errorCallback) {
		try {
			var self = this;
			var dataObj = {};
			sql = _dbOperations._buildFetchScript(
					rootConfig.requirementTableFetchFields, "RequirementFiles", {})

					+ " WHERE identifier IN ("
					+ "'"+trastrackingId+"'"
					+ ") and documentName IN("
					+ "'"+documentName+"'"
					+ ")and status NOT IN ('Deleted')";

			//console.log(sql);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				successCallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	},

    getStatusForDocuments : function(transactionObj, successCallback,
            errorCallback) {
        try {
            var self = this;
            var dataObj = {};
            sql = "select Distinct (documentName) as documentName,'Saved' as STATUS from RequirementFiles where (status ='Saved' or status='Deleted') and Identifier IN ("
				+ "'"+transactionObj.transtrackingId+"'"
				+ ") union select Distinct (documentName),status from RequirementFiles where status ='Synced' and Identifier IN ("
				+ "'"+transactionObj.transtrackingId+"'"
				+ ") and documentName not in (select documentName from(select Distinct (documentName),status from RequirementFiles where (status ='Saved' or status='Deleted')))"

            //console.log(sql);
            _dbOperations._executeSql(sql, function(results) {
                var data = _dbOperations._getRecordsArray(results);
                successCallback(data);
            }, function(error) {
                errorCallback(error);
            });
        } catch (exception) {
            errorCallback(exception);
        }
    },
   
		closeDatabase : function(dbName, successCallback, errorCallback) {
			try {
				_dbOperations._closeDB(dbName, function(data) {
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		deleteFromTable : function(tableName, whereClause, successCallback,
				errorCallback) {
			try {
				var self = this;
				sql = _dbOperations._buildDeleteScript(tableName, whereClause);
				_dbOperations._executeSql(sql, function(data) {
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		fetchOne : function(dataObj, successCallback, errorCallback) {
			try {
				var self = this;

			} catch (exception) {
				errorCallback(exception);
			}
		},
		fetchAll : function(dataObj, tblName, successCallback, errorCallback) {
			try {
				var self = this;
				sql = _dbOperations._buildFetchScript("*", tblName, dataObj);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		_syncProgress : function(message, percent, msgKey) {
			//console.log(message + ' (' + percent + '%)');
		},
		syncData : function(transactionObj, successCallback, errorCallback,
				options) {
			try {
				var self = this;
				var requestInfo = Request();
				requestInfo.Request.RequestPayload.Transactions
						.push(transactionObj);
				var dataObj = {};
				dataObj.type = transactionObj.Type;
				dataObj.agentId = transactionObj.Key11;
				sql = _dbOperations._buildFetchScript("last_sync", "sync_info",
						dataObj);
				_dbOperations
						._executeSql(
								sql,
								function(results) {
									var data = _dbOperations
											._getRecordsArray(results);
									var lastSyncDate;
									if (data.length == 0) {
										lastSyncDate = 0;
										var _archivalPeriod;
										switch (transactionObj.Type) {
										case "LMS":
											_archivalPeriod = rootConfig.lmsArchieveAge;
											break;
										case "FNA":
											_archivalPeriod = rootConfig.fnaArchieveAge;
											break;
										case "illustration":
											_archivalPeriod = rootConfig.illustrationArchieveAge;
											break;
										case "eApp":
											_archivalPeriod = rootConfig.eAppArchieveAge;
											break;
										}
										if (_archivalPeriod) {
											requestInfo.Request.RequestInfo.ArchivalPeriod = _archivalPeriod;
										}
									} else {
										lastSyncDate = data[0]["last_sync"];
										requestInfo.Request.RequestInfo.FirstTimeSync = false;
									}
									requestInfo.Request.RequestInfo.LastSyncDate = lastSyncDate == 0 ? ""
											: lastSyncDate;
									//console.log(JSON.stringify(requestInfo));
									_dbOperations._refreshNow(requestInfo,
											self._syncProgress,
											function(result) {
												if (result.syncOK === true) {
													// Synchronized successfully
													//console.log(result);
													successCallback();
												}
											}, errorCallback, options);
								}, function(error) {
									errorCallback(error);
								});

			} catch (exception) {
				errorCallback(exception);
			}
		},
		syncDataToServer : function(transactionObj, successCallback,
				errorCallback, saveBandwidth, options) {
			try {
				var self = this;
				var recordType = transactionObj.Type;
				options.agentId = transactionObj.Key11;
				_dbOperations._syncNow(recordType, self._syncProgress,
						function(result) {
							if (result.syncOK === true) {
								// Synchronized successfully
								var table = "Transactions";
								/*
								 * if (recordType != "FNA" && recordType !=
								 * "illustration") { successCallback(result,
								 * recordType); } else { if (result != null &&
								 * result != "" &&
								 * result.serverAnswer.Response.ResponsePayload.Transactions &&
								 * result.serverAnswer.Response.ResponsePayload.Transactions.length >
								 * 0) { _dbOperations._keyUpdation(table,
								 * result, recordType, function() {
								 * successCallback(result, recordType); },
								 * errorCallback); } else {
								 * successCallback(result, recordType); } }
								 */

								successCallback(result, recordType);
							}
						}, errorCallback, saveBandwidth, options);

			} catch (exception) {
				errorCallback(exception);
			}
		},
		updateProposalIds : function(data, successCallback, errorCallback) {
			try {
				var proposalLength = data.length;
				var updatedProposals = [];
				$
						.each(
								data,
								function(i, proposal) {
									var dataObj = {};
									if (proposal.Type != null
											&& proposal.Type == "eApp") {
										dataObj.Key4 = proposal.Key4;
									} else if (proposal.Type != null
											&& proposal.Type == "illustration") {
										dataObj.Key3 = proposal.Key3;
									} else if (proposal.Type != null
											&& proposal.Type == "FNA") {
										dataObj.Key2 = proposal.Key2;
									}
									dataObj.Key15 = proposal.Key15 == "success" ? "inProgress"
											: "saved";
									sql = _dbOperations._buildUpdateScript(
											"Transactions", dataObj);
									sql += " WHERE id = " + proposal["Id"]
											+ " ;";
									//console.log(sql);
									if (proposal.Type != null
											&& proposal.Type == "eApp") {
										updatedProposals.push({
											proposalId : proposal.Key4,
											transactionId : proposal.Id,
											status : proposal.Key15
										});
									} else if (proposal.Type != null
											&& proposal.Type == "illustration") {
										updatedProposals.push({
											proposalId : proposal.Key3,
											transactionId : proposal.Id,
											status : proposal.Key15
										});
									} else if (proposal.Type != null
											&& proposal.Type == "FNA") {
										updatedProposals.push({
											proposalId : proposal.Key2,
											transactionId : proposal.Id,
											status : proposal.Key15
										});
									}

								});
				_dbOperations._executeSql(sql, function(results) {
					successCallback(updatedProposals);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		getTransIdForProposal : function(proposalId, successCallback,
				errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				dataObj.Key4 = proposalId;
				sql = _dbOperations._buildFetchScript("Id", "Transactions",
						dataObj);
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					var transactionid = data[0].Id;
					successCallback(transactionid);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		updateTransTblStatus : function(tableName, syncStatus, transactionIds,
				successCallback, errorCallback) {
			try {
				var self = this;
				var transactionData = {
					Key16 : syncStatus
				};
				sql = _dbOperations._buildUpdateScript("Transactions",
						transactionData);
				sql += " WHERE 	Id in (" + transactionIds.toString() + ");";
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					self.updateStatusTblStatus(syncStatus, transactionIds,
							successCallback, errorCallback);
				}, function(error) {
					errorCallback(error);
				});
			} catch (e) {
				errorCallback(e);
			}
		},
		updateTransStatusAndData : function(transactionData, syncStatus,
				transactionIds, successCallback, errorCallback) {
			try {
				var self = this;
				var transactionDatas = {};
				transactionDatas.Key16 = syncStatus;
				transactionDatas.TransactionData = angular
						.toJson(transactionData);
				sql = _dbOperations._buildUpdateScript("Transactions",
						transactionDatas);
				sql += " WHERE 	Id in (" + transactionIds.toString() + ");";

				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					self.updateStatusTblStatus(syncStatus, transactionIds,
							successCallback, errorCallback);
				}, function(error) {
					errorCallback(error);
				});
			} catch (e) {
				errorCallback(e);
			}
		},
		updateStatusTblStatus : function(status, transactionIds,
				successCallback, errorCallback) {
			try {
				var self = this;
				var data = {
					SyncResponseStatus : status
				};
				sql = _dbOperations._buildUpdateScript("SyncStatus", data);
				sql += " WHERE 	RecordId in (" + transactionIds.toString()
						+ ");";
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					self.deleteFrmNewElem(transactionIds, successCallback,
							errorCallback);
				}, function(error) {
					errorCallback(error);
				});
			} catch (error) {
				errorCallback(error);
			}
		},
		deleteFrmNewElem : function(transactionIds, successCallback,
				errorCallback) {
			try {
				var self = this;
				sql = " DELETE FROM new_elem WHERE Id in ("
						+ transactionIds.toString() + ");";
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					successCallback();
				}, function(error) {
					errorCallback(error);
				});
			} catch (e) {
				errorCallback(e);
			}
		},
		getDocumentsForUpload : function(transactionId, successCallback,
				errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				dataObj.parentId = transactionId;
				sql = _dbOperations._buildFetchScript("DocumentObject",
						"Eapp_Attachments", dataObj);
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		getAllDocsToSync : function(syncStatus, recordType,selectedIds, successCallback,
				errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				dataObj.type = recordType;
				sql = _dbOperations._buildFetchScript("Id", "Transactions",
						dataObj)
						+ " AND Key16 NOT IN ('" + syncStatus + "') ";
				if(selectedIds && selectedIds.length)
					sql= sql + "AND Id IN ("+selectedIds+")";
				mainSql = _dbOperations._buildFetchScript("*",
						"Eapp_Attachments", {})
						+ " WHERE documentStatus NOT IN ('Synced') and  parentId IN ("
						+ sql + ") ORDER BY parentId";
				//console.log(mainSql);
				_dbOperations._executeSql(mainSql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		getKey4ForDoc : function(id, keyToUpdate, successCallback,
				errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				dataObj.Id = id;
				sql = _dbOperations._buildFetchScript(keyToUpdate,
						"Transactions", dataObj);
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					if (keyToUpdate == 'Key4') {
						var id = data[0].Key4;
					} else if (keyToUpdate == 'Key3') {
						var id = data[0].Key3;
					}
					successCallback(id);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		getDocumentsForTransaction : function(transactionId, successCallback,
				errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				sql = _dbOperations._buildFetchScript(
						rootConfig.docTableFetchFields, "Eapp_Attachments", {})
						+ " WHERE parentId IN ("
						+ transactionId
						+ ") and documentStatus NOT IN ('Deleted') ";

				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		getRequirementsFilesToSync : function(transtrackingId, successCallback,
                errorCallback) {
            try {
                var self = this;
                var dataObj = {};
                sql = _dbOperations._buildFetchScript(
                        rootConfig.requirementTableFetchFields, "RequirementFiles", {})
                        + " WHERE identifier IN ('"
                        + transtrackingId
                        + "') AND status NOT IN ('Synced')";

                console.log(sql);
                _dbOperations._executeSql(sql, function(results) {
                    var data = _dbOperations._getRecordsArray(results);
                    successCallback(data);
                }, function(error) {
                    errorCallback(error);
                });
            } catch (exception) {
                errorCallback(exception);
            }
        },
		getTransDataWithStatus : function(ReqTransTblCol, proposalStatus,type, agentId,
                successCallback, errorCallback) {
            // to get the req columns from the transaction table whose status in
            // syncstatus table is the given proposal status
            try {
                var self = this;
                var dataObj = {};
                dataObj.SyncResponseStatus = proposalStatus;
                sql = _dbOperations._buildFetchScript("RecordId", "SyncStatus",
                        dataObj);
                mainSql = _dbOperations._buildFetchScript(ReqTransTblCol,
                        "Transactions", {})
                        + " WHERE Id IN (" + sql + ") and type = '"+ type +"' and Key11='"+ agentId +"'";
                console.log(mainSql);
                _dbOperations._executeSql(mainSql, function(results) {
                    var data = _dbOperations._getRecordsArray(results);
                    successCallback(data);
                }, function(error) {
                    errorCallback(error);
                });
            } catch (exception) {
                errorCallback(exception);
            }
        },
		getFile : function(fileToview, successCallback, errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				if (fileToview.fileName != null) {
					dataObj.fileName = fileToview.fileName;
				}
				sql = _dbOperations._buildFetchScript("*", "Eapp_Attachments",
						dataObj);
				//console.log(sql);

				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		downloadPDF : function(transactionObj, templateId, successCallback,
				errorCallback, options, $http) {
			var self = this;
			var downloadPdfUrl = rootConfig.serviceBaseUrl
					+ "documentService/getbase64stringpdf";

			var requestInfo = Request();
			//console.log(JSON.stringify(transactionObj));
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObj);

			var request = {
				method : 'POST',
				url : downloadPdfUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request)
					.success(
							function(data, status, headers, config) {
								var base64pdf = data.Response.ResponsePayload.Transactions[0].base64pdf;
								cordova.exec(function() {
								}, function() {
								}, "PdfViewer", "showPdfFromBase64",
										[ base64pdf ]);
								successCallback(transactionObj);
							}).error(
							function(data, status, headers, config) {
								//console.log('error' + JSON.stringify(data)
										//+ 'status' + status);
								errorCallback(data, status,
										resources.errorMessage);
							});
		},
		saveOnline : function(transactionObj, successCallback, errorCallback,
				options, $http) {
			var self = this;
			var saveUrl = rootConfig.serviceBaseUrl + "lifeEngageService/save";
			var requestInfo = Request();
			//console.log(JSON.stringify(transactionObj));
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObj);

			var request = {
				method : 'POST',
				url : saveUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request)
					.success(
							function(data, status, headers, config) {
								if( data && data.Response.ResponsePayload.Transactions && data.Response.ResponsePayload.Transactions.length > 0 ){
									var key4 = data.Response.ResponsePayload.Transactions[0].Key4;
									transactionObj.Key3 = data.Response.ResponsePayload.Transactions[0].Key3;
									transactionObj.Key4 = key4;
									transactionObj.Key12 = data.Response.ResponsePayload.Transactions[0].Key12;
									successCallback(transactionObj);
								}else{
									errorCallback(data, status);
								}
								
							}).error(
							function(data, status, headers, config) {
								//console.log('error' + JSON.stringify(data)
										//+ 'status' + status);
								errorCallback(data, status,
										resources.errorMessage);
							});
		},
		getDocument : function(document, successCallback, errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				dataObj.documentName = document.documentName;
				sql = _dbOperations._buildFetchScript("*", "Eapp_Attachments",
						dataObj);
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		isDuplicateLead : function(transactionData, successCallback,
				errorCallback) {
			try {
				var dataObj = {};
				var isDuplicate = false;
				var Id = parseInt(transactionData.Id);
				dataObj.Key2 = transactionData.Key2;
				dataObj.Key4 = transactionData.Key4;
				dataObj.filter = "Key15  <> 'Cancelled'";
				var sql = _dbOperations._buildFetchScript("Id", "Transactions",
						dataObj);
				_dbOperations._executeSql(sql, function(results) {

					if (results.rows.length !== 0) {
						var data = _dbOperations._getRecordsArray(results);
						if (Id !== data[0].Id) {
							isDuplicate = true;
						}
					}
					successCallback(isDuplicate);

				}, function(error) {
					errorCallback(error);
				});

			} catch (exception) {
				errorCallback(exception);
			}
		},
		pushEncryptionFilesToArray: function(fileArray){
			var filesToBeEncrypted = [];
			for(var i=0 ;i<fileArray.length; i++) {
				var fileDesc = {};
				fileDesc.fileName = fileArray[i];
				filesToBeEncrypted.push(fileDesc);
			}
			return filesToBeEncrypted;
		},
		initializeDb : function(successFileList) {
			var self = this;
			var filesArray = [];
			for ( var i = 0; i < successFileList.length; i++) {
				if (successFileList[i].indexOf(".db") >= 0 && successFileList[i] != "LifeEngageData.db") {
					filesArray.push(successFileList[i]);
				}
				if (successFileList[i] == "LifeEngageData.db") {
					self.closeDatabase("LifeEngageData", function() {
						_dbOperations._db = null;
					});
				}
			}
			var fileListToBeEncrypted = {};
			fileListToBeEncrypted.fileArray = self.pushEncryptionFilesToArray(filesArray);
			fileListToBeEncrypted.appName = rootConfig.appName;
			window.plugins.LEEncryption.encryptDB([fileListToBeEncrypted], function(data){});
		},
		getLMSCount : function(transactionObj, agentId, successCallback, errorCallback) {

			sql = "SELECT A.Id,A.Type,A.Key1,A.Key2,A.Key4,A.Key16, '' as Key19 ,B.Key7 FROM Transactions A JOIN Transactions B ON B.Type = 'FNA' AND A.TransTrackingId= B.Key1 WHERE A.Type = 'LMS' AND A.Key11='"+ agentId +"' UNION ALL SELECT A.Id,A.Type,A.Key1,A.Key2,A.Key4,A.Key16,A.Key19, '' as Key7 FROM Transactions A WHERE A.Type = 'LMS' AND A.Key11='"+ agentId +"'";

			_dbOperations
					._executeSql(
							sql,
							function(results) {
								var data = _dbOperations
										._getRecordsArray(results);
								var dayCount = 0;
								var weekCount = 0;
								var monthCount = 0;
								var viewallCount = 0;
								var today = new Date();
								var yyyy = today.getFullYear();
								var dd = today.getDate();
								var mm = today.getMonth() + 1;
								if (mm < 10) {
									mm = '0' + mm;
								}
								var currentDate = yyyy + '-' + mm + '-' + dd;
								var dateTemp = currentDate.split("-");
								var dateSplit = dateTemp[0] + "-"
										+ ("0" + dateTemp[1]).slice(-2) + "-"
										+ ("0" + dateTemp[2]).slice(-2);
								var dateDOB = "-"
										+ ("0" + dateTemp[1]).slice(-2) + "-"
										+ ("0" + dateTemp[2]).slice(-2);

								for (var i = 0; i < data.length; i++) {
									var date1 = new Date();
									var endDate = new Date(date1.setTime(date1
											.getTime() + 7 * 86400000));
									var year = endDate.getFullYear();
									var day = endDate.getDate();
									var month = endDate.getMonth() + 1;
									endDate = year + '-' + month + '-' + day;
									var endTemp = endDate.split("-");
									var endDOB = "-"
											+ ("0" + endTemp[1]).slice(-2)
											+ "-"
											+ ("0" + endTemp[2]).slice(-2);
									if ((LEDate(data[i].Key19) >= LEDate(currentDate) && LEDate(data[i].Key19) < LEDate(endDate))) {
										weekCount = weekCount + 1;
									}
									if (data[i].Key7 != ""
											&& data[i].Key7 != null) {
										var fnaDOB = data[i].Key7;
										var fnaTempDOB = fnaDOB.split("-");
										var fnaDateDOB = "-" + fnaTempDOB[1]
												+ "-" + fnaTempDOB[2];
										if ((fnaDateDOB >= dateDOB)
												&& (fnaDateDOB < endDOB)) {
											weekCount = weekCount + 1;
										}
									}
								}

								for (var i = 0; i < data.length; i++) {
									var lmsDate = data[i].Key19;
									var setLmsDate = new Array();
									var setLmsDate = lmsDate.split(" ");
									if (setLmsDate[0] == dateSplit) {
										dayCount = dayCount + 1;
									}
									if (data[i].Key7 != ""
											&& data[i].Key7 != null) {
										var fnaDOB = data[i].Key7;
										var fnaTempDOB = fnaDOB.split("-");
										var fnaDateDOB = "-" + fnaTempDOB[1]
												+ "-" + fnaTempDOB[2];
										if (fnaDateDOB == dateDOB) {
											dayCount = dayCount + 1;
										}
									}
								}

								for (var i = 0; i < data.length; i++) {
									var date2 = new Date();
									var endDate = new Date(date2.setTime(date1
											.getTime() + 30 * 86400000));
									var year = endDate.getFullYear();
									var day = endDate.getDate();
									var month = endDate.getMonth() + 1;
									endDate = year + '-' + month + '-' + day;
									var endTemp = endDate.split("-");
									var endDOB = "-"
											+ ("0" + endTemp[1]).slice(-2)
											+ "-"
											+ ("0" + endTemp[2]).slice(-2);
									if ((LEDate(data[i].Key19) >= LEDate(currentDate) && LEDate(data[i].Key19) < LEDate(endDate))) {
										monthCount = monthCount + 1;
									}
									if (data[i].Key7 != ""
											&& data[i].Key7 != null) {
										var fnaDOB = data[i].Key7;
										var fnaTempDOB = fnaDOB.split("-");
										var fnaDateDOB = "-" + fnaTempDOB[1]
												+ "-" + fnaTempDOB[2];
										if ((fnaDateDOB >= dateDOB)
												&& (fnaDateDOB < endDOB)) {
											monthCount = monthCount + 1;
										}
									}
								}

								var sum = {
									"day" : dayCount,
									"week" : weekCount,
									"month" : monthCount
								};

								successCallback(sum);
							}, function(error) {
								errorCallback(error);
							});
		},
		getCalenderListings : function(currentDate, todoSelected, endDate,
				viewallDate, transactionObj, successCallback, errorCallback) {
			try {
				var self = this;

				sql = "SELECT A.Id,A.Type,A.Key1,A.Key2,A.Key4,A.Key16, '' as Key19 ,B.Key7 FROM Transactions A JOIN Transactions B ON B.Type = 'FNA' AND A.TransTrackingId= B.Key1 WHERE A.Type = 'LMS' UNION ALL SELECT A.Id,A.Type,A.Key1,A.Key2,A.Key4,A.Key16,A.Key19, '' as Key7 FROM Transactions A WHERE A.Type = 'LMS'";

				_dbOperations
						._executeSql(
								sql,
								function(results) {
									var out = [];
									var data = _dbOperations
											._getRecordsArray(results);

									if (todoSelected == "Week"
											|| todoSelected == "Month") {
										var out = [];
										var dateTemp = currentDate.split("-");
										var endTemp = endDate.split("-");
										var currentYear = dateTemp[0];
										var dateDOB = "-"
												+ ("0" + dateTemp[1]).slice(-2)
												+ "-"
												+ ("0" + dateTemp[2]).slice(-2);
										var endDOB = "-"
												+ ("0" + endTemp[1]).slice(-2)
												+ "-"
												+ ("0" + endTemp[2]).slice(-2);
										for (var i = 0; i < data.length; i++) {
											if ((LEDate(data[i].Key19) >= LEDate(currentDate) && LEDate(data[i].Key19) < LEDate(endDate))) {
												out.push(data[i]);
											}

										}
										for (var i = 0; i < data.length; i++) {
											if (data[i].Key7 != ""
													&& data[i].Key7 != null) {

												var fnaDOB = data[i].Key7;
												var fnaTempDOB = fnaDOB
														.split("-");
												var fnaDateDOB = "-"
														+ fnaTempDOB[1] + "-"
														+ fnaTempDOB[2];
												if ((fnaDateDOB >= dateDOB)
														&& (fnaDateDOB < endDOB)) {
													var currentDOBYear = currentYear
															+ fnaDateDOB;
													data[i].Key7 = currentDOBYear;
													data[i].Key19 = "";
													out.push(data[i]);
												}
											}
										}
									} else if (todoSelected == "All") {
										for (var i = 0; i < data.length; i++) {
											if ((LEDate(data[i].Key19) >= LEDate(viewallDate) && LEDate(data[i].Key19) < LEDate(endDate))) {
												out.push(data[i]);
											}
										}
										for (var i = 0; i < data.length; i++) {
											if (data[i].Key7 != ""
													&& data[i].Key7 != null) {
												var dateTemp = currentDate
														.split("-");
												var endTemp = endDate
														.split("-");
												var currentYear = dateTemp[0];
												var dateDOB = "-"
														+ ("0" + dateTemp[1])
																.slice(-2)
														+ "-"
														+ ("0" + dateTemp[2])
																.slice(-2);
												var endDOB = "-"
														+ ("0" + endTemp[1])
																.slice(-2)
														+ "-"
														+ ("0" + endTemp[2])
																.slice(-2);
												var fnaDOB = data[i].Key7;
												var fnaTempDOB = fnaDOB
														.split("-");
												var fnaDateDOB = "-"
														+ fnaTempDOB[1] + "-"
														+ fnaTempDOB[2];
												if ((fnaDateDOB >= dateDOB)
														&& (fnaDateDOB < endDOB)) {
													var currentDOBYear = currentYear
															+ fnaDateDOB;
													data[i].Key7 = currentDOBYear;
													out.push(data[i]);
												}
											}
										}
									} else {
										var dateTemp = currentDate.split("-");
										var dateSplit = dateTemp[0] + "-"
												+ ("0" + dateTemp[1]).slice(-2)
												+ "-"
												+ ("0" + dateTemp[2]).slice(-2);
										var dateDOB = "-"
												+ ("0" + dateTemp[1]).slice(-2)
												+ "-"
												+ ("0" + dateTemp[2]).slice(-2);
										for (var i = 0; i < data.length; i++) {
											var lmsDate = formatForDateControl(data[i].Key19);
											if (lmsDate == dateSplit) {
												out.push(data[i]);
											}
										}
										for (var i = 0; i < data.length; i++) {
											if (data[i].Key7 != ""
													&& data[i].Key7 != null) {

												var fnaDOB = data[i].Key7;
												var fnaTempDOB = fnaDOB
														.split("-");
												var currentYear = dateTemp[0];
												var fnaDateDOB = "-"
														+ fnaTempDOB[1] + "-"
														+ fnaTempDOB[2];
												if (fnaDateDOB == dateDOB) {
													var currentDOBYear = currentYear
															+ fnaDateDOB;
													data[i].Key7 = currentDOBYear;
													out.push(data[i]);
												}
											}
										}
									}

									successCallback(out, todoSelected,
											currentDate, endDate, viewallDate);

								}, function(error) {
									errorCallback(error);
								});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		getEmptyTransTrackingID : function(successCallback) {
			var transTrackingIDQuery = "select Id from transactions where Id IN ( SELECT DISTINCT id FROM new_elem ) AND ( TransTrackingID IS NULL OR TransTrackingID = '')";
			_dbOperations._executeSql(transTrackingIDQuery, function(result) {
				var data = _dbOperations._getRecordsArray(result);
				successCallback(data);
			});
		},
		updateTransTrackingID : function(data, transactionData, successCallback) {
			$.each(data, function(i, obj) {
				if (obj) {
					sql = _dbOperations._buildUpdateScript("Transactions",
							transactionData[i]);
					sql += " WHERE 	Id = " + obj.Id;
					_dbOperations._executeSql(sql);
				}
			});
			successCallback();
		},
		// emailBI change 
		fetchTransactionForIds : function(transObj, successCallback,
				errorCallback) {
			var selectedIds = transObj.TransactionData.searchCriteriaRequest.selectedIds;
			try {
				sql = _dbOperations._buildFetchScript("*", "Transactions");
				sql += " WHERE 	Id IN ("
						+ _dbOperations._arrayToString(selectedIds, ',') + ")";
				_dbOperations._executeSql(sql, function(result) {
					var data = _dbOperations._getRecordsArray(result);
					$.each(data, function(i, obj) {
						var formattedData = JSON
								.parse(unescape(obj.TransactionData));
						obj.TransactionData = formattedData;
					});
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},

		saveTransactionObjectsMultiple : function(transObjects,
				successCallback, errorCallback) {
			var index = 0;
			var errorObjs = [];
			var saveSingle = function() {
				index += 1;
				if (index < transObjects.length) {
					DbOperationsUtility.saveTransactions(transObjects[index],
							saveSingle, errorCB);
				} else {
					successCallback(errorObjs);
				}
			}

			var errorCB = function(err) {// Keeping failed transactions in
											// errorObj - Cab be used if needed.
				errorObj = {};
				errorObj.id = transObjects[index].Id;
				errorObj.error = err;
				errorObj.push(errorObj);
				index += 1;
				if (index < transObjects.length) {
					DbOperationsUtility.saveTransactions(transObjects[index],
							saveSingle, errorCB);
				} else {
					successCallback(errorObjs);
				}
			}
			DbOperationsUtility.saveTransactions(transObjects[index],
					saveSingle, errorCB);
		},
		saveAgentProfile: function(transactionObj, successCallback,
	            errorCallback) {
	            var self = this;
	            var achievementDetails = [];
	            var agentDetails = transactionObj.AgentDetails;
	            achievementDetails = transactionObj.Achievements;
	            self.saveOrUpdateAgentDetails(agentDetails,
	                function() {
	                	successCallback();
	                	/*FNA changes by LE Team - achievementDetails needs to be saved - starts>>*/
	                    if (achievementDetails.length > 0) {
	                        self.isAchievementExistsForAgent(agentDetails.agentCode, function(isExists) {

	                                self.saveOrUpdateAchievements(achievementDetails, isExists, agentDetails.agentCode, successCallback,
	                                    errorCallback);
	                            },
	                            errorCallback);


	                    }else{
	                    	successCallback();
	                    }
	                },
	                errorCallback);
	            /*FNA changes by LE Team - achievementDetails needs to be saved - ends>>*/

	        },
	        saveOrUpdateAgentDetails: function(transactionData, successCallback,
	            errorCallback) {
	            try {
	                delete transactionData.group;
	                sql = _dbOperations._buildInsertOrUpdateScript("AGENT", "agentCode",
	                    transactionData);
	                _dbOperations._executeSql(sql, function(data) {
	                    successCallback();
	                }, function(error) {
	                    errorCallback(error);
	                });
	            } catch (exception) {
	                errorCallback(exception);
	            }
	        },
	        saveOrUpdateAchievements: function(transactionData, isExists, agentId, successCallback,
	            errorCallback) {
	            var self = this;
	            $.each(transactionData, function(i, obj) {
	                obj.agentId = agentId;
	            });
	            if (isExists) {
	                var whereClause = "agentId=" + agentId;
	                self.deleteFromTable("ACHIEVEMENT", whereClause,
	                    function() {
	                        self.saveAchievements(transactionData, agentId, successCallback,
	                            errorCallback);
	                    }, errorCallback)
	            } else {
	                self.saveAchievements(transactionData, agentId, successCallback,
	                    errorCallback);
	            }


	        },
	        saveAchievements: function(transactionData, agentId, successCallback,
	            errorCallback) {
	            try {
	                sql = _dbOperations._buildBatchInsertScriptAchievement("ACHIEVEMENT",
	                    transactionData);
	                //console.log("achievemnt query" + sql);
	                _dbOperations._executeSql(sql, function(data) {
	                    successCallback();
	                }, function(error) {
	                    errorCallback(error);
	                });
	            } catch (exception) {
	                errorCallback(exception);
	            }
	        },
	        isAchievementExistsForAgent: function(agentId, successCallback, errorCallback) {
	            try {
	                var isExists = false;
	                var dataObj = {};
	                dataObj.agentId = agentId;
	                var sql = _dbOperations._buildFetchScript("Id", "ACHIEVEMENT",
	                    dataObj);
	                //console.log(sql);
	                _dbOperations._executeSql(sql, function(results) {
	                    var data = _dbOperations._getRecordsArray(results);
	                    if (data.length > 0) {
	                        isExists = true;
	                    }
	                    successCallback(isExists);
	                }, function(error) {
	                    errorCallback(error);
	                });
	            } catch (exception) {
	                errorCallback(exception);
	            }
	        },
	        retrieveAgentDetails: function(transactionObj, successCallback,
	            errorCallback) {
	            try {
	                var self = this;
	                var dataObj = {};
	                dataObj.agentCode = transactionObj.Key11;
	                sql = _dbOperations._buildFetchScript("*",
	                    "AGENT", dataObj);
	                //console.log(sql);
	                _dbOperations._executeSql(sql, function(results) {

	                    //console.log(results);
	                    var data = _dbOperations._getRecordsArray(results);
	                    successCallback(data);
	                }, function(error) {
	                    errorCallback(error);
	                });
	            } catch (exception) {
	                errorCallback(exception);
	            }
	        },
	        retrieveAchievements: function(transactionObj, successCallback,
	            errorCallback) {
	            try {
	                var self = this;
	                var dataObj = {};
	                dataObj.agentId = transactionObj.Key11;
	                sql = _dbOperations._buildFetchScript("*", "ACHIEVEMENT",
	                    dataObj);
	                _dbOperations._executeSql(sql, function(results) {

	                    //console.log(results);
	                    var data = _dbOperations._getRecordsArray(results);
	                    successCallback(data);
	                }, function(error) {
	                    errorCallback(error);
	                });
	            } catch (exception) {
	                errorCallback(exception);
	            }
	        },
		updateTransactionAfterSync : function(transactionData, 
				transactionId, successCallback, errorCallback) {
			try {
				var self = this;
				sql = _dbOperations._buildUpdateScript("Transactions",
						transactionData);
				sql += " WHERE 	Id in (" + transactionId + ");";
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					successCallback(results);
				}, function(error) {
					errorCallback(error);
				});
			} catch (error) {
				errorCallback(error);
			}
		}
		
	};
	var _dbOperations = {
		_db : null,
		_dbInit : function(cb) {
			var self = this;
			window.sqlitePlugin.openDatabase("LifeEngageData",
					"1.0", "LifeEngageData", -1, function(db){
                self._db = db;
                var alterQueriesArray =[];
                var tblTransactionQuery = "CREATE TABLE IF NOT EXISTS Transactions (Id INTEGER PRIMARY KEY ,Key1 TEXT NOT NULL,Key2 TEXT ,Key3 TEXT ,Key4 TEXT,Key5 TEXT NOT NULL,Key6 TEXT,Key7 TEXT,Key8 TEXT,Key9 TEXT,Key10 TEXT,Key11 TEXT NOT NULL,Key12 TEXT,Key13 TEXT,Key14 TEXT,Key15 TEXT,Key16 TEXT,Key17 TEXT,Key18 TEXT,Key19 TEXT,Key20 TEXT,Key21 TEXT,Key22 TEXT,Key23 TEXT,Key24 TEXT,Key25 TEXT,Key26 TEXT,Key27 TEXT,Key28 TEXT,Key29 TEXT,Key30 TEXT,Type TEXT,TransactionData TEXT,TransTrackingID INTEGER,creationDate TEXT,modifiedDate TEXT,isLocked TEXT DEFAULT 'false',isUnlockedByAgent TEXT)";
                self._executeSql(tblTransactionQuery, function(){
                    var tblTransactionMetadataQuery = "PRAGMA table_info(Transactions);";
                    self._executeSql(tblTransactionMetadataQuery,function(result) {
                        var data = _dbOperations._getRecordsArray(result);
                        var isColumnFlag = false;
                        var isCreationDateFlag=false;
                        var isModifiedDateFlag=false;
						var isLockedFlag=false;
    					var isUnlockedByAgentFlag=false;
                        $.each(data, function(i, obj) {
                            if (obj.name == "TransTrackingID") {
                                isColumnFlag = true;
                            } else if(obj.name == "creationDate"){
                                isCreationDateFlag= true;
                            } else if(obj.name == "modifiedDate"){
                                isModifiedDateFlag= true;
                            }else if(obj.name == "isLocked"){
    							isLockedFlag=true;
    						}else if(obj.name == "isUnlockedByAgent"){
    							isUnlockedByAgentFlag=true;
    						}
                        });
                        if (!isColumnFlag) {
                            alterQueriesArray.push("ALTER TABLE Transactions ADD TransTrackingID INTEGER");
                        }
                        if(!isCreationDateFlag){
                            alterQueriesArray.push("ALTER TABLE Transactions ADD creationDate TEXT");
                        }
                        if(!isModifiedDateFlag){
                            alterQueriesArray.push("ALTER TABLE Transactions ADD modifiedDate TEXT");
                        }
						if(!isLockedFlag){
    						alterQueriesArray.push("ALTER TABLE Transactions ADD isLocked TEXT DEFAULT 'false'");
    					}
    					if(!isUnlockedByAgentFlag){
    						alterQueriesArray.push("ALTER TABLE Transactions ADD isUnlockedByAgent TEXT");
    					}
                        var tblRequirementQuery = "CREATE TABLE IF NOT EXISTS RequirementFiles (Id INTEGER PRIMARY KEY,identifier TEXT,requirementName TEXT,documentName TEXT,fileName TEXT,status TEXT, date TEXT,base64string TEXT,description TEXT)";
                        self._executeSql(tblRequirementQuery, function(){
                            var tblAttachmentsQuery = "CREATE TABLE IF NOT EXISTS Eapp_Attachments (Id INTEGER PRIMARY KEY,ParentId TEXT,DocumentType TEXT,DocumentName TEXT,DocumentStatus TEXT,Date TEXT,DocumentDescription TEXT,DocumentObject TEXT)";
                            self._executeSql(tblAttachmentsQuery, function() {
                                var tblSyncStatusQuery = "CREATE TABLE IF NOT EXISTS SyncStatus (Id INTEGER PRIMARY KEY,RecordId INTEGER,SynAttemptTS TEXT,SyncResponseStatus TEXT,SyncResponseStatusDesc TEXT,SyncResponseTS TEXT)";
                                self._executeSql(tblSyncStatusQuery, function() {
                                    var tblAgentQuery = "CREATE TABLE IF NOT EXISTS AGENT (Id INTEGER PRIMARY KEY,agentCode TEXT,agentDesc TEXT,agentName TEXT,designation TEXT,userId TEXT,employeeType TEXT,supervisorCode TEXT,office TEXT,unit TEXT,agentGroup TEXT,yearsofExperience TEXT,businessSourced TEXT,customerServicing TEXT,licenseNumber TEXT,agentLicensestartdate TEXT,agentLicenseExpDate TEXT,emailId TEXT,mobileNumber TEXT,channelId TEXT,status TEXT)";
                                    self._executeSql(tblAgentQuery, function(){
                                        var tblAgentMetadataQuery = "PRAGMA table_info(AGENT);";
                                        self._executeSql(tblAgentMetadataQuery,function(result) {
                                            var data = _dbOperations._getRecordsArray(result);
                                            isStatusColumnFlag = false;
                                            $.each(data, function(i, obj) {
                                                if (obj.name == "status") {
                                                    isStatusColumnFlag = true;
                                                }
                                            });
                                            if (!isStatusColumnFlag) {
                                                alterQueriesArray.push("ALTER TABLE AGENT ADD status TEXT");
                                            }
                                            var tblAchievementQuery = "CREATE TABLE IF NOT EXISTS ACHIEVEMENT (Id INTEGER PRIMARY KEY ,agentId TEXT,image TEXT,title TEXT,description TEXT)";
                                            self._executeSql(tblAchievementQuery, function(){
                                                var tblUserDetails = "CREATE TABLE IF NOT EXISTS UserDetails (userId TEXT PRIMARY KEY ,password TEXT,token TEXT NOT NULL,lastLoggedInDate TEXT NOT NULL)";
                                                self._executeSql(tblUserDetails, function(){
                                                     var tblMappedBranches = "CREATE TABLE IF NOT EXISTS MappedBranches (Id INTEGER PRIMARY KEY ,branchId TEXT,branchName TEXT ,latitude TEXT,longitude TEXT,radius TEXT,agentCode TEXT)";
													 self._executeSql(tblMappedBranches, function(){
														self._executeUpdateQueries(alterQueriesArray, function(){
															self._createDBSync(function(){
																cb();
															});
														});
													});
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
		},
		_executeUpdateQueries : function(updateQueryArray, successCallback){
		    var self = this;
		    if(updateQueryArray.length == 0) {
		        successCallback();
		    } else {
		        var tblUserDetails = updateQueryArray.shift();
                self._executeSql(tblUserDetails, function(){
                    self._executeUpdateQueries(updateQueryArray, successCallback);
                });
		    }
		},
		_getIdExitingInDB : function(tableName, idName, listIdToCheck,
				successCallback, errorCallback) {
			if (listIdToCheck.length === 0) {
				successCallback([]);
				return;
			}
			var self = this;
			var SQL = 'SELECT ' + idName + ' FROM ' + tableName + ' WHERE '
					+ idName + ' IN ("'
					+ self._arrayToString(listIdToCheck, '","') + '")';
			self._executeSql(SQL, function(ids) {
				var data = _dbOperations._getRecordsArray(ids);
				var idsInDb = [];
				for (var i = 0; i < data.length; ++i) {
					idsInDb[data[i][idName]] = true;
				}
				successCallback(idsInDb);
			}, errorCallback);
		},
		_syncNow : function(type, progressIndicator, successCallback,
				errorCallback, saveBandwidth, options) {
			DBSYNC.syncNow(type, progressIndicator, function(result) {
				if (result.syncOK === true) {
					// Synchronized successfully
					//console.log(result);
					successCallback(result);
				}
			}, errorCallback, saveBandwidth, options);
		},
		_refreshNow : function(transObj, progressIndicator, successCallback,
				errorCallback, options) {
			DBSYNC.refreshNow(transObj, progressIndicator, function(response) {

				successCallback(response);
			}, errorCallback, options);
		},
		_createDBSync : function(cb) {
			var self = this;
			var saveUrl = rootConfig.serviceBaseUrl + "lifeEngageService/save";
			TABLES_TO_SYNC = [ {
				tableName : 'Transactions',
				idName : 'Id'
			} ];
			var syncInfo;
			DBSYNC.initSync(TABLES_TO_SYNC, self._db, {
				userEmail : 'test@gmail.com',
				device_uuid : 'UNIQUE_DEVICE_ID_287CHBE873JB',
				lastSyncDate : 0
			}, saveUrl, function() {
				//console.log('db initialisation success');
				cb();
			});
		},
		_closeDB : function(dbName, successCallback, errorCallback) {
			try {
				var self = this;
				if (self._db != null) {
					self._db.close(dbName);
					if (successCallback && typeof successCallback == "function") {
						successCallback();
					} else {
						self._defaultCallBack();
					}
				}
			} catch (e) {
				//console.log("ERROR: " + e.message);
				errorCallback(e);
			}
		},
		_executeSql : function(sql, successCallback, errorCallback) {
			var self = this;
			try {
				if (self._db == null) {
					self._dbInit(function(){
						self._getTransactionAndExecuteSql(sql, successCallback, errorCallback);
					});
				} else {
					self._getTransactionAndExecuteSql(sql, successCallback, errorCallback);
				}
			} catch (e) {
				console.log("ERROR: " + e.message);
				errorCallback(e);
			}
        },
		_getTransactionAndExecuteSql : function(sql, successCallback, errorCallback){
			var self = this;
			self._db.transaction(function(tx) {
				tx.executeSql(sql, [], function(tx, results) {
					if (successCallback && typeof successCallback == "function") {
						successCallback(results);
					} else {
						self._defaultCallBack(tx, results);
					}
				}, function(tx, e) {
					if (errorCallback && typeof errorCallback == "function") {
						errorCallback(e);
					} else {
						self._errorHandler(tx, results);
					}
				});
			});
		},
		_buildInsertScript : function(tableName, trasactionData) {
			var members = this._getAttributesList(trasactionData);
			var values = this._getMembersValue(trasactionData, members);
			if (members.length === 0) {
				throw 'buildInsertSQL : Error, try to insert an empty object in the table '
						+ tableName;
			}
			var sql = "INSERT INTO " + tableName + " (";
			sql += this._arrayToString(members, ',');
			sql += ") VALUES (";
			sql += this._arrayToString(values, ',');
			sql += ")";
			return sql;

		},
		_buildBatchInsertScript: function(tableName, transactionData) {
            var self = this;
            var members = this._getAttributesList(transactionData[0]);
            var values = this._getMembersValue(transactionData, members);
            if (members.length === 0) {
                throw 'buildInsertSQL : Error, try to insert an empty object in the table ' + tableName;
            }
            var sql = "INSERT INTO " + tableName + " (";
            sql += this._arrayToString(members, ',');
            sql += ") VALUES ";

            $.each(transactionData, function(i, obj) {
                var values = self._getMembersValue(obj, members);
                sql += "(";
                sql += self._arrayToString(values, ',');
                sql += ")";
                if (i < transactionData.length - 1) {
                    sql += ",";
                }

            });
            //console.log("_buildBatchInsertScript" + sql);
            return sql;

        },
        _buildBatchInsertScriptAchievement: function(tableName, transactionData) {
            var self = this;
            var members = this._getAttributesList(transactionData[0]);
            var values = this._getMembersValue(transactionData, members);
            if (members.length === 0) {
                throw 'buildInsertSQL : Error, try to insert an empty object in the table ' + tableName;
            }
            var sql = "INSERT OR REPLACE INTO " + tableName + " (";
            sql += this._arrayToString(members, ',');
            sql += ") VALUES ";

            $.each(transactionData, function(i, obj) {
                var values = self._getMembersValue(obj, members);
                sql += "(";
                sql += self._arrayToString(values, ',');
                sql += ")";
                if (i < transactionData.length - 1) {
                    sql += ",";
                }

            });
            //console.log("_buildBatchInsertScript" + sql);
            return sql;

        },
        _buildInsertOrUpdateScript: function(tableName, columnName, transactionData) {
            var members = this._getAttributesList(transactionData);
            var values = this._getMembersValue(transactionData, members);
            if (members.length === 0) {
                throw 'buildInsertSQL : Error, try to insert an empty object in the table ' + tableName;
            }
            var sql = "INSERT OR REPLACE INTO " + tableName + " (ID, ";
            sql += this._arrayToString(members, ',');
            sql += ") VALUES (";
            sql += "(SELECT ID FROM " + tableName + "  WHERE  " + columnName + "=" +"'"+ transactionData[columnName] +"'"+ "),";
            sql += this._arrayToString(values, ',');
            sql += ")";
            //console.log("_buildInsertOrUpdateScript" + sql);
            return sql;

        },
		_buildUpdateScript : function(tableName, trasactionData) {
			var self = this;
			var sql = "UPDATE " + tableName + " SET ";
			var members = self._getAttributesList(trasactionData);
			if (members.length === 0) {
				throw 'buildUpdateSQL : Error, try to insert an empty object in the table '
						+ tableName;
			}
			var values = self._getMembersValue(trasactionData, members);

			var memLength = members.length;
			for (var i = 0; i < memLength; i++) {
				//console.log(members[i] + " = " + values[i]);
				sql += members[i] + " = " + values[i];
				if (i < memLength - 1) {
					sql += ', ';
				}
			}

			return sql;
		},
		_buildFetchScript : function(selectCol, tableName, dataObj) {
			var self = this;
			var sql = "SELECT " + selectCol + " FROM " + tableName;
			if (!jQuery.isEmptyObject(dataObj)) {
				sql += " WHERE ";
				var members = self._getAttributesList(dataObj);
				if (members.length === 0) {
					throw 'buildUpdateSQL : Error, try to insert an empty object in the table '
							+ tableName;
				}
				var values = self._getMembersValue(dataObj, members);
				var memLength = members.length;
				for (var i = 0; i < memLength; i++) {
					if (members[i] == "filter") {
						sql += dataObj.filter;
					} else if(angular.isArray(values[i])){
							sql += members[i] + " in (" ;
							for(var j=0;j<values[i].length;j++){
								sql +="'"+ values[i][j]+"'";
								 if(!(j+1 == values[i].length))
									sql += ",";
							}
						sql += ")";
					}else {
						sql += members[i] + " = " + values[i];
					}

					if (i < memLength - 1) {
						sql += " AND ";
					}
				}

			}
			return sql;
		},
		_buildDeleteScript : function(tableName, whereClause) {
			var self = this;
			var sql = 'DELETE FROM ' + tableName + ' WHERE ' + whereClause;
			return sql;
		},
		_getRecordsArray : function(results) {
			var resultsArray = [];
			if (results.rows.length == 0) {
				//console.log("No Results returned from DB");
			} else {
				for (var i = 0; i < results.rows.length; i++) {
					resultsArray.push(results.rows.item(i));
				}
			}
			return resultsArray;
		},
		_getMembersValue : function(obj, members) {
			var valueArray = [];
			for (var i = 0; i < members.length; i++) {
				if (members[i].toUpperCase() == "ID"  || angular.isArray(obj[members[i]]) ) { //|| members[i] == "TransTrackingID) {
					valueArray.push(obj[members[i]]);
				} else if (members[i].toUpperCase() == "DOCUMENTOBJECT") {
					valueArray
							.push("'" + JSON.stringify(obj[members[i]]) + "'");
				} else if (members[i] == "TransactionData") {
					valueArray.push("'" + escape(obj[members[i]]) + "'");
				} else {
					valueArray.push('"' + obj[members[i]] + '"');
				}
			}
			return valueArray;
		},
		_getAttributesList : function(obj) {
			var memberArray = [];
			for ( var elm in obj) {
				if (typeof this[elm] === 'function' && !obj.hasOwnProperty(elm)) {
					continue;
				}
				memberArray.push(elm);
			}
			return memberArray;
		},
		_arrayToString : function(array, separator) {
			var result = '';
			for (var i = 0; i < array.length; i++) {
				result += array[i];
				if (i < array.length - 1) {
					result += separator;
				}
			}
			return result;
		},
		_defaultCallBack : function(transaction, results) {
			//console.log('SQL Query executed. insertId: ' + results.insertId
					//+ ' rows.length ' + results.rows.length);
		},
		_errorHandler : function(transaction, error) {
			//console.log('Error : ' + error.message + ' (Code ' + error.code
					//+ ') Transaction.sql = ' + transaction.sql);
		}
	};
	return DbOperationsUtility;
})();
