//Implementation for Rule Execution
var RuleHelper;
(function() {
	RuleHelper = {
		run : function(transactionObj, successCallback, errorCallback, options,
				$http) {
			var saveUrl = rootConfig.serviceBaseUrl
					+ "lifeEngageService/generateIllustration";
			var requestInfo = Request();
			var canonicalModelInput = {};

			var validationFunctionName = transactionObj.TransactionData.validationRuleName;
			var illustrationFunctionName = transactionObj.TransactionData.illustrationRuleName;
			var databaseName = transactionObj.TransactionData.offlineDB;

			canonicalModelInput.ruleInput = convertToCanonicalModel(transactionObj.TransactionData);
			canonicalModelInput.illustrationRuleName = illustrationFunctionName;
			canonicalModelInput.validationRuleName = validationFunctionName;
			canonicalModelInput.offlineDB = databaseName;
			canonicalModelInput.illustrationRuleGroup = transactionObj.TransactionData.illustrationRuleGroup;
			canonicalModelInput.validationRuleGroup = transactionObj.TransactionData.validationRuleGroup;

			transactionObj.TransactionData = canonicalModelInput;
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObj);

			var request = {
				method : 'POST',
				url : saveUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request)
					.success(
							function(data, status, headers, config) {

								if (typeof data.Response.ResponsePayload.Transactions[0].TransactionData.ruleOutput.isSuccess != "undefined"
										&& data.Response.ResponsePayload.Transactions[0].TransactionData.ruleOutput.isSuccess == false) {
									errorCallback(data.Response.ResponsePayload.Transactions[0].TransactionData.ruleOutput);
								} else {
									successCallback(data.Response.ResponsePayload.Transactions[0].TransactionData.ruleOutput);
								}

							}).error(function(data, status, headers, config) {
						errorCallback(data, status);
					});
		},
		calculateGoal : function(transactionObj, goalObject, successCallback,
				errorCallback, options, $http) {

			var saveUrl = rootConfig.serviceBaseUrl
					+ "lifeEngageService/executeRule";
			var requestInfo = Request();
			var canonicalModelInput = {};
			var transactionObjRule = {};
			var RuleName = "";
			var FunctionName = "";
			RuleName = goalObject.ruleName;
			FunctionName = goalObject.functionName;

			canonicalModelInput.ruleInput = transactionObj;
			canonicalModelInput.ruleName = FunctionName;
			//Modifying the rule group to 11 for generali implementation
            //FNA changes made by LE Team
			canonicalModelInput.ruleGroup = "11";            
			transactionObjRule.TransactionData = canonicalModelInput;
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObjRule);

			var request = {
				method : 'POST',
				url : saveUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request).success(function(data, status, headers, config) {
				successCallback(data);
			}).error(function(data, status, headers, config) {
				errorCallback(data, status);
			});
		},
		calculateRisk : function(input, ruleSetObj, successCallback,
				errorCallback, options, $http) {
			var inputJson = JSON.parse(input);
			var saveUrl = rootConfig.serviceBaseUrl
					+ "lifeEngageService/executeRule";
			var requestInfo = Request();
			var canonicalModelInput = {};
			var transactionObjRule = {};
			var RuleName = "";
			var FunctionName = "";
			RuleName = ruleSetObj.ruleSetName;
			FunctionName = ruleSetObj.functionName;

			canonicalModelInput.ruleInput = inputJson;
			canonicalModelInput.ruleName = FunctionName;
			// Changing the rule group of riskcalculator to 14 for generali implementation
			canonicalModelInput.ruleGroup = "14";
			transactionObjRule.TransactionData = canonicalModelInput;
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObjRule);

			var request = {
				method : 'POST',
				url : saveUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request).success(function(data, status, headers, config) {
				successCallback(data);
			}).error(function(data, status, headers, config) {
				errorCallback(data, status);
			});
		},
		getRecommendedProducts : function(input, successCallback,
				errorCallback, options, $http) {
			var inputJson = JSON.parse(input);
			var saveUrl = rootConfig.serviceBaseUrl
					+ "lifeEngageService/executeRule";
			var requestInfo = Request();
			var canonicalModelInput = {};
			var transactionObjRule = {};
			var FunctionName = "LEProductSelector";
			canonicalModelInput.ruleInput = inputJson;
			canonicalModelInput.ruleName = FunctionName;
            //FNA changes made by LE Team
			//canonicalModelInput.ruleGroup = "16";
            canonicalModelInput.ruleGroup = "11";
			transactionObjRule.TransactionData = canonicalModelInput;
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObjRule);

			var request = {
				method : 'POST',
				url : saveUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request).success(function(data, status, headers, config) {
				successCallback(data);
			}).error(function(data, status, headers, config) {
				errorCallback(data, status);
			});
		},
		checkValidationFNAtoBI : function(transactionObj, successCallback,
				errorCallback, options, $http) {
			var inputJson = transactionObj;
			var saveUrl = rootConfig.serviceBaseUrl
					+ "lifeEngageService/executeRule";
			var requestInfo = Request();
			var canonicalModelInput = {};
			var transactionObjRule = {};
			var FunctionName = "LESampleValidation";
			canonicalModelInput.ruleInput = inputJson;
			canonicalModelInput.ruleName = FunctionName;
			/*canonicalModelInput.ruleGroup = "10010";*/
/*			FNA changes by LE Team >>> BUG - 3702  starts*/
			canonicalModelInput.ruleGroup = "11";
			/*FNA changes by LE Team >>> ends*/
			transactionObjRule.TransactionData = canonicalModelInput;
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObjRule);

			var request = {
				method : 'POST',
				url : saveUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request).success(function(data, status, headers, config) {
				if (data.LEStatus == "Pass") {
					successCallback(data);
				} else {
					errorCallback(data);
				}
			}).error(function(data, status, headers, config) {
				errorCallback(data, status);
			});
		}

	};
	return RuleHelper;
})();