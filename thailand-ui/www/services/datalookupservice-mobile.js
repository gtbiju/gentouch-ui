﻿/*
 * Copyright 2015, LifeEngage 
 */
var dataLookupServiceUtility;
(function () {
    dataLookupServiceUtility = {
        getLookUpData: function (transactionObj, successCallback, errorCallback, options) {
            transactionObj.carrierId = 1;
			if (localStorage["locale"]) {
				locale = localStorage["locale"].split("_")
				transactionObj.language = locale[0];
				transactionObj.country = locale[1];
			} else {
				transactionObj.language = "en";
				transactionObj.country = "US";
			}
            LEDataLookUpService.getLookUpData(transactionObj, successCallback, errorCallback);
        },

        getMultipleLookUpData : function(typeNameList, successCallback,
				errorCallback, options, $http) {        	
        	carrierId = 1;
        	LEDataLookUpService.getMultipleLookUpData(typeNameList, carrierId, successCallback, errorCallback);
        	
        }
    
	};
    return dataLookupServiceUtility;
})();