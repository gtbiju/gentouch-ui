/*
 * Copyright 2015, LifeEngage 
 */
var FileSelectUtility;
(function() {
	FileSelectUtility = {
		onFileOpenerExit : false,
		eAppFolderPath : "",
		filesFormated : [],
		savedDocFolderPaths : [],
		getApplicationPath : function() {
			var self = this;
			window.plugins.fileOpener.getApplicationPath(function(path) {
				//console.log(path);
				self.eAppFolderPath = path;
			}, function(e) {
				//console.log(e);
			});
		},
		/*To browse for a file to upload*/
		browseFile : function(document, agentId, successCallback, errorCallback) {
			var self = this;
			if (document.documentType == "Photograph") {
				index = 0;
			}
			navigator.camera.getPicture(onSuccess, onFail, {
				quality : 50,
				destinationType : navigator.camera.DestinationType.DATA_URL,
				sourceType : navigator.camera.PictureSourceType.PHOTOLIBRARY,
				mediaType : navigator.camera.MediaType.PICTURE
			});

			function onSuccess(imageData) {
				var d = new Date();
				var n = d.getTime();
				var entryName = "cdv_photo_001.jpg";
				var newFileName = agentId + n + "_" + entryName;
				if (document.documentType == "Photo") {
					index = 0;
					document.base64string = imageData;
					successCallback(document);
				}
				if (document.documentType != "Photo" && document.documentObject.pages[index].fileName) {
					document.documentObject.pages[index].fileName = newFileName;
					document.documentObject.pages[index].base64string = imageData;
					if (document.documentType == "Photograph") {
						document.documentObject.documentName = document.documentName;
					}
					document.documentObject.documentType = document.documentType;
					successCallback(document);
				} else if (document.documentType != "Photo") {
					document.documentObject.pages[index].base64string = imageData;
					document.documentObject.pages[index].fileName = newFileName;
					if (document.documentType == "Photograph") {
						document.documentObject.documentName = document.documentName;
					}
					document.documentObject.documentType = document.documentType;
					successCallback(document);
				}
			}

			function onFail(message) {
				//console.log('Failed because: ' + message);
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(message);
				} else {
					self._errorHandler(message);
				}
			}
		},
        /*To browse for a file to upload*/
		browsePicture : function(document, agentId, successCallback, errorCallback) {
			var self = this;
			if (document.documentType == "Photograph") {
				index = 0;
			}
			navigator.camera.getPicture(onSuccess, onFail, {
				quality : 50,
				destinationType : navigator.camera.DestinationType.DATA_URL,
				sourceType : navigator.camera.PictureSourceType.PHOTOLIBRARY,
				mediaType : navigator.camera.MediaType.PICTURE
			});

			function onSuccess(imageData) {
				var d = new Date();
				var n = d.getTime();
				var entryName = "cdv_photo_001.jpg";
				var newFileName = agentId + n + "_" + entryName;
				if (document.documentType == "Photo") {
					index = 0;
					document.base64string = imageData;
					successCallback(document);
				}
			}

			function onFail(message) {
				//console.log('Failed because: ' + message);
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(message);
				} else {
					self._errorHandler(message);
				}
			}
		},
		saveRequirementOnly : function(requirementObj, successCallback, errorCallback, options, $http) {
		    var saveReqUrl = rootConfig.serviceBaseUrl + "requirementFileUploadService/saveRequirement";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions
					.push(requirementObj)

			var request = {
				method : 'POST',
				url : saveReqUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request)
					.success(
							function(data, status, headers, config) {
								if (data.Response.ResponsePayload.Transactions[0].StatusData.StatusCode == "100") {
									successCallback(data.Response.ResponsePayload.Transactions[0]);
								} else {
									errorCallback(data.Response.ResponsePayload.Transactions[0].StatusData.StatusMessage);
								}
							}).error(function(data, status, headers, config) {
						errorCallback(data, status, resources.errorMessage);
					});
		},
		/*To capture a file using camera to upload*/
		captureFile : function(document, index, agentId, successCallback,
				errorCallback) {
			if (document.documentType == "Photograph") {
				index = 0;
			}
			var self = this;
			navigator.camera.getPicture(onSuccess, onFail, {
				quality : 50,
				destinationType : navigator.camera.DestinationType.DATA_URL,
				targetWidth : rootConfig.cameraTargetWidth,
				targetHeight : rootConfig.cameraTargetHeight,
				saveToPhotoAlbum : rootConfig.cameraSaveToPhotoAlbum
			});

			function onSuccess(imageData) {
				var d = new Date();
				var n = d.getTime();
				var entryName = "cdv_photo_001.jpg";
				var newFileName = agentId + n + "_" + entryName;
				if (document.documentType == "Photo") {
					index = 0;
					document.base64string = imageData;
					successCallback(document);
				} else {
					document.documentObject.pages[index].base64string = imageData;
					document.documentObject.pages[index].fileName = newFileName;
					if (document.documentType == "Photograph") {
						document.documentObject.documentName = document.documentName;
					}
					document.documentObject.documentType = document.documentType;
					successCallback(document);
				}
			}

			function onFail(message) {
				//console.log('Failed because: ' + message);
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(message);
				} else {
					self._errorHandler(message);
				}
			}
		},
		/*Move the captured image to the application folder*/
		_moveFile : function(URI, document, agentId, successCallback,
				errorCallback) {
			var self = this;
			window.resolveLocalFileSystemURI(URI, resolveOnSuccessMove,
					resOnError);

			function resolveOnSuccessMove(entry) {
				entry.file(function(fileObj) {
					//console.log("File Size" + fileObj.size);
				});
				var d = new Date();
				var n = d.getTime();
				var fileExtension = entry.name.substring(entry.name
						.lastIndexOf('.') + 1);
				var newFileName = agentId + n + "_" + entry.name;
				if ($.inArray(fileExtension, [ 'tif', 'jpeg', 'jpg', 'pdf' ]) == -1) {
					$rootScope.NotifyMessages(false, resources.docExtInvalid);
				} else {
					entry.file(function(fileObj) {
						if (fileObj.size > 2097152) {
							$rootScope.NotifyMessages(false,
									resources.docSizeExceeded);

						} else {
							var myFolderApp = self.eAppFolderPath
									+ "/eappAttachements";

							window.requestFileSystem(
									LocalFileSystem.PERSISTENT, 0, function(
											fileSys) {
										//The folder is created if doesn't exist
										fileSys.root.getDirectory(myFolderApp,
												{
													create : true,
													exclusive : false
												}, function(directory) {
													entry.moveTo(directory,
															newFileName,
															successMove,
															resOnError);
												}, resOnError);
									}, resOnError);
						}
					});

				}

			}

			function successMove(entry) {
				document.documentPath = entry.fullPath;
				document.documentName = entry.name;
				document.documentDisplayName = entry.name.substring(entry.name
						.indexOf('_') + 1);
				if (successCallback && typeof successCallback == "function") {
					successCallback(document);
				} else {
					self._defaultCallBack("File moved to the apps folder");
				}
			}

			function resOnError(e) {
				//console.log(e.code);
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(e);
				} else {
					self._errorHandler(message);
				}
			}
		},
		/*Copy the file from the device folder to the application folder*/
		_copyFile : function(URI, document, agentId, successCallback,
				errorCallback) {
			var self = this;
			window.resolveLocalFileSystemURI(URI, resolveOnSuccessCopy,
					resOnError);

			function resolveOnSuccessCopy(entry) {
				var d = new Date();
				var n = d.getTime();
				var fileExtension = entry.name.substring(entry.name
						.lastIndexOf('.') + 1);
				var newFileName = agentId + n + "_" + entry.name;
				if ($.inArray(fileExtension, [ 'tif', 'jpeg', 'jpg', 'pdf' ]) == -1) {
					$rootScope.NotifyMessages(false, resources.docExtInvalid);
				} else {
					entry.file(function(fileObj) {
						if (fileObj.size > 2097152) {
							$rootScope.NotifyMessages(false,
									resources.docSizeExceeded);

						} else {
							var myFolderApp = self.eAppFolderPath
									+ "/eappAttachements";

							window.requestFileSystem(
									LocalFileSystem.PERSISTENT, 0, function(
											fileSys) {
										//The folder is created if doesn't exist
										fileSys.root.getDirectory(myFolderApp,
												{
													create : true,
													exclusive : false
												}, function(directory) {
													entry.copyTo(directory,
															newFileName,
															successMove,
															resOnError);
												}, resOnError);
									}, resOnError);
						}
					});

				}
			}

			function successMove(entry) {
				document.documentPath = entry.fullPath;
				document.documentName = entry.name;
				document.documentDisplayName = entry.name.substring(entry.name
						.indexOf('_') + 1);
				if (successCallback && typeof successCallback == "function") {
					successCallback(document);
				} else {
					self._defaultCallBack("File copied to the apps folder");
				}
			}

			function resOnError(e) {
				//console.log(e.code);
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(e);
				} else {
					self._errorHandler(message);
				}
			}
		},

		copyUploadedFile : function(oldFileName,successCallback, errorCallback){
            window.plugins.LEFileUtils.getApplicationPath(function(path){
                oldFileName=oldFileName.replace(/^.*[\\\/]/, '');
                var fileDesc = [];
                var descObj = {};
                descObj.fileName = oldFileName;
                descObj.srcPath = path + '/Uploads';
                descObj.newName = new Date().getTime() + '_' + oldFileName;
                descObj.folderName = 'Uploads';
                fileDesc.push(descObj);

                window.plugins.LEFileUtils.copyFile(fileDesc, function(){
                    successCallback(path + '/Uploads/' + descObj.newName);
                }, errorCallback);
            });
        },

		/*To open the file in FileOpener plugin. We need to first copy the file to a Temp directory in the root folder where the File Opener can access*/
		openFile : function(filePath, fileName, successCallback, errorCallback) {
			var self = this;
			window.resolveLocalFileSystemURI(filePath, copyToTemp,
					errorTempCopy);

			function copyToTemp(entry) {
				var myFolderApp = "eappAttachements";
				window.requestFileSystem(LocalFileSystem.TEMPORARY,
						1024 * 1024, function(fileSys) {
							//The folder is created if doesn't exist
							fileSys.root.getDirectory(myFolderApp, {
								create : true,
								exclusive : false
							}, function(directory) {
								entry.copyTo(directory, entry.name,
										successTempCopy, errorTempCopy);
							}, errorTempCopy);
						}, errorTempCopy);

			}

			function successTempCopy(entry) {
				self.onFileOpenerExit = true;
				fileNameTemp = entry.fullPath;
				window.plugins.fileOpener.open(entry.fullPath, successCallback);
			}

			function errorTempCopy(e) {
				//console.log(e.code);
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(e);
				} else {
					self._errorHandler(e);
				}
			}
		},
		/*To delete the file from the saved folder*/
		deleteFile : function(doc, successCallback, errorCallback) {
			window.plugins.LEFileUtils.getApplicationPath(function(path){
			    var folderPath = path + '/Uploads';
			    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onSuccess,
                					errorHandler);
                function onSuccess(fs) {
                    fs.root.getDirectory(folderPath, {}, function(dirEntry) {
                        dirEntry.getFile(doc, {}, function(fileEntry) {
                            fileEntry.remove(function() {
                                if (successCallback
                                        && typeof successCallback == "function") {
                                    successCallback("File removed");
                                } else {
                                    self._defaultCallBack("File removed.");
                                }
                            }, errorHandler);
                        }, errorHandler)
                    }, errorHandler);
                }

                function errorHandler(e) {
                    //console.log(e.code);
                    if (errorCallback && typeof errorCallback == "function") {
                        errorCallback(e);
                    } else {
                        self._errorHandler(message);
                    }
                }
			});
		},
		/*When the user closes the viewer , the file temporary directory is deleted*/
		deleteTempFile : function(successCallback, errorCallback) {
			var self = this;
			if (self.onFileOpenerExit) {
				self.onFileOpenerExit = false;
				window.requestFileSystem(LocalFileSystem.TEMPORARY,
						1024 * 1024, onSuccess, errorHandler);
			}

			function onSuccess(fs) {
				fs.root
						.getDirectory(
								'eappAttachements',
								{},
								function(dirEntry) {
									dirEntry
											.removeRecursively(
													function() {
														if (successCallback
																&& typeof successCallback == "function") {
															successCallback();
														} else {
															self
																	._defaultCallBack("Temp Directory removed.");
														}
													}, errorHandler);

								}, errorHandler);
			}

			function errorHandler(e) {
				//console.log(e.code);
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(e);
				} else {
					self._errorHandler(e);
				}
			}
		},
		uploadDocuments : function(docData, count, successCallback,
				errorCallback, options, $http) {
			var self = this;
			var uploadUrl = rootConfig.serviceBaseUrl
					+ "documentService/uploadDoc";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions.push(docData);
			//console.log('docrequest: ' + angular.toJson(requestInfo));
			
			  var request = {
		   	    		 method: 'POST',
		   	    		 url: uploadUrl,
		   	    		 headers: {
		   	    		   'Content-Type': "application/json; charset=utf-8",
		   	    		   "Token": options.headers.Token   
		   	    		 },
		   	    		 data: requestInfo
		   	    		}

		   	    		$http(request).success(function(data, status, headers, config){
		   	    			if (successCallback
									&& typeof successCallback == "function") {
								successCallback(data.Response, count);
							} else {
								self._defaultCallBack("File data saved");
							}
		   	    			}).error(function(data, status, headers, config){
		   	    				//console.log('error' + JSON.stringify(data)
										//+ 'status' + status);
		   	    				errorCallback(data, status);
		   	    			});
		},
		startDownloadForEachTransaction : function(count, docDetails,
				successCallback, errorCallback, options, $http) {
			var downloadUrl = rootConfig.serviceBaseUrl
					+ "documentService/getDoc";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions.push(docDetails);
			//console.log('docrequest: ' + angular.toJson(requestInfo));
			
			  var request = {
		   	    		 method: 'POST',
		   	    		 url: downloadUrl,
		   	    		 headers: {
		   	    		   'Content-Type': "application/json; charset=utf-8",
		   	    		   "Token": options.headers.Token   
		   	    		 },
		   	    		 data: requestInfo
		   	    		}

		   	    		$http(request).success(function(data, status, headers, config){
		   	    			if (successCallback
									&& typeof successCallback == "function") {
								var documentData = {};
								documentData = data.Response.ResponsePayload.Transactions[0].TransactionData.Documents;
								//console.log(JSON.stringify(documentData));
								successCallback(documentData, count);
							} else {
								self._defaultCallBack("File data saved");
							}
		   	    			}).error(function(data, status, headers, config){
		   	    				//console.log('error' + JSON.stringify(data)
										//+ 'status' + status);
		   	    				errorCallback(data, status);
		   	    			});
		},
		writeFile : function(document, agentId, successCallback, errorCallback) {
			var self = this;
			var curDate = new Date();
			var curTime = curDate.getTime();
			var dataURI = document.fileObj;
			var docName = agentId + curTime + "_" + document.documentType
					+ ".txt";
			window.requestFileSystem(LocalFileSystem.TEMPORARY, 1024 * 1024,
					onFSSuccess, failure);

			function onFSSuccess(fs) {
				dataFolderPath = fs.root.fullPath;
				if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) //check for device
				{
					dataFolderPath = dataFolderPath + "/";
				} else if (/Android/i.test(navigator.userAgent)) //for android
				{
					dataFolderPath = self.eAppFolderPath + "/eappAttachements";
				}
				dataFilePath = dataFolderPath + "/" + docName;
				fs.root.getDirectory(dataFolderPath, {
					create : true,
					exclusive : false
				}, function(directory) {
					directory.getFile(docName, {
						create : true,
						exclusive : false
					}, gotFileEntryToWrite, failure);
				}, failure);
			}

			function gotFileEntryToWrite(fileEntry) {
				document.documentName = fileEntry.name;
				document.documentPath = fileEntry.fullPath;
				fileEntry.createWriter(gotFileWriter, failure);
			}

			function gotFileWriter(writer) {
				writer.onwrite = function(evt) {
					successCallback(document);
				};
				writer.write(dataURI);
			}

			function failure(e) {
				errorCallback(e);
			}
		},

		readFile : function(fileName, successCallback, errorCallback) {
			/*var self = this;
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
					onFSSuccess, failure);

			function onFSSuccess(fs) {
				dataFolderPath = fs.root.fullPath;
				if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) //check for device
				{
					dataFolderPath = dataFolderPath + "/";
				} else if (/Android/i.test(navigator.userAgent)) //for android
				{
					dataFolderPath = self.eAppFolderPath + "/eappAttachements/";
				}
				dataFolderPath = dataFolderPath + fileName;

				fs.root.getFile(dataFolderPath, null, gotFileEntryToRead,
						failure);
			}

			function gotFileEntryToRead(fileEntry) {
				fileEntry.file(gotFileToRead, failure);
			}

			function gotFileToRead(file) {
				readAsText(file); //has to use this one
			}

			function readAsText(file) {
				var reader = new FileReader();
				reader.onloadend = function(evt) {
					successCallback(evt.target.result);
				};
				reader.readAsText(file);
			}

			function failure(e) {
				errorCallback(e);
			}*/
			var fileDetails = [];
            var fileObj = {};
            fileObj.fileName = fileName;
            fileObj.destFolder = 'Uploads';
            fileDetails.push(fileObj);
            window.plugins.LEFileUtils.readFileAsBase64(fileDetails, successCallback, errorCallback);
		},

		_readFileContent : function(documents, docIndex, successCallback,
				errorCallback) {
			var self = this;
			var docLength = documents.length;
			var documentPath = documents[docIndex].DocumentPath;
			var documentName = documents[docIndex].DocumentName;

			var fileName = documentPath
					.substr(documentPath.lastIndexOf('/') + 1);
			var myFolderApp = self.eAppFolderPath + "/eappAttachements";
			var fileContent;
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onSuccess,
					errorHandler);

			function onSuccess(fs) {

				fs.root
						.getDirectory(
								myFolderApp,
								{},
								function(dirEntry) {
									dirEntry
											.getFile(
													fileName,
													{},
													function(fileEntry) {
														fileEntry
																.file(function(
																		file) {
																	var reader = new FileReader();
																	reader.onloadend = function(
																			e) {
																		fileContent = e.target.result;
																		if (docIndex == docLength - 1) {
																			self.filesFormated
																					.push({
																						fileName : documentName,
																						fileContent : fileContent
																					});
																			if (successCallback
																					&& typeof successCallback == "function") {
																				successCallback(self.filesFormated);
																			} else {
																				self
																						._defaultCallBack("File data saved");
																			}
																		} else {
																			self.filesFormated
																					.push({
																						fileName : documentName,
																						fileContent : fileContent
																					});
																			docIndex++;
																			self.filesFormated
																					.push({
																						fileName : documentName,
																						fileContent : fileContent
																					});
																			self
																					._readFileContent(
																							documents,
																							docIndex,
																							successCallback,
																							errorCallback);
																		}
																	};
																	reader
																			.readAsDataURL(file);
																});
													});
								});
			}

			function getFileContant(file, callback) {
				var reader = new FileReader();
				reader.onload = function(e) {
					if (callback && typeof callback == "function") {
						callback(e.target.result);
					} else {
						self._defaultCallBack("File data saved");
					}
				};
				reader.readAsDataURL(file);
			}

			function errorHandler(e) {
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(e);
				} else {
					self._errorHandler(message);
				}
			}
		},
		copyFile : function(flag,successCallback, errorCallback){
		    var path = '';
		    if (flag == 'camera') {
				navigator.camera
						.getPicture(
								onSuccess,
								onFail,
								{
									quality : rootConfig.cameraQuality,
									destinationType : navigator.camera.DestinationType.FILE_URI,
									sourceType : navigator.camera.PictureSourceType.CAMERA,
									targetWidth : rootConfig.cameraTargetWidth,
                                    targetHeight : rootConfig.cameraTargetHeight,
									mediaType : navigator.camera.MediaType.PICTURE,
									saveToPhotoAlbum : rootConfig.cameraSaveToPhotoAlbum
								});
			} else if (flag == 'photolibrary') {
					if(navigator.userAgent.indexOf("Android") > 0 ) {
              					window.plugins.mfilechooser.open(['.pdf','.png','.bmp','.jpeg','.jpg','.doc','.docx'],
              						function (uri) {
              						onSuccess(uri);
              					 },onFail);
              				} else {
             					navigator.camera
             						.getPicture(
             								onSuccess,
             								onFail,
             								{
             									quality : 50,
             									destinationType : navigator.userAgent.indexOf("Android") > 0 ? navigator.camera.DestinationType.FILE_URI:navigator.camera.DestinationType.NATIVE_URI ,
             									sourceType : navigator.camera.PictureSourceType.PHOTOLIBRARY,
             									mediaType : navigator.camera.MediaType.PICTURE
             								});

                                    // window.plugins.mfilechooser.open(['.pdf','.png','.bmp','.jpeg','.jpg','.doc','.docx'],
                                    //     function (uri) {
                                    //     onSuccess(uri);
                                    //  },onFail);
              				}
              			}
              			function onSuccess(imageURI) {
              				if (navigator.userAgent.indexOf("Android") > 0) {
              					console.log('imageURI***'+imageURI);
              					if (flag == 'photolibrary') {
              						imageURI = "file://"+imageURI;
              					}
              					console.log('imageURI***After resolveNativePath *** '+imageURI);
              					window.resolveLocalFileSystemURI(imageURI,
              							resolveOnSuccess, onFail);
              				} else {
              					window.resolveLocalFileSystemURI(imageURI,
              							resolveOnSuccess, onFail);
              				}
              			}

//            function onSuccess(imageURI){
//                //window.resolveLocalFileSystemURI(imageURI, resolveOnSuccess, onFail);
//				if(navigator.userAgent.indexOf("Android") > 0){
//                    window.FilePath.resolveNativePath(imageURI,function(filePath){
//                        imageURI = filePath;
//                        window.resolveLocalFileSystemURI(imageURI, resolveOnSuccess, onFail);
//                    }, errorCallback);
//                }
//                else {
//                    window.resolveLocalFileSystemURI(imageURI, resolveOnSuccess, onFail);
//                }
//            }

            function resolveOnSuccess(entry) {
				var fileSize = "";
				entry.file(function(fileObj) {
					fileSize = fileObj.size;
					var fileUri = '';
					var fileDesc = [];
					var path = decodeURIComponent(entry.nativeURL);
					var descObj = {};
					descObj.fileName = entry.name;
					var temp3 = decodeURIComponent(entry.nativeURL).replace('file://', '');
					var temp = temp3.replace('content:/', '');
					var temp1 = '/' + entry.name;
					var temp2 = temp.replace(temp1, '');
					var descObj = {};
					descObj.fileName = entry.name;
					descObj.srcPath = temp2;
					descObj.folderName = 'Uploads';
					fileDesc.push(descObj);
					 var _copyFile;
				   if (entry.nativeURL.indexOf("assets-library://") < 0) {
				   descObj.fileName = entry.name;
				   _copyFile=function()
				   {
						window.plugins.LEFileUtils.copyFile(fileDesc,
							function() {
								  window.plugins.LEFileUtils.getApplicationPath(function(path) {
								  fileUri = path + '/Uploads/'+ descObj.newName;
								  successCallback(fileUri);
				   });
				   }, errorCallback);
				   };
				   }
				   else
				   {
				   descObj.fileName = entry.name.substring(0,entry.name.indexOf('?'));
				   _copyFile=function()
				   {
				   function _onSuccess(fs) {
				   fs.root.getDirectory("Uploads/",{create:true,exclusive:false},function(dir)
										{
										entry.copyTo(dir,descObj.newName,function(){
													 fileUri = dir.nativeURL+ '/' + descObj.newName;
													 successCallback(fileUri);
													 },
													 function(){}
													 );
										},errorCallback);
				   }
				   window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, _onSuccess,
											errorCallback);
				   };
				   }
				    //var fileExtension = descObj.fileName.substring(entry.name.lastIndexOf('.') + 1);
					var fileExtension = descObj.fileName.substring(entry.name.lastIndexOf('.') + 1).toLowerCase();
					descObj.newName = new Date().getTime() + '_'+ descObj.fileName;
					if ($.inArray(fileExtension, [ 'jpg','jpeg','pdf', 'png', 'PNG',
							'bmp','doc','docx' ]) == -1) {
						var errorMsg = "ext";
						errorCallback(errorMsg);
					} else if (fileSize < 2097152) {
						 _copyFile();
					} else {
						var errorMsg = "size";
						errorCallback(errorMsg);
					}
				});
			}

            function onFail(error){
                //console.log('Something went wrong ' + error);
                errorCallback(error);
            }
		},
		copyUploadedFile : function(oldFileName,successCallback, errorCallback){
            window.plugins.LEFileUtils.getApplicationPath(function(path){
                oldFileName=oldFileName.replace(/^.*[\\\/]/, '');
                var fileDesc = [];
                var descObj = {};
                descObj.fileName = oldFileName;
                descObj.srcPath = path + '/Uploads';
                descObj.newName = new Date().getTime() + '_' + oldFileName;
                descObj.folderName = 'Uploads';
                fileDesc.push(descObj);

                window.plugins.LEFileUtils.copyFile(fileDesc, function(){
                    successCallback(path + '/Uploads/' + descObj.newName);
                }, errorCallback);
            });
        },

		base64AsFile : function(base64String,successCallback, errorCallback){
		    var fileDetails = [];
		    var fileObj = {};
		    var currentDate = new Date();
            var timeStamp = currentDate.getTime();
            var imageBase64= base64String.replace(/^data:image\/(png|jpeg);base64,/, "");
		    fileObj.fileName = 'Signature' + timeStamp + '.jpg';
		    fileObj.destFolder = 'Uploads';
		    fileObj.base64String = imageBase64;
		    fileDetails.push(fileObj);

            window.plugins.LEFileUtils.writeBase64AsFile(fileDetails, function(){
                window.plugins.LEFileUtils.getApplicationPath(function(path){
                    fileUri = path + '/Uploads/' + fileObj.fileName;
                    successCallback(fileUri);
                });
            }, errorCallback);
		},
		/*Default success callback if no callback specified*/
		_defaultCallBack : function(message) {
			//console.log(message);
		},
		/*Default failure callback if the error callback is not specified*/
		_errorHandler : function(error) {
			//console.log('Error : ' + error.message + ' (Code ' + error.code
					//+ ')');
		}
	};
	return FileSelectUtility;
})();
document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
	FileSelectUtility.getApplicationPath();
}