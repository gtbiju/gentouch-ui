﻿/*
 * Copyright 2015, LifeEngage 
 */
//Implementation for Rule Execution
var RuleHelperGLI;
(function() {
	RuleHelperGLI = {
		runCustom : function(transactionObj, successCallback, errorCallback, options, ruleInfo, container) {

			var requestInfo = Request();
			var canonicalModelInput = {};

			var ruleName = ruleInfo.ruleName
			+ '.js';
			var functionName = ruleInfo.ruleName;
			var databaseName = ruleInfo.offlineDB + '.db';

			
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObj);
			jQuery.support.cors = true;
			try {
				
				// Call rule engine and execute rule
				var inputJson = '{ "data": {"OfflineDbName": "' + databaseName
						+ '", "RuleSetId": "1","RuleSetName": "'
						+ ruleName + '", "FunctionName": "'
						+ functionName + '","variables": '
						+ JSON.stringify(transactionObj.TransactionData)
						+ '} }';
				LERuleEngine.execute(inputJson,	function(output) {
					var result = $.parseJSON(output);
					if(container == "validate"){
						if (result.isSuccess) {
							successCallback($.parseJSON(output));
						} else {
							errorCallback($.parseJSON(output));
						}
					} else {
						successCallback($.parseJSON(output));
					}
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		runCommonFunctionRule : function(transactionObj, successCallback, errorCallback, options, fileName,functionName,value,riderName,index,fieldTobePopulated,callLoadSummary) {

			var fileName=fileName+'.js';
			var databaseName='code-lookup.db';
			var validationFunctionName=functionName;
			var riderName=riderName;
			var riderIndexnew=index;
			var field = fieldTobePopulated;
			
			
			try {
				
				var inputJson = '{ "data": {"OfflineDbName": "' + databaseName
				+ '", "RuleSetId": "1","RuleSetName": "'
				+ fileName + '", "FunctionName": "'
				+ validationFunctionName + '","variables": "'
				+ value
				+ '"} }';
				if(typeof value =="object"){
					var inputJson = '{ "data": {"OfflineDbName": "' + databaseName
				+ '", "RuleSetId": "1","RuleSetName": "'
				+ fileName + '", "FunctionName": "'
				+ validationFunctionName + '","variables": '
				+ JSON.stringify(value)
				+ '} }';
				}
				LERuleEngine
				.execute(
						inputJson,
						function(output) {
							var result = $.parseJSON(output);
							if(result != undefined){
								successCallback($
										.parseJSON(output));
							}
							else{
								errorCallback($
										.parseJSON(output));
							}
						});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		runInsuranceEngagement :function(transactionObj,ruleInfo, successCallback, errorCallback,options){
			var requestInfo = Request();
			var canonicalModelInput = {};

			var ruleName = ruleInfo.ruleName
			+ '.js';
			var functionName = ruleInfo.ruleName;
			var databaseName = ruleInfo.offlineDB + '.db';

			
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObj);
			jQuery.support.cors = true;
			try {
				
				// Call rule engine and execute rule
				var inputJson = '{ "data": {"OfflineDbName": "' + databaseName
						+ '", "RuleSetId": "1","RuleSetName": "'
						+ ruleName + '", "FunctionName": "'
						+ functionName + '","variables": '
						+ JSON.stringify(transactionObj)
						+ '} }';
				LERuleEngine
						.execute(
								inputJson,
								function(output) {
									var result = $.parseJSON(output);
										successCallback($
												.parseJSON(output));
									
								});
			} catch (exception) {
				errorCallback(exception);
			}
		}
		}
})();