var PlatformInfoUtility;

(function() {
	PlatformInfoUtility = {
			getPlatformInfo : function(successCallback, errorCallback) {
				var self = this;
				
								try {			
											var appVersion = navigator.appVersion;
											var userAgent = navigator.userAgent;
											var browserName  = navigator.appName;
											var language = navigator.language || navigator.userLanguage;
											var platform = navigator.platform;
											var fullVersion  = ''+parseFloat(navigator.appVersion); 											
											var nameOffset,verOffset,ix;

										if ((verOffset=userAgent.indexOf("Opera"))!=-1) {
											 browserName = "Opera";
											 fullVersion = userAgent.substring(verOffset+6);
											 if ((verOffset=userAgent.indexOf("Version"))!=-1) 
											   fullVersion = userAgent.substring(verOffset+8);
											}
											else if ((verOffset=userAgent.indexOf("MSIE"))!=-1) {
											 browserName = "Microsoft Internet Explorer";
											 fullVersion = userAgent.substring(verOffset+5);
											}
											else if ((verOffset=userAgent.indexOf("Chrome"))!=-1) {
											 browserName = "Chrome";
											 fullVersion = userAgent.substring(verOffset+7);
											}
											else if ((verOffset=userAgent.indexOf("Safari"))!=-1) {
											 browserName = "Safari";
											 fullVersion = userAgent.substring(verOffset+7);
											 if ((verOffset=userAgent.indexOf("Version"))!=-1) 
											   fullVersion = userAgent.substring(verOffset+8);
											}
											else if ((verOffset=userAgent.indexOf("Firefox"))!=-1) {
											 browserName = "Firefox";
											 fullVersion = userAgent.substring(verOffset+8);
											}
											else if ( (nameOffset=userAgent.lastIndexOf(' ')+1) < 
													  (verOffset=userAgent.lastIndexOf('/')) ) 
											{
											 browserName = userAgent.substring(nameOffset,verOffset);
											 fullVersion = userAgent.substring(verOffset+1);
											 if (browserName.toLowerCase()==browserName.toUpperCase()) {
											  browserName = navigator.appName;
											 }
											}
											if ((ix=fullVersion.indexOf(";"))!=-1)
											   fullVersion=fullVersion.substring(0,ix);
											if ((ix=fullVersion.indexOf(" "))!=-1)
											   fullVersion=fullVersion.substring(0,ix);
											
									var browserInfo= {
													"browserName"	: browserName,													
													"browserVersion" : fullVersion,
													"language"	: language,
													"platform" : platform
													};
																		
									successCallback(browserInfo);
									
									}
									catch (exception) 
									{
										if (errorCallback && typeof errorCallback == "function") {
											errorCallback(exception);
										} else {
											self._errorHandler(exception);
										}
									}

		},
		
		/*Default failure callback if the error callback is not specified*/
		_errorHandler : function(error) {
			console.log('Error : ' + error.message + ' (Code ' + error.code
					+ ')');
		}

	};

	return PlatformInfoUtility;
})();

