var RequirementUtility;
(function() {
    RequirementUtility = {
            eAppFolderPath : "Uploads",
            getApplicationPath : function() {
                var self = this;
                window.plugins.fileOpener.getApplicationPath(function(path) {
                    console.log(path);
                    self.eAppFolderPath = path;
                }, function(e) {
                    console.log(e);
                });
    },
    uploadDocumentFile : function(transactionObj, successCallback,
            errorCallback, options, $http) {
        var self = this;
        var uploadUrl = rootConfig.serviceBaseUrl
                + "requirementFileUploadService/uploadRequirementDocFile";
        var requestInfo = Request();
        requestInfo.Request.RequestPayload.Transactions.push(transactionObj);
        //console.log('requirementFileUploadRequest: ' + angular.toJson(requestInfo));
        
          var request = {
                     method: 'POST',
                     url: uploadUrl,
                     headers: {
                       'Content-Type': "application/json; charset=utf-8",
                       "Token": options.headers.Token   
                     },
                     data: requestInfo
                    }
                    //console.log(request);
                    $http(request).success(function(data, status, headers, config){
                        if (successCallback
                                && typeof successCallback == "function") {
                            successCallback(data.Response);
                        } else {
                            self._defaultCallBack("File data saved");
                        }
                        }).error(function(data, status, headers, config){
                            console.log('error' + JSON.stringify(data)
                                    + 'status' + status);
                            errorCallback(data, status);
                        });
    },
    
        readFile : function(curReqFileToSync, successCallback, errorCallback) {
            if(curReqFileToSync.status == 'Deleted') {
                successCallback("");
            } else {
                var finalPath =  curReqFileToSync.base64string.replace('../',"");
                var self = this;
                try{
                    var fs = require("fs");
                    var base64Image = fs.readFileSync(finalPath, "base64");
                    //console.log(base64Image);
                    successCallback(base64Image);                       
                }catch(err){
                    errorCallback();
                }
            }
        },
    
        downLoadFile : function(transactionObj,successCallback, errorCallback, options, $http) {
            
            var self = this;
            var downloadUrl = rootConfig.serviceBaseUrl
                    + "requirementFileUploadService/getRequirementDocFile";
            var requestInfo = Request();
            requestInfo.Request.RequestPayload.Transactions.push(transactionObj);
            //console.log('getRequirementDocFileRequest: ' + angular.toJson(requestInfo));
            
              var request = {
                         method: 'POST',
                         url: downloadUrl,
                         headers: {
                           'Content-Type': "application/json; charset=utf-8",
                           "Token": options.headers.Token   
                         },
                         data: requestInfo
                        }

                        $http(request).success(function(data, status, headers, config){
                            if (successCallback
                                    && typeof successCallback == "function") {
                                successCallback(data.Response);
                            } else {
                                self._defaultCallBack("File data retrieved");
                            }
                            }).error(function(data, status, headers, config){
                                console.log('error' + JSON.stringify(data)
                                        + 'status' + status);
                                errorCallback(data, status);
                            });
            
        },
        
        writeFile : function(fileObj, successCallback, errorCallback) {
            var fs = require("fs");
            var base64Data = fileObj.base64string;
            //'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAZABkAAD/4Q3zaHR0cDovL25zLmFkb2JlLmN...';

            var imageBuffer = RequirementUtility.decodeBase64Image(base64Data);             
            var folderName = "Uploads";
            if(!fs.existsSync(folderName)){
              fs.mkdirSync(folderName);
            }
            var path = folderName +"/"+ fileObj.fileName;
            fs.writeFile(path, imageBuffer.data, function(err) {  });
            successCallback('../'+path);
        },

        decodeBase64Image : function(dataString) {
          var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
            response = {};

          if (matches.length !== 3) {
            return new Error('Invalid input string');
          }

          response.type = matches[1];
          response.data = new Buffer(matches[2], 'base64');

          return response;
        },

             
        
    /* Default success callback if no callback specified */
    _defaultCallBack : function(message) {
        console.log(message);
    },
    /* Default failure callback if the error callback is not specified */
    _errorHandler : function(error) {
        console.log('Error : ' + error.message + ' (Code ' + error.code
                + ')');
    }     
    };
return RequirementUtility;
})();