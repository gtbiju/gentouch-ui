/*
 * Copyright 2015, LifeEngage 
 */
var FileSelectUtility;
(function() {
	FileSelectUtility = {
		browseFile : function(data, agentId, successCallback, errorCallback) {
				var self = this;
				try {
					var file = data;
					reader = new FileReader();
					reader.onload = function(oFREvent) {
						file.documentPath = oFREvent.target.result;
						if (successCallback && typeof successCallback == "function") {
							successCallback(file);
						} else {
							self._defaultCallBack("File data saved");
						}
					};
					var currDate = new Date();
					var currTime = currDate.getTime();
					if (file.fileObj) {
						file.documentName = agentId + currTime + "_"
								+ file.fileObj.name;
						file.documentDisplayName = file.fileObj.name;
					} else {
						file.documentName = agentId + currTime + "_"
								+ file.documentObject.name;
						file.documentDisplayName = file.documentObject.name;
					}
					var ext = file.documentName.split('.').pop().toLowerCase();
					if ($.inArray(ext, ['jpg','pdf','png','PNG','bmp','doc','docx']) == -1) {
					  var errorMsg ="ext";
					errorCallback(errorMsg);

					} else {
						if (file.fileObj) {
							if (file.fileObj.size > 2097152) {
							 var errorMsg ="size";
								errorCallback(errorMsg);
							} else {
								reader.readAsDataURL(file.fileObj);
							}
						} else {
							if (file.documentObject.size > 2097152) {
								 var errorMsg ="size";
								errorCallback(errorMsg);
							} else {
								reader.readAsDataURL(file.documentObject);
							}
						}
					}
				} catch (e) {
					if (errorCallback && typeof errorCallback == "function") {
						errorCallback(e);
					} else {
						self._errorHandler(e);
					}
				}
		},
		/* To capture a file using camera to upload */
		captureFile : function(document, index, agentId, successCallback,
				errorCallback) {
			if (document.documentType == "Photograph") {
				index = 0;
			}
			var self = this;
			navigator.camera.getPicture(onSuccess, onFail, {
				quality : 50,
				destinationType : navigator.camera.DestinationType.DATA_URL,
				targetWidth : rootConfig.cameraTargetWidth,
				targetHeight : rootConfig.cameraTargetHeight,
				saveToPhotoAlbum : rootConfig.cameraSaveToPhotoAlbum
			});

			function onSuccess(imageData) {
				var d = new Date();
				var n = d.getTime();
				var entryName = "cdv_photo_001.jpg";
				var newFileName = agentId + n + "_" + entryName;
				if (document.documentType == "Photo") {
					index = 0;
					document.base64string = imageData;
					successCallback(document);
				} else {
					document.documentObject.pages[index].base64string = imageData;
					document.documentObject.pages[index].fileName = newFileName;
					if (document.documentType == "Photograph") {
						document.documentObject.documentName = document.documentName;
					}
					document.documentObject.documentType = document.documentType;
					successCallback(document);
				}
			}

			function onFail(message) {
				//console.log('Failed because: ' + message);
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(message);
				} else {
					self._errorHandler(message);
				}
			}
		},

		downloadDocument : function(docName, proposalId, successCallback,
				errorCallback, options, $http) {
			try {
				var self = this;
				var proposalId = proposalId;
				var docName = docName;
				var uploadUrl = rootConfig.serviceBaseUrl
						+ "documentService/file/?fileName=" + docName
						+ "&proposalNumber=" + proposalId;
				
				var request = {
		   	    		 method: 'GET',
		   	    		 url: uploadUrl,
		   	    		 headers: {
		   	    		   "Token": options.headers.Token   
		   	    		 },
		   	    		 data: ""
		   	    		}

		   	    		$http(request).success(function(data, status, headers, config){
		   	    			if (successCallback
									&& typeof successCallback == "function") {
								successCallback();
							} else {
								self._defaultCallBack("Files downloaded");
							}
		   	    			}).error(function(data, status, headers, config){
		   	    				if (errorCallback
										&& typeof errorCallback == "function") {
									errorCallback(data, status);
								} else {
									self._errorHandler(status);
								}
		   	    			});
				
			} catch (e) {
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(e);
				} else {
					self._errorHandler(e);
				}
			}
		},
		uploadDocuments : function(data, count, successCallback, errorCallback, options, $http) {
			var self = this;

			var uploadUrl = rootConfig.serviceBaseUrl
					+ "documentService/uploadDoc";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions.push(data);
			//console.log('docrequest: ' + angular.toJson(requestInfo));
			
			var request = {
	   	    		 method: 'POST',
	   	    		 url: uploadUrl,
	   	    		 headers: {
	   	    		   'Content-Type': "application/json; charset=utf-8",
	   	    		   "Token": options.headers.Token   
	   	    		 },
	   	    		 data: requestInfo
	   	    		}

	   	    		$http(request).success(function(data, status, headers, config){
	   	    			if (successCallback
								&& typeof successCallback == "function") {
							successCallback(data.Response);
						} else {
							self._defaultCallBack("File data saved");
						}
	   	    			}).error(function(data, status, headers, config){
	   	    				//console.log('error' + JSON.stringify(data)
									//+ 'status' + status);
	   	    				errorCallback(data, status, resources.errorMessage);
	   	    			});

		},
		startDownloadForEachTransaction : function(count, docDetails, successCallback,
				errorCallback, options, $http) {
			var downloadUrl = rootConfig.serviceBaseUrl
					+ "documentService/getDoc";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions.push(docDetails);
			//console.log('docrequest: ' + angular.toJson(requestInfo));
			
			var request = {
	   	    		 method: 'POST',
	   	    		 url: downloadUrl,
	   	    		 headers: {
	   	    		   'Content-Type': "application/json; charset=utf-8",
	   	    		   "Token": options.headers.Token   
	   	    		 },
	   	    		 data: requestInfo
	   	    		}

	   	    		$http(request).success(function(data, status, headers, config){
	   	    			if (successCallback
								&& typeof successCallback == "function") {
							var documentData = {};
							documentData = data.Response.ResponsePayload.Transactions[0].TransactionData.Documents;
							successCallback(documentData);
						} else {
							self._defaultCallBack("File data saved");
						}
	   	    			}).error(function(data, status, headers, config){
	   	    				//console.log('error' + JSON.stringify(data)
									//+ 'status' + status);
	   	    				errorCallback(data, status);
	   	    			});
		},
		
		saveRequirementOnly : function(requirementObj, successCallback, errorCallback, options, $http) {
		    var saveReqUrl = rootConfig.serviceBaseUrl + "requirementFileUploadService/saveRequirement";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions
					.push(requirementObj)

			var request = {
				method : 'POST',
				url : saveReqUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request)
					.success(
							function(data, status, headers, config) {
								if (data.Response.ResponsePayload.Transactions[0].StatusData.StatusCode == "100") {
									successCallback(data.Response.ResponsePayload.Transactions[0]);
								} else {
									errorCallback(data.Response.ResponsePayload.Transactions[0].StatusData.StatusMessage);
								}
							}).error(function(data, status, headers, config) {
						errorCallback(data, status, resources.errorMessage);
					});
		},

		deleteFile : function(doc, successCallback, errorCallback, options, $http) {
			var self = this;
			try {
				var filesFormated = [];
				var proposalId = doc.proposalId;
				var docName = doc.document.documentName;
				var uploadUrl = rootConfig.serviceBaseUrl
						+ "documentService/deleteFile";
				var docData = {
					proposalNumber : proposalId,
					fileName : docName
				};
				
				  var request = {
			   	    		 method: 'POST',
			   	    		 url: uploadUrl,
			   	    		 headers: {
			   	    		   'Content-Type': "application/json; charset=utf-8",
			   	    		   "Token": options.headers.Token   
			   	    		 },
			   	    		 data: docData
			   	    		}

			   	    		$http(request).success(function(data, status, headers, config){
			   	    			if (successCallback
										&& typeof successCallback == "function") {
									successCallback(doc);
								} else {
									self._defaultCallBack("Files deleted");
								}
			   	    			}).error(function(data, status, headers, config){
			   	    				if (errorCallback
											&& typeof errorCallback == "function") {
										errorCallback(data, status);
									} else {
										self._errorHandler(status);
									}
			   	    			});
				  
			} catch (e) {
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(e);
				} else {
					self._errorHandler(e);
				}
			}
		},
		/*Default success callback if no callback specified*/
		_defaultCallBack : function(message) {
		},
		/*Default failure callback if the error callback is not specified*/
		_errorHandler : function(error) {
		}
	};
	return FileSelectUtility;
})();