var FileSelectUtility;
(function() {
	FileSelectUtility = {
		onFileOpenerExit : false,
		eAppFolderPath : "",
		filesFormated : [],
		savedDocFolderPaths : [],
		getApplicationPath : function() {
			var self = this;
			window.plugins.fileOpener.getApplicationPath(function(path) {
				console.log(path);
				self.eAppFolderPath = path;
			}, function(e) {
				console.log(e);
			});
		},
		
		copyFile : function(path,successCallback, errorCallback,data) {
		try {
		  var fs = require('fs');
		  var folderName = "Uploads";
		  var currentDate = new Date();
		 var timeStamp = currentDate.getTime();
		 if(!fs.existsSync(folderName)){
		  fs.mkdirSync(folderName);
		 }
		var inStr = fs.createReadStream(path);
		  var filename = path.replace(/^.*[\\\/]/, '')
		  var ext = filename.split('.').pop().toLowerCase();
		  if($.inArray(ext, ['jpg','pdf','png','PNG','bmp','doc','docx']) == -1){
		  var errorMsg ="ext";
		  errorCallback(errorMsg);
		  
		  }else if(data.size < 2097152){
		    var outStr = fs.createWriteStream('Uploads/'+timeStamp+'_' +filename);
			inStr.pipe(outStr);
			if (successCallback && typeof successCallback == "function") {
					
					successCallback(outStr);
				} else {
					errorCallback();
				}
		  
		  }else{
		   var errorMsg ="size";
		    errorCallback(errorMsg);
		  }
		  
			}catch (e) {
			
			}
			},
		
		copyUploadedFile : function(oldFileName,successCallback, errorCallback) {
		try{
			var fs = require('fs');
			var currentDate = new Date();
			var timeStamp = currentDate.getTime();
			oldFileName=oldFileName.replace(/^.*[\\\/]/, '');
			
			if(fs.existsSync('Uploads')){
			var inStr = fs.createReadStream('Uploads/'+oldFileName);
			var outStr = fs.createWriteStream('Uploads/'+timeStamp+'_' +oldFileName);
			inStr.pipe(outStr);
			
			if (successCallback && typeof successCallback == "function") {
					
					successCallback(outStr);
				} else {
					errorCallback();
				}
			
			}
		
		
		}catch(e){
		
		
		}
		
		
		},

		saveRequirementOnly : function(requirementObj, successCallback, errorCallback, options, $http) {
		    var saveReqUrl = rootConfig.serviceBaseUrl + "requirementFileUploadService/saveRequirement";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions
					.push(requirementObj)

			var request = {
				method : 'POST',
				url : saveReqUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request)
					.success(
							function(data, status, headers, config) {
								if (data.Response.ResponsePayload.Transactions[0].StatusData.StatusCode == "100") {
									successCallback(data.Response.ResponsePayload.Transactions[0]);
								} else {
									errorCallback(data.Response.ResponsePayload.Transactions[0].StatusData.StatusMessage);
								}
							}).error(function(data, status, headers, config) {
						errorCallback(data, status, resources.errorMessage);
					});
		},
		/* To browse for a file to upload */
			base64AsFile : function(base64String,successCallback, errorCallback) {
		var fs = require('fs');
		var imageBuffer = FileSelectUtility.decodeBase64Image(base64String);
		var currentDate = new Date();
		var timeStamp = currentDate.getTime();
		var folderName = "Uploads";
		  
		if (rootConfig && rootConfig.configAndContentPath) {
			documentsPath = rootConfig.configAndContentPath + "/";
		}
		
		if (!fs.existsSync(documentsPath + folderName)) {
		  //if(!fs.existsSync(folderName)){
		  fs.mkdirSync(documentsPath + folderName);
		 }		  
		  
		var path=documentsPath + folderName+'/Signature'+timeStamp+'.jpg'
		var fs = require('fs');
		fs.writeFile(path, imageBuffer.data, function(err) {  });
		successCallback(path);
		},
		decodeBase64Image: function(dataString) {
		 var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
					response = {};

				  if (matches.length !== 3) {
					return new Error('Invalid input string');
				  }

				  response.type = matches[1];
				  response.data = new Buffer(matches[2], 'base64');

				  return response;
		
		
		},
		
		browseFiles : function(data, agentId, successCallback, errorCallback) {
			var self = this;
			try {
				var file = data;
				reader = new FileReader();
				reader.onload = function(oFREvent) {
					file.documentPath = oFREvent.target.result;
					if (successCallback && typeof successCallback == "function") {
						successCallback(file);
					} else {
						self._defaultCallBack("File data saved");
					}
				};
				var currDate = new Date();
				var currTime = currDate.getTime();
				file.documentName = agentId + currTime + "_"
						+ file.documentObject.name;
				file.documentDisplayName = file.documentObject.name;
				var ext = file.documentName.split('.').pop().toLowerCase();
				if ($.inArray(ext, [ 'tif', 'jpeg', 'jpg', 'pdf' ]) == -1) {
					$rootScope.NotifyMessages(false, resources.docExtInvalid);
				} else {
					if (file.documentObject.size > 2097152) {
						$rootScope.NotifyMessages(false,
								resources.docSizeExceeded);

					} else {
						reader.readAsDataURL(file.documentObject);

					}
				}
			} catch (e) {
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(e);
				} else {
					self._errorHandler(e);
				}
			}
		},
		
	      /* To browse for a file to upload */
        browseFile : function(data, agentId, successCallback, errorCallback) {
            var self = this;
            try {
                var file = data;
                reader = new FileReader();
                reader.onload = function(oFREvent) {
                    file.documentPath = oFREvent.target.result;
                    if (successCallback && typeof successCallback == "function") {
                        successCallback(file);
                    } else {
                        self._defaultCallBack("File data saved");
                    }
                };
                var currDate = new Date();
                var currTime = currDate.getTime();
                file.documentName = agentId + currTime + "_"
                        + file.documentObject.name;
                file.documentDisplayName = file.documentObject.name;
                var ext = file.documentName.split('.').pop().toLowerCase();
                if ($.inArray(ext, [ 'tif', 'jpeg', 'jpg', 'pdf' ]) == -1) {
                    $rootScope.NotifyMessages(false, resources.docExtInvalid);
                } else {
                    if (file.documentObject.size > 2097152) {
                        $rootScope.NotifyMessages(false,
                                resources.docSizeExceeded);

                    } else {
                        reader.readAsDataURL(file.documentObject);

                    }
                }
            } catch (e) {
                if (errorCallback && typeof errorCallback == "function") {
                    errorCallback(e);
                } else {
                    self._errorHandler(e);
                }
            }
        },
		/* To capture a file using camera to upload */
		captureFile : function(document, index, agentId, successCallback,
				errorCallback) {
			if (document.documentType == "Photograph") {
				index = 0;
			}
			var self = this;
			navigator.camera.getPicture(onSuccess, onFail, {
				quality : 50,
				destinationType : navigator.camera.DestinationType.DATA_URL,
				targetWidth : rootConfig.cameraTargetWidth,
				targetHeight : rootConfig.cameraTargetHeight,
				saveToPhotoAlbum : rootConfig.cameraSaveToPhotoAlbum
			});

			function onSuccess(imageData) {
				var d = new Date();
				var n = d.getTime();
				var entryName = "cdv_photo_001.jpg";
				var newFileName = agentId + n + "_" + entryName;
				if (document.documentType == "Photo") {
					index = 0;
					document.base64string = imageData;
					successCallback(document);
				} else {
					document.documentObject.pages[index].base64string = imageData;
					document.documentObject.pages[index].fileName = newFileName;
					if (document.documentType == "Photograph") {
						document.documentObject.documentName = document.documentName;
					}
					document.documentObject.documentType = document.documentType;
					successCallback(document);
				}
			}

			function onFail(message) {
				console.log('Failed because: ' + message);
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(message);
				} else {
					self._errorHandler(message);
				}
			}
		},
		/* Move the captured image to the application folder */
		_moveFile : function(URI, document, agentId, successCallback,
				errorCallback) {
			var self = this;
			window.resolveLocalFileSystemURI(URI, resolveOnSuccessMove,
					resOnError);

			function resolveOnSuccessMove(entry) {
				entry.file(function(fileObj) {
					console.log("File Size" + fileObj.size);
				});
				var d = new Date();
				var n = d.getTime();
				var fileExtension = entry.name.substring(entry.name
						.lastIndexOf('.') + 1);
				var newFileName = agentId + n + "_" + entry.name;
				if ($.inArray(fileExtension, [ 'tif', 'jpeg', 'jpg', 'pdf' ]) == -1) {
					$rootScope.NotifyMessages(false, resources.docExtInvalid);
				} else {
					entry.file(function(fileObj) {
						if (fileObj.size > 2097152) {
							$rootScope.NotifyMessages(false,
									resources.docSizeExceeded);

						} else {
							var myFolderApp = self.eAppFolderPath
									+ "/eappAttachements";

							window.requestFileSystem(
									LocalFileSystem.PERSISTENT, 0, function(
											fileSys) {
										// The folder is created if doesn't
										// exist
										fileSys.root.getDirectory(myFolderApp,
												{
													create : true,
													exclusive : false
												}, function(directory) {
													entry.moveTo(directory,
															newFileName,
															successMove,
															resOnError);
												}, resOnError);
									}, resOnError);
						}
					});

				}

			}

			function successMove(entry) {
				document.documentPath = entry.fullPath;
				document.documentName = entry.name;
				document.documentDisplayName = entry.name.substring(entry.name
						.indexOf('_') + 1);
				if (successCallback && typeof successCallback == "function") {
					successCallback(document);
				} else {
					self._defaultCallBack("File moved to the apps folder");
				}
			}

			function resOnError(e) {
				console.log(e.code);
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(e);
				} else {
					self._errorHandler(message);
				}
			}
		},
		/* Copy the file from the device folder to the application folder */
		_copyFile : function(URI, document, agentId, successCallback,
				errorCallback) {
			var self = this;
			window.resolveLocalFileSystemURI(URI, resolveOnSuccessCopy,
					resOnError);

			function resolveOnSuccessCopy(entry) {
				var d = new Date();
				var n = d.getTime();
				var fileExtension = entry.name.substring(entry.name
						.lastIndexOf('.') + 1);
				var newFileName = agentId + n + "_" + entry.name;
				if ($.inArray(fileExtension, [ 'tif', 'jpeg', 'jpg', 'pdf' ]) == -1) {
					$rootScope.NotifyMessages(false, resources.docExtInvalid);
				} else {
					entry.file(function(fileObj) {
						if (fileObj.size > 2097152) {
							$rootScope.NotifyMessages(false,
									resources.docSizeExceeded);

						} else {
							var myFolderApp = self.eAppFolderPath
									+ "/eappAttachements";

							window.requestFileSystem(
									LocalFileSystem.PERSISTENT, 0, function(
											fileSys) {
										// The folder is created if doesn't
										// exist
										fileSys.root.getDirectory(myFolderApp,
												{
													create : true,
													exclusive : false
												}, function(directory) {
													entry.copyTo(directory,
															newFileName,
															successMove,
															resOnError);
												}, resOnError);
									}, resOnError);
						}
					});

				}
			}

			function successMove(entry) {
				document.documentPath = entry.fullPath;
				document.documentName = entry.name;
				document.documentDisplayName = entry.name.substring(entry.name
						.indexOf('_') + 1);
				if (successCallback && typeof successCallback == "function") {
					successCallback(document);
				} else {
					self._defaultCallBack("File copied to the apps folder");
				}
			}

			function resOnError(e) {
				console.log(e.code);
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(e);
				} else {
					self._errorHandler(message);
				}
			}
		},
		/*
		 * To open the file in FileOpener plugin. We need to first copy the file
		 * to a Temp directory in the root folder where the File Opener can
		 * access
		 */
		openFile : function(filePath, fileName, successCallback, errorCallback) {
			var self = this;
			window.resolveLocalFileSystemURI(filePath, copyToTemp,
					errorTempCopy);

			function copyToTemp(entry) {
				var myFolderApp = "eappAttachements";
				window.requestFileSystem(LocalFileSystem.TEMPORARY,
						1024 * 1024, function(fileSys) {
							// The folder is created if doesn't exist
							fileSys.root.getDirectory(myFolderApp, {
								create : true,
								exclusive : false
							}, function(directory) {
								entry.copyTo(directory, entry.name,
										successTempCopy, errorTempCopy);
							}, errorTempCopy);
						}, errorTempCopy);

			}

			function successTempCopy(entry) {
				self.onFileOpenerExit = true;
				fileNameTemp = entry.fullPath;
				window.plugins.fileOpener.open(entry.fullPath, successCallback);
			}

			function errorTempCopy(e) {
				console.log(e.code);
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(e);
				} else {
					self._errorHandler(e);
				}
			}
		},
		/* To delete the file from the saved folder */
		deleteFile : function(doc, successCallback, errorCallback) {
			var fs = require('fs');
			
			if(fs.existsSync('Uploads')){
					if(fs.existsSync('Uploads/' + doc)){
						try{
							fs.unlinkSync('Uploads/' + doc);
						} catch(exception){
							if (errorCallback && typeof errorCallback == "function") {
									errorCallback(exception);
								} else {
									self._errorHandler(exception);
								}
							}
					} else {
						errorCallback("File does not exists");
					}
			}

			if (successCallback && typeof successCallback == "function") {
					successCallback();
			} else {
					self._defaultCallBack("Files removed.");
			}
		},
		/*
		 * When the user closes the viewer , the file temporary directory is
		 * deleted
		 */
		deleteTempFile : function(successCallback, errorCallback) {
			var self = this;
			if (self.onFileOpenerExit) {
				self.onFileOpenerExit = false;
				window.requestFileSystem(LocalFileSystem.TEMPORARY,
						1024 * 1024, onSuccess, errorHandler);
			}

			function onSuccess(fs) {
				fs.root
						.getDirectory(
								'eappAttachements',
								{},
								function(dirEntry) {
									dirEntry
											.removeRecursively(
													function() {
														if (successCallback
																&& typeof successCallback == "function") {
															successCallback();
														} else {
															self
																	._defaultCallBack("Temp Directory removed.");
														}
													}, errorHandler);

								}, errorHandler);
			}

			function errorHandler(e) {
				console.log(e.code);
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(e);
				} else {
					self._errorHandler(e);
				}
			}
		},
		uploadDocuments : function(docData, count, successCallback,
				errorCallback, options, $http) {
			var self = this;
			var uploadUrl = rootConfig.serviceBaseUrl
					+ "documentService/uploadDoc";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions.push(docData);
			//console.log('docrequest: ' + angular.toJson(requestInfo));

			var request = {
				method : 'POST',
				url : uploadUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request).success(function(data, status, headers, config) {
				if (successCallback && typeof successCallback == "function") {
					successCallback(data.Response, count);
				} else {
					self._defaultCallBack("File data saved");
				}
			}).error(
					function(data, status, headers, config) {
						console.log('error' + JSON.stringify(data) + 'status'
								+ status);
						errorCallback(data, status);
					});
		},
		startDownloadForEachTransaction : function(count, docDetails,
				successCallback, errorCallback, options, $http) {
			var downloadUrl = rootConfig.serviceBaseUrl
					+ "documentService/getDoc";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions.push(docDetails);
			//console.log('docrequest: ' + angular.toJson(requestInfo));

			var request = {
				method : 'POST',
				url : downloadUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request)
					.success(
							function(data, status, headers, config) {
								if (successCallback
										&& typeof successCallback == "function") {
									var documentData = {};
									documentData = data.Response.ResponsePayload.Transactions[0].TransactionData.Documents;
									console.log(JSON.stringify(documentData));
									successCallback(documentData, count);
								} else {
									self._defaultCallBack("File data saved");
								}
							}).error(
							function(data, status, headers, config) {
								console.log('error' + JSON.stringify(data)
										+ 'status' + status);
								errorCallback(data, status);
							});
		},
		writeFile : function(document, agentId, successCallback, errorCallback) {
			var self = this;
			var curDate = new Date();
			var curTime = curDate.getTime();
			var dataURI = document.fileObj;
			var docName = agentId + curTime + "_" + document.documentType
					+ ".txt";
			window.requestFileSystem(LocalFileSystem.TEMPORARY, 1024 * 1024,
					onFSSuccess, failure);

			function onFSSuccess(fs) {
				dataFolderPath = fs.root.fullPath;
				if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) // check for
				// device
				{
					dataFolderPath = dataFolderPath + "/";
				} else if (/Android/i.test(navigator.userAgent)) // for
				// android
				{
					dataFolderPath = self.eAppFolderPath + "/eappAttachements";
				}
				dataFilePath = dataFolderPath + "/" + docName;
				fs.root.getDirectory(dataFolderPath, {
					create : true,
					exclusive : false
				}, function(directory) {
					directory.getFile(docName, {
						create : true,
						exclusive : false
					}, gotFileEntryToWrite, failure);
				}, failure);
			}

			function gotFileEntryToWrite(fileEntry) {
				document.documentName = fileEntry.name;
				document.documentPath = fileEntry.fullPath;
				fileEntry.createWriter(gotFileWriter, failure);
			}

			function gotFileWriter(writer) {
				writer.onwrite = function(evt) {
					successCallback(document);
				};
				writer.write(dataURI);
			}

			function failure(e) {
				errorCallback(e);
			}
		},

		readFile : function(fileName, successCallback, errorCallback) {
			var self = this;
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
					onFSSuccess, failure);

			function onFSSuccess(fs) {
				dataFolderPath = fs.root.fullPath;
				if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) // check for
				// device
				{
					dataFolderPath = dataFolderPath + "/";
				} else if (/Android/i.test(navigator.userAgent)) // for
				// android
				{
					dataFolderPath = self.eAppFolderPath + "/eappAttachements/";
				}
				dataFolderPath = dataFolderPath + fileName;

				fs.root.getFile(dataFolderPath, null, gotFileEntryToRead,
						failure);
			}

			function gotFileEntryToRead(fileEntry) {
				fileEntry.file(gotFileToRead, failure);
			}

			function gotFileToRead(file) {
				readAsText(file); // has to use this one
			}

			function readAsText(file) {
				var reader = new FileReader();
				reader.onloadend = function(evt) {
					successCallback(evt.target.result);
				};
				reader.readAsText(file);
			}

			function failure(e) {
				errorCallback(e);
			}
		},

		_readFileContent : function(documents, docIndex, successCallback,
				errorCallback) {
			var self = this;
			var docLength = documents.length;
			var documentPath = documents[docIndex].DocumentPath;
			var documentName = documents[docIndex].DocumentName;

			var fileName = documentPath
					.substr(documentPath.lastIndexOf('/') + 1);
			var myFolderApp = self.eAppFolderPath + "/eappAttachements";
			var fileContent;
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onSuccess,
					errorHandler);

			function onSuccess(fs) {

				fs.root
						.getDirectory(
								myFolderApp,
								{},
								function(dirEntry) {
									dirEntry
											.getFile(
													fileName,
													{},
													function(fileEntry) {
														fileEntry
																.file(function(
																		file) {
																	var reader = new FileReader();
																	reader.onloadend = function(
																			e) {
																		fileContent = e.target.result;
																		if (docIndex == docLength - 1) {
																			self.filesFormated
																					.push({
																						fileName : documentName,
																						fileContent : fileContent
																					});
																			if (successCallback
																					&& typeof successCallback == "function") {
																				successCallback(self.filesFormated);
																			} else {
																				self
																						._defaultCallBack("File data saved");
																			}
																		} else {
																			self.filesFormated
																					.push({
																						fileName : documentName,
																						fileContent : fileContent
																					});
																			docIndex++;
																			self.filesFormated
																					.push({
																						fileName : documentName,
																						fileContent : fileContent
																					});
																			self
																					._readFileContent(
																							documents,
																							docIndex,
																							successCallback,
																							errorCallback);
																		}
																	};
																	reader
																			.readAsDataURL(file);
																});
													});
								});
			}

			function getFileContant(file, callback) {
				var reader = new FileReader();
				reader.onload = function(e) {
					if (callback && typeof callback == "function") {
						callback(e.target.result);
					} else {
						self._defaultCallBack("File data saved");
					}
				};
				reader.readAsDataURL(file);
			}

			function errorHandler(e) {
				if (errorCallback && typeof errorCallback == "function") {
					errorCallback(e);
				} else {
					self._errorHandler(message);
				}
			}
		},
		/* Default success callback if no callback specified */
		_defaultCallBack : function(message) {
			console.log(message);
		},
		/* Default failure callback if the error callback is not specified */
		_errorHandler : function(error) {
			console.log('Error : ' + error.message + ' (Code ' + error.code
					+ ')');
		}
	};
	return FileSelectUtility;
})();
document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
	FileSelectUtility.getApplicationPath();
}