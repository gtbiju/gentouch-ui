//Implementation for life engage service save
var DbOperationsUtility;
(function() {
	DbOperationsUtility = {
		saveTransactions : function(transactionObj, successCallback,
				errorCallback, options, $http) {
			var saveUrl = rootConfig.serviceBaseUrl + "lifeEngageService/save";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObj)

			var request = {
				method : 'POST',
				url : saveUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request)
					.success(
							function(data, status, headers, config) {
								if (data.Response.ResponsePayload.Transactions[0].StatusData.StatusCode == "100") {
									successCallback(data.Response.ResponsePayload.Transactions[0]);
								} else {
									errorCallback(data.Response.ResponsePayload.Transactions[0].StatusData.StatusMessage);
								}
							}).error(function(data, status, headers, config) {
						errorCallback(data, status, resources.errorMessage);
					});
		},
		runSTPRule : function(transactionObj, successCallback, errorCallback,
				options, $http) {
			DbOperationsUtility.saveTransactions(transactionObj,
					successCallback, errorCallback, options, $http);
		},
		getListingDetail : function(transactionObj, successCallback,
				errorCallback, options, $http) {
			try {
				var getUrl = rootConfig.serviceBaseUrl
						+ "lifeEngageService/retrieve";
				var requestInfo = Request();
				requestInfo.Request.RequestPayload.Transactions
						.push(transactionObj)

				callAngularPost(getUrl, requestInfo, options, successCallback,
						errorCallback, $http);

			} catch (exception) {
				alert("error in  getListings: " + angular.toJson(exception)
						+ " :error in  getListings");
				errorCallback(resources.errorMessage);
			}
		},
		getDetailsForListing : function(transactionObj, successCallback,
				errorCallback, options, $http) {
			return this.getListings(transactionObj, successCallback,
					errorCallback, options, $http);
		},
		deleteTransactions : function(transactionObj, successCallback,
				errorCallback, options, $http) {
			try {
				var getUrl = rootConfig.serviceBaseUrl
						+ "lifeEngageService/delete";
				var requestInfo = Request();
				requestInfo.Request.RequestPayload.Transactions
						.push(transactionObj)
				if (Object.prototype.toString.call(transactionObj) === '[object Array]') {
					requestInfo.Request.RequestPayload.Transactions = transactionObj;
				}

				callAngularPost(getUrl, requestInfo, options, successCallback,
						errorCallback, $http);

			} catch (exception) {
				alert("error in  deleteEapp: " + angular.toJson(exception)
						+ " :error in  getListings");
				errorCallback(resources.errorMessage);
			}
		},

		runRequirementRule : function(transactionData, successCallback, errorCallback, options, $http) {
		try {
			// Call rule engine and execute rule
			
			var saveUrl = rootConfig.serviceBaseUrl
				+ "lifeEngageService/executeRule";
			var requestInfo = Request();
		    var canonicalModelInput = {};
		    var transactionObjRule = {};
		    var RuleName = "";
		    var FunctionName = "";
		    var RuleName = rootConfig.requirementRuleName;
			var FunctionName = rootConfig.requirementFunctionName;
			
			canonicalModelInput.ruleInput = transactionData;
		    canonicalModelInput.ruleName = FunctionName;
		    canonicalModelInput.ruleGroup = "10";
		    transactionObjRule.TransactionData = canonicalModelInput;
		    requestInfo.Request.RequestPayload.Transactions
				.push(transactionObjRule);
        	var request = {
			method : 'POST',
			url : saveUrl,
			headers : {
				'Content-Type' : "application/json; charset=utf-8",
				"Accept" : "application/json; charset=utf-8",
				"Token" : options.headers.Token
			},
			data : requestInfo
		    }

		$http(request).success(function(data, status, headers, config) {
			successCallback(data);
		}).error(function(data, status, headers, config) {
			errorCallback(data, status);
		});
		} catch (exception) {
			errorCallback(exception);
		}
	},

		saveRequirement : function(data, successCallback, errorCallback, options, $http) {
		var self = this;
	
		var uploadUrl = rootConfig.serviceBaseUrl
				+ "requirementFileUploadService/uploadRequirementDocFile";
		var requestInfo = Request();
		requestInfo.Request.RequestPayload.Transactions.push(data);
		//console.log('requirementfilerequest: ' + angular.toJson(requestInfo));
		
		var request = {
		    		 method: 'POST',
		    		 url: uploadUrl,
		    		 headers: {
		    		   'Content-Type': "application/json; charset=utf-8",
		    		   "Token": options.headers.Token   
		    		 },
		    		 data: requestInfo
		    		}
	
		    		$http(request).success(function(data, status, headers, config){
		    			if (successCallback
							&& typeof successCallback == "function") {
						successCallback(data.Response);
					} else {
						self._defaultCallBack("File data saved");
					}
		    			}).error(function(data, status, headers, config){
		    				//console.log('error' + JSON.stringify(data)+ 'status' + status);
								
		    				errorCallback(data, status, resources.errorMessage);
		    			});
	},
	
	getDocumentsForRequirement : function(data, successCallback, errorCallback, options, $http) {
		var self = this;

		var uploadUrl = rootConfig.serviceBaseUrl
				+ "requirementFileUploadService/getRequirementDocFilesList";
		var requestInfo = Request();
		requestInfo.Request.RequestPayload.Transactions.push(data);
		//console.log('getrequirementfilelistrequest: ' + angular.toJson(requestInfo));
		
		var request = {
   	    		 method: 'POST',
   	    		 url: uploadUrl,
   	    		 headers: {
   	    		   'Content-Type': "application/json; charset=utf-8",
   	    		   "Token": options.headers.Token   
   	    		 },
   	    		 data: requestInfo
   	    		}

   	    		$http(request).success(function(data, status, headers, config){
   	    			if (successCallback
							&& typeof successCallback == "function") {
						if (data.Response.ResponsePayload.Transactions[0].Transactions[0])
		                  successCallback(data.Response.ResponsePayload.Transactions[0].Transactions[0].TransactionData.Requirement);		
						else
						  errorCallback(data, status, "Empty requirements array");
					} else {
						self._defaultCallBack("File data saved");
					}
   	    			}).error(function(data, status, headers, config){
   	    				//console.log('error' + JSON.stringify(data)+ 'status' + status);
								
   	    				errorCallback(data, status, resources.errorMessage);
   	    			});
	},
	
	getDocumentFileForRequirement : function(data, successCallback, errorCallback, options, $http) {
		var self = this;

		var uploadUrl = rootConfig.serviceBaseUrl
				+ "requirementFileUploadService/getRequirementDocFile";
		var requestInfo = Request();
		requestInfo.Request.RequestPayload.Transactions.push(data);
		//console.log('getrequirementfilelistrequest: ' + angular.toJson(requestInfo));
		
		var request = {
   	    		 method: 'POST',
   	    		 url: uploadUrl,
   	    		 headers: {
   	    		   'Content-Type': "application/json; charset=utf-8",
   	    		   "Token": options.headers.Token   
   	    		 },
   	    		 data: requestInfo
   	    		}

   	    		$http(request).success(function(data, status, headers, config){
   	    			if (successCallback
							&& typeof successCallback == "function") {
						if (data.Response.ResponsePayload.Transactions[0].StatusData.StatusCode == "100") {
		                  successCallback(data.Response.ResponsePayload.Transactions[0].TransactionData.RequirementFile);
						}							  
						else
						  errorCallback(data, status, "Empty requirements array");
					} else {
						self._defaultCallBack("File data saved");
					}
   	    			}).error(function(data, status, headers, config){
   	    				//console.log('error' + JSON.stringify(data)
								//+ 'status' + status);
   	    				errorCallback(data, status, resources.errorMessage);
   	    			});
	},

	saveRequirementFiles : function(data, successCallback, errorCallback, options, $http) {
		var self = this;

		var uploadUrl = rootConfig.serviceBaseUrl
				+ "requirementFileUploadService/uploadRequirementDocFile";
		var requestInfo = Request();
		requestInfo.Request.RequestPayload.Transactions.push(data);
		//console.log('requirementfilerequest: ' + angular.toJson(requestInfo));
		
		var request = {
   	    		 method: 'POST',
   	    		 url: uploadUrl,
   	    		 headers: {
   	    		   'Content-Type': "application/json; charset=utf-8",
   	    		   "Token": options.headers.Token   
   	    		 },
   	    		 data: requestInfo
   	    		}

   	    		$http(request).success(function(data, status, headers, config){
   	    			if (successCallback
							&& typeof successCallback == "function") {
						if (data.Response.ResponsePayload.Transactions[0].StatusData.StatusCode == "100") {
		                  successCallback(data.Response.ResponsePayload.Transactions[0].StatusData.StatusCode);
						}							  
						else{
							errorCallback(data.Response.ResponsePayload.Transactions[0].StatusData.StatusCode);
						}
						  
					} else {
						self._defaultCallBack("File data saved");
					}
   	    			}).error(function(data, status, headers, config){
   	    				//console.log('error' + JSON.stringify(data)+ 'status' + status);
								
   	    				errorCallback(data, status, resources.errorMessage);
   	    			});
	},
	
	getRequirementsForTransId : function(data,transtrackingId, successCallback,
			errorCallback) {
		try {
			var self = this;
			var dataObj = {};

		     successCallback(data);
		
		} catch (exception) {
			errorCallback(exception);
		}
	},
	
	getRequirementsUploadedForTransId : function(data,transtrackingId, successCallback,
			errorCallback) {
		try {
			var self = this;
			var dataObj = {}; 
			var dataArray=[];
			for(var i=0;i<data.Documents.length;i++){
			
			dataArray.push("'"+data.Documents[i].documentName+"'");
			}
        	successCallback(data);

		} catch (exception) {
			errorCallback(exception);
		}
	},	
	
	getStatusForRequirement : function(data,transtrackingId, successCallback,
			errorCallback) {
		try {
			var self = this;
			var dataObj = {}; 
			var dataArray=[];
			successCallback(data);
		
		} catch (exception) {
			errorCallback(exception);
		}
	},
	
		getListings : function(transactionObj, successCallback, errorCallback,
				options, $http) {
			try {
				var getUrl = rootConfig.serviceBaseUrl
						+ "lifeEngageService/retrieveAll";
				var requestInfo = Request();

				requestInfo.Request.RequestPayload.Transactions
						.push(transactionObj)
				callAngularPost(getUrl, requestInfo, options, successCallback,
						errorCallback, $http);

			} catch (exception) {
				alert("error in  getListings: " + angular.toJson(exception)
						+ " :error in  getListings");
				errorCallback(resources.errorMessage);
			}
		},
		getFilteredListing : function(transactionObj, successCallback, errorCallback,
				options, $http) {
			try {
				var getUrl = rootConfig.serviceBaseUrl
						+ "lifeEngageService/retrieveByFilter";
				var requestInfo = Request();

				requestInfo.Request.RequestPayload.Transactions
						.push(transactionObj)
				callAngularPost(getUrl, requestInfo, options, successCallback,
						errorCallback, $http);

			} catch (exception) {
				alert("error in  getFilteredListing: " + angular.toJson(exception)
						+ " :error in  getFilteredListing");
				errorCallback(resources.errorMessage);
			}
		},
		retrieveByFilterCount : function(transactionObj, successCallback, errorCallback,
				options,$http) {
			try {
				var getUrl = rootConfig.serviceBaseUrl
						+ "lifeEngageService/retrieveByFilterCount";
				var requestInfo = Request();

				requestInfo.Request.RequestPayload.Transactions
						.push(transactionObj)
				callAngularPost(getUrl, requestInfo, options, successCallback,
						errorCallback, $http);

			} catch (exception) {
				alert("error in  retrieveByFilterCount: " + angular.toJson(exception)
						+ " :error in  retrieveByFilterCount");
				errorCallback(resources.errorMessage);
			}
		},
        downloadBase64 : function(transactionObj, successCallback,
				errorCallback, options, $http) {
			try {
				
				var downloadPdfUrl = rootConfig.serviceBaseUrl + "documentService/getbase64stringpdf";

				var requestInfo = Request();
				requestInfo.Request.RequestPayload.Transactions.push(transactionObj);

				var request = {
					method : 'POST',
					url : downloadPdfUrl,
					headers : {
						'Content-Type' : "application/json; charset=utf-8",
						"Token" : options.headers.Token
					},
					data : requestInfo
				}
	           	
	            $http(request)
					.then(function successCB(response) {
						
		                successCallback(response);
		                
					}, function errorCB(response) {
						errorCallback(response.data,response.status);
					});
			} catch (exception) {
				errorCallback(exception);
			}
		},
        downloadPDFFromByteArray : function(transactionObj, templateId, successCallback,
				errorCallback, options, $http) {					
			try {
				var type = transactionObj.Type;
				var proposalNumber = "";
				if (type == "eApp") {
					proposalNumber = transactionObj.Key4;
				} else if(type == "illustration") {
					proposalNumber = transactionObj.Key3;
				} else if(type == "FNA") {
					proposalNumber = transactionObj.Key2;
				}
				var cacheParamValue = (new Date()).getTime();
				var downloadPDFUrl = rootConfig.serviceBaseUrl
						+ "documentService/generatePdf/" + proposalNumber + "/"
						+ type + "/" + templateId + "?cache=" + cacheParamValue;
				var a = document.createElement("a");
	            document.body.appendChild(a);
	            a.style = "display: none";
	           	var request = {
	    			method : 'GET',
	    			url : downloadPDFUrl,
	    			headers : {
	    				"Token" : options.headers.Token
	    			},
					responseType: 'arraybuffer'
	    		};
	            $http(request)
					.then(function successCB(response) {
						var fileName = response.headers("Content-Disposition").match(/filename="([^"\\]*(?:\\.[^"\\]*)*)"/i)[1];
						var userAgent = window.navigator.userAgent;
						if (rootConfig.isDeviceMobile == "false" &&  userAgent.match(/iPad/i) || userAgent.match(/iPhone/i)) {
							
							var str = base64ArrayBuffer(response.data);
							var pdfAsDataUri = "data:application/pdf;base64,"+str;
							window.open(pdfAsDataUri );
						}
						else{
							
							saveAs(new Blob([response.data], {type: "application/octet-stream;charset=UTF-8"}), fileName);
						}

		                successCallback();
					}, function errorCB(response) {
						errorCallback(response.data,response.status);
					});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		downloadPDF : function(transactionObj, templateId, successCallback,
				errorCallback, options, $http) {
				var base64data="";
				DbOperationsUtility.downloadBase64(transactionObj,
							function successCB(response){
								
								base64data = response.data.Response.ResponsePayload.Transactions[0].base64pdf;
								var type = transactionObj.Type;
								var fileName="";
								if (type == "eApp") {
									fileName = "EappProposal_" + transactionObj.Key4 + ".pdf";
								} else if(type == "illustration") {
									fileName = "IllustrationProposal_" + transactionObj.Key3 + ".pdf";
								} else if(type == "FNA") {
									fileName = "FNAReport_" + transactionObj.Key2 + ".pdf";
								}
								
								var userAgent = window.navigator.userAgent;
								if (rootConfig.isDeviceMobile == "false" &&  userAgent.match(/iPad/i) || userAgent.match(/iPhone/i)) {
									
									var pdfAsDataUri = "data:application/pdf;base64,"+base64data;
									window.open(pdfAsDataUri);
								}
								else{
									var blob = base64toBlob(base64data);
									saveAs(new Blob([blob], {type: "application/octet-stream;charset=UTF-8"}), fileName);
								}

								successCallback();
							}, function errorCB(response) {
								errorCallback(response.data,response.status);
							},options, $http);
			
		},
		downloadDocumentPdf : function(fileName, proposalNo, successCallback,
				errorCallback, options, $http) {
		try {
				var cacheParamValue = (new Date()).getTime();
				var downloadPDFUrl = rootConfig.serviceBaseUrl
						+ "requirementFileUploadService/generatePdfDocument/"
						+ fileName + "/" +  proposalNo + "?cache=" + cacheParamValue;
				var a = document.createElement("a");
	            document.body.appendChild(a);
	            a.style = "display: none";
	           	var request = {
	    			method : 'GET',
	    			url : downloadPDFUrl,
	    			headers : {
	    				"Token" : options.headers.Token
	    			},
					responseType: 'arraybuffer'
	    		};
	            $http(request)
					.then(function successCB(response) {
						var file = new Blob([response.data], {type: 'application/pdf'});
						var fileURL = window.URL.createObjectURL(file);
						a.href = fileURL;
						var contentDisposition = response.headers("Content-Disposition");
						var res = contentDisposition.match(/filename="([^"\\]*(?:\\.[^"\\]*)*)"/i);
						if(res) {
							a.download = res[1];
						} else {
								a.download = "EappProposal_" + fileName + ".pdf";
						}
						a.click();
		                successCallback();
					}, function errorCB(response) {
						errorCallback();
					});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		lookupCall : function(transactionObj, successCallback, errorCallback,
				options, $http) {
			var subDivisionType = transactionObj.TransactionData.subDivisionType;
			var lookupServiceUrl = rootConfig.serviceBaseUrl
					+ "lookUpService/getCountryOrCountrySubDivisionList";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObj);

			var request = {
				method : 'POST',
				url : lookupServiceUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request)
					.success(
							function(data, status, headers, config) {
								dResult = data.Response.ResponsePayload.Transactions[0].TransactionData.List;
								for (i in data.Response.ResponsePayload.Transactions[0].TransactionData.List) {
									data.Response.ResponsePayload.Transactions[0].TransactionData.List[i]['Dependant'] = requestInfo.Request.RequestPayload.Transactions[0].TransactionData.Id;
								}
								successCallback(
										"LookupService." + subDivisionType,
										data.Response.ResponsePayload.Transactions[0].TransactionData.List);

							}).error(function(data, status, headers, config) {
						errorCallback(data, status, resources.errorMessage);
					});
		},
		getCalenderListings : function(currentDate, todoSelected, endDate,
				viewallDate, transactionObj, successCallback, errorCallback) {
			errorCallback();
		},
		payment : function(transactionObj, successCallback, errorCallback) {

			transactionObj = '';
			var paymentServiceUrl = rootConfig.serviceBaseUrl
					+ "paymentService/makePayment";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObj);
			return (true);

		},
		getAgentDetails : function(transactionObj, successCallback,
				errorCallback) {
			try {
				var data = [ {
					"businessSourced" : 11,
					"experience" : 10,
					"servicedCustomers" : 12
				} ];
				successCallback(data);
			} catch (exception) {
				errorCallback(exception);
			}
		},
		getAchievementDetails : function(transactionObj, successCallback,
				errorCallback) {
			try {
				var data = [ {
					"text" : "Staff Excellence Award"
				}, {
					"text" : "Performer of the Year 2013"
				}, {
					"text" : "Chairman Club Member"
				} ];
				successCallback(data);
			} catch (exception) {
				errorCallback(exception);
			}
		},
		fetchTransactionForIds : function(transactionObj, successCallback, errorCallback,
				options, $http) {
			try {
				var getUrl = rootConfig.serviceBaseUrl
						+ "lifeEngageService/retrieveByIds";
				var requestInfo = Request();

				requestInfo.Request.RequestPayload.Transactions
						.push(transactionObj)
				callAngularPost(getUrl, requestInfo, options, successCallback,
						errorCallback, $http);

			} catch (exception) {
				alert("error in  fetchTransactionForIds: " + angular.toJson(exception)
						+ " :error in  fetchTransactionForIds");
				errorCallback(resources.errorMessage);
			}
		},
		saveTransactionObjectsMultiple : function(transactionObj, successCallback,
				errorCallback, options, $http) {
			var saveUrl = rootConfig.serviceBaseUrl + "emailService/updateEmailDetails";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions = transactionObj;
				//	.push(transactionObj)

			var request = {
				method : 'POST',
				url : saveUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}

			$http(request)
					.success(
							function(data, status, headers, config) {
								if (data.Response.ResponsePayload.Transactions[0].StatusData.StatusCode == "100") {
									successCallback(data.Response.ResponsePayload.Transactions[0]);
								} else {
									errorCallback(data.Response.ResponsePayload.Transactions[0].StatusData.StatusMessage);
								}
							}).error(function(data, status, headers, config) {
						errorCallback(data, status, resources.errorMessage);
					});		
		},
		
		getTransactions : function(transactionObj, successCallback,
				errorCallback, options, $http) {
			try {
				var getUrl = rootConfig.serviceBaseUrl+rootConfig.servicePath;

				var requestInfo = Request();
	            
				// switch -- check the type and add the urls
				switch (transactionObj.Type) {
				
				case "UserDetails":
		      		getUrl = getUrl + "userService/register";
		      		break;
				case "ForgetPassword":
		        	getUrl = getUrl + "userService/forgetPassword";
		        	break;
				case "CreatePassword":
		        	getUrl = getUrl + "userService/createPassword";
		        	break;
				case "ResetPassword":
		        	getUrl = getUrl + "userService/resetPassword";
		        	break;	
				default:break;
				
			} 

				requestInfo.Request.RequestPayload.Transactions
						.push(transactionObj);
				jQuery.support.cors = true;
				
				var request = {
					method : 'POST',
					url : getUrl,
					headers : {
						'Content-Type' : "application/json; charset=utf-8",
						"Token" : options.Token
					},
					data : requestInfo
			    }
				
				$http(request).success(function(data, status, headers, config) {
								if (data.Response.ResponsePayload.Transactions != null) {
					                 successCallback(data.Response.ResponsePayload.Transactions);
				                } else {
					                 successCallback(data.Response.ResponsePayload);
								}
							}).error(function(data, status, headers, config) {
						errorCallback(data, status, resources.errorMessage);
					});
				

				/* callAjaxPost(getUrl, true, requestInfo, successCallback,
						errorCallback); */
			} catch (exception) {
				alert("error in  getTransactions: " + angular.toJson(exception)
						+ " :error in  getTransactions");
				errorCallback(resources.errorMessage);
			}
		},
		
		retrieveSalesActivityDetails : function(transactionObj, successCallback, errorCallback,
				options,$http) {
			try {
				var getUrl = rootConfig.serviceBaseUrl
						+ "generaliSalesActivityService/retrieveSalesActivityDetails";
				var requestInfo = Request();

				requestInfo.Request.RequestPayload.Transactions
						.push(transactionObj)
				callAngularPost(getUrl, requestInfo, options, successCallback,
						errorCallback, $http);

			} catch (exception) {
				alert("error in  retrieveSalesActivityDetails: " + angular.toJson(exception)
						+ " :error in  retrieveSalesActivityDetails");
				errorCallback(resources.errorMessage);
			}
		}
		
	};
	return DbOperationsUtility;
})();

/*
 * Generic Method for calling Angular $http service.
 */
function callAngularPost(url, data, options, successCallback, errorCallback,
		$http) {
	var request = {
		method : 'POST',
		url : url,
		headers : {
			'Content-Type' : "application/json; charset=utf-8",
			"Token" : options.headers.Token
		},
		data : data
	}

	$http(request).success(function(data, status, headers, config) {
		if (data.Response.ResponsePayload.Transactions)
			successCallback(data.Response.ResponsePayload.Transactions);
		else
			successCallback(data.Response.ResponsePayload);
	}).error(function(data, status, headers, config) {
		errorCallback(data, status, resources.errorMessage);
	});
}
