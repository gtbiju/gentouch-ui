/*
 * Copyright 2015, LifeEngage 
 */

(function() {

	DbOperationsUtility['getDetailsForListing'] = function(transactionObj,
			successCallback, errorCallback, options) {
		try {
			this
					.getListings(
							transactionObj,
							function(data) {
								var requiredData = [];
								for ( var i = 0; i < data.length; i++) {
									if (data[i].TransactionData) {
										if (data[i].Type == "illustration")
											data[i].TransactionData.IllustrationOutput = {};
										// get only the required details to
										// be displayed in the listing
										// screen
										var dataNew = {
											"TransactionData" : {
												"Insured" : {
													"BasicDetails" : {
														"firstName" : "",
														"lastName" : "",
														"dob" : ""
													}
												},
												"Proposer" : {
													"BasicDetails" : {
														"firstName" : "",
														"lastName" : ""
													}
												},
												"Payer" : {
													"BasicDetails" : {
														"firstName" : "",
														"lastName" : ""
													}
												},
												"Product" : {
													"ProductDetails" : {
														"productName" : "",
														"productCode" : "",
														"productType" : ""
													},
													"premiumSummary" : {
														"validatedDate" : ""
													}
												}
											}
										};
										if (data[i].Type == "eApp") {
											if (data[i].TransactionData.Proposer.BasicDetails) {
												dataNew.TransactionData.Proposer.BasicDetails.firstName = data[i].TransactionData.Proposer.BasicDetails.firstName;
												dataNew.TransactionData.Proposer.BasicDetails.lastName = data[i].TransactionData.Proposer.BasicDetails.lastName;
											}
											if (data[i].TransactionData.Insured.BasicDetails) {
												dataNew.TransactionData.Insured.BasicDetails.firstName = data[i].TransactionData.Insured.BasicDetails.firstName;
												dataNew.TransactionData.Insured.BasicDetails.lastName = data[i].TransactionData.Insured.BasicDetails.lastName;
											}
											if (data[i].TransactionData.Product.ProductDetails) {
												dataNew.TransactionData.Product.ProductDetails.productName = data[i].TransactionData.Product.ProductDetails.productName;
												dataNew.TransactionData.Product.ProductDetails.productCode = data[i].TransactionData.Product.ProductDetails.productCode;
												dataNew.TransactionData.Product.ProductDetails.productType = data[i].TransactionData.Product.ProductDetails.productType;
											}
										}
										if (data[i].Type != "FNA") {
											if (data[i].TransactionData.Product.ProductDetails) {
												dataNew.TransactionData.Product.ProductDetails.productName = data[i].TransactionData.Product.ProductDetails.productName;
												dataNew.TransactionData.Product.ProductDetails.productCode = data[i].TransactionData.Product.ProductDetails.productCode;
												dataNew.TransactionData.Product.ProductDetails.productType = data[i].TransactionData.Product.ProductDetails.productType;
											}
											if (data[i].TransactionData.Insured.BasicDetails) {
												dataNew.TransactionData.Insured.BasicDetails.firstName = data[i].TransactionData.Insured.BasicDetails.firstName;
												dataNew.TransactionData.Insured.BasicDetails.lastName = data[i].TransactionData.Insured.BasicDetails.lastName;
												dataNew.TransactionData.Insured.BasicDetails.dob = data[i].TransactionData.Insured.BasicDetails.dob;
											}
											if (data[i].TransactionData.Payer.BasicDetails) {
												dataNew.TransactionData.Payer.BasicDetails.firstName = data[i].TransactionData.Payer.BasicDetails.firstName;
												dataNew.TransactionData.Payer.BasicDetails.lastName = data[i].TransactionData.Payer.BasicDetails.lastName;
											}

										}
										if (data[i].Type == "illustration") {
											if (data[i].TransactionData.Product.premiumSummary) {
												dataNew.TransactionData.Product.premiumSummary.validatedDate = data[i].TransactionData.Product.premiumSummary.validatedDate;
											}
										} else {
											var partiesArray = data[i].TransactionData.parties;
											var basicDetails = {
												"BasicDetails" : {
													"firstName" : "",
													"dob" : ""
												}
											};
											dataNew.TransactionData.parties = [];
											for (party in partiesArray) {
												if (partiesArray[party].type == "FNAMyself") {
													basicDetails.BasicDetails.firstName = partiesArray[party].BasicDetails.firstName
															+ " "
															+ partiesArray[party].BasicDetails.lastName;
													basicDetails.BasicDetails.dob = partiesArray[party].BasicDetails.dob;
													dataNew.TransactionData.parties
															.push(basicDetails);
												}
											}
										}
										dataNew.Key1 = data[i].Key1;
										dataNew.Key2 = data[i].Key2;
										dataNew.Key3 = data[i].Key3;
										dataNew.Key4 = data[i].Key4;
										dataNew.Key5 = data[i].Key5;
										dataNew.Key6 = data[i].Key6;
										dataNew.Key13 = data[i].Key13;
										dataNew.Key15 = data[i].Key15;
										dataNew.Key16 = data[i].Key16;
										dataNew.Key19 = data[i].Key19;
										dataNew.Key21 = data[i].Key21;
										dataNew.Id = data[i].Id;
										delete data[i];
										requiredData.push(dataNew);
									}
								}
								successCallback(requiredData);
							}, function(error) {
								errorCallback(error);
							});
		} catch (exception) {
			errorCallback(exception);
		}

	};
	 DbOperationsUtility['getTransactions'] = function(transactionObj, successCallback,
	      		errorCallback) {
	      	var getUrl = rootConfig.registrationURL;
	      	var requestInfo = Request();
	      	switch (transactionObj.Type) {
	      	case "UserDetails":
	      		getUrl = getUrl + "userService/register";
	      		break;
	      	case "ForgetPassword":
	      		getUrl = getUrl + "userService/forgetPassword";
	      		break;
			case "CreatePassword":
		        getUrl = getUrl + "userService/createPassword";
		        requestInfo = getRequest();
		        break;
			case "ResetPassword":
			        	getUrl = getUrl + "userService/resetPassword";
			        	break;
			default:break;
	      	}
	      	requestInfo.Request.RequestPayload.Transactions.push(transactionObj);
	      	jQuery.support.cors = true;
	      	$.ajax({
	      				type : "POST",
	      				crossDomain : true,
	      				url : getUrl,
	      				headers: {
	      					"SOURCE": 100
	      			    },
	      				data : angular.toJson(requestInfo),
	      				contentType : "application/json; charset=utf-8",
	      				dataType : "json",
	      				success : function(data, status) {
	      					try{
	      						if ((data.STATUS)&& (data.STATUS == 'E')) {

	      						    errorCallback("Error in ajax call"+data.STATUS);
	      						}
	      						else
	      						{
	      						    if (data.Response.ResponsePayload.Transactions !== null)
	      						    {
										successCallback(data.Response.ResponsePayload.Transactions);
	      						    }
	      						    else{
										successCallback(data.Response.ResponsePayload);
									}
	      						}
	      					}
	      				catch(error){
	      				var data =
	      				errorCallback("Error in ajax call"+error);
	      				}
	      				},
	      				error : function(data, status) {
	      						errorCallback(data.status);
	      				}
	      			});
	      };
	
	DbOperationsUtility['retrieveSalesActivityDetails'] = function(transactionObj, successCallback, errorCallback,
					options,$http) {
				try {
					var getUrl = rootConfig.serviceBaseUrl
							+ "generaliSalesActivityService/retrieveSalesActivityDetails";
					var requestInfo = Request();

					requestInfo.Request.RequestPayload.Transactions
							.push(transactionObj)
					callAngularPost(getUrl, requestInfo, options, successCallback,
							errorCallback, $http);

				} catch (exception) {
					alert("error in  retrieveSalesActivityDetails: " + angular.toJson(exception)
							+ " :error in  retrieveSalesActivityDetails");
					errorCallback(resources.errorMessage);
				}
			};
			
	DbOperationsUtility["retrieveByFilterCount"] = function(transactionObj, successCallback,
			errorCallback) {
		try {
			var self = this;
			var searchData = [];
			var sql = "";
			//var date = new Date();
			//date.setDate(date.getDate() - 30);
			//var dateString = date.toISOString().split('T')[0] + " 00:00:00";
			var searchvalue = transactionObj.TransactionData.searchCriteriaRequest.value;
			if (transactionObj.TransactionData.searchCriteriaRequest.command === 'LandingDashBoardCount') {
				if (transactionObj.TransactionData.searchCriteriaRequest.modes !== undefined
						&& transactionObj.TransactionData.searchCriteriaRequest.modes.length > 0) {

					for (var i = 0; i < transactionObj.TransactionData.searchCriteriaRequest.modes.length; i++) {
						if (transactionObj.TransactionData.searchCriteriaRequest.modes[i] === 'LMS') {
							
							var agents="";
							if(transactionObj.TransactionData.agentType=="BranchUser"){
								for(var indx=0; indx < transactionObj.TransactionData.reportingAgents.length; indx++){
									if (indx==(transactionObj.TransactionData.reportingAgents.length-1)){
										agents=agents+"'"+transactionObj.TransactionData.reportingAgents[indx].agentCode+"'";
									}else{
										agents=agents+"'"+transactionObj.TransactionData.reportingAgents[indx].agentCode+"',";
									}
								}
							} else{
								agents="'"+transactionObj.Key11+"'";
							}
							
							
							if (sql === "") {
								sql = "select count(*) as Count,Key15 as Status,SUM(Key27) as Appointment,null as MemoCount,Type from Transactions where Type='LMS' and Key15 <> 'Cancelled'";
								if (searchvalue != "") {
									/*sql += " and Key11 in ("
											+ agents
											+ ") and  ((UPPER(Key2) LIKE '%"
											+ searchvalue
											+ "%' or UPPER(Key2)=UPPER('"
											+ searchvalue
											+ "')) or (UPPER(Key3) LIKE '%"
											+ searchvalue
											+ "%' or UPPER(Key3)=UPPER('"
											+ searchvalue
											+ "')) or (UPPER(Key4) LIKE '%"
											+ searchvalue
											+ "%' or UPPER(Key4)=UPPER('"
											+ searchvalue + "'))) ";*/
									sql += " and Key11 in ("
										+ agents
										+ ") and  ((UPPER(Key2) LIKE '%"
										+ searchvalue
										+ "%' or UPPER(Key2)=UPPER('"
										+ searchvalue
										+ "')) or (UPPER(Key3) LIKE '%"
										+ searchvalue
										+ "%' or UPPER(Key3)=UPPER('"
										+ searchvalue
										+ "')) or (UPPER(Key4) LIKE '%"
										+ searchvalue
										+ "%' or UPPER(Key4)=UPPER('"
										+ searchvalue
										+ "')) or (UPPER(Key26) LIKE '%"
										+ searchvalue 
										+ "%' or UPPER(Key26)=UPPER('"
										+ searchvalue + "'))) ";
								}
								sql += " and Key11 in ("
										+ agents
										+ ")  and (strftime('%m',date(Key13)) = strftime('%m',date('now')) or (strftime('%m',date(modifiedDate)) = strftime('%m',date('now'))))"
										+ " group by Key15,Type ";
							} else {
								sql += " UNION ALL select count(*) as Count,Key15 as Status,SUM(Key27) as Appointment,null as MemoCount ,Type from Transactions where Type='LMS' and Key15 <> 'Cancelled'";
								if (searchvalue != "") {
									sql += " and Key11 = '"
											+ transactionObj.Key11
											+ "' and  ((UPPER(Key2) LIKE '%"
											+ searchvalue
											+ "%' or UPPER(Key2)=UPPER('"
											+ searchvalue
											+ "')) or (UPPER(Key3) LIKE '%"
											+ searchvalue
											+ "%' or UPPER(Key3)=UPPER('"
											+ searchvalue
											+ "')) or (UPPER(Key4) LIKE '%"
											+ searchvalue
											+ "%' or UPPER(Key4)=UPPER('"
											+ searchvalue + "'))) ";
								}
								sql += " and Key11 = '"
										+ transactionObj.Key11
										+ "'  group by Key15,Type ";
							}

						} else if (transactionObj.TransactionData.searchCriteriaRequest.modes[i] === 'FNA') {
							if (sql === "") {
								sql = "select count(*) as Count,Key15 as Status,SUM(Key27) as Appointment, null as MemoCount,Type from Transactions where Type='FNA'";
								if (searchvalue != "") {
									sql += " and Key11 = '"
											+ transactionObj.Key11
											+ "'  and ((UPPER(Key6) LIKE '%"
											+ searchvalue
											+ "%' or UPPER(Key6)=UPPER('"
											+ searchvalue
											+ "')) or (UPPER(Key8) LIKE '%"
											+ searchvalue
											+ "%' or UPPER(Key8)=UPPER('"
											+ searchvalue + "')))";
								}
								sql += " and Key11 = '"
										+ transactionObj.Key11
										+ "'  group by Key15,Type";
							} else {
								sql += " UNION ALL select count(*) as Count,Key15 as Status,SUM(Key27) as Appointment,null as MemoCount,Type from Transactions where Type='FNA'";
								if (searchvalue != "") {
									sql += " and Key11 = '"
											+ transactionObj.Key11
											+ "'  and ((UPPER(Key6) LIKE '%"
											+ searchvalue
											+ "%' or UPPER(Key6)=UPPER('"
											+ searchvalue
											+ "')) or (UPPER(Key8) LIKE '%"
											+ searchvalue
											+ "%' or UPPER(Key8)=UPPER('"
											+ searchvalue + "')))";
								}
								sql += " and Key11 = '"
										+ transactionObj.Key11
										+ "'  group by Key15,Type";
							}
						} else if (transactionObj.TransactionData.searchCriteriaRequest.modes[i]
								.toLowerCase() === 'illustration') {
							if (sql === "") {
								sql = "select count(*) as Count,Key15 as Status,SUM(Key27) as Appointment,null as MemoCount,Type from Transactions where Type='illustration' and Key15 <> 'Cancelled'";
								if (searchvalue != "") {
									/*sql += " and Key11 = '"
											+ transactionObj.Key11
											+ "'  and ((UPPER(Key6) LIKE '%"
											+ searchvalue
											+ "%' or UPPER(Key6)=UPPER('"
											+ searchvalue
											+ "')) or (UPPER(Key8) LIKE '%"
											+ searchvalue
											+ "%' or UPPER(Key8)=UPPER('"
											+ searchvalue + "')))";*/
									sql += " and Key11 = '"
										+ transactionObj.Key11
										+ "'  and ((UPPER(Key6) LIKE '%"
										+ searchvalue
										+ "%' or UPPER(Key6)=UPPER('"
										+ searchvalue
										+ "')) or (UPPER(Key4) LIKE '%"
										+ searchvalue
										+ "%' or UPPER(Key26)=UPPER('"
										+ searchvalue
										+ "')) or (UPPER(Key26) LIKE '%"
										+ searchvalue
										+ "%' or UPPER(Key8)=UPPER('"
										+ searchvalue
                                        + "') or UPPER(Key8) LIKE '%"
                                        + searchvalue
                                        +"%'))";
										
								}
								sql += " and Key11 = '"
										+ transactionObj.Key11
										+ "'  group by Key15,Type";
							} else {
								sql += " UNION ALL select count(*) as Count,Key15 as Status,SUM(Key27) as Appointment,null as MemoCount,Type from Transactions where Type='illustration' and Key15 <> 'Cancelled'";
								if (searchvalue != "") {
									/*sql += " and Key11 = '"
											+ transactionObj.Key11
											+ "'  and ((UPPER(Key6) LIKE '%"
											+ searchvalue
											+ "%' or UPPER(Key6)=UPPER('"
											+ searchvalue
											+ "')) or (UPPER(Key8) LIKE '%"
											+ searchvalue
											+ "%' or UPPER(Key8)=UPPER('"
											+ searchvalue + "')))";*/
									sql += " and Key11 = '"
										+ transactionObj.Key11
										+ "'  and ((UPPER(Key6) LIKE '%"
										+ searchvalue
										+ "%' or UPPER(Key6)=UPPER('"
										+ searchvalue
										+ "')) or (UPPER(Key4) LIKE '%"
										+ searchvalue
										+ "%' or UPPER(Key26)=UPPER('"
										+ searchvalue
										+ "')) or (UPPER(Key26) LIKE '%"
										+ searchvalue
										+ "%' or UPPER(Key8)=UPPER('"
										+ searchvalue
                                        + "') or UPPER(Key8) LIKE '%"
                                        + searchvalue
                                        +"%'))";
								}
								sql += " and Key11 = '"
									+ transactionObj.Key11											
									+ "' "
									+"and (((Key15 = 'Completed' or Key15 = 'Confirmed') and strftime('%m',date(Key14)) = strftime('%m',date('now'))) or (Key15 = 'Draft' and strftime('%m',date(Key13)) = strftime('%m',date('now')))) "
									+"group by Key15,Type";
							}
						} else if (transactionObj.TransactionData.searchCriteriaRequest.modes[i]
								.toLowerCase() === 'eapp') {
							if (sql === "") {
								sql = "select count(*) as Count,Key15 as Status,SUM(Key27) as Appointment, SUM(Key30) as MemoCount,Type from Transactions where Type='eApp' and Key15 <> 'Cancelled'";
								if (searchvalue != "") {
									sql += " and Key11 = '"
										+ transactionObj.Key11
										+ "'  and ((UPPER(Key6) LIKE '%"
										+ searchvalue
										+ "%' or UPPER(Key6)=UPPER('"
										+ searchvalue
										+ "')) or (UPPER(Key4) LIKE '%"
										+ searchvalue
										+ "%' or UPPER(Key26)=UPPER('"
										+ searchvalue
										+ "')) or (UPPER(Key26) LIKE '%"
										+ searchvalue
										+ "%' or UPPER(Key8)=UPPER('"
										+ searchvalue
                                        + "') or UPPER(Key8) LIKE '%"
                                        + searchvalue
                                        +"%'))";
								}
								sql += " and Key11 = '"
										+ transactionObj.Key11
										+ "' "
                                        +"and (strftime('%m',date(Key13)) = strftime('%m',date('now')) or (strftime('%m',date(modifiedDate)) = strftime('%m',date('now'))))"
                                        +"group by Key15,Type";
							} else {
								sql += " UNION ALL select count(*) as Count,Key15 as Status,SUM(Key27) as Appointment,SUM(Key30) as MemoCount,Type from Transactions where Type='eApp' and Key15 <> 'Cancelled'";
								if (searchvalue != "") {
									sql += " and Key11 = '"
										+ transactionObj.Key11
										+ "'  and ((UPPER(Key6) LIKE '%"
										+ searchvalue
										+ "%' or UPPER(Key6)=UPPER('"
										+ searchvalue
										+ "')) or (UPPER(Key4) LIKE '%"
										+ searchvalue
										+ "%' or UPPER(Key26)=UPPER('"
										+ searchvalue
										+ "')) or (UPPER(Key26) LIKE '%"
										+ searchvalue
										+ "%' or UPPER(Key8)=UPPER('"
										+ searchvalue
                                        + "') or UPPER(Key8) LIKE '%"
                                        + searchvalue
                                        +"%'))";
								}
								sql += " and Key11 = '"
										+ transactionObj.Key11
										+ "' "
                                        +"and (strftime('%m',date(Key13)) = strftime('%m',date('now')) or (strftime('%m',date(modifiedDate)) = strftime('%m',date('now'))))"
                                        +"group by Key15,Type";
							}
						}
					}
				}
				//debugger;
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					//sql1 = "select TransactionData as LeadData,Type from Transactions where Type='LMS' and (strftime('%m',date(Key13)) = strftime('%m',date('now')) or (strftime('%m',date(modifiedDate)) = strftime('%m',date('now'))))";
					sql1 = "select TransactionData as LeadData,Type from Transactions where Type='LMS'";
					//console.log(sql1);
					_dbOperations._executeSql(sql1, function(results1) {
						var data1 = _dbOperations._getRecordsArray(results1);
						var appointmentCount = 0;
						var currentDate = new Date();
					    var currentMonth = currentDate.getMonth();
					    var actualMonth =  Number(currentMonth+1);
						for (var z=0; z < data1.length; z++){
							var m1 = JSON.parse(unescape(data1[z].LeadData));
							for (var comm=0; comm < m1.Communication.length; comm++){
								if(m1.Communication[comm].interactionType == "Appointment" && m1.Communication[comm].followUps.length > 0){
									for(var s1 =0; s1 < m1.Communication[comm].followUps.length; s1++){
										compareDate = m1.Communication[comm].followUps[s1].metDate
										var checkComparsion = compareDate.split('-');
										if(checkComparsion[1] == actualMonth){
											appointmentCount++;
										}
									}
								}
							}
						}
						var eAppmodeCount = 0, lmsmodeCount = 0, fnamodeCount = 0, illustrationmodeCount = 0, flageApp = true, flagLMS = true, flagFNA = true, flagillustration = true, eAppcount = 0, LMScount = 0, FNAcount = 0, illustrationcount = 0;
						var eAppIndex, lmsIndex, fnaIndex, illustrationIndex, commonIndex = 0, appointmentType = "Appointment";
						for (var y = 0; y < data.length; y++) {
							if (data[y].Type.toLowerCase() === "eapp") {
								if (flageApp) {
									eAppIndex = commonIndex;
								}
								if (eAppcount == 0 && flageApp) {
									searchData.push({
										"mode" : data[y].Type,
										"count" : eAppmodeCount,
										"subModes" : []
									});
									searchData[eAppIndex].subModes.push({
										"mode" : rootConfig.statusCountListeAppMemo,
										"count" : 0
									});
									eAppcount++;
									commonIndex++;
									flageApp = false;
								}
								if (!flageApp) {
									eAppcount++;
									searchData[eAppIndex].subModes.push({
										"mode" : data[y].Status,
										"count" : data[y].Count
									});
									searchData[eAppIndex].count += data[y].Count;
									if(data[y].MemoCount!=null && data[y].MemoCount>0){
										searchData[eAppIndex].subModes[0].count +=data[y].MemoCount;
										searchData[eAppIndex].count += data[y].MemoCount;
									}
								}
							} else if (data[y].Type === "LMS") {
								if (flagLMS) {
									lmsIndex = commonIndex;
								}
								if (LMScount == 0 && flagLMS) {
									searchData.push({
										"mode" : data[y].Type,
										"count" : lmsmodeCount,
										"subModes" : []
									});
									LMScount++;
									commonIndex++;
									flagLMS = false;
								}
								if (!flagLMS) {
									searchData[lmsIndex].subModes.push({
										"mode" : data[y].Status,
										"count" : data[y].Count
									});
									if(data[y].Appointment && data[y].Status!=""){
										searchData[lmsIndex].subModes.push({
											"mode" : appointmentType,
											"count" : data[y].Appointment
										});
									}
									/* Existing code before adding Appointment Count on Monthly Basis for Thailand. */
									/*if(data[y].Appointment && data[y].Status!=""){
										searchData[lmsIndex].subModes
										.push({
											"mode" : appointmentType,
											"count" : data[y].Appointment
										});
									}*/
									searchData[lmsIndex].count += data[y].Count;
								}
							} else if (data[y].Type === "FNA") {
								if (flagFNA) {
									fnaIndex = commonIndex;
								}
								if (FNAcount == 0 && flagFNA) {
									searchData.push({
										"mode" : data[y].Type,
										"count" : fnamodeCount,
										"subModes" : []
									});
									FNAcount++;
									commonIndex++;
									flagFNA = false;
								}
								if (!flagFNA) {
									searchData[fnaIndex].subModes.push({
										"mode" : data[y].Status,
										"count" : data[y].Count
									});
									searchData[fnaIndex].count += data[y].Count;
								}
							} else if (data[y].Type.toLowerCase() === "illustration") {
								if (flagillustration) {
									illustrationIndex = commonIndex;
								}
								if (illustrationcount == 0 && flagillustration) {
									searchData.push({
										"mode" : data[y].Type,
										"count" : illustrationmodeCount,
										"subModes" : []
									});
									illustrationcount++;
									commonIndex++;
									flagillustration = false;
								}
								if (!flagillustration) {
									searchData[illustrationIndex].subModes.push({
										"mode" : data[y].Status,
										"count" : data[y].Count
									});
									searchData[illustrationIndex].count += data[y].Count;
								}
							}
						}
						//console.log("Appointment Count => " + appointmentCount);
						if(data.length ==  0 && appointmentCount > 0){
							searchData.push({
								"mode" : "LMS",
								"count": 0,
								"subModes":[{"mode":"New", "count":0},{"mode":"Appointment","count":appointmentCount}]
							});
						}
						successCallback(searchData);
					}, function(error) {
						errorCallback(error);
					});
				}, function(error) {
					errorCallback(error);
				});
			} else if(transactionObj.TransactionData.searchCriteriaRequest.command === 'LeadAssigned'){

				var agents="";
				for(var indx=0; indx < transactionObj.TransactionData.reportingAgents.length; indx++){
					if (indx==(transactionObj.TransactionData.reportingAgents.length-1)){
						agents=agents+"'"+transactionObj.TransactionData.reportingAgents[indx].agentCode+"'";
					}else{
						agents=agents+"'"+transactionObj.TransactionData.reportingAgents[indx].agentCode+"',";
					}
				}
				if (transactionObj.TransactionData.searchCriteriaRequest.modes !== undefined
						&& transactionObj.TransactionData.searchCriteriaRequest.modes.length > 0) {

					for (var i = 0; i < transactionObj.TransactionData.searchCriteriaRequest.modes.length; i++) {
						if (transactionObj.TransactionData.searchCriteriaRequest.modes[i] === 'LMS') {
							if(transactionObj.TransactionData.agentType=="BranchUser"){
								sql = "select MTD.*,YTD.* from (select count(*) as MTDCount from Transactions where Type='LMS'";
								sql += " and Key25 ='"
										+ transactionObj.Key25
										+ "' and Key16='Synced' and strftime('%Y',Key13) in ('"
										+ transactionObj.TransactionData.currentYear
										+ "') and strftime('%m',Key13) in ('"
										+ transactionObj.TransactionData.currentMonth
										+ "')) as MTD JOIN (select count(*) as YTDCount, Type from Transactions where Type='LMS' and Key11 in ("
										+ agents
										+ ")and Key25 ='"
										+ transactionObj.Key25
										+ "' and Key16='Synced' and strftime('%Y',Key13) in ('"
										+ transactionObj.TransactionData.currentYear
										+ "')) as YTD";
										
							} else if(transactionObj.TransactionData.agentType=="IOIS" || transactionObj.TransactionData.agentType=="Agency"){
								sql = "select MTD.*,YTD.* from (select count(*) as MTDCount from Transactions where Type='LMS'";
								sql += " and Key11='"
										+ transactionObj.Key11
										+ "' and Key25!=''"
										+ " and strftime('%Y',Key13) in ('"
										+ transactionObj.TransactionData.currentYear
										+ "') and strftime('%m',Key13) in ('"
										+ transactionObj.TransactionData.currentMonth
										+ "')) as MTD JOIN (select count(*) as YTDCount, Type from Transactions where Type='LMS' and Key11='"
										+ transactionObj.Key11
										+ "' and Key25!=''"
										+ " and strftime('%Y',Key13) in ('"
										+ transactionObj.TransactionData.currentYear
										+ "')) as YTD";
							}
						} 
					}
				}
				_dbOperations
						._executeSql(
								sql,
								function(results) {

									var data = _dbOperations
											._getRecordsArray(results);
									assignedLeadsData=[];
									for (var y = 0; y < data.length; y++) {

										if (data[y].Type === "LMS") {
												assignedLeadsData.push({
													"MTDCount" : data[y].MTDCount,
													"YTDCount" : data[y].YTDCount,
												});
										}

									}
									successCallback(assignedLeadsData);
								}, function(error) {
									errorCallback(error);
								});
			
				
			} else if(transactionObj.TransactionData.searchCriteriaRequest.command === 'LeadClosed'){
				var agents="";
				for(var indx=0; indx < transactionObj.TransactionData.reportingAgents.length; indx++){
					if (indx==(transactionObj.TransactionData.reportingAgents.length-1)){
						agents=agents+"'"+transactionObj.TransactionData.reportingAgents[indx].agentCode+"'";
					}else{
						agents=agents+"'"+transactionObj.TransactionData.reportingAgents[indx].agentCode+"',";
					}
				}
				if (transactionObj.TransactionData.searchCriteriaRequest.modes !== undefined
						&& transactionObj.TransactionData.searchCriteriaRequest.modes.length > 0) {

					for (var i = 0; i < transactionObj.TransactionData.searchCriteriaRequest.modes.length; i++) {
						if (transactionObj.TransactionData.searchCriteriaRequest.modes[i] === 'LMS') {
							if(transactionObj.TransactionData.agentType=="BranchUser"){
								sql = "select MTD.*,YTD.* from (select count(*) as MTDCount from Transactions where Type='LMS'";
								sql += " and Key25 ='"
										+ transactionObj.Key25
										+ "' and Key15='Successful Sale'"
										+ " and strftime('%Y',Key13) in ('"
										+ transactionObj.TransactionData.currentYear
										+ "') and strftime('%m',Key13) in ('"
										+ transactionObj.TransactionData.currentMonth
										+ "')) as MTD JOIN (select count(*) as YTDCount, Type from Transactions where Type='LMS' and Key11 in ("
										+ agents
										+ ")and Key25 ='"
										+ transactionObj.Key25
										+ "' and Key15='Successful Sale'"
										+ " and strftime('%Y',Key13) in ('"
										+ transactionObj.TransactionData.currentYear
										+ "')) as YTD";
										
							} else if (transactionObj.TransactionData.agentType=="IOIS" || transactionObj.TransactionData.agentType=="Agency"){
								

								sql = "select MTD.*,YTD.* from (select count(*) as MTDCount from Transactions where Type='LMS'";
								sql += " and Key11='"
										+ transactionObj.Key11
										+ "' and Key15='Successful Sale'"
										+ " and strftime('%Y',Key13) in ('"
										+ transactionObj.TransactionData.currentYear
										+ "') and strftime('%m',Key13) in ('"
										+ transactionObj.TransactionData.currentMonth
										+ "')) as MTD JOIN (select count(*) as YTDCount, Type from Transactions where Type='LMS' and Key11='"
										+ transactionObj.Key11
										+ "' and Key15='Successful Sale'"
										+ " and strftime('%Y',Key13) in ('"
										+ transactionObj.TransactionData.currentYear
										+ "')) as YTD";
							}
						} 
					}
				}
				_dbOperations
						._executeSql(
								sql,
								function(results) {

									var data = _dbOperations
											._getRecordsArray(results);
									closedLeadsData=[];
									for (var y = 0; y < data.length; y++) {

										if (data[y].Type === "LMS") {
											closedLeadsData.push({
												"MTDCount" : data[y].MTDCount,
												"YTDCount" : data[y].YTDCount,
												});
										}

									}
									successCallback(closedLeadsData);
								}, function(error) {
									errorCallback(error);
								});
			}else if(transactionObj.TransactionData.searchCriteriaRequest.command === 'APEValues'){
				var agents="";
				for(var indx=0; indx < transactionObj.TransactionData.reportingAgents.length; indx++){
					if (indx==(transactionObj.TransactionData.reportingAgents.length-1)){
						agents=agents+"'"+transactionObj.TransactionData.reportingAgents[indx].agentCode+"'";
					}else{
						agents=agents+"'"+transactionObj.TransactionData.reportingAgents[indx].agentCode+"',";
					}
				}
				if (transactionObj.TransactionData.searchCriteriaRequest.modes !== undefined
						&& transactionObj.TransactionData.searchCriteriaRequest.modes.length > 0) {

					for (var i = 0; i < transactionObj.TransactionData.searchCriteriaRequest.modes.length; i++) {
						if (transactionObj.TransactionData.searchCriteriaRequest.modes[i] === 'LMS') {
							if(transactionObj.TransactionData.agentType=="BranchUser"){
								sql = "select Key24 as MTDPremium, '' as YTDPremium, Type from Transactions where Type='LMS'";
								sql += " and Key25 ='"
										+ transactionObj.Key25
										+ "' and Key15='Successful Sale'"
										+ " and strftime('%Y',Key13) in ('"
										+ transactionObj.TransactionData.currentYear
										+ "') and strftime('%m',Key13) in ('"
										+ transactionObj.TransactionData.currentMonth
										+ "') UNION ALL select '' as MTDPremium, Key24 as YTDPremium, Type  from Transactions where Type='LMS' and Key11 in ("
										+ agents
										+ ")and Key25 ='"
										+ transactionObj.Key25
										+ "' and Key15='Successful Sale'"
										+ " and strftime('%Y',Key13) in ('"
										+ transactionObj.TransactionData.currentYear
										+ "')";
										
							} else if (transactionObj.TransactionData.agentType=="IOIS" || transactionObj.TransactionData.agentType=="Agency"){
								

								sql = "select Key24 as MTDPremium, '' as YTDPremium, Type from Transactions where Type='LMS'";
								sql += " and Key11='"
										+ transactionObj.Key11
										+ "' and Key15='Successful Sale'"
										+ " and strftime('%Y',Key13) in ('"
										+ transactionObj.TransactionData.currentYear
										+ "') and strftime('%m',Key13) in ('"
										+ transactionObj.TransactionData.currentMonth
										+ "') UNION ALL select '' as MTDPremium, Key24 as YTDPremium, Type  from Transactions where Type='LMS' and Key11='"
										+ transactionObj.Key11
										+ "' and Key15='Successful Sale'"
										+ " and strftime('%Y',Key13) in ('"
										+ transactionObj.TransactionData.currentYear
										+ "')";
							}
						} 
					}
				}
				_dbOperations
						._executeSql(
								sql,
								function(results) {

									var data = _dbOperations
											._getRecordsArray(results);
									APEValues=[];
									var totalMTDPremium=0;
									var totalYTDPremium=0;
									for (var y = 0; y < data.length; y++) {
										if (data[y].Type === "LMS") {
											if (data[y].MTDPremium!='undefined' && data[y].MTDPremium!="" ){
												totalMTDPremium= totalMTDPremium+ (Number(data[y].MTDPremium)/1000000);
											}
											if (data[y].YTDPremium!='undefined' && data[y].YTDPremium!="" ){
												totalYTDPremium= totalYTDPremium+ (Number(data[y].YTDPremium)/1000000);
											}
										}
									}
									APEValues.push({
										"MTDPremium" : totalMTDPremium.toFixed(2),
										"YTDPremium" : totalYTDPremium.toFixed(2)
										});
									successCallback(APEValues);
								}, function(error) {
									errorCallback(error);
								});
			}
		} catch (exception) {
			errorCallback(exception);
		}
	};
	
	DbOperationsUtility["getFilteredListing"] = function(transactionObj, successCallback,
			errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				if(transactionObj.loginType == "BranchUser"){
					dataObj.filter = "Key25 = '" + transactionObj.Key25 + "'";
				}else{
					dataObj.filter = "Key11 = '" + transactionObj.Key11 + "'";
				}
				var searchvalue = transactionObj.TransactionData.searchCriteriaRequest.value;
				if (transactionObj.TransactionData.searchCriteriaRequest.command === 'ListingDashBoard') {
					if (transactionObj.TransactionData.searchCriteriaRequest.modes !== undefined
							&& transactionObj.TransactionData.searchCriteriaRequest.modes.length > 0) {
						if (searchvalue != undefined
								&& searchvalue !== ""
								&& transactionObj.TransactionData.searchCriteriaRequest.modes[0] === 'LMS') {
							dataObj.filter += " AND ((UPPER(Key2) LIKE '%"
									+ searchvalue + "%' or UPPER(Key2)=UPPER('"
									+ searchvalue
									+ "')) or (UPPER(Key3) LIKE '%"
									+ searchvalue + "%' or UPPER(Key3)=UPPER('"
									+ searchvalue
									+ "')) or (UPPER(Key4) LIKE '%"
									+ searchvalue + "%' or UPPER(Key4)=UPPER('"
									+ searchvalue + "')))";
						} else if (searchvalue != undefined
								&& searchvalue !== ""
								&& transactionObj.TransactionData.searchCriteriaRequest.modes[0] === 'FNA') {
							dataObj.filter += " AND ((UPPER(Key6) LIKE '%"
									+ searchvalue + "%' or UPPER(Key6)=UPPER('"
									+ searchvalue
									+ "')) or (UPPER(Key8) LIKE '%"
									+ searchvalue + "%' or UPPER(Key8)=UPPER('"
									+ searchvalue + "')))";
						} else if (searchvalue != undefined
								&& searchvalue !== ""
								&& transactionObj.TransactionData.searchCriteriaRequest.modes[0]
										.toLowerCase() === 'illustration') {
							dataObj.filter += " AND ((UPPER(Key6) LIKE '%"
									+ searchvalue + "%' or UPPER(Key6)=UPPER('"
									+ searchvalue
									+ "')) or (UPPER(Key8) LIKE '%"
									+ searchvalue + "%' or UPPER(Key8)=UPPER('"
									+ searchvalue + "')))";
						} else if (searchvalue != undefined
								&& searchvalue !== ""
								&& transactionObj.TransactionData.searchCriteriaRequest.modes[0]
										.toLowerCase() === 'eapp') {
							dataObj.filter += " AND ((UPPER(Key6) LIKE '%"
									+ searchvalue + "%' or UPPER(Key6)=UPPER('"
									+ searchvalue
									+ "')) or (UPPER(Key8) LIKE '%"
									+ searchvalue + "%' or UPPER(Key8)=UPPER('"
									+ searchvalue + "')))"
						}
					}
				}

				if ((transactionObj.Type == "FNA")
						&& transactionObj.Key2 != null
						&& transactionObj.Key2 != "") {
					dataObj.Key2 = transactionObj.Key2;
				} else if (transactionObj.Type.toLowerCase() == "illustration"
						&& transactionObj.Key3 != null
						&& transactionObj.Key3 != "") {
					dataObj.Key3 = transactionObj.Key3;
				} else if (transactionObj.Type.toLowerCase() == "eapp"
						&& transactionObj.Key4 != null
						&& transactionObj.Key4 != "") {
					dataObj.Key4 = transactionObj.Key4;
				} else if (transactionObj.Id != null && transactionObj.Id != "") {
					dataObj.Id = transactionObj.Id;
				}
				if (transactionObj.Type != null && transactionObj.Type != "") {
					dataObj.Type = transactionObj.Type;
				}
				if (transactionObj.filter != null
						&& transactionObj.filter != "") {
					if (dataObj.filter !== undefined) {
						dataObj.filter += " AND " + transactionObj.filter;
					} else {
						dataObj.filter = transactionObj.filter;
					}

				}

				var fields = '';
				switch (transactionObj.Key10) {
				// this wil help to select custom fields
				case "Custom":
					if (transactionObj.fields != null
							&& transactionObj.fields != "") {
						fields = transactionObj.fields;
					} else {
						if (transactionObj.Type == "LMS") {
							fields = rootConfig.transactionTableBasicLMSFields;
						} else {
							fields = rootConfig.transactionTableBasicFields;
						}
					}

					fields = fields
							+ ",(select count(*) from Transactions T1 where T1.Key1=T.TransTrackingID and Type ='eApp' and Key15 <> 'Cancelled') as eAppCount ,(select count(*) from Transactions T1 where T1.Key1=T.TransTrackingID and Type ='illustration' and Key15 <> 'Cancelled') as illustrationCount,(select count(*) from Transactions T1 where T1.Key1=T.TransTrackingID and Type ='FNA' and (Key15 <> 'Cancelled' and Key15 <> 'Draft')) as fnaCount";

					break;
				// Selects all feilds except the transaction data
				case "BasicDetails":
					if (transactionObj.Type == "LMS") {
						fields = rootConfig.transactionTableBasicLMSFields;
					} else {
						fields = rootConfig.transactionTableBasicFields;
					}
					break;
				// Selects entire fields
				case "FullDetails":
					fields = " * ";
					break;
				default:
					break;
				// Do nothing
				}

				sql = _dbOperations._buildFetchScript(fields, "Transactions T",
						dataObj);
				sql = sql + " ORDER BY Key14 DESC";
				//console.log('sql ' + sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);

				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
	};

	DbOperationsUtility['getParentDetails'] = function(parentId, successCallback, errorCallback) {

			try {
				var self = this;
				var fields = rootConfig.lmsParentDetailsKeys;
				var dataObj = {};
				var sql = _dbOperations._buildFetchScript(fields,
						"Transactions", dataObj);
				sql = sql + " where Type = 'LMS' and Key1='" + parentId+"'";

				_dbOperations._executeSql(sql, function(results) {

					var data = _dbOperations._getRecordsArray(results);

					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}

		};
	
	DbOperationsUtility['callToUpdateBPMUpload'] = function(keyForBPM, isSubmissionPending, successCallback, errorCallback,
				options, $http) {
		var getUrl = "";
		if(isSubmissionPending){
			getUrl = rootConfig.serviceBaseUrl + "generaliService/resubmit/"+keyForBPM;
		} else{
			getUrl = rootConfig.serviceBaseUrl + "bpmService/submit/"+keyForBPM;
		}
			var request = {
					method : 'GET',
					url : getUrl,
					headers : {
						'Content-Type' : "application/json; charset=utf-8",
						"Token" : options.headers.Token,
						'SOURCE': 200
					}
				}

				$http(request).success(function(data, status, headers, config) {
						successCallback();
				}).error(function(data, status, headers, config) {
					errorCallback();
				});
	};
	
	DbOperationsUtility['getAllDocsToResubmit'] = function(docStatus,transtrackingId,successCallback,errorCallback){
			try {
				var dataObj = {};
				dataObj.status = docStatus;
				dataObj.identifier = transtrackingId;
				sql = _dbOperations._buildFetchScript(rootConfig.requirementTableFetchFields, "RequirementFiles",
						dataObj);

				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		};

	DbOperationsUtility['generateBPMEmailPDF'] = function(transactionObj, successCallback, errorCallback,
			options, $http) {
		var self = this;
		var generateBPMPdfUrl = rootConfig.serviceBaseUrl + "generaliService/generateBPMEmailPdf";

		var requestInfo = Request();
		requestInfo.Request.RequestPayload.Transactions
				.push(transactionObj);

		var request = {
			method : 'POST',
			url : generateBPMPdfUrl,
			headers : {
				'Content-Type' : "application/json; charset=utf-8",
				"Token" : options.headers.Token
			},
			data : requestInfo
		}

		$http(request)
				.success(
						function(data, status, headers, config) {
							successCallback(status);
						}).error(
						function(data, status, headers, config) {
							errorCallback(data, status, resources.errorMessage);
						});
	};
	
	DbOperationsUtility['updateTransactionAfterBPMEmailPDF'] = function(transactionData, 
			transactionId, successCallback, errorCallback) {
		try {
			var self = this;
			sql = _dbOperations._buildUpdateScript("Transactions",
					transactionData);
			sql += " WHERE 	Id in (" + transactionId + ");";
			_dbOperations._executeSql(sql, function(results) {
					sql = " DELETE FROM new_elem WHERE Id in ("
						+ transactionId + ");";
				_dbOperations._executeSql(sql, function(results) {
					successCallback(results);
				}, function(error) {
					errorCallback(error);
				});
			}, function(error) {
				errorCallback(error);
			});
		} catch (error) {
			errorCallback(error);
		}

	};

	DbOperationsUtility['getRelatedIllustrationForLMS'] = function(lmsId,
			successCallback, errorCallback) {
		try {
			var self = this;
			sql = _dbOperations._buildFetchScript("*", "Transactions", {})
					+ " WHERE (Type = 'FNA' AND Key1 = '" + lmsId + "')";
			//console.log(sql);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				$.each(data, function(i, obj) {
					var formattedData = JSON
							.parse(unescape(obj.TransactionData));
					obj.TransactionData = formattedData;
				});
				successCallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}

	};
	
	DbOperationsUtility['getRelatedLMS'] = function(leadId, successCallback, errorCallback) {
			try {
				var self = this;
				sql = _dbOperations._buildFetchScript("*", "Transactions", {})
						+ " WHERE Type = 'LMS' AND TransTrackingID = '" + leadId
						+ "'";
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					$.each(data, function(i, obj) {
						var formattedData = JSON
								.parse(unescape(obj.TransactionData));
						obj.TransactionData = formattedData;
					});
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		};

	DbOperationsUtility['getRelatedFNAForLMS'] = function(lmsId,
			successCallback, errorCallback) {
		try {
			var self = this;
			sql = _dbOperations._buildFetchScript("*", "Transactions", {})
					+ " WHERE (Type = 'FNA' AND Key1 = '" + lmsId + "')";
			//console.log(sql);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				$.each(data, function(i, obj) {
					var formattedData = JSON
							.parse(unescape(obj.TransactionData));
					obj.TransactionData = formattedData;
				});
				successCallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	};
	
	/**/
	DbOperationsUtility['getRelatedTransactionsToSync'] = function( lmsId, fnaId, biId,
			successCallback, errorCallback) {
		try {
			var self = this;
			sql = _dbOperations._buildFetchScript("*", "Transactions", {})
					+ " WHERE (Type = 'LMS' AND TransTrackingId = '" + lmsId + "' AND Key16 != 'Synced' )" +
							" OR (Type = 'FNA' AND TransTrackingId = '" + fnaId + "' AND Key16 != 'Synced' )" +
							" OR (Type = 'illustration' AND TransTrackingId = '" + biId + "' AND Key16 != 'Synced')";
			console.log(sql);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				var relatedTrans = {};
				$.each(data, function(i, obj) {
					/*var formattedData = JSON
							.parse(unescape(obj.TransactionData));
					obj.TransactionData = formattedData;*/
					if( obj.Type == "LMS" ){
						relatedTrans.LMS = obj.Id;
					}else if( obj.Type == "FNA" ){
						relatedTrans.FNA = obj.Id;
					}else if( obj.Type == "illustration" ){
						relatedTrans.illustration = obj.Id +"##" + obj.Key16;
					}
				});
				delete data;
				successCallback(relatedTrans);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	};
	/**/
	
	DbOperationsUtility['getRelatedEappForBI'] = function(transTrackingId,
			successCallback, errorCallback) {
		try {
			var self = this;
			sql = _dbOperations._buildFetchScript("*", "Transactions", {})
					+ " WHERE (Type = 'eApp' AND Key3 = '" + transTrackingId + "' AND Key15  <> 'Cancelled')";
			//console.log(sql);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				$.each(data, function(i, obj) {
					var formattedData = JSON
							.parse(unescape(obj.TransactionData));
					obj.TransactionData = formattedData;
				});
				successCallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	};
	
	
	DbOperationsUtility['getRelatedEappForLMS'] = function(transTrackingId,
			successCallback, errorCallback) {
		try {
			var self = this;
			sql = _dbOperations._buildFetchScript("*", "Transactions", {})
					+ " WHERE (Type = 'eApp' AND Key1 = '" + transTrackingId + "' AND Key15  <> 'Cancelled')";
			//console.log(sql);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				$.each(data, function(i, obj) {
					var formattedData = JSON
							.parse(unescape(obj.TransactionData));
					obj.TransactionData = formattedData;
				});
				successCallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	};
	
	DbOperationsUtility['getRelatedBIForEapp'] = function(transTrackingId,
			successCallback, errorCallback) {
		try {
			var self = this;
			sql = _dbOperations._buildFetchScript("*", "Transactions", {})
					+ " WHERE (Type = 'illustration' AND TransTrackingID = '" + transTrackingId + "' AND Key15  <> 'Cancelled')";
			//console.log(sql);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				$.each(data, function(i, obj) {
					var formattedData = JSON
							.parse(unescape(obj.TransactionData));
					obj.TransactionData = formattedData;
				});
				successCallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	};

	DbOperationsUtility['downloadPDF'] = function(transactionObj, templateId,
			successCallback, errorCallback, options, $http) {

			var type = transactionObj.Type;
				if (type == "eApp") {
					transactionObj.TransactionData.Product.templates.eAppPdfTemplate = templateId;
				}else{
					transactionObj.TransactionData.Product.templates.illustrationPdfTemplate = templateId;
				}
		
		var downloadPdfUrl = rootConfig.serviceBaseUrl
				+ "documentService/getbase64stringpdf";

		var requestInfo = Request();
		requestInfo.Request.RequestPayload.Transactions.push(transactionObj);

		var request = {
			method : 'POST',
			url : downloadPdfUrl,
			headers : {
				'Content-Type' : "application/json; charset=utf-8",
				"Token" : options.headers.Token
			},
			data : requestInfo
		}

		$http(request)
				.success(
						function(data, status, headers, config) {
							if (navigator.userAgent.indexOf("iPad") > 0) {
								var base64 = data.Response.ResponsePayload.Transactions[0].base64pdf;
								var base64pdf = "data:application/pdf;base64,"
										+ base64;
								cordova.exec(function() {
									//alert('file opened successfully');
									//console.log('file opened successfully');
								}, function(args) {
									//console.log('Error');
									//console.log(angular.toJson(args));
								}, 'FileOpener2', 'open', [ base64pdf,
										'application/pdf' ]);

							} else {
								var base64pdf = data.Response.ResponsePayload.Transactions[0].base64pdf;
								if(transactionObj.Type==="eApp"){
                                    cordova.exec(function() {
								    }, function() {
								    }, "PdfViewer", "downloadPdf", [ base64pdf,
										transactionObj.Key4,
										rootConfig.downloadFolder ]);
                                }else{
                                    cordova.exec(function() {
								    }, function() {
								    }, "PdfViewer", "downloadPdf", [ base64pdf,
										transactionObj.Key3,
										rootConfig.downloadFolder ]);
                                }
								
							}
							transactionObj.TransactionData.IllustrationOutput.base64PdfString = base64pdf;
							successCallback(transactionObj);
						}).error(
						function(data, status, headers, config) {
							//console.log('error' + JSON.stringify(data)+ 'status' + status);
									
							errorCallback(data, status);
						});
	};

	DbOperationsUtility['printPDF'] = function(transactionObj, templateId,
			successCallback, errorCallback, options, $http) {
		transactionObj.TransactionData.Product.templates.illustrationPdfTemplate = templateId;
		var downloadPdfUrl = rootConfig.serviceBaseUrl
				+ "documentService/getbase64stringpdf";

		var requestInfo = Request();
		requestInfo.Request.RequestPayload.Transactions.push(transactionObj);

		var request = {
			method : 'POST',
			url : downloadPdfUrl,
			headers : {
				'Content-Type' : "application/json; charset=utf-8",
				"Token" : options.headers.Token
			},
			data : requestInfo
		}

		$http(request)
				.success(
						function(data, status, headers, config) {
							if (navigator.userAgent.indexOf("iPad") > 0) {
								var base64 = data.Response.ResponsePayload.Transactions[0].base64pdf;
								var base64pdf = "data:application/pdf;base64,"
										+ base64;
								cordova.exec(function() {
									//alert('file opened successfully');
									//console.log('file opened successfully');
								}, function(args) {
									//console.log('Error');
									//console.log(angular.toJson(args));
								}, 'FileOpener2', 'open', [ base64pdf,
										'application/pdf' ]);

							} else {

								var base64pdf = data.Response.ResponsePayload.Transactions[0].base64pdf;
								cordova.exec(function() {
								}, function() {
								}, "PdfViewer", "printPdf", [ base64pdf,
										transactionObj.Key3,
										rootConfig.downloadFolder ]);

							}
							transactionObj.TransactionData.IllustrationOutput.base64PdfString = base64pdf;
							successCallback(transactionObj);
						}).error(
						function(data, status, headers, config) {
							//console.log('error' + JSON.stringify(data)+ 'status' + status);
									
							errorCallback(data, status);
						});
	};

	DbOperationsUtility['isDuplicateLead'] = function(transactionData,
			successCallback, errorCallback) {
		try {
			var dataObj = {};
            var count=1;
			var isDuplicate = false;
			var Id = parseInt(transactionData.Id);
			
			if(transactionData.Key11!=undefined){
			dataObj.Key11 = transactionData.Key11;
			}
			
			dataObj.Key4 = transactionData.Key4;
			
			if(transactionData.Key23!=undefined){
				dataObj.Key23 = transactionData.Key23;
			}
				
			dataObj.filter = "Key15  NOT IN('Successful Sale','Cancelled')";
			var sql = _dbOperations._buildFetchScript("Id", "Transactions",
					dataObj);
			_dbOperations._executeSql(sql, function(results) {

				if (results.rows.length !== 0) {
					var data = _dbOperations._getRecordsArray(results);
					for ( var i = 0; i < data.length; i++) {
                        if (Id !== data[i].Id && dataObj.Key4!=="" && dataObj.Key4!==undefined) {
							count++;
						}
						if (Id !== data[i].Id && count>5 && dataObj.Key4!=="" && dataObj.Key4!==undefined) {
							isDuplicate = true;
						}
					}
				}
				successCallback(isDuplicate);

			}, function(error) {
				errorCallback(error);
			});

		} catch (exception) {
			errorCallback(exception);
		}
	};
	DbOperationsUtility['retrieveSPAJNumbers'] = function(transactionObj, successCallback, errorCallback, options, $http) {
		try {
				var getUrl = rootConfig.serviceBaseUrl + "generaliService/generateSpaj";
				var requestInfo = Request();
				requestInfo.Request.RequestPayload.Transactions
						.push(transactionObj);
			 var request= {
				method : 'POST',
				url : getUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" :  options.headers.Token
				},
				data : requestInfo
			}

			$http(request).success(function(data, status, headers, config) {
				if (data.Response.ResponsePayload.Transactions)
					successCallback(data.Response.ResponsePayload.Transactions[0].TransactionData);
				else
					successCallback(data.Response.ResponsePayload);
			}).error(function(data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});

			} catch (exception) {
				alert("error in  retrieving SPAJ: " + angular.toJson(exception)
						+ " :error in  retrieving SPAJ");
				errorCallback(exception);
			}
		};
	DbOperationsUtility['retrieveData'] =function(selectCol, tableName,additionalClause, dataObj,sucesscallback,errorCallback) {
		try {
			var sql = _dbOperations._buildFetchScript("*", tableName, dataObj);
			if(additionalClause!=''){
				sql =sql+additionalClause;
			}
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				sucesscallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	};
	
	DbOperationsUtility['saveReportingAgentsDetails'] =function(reportingList, successCallback, errorCallback) {
				/*
					* has   reporting agent list , success and error call back as parameters
					* initially delete all the  reporting Agents exist in the DB against the current ParentAgentCode 
					* on success delete operation followed by a batch insert operation of reporting agents
				*/
		try {
			 var self = this;
			 var deleteWhereClause="parentAgentCode = '"+reportingList[0].parentAgentCode+"'";
			 
				var sql = _dbOperations._buildDeleteScript("ReportingAgents",deleteWhereClause);		
				_dbOperations._runSql(sql, function(data) {
					
						var sql = _dbOperations._buildBatchInsertScript("ReportingAgents",
								reportingList);
						_dbOperations._runSql(sql, function(data) {
							successCallback(data);
						}, function(error) {
							errorCallback(error);
						});	
				}, function(error) {
					errorCallback(error);
				});							
			} catch (exception) {
				errorCallback(exception);
			}

	};
    
    DbOperationsUtility['saveApplicableProductDetails'] =function(productList, successCallback, errorCallback) {
		try {
			 var self = this;
			 var deleteWhereClause="parentAgentCode = '"+productList[0].parentAgentCode+"'";
			 
				var sql = _dbOperations._buildDeleteScript("ApplicableProducts",deleteWhereClause);		
				_dbOperations._runSql(sql, function(data) {
					
						var sql = _dbOperations._buildBatchInsertScript("ApplicableProducts",
								productList);
						_dbOperations._runSql(sql, function(data) {
							successCallback(data);
						}, function(error) {
							errorCallback(error);
						});	
				}, function(error) {
					errorCallback(error);
				});							
			} catch (exception) {
				errorCallback(exception);
			}

	};
	
	DbOperationsUtility['retrieveApplicableProductDetails'] =function(ParentAgentCode,sucesscallback,errorCallback) {
		try {
			var sql = _dbOperations._buildFetchScript("*", "ApplicableProducts", ParentAgentCode);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				sucesscallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	};
	
	DbOperationsUtility['retrieveReportingAgentsDetails'] =function(ParentAgentCode,sucesscallback,errorCallback) {
		try {
			var sql = _dbOperations._buildFetchScript("*", "ReportingAgents", ParentAgentCode);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				sucesscallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	};
	
	DbOperationsUtility['saveHierarchyResponse'] =function(hierarchyResponse, successCallback, errorCallback)  {
			try {
				var self = this;
					sql = _dbOperations._buildInsertScript("Hierarchy",
							hierarchyResponse);
					_dbOperations._runSql(sql, function(data) {
						//var transId = data;
						//self.insertStatusRec(transId, successCallback,
								//errorCallback);
					}, function(error) {
						errorCallback(error);
					});
				

			} catch (exception) {
				errorCallback(exception);
			}
		};
		
		DbOperationsUtility['retrieveHierarchyResponse'] =function(AgentCode,sucesscallback,errorCallback) {
		try {
			var sql = _dbOperations._buildFetchScript("*", "Hierarchy", AgentCode);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				sucesscallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	};
	
	DbOperationsUtility['saveMappedBranchesDetails'] =function(mappedBranchList, successCallback, errorCallback) {
				/*
					* has   reporting agent list , success and error call back as parameters
					* initially delete all the  reporting Agents exist in the DB against the current ParentAgentCode 
					* on success delete operation followed by a batch insert operation of reporting agents
				*/
		try {
			 var self = this;
			 var deleteWhereClause="parentAgentCode = '"+mappedBranchList[0].parentAgentCode+"'";
			 
				var sql = _dbOperations._buildDeleteScript("MappedBranches",deleteWhereClause);		
				_dbOperations._runSql(sql, function(data) {
					
						var sql = _dbOperations._buildBatchInsertScript("MappedBranches",
								mappedBranchList);
						_dbOperations._runSql(sql, function(data) {
							successCallback(data);
						}, function(error) {
							errorCallback(error);
						});	
				}, function(error) {
					errorCallback(error);
				});							
			} catch (exception) {
				errorCallback(exception);
			}

	};	
	
		DbOperationsUtility['retrieveMappedBranchesDetails'] =function(ParentAgentCode,sucesscallback,errorCallback) {
		try {
			var sql = _dbOperations._buildFetchScript("*", "MappedBranches", ParentAgentCode);
			_dbOperations._executeSql(sql, function(results) {
				var data = _dbOperations._getRecordsArray(results);
				sucesscallback(data);
			}, function(error) {
				errorCallback(error);
			});
		} catch (exception) {
			errorCallback(exception);
		}
	};
	DbOperationsUtility['saveReportingRM'] =function(reportingRMList, successCallback, errorCallback) {
    		try {
    			 var self = this;
    			 var deleteWhereClause="parentAgentCode = '"+reportingRMList[0].parentAgentCode+"'";
    			 	var sql = _dbOperations._buildDeleteScript("ReportingRM",deleteWhereClause);
    				_dbOperations._executeSql(sql, function(data) {
    						var sql = _dbOperations._buildBatchInsertScript("ReportingRM",reportingRMList);
    						_dbOperations._executeSql(sql, function(data) {
    							successCallback(data);
    						}, function(error) {
    							errorCallback(error);
    						});
    			 	}, function(error) {
    					errorCallback(error);
    				});
    			} catch (exception) {
    				errorCallback(exception);
    			}
	};
	DbOperationsUtility['retrieveReportingRM'] =function(ParentAgentCode,sucesscallback,errorCallback) {
    		try {
    			var sql = _dbOperations._buildFetchScript("*", "ReportingRM", ParentAgentCode);
    			_dbOperations._executeSql(sql, function(results) {
    				var data = _dbOperations._getRecordsArray(results);
    				sucesscallback(data);
    			}, function(error) {
    				errorCallback(error);
    			});
    		} catch (exception) {
    			errorCallback(exception);
    		}
    };
	DbOperationsUtility['saveSPAJDetails'] =function(SPAJNo,agentId, successCallback, errorCallback) {

		try {
			var self = this;
			var data;
			for(i = 0; i< SPAJNo.length ; i++){
				var dataObj = {};
				dataObj.agentId = agentId;
				dataObj.SPAJNo = SPAJNo[i];
				var sql = _dbOperations._buildInsertScript("GeneraliSPAJNo", dataObj);
				_dbOperations._executeSql(sql, function(results) {
					data = _dbOperations._getRecordsArray(results);
					successCallback();
				}, function(error) {
					errorCallback(error);
				});
			}
		} catch (exception) {
			errorCallback(exception);
		}

	};
	DbOperationsUtility['retrieveSPAJCount'] =function(agentId, successCallback, errorCallback) {
		try {
			var self = this;
			var dataObj = {};
			dataObj.agentId = agentId;
			self.retrieveData("*", 'GeneraliSPAJNo','', dataObj,function(retrieveData){
				successCallback(retrieveData.length);
			},errorCallback);
			
		} catch (exception) {
			errorCallback(exception);
		}
	};
	DbOperationsUtility['isSpajExisting'] =function(transactionObj, successCallback, errorCallback,options, $http) {
		/*transactionObj.Key16 = "SUCCESS";
		successCallback(transactionObj);*/
		try {
				var getUrl = rootConfig.serviceBaseUrl + "generaliService/isSpajExisting";
				var requestInfo = Request();
				requestInfo.Request.RequestPayload.Transactions.push(transactionObj);
				var request = {
				method : 'POST',
				url : getUrl,
				    headers : {
					  'Content-Type' : "application/json; charset=utf-8",
					  "Token" : options.headers.Token
				    },
				   data : requestInfo
			    }

			$http(request).success(function(data, status, headers, config) {
				if (data.Response.ResponsePayload.Transactions)
					successCallback(data.Response.ResponsePayload.Transactions[0]);
				else
					successCallback(data.Response.ResponsePayload);
			}).error(function(data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
			
			
		} catch (exception) {
			errorCallback(exception);
		}

	};
	DbOperationsUtility['AgentCodeValidation'] =function(transactionObj, successCallback, errorCallback,options, $http) {
		/*transactionObj.Key16 = "SUCCESS";
		successCallback(transactionObj);*/
		try {
				var getUrl = rootConfig.serviceBaseUrl + "generaliService/validateAgentCode";
				var requestInfo = Request();
				requestInfo.Request.RequestPayload.Transactions.push(transactionObj);
				var request = {
				method : 'POST',
				url : getUrl,
				    headers : {
					  'Content-Type' : "application/json; charset=utf-8",
					  "Token" : options.headers.Token
				    },
				   data : requestInfo
			    }

			$http(request).success(function(data, status, headers, config) {
				if (data.Response.ResponsePayload.Transactions)
					successCallback(data.Response.ResponsePayload.Transactions[0]);
				else
					successCallback(data.Response.ResponsePayload);
			}).error(function(data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
			
			
		} catch (exception) {
			errorCallback(exception);
		}

	};
	DbOperationsUtility['retrieveSPAJNo'] =function(agentId, successCallback, errorCallback) {
		try {
			var self = this;
			var dataObj = {};
			dataObj.agentId = agentId;
			var clause = " ORDER BY id ASC LIMIT 1";
			self.retrieveData("*", 'GeneraliSPAJNo', clause , dataObj,function(retrieveData){
				var whereClause = "id=" + retrieveData[0].id;
                self.deleteFromTable("GeneraliSPAJNo", whereClause,
                    function() {
                		successCallback(retrieveData);
                    }, errorCallback);
			},errorCallback);
		} catch (exception) {
			errorCallback(exception);
		}

	};
	//Function to call the email for memo online
		DbOperationsUtility['memoEmailCall'] = function(transactionObj,successCallback, errorCallback,options, $http) {
			var saveUrl = rootConfig.serviceBaseUrl + "emailService/updateEmailDetails";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions = transactionObj;
			var request = {
				method : 'POST',
				url : saveUrl,
				headers : {
					'Content-Type' : "application/json; charset=utf-8",
					"Token" : options.headers.Token
				},
				data : requestInfo
			}
			$http(request).success(function(data, status, headers, config) {
				if (data.Response.ResponsePayload.Transactions[0].StatusData.StatusCode == "100") {
					successCallback(data.Response.ResponsePayload.Transactions[0]);
				} else {
					errorCallback(data.Response.ResponsePayload.Transactions[0].StatusData.StatusMessage);
				}
			}).error(function(data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
		};
		//Function to call the print for memo
		DbOperationsUtility['memoDownloadPDF'] = function(transactionObj,templateId,successCallback, errorCallback,options, $http) {
			try {
				var type = transactionObj.Type;
				var proposalNumber = "";
				if (type == "eApp") {
					proposalNumber = transactionObj.Key4;
				}else if(type == "Memo"){
					proposalNumber = transactionObj.Key21;
				}else {
					proposalNumber = transactionObj.Key3;
				}
				var cacheParamValue = (new Date()).getTime();
				var downloadPDFUrl = rootConfig.serviceBaseUrl
						+ "documentService/getpdf/" + proposalNumber + "/"
						+ type + "/" + templateId + "?cache=" + cacheParamValue;
				var gui = require('nw.gui');
				gui.Shell.openItem(downloadPDFUrl); 
				/* $("body").append(
						"<iframe src='" + downloadPDFUrl
								+ "' style='display: none;' ></iframe>"); */
				successCallback();

			} catch (exception) {
				errorCallback(exception);
			}
		};
		//Function to delete the agent saved in userdetails table while logging in
		DbOperationsUtility['deleteUserDetailsTableForAnAgent'] = function(agentId,successCallback, errorCallback) {
			try {
                sql = _dbOperations._buildDeleteScript("UserDetails","userId='" + agentId + "'");
                _dbOperations._executeSql(sql, function(data) {
                    successCallback(data);
                }, function(error) {
                    errorCallback(error);
                });
            } catch (error) {
                errorCallback(error);
            }
		};
		DbOperationsUtility['dbInit'] = function(successCallback) {
			try {
                _dbOperations._dbInit(successCallback);
                
            } catch (error) {
                errorCallback(error);
            }
		};

		//privacy law
		DbOperationsUtility['sendOTP'] = function (transactionObj, successCallback, errorCallback, options, $http) {
			var getUrl = rootConfig.serviceBaseUrl + "otpService/generateOTP";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions.push(transactionObj);
			var request = {
				method: 'POST',
				url: getUrl,
				headers: {
					'Content-Type': "application/json; charset=utf-8",
					"Token": options.headers.Token
				},
				data: requestInfo
			}
			$http(request).success(function (data, status, headers, config) {
				successCallback(data);
			}).error(function (data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
		};
		DbOperationsUtility['resendOtp'] = function (transactionObj, successCallback, errorCallback, options, $http) {
			var getUrl = rootConfig.serviceBaseUrl + "otpService/resendOTP";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions.push(transactionObj);
			var request = {
				method: 'POST',
				url: getUrl,
				headers: {
					'Content-Type': "application/json; charset=utf-8",
					"Token": options.headers.Token
				},
				data: requestInfo
			}
			$http(request).success(function (data, status, headers, config) {
				successCallback(data);
			}).error(function (data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
		};
		DbOperationsUtility['validateOTP'] = function (transactionObj, successCallback, errorCallback, options, $http) {
			var getUrl = rootConfig.serviceBaseUrl + "otpService/validateOTP";
			var requestInfo = Request();
			requestInfo.Request.RequestPayload.Transactions.push(transactionObj);
			var request = {
				method: 'POST',
				url: getUrl,
				headers: {
					'Content-Type': "application/json; charset=utf-8",
					"Token": options.headers.Token
				},
				data: requestInfo
			}
			$http(request).success(function (data, status, headers, config) {
				successCallback(data);
			}).error(function (data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
		};
		DbOperationsUtility['generateOTP'] = function (otpInput, successCallback, errorCallback, options, $http) {
			var getOTPGenerateUrl = rootConfig.generateOTPURL;
			var username = otpInput.username;
			var password = otpInput.password;
			var msg = otpInput.msg;
			var sender = otpInput.sender;
			var msisdn = otpInput.msisdn;
			var req = {
				method : 'POST',
				crossDomain : true,
				url : getOTPGenerateUrl,
				headers : {
					'Content-Type' : "application/x-www-form-urlencoded; charset=utf-8"
				},
				transformRequest : function(obj) {
					var str = [];
					for ( var p in obj)
						str.push(encodeURIComponent(p) + "="
								+ encodeURIComponent(obj[p]));
					return str.join("&");
				},
				data : {
					username : username,
					password : password,
					msg : msg,
					sender : sender,
					msisdn : msisdn
				}
			}
			
			$http(req).success(function(data, status, headers, config) {
				successCallback(data);
			}).error(function(data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
			
		};
		DbOperationsUtility['verifyOTP'] = function (otpVerifyInput, successCallback, errorCallback, options, $http) {
			var getOTPVerifyUrl = rootConfig.verifyOTPURL;
			var username = otpVerifyInput.username;
			var password = otpVerifyInput.password;
			var otp = otpVerifyInput.otp;
			var ref = otpVerifyInput.ref;
			var msisdn = otpVerifyInput.msisdn;
			var transid = otpVerifyInput.transid;
			var req = {
				method : 'POST',
				crossDomain : true,
				url : getOTPVerifyUrl,
				headers : {
					'Content-Type' : "application/x-www-form-urlencoded; charset=utf-8"
				},
				transformRequest : function(obj) {
					var str = [];
					for ( var p in obj)
						str.push(encodeURIComponent(p) + "="
								+ encodeURIComponent(obj[p]));
					return str.join("&");
				},
				data : {
					username : username,
					password : password,
					otp : otp,
					ref : ref,
					transid : transid,
					msisdn : msisdn
				}
			}
			
			$http(req).success(function(data, status, headers, config) {
				successCallback(data);
			}).error(function(data, status, headers, config) {
				errorCallback(data, status, resources.errorMessage);
			});
			
		};
		
		
		//end
		
		
	var _dbOperations = {
		_db : null,
		_dbInit : function(cb) {
			var self = this;
			self._db = new sqlite3.Database(rootConfig.configAndContentPath + '/LifeEngageData.db');
			// Add functions to db that mimic a WebSQL database
			self._db.transaction = function(callBack) {
				var tx = {};
				callBack(tx);
			};
			var tblTransactionQuery = "CREATE TABLE IF NOT EXISTS Transactions (Id INTEGER PRIMARY KEY ,Key1 TEXT NOT NULL,Key2 TEXT ,Key3 TEXT ,Key4 TEXT,Key5 TEXT NOT NULL,Key6 TEXT,Key7 TEXT,Key8 TEXT,Key9 TEXT,Key10 TEXT,Key11 TEXT NOT NULL,Key12 TEXT,Key13 TEXT,Key14 TEXT,Key15 TEXT,Key16 TEXT,Key17 TEXT,Key18 TEXT,Key19 TEXT,Key20 TEXT,Key21 TEXT,Key22 TEXT,Key23 TEXT,Key24 TEXT,Key25 TEXT,Type TEXT,TransactionData TEXT,TransTrackingID INTEGER)";
			self._executeSql(tblTransactionQuery,function(){
				var tblTransactionMetadataQuery = "PRAGMA table_info(Transactions);";
				self._executeSql(tblTransactionMetadataQuery,function(result) {
					var data = _dbOperations._getRecordsArray(result);
					var isColumnFlag = false;
					var isKey31Flag = false;
					var isKey32Flag = false;
					var isKey33Flag = false;
					var isKey34Flag = false;
					var isKey35Flag = false;
					var isKey36Flag = false;
					var isKey37Flag = false;
					var isKey38Flag = false;
					var isKey39Flag = false;
					var isKey40Flag = false;
					$.each(data, function(i, obj) {
						if (obj.name == "TransTrackingID") {
							isColumnFlag = true;
						}
						if (obj.name == "Key31") {
							isKey31Flag = true;
						}
						if (obj.name == "Key32") {
							isKey32Flag = true;
						}
						if (obj.name == "Key33") {
							isKey33Flag = true;
						}
						if (obj.name == "Key34") {
							isKey34Flag = true;
						}
						if (obj.name == "Key35") {
							isKey35Flag = true;
						}
						if (obj.name == "Key36") {
							isKey36Flag = true;
						}
						if (obj.name == "Key37") {
							isKey37Flag = true;
						}
						if (obj.name == "Key38") {
							isKey38Flag = true;
						}
						if (obj.name == "Key39") {
							isKey39Flag = true;
						}
						if (obj.name == "Key40") {
							isKey40Flag = true;
						}
					});
					if (!isColumnFlag) {
						var tblTransactionAlterQuery = "ALTER TABLE Transactions ADD TransTrackingID INTEGER";
						self._executeSql(tblTransactionAlterQuery);
					}
					if (!isKey31Flag) {
						var tblTransactionKey31AlterQuery = "ALTER TABLE Transactions ADD Key31 TEXT";
						self._executeSql(tblTransactionKey31AlterQuery);
					}
					if (!isKey32Flag) {
						var tblTransactionKey32AlterQuery = "ALTER TABLE Transactions ADD Key32 TEXT";
						self._executeSql(tblTransactionKey32AlterQuery);
					}
					if (!isKey33Flag) {
						var tblTransactionKey33AlterQuery = "ALTER TABLE Transactions ADD Key33 TEXT";
						self._executeSql(tblTransactionKey33AlterQuery);
					}
					if (!isKey34Flag) {
						var tblTransactionKey34AlterQuery = "ALTER TABLE Transactions ADD Key34 TEXT";
						self._executeSql(tblTransactionKey34AlterQuery);
					}
					if (!isKey35Flag) {
						var tblTransactionKey35AlterQuery = "ALTER TABLE Transactions ADD Key35 TEXT";
						self._executeSql(tblTransactionKey35AlterQuery);
					}
					if (!isKey36Flag) {
						var tblTransactionKey36AlterQuery = "ALTER TABLE Transactions ADD Key36 TEXT";
						self._executeSql(tblTransactionKey36AlterQuery);
					}
					if (!isKey37Flag) {
						var tblTransactionKey37AlterQuery = "ALTER TABLE Transactions ADD Key37 TEXT";
						self._executeSql(tblTransactionKey37AlterQuery);
					}
					if (!isKey38Flag) {
						var tblTransactionKey38AlterQuery = "ALTER TABLE Transactions ADD Key38 TEXT";
						self._executeSql(tblTransactionKey38AlterQuery);
					}
					if (!isKey39Flag) {
						var tblTransactionKey39AlterQuery = "ALTER TABLE Transactions ADD Key39 TEXT";
						self._executeSql(tblTransactionKey39AlterQuery);
					}
					if (!isKey40Flag) {
						var tblTransactionKey40AlterQuery = "ALTER TABLE Transactions ADD Key40 TEXT";
						self._executeSql(tblTransactionKey40AlterQuery);
					}
					var tblTransactionMetadataQuery = "PRAGMA table_info(AGENT);";
					self._executeSql(tblTransactionMetadataQuery,function(result) {
						var data = _dbOperations._getRecordsArray(result);
						var gaoNameFlag = false;
						var managerNameFlag = false;
						var licExpDateFlag = false;
						var securityNoFlag = false;
							$.each(data, function(i, obj) {
								if (obj.name == "gaoName") {
									gaoNameFlag = true;
								}
								if (obj.name == "managerName") {
									managerNameFlag = true;
								}
								if (obj.name == "licExpDate") {
									licExpDateFlag = true;
								}
								if (obj.name == "securityNo"){
									securityNoFlag = true;
								}
								
						});
						if (!gaoNameFlag) {
							var tblAgentGAONameAlterQuery = "ALTER TABLE AGENT ADD gaoName TEXT";
							self._executeSql(tblAgentGAONameAlterQuery);
						}
						if (!managerNameFlag) {
							var tblAgentManagerNameAlterQuery = "ALTER TABLE AGENT ADD managerName TEXT";
							self._executeSql(tblAgentManagerNameAlterQuery);
						}
						if (!licExpDateFlag) {
							var tblAgentLicExpDateAlterQuery = "ALTER TABLE AGENT ADD licExpDate TEXT";
							self._executeSql(tblAgentLicExpDateAlterQuery);
						}
						if (!securityNoFlag) {
							var tblAgentSecurityNoAlterQuery = "ALTER TABLE AGENT ADD securityNo TEXT";
							self._executeSql(tblAgentSecurityNoAlterQuery);
						}
						var tblAttachmentsQuery = "CREATE TABLE IF NOT EXISTS Eapp_Attachments (Id INTEGER PRIMARY KEY,ParentId TEXT,DocumentType TEXT,DocumentName TEXT,DocumentStatus TEXT,Date TEXT,DocumentDescription TEXT,DocumentObject TEXT)";
					self._executeSql(tblAttachmentsQuery,function(){
						var tblSyncStatusQuery = "CREATE TABLE IF NOT EXISTS SyncStatus (Id INTEGER PRIMARY KEY,RecordId INTEGER,SynAttemptTS TEXT,SyncResponseStatus TEXT,SyncResponseStatusDesc TEXT,SyncResponseTS TEXT)";
						self._executeSql(tblSyncStatusQuery,function(){
							var tblAgentQuery = "CREATE TABLE IF NOT EXISTS AGENT (Id INTEGER PRIMARY KEY,agentCode TEXT,agentDesc TEXT,agentName TEXT,designation TEXT,userId TEXT,employeeType TEXT,supervisorCode TEXT,office TEXT,unit TEXT,agentGroup TEXT,yearsofExperience TEXT,businessSourced TEXT,customerServicing TEXT,licenseNumber TEXT,agentLicensestartdate TEXT,agentLicenseExpDate TEXT,emailId TEXT,mobileNumber TEXT)";
							self._executeSql(tblAgentQuery,function(){
								var tblAchievementQuery = "CREATE TABLE IF NOT EXISTS ACHIEVEMENT (Id INTEGER PRIMARY KEY ,agentId TEXT,achievementCode TEXT,title TEXT,description TEXT)";
								self._executeSql(tblAchievementQuery,function(){
									var tblMappedBranchesQuery = "CREATE TABLE IF NOT EXISTS MappedBranches (branchCode TEXT, branchName TEXT, parentAgentCode TEXT);";
									self._executeSql(tblMappedBranchesQuery,function(){
										var tblReportingRMQuery = "CREATE TABLE IF NOT EXISTS ReportingRM (rmCode TEXT, rmName TEXT, parentAgentCode TEXT);";
										self._executeSql(tblReportingRMQuery,function(){
											var tblMappedBranchesMetadataQuery = "PRAGMA table_info(MappedBranches);";
											self._executeSql(tblMappedBranchesMetadataQuery,function(result) {
												var data = _dbOperations._getRecordsArray(result);
												isRadiusColumnFlag = false;
												isLongitudeColumnFlag = false;
												isLatitudeColumnFlag = false;
												$.each(data, function(i, obj) {
													if (obj.name == "radius") {
														isRadiusColumnFlag = true;
													}
													if (obj.name == "longitude") {
														isLongitudeColumnFlag = true;
													}
													if (obj.name == "latitude") {
														isLatitudeColumnFlag = true;
													}
												});
												if (!isRadiusColumnFlag) {
													var alterMappedBrancehsforRadius = "ALTER TABLE MappedBranches ADD radius INTEGER";
													self._executeSql(alterMappedBrancehsforRadius);
												}
												if (!isLongitudeColumnFlag) {
													var alterMappedBrancehsforLongitude = "ALTER TABLE MappedBranches ADD longitude INTEGER";
													self._executeSql(alterMappedBrancehsforLongitude);
												}
												if (!isLatitudeColumnFlag) {
													var alterMappedBrancehsforLatitude = "ALTER TABLE MappedBranches ADD latitude INTEGER";
													self._executeSql(alterMappedBrancehsforLatitude);
												}
												self._createDBSync();
												cb();
											});
										});
									});
								});
							});
						});
					});
					});
				});
			});
		},
		_getIdExitingInDB : function(tableName, idName, listIdToCheck,
				successCallback, errorCallback) {
			if (listIdToCheck.length === 0) {
				successCallback([]);
				return;
			}
			var self = this;
			var SQL = 'SELECT ' + idName + ' FROM ' + tableName + ' WHERE '
					+ idName + ' IN ("'
					+ self._arrayToString(listIdToCheck, '","') + '")';
			self._executeSql(SQL, function(ids) {
				var data = _dbOperations._getRecordsArray(ids);
				var idsInDb = [];
				for ( var i = 0; i < data.length; ++i) {
					idsInDb[data[i][idName]] = true;
				}
				successCallback(idsInDb);
			}, errorCallback);
		},
		_syncNow : function(type, progressIndicator, successCallback,
				errorCallback, saveBandwidth, options) {

			var self = this;
			if (self._db == null) {
				self._dbInit();
			}
			DBSYNC.syncNow(type, progressIndicator, function(result) {
				if (result.syncOK === true) {
					// Synchronized successfully
					//console.log(result);
					successCallback(result);
				}
			}, errorCallback, saveBandwidth, options);
		},
		_refreshNow : function(transObj, progressIndicator, successCallback,
				errorCallback, options) {
			var self = this;
			if (self._db == null) {
				self._dbInit();
			}
			DBSYNC.refreshNow(transObj, progressIndicator, function(response) {

				successCallback(response);
			}, errorCallback, options);
		},
		_createDBSync : function() {
			var self = this;
			var saveUrl = rootConfig.serviceBaseUrl + "lifeEngageService/save";
			TABLES_TO_SYNC = [ {
				tableName : 'Transactions',
				idName : 'Id'
			} ];
			var syncInfo;
			DBSYNC.initSync(TABLES_TO_SYNC, self._db, {
				userEmail : 'test@gmail.com',
				device_uuid : 'UNIQUE_DEVICE_ID_287CHBE873JB',
				lastSyncDate : 0
			}, saveUrl, function() {
				//console.log('db initialisation success');
			});
		},
		_closeDB : function(dbName, successCallback, errorCallback) {
			try {
				var self = this;
				if (self._db != null) {
					self._db.close();
					if (successCallback && typeof successCallback == "function") {
						successCallback();
					} else {
						self._defaultCallBack();
					}
				}
			} catch (e) {
				//console.log("ERROR: " + e.message);
				errorCallback(e);
			}
		},
		_executeSql : function(sql, successCallback, errorCallback) {

			var self = this;
			try {
				if (self._db == null) {
					self._dbInit();
				}

				self._db
						.all(
								sql,
								[],
								function(error, rows) {
									if (error == null) {
										var results = {};
										results.rows = rows;
										results.rows.item = function(i) {
											return this[i];
										};
										if (successCallback
												&& typeof successCallback == "function") {

											successCallback(results);
										} else {

											self._defaultCallBack(sql, results);
										}
									} else {
										if (errorCallback
												&& typeof errorCallback == "function") {
											errorCallback(error);
										} else {
											self._errorHandler(error);
										}
									}
								});

			} catch (e) {
				//console.log("ERROR: " + e.message);
				errorCallback(e);
			}

		},
		_runSql : function(sql, successCallback, errorCallback) {

			var self = this;
			try {
				if (self._db == null) {
					self._dbInit();
				}

				self._db
						.run(
								sql,
								[],
								function(error) {
									if (error == null) {
										if (successCallback
												&& typeof successCallback == "function") {
											successCallback(this.lastID);
										} else {
											self._defaultCallBack(sql,
													this.lastID);
										}
									} else {
										if (errorCallback
												&& typeof errorCallback == "function") {
											errorCallback(error);
										} else {
											self._errorHandler(error);
										}
									}
								});

			} catch (e) {
				//console.log("ERROR: " + e.message);
				errorCallback(e);
			}

		},
		_buildInsertScript : function(tableName, trasactionData) {
			var members = this._getAttributesList(trasactionData);
			var values = this._getMembersValue(trasactionData, members);
			if (members.length === 0) {
				throw 'buildInsertSQL : Error, try to insert an empty object in the table '
						+ tableName;
			}
			var sql = "INSERT INTO " + tableName + " (";
			sql += this._arrayToString(members, ',');
			sql += ") VALUES (";
			sql += this._arrayToString(values, ',');
			sql += ")";
			return sql;

		}, 
		_buildBatchInsertScript: function(tableName, transactionData) {
            var self = this;
            var members = this._getAttributesList(transactionData[0]);
            var values = this._getMembersValue(transactionData, members);
            if (members.length === 0) {
                throw 'buildInsertSQL : Error, try to insert an empty object in the table ' + tableName;
            }
            var sql = "INSERT INTO " + tableName + " (";
            sql += this._arrayToString(members, ',');
            sql += ") VALUES ";

            $.each(transactionData, function(i, obj) {
                var values = self._getMembersValue(obj, members);
                sql += "(";
                sql += self._arrayToString(values, ',');
                sql += ")";
                if (i < transactionData.length - 1) {
                    sql += ",";
                }

            });
           // console.log("_buildBatchInsertScript" + sql);
            return sql;

        },
        _buildInsertOrUpdateScript: function(tableName, columnName, transactionData) {
            var members = this._getAttributesList(transactionData);
            var values = this._getMembersValue(transactionData, members);
            if (members.length === 0) {
                throw 'buildInsertSQL : Error, try to insert an empty object in the table ' + tableName;
            }
            var sql = "INSERT OR REPLACE INTO " + tableName + " (ID, ";
            sql += this._arrayToString(members, ',');
            sql += ") VALUES (";
            sql += "(SELECT ID FROM " + tableName + "  WHERE  " + columnName + "='" + transactionData[columnName] + "'),";
            sql += this._arrayToString(values, ',');
            sql += ")";
            //console.log("_buildInsertOrUpdateScript" + sql);
            return sql;

        },
		_buildUpdateScript : function(tableName, trasactionData) {
			var self = this;
			var sql = "UPDATE " + tableName + " SET ";
			var members = self._getAttributesList(trasactionData);
			if (members.length === 0) {
				throw 'buildUpdateSQL : Error, try to insert an empty object in the table '
						+ tableName;
			}
			var values = self._getMembersValue(trasactionData, members);

			var memLength = members.length;
			for ( var i = 0; i < memLength; i++) {
				//console.log(members[i] + " = " + values[i]);
				sql += members[i] + " = " + values[i];
				if (i < memLength - 1) {
					sql += ', ';
				}
			}

			return sql;
		},
		_buildFetchScript : function(selectCol, tableName, dataObj) {
			var self = this;
			var sql = "SELECT " + selectCol + " FROM " + tableName;
			if (!jQuery.isEmptyObject(dataObj)) {
				sql += " WHERE ";
				var members = self._getAttributesList(dataObj);
				if (members.length === 0) {
					throw 'buildUpdateSQL : Error, try to insert an empty object in the table '
							+ tableName;
				}
				var values = self._getMembersValue(dataObj, members);
				var memLength = members.length;
				for ( var i = 0; i < memLength; i++) {
					if (members[i] == "filter") {
						sql += dataObj.filter;
					} else {
						sql += members[i] + " = " + values[i];
					}

					if (i < memLength - 1) {
						sql += " AND ";
					}
				}

			}
			return sql;
		},
		_buildDeleteScript : function(tableName, whereClause) {
			var self = this;
			var sql = 'DELETE FROM ' + tableName + ' WHERE ' + whereClause;
			return sql;
		},
		_getRecordsArray : function(results) {
			var resultsArray = [];
			if (results.rows.length == 0) {
				//console.log("No Results returned from DB");
			} else {
				for ( var i = 0; i < results.rows.length; i++) {
					resultsArray.push(results.rows.item(i));
				}
			}
			return resultsArray;
		},
		_getMembersValue : function(obj, members) {
			var valueArray = [];
			for ( var i = 0; i < members.length; i++) {
				if (members[i].toUpperCase() == "ID") {
					valueArray.push(obj[members[i]]);
				} else if (members[i].toUpperCase() == "DOCUMENTOBJECT") {
					valueArray
							.push("'" + JSON.stringify(obj[members[i]]) + "'");
				} else if (members[i] == "TransactionData") {
					valueArray.push("'" + escape(obj[members[i]]) + "'");
				} else {
					valueArray.push('"' + obj[members[i]] + '"');
				}
			}
			return valueArray;
		},
		_getAttributesList : function(obj) {
			var memberArray = [];
			for ( var elm in obj) {
				if (typeof this[elm] === 'function' && !obj.hasOwnProperty(elm)) {
					continue;
				}
				memberArray.push(elm);
			}
			return memberArray;
		},
		_arrayToString : function(array, separator) {
			var result = '';
			for ( var i = 0; i < array.length; i++) {
				result += array[i];
				if (i < array.length - 1) {
					result += separator;
				}
			}
			return result;
		},
		_defaultCallBack : function(transaction, results) {
			//console.log('SQL Query executed. insertId: ' + results.insertId+ ' rows.length ' + results.rows.length);
					
		},
		_errorHandler : function(transaction, error) {
			//console.log('Error : ' + error.message + ' (Code ' + error.code+ ') Transaction.sql = ' + transaction.sql);
					
		}
	};
	return DbOperationsUtility;
})();
