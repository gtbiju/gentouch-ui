var loginUtility;
(function() {
	loginUtility = {

		saveToken : function(userDetails, successCallback, errorCallback) {
			try {
				var self = this;
				self.checkAgentIdExistingInDb(userDetails, function(data) {
					if (data.length > 0) {
						sql = _dbOperations._buildUpdateScript("UserDetails",
								userDetails);
						sql += " WHERE 	userId = '" + userDetails["userId"]
								+ "'";
					} else {
					
						sql = _dbOperations._buildInsertScript("UserDetails",
								userDetails);
					}
					_dbOperations._executeSql(sql, function(data) {
					//var userId = (data.insertId==null)?userdetailsService.userId:data.insertId;
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
				},function(error) {
					errorCallback(error);
				});
				
			} catch (exception) {
				errorCallback(exception);
			}
			//successCallback();
		},

		fetchToken : function(userId, successCallback, errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				dataObj.userId = userId;
				sql = _dbOperations._buildFetchScript("token,lastLoggedInDate",
						"UserDetails", dataObj);
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},

		checkAgentIdExistingInDb : function(user, successCallback,
				errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				dataObj.userId = user.userId;
				sql = _dbOperations._buildFetchScript("password",
						"UserDetails", dataObj);
				//console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		},
		
		checkDuplicateAgentInDb : function(user, successCallback,
				errorCallback) {
			try {
				var self = this;
				var dataObj = {};
				dataObj.userId = user.userId;
				sql = _dbOperations._buildFetchScript("userId",
						"UserDetails");
			//	console.log(sql);
				_dbOperations._executeSql(sql, function(results) {
					var data = _dbOperations._getRecordsArray(results);
					successCallback(data);
				}, function(error) {
					errorCallback(error);
				});
			} catch (exception) {
				errorCallback(exception);
			}
		}
	};
	var _dbOperations = {
		_db : null,
		_dbInit : function(cb) {
			var self = this;
                    window.sqlitePlugin.openDatabase("LifeEngageData",
                            "1.0", "LifeEngageData", -1, function(db){
            self._db = db;
			var tblUserDetails = "CREATE TABLE IF NOT EXISTS UserDetails (userId TEXT PRIMARY KEY ,password TEXT,token TEXT NOT NULL,lastLoggedInDate TEXT NOT NULL)"
			self._executeSql(tblUserDetails);
			cb();
            });
		},

		_closeDB : function(dbName, successCallback, errorCallback) {
			try {
				if (self._db != null) {
					self._db.close(dbName);
					if (successCallback && typeof successCallback == "function") {
						successCallback();
					} else {
						self._defaultCallBack();
					}
				}
			} catch (e) {
			//	console.log("ERROR: " + e.message);
				errorCallback(e);
			}
		},
		_executeSql : function(sql, successCallback, errorCallback) {
			var self = this;
			try {
				if (self._db == null) {
					self._dbInit(function(){
						self._getTransactionAndExecuteSql(sql, successCallback, errorCallback);
					});
				} else {
					self._getTransactionAndExecuteSql(sql, successCallback, errorCallback);
				}
			} catch (e) {
				console.log("ERROR: " + e.message);
				errorCallback(e);
			}
        },
		_buildInsertScript : function(tableName, trasactionData) {
			var members = this._getAttributesList(trasactionData);
			var values = this._getMembersValue(trasactionData, members);
			if (members.length === 0) {
				throw 'buildInsertSQL : Error, try to insert an empty object in the table '
						+ tableName;
			}
			var sql = "INSERT INTO " + tableName + " (";
			sql += this._arrayToString(members, ',');
			sql += ") VALUES (";
			sql += this._arrayToString(values, ',');
			sql += ")";
			return sql;

		},
		_buildUpdateScript : function(tableName, trasactionData) {
			var self = this;
			var sql = "UPDATE " + tableName + " SET ";
			var members = self._getAttributesList(trasactionData);
			if (members.length === 0) {
				throw 'buildUpdateSQL : Error, try to insert an empty object in the table '
						+ tableName;
			}
			var values = self._getMembersValue(trasactionData, members);

			var memLength = members.length;
			for (var i = 0; i < memLength; i++) {
				//console.log(members[i] + " = " + values[i]);
				sql += members[i] + " = " + values[i];
				if (i < memLength - 1) {
					sql += ', ';
				}
			}

			return sql;
		},
		_buildFetchScript : function(selectCol, tableName, dataObj) {
			var self = this;
			var sql = "SELECT " + selectCol + " FROM " + tableName;
			//if(dataObj.filter==null && dataObj.filter!="All"){
			if (!jQuery.isEmptyObject(dataObj)) {
				sql += " WHERE ";
				var members = self._getAttributesList(dataObj);
				if (members.length === 0) {
					throw 'buildUpdateSQL : Error, try to insert an empty object in the table '
							+ tableName;
				}
				var values = self._getMembersValue(dataObj, members);
				var memLength = members.length;
				for (var i = 0; i < memLength; i++) {
					if (members[i] == "filter") {
						continue;
					}
					sql += members[i] + " = " + values[i];
					if (i < memLength - 1) {
						sql += " AND ";
					}
				}
			}
			return sql;
		},
		_buildDeleteScript : function(tableName, whereClause) {
			var self = this;
			var sql = 'DELETE FROM ' + tableName + ' WHERE ' + whereClause;
			return sql;
		},
		_getRecordsArray : function(results) {
			var resultsArray = [];
			if (results.rows.length == 0) {
			//	console.log("No Results returned from DB");
			} else {
				for (var i = 0; i < results.rows.length; i++) {
					resultsArray.push(results.rows.item(i));
				}
			}
			return resultsArray;
		},
		_getMembersValue : function(obj, members) {
			var valueArray = [];
			for (var i = 0; i < members.length; i++) {
				if (members[i].toUpperCase() == "ID") {
					valueArray.push(obj[members[i]]);
				} else {
					valueArray.push("'" + obj[members[i]] + "'");
				}
			}
			return valueArray;
		},
		_getAttributesList : function(obj) {
			var memberArray = [];
			for ( var elm in obj) {
				if (typeof this[elm] === 'function' && !obj.hasOwnProperty(elm)) {
					continue;
				}
				memberArray.push(elm);
			}
			return memberArray;
		},
		_arrayToString : function(array, separator) {
			var result = '';
			for (var i = 0; i < array.length; i++) {
				result += array[i];
				if (i < array.length - 1) {
					result += separator;
				}
			}
			return result;
		},
		_defaultCallBack : function(transaction, results) {
			//console.log('SQL Query executed. insertId: ' + results.insertId + ' rows.length ' + results.rows.length);
					
		},
		_errorHandler : function(transaction, error) {
			//console.log('Error : ' + error.message + ' (Code ' + error.code + ') Transaction.sql = ' + transaction.sql);
					
		},
		_getTransactionAndExecuteSql : function(sql, successCallback, errorCallback){
			var self = this;
			self._db.transaction(function(tx) {
				tx.executeSql(sql, [], function(tx, results) {
					if (successCallback && typeof successCallback == "function") {
						successCallback(results);
					} else {
						self._defaultCallBack(tx, results);
					}
				}, function(tx, e) {
					if (errorCallback && typeof errorCallback == "function") {
						errorCallback(e);
					} else {
						self._errorHandler(tx, results);
					}
				});
			});
		}

	};
	return loginUtility;
})();