//Implementation for Rule Execution
var RuleHelper;
(function() {
	RuleHelper = {
		run : function(transactionObj, successCallback, errorCallback, options) {

			var requestInfo = Request();
			var canonicalModelInput = {};

			var validationRuleName = transactionObj.TransactionData.validationRuleName
					+ '.js';
			var validationFunctionName = transactionObj.TransactionData.validationRuleName;
			var databaseName = transactionObj.TransactionData.offlineDB + '.db';

			var illustrationRuleName = transactionObj.TransactionData.illustrationRuleName
					+ '.js';
			var illustrationFunctionName = transactionObj.TransactionData.illustrationRuleName;
			canonicalModelInput = convertToCanonicalModel(transactionObj.TransactionData);
			transactionObj.TransactionData = canonicalModelInput;
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObj);
			jQuery.support.cors = true;
			try {
				// Call rule engine and execute rule

				var inputJson = '{ "data": {"OfflineDbName": "' + databaseName
						+ '", "RuleSetId": "1","RuleSetName": "'
						+ validationRuleName + '", "FunctionName": "'
						+ validationFunctionName + '","variables": '
						+ JSON.stringify(transactionObj.TransactionData)
						+ '} }';
				LERuleEngine
						.execute(
								inputJson,
								function(output) {
									var result = $.parseJSON(output);

									if (result.isSuccess) {
										var inputJsonIllus = '{ "data": {"OfflineDbName": "'
												+ databaseName
												+ '", "RuleSetId": "1","RuleSetName": "'
												+ illustrationRuleName
												+ '", "FunctionName": "'
												+ illustrationFunctionName
												+ '","variables": '
												+ JSON
														.stringify(transactionObj.TransactionData)
												+ '} }';
										LERuleEngine
												.execute(
														inputJsonIllus,
														function(output) {

															successCallback($
																	.parseJSON(output));

														});
									} else {

										errorCallback($.parseJSON(output));
									}
								});
			} catch (exception) {
				errorCallback(exception);
			}
		},

		calculateRisk : function(input, ruleSetObj, successCallback,
				errorCallback) {

			var inputJson = JSON.parse(input);

			var ruleJson = JSON
					.parse('{ "data": { "RuleSetId": "1","RuleSetName": "", "FunctionName": "", "OfflineDbName":"core-products.db", "variables":{}}}');

			ruleJson.data.variables = inputJson;
			ruleJson.data.RuleSetName = ruleSetObj.ruleSetName;
			ruleJson.data.FunctionName = ruleSetObj.functionName;

			LERuleEngine.execute(JSON.stringify(ruleJson),
					ruleEngineSuccessCallback, ruleEngineErrorCallback);

			function ruleEngineSuccessCallback(output) {
				var result = $.parseJSON(output);
				successCallback(result);
			}
			function ruleEngineErrorCallback(output) {
				//console.log("LEProductSelector : There was an error in executing the rules. "+ output);
				errorCallback(output);
			}
		},

		calculateGoal : function(transactionObj, goalObject, successCallback,
				errorCallback) {

			var RuleName = "";
			var FunctionName = "";
			RuleName = goalObject.ruleName;
			FunctionName = goalObject.functionName;

			var inputJson = '{ "data": {"OfflineDbName": "core-products.db", "RuleSetId": "1","RuleSetName": "'
					+ RuleName
					+ '", "FunctionName": "'
					+ FunctionName
					+ '","variables": '
					+ JSON.stringify(transactionObj)
					+ '} }';
			LERuleEngine.execute(inputJson, function(output) {

				successCallback($.parseJSON(output));
			});
		},

		checkValidationFNAtoBI : function(transactionObj, successCallback,
				errorCallback, options) {

			var requestInfo = Request();
			var canonicalModelInput = {};

			var validationRuleName = "LESampleValidation.js";
			var validationFunctionName = "LESampleValidation";
			requestInfo.Request.RequestPayload.Transactions
					.push(transactionObj);
			jQuery.support.cors = true;
			try {
				// Call rule engine and execute rule

				var inputJson = '{ "data": {"OfflineDbName": "core-products.db", "RuleSetId": "1","RuleSetName": "'
						+ validationRuleName
						+ '", "FunctionName": "'
						+ validationFunctionName
						+ '","variables": '
						+ JSON.stringify(transactionObj) + '} }';

				LERuleEngine.execute(inputJson, function(output) {
					var result = $.parseJSON(output);

					if (result.LEStatus == "Pass") {
						successCallback($.parseJSON(output));
					} else {
						errorCallback($.parseJSON(output));
					}
				});
			} catch (exception) {
				errorCallback(exception);
			}
		}
	};

	return RuleHelper;
})();