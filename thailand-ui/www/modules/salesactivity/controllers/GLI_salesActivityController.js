  /*
 * Copyright 2015, LifeEngage
 */
/*Name:GLI_salesActivityController
 * For implementing GLI specific changes
 */
storeApp.controller('GLI_salesActivityController', GLI_salesActivityController);
GLI_salesActivityController.$inject = ['$rootScope',
						'$scope',
						'$compile',
						'$routeParams',
						'GLI_DataService',
						'LmsVariables',
						'LmsService',
						'IllustratorVariables',
						'PersistenceMapping',
						'DocumentService',
						'$translate',
						'$route',
						'UtilityService',
						'globalService',
						'$location',
						'FnaVariables',
						'$controller',
						'GLI_LmsService',
						'DataLookupService',
						'GLI_IllustratorVariables',
						'GLI_globalService',
						'UserDetailsService',
						'DataService',
						'GLI_LmsVariables',
						'$timeout'];
function GLI_salesActivityController($rootScope, $scope,
    $compile, $routeParams, GLI_DataService,
    LmsVariables, LmsService, IllustratorVariables,
    PersistenceMapping, DocumentService,
    $translate, $route, UtilityService,
    globalService, $location, FnaVariables,
    $controller, GLI_LmsService, DataLookupService,
    GLI_IllustratorVariables, GLI_globalService, UserDetailsService, DataService, GLI_LmsVariables, $timeout) {    
	
	$scope.OfflineshowPopUpMsg = false;
	$rootScope.moduleVariable ="";
	$rootScope.selectedPage = "salesactivity";
    $scope.timePeriodSelect="CurrentMonth";
    $scope.teamSelect="All";
    $scope.agentCodeSelect="All";
    $scope.agentNameSelect="All";
	$scope.languageSelected = localStorage["locale"];
    $rootScope.hideLanguageSetting = false;
    $rootScope.enableRefresh=false;
	$scope.tempTeamArray = [];
	$scope.agentDetailsArray = [];
	
   
   //Checks for user is online 
        $scope.isUserOnline = function() {
            var userOnline = true;
            if((rootConfig.isDeviceMobile)&& !(rootConfig.isOfflineDesktop)) {
            	if (navigator.network.connection.type == Connection.NONE) {
                    userOnline = false;
                }
            }
            if(rootConfig.isOfflineDesktop) {
                if (!navigator.onLine) {
                    userOnline = false;
                }
            }
            return userOnline;
        };

        $scope.closePopup = function() {
            $scope.OfflineshowPopUpMsg = false;
        };
		
		$scope.getSalesActivityDetailsForTimePeriod = function(timePeriodSelected) {
        	if((rootConfig.isDeviceMobile) || !(rootConfig.isOfflineDesktop)) {
        		if($scope.isUserOnline()) {
					
	                var transObj = $scope.mapScopeToPersistance();
	                var searchCriteria = {
							"searchCriteriaRequest": {
                              "command": timePeriodSelected,
                              "team": $scope.teamSelect, 
						      "agentCode": $scope.agentCodeSelect,
							  "language": $scope.languageSelected
					        }							  
                    };
					transObj.TransactionData = searchCriteria;
	                transObj.Type = "SalesActivityDetails";
                    $rootScope.showHideLoadingImage(true, "Loading..");
                    GLI_DataService.retrieveSalesActivityDetails(transObj, $scope.getTransactionSuccess, 
					                                                       $scope.getTransactionError);					
			    } else {
        			$scope.OfflineshowPopUpMsg = true;
        			$scope.offlineErrorMsg = translateMessages($translate, "salesActivity.connectToInternetMsg");
        		}
        	} 
		};
		
		$scope.getSalesActivityDetailsForTeam = function(teamSelected) {
        	if((rootConfig.isDeviceMobile) || !(rootConfig.isOfflineDesktop)) {
        		if($scope.isUserOnline()) {
					
	                var transObj = $scope.mapScopeToPersistance();
	                var searchCriteria = {
							"searchCriteriaRequest": {
                              "command": $scope.timePeriodSelect,
                              "team": teamSelected, 
						      "agentCode": $scope.agentCodeSelect,
							  "language": $scope.languageSelected
					        }							  
                    };
					transObj.TransactionData = searchCriteria;
	                transObj.Type = "SalesActivityDetails";
                    $rootScope.showHideLoadingImage(true, "Loading..");
                    GLI_DataService.retrieveSalesActivityDetails(transObj, $scope.getTransactionSuccess, 
					                                                       $scope.getTransactionError);					
				} else {
        			$scope.OfflineshowPopUpMsg = true;
        			$scope.offlineErrorMsg = translateMessages($translate, "salesActivity.connectToInternetMsg");
        		}
        	} 
		};
		
		$scope.getSalesActivityDetailsForAgentCode = function(agentCodeSelected) {
        	if((rootConfig.isDeviceMobile) || !(rootConfig.isOfflineDesktop)) {
        		if($scope.isUserOnline()) {
					
	                var transObj = $scope.mapScopeToPersistance();
	                var searchCriteria = {
							"searchCriteriaRequest": {
                              "command": $scope.timePeriodSelect,
                              "team": $scope.teamSelect, 
						      "agentCode": agentCodeSelected,
							  "language": $scope.languageSelected
					        }							  
                    };
					transObj.TransactionData = searchCriteria;
	                transObj.Type = "SalesActivityDetails";
                    $rootScope.showHideLoadingImage(true, "Loading..");
                    GLI_DataService.retrieveSalesActivityDetails(transObj, $scope.getTransactionSuccess, 
					                                                       $scope.getTransactionError);					
				} else {
        			$scope.OfflineshowPopUpMsg = true;
        			$scope.offlineErrorMsg = translateMessages($translate, "salesActivity.connectToInternetMsg");
        		}
        	} 
		};
		
        $scope.mapScopeToPersistance = function(type) {
    	   PersistenceMapping.clearTransactionKeys();
           var transactionObj = PersistenceMapping.mapScopeToPersistence({});
		   transactionObj.Key11= $rootScope.username;
           return transactionObj;
        };			
		
		$scope.getTransactionSuccess = function(data) {
    		if(data[0] !== undefined && data[0].TransactionData !== null && 
			        data[0].TransactionData.SearchCriteriaResponse !== undefined && data[0].TransactionData.SearchCriteriaResponse !== null){
				$scope.getTeamDetails(data);
				$scope.getSalesActivityTableDetails(data);
				$scope.getScoringPointsTableDetails(data);
				$scope.getScoringPercentTableDetails(data);
				$scope.getCurrentConversionRatioTableDetails(data);
				$scope.getPreviousConversionRatioTableDetails(data);
       		    $location.path('/salesactivity');
			}	
			$rootScope.showHideLoadingImage(false, "Loading..");
		};	

		$scope.getTransactionError = function(data) {
             $rootScope.showHideLoadingImage(false, "Loading..");
   			 $rootScope.serviceFailed=true;
			 if (rootConfig.isDeviceMobile && !checkConnection()) {
				$scope.message = translateMessages($translate, "networkValidationErrorMessage");
			 } else {
				$scope.message = translateMessages($translate, "regServerError");
			 }
			 $scope.$emit('tokenEvent', { message: $scope.message });
			 if (data == "Error in ajax callE") {
				 $scope.onServerError=true;
				 $scope.serverErrorMessage = translateMessages($translate, "serverErrorMessage");
			 }  else {
			        //showHideLoadingImage(false);
			 }
        }; 
	   
	    $scope.getTeamDetails = function(data) {
			
		  if(data[0].TransactionData.SearchCriteriaResponse.Team !== undefined && data[0].TransactionData.SearchCriteriaResponse.Team !== null &&
		     data[0].TransactionData.SearchCriteriaResponse.position !== undefined && data[0].TransactionData.SearchCriteriaResponse.position !== null) {
				 
			var team = data[0].TransactionData.SearchCriteriaResponse.Team;		
			var position = data[0].TransactionData.SearchCriteriaResponse.position;
			$scope.teamArray = [];
			var teamSelectCheck = false;
			if (team.length >= 1 && position != "A") {
                for (var i = 0; i < team.length; i++) {
					$scope.teamArray.push(team[i]);
				}				
				if($scope.tempTeamArray.length === 0){
                    $scope.tempTeamArray = $scope.teamArray;
                }                                                                
                if(!angular.equals($scope.teamArray, $scope.tempTeamArray)){
                    for(var i = 0; i < $scope.teamArray.length; i++){
                        for(var j = 0; j < $scope.tempTeamArray.length; j++){
                            if($scope.teamArray[i].teamName == $scope.tempTeamArray[j].teamName){     
							    teamSelectCheck = true;                                                                         
							    break;
                            }
                        }
                    }
                }                                                                
                if(teamSelectCheck == true){
                    $scope.teamArray = $scope.tempTeamArray;
                }
            } else {
				$("#team_id").children().prop('disabled',true);				
				$("#agentCode_id").children().prop('disabled',true);
				$("#agentName_id").children().prop('disabled',true);
				
				for (var i = 0; i < team.length; i++) {
					$scope.teamArray.push(team[i]);
				}
				
				$scope.teamSelect = $scope.teamArray[0].teamName;
				$scope.agentDetailsArray = $scope.teamArray[0].agentDetails;
				$scope.agentCodeSelect = $scope.agentDetailsArray[0].agentCode;
				$scope.agentNameSelect = $scope.agentDetailsArray[0].agentName;
			}
		  }			
        };
		
		$scope.OnSelectEventForTeam = function(selOption) {
			$scope.teamNameArray = [];
            $scope.agentDetailsArray = [];			
			if(selOption != "All") {
				$scope.agentCodeSelect = "All";
				$scope.agentNameSelect = "All";
				$scope.getSalesActivityDetailsForTeam(selOption);
				for(var i = 0; i < $scope.tempTeamArray.length; i++) {
					if(selOption == $scope.tempTeamArray[i].teamName) {
						$scope.teamNameArray.push($scope.tempTeamArray[i]);					
					} 
				}
				$scope.agentDetailsArray = $scope.teamNameArray[0].agentDetails;				
			} else {
				$scope.agentCodeSelect = "All";
				$scope.agentNameSelect = "All";
				$scope.getSalesActivityDetailsForTeam(selOption);
			}
		};
		
		$scope.OnSelectEventForAgentCode = function(selOption) {			          			
			if(selOption != "All") {
				$scope.getSalesActivityDetailsForAgentCode(selOption);
				for(var i = 0; i < $scope.agentDetailsArray.length; i++) {                    				
					if(selOption == $scope.agentDetailsArray[i].agentCode) {
						$scope.agentNameSelect = $scope.agentDetailsArray[i].agentName;						
					}				
				}
			} else {
				$scope.agentNameSelect = "All";
				$scope.getSalesActivityDetailsForAgentCode(selOption);
			}
		};

        $scope.OnSelectEventForAgentName = function(selOption) {
			if(selOption != "All") {				
				for(var i = 0; i < $scope.agentDetailsArray.length; i++) {                    				
					if(selOption == $scope.agentDetailsArray[i].agentName) {
						$scope.agentCodeSelect = $scope.agentDetailsArray[i].agentCode;
						$scope.getSalesActivityDetailsForAgentCode($scope.agentCodeSelect);
					}				
				}
			} else {
				$scope.agentCodeSelect = "All";
				$scope.getSalesActivityDetailsForAgentCode($scope.agentCodeSelect);
			}
		};
	
		$scope.getSalesActivityTableDetails = function(data) {
			
		  if(data[0].TransactionData.SearchCriteriaResponse.SalesActivity !== undefined && 
			       data[0].TransactionData.SearchCriteriaResponse.SalesActivity !== null) {
					   
			$scope.activityArray = [];			
			$scope.colHeaderArrayActivity = [];
			var tempColHeaderArrayActivity = [];
			
            var activity = data[0].TransactionData.SearchCriteriaResponse.SalesActivity;			
			if(activity.length >= 1) {
                for(var i = 0; i < activity.length; i++) {
					$scope.activityArray.push(activity[i]);
				}
			}	
	
			if($scope.activityArray.length >= 1) {
			    for(var i = 0; i < $scope.activityArray.length; i++) {                    				
				    tempColHeaderArrayActivity.push($scope.activityArray[i].colHeader);					
			    }
			}				
				
            if(tempColHeaderArrayActivity.length >= 1) {
				for(var i = 0; i < tempColHeaderArrayActivity.length-3; i++) {
					
                    var splitActivityValue = tempColHeaderArrayActivity[i].split(' ');					
				    $scope.colHeaderArrayActivity.push($scope.getColHeaderArrayValues(splitActivityValue));
				}
				
				for(var i = tempColHeaderArrayActivity.length-3; i < tempColHeaderArrayActivity.length; i++) {
					var otherActivityValues = translateMessages($translate, "salesActivity." + (tempColHeaderArrayActivity[i].toString()));
				    $scope.colHeaderArrayActivity.push(otherActivityValues);					
			    } 
			}
		  }
        };
		
		$scope.getScoringPointsTableDetails = function(data) {
			
		  if(data[0].TransactionData.SearchCriteriaResponse.Scoring !== undefined && 
			      data[0].TransactionData.SearchCriteriaResponse.Scoring !== null && 
				  data[0].TransactionData.SearchCriteriaResponse.Scoring.ScoringPoints !== undefined && 
			      data[0].TransactionData.SearchCriteriaResponse.Scoring.ScoringPoints !== null) {
					  
			$scope.scoringPointsArray = [];
			$scope.colHeaderArrayScoringPoints = [];
			var tempColHeaderArrayScoringPoints = [];
					   
            var scoringPoints = data[0].TransactionData.SearchCriteriaResponse.Scoring.ScoringPoints;	
			
			if(scoringPoints.length >= 1) {
                for(var i = 0; i < scoringPoints.length; i++) {
					$scope.scoringPointsArray.push(scoringPoints[i]);
				}
			}
			
		    if($scope.scoringPointsArray.length >= 1) {
			    for(var i = 0; i < $scope.scoringPointsArray.length; i++) {                    				
				    tempColHeaderArrayScoringPoints.push($scope.scoringPointsArray[i].colHeader);					
			    }
			}			
				
			if(tempColHeaderArrayScoringPoints.length >= 1) {
				for(var i = 0; i < tempColHeaderArrayScoringPoints.length-3; i++) {
					
                    var splitPointsValue = tempColHeaderArrayScoringPoints[i].split(' ');					
				    $scope.colHeaderArrayScoringPoints.push($scope.getColHeaderArrayValues(splitPointsValue));
				}
				
				for(var i = tempColHeaderArrayScoringPoints.length-3; i < tempColHeaderArrayScoringPoints.length; i++) {
					var otherPointsValues = translateMessages($translate, "salesActivity." + (tempColHeaderArrayScoringPoints[i].toString()));
				    $scope.colHeaderArrayScoringPoints.push(otherPointsValues);					
			    } 
			}	
		  }			
        };
		
		$scope.getScoringPercentTableDetails = function(data) {            
			
		  if(data[0].TransactionData.SearchCriteriaResponse.Scoring !== undefined && 
			      data[0].TransactionData.SearchCriteriaResponse.Scoring !== null && 
				  data[0].TransactionData.SearchCriteriaResponse.Scoring.ScoringPercent !== undefined && 
			      data[0].TransactionData.SearchCriteriaResponse.Scoring.ScoringPercent !== null) {
					  
			$scope.scoringPercentArray = [];
			$scope.colHeaderArrayScoringPercent = [];
			var tempColHeaderArrayScoringPercent = [];
			
            var scoringPercent = data[0].TransactionData.SearchCriteriaResponse.Scoring.ScoringPercent;	
			
			if(scoringPercent.length >= 1) {
                for(var i = 0; i < scoringPercent.length; i++) {
					$scope.scoringPercentArray.push(scoringPercent[i]);
				}
			}
			
		    if($scope.scoringPercentArray.length >= 1) {
			    for(var i = 0; i < $scope.scoringPercentArray.length; i++) {                    				
				    tempColHeaderArrayScoringPercent.push($scope.scoringPercentArray[i].colHeader);					
			    }
			}			
				
			if(tempColHeaderArrayScoringPercent.length >= 1) {
				for(var i = 0; i < tempColHeaderArrayScoringPercent.length-3; i++) {
					
                    var splitPercentValue = tempColHeaderArrayScoringPercent[i].split(' ');					
				    $scope.colHeaderArrayScoringPercent.push($scope.getColHeaderArrayValues(splitPercentValue));
				}
				
				for(var i = tempColHeaderArrayScoringPercent.length-3; i < tempColHeaderArrayScoringPercent.length; i++) {
					var otherPercentValues = translateMessages($translate, "salesActivity." + (tempColHeaderArrayScoringPercent[i].toString()));
				    $scope.colHeaderArrayScoringPercent.push(otherPercentValues);					
			    } 
			}
		  }			
        };
		
		$scope.getCurrentConversionRatioTableDetails = function(data) {
			
		  if(data[0].TransactionData.SearchCriteriaResponse.ConversionRatio !== undefined && 
			       data[0].TransactionData.SearchCriteriaResponse.ConversionRatio !== null) {
					   
			$scope.currentConversionRatioArray = [];
			$scope.currentColHeaderArrayConversionRatio = [];
			var currentTempColHeaderArrayConversionRatio = [];
			var conversionRatioCurrent = [];
			
			var conversionRatio = data[0].TransactionData.SearchCriteriaResponse.ConversionRatio;
			
			if($scope.timePeriodSelect == "CurrentWeek" && conversionRatio.CurrentWeek !== undefined && 
	        		conversionRatio.CurrentWeek !== null) {
	    	  				
				conversionRatioCurrent = conversionRatio.CurrentWeek;
	    	  		
	    	} else if($scope.timePeriodSelect == "CurrentMonth" && conversionRatio.CurrentMonth !== undefined && 
	    	  			   conversionRatio.CurrentMonth !== null) {
	    	  				
	    		conversionRatioCurrent = conversionRatio.CurrentMonth;     	  	    
	    	}
			
			if(conversionRatioCurrent.length >= 1) {
                for(var i = 0; i < conversionRatioCurrent.length; i++) {
					$scope.currentConversionRatioArray.push(conversionRatioCurrent[i]);
				}
			}
			
		    if($scope.currentConversionRatioArray.length >= 1) {
			    for(var i = 0; i < $scope.currentConversionRatioArray.length; i++) {                    				
			    	currentTempColHeaderArrayConversionRatio.push($scope.currentConversionRatioArray[i].colHeader);					
			    }
			}			
				
			if(currentTempColHeaderArrayConversionRatio.length >= 1) {
				for(var i = 0; i < currentTempColHeaderArrayConversionRatio.length-2; i++) {
					
                    var currentSplitRatioValue = currentTempColHeaderArrayConversionRatio[i].split(' ');					
				    $scope.currentColHeaderArrayConversionRatio.push($scope.getColHeaderArrayValues(currentSplitRatioValue));
				}
				
				for(var i = currentTempColHeaderArrayConversionRatio.length-2; i < currentTempColHeaderArrayConversionRatio.length; i++) {
					var otherCurrentRatioValues = translateMessages($translate, "salesActivity." + (currentTempColHeaderArrayConversionRatio[i].toString()));
				    $scope.currentColHeaderArrayConversionRatio.push(otherCurrentRatioValues);					
			    } 
			}
		  }			
        };
        
        $scope.getPreviousConversionRatioTableDetails = function(data) {
			
  		  if(data[0].TransactionData.SearchCriteriaResponse.ConversionRatio !== undefined && 
  			    data[0].TransactionData.SearchCriteriaResponse.ConversionRatio !== null) {
  					   
  			$scope.prevConversionRatioArray = [];
  			$scope.prevColHeaderArrayConversionRatio = [];
  			var prevTempColHeaderArrayConversionRatio = [];
  			
  			var conversionRatioPrev = $scope.getConversionRatioArrayBasedOnTimePeriod(data[0].TransactionData.SearchCriteriaResponse.ConversionRatio);
  			
  			if(conversionRatioPrev.length >= 1) {
                  for(var i = 0; i < conversionRatioPrev.length; i++) {
  					$scope.prevConversionRatioArray.push(conversionRatioPrev[i]);
  				}
  			}
  			
  		    if($scope.prevConversionRatioArray.length >= 1) {
  			    for(var i = 0; i < $scope.prevConversionRatioArray.length; i++) {                    				
  			    	prevTempColHeaderArrayConversionRatio.push($scope.prevConversionRatioArray[i].colHeader);					
  			    }
  			}			
  				
  			if(prevTempColHeaderArrayConversionRatio.length >= 1) {
  				for(var i = 0; i < prevTempColHeaderArrayConversionRatio.length-2; i++) {
  					
                    var prevSplitRatioValue = prevTempColHeaderArrayConversionRatio[i].split(' ');					
  				    $scope.prevColHeaderArrayConversionRatio.push($scope.getColHeaderArrayValues(prevSplitRatioValue));
  				}
  				
  				for(var i = prevTempColHeaderArrayConversionRatio.length-2; i < prevTempColHeaderArrayConversionRatio.length; i++) {
  					var otherPrevRatioValues = translateMessages($translate, "salesActivity." + (prevTempColHeaderArrayConversionRatio[i].toString()));
  				    $scope.prevColHeaderArrayConversionRatio.push(otherPrevRatioValues);					
  			    } 
  			}
  		  }			
          };
          
        $scope.getConversionRatioArrayBasedOnTimePeriod = function(conversionRatioArray) {
        	
        	var conversionRatio = [];
        	
        	if(($scope.timePeriodSelect == "CurrentWeek" || $scope.timePeriodSelect == "PreviousWeek") &&
    	  			    conversionRatioArray.PreviousWeek !== undefined && 
    	  			    conversionRatioArray.PreviousWeek !== null) {
      				
      		    conversionRatio = conversionRatioArray.PreviousWeek;
      				
      		} else if(($scope.timePeriodSelect == "CurrentMonth" || $scope.timePeriodSelect == "PreviousMonth") &&
      				    conversionRatioArray.PreviousMonth !== undefined && 
      				    conversionRatioArray.PreviousMonth !== null) {
      				
      			conversionRatio = conversionRatioArray.PreviousMonth;  
      				 
      		} else if($scope.timePeriodSelect == "LastThreeMonth" && conversionRatioArray.LastThreeMonth !== undefined && 
      				   conversionRatioArray.LastThreeMonth !== null) {
    	  			
    	  		conversionRatio = conversionRatioArray.LastThreeMonth;
    	  			
    	  	} else if($scope.timePeriodSelect == "LastSixMonth" && conversionRatioArray.LastSixMonth !== undefined && 
    	  			   conversionRatioArray.LastSixMonth !== null) {
    	  			
    	  		conversionRatio = conversionRatioArray.LastSixMonth;    	  			
    	  	} 
        	return conversionRatio;
        };
		
		$scope.getColHeaderArrayValues = function(test) {			
								
			if($scope.timePeriodSelect == "LastThreeMonth" || $scope.timePeriodSelect == "LastSixMonth") {
			    if($scope.languageSelected == "th_TH") {						   
				    test[1] = parseInt(test[1]) + rootConfig.thaiYearDifference;
					test[0] = "salesActivity." + test[0];
				    test = translateMessages($translate, test[0]) + "'" + (test[1].toString()).substring(2);
				            							
			    } else {
					test[0] = "salesActivity." + test[0];
				    test = translateMessages($translate, test[0]) + "'" + test[1].substring(2);
			    }					
			} else if ($scope.timePeriodSelect == "CurrentMonth" || $scope.timePeriodSelect == "PreviousMonth") {
				
				    if(test[1] !== undefined && test[2] !== undefined) {
					    test[0] = "salesActivity." + test[0] + test[1];	
                        test = translateMessages($translate, test[0]) + " " + test[2];						
					} else {
						test = translateMessages($translate, "salesActivity." + test[0]);
					}
				    
			} else { 
			        if(test[1] !== undefined) {
			            test[0] = "salesActivity." + test[0];
				        test = translateMessages($translate, test[0]) + " " + test[1];
					} else {
						test = translateMessages($translate, "salesActivity." + test[0])
					}
			}					
			return test;
		};

        //Checks for user is online 
        $scope.checkUserOnline = function() {
            var userOnline = true;
            $scope.OfflineshowPopUpMsg = false;

            if((rootConfig.isDeviceMobile)&& !(rootConfig.isOfflineDesktop)) {
            	if (navigator.network.connection.type == Connection.NONE) {
                    userOnline = false;
                    $scope.OfflineshowPopUpMsg = true;
                    $scope.offlineErrorMsg = translateMessages($translate, "salesActivity.connectToInternetMsg");
                }
            }
            if(rootConfig.isOfflineDesktop) {
                if (!navigator.onLine) {
                    userOnline = false;
                    $scope.OfflineshowPopUpMsg = true;
                    $scope.offlineErrorMsg = translateMessages($translate, "salesActivity.connectToInternetMsg");
                }
            }                                
        };
};
