/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:FeedbackController
 CreatedDate:30/03/2015
 Description:Lead Management Module.
 */
storeApp.controller('FeedbackController', [
		'$rootScope',
		'$scope',
		'$compile',
		'DataService',
		'$translate',
		'LmsVariables',
		'LmsService',
		'$location',
		'globalService',
		'UtilityService',
		'$timeout',
		function FeedbackController($rootScope, $scope, $compile, DataService,
				$translate, LmsVariables, LmsService, $location, globalService,
				UtilityService, $timeout) {

			$scope.Initialize = function() {
				$scope.LmsModel = LmsVariables.getLmsModel();

			};

			$scope.onSaveSuccess = function() {
				$rootScope.showHideLoadingImage(false);
				$rootScope.lePopupCtrl.showSuccess(translateMessages(
						$translate, "lifeEngage"), translateMessages(
						$translate, "leadSaved"), translateMessages($translate,
						"fna.ok"));
				// $rootScope.showMessagePopup(true, "success",
				// translateMessages($translate, "leadSaved"));
			};
			$scope.onSaveError = function(msg) {
				// TODO
				$rootScope.showHideLoadingImage(false);
				$scope.refresh();
				if (msg == "This Trasaction Data is already processed : ") {
					$rootScope.lePopupCtrl.showError(translateMessages(
							$translate, "lifeEngage"), translateMessages(
							$translate, "leadAlreadyExists"),
							translateMessages($translate, "fna.ok"));
					// $rootScope.showMessagePopup(true, "error",
					// translateMessages($translate, "leadAlreadyExists"));
				} else {
					$rootScope.lePopupCtrl.showError(translateMessages(
							$translate, "lifeEngage"), translateMessages(
							$translate, "failure"), translateMessages(
							$translate, "fna.ok"));
					// $rootScope.showMessagePopup(true, "error",
					// translateMessages($translate, "Failure"));
				}
			};

			$scope.ratingCount = [{},{},{},{},{}];
			$scope.accordion_click = function(index) {
				$scope.accordionIndex = index;
			};

			$scope.Save = function() {
				$rootScope.showHideLoadingImage(true, 'paintUIMessage',
						$translate);
				LmsVariables.setLmsModel($scope.LmsModel);
				LmsService.saveTransactions($scope.onSaveSuccess,
						$scope.onSaveError, $scope.LmsModel);
			};

			$scope.Initialize();
		} ]);