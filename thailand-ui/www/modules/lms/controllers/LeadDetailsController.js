/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:LeadDetailsController
 CreatedDate:20/02/2015
 Description:Lead Management Module.
 */
storeApp
		.controller(
				'LeadDetailsController',
				[
						'$rootScope',
						'$scope',
						'$compile',
						'DataService',
						'$translate',
						'LmsVariables',
						'LmsService',
						'$location',
						'globalService',
						'UtilityService',
						'$timeout',
						function LeadDetailsController($rootScope, $scope,
								$compile, DataService, $translate,
								LmsVariables, LmsService, $location,
								globalService, UtilityService,
								$timeout) {
							
							
							$scope.Initialize = function() {
								$scope.editMode = LmsVariables.editMode;
								$scope.prevdateTime = "";
                             
								if ($scope.editMode) {
									$scope.$parent.showLeadPopup = true;
									$scope.$parent.isCreatePopup = false;
								} else {
									$scope.$parent.showLeadPopup = true;
									$scope.$parent.isCreatePopup = true;
								}

								$scope.LmsModel = LmsVariables.getLmsModel();
								$scope.sourceArray = [];// =
								// LmsVariables.sourceArray;
								$scope.subSourceArray = LmsVariables.subSourceArray;
								$scope.severityArray;// LmsVariables.severityArray;
								UtilityService.InitializeErrorDisplay($scope,
										$rootScope, $translate);

								if (!((rootConfig.isDeviceMobile))) {
									$('.custdatepicker')
											.datepicker({
												format : 'yyyy-mm-dd',
												autoclose : true
											})
											.on(
													'change',
													function() {

														/*$timeout(
																function() {
																	$rootScope
																			.updateErrorCount('leadDetailsSection');
																	if (!$scope.editMode) {
																		if( $scope.LmsModel.Communication.followUps && $scope.LmsModel.Communication.followUps[0]){
																			var followUpDate = $scope.LmsModel.Communication.followUps[0].followUpsDate;
																			if ($scope.LmsModel.Communication.followUps[0].followUpsTime != undefined
																					&& $scope.LmsModel.Communication.followUps[0].followUpsTime != "Invalid Date")
																				followUpDate = followUpDate
																						+ " "
																						+ $scope.LmsModel.Communication.followUps[0].followUpsTime;
																			if (LEDate(followUpDate) <= LEDate()) {
																				$scope.errorCount = parseInt($scope.errorCount) + 1;
																				$scope
																						.refresh();
																			}
																		}
																	}
																}, 10);*/

													});
								}

								$scope.updateErrorCount = function(arg1, arg2) {

									$rootScope.updateErrorCount(arg1, arg2);
									if (!$scope.editMode) {
										var followUpDate = $scope.LmsModel.Communication.followUps[0].followUpsDate;
										if (followUpDate != undefined && followUpDate != "Invalid Date" && $scope.LmsModel.Communication.followUps[0].followUpsTime != undefined && $scope.LmsModel.Communication.followUps[0].followUpsTime != "Invalid Date")
											followUpDate = followUpDate.toJSON().slice(0,10) +'T'+ $scope.LmsModel.Communication.followUps[0].followUpsTime.toTimeString().slice(0,8)+"Z";
											if (LEDate(followUpDate) <= LEDate()) {
											$scope.errorCount = parseInt($scope.errorCount) + 1;
											$scope.refresh();
										}
									}
								};
								$scope.showErrorPopup = function(arg1, arg2) {
									$rootScope.showErrorPopup(arg1, arg2);
									if (!$scope.editMode
											&& $scope.LmsModel.Communication.followUps && $scope.LmsModel.Communication.followUps[0].followUpsDate != undefined
											&& $scope.LmsModel.Communication.followUps[0].followUpsTime != undefined) {
										var followUpDate = $scope.LmsModel.Communication.followUps[0].followUpsDate;
										if (followUpDate != undefined && followUpDate != "Invalid Date" && $scope.LmsModel.Communication.followUps[0].followUpsTime != undefined 
												&& $scope.LmsModel.Communication.followUps[0].followUpsTime != "Invalid Date")
											followUpDate = followUpDate.toJSON().slice(0,10) +'T'+ $scope.LmsModel.Communication.followUps[0].followUpsTime.toTimeString().slice(0,8)+"Z";
										if (LEDate(followUpDate) <= LEDate()) {
											var error = {};
											error.message = translateMessages(
													$translate,
													"lms.meetingDateFuturedate");
											// meetingDateLesserThanFollowup
											error.key = 'followUpDate';
											$scope.errorMessages.push(error);
										}
									}
								};
								/*$timeout(
										function() {
											$rootScope
													.updateErrorCount('leadDetailsSection');
											if (!$scope.editMode
													&& $scope.LmsModel.Communication.followUps[0].followUpsDate != undefined
													&& $scope.LmsModel.Communication.followUps[0].followUpsTime != undefined) {
												var followUpDate = $scope.LmsModel.Communication.followUps[0].followUpsDate;
												if ($scope.LmsModel.Communication.followUps[0].followUpsTime != undefined
														&& $scope.LmsModel.Communication.followUps[0].followUpsTime != "Invalid Date")
													followUpDate = followUpDate
															+ " "
															+ $scope.LmsModel.Communication.followUps[0].followUpsTime;
												if (LEDate(followUpDate) <= LEDate()) {
													$scope.errorCount = parseInt($scope.errorCount) + 1;
													$scope.refresh();
												}
											}
										}, 10);*/
								$scope.mobnumberPattern = rootConfig.mobnumberPattern;
								$scope.leadNamePattern = rootConfig.leadNamePattern;
								$scope.numberPattern = rootConfig.numberPattern;

							};
							$scope.onSaveSuccess = function(data) {
								$rootScope.showHideLoadingImage(false);
								//Adding Calendar event for Follow up Date and time
								if( (rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
									if(LEDate($scope.LmsModel.Communication.followUps[0].dateAndTime) - $scope.prevdateTime != 0){// Add calendar event only if date or time changed 
											var startDate = LEDate($scope.LmsModel.Communication.followUps[0].dateAndTime);  
											var endDate =LEDate($scope.LmsModel.Communication.followUps[0].dateAndTime);
											endDate.setHours ( startDate.getHours() + parseInt(rootConfig.leadMeetingTimeInHr) );
											var title = translateMessages($translate,"lms.eventTitle")+$scope.LmsModel.Lead.BasicDetails.fullName;
											var location = "";
											var notes =  translateMessages($translate,"lms.eventNote");
											var reminderTimeInMin = rootConfig.reminderTimeInMin;
											var success = function(message) { 
											};
											var error = function(message) {
											};

											window.plugins.calendar.createEvent(title,location,notes,startDate,endDate,reminderTimeInMin,success,error);
											$scope.prevdateTime = LEDate($scope.LmsModel.Communication.followUps[0].dateAndTime);
									}
								}
								$scope.refresh();
								// /TODO
								// /Show NotifyMessage
								// /Proceed to next page
								// /
								$rootScope.NotifyMessages(false, "Success",
										$translate);
								$rootScope.lePopupCtrl
										.showSuccess(translateMessages(
												$translate, "lifeEngage"),
												translateMessages($translate,
														"leadSaved"),
												translateMessages($translate,
														"fna.ok"));
								// $rootScope.showMessagePopup(true, "success",
								// translateMessages($translate, "leadSaved"));

							};
							$scope.onSaveError = function(msg) {
								$rootScope.showHideLoadingImage(false);
								$scope.refresh();
								// /TODO
								// /Show NotifyMessage
								// /
								if (msg == "This Trasaction Data is already processed : ") {
									$rootScope.lePopupCtrl.showError(
											translateMessages($translate,
													"lifeEngage"),
											translateMessages($translate,
													"leadAlreadyExists"),
											translateMessages($translate,
													"fna.ok"));
									// $rootScope.showMessagePopup(true,
									// "error", translateMessages($translate,
									// "leadAlreadyExists"));
								} else {
									$rootScope.lePopupCtrl.showError(
											translateMessages($translate,
													"lifeEngage"),
											translateMessages($translate,
													"saveFailure"),
											translateMessages($translate,
													"fna.ok"));
									//$rootScope.showMessagePopup(true, "error", translateMessages($translate, "Failure"));
								}
							};
							$scope.Save = function() {
								$rootScope.showHideLoadingImage(true,
										'paintUIMessage', $translate);
								if ($scope.errorCount == 0 || $scope.editMode) {
									if ($scope.LmsModel.Communication.followUps && $scope.LmsModel.Communication.followUps[0].followUpsTime == "") {
										$scope.LmsModel.Communication.followUps[0].followUpsTime = formatForTimeControl(
												new Date(), false);
									}
									if ($scope.LmsModel.Communication.followUps && $scope.LmsModel.Communication.followUps[0].followUpsDate == "") {
										$scope.LmsModel.Communication.followUps[0].followUpsDate = formatForDateControl(new Date());
									}
									if ($scope.LmsModel.Communication.followUps && typeof $scope.LmsModel.Communication.followUps[0].followUpsDate != "string")
									$scope.LmsModel.Communication.followUps[0].followUpsDate =formatForDateControl($scope.LmsModel.Communication.followUps[0].followUpsDate);
									if ($scope.LmsModel.Communication.followUps && typeof $scope.LmsModel.Communication.followUps[0].followUpsTime != "string")
									$scope.LmsModel.Communication.followUps[0].followUpsTime = formatForTimeControl($scope.LmsModel.Communication.followUps[0].followUpsTime);
									LmsVariables.setLmsModel($scope.LmsModel);
									LmsService
											.saveTransactions(
													$scope.onSaveSuccess,
													$scope.onSaveError,
													$scope.LmsModel);
								} else {
									$rootScope.showHideLoadingImage(false);
									$scope.lePopupCtrl.showWarning(
											translateMessages($translate,
													"lifeEngage"),
											translateMessages($translate,
													"enterMandatory"),
											translateMessages($translate,
													"fna.ok"));
									//		$rootScope.showMessagePopup(true, "warning", translateMessages($translate, "enterMandatory"));

								}
							};
							$scope.reset = function() {
								LmsVariables
										.setNewLmsModel($rootScope.username);
								$scope.LmsModel = LmsVariables.getLmsModel();

							};

								
							
							$scope.$on('$viewContentLoaded', function(){
								$scope.Initialize();
							});
							

						} ]);