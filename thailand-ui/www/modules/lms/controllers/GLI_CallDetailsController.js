	/*
	 * Copyright 2015, LifeEngage 
	 */
	/*
	 Name:GLI_CalldetailsController
	 CreatedDate:20/07/2015
	 Description:KMC module to implement Generali specific changes. 
	 */
	storeApp.controller('GLI_CallDetailsController', GLI_CallDetailsController);
	GLI_CallDetailsController.$inject = ['$rootScope', '$scope', '$compile', 'DataService', '$translate', 'LmsVariables', 'LmsService', '$location', 'PersistenceMapping', 'globalService', 'UtilityService', '$timeout', 'GLI_LmsService', 'GLI_LmsVariables', '$controller', 'DataLookupService', '$filter', 'GLI_DataService', 'UserDetailsService','GLI_DataLookupService'];
	function GLI_CallDetailsController($rootScope, $scope, $compile, DataService, $translate, LmsVariables, LmsService, $location, PersistenceMapping, globalService, UtilityService, $timeout, GLI_LmsService, GLI_LmsVariables, $controller, DataLookupService, $filter, GLI_DataService, UserDetailsService,GLI_DataLookupService) {
	    $controller('CallDetailsController', {
	        $rootScope: $rootScope,
	        $scope: $scope,
	        $compile: $compile,
	        DataService: GLI_DataService,
	        $translate: $translate,
	        LmsVariables: GLI_LmsVariables,
	        LmsService: GLI_LmsService,
	        $location: $location,
	        PersistenceMapping: PersistenceMapping,
	        globalService: globalService,
	        UtilityService: UtilityService,
	        $timeout: $timeout,
	        DataLookupService: GLI_DataLookupService
	    });
	    $scope.tempFollowUps = [];
	    $scope.popupTabs = ['', '', '', ''];
	    $scope.popupTabs[1] = 'active';
	    $scope.expectedBusinessPattern = rootConfig.expectedBusinessPattern;
	    $scope.alphabeticPattern = rootConfig.alphabeticPattern;
	    $scope.clearStatusDetails = true;
	    $scope.fromPopupTabsNav = true;
	    $scope.accordionIndex;
	    $scope.numberPatternMob = rootConfig.numberPatternMob;
	    $scope.expectedBusinessDecimalPattern = rootConfig.expectedBusinessDecimalPattern;
	    $scope.isEditMode = LmsVariables.editMode;
	    $scope.lmsSectionId = "callDetailsSection";
	    $scope.svaeRemarksOnChange = false;
	    $scope.overWriteFollowUp = false;
	    $scope.actualDateAndTimeError = false;
	    $scope.disableStatus = false;
	    $scope.showContactLogTable=false;
	    $scope.callTable=[];
	    $scope.callMetDate=[];
	 	$scope.callcount=[];
	 	$scope.callstatusCode=[];
		
	    //Initializing 
	    $scope.GLI_init = function () {
			//temp fix for localStorage
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
	}
	        $scope.mandatory = true;
	        $scope.fromPopupTabsNav = true;
	        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
	        $scope.LmsModel = LmsVariables.getLmsModel();
	        if ($scope.LmsModel.Lead.RMInfo.RMName != '') {
	            $scope.showFeedback = true;
	        }
	        $scope.editMode = LmsVariables.editMode;
	        $rootScope.selectedPage = "callDetails";
	        $scope.appDateFormat = rootConfig.appDateFormat;
	        $scope.appDateTimeFormat = rootConfig.appDateTimeFormat;

	        LEDynamicUI.paintUI(rootConfig.template,
	            "leadListingWithTabs.json",
	            "callDetailsSection", "#popupContent", true,
	            function () {
	                $scope.callback();
	            }, $scope, $compile);


	        $scope.interactionArray = LmsVariables.interactionArray;
	        //init default values on create mode
	        if (!$scope.editMode) {
	            //todo - change 
	            if (!$scope.LmsModel.Lead.StatusDetails.disposition) {
	                $scope.LmsModel.Lead.StatusDetails.disposition = $scope.defaultDisposition;
	            }

	        } else {
	            $scope.remarks = "";
	            $scope.subheading = $scope.Editsubheading;
	        }

	        if ($scope.LmsModel.Communication[0].remarks != "") {
	            $scope.remarks = $scope.LmsModel.Communication[0].remarks;
	        }
	        
	           if($scope.editMode){
	       			
	       			for(i in $scope.LmsModel.Communication){
	       			
	       						if($scope.LmsModel.Communication[i].interactionType=="Call"){
	       								
	       								paintCallMeetingHistory_Edit_init(i);
	       						
	       						}
	       						else if($scope.LmsModel.Communication[i].interactionType=="Appointment"){
	       								
	       								paintAppointmentMeetingHistory_Edit_init(i);
	       						
	       						}
	       						else if($scope.LmsModel.Communication[i].interactionType=="Visit"){
	       								
	       								paintVisitMeetingHistory_Edit_init(i);
	       						
	       						}
	       			
	       			}
	       			
	       			
	       			 
	       }

	        $scope.currentStatus = $scope.LmsModel.Lead.StatusDetails.disposition;
			if(!$scope.editMode){
			paintCallMeetingHistory_init();
	        	paintAppointmentMeetingHistory_init();
	        	paintVisitMeetingHistory_init();
			}
	        //$scope.tempFollowUps = [];
	        if ($scope.LmsModel.Communication && $scope.editMode) {
	            if ($scope.LmsModel.Communication[0].followUps && $scope.LmsModel.Communication[0].followUps.length == 0 &&
	                $scope.LmsModel.Communication[0].metDateAndTime != "") {
	                $scope.planedMeetingDate = LEDate($scope.LmsModel.Communication[0].metDateAndTime);
	                $scope.planedMeetingTime = LEDate($scope.LmsModel.Communication[0].metDateAndTime);
	            } else {
	                $scope.planedMeetingDate = "";
	                $scope.planedMeetingTime = "";
	            }

	            //Create temp array for displaying table with planed date as last entry
	            
	        }

	        if (LmsVariables.actualMeetingDate != "") {
	            $scope.actualMeetingDate = LmsVariables.actualMeetingDate;
	        }
	        if (LmsVariables.actualMeetingTime != "") {
	            $scope.actualMeetingTime = LEDate(LmsVariables.actualMeetingTime);
	        }

	        if (LmsVariables.plannedMeetingDate != "") {
	            $scope.planedMeetingDate = LmsVariables.plannedMeetingDate;
	        }
	        if (LmsVariables.plannedMeetingTime != "") {
	            $scope.planedMeetingTime = LEDate(LmsVariables.plannedMeetingTime);
	        }

	        if ($scope.editMode && $scope.editMode && $scope.currentStatus != 'New') {
	            $scope.populateIndependentLookupDateInScope('STATUS', 'status', 'LmsModel.Lead.StatusDetails.disposition', false, 'status', 'callDetailsSection');
	        }

	        if (LmsVariables.submissionDate != "") {
	            $scope.LmsModel.Policies.productSold.submissionDate = LmsVariables.submissionDate;
	        }
	        if ($scope.LmsModel.Lead.StatusDetails.disposition != 'Successful Sale') {
	            clearProductDetails();
	        } else {
	            $scope.disableSavedPolicyDetails();
	            $scope.disableStatus = true;
	        }
	    };
	    $scope.disablePolicyDetails = [];
	    $scope.disableSavedPolicyDetails = function () {
	        for (var i = 0; i < $scope.LmsModel.Policies.productSold.length; i++) {
	            if ($scope.LmsModel.Policies.productSold[i].proposalNumber) {
	                $scope.disablePolicyDetails[i] = true;
	            } else {
	                if ($scope.LmsModel.Policies.productSold.length > 1) {
	                    $scope.LmsModel.Policies.productSold.pop(i);
	                    i--;
	                }
	            }
	        }
	    }

	    $scope.onStatusChange = function () {
	        if ($scope.editMode && $scope.currentStatus == 'New') {
	            $scope.populateIndependentLookupDateInScope('STATUS', 'status', 'LmsModel.Lead.StatusDetails.disposition', false, 'status', 'callDetailsSection');
	            $scope.currentStatus = $scope.LmsModel.Lead.StatusDetails.disposition;
	        }
	        if ($scope.LmsModel.Lead.StatusDetails.disposition != 'Successful Sale') {
	            clearProductDetails();
	        } else {
	            if ($scope.LmsModel.Policies.productSold.length === 0)
	                $scope.addPolicyDetail();
	            $scope.planedMeetingDate = "";
	            $scope.planedMeetingTime = "";
	            $scope.LmsModel.Policies.submissionDate = "";
	        }
	    };

	    function clearProductDetails() {
	        if ($scope.LmsModel.Policies.productSold.length > 0) {
	            $scope.LmsModel.Policies.productSold = [];
	        }
	    };
	    $scope.populateIndependentLookupDateInScope = function (type, fieldName, model, setDefault, field, currentTab,cacheable) {
		if (!cacheable){
						cacheable = false;
					}
	        $scope[fieldName] = [{
	            "key": "",
	            "value": "loading ...."
        }];
	        var options = {};
	        options.type = type;
	        DataLookupService.getLookUpData(options, function (data) {
	            if (data[data.length - 1].code == 'New') {
	                data.pop();
	            }
	            $scope[fieldName] = data;

	            if (typeof field != "undefined" && typeof $scope.currentTab != "undefined") {
	                setTimeout(function () {
	                    if (typeof $scope.lmsSectionId != "undefined") {
	                        var data_Evaluvated = $scope.lmsSectionId[fieldName];
	                        if (data_Evaluvated) {
	                            data_Evaluvated.$render();
	                        }
	                    }
	                    var data_Evaluvated = $scope.currentTab[field];
	                    if (typeof data_Evaluvated != "undefined") {
	                        data_Evaluvated.$render();
	                    }
	                }, 0);
	            }
	            $scope.refresh();
	        }, function (errorData) {
	            $scope[fieldName] = [];

	        },cacheable);
	    };
	    $scope.evaluateString = function (value) {
	        var val;
	        var variableValue = $scope;
	        val = value.split('.');
	        if (value.indexOf('$scope') != -1) {
	            val = val.shift();
	        }
	        for (var i in val) {
	            variableValue = variableValue[val[i]];
	        }
	        return variableValue;
	    };

	    $scope.GLI_errorCheck = function () {
	        var planedMeetingDate = "";
	        var planedMeetingTime = "";
	        var actualMeetingDate = "";
	        var actualMeetingTime = "";
	        if ($scope.planedMeetingDate && $scope.planedMeetingTime) {
	            if (typeof $scope.planedMeetingTime == "object") {
	                planedMeetingTime = formatForTimeControl($scope.planedMeetingTime, false);
	            } else {
	                planedMeetingTime = $scope.planedMeetingTime;
	            }
	            planedMeetingDate = formatForDateControl($scope.planedMeetingDate);
	        }
	        if ($scope.actualMeetingDate && $scope.actualMeetingTime) {
	            if (typeof $scope.actualMeetingTime == "object") {
	                actualMeetingTime = formatForTimeControl($scope.actualMeetingTime, false);
	            } else {
	                actualMeetingTime = $scope.actualMeetingTime;
	            }
	            actualMeetingDate = formatForDateControl($scope.actualMeetingDate);
	        }
	        $scope.LmsModel.Communication[0].remarks = $scope.remarks;
	        if ($scope.errorCount < 1) {

	            var followUpsLength = $scope.LmsModel.Communication[0].followUps.length;

	            if ($scope.actualMeetingDate) {

	                if ($scope.overWriteFollowUp || !$scope.editMode) {
	                    if ($scope.LmsModel.Communication[0].followUps && $scope.LmsModel.Communication[0].followUps.length > 0) {
	                        var _index = $scope.LmsModel.Communication[0].followUps.length - 1;
	                        if ($scope.LmsModel.Communication[0].followUps[_index].actualDateAndTime) {
	                            $scope.LmsModel.Communication[0].followUps[_index] =
	                                $scope.getNewFollowUp(actualMeetingDate + " " + actualMeetingTime, planedMeetingDate + " " + planedMeetingTime,
	                                    $scope.LmsModel.Communication[0].interactionType, $scope.LmsModel.Lead.StatusDetails.disposition, $scope.remarks);
	                        }
	                    } else {
	                        $scope.LmsModel.Communication[0].followUps[followUpsLength] =
	                            $scope.getNewFollowUp(actualMeetingDate + " " + actualMeetingTime, planedMeetingDate + " " + planedMeetingTime,
	                                $scope.LmsModel.Communication[0].interactionType, $scope.LmsModel.Lead.StatusDetails.disposition, $scope.remarks);
	                    }
	                } else {
	                    $scope.LmsModel.Communication[0].followUps[followUpsLength] =
	                        $scope.getNewFollowUp(actualMeetingDate + " " + actualMeetingTime, planedMeetingDate + " " + planedMeetingTime,
	                            $scope.LmsModel.Communication[0].interactionType, $scope.LmsModel.Lead.StatusDetails.disposition, $scope.remarks);
	                }
	                $scope.LmsModel.Communication[0].metDateAndTime = planedMeetingDate + " " + planedMeetingTime;
	            }
	        }
	    };
	    
	    $scope.GLI_errorCheck_custom = function (arg1) {
	        var planedMeetingDate = "";
	        var planedMeetingTime = "";
	        var actualMeetingDate = "";
	        var actualMeetingTime = "";
	        var calldate = "";
	        if ($scope.planedMeetingDate) {
	          
	            planedMeetingDate = formatForDateControl($scope.planedMeetingDate);
	        }
	        if ($scope.actualMeetingDate && $scope.actualMeetingTime) {
	            if (typeof $scope.actualMeetingTime == "object") {
	                actualMeetingTime = formatForTimeControl($scope.actualMeetingTime, false);
	            } else {
	                actualMeetingTime = $scope.actualMeetingTime;
	            }
	            actualMeetingDate = formatForDateControl($scope.actualMeetingDate);
	        }
	        
	        if ($scope.calldate) {
	        	calldate = formatForDateControl($scope.calldate);
	        }
	        
	      //  $scope.LmsModel.Communication.remarks = $scope.remarks;
	        if ($scope.errorCount < 1) {
 
	        	if(arg1=='Call'){
	         var followUpsLength = $scope.LmsModel.Communication[1].followUps.length;

	            if ($scope.calldate) {

	                if ($scope.overWriteFollowUp) {
	                    if ($scope.LmsModel.Communication[1].followUps && $scope.LmsModel.Communication[1].followUps.length > 0) {
	                        var _index = $scope.LmsModel.Communication[1].followUps.length - 1;
	                     if ($scope.LmsModel.Communication[1].followUps[_index].metDate) {
	                            $scope.LmsModel.Communication[1].followUps[_index] =
	                            		 $scope.getNewFollowUp_Call($scope.calldate, $scope.communicationStatusCodeCall,
	             	                            $scope.LmsModel.Communication[1].interactionType, $scope.LmsModel.Communication[1].count);
	                       }
	                    } else {
	                        $scope.LmsModel.Communication[1].followUps[followUpsLength] =
	                        		 $scope.getNewFollowUp_Call($scope.calldate,$scope.communicationStatusCodeCall,
	         	                            $scope.LmsModel.Communication[1].interactionType, $scope.LmsModel.Communication[1].count);
	                    }
	                } else {
	                    $scope.LmsModel.Communication[1].followUps[followUpsLength] =
	                        $scope.getNewFollowUp_Call($scope.calldate, $scope.communicationStatusCodeCall,
	                            $scope.LmsModel.Communication[1].interactionType, $scope.LmsModel.Communication[1].count);
	                }
	               // $scope.LmsModel.Communication.metDateAndTime = planedMeetingDate + " " + planedMeetingTime;
	               // $scope.LmsModel.Communication[1].metDateAndTime = actualMeetingDate + " " + actualMeetingTime;
	              
	            }
	           
	 
	            	
	            	var callcontact={
	            			"Communication":[{
	            				
	            				"metDate":  $scope.LmsModel.Communication[1].metDate ,
	            				"count": $scope.LmsModel.Communication[1].count,
	            				"communicationStatusCode": $scope.LmsModel.Communication[1].communicationStatusCode
	            				
	            			},{
	            				"metDate": "",
	            				"count":0,
	            				"communicationStatusCode":""
	            				
	            			}]
	            	}
	            	
					
              	  $scope.LmsModel.Communication[1].metDate=calldate;
              	  $scope.LmsModel.Communication[1].metType="";
              	  $scope.communicationStatusCode= $scope.LmsModel.Communication[1].communicationStatusCode;
              //	$scope.calldate= $scope.LmsModel.Communication[1].metDate;
              	$scope.count=$scope.LmsModel.Communication[1].count;
              	$scope.CallSectionDate= $scope.LmsModel.Communication[1].metDate;
              
			//  var callUIdate=InternalToExternal(calldate);
			 // $scope.calldate=callUIdate;
              	
               // $scope.communicationData=Communication;
              	$scope.callMetDate= $scope.LmsModel.Communication[1].metDate;
              	$scope.callcount= $scope.LmsModel.Communication[1].count;
              	$scope.callstatusCode= $scope.LmsModel.Communication[1].communicationStatusCode;
              	 // $scope.callTable= $scope.LmsModel.Communication[1].communicationStatusCode;
              	  //$scope.callTable[1]= $scope.LmsModel.Communication[1].metDate;
              	 
              	  
             
	        	}
	        	  else if (arg1=="Appointment"){
		            	  $scope.actualMeetingTime=actualMeetingTime;
		                var followUpsLength = $scope.LmsModel.Communication[2].followUps.length;
						var duplicateFlag=false;
		                if ($scope.actualMeetingDate && $scope.actualMeetingTime) {
							 
								
								
			                if ($scope.overWriteFollowUp) {
			                    if ($scope.LmsModel.Communication[2].followUps && $scope.LmsModel.Communication[2].followUps.length > 0) {
			                        var _index = $scope.LmsModel.Communication[2].followUps.length - 1;
			                        if ($scope.LmsModel.Communication[2].followUps[_index].actualDateAndTime) {
			                            $scope.LmsModel.Communication[2].followUps[_index] =
			                            		 $scope.getNewFollowUp_Appointment($scope.actualMeetingDate, $scope.actualMeetingTime,
			             	                            $scope.LmsModel.Communication[2].interactionType, $scope.LmsModel.Communication[2].count);
			                        }
			                    } else {
									if($scope.LmsModel.Communication[2].followUps.length>0){

										for (i in $scope.LmsModel.Communication[2].followUps){
										if(!($scope.actualMeetingDate==$scope.LmsModel.Communication[2].followUps[i].metDate && $scope.actualMeetingTime==$scope.LmsModel.Communication[2].followUps[i].metTime)){
											
									if(i==$scope.LmsModel.Communication[2].followUps.length-1){
			                        $scope.LmsModel.Communication[2].followUps[followUpsLength] =
			                        		 $scope.getNewFollowUp_Appointment($scope.actualMeetingDate,$scope.actualMeetingTime,
			         	                            $scope.LmsModel.Communication[2].interactionType, $scope.LmsModel.Communication[2].count);
									}
										}
										}
									}
									else if($scope.LmsModel.Communication[2].followUps.length==0){
										   $scope.LmsModel.Communication[2].followUps[followUpsLength] =
			                        		 $scope.getNewFollowUp_Appointment($scope.actualMeetingDate,$scope.actualMeetingTime,
			         	                            $scope.LmsModel.Communication[2].interactionType, $scope.LmsModel.Communication[2].count);
									}
			                    }
			                } else {
								if($scope.LmsModel.Communication[2].followUps.length>0){
									
									for (i in $scope.LmsModel.Communication[2].followUps){
											
										if(($scope.actualMeetingDate==$scope.LmsModel.Communication[2].followUps[i].metDate && $scope.actualMeetingTime==$scope.LmsModel.Communication[2].followUps[i].metTime)){
											duplicateFlag=true;
										}
									}
										for (i in $scope.LmsModel.Communication[2].followUps){
											
										if(!($scope.actualMeetingDate==$scope.LmsModel.Communication[2].followUps[i].metDate && $scope.actualMeetingTime==$scope.LmsModel.Communication[2].followUps[i].metTime)){
									if(i==$scope.LmsModel.Communication[2].followUps.length-1 &&!duplicateFlag){
			                        $scope.LmsModel.Communication[2].followUps[followUpsLength] =
			                        		 $scope.getNewFollowUp_Appointment($scope.actualMeetingDate,$scope.actualMeetingTime,
			         	                            $scope.LmsModel.Communication[2].interactionType, $scope.LmsModel.Communication[2].count);
									}
										}
										}
									}
									else if($scope.LmsModel.Communication[2].followUps.length==0){
			                    $scope.LmsModel.Communication[2].followUps[followUpsLength] =
			                        $scope.getNewFollowUp_Appointment($scope.actualMeetingDate, $scope.actualMeetingTime,
			                            $scope.LmsModel.Communication[2].interactionType, $scope.LmsModel.Communication[2].count);
									}
			                }
							 
							 
			               // $scope.LmsModel.Communication.metDateAndTime = planedMeetingDate + " " + planedMeetingTime;
			               // $scope.LmsModel.Communication[2].metDateAndTime = actualMeetingDate + " " + actualMeetingTime;
			              
			            }
		            	
		            	 $scope.LmsModel.Communication[2].metDate=actualMeetingDate;
		            	 $scope.LmsModel.Communication[2].metTime=actualMeetingTime;
						 	  
							//  var appointmentUIdate=InternalToExternal(actualMeetingDate);
							 // $scope.actualMeetingDate=appointmentUIdate;
		            	 
	              	// $scope.LmsModel.Communication[2].metDateAndTime = actualMeetingDate + " " + actualMeetingTime;
	              }
	            else if (arg1=="Visit"){
	            	
	            	var followUpsLength = $scope.LmsModel.Communication[3].followUps.length;

	                if ($scope.planedMeetingDate) {

		                if ($scope.overWriteFollowUp) {
		                    if ($scope.LmsModel.Communication[3].followUps && $scope.LmsModel.Communication[3].followUps.length > 0) {
		                        var _index = $scope.LmsModel.Communication[3].followUps.length - 1;
		                        if ($scope.LmsModel.Communication[3].followUps[_index].actualDateAndTime) {
		                            $scope.LmsModel.Communication[3].followUps[_index] =
		                            		 $scope.getNewFollowUp_Visit($scope.planedMeetingDate, $scope.communicationStatusCodeVisit,
		             	                            $scope.LmsModel.Communication[3].interactionType, $scope.LmsModel.Communication[3].count);
		                        }
		                    } else {
		                        $scope.LmsModel.Communication[3].followUps[followUpsLength] =
		                        		 $scope.getNewFollowUp_Visit($scope.planedMeetingDate,$scope.communicationStatusCodeVisit,
		         	                            $scope.LmsModel.Communication[1].interactionType, $scope.LmsModel.Communication[1].count);
		                    }
		                } else {
		                    $scope.LmsModel.Communication[3].followUps[followUpsLength] =
		                        $scope.getNewFollowUp_Visit($scope.planedMeetingDate, $scope.communicationStatusCodeVisit,
		                            $scope.LmsModel.Communication[3].interactionType, $scope.LmsModel.Communication[3].count);
		                }
		               // $scope.LmsModel.Communication.metDateAndTime = planedMeetingDate + " " + planedMeetingTime;
		               // $scope.LmsModel.Communication[3].metDateAndTime = actualMeetingDate + " " + actualMeetingTime;
		              
		            }
					
					 //var plannedUIdate=InternalToExternal(planedMeetingDate);
						//	  $scope.planedMeetingDate=plannedUIdate;
              	  $scope.LmsModel.Communication[3].metDate=planedMeetingDate;
              	 
             // 	 $scope.communicationStatusCode= $scope.LmsModel.Communication[3].communicationStatusCode;
              }
	        }
	    };
		
		 $scope.GLI_errorCheck_editCall = function (arg1) {
	        var planedMeetingDate = "";
	        var planedMeetingTime = "";
	        var actualMeetingDate = "";
	        var actualMeetingTime = "";
	        var calldate = "";
	       
	      	        
	        if ($scope.calldate) {
	        	calldate = formatForDateControl($scope.calldate);
	        }
	        
	      //  $scope.LmsModel.Communication.remarks = $scope.remarks;
	        if ($scope.errorCount < 1) {
 
	        	if(arg1=='Call'){
					
					if($scope.LmsModel.Communication[0].interactionType=="Call"){
	         var followUpsLength = $scope.LmsModel.Communication[0].followUps.length;
					
					
	            if ($scope.calldate) {

	                if ($scope.overWriteFollowUp) {
	                    if ($scope.LmsModel.Communication[0].followUps && $scope.LmsModel.Communication[0].followUps.length > 0) {
	                        var _index = $scope.LmsModel.Communication[0].followUps.length - 1;
	                     if ($scope.LmsModel.Communication[0].followUps[_index].metDate) {
	                            $scope.LmsModel.Communication[0].followUps[_index] =
	                            		 $scope.getNewFollowUp_Call($scope.calldate, $scope.communicationStatusCodeCall,
	             	                            $scope.LmsModel.Communication[0].interactionType, $scope.LmsModel.Communication[0].count);
	                       }
	                    } else {
	                        $scope.LmsModel.Communication[0].followUps[followUpsLength] =
	                        		 $scope.getNewFollowUp_Call($scope.calldate, $scope.communicationStatusCodeCall,
	         	                            $scope.LmsModel.Communication[0].interactionType, $scope.LmsModel.Communication[0].count);
	                    }
	                } else {
	                    $scope.LmsModel.Communication[0].followUps[followUpsLength] =
	                        $scope.getNewFollowUp_Call($scope.calldate, $scope.communicationStatusCodeCall,
	                            $scope.LmsModel.Communication[0].interactionType, $scope.LmsModel.Communication[0].count);
	                }
	               // $scope.LmsModel.Communication.metDateAndTime = planedMeetingDate + " " + planedMeetingTime;
	              //  $scope.LmsModel.Communication[0].metDateAndTime = actualMeetingDate + " " + actualMeetingTime;
	              
	            }
	           
	 
	            	
	            	var callcontact={
	            			"Communication":[{
	            				
	            				"metDate":  $scope.LmsModel.Communication[0].metDate ,
	            				"count": $scope.LmsModel.Communication[0].count,
	            				"communicationStatusCode": $scope.LmsModel.Communication[0].communicationStatusCode
	            				
	            			},{
	            				"metDate": "",
	            				"count":0,
	            				"communicationStatusCode":""
	            				
	            			}]
	            	}
	            	
					
              	  $scope.LmsModel.Communication[0].metDate=calldate;
              	  $scope.LmsModel.Communication[0].metType="";
              	  $scope.communicationStatusCode= $scope.LmsModel.Communication[0].communicationStatusCode;
              //	$scope.calldate= $scope.LmsModel.Communication[0].metDate;
              	$scope.count=$scope.LmsModel.Communication[0].count;
              	$scope.CallSectionDate= $scope.LmsModel.Communication[0].metDate;
              
		//	  var callUIdate=InternalToExternal(calldate);
			//  $scope.calldate=callUIdate;
              	
               // $scope.communicationData=Communication;
              	$scope.callMetDate= $scope.LmsModel.Communication[0].metDate;
              	$scope.callcount= $scope.LmsModel.Communication[0].count;
              	$scope.callstatusCode= $scope.LmsModel.Communication[0].communicationStatusCode;
              	 // $scope.callTable= $scope.LmsModel.Communication[0].communicationStatusCode;
              	  //$scope.callTable[1]= $scope.LmsModel.Communication[0].metDate;
              	 
              	  
					}
					
				
					
					else if($scope.LmsModel.Communication[1].interactionType=="Call"){
	         var followUpsLength = $scope.LmsModel.Communication[1].followUps.length;
					
					
	            if ($scope.calldate) {

	                if ($scope.overWriteFollowUp) {
	                    if ($scope.LmsModel.Communication[1].followUps && $scope.LmsModel.Communication[1].followUps.length > 0) {
	                        var _index = $scope.LmsModel.Communication[1].followUps.length - 1;
	                     if ($scope.LmsModel.Communication[1].followUps[_index].metDate) {
	                            $scope.LmsModel.Communication[1].followUps[_index] =
	                            		 $scope.getNewFollowUp_Call($scope.calldate,$scope.communicationStatusCodeCall,
	             	                            $scope.LmsModel.Communication[1].interactionType, $scope.LmsModel.Communication[1].count);
	                       }
	                    } else {
	                        $scope.LmsModel.Communication[1].followUps[followUpsLength] =
	                        		 $scope.getNewFollowUp_Call($scope.calldate,$scope.communicationStatusCodeCall,
	         	                            $scope.LmsModel.Communication[1].interactionType, $scope.LmsModel.Communication[1].count);
	                    }
	                } else {
	                    $scope.LmsModel.Communication[1].followUps[followUpsLength] =
	                        $scope.getNewFollowUp_Call($scope.calldate,$scope.communicationStatusCodeCall,
	                            $scope.LmsModel.Communication[1].interactionType, $scope.LmsModel.Communication[1].count);
	                }
	               // $scope.LmsModel.Communication.metDateAndTime = planedMeetingDate + " " + planedMeetingTime;
	             //   $scope.LmsModel.Communication[1].metDateAndTime = actualMeetingDate + " " + actualMeetingTime;
	              
	            }
	           
	 
	            	
	            	var callcontact={
	            			"Communication":[{
	            				
	            				"metDate":  $scope.LmsModel.Communication[1].metDate ,
	            				"count": $scope.LmsModel.Communication[1].count,
	            				"communicationStatusCode": $scope.LmsModel.Communication[1].communicationStatusCode
	            				
	            			},{
	            				"metDate": "",
	            				"count":0,
	            				"communicationStatusCode":""
	            				
	            			}]
	            	}
	            	
					
              	  $scope.LmsModel.Communication[1].metDate=calldate;
              	  $scope.LmsModel.Communication[1].metType="";
              	  $scope.communicationStatusCode= $scope.LmsModel.Communication[1].communicationStatusCode;
              //	$scope.calldate= $scope.LmsModel.Communication[1].metDate;
              	$scope.count=$scope.LmsModel.Communication[1].count;
              	$scope.CallSectionDate= $scope.LmsModel.Communication[1].metDate;
              
			//  var callUIdate=InternalToExternal(calldate);
			 // $scope.calldate=callUIdate;
              	
               // $scope.communicationData=Communication;
              	$scope.callMetDate= $scope.LmsModel.Communication[1].metDate;
              	$scope.callcount= $scope.LmsModel.Communication[1].count;
              	$scope.callstatusCode= $scope.LmsModel.Communication[1].communicationStatusCode;
              	 // $scope.callTable= $scope.LmsModel.Communication[1].communicationStatusCode;
              	  //$scope.callTable[1]= $scope.LmsModel.Communication[1].metDate;
              	 
              	  
					}
					
					
					
					else if($scope.LmsModel.Communication[2].interactionType=="Call"){
	         var followUpsLength = $scope.LmsModel.Communication[2].followUps.length;
					
					
	            if ($scope.calldate) {

	                if ($scope.overWriteFollowUp) {
	                    if ($scope.LmsModel.Communication[2].followUps && $scope.LmsModel.Communication[2].followUps.length > 0) {
	                        var _index = $scope.LmsModel.Communication[2].followUps.length - 1;
	                     if ($scope.LmsModel.Communication[2].followUps[_index].metDate) {
	                            $scope.LmsModel.Communication[2].followUps[_index] =
	                            		 $scope.getNewFollowUp_Call($scope.calldate, $scope.communicationStatusCodeCall,
	             	                            $scope.LmsModel.Communication[2].interactionType, $scope.LmsModel.Communication[2].count);
	                       }
	                    } else {
	                        $scope.LmsModel.Communication[2].followUps[followUpsLength] =
	                        		 $scope.getNewFollowUp_Call($scope.calldate, $scope.communicationStatusCodeCall,
	         	                            $scope.LmsModel.Communication[2].interactionType, $scope.LmsModel.Communication[2].count);
	                    }
	                } else {
	                    $scope.LmsModel.Communication[2].followUps[followUpsLength] =
	                        $scope.getNewFollowUp_Call($scope.calldate, $scope.communicationStatusCodeCall,
	                            $scope.LmsModel.Communication[2].interactionType, $scope.LmsModel.Communication[2].count);
	                }
	               // $scope.LmsModel.Communication.metDateAndTime = planedMeetingDate + " " + planedMeetingTime;
	             //   $scope.LmsModel.Communication[2].metDateAndTime = actualMeetingDate + " " + actualMeetingTime;
	              
	            }
	           
	 
	            	
	            	var callcontact={
	            			"Communication":[{
	            				
	            				"metDate":  $scope.LmsModel.Communication[2].metDate ,
	            				"count": $scope.LmsModel.Communication[2].count,
	            				"communicationStatusCode": $scope.LmsModel.Communication[2].communicationStatusCode
	            				
	            			},{
	            				"metDate": "",
	            				"count":0,
	            				"communicationStatusCode":""
	            				
	            			}]
	            	}
	            	
					
              	  $scope.LmsModel.Communication[2].metDate=calldate;
              	  $scope.LmsModel.Communication[2].metType="";
              	  $scope.communicationStatusCode= $scope.LmsModel.Communication[2].communicationStatusCode;
              //	$scope.calldate= $scope.LmsModel.Communication[2].metDate;
              	$scope.count=$scope.LmsModel.Communication[2].count;
              	$scope.CallSectionDate= $scope.LmsModel.Communication[2].metDate;
              
			//  var callUIdate=InternalToExternal(calldate);
			//  $scope.calldate=callUIdate;
              	
               // $scope.communicationData=Communication;
              	$scope.callMetDate= $scope.LmsModel.Communication[2].metDate;
              	$scope.callcount= $scope.LmsModel.Communication[2].count;
              	$scope.callstatusCode= $scope.LmsModel.Communication[2].communicationStatusCode;
              	 // $scope.callTable= $scope.LmsModel.Communication[2].communicationStatusCode;
              	  //$scope.callTable[1]= $scope.LmsModel.Communication[2].metDate;
              	 
              	  
					}
					

					
				else if($scope.LmsModel.Communication[3].interactionType=="Call"){
	         var followUpsLength = $scope.LmsModel.Communication[3].followUps.length;
					
					
	            if ($scope.calldate) {

	                if ($scope.overWriteFollowUp) {
	                    if ($scope.LmsModel.Communication[3].followUps && $scope.LmsModel.Communication[3].followUps.length > 0) {
	                        var _index = $scope.LmsModel.Communication[3].followUps.length - 1;
	                     if ($scope.LmsModel.Communication[3].followUps[_index].metDate) {
	                            $scope.LmsModel.Communication[3].followUps[_index] =
	                            		 $scope.getNewFollowUp_Call($scope.calldate,$scope.communicationStatusCodeCall,
	             	                            $scope.LmsModel.Communication[3].interactionType, $scope.LmsModel.Communication[3].count);
	                       }
	                    } else {
	                        $scope.LmsModel.Communication[3].followUps[followUpsLength] =
	                        		 $scope.getNewFollowUp_Call($scope.calldate, $scope.communicationStatusCodeCall,
	         	                            $scope.LmsModel.Communication[3].interactionType, $scope.LmsModel.Communication[3].count);
	                    }
	                } else {
	                    $scope.LmsModel.Communication[3].followUps[followUpsLength] =
	                        $scope.getNewFollowUp_Call($scope.calldate,$scope.communicationStatusCodeCall,
	                            $scope.LmsModel.Communication[3].interactionType, $scope.LmsModel.Communication[3].count);
	                }
	               // $scope.LmsModel.Communication.metDateAndTime = planedMeetingDate + " " + planedMeetingTime;
	               // $scope.LmsModel.Communication[3].metDateAndTime = actualMeetingDate + " " + actualMeetingTime;
	              
	            }
	           
	 
	            	
	            	var callcontact={
	            			"Communication":[{
	            				
	            				"metDate":  $scope.LmsModel.Communication[3].metDate ,
	            				"count": $scope.LmsModel.Communication[3].count,
	            				"communicationStatusCode": $scope.LmsModel.Communication[3].communicationStatusCode
	            				
	            			},{
	            				"metDate": "",
	            				"count":0,
	            				"communicationStatusCode":""
	            				
	            			}]
	            	}
	            	
					
              	  $scope.LmsModel.Communication[3].metDate=calldate;
              	  $scope.LmsModel.Communication[3].metType="";
              	  $scope.communicationStatusCode= $scope.LmsModel.Communication[3].communicationStatusCode;
              //	$scope.calldate= $scope.LmsModel.Communication[3].metDate;
              	$scope.count=$scope.LmsModel.Communication[3].count;
              	$scope.CallSectionDate= $scope.LmsModel.Communication[3].metDate;
              
			// var callUIdate=InternalToExternal(calldate);
			 // $scope.calldate=callUIdate;
              	
               // $scope.communicationData=Communication;
              	$scope.callMetDate= $scope.LmsModel.Communication[3].metDate;
              	$scope.callcount= $scope.LmsModel.Communication[3].count;
              	$scope.callstatusCode= $scope.LmsModel.Communication[3].communicationStatusCode;
              	 // $scope.callTable= $scope.LmsModel.Communication[3].communicationStatusCode;
              	  //$scope.callTable[1]= $scope.LmsModel.Communication[3].metDate;
              	 
              	  
					}
	        	
				}
	        
	        }
	    };
		
		 $scope.GLI_errorCheck_editAppointment = function (arg1) {
	        var planedMeetingDate = "";
	        var planedMeetingTime = "";
	        var actualMeetingDate = "";
	        var actualMeetingTime = "";
	        var calldate = "";
	       
	      	        
	      if ($scope.actualMeetingDate && $scope.actualMeetingTime) {
	            if (typeof $scope.actualMeetingTime == "object") {
	                actualMeetingTime = formatForTimeControl($scope.actualMeetingTime, false);
	            } else {
	                actualMeetingTime = $scope.actualMeetingTime;
	            }
	            actualMeetingDate = formatForDateControl($scope.actualMeetingDate);
	        }
	      //  $scope.LmsModel.Communication.remarks = $scope.remarks;
	        if ($scope.errorCount < 1) {
 
	        	if(arg1=='Appointment'){
					$scope.actualMeetingTime=actualMeetingTime;
					if($scope.LmsModel.Communication[0].interactionType=="Appointment"){
						var duplicateFlag=false;
	         var followUpsLength = $scope.LmsModel.Communication[0].followUps.length;
					
					
	            if ($scope.actualMeetingDate && $scope.actualMeetingTime) {

	                if ($scope.overWriteFollowUp) {
	                    if ($scope.LmsModel.Communication[0].followUps && $scope.LmsModel.Communication[0].followUps.length > 0) {
	                        var _index = $scope.LmsModel.Communication[0].followUps.length - 1;
	                     if ($scope.LmsModel.Communication[0].followUps[_index].metDate) {
	                            $scope.LmsModel.Communication[0].followUps[_index] =
	                            		 $scope.getNewFollowUp_Appointment($scope.actualMeetingDate, $scope.actualMeetingTime,
	             	                            $scope.LmsModel.Communication[0].interactionType, $scope.LmsModel.Communication[0].count);
	                       }
	                    } else {
	                        $scope.LmsModel.Communication[0].followUps[followUpsLength] =
	                        		 $scope.getNewFollowUp_Appointment($scope.actualMeetingDate, $scope.actualMeetingTime,
	         	                            $scope.LmsModel.Communication[0].interactionType, $scope.LmsModel.Communication[0].count);
	                    }
	                } else {
	                   	if($scope.LmsModel.Communication[0].followUps.length>0){
									
									for (i in $scope.LmsModel.Communication[0].followUps){
											
										if(($scope.actualMeetingDate==$scope.LmsModel.Communication[0].followUps[i].metDate && $scope.actualMeetingTime==$scope.LmsModel.Communication[0].followUps[i].metTime)){
											duplicateFlag=true;
										}
									}
										for (i in $scope.LmsModel.Communication[0].followUps){
											
										if(!($scope.actualMeetingDate==$scope.LmsModel.Communication[0].followUps[i].metDate && $scope.actualMeetingTime==$scope.LmsModel.Communication[0].followUps[i].metTime)){
									if(i==$scope.LmsModel.Communication[0].followUps.length-1 &&!duplicateFlag){
			                        $scope.LmsModel.Communication[0].followUps[followUpsLength] =
			                        		 $scope.getNewFollowUp_Appointment($scope.actualMeetingDate,$scope.actualMeetingTime,
			         	                            $scope.LmsModel.Communication[0].interactionType, $scope.LmsModel.Communication[0].count);
									}
										}
										}
									}
									else if($scope.LmsModel.Communication[0].followUps.length==0){
			                    $scope.LmsModel.Communication[0].followUps[followUpsLength] =
			                        $scope.getNewFollowUp_Appointment($scope.actualMeetingDate, $scope.actualMeetingTime,
			                            $scope.LmsModel.Communication[0].interactionType, $scope.LmsModel.Communication[0].count);
									}
	                }
	              
						$scope.LmsModel.Communication[0].metDate=actualMeetingDate;
						 $scope.LmsModel.Communication[0].metTime=actualMeetingTime;
	            }
	           
	 
	            	
	            	var callcontact={
	            			"Communication":[{
	            				
	            				"metDate":  $scope.LmsModel.Communication[0].metDate ,
	            				"count": $scope.LmsModel.Communication[0].count,
	            				"communicationStatusCode": $scope.LmsModel.Communication[0].communicationStatusCode
	            				
	            			},{
	            				"metDate": "",
	            				"count":0,
	            				"communicationStatusCode":""
	            				
	            			}]
	            	}
	            	
					
              	 
              	  $scope.LmsModel.Communication[0].metType="";
              	  $scope.communicationStatusCode= $scope.LmsModel.Communication[0].communicationStatusCode;
              
              	$scope.count=$scope.LmsModel.Communication[0].count;
              	$scope.CallSectionDate= $scope.LmsModel.Communication[0].metDate;
              
			
              	
              
              	$scope.callMetDate= $scope.LmsModel.Communication[0].metDate;
              	$scope.callcount= $scope.LmsModel.Communication[0].count;
              	$scope.callstatusCode= $scope.LmsModel.Communication[0].communicationStatusCode;
              	
              	  
					}
					
				
					
					else if($scope.LmsModel.Communication[1].interactionType=="Appointment"){
						var duplicateFlag=false;
	         var followUpsLength = $scope.LmsModel.Communication[1].followUps.length;
					
					
	            if ($scope.actualMeetingDate && $scope.actualMeetingTime) {

	                if ($scope.overWriteFollowUp) {
	                    if ($scope.LmsModel.Communication[1].followUps && $scope.LmsModel.Communication[1].followUps.length > 0) {
	                        var _index = $scope.LmsModel.Communication[1].followUps.length - 1;
	                     if ($scope.LmsModel.Communication[1].followUps[_index].metDate) {
	                            $scope.LmsModel.Communication[1].followUps[_index] =
	                            		 $scope.getNewFollowUp_Appointment($scope.actualMeetingDate, $scope.actualMeetingTime,
	             	                            $scope.LmsModel.Communication[1].interactionType, $scope.LmsModel.Communication[1].count);
	                       }
	                    } else {
	                        $scope.LmsModel.Communication[1].followUps[followUpsLength] =
	                        		 $scope.getNewFollowUp_Appointment($scope.actualMeetingDate, $scope.actualMeetingTime,
	         	                            $scope.LmsModel.Communication[1].interactionType, $scope.LmsModel.Communication[1].count);
	                    }
	                } else {
	                      	if($scope.LmsModel.Communication[1].followUps.length>0){
									
									for (i in $scope.LmsModel.Communication[1].followUps){
											
										if(($scope.actualMeetingDate==$scope.LmsModel.Communication[1].followUps[i].metDate && $scope.actualMeetingTime==$scope.LmsModel.Communication[1].followUps[i].metTime)){
											duplicateFlag=true;
										}
									}
										for (i in $scope.LmsModel.Communication[1].followUps){
											
										if(!($scope.actualMeetingDate==$scope.LmsModel.Communication[1].followUps[i].metDate && $scope.actualMeetingTime==$scope.LmsModel.Communication[1].followUps[i].metTime)){
									if(i==$scope.LmsModel.Communication[1].followUps.length-1 &&!duplicateFlag){
			                        $scope.LmsModel.Communication[1].followUps[followUpsLength] =
			                        		 $scope.getNewFollowUp_Appointment($scope.actualMeetingDate,$scope.actualMeetingTime,
			         	                            $scope.LmsModel.Communication[1].interactionType, $scope.LmsModel.Communication[1].count);
									}
										}
										}
									}
									else if($scope.LmsModel.Communication[1].followUps.length==0){
			                    $scope.LmsModel.Communication[1].followUps[followUpsLength] =
			                        $scope.getNewFollowUp_Appointment($scope.actualMeetingDate, $scope.actualMeetingTime,
			                            $scope.LmsModel.Communication[1].interactionType, $scope.LmsModel.Communication[1].count);
									}
	                }
						$scope.LmsModel.Communication[1].metDate=actualMeetingDate;
						$scope.LmsModel.Communication[1].metTime=actualMeetingTime;
	            }
	           
	 
	            	
	            	var callcontact={
	            			"Communication":[{
	            				
	            				"metDate":  $scope.LmsModel.Communication[1].metDate ,
	            				"count": $scope.LmsModel.Communication[1].count,
	            				"communicationStatusCode": $scope.LmsModel.Communication[1].communicationStatusCode
	            				
	            			},{
	            				"metDate": "",
	            				"count":0,
	            				"communicationStatusCode":""
	            				
	            			}]
	            	}
	            	
					
              	 
              	  $scope.LmsModel.Communication[1].metType="";
              	  $scope.communicationStatusCode= $scope.LmsModel.Communication[1].communicationStatusCode;
             
              	$scope.count=$scope.LmsModel.Communication[1].count;
              	$scope.CallSectionDate= $scope.LmsModel.Communication[1].metDate;
              
			
              	$scope.callMetDate= $scope.LmsModel.Communication[1].metDate;
              	$scope.callcount= $scope.LmsModel.Communication[1].count;
              	$scope.callstatusCode= $scope.LmsModel.Communication[1].communicationStatusCode;
              	
              	  
					}
					
					
					else if($scope.LmsModel.Communication[2].interactionType=="Appointment"){
						var duplicateFlag=false;
	         var followUpsLength = $scope.LmsModel.Communication[2].followUps.length;
					
					
	            if ($scope.actualMeetingDate && $scope.actualMeetingTime) {

	                if ($scope.overWriteFollowUp) {
	                    if ($scope.LmsModel.Communication[2].followUps && $scope.LmsModel.Communication[2].followUps.length > 0) {
	                        var _index = $scope.LmsModel.Communication[2].followUps.length - 1;
	                     if ($scope.LmsModel.Communication[2].followUps[_index].metDate) {
	                            $scope.LmsModel.Communication[2].followUps[_index] =
	                            		 $scope.getNewFollowUp_Appointment($scope.actualMeetingDate, $scope.actualMeetingTime,
	             	                            $scope.LmsModel.Communication[2].interactionType, $scope.LmsModel.Communication[2].count);
	                       }
	                    } else {
	                        $scope.LmsModel.Communication[2].followUps[followUpsLength] =
	                        		 $scope.getNewFollowUp_Appointment($scope.actualMeetingDate, $scope.actualMeetingTime,
	         	                            $scope.LmsModel.Communication[2].interactionType, $scope.LmsModel.Communication[2].count);
	                    }
	                } else {
	                  if($scope.LmsModel.Communication[2].followUps.length>0){
									
									for (i in $scope.LmsModel.Communication[2].followUps){
											
										if(($scope.actualMeetingDate==$scope.LmsModel.Communication[2].followUps[i].metDate && $scope.actualMeetingTime==$scope.LmsModel.Communication[2].followUps[i].metTime)){
											duplicateFlag=true;
										}
									}
										for (i in $scope.LmsModel.Communication[2].followUps){
											
										if(!($scope.actualMeetingDate==$scope.LmsModel.Communication[2].followUps[i].metDate && $scope.actualMeetingTime==$scope.LmsModel.Communication[2].followUps[i].metTime)){
									if(i==$scope.LmsModel.Communication[2].followUps.length-1 &&!duplicateFlag){
			                        $scope.LmsModel.Communication[2].followUps[followUpsLength] =
			                        		 $scope.getNewFollowUp_Appointment($scope.actualMeetingDate,$scope.actualMeetingTime,
			         	                            $scope.LmsModel.Communication[2].interactionType, $scope.LmsModel.Communication[2].count);
									}
										}
										}
									}
									else if($scope.LmsModel.Communication[2].followUps.length==0){
			                    $scope.LmsModel.Communication[2].followUps[followUpsLength] =
			                        $scope.getNewFollowUp_Appointment($scope.actualMeetingDate, $scope.actualMeetingTime,
			                            $scope.LmsModel.Communication[2].interactionType, $scope.LmsModel.Communication[2].count);
									}
	                }
	              $scope.LmsModel.Communication[2].metDate=actualMeetingDate;
	               $scope.LmsModel.Communication[2].metTime=actualMeetingTime;
	            }
	           
	 
	            	
	            	var callcontact={
	            			"Communication":[{
	            				
	            				"metDate":  $scope.LmsModel.Communication[2].metDate ,
	            				"count": $scope.LmsModel.Communication[2].count,
	            				"communicationStatusCode": $scope.LmsModel.Communication[2].communicationStatusCode
	            				
	            			},{
	            				"metDate": "",
	            				"count":0,
	            				"communicationStatusCode":""
	            				
	            			}]
	            	}
	            	
					
              	 
              	  $scope.LmsModel.Communication[2].metType="";
              	  $scope.communicationStatusCode= $scope.LmsModel.Communication[2].communicationStatusCode;
              
              	$scope.count=$scope.LmsModel.Communication[2].count;
              	$scope.CallSectionDate= $scope.LmsModel.Communication[2].metDate;
              
              	$scope.callMetDate= $scope.LmsModel.Communication[2].metDate;
              	$scope.callcount= $scope.LmsModel.Communication[2].count;
              	$scope.callstatusCode= $scope.LmsModel.Communication[2].communicationStatusCode;
              	
              	  
					}
					

					
					else if($scope.LmsModel.Communication[3].interactionType=="Appointment"){
						var duplicateFlag=false;
	         var followUpsLength = $scope.LmsModel.Communication[3].followUps.length;
					
					
	            if ($scope.actualMeetingDate && $scope.actualMeetingTime) {

	                if ($scope.overWriteFollowUp) {
	                    if ($scope.LmsModel.Communication[3].followUps && $scope.LmsModel.Communication[3].followUps.length > 0) {
	                        var _index = $scope.LmsModel.Communication[3].followUps.length - 1;
	                     if ($scope.LmsModel.Communication[3].followUps[_index].metDate) {
	                            $scope.LmsModel.Communication[3].followUps[_index] =
	                            		 $scope.getNewFollowUp_Appointment($scope.actualMeetingDate, $scope.actualMeetingTime,
	             	                            $scope.LmsModel.Communication[3].interactionType, $scope.LmsModel.Communication[3].count);
	                       }
	                    } else {
	                        $scope.LmsModel.Communication[3].followUps[followUpsLength] =
	                        		 $scope.getNewFollowUp_Appointment($scope.actualMeetingDate, $scope.actualMeetingTime,
	         	                            $scope.LmsModel.Communication[3].interactionType, $scope.LmsModel.Communication[3].count);
	                    }
	                } else {
	                   if($scope.LmsModel.Communication[3].followUps.length>0){
									
									for (i in $scope.LmsModel.Communication[3].followUps){
											
										if(($scope.actualMeetingDate==$scope.LmsModel.Communication[3].followUps[i].metDate && $scope.actualMeetingTime==$scope.LmsModel.Communication[3].followUps[i].metTime)){
											duplicateFlag=true;
										}
									}
										for (i in $scope.LmsModel.Communication[3].followUps){
											
										if(!($scope.actualMeetingDate==$scope.LmsModel.Communication[3].followUps[i].metDate && $scope.actualMeetingTime==$scope.LmsModel.Communication[3].followUps[i].metTime)){
									if(i==$scope.LmsModel.Communication[3].followUps.length-1 &&!duplicateFlag){
			                        $scope.LmsModel.Communication[3].followUps[followUpsLength] =
			                        		 $scope.getNewFollowUp_Appointment($scope.actualMeetingDate,$scope.actualMeetingTime,
			         	                            $scope.LmsModel.Communication[3].interactionType, $scope.LmsModel.Communication[3].count);
									}
										}
										}
									}
									else if($scope.LmsModel.Communication[3].followUps.length==0){
			                    $scope.LmsModel.Communication[3].followUps[followUpsLength] =
			                        $scope.getNewFollowUp_Appointment($scope.actualMeetingDate, $scope.actualMeetingTime,
			                            $scope.LmsModel.Communication[3].interactionType, $scope.LmsModel.Communication[3].count);
									}
	                }
	              
	              $scope.LmsModel.Communication[3].metDate=actualMeetingDate;
				   $scope.LmsModel.Communication[3].metTime=actualMeetingTime;
	            }
	           
	 
	            	
	            	var callcontact={
	            			"Communication":[{
	            				
	            				"metDate":  $scope.LmsModel.Communication[3].metDate ,
	            				"count": $scope.LmsModel.Communication[3].count,
	            				"communicationStatusCode": $scope.LmsModel.Communication[3].communicationStatusCode
	            				
	            			},{
	            				"metDate": "",
	            				"count":0,
	            				"communicationStatusCode":""
	            				
	            			}]
	            	}
	            	
					
              	 
              	  $scope.LmsModel.Communication[3].metType="";
              	  $scope.communicationStatusCode= $scope.LmsModel.Communication[3].communicationStatusCode;
            
              	$scope.count=$scope.LmsModel.Communication[3].count;
              	$scope.CallSectionDate= $scope.LmsModel.Communication[3].metDate;
              
              	$scope.callMetDate= $scope.LmsModel.Communication[3].metDate;
              	$scope.callcount= $scope.LmsModel.Communication[3].count;
              	$scope.callstatusCode= $scope.LmsModel.Communication[3].communicationStatusCode;
              
					}
	        	
				}
	        
	        }
	    };
		
		 $scope.GLI_errorCheck_editVisit = function (arg1) {
	        var planedMeetingDate = "";
	        var planedMeetingTime = "";
	        var actualMeetingDate = "";
	        var actualMeetingTime = "";
	        var calldate = "";
	       
	      	        
	        if ($scope.planedMeetingDate) {
	        	planedMeetingDate = formatForDateControl($scope.planedMeetingDate);
	        }
	        
	    
	        if ($scope.errorCount < 1) {
 
	        	if(arg1=='Visit'){
				
					if($scope.LmsModel.Communication[0].interactionType=="Visit"){
	         var followUpsLength = $scope.LmsModel.Communication[0].followUps.length;
					
					
	            if ($scope.planedMeetingDate) {

	                if ($scope.overWriteFollowUp) {
	                    if ($scope.LmsModel.Communication[0].followUps && $scope.LmsModel.Communication[0].followUps.length > 0) {
	                        var _index = $scope.LmsModel.Communication[0].followUps.length - 1;
	                     if ($scope.LmsModel.Communication[0].followUps[_index].metDate) {
	                            $scope.LmsModel.Communication[0].followUps[_index] =
	                            		 $scope.getNewFollowUp_Visit($scope.planedMeetingDate, $scope.communicationStatusCodeVisit,
	             	                            $scope.LmsModel.Communication[0].interactionType, $scope.LmsModel.Communication[0].count);
	                       }
	                    } else {
	                        $scope.LmsModel.Communication[0].followUps[followUpsLength] =
	                        		 $scope.getNewFollowUp_Visit($scope.planedMeetingDate,$scope.communicationStatusCodeVisit,
	         	                            $scope.LmsModel.Communication[0].interactionType, $scope.LmsModel.Communication[0].count);
	                    }
	                } else {
	                    $scope.LmsModel.Communication[0].followUps[followUpsLength] =
	                       $scope.getNewFollowUp_Visit($scope.planedMeetingDate,$scope.communicationStatusCodeVisit,
	                            $scope.LmsModel.Communication[0].interactionType, $scope.LmsModel.Communication[0].count);
	                }

	            }
	           
	 
	            	
	            	var callcontact={
	            			"Communication":[{
	            				
	            				"metDate":  $scope.LmsModel.Communication[0].metDate ,
	            				"count": $scope.LmsModel.Communication[0].count,
	            				"communicationStatusCode": $scope.LmsModel.Communication[0].communicationStatusCode
	            				
	            			},{
	            				"metDate": "",
	            				"count":0,
	            				"communicationStatusCode":""
	            				
	            			}]
	            	}
	            	
					
              	  $scope.LmsModel.Communication[0].metDate=calldate;
              	  $scope.LmsModel.Communication[0].metType="";
              	  $scope.communicationStatusCode= $scope.LmsModel.Communication[0].communicationStatusCode;
            
              	$scope.count=$scope.LmsModel.Communication[0].count;
              	$scope.CallSectionDate= $scope.LmsModel.Communication[0].metDate;
              
              	$scope.callMetDate= $scope.LmsModel.Communication[0].metDate;
              	$scope.callcount= $scope.LmsModel.Communication[0].count;
              	$scope.callstatusCode= $scope.LmsModel.Communication[0].communicationStatusCode;
              	 
              	  
					}
					
				
					
					else if($scope.LmsModel.Communication[1].interactionType=="Visit"){
	         var followUpsLength = $scope.LmsModel.Communication[1].followUps.length;
					
					
	            if ($scope.planedMeetingDate) {

	                if ($scope.overWriteFollowUp) {
	                    if ($scope.LmsModel.Communication[1].followUps && $scope.LmsModel.Communication[1].followUps.length > 0) {
	                        var _index = $scope.LmsModel.Communication[1].followUps.length - 1;
	                     if ($scope.LmsModel.Communication[1].followUps[_index].metDate) {
	                            $scope.LmsModel.Communication[1].followUps[_index] =
	                            		 $scope.getNewFollowUp_Visit($scope.planedMeetingDate,$scope.communicationStatusCodeVisit,
	             	                            $scope.LmsModel.Communication[1].interactionType, $scope.LmsModel.Communication[1].count);
	                       }
	                    } else {
	                        $scope.LmsModel.Communication[1].followUps[followUpsLength] =
	                        	 $scope.getNewFollowUp_Visit($scope.planedMeetingDate,$scope.communicationStatusCodeVisit,
	         	                            $scope.LmsModel.Communication[1].interactionType, $scope.LmsModel.Communication[1].count);
	                    }
	                } else {
	                    $scope.LmsModel.Communication[1].followUps[followUpsLength] =
	                      $scope.getNewFollowUp_Visit($scope.planedMeetingDate, $scope.communicationStatusCodeVisit,
	                            $scope.LmsModel.Communication[1].interactionType, $scope.LmsModel.Communication[1].count);
	                }
	              
	            }
	           
	 
	            	
	            	var callcontact={
	            			"Communication":[{
	            				
	            				"metDate":  $scope.LmsModel.Communication[1].metDate ,
	            				"count": $scope.LmsModel.Communication[1].count,
	            				"communicationStatusCode": $scope.LmsModel.Communication[1].communicationStatusCode
	            				
	            			},{
	            				"metDate": "",
	            				"count":0,
	            				"communicationStatusCode":""
	            				
	            			}]
	            	}
	            	
					
              	  $scope.LmsModel.Communication[1].metDate=calldate;
              	  $scope.LmsModel.Communication[1].metType="";
              	  $scope.communicationStatusCode= $scope.LmsModel.Communication[1].communicationStatusCode;
             
              	$scope.count=$scope.LmsModel.Communication[1].count;
              	$scope.CallSectionDate= $scope.LmsModel.Communication[1].metDate;
              
              	$scope.callMetDate= $scope.LmsModel.Communication[1].metDate;
              	$scope.callcount= $scope.LmsModel.Communication[1].count;
              	$scope.callstatusCode= $scope.LmsModel.Communication[1].communicationStatusCode;
              	}
					
				
					else if($scope.LmsModel.Communication[2].interactionType=="Visit"){
	         var followUpsLength = $scope.LmsModel.Communication[2].followUps.length;
					
					
	            if ($scope.planedMeetingDate) {

	                if ($scope.overWriteFollowUp) {
	                    if ($scope.LmsModel.Communication[2].followUps && $scope.LmsModel.Communication[2].followUps.length > 0) {
	                        var _index = $scope.LmsModel.Communication[2].followUps.length - 1;
	                     if ($scope.LmsModel.Communication[2].followUps[_index].metDate) {
	                            $scope.LmsModel.Communication[2].followUps[_index] =
	                            		 $scope.getNewFollowUp_Visit($scope.planedMeetingDate,$scope.communicationStatusCodeVisit,
	             	                            $scope.LmsModel.Communication[2].interactionType, $scope.LmsModel.Communication[2].count);
	                       }
	                    } else {
	                        $scope.LmsModel.Communication[2].followUps[followUpsLength] =
	                        		 $scope.getNewFollowUp_Visit($scope.planedMeetingDate,$scope.communicationStatusCodeVisit,
	         	                            $scope.LmsModel.Communication[2].interactionType, $scope.LmsModel.Communication[2].count);
	                    }
	                } else {
	                    $scope.LmsModel.Communication[2].followUps[followUpsLength] =
	                        $scope.getNewFollowUp_Visit($scope.planedMeetingDate,$scope.communicationStatusCodeVisit,
	                            $scope.LmsModel.Communication[2].interactionType, $scope.LmsModel.Communication[2].count);
	                }
	              
	            }
	           
	 
	            	
	            	var callcontact={
	            			"Communication":[{
	            				
	            				"metDate":  $scope.LmsModel.Communication[2].metDate ,
	            				"count": $scope.LmsModel.Communication[2].count,
	            				"communicationStatusCode": $scope.LmsModel.Communication[2].communicationStatusCode
	            				
	            			},{
	            				"metDate": "",
	            				"count":0,
	            				"communicationStatusCode":""
	            				
	            			}]
	            	}
	            	
					
              	  $scope.LmsModel.Communication[2].metDate=calldate;
              	  $scope.LmsModel.Communication[2].metType="";
              	  $scope.communicationStatusCode= $scope.LmsModel.Communication[2].communicationStatusCode;
           
              	$scope.count=$scope.LmsModel.Communication[2].count;
              	$scope.CallSectionDate= $scope.LmsModel.Communication[2].metDate;
            
              	$scope.callMetDate= $scope.LmsModel.Communication[2].metDate;
              	$scope.callcount= $scope.LmsModel.Communication[2].count;
              	$scope.callstatusCode= $scope.LmsModel.Communication[2].communicationStatusCode;
              	 
					}
					

					
				  else if($scope.LmsModel.Communication[3].interactionType=="Visit"){
	         var followUpsLength = $scope.LmsModel.Communication[3].followUps.length;
					
					
	            if ($scope.planedMeetingDate) {

	                if ($scope.overWriteFollowUp) {
	                    if ($scope.LmsModel.Communication[3].followUps && $scope.LmsModel.Communication[3].followUps.length > 0) {
	                        var _index = $scope.LmsModel.Communication[3].followUps.length - 1;
	                     if ($scope.LmsModel.Communication[3].followUps[_index].metDate) {
	                            $scope.LmsModel.Communication[3].followUps[_index] =
	                            		 $scope.getNewFollowUp_Visit($scope.planedMeetingDate, $scope.communicationStatusCodeVisit,
	             	                            $scope.LmsModel.Communication[3].interactionType, $scope.LmsModel.Communication[3].count);
	                       }
	                    } else {
	                        $scope.LmsModel.Communication[3].followUps[followUpsLength] =
	                        		 $scope.getNewFollowUp_Visit($scope.planedMeetingDate, $scope.communicationStatusCodeVisit,
	         	                            $scope.LmsModel.Communication[3].interactionType, $scope.LmsModel.Communication[3].count);
	                    }
	                } else {
	                    $scope.LmsModel.Communication[3].followUps[followUpsLength] =
	                        $scope.getNewFollowUp_Visit($scope.planedMeetingDate, $scope.communicationStatusCodeVisit,
	                            $scope.LmsModel.Communication[3].interactionType, $scope.LmsModel.Communication[3].count);
	                }
	           
	            }
	           
	 
	            	
	            	var callcontact={
	            			"Communication":[{
	            				
	            				"metDate":  $scope.LmsModel.Communication[3].metDate ,
	            				"count": $scope.LmsModel.Communication[3].count,
	            				"communicationStatusCode": $scope.LmsModel.Communication[3].communicationStatusCode
	            				
	            			},{
	            				"metDate": "",
	            				"count":0,
	            				"communicationStatusCode":""
	            				
	            			}]
	            	}
	            	
					
              	  $scope.LmsModel.Communication[3].metDate=calldate;
              	  $scope.LmsModel.Communication[3].metType="";
              	  $scope.communicationStatusCode= $scope.LmsModel.Communication[3].communicationStatusCode;
              
              	$scope.count=$scope.LmsModel.Communication[3].count;
              	$scope.CallSectionDate= $scope.LmsModel.Communication[3].metDate;
             
              	$scope.callMetDate= $scope.LmsModel.Communication[3].metDate;
              	$scope.callcount= $scope.LmsModel.Communication[3].count;
              	$scope.callstatusCode= $scope.LmsModel.Communication[3].communicationStatusCode;
              	
              	  
					}
	        	
				}
	        
	        }
	    };

	    $scope.GLI_Save = function () {
	        LmsVariables.totalAPE = 0;
	        if ($scope.LmsModel.Policies.productSold && $scope.LmsModel.Policies.productSold.length > 0 && $scope.LmsModel.Policies.productSold[0].premium != "") {
	            for (var indx = 0; indx < $scope.LmsModel.Policies.productSold.length; indx++) {
	                LmsVariables.totalAPE = Number(LmsVariables.totalAPE) + Number($scope.LmsModel.Policies.productSold[indx].premium);
	            }
	        }
	        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
	        $scope.validateFields('callDetailsSection');

	        LmsVariables.setLmsModel($scope.LmsModel);
	        if ($scope.errorCount > 0) {
	            $scope.popupFlag = true;
	            $rootScope.showHideLoadingImage(false);
	            // $scope.lePopupCtrl.showWarning(
	                // translateMessages($translate,
	                    // "errorHeader"),
	                // translateMessages($translate,
	                    // "enterMandatory"),
	                // translateMessages($translate,
	                    // "lms.ok"));
				$scope.leadDetShowPopUpMsg = true;
				$scope.enterMandatory = translateMessages($translate, "enterMandatory");
	        } else {
	            $scope.GLI_errorCheck();
	            GLI_LmsService.saveTransactions($scope.onSaveSuccess, $scope.onSaveError,
	                $scope.LmsModel);
	        }
	    };
	    
   $scope.GLI_Save_custom = function (arg1) {
	    //temp fix for localStorage
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		}
	        LmsVariables.totalAPE = 0;
	        if ($scope.LmsModel.Policies.productSold && $scope.LmsModel.Policies.productSold.length > 0 && $scope.LmsModel.Policies.productSold[0].premium != "") {
	            for (var indx = 0; indx < $scope.LmsModel.Policies.productSold.length; indx++) {
	                LmsVariables.totalAPE = Number(LmsVariables.totalAPE) + Number($scope.LmsModel.Policies.productSold[indx].premium);
	            }
	        }
	        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
	        $scope.validateFields('callDetailsSection',arg1);
			
	        LmsVariables.setLmsModel($scope.LmsModel);
	        if ($scope.errorCount > 0) {
	            $scope.popupFlag = true;
	            $rootScope.showHideLoadingImage(false);
				$scope.leadDetShowPopUpMsg = true;
				$scope.enterMandatory = translateMessages($translate, "enterMandatory");
	        } else {
				
			for(i in $scope.LmsModel.Communication){
				for(j in $scope.LmsModel.Communication[i].followUps){
					var date=$scope.LmsModel.Communication[i].followUps[j].metDate;
				
					if(date.indexOf('/')>0){
					var dateArr=date.split('/');
					$scope.LmsModel.Communication[i].followUps[j].metDate=dateArr[2] +'-'+ dateArr[1]+ '-' +dateArr[0];
					}
				}
			}
			for(i in $scope.LmsModel.Communication){
				for(j in $scope.LmsModel.Communication[i].followUps){
					var date=$scope.LmsModel.Communication[i].followUps[j].metDate;
				
						if(date.indexOf('-')>0){
						var dateArr=date.split('-');
						var modelYear=dateArr[0];
						var currDate=new Date();
						var currYear=currDate.getFullYear();
						if(arg1=="Call" || arg1=="Visit"){
							if(modelYear>currYear){
								var englishYear=Number(modelYear)-543;
								$scope.LmsModel.Communication[i].followUps[j].metDate=englishYear+ '-' +dateArr[1]+ '-' +dateArr[2];
						}
						
					
					}
					
					
					else if(arg1=="Appointment"){
						var thaiYear=Number(currYear)+543;
							if(modelYear>=thaiYear){
								var englishYear=Number(modelYear)-543;
								$scope.LmsModel.Communication[i].followUps[j].metDate=englishYear+ '-' +dateArr[1]+ '-' +dateArr[2];
						}
						
					
					}
					}
				}
			}
			
	        	
	        	if(arg1=="Call"){
	            
	        		$scope.LmsModel.Communication[1].count++;
	        		if(!$scope.editMode){
						$scope.GLI_errorCheck_custom('Call');
						 GLI_LmsService.saveTransactions($scope.onCallSaveSuccess, $scope.onSaveError,
						$scope.LmsModel);
					}
					else {
						$scope.GLI_errorCheck_editCall('Call');
						 GLI_LmsService.saveTransactions($scope.onEditCallSuccess, $scope.onSaveError,
						$scope.LmsModel);
					}
	           
	        }
	            else if(arg1=="Appointment"){
	            	
	            	$scope.LmsModel.Communication[2].count++;
	            	
	            	if(!$scope.editMode){
						$scope.GLI_errorCheck_custom('Appointment');
		            GLI_LmsService.saveTransactions($scope.onAppointmentSaveSuccess, $scope.onSaveError,
		                $scope.LmsModel);
					}
					else {
						$scope.GLI_errorCheck_editAppointment('Appointment');
						 GLI_LmsService.saveTransactions($scope.onEditAppointmentSuccess, $scope.onSaveError,
						$scope.LmsModel);
					}
	            }

	            else if(arg1=="Visit"){
	            	
	            	$scope.LmsModel.Communication[3].count++;
	            	
	            	if(!$scope.editMode){
						$scope.GLI_errorCheck_custom('Visit');
		            GLI_LmsService.saveTransactions($scope.onVisitSaveSuccess, $scope.onSaveError,
		                $scope.LmsModel);
					}
					else {
						$scope.GLI_errorCheck_editVisit('Visit');
						 GLI_LmsService.saveTransactions($scope.onEditVisitSuccess, $scope.onSaveError,
						$scope.LmsModel);
					}
	            }
	        }
	    };

	    $scope.cancelfunction = function () {
	        $location.path('/lms');
	        $scope.refresh();
	    };

	    $scope.closeFunction = function () {
	       $location.path('/lms');
	        $scope.refresh();
	    };
	   
	    $scope.onCallSaveSuccess = function () {
			
			$scope.calldate = "";
			$scope.communicationStatusCodeCall="";
			$scope.communicationStatusCodeVisit="";
			$scope.planedMeetingDate="";
			$scope.actualMeetingDate="";
			$scope.actualMeetingTime="";
	        //Clear the actual meeting date and time to prevent the additional entry on multiple save click
	        if ($scope.LmsModel.Policies.productSold.submissionDate != "" && $scope.LmsModel.Policies.productSold.submissionDate != null &&
	            typeof $scope.LmsModel.Policies.productSold.submissionDate != "undefined") {
	            $scope.LmsModel.Policies.productSold.submissionDate = new Date($scope.LmsModel.Policies.productSold.submissionDate);
	        }
	        var actualMeetingDateTime = $scope.actualMeetingDate + " " + $scope.actualMeetingTime;
	        if (!(rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
	            var t = $scope.LmsModel.Communication[0].metDateAndTime.split(/[- :]/);
	            if ($scope.planedMeetingTime != "" && $scope.planedMeetingTime != null && typeof $scope.planedMeetingTime != "object") {
	                $scope.planedMeetingTime = new Date(t[0], t[1] - 1, t[2], t[3], t[4]);
	            }
	            if (typeof $scope.actualMeetingTime != "object" && $scope.actualMeetingTime != "" && $scope.actualMeetingTime != null) {
	                $scope.actualMeetingTime = new Date(actualMeetingDateTime);
	            }
	        } else {
	            $scope.planedMeetingTime = LEDate($scope.LmsModel.Communication[0].metDateAndTime);
	            if (typeof $scope.actualMeetingTime != "object" && $scope.actualMeetingTime != "" && $scope.actualMeetingTime != null) {
	                $scope.actualMeetingTime = LEDate(actualMeetingDateTime);
	            }
	        }
	        if ($scope.LmsModel.Lead.StatusDetails.disposition != "Successful Sale") {
	            var eventAlreadyExist = false;
	            if ((rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
	                if ($scope.LmsModel.Communication[0].metDateAndTime.trim() != "") {
	                    var metDateAndTime = LEDate($scope.LmsModel.Communication[0].metDateAndTime);
	                    for (var i = 0; i < $scope.LmsModel.Communication[0].followUps.length - 1; i++) {
	                        if (metDateAndTime - LEDate($scope.LmsModel.Communication[0].followUps[i].planedDateAndTime) == 0) {
	                            eventAlreadyExist = true;
	                            break;
	                        }
	                    }
	                    if (!eventAlreadyExist) {
	                        var startDate = angular.copy(metDateAndTime);
	                        var endDate = angular.copy(metDateAndTime);
	                        endDate.setHours(startDate.getHours() + parseInt(rootConfig.leadMeetingTimeInHr));
	                        var title = translateMessages($translate, "lms.eventTitle") + $scope.LmsModel.Lead.BasicDetails.fullName;
	                        var location = "";
	                        var notes = translateMessages($translate, "lms.eventNote");
	                        var reminderTimeInMin = rootConfig.reminderTimeInMin;
	                        var success = function (message) {
	                        };
	                        var error = function (message) {
	                        };
	                        window.plugins.calendar.createEvent(title, location, notes, startDate, endDate, reminderTimeInMin, success, error);
	                    }
	                } else if ($scope.LmsModel.Communication[0].followUps.length == 1 && $scope.LmsModel.Communication[0].followUps[0].planedDateAndTime != "") {
	                    var metDateAndTime = LEDate($scope.LmsModel.Communication[0].followUps[0].planedDateAndTime);
	                    var startDate = angular.copy(metDateAndTime);
	                    var endDate = angular.copy(metDateAndTime);
	                    endDate.setHours(startDate.getHours() + parseInt(rootConfig.leadMeetingTimeInHr));
	                    var title = translateMessages($translate, "lms.eventTitle") + $scope.LmsModel.Lead.BasicDetails.fullName;
	                    var location = "";
	                    var notes = translateMessages($translate, "lms.eventNote");
	                    var reminderTimeInMin = rootConfig.reminderTimeInMin;
	                    var success = function (message) {
	                    };
	                    var error = function (message) {
	                    };
	                    window.plugins.calendar.createEvent(title, location, notes, startDate, endDate, reminderTimeInMin, success, error);
	                }
	            }
	        } else {
	            $scope.disableSavedPolicyDetails();
	        }

			paintCallMeetingHistory();
			paintAppointmentMeetingHistory_init();
			paintVisitMeetingHistory_init();
			
			
			
	        	
			
	        $rootScope.showHideLoadingImage(false);
	        if (!$scope.editMode) {
	            $scope.userDetails = UserDetailsService.getUserDetailsModel();
	            if ($scope.userDetails.agentType == "BranchUser") {
	                $rootScope.lePopupCtrl.showError(
	                    "OmniChannel",
	                    translateMessages($translate,
	                        "leadCreationSuccMsg"),
	                    translateMessages($translate,
	                        "btnClose"),
	                    $scope.closeFunction,
	                    function () {
	                        $rootScope
	                            .showHideLoadingImage(false);
	                    });

	            }
	        } else if (!$scope.fromPopupTabsNav) {
	            //repaintMeetingHistory();
			
	        	
				
				
	        	
			
	            $scope.validateFields('callDetailsSection');
	            $rootScope.lePopupCtrl
	                .showSuccess(translateMessages(
	                        $translate, "successHeader"),
	                    translateMessages($translate,
	                        "leadSavedEditMode"),
	                    translateMessages($translate,
	                        "lms.close"));
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                    $scope.callback();
	                }, $scope, $compile);
	        }

	    };
		
		  $scope.onAppointmentSaveSuccess = function () {
			
			$scope.calldate = "";
			$scope.communicationStatusCodeCall="";
			$scope.communicationStatusCodeVisit="";
			$scope.planedMeetingDate="";
			$scope.actualMeetingDate="";
			$scope.actualMeetingTime="";
	        //Clear the actual meeting date and time to prevent the additional entry on multiple save click
	        if ($scope.LmsModel.Policies.productSold.submissionDate != "" && $scope.LmsModel.Policies.productSold.submissionDate != null &&
	            typeof $scope.LmsModel.Policies.productSold.submissionDate != "undefined") {
	            $scope.LmsModel.Policies.productSold.submissionDate = new Date($scope.LmsModel.Policies.productSold.submissionDate);
	        }
	        var actualMeetingDateTime = $scope.actualMeetingDate + " " + $scope.actualMeetingTime;
	        if (!(rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
	            var t = $scope.LmsModel.Communication[0].metDateAndTime.split(/[- :]/);
	            if ($scope.planedMeetingTime != "" && $scope.planedMeetingTime != null && typeof $scope.planedMeetingTime != "object") {
	                $scope.planedMeetingTime = new Date(t[0], t[1] - 1, t[2], t[3], t[4]);
	            }
	            if (typeof $scope.actualMeetingTime != "object" && $scope.actualMeetingTime != "" && $scope.actualMeetingTime != null) {
	                $scope.actualMeetingTime = new Date(actualMeetingDateTime);
	            }
	        } else {
	            $scope.planedMeetingTime = LEDate($scope.LmsModel.Communication[0].metDateAndTime);
	            if (typeof $scope.actualMeetingTime != "object" && $scope.actualMeetingTime != "" && $scope.actualMeetingTime != null) {
	                $scope.actualMeetingTime = LEDate(actualMeetingDateTime);
	            }
	        }
	        if ($scope.LmsModel.Lead.StatusDetails.disposition != "Successful Sale") {
	            var eventAlreadyExist = false;
	            if ((rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
					var metDate=$scope.LmsModel.Communication[2].metDate;
					var metTime=$scope.LmsModel.Communication[2].metTime;
					var metDateTime=metDate + " " + metTime;
	                if (metDateTime.trim() != "") {
	                    var metDateAndTime = LEDate(metDateTime);
	                    for (var i = 0; i < $scope.LmsModel.Communication[2].followUps.length; i++) {
							if(i!=$scope.LmsModel.Communication[2].followUps.length-1){
							var metDateFollowUp=$scope.LmsModel.Communication[2].followUps[i].metDate;
							var metTimeFollowUp=$scope.LmsModel.Communication[2].followUps[i].metTime;
							var metDateTimeFollowUp=metDateFollowUp + " " + metTimeFollowUp;
							}
	                        if (metDateAndTime - LEDate(metDateTimeFollowUp) == 0) {
	                            eventAlreadyExist = true;
	                            break;
	                        }
	                    }
	                    if (!eventAlreadyExist) {
	                        var startDate = angular.copy(metDateAndTime);
	                        var endDate = angular.copy(metDateAndTime);
	                        endDate.setHours(startDate.getHours() + parseInt(rootConfig.leadMeetingTimeInHr));
	                        var title = translateMessages($translate, "lms.eventTitle") + $scope.LmsModel.Lead.BasicDetails.fullName;
	                        var location = "";
	                        var notes = translateMessages($translate, "lms.eventNote");
	                        var reminderTimeInMin = rootConfig.reminderTimeInMin;
	                        var success = function (message) {
	                        };
	                        var error = function (message) {
	                        };
	                        window.plugins.calendar.createEvent(title, location, notes, startDate, endDate, reminderTimeInMin, success, error);
	                    }
	                } 
	            }
	        } else {
	            $scope.disableSavedPolicyDetails();
	        }

		
			
			
	        	paintAppointmentMeetingHistory();
				paintCallMeetingHistory_init();
				paintVisitMeetingHistory_init();
			
		
			
	        $rootScope.showHideLoadingImage(false);
	        if (!$scope.editMode) {
	            $scope.userDetails = UserDetailsService.getUserDetailsModel();
	            if ($scope.userDetails.agentType == "BranchUser") {
	                $rootScope.lePopupCtrl.showError(
	                    "OmniChannel",
	                    translateMessages($translate,
	                        "leadCreationSuccMsg"),
	                    translateMessages($translate,
	                        "btnClose"),
	                    $scope.closeFunction,
	                    function () {
	                        $rootScope
	                            .showHideLoadingImage(false);
	                    });

	            }
	        } else if (!$scope.fromPopupTabsNav) {
	            //repaintMeetingHistory();
			
			
	            $scope.validateFields('callDetailsSection');
	            $rootScope.lePopupCtrl
	                .showSuccess(translateMessages(
	                        $translate, "successHeader"),
	                    translateMessages($translate,
	                        "leadSavedEditMode"),
	                    translateMessages($translate,
	                        "lms.close"));
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                    $scope.callback();
	                }, $scope, $compile);
	        }
	      //    $scope.UpdateRows();
	    };
		
		  $scope.onVisitSaveSuccess = function () {
			
			$scope.calldate = "";
			$scope.communicationStatusCodeCall="";
			$scope.communicationStatusCodeVisit="";
			$scope.planedMeetingDate="";
			$scope.actualMeetingDate="";
			$scope.actualMeetingTime="";
	        //Clear the actual meeting date and time to prevent the additional entry on multiple save click
	        if ($scope.LmsModel.Policies.productSold.submissionDate != "" && $scope.LmsModel.Policies.productSold.submissionDate != null &&
	            typeof $scope.LmsModel.Policies.productSold.submissionDate != "undefined") {
	            $scope.LmsModel.Policies.productSold.submissionDate = new Date($scope.LmsModel.Policies.productSold.submissionDate);
	        }
	        var actualMeetingDateTime = $scope.actualMeetingDate + " " + $scope.actualMeetingTime;
	        if (!(rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
	            var t = $scope.LmsModel.Communication[0].metDateAndTime.split(/[- :]/);
	            if ($scope.planedMeetingTime != "" && $scope.planedMeetingTime != null && typeof $scope.planedMeetingTime != "object") {
	                $scope.planedMeetingTime = new Date(t[0], t[1] - 1, t[2], t[3], t[4]);
	            }
	            if (typeof $scope.actualMeetingTime != "object" && $scope.actualMeetingTime != "" && $scope.actualMeetingTime != null) {
	                $scope.actualMeetingTime = new Date(actualMeetingDateTime);
	            }
	        } else {
	            $scope.planedMeetingTime = LEDate($scope.LmsModel.Communication[0].metDateAndTime);
	            if (typeof $scope.actualMeetingTime != "object" && $scope.actualMeetingTime != "" && $scope.actualMeetingTime != null) {
	                $scope.actualMeetingTime = LEDate(actualMeetingDateTime);
	            }
	        }
	        if ($scope.LmsModel.Lead.StatusDetails.disposition != "Successful Sale") {
	            var eventAlreadyExist = false;
	            if ((rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
	                if ($scope.LmsModel.Communication[0].metDateAndTime.trim() != "") {
	                    var metDateAndTime = LEDate($scope.LmsModel.Communication[0].metDateAndTime);
	                    for (var i = 0; i < $scope.LmsModel.Communication[0].followUps.length - 1; i++) {
	                        if (metDateAndTime - LEDate($scope.LmsModel.Communication[0].followUps[i].planedDateAndTime) == 0) {
	                            eventAlreadyExist = true;
	                            break;
	                        }
	                    }
	                    if (!eventAlreadyExist) {
	                        var startDate = angular.copy(metDateAndTime);
	                        var endDate = angular.copy(metDateAndTime);
	                        endDate.setHours(startDate.getHours() + parseInt(rootConfig.leadMeetingTimeInHr));
	                        var title = translateMessages($translate, "lms.eventTitle") + $scope.LmsModel.Lead.BasicDetails.fullName;
	                        var location = "";
	                        var notes = translateMessages($translate, "lms.eventNote");
	                        var reminderTimeInMin = rootConfig.reminderTimeInMin;
	                        var success = function (message) {
	                        };
	                        var error = function (message) {
	                        };
	                        window.plugins.calendar.createEvent(title, location, notes, startDate, endDate, reminderTimeInMin, success, error);
	                    }
	                } else if ($scope.LmsModel.Communication[0].followUps.length == 1 && $scope.LmsModel.Communication[0].followUps[0].planedDateAndTime != "") {
	                    var metDateAndTime = LEDate($scope.LmsModel.Communication[0].followUps[0].planedDateAndTime);
	                    var startDate = angular.copy(metDateAndTime);
	                    var endDate = angular.copy(metDateAndTime);
	                    endDate.setHours(startDate.getHours() + parseInt(rootConfig.leadMeetingTimeInHr));
	                    var title = translateMessages($translate, "lms.eventTitle") + $scope.LmsModel.Lead.BasicDetails.fullName;
	                    var location = "";
	                    var notes = translateMessages($translate, "lms.eventNote");
	                    var reminderTimeInMin = rootConfig.reminderTimeInMin;
	                    var success = function (message) {
	                    };
	                    var error = function (message) {
	                    };
	                    window.plugins.calendar.createEvent(title, location, notes, startDate, endDate, reminderTimeInMin, success, error);
	                }
	            }
	        } else {
	            $scope.disableSavedPolicyDetails();
	        }

		
			
			
	        	paintVisitMeetingHistory();
				paintAppointmentMeetingHistory_init();
			paintCallMeetingHistory_init();
			
		
			
	        $rootScope.showHideLoadingImage(false);
	        if (!$scope.editMode) {
	            $scope.userDetails = UserDetailsService.getUserDetailsModel();
	            if ($scope.userDetails.agentType == "BranchUser") {
	                $rootScope.lePopupCtrl.showError(
	                    "OmniChannel",
	                    translateMessages($translate,
	                        "leadCreationSuccMsg"),
	                    translateMessages($translate,
	                        "btnClose"),
	                    $scope.closeFunction,
	                    function () {
	                        $rootScope
	                            .showHideLoadingImage(false);
	                    });

	            }
	        } else if (!$scope.fromPopupTabsNav) {
	            //repaintMeetingHistory();
			
	        	
				
				
	 
				
	        	
	        
			
	            $scope.validateFields('callDetailsSection');
	            $rootScope.lePopupCtrl
	                .showSuccess(translateMessages(
	                        $translate, "successHeader"),
	                    translateMessages($translate,
	                        "leadSavedEditMode"),
	                    translateMessages($translate,
	                        "lms.close"));
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                    $scope.callback();
	                }, $scope, $compile);
	        }
	      //    $scope.UpdateRows();
	    };
		
		  $scope.onEditCallSuccess = function () {
			  $scope.calldate = "";
			$scope.communicationStatusCodeCall="";
			$scope.communicationStatusCodeVisit="";
			$scope.planedMeetingDate="";
			$scope.actualMeetingDate="";
			$scope.actualMeetingTime="";
	        //Clear the actual meeting date and time to prevent the additional entry on multiple save click
	        if ($scope.LmsModel.Policies.productSold.submissionDate != "" && $scope.LmsModel.Policies.productSold.submissionDate != null &&
	            typeof $scope.LmsModel.Policies.productSold.submissionDate != "undefined") {
	            $scope.LmsModel.Policies.productSold.submissionDate = new Date($scope.LmsModel.Policies.productSold.submissionDate);
	        }
	        var actualMeetingDateTime = $scope.actualMeetingDate + " " + $scope.actualMeetingTime;
	        if (!(rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
	            var t = $scope.LmsModel.Communication[0].metDateAndTime.split(/[- :]/);
	            if ($scope.planedMeetingTime != "" && $scope.planedMeetingTime != null && typeof $scope.planedMeetingTime != "object") {
	                $scope.planedMeetingTime = new Date(t[0], t[1] - 1, t[2], t[3], t[4]);
	            }
	            if (typeof $scope.actualMeetingTime != "object" && $scope.actualMeetingTime != "" && $scope.actualMeetingTime != null) {
	                $scope.actualMeetingTime = new Date(actualMeetingDateTime);
	            }
	        } else {
	            $scope.planedMeetingTime = LEDate($scope.LmsModel.Communication[0].metDateAndTime);
	            if (typeof $scope.actualMeetingTime != "object" && $scope.actualMeetingTime != "" && $scope.actualMeetingTime != null) {
	                $scope.actualMeetingTime = LEDate(actualMeetingDateTime);
	            }
	        }
	        if ($scope.LmsModel.Lead.StatusDetails.disposition != "Successful Sale") {
	            var eventAlreadyExist = false;
	            if ((rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
	                if ($scope.LmsModel.Communication[0].metDateAndTime.trim() != "") {
	                    var metDateAndTime = LEDate($scope.LmsModel.Communication[0].metDateAndTime);
	                    for (var i = 0; i < $scope.LmsModel.Communication[0].followUps.length - 1; i++) {
	                        if (metDateAndTime - LEDate($scope.LmsModel.Communication[0].followUps[i].planedDateAndTime) == 0) {
	                            eventAlreadyExist = true;
	                            break;
	                        }
	                    }
	                    if (!eventAlreadyExist) {
	                        var startDate = angular.copy(metDateAndTime);
	                        var endDate = angular.copy(metDateAndTime);
	                        endDate.setHours(startDate.getHours() + parseInt(rootConfig.leadMeetingTimeInHr));
	                        var title = translateMessages($translate, "lms.eventTitle") + $scope.LmsModel.Lead.BasicDetails.fullName;
	                        var location = "";
	                        var notes = translateMessages($translate, "lms.eventNote");
	                        var reminderTimeInMin = rootConfig.reminderTimeInMin;
	                        var success = function (message) {
	                        };
	                        var error = function (message) {
	                        };
	                        window.plugins.calendar.createEvent(title, location, notes, startDate, endDate, reminderTimeInMin, success, error);
	                    }
	                } else if ($scope.LmsModel.Communication[0].followUps.length == 1 && $scope.LmsModel.Communication[0].followUps[0].planedDateAndTime != "") {
	                    var metDateAndTime = LEDate($scope.LmsModel.Communication[0].followUps[0].planedDateAndTime);
	                    var startDate = angular.copy(metDateAndTime);
	                    var endDate = angular.copy(metDateAndTime);
	                    endDate.setHours(startDate.getHours() + parseInt(rootConfig.leadMeetingTimeInHr));
	                    var title = translateMessages($translate, "lms.eventTitle") + $scope.LmsModel.Lead.BasicDetails.fullName;
	                    var location = "";
	                    var notes = translateMessages($translate, "lms.eventNote");
	                    var reminderTimeInMin = rootConfig.reminderTimeInMin;
	                    var success = function (message) {
	                    };
	                    var error = function (message) {
	                    };
	                    window.plugins.calendar.createEvent(title, location, notes, startDate, endDate, reminderTimeInMin, success, error);
	                }
	            }
	        } else {
	            $scope.disableSavedPolicyDetails();
	        }
			for(i in $scope.LmsModel.Communication){

	       			
	       						if($scope.LmsModel.Communication[i].interactionType=="Call"){
	       								
	       								paintCallMeetingHistory_Edit(i);
	       						
	       						}
								else if($scope.LmsModel.Communication[i].interactionType=="Visit"){
	       								
	       								paintVisitMeetingHistory_Edit_init(i);
	       						
	       						}
								else if($scope.LmsModel.Communication[i].interactionType=="Appointment"){
	       								
	       								paintAppointmentMeetingHistory_Edit_init(i);
	       						
	       						}
	       						
	       			
	       	
					}
					
				
					
	        $rootScope.showHideLoadingImage(false);
	        if (!$scope.editMode) {
	            $scope.userDetails = UserDetailsService.getUserDetailsModel();
	            if ($scope.userDetails.agentType == "BranchUser") {
	                $rootScope.lePopupCtrl.showError(
	                    "OmniChannel",
	                    translateMessages($translate,
	                        "leadCreationSuccMsg"),
	                    translateMessages($translate,
	                        "btnClose"),
	                    $scope.closeFunction,
	                    function () {
	                        $rootScope
	                            .showHideLoadingImage(false);
	                    });

	            }
	        } else if (!$scope.fromPopupTabsNav) {
	            //repaintMeetingHistory();
	        	
	            $scope.validateFields('callDetailsSection');
	            $rootScope.lePopupCtrl
	                .showSuccess(translateMessages(
	                        $translate, "successHeader"),
	                    translateMessages($translate,
	                        "leadSavedEditMode"),
	                    translateMessages($translate,
	                        "lms.close"));
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                    $scope.callback();
	                }, $scope, $compile);
	        }
	      //    $scope.UpdateRows();
	    };
		
		  $scope.onEditVisitSuccess = function () {
			  $scope.calldate = "";
			$scope.communicationStatusCodeCall="";
			$scope.communicationStatusCodeVisit="";
			$scope.planedMeetingDate="";
			$scope.actualMeetingDate="";
			$scope.actualMeetingTime="";
	        //Clear the actual meeting date and time to prevent the additional entry on multiple save click
	        if ($scope.LmsModel.Policies.productSold.submissionDate != "" && $scope.LmsModel.Policies.productSold.submissionDate != null &&
	            typeof $scope.LmsModel.Policies.productSold.submissionDate != "undefined") {
	            $scope.LmsModel.Policies.productSold.submissionDate = new Date($scope.LmsModel.Policies.productSold.submissionDate);
	        }
	        var actualMeetingDateTime = $scope.actualMeetingDate + " " + $scope.actualMeetingTime;
	        if (!(rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
	            var t = $scope.LmsModel.Communication[0].metDateAndTime.split(/[- :]/);
	            if ($scope.planedMeetingTime != "" && $scope.planedMeetingTime != null && typeof $scope.planedMeetingTime != "object") {
	                $scope.planedMeetingTime = new Date(t[0], t[1] - 1, t[2], t[3], t[4]);
	            }
	            if (typeof $scope.actualMeetingTime != "object" && $scope.actualMeetingTime != "" && $scope.actualMeetingTime != null) {
	                $scope.actualMeetingTime = new Date(actualMeetingDateTime);
	            }
	        } else {
	            $scope.planedMeetingTime = LEDate($scope.LmsModel.Communication[0].metDateAndTime);
	            if (typeof $scope.actualMeetingTime != "object" && $scope.actualMeetingTime != "" && $scope.actualMeetingTime != null) {
	                $scope.actualMeetingTime = LEDate(actualMeetingDateTime);
	            }
	        }
	        if ($scope.LmsModel.Lead.StatusDetails.disposition != "Successful Sale") {
	            var eventAlreadyExist = false;
	            if ((rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
	                if ($scope.LmsModel.Communication[0].metDateAndTime.trim() != "") {
	                    var metDateAndTime = LEDate($scope.LmsModel.Communication[0].metDateAndTime);
	                    for (var i = 0; i < $scope.LmsModel.Communication[0].followUps.length - 1; i++) {
	                        if (metDateAndTime - LEDate($scope.LmsModel.Communication[0].followUps[i].planedDateAndTime) == 0) {
	                            eventAlreadyExist = true;
	                            break;
	                        }
	                    }
	                    if (!eventAlreadyExist) {
	                        var startDate = angular.copy(metDateAndTime);
	                        var endDate = angular.copy(metDateAndTime);
	                        endDate.setHours(startDate.getHours() + parseInt(rootConfig.leadMeetingTimeInHr));
	                        var title = translateMessages($translate, "lms.eventTitle") + $scope.LmsModel.Lead.BasicDetails.fullName;
	                        var location = "";
	                        var notes = translateMessages($translate, "lms.eventNote");
	                        var reminderTimeInMin = rootConfig.reminderTimeInMin;
	                        var success = function (message) {
	                        };
	                        var error = function (message) {
	                        };
	                        window.plugins.calendar.createEvent(title, location, notes, startDate, endDate, reminderTimeInMin, success, error);
	                    }
	                } else if ($scope.LmsModel.Communication[0].followUps.length == 1 && $scope.LmsModel.Communication[0].followUps[0].planedDateAndTime != "") {
	                    var metDateAndTime = LEDate($scope.LmsModel.Communication[0].followUps[0].planedDateAndTime);
	                    var startDate = angular.copy(metDateAndTime);
	                    var endDate = angular.copy(metDateAndTime);
	                    endDate.setHours(startDate.getHours() + parseInt(rootConfig.leadMeetingTimeInHr));
	                    var title = translateMessages($translate, "lms.eventTitle") + $scope.LmsModel.Lead.BasicDetails.fullName;
	                    var location = "";
	                    var notes = translateMessages($translate, "lms.eventNote");
	                    var reminderTimeInMin = rootConfig.reminderTimeInMin;
	                    var success = function (message) {
	                    };
	                    var error = function (message) {
	                    };
	                    window.plugins.calendar.createEvent(title, location, notes, startDate, endDate, reminderTimeInMin, success, error);
	                }
	            }
	        } else {
	            $scope.disableSavedPolicyDetails();
	        }
						for(i in $scope.LmsModel.Communication){

	       			
	       						if($scope.LmsModel.Communication[i].interactionType=="Visit"){
	       								
	       								paintVisitMeetingHistory_Edit(i);
	       						
	       						}
								else if($scope.LmsModel.Communication[i].interactionType=="Appointment"){
	       								
	       								paintAppointmentMeetingHistory_Edit_init(i);
	       						
	       						}
								else if($scope.LmsModel.Communication[i].interactionType=="Call"){
	       								
	       								paintCallMeetingHistory_Edit_init(i);
	       						
	       						}
	       						
	       			
	       	
					}
					
					
					
					
	        $rootScope.showHideLoadingImage(false);
	        if (!$scope.editMode) {
	            $scope.userDetails = UserDetailsService.getUserDetailsModel();
	            if ($scope.userDetails.agentType == "BranchUser") {
	                $rootScope.lePopupCtrl.showError(
	                    "OmniChannel",
	                    translateMessages($translate,
	                        "leadCreationSuccMsg"),
	                    translateMessages($translate,
	                        "btnClose"),
	                    $scope.closeFunction,
	                    function () {
	                        $rootScope
	                            .showHideLoadingImage(false);
	                    });

	            }
	        } else if (!$scope.fromPopupTabsNav) {
	            //repaintMeetingHistory();
	        	
	            $scope.validateFields('callDetailsSection');
	            $rootScope.lePopupCtrl
	                .showSuccess(translateMessages(
	                        $translate, "successHeader"),
	                    translateMessages($translate,
	                        "leadSavedEditMode"),
	                    translateMessages($translate,
	                        "lms.close"));
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                    $scope.callback();
	                }, $scope, $compile);
	        }
	      //    $scope.UpdateRows();
	    };
		
		
		  $scope.onEditAppointmentSuccess = function () {
			  $scope.calldate = "";
			$scope.communicationStatusCodeCall="";
			$scope.communicationStatusCodeVisit="";
			$scope.planedMeetingDate="";
			$scope.actualMeetingDate="";
			$scope.actualMeetingTime="";
	        //Clear the actual meeting date and time to prevent the additional entry on multiple save click
	        if ($scope.LmsModel.Policies.productSold.submissionDate != "" && $scope.LmsModel.Policies.productSold.submissionDate != null &&
	            typeof $scope.LmsModel.Policies.productSold.submissionDate != "undefined") {
	            $scope.LmsModel.Policies.productSold.submissionDate = new Date($scope.LmsModel.Policies.productSold.submissionDate);
	        }
	        var actualMeetingDateTime = $scope.actualMeetingDate + " " + $scope.actualMeetingTime;
	        if (!(rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
	            var t = $scope.LmsModel.Communication[0].metDateAndTime.split(/[- :]/);
	            if ($scope.planedMeetingTime != "" && $scope.planedMeetingTime != null && typeof $scope.planedMeetingTime != "object") {
	                $scope.planedMeetingTime = new Date(t[0], t[1] - 1, t[2], t[3], t[4]);
	            }
	            if (typeof $scope.actualMeetingTime != "object" && $scope.actualMeetingTime != "" && $scope.actualMeetingTime != null) {
	                $scope.actualMeetingTime = new Date(actualMeetingDateTime);
	            }
	        } else {
	            $scope.planedMeetingTime = LEDate($scope.LmsModel.Communication[0].metDateAndTime);
	            if (typeof $scope.actualMeetingTime != "object" && $scope.actualMeetingTime != "" && $scope.actualMeetingTime != null) {
	                $scope.actualMeetingTime = LEDate(actualMeetingDateTime);
	            }
	        }
	        if ($scope.LmsModel.Lead.StatusDetails.disposition != "Successful Sale") {
	            var eventAlreadyExist = false;
				var j;
	            if ((rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
						for(i in $scope.LmsModel.Communication){

	       			
	       						if($scope.LmsModel.Communication[i].interactionType=="Appointment"){
	       								
	       								j=i;
	       						
	       						}
								
	       						
	       			
	       	
					}
	               var metDate=$scope.LmsModel.Communication[j].metDate;
					var metTime=$scope.LmsModel.Communication[j].metTime;
					var metDateTime=metDate + " " + metTime;
	                if (metDateTime.trim() != "") {
	                    var metDateAndTime = LEDate(metDateTime);
	                    for (var i = 0; i < $scope.LmsModel.Communication[j].followUps.length; i++) {
							if(i!=$scope.LmsModel.Communication[j].followUps.length-1){
							var metDateFollowUp=$scope.LmsModel.Communication[j].followUps[i].metDate;
							var metTimeFollowUp=$scope.LmsModel.Communication[j].followUps[i].metTime;
							var metDateTimeFollowUp=metDateFollowUp + " " + metTimeFollowUp;
							}
	                        if (metDateAndTime - LEDate(metDateTimeFollowUp) == 0) {
	                            eventAlreadyExist = true;
	                            break;
	                        }
	                    }
	                    if (!eventAlreadyExist) {
	                        var startDate = angular.copy(metDateAndTime);
	                        var endDate = angular.copy(metDateAndTime);
	                        endDate.setHours(startDate.getHours() + parseInt(rootConfig.leadMeetingTimeInHr));
	                        var title = translateMessages($translate, "lms.eventTitle") + $scope.LmsModel.Lead.BasicDetails.fullName;
	                        var location = "";
	                        var notes = translateMessages($translate, "lms.eventNote");
	                        var reminderTimeInMin = rootConfig.reminderTimeInMin;
	                        var success = function (message) {
	                        };
	                        var error = function (message) {
	                        };
	                        window.plugins.calendar.createEvent(title, location, notes, startDate, endDate, reminderTimeInMin, success, error);
	                    }
	                } 
	            }
	        } else {
	            $scope.disableSavedPolicyDetails();
	        }
			for(i in $scope.LmsModel.Communication){

	       			
	       						if($scope.LmsModel.Communication[i].interactionType=="Appointment"){
	       								
	       								paintAppointmentMeetingHistory_Edit(i);
	       						
	       						}
								else if($scope.LmsModel.Communication[i].interactionType=="Visit"){
	       								
	       								paintVisitMeetingHistory_Edit_init(i);
	       						
	       						}
								else if($scope.LmsModel.Communication[i].interactionType=="Call"){
	       								
	       								paintCallMeetingHistory_Edit_init(i);
	       						
	       						}
	       						
	       			
	       	
					}
					
					
					
	        $rootScope.showHideLoadingImage(false);
	        if (!$scope.editMode) {
	            $scope.userDetails = UserDetailsService.getUserDetailsModel();
	            if ($scope.userDetails.agentType == "BranchUser") {
	                $rootScope.lePopupCtrl.showError(
	                    "OmniChannel",
	                    translateMessages($translate,
	                        "leadCreationSuccMsg"),
	                    translateMessages($translate,
	                        "btnClose"),
	                    $scope.closeFunction,
	                    function () {
	                        $rootScope
	                            .showHideLoadingImage(false);
	                    });

	            }
	        } else if (!$scope.fromPopupTabsNav) {
	            //repaintMeetingHistory();
	        	
	            $scope.validateFields('callDetailsSection');
	            $rootScope.lePopupCtrl
	                .showSuccess(translateMessages(
	                        $translate, "successHeader"),
	                    translateMessages($translate,
	                        "leadSavedEditMode"),
	                    translateMessages($translate,
	                        "lms.close"));
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                    $scope.callback();
	                }, $scope, $compile);
	        }
	      //    $scope.UpdateRows();
	    };
		  
	 
	    $scope.onSaveError = function (msg) {
	        // TODO
	        $rootScope.showHideLoadingImage(false);
	        $scope.refresh();
	        if (msg == "This Trasaction Data is already processed : " || msg == "Duplicate Lead : ") {
	            $rootScope.lePopupCtrl.showError(
	                translateMessages($translate,
	                    "errorHeader"),
	                translateMessages($translate,
	                    "leadAlreadyExists"),
	                translateMessages($translate,
	                    "lms.ok"));
	        } else {
	            $rootScope.lePopupCtrl.showError(
	                translateMessages($translate,
	                    "errorHeader"),
	                translateMessages($translate,
	                    "failure"),
	                translateMessages($translate,
	                    "lms.close"));
	        }
	    };

	    $scope.getNewFollowUp = function (actualDate, planedDate, type, status, remark) {
	        var newFollowUp = {
	            "actualDateAndTime": actualDate,
	            "planedDateAndTime": planedDate,
	            "interactionType": type,
	            "status": status,
	            "remarks": remark
	        };
	        return newFollowUp;
	    };
	    
	    $scope.getNewFollowUp_Call = function (callDate, statusCode, interactionType,countNum ) {
	    	if(statusCode=="NI"){
	    		$scope.resultStatus =translateMessages($translate, "lms.notInterestedType");
	    	}
	    	else if (statusCode=="IBDWMA"){
	    		$scope.resultStatus =translateMessages($translate, "lms.interestedbutnoAppointmentType");
	    	}
	    	else if (statusCode=="MA"){
	    		$scope.resultStatus =translateMessages($translate, "lms.makeAppointmentType");
	    	}
	    	else if (statusCode=="CR"){
	    		$scope.resultStatus =translateMessages($translate, "lms.cantReachType");
	    	}
			else if(statusCode=="Other"){
	    		$scope.resultStatus =translateMessages($translate, "lms.OtherType");
	    	}
	    	
	        var newFollowUp = {
	            "metDate": callDate,
	            "communicationStatusCode": statusCode,
	            "interactionType": interactionType,
	            "count" : countNum,
				"resultDisplay" : $scope.resultStatus
	        };
	        return newFollowUp;
	    };
	    
	    $scope.getNewFollowUp_Appointment = function (appointmentDate, appointmentTime, interactionType, countNum) {
	        var newFollowUp = {
	            "metDate": appointmentDate,
	            "metTime": appointmentTime,
	            "interactionType": interactionType,
	            "count": countNum
	           
	        };
	        return newFollowUp;
	    };
	    
	    $scope.getNewFollowUp_Visit = function (visitDate, statusCode, interactionType,countNum ) {
	    	if(statusCode=="MWC"){
	    		$scope.resultStatus =translateMessages($translate, "lms.metCustomerType");
	    	}
	    	else if (statusCode=="MC"){
	    		$scope.resultStatus =translateMessages($translate, "lms.meetingCancelledType");
	    	}
	    	else if (statusCode=="MR"){
	    		$scope.resultStatus =translateMessages($translate, "lms.meetingRescheduledType");
	    	}
	    	else if(statusCode=="Other"){
	    		$scope.resultStatus =translateMessages($translate, "lms.OtherType");
	    	}
	    	
	        var newFollowUp = {
	            "metDate": visitDate,
				 "communicationStatusCode": statusCode,
	            "interactionType": interactionType,
	            "count": countNum,
				"resultDisplay" : 	$scope.resultStatus
	        };
	        return newFollowUp;
	    };



	    $scope.formatTime = function (time) {
	        var timeArray = time.split(":");
	        var formatedTime = "";
	        for (var i = 0; i < timeArray.length; i++) {
	            if (timeArray[i].length == 1) {
	                timeArray[i] = "0" + timeArray[i];
	            }
	            if (i != timeArray.length - 1) {
	                formatedTime = formatedTime + timeArray[i] + ":";
	            } else {
	                formatedTime = formatedTime + timeArray[i];
	            }
	        }
	        return formatedTime;
	    };

		$scope.updateContactLogDate = function(){
			$scope.validateFields('callDetailsSection','callDetailsSection');
			$scope.refresh();
		}
	    //Validate all fields and manage dynamic error count
	    $scope.validateFields = function (arg1, arg2) {
			var callString = ""+ $scope.calldate;
			if($scope.calldate){
				if(callString.indexOf("/")>0){
			var calldateConvert=ExternalToInternal($scope.calldate);
			$scope.calldate=calldateConvert;
			}
			}
			
			if($scope.actualMeetingDate){
				var appointmentString = ""+ $scope.actualMeetingDate;
			if(appointmentString.indexOf("/")>0){	
			var actualMeetingDateConvert=ExternalToInternal($scope.actualMeetingDate);
			$scope.actualMeetingDate=actualMeetingDateConvert;
			}
			}
			
			if($scope.planedMeetingDate){	
			var plannedString = ""+ $scope.planedMeetingDate;
			if(plannedString.indexOf("/")>0){			
			var planedMeetingDateConvert=ExternalToInternal($scope.planedMeetingDate);
			$scope.planedMeetingDate=planedMeetingDateConvert;
			}
			}
		
	        var _errorCount = 0;
	        var _errors = [];
	        if ($scope.LmsModel.Policies.productSold.submissionDate != "") {
	            LmsVariables.submissionDate = $scope.LmsModel.Policies.productSold.submissionDate;
	        }

	        var actualMeetingDate = "";
	        var actualMeetingTime = "";
	        if ($scope.actualMeetingDate && $scope.actualMeetingTime) {
	            LmsVariables.actualMeetingDate = $scope.actualMeetingDate;

	            if (typeof $scope.actualMeetingTime == "object") {
	                actualMeetingTime = formatForTimeControl($scope.actualMeetingTime, false);
	            } else {
	                actualMeetingTime = $scope.formatTime($scope.actualMeetingTime);
	            }

	            actualMeetingDate = formatForDateControl($scope.actualMeetingDate);
	        }
		  
			if(arg2=='Appointment'){
	        //---- On edit mode if any of field date or time is present both most have---
	        if (($scope.actualMeetingDate && !$scope.actualMeetingTime)) {
	            LmsVariables.actualMeetingDate = $scope.actualMeetingDate;
	            LmsVariables.actualMeetingTime = "";
	            _errorCount++;
	            var error = {};
	            error.message = translateMessages($translate, "lms.callDetailsSectionmetTimeRequiredValidationMessage");
	            error.key = 'meetingTime';
	            _errors.push(error);
	        }
	        
	        if (($scope.actualMeetingTime && !$scope.actualMeetingDate)) {
	            LmsVariables.actualMeetingDate = $scope.actualMeetingDate;
	            LmsVariables.actualMeetingTime = "";
	            _errorCount++;
	            var error = {};
	            error.message = translateMessages($translate, "lms.callDetailsSectionmeetingDateRequiredValidationMessage");
	            error.key = 'appointmentdate';
	            _errors.push(error);
	        }
	      
	        if ((!$scope.actualMeetingTime && !$scope.actualMeetingDate)) {
	            LmsVariables.actualMeetingDate = $scope.actualMeetingDate;
	            LmsVariables.actualMeetingTime = "";
	            _errorCount++;
	            var error = {};
	            error.message = translateMessages($translate, "lms.contactLogsectionMandatoryValidationMessage");
	            error.key = 'appointmentdate';
	          
	            _errors.push(error);
	        }
		
	       
	        }
			  
	        if(arg2=='Call'){
	      
	        if (($scope.communicationStatusCodeCall && !$scope.calldate)) {
		           // LmsVariables.actualMeetingDate = $scope.calldate;
		           // LmsVariables.actualMeetingTime = "";
		            _errorCount++;
		            var error = {};
		            error.message = translateMessages($translate, "lms.callDetailsSectionCallDateRequiredValidationMessage");
		            error.key = 'meetingDate';
		            _errors.push(error);
		        }
	        if (($scope.calldate && !$scope.communicationStatusCodeCall)) {
	           // LmsVariables.actualMeetingDate = $scope.calldate;
	           // LmsVariables.actualMeetingTime = "";
	            _errorCount++;
	            var error = {};
	            error.message = translateMessages($translate, "lms.callDetailsSectionCallResultStatusValidationMessage");
	            error.key = 'callstatus';
	            _errors.push(error);
	        }

	        if((!$scope.calldate && !$scope.communicationStatusCodeCall)) {
	            LmsVariables.actualMeetingDate = $scope.actualMeetingDate;
	            LmsVariables.actualMeetingTime = "";
	            _errorCount++;
	            var error = {};
	            error.message = translateMessages($translate, "lms.contactLogsectionMandatoryValidationMessage");
	            error.key = 'callstatus';
	          
	            _errors.push(error);
	        }
			}

			
	        
				if(arg2=='Visit'){
	        if (($scope.communicationStatusCodeVisit && !$scope.planedMeetingDate)) {
		           // LmsVariables.actualMeetingDate = $scope.calldate;
		           // LmsVariables.actualMeetingTime = "";
		            _errorCount++;
		            var error = {};
		            error.message = translateMessages($translate, "lms.callDetailsSectionVisitDateRequiredValidationMessage");
		            error.key = 'nextMeetingDate';
		            _errors.push(error);
		        }
	        
	        if (($scope.planedMeetingDate && !$scope.communicationStatusCodeVisit)) {
		           // LmsVariables.actualMeetingDate = $scope.calldate;
		           // LmsVariables.actualMeetingTime = "";
		            _errorCount++;
		            var error = {};
		            error.message = translateMessages($translate, "lms.callDetailsSectionVisitResultStatusValidationMessage");
		            error.key = 'visitstatus';
		            _errors.push(error);
		        }
	        
	        if ((!$scope.planedMeetingDate && !$scope.communicationStatusCodeVisit)) {
		           // LmsVariables.actualMeetingDate = $scope.calldate;
		           // LmsVariables.actualMeetingTime = "";
		            _errorCount++;
		            var error = {};
		            error.message = translateMessages($translate, "lms.contactLogsectionMandatoryValidationMessage");
					
		            error.key = 'visitstatus';
		            _errors.push(error);
		        }
				}
				
	      //  if ((!$scope.actualMeetingDate && $scope.actualMeetingTime)) {
	        //    LmsVariables.actualMeetingTime = "1900-01-01 " + actualMeetingTime;
	        //    LmsVariables.actualMeetingDate = "";
	       // }
	        //---- validate expected business
//	        var planedMeetingTime = "";
//	        var planedMeetingDate = "";
//	        if ($scope.planedMeetingDate && $scope.planedMeetingTime) {
//	            LmsVariables.plannedMeetingDate = $scope.planedMeetingDate;
//	            if (typeof $scope.planedMeetingTime == "object") {
//	                planedMeetingTime = formatForTimeControl($scope.planedMeetingTime, false);
//	            } else {
//	                planedMeetingTime = $scope.formatTime($scope.planedMeetingTime);
//	            }
//
//	            planedMeetingDate = formatForDateControl($scope.planedMeetingDate);
//	        }
//	        if (planedMeetingDate && planedMeetingTime && planedMeetingDate.trim() != "" && planedMeetingTime.trim() != "" && $scope.LmsModel.Lead.StatusDetails.disposition != "Successful Sale") {
//	            LmsVariables.plannedMeetingTime = planedMeetingDate + " " + planedMeetingTime;
//	            var dateWithSec = planedMeetingDate + " " + planedMeetingTime + ":00";
//	            var arr = dateWithSec.split(/[- :]/),
//	                dateFormat = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
//	            var _planedDate = Date.parse(dateFormat);
//	            var _after90Days = new Date();
//	            _after90Days.setDate(_after90Days.getDate() + 90);
//	            _after90Days = Date.parse(_after90Days);
//
//	            if (_after90Days < _planedDate) {
//	                _errorCount++;
//	                var error = {};
//	                error.message = translateMessages($translate, "lms.callDetailsSectionmeetingNextMeetingDateErrorValidationMessage");
//	                // meetingDateLesserThanFollowup
//	                error.key = 'nextMeetingDate';
//	                _errors.push(error);
//	            } else if (_planedDate < Date.parse(new Date())) {
//	                _errorCount++;
//	                var error = {};
//	                error.message = translateMessages($translate, "lms.callDetailsSectionmeetingNextMeetingDateErrorValidationMessage");
//	                // meetingDateLesserThanFollowup
//	                error.key = 'nextMeetingDate';
//	                _errors.push(error);
//	            }
//	        }
	if(arg2=='Visit'){
	        var planedMeetingDate = "";
	        if ($scope.planedMeetingDate) {
	            planedMeetingDate = formatForDateControl($scope.planedMeetingDate);
	        }
	        if (planedMeetingDate && planedMeetingDate.trim() != "") {	            
	            dateFormat = new Date(planedMeetingDate);
	            var _actualDate = Date.parse(dateFormat);
	            var _currentDate = Date.parse(new Date());
	            if (_actualDate > _currentDate) {
	                _errorCount++;
	                var error = {};
	                error.message = translateMessages($translate, "lms.callDetailsSectionmeetingDateRequiredValidationMessagepast");
	                // meetingDateLesserThanFollowup
	                error.key = 'nextMeetingDate';
	                _errors.push(error);
	        }
            }
	}
	
            
            
            
            
            //---- validate expected business
	       
	     //   var calldate = "";
	     //   if ($scope.calldate) {
	      //      calldate = formatForDateControl($scope.calldate);
		//		$scope.calldate=calldate;
	     //   }
			if(arg2=='Call'){
	        if ($scope.calldate && $scope.calldate.trim() != "") {	            
	            dateFormat = new Date($scope.calldate);
	            var _actualDate = Date.parse(dateFormat);
	            var _currentDate = Date.parse(new Date());
	            if (_actualDate > _currentDate) {
	                _errorCount++;
	                var error = {};
	                error.message = translateMessages($translate, "lms.callDetailsSectionmeetingDateRequiredValidationMessagecall");
	                // meetingDateLesserThanFollowup
	                error.key = 'calldate';
	                _errors.push(error);
	        }
            }
			}
			
				if(arg2=='Appointment'){
				  if (actualMeetingDate && actualMeetingTime && actualMeetingDate.trim() != "" && actualMeetingTime.trim() != "") {
	            LmsVariables.actualMeetingTime = actualMeetingDate + " " + actualMeetingTime;
	            var dateWithSec = actualMeetingDate + " " + actualMeetingTime + ":00";
	            var arr = dateWithSec.split(/[- :]/),
	                dateFormat = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
	            var actualDtParse = Date.parse(dateFormat);
	            var _actualDate = actualDtParse;

	            var _currentDate = Date.parse(new Date());
	         
	            	if (_actualDate < _currentDate) {
	                _errorCount++;
	                var error = {};
	                error.message = translateMessages($translate, "lms.callDetailsSectionmeetingDateRequiredValidationMessageCurtApp");
	                // meetingDateLesserThanFollowup
	                error.key = 'appointmentdate';
	                _errors.push(error);

	            }
	            	
	           
	        }
				}
	        //---- On edit mode if any of field date or time is present both most have---
//	        if ($scope.planedMeetingDate && !$scope.planedMeetingTime && $scope.LmsModel.Lead.StatusDetails.disposition != "Successful Sale") {
//	            LmsVariables.plannedMeetingDate = $scope.planedMeetingDate;
//	            LmsVariables.plannedMeetingTime = "";
//
//	            _errorCount++;
//	            var error = {};
//	            error.message = translateMessages($translate, "lms.callDetailsSectionsubfollowUpTimeRequiredValidationMessage");
//	            // meetingDateLesserThanFollowup
//	            error.key = 'nextMeetingTime';
//	            _errors.push(error);
//	        }
//	       if (!$scope.planedMeetingDate && $scope.planedMeetingTime) {
//	            LmsVariables.plannedMeetingTime = "1900-01-01 " + planedMeetingTime;
//	            LmsVariables.plannedMeetingDate = "";
//	        }
	        //validatin policy details
	        if ($scope.LmsModel.Lead.StatusDetails.disposition == "Successful Sale") {
	            for (var i = 0; i < $scope.LmsModel.Policies.productSold.length; i++) {
	                var isEmpty = false;
	                if (!$scope.LmsModel.Policies.productSold[i].proposalNumber || $scope.LmsModel.Policies.productSold[i].proposalNumber == "") {
	                    _errorCount++;
	                    var error = {};
	                    error.message = translateMessages($translate, "lms.callDetailsSectionProposalNoRequiredValidationMessage");
	                    // meetingDateLesserThanFollowup
	                    error.key = 'policyNumber' + i;
	                    _errors.push(error);
	                    isEmpty = true;
	                }
	                if (!$scope.LmsModel.Policies.productSold[i].premium || $scope.LmsModel.Policies.productSold[i].premium == "") {
	                    _errorCount++;
	                    var error = {};
	                    error.message = translateMessages($translate, "lms.callDetailsSectionPremiumRequiredValidationMessage");
	                    // meetingDateLesserThanFollowup
	                    error.key = 'premium' + i;
	                    _errors.push(error);
	                    isEmpty = true;
	                }
	                if (isEmpty == true)
	                    break;
	            }
	        }

	        $scope.dynamicErrorCount = _errorCount;
	        if (!$scope.dynamicErrorMessages) {
	            $scope.dynamicErrorMessages = [];
	        }
	        $scope.dynamicErrorMessages = _errors;
	        $rootScope.updateErrorCount(arg1);

	    };
	    $scope.getsplitedDateOrTime = function (dateAndTime, type) {
	        if (dateAndTime && dateAndTime.indexOf(" ") > 0) {
	            var tokens = dateAndTime.split(" ");
	            if (type == "time") {
	                return tokens[1];
	            } else {
	                return tokens[0];
	            }

	        }
	        return " ";

	    };
	    $scope.getFormattedDate = function () {
	        now = new Date();
	        year = "" + now.getFullYear();
	        month = "" + (now.getMonth() + 1);
	        if (month.length == 1) {
	            month = "0" + month;
	        }
	        day = "" + now.getDate();
	        if (day.length == 1) {
	            day = "0" + day;
	        }
	        hour = "" + now.getHours();
	        if (hour.length == 1) {
	            hour = "0" + hour;
	        }
	        minute = "" + now.getMinutes();
	        if (minute.length == 1) {
	            minute = "0" + minute;
	        }
	        second = "" + now.getSeconds();
	        if (second.length == 1) {
	            second = "0" + second;
	        }
	        return month + "/" + year + "/" + day + " " + hour + ":" + minute;
	    };

	    $scope.IsNumeric = function (input) {
	        return (input - 0) == input && ('' + input).trim().length > 0;
	    };


	    $scope.popupTabsNav = function (rel, index) {
	         $scope.validateFields('callDetailsSection');
	        if (index != 1) {

	            $rootScope.updateErrorCount("callDetailsSection");
	             if (!$scope.errorCount > 0 || index == 0) {

	                if (!$scope.errorCount > 0) {
	                    $scope.fromPopupTabsNav = true;
	                   saveContactLog();
	                } else {
	                     LmsVariables.setLmsModel($scope.LmsModel);
	                }

	               if (!$scope.popupFlag) {
	                    $scope.popupFlag = true;
	                     $("#popupContent").empty();
	                    var url = "";

	                   for (nav in $scope.lmsnavigations) {
	                        if ($scope.lmsnavigations[nav].name == rel) {
	                             url = $scope.lmsnavigations[nav].url;
	                             break;
	                         }
	                    }
	                   $location.path(url);
	                } else {
	                    $scope.popupFlag = false;
	                 }
	                $scope.refresh();
	            } else {
	                 $rootScope.showHideLoadingImage(false);
	                // $scope.lePopupCtrl.showWarning(
	                    // translateMessages($translate,
	                        // "errorHeader"),
	                    // translateMessages($translate,
	                        // "enterMandatory"),
	                    // translateMessages($translate,
	                        // "lms.ok"));
					$scope.leadDetShowPopUpMsg = true;
					$scope.enterMandatory = translateMessages($translate, "enterMandatory");
	             }
	        }
	    };

	    function saveContactLog () {
	        $rootScope.showHideLoadingImage(true,
	            'paintUIMessage', $translate);
	        $scope.validateFields('callDetailsSection');
	        $rootScope.updateErrorCount("callDetailsSection");
	        if ($scope.errorCount == 0) {
	            $scope.GLI_errorCheck();
	            LmsVariables.setLmsModel($scope.LmsModel);
	        } else {
	            $rootScope.showHideLoadingImage(false);
	            // $scope.lePopupCtrl.showWarning(
	                // translateMessages($translate,
	                    // "errorHeader"),
	                // translateMessages($translate,
	                    // "enterMandatory"),
	                // translateMessages($translate,
	                    // "lms.ok"));
				$scope.leadDetShowPopUpMsg = true;
				$scope.enterMandatory = translateMessages($translate, "enterMandatory");
	        }

	        $rootScope.showHideLoadingImage(false);
	    };
	    $scope.callback = function () {
	        if (LmsVariables.editMode == false) {
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "popupTabsStartbar", "#popupTabs", true,
	                function () {
	                   detailedCallback();
	                }, $scope, $compile);

	        } else {
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "popupTabsNavbar", "#popupTabs", true,
	                function () {
	                    detailedCallback();
	                }, $scope, $compile);
	            $timeout(function () {
	                LEDynamicUI.paintUI(rootConfig.template,
	                    "leadListingWithTabs.json",
	                    "meetingHistorySection", "#popupMeetingHistory", true,
	                    function () {
	                        detailedCallback();
	                    }, $scope, $compile);
	            }, 50);

	        }
	    };

	    function detailedCallback() {
	        $scope.populateResult();
	         if ($scope.editMode) {
	                 $scope.intractionType = $scope.LmsModel.Communication[1].interactionType;
	                 $scope.refresh();
	         }
	         $rootScope.updateErrorCount('callDetailsSection');
	         $rootScope.showHideLoadingImage(false);
	 };
	         
	 $scope.populateResult=function(){
	             $scope.populateIndependentLookupDateInScope('CALLSTATUS', 'callstatus', 'LmsModel.Communication[1].callstatus', false, 'callstatus', 'callDetailsSection');
	             $scope.populateIndependentLookupDateInScope('VISITSTATUS', 'visitstatus', 'LmsModel.Communication[3].visitstatus', false, 'visitstatus', 'callDetailsSection');  
	 };

	    LEDynamicUI.getUIJson(rootConfig.lmsConfigJson, false, function (lmsConfig) {
	        $scope.lmsnavigations = lmsConfig.lmsnavigations;
	        $scope.defaultFieldValues = lmsConfig.DefaultFieldValues[0];
	        $scope.subheading = $scope.defaultFieldValues.subheading;
	        $scope.defaultDisposition = $scope.defaultFieldValues.status;
	        $scope.defaultIntractionType = $scope.defaultFieldValues.intractionType;
	        $scope.GLI_init();

	    });
	    //back tab        
        $scope.List = function () {
        $scope.lePopupCtrl.showWarning(
						translateMessages($translate,
								"lifeEngage"),
						translateMessages($translate,
								"pageNavigationText"),  
						translateMessages($translate,
								"fna.navok"),$scope.okPressed,translateMessages($translate,
								"general.navproceed"),$scope.backToListing);        
        };
    
        $scope.backToListing=function(){
            $rootScope.showHideLoadingImage(true,
                'paintUIMessage', $translate);
            GLI_LmsService.navigateToListingPage("/lms");
        }

	    function repaintMeetingHistory () {
	        $scope.tempFollowUps = [];
	        if ($scope.LmsModel.Communication[0].followUps && $scope.LmsModel.Communication[0].followUps.length > 0) {
	            for (i in $scope.LmsModel.Communication[0].followUps) {
	                $scope.tempFollowUps.push($scope.LmsModel.Communication[0].followUps[i]);
	            }
	            $scope.tempFollowUps = $filter('orderBy')($scope.tempFollowUps, 'actualDateAndTime', false);
	        }
	        $("#popupMeetingHistory").empty();
	        $timeout(function () {
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "meetingHistorySection", "#popupMeetingHistory", true,
	                function () {
	                   detailedCallback();
	                }, $scope, $compile);
	        }, 50);
	    };
	     function paintCallMeetingHistory_Edit (arg) {
	        $scope.tempFollowUps = [];
	        if ($scope.LmsModel.Communication[arg].followUps && $scope.LmsModel.Communication[arg].followUps.length > 0) {
	            for (i in $scope.LmsModel.Communication[arg].followUps) {
					if($scope.LmsModel.Communication[arg].followUps[i].communicationStatusCode=="NI"){
						
	    		$scope.LmsModel.Communication[arg].followUps[i].resultDisplay =translateMessages($translate, "lms.notInterestedType");
	    	}
	    	else if($scope.LmsModel.Communication[arg].followUps[i].communicationStatusCode=="IBDWMA"){
	    	$scope.LmsModel.Communication[arg].followUps[i].resultDisplay =translateMessages($translate, "lms.interestedbutnoAppointmentType");
	    	}
	    	else if($scope.LmsModel.Communication[arg].followUps[i].communicationStatusCode=="MA"){
	    		$scope.LmsModel.Communication[arg].followUps[i].resultDisplay =translateMessages($translate, "lms.makeAppointmentType");
	    	}
	    	else if($scope.LmsModel.Communication[arg].followUps[i].communicationStatusCode=="CR"){
	    			$scope.LmsModel.Communication[arg].followUps[i].resultDisplay  =translateMessages($translate, "lms.cantReachType");
	    	}else{
	    		$scope.LmsModel.Communication[arg].followUps[i].resultDisplay =translateMessages($translate, "lms.OtherType");
	    	}
					var followupDate=""+$scope.LmsModel.Communication[arg].followUps[i].metDate;
					var dateArr =followupDate.split('-');
					var metYear=dateArr[0];
					var currDate=new Date();
					var currYear=currDate.getFullYear()+543;
					
						if((i==$scope.LmsModel.Communication[arg].followUps.length-1) &&!$scope.LmsModel.Communication[arg].followUps[i].metDate.indexOf("/")>0){
					$scope.LmsModel.Communication[arg].followUps[i].metDate = InternalToExternalInString($scope.LmsModel.Communication[arg].followUps[i].metDate);
					var date=""+$scope.LmsModel.Communication[arg].followUps[i].metDate;
					var dateArr=date.split('-');
					$scope.LmsModel.Communication[arg].followUps[i].metDate=dateArr[2] + '/' +dateArr[1]+ '/'+dateArr[0];
					 $scope.tempFollowUps.push($scope.LmsModel.Communication[arg].followUps[i]);
					//$scope.dateFlagConvCall=true;
				
						}
						
						else if (metYear<=currDate.getFullYear()){	
							var metYearinNum=Number(metYear);
							var thaiYear=metYearinNum+543;
							dateArr[0]=thaiYear;
							$scope.LmsModel.Communication[arg].followUps[i].metDate=dateArr[2] + '/' + dateArr[1] + '/' +dateArr[0];
							 $scope.tempFollowUps.push($scope.LmsModel.Communication[arg].followUps[i]);
							}	
							
							else if(metYear<=currYear){
								   $scope.tempFollowUps.push($scope.LmsModel.Communication[arg].followUps[i]);
							}
				}
	            $scope.tempFollowUps = $filter('orderBy')($scope.tempFollowUps, 'actualDateAndTime', false);
	        }
	        $("#popupContent").empty();
	        $timeout(function () {
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                   detailedCallback();
	                }, $scope, $compile);
	        }, 50);
	    };
		
		
	    function paintCallMeetingHistory_Edit_init (arg) {
	        $scope.tempFollowUps = [];
	        if ($scope.LmsModel.Communication[arg].followUps && $scope.LmsModel.Communication[arg].followUps.length > 0) {
	            for (i in $scope.LmsModel.Communication[arg].followUps) {
					if($scope.LmsModel.Communication[arg].followUps[i].communicationStatusCode=="NI"){
						
	    		$scope.LmsModel.Communication[arg].followUps[i].resultDisplay =translateMessages($translate, "lms.notInterestedType");
	    	}
	    	else if($scope.LmsModel.Communication[arg].followUps[i].communicationStatusCode=="IBDWMA"){
	    	$scope.LmsModel.Communication[arg].followUps[i].resultDisplay =translateMessages($translate, "lms.interestedbutnoAppointmentType");
	    	}
	    	else if($scope.LmsModel.Communication[arg].followUps[i].communicationStatusCode=="MA"){
	    		$scope.LmsModel.Communication[arg].followUps[i].resultDisplay =translateMessages($translate, "lms.makeAppointmentType");
	    	}
	    	else if($scope.LmsModel.Communication[arg].followUps[i].communicationStatusCode=="CR"){
	    			$scope.LmsModel.Communication[arg].followUps[i].resultDisplay  =translateMessages($translate, "lms.cantReachType");
	    	}else{
	    		$scope.LmsModel.Communication[arg].followUps[i].resultDisplay =translateMessages($translate, "lms.OtherType");
	    	}
						
					var followupDate=""+$scope.LmsModel.Communication[arg].followUps[i].metDate;
					var dateArr=followupDate.split('-');
					var metYear=dateArr[0];
					var currDate=new Date();
					var currYear=currDate.getFullYear() +543;
					
					if(metYear<=currDate.getFullYear()){
						
						var metYearinNum=Number(metYear);
						var thaiYear=metYearinNum+543;
						dateArr[0]=thaiYear;
						$scope.LmsModel.Communication[arg].followUps[i].metDate=dateArr[2] + '/' + dateArr[1] + '/' +dateArr[0] ;
						 $scope.tempFollowUps.push($scope.LmsModel.Communication[arg].followUps[i]);
						 
					}
					else if(metYear<=currYear){
						   $scope.tempFollowUps.push($scope.LmsModel.Communication[arg].followUps[i]);
					}
					
							
				}
	            $scope.tempFollowUps = $filter('orderBy')($scope.tempFollowUps, 'actualDateAndTime', false);
	        }
	        $("#popupContent").empty();
	        $timeout(function () {
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                   detailedCallback();
	                }, $scope, $compile);
	        }, 50);
	    };
	    
	    
	    function paintAppointmentMeetingHistory_Edit (arg) {
	        $scope.tempFollowUpsApp = [];
	        if ($scope.LmsModel.Communication[arg].followUps && $scope.LmsModel.Communication[arg].followUps.length > 0) {
	            for (i in $scope.LmsModel.Communication[arg].followUps) {
					
					var followupDate=""+$scope.LmsModel.Communication[arg].followUps[i].metDate;
					var dateArr =followupDate.split('-');
					var metYear=dateArr[0];
					var currDate=new Date();
					var currYear=currDate.getFullYear()+543;
					
						if((i==$scope.LmsModel.Communication[arg].followUps.length-1) &&!$scope.LmsModel.Communication[arg].followUps[i].metDate.indexOf("/")>0){
					$scope.LmsModel.Communication[arg].followUps[i].metDate = InternalToExternalInString($scope.LmsModel.Communication[arg].followUps[i].metDate);
					var date=""+$scope.LmsModel.Communication[arg].followUps[i].metDate;
					var dateArr=date.split('-');
					$scope.LmsModel.Communication[arg].followUps[i].metDate=dateArr[2] + '/' +dateArr[1]+ '/'+dateArr[0];
					 $scope.tempFollowUpsApp.push($scope.LmsModel.Communication[arg].followUps[i]);
					//$scope.dateFlagConvCall=true;
				
						}
						
						else if (metYear<currYear){	
							var metYearinNum=Number(metYear);
							var thaiYear=metYearinNum+543;
							dateArr[0]=thaiYear;
							$scope.LmsModel.Communication[arg].followUps[i].metDate=dateArr[2] + '/' + dateArr[1] + '/' +dateArr[0];
							 $scope.tempFollowUpsApp.push($scope.LmsModel.Communication[arg].followUps[i]);
							}	
							
							else if(metYear>=currYear){
								   $scope.tempFollowUpsApp.push($scope.LmsModel.Communication[arg].followUps[i]);
							}
	            }
	            $scope.tempFollowUpsApp = $filter('orderBy')($scope.tempFollowUpsApp, 'actualDateAndTime', false);
	        }
	        $("#popupContent").empty();
	        $timeout(function () {
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                   detailedCallback();
	                }, $scope, $compile);
	        }, 50);
	    };
		
		  function paintAppointmentMeetingHistory_Edit_init (arg) {
	        $scope.tempFollowUpsApp = [];
	        if ($scope.LmsModel.Communication[arg].followUps && $scope.LmsModel.Communication[arg].followUps.length > 0) {
	            for (i in $scope.LmsModel.Communication[arg].followUps) {
					
	            	var followupDate=""+$scope.LmsModel.Communication[arg].followUps[i].metDate;
					var dateArr = followupDate.split('-');
					var metYear=dateArr[0];
					var currDate=new Date();
					var currYear=currDate.getFullYear()+543;
					if(metYear>=currYear){
						   $scope.tempFollowUpsApp.push($scope.LmsModel.Communication[arg].followUps[i]);
					}
					else{
						var metYearinNum=Number(metYear);
						var thaiYear=metYearinNum+543;
						dateArr[0]=thaiYear;
						$scope.LmsModel.Communication[arg].followUps[i].metDate=dateArr[2] + '/' + dateArr[1] + '/' +dateArr[0];
						 $scope.tempFollowUpsApp.push($scope.LmsModel.Communication[arg].followUps[i]);
					}
					
					
					
	            
						
	               
	            }
	            $scope.tempFollowUpsApp = $filter('orderBy')($scope.tempFollowUpsApp, 'actualDateAndTime', false);
	        }
	        $("#popupContent").empty();
	        $timeout(function () {
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                   detailedCallback();
	                }, $scope, $compile);
	        }, 50);
	    };
	    
	    function paintVisitMeetingHistory_Edit (arg) {
	        $scope.tempFollowUpsVisit = [];
	        if ($scope.LmsModel.Communication[arg].followUps && $scope.LmsModel.Communication[arg].followUps.length > 0) {
	            for (i in $scope.LmsModel.Communication[arg].followUps) {
					if($scope.LmsModel.Communication[arg].followUps[i].communicationStatusCode=="MWC"){
						
	    		$scope.LmsModel.Communication[arg].followUps[i].resultDisplay =translateMessages($translate, "lms.metCustomerType");
	    	}
	    	else if($scope.LmsModel.Communication[arg].followUps[i].communicationStatusCode=="MC"){
	    	$scope.LmsModel.Communication[arg].followUps[i].resultDisplay =translateMessages($translate, "lms.meetingCancelledType");
	    	}
	    	else if($scope.LmsModel.Communication[arg].followUps[i].communicationStatusCode=="MR"){
	    		$scope.LmsModel.Communication[arg].followUps[i].resultDisplay =translateMessages($translate, "lms.meetingRescheduledType");
	    	}
	    	else{
	    		$scope.LmsModel.Communication[arg].followUps[i].resultDisplay =translateMessages($translate, "lms.OtherType");
	    	}
						

					var followupDate=""+$scope.LmsModel.Communication[arg].followUps[i].metDate;
					var dateArr =followupDate.split('-');
					var metYear=dateArr[0];
					var currDate=new Date();
					var currYear=currDate.getFullYear()+543;
				
						if((i==$scope.LmsModel.Communication[arg].followUps.length-1) &&!$scope.LmsModel.Communication[arg].followUps[i].metDate.indexOf("/")>0){
					$scope.LmsModel.Communication[arg].followUps[i].metDate = InternalToExternalInString($scope.LmsModel.Communication[arg].followUps[i].metDate);
						var date=""+$scope.LmsModel.Communication[arg].followUps[i].metDate;
					var dateArr=date.split('-');
					$scope.LmsModel.Communication[arg].followUps[i].metDate=dateArr[2] + '/' +dateArr[1]+ '/'+dateArr[0];
					 $scope.tempFollowUpsVisit.push($scope.LmsModel.Communication[arg].followUps[i]);
					//$scope.dateFlagConvCall=true;
				
						} else if (metYear<=currDate.getFullYear()){	
						var metYearinNum=Number(metYear);
						var thaiYear=metYearinNum+543;
						dateArr[0]=thaiYear;
						$scope.LmsModel.Communication[arg].followUps[i].metDate=dateArr[2] + '/' + dateArr[1] + '/' +dateArr[0];
						 $scope.tempFollowUpsVisit.push($scope.LmsModel.Communication[arg].followUps[i]);
						}	
						
						else if(metYear<=currYear){
							   $scope.tempFollowUpsVisit.push($scope.LmsModel.Communication[arg].followUps[i]);
						}
						
						
	               
	            }
	            $scope.tempFollowUpsVisit = $filter('orderBy')($scope.tempFollowUpsVisit, 'actualDateAndTime', false);
	        }
	        $("#popupContent").empty();
	        $timeout(function () {
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                   detailedCallback();
	                }, $scope, $compile);
	        }, 50);
	    };
		
		
		  function paintVisitMeetingHistory_Edit_init (arg) {
	        $scope.tempFollowUpsVisit = [];
	        if ($scope.LmsModel.Communication[arg].followUps && $scope.LmsModel.Communication[arg].followUps.length > 0) {
	            for (i in $scope.LmsModel.Communication[arg].followUps) {
					if($scope.LmsModel.Communication[arg].followUps[i].communicationStatusCode=="MWC"){
						
	    		$scope.LmsModel.Communication[arg].followUps[i].resultDisplay =translateMessages($translate, "lms.metCustomerType");
	    	}
	    	else if($scope.LmsModel.Communication[arg].followUps[i].communicationStatusCode=="MC"){
	    	$scope.LmsModel.Communication[arg].followUps[i].resultDisplay =translateMessages($translate, "lms.meetingCancelledType");
	    	}
	    	else if($scope.LmsModel.Communication[arg].followUps[i].communicationStatusCode=="MR"){
	    		$scope.LmsModel.Communication[arg].followUps[i].resultDisplay =translateMessages($translate, "lms.meetingRescheduledType");
	    	}
	    	else{
	    		$scope.LmsModel.Communication[arg].followUps[i].resultDisplay =translateMessages($translate, "lms.OtherType");
	    	}
						
				
						
					var followupDate=""+$scope.LmsModel.Communication[arg].followUps[i].metDate;
					var dateArr =followupDate.split('-');
					var metYear=dateArr[0];
					var currDate=new Date();
					var currYear=currDate.getFullYear()+543;
					if(metYear<=currDate.getFullYear()){
						
						var metYearinNum=Number(metYear);
						var thaiYear=metYearinNum+543;
						dateArr[0]=thaiYear;
						$scope.LmsModel.Communication[arg].followUps[i].metDate=dateArr[2] + '/' + dateArr[1] + '/' +dateArr[0];
						 $scope.tempFollowUpsVisit.push($scope.LmsModel.Communication[arg].followUps[i]);
						 
					}
					else if(metYear<=currYear){
						   $scope.tempFollowUpsVisit.push($scope.LmsModel.Communication[arg].followUps[i]);
					}
						
						
						
	               
	            }
	            $scope.tempFollowUpsVisit = $filter('orderBy')($scope.tempFollowUpsVisit, 'actualDateAndTime', false);
	        }
	        $("#popupContent").empty();
	        $timeout(function () {
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                   detailedCallback();
	                }, $scope, $compile);
	        }, 50);
	    };
	    
	    function paintCallMeetingHistory () {
			if($scope.LmsModel.Communication[1].interactionType=='Call'){
	        $scope.tempFollowUps = [];
	        if ($scope.LmsModel.Communication[1].followUps && $scope.LmsModel.Communication[1].followUps.length > 0) {
	            for (i in $scope.LmsModel.Communication[1].followUps) {
						$scope.dateFlagConvCall=false;
						
					if((i==$scope.LmsModel.Communication[1].followUps.length-1) &&!$scope.LmsModel.Communication[1].followUps[i].metDate.indexOf("/")>0){
					$scope.LmsModel.Communication[1].followUps[i].metDate = InternalToExternalInString($scope.LmsModel.Communication[1].followUps[i].metDate);
					var date=""+$scope.LmsModel.Communication[1].followUps[i].metDate;
					var dateArr=date.split('-');
					$scope.LmsModel.Communication[1].followUps[i].metDate=dateArr[2] + '/' +dateArr[1]+ '/'+dateArr[0];
					 $scope.tempFollowUps.push($scope.LmsModel.Communication[1].followUps[i]);
					//$scope.dateFlagConvCall=true;
				
						} else{
	             $scope.tempFollowUps.push($scope.LmsModel.Communication[1].followUps[i]);
						}	
				
	            }
				 $scope.tempFollowUps = [];
				 for (i in $scope.LmsModel.Communication[1].followUps){
					 	var date=$scope.LmsModel.Communication[1].followUps[i].metDate;
						if(date.indexOf('-')>0){
							var dateArr=date.split('-');
							var metYear=dateArr[0];
							var currDate=new Date();
							var currYear=currDate.getFullYear();
							if(metYear<=currYear){
								var thaiConv=Number(metYear)+543;
							$scope.LmsModel.Communication[1].followUps[i].metDate=dateArr[2] + '/' + dateArr[1] + '/' + thaiConv;
							    $scope.tempFollowUps.push($scope.LmsModel.Communication[1].followUps[i]);
						}
						else{
							$scope.tempFollowUps.push($scope.LmsModel.Communication[1].followUps[i]);
						}
						}
						else{
							$scope.tempFollowUps.push($scope.LmsModel.Communication[1].followUps[i]);
						}
				 }
				      
	            
	            $scope.tempFollowUps = $filter('orderBy')($scope.tempFollowUps, 'actualDateAndTime', false);
	        }
	        $("#popupContent").empty();
	        $timeout(function () {
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                   detailedCallback();
	                }, $scope, $compile);
	        }, 50);
			}
	    };
		
		  function paintCallMeetingHistory_init () {
			if($scope.LmsModel.Communication[1].interactionType=='Call'){
	        $scope.tempFollowUps = [];
	        if ($scope.LmsModel.Communication[1].followUps && $scope.LmsModel.Communication[1].followUps.length > 0) {
	            for (i in $scope.LmsModel.Communication[1].followUps) {
						var date=$scope.LmsModel.Communication[1].followUps[i].metDate;
						if(date.indexOf('-')>0){
								var dateArr=date.split('-');
							var metYear=dateArr[0];
							var currDate=new Date();
							var currYear=currDate.getFullYear();
							if(metYear<=currYear){
								var thaiConv=Number(metYear)+543;
							$scope.LmsModel.Communication[1].followUps[i].metDate=dateArr[2] + '/' + dateArr[1] + '/' + thaiConv;
							    $scope.tempFollowUps.push($scope.LmsModel.Communication[1].followUps[i]);
						}
						else{
							$scope.tempFollowUps.push($scope.LmsModel.Communication[1].followUps[i]);
						}
							
							
						}
						else{
							 $scope.tempFollowUps.push($scope.LmsModel.Communication[1].followUps[i]);
						}
						
					
	            
						
				
	            }
				      
	            
	            $scope.tempFollowUps = $filter('orderBy')($scope.tempFollowUps, 'actualDateAndTime', false);
	        }
	        $("#popupContent").empty();
	        $timeout(function () {
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                   detailedCallback();
	                }, $scope, $compile);
	        }, 50);
			}
	    };
	    
	    function paintAppointmentMeetingHistory () {
	        $scope.tempFollowUpsApp = [];
	        if ($scope.LmsModel.Communication[2].followUps && $scope.LmsModel.Communication[2].followUps.length > 0) {
	            for (i in $scope.LmsModel.Communication[2].followUps) {
					if((i==$scope.LmsModel.Communication[2].followUps.length-1) &&!$scope.LmsModel.Communication[2].followUps[i].metDate.indexOf("/")>0){
					$scope.LmsModel.Communication[2].followUps[i].metDate = InternalToExternalInString($scope.LmsModel.Communication[2].followUps[i].metDate);
					var date=""+$scope.LmsModel.Communication[2].followUps[i].metDate;
					var dateArr=date.split('-');
					$scope.LmsModel.Communication[2].followUps[i].metDate=dateArr[2] + '/' +dateArr[1]+ '/'+dateArr[0];
					 $scope.tempFollowUpsApp.push($scope.LmsModel.Communication[2].followUps[i]);
						} else {
	                $scope.tempFollowUpsApp.push($scope.LmsModel.Communication[2].followUps[i]);
	            }
				}
				 $scope.tempFollowUpsApp = [];
				 for (i in $scope.LmsModel.Communication[2].followUps){
					 	var date=$scope.LmsModel.Communication[2].followUps[i].metDate;
						if(date.indexOf('-')>0){
							var dateArr=date.split('-');
							var metYear=dateArr[0];
							var currDate=new Date();
							var currYear=currDate.getFullYear();
							var currThaiYear=currDate.getFullYear()+543;
							if(metYear<currThaiYear){
								var thaiConv=Number(metYear)+543;
							$scope.LmsModel.Communication[2].followUps[i].metDate=dateArr[2] + '/' + dateArr[1] + '/' + thaiConv;
							    $scope.tempFollowUpsApp.push($scope.LmsModel.Communication[2].followUps[i]);
						}
						else{
							$scope.tempFollowUpsApp.push($scope.LmsModel.Communication[2].followUps[i]);
						}
						}
						else{
							$scope.tempFollowUpsApp.push($scope.LmsModel.Communication[2].followUps[i]);
						}
				 }
				     
	            $scope.tempFollowUpsApp = $filter('orderBy')($scope.tempFollowUpsApp, 'actualDateAndTime', false);
	        }
	        $("#popupContent").empty();
	        $timeout(function () {
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                   detailedCallback();
	                }, $scope, $compile);
	        }, 50);
	    };
		
		function paintAppointmentMeetingHistory_init () {
	        $scope.tempFollowUpsApp = [];
	        if ($scope.LmsModel.Communication[2].followUps && $scope.LmsModel.Communication[2].followUps.length > 0) {
	            for (i in $scope.LmsModel.Communication[2].followUps) {
					
	               var date=$scope.LmsModel.Communication[2].followUps[i].metDate;
						if(date.indexOf('-')>0){
							var dateArr=date.split('-');
							var metYear=dateArr[0];
							var currDate=new Date();
							var currYear=currDate.getFullYear();
							var currThaiYear=currDate.getFullYear()+543;
							if(metYear<currThaiYear){
								var thaiConv=Number(metYear)+543;
							$scope.LmsModel.Communication[2].followUps[i].metDate=dateArr[2] + '/' + dateArr[1] + '/' + thaiConv;
							    $scope.tempFollowUpsApp.push($scope.LmsModel.Communication[2].followUps[i]);
						}
						else{
							$scope.tempFollowUpsApp.push($scope.LmsModel.Communication[2].followUps[i]);
						}
						}
						else{
							 $scope.tempFollowUpsApp.push($scope.LmsModel.Communication[2].followUps[i]);
						}
	            
				}
	            $scope.tempFollowUpsApp = $filter('orderBy')($scope.tempFollowUpsApp, 'actualDateAndTime', false);
	        }
	        $("#popupContent").empty();
	        $timeout(function () {
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                   detailedCallback();
	                }, $scope, $compile);
	        }, 50);
	    };
	    
	    function paintVisitMeetingHistory () {
	        $scope.tempFollowUpsVisit = [];
	        if ($scope.LmsModel.Communication[3].followUps && $scope.LmsModel.Communication[3].followUps.length > 0) {
	            for (i in $scope.LmsModel.Communication[3].followUps) {
				
				if((i==$scope.LmsModel.Communication[3].followUps.length-1) &&!$scope.LmsModel.Communication[3].followUps[i].metDate.indexOf("/")>0){
					$scope.LmsModel.Communication[3].followUps[i].metDate = InternalToExternalInString($scope.LmsModel.Communication[3].followUps[i].metDate);
					var date=""+$scope.LmsModel.Communication[3].followUps[i].metDate;
					var dateArr=date.split('-');
					$scope.LmsModel.Communication[3].followUps[i].metDate=dateArr[2] + '/' +dateArr[1]+ '/'+dateArr[0];
					 $scope.tempFollowUpsVisit.push($scope.LmsModel.Communication[3].followUps[i]);
						} else {
	              $scope.tempFollowUpsVisit.push($scope.LmsModel.Communication[3].followUps[i]);
						}
				
	            }
				 $scope.tempFollowUpsVisit = [];
				 for (i in $scope.LmsModel.Communication[3].followUps){
					 		var date=$scope.LmsModel.Communication[3].followUps[i].metDate;
						if(date.indexOf('-')>0){
							var dateArr=date.split('-');
							var metYear=dateArr[0];
							var currDate=new Date();
							var currYear=currDate.getFullYear();
							if(metYear<=currYear){
								var thaiConv=Number(metYear)+543;
							$scope.LmsModel.Communication[3].followUps[i].metDate=dateArr[2] + '/' + dateArr[1] + '/' + thaiConv;
							    $scope.tempFollowUpsVisit.push($scope.LmsModel.Communication[3].followUps[i]);
						}
						else{
							$scope.tempFollowUpsVisit.push($scope.LmsModel.Communication[3].followUps[i]);
						}
						}
						else{
							 $scope.tempFollowUpsVisit.push($scope.LmsModel.Communication[3].followUps[i]);
						}
				 }
	            $scope.tempFollowUpsVisit = $filter('orderBy')($scope.tempFollowUpsVisit, 'actualDateAndTime', false);
	        }
	        $("#popupContent").empty();
	        $timeout(function () {
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                   detailedCallback();
	                }, $scope, $compile);
	        }, 50);
	    };
		function paintVisitMeetingHistory_init () {
	        $scope.tempFollowUpsVisit = [];
	        if ($scope.LmsModel.Communication[3].followUps && $scope.LmsModel.Communication[3].followUps.length > 0) {
	            for (i in $scope.LmsModel.Communication[3].followUps) {
				
				
	              var date=$scope.LmsModel.Communication[3].followUps[i].metDate;
						if(date.indexOf('-')>0){
								var dateArr=date.split('-');
							var metYear=dateArr[0];
							var currDate=new Date();
							var currYear=currDate.getFullYear();
							if(metYear<=currYear){
								var thaiConv=Number(metYear)+543;
							$scope.LmsModel.Communication[3].followUps[i].metDate=dateArr[2] + '/' + dateArr[1] + '/' + thaiConv;
							    $scope.tempFollowUpsVisit.push($scope.LmsModel.Communication[3].followUps[i]);
						}
						else{
							$scope.tempFollowUpsVisit.push($scope.LmsModel.Communication[3].followUps[i]);
						}
							
							
						}
						else{
							 $scope.tempFollowUpsVisit.push($scope.LmsModel.Communication[3].followUps[i]);
						}
	            
						
				
	            }
	            $scope.tempFollowUpsVisit = $filter('orderBy')($scope.tempFollowUpsVisit, 'actualDateAndTime', false);
	        }
	        $("#popupContent").empty();
	        $timeout(function () {
	            LEDynamicUI.paintUI(rootConfig.template,
	                "leadListingWithTabs.json",
	                "callDetailsSection", "#popupContent", true,
	                function () {
	                   detailedCallback();
	                }, $scope, $compile);
	        }, 50);
	    };
	    
	    
	    var onLangChange = $scope
	        .$on(
	            'lang-changed',
	            function (event, args) {

	                $scope
	                    .validateFields('callDetailsSection');
	            });

	    $scope.$on('$destroy', function () {
	        onLangChange();
	    });

	    $scope.Initialize = function () {
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6');
	}
	        $scope.leadHeader = "";
	        $scope.editMode = LmsVariables.editMode;
	        $scope.LmsModel = LmsVariables.getLmsModel();
	        UtilityService.InitializeErrorDisplay($scope,
	            $rootScope, $translate);
	        $scope.dispositionArray = LmsVariables.dispositionArray;
	        $scope.subDispositionArray = LmsVariables.subDispositionArray;
	        $scope.interactionArray;
	        $scope.jointCallWithArray = LmsVariables.jointCallWithArray;
	        $scope.droppedReasonArray = LmsVariables.droppedReasonArray;
	        $scope.droppedSubReasonArray = LmsVariables.droppedSubReasonArray;
	        $scope.productSoldArray = LmsVariables.productSoldArray;
	        $scope.numberPattern = rootConfig.numberPattern;
	        //Commenting as it is not needed in GLI
	        if (!((rootConfig.isDeviceMobile))) {
	            $('.custdatepicker')
	                .datepicker({
	                    format: 'yyyy-mm-dd',
	                    autoclose: true
	                })
	                .on(
	                    'change',
	                    function () {

	                    });
	        }
	        $scope.updateErrorCount = function (arg1, arg2) {
	            $rootScope.updateErrorCount(arg1, arg2);
	            if ($scope.LmsModel.Communication[0].followUps && $scope.LmsModel.Communication[0].followUps[0]) {
	                if ($scope.LmsModel.Lead.StatusDetails.disposition == "Open" &&
	                    $scope.LmsModel.Communication[0].followUps[0].followUpsDate != undefined &&
	                    $scope.LmsModel.Communication[0].followUps[0].followUpsTime != undefined) {
	                    var followUpDate = $scope.LmsModel.Communication.followUps[0].followUpsDate;
	                    if ($scope.LmsModel.Communication[0].followUps[0].followUpsTime != undefined &&
	                        $scope.LmsModel.Communication[0].followUps[0].followUpsTime != "Invalid Date")
	                        followUpDate = followUpDate.toJSON().slice(0, 10) +
	                        "T" +
	                        $scope.LmsModel.Communication[0].followUps[0].followUpsTime.toTimeString().slice(0, 8) + "Z";

	                    if (LEDate(followUpDate) <= LEDate()) {
	                        $scope.errorCount = parseInt($scope.errorCount) + 1;
	                    }
	                } else if ($scope.LmsModel.Lead.StatusDetails.disposition == "Login") {
	                    if (LEDate($scope.LmsModel.SystemRecordDetails.loginDate) > LEDate())
	                        $scope.errorCount = parseInt($scope.errorCount) + 1;
	                }
	            }
	        };
	        $scope.showErrorPopup = function (arg1, arg2) {
	            $rootScope.showErrorPopup(arg1, arg2);
	            if ($scope.LmsModel.Communication[0].followUps && $scope.LmsModel.Communication[0].followUps[0]) {
	                if ($scope.LmsModel.Lead.StatusDetails.disposition == "Open" &&
	                    $scope.LmsModel.Communication[0].followUps[0].followUpsDate != undefined &&
	                    $scope.LmsModel.Communication[0].followUps[0].followUpsTime != undefined) {
	                    var followUpDate = $scope.LmsModel.Communication[0].followUps[0].followUpsDate;
	                    if ($scope.LmsModel.Communication[0].followUps[0].followUpsTime != undefined &&
	                        $scope.LmsModel.Communication[0].followUps[0].followUpsTime != "Invalid Date")
	                        followUpDate = followUpDate.toJSON().slice(0, 10) +
	                        "T" +
	                        $scope.LmsModel.Communication[0].followUps[0].followUpsTime.toTimeString().slice(0, 8) + "Z";
	                    if (LEDate(followUpDate) <= LEDate()) {
	                        var error = {};
	                        error.message = translateMessages(
	                            $translate,
	                            "lms.meetingDateFuturedate");
	                        // meetingDateLesserThanFollowup
	                        error.key = 'followUpDate';
	                        $scope.errorMessages.push(error);
	                    }
	                } else if ($scope.LmsModel.Lead.StatusDetails.disposition == "Login") {
	                    if (LEDate($scope.LmsModel.SystemRecordDetails.loginDate) > LEDate()) {
	                        var error = {};
	                        error.message = translateMessages(
	                            $translate,
	                            "lms.callDetailsSectionloginDateNotFutureValidationMessage");
	                        // meetingDateLesserThanFollowup
	                        error.key = 'loginDate';
	                        $scope.errorMessages.push(error);
	                    }
	                }
	            }
	        };
	    };
	    $scope.addPolicyDetail = function () {
	        var policyModel = {
	            "productCode": "",
	            "proposalNumber": "",
	            "premium": "",
	            "submissionDate": ""
	        };
	        $scope.LmsModel.Policies.productSold.push(policyModel);
	        $scope.validateFields('callDetailsSection');
	    };

	    $scope.deletePolicyDetail = function (index) {
	        $scope.LmsModel.Policies.productSold.splice(index, 1);
	        if ($scope.LmsModel.Policies.productSold.length == 0)
	            $scope.addPolicyDetail();
	        $scope.validateFields('callDetailsSection');
	    };

	    $scope.setSubmissionDate = function (index) {
	        $scope.LmsModel.Policies.productSold[index].submissionDate = LEDate();
	    };

	    $scope.reset = function () {
	        $scope.actualMeetingDate = "";
	        $scope.actualMeetingTime = "";
	        $scope.LmsModel.Communication[0].interactionType = "";
	        if ($scope.editMode) {
	            $scope.LmsModel.Lead.StatusDetails.disposition = "";
	        }
	        $scope.remarks = "";
	        $scope.LmsModel.Lead.BasicDetails.action = "";
	        $scope.LmsModel.Policies.productSold.submissionDate = "";
	        $scope.planedMeetingDate = "";
	        $scope.planedMeetingTime = "";
	        $scope.LmsModel.Lead.BasicDetails.nextAction = "";
	        for (var i = 0; i < $scope.LmsModel.Policies.productSold.length; i++) {
	            $scope.LmsModel.Policies.productSold[i].proposalNumber = "";
	            $scope.LmsModel.Policies.productSold[i].premium = "";
	        }
	    };
		$scope.closePopup = function () {
            $scope.leadDetShowPopUpMsg = false;
        };
	};