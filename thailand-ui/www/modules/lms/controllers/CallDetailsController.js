
/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:CallDetailsController
 CreatedDate:20/02/2015
 Description:Lead Management Module.
 */
storeApp
		.controller(
				'CallDetailsController',
				[
						'$rootScope',
						'$scope',
						'$compile',
						'DataService',
						'$translate',
						'LmsVariables',
						'LmsService',
						'$location',
						'globalService',
						'UtilityService',
						'$timeout',
						function CallDetailsController($rootScope, $scope,
								$compile, DataService, $translate,
								LmsVariables, LmsService, $location,
								globalService, UtilityService, $timeout) {

							var old_updateErrorCount = $rootScope.updateErrorCount;
							var old_showErrorPopup = $rootScope.showErrorPopup;
							var dispositionWatch;
							var reasonWatch;
							var isJointCallWatch;
							$scope.isSubReasonDisabled = true;
							$scope.isJointCall = false;

							$scope.Initialize = function() {
								$scope.leadHeader = "";
								$scope.editMode = LmsVariables.editMode;
								$scope.LmsModel = LmsVariables.getLmsModel();
								$scope.prevdateTime = LEDate($scope.LmsModel.Communication.followUps[0].dateAndTime);
								UtilityService.InitializeErrorDisplay($scope,
										$rootScope, $translate);
								$scope.dispositionArray = LmsVariables.dispositionArray;
								$scope.subDispositionArray = LmsVariables.subDispositionArray;
								$scope.interactionArray;// =
														// LmsVariables.interactionArray;
								$scope.jointCallWithArray = LmsVariables.jointCallWithArray;
								$scope.droppedReasonArray = LmsVariables.droppedReasonArray;
								$scope.droppedSubReasonArray = LmsVariables.droppedSubReasonArray;
								$scope.productSoldArray = LmsVariables.productSoldArray;
								$scope.numberPattern = rootConfig.numberPattern;
								if ($scope.LmsModel.SystemRecordDetails.expectedLogin == "") {
									if($scope.LmsModel.Communication.followUps && $scope.LmsModel.Communication.followUps[0]){
										$scope.LmsModel.SystemRecordDetails.expectedLogin = formatForDateControl($scope.LmsModel.Communication.followUps[0].followUpsDate);
									}
								} else {
									$scope.LmsModel.SystemRecordDetails.expectedLogin = new Date($scope.LmsModel.SystemRecordDetails.expectedLogin);
								}

								if (!((rootConfig.isDeviceMobile))) {
									$('.custdatepicker')
											.datepicker({
												format : 'yyyy-mm-dd',
												autoclose : true
											})
											.on(
													'change',
													function() {

														/*$timeout(
																function() {
																	$rootScope
																			.updateErrorCount('callDetailsSection');
																	if ($scope.LmsModel.Lead.StatusDetails.disposition == "Open") {
																		if($scope.LmsModel.Communication.followUps && $scope.LmsModel.Communication.followUps[0]){
																			if($scope.LmsModel.Communication.followUps && $scope.LmsModel.Communication.followUps[0]){
																				var followUpDate = $scope.LmsModel.Communication.followUps[0].followUpsDate;
																				if ($scope.LmsModel.Communication.followUps[0].followUpsTime != undefined
																						&& $scope.LmsModel.Communication.followUps[0].followUpsTime != "Invalid Date")
																					followUpDate = followUpDate
																							+ " "
																							+ $scope.LmsModel.Communication.followUps[0].followUpsTime;
	
																				if (LEDate(followUpDate) <= LEDate()) {
																					$scope.errorCount = parseInt($scope.errorCount) + 1;
																				}
																			}
																		}
																	} else if ($scope.LmsModel.Lead.StatusDetails.disposition == "Login") {
																		if (LEDate($scope.LmsModel.SystemRecordDetails.loginDate) > LEDate())
																			$scope.errorCount = parseInt($scope.errorCount) + 1;
																	}
																}, 10);*/

													});
								}

								$scope.updateErrorCount = function(arg1, arg2) {

									$rootScope.updateErrorCount(arg1, arg2);
									if($scope.LmsModel.Communication.followUps && $scope.LmsModel.Communication.followUps[0]){
										if ($scope.LmsModel.Lead.StatusDetails.disposition == "Open"
												&& $scope.LmsModel.Communication.followUps[0].followUpsDate != undefined
												&& $scope.LmsModel.Communication.followUps[0].followUpsTime != undefined) {
											var followUpDate = $scope.LmsModel.Communication.followUps[0].followUpsDate;
											if ($scope.LmsModel.Communication.followUps[0].followUpsTime != undefined
													&& $scope.LmsModel.Communication.followUps[0].followUpsTime != "Invalid Date")
												followUpDate = followUpDate.toJSON().slice(0,10)
												+ "T"
												+ $scope.LmsModel.Communication.followUps[0].followUpsTime.toTimeString().slice(0,8)+"Z";
	
											if (LEDate(followUpDate) <= LEDate()) {
												$scope.errorCount = parseInt($scope.errorCount) + 1;
											}
										} else if ($scope.LmsModel.Lead.StatusDetails.disposition == "Login") {
											if (LEDate($scope.LmsModel.SystemRecordDetails.loginDate) > LEDate())
												$scope.errorCount = parseInt($scope.errorCount) + 1;
										}
									}
								};
								$scope.showErrorPopup = function(arg1, arg2) {
									$rootScope.showErrorPopup(arg1, arg2);
									if($scope.LmsModel.Communication.followUps && $scope.LmsModel.Communication.followUps[0]){
										if ($scope.LmsModel.Lead.StatusDetails.disposition == "Open"
												&& $scope.LmsModel.Communication.followUps[0].followUpsDate != undefined
												&& $scope.LmsModel.Communication.followUps[0].followUpsTime != undefined) {
											var followUpDate = $scope.LmsModel.Communication.followUps[0].followUpsDate;
											if ($scope.LmsModel.Communication.followUps[0].followUpsTime != undefined
													&& $scope.LmsModel.Communication.followUps[0].followUpsTime != "Invalid Date")
												followUpDate = followUpDate.toJSON().slice(0,10)
												+ "T"
												+ $scope.LmsModel.Communication.followUps[0].followUpsTime.toTimeString().slice(0,8)+"Z";

	
											if (LEDate(followUpDate) <= LEDate()) {
												var error = {};
												error.message = translateMessages(
														$translate,
														"lms.meetingDateFuturedate");
												// meetingDateLesserThanFollowup
												error.key = 'followUpDate';
												$scope.errorMessages.push(error);
											}
										} else if ($scope.LmsModel.Lead.StatusDetails.disposition == "Login") {
											if (LEDate($scope.LmsModel.SystemRecordDetails.loginDate) > LEDate()) {
												var error = {};
												error.message = translateMessages(
														$translate,
														"lms.callDetailsSectionloginDateNotFutureValidationMessage");
												// meetingDateLesserThanFollowup
												error.key = 'loginDate';
												$scope.errorMessages.push(error);
											}
										}
									}
								};
								/*$timeout(
										function() {
											$rootScope
													.updateErrorCount('callDetailsSection')
											if ($scope.LmsModel.Lead.StatusDetails.disposition == "Open") {
												if($scope.LmsModel.Communication.followUps && $scope.LmsModel.Communication.followUps[0]){
													var followUpDate = $scope.LmsModel.Communication.followUps[0].followUpsDate;
													if ($scope.LmsModel.Communication.followUps[0].followUpsTime != undefined
															&& $scope.LmsModel.Communication.followUps[0].followUpsTime != "Invalid Date")
														followUpDate = followUpDate
																+ " "
																+ $scope.LmsModel.Communication.followUps[0].followUpsTime;
	
													if (LEDate(followUpDate) <= LEDate()) {
														$scope.errorCount = parseInt($scope.errorCount) + 1;
													}
												}
											} else if ($scope.LmsModel.Lead.StatusDetails.disposition == "Login") {
												if (LEDate($scope.LmsModel.SystemRecordDetails.loginDate) > LEDate())
													$scope.errorCount = parseInt($scope.errorCount) + 1;
											}
										}, 10);*/

							};
							$scope.onSaveSuccess = function() {
								$rootScope.showHideLoadingImage(false);
								if( (rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
									if(LEDate($scope.LmsModel.Communication.followUps[0].dateAndTime) - $scope.prevdateTime != 0){// Add calendar event only if date or time changed 
											var startDate = LEDate($scope.LmsModel.Communication.followUps[0].dateAndTime);  
											var endDate =LEDate($scope.LmsModel.Communication.followUps[0].dateAndTime);
											endDate.setHours ( startDate.getHours() + parseInt(rootConfig.leadMeetingTimeInHr) );
											var title = translateMessages($translate,"lms.eventTitle")+$scope.LmsModel.Lead.BasicDetails.fullName;
											var location = "";
											var notes =  translateMessages($translate,"lms.eventNote");
											var reminderTimeInMin = rootConfig.reminderTimeInMin;
											var success = function(message) { 
											};
											var error = function(message) {
											};

											window.plugins.calendar.createEvent(title,location,notes,startDate,endDate,reminderTimeInMin,success,error);
											$scope.prevdateTime = LEDate($scope.LmsModel.Communication.followUps[0].dateAndTime);
									}
								}
								$scope.refresh();
								$rootScope.lePopupCtrl
										.showSuccess(translateMessages(
												$translate, "lifeEngage"),
												translateMessages($translate,
														"leadSaved"),
												translateMessages($translate,
														"fna.ok"));
								// $rootScope.showMessagePopup(true, "success",
								// translateMessages(
								// $translate, "leadSaved"));
							};
							$scope.onSaveError = function(msg) {
								// TODO
								$rootScope.showHideLoadingImage(false);
								$scope.refresh();
								if (msg == "This Trasaction Data is already processed : ") {
									$rootScope.lePopupCtrl.showError(
											translateMessages($translate,
													"lifeEngage"),
											translateMessages($translate,
													"leadAlreadyExists"),
											translateMessages($translate,
													"fna.ok"));
									// $rootScope.showMessagePopup(true,
									// "error", translateMessages($translate,
									// "leadAlreadyExists"));
								} else {
									$rootScope.lePopupCtrl.showError(
											translateMessages($translate,
													"lifeEngage"),
											translateMessages($translate,
													"failure"),
											translateMessages($translate,
													"fna.ok"));
									//$rootScope.showMessagePopup(true, "error", translateMessages($translate, "Failure"));
								}
							};
							$scope.Save = function() {
								$rootScope.showHideLoadingImage(true,
										'paintUIMessage', $translate);
								LmsVariables.setLmsModel($scope.LmsModel);
								LmsService.saveTransactions(
										$scope.onSaveSuccess,
										$scope.onSaveError, $scope.LmsModel);
							};
							
							$scope.$on('$viewContentLoaded', function(){
								$scope.Initialize();
							  });

							dispositionWatch = $scope
									.$watch(
											'LmsModel.Lead.StatusDetails.disposition',
											function(value) {
												if (value == 'Dropped') {
													$scope.errorCount = parseInt($scope.errorCount) + 1;
												}
											}, true);

							reasonWatch = $scope
									.$watch(
											'LmsModel.Lead.StatusDetails.reason',
											function(value) {
												if (value == ''
														|| typeof value == 'undefined') {
													$scope.isSubReasonDisabled = true;
													$scope.refresh();
												} else {
													$scope.isSubReasonDisabled = false;
												}

											}, true);

							jointCallWatch = $scope
									.$watch(
											'LmsModel.Communication.followUps[0].isJointCall',
											function(value) {
												if (value == "false") {
													$scope.LmsModel.Communication.followUps[0].jointCallWith = "";
													$scope.refresh();
												}
											}, true);
							
							$scope.$on("$destroy",function() {
							    jointCallWatch();
							    reasonWatch();
							    dispositionWatch();
							});

						} ]);