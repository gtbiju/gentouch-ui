/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:GLI_FeedbackController
 CreatedDate:20/07/2015
 Description:KMC module to implement Generali specific changes. 
 */
storeApp.controller('GLI_FeedbackController', GLI_FeedbackController);
GLI_FeedbackController.$inject = ['$rootScope', '$scope', '$compile', 'DataService', '$translate', 'LmsVariables', 'LmsService', '$location', 'globalService', 'UtilityService', '$timeout', '$controller', 'GLI_LmsService'];

function GLI_FeedbackController($rootScope, $scope, $compile, DataService, $translate, LmsVariables, LmsService, $location, globalService, UtilityService, $timeout, $controller, GLI_LmsService) {
    $controller('FeedbackController', {
        $rootScope: $rootScope,
        $scope: $scope,
        $compile: $compile,
        DataService: DataService,
        $translate: $translate,
        LmsVariables: LmsVariables,
        LmsService: LmsService,
        $location: $location,
        globalService: globalService,
        UtilityService: UtilityService,
        $timeout: $timeout
    });

    $scope.mandatory = false;
    $scope.isEditMode = LmsVariables.editMode;
    $scope.popupTabs = ['', '', '', ''];
    $scope.popupTabs[3] = 'active';
    $rootScope.selectedPage = "leadFeedback";
    $scope.appDateTimeFormat = rootConfig.appDateTimeFormat;

    function GLI_init() {
        if (LmsVariables.editMode == false) {

            LEDynamicUI.paintUI(rootConfig.template,
                "leadListingWithTabs.json",
                "popupTabsStartbar", "#popupTabs", true,
                function () {
                    $scope.callback();
                }, $scope, $compile);

        } else {
            $scope.LmsModel.Feedback.LastUpdatedDetails = "";
            $scope.feedbackQuestion5 = translateMessages($translate, "lms.feedbackQuestion5");
            $scope.LmsModel = LmsVariables.getLmsModel();
            if ($scope.LmsModel.Lead.RMInfo.RMName != '') {
                $scope.showFeedback = true;
            }
            $scope.FeedbackQuestion = $scope.LmsModel.Feedback.questions;

            if ($scope.FeedbackQuestion[1].option == "0")
                $scope.LmsModel.Feedback.questions[1].option = "Yes"
            if ($scope.FeedbackQuestion[1].option == "1")
                $scope.LmsModel.Feedback.questions[1].option = "No"
            LEDynamicUI.paintUI(rootConfig.template,
                "leadListingWithTabs.json",
                "popupTabsNavbar", "#popupTabs", true,
                function () {
                    $scope.callback();
                }, $scope, $compile);

        }
        LEDynamicUI.paintUI(rootConfig.template,
            "leadListingWithTabs.json",
            "feedbackDetailsSection", "#popupContent", true,
            function () {
                $scope.callback();
            }, $scope, $compile);
        LEDynamicUI.getUIJson(rootConfig.lmsConfigJson, false, function (lmsConfig) {
            $scope.lmsnavigations = lmsConfig.lmsnavigations;

        });
    };

    //Implementing GLI_LmsService
    $scope.Save = function () {
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
        var today = new Date();
        $scope.LastUpdatedDate = formatForDateControl(today) + " " + formatForTimeControl(today, false);
        $scope.LmsModel.Feedback.LastUpdatedDetails = formatForDateControl(today) + " " + formatForTimeControl(today, false);

        if ($scope.LmsModel.Feedback.questions[1].option == "Yes")
            $scope.LmsModel.Feedback.questions[1].option = "0";
        if ($scope.LmsModel.Feedback.questions[1].option == "No")
            $scope.LmsModel.Feedback.questions[1].option = "1";
        LmsVariables.setLmsModel($scope.LmsModel);
        GLI_LmsService.saveTransactions($scope.onSaveSuccess, $scope.onSaveError, $scope.LmsModel);
    };
    $scope.onSaveSuccess = function () {
        $rootScope.showHideLoadingImage(false, "Loading..");
        $rootScope.lePopupCtrl
            .showSuccess(translateMessages(
                    $translate, "lifeEngage"),
                translateMessages($translate,
                    "leadFeedbackSaved"),
                translateMessages($translate,
                    "lms.ok"), $scope.backToLeadListing);

    }
    $scope.backToLeadListing = function () {
        $location.path('/lms');
        $scope.refresh();
    }

    $scope.popupTabsNav = function (rel, index) {
        if (index != 3) {
            $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
            $("#popupContent").empty();
            var url = "";
            for (nav in $scope.lmsnavigations) {
                if ($scope.lmsnavigations[nav].name == rel) {
                    url = $scope.lmsnavigations[nav].url;
                    break;
                }
            }
            $rootScope.showHideLoadingImage(false);
            $location.path(url);
            $scope.refresh();
        }
    };
    $scope.callback = function () {
        $rootScope.showHideLoadingImage(false);

    };

    GLI_init();

    //back tab
    $scope.List = function () {
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
        GLI_LmsService.navigateToListingPage("/lms");
    };

};