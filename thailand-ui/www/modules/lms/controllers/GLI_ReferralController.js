/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:GLI_ReferralController
 CreatedDate:20/07/2015
 Description:KMC module to implement Generali specific changes. 
 */
storeApp
    .controller(
        'GLI_ReferralController', GLI_ReferralController);
GLI_ReferralController.$inject = ['$controller',
						'$rootScope',
						'$scope',
						'$compile',
						'GLI_DataService',
						'$translate',
						'LmsVariables',
						'LmsService',
						'$location',
						'globalService',
						'UtilityService',
						'$timeout',
						'PersistenceMapping',
						'$routeParams',
						'GLI_LmsService',
						'DataService'];

function GLI_ReferralController($controller,
    $rootScope, $scope, $compile, GLI_DataService,
    $translate, LmsVariables, LmsService,
    $location, globalService, UtilityService,
    $timeout, PersistenceMapping, $routeParams,
    GLI_LmsService, DataService) {
    $controller('ReferralController', {
        $rootScope: $rootScope,
        $scope: $scope,
        $compile: $compile,
        DataService: GLI_DataService,
        $translate: $translate,
        LmsVariables: LmsVariables,
        LmsService: GLI_LmsService,
        $location: $location,
        globalService: globalService,
        UtilityService: UtilityService,
        $timeout: $timeout,
        PersistenceMapping: PersistenceMapping,
        $routeParams: $routeParams
    });
    $scope.mandatory = true;
    $rootScope.selectedPage = "leadFeedback";
    $scope.popupTabs = ['', '', '', ''];
    $scope.popupTabs[2] = 'active';
    $scope.isEditMode = LmsVariables.editMode;
    $scope.GLI_init = function () {
        if (LmsVariables.editMode == false) {
            LEDynamicUI.paintUI(rootConfig.template,
                "leadListingWithTabs.json",
                "popupTabsStartbar", "#popupTabs",
                true,
                function () {
                    $scope.callback();
                }, $scope, $compile);

        } else {
            LEDynamicUI.paintUI(rootConfig.template,
                "leadListingWithTabs.json",
                "popupTabsNavbar", "#popupTabs",
                true,
                function () {
                    $scope.callback();
                }, $scope, $compile);

        }
        LEDynamicUI.paintUI(rootConfig.template,
            "leadListingWithTabs.json",
            "referralsDetailsSection",
            "#popupContent", true,
            function () {
                $scope.callback();
            }, $scope, $compile);
        LEDynamicUI
            .getUIJson(
                rootConfig.lmsConfigJson,
                false,
                function (lmsConfig) {
                    $scope.lmsnavigations = lmsConfig.lmsnavigations;

                });
    };

    $scope.Initialize = function () {
        $scope.leadHeader = "";
        $scope.editMode = LmsVariables.editMode;
        $scope.BackUpParent();
        $scope.LmsModel = new LmsObject();
        if ($scope.parentLmsModel.Lead.RMInfo.RMName != '') {
            $scope.showFeedback = true;
        }
        $scope.referralContact = "";
        if ($scope.LmsModel.Communication.followUps && $scope.LmsModel.Communication.followUps[0]) {
            $scope.LmsModel.Communication.followUps[0].followUpsDate = formatForDateControl(new Date());
            $scope.LmsModel.Communication.followUps[0].followUpsTime = formatForTimeControl(
                new Date(), false);
        }
        UtilityService.InitializeErrorDisplay($scope,
            $rootScope, $translate);

        if (!(rootConfig.isDeviceMobile)) {
            $scope.referralLeads = LmsVariables.getRefLead();
        } else {
            $scope.getReferrals();
        }
        $scope.numberPatternMob = rootConfig.numberPatternMob;
        $scope.leadNamePattern = rootConfig.leadNamePattern;
        $scope.numberPattern = rootConfig.numberPattern;
    };

    //To retrieve all leads
    $scope.getReferrals = function () {
        PersistenceMapping.clearTransactionKeys();
        var searchCriteria = {
            searchCriteriaRequest: {
                "command": "ListingDashBoard",
                "modes": ['LMS'],
                "value": ""
            }
        };
        PersistenceMapping.dataIdentifyFlag = false;
        var transactionObj = PersistenceMapping
            .mapScopeToPersistence(searchCriteria);
        transactionObj.Id = $routeParams.transactionId;
        transactionObj.Key10 = "Custom";
        transactionObj.Type = "LMS";
        transactionObj.filter = "Key15 <> 'Cancelled'";
        GLI_DataService.getFilteredListing(
            transactionObj,
            $scope.onGetListingsSuccess);
    };

    $scope.onGetListingsSuccess = function (dbObject) {
        if (!(rootConfig.isDeviceMobile)) {
            $scope.leads = dbObject.sort(function (a, b) {
                return LEDate(b.Key14) - LEDate(a.Key14);
            });
        } else {
            $scope.referralLeads = dbObject;
            for (var i = 0; i < dbObject.length; i++) {
                if (dbObject[i].Key4 == $scope.parentLmsModel.Lead.ContactDetails.mobileNumber1) {
                    $scope.parentLmsKeys.Key1 = dbObject[i].Key1;
                    break;
                }
            }
        }
    };

    $scope.cancel = function () {
        $scope.dvDeleteConfirm = false;
    };
    $scope.DeleteLead = function (id, thisLead, isSyncId) {
        $scope.id = id;
        $scope.thisLead = thisLead;
        $scope.isSyncId = isSyncId;
        if ($scope.isSyncId == "" && (rootConfig.isDeviceMobile)) {
            $rootScope.lePopupCtrl
                .showQuestion(
                    translateMessages($translate,
                        "delete"),
                    translateMessages($translate,
                        "deleteWarningText"),
                    translateMessages($translate,
                        "deleteConfirmButtonText"),
                    $scope.deleteConfirmed,
                    translateMessages($translate,
                        "cancelButtonText"),
                    $scope.cancel,
                    '',
                    '',
                    function () {
                        $scope.dvDeleteConfirm = false;
                    });
        } else {
            return '';
        }

    };

    // implementing GLI_LmsService
    $scope.deleteConfirmed = function () {
        // isSyncId added For getting key1 that ensures whether the lead is synced or not
        $scope.deleteLead = $scope.thisLead;
        $scope.thisLead.Key15 = "Cancelled";
        $scope.BackUpParent();
        LmsVariables.lmsKeys.Id = $scope.id;
        GLI_LmsService
            .getLead(function (refLeadData) {
                LmsVariables.editMode = true;
                LmsVariables.LmsModel.Lead.StatusDetails.disposition = "Cancelled";
                $scope.selectedReferral = [];
                $scope.selectedReferral[0] = refLeadData;
                $scope.selectedReferral[0].Id = $scope.id;
                $scope.selectedReferral[0].Key1 = $scope.thisLead.Key1;
                LmsService.deleteClient(refLeadData, $scope.onFirstSuccessCallBack, $scope.onSaveError);
            });
    };

    $scope.onFirstSuccessCallBack = function () {
        var whereClause = 'Key1 = ' + '"' + $scope.selectedReferral[0].TransTrackingID + '"';
        DataService.deleteFromTable('Transactions', whereClause, $scope.onDeleteSuccess, $scope.onSaveError);
    };

    $scope.onDeleteSuccess = function () {
        var idx = $scope.referralLeads
            .indexOf($scope.deleteLead);
        if (idx > -1) {
            $scope.referralLeads.splice(idx, 1);
        }
        idx = $scope.referralLeads.indexOf($scope.deleteLead);
        if (idx > -1) {
            $scope.referralLeads.splice(idx, 1);
        }
        $scope.refresh();
        $scope.RestoreParent();
        $rootScope.showHideLoadingImage(false);
    };

    $scope.onSaveSuccess = function (data) {
        $scope.refresh();
        $rootScope.NotifyMessages(false, "Success",
            $translate);
        var leadObject = {
            "Id": "",
            "Key1": "",
            "Key2": "",
            "Key4": "",
            "Key8": ""
        };
        leadObject.Id = data;
        leadObject.Key2 = $scope.LmsModel.Lead.BasicDetails.fullName;
        leadObject.Key4 = $scope.LmsModel.Lead.ContactDetails.mobileNumber1 &&
            $scope.LmsModel.Lead.ContactDetails.mobileNumber1 != "" ? $scope.LmsModel.Lead.ContactDetails.mobileNumber1 :
            $scope.LmsModel.Lead.ContactDetails.homeNumber1 ? $scope.LmsModel.Lead.ContactDetails.homeNumber1 :
            "";
        leadObject.Key8 = $scope.parentLmsKeys.Key1;

        LmsVariables.getRefLead().push(leadObject);
        $scope.RestoreParent();
        $scope.refresh();

        $scope.Initialize();
        $scope.showRefPop = false;
        $rootScope.showHideLoadingImage(false);

    };

    $scope.onSaveError = function (msg) {
        $rootScope.showHideLoadingImage(false);
        $scope.refresh();
        $scope.RestoreParent();
        if (msg == "This Trasaction Data is already processed : " || msg == "Duplicate Lead : ") {
            $rootScope.lePopupCtrl.showError(
                translateMessages($translate,
                    "lifeEngage"),
                translateMessages($translate,
                    "leadAlreadyExists"),
                translateMessages($translate,
                    "lms.ok"));
        } else {
            $rootScope.lePopupCtrl.showError(
                translateMessages($translate,
                    "lifeEngage"),
                translateMessages($translate,
                    "failure"),
                translateMessages($translate,
                    "lms.ok"));
        }
    };

    $scope.Save = function () {
        if ($scope.parentLmsKeys.Key1 != "" &&
            $scope.parentLmsKeys.Key1 != null) {
            $rootScope
                .updateErrorCount("referralsDetailsSection");
            if ($scope.errorCount == 0) {
                $rootScope.showHideLoadingImage(true,
                    'paintUIMessage', $translate);
                LmsVariables.lmsKeys.Id = "";
                LmsVariables.lmsKeys.Key1 = "";
                LmsVariables.lmsKeys.Key25 = "";
                LmsVariables.transTrackingID = "";
                $scope.LmsModel.Referrals.parentLeadId = angular
                    .copy($scope.parentLmsKeys.Key1);
                $scope.LmsModel.Referrals.referredByName = LmsVariables.LmsModel.Lead.BasicDetails.fullName;
                $scope.LmsModel.Referrals.referredByContact = LmsVariables.LmsModel.Lead.ContactDetails.mobileNumber1;
                $scope.LmsModel.Referrals.referredByEmail = LmsVariables.LmsModel.Lead.ContactDetails.emailId;

                $scope.LmsModel.Lead.ContactDetails.mobileNumber1 = $scope.referralContact;
                $scope.LmsModel.Communication.interactionType = $scope.defaultIntractionType;
                GLI_LmsService.saveTransactions(
                    $scope.onSaveSuccess,
                    $scope.onSaveError,
                    $scope.LmsModel);
            } else {
                $rootScope.lePopupCtrl.showWarning(
                    translateMessages($translate,
                        "lifeEngage"),
                    translateMessages($translate,
                        "enterMandatory"),
                    translateMessages($translate,
                        "lms.ok"));
            }
        } else {
            $rootScope.lePopupCtrl.showError(
                translateMessages($translate,
                    "lifeEngage"),
                translateMessages($translate,
                    "leadSyncMessage"),
                translateMessages($translate,
                    "lms.ok"));

        }
    };

    $scope.getNewFollowUp = function (actualDate, planedDate, type, status, remark) {
        var newFollowUp = {
            "actualDateAndTime": actualDate,
            "planedDateAndTime": planedDate,
            "status": status,
            "remark": remark
        };
        return newFollowUp;
    };

    $scope.popupTabsNav = function (rel, index) {
        if (index != 2) {
            $scope.RestoreParent();
            $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
            var url = "";
            for (nav in $scope.lmsnavigations) {
                if ($scope.lmsnavigations[nav].name == rel) {
                    url = $scope.lmsnavigations[nav].url;
                    break;
                }
            }
            $location.path(url);
            $scope.refresh();
        }
    };
    $scope.callback = function () {
        $rootScope.showHideLoadingImage(false);
    };

    //$scope.GLI_init();

    LEDynamicUI.getUIJson(rootConfig.lmsConfigJson, false, function (lmsConfig) {
        $scope.defaultFieldValues = lmsConfig.DefaultFieldValues[0];
        $scope.subheading = $scope.defaultFieldValues.subheading;
        $scope.defaultDisposition = $scope.defaultFieldValues.status;
        $scope.defaultIntractionType = $scope.defaultFieldValues.intractionType;
        $scope.GLI_init();

    });
    //$scope.Initialize();

    //back tab
    $scope.List = function () {
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
        GLI_LmsService.navigateToListingPage("/lms");
    };


    $scope.CarouselClass = function (cIndex) {
        var retClass = "";
        if (cIndex == 0) {
            retClass = "active";
        }
        return retClass;
    };

    $scope.CarouselButtonClass = function (cIndex) {
        retClass = "";
        if (cIndex == 0) {
            retClass = "active";
        }
        return retClass;
    };
    $scope.nextQuestion = function () {
        /* $timeout(
        		function() {
        			if ($scope.qIndex < 4) {
        				$scope.qIndex = $scope.qIndex + 1;
        			}
        		}, 1000); */
    };
    $scope.prevQuestion = function () {
        /* $timeout(
        		function() {
        			if ($scope.qIndex > 0) {
        				$scope.qIndex = $scope.qIndex - 1;
        			}
        		}, 1000); */
    };
    $scope.getPrevNextClasses = function (arrow) {
        /* var retClass = "";
        switch (arrow) {
        case 'prev': {
        	if ($scope.qIndex == 0) {
        		retClass = 'disable ';
        	}
        	break;
        }
        case 'next': {
        	if ($scope.qIndex == 4) {
        		retClass = 'disable ';
        	}
        	break;
        }

        }
        return retClass; */
    };
    $scope.ListButtonClass = function (Key1) {
        var retClass = "";
        if ((Key1 != "") ||
            (!(rootConfig.isDeviceMobile))) {
            retClass = "disabled";
        }

        return retClass;
    };

    /* 	
    	Edit User > Refferal Tab
    	Auther: Jerry
    */
    $scope.cancelRefPop = function () {
        $scope.hideAddRefPop();
        $scope.LmsModel.Lead.BasicDetails.fullName = "";
        $scope.referralContact = "";
        $scope.LmsModel.Lead.ContactDetails.homeNumber1 = "";

    };

    $scope.showRefPop = false;
    $scope.showAddRefPop = function () {
        $scope.showRefPop = true;
    };
    $scope.hideAddRefPop = function () {
        $scope.errorCount = 0;
        $scope.showRefPop = false;
    };
};