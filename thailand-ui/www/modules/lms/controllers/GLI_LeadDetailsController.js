/*
 * Copyright 2015, LifeEngage
 */
/*
 Name:GLI_LeadDetailsController
 CreatedDate:20/07/2015
 Description:KMC module to implement Generali specific changes.
 */
storeApp
    .controller(
        'GLI_LeadDetailsController', GLI_LeadDetailsController);
GLI_LeadDetailsController.$inject = ['$rootScope',
						'$scope',
						'$compile',
						'DataService',
						'$translate',
						'$route',
						'LmsVariables',
						'LmsService',
						'$location',
						'globalService',
						'$translate',
						'UtilityService',
						'$timeout',
						'GLI_LmsService',
						'DataLookupService',
						'$controller',
						'GLI_DataService',
						'UserDetailsService',
						'IllustratorVariables',
						'GLI_LmsVariables','GLI_DataLookupService'];

function GLI_LeadDetailsController($rootScope, $scope,
    $compile, DataService, $translate, $route,
    LmsVariables, LmsService, $location,
    globalService, $translate, UtilityService,
    $timeout, GLI_LmsService, DataLookupService, $controller,
    GLI_DataService, UserDetailsService, IllustratorVariables, GLI_LmsVariables,GLI_DataLookupService) {
    $controller('LeadDetailsController', {
        $rootScope: $rootScope,
        $scope: $scope,
        $compile: $compile,
        DataService: GLI_DataService,
        $translate: $translate,
        $route: $route,
        LmsVariables: GLI_LmsVariables,
        LmsService: GLI_LmsService,
        DataLookupService: GLI_DataLookupService,
        $location: $location,
        globalService: globalService,
        $translate: $translate,
        UtilityService: UtilityService,
        $timeout: $timeout
    });
	
	
    $scope.disablSource = false;
	$scope.allowFormatLabel=false;
    $scope.fromReset = false;
    $scope.nationalityRequired = false;
    $rootScope.alertObj.showPopup = false;
    $scope.stateArray = [];
    $scope.districtArray = [];
    $scope.wardOrHamletArray = [];
    $rootScope.hideLanguageSetting = false;
    $rootScope.enableRefresh=false;
    $scope.leadDetShowPopUpMsg = false;
     
    $scope.Initialize = function () {
        if ($rootScope.isTabsEnabled == true) {
            enabled_stages = [1, 2, 3];
        }
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
	}    
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5');
	}
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5');
	}
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5');
	}
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5');
	}    
       
        LmsVariables.selectedPage = "Lead Details Page";
        $scope.editMode = LmsVariables.editMode;
        $scope.prevdateTime = "";
        $scope.RMPopupShow = false;
        $scope.agentDetail = UserDetailsService
            .getUserDetailsModel();
        $scope.agentNames = UserDetailsService
            .getReportingAgents();
        $scope.branchNames = UserDetailsService
            .getMappedBranches();
        if (!rootConfig.isDeviceMobile &&
            !rootConfig.isOfflineDesktop) {
            $scope.agentNames = UserDetailsService
                .getReportingAgents();
        } else {
            GLI_DataService
                .retrieveReportingAgentsDetails({
                        "parentAgentCode": $scope.agentDetail.agentCode
                    },
                    $scope.onRetrieveReportingAgentsSucess,
                    $scope.onRetrieveReportingAgentsError);
        }

        if ($scope.editMode) {
            $scope.$parent.showLeadPopup = true;
            $scope.$parent.isCreatePopup = false;
        } else {
            $scope.$parent.showLeadPopup = true;
            $scope.$parent.isCreatePopup = true;
        }

        $scope.LmsModel = LmsVariables.getLmsModel();
		if($scope.LmsModel.Lead.BasicDetails.identityProof=="PP"){
			$scope.passportNum=$scope.LmsModel.Lead.BasicDetails.IDcard;
		}
		else if($scope.LmsModel.Lead.BasicDetails.identityProof=="DL"){
		$scope.driverLicenseNum=$scope.LmsModel.Lead.BasicDetails.IDcard;
		}
		else if($scope.LmsModel.Lead.BasicDetails.identityProof=="GID"){
			$scope.governmentIdNum=$scope.LmsModel.Lead.BasicDetails.IDcard ;
		}
		if($scope.LmsModel.Lead.SeverityDetails.severity == '' && !$scope.editMode)
		{
			$scope.LmsModel.Lead.SeverityDetails.severity ="Warm";
		} 
		if($scope.LmsModel.Lead.BasicDetails.dob){
		$scope.LmsModel.Lead.BasicDetails.dobTh = InternalToExternal($scope.LmsModel.Lead.BasicDetails.dob)
		}
        if ($scope.LmsModel.Lead.RMInfo.RMName != '') {
            $scope.showFeedback = true;
        }
        $scope.sourceArray = []; // =
        $scope.subSourceArray = LmsVariables.subSourceArray;
        $scope.severityArray; // LmsVariables.severityArray;
        UtilityService.InitializeErrorDisplay($scope,
            $rootScope, $translate);

        if (!((rootConfig.isDeviceMobile))) {
            $('.custdatepicker')
                .datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true
                })
                .on(
                    'change',
                    function () {

                        /*$timeout(
                        		function() {
                        			$rootScope
                        					.updateErrorCount('leadDetailsSection');
                        			if (!$scope.editMode) {
                        				if( $scope.LmsModel.Communication.followUps && $scope.LmsModel.Communication.followUps[0]){
                        					var followUpDate = $scope.LmsModel.Communication.followUps[0].followUpsDate;
                        					if ($scope.LmsModel.Communication.followUps[0].followUpsTime != undefined
                        							&& $scope.LmsModel.Communication.followUps[0].followUpsTime != "Invalid Date")
                        						followUpDate = followUpDate
                        								+ " "
                        								+ $scope.LmsModel.Communication.followUps[0].followUpsTime;
                        					if (LEDate(followUpDate) <= LEDate()) {
                        						$scope.errorCount = parseInt($scope.errorCount) + 1;
                        						$scope
                        								.refresh();
                        					}
                        				}
                        			}
                        		}, 10);*/

                    });
        }

        $scope.updateErrorCount = function (arg1, arg2) {
            $rootScope.updateErrorCount(arg1, arg2);
			$scope.validateFields(arg1); 
            /*if (!$scope.editMode) {
            	var followUpDate = $scope.LmsModel.Communication.followUps[0].followUpsDate;
            if (followUpDate != undefined && followUpDate != "Invalid Date" && $scope.LmsModel.Communication.followUps[0].followUpsTime != undefined && $scope.LmsModel.Communication.followUps[0].followUpsTime != "Invalid Date"){
            		followUpDate = followUpDate.toJSON().slice(0,10) +'T'+ $scope.LmsModel.Communication.followUps[0].followUpsTime.toTimeString().slice(0,8)+"Z";
            		if (LEDate(followUpDate) <= LEDate()) {
            		$scope.errorCount = parseInt($scope.errorCount) + 1;
            		$scope.refresh();
            	}
            	}
            }*/
        };

        $scope.showErrorPopup = function (arg1, arg2) {
            $rootScope.showErrorPopup(arg1, arg2);
            if (!$scope.editMode &&
                $scope.LmsModel.Communication.followUps && $scope.LmsModel.Communication.followUps[0].followUpsDate != undefined &&
                $scope.LmsModel.Communication.followUps[0].followUpsTime != undefined) {
                var followUpDate = $scope.LmsModel.Communication.followUps[0].followUpsDate;
                if (followUpDate != undefined && followUpDate != "Invalid Date" && $scope.LmsModel.Communication.followUps[0].followUpsTime != undefined &&
                    $scope.LmsModel.Communication.followUps[0].followUpsTime != "Invalid Date")
                    followUpDate = followUpDate.toJSON().slice(0, 10) + 'T' + $scope.LmsModel.Communication.followUps[0].followUpsTime.toTimeString().slice(0, 8) + "Z";
                if (LEDate(followUpDate) <= LEDate()) {
                    var error = {};
                    error.message = translateMessages(
                        $translate,
                        "lms.meetingDateFuturedate");
                    // meetingDateLesserThanFollowup
                    error.key = 'followUpDate';
                    $scope.errorMessages.push(error);
                }
            }
        };
        /*$timeout(
        		function() {
        			$rootScope
        					.updateErrorCount('leadDetailsSection');
        			if (!$scope.editMode
        					&& $scope.LmsModel.Communication.followUps[0].followUpsDate != undefined
        					&& $scope.LmsModel.Communication.followUps[0].followUpsTime != undefined) {
        				var followUpDate = $scope.LmsModel.Communication.followUps[0].followUpsDate;
        				if ($scope.LmsModel.Communication.followUps[0].followUpsTime != undefined
        						&& $scope.LmsModel.Communication.followUps[0].followUpsTime != "Invalid Date")
        					followUpDate = followUpDate
        							+ " "
        							+ $scope.LmsModel.Communication.followUps[0].followUpsTime;
        				if (LEDate(followUpDate) <= LEDate()) {
        					$scope.errorCount = parseInt($scope.errorCount) + 1;
        					$scope.refresh();
        				}
        			}
        		}, 10);*/
        $scope.mobnumberPattern = rootConfig.mobnumberPattern;
        $scope.leadNamePattern = rootConfig.leadNamePattern;
        $scope.numberPattern = rootConfig.numberPattern;

        $scope.mandatory = true;
        $scope.gliLeadNamePattern = rootConfig.gliLeadNamePattern;
        $scope.nationalIDPattern = rootConfig.nationalIDPattern;
        $scope.zipPattern = rootConfig.zipPattern;
        $scope.occupationPattern = rootConfig.occupationPattern;
        $scope.alphanumericPattern = rootConfig.alphanumericPattern;
		$scope.alphanumericPatterng = rootConfig.alphanumericPatterng;
        $scope.passportEappPattern=rootConfig.passportEappPattern;
        $scope.addressPattern = rootConfig.addressPattern;
        $scope.alphabetspecialPattern = rootConfig.alphabetspecialPattern;
        $scope.nationalityPattern = rootConfig.nationalityPattern;
        $scope.alphabeticPattern = rootConfig.alphabeticPattern;
        $scope.alphabetWithSpacePattern = rootConfig.alphabetWithSpacePattern;
        $scope.referenceContPattern = rootConfig.referenceContPattern;
        $scope.nonNumericalPattern = rootConfig.nonNumericalPattern;
        $scope.emailPattern = rootConfig.emailPattern;
        $scope.numberPatternMob = rootConfig.numberPatternMob;
        $scope.namePattern = rootConfig.namePattern;
        $scope.mobileNumberExtension = 628;
        $scope.contactNo = "";
        $scope.homeNumber = "";
        $scope.errorPopUp = false;
        $scope.lmsSectionId = "leadDetailsSection";
        $scope.svaeOnTabNav = true;
        $scope.showDescriptionOthersForOccupation = false;

        $scope.spaceCode = JSON.parse(JSON
            .stringify(rootConfig.spaceAsciCode));
        $rootScope.selectedPage = "leadDetails";
        $scope.isEditMode = LmsVariables.editMode;
        //set RMinfo
        if ($scope.agentDetail.agentType == "BranchUser") {
            $scope.LmsModel.Lead.RMInfo.RMName = LmsVariables.getRMInfoModel().RMName;
            $scope.LmsModel.Lead.RMInfo.RMNationalID = LmsVariables.getRMInfoModel().RMNationalID;
            $scope.LmsModel.Lead.RMInfo.RMMobileNumber = LmsVariables.getRMInfoModel().RMMobileNumber;
            $scope.LmsModel.Referrals.referredByName = $scope.LmsModel.Lead.RMInfo.RMName;
            $scope.LmsModel.Referrals.referredByContact = $scope.LmsModel.Lead.RMInfo.RMMobileNumber;

        } else {
            if ($scope.LmsModel.Referrals.parentLeadId != "") {
                $scope.isNonEditable = true;
            } else if (($scope.LmsModel.Lead.RMInfo.RMName != "") && ($scope.LmsModel.Lead.RMInfo.RMMobileNumber != "")) {
                $scope.LmsModel.Referrals.referredByName = $scope.LmsModel.Lead.RMInfo.RMName;
                $scope.LmsModel.Referrals.referredByContact = $scope.LmsModel.Lead.RMInfo.RMMobileNumber;
                $scope.isNonEditable = true;
            } else if (($scope.LmsModel.Referrals.referredByName != "") && ($scope.LmsModel.Referrals.referredByContact != "") && ($scope.LmsModel.Referrals.referredByNationalID != "") && ($scope.LmsModel.Referrals.referredByBranchName != "")) {
                $scope.isNonEditable = true;
            } else {
                $scope.isNonEditable = false;

            }

        }
        // function to add Contact Prefix
        /*$scope.addContactPrefix = function(data, isMobile,prePopulate) {
        	if (isMobile) {
        		if (data == "") {
        			var modelValue = $scope.LmsModel.Lead.ContactDetails.mobileNumber1;
        			$scope.contactNo = modelValue
        					.substring(
        							rootConfig.mobileNoPrefix.length,
        							modelValue.length);
        		} else {
        			$scope.LmsModel.Lead.ContactDetails.mobileNumber1 = rootConfig.mobileNoPrefix
        					+ data;
        		}
        	} else {
        		if(data=="" && $scope.LmsModel.Lead.ContactDetails.homeNumber1 != "" && prePopulate){

        			var modelValue = $scope.LmsModel.Lead.ContactDetails.homeNumber1;
        			$scope.homeNumber = modelValue
        					.substring(
        							rootConfig.mobileNoPrefix.length,
        							modelValue.length);

        		} else {
        			$scope.LmsModel.Lead.ContactDetails.homeNumber1 = rootConfig.mobileNoPrefix
        			+ data;
        		}
        	}
        }*/
        if (!$scope.editMode) {
            $scope.LmsModel.Lead.Dependents.hasDependent = "No";
            // $scope.LmsModel.Lead.ContactDetails.mobileNumber1
            // = 628;
        }
        //$scope.addContactPrefix($scope.contactNo, true,true);
        //$scope.addContactPrefix($scope.homeNumber, false,true);

        $scope.popupTabs = ['', '', '', ''];
        $scope.popupTabs[0] = 'active';

        LEDynamicUI
            .getUIJson(
                rootConfig.lmsConfigJson,
                false,
                function (lmsConfig) {
                    LmsVariables.occupation = lmsConfig.occupation;
                    $scope.occupation = LmsVariables.occupation;
                    $scope.countryLists = lmsConfig.countryListslms;
                    $scope.cityList = lmsConfig.cityListslms;
                    if (!$scope.editMode &&
                        $scope.LmsModel.Lead.BasicDetails.nationality == "") {
                        $scope.LmsModel.Lead.BasicDetails.nationality = lmsConfig.DefaultFieldValues[0].nationality;
                    }
                    if (!$scope.editMode &&
                        $scope.LmsModel.Lead.ContactDetails.birthAddress.country == "") {
                        $scope.LmsModel.Lead.ContactDetails.birthAddress.country = lmsConfig.DefaultFieldValues[0].countyOfBirth;
                    }
                });


        if ($scope.agentDetail.agentType == "BranchUser") {
            $scope.stages = [
                {
                    ID: 1,
                    stageTitle: "Personal Details",
											},
                {
                    ID: 2,
                    stageTitle: "Other Details",
											}
										];
        } else {
            $scope.stages = [
                {
                    ID: 1,
                    stageTitle: "Personal Details",
											},
                {
                    ID: 2,
                    stageTitle: "Other Details",
											},
                {
                    ID: 3,
                    stageTitle: "Referrer Details",
											}
										];
        }

//        $scope.showDescriptionIfOtherOccupation($scope.LmsModel.Lead.OccupationDetails.occupationCategory);
        $rootScope.showHideLoadingImage(true,
            'paintUIMessage', $translate);

        if ($scope.agentDetail.agentType == "BranchUser" && LmsVariables.isFromLeadListing == false) {
            if ($scope.agentNames) {
                if ($scope.agentNames.length == 0) {
                    // $rootScope.lePopupCtrl.showError(
                        // translateMessages($translate,
                            // "lifeEngage"),
                        // translateMessages($translate, "lms.cannotCreateNewLead"),
                        // translateMessages($translate,
                            // "lms.ok"));
					$scope.leadDetShowPopUpMsg = true;
					$scope.enterMandatory = translateMessages($translate, "lms.cannotCreateNewLead");
                } else {
                    $scope.RMPopupShow = true;

                    LEDynamicUI
                        .paintUI(
                            rootConfig.template,
                            "leadListingWithTabs.json",
                            "leadCreatorsDetailsView",
                            "#popupContent",
                            true,
                            function () {
                                $rootScope.showHideLoadingImage(false);
                                $scope.refresh();
                            }, $scope, $compile);
                }
            } else {
                //$scope.RMPopupShow = false;

                // $rootScope.lePopupCtrl.showError(
                    // translateMessages($translate,
                        // "lifeEngage"),
                    // translateMessages($translate, "lms.cannotCreateNewLead"),
                    // translateMessages($translate,
                        // "lms.ok"));
				$scope.leadDetShowPopUpMsg = true;
				$scope.enterMandatory = translateMessages($translate, "lms.cannotCreateNewLead");
                $rootScope.showHideLoadingImage(false);
                $scope.cancelPopup();
            }


        } else {
            LmsVariables.isFromLeadListing = false;
            $rootScope.showHideLoadingImage(true,
                'paintUIMessage', $translate);
            initiatePopulation();
        }

        LEDynamicUI
            .getUIJson(
                rootConfig.lmsConfigJson,
                false,
                function (lmsConfig) {
                    $scope.lmsnavigations = lmsConfig.lmsnavigations;

                });

        if ($scope.LmsModel.Lead.SourceDetails.source == 'Referral From Branch') {
            $scope.disablSource = true;
        }
        
        initialMobileValidationError();
    };

    $scope.setValuetoScope = function (model, value) {
        var _lastIndex = model.lastIndexOf('.');
        if (_lastIndex > 0) {
            var parentModel = model.substring(0, _lastIndex);
            var scopeVar = $scope.getValue(parentModel);
            var remain_parentModel = model.substring(_lastIndex + 1, model.length);
            scopeVar[remain_parentModel] = value;
        } else {
            $scope[model] = value;
        }
    }

    $scope.getValue = function (model) {
        if ($scope.isVariableExists(model)) {
            return $scope.evaluateString(model);
        }
    }

    $scope.isVariableExists = function (variable) {
        var modelData = variable;
        variable = variable.split('.');
        var obj = $scope[variable.shift()];
        for (a in variable) {
            if (variable[a].indexOf('[') >= 0) {
                var length = variable[a].indexOf('[');
                if (variable[a].indexOf(']') == (length + 2)) {
                    var flag = true;
                    break;
                }
            }
        }
        while (obj && variable.length)
            obj = obj[variable.shift()];

        if (flag == true)
            return modelData;
        else
            return obj;
    }
    
    $scope.GLI_Save = function () {
                    LmsVariables.totalAPE = 0;
                    if ($scope.LmsModel.Policies.productSold && $scope.LmsModel.Policies.productSold.length > 0 && $scope.LmsModel.Policies.productSold[0].premium != "") {
                        for (var indx = 0; indx < $scope.LmsModel.Policies.productSold.length; indx++) {
                            LmsVariables.totalAPE = Number(LmsVariables.totalAPE) + Number($scope.LmsModel.Policies.productSold[indx].premium);
                        }
                    }
					if($scope.LmsModel.Lead.BasicDetails.dob=="NaN-NaN-NaN" || angular.isUndefined($scope.LmsModel.Lead.BasicDetails.dob) || $scope.LmsModel.Lead.BasicDetails.dob==""){
                        $scope.LmsModel.Lead.BasicDetails.dobTh="";
                        $scope.LmsModel.Lead.BasicDetails.dob="";
                    }

                    $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
                    $scope.validateFields('leadDetailsSection');

                    LmsVariables.setLmsModel($scope.LmsModel);
                    if ($scope.errorCount > 0) {
                        $scope.popupFlag = true;
                        $rootScope.showHideLoadingImage(false);
                        // $scope.lePopupCtrl.showWarning(
                            // translateMessages($translate,
                                // "errorHeader"),
                            // translateMessages($translate,
                                // "enterMandatory"),
                            // translateMessages($translate,
                                // "lms.ok"));
						$scope.leadDetShowPopUpMsg = true;
						$scope.enterMandatory = translateMessages($translate, "enterMandatory");
                    } else {
                        $scope.GLI_errorCheck();
                        GLI_LmsService.saveTransactions($scope.onSaveSuccess, $scope.onSaveError,
                            $scope.LmsModel);
                    }
    };
    
    $scope.onRetrieveReportingAgentsSucess = function (data) {
        $scope.agentNames = data;
    };
    $scope.onRetrieveReportingAgentsError = function (data) {
       
    };

    $scope.initialPaintUI = function () {
        $scope.RMPopupShow = false;
        LEDynamicUI.paintUI(rootConfig.template,
            "leadListingWithTabs.json",
            "leadDetailsSection", "#popupContent",
            true,
            function () {
                $scope.callback();
            }, $scope, $compile);
    };

    $scope.cancelPopup = function () {

        $scope.RMPopupShow = false;
        if ($scope.LmsModel.Lead.RMInfo.RMMobileNumber) {
            $scope.LmsModel.Lead.RMInfo.RMMobileNumber = "";
        }
        if ($scope.LmsModel.Lead.RMInfo.RMName) {
            $scope.LmsModel.Lead.RMInfo.RMName = "";
        }
        if ($scope.LmsModel.Lead.RMInfo.RMNationalID) {
            $scope.LmsModel.Lead.RMInfo.RMNationalID = "";
        }
        LmsVariables.setLmsModel($scope.LmsModel);
        $location.path('/landingPage');
    };

    $scope.validateCreatorDetails = function () {
        if ($("#creatorDetails input.ng-invalid").length <= 0) {
            return true;
        } else {
            $("#creatorDetails input.ng-invalid").addClass("highlight");
            return false;
        }
    };

    $scope.proceedToCreateLead = function () {

        if ($scope.validateCreatorDetails()) {
            LmsVariables.setLmsModel($scope.LmsModel);
            LmsVariables.setRMInfoModel($scope.LmsModel.Lead.RMInfo);
            $rootScope.showHideLoadingImage(true,
                'paintUIMessage', $translate);
            initiatePopulation();
        }
    };
    $scope.getLookUpDataFromCode = function (data, value) {
        return data.filter(function (data) {
            return angular.lowercase(data.code) == angular.lowercase(value);
        });
    };
    $scope.setCategoryCode = function (model, occupationArray) {
        if (model) {
            var _lastIndex = model.lastIndexOf('.');
            if (_lastIndex > 0) {
                var parentModel = model.substring(0, _lastIndex);
                var value = $scope.evaluateString(model);
                var scopeVar = $scope.evaluateString(parentModel);
                if (occupationArray) {
                    var value = $scope.getLookUpDataFromCode(occupationArray, value);
                    if (value && value != "" && value.length > 0) {
                        scopeVar['occupationCategoryValue'] = value[0].value;
                    }
                }

            }
        }
    };
    $scope.evaluateString = function (value) {

        var val;
        var variableValue = $scope;
        val = value.split('.');
        if (value.indexOf('$scope') != -1) {
            val = val.shift();
        }
        for (var i in val) {
            variableValue = variableValue[val[i]];
        }
        return variableValue;
    };

    // implementing GLI_LmsService
    $scope.Save = function () {
        $rootScope.showHideLoadingImage(true,
            "paintUIMessage", $translate);
        LmsVariables.setLmsModel($scope.LmsModel);
        if ($scope.errorCount == 0) {
            IllustratorVariables.leadEmailId = $scope.LmsModel.Lead.ContactDetails.emailId;
            if ($scope.editMode) {
                GLI_LmsService.saveTransactions(
                    $scope.onSaveSuccess,
                    $scope.onSaveError,
                    $scope.LmsModel);
            } else {
                $location.path("/lms/contactlog");
                $scope.refresh();
            }
        } else {

            $rootScope.showHideLoadingImage(false);
            // $scope.lePopupCtrl.showWarning(
                // translateMessages($translate,
                    // "errorHeader"),
                // translateMessages($translate,
                    // "enterMandatory"),
                // translateMessages($translate,
                    // "lms.ok"));
			$scope.leadDetShowPopUpMsg = true;
			$scope.enterMandatory = translateMessages($translate, "enterMandatory");
        }
        $rootScope.showHideLoadingImage(false);
    }

    $scope.onSaveError = function (msg) {
        $rootScope.showHideLoadingImage(false);
        $scope.refresh();
        // /TODO
        // /Show NotifyMessage
        // /
        if (msg == "This Trasaction Data is already processed : " || msg == "Duplicate Lead : ") {
            // $rootScope.lePopupCtrl.showError(
                // translateMessages($translate,
                    // "errorHeader"),
                // translateMessages($translate,
                    // "leadAlreadyExists"),
                // translateMessages($translate,
                    // "lms.ok"));
			$scope.leadDetShowPopUpMsg = true;
			$scope.enterMandatory = translateMessages($translate, "leadAlreadyExists");
        } else {
            // $rootScope.lePopupCtrl.showError(
                // translateMessages($translate,
                    // "errorHeader"),
                // translateMessages($translate,
                    // "saveFailure"),
                // translateMessages($translate,
                    // "lms.ok"));
			$scope.leadDetShowPopUpMsg = true;
			$scope.enterMandatory = translateMessages($translate, "saveFailure");
        }
    };
    /*
     * $scope.saveClientProfile = function() {
     *
     * if
     * ($scope.LmsModel.Lead.BasicDetails.identityProof != "" ||
     * $scope.LmsModel.Lead.BasicDetails.identityProof !=
     * undefined ||
     * $scope.LmsModel.Lead.BasicDetails.identityProof !=
     * null) {
     * $scope.validateFields('leadDetailsSection'); }
     * //$rootScope.showHideLoadingImage(false);
     * $rootScope.showHideLoadingImage(true,
     * 'paintUIMessage', $translate);
     *
     * if ($scope.errorCount == 0) { $scope.errorPopUp =
     * true; LmsVariables.setLmsModel($scope.LmsModel);
     * IllustratorVariables.leadEmailId =
     * $scope.LmsModel.Lead.ContactDetails.emailId;
     * GLI_LmsService .saveTransactions(
     * $scope.saveClientProfileSuccess,
     * $scope.onSaveError, $scope.LmsModel); } else {
     * $scope.errorPopUp = false;
     * $rootScope.showHideLoadingImage(false);
     * $scope.lePopupCtrl.showWarning(
     * translateMessages($translate, "lifeEngage"),
     * translateMessages($translate, "enterMandatory"),
     * translateMessages($translate, "fna.ok")); } }
     * $scope.saveClientProfileSuccess = function() {
     * $rootScope.showHideLoadingImage(false);
     * $location.path("/lms/contactlog");
     * $scope.refresh(); }
     */

    // to do- change code - for build purpose only
    $scope.saveClientProfileTab = function (
        successcallBack) {
        if ($scope.LmsModel.Lead.BasicDetails.identityProof != "" ||
            $scope.LmsModel.Lead.BasicDetails.identityProof != undefined ||
            $scope.LmsModel.Lead.BasicDetails.identityProof != null) {
            $scope.validateFields('leadDetailsSection');
        }
        $rootScope.showHideLoadingImage(true,
            'paintUIMessage', $translate);

        $timeout(function () {
            if ($scope.errorCount == 0) {
                $scope.errorPopUp = true;
                LmsVariables.setLmsModel($scope.LmsModel);
                IllustratorVariables.leadEmailId = $scope.LmsModel.Lead.ContactDetails.emailId;
                if ($scope.svaeOnTabNav) {
                    $scope.svaeOnTabNav = false;
					
					for(i in $scope.LmsModel.Communication){
				for(j in $scope.LmsModel.Communication[i].followUps){
					var date=$scope.LmsModel.Communication[i].followUps[j].metDate;
				
					if(date.indexOf('/')>0){
					var dateArr=date.split('/');
					$scope.LmsModel.Communication[i].followUps[j].metDate=dateArr[2] +'-'+ dateArr[1]+ '-' +dateArr[0];
					}
				}
			}
			for(i in $scope.LmsModel.Communication){
				for(j in $scope.LmsModel.Communication[i].followUps){
					var date=$scope.LmsModel.Communication[i].followUps[j].metDate;
				
						if(date.indexOf('-')>0){
						var dateArr=date.split('-');
						var modelYear=dateArr[0];
						var currDate=new Date();
						var currYear=currDate.getFullYear();
						if($scope.LmsModel.Communication[i].interactionType=="Call" || $scope.LmsModel.Communication[i].interactionType=="Visit"){
							if(modelYear>currYear){
								var englishYear=Number(modelYear)-543;
								$scope.LmsModel.Communication[i].followUps[j].metDate=englishYear+ '-' +dateArr[1]+ '-' +dateArr[2];
						}
						
					
					}
					
					
					else if($scope.LmsModel.Communication[i].interactionType=="Appointment"){
						var thaiYear=Number(currYear)+543;
							if(modelYear>=thaiYear){
								var englishYear=Number(modelYear)-543;
								$scope.LmsModel.Communication[i].followUps[j].metDate=englishYear+ '-' +dateArr[1]+ '-' +dateArr[2];
						}
						
					
					}
					}
				}
			}
                    GLI_LmsService.saveTransactions(
                        function (data) {
                            $scope.onSaveSuccess(data);
                            $scope.errorPopUp = true;
                            successcallBack("success");
                        }, $scope.onSaveError,
                        $scope.LmsModel);
                } else {
                    successcallBack("success");
                }

            } else {
                $scope.errorPopUp = false;
                $rootScope.showHideLoadingImage(false);
            }
        }, 100);
    }

    $scope.onSaveSuccess = function (data) {
        $rootScope.showHideLoadingImage(false);
        //Not needed for vietnam project
        //Adding Calendar event for Follow up Date and time
        /*if( eval(rootConfig.isDeviceMobile) && !eval(rootConfig.isOfflineDesktop)) {
        if($scope.LmsModel.Communication.followUps.length>0){
        	if(LEDate($scope.LmsModel.Communication.followUps[0].dateAndTime) - $scope.prevdateTime != 0){// Add calendar event only if date or time changed
        			var startDate = LEDate($scope.LmsModel.Communication.followUps[0].dateAndTime);
        			var endDate =LEDate($scope.LmsModel.Communication.followUps[0].dateAndTime);
        			endDate.setHours ( startDate.getHours() + parseInt(rootConfig.leadMeetingTimeInHr) );
        			var title = translateMessages($translate,"lms.eventTitle")+$scope.LmsModel.Lead.BasicDetails.fullName;
        			var location = "";
        			var notes =  translateMessages($translate,"lms.eventNote");
        			var reminderTimeInMin = rootConfig.reminderTimeInMin;
        			var success = function(message) {
        			};
        			var error = function(message) {
        			};

        			window.plugins.calendar.createEvent(title,location,notes,startDate,endDate,reminderTimeInMin,success,error);
        			$scope.prevdateTime = LEDate($scope.LmsModel.Communication.followUps[0].dateAndTime);
        	}
        }
        }*/
        $scope.refresh();
        // /TODO
        // /Show NotifyMessage
        // /Proceed to next page
        // /
        if($scope.editMode){
			$rootScope.NotifyMessages(false, "Success",
				$translate);
			// $rootScope.lePopupCtrl
				// .showSuccess(translateMessages(
						// $translate, "lifeEngage"),
					// translateMessages($translate,
						// "leadSavedEditMode"),
					// translateMessages($translate,
						// "lms.ok"));
			$scope.leadDetShowPopUpMsg = true;
            $scope.enterMandatory = translateMessages($translate, "leadSavedEditMode");
		} else {
			$rootScope.NotifyMessages(false, "Success",
				$translate);
			// $rootScope.lePopupCtrl
				// .showSuccess(translateMessages(
						// $translate, "lifeEngage"),
					// translateMessages($translate,
						// "leadSaved"),
					// translateMessages($translate,
						// "lms.ok"));
			$scope.leadDetShowPopUpMsg = true;
            $scope.enterMandatory = translateMessages($translate, "leadSaved");
		}
    };

    $scope.onSaveErrorTab = function (msg) {
        $rootScope.showHideLoadingImage(false);
        $scope.refresh();
        if (msg == "This Trasaction Data is already processed : ") {
            $scope.errorPopUp = false;
            // $rootScope.lePopupCtrl.showError(
                // translateMessages($translate,
                    // "errorHeader"),
                // translateMessages($translate,
                    // "leadAlreadyExists"),
                // translateMessages($translate,
                    // "lms.ok"));
			$scope.leadDetShowPopUpMsg = true;
            $scope.enterMandatory = translateMessages($translate, "leadAlreadyExists");
        } else {
            $scope.errorPopUp = true;
            // $rootScope.lePopupCtrl.showError(
                // translateMessages($translate,
                    // "errorHeader"),
                // translateMessages($translate,
                    // "failure"),
                // translateMessages($translate,
                    // "lms.ok"));
			$scope.leadDetShowPopUpMsg = true;
            $scope.enterMandatory = translateMessages($translate, "failure");
        }
    };

    $scope.validateSmartSearch = function (value, list) {
        if ((list != undefined && value != undefined) &&
            (list != undefined && value != "")) {
            for (var i = 0; i < list.length; i++) {
                if (value == list[i].occupationName) {

                    return list[i];
                }
            }
        }
        return false;
    }

    $scope.validateCountrySearch = function (value, list) {
        if ((list != undefined && value != undefined) &&
            (list != undefined && value != "")) {
            for (var i = 0; i < list.length; i++) {
                if (value == list[i].longDesc) {
                    return list[i];
                }
            }
        }
        return false;
    }

    // Calculating age in years, month , days
    $scope.ageCalculation = function (currDate, dobDate) {
        date1 = new Date(currDate);
        date2 = new Date(dobDate);
        var y1 = date1.getFullYear(),
            m1 = date1
            .getMonth(),
            d1 = date1.getDate(),
            y2 = date2
            .getFullYear(),
            m2 = date2.getMonth(),
            d2 = date2
            .getDate();

        var nextDOB;
        if (m1 < m2) {
            nextDOB = new Date(date2.setYear(date1
                .getFullYear()));
        } else if ((m1 == m2) && (d1 < d2)) {
            nextDOB = new Date(date2.setYear(date1
                .getFullYear()));
        } else {
            nextDOB = new Date(date2.setYear(date1
                .getFullYear() + 1));
        }
        var marginDate = new Date(date1.setMonth(date1
            .getMonth() + 6));

        if (m1 < m2) {
            y1--;
            m1 += 12;
        }

        if (nextDOB.getTime() > marginDate.getTime()) {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() -
                birthDate.getFullYear();
            var mnth = today.getMonth() -
                birthDate.getMonth();
            if (mnth < 0 ||
                (mnth === 0 && today.getDate() < birthDate
                    .getDate())) {
                ageVal--;
            }
            return [ageVal, "false"];
        } else {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() -
                birthDate.getFullYear();
            var mnth = today.getMonth() -
                birthDate.getMonth();
            if (mnth < 0 ||
                (mnth === 0 && today.getDate() < birthDate
                    .getDate())) {
                ageVal--;
            }
            return [ageVal, "true"];
        }

    }

    // Age Calculation
    $scope.calculateAge = function (dob, id) {
        if (dob != "" && dob != undefined &&
            dob != null) {
            var dobDate = new Date(dob);
            /*
             * In I.E it will accept date format in '/'
             * separator only
             */
            if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
                dobDate = new Date(dob.split("-").join(
                    "/"));
            }
            var todayDate = new Date();
            if (formatForDateControl(dob) >= formatForDateControl(new Date())) {
                $scope.LmsModel.Lead.BasicDetails.age = 0;
                //$scope.LmsModel.Lead.BasicDetails.dob = formatForDateControlddmmyyyy(new Date());
            } else {
                var curd = new Date(todayDate
                    .getFullYear(), todayDate
                    .getMonth(), todayDate
                    .getDate());
                var cald = new Date(dobDate
                    .getFullYear(), dobDate
                    .getMonth(), dobDate.getDate());
                var dife = $scope.ageCalculation(curd,
                    cald);
                var age = "";
                if (age < 1) {
                    var datediff = curd.getTime() -
                        cald.getTime();
                    var days = (datediff / (24 * 60 * 60 * 1000)) / 365;
                    days = Number(Math.round(days +
                            'e3') +
                        'e-3');
                    age = 0;
                }
                if (dife[1] == "false") {
                    age = dife[0];
                } else {
                    age = dife[0] + 1;
                }

                if (dob == undefined || dob == "") {
                    age = "-";
                }
                $scope.LmsModel.Lead.BasicDetails.age = age;
            }
        } else {
            $scope.LmsModel.Lead.BasicDetails.age = "";
        }
    };
    
    $scope.updateDate=function(){
		$scope.calculateAge($scope.LmsModel.Lead.BasicDetails.dobTh, 'clientDOB');
 
		$scope.refresh();
	}

    // To clear dependent data if hasdepentend is No
    $scope.clearDependentData = function () {
        if ($scope.LmsModel.Lead.Dependents.hasDependent == 'No') {
            $scope.LmsModel.Lead.Dependents.hasSpouse = '';
            $scope.LmsModel.Lead.Dependents.spouseEstIncome = '';
            $scope.LmsModel.Lead.Dependents.noOfChildren = '';
            $scope.LmsModel.Lead.Dependents.hasParent = '';
        } else if ($scope.LmsModel.Lead.Dependents.hasSpouse == 'No') {
            $scope.LmsModel.Lead.Dependents.spouseEstIncome = '';
        }
    }

    $scope.cancelfunction = function () {
        $location.path('/lms');
        $scope.refresh();
    };
    $scope.rm_Save = function () {
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
        $scope.updateErrorCount('leadDetailsSection');

        LmsVariables.setLmsModel($scope.LmsModel);
        if ($scope.errorCount > 0) {
            $scope.popupFlag = true;
            $rootScope.showHideLoadingImage(false);
            // $scope.lePopupCtrl.showWarning(
                // translateMessages($translate,
                    // "errorHeader"),
                // translateMessages($translate,
                    // "enterMandatory"),
                // translateMessages($translate,
                    // "lms.ok"));
			$scope.leadDetShowPopUpMsg = true;
			$scope.enterMandatory = translateMessages($translate, "enterMandatory");
        } else {
            $scope.GLI_errorCheck();

            GLI_LmsService.saveTransactions($scope.onRMSaveSuccess, $scope.onRMSaveError,
                $scope.LmsModel);
        }
    };

    $scope.GLI_errorCheck = function () {
        var planedMeetingDate = "";
        var planedMeetingTime = "";
        if ($scope.planedMeetingDate && $scope.planedMeetingTime) {
            if (typeof $scope.planedMeetingTime == "object") {
                planedMeetingTime = formatForTimeControl($scope.planedMeetingTime, false);
            } else {
                planedMeetingTime = $scope.formatTime($scope.planedMeetingTime);
            }
            planedMeetingDate = formatForDateControl($scope.planedMeetingDate);
        }
        if (planedMeetingTime && planedMeetingTime) {
            $scope.LmsModel.Communication.metDateAndTime = planedMeetingDate + " " + planedMeetingTime;
        }
        if ($scope.remarks) {
            $scope.LmsModel.Communication.remarks = $scope.remarks;
        }
    };


    $scope.onRMSaveError = function (msg) {
        $rootScope.showHideLoadingImage(false);
        $scope.refresh();
        if (msg == "This Trasaction Data is already processed : " || msg == "Duplicate Lead : ") {
            // $rootScope.lePopupCtrl.showError(
                // translateMessages($translate,
                    // "errorHeader"),
                // translateMessages($translate,
                    // "leadAlreadyExists"),
                // translateMessages($translate,
                    // "lms.ok"));
			$scope.leadDetShowPopUpMsg = true;
			$scope.enterMandatory = translateMessages($translate, "leadAlreadyExists");
        } else {
            // $rootScope.lePopupCtrl.showError(
                // translateMessages($translate,
                    // "errorHeader"),
                // translateMessages($translate,
                    // "failure"),
                // translateMessages($translate,
                    // "lms.ok"));
			$scope.leadDetShowPopUpMsg = true;
			$scope.enterMandatory = translateMessages($translate, "failure");
        }
    };

    $scope.onRMSaveSuccess = function () {
        if (rootConfig.isDeviceMobile == true) {
            $rootScope.showHideLoadingImage(false);
            $rootScope.lePopupCtrl.showError(
                translateMessages($translate,
                    "successHeader"),
                translateMessages($translate,
                    "leadCreationSuccMsg"),
                translateMessages($translate,
                    "printButtonClose"),
                $scope.closeFunction,
                function () {
                    $rootScope
                        .showHideLoadingImage(false);
                });
        } else {
            $rootScope.showHideLoadingImage(false);
            $rootScope.lePopupCtrl.showError(
                translateMessages($translate,
                    "successHeader"),
                translateMessages($translate,
                    "leadSaved"),
                translateMessages($translate,
                    "printButtonClose"),
                $scope.closeFunction,
                function () {
                    $rootScope
                        .showHideLoadingImage(false);
                });
        }
    }

    $scope.closeFunction = function () {
        $location.path('/lms');
        $scope.refresh();
    };
	$scope.closePopup = function () {
            $scope.leadDetShowPopUpMsg = false;
        };

    $scope.nextButtonSave = function () {
        var splitWard=[];
        var optionValue=$scope.LmsModel.Lead.ContactDetails.currentAddress.wardOrHamletValue;
        if(optionValue!=="" && !angular.isUndefined(optionValue)){
            if(optionValue.indexOf(',')>0){
       	    optionValue=optionValue.substring(0,optionValue.length-1);
       	    splitWard=optionValue.split(',');
            $scope.LmsModel.Lead.ContactDetails.currentAddress.wardOrHamletValue = splitWard[0];
            }   
        }
        $rootScope.showHideLoadingImage(true,
            'paintUIMessage', $translate);
        if($scope.errorCount > 0){
            $rootScope.showHideLoadingImage(false);
	        // $scope.lePopupCtrl.showWarning(
	            // translateMessages($translate,
	            // "errorHeader"),
	            // translateMessages($translate,
	            // "enterMandatory"),
	            // translateMessages($translate,
	            // "lms.ok"));
				$scope.leadDetShowPopUpMsg = true;
                $scope.enterMandatory = translateMessages($translate, "enterMandatory");
        }
        if ($scope.editMode) {
            $scope.saveClientProfileTab(function (
                success) {
                $rootScope.showHideLoadingImage(false);
                $scope.goNextStage();
                $scope.refresh();
            });
        } else {
            $scope.svaeOnTabNav = false;
            $scope.saveClientProfileTab(function (
                success) {
                $rootScope.showHideLoadingImage(false);
                $scope.goNextStage();
                $rootScope.isTabsEnabled = true;
                $scope.refresh();
            });
        }
    };

    $scope.popupTabsNav = function (rel, index) {
        if (index != 0) {
            if (!$scope.editMode) {
                $scope.svaeOnTabNav = false;
            } else {
                $scope.svaeOnTabNav = true;
            }
            $scope
                .saveClientProfileTab(function (
                    success) {
                    //if ($scope.errorPopUp && $rootScope.isTabsEnabled == true) {
						if ($scope.errorPopUp){
                        $rootScope
                            .showHideLoadingImage(
                                true,
                                'paintUIMessage',
                                $translate);
                        $("#popupContent").empty();
                        $scope.errorPopUp = false;
                        var url = "";

                        for (nav in $scope.lmsnavigations) {
                            if ($scope.lmsnavigations[nav].name == rel) {
                                url = $scope.lmsnavigations[nav].url;
                                break;
                            }
                        }
                        $location.path(url);
                        $scope.refresh();
                    } else {
                        $scope.errorPopUp = true;
                        $rootScope
                            .showHideLoadingImage(false);
                        $scope.refresh();
                    }
                });
            
            if($scope.errorCount > 0){
                   $rootScope.showHideLoadingImage(false);
	               // $scope.lePopupCtrl.showWarning(
	                   // translateMessages($translate,
	                   // "errorHeader"),
	                   // translateMessages($translate,
	                   // "enterMandatoryTabnav"),
	                   // translateMessages($translate,
	                   // "lms.ok"));
					 $scope.leadDetShowPopUpMsg = true;
					$scope.enterMandatory = translateMessages($translate, "enterMandatoryTabnav");
            }
        }

    };
    $scope.updateErrorCount = function (arg1) {
        $rootScope.updateErrorCount(arg1);
    };
    $scope.callback = function () {
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		}
        $rootScope.showHideLoadingImage(true, "paintUIMessage", $translate);
        if (LmsVariables.editMode == false) {
            LEDynamicUI.paintUI(rootConfig.template,
                "leadListingWithTabs.json",
                "popupTabsStartbar", "#popupTabs",
                true,
                function () {
                    detailedCallback();
                }, $scope, $compile);

        } else {
            LEDynamicUI.paintUI(rootConfig.template,
                "leadListingWithTabs.json",
                "popupTabsNavbar", "#popupTabs",
                true,
                function () {
                    detailedCallback();
                }, $scope, $compile);

        }
    };

    $scope.populateIndependentLookupDateInScope = function (type, fieldName, model, setDefault, field, currentTab, successCallBack, errorCallBack,cacheable) {
        $scope[fieldName] = [{
            "key": "",
            "value": "loading...."
					               }];
        var options = {};
		if (!cacheable){
						cacheable = false;
					}
        options.type = type;
        DataLookupService.getLookUpData(options, function (data) {
            $scope[fieldName] = data;
            if (setDefault) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isDefault == 1) {
                        if (type == "NATIONALITY") {
                            if ($scope.LmsModel.Lead.BasicDetails.nationality == "") {
                                $scope.setValuetoScope(model, data[i].code);
                                break;
                            } else {
                                $scope.fromReset = false;
                            }
                        } else {
                            $scope.setValuetoScope(model, data[i].value);
                            break;
                        }
                    }
                }
            } else {
                var n = "$scope." + model;
                if (typeof "$scope." + model != "undefined")
                    var m = $scope.evaluateString(model);
                if (m) {
                    $scope.setValuetoScope(model, m);
                }

            }
            if (typeof field != "undefined" && typeof currentTab != "undefined") {
                setTimeout(function () {
                    if (typeof $scope.currentTab != "undefined") {
                        var data_Evaluvated = $scope.currentTab[field];
                        if (typeof data_Evaluvated != "undefined") {
                            dataEvaluvated.$render();
                        }
                    }
                }, 0);
            }
            if (typeof $scope.lmsSectionId != "undefined" && $scope.evaluateString($scope.lmsSectionId) != undefined) {
                var data_Evaluvated = $scope.lmsSectionId[fieldName];
                if (data_Evaluvated) {
                    data_Evaluvated.$render();
                }
            }
            $scope.refresh();
            if(successCallBack && successCallBack!=""){
                successCallBack();
            }
        }, function (errorData) {
            $scope[fieldName] = [];

        },cacheable);
    };

    function initiatePopulation() {
       /* $scope.populateIndependentLookupDateInScope('WARD', 'wardOrHamlet', 'LmsModel.Lead.ContactDetails.currentAddress.ward', false, 'wardOrHamlet', 'leadDetailsSection',$scope.initialPaintUI);
		$scope.initialPaintUI();	IDENTITY DISTRICT  STATE  ZIPCODE*/
		var typeNameList = [{ "type": "WARD", "isCacheable": true },
			{ "type": "SEVERITY", "isCacheable": true },
			{ "type": "SOURCE", "isCacheable": true },
			{ "type": "IDENTITY", "isCacheable": true }];
		GLI_DataLookupService.getMultipleLookUpData(typeNameList, function (lookUpList) {
			$scope.initialPaintUI();
		}, function () {
			console.log("Error in multilookup data call");
			$scope.initialPaintUI();
		});
    }

    function detailedCallback() {
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6');
	}
        $rootScope.showHideLoadingImage(false);
        $rootScope.updateErrorCount('leadDetailsSection');
       // $scope.populateIndependentLookupDateInScope('WARD', 'wardOrHamlet', 'LmsModel.Lead.ContactDetails.currentAddress.ward', false, 'wardOrHamlet', 'leadDetailsSection');
    };

    // back tab
    $scope.List = function () {
        $scope.lePopupCtrl.showWarning(
						translateMessages($translate,
								"lifeEngage"),
						translateMessages($translate,
								"pageNavigationText"),  
						translateMessages($translate,
								"fna.navok"),$scope.okPressed,translateMessages($translate,
								"general.navproceed"),$scope.backToListing);        
    };
    
    $scope.backToListing=function(){
            $rootScope.showHideLoadingImage(true,
                'paintUIMessage', $translate);
            GLI_LmsService.navigateToListingPage("/lms");
    }

    $scope.formatTime = function (time) {
        var timeArray = time.split(":");
        var formatedTime = "";
        for (var i = 0; i < timeArray.length; i++) {
            if (timeArray[i].length == 1) {
                timeArray[i] = "0" + timeArray[i];
            }
            if (i != timeArray.length - 1) {
                formatedTime = formatedTime + timeArray[i] + ":";
            } else {
                formatedTime = formatedTime + timeArray[i];
            }
        }
        return formatedTime;
    };

	$scope.validateCitizenId=function(){
		var idCardSubstr=$scope.LmsModel.Lead.BasicDetails.IDcard.substring(0,12);
		var count=0;
		var len=$scope.LmsModel.Lead.BasicDetails.IDcard.length+1;
		var rem=0;
		var quotient=0;
		var min=0;
		var nDigit=0;
                    
        for(var i=0;i<idCardSubstr.length;i++)
         {
			len=len-1;                            
			count=count+parseInt($scope.LmsModel.Lead.BasicDetails.IDcard.substring(i,i+1),10)*len;
         }
                    
         quotient = Math.floor(count/11);
         rem=count%11;
         min=11-rem;
         nDigit=parseInt($scope.LmsModel.Lead.BasicDetails.IDcard.substring(12,13),10);
                
         if(min>9){
             min=min%10;
         }
         if(nDigit!==min)
         {
            return true;
		 }
    }; 

    // For field validations
    $scope.validateFields = function (data) {
        $scope.dynamicErrorMessages = [];
        $scope.dynamicErrorCount = 0;
        var _errors = [];
        var _errorCount = 0;
        
        if(($scope.LmsModel.Lead.ContactDetails.mobileNumber1 && $scope.LmsModel.Lead.ContactDetails.mobileNumber1.length== 0) && 
                ($scope.LmsModel.Lead.ContactDetails.homeNumber && $scope.LmsModel.Lead.ContactDetails.homeNumber.length== 0))
        {
                _errorCount++;
                var error = {};
                error.message = translateMessages($translate, "lms.leadDetailsSectionmobileNumberRequiredValidationMessage");
                error.key = 'contactNumbernew';
                _errors.push(error);     
        }
                    
        if($scope.LmsModel.Lead.ContactDetails.mobileNumber1==""  && $scope.LmsModel.Lead.ContactDetails.homeNumber==""){
                _errorCount++;
                var error = {};
                error.message = translateMessages($translate, "lms.leadDetailsSectionmobileNumberRequiredValidationMessage");
                error.key = 'mobileNumber';
                _errors.push(error); 
        }
        
         if($scope.LmsModel.Lead.BasicDetails.fullName && $scope.LmsModel.Lead.BasicDetails.fullName.length <3 ){                
                                _errorCount++;
                                var error = {};
                                error.message = translateMessages($translate, "lms.leadDetailsSectionleadFirstNameLengthValidationMessage");
                                error.key = 'leadName';
                                _errors.push(error);                    
		}
                    
        if($scope.LmsModel.Lead.BasicDetails.nickName  && $scope.LmsModel.Lead.BasicDetails.nickName.length <2){       
                                 _errorCount++;
                                var error = {};
                                error.message = translateMessages($translate, "lms.leadDetailsSectionleadNickNameLengthValidationMessage");
                                error.key = 'leadNickName';
                                _errors.push(error);
        }
		if($scope.LmsModel.Lead.BasicDetails.identityProof=="PP"){
			$scope.LmsModel.Lead.BasicDetails.IDcard = $scope.passportNum
		}
		else if($scope.LmsModel.Lead.BasicDetails.identityProof=="DL"){
			$scope.LmsModel.Lead.BasicDetails.IDcard =$scope.driverLicenseNum;
		}
		else if($scope.LmsModel.Lead.BasicDetails.identityProof=="GID"){
			$scope.LmsModel.Lead.BasicDetails.IDcard= $scope.governmentIdNum;
		}
		
		if($scope.LmsModel.Lead.BasicDetails.IDcard && $scope.LmsModel.Lead.BasicDetails.identityProof=='CID' && $scope.validateCitizenId()){
            _errorCount++;
            var error = {};
            error.message = translateMessages($translate, "lms.invalidCitizenId");
            error.key = 'IDcard';
            _errors.push(error);
		} ;

        if ($scope.isActiveStage(2)) {
            var planedMeetingTime = "";
            var planedMeetingDate = "";
            if ($scope.planedMeetingDate && $scope.planedMeetingTime) {
                if (typeof $scope.planedMeetingTime == "object") {
                    planedMeetingTime = formatForTimeControl($scope.planedMeetingTime, false);
                } else {
                    planedMeetingTime = $scope.formatTime($scope.planedMeetingTime);
                }

                planedMeetingDate = formatForDateControl($scope.planedMeetingDate);
            }
            if ($scope.agentDetail.agentType == 'BranchUser') {
                if ($scope.planedMeetingDate != "" && $scope.planedMeetingTime == null && $scope.planedMeetingTime != undefined) {
                    _errorCount++;
                    var error = {};
                    error.message = translateMessages($translate, "lms.callDetailsSectionsubfollowUpTimeRequiredValidationMessage");
                    error.key = 'nextMeetingTime';
                    _errors.push(error);
                } else if (($scope.planedMeetingDate == "" || $scope.planedMeetingDate == undefined) && $scope.planedMeetingTime != null && $scope.planedMeetingTime != undefined && $scope.planedMeetingTime != "") {
                    _errorCount++;
                    var error = {};
                    error.message = translateMessages($translate, "lms.callDetailsSectionsubfollowUpDateRequiredValidationMessage");
                    error.key = 'nextMeetingDate';
                    _errors.push(error);
                } else if (($scope.planedMeetingTime == undefined || $scope.planedMeetingTime == "") && $scope.planedMeetingDate != null && $scope.planedMeetingDate != undefined && $scope.planedMeetingDate != "") {
                    _errorCount++;
                    var error = {};
                    error.message = translateMessages($translate, "lms.callDetailsSectionsubfollowUpTimeRequiredValidationMessage");
                    error.key = 'nextMeetingTime';
                    _errors.push(error);
                }

            }
            if (planedMeetingDate && planedMeetingTime && planedMeetingDate.trim() != "" && planedMeetingTime.trim() != "") {
                var dateWithSec = planedMeetingDate + " " + planedMeetingTime + ":00";
                var arr = dateWithSec.split(/[- :]/),
                    dateFormat = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                var _planedDate = Date.parse(dateFormat);
                var _after90Days = new Date();
                _after90Days.setDate(_after90Days.getDate() + 90);
                _after90Days = Date.parse(_after90Days);

                if (_after90Days < _planedDate) {
                    _errorCount++;
                    var error = {};
                    error.message = translateMessages($translate, "lms.callDetailsSectionmeetingNextMeetingDateErrorValidationMessage");
                    // meetingDateLesserThanFollowup
                    error.key = 'nextMeetingDate';
                    _errors.push(error);
                } else if (_planedDate < Date.parse(new Date())) {
                    _errorCount++;
                    var error = {};
                    error.message = translateMessages($translate, "lms.callDetailsSectionmeetingNextMeetingDateErrorValidationMessage");
                    // meetingDateLesserThanFollowup
                    error.key = 'nextMeetingDate';
                    _errors.push(error);
                }
            }
        }

        // occupation field
        /*var occupationTaken = $scope.LmsModel.Lead.OccupationDetails.occupationCategory;

        if (occupationTaken != ""
        		&& occupationTaken != null
        		&& occupationTaken != undefined) {
        	$scope.spaceCode = 25;
        	_occupation = $scope
        			.validateSmartSearch(
        					$scope.LmsModel.Lead.OccupationDetails.occupationCategory,
        					$scope.occupation);
        	if (!_occupation) {
        		_errorCount++;
        		var error = {};
        		error.message = translateMessages(
        				$translate,
        				"lms.leadDetailsSectionoccupationRequiredValidationMessage");
        		error.key = "occupation";
        		_errors.push(error);
        		eval("$scope.isValidoccupation"
        				+ " = false");
        	} else {
        		$scope.LmsModel.Lead.OccupationDetails.jobCode = _occupation.jobCode;
        		eval("$scope.isValidoccupation"
        				+ " = true");
        	}
        } else {
        	$scope.spaceCode = JSON
        			.parse(JSON
        					.stringify(rootConfig.spaceAsciCode));
        	;
        	$scope.dynamicErrorCount;
        	eval("$scope.isValidoccupation" + " = true");
        }*/


        // nationality field
        //var nationalityTaken = $scope.LmsModel.Lead.BasicDetails.nationality;

        /*if (nationalityTaken != ""
        		&& nationalityTaken != null
        		&& nationalityTaken != undefined) {
        	$scope.spaceCode = 25;
        	_country = $scope
        			.validateCountrySearch(
        					$scope.LmsModel.Lead.BasicDetails.nationality,
        					$scope.countryLists);
        	if (!_country) {
        		_errorCount++;
        		var error = {};
        		error.message = translateMessages(
        				$translate,
        				"lms.leadDetailsSectionnationalityRequiredValidationMessage");
        		error.key = "nationality";
        		_errors.push(error);
        		eval("$scope.isValidnationality"
        				+ " = false");

        	}
        } else {
        	$scope.spaceCode = JSON
        			.parse(JSON
        					.stringify(rootConfig.spaceAsciCode));
        	;
        	$scope.dynamicErrorCount;
        	eval("$scope.isValidnationality"
        			+ " = true");
        }*/

        // country of birth field
        var birthAddressTaken = $scope.LmsModel.Lead.ContactDetails.birthAddress.country;

        if (birthAddressTaken != "" &&
            birthAddressTaken != null &&
            birthAddressTaken != undefined) {
            $scope.spaceCode = 25;
            _birthCountry = $scope
                .validateCountrySearch(
                    $scope.LmsModel.Lead.ContactDetails.birthAddress.country,
                    $scope.countryLists);
            if (!_birthCountry) {
                _errorCount++;
                var error = {};
                error.message = translateMessages(
                    $translate,
                    "lms.leadDetailsSectionCountryofbirthRequiredValidationMessage");
                error.key = "birthAddressCountry";
                _errors.push(error);
                $scope.isValidbirthAddressCountry = false;
            }
        } else {
            $scope.spaceCode = JSON
                .parse(JSON
                    .stringify(rootConfig.spaceAsciCode));;
            $scope.dynamicErrorCount;
            $scope.isValidbirthAddressCountry = true;
        }

        // identity card field
        $scope.idProof = $scope.LmsModel.Lead.BasicDetails.identityProof;
        // identity card expDate field
        if ($scope.LmsModel.Lead.BasicDetails.IdentityExpDate &&
            $scope.LmsModel.Lead.BasicDetails != "") {
            if (formatForDateControl($scope.LmsModel.Lead.BasicDetails.IdentityExpDate) <= formatForDateControl(new Date())) {
                var error = {};
                _errorCount++;
                error.message = translateMessages(
                    $translate,
                    "lms.leadDetailsSectionexpirydatePatternValidationMessage");
                // meetingDateLesserThanFollowup
                error.key = 'expiredDate';
                _errors.push(error);
            }
        }
        // City
        var cityTaken = $scope.LmsModel.Lead.ContactDetails.currentAddress.city;
        $scope.dynamicErrorCount = _errorCount;
        if (!$scope.dynamicErrorMessages) {
            $scope.dynamicErrorMessages = [];
        }
        $scope.dynamicErrorMessages = _errors;
        $rootScope.updateErrorCount(data);

    };
    var onLangChange = $scope
        .$on(
            'lang-changed',
            function (event, args) {

                $scope
                    .validateFields('leadDetailsSection');
            });

    $scope.$on('$destroy', function () {
        onLangChange();
    });

    //to monitor field change to trigger save on tab navigation
    $scope.markAsEdited = function () {
        $scope.svaeOnTabNav = true;
    };
    $scope.showDescriptionIfOtherOccupation = function (model) {
        if (typeof model != "undefined" && model != "") {
            $scope.showDescriptionOthersForOccupation = true;
        } else {
            $scope.showDescriptionOthersForOccupation = false;
        }
    };
    var enabled_stages = [1];
    if ($rootScope.isTabsEnabled == true) {
        enabled_stages = [1, 2, 3];
    }
    var active_stage;
    $scope.stagesCompleted = false;
    $scope.setStageStatus = function () {
        if (active_stage == enabled_stages[(enabled_stages.length - 1)])
            $scope.stagesCompleted = true;
    };
    $scope.isStageEnabled = function (index) {
        if (enabled_stages.indexOf(index) >= 0) {
            return true;
        }
    };
    $scope.isActiveStage = function (index) {
        if (active_stage == null) {
            active_stage = enabled_stages[0];
        }

        if ($(window).width() < 799) {
            if (active_stage == index) {
                return true;
            }
        } else {
            return true;
        }
    };
    $scope.goNextStage = function () {
        if ($scope.errorCount == 0) {
            if (active_stage == null) {
                active_stage = enabled_stages[(enabled_stages.length - 1)];
            }
            var el = Array.prototype.slice.call($('#leadDetailsSection .row-fluid > div.span12:not([class*="ng-hide"]) .trueRequired:not([class*="ng-hide"])'));
            if (el.length <= 0) {
                if ($(window).width() > 799 || $(window).width() < 480) {
                    $rootScope.isTabsEnabled = true;
                    $scope.GLI_Save();
                    //$location.path("/lms/contactlog");
                } else {
                    active_stage++;
                    if (active_stage > $scope.stages.length) {
                        $rootScope.isTabsEnabled = true;
                        $scope.GLI_Save();
                        //$location.path("/lms/contactlog");
                    } else {
                        if (enabled_stages.indexOf(active_stage) < 0) {
                            enabled_stages.push(active_stage);

                            $timeout(function () {
                                $rootScope.updateErrorCount("leadDetailsSection");
                                $scope.refresh();
                            }, 50);
                        }
                    }
                }
            } else {
                $(el).each(function () {
                });
            }
            $scope.validateFields('leadDetailsSection');
        }
    };

    $scope.onSelectTypeHead = function (id) {
        $timeout(function () {
            $('#' + id).blur();
        }, 300);
    }

	$scope.clearField=function(val){
                    
                    if(val=='address' && $scope.editMode && $scope.LmsModel.Lead.ContactDetails.currentAddress.city){						
						$scope.allowFormatLabel=true;
                        $scope.LmsModel.Lead.ContactDetails.currentAddress.city="";
                        $scope.LmsModel.Lead.ContactDetails.currentAddress.state="";
                        $scope.LmsModel.Lead.ContactDetails.currentAddress.zipcode="";                                                $scope.LmsModel.Lead.ContactDetails.currentAddress.cityValue="";
                        $scope.LmsModel.Lead.ContactDetails.currentAddress.stateValue="";
                        $scope.LmsModel.Lead.ContactDetails.currentAddress.wardOrHamletValue="";
					}else if(val=='address' && !$scope.editMode && $scope.LmsModel.Lead.ContactDetails.currentAddress.city){
						$scope.allowFormatLabel=true;
                        $scope.LmsModel.Lead.ContactDetails.currentAddress.city="";
                        $scope.LmsModel.Lead.ContactDetails.currentAddress.state="";
                        $scope.LmsModel.Lead.ContactDetails.currentAddress.zipcode="";
                        $scope.LmsModel.Lead.ContactDetails.currentAddress.cityValue="";
                        $scope.LmsModel.Lead.ContactDetails.currentAddress.stateValue="";
                        $scope.LmsModel.Lead.ContactDetails.currentAddress.wardOrHamletValue="";
                    }else if(val=='id'){
                        $scope.LmsModel.Lead.BasicDetails.IDcard="";
                    }
    };
	$scope.formatLabel = function (model, options, type) {
                    if (options && options.length >= 1) {
                        for (var i = 0; i < options.length; i++) {
                            if (model === options[i].code && ((type !== 'district' && !$scope.editMode) || (type !== 'district' && $scope.editMode && angular.equals(undefined, $scope.LmsModel.Lead.ContactDetails.currentAddress.wardOrHamlet)) || (type !== 'district' && $scope.allowFormatLabel))) {
                                if (type == 'wardOrHamlet') {
                                    var splitWard=[];
                                    var optionValue=options[i].value;
                                    if(optionValue.indexOf(',')>0){
										$rootScope.splitInd=true;
       		   			                 optionValue=optionValue.substring(0,options[i].value.length-1);
       		   			                 splitWard=optionValue.split(',');
                                         $scope.LmsModel.Lead.ContactDetails.currentAddress.wardOrHamletValue = splitWard[0];
                                    }else{
                                    $rootScope.splitInd=false;
                                    $scope.LmsModel.Lead.ContactDetails.currentAddress.wardOrHamletValue = options[i].value;
                                    }
                                    $rootScope.updateAddress=true;                                    
                                }
                                return options[i].value;
                            }
                            if (type == 'district' && ($scope.allowFormatLabel || $rootScope.updateAddress)){
                                $scope.LmsModel.Lead.ContactDetails.currentAddress.cityValue = options[0].value;
                                $scope.LmsModel.Lead.ContactDetails.currentAddress.stateValue = options[1].value;
                                $scope.LmsModel.Lead.ContactDetails.currentAddress.zipcodeValue = options[2].value;
                                $scope.LmsModel.Lead.ContactDetails.currentAddress.city = options[0].code;
                                $scope.LmsModel.Lead.ContactDetails.currentAddress.state = options[1].code;
                                $scope.LmsModel.Lead.ContactDetails.currentAddress.zipcode = options[2].value;
                                $rootScope.updateAddress=false;
                                return options[0].value;
                            }

                            if(type == 'wardOrHamlet' && !$scope.allowFormatLabel && ($rootScope.updateAddress|| $scope.LmsModel.Lead.ContactDetails.currentAddress.city)){
								if($rootScope.splitInd && !$scope.editMode){
                                    $rootScope.splitInd=false;
									return $scope.LmsModel.Lead.ContactDetails.currentAddress.wardOrHamletValue+","+$scope.LmsModel.Lead.ContactDetails.currentAddress.cityValue;
								}else{
									return $scope.LmsModel.Lead.ContactDetails.currentAddress.wardOrHamletValue;
								}
							}else if(type == 'district' && ((!$scope.editMode && !$scope.allowFormatLabel)||($scope.editMode && !$scope.allowFormatLabel)||(!$scope.editMode && $scope.LmsModel.Lead.ContactDetails.currentAddress.ward))){
								return model;
							}
                        }
                    }
                };

    $scope.goPrevStage = function () {
        if (active_stage == null) {
            active_stage = enabled_stages[(enabled_stages.length - 1)];
        }
        if (enabled_stages.indexOf(active_stage) > 0)
            active_stage--;

        $timeout(function () {
            $rootScope.updateErrorCount("leadDetailsSection");
            $scope.refresh();
        }, 50);
    };
    $scope.goToStage = function (index) {
        $rootScope.updateErrorCount("leadDetailsSection");
        if (active_stage > index) {
            active_stage = index;
            $timeout(function () {
                $rootScope.updateErrorCount("leadDetailsSection");
                $scope.refresh();
                $scope.validateFields('leadDetailsSection');
            }, 50);
        } else if ($scope.errorCount == 0) {
            if (enabled_stages.indexOf(index) < 0) {
                return false;
            }
            active_stage = index;
            $timeout(function () {
                $rootScope.updateErrorCount("leadDetailsSection");
                $scope.refresh();
                $scope.validateFields('leadDetailsSection');
            }, 50);
        }
    };

    $scope.checkLanguage = function () {
        if ($translate.use() == 'th_TH') {
            return true;
        } else {
            return false;
        }
    };

    $scope.toggleNavButton = function (navButton) {
        if (active_stage == null) {
            active_stage = enabled_stages[0];
        }

        return false; // Changes done for Mobile web responsiveness

        /*if ($(window).width() > 480) {
            return false;
        } else if ((active_stage == enabled_stages[0]) && (navButton == "prev")) {
            return false;
        } else if (navButton == "prev") {
            return true;
        } else if ((active_stage == $scope.stages[($scope.stages.length - 1)].ID) && (navButton == "next")) {
            return false;
        } else {
            return true;
        }*/
    };

    /* 	To Set Active Stages Ends */
    $scope.onSourceChange = function () {
        if ($scope.LmsModel.Lead.SourceDetails.source != "Referral From Branch") {
            $scope.LmsModel.Referrals.referredByBranchName = "";
            $scope.LmsModel.Referrals.referredByNationalID = "";
        }
    }
    
    var initialMobileValidationError = function(){                                       
        if($scope.LmsModel.Lead.ContactDetails.mobileNumber1==""  && $scope.LmsModel.Lead.ContactDetails.homeNumber==""){
            var _errors=[];
            var error = {};
            error.message = translateMessages($translate, "lms.leadDetailsSectionmobileNumberRequiredValidationMessage");
            _errors.push(error);
            error.key = 'mobileNumber';
            $scope.dynamicErrorCount = 1;
            $scope.dynamicErrorMessages = _errors;
            $rootScope.updateErrorCount('leadDetailsSection');
        }                                                
    }
}