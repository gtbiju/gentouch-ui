/*
 * Copyright 2015, LifeEngage
 */
/*Name:GLI_LeadListingController
 * For implementing GLI specific changes
 */
storeApp.controller('GLI_LeadListingController', GLI_LeadListingController);
GLI_LeadListingController.$inject = ['$rootScope',
						'$scope',
						'$compile',
						'$routeParams',
						'GLI_DataService',
						'LmsVariables',
						'LmsService',
						'IllustratorVariables',
						'PersistenceMapping',
						'DocumentService',
						'$translate',
						'$route',
						'UtilityService',
						'globalService',
						'$location',
						'FnaVariables',
						'$controller',
						'GLI_LmsService',
						'DataLookupService',
						'GLI_IllustratorVariables',
						'GLI_globalService',
						'UserDetailsService',
						'DataService',
						'GLI_LmsVariables',
						'$timeout'];

function GLI_LeadListingController($rootScope, $scope,
    $compile, $routeParams, GLI_DataService,
    LmsVariables, LmsService, IllustratorVariables,
    PersistenceMapping, DocumentService,
    $translate, $route, UtilityService,
    globalService, $location, FnaVariables,
    $controller, GLI_LmsService, DataLookupService,
    GLI_IllustratorVariables, GLI_globalService, UserDetailsService, DataService, GLI_LmsVariables, $timeout) {
    $controller('LeadListingController', {
        $rootScope: $rootScope,
        $scope: $scope,
        $compile: $compile,
        $routeParams: $routeParams,
        DataService: DataService,
        PersistenceMapping: PersistenceMapping,
        DocumentService: DocumentService,
        $translate: $translate,
        $route: $route,
        UtilityService: UtilityService,
        globalService: globalService,
        $location: $location,
        FnaVariables: FnaVariables,
        LmsVariables: GLI_LmsVariables,
        LmsService: GLI_LmsService,
        DataLookupService: DataLookupService,
        IllustratorVariables: GLI_IllustratorVariables
    });
	
	

    LmsVariables.selectedPage = "Listing Page";
    $scope.LmsModel = LmsVariables.getLmsModel();
    $scope.userDetails = UserDetailsService.getUserDetailsModel();
    $scope.RMPopupShow = false;
    $scope.nextActionFilter = "";
    $scope.actionFilter = "";
    $scope.filterPotential = "";
    $scope.statusFilter = "";
    $scope.referenceNameFilter = "";
    $scope.isBankUser = false;
    $scope.status = "";
    $scope.isTableSwiped = false;
    $scope.filterOpen = false;
    $scope.naviagationFlag = false;
    $rootScope.hideLanguageSetting = false;
    $rootScope.enableRefresh=false;
    $scope.appDateFormat = rootConfig.appDateFormat;
    $scope.appDateTimeFormat = rootConfig.appDateTimeFormat;
    if (rootConfig.isDeviceMobile && rootConfig.isOfflineDesktop) {
        $scope.showSyncIcon = false;
    } else {
        $scope.showSyncIcon = true;
    }

    LEDynamicUI
        .getUIJson(
            rootConfig.FnaConfigJson,
            false,
            function (FnaConfigJson) {
                FnaVariables.goals = FnaConfigJson.goals;
                FnaVariables.pages = FnaConfigJson.pages;
                FnaVariables.pageTypes = FnaConfigJson.pageTypes;
                $scope.refresh();
            });

    /*
     * Over writing the function Only a single FNA will
     * be created from a KMC If FNA is complete navigate
     * to product recommendation page else to landing
     * page
     */
    $scope.translateErrorMesgs = function (msg) {
        $scope.translatedMsg = translateMessages(
            $translate, msg);
        return $scope.translatedMsg;
    };
    $scope.overflowClass = false;
    $rootScope.selectedPage = "lms";
    $scope.accordionIndex = 0;
    /*
     * ADDED FOR SHOWING
     * ACCORDION 


     * ARROW FOR OPENED
     * SECTION
     */
    //sorting in priority view
    /*$scope.tblhead1 = false;
    $scope.tblhead2 = false;
    $scope.tblhead3 = false;
    $scope.tblhead4 = false;
    $scope.tblhead5 = false;
    $scope.tblhead6 = false;
    $scope.tblhead7 = false;
    $scope.tblhead8 = false;
    $scope.tblhead9 = false;
    $scope.tblhead10 = false;
    $scope.tblhead11 = false;
    $scope.tblhead12 = false;
    $scope.tblhead13 = false;
    $scope.tblhead14 = false;
    $scope.tblhead15 = false;
    $scope.tblhead16 = false;
    $scope.tblhead17 = false;
    $scope.tblhead18 = false;
    $scope.tblhead19 = false;
    $scope.tblhead20 = false;
    $scope.tblhead21 = false;*/
    for (var i = 1; i <= 21; i++) {
        $scope['tblhead' + i] = false;
    }
    $scope.orderValues = function (key, e) {
        $scope.accordiationList = rootConfig.accordiationList;
        var reverseFlag = false;
        var predicateFlag = -1;
        var approchability = key.split(".");

        for (i = 0; i < $scope.accordiationList.accordiation.length; i++) {
            if (approchability[0] == $scope.accordiationList.accordiation[i].name) {
                predicateFlag = i;
            } else {
                $scope.accordiationList.accordiation[i].predicate = "";
            }
        }

        for (i = 0; i < $scope.accordiationList.accordiation.length; i++) {
            if ($scope.accordiationList.accordiation[i].predicate == approchability[1]) {
                $scope.accordiationList.accordiation[i].reverse = !($scope.accordiationList.accordiation[i].reverse);
                reverseFlag = true;
                break;
            } else {
                $scope.accordiationList.accordiation[i].reverse = "false";
            }
        }
        if (!reverseFlag) {
            /* $scope.tblhead1 = false;
             $scope.tblhead2 = false;
             $scope.tblhead3 = false;
             $scope.tblhead4 = false;
             $scope.tblhead5 = false;
             $scope.tblhead6 = false;
             $scope.tblhead7 = false;
             $scope.tblhead8 = false;
             $scope.tblhead9 = false;
             $scope.tblhead10 = false;
             $scope.tblhead11 = false;
             $scope.tblhead12 = false;
             $scope.tblhead13 = false;
             $scope.tblhead14 = false;
             $scope.tblhead15 = false;
             $scope.tblhead16 = false;
             $scope.tblhead17 = false;
             $scope.tblhead18 = false;
             $scope.tblhead19 = false;
             $scope.tblhead20 = false;
             $scope.tblhead21 = false;*/
            for (var j = 1; j <= 21; j++) {
                $scope['tblhead' + j] = false;
            }
            $scope.accordiationList.accordiation[predicateFlag].reverse = true;
        }
        if (predicateFlag > -1) {
            $scope.accordiationList.accordiation[predicateFlag].predicate = approchability[1];
        }
    };
    $scope.toggleSorter = function (id) {
        var rightValue = !$scope['tblhead' + id];
        $scope['tblhead' + id] = rightValue;
    }

    $scope.populateIndependentLookupDateInScope = function (type, fieldName, model, setDefault, field, currentTab, successCallBack, errorCallBack,cacheable) {
	if (!cacheable){
						cacheable = false;
					}
        $scope[fieldName] = [{
            "key": "",
            "value": "loading ...."
                            					               }];
        var options = {};
        options.type = type;
        DataLookupService.getLookUpData(options, function (data) {
            $scope[fieldName] = data;
            if (setDefault) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isDefault == 1) {
                        if (type == "NATIONALITY") {
                            if ($scope.LmsModel.Lead.BasicDetails.nationality == "" && !$scope.fromReset) {
                                $scope.setValuetoScope(model, data[i].value);
                                break;
                            } else {
                                $scope.fromReset = false;
                            }
                        } else {
                            $scope.setValuetoScope(model, data[i].value);
                            break;
                        }
                    }
                }
            } else {
                var n = "$scope." + model;
                var m = $scope.evaluateString(model);
                if (m) {
                    $scope.setValuetoScope(model, m);
                }

            }
            if (typeof field != "undefined" && typeof $scope.currentTab != "undefined") {
                setTimeout(function () {
                    var data_Evaluvated = $scope.currentTab[field];
                    // var dataEvaluvated=$scope.evaluateString($scope.currentTab+'.'+field);
                    if (typeof data_Evaluvated != "undefined") {
                        data_Evaluvated.$render();
                    }
                }, 0);
            }
            if (typeof $scope.lmsSectionId != "undefined" && $scope.evaluateString(lmsSectionId) != undefined) {
                var data_Evaluvated = $scope.lmsSectionId[fieldName];
                // var data_Evaluvated=$scope.evaluateString($scope.lmsSectionId+'.'+fieldName);
                if (data_Evaluvated) {
                    data_Evaluvated.$render();
                }
            }
            $scope.refresh();
            successCallBack();
        }, function (errorData) {
            $scope[fieldName] = [];

        },cacheable);
    };

    $scope.evaluateString = function (value) {
        var val;
        var variableValue = $scope;
        val = value.split('.');
        if (value.indexOf('$scope') != -1) {
            val = val.shift();
        }
        for (var i in val) {
            variableValue = variableValue[val[i]];
        }
        return variableValue;
    };
   /* $scope.populateIndependentLookupDateInScope('SEVERITY', 'approachability', 'filterPotential', false, 'approachability', 'filterSection', function () {
        $scope.populateSourceLookUp();
    }, function () {

    });*/
    $scope.populateSourceLookUp = function () {
        $scope.populateIndependentLookupDateInScope('SOURCE', 'Source', 'source', false, 'Source', 'filterSection', function () {}, function () {});
    };

    $scope.getLeadDetailsForLead = function (data) {
        LmsService.getLead(function (data) {
            var parties = [];
            var party = new globalService.party();
            var dataOfLead = LmsVariables.getLmsModel().Lead;
            party.ContactDetails.mobileNumber1 = dataOfLead.ContactDetails.mobileNumber1;
            //party.ContactDetails.emailID = dataOfLead.ContactDetails.emailID;
            //party.OccupationDetails= dataOfLead.OccupationDetails.occupationCategory;
            //Changes done by LETeam << change starts here
            party.BasicDetails.IDcard = dataOfLead.BasicDetails.IDcard;
            party.BasicDetails = dataOfLead.BasicDetails;
            party.ContactDetails = dataOfLead.ContactDetails;
            party.OccupationDetails = dataOfLead.OccupationDetails;
            
            
            party.ContactDetails.currentAddress.addressLine1 = dataOfLead.ContactDetails.currentAddress.addressLine1;            
            //Changes done by LETeam << change ends here 
            party.BasicDetails.firstName = dataOfLead.BasicDetails.fullName;
            party.ContactDetails.homeNumber1 = dataOfLead.ContactDetails.homeNumber1; // data.Key4;
            party.ContactDetails.emailId = data.Key20;
            party.BasicDetails.lastName = data.Key3;

            party.BasicDetails.dob = dataOfLead.BasicDetails.dob;
            party.BasicDetails.gender = dataOfLead.BasicDetails.gender;
            party.ContactDetails.currentAddress.houseNo = dataOfLead.ContactDetails.currentAddress.addressLine1;
            party.ContactDetails.currentAddress.street = dataOfLead.ContactDetails.currentAddress.streetName;
            party.ContactDetails.currentAddress.district = dataOfLead.ContactDetails.currentAddress.city;
            party.ContactDetails.currentAddress.city = dataOfLead.ContactDetails.currentAddress.state;
            party.ContactDetails.currentAddress.zipCode = dataOfLead.ContactDetails.currentAddress.zipCode;
            //Comment this country mapping since country is mapping to basicdetails
            //party.ContactDetails.birthAddress.country = dataOfLead.ContactDetails.currentAddress.country;
            // occupationCategory For occupation code  & occupationCategoryValue for occupation category name
//            party.OccupationDetails.occupationCategory = dataOfLead.OccupationDetails.occupationCategory;
//            party.OccupationDetails.occupationCategoryValue = dataOfLead.OccupationDetails.occupationCategoryValue;
//            party.OccupationDetails.description = dataOfLead.OccupationDetails.description;
//            party.OccupationDetails.jobCode = dataOfLead.OccupationDetails.jobCode;
//            party.OccupationDetails.jobClass = dataOfLead.OccupationDetails.jobCode;
//            party.OccupationDetails.descriptionOthers = dataOfLead.OccupationDetails.descriptionOthers;

            party.BasicDetails.incomeRange = dataOfLead.BasicDetails.incomeRange;
            party.BasicDetails.nationality = dataOfLead.BasicDetails.nationality;
            party.BasicDetails.countryofResidence = dataOfLead.ContactDetails.currentAddress.country;
            party.BasicDetails.identityProof = dataOfLead.BasicDetails.identityProof;

            if (dataOfLead.BasicDetails.gender == 'NotMentioned') {
                //dataOfLead.BasicDetails.gender = "";
                party.BasicDetails.gender = "";
            } else {
                party.BasicDetails.gender = dataOfLead.BasicDetails.gender;
            }

            //party.BasicDetails.dob =data.Key7; to be used
            party.BasicDetails.dob = dataOfLead.BasicDetails.dob;
            party.type = "Lead";
            parties.push(party);
            globalService
                .setParties(parties);
        });
    };

    function fnaInitialLoad() {
        LEDynamicUI.getUIJson(rootConfig.FnaConfigJson, false, function (FnaConfigJson) {
            FnaVariables.goals = FnaConfigJson.goals;
            FnaVariables.lifeStageGoals = FnaConfigJson.lifeStageGoals;
            FnaVariables.familyMembers = FnaConfigJson.familyMembers;
            FnaVariables.riskList = FnaConfigJson.riskList;
            FnaVariables.aboutCompany = FnaConfigJson.aboutCompany;
            FnaVariables.custLifeStagesList = FnaConfigJson.custLifeStages;
            FnaVariables.custGenderSelectionsList = FnaConfigJson.custGenderSelections;
            FnaVariables.pages = FnaConfigJson.pages;
            FnaVariables.summaryGraph = FnaConfigJson.summaryGraph;
            FnaVariables.pageTypes = FnaConfigJson.pageTypes;
            $scope.performanceDetails = FnaVariables.aboutCompany.performanceDetails;
            $scope.revenueDetails = FnaVariables.aboutCompany.revenueDetails;
            $scope.refresh();
        });
    };
    /* fna edit mode */
    $scope.getFnaData = function (data, fnaData, successcallback) {
        FnaVariables
            .setFnaModel({
                'FNA': JSON
                    .parse(JSON
                        .stringify(fnaData.TransactionData))
            });
        $scope.FNAObject = FnaVariables.getFnaModel();
        globalService
            .setParties($scope.FNAObject.FNA.parties);
        FnaVariables.leadId = fnaData.Key1;
        FnaVariables.fnaId = fnaData.Key2;
        FnaVariables.transTrackingID = fnaData.TransTrackingID;
        FnaVariables.fnaKeys.Key12 = fnaData.Key12;
        $rootScope.transactionId = fnaData.Id;
        successcallback();
    };

    $scope.navigateToFna = function (data) {
        FnaVariables.choosePartyStatus = false;
        try {
            if ($scope.Illustration_data != "" && typeof $scope.Illustration_data != "undefined") {
                data = $scope.Illustration_data;
            }
            if ((rootConfig.isDeviceMobile)) {
                GLI_DataService
                    .getRelatedFNAForLMS(
                        data.TransTrackingID,
                        function (fnaData) {
                            /* FNA Changes by LE Team -- starts -- Bug 3800 */ 
                            if (fnaData.length != 0) {
                                FnaVariables.transactionID = fnaData[0].Id;
                            } else {
                                FnaVariables.transactionID = 0;
                            }
                            /* FNA Changes by LE Team -- ends -- Bug 3800 */ 
                            $scope.navigatingToFNAClick(fnaData, data);

                        });
            } else {
                PersistenceMapping.clearTransactionKeys();
                var searchCriteria = {
                    searchCriteriaRequest: {
                        "command": "RelatedTransactions",
                        "modes": ['FNA'],
                        "value": data.TransTrackingID
                    }
                };
                PersistenceMapping.Type = "FNA";
                var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
                transactionObj.Key1 = data.TransTrackingID;
                transactionObj.filter = "Key15 <> 'Cancelled'";

                DataService.getFilteredListing(transactionObj, function (fnaData) {
                    $scope.navigatingToFNAClick(fnaData, data);
                });
            }



        } catch (exception) {
            $rootScope.lePopupCtrl.showError(
                translateMessages($translate,
                    "lifeEngage"),
                "Error Output " + exception,
                translateMessages($translate,
                    "lms.ok"));
        }
    };
    
    $scope.navigateToIllustration = function (data) {
            try {

                $scope.illustrationCount = data.illustrationCount;
                IllustratorVariables.illustrationFromKMC = true;
                IllustratorVariables.fromFNAChoosePartyScreenFlow = false;
                 /*FNA changes by LE Team >>> starts - Bug 4194*/
                FnaVariables.flagFromCustomerProfileFNA = false;
                 /*FNA changes by LE Team >>> ends - Bug 4194*/
                $scope.Illustration_data = angular.copy(data);
               // $scope.proceedToIllustration(data);
                if (rootConfig.isDeviceMobile) {
                    GLI_DataService.getRelatedFNAForLMS(data.TransTrackingID,
                            function(fnaData) {
                        $scope.navigatingToIllustratonClick(fnaData, data);
                    });
                }
                else{
                    PersistenceMapping.clearTransactionKeys();
                    var  searchCriteria = {
                             searchCriteriaRequest:{
                    "command" : "RelatedTransactions",
                    "modes" :['FNA'],
                    "value" : data.TransTrackingID
                    }
                    };
                    PersistenceMapping.Type = "FNA";
                    var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
                    transactionObj.Key1 = data.TransTrackingID;
                    transactionObj.filter = "Key15 <> 'Cancelled'";

                    DataService.getFilteredListing(transactionObj, function(fnaData) {
                        $scope.navigatingToIllustratonClick(fnaData, data);
                    });
                } 
            } catch (exception) {
                $rootScope.lePopupCtrl.showError(
                    translateMessages($translate,
                        "lifeEngage"),
                    "Error Output " + exception,
                    translateMessages($translate,
                        "lms.ok"));
            }
    };

    $scope.CancelFunction = function () {};
    $scope.checkLanguage = function () {
        if ($translate.use() == 'vt_VT') {
            return true;
        } else {
            return false;
        }
    };
    $scope.OverlayPopoverFunction = function () {
        $rootScope.alertObj.showPopup = true;
        $rootScope.refresh();
        $scope.overflowClass = true;
    };
    $scope.closeOverlay = function () {
        $scope.overflowClass = false;
    };
    $scope.closeMobileOverlay = function () {
        $scope.alertObj.showPopup = false;
        angular.element('.popover-mobile').hide();
    };

    $scope.proceedToFna = function () {
        $location.path("/fnaLandingPage/0/0");
        $scope.refresh();

    };

    $scope.proceedToIllustration = function () {
        $rootScope.isFromIllustration = true;
        var data = $scope.Illustration_data;
        IllustratorVariables.leadData = data;
        $scope.illustrationCount = data.illustrationCount;
        if (data.Id) {
            LmsVariables.lmsKeys.Id = data.Id;
        } else {
            // Key1 for web
            LmsVariables.lmsKeys.Key1 = data.Key1;
        }
        LmsService.getLead(function (data) {
            var parties = [];
            var party = new globalService.party();
            var dataOfLead = LmsVariables.getLmsModel().Lead;
			party.BasicDetails.firstName = dataOfLead.BasicDetails.fullName;
			party.BasicDetails.lastName = dataOfLead.BasicDetails.lastName;
			if (dataOfLead.BasicDetails.gender == 'NotMentioned') {
                party.BasicDetails.gender = "";
            } else {
                party.BasicDetails.gender = dataOfLead.BasicDetails.gender;
            }
			party.BasicDetails.dob = dataOfLead.BasicDetails.dob;
			party.BasicDetails.age = dataOfLead.BasicDetails.age;
            party.ContactDetails.mobileNumber1 = dataOfLead.ContactDetails.mobileNumber1;
            party.ContactDetails = dataOfLead.ContactDetails;
            party.ContactDetails.homeNumber1 = dataOfLead.ContactDetails.homeNumber1;
            party.ContactDetails.emailId =  dataOfLead.ContactDetails.emailId
            party.BasicDetails.nickName = dataOfLead.BasicDetails.nickName;
            party.BasicDetails.identityProof = dataOfLead.BasicDetails.identityProof;
            party.BasicDetails.IDcard = dataOfLead.BasicDetails.IDcard;
           
            /* party.ContactDetails.currentAddress.houseNo = dataOfLead.ContactDetails.currentAddress.addressLine1;
            party.ContactDetails.currentAddress.street = dataOfLead.ContactDetails.currentAddress.streetName;
            party.ContactDetails.currentAddress.district = dataOfLead.ContactDetails.currentAddress.city;
            party.ContactDetails.currentAddress.city = dataOfLead.ContactDetails.currentAddress.state;
            party.ContactDetails.currentAddress.zipCode = dataOfLead.ContactDetails.currentAddress.zipCode;
            party.OccupationDetails.description = dataOfLead.OccupationDetails.description;
            party.OccupationDetails.descriptionOthers = dataOfLead.OccupationDetails.descriptionOthers;
            party.BasicDetails.incomeRange = dataOfLead.BasicDetails.incomeRange;
            party.BasicDetails.nationality = dataOfLead.BasicDetails.nationality;
            party.BasicDetails.countryofResidence = dataOfLead.ContactDetails.currentAddress.country;
            party.BasicDetails.identityProof = dataOfLead.BasicDetails.identityProof;
            party.BasicDetails.dob = dataOfLead.BasicDetails.dob;*/
			
            party.type = "Lead";
            parties.push(party);
            globalService.setParties(parties);
            if ($scope.illustrationCount == 0) {
                try {
                    $rootScope.transactionId = 0;
                    IllustratorVariables.clearIllustrationVariables();
                    if (data.TransTrackingID)
                        IllustratorVariables.leadId = data.TransTrackingID;
                	IllustratorVariables.leadName = data.Key2;
                	IllustratorVariables.leadEmailId = data.Key20;
                	IllustratorVariables.illustrationStatus = "Draft";
                	//used for redirecting
                	UtilityService.leadPage = 'LeadSpecificListing';
                	$location.path("/products/0/0/0/0/false");
                	$scope.refresh();
                } catch (exception) {
                    $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
                    								 "Error Output " + exception,
                    								 translateMessages($translate, "lms.ok"));
                }
            } else {
                var leadId = 0;
                if (data.TransTrackingID)
                    leadId = data.TransTrackingID;
                LmsVariables.lmsKeys.Key2 = data.Key2;
                LmsVariables.lmsKeys.Key20 = data.Key20;
                UtilityService.leadPage = 'LeadSpecificListing';
                $location.path("/MyAccount/Illustration/" + leadId + '/0');
                $scope.refresh();
                // List the Illustration with this leadID
            }
        });
    	/*
        $scope.getLeadDetailsForLead(function (data) {
        	if ($scope.illustrationCount == 0) {
                try {
                    $rootScope.transactionId = 0;
                    IllustratorVariables.clearIllustrationVariables();
                    if (data.TransTrackingID)
                        IllustratorVariables.leadId = data.TransTrackingID;
                	IllustratorVariables.leadName = data.Key2;
                	IllustratorVariables.leadEmailId = data.Key20;
                	IllustratorVariables.illustrationStatus = "Draft";
                	//used for redirecting
                	UtilityService.leadPage = 'LeadSpecificListing';
                	$location.path("/products/0/0/0/0/false");
                	$scope.refresh();
                } catch (exception) {
                    $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
                    								 "Error Output " + exception,
                    								 translateMessages($translate, "lms.ok"));
                }
            } else {
                var leadId = 0;
                if (data.TransTrackingID)
                    leadId = data.TransTrackingID;
                LmsVariables.lmsKeys.Key2 = data.Key2;
                LmsVariables.lmsKeys.Key20 = data.Key20;
                UtilityService.leadPage = 'LeadSpecificListing';
                $location.path("/MyAccount/Illustration/" + leadId + '/0');
                $scope.refresh();
                // List the Illustration with this leadID
            }
        });
        */
        /*
        
        $scope.getLeadDetailsForLead(data);
        if ($scope.illustrationCount == 0) {
            try {
                $rootScope.transactionId = 0;
                IllustratorVariables.clearIllustrationVariables();
                if (data.TransTrackingID)
                    IllustratorVariables.leadId = data.TransTrackingID;
            	IllustratorVariables.leadName = data.Key2;
            	IllustratorVariables.leadEmailId = data.Key20;
            	IllustratorVariables.illustrationStatus = "Draft";
            	//used for redirecting
            	UtilityService.leadPage = 'LeadSpecificListing';
            	$location.path("/products/0/0/0/0/false");
            	$scope.refresh();
            } catch (exception) {
                $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
                								 "Error Output " + exception,
                								 translateMessages($translate, "lms.ok"));
            }
        } else {
            var leadId = 0;
            if (data.TransTrackingID)
                leadId = data.TransTrackingID;
            LmsVariables.lmsKeys.Key2 = data.Key2;
            LmsVariables.lmsKeys.Key20 = data.Key20;
            UtilityService.leadPage = 'LeadSpecificListing';
            $location.path("/MyAccount/Illustration/" + leadId + '/0');
            $scope.refresh();
            // List the Illustration with this leadID
        }*/
    };

    $scope.navigateToEApp = function (data) {        
        globalService.clearData();
        if (data.illustrationCount == 0) { //  eAppCount    
            $scope.illustrationCount = data.illustrationCount;
            IllustratorVariables.illustrationFromKMC = true;
            IllustratorVariables.fromFNAChoosePartyScreenFlow = false;
             /*FNA changes by LE Team >>> starts - Bug 4194*/
            FnaVariables.flagFromCustomerProfileFNA = false;
             /*FNA changes by LE Team >>> ends - Bug 4194*/
            $scope.Illustration_data = angular.copy(data);
            
            $rootScope.lePopupCtrl.showWarning(
						translateMessages($translate,
								"lifeEngage"),
						translateMessages($translate,
								"lms.completeIllustrationToProceed"),  
						translateMessages($translate,
								"illustrator.popUpClose"),$scope.okPressed,translateMessages($translate,
								"proceed"),$scope.proceedToCreateIllustration);
        } else if (data.eAppCount == 0) {
            var leadId = 0;
            leadId = data.TransTrackingID;
            if (UtilityService.leadPage == 'LeadSpecificListing' || (leadId != '' && leadId != undefined)) {
                $location.path("/MyAccount/Illustration/" + leadId + '/0');
            } else if (UtilityService.leadPage == 'GenericListing') {
                $location.path("/MyAccount/Illustration/0/0");
            }else{
                $location.path("/MyAccount/Illustration/0/0");
            }
            $scope.refresh();
        }else {
            var leadId = 0;
            leadId = data.TransTrackingID;
            $location.path("/MyAccount/eApp/" + leadId + '/0'); //UAT bug fix 396 not able to navigate to eapp when clicking leads eapp
            $scope.refresh();
        }
        
    };
    
    $scope.proceedToCreateIllustration=function(){            
        $rootScope.isFromIllustration = true;
        var data = $scope.Illustration_data;
        IllustratorVariables.leadData = angular.copy(data);
//        $scope.illustrationCount = data.illustrationCount;
        if (data.Id) {
            LmsVariables.lmsKeys.Id = data.Id;
        } else {
            // Key1 for web
            LmsVariables.lmsKeys.Key1 = data.Key1;
        }
        LmsService.getLead(function (data) {
            var parties = [];
            var party = new globalService.party();
            var dataOfLead = LmsVariables.getLmsModel().Lead;
			party.BasicDetails.firstName = dataOfLead.BasicDetails.fullName;
			party.BasicDetails.lastName = dataOfLead.BasicDetails.lastName;
			if (dataOfLead.BasicDetails.gender == 'NotMentioned') {
                party.BasicDetails.gender = "";
            } else {
                party.BasicDetails.gender = dataOfLead.BasicDetails.gender;
            }
			party.BasicDetails.dob = dataOfLead.BasicDetails.dob;
			party.BasicDetails.age = dataOfLead.BasicDetails.age;
            party.ContactDetails.mobileNumber1 = dataOfLead.ContactDetails.mobileNumber1;
            party.ContactDetails = dataOfLead.ContactDetails;
            party.ContactDetails.homeNumber1 = dataOfLead.ContactDetails.homeNumber1;
            party.ContactDetails.emailId =  dataOfLead.ContactDetails.emailId
            party.BasicDetails.nickName = dataOfLead.BasicDetails.nickName;
			party.BasicDetails.identityProof = dataOfLead.BasicDetails.identityProof;
            party.BasicDetails.IDcard = dataOfLead.BasicDetails.IDcard; 
    			
            party.type = "Lead";
            parties.push(party);
            globalService.setParties(parties);
            if ($scope.illustrationCount == 0) {
                try {
                    $rootScope.transactionId = 0;
                    IllustratorVariables.clearIllustrationVariables();
                    if (data.TransTrackingID)
                        IllustratorVariables.leadId = data.TransTrackingID;
                	IllustratorVariables.leadName = data.Key2;
                	IllustratorVariables.leadEmailId = data.Key20;
                	IllustratorVariables.illustrationStatus = "Draft";
                	//used for redirecting
                	UtilityService.leadPage = 'LeadSpecificListing';
                	$location.path("/products/0/0/0/0/false");
                	$scope.refresh();
                } catch (exception) {
                    $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
                    								 "Error Output " + exception,
                    								 translateMessages($translate, "lms.ok"));
                }
            } else {
                var leadId = 0;
                if (data.TransTrackingID)
                    leadId = data.TransTrackingID;
                LmsVariables.lmsKeys.Key2 = data.Key2;
                LmsVariables.lmsKeys.Key20 = data.Key20;
                UtilityService.leadPage = 'LeadSpecificListing';
                $location.path("/MyAccount/Illustration/" + leadId + '/0');
                $scope.refresh();
                // List the Illustration with this leadID
            }
        });
    }

    $scope.confirmDelete = function (lead) {
        $scope.selectedLead = lead;
        $rootScope.lePopupCtrl.showQuestion(
                translateMessages($translate,
                    "delete"),
                translateMessages($translate,
                    "deleteLeadWarningText"),
                translateMessages($translate,
                    "deleteConfirmButtonText"),
                $scope.deleteClientValidation,
                translateMessages($translate,
                    "cancelButtonText"),
                $scope.cancel,
                '',
                '',
                function () {
                    $scope.dvDeleteConfirm = false;
                });

    };

    $scope.deleteClientValidation = function () {
        $scope.deletePopup('LMS');
    };

    // Over riding function
    $scope.ondelteSuccessCallBack = function () {
        $scope.selectedProposal.splice(0, 1);

        if ($scope.selectedProposal.length > 0) {
            $scope.deleteClient();
        } else {

            /*p$rootScope.showHideLoadingImage(true,
                'deleteMessage', $translate);*/

            PersistenceMapping.clearTransactionKeys();
            var selectedIds = [];
            if ($scope.userDetails.agentType == "BranchUser" && !(rootConfig.isDeviceMobile)) {
                var reportingAgentsList = UserDetailsService.getReportingAgents();
                for (var i = 0; i < reportingAgentsList.length; i++) {
                    selectedIds.push(reportingAgentsList[i].agentCode);
                }
                var searchCriteria = {
                    searchCriteriaRequest: {
                        "command": "ListingDashBoard",
                        "modes": ['LMS'],
                        "selectedIds": selectedIds,
                        "value": ""
                    }
                };
            } else {
                var searchCriteria = {
                    searchCriteriaRequest: {
                        "command": "ListingDashBoard",
                        "modes": ['LMSRelatedTransCount'],
                        "selectedIds": selectedIds,
                        "value": ""
                    }
                };
            }


            PersistenceMapping.dataIdentifyFlag = false;
            var transactionObj = PersistenceMapping
                .mapScopeToPersistence(searchCriteria);
            transactionObj.Id = $routeParams.transactionId;
            if ($scope.userDetails.agentType == "BranchUser") {
                transactionObj.loginType = "BranchUser";
            }
            transactionObj.Key10 = "Custom";
            transactionObj.Type = "LMS";
            transactionObj.filter = "Key15 <> 'Cancelled'";
         /*p   $rootScope.showHideLoadingImage(true,
                'paintUIMessage', $translate);*/
            DataService.getFilteredListing(
                transactionObj,
                $scope.onGetListingsSuccess,
                $scope.onGetListingsError);
            $rootScope.showHideLoadingImage(false);
        }
    };

    $scope.onFirstSuccessCallBack = function () {
        if ((rootConfig.isDeviceMobile)) {
            var whereClause = 'Key1 = ' +
                '"' +
                $scope.selectedLead.TransTrackingID +
                '"';
            DataService.deleteFromTable('Transactions',
                whereClause,
                $scope.ondelteSuccessCallBack,
                $scope.ondelteError);
        } else {
            $scope.ondelteSuccessCallBack();
        }
    };

    $scope.deleteClient = function () {
        nextSyncType = $scope.deleteType;
        LmsService.deleteClient($scope.selectedLead, $scope.onFirstSuccessCallBack, $scope.ondelteError);
    };

    $scope.deletePopup = function (type) {
        $scope.deleteType = type;
        PersistenceMapping.clearTransactionKeys();
        $scope.mapKeysforPersistence();
        $scope.deleteClient();

    };
    $scope.validateCreatorDetails = function () {
        if ($("#creatorDetails input.ng-invalid").length <= 0) {
            return true;
        } else {
            $("#creatorDetails input.ng-invalid").addClass("highlight");
            return false;
        }
    };

    $scope.cancelPopup = function () {
        $scope.RMPopupShow = false;
        if ($scope.LmsModel.Lead.RMInfo.RMMobileNumber) {
            $scope.LmsModel.Lead.RMInfo.RMMobileNumber = "";
        }
        if ($scope.LmsModel.Lead.RMInfo.RMName) {
            $scope.LmsModel.Lead.RMInfo.RMName = "";
        }
        if ($scope.LmsModel.Lead.RMInfo.RMNationalID) {
            $scope.LmsModel.Lead.RMInfo.RMNationalID = "";
        }
        LmsVariables.setLmsModel($scope.LmsModel);
        $route.reload();
    };

    $scope.proceedToCreateLead = function () {
        if ($scope.validateCreatorDetails()) {

           /*p $rootScope.showHideLoadingImage(true,
                'paintUIMessage', $translate);*/
            LmsVariables.editMode = false;
            $rootScope.transactionId = 0;
            LmsVariables.clearLmsVariables();
            LmsVariables
                .setNewLmsModel($rootScope.username);
            var rmInfo = {
                "RMName": "",
                "RMNationalID": "",
                "RMMobileNumber": ""
            };

            rmInfo.RMName = $scope.LmsModel.Lead.RMInfo.RMName;
            rmInfo.RMNationalID = $scope.LmsModel.Lead.RMInfo.RMNationalID;
            rmInfo.RMMobileNumber = $scope.LmsModel.Lead.RMInfo.RMMobileNumber;

            var RMDetails = JSON.stringify(rmInfo);
            localStorage.setItem("RMDetails", RMDetails);
            LmsVariables.isFromLeadListing = true;
            LmsVariables.setRMInfoModel(rmInfo);
            $location.path('/lms/clientprofile');
            $scope.refresh();
        }
    };

    $scope.createLead = function () {
        LmsVariables.actualMeetingDate = "";
        LmsVariables.actualMeetingTime = "";
        LmsVariables.plannedMeetingDate = "";
        LmsVariables.plannedMeetingTime = "";
        LmsVariables.submissionDate = "";
        $rootScope.isTabsEnabled = false;
        if ($scope.userDetails.agentType == "BranchUser") {
            $scope.reportingAgentList = UserDetailsService.getReportingAgents();
            if ($scope.reportingAgentList) {
                if ($scope.reportingAgentList.length == 0) {
                    $rootScope.lePopupCtrl.showError(
                        translateMessages($translate,
                            "lifeEngage"),
                        translateMessages($translate, "lms.cannotCreateNewLead"),
                        translateMessages($translate,
                            "lms.ok"));
                } else {
                    $scope.RMPopupShow = true;
                    $scope.LmsModel.Lead.RMInfo.RMName = LmsVariables.getRMInfoModel().RMName;
                    $scope.LmsModel.Lead.RMInfo.RMNationalID = LmsVariables.getRMInfoModel().RMNationalID;
                    $scope.LmsModel.Lead.RMInfo.RMMobileNumber = LmsVariables.getRMInfoModel().RMMobileNumber;
                    LmsVariables.setLmsModel($scope.LmsModel);

                    LEDynamicUI
                        .paintUI(
                            rootConfig.template,
                            "leadListingWithTabs.json",
                            "leadCreatorsDetailsView",
                            "#leadListingWithTabs",
                            true,
                            function () {
                                $scope.callback();
                            }, $scope, $compile);
                }
            } else {
                $rootScope.lePopupCtrl.showError(
                    translateMessages($translate,
                        "lifeEngage"),
                    translateMessages($translate, "lms.cannotCreateNewLead"),
                    translateMessages($translate,
                        "lms.ok"));
            }
        } else {
            $scope.RMPopupShow = false;
          /*p  $rootScope.showHideLoadingImage(true,
                'paintUIMessage', $translate);*/
            LmsVariables.editMode = false;
            $rootScope.transactionId = 0;
            LmsVariables.clearLmsVariables();
            LmsVariables
                .setNewLmsModel($rootScope.username);
            $location.path('/lms/clientprofile');
            $scope.refresh();
        }
    };



    $scope.editLead = function (leadID, key, status) {
        LmsVariables.actualMeetingDate = "";
        LmsVariables.actualMeetingTime = "";
        LmsVariables.plannedMeetingDate = "";
        LmsVariables.plannedMeetingTime = "";
        LmsVariables.submissionDate = "";
        $rootScope.isTabsEnabled = true;
        if ($scope.userDetails.agentType == "BranchUser") {

        } else {

            LmsVariables.clearLmsVariables();
            if (leadID) {
                LmsVariables.lmsKeys.Id = leadID;
            } else {
                // Key1 for web
                LmsVariables.lmsKeys.Key1 = key;
            }
            LmsService.getLead(function (data) {
                LmsVariables.editMode = true;
                LmsVariables.transTrackingID = data.TransTrackingID;
                $scope.leadHeader = translateMessages(
                    $translate, "lms.editLead");
                $location.path('/lms/clientprofile');
                // $scope.refresh();
                LEDynamicUI.paintUI(rootConfig.template,
                    "leadListingWithTabs.json",
                    "popupTabsNavbar", "#leadDetail",
                    true,
                    function () {
                        $scope.callback();
                    }, $scope, $compile);
                LEDynamicUI.paintUI(rootConfig.template,
                    "leadListingWithTabs.json",
                    "leadDetailsSection",
                    "#leadDetailContent", true,
                    function () {
                        $scope.callback();
                        if ($scope.alertObj.showPopup == true) {
                            $scope.alertObj.showPopup = false;
                        }
                    }, $scope, $compile);
            });
        }
    };
    $scope.hideFilterOverlay = function () {
        $scope.filterOpen = !($scope.filterOpen);
    };
   
  $scope.showNormalListingView = function() {
								$scope.selectedProposal = [];
								$scope.toggleListing = true;
								if ($scope.filterOpen == true) {
									$scope.reset();
									$scope.filterOpen = false;
								}
							}
							$scope.showPriorityListingView = function() {
								$scope.selectedProposal = [];
								$scope.toggleListing = false;
								if ($scope.filterOpen == true) {
									$scope.reset();
									$scope.filterOpen = false;
								}

							}

    $scope.filterLeads = function () {
        LEDynamicUI.paintUI(rootConfig.template,
            "leadListingWithTabs.json", "filterSection",
            "#filterSection", true,
            function () {
                $scope.filtercallback();
            }, $scope, $compile);
    };

    $scope.filtercallback = function () {
        $rootScope.showHideLoadingImage(false);

    };

    /* Checked styling for Table checkbox */
    $scope.getSelectedClass = function (item) {
        return $scope.selectedProposal.indexOf(item) > -1 ? 'checked' :
            '';
    };

    $scope.severityValidate=function(value){ 
        if(value==='VeryHot'){
            return translateMessages($translate, "lms.VeryHot");    
        }else if(value==="null"){
            return "";
        }
    }
	function customSortForListing(data, callBack) {
                                if(!$scope.sortApplied) {
                                    data.sort(function(a,b){
                                       if (rootConfig.isDeviceMobile) {
                                            b.modifiedDate = b.modifiedDate ? b.modifiedDate : '1900-01-01 01:01:01';
                                            a.modifiedDate = a.modifiedDate ? a.modifiedDate : '1900-01-01 01:01:01';
                                        }
                                        return new Date(b.modifiedDate) - new Date(a.modifiedDate);
                                     });
                                }
                                /*$timeout(function() {
                                   callBack(data);
                                },0);*/
                                 callBack(data);
                            }


    // Combining first name and last name
    $scope.onGetListingsSuccess = function (dbObject) {
        var dataNew = [];
        $scope.redHotLeads = [];
        $scope.hotLeads = [];
        $scope.warmLeads = [];
        $scope.coldLeads = [];

        for (var i = 0; i < dbObject.length; i++) {
            var requiredData = {};
            requiredData.Id = dbObject[i].Id;
            requiredData.Key1 = dbObject[i].Key1;
            requiredData.Key2 = dbObject[i].Key2;
            requiredData.Key3 = dbObject[i].Key3;
            requiredData.Key4 = dbObject[i].Key4;
            requiredData.Key5 = dbObject[i].Key5;
            requiredData.Key6 = dbObject[i].Key6;
            requiredData.Key7 = dbObject[i].Key7;
            requiredData.Key8 = dbObject[i].Key8;
            requiredData.Key14 = dbObject[i].Key14;
            requiredData.Key11 = dbObject[i].Key11;
            requiredData.Key15 = dbObject[i].Key15;
            requiredData.Key16 = dbObject[i].Key16;
            requiredData.Key17 = dbObject[i].Key17;
            requiredData.Key18 = dbObject[i].Key18;
            if (dbObject[i].Key15 == 'Successful Sale') {
                requiredData.Key19 = "";
            } else {
				
				if(rootConfig.isDeviceMobile){
					
					var tempNextDateArray = [];
					var	dateArray = [];
					requiredData.Key19 = "";
					
					if(dbObject[i].Key19 !== undefined && dbObject[i].Key19 !== null && dbObject[i].Key19 !== "") {						
						
						var str = dbObject[i].Key19;					
						dateArray = str.split(",");
						
						if(dateArray !== null && dateArray !== undefined 
								&& dateArray !== "" && dateArray.length > 0) {
							for(var j=0; j < dateArray.length; j++) {
								
								var _currentDate = Date.parse(new Date());
								var _actualDate = Date.parse(dateArray[j]);
								
								if(_actualDate > _currentDate) {							
									tempNextDateArray.push(_actualDate);
								}
							}
						}
		
						if(tempNextDateArray !== null && tempNextDateArray !== undefined 
								&& tempNextDateArray !== "" && tempNextDateArray.length > 0) {
									
							tempNextDateArray.sort();
																		
							var d = new Date(tempNextDateArray[0]);	
							requiredData.Key19 = formatForDateControl(d);
						} 
					}
				} else {
					    requiredData.Key19 = dbObject[i].Key19;
				}
               
			}
				if(rootConfig.isDeviceMobile){
					requiredData.modifiedDate=dbObject[i].modifiedDate;
				}
            requiredData.Key20 = dbObject[i].Key20;
            requiredData.Key21 = dbObject[i].Key21;
            requiredData.Key22 = dbObject[i].Key22;
			requiredData.Key26 = dbObject[i].Key26;
            requiredData.Type = dbObject[i].Type;
            requiredData.fullName = dbObject[i].Key2 + " " +
                dbObject[i].Key3;
            requiredData.illustrationCount = dbObject[i].illustrationCount;
            requiredData.eAppCount = dbObject[i].eAppCount;
            requiredData.fnaCount = dbObject[i].fnaCount;
            requiredData.TransTrackingID = dbObject[i].TransTrackingID;
            dataNew.push(requiredData);
        }
		
		if (!rootConfig.isDeviceMobile) {
            $scope.leads = dataNew.sort(function (a, b) {
                return new Date(b.Key14) -
                    new Date(a.Key14);
            });
        }else {
                  customSortForListing(dataNew,function(data){
                     $scope.leads=data;
                            });
        }


       /* if (!(rootConfig.isDeviceMobile)) {
            $scope.leads = dataNew.sort(function (a, b) {
                return LEDate(b.Key14) -
                    LEDate(a.Key14);
            });
        } else {
            $scope.leads = dataNew;
        }*/
        LmsVariables.setRefLead($scope.leads);

        for (var i = 0; i < $scope.leads.length; i++) {

            var severity = $scope.leads[i].Key18;

            switch (severity) {

                case LmsVariables.SORT_DETAILS.REDHOT:
                    $scope.redHotLeads
                        .push($scope.leads[i]);
                    break;

                case LmsVariables.SORT_DETAILS.HOT:
                    $scope.hotLeads.push($scope.leads[i]);
                    break;

                case LmsVariables.SORT_DETAILS.WARM:
                    $scope.warmLeads.push($scope.leads[i]);
                    break;

                case LmsVariables.SORT_DETAILS.COLD:
                    $scope.coldLeads.push($scope.leads[i]);
                    break;
                default:
                    // Do nothing
            }
            $scope.tableLeads = [].concat($scope.leads);
            $scope.tableWarmLeads = [].concat($scope.warmLeads);
            $scope.tableHotLeads = [].concat($scope.hotLeads);
            $scope.tableColdLeads = [].concat($scope.coldLeads);
        }
        if ($scope.redHotLeads.length <= 0) {
            $scope.redHotLeadsEmpty = true;
        }
        if ($scope.hotLeads.length <= 0) {
            $scope.hotLeadsEmpty = true;
        }
        if ($scope.warmLeads.length <= 0) {
            $scope.warmLeadsEmpty = true;
        }
        if ($scope.coldLeads.length <= 0) {
            $scope.coldLeadsEmpty = true;
        }

        $scope.filterLeads();
        $scope.refresh();
        $rootScope.showHideLoadingImage(false);
        // $scope.refresh();
    };

    LEDynamicUI
        .getUIJson(
            rootConfig.lmsConfigJson,
            false,
            function (lmsConfig) {
                $scope.showReferenceDetails = lmsConfig.showReferenceDetails;
            });

    $scope.reset = function () {
        $scope.leadName = "";
        $scope.lastName = "";
        $scope.source = "";
        $scope.agentName = "";
        $scope.contactNumber = "";
        $scope.nextMeetingDate = "";
        $scope.status = "";
        $scope.nextActionFilter = "";
        $scope.filterPotential = "";
        $scope.referenceNameFilter = "";
        $scope.actionFilter = "";
        $scope.filterLeads();
    };

    $scope.initialLoad = function () {
		
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		}
	
        LEDynamicUI
            .paintUI(
                rootConfig.template,
                "leadListingWithTabs.json",
                "leadListingWithTabs",
                "#leadListingWithTabs",
                true,
                function () {
                    $scope.callback();
                    PersistenceMapping.clearTransactionKeys();
                    var searchCriteria = {
                        searchCriteriaRequest: {
                            "command": "ListingDashBoard",
                            "modes": ['LMSRelatedTransCount'],
                            "value": ""
                        }
                    };
					
                    
                    if ($scope.userDetails.agentType == "BranchUser") {
                        $scope.isBankUser = true;
                    }

                    PersistenceMapping.dataIdentifyFlag = false;
                    if (!(rootConfig.isDeviceMobile)) {
                        if ($routeParams.leadId == null ||
                            $routeParams.leadId == 0) {
                            PersistenceMapping.Key13 = UtilityService.getFormattedDate();
                        }
                        PersistenceMapping.clearTransactionKeys();
                        var selectedIds = [];
                        if ($scope.userDetails.agentType == "BranchUser" && !(rootConfig.isDeviceMobile)) {
                            var reportingAgentsList = UserDetailsService.getReportingAgents();
                            for (var i = 0; i < reportingAgentsList.length; i++) {
                                selectedIds.push(reportingAgentsList[i].agentCode);
                            }
                            var searchCriteria = {
                                searchCriteriaRequest: {
                                    "command": "ListingDashBoard",
                                    "modes": ['LMS'],
                                    "selectedIds": selectedIds,
                                    "value": ""
                                }
                            };
                        } else {
                            var searchCriteria = {
                                searchCriteriaRequest: {
                                    "command": "ListingDashBoard",
                                    "modes": ['LMSRelatedTransCount'],
                                    "selectedIds": selectedIds,
                                    "value": ""
                                }
                            };
                        }

                    }
                    //prepopulation RM details
                    var rmDetails = localStorage.RMDetails;
                    if (rmDetails && rmDetails != "") {
                        var details = JSON.parse(rmDetails);
                        $scope.LmsModel.Lead.RMInfo.RMName = details.name;
                        $scope.LmsModel.Lead.RMInfo.RMNationalID = details.nationality;
                        $scope.LmsModel.Lead.RMInfo.RMMobileNumber = details.mobileNumber;
                    }

                    PersistenceMapping.Type = 'LMS';
                    var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
                    transactionObj.Id = $routeParams.transactionId;
                    if ($scope.userDetails.agentType == "BranchUser") {
                        transactionObj.loginType = "BranchUser";
                    }
                    transactionObj.Key10 = "Custom";
                    transactionObj.Type = "LMS";
                    transactionObj.filter = "Key15 <> 'Cancelled'";
                    $scope.searchKey = LmsVariables.searchKey;
                    $rootScope.searchKey = "";
                    $scope.statusFilter = $rootScope.searchValue;
                    $rootScope.searchValue = "";
//                    transactionObj.Key11 = "00007766";
                   /*p $rootScope.showHideLoadingImage(true,
                        'paintUIMessage', $translate);*/
                    DataService.getFilteredListing(transactionObj,
                        $scope.onGetListingsSuccess,
                        $scope.onGetListingsError);

                }, $scope, $compile);
        $scope.leadNamePattern = rootConfig.leadNamePattern;
        $scope.alphabeticPattern = rootConfig.alphabeticPattern;
        $scope.nationalIDPattern = rootConfig.nationalIDPattern;
        $scope.alphabetWithSpacePattern = rootConfig.alphabetWithSpacePattern;
    };

    // back tab
    $scope.back = function () {
        /*p$rootScope.showHideLoadingImage(true,
            'paintUIMessage', $translate);*/
        GLI_LmsService.navigateToListingPage("/landingPage");
    };

    // back tab
    $scope.List = function () {
       /*p $rootScope.showHideLoadingImage(true,
            'paintUIMessage', $translate);*/
        GLI_LmsService.navigateToListingPage("/landingPage");
    };

    // Separating the success call back of navigateToFNA method since it is needed in both mobile and desktop
    $scope.navigatingToFNAClick = function (fnaData, data) {

        fnaInitialLoad();
        /* FNA changes by LE Team Start*/
        if (fnaData &&
            fnaData[0] &&
            fnaData[0].TransactionData) {
		if(fnaData[0].Key2 == ""){
		fnaData[0].Key2 = 0;
		}
		FnaVariables.fnaId = fnaData[0].Key2;
        }
        /* FNA changes by LE Team End */
        if (fnaData &&
            fnaData[0] &&
            fnaData[0].TransactionData) {
            $scope.getFnaData(data, fnaData[0], function () {
                FnaVariables.fnaKeys.Key22 = data.Key2;
                FnaVariables.fnaKeys.Key23 = data.Key20;
                if (fnaData[0].Key15 == "Completed") {
                    FnaVariables.fnaKeys.Key15 = FnaVariables.getStatusOptionsFNA()[1].status;
                    $scope.status = FnaVariables.getStatusOptionsFNA()[1].status;
                    $location.path('/fnaRecomendedProducts');
                    $scope.refresh();
					/* FNA changes made by LE Team start */
                } else if (fnaData[0].Key15 == "FNA Report sent") {
					/* FNA changes made by LE Team end */
                    FnaVariables.fnaKeys.Key15 = FnaVariables.getStatusOptionsFNA()[2].status;
                    $scope.status = FnaVariables.getStatusOptionsFNA()[2].status;
                    if ($scope.naviagationFlag == true) {
                        $location.path('/fnaRecomendedProducts');
                    } else {
                        $location.path('/fnaReport');
                    }
                    $scope.refresh();
                } else {
                    FnaVariables.fnaKeys.Key15 = FnaVariables.getStatusOptionsFNA()[0].status;
                    $scope.status = FnaVariables.getStatusOptionsFNA()[0].status;

                    var lastVisitedPage = "/aboutCompany";
                    //getting last visted page
                    for (var i = 0; i < FnaVariables.pages.length; i++) {
                        if (FnaVariables.pages[i].pageName == FnaVariables.getFnaModel().FNA.lastVisitedPage) {
                            lastVisitedPage = FnaVariables.pages[i].url;
                            break;
                        }
                    }

                    $location.path(lastVisitedPage);
                    $scope.refresh();
                }
            });
        } else {
            FnaVariables.fnaKeys.Key15 = FnaVariables.getStatusOptionsFNA()[0].status;
            $scope.status = FnaVariables.getStatusOptionsFNA()[0].status;
            if (data.Id) {
                LmsVariables.lmsKeys.Id = data.Id;
            } else {
                // Key1 for web
                LmsVariables.lmsKeys.Key1 = data.Key1;
            }

            $scope.getLeadDetailsForLead(data);

            $rootScope.transactionId = 0;
            FnaVariables.fnaId = 0;
            FnaVariables
                .setFnaModel(null);
            // Set key1 as
            // leadId if synced,
            // else set
            // transactionId as
            // leadId
            FnaVariables.leadId = data.TransTrackingID;
            FnaVariables.transTrackingID = 0;
            FnaVariables.fnaKeys.Key12 = "";
            FnaVariables.fnaKeys.Key22 = data.Key2;
            FnaVariables.fnaKeys.Key23 = data.Key20;
            $location
                .path('/aboutCompany/0/0/true/');
            $scope.refresh();
        }


    };

    $scope.refresh = function () {
        $timeout(function () {
            $scope.$apply();
        }, 0)
    };

    // Separating the success call back of navigateToIllustration method since it is needed in both mobile and desktop
    $scope.navigatingToIllustratonClick = function (fnaData, data) {
		/* FNA changes made by LE Team start */
        if (fnaData && fnaData[0] && (fnaData[0].Key15 == "Completed" || fnaData[0].Key15 == "FNA Report sent")) {
		/* FNA changes made by LE Team end */
            IllustratorVariables.fromFNAChoosePartyScreenFlow = false;
             /*FNA changes by LE Team >>> starts - Bug 4194*/
            FnaVariables.flagFromCustomerProfileFNA = false;
             /*FNA changes by LE Team >>> ends - Bug 4194*/
            if ($scope.illustrationCount == 0) {
                $scope.naviagationFlag = true;
                $scope.navigateToFna(data);
            } else {
                //client's illustarion listing page
                $scope.proceedToIllustration(data);
            }


        } else {
            $rootScope.lePopupCtrl.showInfo(
                 translateMessages($translate,
                    "lifeEngage"),
                translateMessages($translate,
                    "lms.completeFnaOrIllustrationProceed"),
                translateMessages($translate,
                    "btnCancel"),
                $scope.CancelFunction,
//                translateMessages($translate,
//                    "lms.proceedToFnaButtonText"),
//                $scope.navigateToFna,
                translateMessages($translate,
                    "lms.proceedToIllustrationButtonText"),
                $scope.proceedToIllustration,
                function () {
                    $rootScope
                        .showHideLoadingImage(false);
                });
        }
    };

    /* Route Mape code starts here */
    $scope.getAddress = function (data, successCall) {
        var address = "";
        if (typeof data.currentAddress.addressLine1 != "undefined" && data.currentAddress.addressLine1 != "") {
            if (address == "") {
                address = data.currentAddress.addressLine1;
            } else {
                address = address + ',' + data.currentAddress.addressLine1;
            }
        }
        if (typeof data.currentAddress.streetName != "undefined" && data.currentAddress.streetName != "") {
            if (address == "") {
                address = data.currentAddress.streetName;
            } else {
                address = address + ',' + data.currentAddress.streetName;
            }
        }

        if (typeof data.currentAddress.stateValue != "undefined" && data.currentAddress.stateValue != "") {
            if (address == "") {
                address = data.currentAddress.stateValue;
            } else {
                address = address + ',' + data.currentAddress.stateValue;
            }
        }
        if (typeof data.currentAddress.cityValue != "undefined" && data.currentAddress.cityValue != "") {
            if (address == "") {
                address = data.currentAddress.cityValue;
            } else {
                address = address + ',' + data.currentAddress.cityValue;
            }
        }
        if (typeof data.currentAddress.wardOrHamletValue != "undefined" && data.currentAddress.wardOrHamletValue != "") {
            if (address == "") {
                address = data.currentAddress.wardOrHamletValue;
            } else {
                address = address + ',' + data.currentAddress.wardOrHamletValue;
            }
        }
        if (typeof data.currentAddress.countryValue != "undefined" && data.currentAddress.countryValue != "") {
            if (address == "") {
                address = data.currentAddress.countryValue;
            } else {
                address = address + ',' + data.currentAddress.countryValue;
            }
        }
        successCall(address);
    }
    $scope.showRouteMap = function (TransTrackingID, Type, Id) {
      //p  $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
        $('body').addClass('loader');

        var ids = [];
        ids.push(TransTrackingID);
        PersistenceMapping.clearTransactionKeys();
        PersistenceMapping.Type = Type;
        var transactionObj = PersistenceMapping.mapScopeToPersistence();
        transactionObj.TransTrackingID = ids;
        transactionObj.Id = "";
        DataService.getListingDetail(transactionObj, function (data) {
            var addressList = [];
            for (var i = 0; i < data.length; i++) {
                var transactionData = JSON.parse(unescape(data[i].TransactionData));
                $scope.getAddress(transactionData.Lead.ContactDetails, function (address) {
                    addressList.push(address)
                    if (i == data.length - 1) {
                        $scope.createMap(addressList);
                    }
                });
            }
        }, function (err) {
            $rootScope.showHideLoadingImage(false);
            $('body').removeClass('loader');
        });
    }
    $scope.createMap = function (addressList) {
        var onSuccess = function (position) {
           
            var map = null;
            var directionsService = null;
            var directionsDisplay = null;
            document.getElementById('map').innerHTML = '';
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 5,
                fullscreenControl: false,
                center: {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                } // need to be configured
            });
            directionsService = new google.maps.DirectionsService;
            directionsDisplay = new google.maps.DirectionsRenderer({
                // draggable: true,
                map: map,
                panel: document.getElementById('right-panel')
            });
            //directionsDisplay.setDirections(null);
            /*directionsDisplay.addListener('directions_changed', function() {
                            						   scope.computeTotalDistance(directionsDisplay.getDirections());
                            					  	});*/
            var origin = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            var destination = addressList.pop();
            var waypoints = [];
            for (var i = 0; i < addressList.length; i++) {
                var point = {
                    location: addressList[i],
                    stopover: true
                };
                waypoints.push(point);
            }
            $scope.displayRoute(origin, destination, directionsService, directionsDisplay, waypoints);
        }
        // onError Callback receives a PositionError object
        function onError(error) {
            $rootScope.showHideLoadingImage(false);
            $('body').removeClass('loader');
            if (error.code == 3) {
                $scope.meassage = "Please check GPS and Net Connectivity and validity of address";
            } else {
                $scope.meassage = error.message;
            }
            $scope.showMap = false;
            $('body').addClass('map-timed-out');
            $scope.$apply();
            $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "checkGPSOrNetConnection"), translateMessages($translate, "ok"), $scope.close);
        }
        navigator.geolocation.getCurrentPosition(onSuccess, onError, {
            maximumAge: 3000,
            timeout: 20000,
            enableHighAccuracy: true
        });
    }
    $scope.close = function () {
        $('body').removeClass('map-timed-out');
    }

    function displayRoute(origin, destination, service, display, waypoints) {
        service.route({
            origin: origin,
            destination: destination,
            waypoints: waypoints,
            optimizeWaypoints: true,
            travelMode: 'DRIVING',
            avoidTolls: true
        }, function (response, status) {
            if (status === 'OK') {
                display.setDirections(response);
                $rootScope.showHideLoadingImage(false);
                $('body').removeClass('loader');
            } else {
                $scope.showMap = false;
                $('body').addClass('map-timed-out');
                $rootScope.showHideLoadingImage(false);
                $('body').removeClass('loader');
                $scope.$apply();
                $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "locationUnidentified"), translateMessages($translate, "ok"), $scope.close);
            }
        });
    };

    function computeTotalDistance(result) {
        var total = 0;
        var myroute = result.routes[0];
        for (var i = 0; i < myroute.legs.length; i++) {
            total += myroute.legs[i].distance.value;
        }
        total = total / 1000;
        document.getElementById('total').innerHTML = total + ' km';
    };

};