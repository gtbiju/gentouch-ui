/*
 Name:LeadListingController
 CreatedDate:1/13/2015
 Description:Contact Management Module.
 */

storeApp.controller('LeadListingController',
		[
				'$rootScope',
				'$scope',
				'$compile',
				'$routeParams',
				'DataService',
				'LmsVariables',
				'LmsService',
				'IllustratorVariables',
				'PersistenceMapping',
				'DocumentService',
				'$translate',
				'$route',
				'UtilityService',
				'globalService',
				'$location',
				'FnaVariables',
				function LeadListingController($rootScope, $scope, $compile,
						$routeParams, DataService, LmsVariables, LmsService,IllustratorVariables,
						PersistenceMapping, DocumentService, $translate,
						$route, UtilityService, globalService, $location, 
						FnaVariables) {

					$scope.leads = "";
					$scope.redHotLeads = [];
					$scope.hotLeads = [];
					$scope.warmLeads = [];
					$scope.coldLeads = [];
					$scope.popupTabs = [ '', '', '', '' ];
					$scope.popupTabs[0] = 'active';

					$rootScope.moduleHeader = "lms.header";
					$rootScope.moduleVariable = translateMessages($translate,
							$rootScope.moduleHeader);
					$scope.sourceArray = LmsVariables.sourceArray;
					$scope.subSourceArray = LmsVariables.subSourceArray;
					$scope.toggleListing = true;
					$scope.filterOpen = false;
					$scope.leadName = "";
					$scope.source = "";
					$scope.subSource = "";
					$scope.agentName = "";
					$scope.contactNumber = "";
					$scope.searchLead = "";
					var isSync = false;
					var nextSyncType;
					var syncWarning = false;
					$scope.tblhead1 = false;
					$scope.tblhead2 = false;
					$scope.tblhead3 = false;
					$scope.tblhead4 = false;
					$scope.tblhead5 = false;
					$scope.tblhead6 = false;
					$scope.predicate = '';
					$scope.prevPredicate = '';
					$scope.reverse = false;
					$scope.toggleButtonName = translateMessages($translate,
							"priorityListingView");
                    $scope.dvDeleteConfirm = false;
                    $scope.deleteValidation = false;
                    $scope.selectedProposal = [];

					$scope.orderValues = function(key, e) {

						if ($scope.predicate == key) {
							$scope.reverse = !($scope.reverse);
						} else {
							$scope.tblhead1 = false;
							$scope.tblhead2 = false;
							$scope.tblhead3 = false;
							$scope.tblhead4 = false;
							$scope.tblhead5 = false;
							$scope.tblhead6 = false;
							$scope.reverse = true;
						}
						$scope.predicate = key;
					};
					$scope.callback = function() {
						$scope.toggleListing = true;
						$rootScope.showHideLoadingImage(false);
						
					};
					$scope.accordion_click = function(index) {
						$scope.accordionIndex = index;
					};
					$scope.onGetListingsSuccess = function(dbObject) {
						if (!(rootConfig.isDeviceMobile)) {
							$scope.leads = dbObject.sort(function(a, b) {
								return LEDate(b.Key14) - LEDate(a.Key14);
							});
						} else {
							$scope.leads = dbObject;
						}

						for (var i = 0; i < $scope.leads.length; i++) {

							var severity = $scope.leads[i].Key18;

							switch (severity) {

							case LmsVariables.SORT_DETAILS.REDHOT:
								$scope.redHotLeads.push($scope.leads[i]);
								break;

							case LmsVariables.SORT_DETAILS.HOT:
								$scope.hotLeads.push($scope.leads[i]);
								break;

							case LmsVariables.SORT_DETAILS.WARM:
								$scope.warmLeads.push($scope.leads[i]);
								break;

							case LmsVariables.SORT_DETAILS.COLD:
								$scope.coldLeads.push($scope.leads[i]);
								break;
							default:
								// Do nothing
							}
						}
						if ($scope.redHotLeads.length <= 0) {
							$scope.redHotLeadsEmpty = true;
						}
						if ($scope.hotLeads.length <= 0) {
							$scope.hotLeadsEmpty = true;
						}
						if ($scope.warmLeads.length <= 0) {
							$scope.warmLeadsEmpty = true;
						}
						if ($scope.coldLeads.length <= 0) {
							$scope.coldLeadsEmpty = true;
						}

						$scope.filterLeads();
						$scope.refresh();
						// $scope.refresh();
					};
					$scope.onGetListingsError = function() {
					$rootScope.showHideLoadingImage(false);
						$rootScope.lePopupCtrl.showError(translateMessages(
								$translate, "lifeEngage"), "Listing Failed",
								translateMessages($translate, "fna.ok"));
						// $rootScope.showMessagePopup(true, "error", "Listing
						// Failed");
					};


					$scope.popupTabsNav = function(rel, index) {
						$("#popupContent").empty();
						for (var i = 0; i < $scope.popupTabs.length; i++) {
							if (i == index) {
								$scope.popupTabs[i] = 'active';
							} else {
								$scope.popupTabs[i] = '';
							}
						}
						LEDynamicUI.paintUI(rootConfig.template,
								"leadListingWithTabs.json", rel,
								"#popupContent", true, function() {
									$scope.callback();
								}, $scope, $compile);
					};
					$scope.filterLeads = function() {
						LEDynamicUI.paintUI(rootConfig.template,
								"leadListingWithTabs.json", "filterSection",
								"#filterSection", true, function() {
									$scope.callback();
								}, $scope, $compile);
					};

					$scope.toggleListingView = function() {
						$scope.toggleListing = !($scope.toggleListing);
						if ($scope.toggleListing) {
							$scope.toggleButtonName = translateMessages(
									$translate, "priorityListingView");
						} else {
							$scope.toggleButtonName = translateMessages(
									$translate, "normalListingView");
						}
						if ($scope.filterOpen == true) {
							$scope.reset();
							$scope.filterOpen = false;
						}

					};

					$scope.translateFilter = function() {

						if ($scope.toggleButtonName == translateMessages(
								$translate, "priorityListingView")) {

							return "priorityListingView";
						} else {

							return "normalListingView";
						}
					};

					$scope.toggleFilterSection = function() {
						$scope.filterOpen = !($scope.filterOpen);
						if ($scope.filterOpen == true) {
							$scope.toggleListing = true;
							$scope.toggleButtonName = translateMessages(
									$translate, "priorityListingView");
						} else {
							$scope.leadName = "";
							$scope.contactNumber = "";
							$scope.source = "";
							$scope.subSource = "";
							$scope.agentName = "";
						}
					};
					
					$scope.hideFilterSection = function() {
						$scope.filterOpen = !($scope.filterOpen);
					};

					$scope.createLead = function() {

						LmsVariables.editMode = false;
						$rootScope.transactionId = 0;
						LmsVariables.clearLmsVariables();
						LmsVariables.setNewLmsModel($rootScope.username);
						$scope.leadHeader = translateMessages($translate,
								"createlead");
						LEDynamicUI.paintUI(rootConfig.template,
								"leadListingWithTabs.json",
								"leadDetailsSection", "#popupContent", true,
								function() {
									$scope.callback();
								}, $scope, $compile);

					};
					$scope.editLead = function(leadID, key) {

						if (leadID) {
							LmsVariables.lmsKeys.Id = leadID;
						} else {
							// Key1 for web
							LmsVariables.lmsKeys.Key1 = key;
						}
						LmsService.getLead(function() {
							LmsVariables.editMode = true;
							$scope.leadHeader = translateMessages($translate,
									"lms.editLead");

							LEDynamicUI.paintUI(rootConfig.template,
									"leadListingWithTabs.json",
									"popupTabsNavbar", "#popupTabs", true,
									function() {
										$scope.callback();
									}, $scope, $compile);
							LEDynamicUI.paintUI(rootConfig.template,
									"leadListingWithTabs.json",
									"leadDetailsSection", "#popupContent",
									true, function() {
										$scope.callback();
									}, $scope, $compile);
						});
					};
                        
                    $scope.syncerror = function(statusMsg,errorMsg) {
                        
                        if(statusMsg == 'SyncError')
                        {
						$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate,errorMsg),translateMessages($translate, "general.ok"),function(){
													$scope.exitApp = true;
													});
                        
                        }

					};
                    
					$scope.closeCreateLead = function() {
						$scope.showLeadPopup = false;
						$scope.popupTabsNav("leadDetailsSection", 0);
						$route.reload();

					};
					$scope.reset = function() {
						$scope.leadName = "";
						$scope.source = "";
						$scope.agentName = "";
						$scope.contactNumber = "";
						$scope.subSource = "";
					};
					$scope.navigateToFna = function(data) {
						try {
							var parties = [];
							var party = new globalService.party();
							party.BasicDetails.firstName = data.Key2;
							party.ContactDetails.homeNumber1 = data.Key4;
							party.type = "Lead";
							parties.push(party);
							globalService.setParties(parties);
							$rootScope.transactionId = 0;
							FnaVariables.clearFnaVariables();// Clear the Fna
																// variables
																// needs to be
																// cleared ,in
																// side this
																// method.
							
							
							FnaVariables.leadId = data.TransTrackingID;
							
							$location.path('/aboutCompany/true/');
						} catch (exception) {
							$rootScope.lePopupCtrl.showError(translateMessages(
									$translate, "lifeEngage"), "Error Output "
									+ exception, translateMessages($translate,
									"fna.ok"));
							//$rootScope.showMessagePopup(true, "error", translateMessages($translate, "Error Output " + exception));
						}
					};
					

				$scope.navigateToIllustration = function(data) {
				
					$scope.illustrationCount = data.illustrationCount;
					
					var parties = [];
					var party = new globalService.party();
					party.BasicDetails.firstName = data.Key2;
					party.ContactDetails.homeNumber1 = data.Key4;
					party.type = "Lead";
					parties.push(party);
					globalService.setParties(parties);
					
					if ($scope.illustrationCount == 0) {
							try {
								
								$rootScope.transactionId = 0;
								IllustratorVariables
										.clearIllustrationVariables();
								if(data.TransTrackingID)
								IllustratorVariables.leadId=data.TransTrackingID;
								
								$location.path("/products/0/0/0/0/false");
							} catch (exception) {
								$rootScope.lePopupCtrl.showError(
										translateMessages($translate,
												"lifeEngage"), "Error Output "
												+ exception, translateMessages(
												$translate, "fna.ok"));
							}
						} else {
							
							var leadId =0;
							if(data.TransTrackingID)
								leadId =data.TransTrackingID;
							$location.path("/MyAccount/Illustration/"+leadId+'/0');
							//  List the Illustration with this leadID
						}

					};
                    
                    $scope.deletePopup = function(type) {
                        $scope.deleteType = type;
                        PersistenceMapping.clearTransactionKeys();
                        $scope.mapKeysforPersistence();
                        
                        $rootScope.lePopupCtrl.showQuestion(translateMessages(
									$translate, "delete"), translateMessages($translate, "deleteLeadWarningText"), translateMessages($translate, "deleteConfirmButtonText"), $scope.deleteClient, translateMessages($translate, "cancelButtonText"), $scope.cancel, '', '', function(){
                            $scope.dvDeleteConfirm = false;
                        });
	               };
                    
                    $scope.mapKeysforPersistence = function() {
                        PersistenceMapping.Key4 = ($routeParams.leadId != null && $routeParams.leadId != 0) ? $routeParams.leadId : "";
                        //PersistenceMapping.Key5 = $scope.productId != null ? $scope.productId : "1";
                        if (isSync) {
                            PersistenceMapping.Key10 = "FullDetails";
                        } else {
                            PersistenceMapping.Key10 = "BasicDetails";
                        }
						if (!rootConfig.isDeviceMobile) {
									PersistenceMapping.creationDate = UtilityService
											.getFormattedDateTime();
									PersistenceMapping.Key13 = UtilityService
											.getFormattedDateTime();
                        }
                        PersistenceMapping.Key15 = $scope.status != null ? $scope.status : "Saved";
                        PersistenceMapping.Type = nextSyncType;
                        //PersistenceMapping.dataIdentifyFlag = false;
	               };
                    
                    $scope.toggleSelection = function(lead) {
                        var idx = $scope.selectedProposal.indexOf(lead);

                        if (idx > -1) {
                            $scope.selectedProposal.splice(idx, 1);
                        }

                        else {
                            $scope.selectedProposal.push(lead);
                        }
	               };
                    
                    $scope.cancel = function(){
                        $scope.dvDeleteConfirm = false;
                        $scope.deleteValidation = false;
                    };
                    
                    $scope.deleteClient = function(){
                        nextSyncType = $scope.deleteType;
                        LmsService.deleteClient($scope.selectedProposal, $scope.onFirstSuccessCallBack, $scope.ondelteError);
                    };
                    
                    $scope.deleteClientValidation = function(){
                        if($scope.selectedProposal.length > 0){
                            var errorFlag = false,
                                escapeFlag = false;
                            for(var y = 0; y < $scope.selectedProposal.length; y++){
                                for(var z = 0; z < rootConfig.disableLeadDeleteStatus.length; z++){
                                    if(rootConfig.disableLeadDeleteStatus[z] == $scope.selectedProposal[y].Key15){
                                        errorFlag = true;
                                        escapeFlag = true;
                                        break;
                                    }
                                }
                                if(escapeFlag){
                                    break;
                                }
                            }
                            if(errorFlag){
                                $rootScope.lePopupCtrl.showError(translateMessages($translate, "info"), translateMessages($translate, "unableToDeleteMsg"), translateMessages($translate, "ok"));
                            } else{
                                $scope.deletePopup('LMS');
                            }
                        }
                    };
                    
                    $scope.onFirstSuccessCallBack = function() {
						  if ((rootConfig.isDeviceMobile)) {
							var whereClause = 'Key1 = '
									+ '"'
									+ $scope.selectedProposal[0].TransTrackingID
									+ '"';
							DataService.deleteFromTable('Transactions',
									whereClause,
									$scope.ondelteSuccessCallBack,
									$scope.ondelteError);
							}
						  else {
						    $scope.ondelteSuccessCallBack();
						  }
					};
                    
                    $scope.ondelteSuccessCallBack = function() {
                        $scope.selectedProposal.splice(0, 1);

                            if ($scope.selectedProposal.length > 0) {
                                $scope.deleteClient();
                            } else {

                                $rootScope.showHideLoadingImage(true, 'deleteMessage', $translate);

                                PersistenceMapping.clearTransactionKeys();
                                var  searchCriteria = {
									 searchCriteriaRequest:{
                                    "command" : "ListingDashBoard",
                                    "modes" :['LMS'],
                                    "value" : ""
                                    }
						          };
                                PersistenceMapping.dataIdentifyFlag = false;
                                var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
                                transactionObj.Type = $scope.deleteType;
                                transactionObj.Key4 = "";
                                transactionObj.Id = "";
                                transactionObj.Key10 = "BasicDetails";
                                transactionObj.filter = "Key15  <> 'Cancelled'";
                                DataService.getFilteredListing(transactionObj,
										$scope.onGetListingsSuccess,
										$scope.onGetListingsError);
                                $rootScope.showHideLoadingImage(false);
                            }
	               };
                    
                    $scope.ondelteError = function() {
                        $rootScope.showHideLoadingImage(false);
                        $scope.refresh();
                    };

					
					$scope.initialLoad= function(){
						LEDynamicUI.paintUI(rootConfig.template,
								"leadListingWithTabs.json", "leadListingWithTabs",
								"#leadListingWithTabs", true, function() {
									$scope.callback();
									PersistenceMapping.clearTransactionKeys();
									 var  searchCriteria = {
										 searchCriteriaRequest:{
								"command" : "ListingDashBoard",
								"modes" :['LMS'],
								"value" : ""
								}
							};
							PersistenceMapping.dataIdentifyFlag = false;
							if (!(rootConfig.isDeviceMobile)) {
								if ($routeParams.leadId == null || $routeParams.leadId == 0) {
									PersistenceMapping.Key13 = UtilityService.getFormattedDateTime();
										PersistenceMapping.creationDate = UtilityService
												.getFormattedDateTime();
								}
							}
							PersistenceMapping.Type='LMS';
							var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
									transactionObj.Id = $routeParams.transactionId;
									transactionObj.Key10 = "Custom";
									transactionObj.Type = "LMS";
									transactionObj.filter = "Key15 <> 'Cancelled'";
									$scope.searchLead=$rootScope.searchValue;
									$rootScope.searchValue="";
									DataService.getFilteredListing(transactionObj,
											$scope.onGetListingsSuccess,
											$scope.onGetListingsError);

								}, $scope, $compile);
					};
					
					
					$scope.$on('$viewContentLoaded', function(){
						$scope.initialLoad();
					});
					
				} ]);