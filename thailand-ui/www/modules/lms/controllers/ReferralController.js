/*
 Name:CallDetailsController
 CreatedDate:20/02/2015
 Description:Lead Management Module.
 */
storeApp
		.controller(
				'ReferralController',
				[
						'$rootScope',
						'$scope',
						'$compile',
						'DataService',
						'$translate',
						'LmsVariables',
						'LmsService',
						'$location',
						'globalService',
						'UtilityService',
						'$timeout',
						'PersistenceMapping',
						'$routeParams',
						function ReferralController($rootScope, $scope,
								$compile, DataService, $translate,
								LmsVariables, LmsService, $location,
								globalService, UtilityService, $timeout,
								PersistenceMapping, $routeParams) {
							$scope.qIndex = 0;
							var isSync = false;

							$scope.LmsModel = "";
							$scope.CarouselClass = function(cIndex) {
								var retClass = "item";
								if ($scope.qIndex != cIndex) {
									retClass = retClass + " hide";
								}
								return retClass;
							};

							$scope.CarouselButtonClass = function(cIndex) {
								retClass = "";
								if ($scope.qIndex == cIndex) {
									retClass = "active";
								}

								return retClass;
							};

							$scope.getPrevNextClasses = function(arrow) {
								var retClass = "";
								switch (arrow) {
								case 'prev': {
									if ($scope.qIndex == 0) {
										retClass = 'disable ';
									}
									retClass = retClass + ' left arrow';
									break;
								}
								case 'next': {
									if ($scope.qIndex == 4) {
										retClass = 'disable ';
									}
									retClass = retClass + ' right arrow';
									break;
								}

								}
								return retClass;
							};
							$scope.ListButtonClass = function(Key1) {
								var retClass = "column_2_btn column_2_delete_btn";
								if ((Key1 != "")
										|| (!(rootConfig.isDeviceMobile))) {
									retClass = "column_2_btn column_2_delete_btn hide";
								}

								return retClass;
							};
							$scope.deleteLead = "";
							$scope.DeleteLead = function(id, thisLead) {
								$scope.deleteLead = thisLead;

								thisLead.Key15 = "Cancelled";
								$scope.BackUpParent();
								LmsVariables.lmsKeys.Id = id;
								LmsService
										.getLead(function() {
											LmsVariables.editMode = true;
											LmsVariables.LmsModel.Lead.StatusDetails.disposition = "Cancelled";
											// LmsService.deleteLead(id,
											// $scope.onDeleteSuccess,
											// $scope.onDeleteError);
											LmsService.saveTransactions(
													$scope.onDeleteSuccess,
													$scope.onSaveError,
													LmsVariables.LmsModel);
										});

							};
							$scope.onDeleteSuccess = function() {
								var idx = $scope.referralLeads
										.indexOf($scope.deleteLead);
								if (idx > -1) {
									$scope.referralLeads.splice(idx, 1);
								}
								idx = $scope.$parent.leads.indexOf($scope.deleteLead);
								if (idx > -1) {
									$scope.$parent.leads.splice(idx, 1);
								}
								$scope.refresh();
								$scope.RestoreParent();
								// $scope.getReferral();
								// $scope.$parent.leads.splice("")
								$rootScope.showHideLoadingImage(false);
								// alert(deleteLead.Key2 + " is Deleted");
							};
							$scope.onDeleteError = function() {

							};
							$scope.nextQuestion = function() {
								if ($scope.qIndex < 4) {
									$scope.qIndex = $scope.qIndex + 1;
								}
							};
							$scope.prevQuestion = function() {
								if ($scope.qIndex > 0) {
									$scope.qIndex = $scope.qIndex - 1;
								}
							};

							$scope.Initialize = function() {
								$scope.leadHeader = "";
								$scope.editMode = LmsVariables.editMode;
								$scope.BackUpParent();
								$scope.LmsModel = new LmsObject();
								if( $scope.LmsModel.Communication.followUps && $scope.LmsModel.Communication.followUps[0]){
								$scope.LmsModel.Communication.followUps[0].followUpsDate = formatForDateControl(new Date());
								$scope.LmsModel.Communication.followUps[0].followUpsTime = formatForTimeControl(
										new Date(), false);
								}
								UtilityService.InitializeErrorDisplay($scope,
										$rootScope, $translate);

								$scope.referralLeads = $scope.$parent.leads;
								$timeout(
										function() {
											$rootScope
													.updateErrorCount('referralsDetailsSection')
										}, 1000);
								$scope.mobnumberPattern = rootConfig.mobnumberPattern;
								$scope.leadNamePattern = rootConfig.leadNamePattern;
								$scope.numberPattern = rootConfig.numberPattern;
							};

							$scope.updateErrorCount = function(arg1, arg2) {

								$rootScope.updateErrorCount(arg1, arg2);

							};
							$scope.showErrorPopup = function(arg1, arg2) {
								$rootScope.showErrorPopup(arg1, arg2);

							};

							$scope.onGetListingsSuccess = function(dbObject) {
								$scope.referralLeads = [];
								$scope.refresh();

							};
							$scope.onGetListingsError = function() {
							$rootScope.showHideLoadingImage(false);
								// $rootScope.showMessagePopup(true, "error",
								// translateMessages($translate, "listing
								// error"));
								$rootScope.lePopupCtrl.showError(
										translateMessages($translate,
												"lifeEngage"),
										"Listing Failed", translateMessages(
												$translate, "fna.ok"));
							};

							$scope.onSaveSuccess = function(data) {

								$scope.refresh();
								$rootScope.NotifyMessages(false, "Success",
										$translate);
								var leadObject = {
									"Id" : "",
									"Key1" : "",
									"Key2" : "",
									"Key4" : "",
									"Key8" : ""
								};
								leadObject.Id = data;
								leadObject.Key2 = $scope.LmsModel.Lead.BasicDetails.fullName;
								leadObject.Key4 = $scope.LmsModel.Lead.ContactDetails.mobileNumber1
										&& $scope.LmsModel.Lead.ContactDetails.mobileNumber1 != "" ? $scope.LmsModel.Lead.ContactDetails.mobileNumber1
										: "";
								leadObject.Key8 = $scope.parentLmsKeys.Key1;

								$scope.$parent.leads.push(leadObject);
								// alert(translateMessages($translate,
								// "referralSaved"));
								$scope.RestoreParent();
								$scope.refresh();

								$scope.Initialize();
								$timeout(
										function() {
											$rootScope
													.updateErrorCount('referralsDetailsSection')
										}, 1000);

								$rootScope.showHideLoadingImage(false);

							};
							$scope.onSaveError = function(msg) {
								$rootScope.showHideLoadingImage(false);
								$scope.refresh();
								$scope.RestoreParent();
								if (msg == "This Trasaction Data is already processed : ") {
									$rootScope.lePopupCtrl.showError(
											translateMessages($translate,
													"lifeEngage"),
											translateMessages($translate,
													"leadAlreadyExists"),
											translateMessages($translate,
													"fna.ok"));
									// $rootScope.showMessagePopup(true,
									// "error", translateMessages($translate,
									// "leadAlreadyExists"));
								} else {
									$rootScope.lePopupCtrl.showError(
											translateMessages($translate,
													"lifeEngage"),
											translateMessages($translate,
													"failure"),
											translateMessages($translate,
													"fna.ok"));
									// $rootScope.showMessagePopup(true,
									// "error", translateMessages($translate,
									// "Failure"));
								}
							};
							$scope.Save = function() {
								if ($scope.parentLmsKeys.Key1 != ""
										&& $scope.parentLmsKeys.Key1 != null) {
									if ($scope.errorCount == 0) {
										$rootScope.showHideLoadingImage(true,
												'paintUIMessage', $translate);
										$rootScope.transactionId = 0;										
										LmsVariables.clearLmsVariables();
										$scope.LmsModel.Lead.AgentInfo.agentId = LmsVariables.LmsModel.Lead.AgentInfo.agentId;
										$scope.LmsModel.Referrals.parentLeadId = angular
												.copy($scope.parentLmsKeys.Key1);
										LmsService.saveTransactions(
												$scope.onSaveSuccess,
												$scope.onSaveError,
												$scope.LmsModel);
									} else {
										$rootScope.lePopupCtrl.showWarning(
												translateMessages($translate,
														"lifeEngage"),
												translateMessages($translate,
														"enterMandatory"),
												translateMessages($translate,
														"fna.ok"));
										//$rootScope.showMessagePopup(true, "warning", translateMessages($translate, "enterMandatory"));
									}
								} else {
									$rootScope.lePopupCtrl.showError(
											translateMessages($translate,
													"lifeEngage"),
											translateMessages($translate,
													"leadSyncMessage"),
											translateMessages($translate,
													"fna.ok"));
									//$rootScope.showMessagePopup(true, "error", translateMessages($translate, "leadSyncMessage"));

								}

							};
							$scope.BackUpParent = function() {
								$scope.parentLmsKeys = JSON.parse(JSON
										.stringify(LmsVariables.lmsKeys));
								$scope.parentLmsModel = JSON.parse(JSON
										.stringify(LmsVariables.getLmsModel()));
								$scope.parentLmsTransTrackingId = LmsVariables.transTrackingID;
								LmsVariables.transTrackingID = "";
							};
							$scope.RestoreParent = function() {
								LmsVariables.lmsKeys = JSON.parse(JSON
										.stringify($scope.parentLmsKeys));
								LmsVariables.setLmsModel(JSON.parse(JSON
										.stringify($scope.parentLmsModel)));
								LmsVariables.transTrackingID = $scope.parentLmsTransTrackingId;
							};
							$scope.$on('$viewContentLoaded', function(){
								$scope.Initialize();
							  });
							
						} ]);