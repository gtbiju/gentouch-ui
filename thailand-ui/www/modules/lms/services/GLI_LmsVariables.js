/*
 * Copyright 2015, LifeEngage 
 */
angular
		.module('lifeEngage.GLI_LmsVariables', [])
		.service(
				"GLI_LmsVariables",['$http','LmsVariables',
				function($http,LmsVariables) {
					LmsVariables.selectedPage="";
					LmsVariables.transTrackingID = "";
					LmsVariables.searchKey=null;
					LmsVariables.actualMeetingDate = "";
					LmsVariables.actualMeetingTime = "";
					LmsVariables.plannedMeetingDate = "";
					LmsVariables.plannedMeetingTime = "";
					LmsVariables.submissionDate = "";
					LmsVariables.totalAPE=0;
					LmsVariables.rmInfo = {};
					LmsVariables.isFromLeadListing = false;
					LmsVariables.lmsKeys= {
						"Id" : "",
						"Key1" : "",
						"Key12" : "",
						"Key13" : "",
                        "Key14":"",
						"Key15" : "",
						"Key25" : "",
					};
					LmsVariables.clearLmsVariables= function() {
						LmsVariables.lmsKeys.Id = "";
						LmsVariables.lmsKeys.Key1 = "";
						LmsVariables.lmsKeys.Key25 = "";
						LmsVariables.transTrackingID = "";
					};
					LmsVariables.setRMInfoModel = function(info) {
							LmsVariables.rmInfo =info;
					};
					LmsVariables.getRMInfoModel = function(info) {
							return LmsVariables.rmInfo;
					};
				}]);