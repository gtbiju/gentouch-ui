/*
 * Copyright 2015, LifeEngage 
 */
//LmsService - To implement Generali KMC functionalities
angular
		.module('lifeEngage.GLI_LmsService',[]).factory(
				"GLI_LmsService",['$http','LmsService','$location', '$translate', 'PersistenceMapping',
						'RuleService', 'globalService', 'LmsVariables', 'DataService',
						'UtilityService','GLI_DataService','$rootScope','UserDetailsService',
				function($http,LmsService,$location, $translate, PersistenceMapping,
						RuleService, globalService, LmsVariables, DataService,
						UtilityService,GLI_DataService,$rootScope,UserDetailsService){
					
					/*
					 * Over writing saveTransactions - changing duplicate check - based on agent code and mobile no:
					 */
					LmsService['saveTransactions'] = function(saveSuccess,
							saveError, lmsModel, isAutoSave) {
						var onSaveSuccess = saveSuccess;
						var onSaveError = saveError;

						onSaveSuccess = function(data) {

							if (saveSuccess) {
								if ((rootConfig.isDeviceMobile)) {
									LmsVariables.lmsKeys.Id = data;
								} else {
									LmsVariables.lmsKeys.Key1 = data.Key1;
								}

								saveSuccess(data);
							}
						};

						onSaveError = function(data) {
							if (saveError) {
								saveError(data);
							}
						};
						/*lmsModel.Communication.followUps[0].dateAndTime = lmsModel.Communication.followUps[0].followUpsDate
								+ " "
								+ formatForTimeControl(
										lmsModel.Communication.followUps[0].followUpsDate
												+ " "
												+ lmsModel.Communication.followUps[0].followUpsTime,
										true);*/
						if ((rootConfig.isDeviceMobile)) {

							var transactionData = {};
							var usrDetails = UserDetailsService.getUserDetailsModel();
							transactionData.Id = LmsVariables.lmsKeys.Id;
							if(usrDetails.agentType=="BranchUser"){
								transactionData.Key23 = lmsModel.Lead.RMInfo.RMNationalID;
							}							
							else {
								transactionData.Key11 = $rootScope.username;
							}
							transactionData.Key24 = LmsVariables.totalAPE;
							transactionData.Key4 = lmsModel.Lead.ContactDetails.mobileNumber1;

							DataService
									.isDuplicateLead(
											transactionData,
											function(isDuplicateLead) {
												if (!isDuplicateLead) {
													PersistenceMapping
															.clearTransactionKeys();
													LmsService
															.mapKeysforPersistence(lmsModel);
													var transactionObj = PersistenceMapping
															.mapScopeToPersistence(lmsModel);

													transactionObj.Key4 = lmsModel.Lead.ContactDetails.mobileNumber1;

							if(usrDetails.agentType=="BranchUser"){
								transactionObj.Key11 = lmsModel.Lead.RMInfo.RMReportingAgentCode;
								transactionObj.Key23=lmsModel.Lead.RMInfo.RMNationalID;
								transactionObj.Key25 = $rootScope.username;
								transactionObj.Key6=lmsModel.Lead.RMInfo.RMName;
							}else{
								transactionObj.Key11 = $rootScope.username;
								transactionObj.Key6 = lmsModel.Referrals.referredByName ? lmsModel.Referrals.referredByName : "";
								if(lmsModel.Lead.SourceDetails.source == 'Referral From Branch' && lmsModel.Referrals.referredByNationalID != "" && lmsModel.Referrals.referredByNationalID != undefined){
									transactionObj.Key23 = lmsModel.Referrals.referredByNationalID ? lmsModel.Referrals.referredByNationalID : "";
									}
								/*
								 * Assigning Key25 on edit lead Banca
								 */
								if(LmsVariables.editMode){
									transactionObj.Key25=LmsVariables.lmsKeys.Key25;
									if(lmsModel.Referrals.referredByBranchName != undefined && lmsModel.Referrals.referredByBranchName != "" && lmsModel.Lead.SourceDetails.source == 'Referral From Branch'){
										transactionObj.Key25 = lmsModel.Referrals.referredByBranchName;
									}
								}else{
									if(lmsModel.Referrals.referredByBranchName != undefined && lmsModel.Referrals.referredByBranchName != "" && lmsModel.Lead.SourceDetails.source == 'Referral From Branch'){
										transactionObj.Key25 = lmsModel.Referrals.referredByBranchName;
									}
								}
							}
									transactionObj.Key21 = lmsModel.Lead.BasicDetails.action ? lmsModel.Lead.BasicDetails.action : "";
									transactionObj.Key22 = lmsModel.Lead.BasicDetails.action ? lmsModel.Lead.BasicDetails.nextAction : "";				
									transactionObj.Key24 = LmsVariables.totalAPE;
                                    transactionObj.Key26 = lmsModel.Lead.BasicDetails.nickName; 
									
									if(lmsModel.Communication[0].interactionType=="Appointment"){
										transactionObj.Key27 = lmsModel.Communication[0].followUps.length;		
									}
									else if(lmsModel.Communication[1].interactionType=="Appointment"){
									transactionObj.Key27 = lmsModel.Communication[1].followUps.length;			
									}
									else if(lmsModel.Communication[2].interactionType=="Appointment"){
									transactionObj.Key27 = lmsModel.Communication[2].followUps.length;			
									}	
									else if(lmsModel.Communication[3].interactionType=="Appointment"){
									transactionObj.Key27 = lmsModel.Communication[3].followUps.length;			
									}		
									
								transactionObj.Key19 = "";
								
								for(var k=0; k<4; k++) {	
									if(lmsModel.Communication[k].interactionType=="Appointment") {
										
										if(lmsModel.Communication[k].followUps.length > 0 ) {
											for(var i=0;i<lmsModel.Communication[k].followUps.length;i++){
										
												var actualMeetingDate=lmsModel.Communication[k].followUps[i].metDate;
												var actualMeetingTime=lmsModel.Communication[k].followUps[i].metTime;
												
												LmsVariables.actualMeetingTime = actualMeetingDate + " " + actualMeetingTime;
												var dateWithSec = actualMeetingDate + " " + actualMeetingTime + ":00";
												
												var _actualDate = Date.parse(dateWithSec);							
												
												var _currentDate = Date.parse(new Date());
												
												if(_actualDate > _currentDate) {
													transactionObj.Key19 += dateWithSec + ",";
												}
											}
										}										
									}
								}
									
														DataService
															.saveTransactions(
																	transactionObj,
																	onSaveSuccess,
																	onSaveError);
												} else {
													onSaveError("This Trasaction Data is already processed : ");

												}
											},
											function(message) {
												alert("Error While Checking duplicate Data");
											});

						} else {
							var usrDetails = UserDetailsService.getUserDetailsModel();
							PersistenceMapping.clearTransactionKeys();
							LmsService.mapKeysforPersistence(lmsModel);
							var transactionObj = PersistenceMapping.mapScopeToPersistence(lmsModel);

							transactionObj.Key4 = lmsModel.Lead.ContactDetails.mobileNumber1;
																	
							if(usrDetails.agentType=="BranchUser"){
								transactionObj.Key11 = lmsModel.Lead.RMInfo.RMReportingAgentCode;
								transactionObj.Key23=lmsModel.Lead.RMInfo.RMNationalID;
								transactionObj.Key25 = $rootScope.username;
								transactionObj.Key6=lmsModel.Lead.RMInfo.RMName;
							}else{
								transactionObj.Key11 = $rootScope.username;
								transactionObj.Key6 = lmsModel.Referrals.referredByName ? lmsModel.Referrals.referredByName : "";
								if(lmsModel.Lead.SourceDetails.source == 'Referral From Branch' && lmsModel.Referrals.referredByNationalID != "" && lmsModel.Referrals.referredByNationalID != undefined){
									transactionObj.Key23 = lmsModel.Referrals.referredByNationalID ? lmsModel.Referrals.referredByNationalID : "";
									}
								/*
								 * Assigning Key25 on edit lead Banca
								 */
								if(LmsVariables.editMode){
									transactionObj.Key25=LmsVariables.lmsKeys.Key25;
									if(lmsModel.Referrals.referredByBranchName != undefined && lmsModel.Referrals.referredByBranchName != "" && lmsModel.Lead.SourceDetails.source == 'Referral From Branch'){
										transactionObj.Key25 = lmsModel.Referrals.referredByBranchName;
									}
								}else{
									if(lmsModel.Referrals.referredByBranchName != undefined && lmsModel.Referrals.referredByBranchName != "" && lmsModel.Lead.SourceDetails.source == 'Referral From Branch'){
										transactionObj.Key25 = lmsModel.Referrals.referredByBranchName;
									}
								}
							}
									transactionObj.Key21 = lmsModel.Lead.BasicDetails.action ? lmsModel.Lead.BasicDetails.action : "";
									transactionObj.Key22 = lmsModel.Lead.BasicDetails.action ? lmsModel.Lead.BasicDetails.nextAction : "";				
									transactionObj.Key24 = LmsVariables.totalAPE;
                                    transactionObj.Key26 = lmsModel.Lead.BasicDetails.nickName;
										
									if(lmsModel.Communication[0].interactionType=="Appointment"){
										transactionObj.Key27 = lmsModel.Communication[0].followUps.length;		
									}
									else if(lmsModel.Communication[1].interactionType=="Appointment"){
									transactionObj.Key27 = lmsModel.Communication[1].followUps.length;			
									}
									else if(lmsModel.Communication[2].interactionType=="Appointment"){
									transactionObj.Key27 = lmsModel.Communication[2].followUps.length;			
									}	
									else if(lmsModel.Communication[3].interactionType=="Appointment"){
									transactionObj.Key27 = lmsModel.Communication[3].followUps.length;			
									}	
									
							DataService.saveTransactions(transactionObj,
									onSaveSuccess, onSaveError);
						}

					};
					
					//Mapping new Keys for persistence
					LmsService.mapKeysforPersistence = function(lmsModel) {

						PersistenceMapping.Key10 = "FullDetails";
						if (!(rootConfig.isDeviceMobile)) {
							if (LmsVariables.lmsKeys.Key1 == null
									|| LmsVariables.lmsKeys.Key1 == 0) {
								PersistenceMapping.Key13 = UtilityService
										.getFormattedDate();
								LmsVariables.lmsKeys.Key13 = UtilityService
										.getFormattedDate();
							} else {
								PersistenceMapping.Key13 = LmsVariables.lmsKeys.Key13;
							}
						}
						PersistenceMapping.Key12 = LmsVariables.lmsKeys.Key12;
						PersistenceMapping.Type = "LMS";
						PersistenceMapping.Key1 = (LmsVariables.lmsKeys.Key1 != 0) ? LmsVariables.lmsKeys.Key1
								: "";
						PersistenceMapping.Key2 = lmsModel.Lead.BasicDetails.fullName ? lmsModel.Lead.BasicDetails.fullName
								: "";
						PersistenceMapping.Key3 = lmsModel.Lead.BasicDetails.lastName ? lmsModel.Lead.BasicDetails.lastName
								: "";
						PersistenceMapping.Key4 = lmsModel.Lead.ContactDetails.mobileNumber1;

						PersistenceMapping.Key5 = lmsModel.Lead.SourceDetails.source;
						PersistenceMapping.Key6 = lmsModel.Lead.SourceDetails.subSource ? lmsModel.Lead.SourceDetails.subSource : "";
						PersistenceMapping.Key7 = lmsModel.Lead.BasicDetails.dob ? lmsModel.Lead.BasicDetails.dob
								: "";
						PersistenceMapping.Key8 = lmsModel.Referrals.parentLeadId;
						
						
						var usrDetails = UserDetailsService.getUserDetailsModel();
						
							if(usrDetails.agentType=="BranchUser"){
								PersistenceMapping.Key11 = lmsModel.Lead.RMInfo.RMReportingAgentCode;
							}else{
								PersistenceMapping.Key11 = lmsModel.Lead.AgentInfo.agentId;
							}
						PersistenceMapping.Key15 = lmsModel.Lead.StatusDetails.disposition;
						PersistenceMapping.Key18 = lmsModel.Lead.SeverityDetails.severity;
						//PersistenceMapping.Key19 = lmsModel.Communication.metDateAndTime;
						
						PersistenceMapping.Key20 = lmsModel.Lead.ContactDetails.emailId ? lmsModel.Lead.ContactDetails.emailId
								: "";
						PersistenceMapping.Key24 = LmsVariables.totalAPESum;
                        PersistenceMapping.Key26 = lmsModel.Lead.BasicDetails.nickName;
						if (LmsVariables.transTrackingID == null
								|| LmsVariables.transTrackingID == 0) {
							PersistenceMapping.TransTrackingID = UtilityService
									.getTransTrackingID();
							LmsVariables.transTrackingID = PersistenceMapping.TransTrackingID;
						} else {
							PersistenceMapping.TransTrackingID = LmsVariables.transTrackingID;
						}
					};
					
					
					//removing invalid mapping
					LmsService.getLead = function(getLeadSuccess, getLeadError) {
						onListingSuccess = function(data) {
							var model;
							if ((rootConfig.isDeviceMobile)) {
								model = JSON
										.parse(unescape(data[0].TransactionData));
							} else {
								model = data[0].TransactionData;
							}
							/*model.Communication.followUps[0].followUpsDate = formatForDateControl(model.Communication.followUps[0].dateAndTime);
							model.Communication.followUps[0].followUpsTime = formatForTimeControl(
									model.Communication.followUps[0].dateAndTime,
									false);
									*/
							// Sorting the questions with questionID
							var questions = [ model.Feedback.questions.length - 1 ];
							for (var i = 0; i < model.Feedback.questions.length; i++) {
								questions[model.Feedback.questions[i].questionID - 1] = model.Feedback.questions[i];
							}
							model.Feedback.questions = questions;

							LmsVariables.setLmsModel(model);
							LmsVariables.lmsKeys.Key12 = data[0].Key12;
							LmsVariables.lmsKeys.Key1 = data[0].Key1;
							LmsVariables.lmsKeys.Key25 = data[0].Key25;
							LmsVariables.lmsKeys.Id = data[0].Id;
							LmsVariables.transTrackingID = data[0].TransTrackingID;
							if (getLeadSuccess) {
								getLeadSuccess(data[0]);
							}

						};
						onListingError = function(data) {
						};

						PersistenceMapping.clearTransactionKeys();
						PersistenceMapping.Key1 = LmsVariables.lmsKeys.Key1;
						PersistenceMapping.Id = LmsVariables.lmsKeys.Id;

						PersistenceMapping.Type = "LMS";
						var transactionObj = PersistenceMapping
								.mapScopeToPersistence();

						DataService.getListingDetail(transactionObj,
								onListingSuccess, onListingError);
					};
					
					 LmsService.deleteClient = function(selectedClient, successCallback, errorCallback){
                        if((rootConfig.isDeviceMobile)){
                            var objectToBeDeleted = {};
                            
                            objectToBeDeleted.Id = selectedClient.Id;
                            

                            objectToBeDeleted.Key15 = 'Cancelled';
                            if(rootConfig.modulesConsideredForDelete.length > 1){
                                objectToBeDeleted.Type = rootConfig.modulesConsideredForDelete.join();
                            } else{
                                objectToBeDeleted.Type = rootConfig.modulesConsideredForDelete[0];
                            }
                        
                            DataService.saveTransactions(objectToBeDeleted, successCallback, errorCallback);
                        } else{
                        	var objectToBeDeleted = {};
							PersistenceMapping.clearTransactionKeys();
							//$scope.mapKeysforPersistence();
							PersistenceMapping.Key1 = selectedClient.Key1;
							PersistenceMapping.Key15 = 'Cancelled';
							objectToBeDeleted.Key1 = selectedClient.Key1;
							objectToBeDeleted.Key15 = 'Cancelled';							
							if (rootConfig.modulesConsideredForDelete.length > 1) {
								objectToBeDeleted.Type = rootConfig.modulesConsideredForDelete
										.join();
							} else {
								objectToBeDeleted.Type = rootConfig.modulesConsideredForDelete[0];
								PersistenceMapping.Type = rootConfig.modulesConsideredForDelete[0];
							}
                            var transactionObj = PersistenceMapping
								.mapScopeToPersistence();
								if(transactionObj.TransTrackingID =="" || typeof transactionObj.TransTrackingID == "undefined"){
								transactionObj.TransTrackingID = LmsVariables.transTrackingID;
								}
							DataService.saveTransactions(transactionObj,
									successCallback, errorCallback);
                        }
                    };
					//Back tab navigation
					LmsService.navigateToListingPage = function(url) {
						$location.path(url);
					};
					
					return LmsService;
				}]);