/*
 * Copyright 2015, LifeEngage 
 */
/*
Name:LmsVariables
CreatedDate:20/02/2015
Description:Lead Management Module.
 */
//DataService for save,retrieve, sysn
angular
		.module('lifeEngage.LmsVariables', [])
		.service(
				"LmsVariables",['$http',
				function($http) {

					var LmsVariables = {
						"getLmsModel" : function() {
							if (!LmsVariables.LmsModel) {
								LmsVariables.lmsKeys.Key1 = "0";
								// Key1 is Lead unique
								// Id
								LmsVariables.lmsKeys.Id = "0";
								return new LmsObject();
							} else {
								var LmsMod = angular.copy(LmsVariables.LmsModel);
								return LmsMod;
							}
						},
						"setLmsModel" : function(value) {
							LmsVariables.LmsModel = value;
						},
						"refLeads" : "",
						"getRefLead" : function() {
							return LmsVariables.refLeads;
						},
						"setRefLead" : function(refLeads) {
							LmsVariables.refLeads = refLeads;
						},
						"setNewLmsModel" : function(userName) {
							LmsVariables.lmsKeys.Key1 = "0";
							// Key1 is Lead unique Id
							LmsVariables.lmsKeys.Id = "0";
							LmsVariables.LmsModel = new LmsObject();
							// LmsVariables.LmsModel.Communication.metDate =
							// formatForDateControl(new Date());
							// LmsVariables.LmsModel.Communication.metTime =
							// formatForTimeControl(new Date());
							//LmsVariables.LmsModel.Communication.followUps[0].followUpsDate = formatForDateControl(new Date());
							//LmsVariables.LmsModel.Communication.followUps[0].followUpsTime = formatForTimeControl(
							//		new Date(), false);
							// alert("--"+LmsVariables.LmsModel.Communication.followUps[0].followUpsTime);

						},
						"clearLmsVariables" : function() {
							LmsVariables.lmsKeys.Id = "";
							LmsVariables.lmsKeys.Key1 = "";
							LmsVariables.transTrackingID = "";
						},
						"lmsKeys" : {
							"Id" : "",
							"Key1" : "",
							"Key12" : "",
							"Key13" : "",
							"Key15" : "",
							"Key3" : "",
							"Key13" : "",
                            "Key14" :"",
							"creationDate" : "",
							"modifiedDate" : ""						
						},
						"SORT_DETAILS" : {
							"WARM" : "Warm",
							"HOT" : "Hot",
							"COLD" : "Cold",
							"REDHOT" : "VeryHot"
						},
						"editMode" : false,
						"sourceArray" : [ {
							"value" : "Source1",
							"translateid" : "lms.source1"
						}, {
							"value" : "Source2",
							"translateid" : "lms.source2"
						}, {
							"value" : "Source3",
							"translateid" : "lms.source3"
						}, {
							"value" : "Source4",
							"translateid" : "lms.source4"
						}, {
							"value" : "Source5",
							"translateid" : "lms.source5"
						} ],
						"subSourceArray" : [ {
							"value" : "Subsource1",
							"translateid" : "lms.subSrc1",
							"sourceId" : "source1"
						}, {
							"value" : "Subsource2",
							"translateid" : "lms.subSrc2",
							"sourceId" : "source1"
						}, {
							"value" : "Subsource3",
							"translateid" : "lms.subSrc3",
							"sourceId" : "source2"
						}, {
							"value" : "Subsource4",
							"translateid" : "lms.subSrc4",
							"sourceId" : "source2"
						}, {
							"value" : "Subsource5",
							"translateid" : "lms.subSrc5",
							"sourceId" : "source3"
						}, {
							"value" : "Subsource6",
							"translateid" : "lms.subSrc6",
							"sourceId" : "source4"
						}, {
							"value" : "Subsource7",
							"translateid" : "lms.subSrc7",
							"sourceId" : "source5"
						} ],
						"dispositionArray" : [ {
							"value" : "Open",
							"translateid" : "lms.open"
						}, {
							"value" : "Approach",
							"translateid" : "lms.approach"
						},{
							"value" : "Appointment",
							"translateid" : "lms.appointment"
						} ,{
							"value" : "FNA",
							"translateid" : "lms.fna"
						} ,{
							"value" : "Solution",
							"translateid" : "lms.solution"
						},{
							"value" : "Submission",
							"translateid" : "lms.submission"
						},{
							"value" : "KIV",
							"translateid" : "lms.kiv"
						} ],
						"subDispositionArray" : [ {
							"value" : "FollowUp",
							"translateid" : "lms.followUp"
						}, {
							"value" : "Interested",
							"translateid" : "lms.interested"
						} ],
						"productSoldArray" : [ {
							"value" : "PensionElite",
							"translateid" : "productName.Pension Elite"
						}, {
							"value" : "LifeForever",
							"translateid" : "productName.Life Forever"
						} ,{
							"value" : "EducationSuperSavings",
							"translateid" : "productName.Education Super Savings"
						} ,{
							"value" : "IncomeForLife",
							"translateid" : "productName.Income For Life"
						} ],
						"severityArray" : [ {
							"value" : "Warm",
							"translateid" : "lms.warm"
						}, {
							"value" : "Hot",
							"translateid" : "lms.hot"
						}, {
							"value" : "RedHot",
							"translateid" : "lms.redHot"
						}, {
							"value" : "Cold",
							"translateid" : "lms.cold"
						} ],
						"interactionArray" : [ {
							"value" : "Face to Face",
							"translateid" : "lms.f2f"
						}, {
							"value" : "PhoneCall",
							"translateid" : "lms.phoneCall"
						},{
							"value" : "Email",
							"translateid" : "lms.email"
						},{
							"value" : "Others",
							"translateid" : "lms.others"
						} ],
						"jointCallWithArray" : [ {
							"value" : "Manager",
							"translateid" : "lms.manager"
						}, {
							"value" : "Senior Manager",
							"translateid" : "lms.seniorManager"
						} ],

						"droppedReasonArray" : [ {
							"value" : "boughtFromCompetition",
							"translateid" : "lms.boughtFromCompetition"
						}, {
							"value" : "noResponse",
							"translateid" : "lms.noResponse"
						}, {
							"value" : "notInterested",
							"translateid" : "lms.notInterested"
						}, {
							"value" : "phSwitchOff",
							"translateid" : "lms.phSwitchOff"
						}, {
							"value" : "shortageOfFunds",
							"translateid" : "lms.shortageOfFunds"
						}, {
							"value" : "wrongNum",
							"translateid" : "lms.wrongNum"
						} ],

						"droppedSubReasonArray" : [ {
							"value" : "reasonOne",
							"translateid" : "lms.reasonOne"
						}, {
							"value" : "reasonTwo",
							"translateid" : "lms.reasonTwo"
						} ],
						"transTrackingID" : ""

					}
					return LmsVariables;
				}]);
/*
 * , { "value" : "closed", "translateid" : "lms.closed" }, { "value" :
 * "dropped", "translateid" : "lms.dropped" }
 */