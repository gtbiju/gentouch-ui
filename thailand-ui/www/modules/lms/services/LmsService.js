/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:LmsService
 CreatedDate:20/02/2015
 Description:Lead Management Module.
 */
angular
		.module('lifeEngage.LmsService', [])
		.factory(
				"LmsService",['$http', '$location', '$translate', 'PersistenceMapping',
						'RuleService', 'globalService', 'LmsVariables', 'DataService',
						'UtilityService',
				function($http, $location, $translate, PersistenceMapping,
						RuleService, globalService, LmsVariables, DataService,
						UtilityService) {
					var LmsService = new Object();

					LmsService.mapKeysforPersistence = function(lmsModel) {

						PersistenceMapping.Key10 = "FullDetails";
						if (!(rootConfig.isDeviceMobile)) {
							if (LmsVariables.lmsKeys.Key1 == null
									|| LmsVariables.lmsKeys.Key1 == 0) {
								PersistenceMapping.creationDate = UtilityService
										.getFormattedDateTime();
								LmsVariables.lmsKeys.creationDate = UtilityService
										.getFormattedDateTime();
										PersistenceMapping.Key13 = UtilityService
										.getFormattedDateTime();
								LmsVariables.lmsKeys.Key13 = UtilityService
										.getFormattedDateTime();
							} else {
								PersistenceMapping.creationDate = LmsVariables.lmsKeys.Key13;
								PersistenceMapping.Key13 = LmsVariables.lmsKeys.Key13;
							}
						}
						PersistenceMapping.Key12 = LmsVariables.lmsKeys.Key12;
						PersistenceMapping.Type = "LMS";
						PersistenceMapping.Key1 = (LmsVariables.lmsKeys.Key1 != 0) ? LmsVariables.lmsKeys.Key1
								: "";
						PersistenceMapping.Key2 = lmsModel.Lead.BasicDetails.fullName ? lmsModel.Lead.BasicDetails.fullName
								: "";
						PersistenceMapping.Key4 = (lmsModel.Lead.ContactDetails.mobileNumber1)?(lmsModel.Lead.ContactDetails.mobileNumber1):"";

						PersistenceMapping.Key5 = lmsModel.Lead.SourceDetails.source;
						PersistenceMapping.Key6 = lmsModel.Lead.SourceDetails.subSource;
						PersistenceMapping.Key8 = lmsModel.Referrals.parentLeadId;
						PersistenceMapping.Key11 = lmsModel.Lead.AgentInfo.agentId;
						PersistenceMapping.Key18 = lmsModel.Lead.SeverityDetails.severity;
						PersistenceMapping.Key19 = lmsModel.Communication.followUps[0].dateAndTime != 'Invalid Date' ? lmsModel.Communication.followUps[0].dateAndTime
								: "";
						PersistenceMapping.Key15 = lmsModel.Lead.StatusDetails.disposition;

						if (LmsVariables.transTrackingID == null
								|| LmsVariables.transTrackingID == 0) {
							PersistenceMapping.TransTrackingID = UtilityService
									.getTransTrackingID();
							LmsVariables.transTrackingID = PersistenceMapping.TransTrackingID;
						} else {
							PersistenceMapping.TransTrackingID = LmsVariables.transTrackingID;
						}
					};

					LmsService.saveTransactions = function(saveSuccess,
							saveError, lmsModel, isAutoSave) {
						var onSaveSuccess = saveSuccess;
						var onSaveError = saveError;

						onSaveSuccess = function(data) {

							if (saveSuccess) {
								if ((rootConfig.isDeviceMobile)) {
									LmsVariables.lmsKeys.Id = data;
								} else {
									LmsVariables.lmsKeys.Key1 = data.Key1;
								}

								saveSuccess(data);
							}
						};

						onSaveError = function(data) {
							if (saveError) {
								saveError(data);
							}
						};
						lmsModel.Communication.followUps[0].dateAndTime = lmsModel.Communication.followUps[0].followUpsDate
								+ " "
								+ formatForTimeControl(
										lmsModel.Communication.followUps[0].followUpsDate
												+ " "
												+ lmsModel.Communication.followUps[0].followUpsTime,
										true);
						if ((rootConfig.isDeviceMobile)) {

							var transactionData = {};

							transactionData.Id = LmsVariables.lmsKeys.Id;
							transactionData.Key2 = lmsModel.Lead.BasicDetails.fullName;
							PersistenceMapping.Key4 = (lmsModel.Lead.ContactDetails.mobileNumber1)?(lmsModel.Lead.ContactDetails.mobileNumber1):"";

							DataService
									.isDuplicateLead(
											transactionData,
											function(isDuplicateLead) {
												if (!isDuplicateLead) {
													PersistenceMapping
															.clearTransactionKeys();
													LmsService
															.mapKeysforPersistence(lmsModel);
													var transactionObj = PersistenceMapping
															.mapScopeToPersistence(lmsModel);

													PersistenceMapping.Key4 = (lmsModel.Lead.ContactDetails.mobileNumber1)?(lmsModel.Lead.ContactDetails.mobileNumber1):"";

													DataService
															.saveTransactions(
																	transactionObj,
																	onSaveSuccess,
																	onSaveError);
												} else {
													onSaveError("This Trasaction Data is already processed : ");

												}
											},
											function(message) {
												alert("Error While Checking duplicate Data");
											});

						} else {

							PersistenceMapping.clearTransactionKeys();
							LmsService.mapKeysforPersistence(lmsModel);
							var transactionObj = PersistenceMapping
									.mapScopeToPersistence(lmsModel);
							PersistenceMapping.Key4 = (lmsModel.Lead.ContactDetails.mobileNumber1)?(lmsModel.Lead.ContactDetails.mobileNumber1):"";

							DataService.saveTransactions(transactionObj,
									onSaveSuccess, onSaveError);
						}

					};
					LmsService.getLead = function(getLeadSuccess, getLeadError) {
						onListingSuccess = function(data) {
							var model;
							if ((rootConfig.isDeviceMobile)) {
								model = JSON
										.parse(unescape(data[0].TransactionData));
							} else {
								model = data[0].TransactionData;
							}
							model.Communication.followUps[0].followUpsDate = formatForDateControl(model.Communication.followUps[0].dateAndTime);
							model.Communication.followUps[0].followUpsTime = formatForTimeControl(
									model.Communication.followUps[0].dateAndTime,
									false);
							// Sorting the questions with questionID
							var questions = [ model.Feedback.questions.length - 1 ];
							for (var i = 0; i < model.Feedback.questions.length; i++) {
								questions[model.Feedback.questions[i].questionID - 1] = model.Feedback.questions[i];
							}
							model.Feedback.questions = questions;

							LmsVariables.setLmsModel(model);
							LmsVariables.lmsKeys.Key12 = data[0].Key12;
							LmsVariables.lmsKeys.Key1 = data[0].Key1;
							LmsVariables.lmsKeys.creationDate=data[0].creationDate;
							LmsVariables.lmsKeys.Key13=data[0].Key13;
							LmsVariables.lmsKeys.modifiedDate=data[0].modifiedDate;
                            LmsVariables.lmsKeys.Key14=data[0].Key14;
							LmsVariables.transTrackingID = data[0].TransTrackingID;
							if (getLeadSuccess) {
								getLeadSuccess();
							}

						};
						onListingError = function(data) {
						};

						PersistenceMapping.clearTransactionKeys();
						PersistenceMapping.Key1 = LmsVariables.lmsKeys.Key1;
						PersistenceMapping.Id = LmsVariables.lmsKeys.Id;

						PersistenceMapping.Type = "LMS";
						var transactionObj = PersistenceMapping
								.mapScopeToPersistence();

						DataService.getListingDetail(transactionObj,
								onListingSuccess, onListingError);
					};
					LmsService.deleteLead = function(id, getLeadSuccess,
							getLeadError) {
						if ((rootConfig.isDeviceMobile)) {
							var transactionObjDeleted = {};
							transactionObjDeleted.Id = id;
							transactionObjDeleted.Key15 = "Cancelled";
							DataService.saveTransactions(transactionObjDeleted,
									getLeadSuccess, getLeadError);
						}
					};
                    
                    LmsService.deleteClient = function(selectedClient, successCallback, errorCallback){
                        if((rootConfig.isDeviceMobile)){
                            var objectToBeDeleted = {};
                            
                            objectToBeDeleted.Id = selectedClient[0].Id;
                            

                            objectToBeDeleted.Key15 = 'Cancelled';
                            if(rootConfig.modulesConsideredForDelete.length > 1){
                                objectToBeDeleted.Type = rootConfig.modulesConsideredForDelete.join();
                            } else{
                                objectToBeDeleted.Type = rootConfig.modulesConsideredForDelete[0];
                            }
                        
                            DataService.saveTransactions(objectToBeDeleted, successCallback, errorCallback);
                        } else{
                        	var objectToBeDeleted = {};
							PersistenceMapping.clearTransactionKeys();
							//$scope.mapKeysforPersistence();
							PersistenceMapping.Key1 = selectedClient[0].Key1;
							PersistenceMapping.Key15 = 'Cancelled';
							objectToBeDeleted.Key1 = selectedClient[0].Key1;
							objectToBeDeleted.Key15 = 'Cancelled';							
							if (rootConfig.modulesConsideredForDelete.length > 1) {
								objectToBeDeleted.Type = rootConfig.modulesConsideredForDelete
										.join();
							} else {
								objectToBeDeleted.Type = rootConfig.modulesConsideredForDelete[0];
								PersistenceMapping.Type = rootConfig.modulesConsideredForDelete[0];
							}
                            var transactionObj = PersistenceMapping
								.mapScopeToPersistence();
							transactionObj.creationDate=PersistenceMapping.creationDate;
							transactionObj.Key13=PersistenceMapping.Key13;
							DataService.saveTransactions(transactionObj,
									successCallback, errorCallback);
                        }
                    };
                    
					return LmsService;
				}]);