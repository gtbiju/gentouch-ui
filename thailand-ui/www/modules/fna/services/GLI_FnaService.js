/*
 * Copyright 2015, LifeEngage 
 */
//FnaService - To implement Generali FNA functionalities
angular
		.module('lifeEngage.GLI_FnaService',[]).factory(
				"GLI_FnaService",
				function($http,FnaService,$location, $translate, PersistenceMapping,
						RuleService, globalService, FnaVariables,SyncService, UserDetailsService,
           				UtilityService){
// Changes done by LETeam << change starts here
					this.evaluateString = function($scope,value) {
                              var val;

                               var variableValue = $scope;
                               if (value.indexOf('.') != -1) {
                                   val = value.split('.');
                                   if (val[0] == '$scope' || val[0] == 'scope') {
                                       val.shift();
                                   }
                                   for (var i in val) {
                                                if(typeof variableValue != 'undefined'){
                                                       if (val[i].indexOf('[') != -1) {
                                                              var indexValue = val[i].substring(val[i].indexOf('[') + 1, val[i].indexOf(']'));
                                                              var arrayObject = val[i].split('[');
                                                              if (variableValue[arrayObject[0]].hasOwnProperty(indexValue)) {
                                                                     variableValue = variableValue[arrayObject[0]][indexValue];
                                                              } else if (!$.isNumeric(value) && typeof $scope[indexValue] != 'undefined') {
                                                                     var indexEval = $scope[indexValue];
                                                                     variableValue = variableValue[arrayObject[0]][indexEval];
                                                              } else {
                                                                     variableValue = variableValue[arrayObject[0]][indexValue];
                                                              }

                                                       } else {
                                                              variableValue = variableValue[val[i]];
                                                       }
                                                }
                                   }
                               } else {
                                   variableValue = value;
                               }
                               return variableValue;
                           };
// Changes done by LETeam << change endstarts here										
					
							FnaService.goalCalculation = function(carrierId,
									goalObject, successCallback, errorCallback,
									UtilityService) {
		
								if (carrierId && successCallback && errorCallback) {
		
									var RuleName;
									var FunctionName;
									var inputJson;
									try {
										// Call rule engine and execute rule
										var transactionObj = {};
										for (var i = 0; i < goalObject.parameters.length; i++) {
											transactionObj[goalObject.parameters[i].name] = goalObject.parameters[i].value;
										}
										transactionObj.myselfDob = goalObject.myselfDob;
										if(goalObject.educationChildNameSlider != "" && typeof goalObject.educationChildNameSlider != "undefined"){
											transactionObj.educationChildNameSlider = goalObject.educationChildNameSlider;
										}
										transactionObj.defaultunit = goalObject.defaultunit;
		
										RuleService.calculateGoal(transactionObj,
												goalObject, successCallback,
												errorCallback);
		
									} catch (exception) {
										alert("Error Output " + exception);
										errorCallback(exception);
									}
								} else {
									alert("service was not invoked correctly");
								}
		
							};
							
							/**
							 * Method to validate the entered text for typehead.
							 * 
							 */

							FnaService.validateSmartSearch = function(value, list){

								if ((list != undefined && value!=undefined) && (list != undefined && value!="")){
									for ( var i = 0 ; i < list.length ; i++ ){
										if (value.toLowerCase() == list[i].longDesc.toLowerCase()){
											return list[i].value;
										}
									}	
								 }
								 return false;
							};
							
							FnaService.mapKeysforPersistence = function(scope) {

								PersistenceMapping.Key1 = FnaVariables.leadId;
								PersistenceMapping.Key2 = (FnaVariables.fnaId != "" && FnaVariables.fnaId != 0) ? FnaVariables.fnaId
										: "";
								PersistenceMapping.Key4 = (scope.proposalId != null && scope.proposalId != 0) ? scope.proposalId
										: "";
								PersistenceMapping.Key5 = scope.productId != null ? scope.productId
										: "1";
								PersistenceMapping.Key10 = "FullDetails";
                                // FNA changes by LE team.- Starts here							     
                                if (scope.FNAObject.FNA.parties[0].BasicDetails.currentDate == null || scope.FNAObject.FNA.parties[0].BasicDetails.currentDate == undefined || scope.FNAObject.FNA.parties[0].BasicDetails.currentDate == "") {
									PersistenceMapping.creationDate = UtilityService.getFormattedDate();
									PersistenceMapping.Key13 = UtilityService.getFormattedDate();
									
									scope.FNAObject.FNA.parties[0].BasicDetails.currentDate = formatForDateControl(UtilityService.getFormattedDate());
								}
                                 if ((scope.FNAObject.FNA.parties[0].BasicDetails.creationDate != undefined || scope.FNAObject.FNA.parties[0].BasicDetails.creationDate != null) && (PersistenceMapping.creationDate != undefined || PersistenceMapping.creationDate != null) ) {
									PersistenceMapping.creationDate = scope.FNAObject.FNA.parties[0].BasicDetails.creationDate;
									PersistenceMapping.Key13 = scope.FNAObject.FNA.parties[0].BasicDetails.Key13;
								}
								
                                // FNA changes by LE team.- Ends here
								for (var i = 0; i < scope.FNAObject.FNA.parties.length; i++) {
									if (scope.FNAObject.FNA.parties[i].type == "FNAMyself") {
										PersistenceMapping.Key6 = scope.FNAObject.FNA.parties[i].BasicDetails.firstName + ' ' + scope.FNAObject.FNA.parties[i].BasicDetails.lastName;
										var dateOFBirth = "";
										if(scope.FNAObject.FNA.parties[i].BasicDetails.dob && scope.FNAObject.FNA.parties[i].BasicDetails.dob != "" ){
											dateOFBirth = scope.FNAObject.FNA.parties[i].BasicDetails.dob
										}
										PersistenceMapping.Key7 = dateOFBirth;										
										//PersistenceMapping.Key7 = formatForDateControl(scope.FNAObject.FNA.parties[i].BasicDetails.dob);
								        if(scope.FNAObject && scope.FNAObject.FNA.parties[i] && scope.FNAObject.FNA.parties[i].ContactDetails && scope.FNAObject.FNA.parties[i].ContactDetails.homeNumber1){
										PersistenceMapping.Key8 = (scope.FNAObject.FNA.parties[i].ContactDetails.homeNumber1)?(scope.FNAObject.FNA.parties[i].ContactDetails.homeNumber1):"";
									 }
									if(scope.FNAObject && scope.FNAObject.FNA.parties[i] && scope.FNAObject.FNA.parties[i].ContactDetails && scope.FNAObject.FNA.parties[i].ContactDetails.emailId )
									{
										PersistenceMapping.Key20 = scope.FNAObject.FNA.parties[i].ContactDetails.emailId;
									}


									}
								}

								if (!eval(rootConfig.isDeviceMobile)
										&& (FnaVariables.fnaId == null || FnaVariables.fnaId == 0)) {
									PersistenceMapping.Key13 = UtilityService
											.getFormattedDate();
								}
								PersistenceMapping.Key12 = FnaVariables.fnaKeys.Key12;
								 PersistenceMapping.Key15 = FnaVariables.fnaKeys.Key15 != "" ? FnaVariables.fnaKeys.Key15 
			                                : scope.status != null ? scope.status  : FnaVariables.getStatusOptionsFNA()[0].status;
								PersistenceMapping.Type = "FNA";

								PersistenceMapping.Key16 = scope.syncStatus != null ? scope.syncStatus
										: "";
								if (FnaVariables.transTrackingID == null
										|| FnaVariables.transTrackingID == 0) {
									PersistenceMapping.TransTrackingID = UtilityService
											.getTransTrackingID();
									FnaVariables.transTrackingID = PersistenceMapping.TransTrackingID;
								} else {
									PersistenceMapping.TransTrackingID = FnaVariables.transTrackingID;
								}
								PersistenceMapping.Key22 = FnaVariables.fnaKeys.Key22;
								PersistenceMapping.Key23 = FnaVariables.fnaKeys.Key23;
							};
							
							FnaService.initalizeProgressbar = function($scope, currentPageType, pageSelections) {
								var tabIndex ;
								for (var j = 0; j < pageSelections.length; j++) {
									if (pageSelections[j].pageType == currentPageType) {
										tabIndex = j;
										pageSelections[j].isSelected = "selected";
										pageSelections[j].isDisabled = "";
									} else {
										pageSelections[j].isSelected = "";
										if( tabIndex >= 0 && j > tabIndex && UtilityService.disableProgressTab == true){
											pageSelections[j].isDisabled = "disabled";
										}
									}
								}
								$scope.FNAObject.FNA.pageSelections = pageSelections;	
								FnaVariables.setFnaModel($scope.FNAObject);
								$scope.$broadcast('ProgressbarReload');
								$scope.refresh();
							};
							
							FnaService.saveTransactions = function($scope, $rootScope,
									DataService, $translate, UtilityService, isAutoSave) {
								this.save = function(successCallback, errorCallback) {
									/* FNA Changes by LE Team Bug 3800 - starts */ 
									if (FnaVariables.transactionID !== undefined && FnaVariables.transactionID !== "") {
										$rootScope.transactionId = FnaVariables.transactionID;
									}
									/* FNA Changes by LE Team Bug 3800 - ends */ 
									if (!isAutoSave) {
										$rootScope.showHideLoadingImage(true,
												"saveTransactionMessage", $translate);
									} else {// To set the fna parties for autosave
										if ($scope.FNAMyself) {
											globalService.setParty($scope.FNAMyself,
													"FNAMyself");
										}
										if ($scope.FNABeneficiaries) {
											globalService
													.setFNABeneficiaries($scope.FNABeneficiaries);
										}
										var parties = globalService.getParties();
										$scope.FNAObject.FNA.parties = parties;
									}
									// - On editing a transaction after synching, the
									// syncStatus will be saved as �NotSynced�
									if (eval(rootConfig.isDeviceMobile)
											&& !(FnaVariables.fnaId == null || FnaVariables.fnaId == 0)) {
										$scope.syncStatus = "NotSynced";
									}
									if (FnaVariables.fnaKeys.Key13 != null && FnaVariables.fnaKeys.Key13 != "") {
										$scope.FNAObject.FNA.parties[0].BasicDetails.creationDate = FnaVariables.fnaKeys.Key13;
										$scope.FNAObject.FNA.parties[0].BasicDetails.Key13 = FnaVariables.fnaKeys.Key13;
										FnaVariables.fnaKeys.Key13 = null;
									}
									PersistenceMapping.clearTransactionKeys();
									FnaService.mapKeysforPersistence($scope);
									
									/*dob getting one day less issue fix -converted date to string before save
									  Its because angular.toJson will take the UTC time and convert if dob is in object*/
									if ($scope.FNAObject.FNA.parties[0].BasicDetails.creationDate == undefined) {
										$scope.FNAObject.FNA.parties[0].BasicDetails.creationDate = UtilityService.getFormattedDate();
										$scope.FNAObject.FNA.parties[0].BasicDetails.Key13 = UtilityService.getFormattedDate();
									}
									var transactionObj = PersistenceMapping
											.mapScopeToPersistence($scope.FNAObject.FNA);
									globalService.setParties($scope	.FNAObject.FNA.parties);
									
									$scope.onSaveSuccess = function(data) {

										if (eval(rootConfig.isDeviceMobile)) {
											if (data != null) {
												$rootScope.transactionId = ($rootScope.transactionId == null || $rootScope.transactionId == 0) ? data
														: $rootScope.transactionId;
											}
											if (!isAutoSave) {
												$rootScope
														.NotifyMessages(
																false,
																"saveTransactionsSuccessOffline",
																$translate);
											}

											if ((successCallback && typeof successCallback == "function")) {
												successCallback();
											}
											/* FNA Changes by LE Team Bug 3800 - starts */ 
											FnaVariables.transactionID = data;
											/* FNA Changes by LE Team Bug 3800 - ends */ 
										} else {
											$scope.proposalId = data.Key4;
											FnaVariables.fnaId = data.Key2;
											/* FNA Changes by LE Team Bug 3800 - starts */ 
											FnaVariables.transactionID = data.Key2;
											/* FNA Changes by LE Team Bug 3800 - ends */ 
											$scope.syncStatus = "Synced";
											if (!isAutoSave) {
												$rootScope.NotifyMessages(false,
														"saveTransactionsSuccess",
														$translate, $scope.proposalId);
											}
											if ((errorCallback && typeof errorCallback == "function")) {
												errorCallback();
											}
											if ((successCallback && typeof successCallback == "function")) {
												successCallback();
											}
										}

										$scope.refresh();

										$rootScope.showHideLoadingImage(false);
										$scope.refresh();
									};
									$scope.onSaveError = function(data) {
										if (!isAutoSave) {
											$rootScope.NotifyMessages(true, data);
										}
										$scope.isSTPSuccess = false;
										$scope.refresh();
										$rootScope.showHideLoadingImage(false);
									};
									DataService.saveTransactions(transactionObj,
											$scope.onSaveSuccess, $scope.onSaveError);
								};

							};
				            FnaService.downloadFNAReport = function($scope, DataService, successCallback, errorCallback) {
				                var templates = {};
				                templates.fnaPdfTemplate = rootConfig.fnaPDFTemplate;

				                PersistenceMapping.clearTransactionKeys();
				                FnaService.mapKeysforPersistence($scope);
				                $scope.FNAObject.FNA.templates = templates;
				                var transactionObj = PersistenceMapping
				                    .mapScopeToPersistence($scope.FNAObject.FNA);

				                var objsToSync = [];
				                objsToSync.push(transactionObj.Id);


				                $scope.onDownloadPDF = function(fnaId) {
				                    PersistenceMapping.clearTransactionKeys();
				                    var transactionObjForPDF = PersistenceMapping.mapScopeToPersistence({});
				                    transactionObjForPDF.Type = "FNA";
				                    transactionObjForPDF.Key2 = fnaId;

				                    DataService.downloadPDF(transactionObjForPDF, rootConfig.fnaPDFTemplate, function(data) {
				                        successCallback();
				                    }, function(error) {
				                        errorCallback();
				                    });
				                };
				                DataService.saveTransactions(transactionObj, function(data) {
				                    if (eval(rootConfig.isDeviceMobile)) {
				                        if (checkConnection()) {

				                            if (objsToSync.length > 0) {
				                                var syncConfig = [{
				                                    "module": "FNA",
				                                    "operation": "syncTo",
				                                    "selectedIds": objsToSync
				                                }];
				                                //call selected sync                                                                                
				                                SyncService.initialize(syncConfig,
				                                    function(syncFailed, syncWarning) {
				                                        if (!syncFailed && !syncWarning) {

				                                            var dataObj = {};
				                                            dataObj.Key16 = "Synced";
				                                            DataService.updateTransactionAfterSync(dataObj, transactionObj.Id,
				                                                function() {

				                                                    if (transactionObj.Key2 == "" || transactionObj.Key2 == undefined) {
				                                                        dataObj = {};
				                                                        dataObj.Id = transactionObj.Id;
				                                                        DataService.fetchAll(dataObj, "Transactions", function(data) {
				                                                                FnaVariables.fnaId = data[0].Key2;
				                                                                FnaVariables.fnaKeys.Key12 = data[0].Key12;
				                                                                $scope.onDownloadPDF(data[0].Key2);
				                                                            },
				                                                            function(error) {
				                                                                errorCallback();
				                                                            }
				                                                        );
				                                                    } else {
				                                                        $scope.onDownloadPDF(transactionObj.Key2);

				                                                    }

				                                                },
				                                                function(error) {
				                                                    errorCallback();
				                                                });
				                                        } else {
				                                            errorCallback();
				                                        }

				                                    });
				                                SyncService.runSyncThread();

				                            }
				                        } else {
				                            errorCallback(true);
				                        }

				                    } else {
				                        FnaVariables.fnaId = data.Key2;
				                        FnaVariables.fnaKeys.Key12 = data.Key12;
				                        $scope.onDownloadPDF(data.Key2);
				                    }

				                }, errorCallback);
				            };
				            FnaService.saveEmailTransactions = function($scope, $rootScope, $translate, emailObj, DataService, successCallback, errorCallback) {
				                var templates = {};
				                templates.fnaPdfTemplate = rootConfig.fnaPDFTemplate;
								$scope.FNAObject.FNA.Email = emailObj;
				                $scope.FNAObject.FNA.templates = templates;
				                PersistenceMapping.clearTransactionKeys();
								if (!eval(rootConfig.isDeviceMobile)) {
								 	FnaVariables.fnaKeys.Key15  = FnaVariables.getStatusOptionsFNA()[2].status;
								}
				                FnaService.mapKeysforPersistence($scope);
				                var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.FNAObject.FNA);
				                transactionObj.Key18 = true;
				                var objsToSync = [];
				                objsToSync.push(transactionObj.Id);
				                if (eval(rootConfig.isDeviceMobile) && !checkConnection()) {
								 	FnaVariables.fnaKeys.Key15  = FnaVariables.getStatusOptionsFNA()[2].status;
								}
                                var transactiondataobj = transactionObj.TransactionData;
				                DataService.saveTransactions(transactionObj, function(data) {
				                    if (rootConfig.online_emailFNA) {
				                        var syncConfig = [{
				                            "module": "FNA",
				                            "operation": "syncTo",
				                            "selectedIds": objsToSync
				                        }];
				                      if (eval(rootConfig.isDeviceMobile)) {
				                        if(checkConnection()){
				                            if (objsToSync.length > 0) {
				                                //call selected sync                                                                                
				                                SyncService.initialize(syncConfig,
				                                    function(syncFailed, syncWarning) {
				                                        if (!syncFailed && !syncWarning) {
				                                            //Call Email Service to mail FNA Report.                                
				                                            PersistenceMapping.clearTransactionKeys();
				                                            //FnaService.mapKeysforPersistence();
				                                            var transactionObjForMailReq = PersistenceMapping.mapScopeToPersistence({});
                                                            transactionObjForMailReq.TransactionData = transactiondataobj;
				                                            transactionObjForMailReq.Type = "FNA";
				                                            var userDetails = UserDetailsService.getUserDetailsModel();
				                                            UtilityService.emailServiceCall(userDetails, transactionObjForMailReq);
				                                            //Email Service to mail FNA Report.

				                                            var dataObj = {};
				                                            dataObj.Key18 = "";
				                                            dataObj.Key16 = "Synced";
															dataObj.Key15 = FnaVariables.getStatusOptionsFNA()[2].status;
															FnaVariables.fnaKeys.Key15  = FnaVariables.getStatusOptionsFNA()[2].status;
				                                            DataService.updateTransactionAfterSync(dataObj, transactionObj.Id,
				                                                function() {
				                                                    dataObj = {};
				                                                    dataObj.Id = transactionObj.Id;
				                                                    DataService.fetchAll(dataObj, "Transactions", function(data) {
				                                                            FnaVariables.fnaId = data[0].Key2;
				                                                            FnaVariables.fnaKeys.Key12 = data[0].Key12;
																			FnaService.mapKeysforPersistence($scope);
				                                                            successCallback(true);
																			//errorCallback();
				                                                        },
				                                                        function(error) {
				                                                           
				                                                        }
				                                                    );

				                                                },
				                                                function(error) {
				                                                  
				                                                });
				                                        } else {
				                                            errorCallback();
				                                        }

				                                    });
				                                SyncService.runSyncThread();
				                            }
				                        }else{
				                        	 transactionObj.Key18 = true;
				                             var obj = new FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
				                             obj.save(function(isAutoSave) {
				                            	 //alert("email placed");
				                            	 successCallback();
				                             			},function(error) { 
				                             					errorCallback(); 
				                             });
				                           } 
				                        } else {
				                            	FnaVariables.fnaId = data.Key2;
										FnaVariables.fnaKeys.Key12 = data.Key12;
										//Call Email Service to mail FNA Report.                                
										PersistenceMapping.clearTransactionKeys();
										//FnaService.mapKeysforPersistence();
										var transactionObjForMailReq = PersistenceMapping.mapScopeToPersistence({});
                                        transactionObjForMailReq.TransactionData = transactiondataobj;    
										transactionObjForMailReq.Type = "FNA";
										var userDetails = UserDetailsService.getUserDetailsModel();
										UtilityService.emailServiceCall(userDetails, transactionObjForMailReq);
										//Email Service to mail FNA Report.
										
										successCallback(true);
				                        }
				                    } else {
				                        successCallback();
				                    }
				                }, errorCallback);

				            };
					
					return FnaService;
				});