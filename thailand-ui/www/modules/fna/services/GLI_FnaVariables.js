angular
		.module('lifeEngage.GLI_FnaVariables', [])
		.service(
				"GLI_FnaVariables",
				function($http,FnaVariables) {
					FnaVariables.stausFilter = "";
					FnaVariables.searchFilter = "";
					FnaVariables.choosePartyStatus = false;
					FnaVariables.myselfMandatoryFields = {};
					FnaVariables.choosepartyMandatoryFields = {};
					FnaVariables.transactionID = "";
					/*FNA changes by LE Team >>> starts - Bug 4194*/
					FnaVariables.insuredFromFNA = [];
					FnaVariables.flagFromCustomerProfileFNA = false;
					FnaVariables.initialLoadSIFromFNA = false;
					/*FNA changes by LE Team >>> ends - Bug 4194*/
					return FnaVariables;
				});