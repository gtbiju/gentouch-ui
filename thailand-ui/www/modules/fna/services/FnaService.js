angular
		.module('lifeEngage.FnaService', [])
		.factory(
				"FnaService",
				function($http, $location, $translate, PersistenceMapping,
						RuleService, globalService, FnaVariables,  SyncService, UserDetailsService,
           				UtilityService) {
					var FnaService = new Object();

					FnaService.mapKeysforPersistence = function(scope) {

						PersistenceMapping.Key1 = FnaVariables.leadId;
						PersistenceMapping.Key2 = (FnaVariables.fnaId != "" && FnaVariables.fnaId != 0) ? FnaVariables.fnaId
								: "";
						PersistenceMapping.Key4 = (scope.proposalId != null && scope.proposalId != 0) ? scope.proposalId
								: "";
						PersistenceMapping.Key5 = scope.productId != null ? scope.productId
								: "1";
						PersistenceMapping.Key10 = "FullDetails";

						for (var i = 0; i < scope.FNAObject.FNA.parties.length; i++) {
							if (scope.FNAObject.FNA.parties[i].type == "FNAMyself") {
								PersistenceMapping.Key6 = scope.FNAObject.FNA.parties[i].BasicDetails.firstName + ' ' + scope.FNAObject.FNA.parties[i].BasicDetails.lastName;
								var dateOFBirth = "";
								if(scope.FNAObject.FNA.parties[i].BasicDetails.dob && scope.FNAObject.FNA.parties[i].BasicDetails.dob != "" ){
									dateOFBirth = formatForDateControl(scope.FNAObject.FNA.parties[i].BasicDetails.dob)
								}
								PersistenceMapping.Key7 = dateOFBirth;
						        if(scope.FNAObject && scope.FNAObject.FNA.parties[i] && scope.FNAObject.FNA.parties[i].ContactDetails && scope.FNAObject.FNA.parties[i].ContactDetails.homeNumber1){
								PersistenceMapping.Key8 = (scope.FNAObject.FNA.parties[i].ContactDetails.homeNumber1)?(scope.FNAObject.FNA.parties[i].ContactDetails.homeNumber1):"";
							 }
							if(scope.FNAObject && scope.FNAObject.FNA.parties[i] && scope.FNAObject.FNA.parties[i].ContactDetails && scope.FNAObject.FNA.parties[i].ContactDetails.emailId )
							{
								PersistenceMapping.Key20 = scope.FNAObject.FNA.parties[i].ContactDetails.emailId;
							}


							}
						}
										
							if($rootScope.transactionId==null || $rootScope.transactionId==0 || typeof $rootScope.transactionId=='undefined'){
								PersistenceMapping.creationDate = UtilityService
											.getFormattedDateTime();
								FnaVariables.fnaKeys.creationDate=UtilityService
											.getFormattedDateTime();
											PersistenceMapping.Key13 = UtilityService
											.getFormattedDateTime();
								FnaVariables.fnaKeys.Key13=UtilityService
											.getFormattedDateTime();
							}else{
								PersistenceMapping.creationDate=FnaVariables.fnaKeys.creationDate;
								PersistenceMapping.Key13=FnaVariables.fnaKeys.Key13;
						}
												
						PersistenceMapping.Key12 = FnaVariables.fnaKeys.Key12;
						 PersistenceMapping.Key15 = scope.status != null ? scope.status
	                                : FnaVariables.fnaKeys.Key15 != "" ? FnaVariables.fnaKeys.Key15: FnaVariables.getStatusOptionsFNA()[0].status;
						PersistenceMapping.Type = "FNA";

						PersistenceMapping.Key16 = scope.syncStatus != null ? scope.syncStatus
								: "";
						if (FnaVariables.transTrackingID == null
								|| FnaVariables.transTrackingID == 0) {
							PersistenceMapping.TransTrackingID = UtilityService
									.getTransTrackingID();
							FnaVariables.transTrackingID = PersistenceMapping.TransTrackingID;
						} else {
							PersistenceMapping.TransTrackingID = FnaVariables.transTrackingID;
						}
					};
					FnaService.goalCalculation = function(carrierId,
							goalObject, successCallback, errorCallback,
							UtilityService) {

						if (carrierId && successCallback && errorCallback) {

							var RuleName;
							var FunctionName;
							var inputJson;
							try {
								// Call rule engine and execute rule
								var transactionObj = {};
								for (var i = 0; i < goalObject.parameters.length; i++) {
									transactionObj[goalObject.parameters[i].name] = goalObject.parameters[i].value;
								}
								transactionObj.myselfDob = goalObject.myselfDob;

								RuleService.calculateGoal(transactionObj,
										goalObject, successCallback,
										errorCallback);

							} catch (exception) {
								alert("Error Output " + exception);
								errorCallback(exception);
							}
						} else {
							alert("service was not invoked correctly");
						}

					};
					
					FnaService.checkValidationFNAtoBI = function(dataNew, successCallback, errorCallback,
							UtilityService) {

						if ((!dataNew == "") && successCallback && errorCallback) {

							var RuleName;
							var FunctionName;
							var inputJson;
							try {
								// Call rule engine and execute rule
								var transactionObj = {};
								transactionObj = dataNew;

								RuleService.checkValidationFNAtoBI(transactionObj, successCallback,
										errorCallback);

							} catch (exception) {
								alert("Error Output " + exception);
								errorCallback(exception);
							}
						} else {
							alert("service was not invoked correctly");
						}

					};

					FnaService.saveTransactions = function($scope, $rootScope,
							DataService, $translate, UtilityService, isAutoSave) {
						this.save = function(successCallback, errorCallback) {
							if (!isAutoSave) {
								$rootScope.showHideLoadingImage(true,
										"saveTransactionMessage", $translate);
							} else {// To set the fna parties for autosave
								if ($scope.FNAMyself) {
									globalService.setParty($scope.FNAMyself,
											"FNAMyself");
								}
								if ($scope.FNABeneficiaries) {
									globalService
											.setFNABeneficiaries($scope.FNABeneficiaries);
								}
								var parties = globalService.getParties();
								$scope.FNAObject.FNA.parties = parties;
							}
							// - On editing a transaction after synching, the
							// syncStatus will be saved as 'NotSynced'
							if (eval(rootConfig.isDeviceMobile)
									&& !(FnaVariables.fnaId == null || FnaVariables.fnaId == 0)) {
								$scope.syncStatus = "NotSynced";
							}
							PersistenceMapping.clearTransactionKeys();
							FnaService.mapKeysforPersistence($scope);

							var transactionObj = PersistenceMapping
									.mapScopeToPersistence($scope.FNAObject.FNA);

							$scope.onSaveSuccess = function(data) {

								if (eval(rootConfig.isDeviceMobile)) {
									if (data != null) {
										$rootScope.transactionId = ($rootScope.transactionId == null || $rootScope.transactionId == 0) ? data
												: $rootScope.transactionId;
									}
									if (!isAutoSave) {
										$rootScope
												.NotifyMessages(
														false,
														"saveTransactionsSuccessOffline",
														$translate);
									}

									if ((successCallback && typeof successCallback == "function")) {
										successCallback();
									}

								} else {
									$scope.proposalId = data.Key4;
									FnaVariables.fnaId = data.Key2;
									$scope.syncStatus = "Synced";
									if (!isAutoSave) {
										$rootScope.NotifyMessages(false,
												"saveTransactionsSuccess",
												$translate, $scope.proposalId);
									}
									if ((errorCallback && typeof errorCallback == "function")) {
										errorCallback();
									}
									if ((successCallback && typeof successCallback == "function")) {
										successCallback();
									}
								}

								$scope.refresh();

								$rootScope.showHideLoadingImage(false);
								$scope.refresh();
							};
							$scope.onSaveError = function(data) {
								if (!isAutoSave) {
									$rootScope.NotifyMessages(true, data);
								}
								$scope.isSTPSuccess = false;
								$scope.refresh();
								$rootScope.showHideLoadingImage(false);
							};
							DataService.saveTransactions(transactionObj,
									$scope.onSaveSuccess, $scope.onSaveError);
						};

					};
					FnaService.saveEmailTransactions = function($scope, emailObj, DataService, successCallback, errorCallback) {
                var templates = {};
                templates.fnaPdfTemplate = rootConfig.fnaPDFTemplate;
				$scope.FNAObject.FNA.Email = emailObj;
                $scope.FNAObject.FNA.templates = templates;
				
                PersistenceMapping.clearTransactionKeys();
                FnaService.mapKeysforPersistence($scope);
                var transactionObj = PersistenceMapping
                    .mapScopeToPersistence($scope.FNAObject.FNA);
                transactionObj.Key18 = true;
                var objsToSync = [];
                objsToSync.push(transactionObj.Id);

                DataService.saveTransactions(transactionObj, function() {
                    if (rootConfig.online_emailFNA) {
                        var syncConfig = [{
                            "module": "FNA",
                            "operation": "syncTo",
                            "selectedIds": objsToSync
                        }];
                        if (eval(rootConfig.isDeviceMobile) && checkConnection()) {
                            if (objsToSync.length > 0) {
                                //call selected sync                                                                                
                                SyncService.initialize(syncConfig,
                                    function(syncFailed, syncWarning) {
                                        if (!syncFailed && !syncWarning) {
                                            //Call Email Service to mail FNA Report.                                
                                            PersistenceMapping.clearTransactionKeys();
                                            //FnaService.mapKeysforPersistence();
                                            var transactionObjForMailReq = PersistenceMapping.mapScopeToPersistence({});
                                            transactionObjForMailReq.Type = "FNA";
                                            var userDetails = UserDetailsService.getUserDetailsModel();
                                            UtilityService.emailServiceCall(userDetails, transactionObjForMailReq);
                                            //Email Service to mail FNA Report.

                                            var dataObj = {};
                                            dataObj.Key18 = "";
                                            dataObj.Key16 = "Synced";
                                            DataService.updateTransactionAfterSync(dataObj, transactionObj.Id,
                                                function() {
                                                    dataObj = {};
                                                    dataObj.Id = transactionObj.Id;
                                                    DataService.fetchAll(dataObj, "Transactions", function(data) {
                                                            FnaVariables.fnaId = data[0].Key2;
                                                            FnaVariables.fnaKeys.Key12 = data[0].Key12;
                                                            successCallback(true);
                                                        },
                                                        function(error) {
                                                           
                                                        }
                                                    );

                                                },
                                                function(error) {
                                                  
                                                });
                                        } else {
                                            errorCallback();
                                        }

                                    });
                                SyncService.runSyncThread();
                            }
                        } else {
                            successCallback();
                        }
                    } else {
                        successCallback();
                    }
                }, errorCallback);

            };
			FnaService.downloadPdfwithFlash = function($scope,
							DataService){
						var base64data="";
						
						PersistenceMapping.clearTransactionKeys();
						FnaService.mapKeysforPersistence($scope);
						PersistenceMapping.dataIdentifyFlag = false;
						
						var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.FNAObject.FNA);
												
						DataService.downloadBase64(transactionObj,
									function(response){
							
										base64data = response.data.Response.ResponsePayload.Transactions[0].base64pdf;
										Downloadify.create('downloadify',{
											dataType: 'base64',
											filename: function(){
												return "FNAReport_" + transactionObj.Key2 + ".pdf";
											},
											data:function(){
												return base64data;
											},
											onComplete: function(){
												//alert('Your File Has Been Saved!');
											},
											onCancel: function(){ 
												//alert('You have cancelled the saving of this file.');
											},
											onError: function(){
												//alert('You must put something in the File Contents or there will be nothing to save!'); 
											},
											swf: './lib/vendor/downloadplugin/downloadify.swf', 
											downloadImage: './css/main-theme/img/download.png',
											width: 100,
											height: 30,
											transparent: true,
											append: false
										});
									},
								function(err){
										//err
									
						});
						
					};
            FnaService.downloadFNAReport = function($scope, DataService, successCallback, errorCallback) {
                var templates = {};
                templates.fnaPdfTemplate = rootConfig.fnaPDFTemplate;

                PersistenceMapping.clearTransactionKeys();
                FnaService.mapKeysforPersistence($scope);
                $scope.FNAObject.FNA.templates = templates;
                var transactionObj = PersistenceMapping
                    .mapScopeToPersistence($scope.FNAObject.FNA);

                var objsToSync = [];
                objsToSync.push(transactionObj.Id);


                $scope.onDownloadPDF = function(fnaId) {
                    PersistenceMapping.clearTransactionKeys();
                    var transactionObjForPDF = PersistenceMapping.mapScopeToPersistence({});
                    transactionObjForPDF.Type = "FNA";
                    transactionObjForPDF.Key2 = fnaId;

                    DataService.downloadPDF(transactionObjForPDF, rootConfig.fnaPDFTemplate, function(data) {
                        successCallback();
                    }, function(error) {
                        errorCallback();
                    });
                };
                DataService.saveTransactions(transactionObj, function(data) {
                    if (eval(rootConfig.isDeviceMobile)) {
                        if (checkConnection()) {

                            if (objsToSync.length > 0) {
                                var syncConfig = [{
                                    "module": "FNA",
                                    "operation": "syncTo",
                                    "selectedIds": objsToSync
                                }];
                                //call selected sync                                                                                
                                SyncService.initialize(syncConfig,
                                    function(syncFailed, syncWarning) {
                                        if (!syncFailed && !syncWarning) {

                                            var dataObj = {};
                                            dataObj.Key16 = "Synced";
                                            DataService.updateTransactionAfterSync(dataObj, transactionObj.Id,
                                                function() {

                                                    if (transactionObj.Key2 == "" || transactionObj.Key2 == undefined) {
                                                        dataObj = {};
                                                        dataObj.Id = transactionObj.Id;
                                                        DataService.fetchAll(dataObj, "Transactions", function(data) {
                                                                FnaVariables.fnaId = data[0].Key2;
                                                                FnaVariables.fnaKeys.Key12 = data[0].Key12;
                                                                $scope.onDownloadPDF(data[0].Key2);
                                                            },
                                                            function(error) {
                                                                errorCallback();
                                                            }
                                                        );
                                                    } else {
                                                        $scope.onDownloadPDF(transactionObj.Key2);

                                                    }

                                                },
                                                function(error) {
                                                    errorCallback();
                                                });
                                        } else {
                                            errorCallback();
                                        }

                                    });
                                SyncService.runSyncThread();

                            }
                        } else {
                            errorCallback(true);
                        }

                    } else {
                        FnaVariables.fnaId = data.Key2;
                        FnaVariables.fnaKeys.Key12 = data.Key12;
                        $scope.onDownloadPDF(data.Key2);
                    }

                }, errorCallback);
            };

					return FnaService;

				});
