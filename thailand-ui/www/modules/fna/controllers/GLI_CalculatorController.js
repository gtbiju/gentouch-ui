/*
 *Copyright 2015, LifeEngage 
 */




/*Name:GLI_CalculatorController
 * For implementing GLI specific changes
 */
storeApp.controller('GLI_CalculatorController',['$timeout', 'PersistenceMapping', 'GLI_FnaService', '$rootScope', '$scope', '$location','$compile', '$routeParams', 'DataService', 'LookupService', 'DocumentService','FnaVariables', 'FnaService', '$translate', 'UtilityService', '$debounce','AutoSave', 'globalService','$controller',
function GLI_CalculatorController($timeout, PersistenceMapping, GLI_FnaService, $rootScope, $scope, $location,$compile, $routeParams, DataService, LookupService, DocumentService,FnaVariables, FnaService, $translate, UtilityService, $debounce,AutoSave, globalService,$controller){
$controller('CalculatorController',{$timeout:$timeout, PersistenceMapping:PersistenceMapping, GLI_FnaService:GLI_FnaService, $rootScope:$rootScope, $scope:$scope, $location:$location,$compile:$compile, $routeParams:$routeParams, DataService:DataService, LookupService:LookupService, DocumentService:DocumentService,FnaVariables:FnaVariables, FnaService:FnaService, $translate:$translate, UtilityService:UtilityService, $debounce:$debounce,AutoSave:AutoSave, globalService:globalService});
$scope.namePattern = rootConfig.namePattern;
$scope.leadNewNamePattern = rootConfig.leadNewNamePattern;
$scope.status = FnaVariables.getStatusOptionsFNA()[0].status;
$scope.newVal = "K";
$rootScope.selectedPage = "Calculator";
$scope.currentPageType  = "Need Analysis";
$scope.s1BachelorDegree = translateMessages($translate,"fna.educationS1");
//Age Calculation
$scope.uniquePengingGoalsCount = 0;
var g_currentlySelected = [];
$scope.changeAll = function(newVal, event) {
	$scope.newVal = newVal;
	var thisObj = event.currentTarget;
	g_currentlySelected[0] = thisObj;
	var thisObjDeno = $(thisObj).text();
	/*
	 * if (thisObjDeno == "K") {
	 * $('.unit_main_btn').removeClass("selected");
	 * $('.unit_main_btn').eq(0).addClass("selected"); } if (thisObjDeno ==
	 * "L") { $('.unit_main_btn').removeClass("selected");
	 * $('.unit_main_btn').eq(1).addClass("selected"); } if (thisObjDeno ==
	 * "C") { $('.unit_main_btn').removeClass("selected");
	 * $('.unit_main_btn').eq(2).addClass("selected"); }
	 */
	$scope.goalObject = $scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex];
	$scope.goalObject.sliderUnit = newVal;
	
	/*if ($scope.goalObject.parameters != undefined) {
		for (var i = 0; i < $scope.goalObject.parameters.length; i++) {
			if ($scope.goalObject.parameters[i] && $scope.goalObject.parameters[i].unit  && $scope.goalObject.parameters[i].unit != "NA") {
				$scope.goalObject.parameters[i].textBoxValue = Number($scope.goalObject.parameters[i].value)
						/ Number(FnaVariables.unitList[newVal]);
				$scope.goalObject.parameters[i].unit = newVal;
			}

		}
	}
	*/
	$scope.getGoalUnit(newVal,function()
	{
		if($scope.goalObject.result){
			if($scope.goalObject.result.ruleExecutionOutput){
				if($scope.goalObject.result.ruleExecutionOutput != "" && typeof $scope.goalObject.result.ruleExecutionOutput != "undefined"){
					$scope.calculateGoal();
				}
			}
		}
		$scope.errorMessages = [];
		var id2 = '#' + id + ' .truePattern' + ":not([class*='ng-hide'])";
		$(id2).each(
				function(index, item) {
					var error = {};
					error.message = translateMessages($translate, $(this).attr(
							'errormessage'));
					error.key = $(this).attr('errorkey');
					$scope.errorMessages.push(error);
				});

		var id3 = '#' + $scope.viewName + ' .trueValid'
						+ ":not([class*='ng-hide'])";
		$(id3).each(
				function(index, item) {
					var error = {};
					var messageTemp = translateMessages($translate, $(this)
							.attr('errormessage'));
					var maxLength = translateMessages($translate, $(this).attr(
							'maxlength'));
					var minLength = translateMessages($translate, $(this).attr(
							'minlength'));
					var unit = $(this).attr('unit');
					var unitModel = ((unit).replace(/FNAObject/g,
							"$scope.FNAObject"));
					var min;
					var max;
					if (eval(unitModel) != "NA") {
						min = Number(minLength)
								/ Number($scope.unitList[eval(unitModel)]);
						max = Number(maxLength)
								/ Number($scope.unitList[eval(unitModel)]);
						error.message = messageTemp + ' ' + min + ' and' + ' '
								+ max + eval(unitModel);
					} else {
						min = Number(minLength) + 1;
						max = maxLength;
						error.message = messageTemp + ' ' + min + ' and' + ' '
								+ max;
					}
					error.key = $(this).attr('errorkey');
					$scope.errorMessages.push(error);
				});
	});
	
	
	
			
}
	$scope.getViewName = function(successCallback) {
	for (var j = 0; j < $scope.goals.length; j++) {
			if ($scope.selectedGoals[$scope.FNAObject.FNA.activeGoalIndex].goalName == $scope.goals[j].goalName) {
				$scope.viewName = $scope.goals[j].goalCalculationUITemplate;
				$scope.refresh();
			}
		}
	successCallback();
	}
	$scope.showDetail = function(index) {
		$scope.modalCalcVisibility=true;
		var priority = Number(index) + 1;
		$scope.FNAObject.FNA.activeGoalIndex = Number(index);
		$scope.highlightedGoal = $scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].priority;
		$scope.goalHeader = $scope.selectedGoals[index].translateID;
		if ($scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].result != undefined) {
			$scope.showCalculations = true;
			$scope.goalruleResults = $scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].result.ruleExecutionOutput[0];
		} else {
			$scope.goalruleResults = [];
			$scope.showCalculations = false;
		}
		$scope.getViewName(function() {
			$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
			$scope.refresh();
			$timeout(function(){
                   $("#" + $scope.viewName).empty();
			LEDynamicUI.paintUI(rootConfig.template,
				FnaVariables.uiJsonFile.goalViewsFile, $scope.viewName, "#"
						+ $scope.viewName, true, function() {
					$scope.callback();
				}, $scope, $compile);         
                        });
			
		});
	}

$scope.calculateGoal = function() {
	$rootScope
			.showHideLoadingImage(true, 'calculationProgress', $translate);
	$scope.refresh();
	var carrierId = 1;
	var goalObject = $scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex];
//	goalObject.myselfDob = $scope.FNAObject.FNA.parties[0].BasicDetails.dob;
	goalObject = $scope.setRuleInputs(goalObject);
	UtilityService.disableProgressTab = true;
	$rootScope.updateErrorCount($scope.viewName);
	if ($scope.errorCount == 0) {
		try {
			PersistenceMapping.clearTransactionKeys();
			$scope.mapKeysforPersistence();
			// var transactionObj = PersistenceMapping
			// .mapScopeToPersistence({});
			GLI_FnaService.goalCalculation(carrierId, goalObject, function(
					output) {
				getProductSuccess(output, goalObject);
			}, function(output) {
				$scope.getProductFailure(output);
			});
		} catch (exception) {
			alert("Error Output " + exception);
			$scope.getProductFailure(exception);
			$rootScope.showHideLoadingImage(false);
		}
	} else {
		//FNA changes by LE Team
		if($scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].goalName == rootConfig.savings){
			$scope.errorMessage = "fna.validationErrorAccumulation";
		}else if($scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].goalName == rootConfig.education){
			$scope.errorMessage = "fna.validationErrorEducation";
		}
				$scope.isDisplay = false;
		//$scope.errorMessage = "fna.validationError";
		$rootScope.lePopupCtrl.showError(translateMessages($translate,
				"lifeEngage"), translateMessages($translate,
				$scope.errorMessage), translateMessages($translate,
				"fna.ok"));
		$rootScope.showHideLoadingImage(false);
	}
	GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
	//$scope.refresh();
}

$scope.setRuleInputs  = function(goalObject) {
	goalObject.myselfDob = $scope.FNAObject.FNA.parties[0].BasicDetails.dob;
	/* if(goalObject.goalName == rootConfig.education){
		goalObject.educationChildNameSlider = $scope.FNAObject.FNA.parties[0].BasicDetails.firstName;
	} */
	if($scope.newVal == "" || typeof $scope.newVal == "undefined"){
		goalObject.defaultunit = Number(FnaVariables.unitList[Object.keys(FnaVariables.unitList)[0]]);
	}else{
		goalObject.defaultunit = Number(FnaVariables.unitList[$scope.newVal]);
	}
	//$scope.newVal = "";
	return goalObject;

}

function getProductSuccess(data, goalObject) {

	$scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].result = data;
	$rootScope.showHideLoadingImage(false);

	if ($scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].result != undefined) {
		$scope.showCalculations = true;
		$scope.goalruleResults = $scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].result.ruleExecutionOutput[0];
	} else {
		$scope.goalruleResults = [];
		$scope.showCalculations = false;
	}
	
	
	$scope.calculatePendingGoals();
	var obj = new GLI_FnaService.saveTransactions($scope, $rootScope,
			DataService, $translate, UtilityService, false);
	obj.save();
	$scope.refresh();

	// $rootScope.updateErrorCount("BeneficiaryDetailsSubTab");
	$rootScope.updateErrorCount($scope.viewName);
}

$scope.calculatePendingGoals = function() {
	$scope.pengingGoals = " ";
	$scope.uniquePengingGoals = " ";
	$scope.uniquePengingGoalsCount = 0;

	var count = 0;
	var count2 = 0;
	for (var i = 0; i < $scope.FNAObject.FNA.goals.length; i++) {
		if ($scope.FNAObject.FNA.goals[i].result == undefined) {
			count++;
		}
	}
	if (count != 0) {
		var pend = "";
		var pendingItems = "";
		for (var i = 0; i < $scope.FNAObject.FNA.goals.length; i++) {
			if ($scope.FNAObject.FNA.goals[i].result == undefined) {
				count2++;
				if (count == 1) {
					pend = pend + "'"
							+ $scope.FNAObject.FNA.goals[i].goalName + "'.";
					
					pendingItems = pendingItems 
					+ $scope.FNAObject.FNA.goals[i].goalName + $scope.FNAObject.FNA.goals[i].priority;
					$scope.uniquePengingGoalsCount++;
				} else if (count2 != count) {
					pend = pend + "'"
							+ $scope.FNAObject.FNA.goals[i].goalName
							+ "', ";
					
					pendingItems = pendingItems 
					+ $scope.FNAObject.FNA.goals[i].goalName + $scope.FNAObject.FNA.goals[i].priority
					+ ", ";
					$scope.uniquePengingGoalsCount++;
				} else {
					pend = pend + "and " + "'"
							+ $scope.FNAObject.FNA.goals[i].goalName + "'.";
					
					pendingItems = pendingItems + "and " 
					+ $scope.FNAObject.FNA.goals[i].goalName + $scope.FNAObject.FNA.goals[i].priority;
					$scope.uniquePengingGoalsCount++;
				}
			}
		}
		$scope.pengingGoals = pend;
		$scope.uniquePengingGoals = pendingItems;
	}
}
$scope.calculatePendingGoals();
//FNA changes by LE Team
//Calculating age in years, month , days
$scope.ageCalculation = function(currDate, dobDate) {
	date1 = new Date(currDate);
        date2 = new Date(dobDate);
        var y1 = date1.getFullYear(),
            m1 = date1
            .getMonth(),
            d1 = date1.getDate(),
            y2 = date2
            .getFullYear(),
            m2 = date2.getMonth(),
            d2 = date2
            .getDate();

        var nextDOB;
        if (m1 < m2) {
            nextDOB = new Date(date2.setYear(date1
                .getFullYear()));
        } else if ((m1 == m2) && (d1 < d2)) {
            nextDOB = new Date(date2.setYear(date1
                .getFullYear()));
        } else {
            nextDOB = new Date(date2.setYear(date1
                .getFullYear() + 1));
        }
        var marginDate = new Date(date1.setMonth(date1
            .getMonth() + 6));

        if (m1 < m2) {
            y1--;
            m1 += 12;
        }

        if (nextDOB.getTime() > marginDate.getTime()) {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() -
                birthDate.getFullYear();
            var mnth = today.getMonth() -
                birthDate.getMonth();
            if (mnth < 0 ||
                (mnth === 0 && today.getDate() < birthDate
                    .getDate())) {
                ageVal--;
            }
            return [ageVal, "false"];
        } else {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() -
                birthDate.getFullYear();
            var mnth = today.getMonth() -
                birthDate.getMonth();
            if (mnth < 0 ||
                (mnth === 0 && today.getDate() < birthDate
                    .getDate())) {
                ageVal--;
            }
            return [ageVal, "true"];
        }

}



$scope.calculateAge = function() {
	
	var dob = $scope.FNAMyself.BasicDetails.dob;
	if (dob != "" && dob != undefined
			&& dob != null) {
		var dobDate = new Date(dob);
		/*
		 * In I.E it will accept date format in '/'
		 * separator only
		 */
		if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
			dobDate = new Date(dob.split("-").join(
					"/"));
		}
		var todayDate = new Date();
		if (formatForDateControl(dob) >= formatForDateControl(new Date())) {
			$scope.FNAMyself.BasicDetails.age = 0;
			$scope.FNAObject.FNA.parties[0].BasicDetails.age = 0;
			$scope.FNAMyself.BasicDetails.dob = formatForDateControl(new Date());
		} else {
			var curd = new Date(todayDate
					.getFullYear(), todayDate
					.getMonth(), todayDate
					.getDate());
			var cald = new Date(dobDate
					.getFullYear(), dobDate
					.getMonth(), dobDate.getDate());
			var dife = $scope.ageCalculation(curd,
					cald);
			var age = "";
// FNA changes by LE Team Start
            if (age < 1) {
                    var datediff = curd.getTime() -
                        cald.getTime();
                    var days = (datediff / (24 * 60 * 60 * 1000)) / 365;
                    days = Number(Math.round(days +
                            'e3') +
                        'e-3');
                    age = 0;
                }
// FNA changes by LE Team End
			if (dife[1] == "false") {
				age = dife[0];
			} else {
				age = dife[0] + 1;
			}

			if (dob == undefined || dob == "") {
				age = "-";
			}
			$scope.FNAMyself.BasicDetails.age = age;
			$scope.FNAObject.FNA.parties[0].BasicDetails.age = age;
		}
	}
}
$scope.calculateAge();
$scope.$on('initiate-save', function (evt, obj) {
	//$scope.disableLePopCtrl = true;
	FnaVariables.setFnaModel($scope.FNAObject);
	$scope.status = "Draft";
	var obj = new GLI_FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
	obj.save(function() {
		$rootScope.showHideLoadingImage(false, "");
		$rootScope.passwordLock  = false;
		$rootScope.isAuthenticated = false;
		$location.path('/login');
	});
});
//over writing function to rename status  initial to draft 
$scope.nextPage = function() {
	$scope.pendingGoalsList = [];
	var isFirsPriorityCalculated = true;
	var isCalculated;
	var isSelected = $scope.FNAObject.FNA.goals.length;
	$scope.disableTab();
	if ($scope.FNAObject.FNA.goals[0].result == undefined) {
		var name = {};
		isFirsPriorityCalculated = false;
		name.translateID = $scope.FNAObject.FNA.goals[0].translateID;
		$scope.pendingGoalsList.push(name);
	}

	if (isFirsPriorityCalculated) {
		for (var i = 0; i < isSelected; i++) {
			var name = {};
			if ($scope.FNAObject.FNA.goals[i].result == undefined) {
				name.translateID = $scope.FNAObject.FNA.goals[i].translateID;
				$scope.pendingGoalsList.push(name);
			}
		}
	}

	isCalculated = Number(isSelected)
			- Number($scope.pendingGoalsList.length);
	if (isCalculated == 0 || !isFirsPriorityCalculated) {
		$('#proceedButton').hide();
	}
	if (isCalculated == isSelected) {
		for (var i = 0; i < FnaVariables.pages.length; i++) {
			if (FnaVariables.pages[i].url == "/fnasummary") {
				$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
				break;
			}
		}

		FnaVariables.setFnaModel($scope.FNAObject);
		$scope.status = "Draft";
		var obj = new GLI_FnaService.saveTransactions($scope, $rootScope,
				DataService, $translate, UtilityService, false);
		// obj.save();
		obj.save(function() {
			$location.path('/fnasummary');
		});
	} else if (isFirsPriorityCalculated == false) {
		$scope.isDisplay = true;
		$scope.popup_setpref_container = true;
		$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "fna.primaryGoalCalculationMesage"), translateMessages($translate, "illustrator.okMessageInCaps"));

	} else if (isCalculated > 0) {
		$('#proceedButton').show();
		$scope.isDisplay = true;
		$scope.popup_setpref_container = true;	
		
		var totalgoalsCount = $scope.FNAObject.FNA.goals.length;
		var selected_goals = totalgoalsCount - $scope.uniquePengingGoalsCount;
		
		var digitsToWordsArray = ["one","two","three","four","five","six","seven","eight"];
		var digitsToWordsPendingArray = ["One","Two","Three","Four","Five","Six","Seven","Eight"];
		var pendingGoalCountInWords = digitsToWordsPendingArray[$scope.uniquePengingGoalsCount - 1 ];
		var selectedGoalCountInWords = digitsToWordsArray[selected_goals - 1];
		//
		$scope.errorMessage = translateMessages( $translate, "fna.pendingGoalsMessagePart1") +
		translateMessages( $translate, selectedGoalCountInWords )+ 
		translateMessages( $translate, "fna.pendingGoalsMessagePart2") +
		translateMessages( $translate, pendingGoalCountInWords )+ 
		translateMessages( $translate, "fna.pendingGoalsMessagePart3") ;
		//
		/*$scope.errorMessage = translateMessages( $translate, "fna.pendingGoalsMsgPart1") +
								$scope.uniquePengingGoalsCount + 
								translateMessages( $translate, "fna.pendingGoalsMsgPart2") ;*/
		
//		/"You have 'Education', 'Education', 'Retirement', and 'Wealth Transfer'. pending for calculation. Do you want to proceed ?"
		$rootScope.lePopupCtrl.showWarning(translateMessages( $translate, "lifeEngage"),
				$scope.errorMessage, translateMessages( $translate, "illustrator.cancelMessageInCaps"), $scope.cancelBtnAction, translateMessages( $translate, "general.proceedMessageInCaps"), $scope.proceedBtnAction);
	}
}

//implementing GLI_FnaService.saveTransactions
$scope.redirect = function() {
	for (var i = 0; i < FnaVariables.pages.length; i++) {
		if (FnaVariables.pages[i].url == "/fnasummary") {
			$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
			break;
		}
	}
	FnaVariables.setFnaModel($scope.FNAObject);
	var obj = new GLI_FnaService.saveTransactions($scope, $rootScope,
			DataService, $translate, UtilityService, false);
	obj.save(function() {
		$location.path('/fnasummary');
	});
}

$scope.proceedBtnAction = function() {

	for (var i = 0; i < FnaVariables.pages.length; i++) {
		if (FnaVariables.pages[i].url == "/fnasummary") {
			$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
			break;
		}
	}

	FnaVariables.setFnaModel($scope.FNAObject);
	$scope.status = "Draft";
	var obj = new GLI_FnaService.saveTransactions($scope, $rootScope,
			DataService, $translate, UtilityService, false);
	// obj.save();
	obj.save(function() {
		$location.path('/fnasummary');
	});												
}

$scope.cancelBtnAction = function() {

}	
$scope.textboxfn = function(value, id, model) {
	UtilityService.disableProgressTab = true;
	//var numericReqExp = "^[0-9]*$";
	//numericReqExp = new RegExp(numericReqExp);
	$scope.templateId = $('form').attr('id');
	var newModel = ((model).replace(/FNAObject/g, "$scope.FNAObject"))
			.substring(0, (model).replace(/FNAObject/g, "$scope.FNAObject")
					.indexOf('.value'));
	var value1 = value;
	var unit;
	eval("unit =" + newModel + ".unit");
	var numericReqExp = new RegExp(/^[0-9]*$/);
	
	
	
	
	
	
	
	/*if(typeof value1 != "undefined" && value1 != "" && !(isNaN(value1)) )
	{
		value1 = parseFloat(value1).toFixed(4);
	}*/
	
	/*if(unit != "NA")
	{
		//numericReqExp = "^([0-9]{1,12})(\.\d{1,4})?$";
		//numericReqExp = /^([0-9]{1,12})(\.\d{1,4})?$/;
		numericReqExp = new RegExp(/^([0-9]{1,12})(\.\d{1,4})?$/);
	}
	else
	{
		//numericReqExp = "^[0-9]*$";
		numericReqExp = new RegExp(/^[0-9]*$/);
	}*/
	//numericReqExp = new RegExp(numericReqExp);
	if (unit != "NA" && numericReqExp.test(value1) && value1 != "") {
		value1 = Number(value1) * Number($scope.unitList[unit]);
	}
	
	
	if(typeof value1 != "undefined"){
		if (value1.length > 1 && (value1.substring(0, 1) == 0)) {
			value1 = value1.substring(1, value1.length)
		}
	}
	
	if(typeof value1 != "undefined" && value1 != "" && !(isNaN(value1)) )
	{
		if(typeof value1 == "string")
			value1 = parseFloat(value1);
		else
			value1 =  value1;
		
	}
	
	
	if (unit != "NA" && !numericReqExp.test(value1) && value1 != "" && !(isNaN(value1))) {
		value1 = 0;
	}

	/*if (typeof value == "undefined" || value == "") {
		value1 = Number(value) * 0;
		
	}*/

	if(typeof value1 == "undefined" || value1 == "" || isNaN(value1)){
		value1 = 0;
	}
	
	eval(newModel + ".value =" +(" value1"));
	//$("#" + id).slider("value", parseInt(value1));
	$("#" + id).slider("value", value1);
	$scope.showCalculations = false;
	$scope.calculatePendingGoals();
	$scope.refresh();
	GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
	//$scope.initializeProgressbarNew();
	//$route.reload();
}
$scope.initialLoad = function(val,id,defaultunit,defaultvalue) {
    /* FNA Changes by LE Team (Bug: 3734) Start */
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6']){
	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
	'_UI_leadDetailsSection#leadDetailContent_6');
    }
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6']){
        localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
        '_UI_leadDetailsSection#popupContent_6');
    }
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6']){
        localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
        '_UI_popupTabsNavbar#leadDetail_6');
    }
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6']){
        localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
        '_UI_popupTabsNavbar#popupTabs_6');
    }
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
        localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
        '_UI_filterSection#filterSection_6');
    }
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4']){
        localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
        '_UI_FNAList#fnaList_4');
    }
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
        localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
        '_UI_HeaderDetails#HeaderDetails_6');
    }
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5']){
        localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5');
    }
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5']){
        localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
        '_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5');
    }
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5']){
        localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
        '_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5');
    }
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5']){
        localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5');
    }
    /* NEW localStorage Definition STAGE-1 start */
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_Education#Education_1_7']){
        localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
        '_UI_Education#Education_1_7');
    }
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
        localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
        '_UI_callDetailsSection#popupContent_6');
    }
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_Protection#Protection_1_7']){
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
		'_UI_Protection#Protection_1_7');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsInvestment#fnaCalculatorTabsInvestment_5']){
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
		'_UI_fnaCalculatorTabsInvestment#fnaCalculatorTabsInvestment_5');
	}
    /* NEW localStorage Definition STAGE-1 end */
    /* FNA Changes by LE Team (Bug: 3734) End */
    
	var newModel = ((val).replace(/FNAObject/g,
	"$scope.FNAObject")).substring(0,
			(val).replace(/FNAObject/g,
			"$scope.FNAObject")
			.indexOf('.value'));
		eval(newModel + "={}");

		eval(newModel + ".name =''+"
				+ angular.toJson(id));
		eval(newModel + ".unit =''+"
				+ angular.toJson(defaultunit));								
		if (eval(newModel + ".unit") != "NA") {
			eval(newModel + ".textBoxValue =Number("
					+ eval("$scope." + defaultvalue)
					+ ")/Number($scope.unitList["
					+ newModel + ".unit])");
		} else {
			eval(newModel + ".textBoxValue =Number("
					+ eval("$scope." + defaultvalue) + ")");
		}
		eval((val).replace(/FNAObject/g,
		"$scope.FNAObject")
		+ "=" + eval("$scope." + defaultvalue));

}
$scope.textboxfnew =  function(value, id, model) {
	UtilityService.disableProgressTab = true;
	
	$scope.templateId = $('form').attr('id');
	var newModel = ((model).replace(/FNAObject/g, "$scope.FNAObject"))
			.substring(0, (model).replace(/FNAObject/g, "$scope.FNAObject")
					.indexOf('.value'));
	var value1 = value;
	
	eval(newModel + ".value =" +(" value1"));
	eval(newModel + ".name =" +(" id"));
	//$scope.initializeProgressbarNew();
	//$route.reload();
}


$scope.test = function(index, id, event) {

	var thisObj = event.currentTarget;
	g_currentlySelected[0] = thisObj;
	var thisObjDeno = $(thisObj).text();
	if (thisObjDeno == "K") {
		$('.unit_selector_popup .denomination').removeClass("selected");
		$('.unit_selector_popup .denomination').eq(0).addClass("selected");
	}
	if (thisObjDeno == "M") {
		$('.unit_selector_popup .denomination').removeClass("selected");
		$('.unit_selector_popup .denomination').eq(1).addClass("selected");
	}
	if (thisObjDeno == "B") {
		$('.unit_selector_popup .denomination').removeClass("selected");
		$('.unit_selector_popup .denomination').eq(2).addClass("selected");
	}
	var individualOffset = findOffsetForScrap(thisObj, 30, -50);
	var topVal = individualOffset[0];
	var leftval = individualOffset[1];
	var chk = $('.unit_selector_popup').css("display");
	if (chk == "none") {
		$('.unit_selector_popup').fadeIn();
		$('.unit_selector_popup').css({
			"display" : "block",
			"top" : topVal,
			"left" : leftval
		});
	}
	if (chk == "block") {
		$('.unit_selector_popup').fadeOut();
		$('.unit_selector_popup').css("display", "none");
	}
	$scope.currentIndexForPopUp = index;
	$scope.currentIdForPopUp = id;
}


	$scope.getMaxFieldLength = function(maxlength)
	{
		var unitLength = 3;
		if($scope.newVal == "K")
			unitLength = 3;
		else if($scope.newVal == "M")
			unitLength = 6;
		else if($scope.newVal == "B")
			unitLength = 9;
		
		var currentMaxLength = maxlength -  unitLength;
		return currentMaxLength;
	}
	
	$scope.getGoalUnit = function(newVal)
	{
		var goalLength = $scope.goalObject.parameters.length;
		if ($scope.goalObject.parameters != undefined) {
			for (var i = 0; i < $scope.goalObject.parameters.length; i++) {
				if ($scope.goalObject.parameters[i] && $scope.goalObject.parameters[i].unit  && $scope.goalObject.parameters[i].unit != "NA") {
					$scope.goalObject.parameters[i].textBoxValue = Number($scope.goalObject.parameters[i].value)
							/ Number(FnaVariables.unitList[newVal]);
					$scope.goalObject.parameters[i].unit = newVal;
				}
				
				if(i == (goalLength-1))
				return goalLength;
			}
		}
		
		
		return goalLength;
	}
	 $scope.$on("$destroy", function() {
                 if (this.$$destroyed) return;     
				 while (this.$$childHead) {      
				 this.$$childHead.$destroy();    
				 }     
				 if (this.$broadcast)
				{ 
					this.$broadcast('$destroy');   
				}    
				 this.$$destroyed = true;                 
				 $timeout.cancel( timer );                           
        });

	
}]);