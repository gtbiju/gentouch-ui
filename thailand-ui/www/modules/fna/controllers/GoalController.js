/*
 *Copyright 2015, LifeEngage 
 */



'use strict';

storeApp.controller('GoalController', ['$rootScope', '$route', '$scope', 'DataService', 'FnaService','LookupService', 'DocumentService', 'ProductService','$compile', 
                                       '$routeParams', '$location', '$translate','IllustratorVariables', 'FnaVariables', 'UtilityService','$debounce', 'PersistenceMapping', 'AutoSave',
                                        'globalService','$timeout','UtilityVariables',
                                       function($rootScope, $route, $scope, DataService, FnaService, LookupService,DocumentService, ProductService, $compile, $routeParams, $location, $translate, 
                                       IllustratorVariables,FnaVariables, UtilityService,$debounce, PersistenceMapping, AutoSave, globalService,$timeout,UtilityVariables) {

	// Set the page name for progressbar implementation - Required in each page
	// specific controller
	FnaVariables.selectedPage = "Goals";

	var previousGoals = [];
	$scope.goalpref = [{
		"prioritynum" : "1",
		"prioritytext" : "Priority"
	}, {
		"prioritynum" : "2",
		"prioritytext" : "Priority"
	}, {
		"prioritynum" : "3",
		"prioritytext" : "Priority"
	}, {
		"prioritynum" : "4",
		"prioritytext" : "Priority"
	}, {
		"prioritynum" : "5",
		"prioritytext" : "Priority"
	}];
	$scope.FNAObject = FnaVariables.getFnaModel();
	$scope.cancel = function() {
	};
	var previousGoals = FnaVariables.getFnaModel().FNA.goals;

	$scope.selectedpage = FnaVariables.selectedPage;
	$scope.members = [];
	$scope.beneficiaryPhotos = [];
	$scope.beneficiaries = [];
	$scope.insuredImage
	var model = "FNAObject";
	if ((rootConfig.autoSave.fna)) {
		AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, FnaService);
	}

	$scope.initalizeProgressbar = function() {
		$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;
		UtilityService.disableProgressTab = false;

		for (var j = 0; j < $scope.pageSelections.length; j++) {
			if ($scope.pageSelections[j].pageType == "Need Analysis") {
				$scope.pageSelections[j].isSelected = "selected";
				$scope.pageSelections[j].isDisabled = "";
			} else {
				$scope.pageSelections[j].isSelected = "";

			}
		}
		$scope.FNAObject.FNA.pageSelections = $scope.pageSelections;
		FnaVariables.setFnaModel($scope.FNAObject);
	};
	
	//Commented for generali
	//$scope.initalizeProgressbar();

	$scope.members = FnaVariables.images;
	$scope.FNAMyself = globalService.getParty("FNAMyself");
	$scope.beneficiaries = globalService.getFNABeneficiaries();
	if ($scope.FNAMyself.BasicDetails.photo == "" || $scope.FNAMyself.BasicDetails.photo == undefined) {
		$scope.insuredImage = FnaVariables.myselfPhoto;
	} else {
		$scope.insuredImage = $scope.FNAMyself.BasicDetails.photo;
	}

	var checkFlag = "";
	for (var i = 0; i < $scope.beneficiaries.length && i < 5; i++) {
		if ($scope.beneficiaries[i] != null) {
			if ($scope.beneficiaries[i].BasicDetails && $scope.beneficiaries[i].BasicDetails.photo != undefined) {
				$scope.beneficiaryPhotos[i] = $scope.beneficiaries[i].BasicDetails.photo;
			} else {
				$scope.beneficiaryPhotos[i] = FnaVariables.choosePartiesPhoto;
			}
		} else {
			$scope.beneficiaryPhotos[i] = "";
		}
	}

	for (var i = 0; i < $scope.beneficiaryPhotos.length; ) {

		if (($scope.beneficiaryPhotos[i] == null) || ($scope.beneficiaryPhotos[i] == "") || ($scope.beneficiaryPhotos[i] == undefined)) {

			$scope.beneficiaryPhotos.splice(i, 1);
		} else {
			i++;
		}
	}

	$rootScope.moduleVariable = translateMessages($translate, "fna.fnaHeading");
	$scope.setindex = function(index) {
		$scope.index = index;
	}

	$scope.removeGoal = function($scope, priority) {
		FnaVariables.goalsCount = FnaVariables.goalsCount - 1;
		FnaVariables.goalsTotal = Number(FnaVariables.goalsTotal) - Number(priority);
		var removedGoal;
		var index;
		for (var i = 0; i < $scope.FNAObject.FNA.goals.length; i++) {
			if ($scope.FNAObject.FNA.goals[i].priority == priority) {
				removedGoal = $scope.FNAObject.FNA.goals[i].goalName;

				$scope.FNAObject.FNA.goals.splice(i, 1);
				$scope.refresh();
			}
		}
		for (var j = 0; j < $scope.goals.length; j++) {
			if ($scope.goals[j].goalName == removedGoal) {
				// alert("b4"+$scope.goals[j].availability);
				$scope.goals[j].availability = Number($scope.goals[j].availability) + 1;
				// alert("aftr"+$scope.goals[j].availability);
				$scope.refresh();

			}
		}
		UtilityService.disableProgressTab = true;

		/*
		* for(var i =0; i< $scope.goals.length;i++){
		* $scope.goals[i].availability=$scope.goals[i].occurance; }
		*/

		// $scope.refresh();
	};
	FnaVariables.goalsCount = 0;
	FnaVariables.goalsTotal = 0;
	$scope.setGoal = function($scope, priority, goal) {
		UtilityService.disableProgressTab = true;
		var selectedGoal = {}
		for (var i = 0; i < $scope.goals.length; i++) {
			if ($scope.goals[i].goalicon == goal) {
				selectedGoal.goalName = $scope.goals[i].goalName;
				selectedGoal.translateID = $scope.goals[i].translateID;
				$scope.goals[i].availability = $scope.goals[i].availability - 1;
				selectedGoal.sliderUnit = $scope.goals[i].sliderUnit;
				selectedGoal.goalId = $scope.goals[i].goalId;
				selectedGoal.goalReportText = $scope.goals[i].goalReportText;
				$scope.refresh();

			}

		}
		FnaVariables.goalsCount = FnaVariables.goalsCount + 1;
		FnaVariables.goalsTotal = Number(FnaVariables.goalsTotal) + Number(priority);
		selectedGoal.priority = priority;
		selectedGoal.parameters = [];
		selectedGoal.productList = [];
		$scope.FNAObject.FNA.goals.push(selectedGoal);

	};
	 $scope.sortGoals = function(array, key) {
		return array.sort(function(a, b) {
			var x = a[key];
			var y = b[key];
			return ((x < y) ? -1 : ((x > y) ? 1 : 0));
		});
	};

	$scope.disableTab = function() {
		if (UtilityService.disableProgressTab) {
			for (var j = 0; j < $scope.pageSelections.length; j++) {
				if ($scope.pageSelections[j].pageType == "Product Recommendation") {
					$scope.pageSelections[j].isDisabled = "disabled";
				}
			}
		}
		$scope.FNAObject.FNA.pageSelections = $scope.pageSelections;
	};
	$scope.nextPage = function() {
		$scope.disableTab();

		var match = FnaVariables.goalsCount * (Number(FnaVariables.goalsCount) + Number(1)) / 2;
		if (match == FnaVariables.goalsTotal && match != 0) {
			$scope.FNAObject.FNA.goals = $scope.sortGoals($scope.FNAObject.FNA.goals, 'priority');
			for (var i = 0; i < FnaVariables.pages.length; i++) {
				if (FnaVariables.pages[i].url == "/fnaCalculator") {
					$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
					break;
				}
			}
			FnaVariables.setFnaModel($scope.FNAObject);

			$scope.status = "Initial";
			var obj = new FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
			obj.save(function() {
				$location.path('/fnaCalculator');
			});
		} else {
			if (match == 0) {
				$scope.errorMessage = "fna.emptyError";
			} else {
				$scope.errorMessage = "fna.blankError";
			}
			$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, $scope.errorMessage), translateMessages($translate, "general.productCancelOption"));

		}
	};

	$scope.FNAObject = FnaVariables.getFnaModel();
	// alert("test "+angular.toJson($scope.FNAObject));
	$scope.goals = [];

	// alert(angular.toJson(FnaVariables.goals))
	$scope.goals = FnaVariables.goals;
	$scope.lifeStageGoals = FnaVariables.lifeStageGoals;

	// to get goals depending on lifestage selected
	$scope.getLifeStageSpecificGoals = function() {
		$scope.lifeStageSpecificGoals = [];
		var lifeStage = $scope.FNAObject.FNA.lifeStage.description;
		var isSenior = $scope.FNAMyself.BasicDetails.isSenior;
		for (var j = 0; j < $scope.lifeStageGoals.length; j++) {
			if (isSenior == "true") {
				if ($scope.lifeStageGoals[j].lifeStage == "Senior Citizen") {
					$scope.specificGoals = $scope.lifeStageGoals[j].goals;
				}
			} else {
				if ($scope.lifeStageGoals[j].lifeStage == lifeStage) {
					$scope.specificGoals = $scope.lifeStageGoals[j].goals;
				}
			}
		}
		if ($scope.specificGoals) {
			for (var j = 0; j < $scope.specificGoals.length; j++) {
				for (var i = 0; i < $scope.goals.length; i++) {
					if ($scope.goals[i].goalName == $scope.specificGoals[j]) {
						$scope.lifeStageSpecificGoals.push($scope.goals[i]);
					}
				}
			}
			$scope.goals = $scope.lifeStageSpecificGoals;
			$scope.FNAObject.FNA.RecommendedGoals=$scope.lifeStageSpecificGoals;
		}
	};

	
	$scope.getClass = function(priorityValue) {
		var className = "priorityorder";
		var index = priorityValue - 1;
		for (var i = 0; i < $scope.FNAObject.FNA.goals.length; i++) {
			for (var j = 0; j < $scope.goals.length; j++) {
				if ($scope.FNAObject.FNA.goals[i].goalName == $scope.goals[j].goalName) {

					if ($scope.FNAObject.FNA.goals[i].priority == priorityValue) {
						className = " priorityset " + $scope.goals[j].goalicon;

						$("#" + $scope.FNAObject.FNA.goals[i].priority + "").children(".priority_number,.priority_txt").css("display", "none");

						return className;
					}

				}

			}

		}

		return className;
	};

	
	
	$scope.initialLoad= function(){
		$scope.getLifeStageSpecificGoals();
		for (var i = 0; i < $scope.goals.length; i++) {
			$scope.goals[i].availability = $scope.goals[i].occurance;
		}
		
		for (var i = 0; i < $scope.FNAObject.FNA.goals.length; i++) {
			for (var j = 0; j < $scope.goals.length; j++) {
				if ($scope.FNAObject.FNA.goals[i].goalName == $scope.goals[j].goalName) {
					$scope.goals[j].availability = $scope.goals[j].availability - 1;

					FnaVariables.goalsTotal = Number(FnaVariables.goalsTotal) + Number($scope.FNAObject.FNA.goals[i].priority);
					FnaVariables.goalsCount = $scope.FNAObject.FNA.goals.length;
					$scope.refresh();
				}

			}
		};

	}
	
	$scope.$on('$viewContentLoaded', function(){
		$scope.initialLoad();
	  });

}]);
