/*
 *Copyright 2015, LifeEngage 
 */




/*Name:GLI_AboutMeController
 * For implementing GLI specific changes
 */
storeApp.controller('GLI_AboutMeController',['$rootScope', '$route', '$scope', '$compile', '$routeParams','DataService', 'LookupService', 'DocumentService', 'FnaVariables', 'GLI_FnaService','$translate', '$location', '$timeout', 'UtilityService', '$debounce', 'AutoSave','globalService','$controller',
function GLI_AboutMeController($rootScope, $route, $scope, $compile, $routeParams,DataService, LookupService, DocumentService, FnaVariables, GLI_FnaService,$translate, $location, $timeout, UtilityService, $debounce, AutoSave,globalService,$controller){
$controller('AboutMeController', {$rootScope:$rootScope, $route:$route, $scope:$scope, $compile:$compile, $routeParams:$routeParams,DataService:DataService, LookupService:LookupService, DocumentService:DocumentService, FnaVariables:FnaVariables, FnaService:GLI_FnaService,$translate:$translate, $location:$location, $timeout:$timeout, UtilityService:UtilityService, $debounce:$debounce, AutoSave:AutoSave,globalService:globalService});
$rootScope.selectedPage = "AboutMe";
$scope.status = FnaVariables.getStatusOptionsFNA()[0].status;
$scope.aboutmeSelected=false; //Next button active/Disable
UtilityService.disableProgressTab = false;
$scope.currentPageType  = "Customers Profile";
//enabling and disabling the tabs
GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
$scope.changeCheck =  false;
$scope.singleCheck = false;
$scope.genderCheck = false;
//overwritten to enable tab click changes
$scope.genderSelectionClick = function(genderId) {
    //FNA Changes made by LE Team Start
	UtilityService.disableProgressTab = false;
    //FNA Changes made by LE Team End
	//GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
	
	var gender = "Male";
	if (genderId.indexOf("female") >= 0) {	
		gender = "Female";
	}
	var isSenior = "false";
	if (genderId.indexOf("senior") == 0) {
		isSenior = "true";
	}
	if ($scope.isPreviousGenderSenior && ($scope.isPreviousGenderSenior != genderId)){
		$scope.genderEditChanged = true;
	}else{
		$scope.genderEditChanged = false;
	}
	// Set the values to fnaobject
	$scope.FNAMyself.BasicDetails.gender = gender;
	$scope.FNAMyself.BasicDetails.isSenior = isSenior;
	$scope.FNAMyself.BasicDetails.genderId = genderId;

	/** Set the insured to Parties in FNAModel * */
	$scope.FNAMyself.id = "myself";
	globalService.setParty($scope.FNAMyself, "FNAMyself");

	// To get the combined image eg: female_newlymarried or
	// senior_male_single
	var lifestageId = $scope.FNAObject.FNA.lifeStage.id;
	if (lifestageId && lifestageId != "") {
		var cutomizeBtnClass = genderId + '_' + lifestageId;
		$scope.cutomizeBtnClass = cutomizeBtnClass;
		$scope.refresh();
	}
	GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
	
};

// Function called on click of LifeStage image,overwritten to enable tab click changes
$scope.lifeStageClick = function(lifeStageId, lifeStageClass) {
    //FNA Changes made by LE Team Start
	UtilityService.disableProgressTab = false;
    //FNA Changes made by LE Team End
	//GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
	
	// Set the values to fnaobject
	$scope.FNAObject.FNA.lifeStage.description = lifeStageId;
	$scope.FNAObject.FNA.lifeStage.id = lifeStageClass;
	if ($scope.previousLifeStage && (lifeStageId != $scope.previousLifeStage)){
		$scope.lifeStageEditChanged = true;
	}else{
		$scope.lifeStageEditChanged = false;
	}
	// To get the combined image
	if ($scope.FNAMyself.BasicDetails.gender
			&& $scope.FNAMyself.BasicDetails.gender != ""
			&& $scope.FNAMyself.BasicDetails.genderId != "") {
		var genderSelect = $scope.FNAMyself.BasicDetails.genderId;

		var cutomizeBtnClass = genderSelect + '_' + lifeStageClass;
		$scope.cutomizeBtnClass = cutomizeBtnClass;
		$scope.refresh();
	}
	GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);

};
$scope.genderValidations =function(){
	//$scope.genderEditChanged = false;
	var spouseCheck = false;
	$scope.flag1 = false;
	if($scope.beneficiaries.length > 0){
		for (var i = 0; i < $scope.beneficiaries.length; i++) {
			if (typeof $scope.beneficiaries[i] != "undefined"
					&& $scope.beneficiaries[i] != null) {
				if (($scope.FNAMyself.BasicDetails.gender == "Male" && $scope.beneficiaries[i].classNameAttached == "Husband")
						|| ($scope.FNAMyself.BasicDetails.gender == "Female" && $scope.beneficiaries[i].classNameAttached == "Wife")) {
					$scope.isNextPage = false;
					var spouseCheck = true;
				} 
			}
		}
		if(spouseCheck == false){
			$scope.lifeStageValidations();
		}else{
			//$scope.genderEditChanged = false;
			$scope.popup_proceed_button = true;
			$scope.flag1 = true;
			$scope.message = translateMessages($translate,
					"fna.fnaGenderSelectionValidationMessage");
			$rootScope.lePopupCtrl.showWarning(translateMessages(
			$translate, "lifeEngage"), $scope.message,
			translateMessages($translate, "summaryConfirmButton"),
			$scope.removeBeneficiary, translateMessages(
					$translate, "general.productCancelOption"),
					$scope.cancel,$scope.cancel);	
		}
	}else{
		$scope.lifeStageValidations();
	}
};
$scope.lifeStageValidations = function(successCallback){
	var spouseCheck = false;
	$scope.flag2 = false;
    if ($scope.FNAObject.FNA.lifeStage.description == "Single" && $scope.FNAObject.FNA.parties[1] != undefined) {
		$scope.FNAObject.FNA.parties[1] == undefined;
	}
	if($scope.beneficiaries.length > 0){
			for (var i = 0; i < $scope.beneficiaries.length; i++) {
			if (typeof $scope.beneficiaries[i] != "undefined"
					&& $scope.beneficiaries[i] != null) {
				if ($scope.FNAObject.FNA.lifeStage.description == "Single"
						&& ($scope.beneficiaries[i].classNameAttached == "Husband" || $scope.beneficiaries[i].classNameAttached == "Wife")) {
					$scope.isNextPage = false;
					var spouseCheck = true;
				}
			}
		}
			if(spouseCheck == false){
				$scope.goalvalidations();
			}else{
				$scope.popup_proceed_button = true;
				$scope.flag2 = true;
				$scope.message = translateMessages($translate,
						"fna.fnaLifestageSelectionValidationMessage");
						$rootScope.lePopupCtrl.showWarning(translateMessages(
				$translate, "lifeEngage"), $scope.message,
				translateMessages($translate, "summaryConfirmButton"),
				$scope.removeBeneficiary, translateMessages(
						$translate, "general.productCancelOption"),
						$scope.cancel,$scope.cancel);	
			}
	}else{
		$scope.goalvalidations();
	}
};
$scope.goalvalidations = function(){
	if($scope.FNAObject.FNA.goals && ($scope.FNAObject.FNA.goals.length > 0)){
		if($scope.lifeStageEditChanged){
			//$scope.lifeStageEditChanged = false;
			$scope.isNextPage = false;
			$scope.popup_proceed_button = true;
			$scope.message = translateMessages($translate, "fna.fnaLifestageModifyValidationMessage");
			//$timeout(function(){
				$rootScope.lePopupCtrl.showWarning(translateMessages(
						$translate, "lifeEngage"), $scope.message,
						translateMessages($translate, "summaryConfirmButton"),
						$scope.removePreviousDetails, translateMessages(
								$translate, "general.productCancelOption"),
								$scope.cancel,$scope.cancel);
          // });
		}else{
			$scope.genderEditChanged = false;
			$scope.lifeStageEditChanged = false;
			$scope.nextPage();
		}
	}else{
		$scope.genderEditChanged = false;
		$scope.lifeStageEditChanged = false;
		$scope.nextPage();
	}
}

$scope.$on('initiate-save', function (evt, obj) {
	//$scope.disableLePopCtrl = true;
	globalService.setParty($scope.FNAMyself, "FNAMyself");
	var parties = globalService.getParties();
	$scope.FNAObject.FNA.parties = parties;		
	FnaVariables.setFnaModel($scope.FNAObject);
	$scope.status = "Draft";
	var obj = new GLI_FnaService.saveTransactions($scope, $rootScope,
			DataService, $translate, UtilityService, false);
	obj.save(function() {
		$rootScope.showHideLoadingImage(false, "");
		$rootScope.passwordLock  = false;
		$rootScope.isAuthenticated = false;
		$location.path('/login');
	});
});

/*$scope.SaveAutomatically = function(successCallback, errorCallback) {
	$scope.disableLePopCtrl = true;
	$rootScope.showHideLoadingImage(true, "paintUIMessage", $translate);
	FnaVariables.setFnaModel($scope.FNAObject);
	$scope.status = "Draft";
	var obj = new GLI_FnaService.saveTransactions($scope, $rootScope,
			DataService, $translate, UtilityService, false);
	obj.save(function() {
		$location.path('/MyFamilyDetails');
	});
};*/

//over writing function to rename status  initial to draft 
$scope.nextPage = function() {
	//$scope.singleCheck = false;
	$scope.isNextPage = true;
	if ($scope.FNAMyself.BasicDetails.gender.length > 0
			&& $scope.FNAObject.FNA.lifeStage.description.length > 0) {
		for (var i = 0; i < FnaVariables.pages.length; i++) {
			if (FnaVariables.pages[i].url == "/MyFamilyDetails") {
				$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
				break;
			}
		}
		$scope.beneficiaries = globalService.getFNABeneficiaries();
		if($scope.genderEditChanged == true){
			$scope.genderValidations();
		}else{
			if($scope.lifeStageEditChanged == true){
				$scope.lifeStageValidations();
			}
		}
		if($scope.isNextPage){
			$scope.disableTab();

			/** Set the insured to Parties in FNAModel * */
			$scope.FNAMyself.id = "myself";
			globalService.setParty($scope.FNAMyself, "FNAMyself");
			var parties = globalService.getParties();
			$scope.FNAObject.FNA.parties = parties;

			FnaVariables.setFnaModel($scope.FNAObject);
			$scope.status = "Draft";
			var obj = new GLI_FnaService.saveTransactions($scope, $rootScope,
					DataService, $translate, UtilityService, false);
			obj.save(function() {
				$location.path('/MyFamilyDetails');
			});
		}
	} else {

		var notMentionedMessage = translateMessages($translate,"fna.notMentioned");
		if ($scope.FNAMyself.BasicDetails.gender.length > 0 && $scope.FNAMyself.BasicDetails.gender == notMentionedMessage)
			$scope.FNAMyself.BasicDetails.gender = "" ;
		if ($scope.FNAObject.FNA.lifeStage.description.length > 0 && $scope.FNAObject.FNA.lifeStage.description == notMentionedMessage)
			$scope.FNAObject.FNA.lifeStage.description = "";
		
		if ($scope.FNAMyself.BasicDetails.gender.length == 0
				&& $scope.FNAObject.FNA.lifeStage.description.length == 0) {
			$rootScope.lePopupCtrl.showError(translateMessages($translate,
					"fna.generaliIndonesia"), translateMessages($translate,
					"fna.blankAboutMe"), translateMessages($translate,
					"general.productCancelOption"));
		} else if ($scope.FNAMyself.BasicDetails.gender.length == 0) {
			$rootScope.lePopupCtrl.showError(translateMessages($translate,
					"fna.generaliIndonesia"), translateMessages($translate,
					"fna.blankGender"), translateMessages($translate,
					"general.productCancelOption"));
		} else {
			$rootScope.lePopupCtrl.showError(translateMessages($translate,
					"fna.generaliIndonesia"), translateMessages($translate,
					"fna.blankLifeStage"), translateMessages($translate,
					"general.productCancelOption"));
		}
	}

};
$scope.removePreviousDetails = function() {
		$scope.FNAObject.FNA.selectedProduct = {};
		$scope.FNAObject.FNA.goals = [];
		$scope.popup_proceed_button = false;
		$scope.lifeStageEditChanged = false;
		$scope.genderEditChanged = false;
		$scope.nextPage();
};

$scope.removeBeneficiary = function() {
	$scope.popup_proceed_button = false;
	//var beneficiaries = globalService.getFNABeneficiaries();
	/*for (var n = 0; n < $scope.beneficiaries.length; n++) {
	// Code commented/removed by LE team << starts here
		 if(($scope.beneficiaries[n].classNameAttached == "Husband")||($scope.beneficiaries[n].classNameAttached == "Wife")){
			$scope.beneficiaries.splice(n,1);
		}	
	}*/
    // Code commented/removed by LE team << ends here
	globalService.setFNABeneficiaries($scope.beneficiaries);
	if($scope.flag1 == true){
		$scope.genderEditChanged = false;
		$scope.flag1 = false;
		$scope.lifeStageValidations();
	}
	if($scope.flag2 == true){
		$scope.flag2 = false;
		$scope.goalvalidations();
	}
	//$scope.nextPage();
};
// Code commented/removed by LE team << starts here
$scope.showMoreAboutMe = function() {
    $scope.FNAMyself.BasicDetails.showMoreAboutMe = !$scope.FNAMyself.BasicDetails.showMoreAboutMe;
};
// Code commented/removed by LE team << ends here    
$scope.onGenderSelectionClick = function(genderSelectionObj){
	var id = genderSelectionObj.gender
	for ( var custGender in $scope.custGenderSelections) {
		$scope.custGenderSelections[custGender].genderSelected = "";
		if ($scope.custGenderSelections[custGender].gender == id) {
			$scope.custGenderSelections[custGender].genderSelected = "_selected";
		}
	}
	$scope.genderSelectionClick(id);
	
	 $scope.genderSelect=true;
	if($scope.genderSelect && $scope.lifeStageSelect){
		$scope.aboutmeSelected=true;
	} 
	/* if($scope.FNAObject.FNA.lifeStage.description != "" && $scope.FNAMyself.BasicDetails.gender != ""){
		$scope.aboutmeSelected=true;
	} */
};

$scope.onLifeStageClick = function(lifeStageObj){
	var id = lifeStageObj.id;
	var lifestage = lifeStageObj.lifestage;
	for ( var cuslifeStage in $scope.custLifeStages) {
		$scope.custLifeStages[cuslifeStage].lifestageSelected = "";
		if ($scope.custLifeStages[cuslifeStage].lifestage == lifestage) {
			$scope.custLifeStages[cuslifeStage].lifestageSelected = "_selected";
		}
	}
	$scope.lifeStageClick(id,lifestage);
 	$scope.lifeStageSelect=true;
		if($scope.genderSelect && $scope.lifeStageSelect){
	$scope.aboutmeSelected=true;
	} 
/* 	if($scope.FNAObject.FNA.lifeStage.description != "" && $scope.FNAMyself.BasicDetails.gender != ""){
		$scope.aboutmeSelected=true;
	} */
};
	
		//Edit Flow next button active/Disable
		 if($scope.FNAObject.FNA.lifeStage.description != "" && $scope.FNAMyself.BasicDetails.gender != ""){
			$scope.aboutmeSelected=true;
		} 
		
		 $scope.$on("$destroy", function() {
                 if (this.$$destroyed) return;     
				 while (this.$$childHead) {      
				 this.$$childHead.$destroy();    
				 }     
				 if (this.$broadcast)
				{ 
					this.$broadcast('$destroy');   
				}    
				 this.$$destroyed = true;                 
				 $timeout.cancel( timer );                           
        });

                                     		
                                    		
}]);                                    		