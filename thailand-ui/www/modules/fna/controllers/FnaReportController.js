'use strict';

// storeApp.controller('FnaReportController',['$rootScope', '$scope', '$location', '$compile', 'DataService', 
		// 'FnaVariables', 'FnaService', '$translate', '$routeParams', 'globalService', 'UtilityService', 'PersistenceMapping',function ($rootScope, $scope, $location, $compile, DataService, UtilityService,
		// FnaVariables, FnaService, $translate, $routeParams, globalService, PersistenceMapping) {
		storeApp
    .controller('FnaReportController', ['$timeout', '$rootScope', '$scope', '$location', '$compile', '$routeParams', 'DataService', 'RuleService', 'LookupService', 'DocumentService', 'FnaVariables', 'FnaService', '$translate', 'UtilityService', '$debounce', 'AutoSave','PersistenceMapping', 'globalService','AgentService','PlatformInfoService', 
        function($timeout, $rootScope, $scope, $location, $compile, $routeParams, DataService, RuleService, LookupService, DocumentService, FnaVariables, FnaService, $translate, UtilityService, $debounce, AutoSave,PersistenceMapping,globalService, AgentService, PlatformInfoService) {


		
		//alert("inside controller");
		
	$scope.initalizeProgressbar = function() {
                UtilityService.disableProgressTab = false;
				$scope.FNAObject = angular.copy(FnaVariables.getFnaModel());
				 $scope.pageSelections = $scope.FNAObject.FNA.pageSelections;
                for (var j = 0; j < $scope.pageSelections.length; j++) {
                    if ($scope.pageSelections[j].pageType == "FNA Report") {
                        $scope.pageSelections[j].isSelected = "selected";
                        $scope.pageSelections[j].isDisabled = "";
                    } else {
                        $scope.pageSelections[j].isSelected = "";

                    }
                }
                $scope.FNAObject.FNA.pageSelections = $scope.pageSelections;
                FnaVariables.setFnaModel($scope.FNAObject);
				// var obj = new FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
                    // obj.save(function() {
                      // //  $location.path('/fnaReport');
                    // });
    };
    //Commented for generali
	//$scope.initalizeProgressbar();
	$scope.emailpopup = false;
	$scope.emailToId=="";
	$scope.emailccId=="";
	$scope.emailfromId = rootConfig.emailFromId;

	$scope.fnaName = 'FNA Controller';
	FnaVariables.selectedPage = "FNA Report";
    $scope.selectedpage = FnaVariables.selectedPage;
	
	$scope.FNAMyself = globalService.getParty("FNAMyself");
	$scope.FNAObject = angular.copy(FnaVariables.getFnaModel());

	$scope.agent = {};
	$scope.applicationCompletionDate = new Date($scope.FNAObject.FNA.fnaCompletion);
	$scope.applicationCompletionDate.setDate($scope.applicationCompletionDate.getDate() + 5);
	
	$scope.onRetrieveAgentProfileSuccess = function(data) {
		$scope.agent = data.AgentDetails;
		$scope.refresh();
		$scope.goalIndex = -1;
		LEDynamicUI.paintUI(rootConfig.template, "FnaReport.json", "FNASummaryReport", "#FNASummaryReport", true, $scope.onPaintUISuccess, $scope, $compile);
	};
	
	$scope.onRetrieveAgentProfileError = function(data) {
	alert("agent retrieval error");
	
	};
	
	PersistenceMapping.clearTransactionKeys();
	var transactionObj = PersistenceMapping.mapScopeToPersistence({});
	$scope.$on('$viewContentLoaded', function(){
		if ((rootConfig.isDeviceMobile)) {
			DataService.retrieveAgentDetails(transactionObj,$scope.onRetrieveAgentProfileSuccess, $scope.onRetrieveAgentProfileError);
		} else {
			AgentService.retrieveAgentProfile(transactionObj,$scope.onRetrieveAgentProfileSuccess, $scope.onRetrieveAgentProfileError);
		}	
	  });
		

	
	$scope.onPaintUISuccess = function(data)
	{
	  $scope.goalIndex = $scope.goalIndex +1;
	  var index= $scope.goalIndex;
	  if($scope.goalIndex < $scope.FNAObject.FNA.goals.length)
	  {
		LEDynamicUI.paintUI(rootConfig.template,'FnaReport.json',$scope.FNAObject.FNA.goals[$scope.goalIndex].goalId,"#"+ $scope.FNAObject.FNA.goals[$scope.goalIndex].goalId+"_"+$scope.FNAObject.FNA.goals[$scope.goalIndex].localPriority,false,$scope.onPaintUISuccess,$scope,$compile);
	  }
	
	};
	var obj = new FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
                    obj.save(function() {
                      //  $location.path('/fnaReport');
                    });
	//E-Mail Fna Report
	$scope.showEmailPopup = function() {
		$scope.emailpopup = true;
		$scope.refresh();
	}
	
	$scope.emailFNAReport = function() {
		$scope.emailToId = "";
		$scope.emailccId ="";
		var subject = "FNA Report for "+$scope.FNAMyself.BasicDetails.firstName;
		$scope.mailSubject = subject;
		$scope.pdfName = subject+".pdf";
		if ($scope.FNAMyself.ContactDetails.emailId != "" || $scope.FNAMyself.ContactDetails.emailId != undefined) {
			$scope.emailToId = $scope.FNAMyself.ContactDetails.emailId;
		}
		$scope.showEmailPopup();
	}
	
	$scope.emailPopupOkClick = function (){		
		if($scope.validateMailIds()) {
		    $scope.emailpopup = false;
		    $rootScope.showHideLoadingImage(true,"Please Wait");
			$scope.saveEmailSendFlagAndData($scope.showSuccessPopup,$scope.showErrorPopup);
		}
	}
	
	$scope.validateMailIds = function() {		
		var EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
		if($scope.emailToId==""|| $scope.emailToId == undefined){
				$scope.emailpopupError = "Please enter To mail id";
			return false;
		}
		var tomailIds = $scope.emailToId.split(',');		
		for ( var obj in tomailIds) {		 
            /*if(!EMAIL_REGEXP.test(tomailIds[obj])){
				$scope.emailpopupError = "Please enter valid To mail id";
				return false;
				break;
			}*/      			
		}
		if($scope.emailccId != undefined && $scope.emailccId !="") {
			var ccMailids = $scope.emailccId.split(',');
			for ( var obj in ccMailids) {		 
				/*if(!EMAIL_REGEXP.test(ccMailids[obj])){
					$scope.emailpopupError = "Please enter valid CC mail id";
					return false;
					break;
				} */     			
			}			
		}
		return true;
	}
	
	$scope.showSuccessPopup = function(online){
	    $rootScope.showHideLoadingImage(false);
        $scope.emailToId = "";
        $scope.emailccId = "";
		$scope.mailSubject = "";
		$scope.pdfName = "";      
        if(!online){
            $rootScope.lePopupCtrl.showSuccess(translateMessages(
                $translate, "lifeEngage"), translateMessages(
                $translate, "emailOkButtonMessageOffline"), translateMessages($translate,
                "fna.ok"));             
        } else if (online){
            $rootScope.lePopupCtrl.showSuccess(translateMessages(
                $translate, "lifeEngage"), translateMessages(
                $translate, "emailOkButtonMessageOnline1"), translateMessages($translate,
                "fna.ok"));              
        }
        $scope.refresh();
    }
	$scope.showErrorPopup = function() {
		$scope.emailpopup = false;
		$rootScope.showHideLoadingImage(false);
		$rootScope.lePopupCtrl.showError(translateMessages(
				$translate, "lifeEngage"), translateMessages(
				$translate, "emailBIErrorMessage"), translateMessages($translate,
				"fna.ok"));
	}
	$scope.saveEmailSendFlagAndData = function(successCallback,errorCallBack) {
			var emailObj = {};
			emailObj.toMailIds = $scope.emailToId;
			emailObj.fromMailId = $scope.emailfromId;
			emailObj.ccMailIds = $scope.emailccId; 
			emailObj.mailSubject =  $scope.mailSubject;
			emailObj.pdfName =  $scope.pdfName; 	
			FnaService.saveEmailTransactions($scope, emailObj, DataService, successCallback, errorCallBack);		
	}
	

	$scope.cancelEmailPopup = function(){
		$scope.emailpopup = false;
	}
	//PDF
	$scope.pdfDownloadErrorPopup = function(isNetworkError) {
		$rootScope.showHideLoadingImage(false);
		if(isNetworkError){
			$rootScope.lePopupCtrl.showError(translateMessages(
					$translate, "lifeEngage"), translateMessages(
					$translate, "networkNotAvailableError"), translateMessages($translate,
					"fna.ok"));
		} else {
			$rootScope.lePopupCtrl.showError(translateMessages(
				$translate, "lifeEngage"), translateMessages(
				$translate, "pdfDownloadTransactionsError"), translateMessages($translate,
				"fna.ok"));
		}
	}
	$scope.pdfDownloadSuccessPopup = function() {
		$rootScope.showHideLoadingImage(false);
	}
	$scope.downloadFNAReport = function(){
		$rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
		FnaService.downloadFNAReport($scope, DataService, $scope.pdfDownloadSuccessPopup, $scope.pdfDownloadErrorPopup);
	}
    $scope.downloadPdfwithFlash = function () {
        FnaService.downloadPdfwithFlash($scope, DataService);
    };
            
    //dowload base64 for pdf
    $scope.isShowFlash = false;
    if (!rootConfig.isDeviceMobile) {
        PlatformInfoService.getPlatformInfo(function (platFormInfo) {
            if (platFormInfo.browserName == "Microsoft Internet Explorer" && parseInt(platFormInfo.browserVersion) == 9) {
                $scope.isShowFlash = true;
                $scope.downloadPdfwithFlash();
            }
        });
    }      
	// $scope.getRiderIndex = function(shortName) {
		// var index = $scope.goalIndex;
		// return index;
	// };
	// $scope.onPaintUISuccessCallBack = function(data)
	// {
	 
	 // // alert("PAINT Success");
	// };
	$scope.BackToProductRecommendationPage = function() {
		 $location.path('/fnaRecomendedProducts');
	};

}]);
