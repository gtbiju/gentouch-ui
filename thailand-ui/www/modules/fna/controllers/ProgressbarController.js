/*
 *Copyright 2015, LifeEngage 
 */


storeApp.controller('ProgressbarController', ['$rootScope', 'FnaVariables', '$scope', 'DataService', 'FnaService','LookupService', 'DocumentService', 'ProductService','$compile', 
'$routeParams', '$location', '$translate','IllustratorVariables', 'FnaVariables', 'UtilityService','$debounce', 'PersistenceMapping', 'AutoSave',
 'globalService','$timeout','UtilityVariables',

 function($rootScope, FnaVariables, $scope, DataService, FnaService, LookupService,DocumentService, ProductService, $compile, $routeParams, $location, $translate, 
IllustratorVariables,FnaVariables, UtilityService,$debounce, PersistenceMapping, AutoSave, globalService,$timeout,UtilityVariables) {

	// Get the selectedpage name set in all controllers specific to a page
	$scope.selectedPage = FnaVariables.selectedPage;
	$scope.FNAObject = FnaVariables.getFnaModel();
	$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;
	$scope.pages = [];
	$scope.pages = FnaVariables.pages;
	$scope.page;
	$scope.pageIndex;
	$scope.pageTypes = FnaVariables.pageTypes;
	$scope.progressbarWidth;
	$scope.nextPage;
	$scope.previousPage;
	$scope.cancel = function() {
	};

	// Get the pages array from FnaVariables and compare with selectedpage name
	// to get the selectedPage object and selectedPageIndex
	for (var i = 0; i < $scope.pages.length; i++) {
		if ($scope.pages[i].pageName == $scope.selectedPage) {
			$scope.pageIndex = i;
			$scope.page = $scope.pages[i];
		}
	}
	$scope.PluginInit = function() {
		var countUp = function() {
			if ($scope.pageIndex == 0) {
				$(".btnPrevious_container").hide();
			}
			if ($scope.pageIndex == ($scope.pages.length - 1)) {
				$(".btnNext_container").hide();
			}
		};
		$timeout(countUp, 1000);
	};

	// Set the progressbar width based on the selected page and width
	var mapPgeTypes = {};
	var k = 0;
	var pgeType = $scope.pages[0].pageType;
	for (var i = 0; i < $scope.pages.length; i++) {
		if (pgeType == $scope.pages[i].pageType) {
			mapPgeTypes[pgeType] = ++k;
		} else {
			k = 0;
			pgeType = $scope.pages[i].pageType;
			mapPgeTypes[pgeType] = ++k;
		}
	}
	var totalwidth = parseInt(0);
	var tabwidth = parseInt(0);
	var tabIndex = parseInt($scope.pageIndex);
	for (var i = 0; i < $scope.pageTypes.length; i++) {
		if ($scope.page.pageType == $scope.pageTypes[i].pageType) {
			tabwidth = parseInt($scope.pageTypes[i].width);
			break;
		}
		tabIndex = tabIndex
				- parseInt(mapPgeTypes[$scope.pageTypes[i].pageType]);
		totalwidth = parseInt(totalwidth) + parseInt($scope.pageTypes[i].width);
	}
	if (tabIndex == 0 && totalwidth != 0) {
		$scope.progressbarWidth = totalwidth;

	} else {
		$scope.progressbarWidth = totalwidth
				+ ((tabwidth / mapPgeTypes[$scope.page.pageType]) * tabIndex);
	}

	// Function called onclick on previouspage link in menu bar

	$scope.mapKeysforPersistence = function() {
		if (!(rootConfig.isDeviceMobile) && (FnaVariables.fnaId == null || FnaVariables.fnaId == 0)) {
			PersistenceMapping.creationDate = formattedDate;
			PersistenceMapping.Key13 = formattedDate;
		}
		PersistenceMapping.Key10 = "FullDetails";
		PersistenceMapping.Type = "FNA";
	};
	$scope.onGetListingsSuccess = function(data) {
		FnaVariables.setFnaModel({
			'FNA' : JSON.parse(JSON.stringify(data[0].TransactionData))
		});
		if ($scope.pageIndex > 0) {
			var previousPage = $scope.pages[$scope.pageIndex - 1];
			$location.path(previousPage.url);
		}
	};
	$scope.onGetListingsError = function() {
		alert("error");
	};
	$scope.previousPage = function() {
		if (UtilityService.disableProgressTab) {
			 $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),translateMessages($translate, "fna.previousPageMessage") ,translateMessages($translate, "fna.ok"), $scope.okClick,translateMessages($translate, "general.productCancelOption"),$scope.cancel,$scope.cancel);
		} else {
			if ($scope.pageIndex > 0) {
				var previousPage = $scope.pages[$scope.pageIndex - 1];
				$location.path(previousPage.url);
			}
		}
	};

	$rootScope.okClick = function() {
		PersistenceMapping.clearTransactionKeys();
		$scope.mapKeysforPersistence();
		var transactionObj = PersistenceMapping.mapScopeToPersistence({});
		DataService.getListingDetail(transactionObj,
				$scope.onGetListingsSuccess, $scope.onGetListingsError);
	};

	// Function called onclick on any tab link in menu bar. In that case, we
	// move to the first page PageType as the selected tab
	$scope.tabClick = function(event, pageType) {

		var thisObj = event.currentTarget;
		var locationUrl;
		for (var j = 0; j < $scope.pageTypes.length; j++) {
			if (pageType == $scope.pageTypes[j].pageType) {
				for (i = 0; i < $scope.pageSelections.length; i++) {
					if (pageType == $scope.pageSelections[i].pageType
							&& $scope.pageSelections[i].isDisabled == "") {
						locationUrl = $scope.pageTypes[j].url;
						$location.path(locationUrl);
					}
				}
			}
		}
	};
	$scope.PluginInit();

}]);
