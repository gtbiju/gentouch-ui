'use strict';

storeApp
    .controller('ProductListingController', ['$timeout', '$rootScope', '$location', '$scope', '$translate', 'FnaVariables', 'IllustratorVariables', 'UtilityService', 'DataService', '$debounce', '$routeParams', 'FnaService', 'RuleService', 'AutoSave', 'globalService',
        function($timeout, $rootScope, $location, $scope, $translate, FnaVariables, IllustratorVariables, UtilityService, DataService, $debounce, $routeParams, FnaService, RuleService, AutoSave, globalService) {
            FnaVariables.selectedPage = "Product Selection";
            $scope.selectedpage = "Product Selection";
            $scope.FNAObject = FnaVariables.getFnaModel();
            $scope.FNAMyself = globalService.getParty("FNAMyself");
            $scope.insuredImage
            $rootScope.moduleHeader = "fna.fnaHeading";
			$scope.goalIndex = -1;
            $rootScope.moduleVariable = translateMessages($translate, $rootScope.moduleHeader);
            if ($scope.FNAMyself.BasicDetails.photo == "" || $scope.FNAMyself.BasicDetails.photo == undefined) {
                $scope.insuredImage = FnaVariables.myselfPhoto;
            } else {
                $scope.insuredImage = $scope.FNAMyself.BasicDetails.photo;
            }
			$scope.FNAObject.FNA.fnaCompletion = new Date();
            var model = "FNAObject";
            if ((rootConfig.autoSave.fna)) {
                AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, FnaService);
            }

	$scope.cancel = function() {
	};

	$scope.goalfirst;
	
	$scope.PluginInit = function(value) {

		var countUp = function() {
			if (value == true) {

				// animationTime: 100, set the animation duration
				// selectedAction:"selectOnly" Two options - switch/selectOnly
				// FastClick.attach(document.body);
				/*
				 * $('.arc_container_wrapper .arc_left').radialOptions({
				 * animationTime : 100, selectedAction : "switch" });
				 */
			}
		}
		$timeout(countUp, 0);
	};

	
            //To be committed in LE
            $scope.getneedId = function(goalName) {
        		var needId = "";
        		if (goalName == "Protection") {
        			needId = "protection";
        		} else if (goalName == "Education") {
        			needId = "education";
        		} else if (goalName == "Retirement") {
        			needId = "retirement";
        		} else if (goalName == "Wealth Transfer") {
        			needId = "wealth";
        		} else if (goalName == "Wealth Accumulation") {
        			needId = "accumulation";
        		} 
        		
        		return needId;
        		
        	};
        	 //To be committed in LE
        	$scope.getLifeStage = function(lifestageDesc) {
        		var lifeStageId = "";
        		if (lifestageDesc == "Single") {
        			lifeStageId = "0";
        		} else if (lifestageDesc == "Newly Married") {
        			lifeStageId = "1";
        		} else if (lifestageDesc == "Married with kids") {
        			lifeStageId = "2";
        		} else if (lifestageDesc == "Married with grown up kids") {
        			lifeStageId = "3";
        		} else if (lifestageDesc == "Golden Age") {
        			lifeStageId = "4";
        		}
        		return lifeStageId;
        	};
			
			
			$scope.getProductSelectionList = function(goalName, priority) {
                for (var i = 0; i < $scope.goalList.length; i++) {
                    if ($scope.goalList[i].goalName == goalName && $scope.goalList[i].priority == priority) {
                        $scope.productDetailsList = $scope.goalList[i].productList;
						return;
                    }
                }
            };
			
            $scope.getGoalProduct = function() {
			
				$scope.goalIndex = $scope.goalIndex + 1;
				if($scope.goalIndex < $scope.goalList.length )
				 {
					var goalName = $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].goalName;
					var priority = $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].priority;
					if ($scope.FNAObject.FNA.productSelectedGoal == goalName) {
						$scope.isProductSelected = true;
					} else {
						$scope.isProductSelected = false;
					}
					$scope.currentGoalName = goalName;
                        var needId = $scope.getneedId(goalName);
                        var lifestageDesc = $scope.FNAObject.FNA.lifeStage.description;
                        var lifestageId = $scope.getLifeStage(lifestageDesc);
                        var lifeStage = {
                            "id": lifestageId
                        };
                        var insured = {
                            "lifeStage": lifeStage
                        };
                        var transactionObj = {
                            goalName: goalName,
                            risk: $scope.FNAObject.FNA.riskProfile.level,
                            customer: insured
                        };
                        var need = {
                            "id": needId
                        };
                        transactionObj.needs = [];
                        transactionObj.needs.push(need);
						 try {
                            $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
                            $scope.refresh();
                            var inputJson = JSON.stringify(transactionObj);
                            if ((rootConfig.isDeviceMobile)) {
                                LEProductSelector.getRecommendedProducts(inputJson, function(output) {
                                    var result = $.parseJSON(output);
									 $scope.getProductRecommendationSuccess(result, goalName, priority);									
                                }, function(output) {});
							} else {
                                RuleService.getRecommendedProducts(inputJson, function(output) {
                                    var result = output;
                                   // getProductSuccess(result, goalName);
								   //Try below code if any issue with  getProductRecommendationSuccess(result, goalName, priority);	
								    // for (var i = 0; i < $scope.goalList.length; i++) {
										// if ($scope.goalList[i].goalName == goalName) {		
											// $scope.goalList[i].productList = result.selectedProducts;
											// $scope.productDetailsList = $scope.goalList[i].productList;
											 // $scope.getGoalProduct();
										// }
									// }
									 $scope.getProductRecommendationSuccess(result, goalName, priority);		
                                }, function(output) {});
                            }
                        } catch (exception) {
                            alert("Error Output " + exception);
                            errorCallback(exception);
                        }
				} else {
					 $scope.getProductSelectionList(goalfirst, 1);
					$rootScope.showHideLoadingImage(false);
					$scope.refresh();
				}
			};
			
			$scope.initialLoad = function(){
				$scope.orderList = [{
					id : 1,
					value : "Priority(Ascending)"
				}, {
					id : 2,
					value : "Priority(Descending)"
				}, {
					id : 3,
					value : "Age(Ascending)"
				}];

				$scope.goals = FnaVariables.goals;

				$scope.selectedGoals = [];

				$scope.goalList = $scope.FNAObject.FNA.selectedGoals;

				$scope.goalPriorityNumber = 1;

				$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;

				$scope.selectedproduct = $scope.FNAObject.FNA.selectedProduct;

	            for (var i = 0; i < $scope.goalList.length; i++) {
	                for (var j = 0; j < $scope.goals.length; j++) {
	                    if ($scope.goals[j].goalName == $scope.goalList[i].goalName) {
	                        var obj = {
	                            "goalName": $scope.goals[j].goalName,
	                            "goalicon": $scope.goals[j].goalicon,
	                            "goalDesc": $scope.goals[j].goalDEsc,
	                            "goalDescImage": $scope.goals[j].goalDescImage,
	                            "priority": $scope.goalList[i].priority,
	                            "translateID": $scope.goals[j].translateID
	                        };
	                        $scope.selectedGoals.push(obj);
	                        if ($scope.goalList[i].priority == 1) {
	                            goalfirst = $scope.goals[j].goalName;
	                        }
	                        
	                    }
	                }
	            }
	           // $scope.initalizeProgressbar();
	            $scope.currentGoalName = goalfirst;
	            $scope.getGoalProduct();
			}
			
			 //To be committed in LE
			$scope.$on('$viewContentLoaded', function(){
				$scope.initialLoad();
			  });
			// $scope.getGoalProduct();
			
			

            function getProductListSuccess(data, goalName) {
                try {
                    var carrierId = 1;
                    var productIdList = data.productList;
                    LEProductService.getProductListDetails(carrierId, productIdList, function(output) {
                        var result = output;
                        getProductSuccess(output, goalName);
                    }, function(output) {
                        $scope.getProductFailure()
                    });
                } catch (exception) {
                    alert("Error Output " + exception);
                    errorCallback(exception);
                }

            };

	$scope.errorCallback = function() {};
	
	
		
	$scope.getProductRecommendationSuccess = function(data, goalName, priority){
                for (var i = 0; i < $scope.goalList.length; i++) {
                    if ($scope.goalList[i].goalName == goalName && $scope.goalList[i].priority == priority) {
                        $scope.goalList[i].productList = data.selectedProducts;
                        $scope.getProductSelectionList(goalName, priority);
                        $scope.productDetailsList = $scope.goalList[i].productList;
						 $scope.getGoalProduct();
						return;
                    }
                }
            };

	function getProductSuccess(data, goalName) {
		for (var i = 0; i < $scope.goalList.length; i++) {
			if ($scope.goalList[i].goalName == goalName) {
				$scope.goalList[i].productList = data.selectedProducts;
				$scope.productDetailsList = $scope.goalList[i].productList;
			}
		}

		$rootScope.showHideLoadingImage(false);
		$scope.refresh();
	};

	$scope.getProductFailure = function(data) {
	};

	$scope.getSelectedProduct = function(product, event) {
		var thisObj = event.currentTarget;
		if (thisObj.checked) {
			$scope.FNAObject.FNA.selectedProduct = product;

			for (var j = 0; j < $scope.FNAObject.FNA.selectedGoals.length; j++) {
				if ($scope.FNAObject.FNA.selectedGoals[j].goalName == $scope.currentGoalName) {
					// $scope.FNAObject.FNA.selectedProduct.parameters=[];
					// $scope.FNAObject.FNA.selectedProduct.parameters=$scope.FNAObject.FNA.goals[j].parameters;
					$scope.FNAObject.FNA.productSelectedGoal = $scope.currentGoalName;
					$scope.FNAObject.FNA.selectedProduct.result = $scope.FNAObject.FNA.selectedGoals[j].result;
					$scope.isProductSelected = true;
				}
			}

		} else {
			$scope.FNAObject.FNA.selectedProduct = {};
		}
		$scope.selectedproduct = $scope.FNAObject.FNA.selectedProduct;
		FnaVariables.setFnaModel($scope.FNAObject);
		$scope.refresh();
	};

	$scope.getAllProducts = function() {
		FnaVariables.setFnaModel($scope.FNAObject);
		$location.path('/fnaAllProducts');
	};
	$scope.generateFnaReport = function() {
		FnaVariables.setFnaModel($scope.FNAObject);
		$location.path('/fnaReport');
	};

	$scope.getIllustration = function() {

		if ($scope.FNAObject.FNA.selectedProduct && $scope.FNAObject.FNA.selectedProduct.productId && $scope.isProductSelected) {
			FnaVariables.setFnaModel($scope.FNAObject);

			var product = globalService.product;
			product.ProductDetails.productCode = $scope.FNAObject.FNA.selectedProduct.productId;
			globalService.setProduct(product);

			// To reset isInsured and isBenificiary for all parties to default
			// false value
			// Since for each illustrate button clisk -- new illustration is
			// created for FNA
			var parties = globalService.getFNAParties();
			$scope.FNAObject.FNA.parties = parties;

			FnaVariables.setFnaModel($scope.FNAObject);
			var obj = new FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
			obj.save(function() {
				UtilityService.previousPage = 'FNA';
				$location.path('/fnaChooseParties/0/'+product.id);
			});

		} else {
			$rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "chooseProductValidationMessage"), translateMessages($translate, "fna.ok"));
		}
	};

	$scope.nextPage = function() {
		FnaVariables.setFnaModel($scope.FNAObject);
		$location.path('/fnaReport');
	};
	// To change the selected tab style with highlighted background
	$scope.initalizeProgressbar = function() {
		UtilityService.disableProgressTab = false;
		for (var j = 0; j < $scope.pageSelections.length; j++) {
			if ($scope.pageSelections[j].pageType == "Product Recommendation") {
				$scope.pageSelections[j].isSelected = "selected";
				$scope.pageSelections[j].isDisabled = "";
			} else {
				$scope.pageSelections[j].isSelected = "";

			}
		}
		$scope.FNAObject.FNA.pageSelections = $scope.pageSelections;
		FnaVariables.setFnaModel($scope.FNAObject);
	};

	
	$scope.PluginInit1 = function(value) {

		var countUp = function() {
			if (value == true) {
				for (var i = 1; i <= ($scope.productDetailsList.length); i++) {
					$("#carousel" + i).elastislide({
						minItems : 2
					});
				}
				/*
				 * $('#carousel2').elastislide({ minItems : 2 });
				 * 
				 * $('#carousel3').elastislide({ minItems : 2 });
				 */

				/*
				 * $('.pdt_recomm_container .recom_items_unselected').click(
				 * function() { $('.pdt_recomm_container
				 * .recom_items_unselected')
				 * .removeClass("recom_items_selected");
				 * $(this).addClass("recom_items_selected"); });
				 * 
				 * $("input:checkbox.rp_chekbox").click( function() {
				 * $("input:checkbox.rp_chekbox").not($(this))
				 * .removeAttr("checked"); $(this).attr("checked",
				 * $(this).attr("checked")); });
				 */
			}
		}
		$timeout(countUp, 0);
	};
    }]);
