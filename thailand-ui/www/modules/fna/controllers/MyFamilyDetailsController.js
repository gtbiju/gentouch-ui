/*
 *Copyright 2015, LifeEngage 
 */




'use strict';

storeApp.controller('MyFamilyDetailsController', ['$rootScope', '$route', '$scope', 'DataService', 'FnaService','LookupService', 'DocumentService', 'ProductService','$compile', 
                                                  '$routeParams', '$location', '$translate','IllustratorVariables', 'FnaVariables', 'UtilityService','$debounce', 'PersistenceMapping', 'AutoSave',
                                                   'globalService','$timeout','UtilityVariables',
                                                  function($rootScope, $route, $scope, DataService, FnaService, LookupService,DocumentService, ProductService, $compile, $routeParams, $location, $translate, 
                                                  IllustratorVariables,FnaVariables, UtilityService,$debounce, PersistenceMapping, AutoSave, globalService,$timeout,UtilityVariables) {


	// Set the page name for progressbar implementation - Required in each page
	// specific controller
	FnaVariables.selectedPage = "My Family";
	$scope.selectedpage = FnaVariables.selectedPage;
	$rootScope.moduleHeader = "fna.fnaHeading";
	$rootScope.moduleVariable = translateMessages($translate, $rootScope.moduleHeader);
	$scope.FNAObject = FnaVariables.getFnaModel();
	$scope.FNAMyself = globalService.getParty("FNAMyself");
	$scope.FNABeneficiaries = globalService.getFNABeneficiaries();
	$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;
	//$scope.showErrorCount = true;
	$scope.namePattern = rootConfig.namePattern;
	$scope.emailPattern = rootConfig.emailPattern;
	$scope.mobnumberPattern = rootConfig.mobnumberPattern;
	$scope.numberPattern = rootConfig.numberPattern;
	$scope.dynamicErrorMessages = [];
	$scope.dynamicErrorCount = 0;
	$scope.insuredPhotoErrorCount = 0;
	$scope.myselfPhotoValidation = "true";
	$scope.benificiaryPhotoValidation = "true";
	$scope.male = false;
	$scope.female = false;
	$scope.showMockDataButton = rootConfig.showFNAMockDataButton;
	var global_val_array = [];
	UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
	var model = "FNAObject";
	if ((rootConfig.isOfflineDesktop) || !(rootConfig.isDeviceMobile)) {
		$scope.isDesktopFlag = true;
		$scope.isMobileFlag = false;
	} else {
		$scope.isDesktopFlag = false;
		$scope.isMobileFlag = true;
	}
	if ((rootConfig.autoSave.fna)) {
		AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, FnaService);
	}

	$scope.initalizeProgressbar = function() {
		UtilityService.disableProgressTab = false;
		for (var j = 0; j < $scope.pageSelections.length; j++) {
			if ($scope.pageSelections[j].pageType == "Customers Profile") {
				$scope.pageSelections[j].isSelected = "selected";
				$scope.pageSelections[j].isDisabled = "";
			} else {
				$scope.pageSelections[j].isSelected = "";

			}
		}
		$scope.FNAObject.FNA.pageSelections = $scope.pageSelections;
		FnaVariables.setFnaModel($scope.FNAObject);
	};
	//commented for generali
	//$scope.initalizeProgressbar();
	$scope.refresh = function() {
		$timeout(function() {
			$scope.$apply();
		}, 300);
	};

	$scope.callback = function() {
	};

	// ** Validate gender- To display husband/wife accordingly -- Begin **/
	if ($scope.FNAMyself.BasicDetails.gender == "Male" && $scope.FNAObject.FNA.lifeStage.description != "Single") {
		$scope.male = true;
	} else if ($scope.FNAMyself.BasicDetails.gender == "Female" && $scope.FNAObject.FNA.lifeStage.description != "Single") {
		$scope.female = true;
	}

	$scope.familyMembers = [];
	$scope.familyMembers = FnaVariables.familyMembers;
	for (var i = 0; i < $scope.familyMembers.length; i++) {
		$scope.familyMembers[i].availability = $scope.familyMembers[i].occurance;
		$scope.familyMembers[i].display = true;
		if (($scope.male && $scope.familyMembers[i].memberName == "husband") || ($scope.female && $scope.familyMembers[i].memberName == "wife")) {
			$scope.familyMembers[i].display = false;
		} else if (!$scope.female && !$scope.male && (($scope.familyMembers[i].memberName == "husband") || ($scope.familyMembers[i].memberName == "wife"))) {
			$scope.familyMembers[i].display = false;
		}
	}
	// ** Validate gender- To display husband/wife accordingly  -- End **/

	$scope.getGlobalArrayValue = function($scope, returnVal) {
		for (var i = 0; i < returnVal.length; i++) {
			if (returnVal[i] != null && typeof returnVal[i] != "undefined") {
				global_val_array[i] = returnVal[i].classNameAttached;
			}
		}
	};

	$scope.getGlobalArrayValue($scope, $scope.FNABeneficiaries);
	var myFamilyDetailsUiJsonFile = FnaVariables.uiJsonFile.fnaAboutMeFile;
	
	$scope.Beneficiary = {};
	var thisObjDeletion;
	var g_placeholder;
	$scope.order
	$scope.memberIndex
	$scope.edit = false;
	$scope.isInsured
	$scope.members = [];
	$scope.BeneficiaryTemp = {};
	$scope.editIndex
	$scope.members = FnaVariables.images;
	$scope.InsuredTemp = {};
	$scope.defaultImageForMembers = [];

	//Scrap button display for delete function
	function findOffsetForScrap(thisObj, x, y) {
		var cordinateVal = $(thisObj).offset();
		var topVal = cordinateVal.top + x + "px";
		var leftVal = cordinateVal.left + y + "px";
		return [topVal, leftVal];
	};

	// Function called on init of family member ng-repeat to initialize all related fns
	$scope.onGetMemberCompete = function(isLast) {
		if (isLast) {
			$scope.initialiseFamilyDetails();
		}
	};

	$scope.myFamilyDragDrop = function() {
		/* INITIALIZE THE DRAG-DROP JQUERY UI PLUGINS HERE */
		// var global_array =[];
		/* DRAG & DROP WIDGET */

		$(".familymember").draggable({
			revert : false,
			helper : "clone",
			revertDuration : 100
		});

		$(".member").droppable({
			drop : function(event, ui) {
				/*
				 * Get the class name of the currently dragged
				 * item
				 */
				var currentlyDragged = $(ui.draggable).attr(
				"class").split(' ')[1];
				
				/* Uppercasing the Class name */
				var classNameAttached = currentlyDragged.substr(0, 1).toUpperCase() + currentlyDragged.substr(1);
				// SOn
				/*
				 * Get the class name of the currently dropped
				 * area ( member/non member)
				 */
				var chkFlag = $(this).attr("class").split(' ')[0];

				if (chkFlag == "member") {
					$scope.FNABeneficiaries[Number($(this).attr("id"))] = {};
					$scope.FNABeneficiaries[Number($(this).attr("id"))].id = $(this).attr("id");
					$scope.FNABeneficiaries[Number($(this).attr("id"))].imageClass = "nonmember " + currentlyDragged;
					$scope.FNABeneficiaries[Number($(this).attr("id"))].classNameAttached = classNameAttached;
					$scope.order = $(this).attr("id");
					$(this).removeClass().addClass('nonmember ' + currentlyDragged + ' ');
					$(this).find('.relationText').html(classNameAttached);

					$scope.getGlobalArrayValue($scope, $scope.FNABeneficiaries);
					$scope.setMember($scope.order, currentlyDragged);
				}
			}
		});
	};
	 // Changes done by LETeam << Change starts from here
	for (var i = 0; i < 7; i++) {
		if ( typeof $scope.defaultImageForMembers[i] == "undefined") {
			$scope.defaultImageForMembers[i] = {};
		}
		$scope.defaultImageForMembers[i].photo = FnaVariables.photo;
	}
        // Changes done by LETeam << Change ends here
	//Initialise function called once all members are [populated using ng-repeat
	$scope.initialiseFamilyDetails = function() {

		//To set the default base64string string so as to avoid the ?, if place-holder has no image
		if ( typeof $scope.FNAMyself.BasicDetails != "undefined") {
			if ( typeof $scope.FNAMyself.BasicDetails.photo == "undefined" || $scope.FNAMyself.BasicDetails.photo == "") {
				$scope.FNAMyself.BasicDetails.photo = FnaVariables.myselfPhoto;
			} else {
				$scope.FNAMyself.BasicDetails.photo = ($scope.FNAMyself.BasicDetails.photo).replace(/ /g, '+');
			}
		}

		for (var i = 0; i < $scope.FNABeneficiaries.length; i++) {
			if ( typeof $scope.FNABeneficiaries[i] != "undefined" && $scope.FNABeneficiaries[i] != null) {
				$scope.FNABeneficiaries[i].id = i.toString();
				if ( typeof $scope.FNABeneficiaries[i].imageClass != "undefined") {
					$("#" + $scope.FNABeneficiaries[i].id + "").removeClass().addClass('' + $scope.FNABeneficiaries[i].imageClass + ' ');
					var classNameAttached = $scope.FNABeneficiaries[i].imageClass
					.split(' ')[1];
					// SOn
					var classNameAttached1 = classNameAttached.substr(0, 1).toUpperCase() + classNameAttached.substr(1);

				}

				//Changing Add Details to Edit Details
				if ( typeof $scope.FNABeneficiaries[i].BasicDetails != "undefined") {
					g_placeholder = i + 1;
					var btntext = "#detailsBtn" + g_placeholder;
					var translateEditDetails = translateMessages($translate, "fna.editDetails");
					$(btntext).removeClass().addClass("editdetl_myfamily").html(translateEditDetails);
				}
				if ( typeof $scope.FNABeneficiaries[i].BasicDetails != "undefined" && $scope.FNABeneficiaries[i].BasicDetails.photo) {
					$scope.FNABeneficiaries[i].BasicDetails.photo = ($scope.FNABeneficiaries[i].BasicDetails.photo).replace(/ /g, '+');
					$scope.defaultImageForMembers[i].photo = $scope.FNABeneficiaries[i].BasicDetails.photo;
				}
			}
			if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
				$scope.refresh();
			}
		}
		for (var i = 0; i < $scope.FNABeneficiaries.length; i++) {
			for (var j = 0; j < $scope.familyMembers.length; j++) {
				if ( typeof $scope.FNABeneficiaries[i] != "undefined" && $scope.FNABeneficiaries[i] != null) {
					var classNameAttached = $scope.FNABeneficiaries[i].imageClass
					.split(' ')[1];
					if (classNameAttached == $scope.familyMembers[j].memberName) {
						$scope.familyMembers[j].availability = $scope.familyMembers[j].availability - 1;
						if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
							$scope.refresh();
						}
					}
				}

			}
		}

		if ($scope.FNAMyself.BasicDetails.firstName != "" || $scope.FNAMyself.BasicDetails.lastName != "" || $scope.FNAMyself.BasicDetails.dob != "" || $scope.FNAMyself.OccupationDetails.description != "" || $scope.FNAMyself.ContactDetails.emailId != "" || $scope.FNAMyself.ContactDetails.homeNumber1 != "" || $scope.FNAMyself.ContactDetails.currentAddress.address != "") {

			var translateEditDetailsEdit = translateMessages($translate, "fna.editDetails");
			$('#selfdetailsBtn').removeClass().addClass("editDetlBtn").html(translateEditDetailsEdit);
			$('.savedDetails').css("display", "block");
			/* $('.sel_myself_wrapper').css("min-height", "240px"); */
			if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
				$scope.refresh();
			}
		}

		if ($scope.FNAMyself.BasicDetails.photo == FnaVariables.myselfPhoto) {
			$scope.insuredPhotoErrorCount = 1;
		}
	};

	//Function called to set the member details on dropping member
	$scope.setMember = function(order, currentlyDragged) {
		UtilityService.disableProgressTab = true;

		/** Validate the occurance of a family member**/
		for (var i = 0; i < $scope.familyMembers.length; i++) {
			if ($scope.familyMembers[i].memberName == currentlyDragged) {
				$scope.familyMembers[i].availability = $scope.familyMembers[i].availability - 1;
				if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
					$scope.refresh();
				}

			}
		}
	};

	/* ON CLICK OF THE DROPPED ITEM, IT SHOULD SHOW THE SCRAP POP UP */
	$scope.onMemberClick = function(event) {
		thisObjDeletion = event.target.parentNode.parentNode;
		var thisObjClassName = String(thisObjDeletion.className);
		var id = thisObjDeletion.id;
		g_placeholder = JSON.parse(id) + 1;
		if (thisObjClassName.indexOf("nonmember") >= 0) {
			var individualOffset = findOffsetForScrap(thisObjDeletion, -10, 80);
			var topVal = individualOffset[0];
			var leftval = individualOffset[1];
			var chk = $('.scrapButton').css("display");
			if (chk == "none") {
				$('.scrapButton').fadeIn();
				$('.scrapButton').css({
					"display" : "block",
					"top" : topVal,
					"left" : leftval
				});
			} else if (chk == "block") {
				$('.scrapButton').css({
					"top" : topVal,
					"left" : leftval
				});
			}
		}
	};

	/* CLICKING ANYWHERE OUTSIDE THE SCRAP BUTTON SHOULD HIDE IT */
	$(document).mouseup(function(e) {
		var container = $(".scrapButton");
		if (!container.is(e.target) && container.has(e.target).length === 0) {
			container.hide();
		}
	});

	//**Function called on delete of a member
	$scope.onDelete = function() {
		var chkFlag = $('.scrapButton').css("display");
		if (chkFlag == "block") {
			$(thisObjDeletion).removeClass().addClass("member ui-droppable");
			$(thisObjDeletion).find('.relationText').html("Member");
		}
		var id = thisObjDeletion.id;
		$scope.removeMember(id);
		global_val_array.splice(id, global_val_array.length);
		$('.scrapButton').css("display", "none");
		for (var i = 1; i <= 10; i++) {
			$("#detailsBtn" + (i)).attr("disabled", false);
			$("#selfdetailsBtn").attr("disabled", false);
		}

	};

	//SubFunction called on delete of a member
	$scope.removeMember = function(id) {
		var removedMember;
		for (var i = 0; i < $scope.FNABeneficiaries.length; i++) {
			if ($scope.FNABeneficiaries[i] != null && $scope.FNABeneficiaries[i].id == id) {
				removedMember = $scope.FNABeneficiaries[i].imageClass
				.split(' ')[1];
				delete $scope.FNABeneficiaries[i];
				$scope.defaultImageForMembers[i].photo = FnaVariables.photo;
				var btntext = "#detailsBtn" + g_placeholder;
				$(btntext).removeClass().addClass("addetl_myfamily").html("Add Details");
				var classtoBePassedFamily = ".myfulltabdetails";
				toggleSlide1(classtoBePassedFamily);
				if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
					$scope.refresh();
				}
			}
		}
		//** Validate the occurance of the family member**/
		for (var j = 0; j < $scope.familyMembers.length; j++) {
			if ($scope.familyMembers[j].memberName == removedMember) {
				$scope.familyMembers[j].availability = Number($scope.familyMembers[j].availability) + 1;
				if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
					$scope.refresh();
				}

			}
		}
		UtilityService.disableProgressTab = true;
	};

	//To toggle the display of myself/member block
	function toggleSlide1(tabname) {
		var chkFlag = $(tabname).css("display");
		if (chkFlag == "block") {
			$(tabname).slideUp("slow", function() {
			});
		}
	}

	//Function called on click of save button to add/edit member details
	$scope.saveBeneficiary = function() {
		$scope.saveFamilyDetails();
		UtilityService.disableProgressTab = false;
		if ($scope.edit == true) {
			$scope.edit = false;
		} else {
			// Binding the model to Beneficiary scope variable. On saving setting the scope variable back to FNAObject model beneficary mapping
			$scope.FNABeneficiaries[$scope.memberIndex] = $scope.Beneficiary;
		}
		if (($scope.FNABeneficiaries[$scope.memberIndex].classNameAttached == "Son") || ($scope.FNABeneficiaries[$scope.memberIndex].classNameAttached == "Father") || ($scope.FNABeneficiaries[$scope.memberIndex].classNameAttached == "Brother")) {
			$scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.gender = "Male";
		} else {
			$scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.gender = "Female";
		}

		$scope.defaultImageForMembers[$scope.memberIndex].photo = $scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.photo;
		$scope.Beneficiary = {};
		for (var i = 1; i <= 10; i++) {
			$("#detailsBtn" + (i)).attr("disabled", false);
			$("#selfdetailsBtn").attr("disabled", false);
		}
		$('.scrapButton').attr("disabled", false);

		globalService.setFNABeneficiaries($scope.FNABeneficiaries);
		var parties = globalService.getParties();
		$scope.FNAObject.FNA.parties = parties;

		FnaVariables.setFnaModel($scope.FNAObject);
		$scope.status = "Initial";
		var obj = new FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
		obj.save();
	};

	//Function called on click of save button to add/edit myself details
	$scope.saveInsured = function() {
		$scope.saveSelfDetails();
		UtilityService.disableProgressTab = false;

		globalService.setParty($scope.FNAMyself, "FNAMyself");
		var parties = globalService.getParties();
		$scope.FNAObject.FNA.parties = parties;

		FnaVariables.setFnaModel($scope.FNAObject);
		$scope.status = "Initial";
		for (var i = 1; i <= 10; i++) {
			$("#detailsBtn" + (i)).attr("disabled", false);
		}
		//$scope.insuredPhotoErrorCountUpdate();
		var obj = new FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
		obj.save();
	};

	//Function called on click of cancel button of member details section
	$scope.cancelBeneficiary = function() {
		$scope.cancelFamilyDetails();
		UtilityService.disableProgressTab = false;
		/** Member section model is bind to  scope variable 'Beneficiary'. Making this variable blank since we are displaying the same variable for all members -- Begin**/
		$scope.Beneficiary.BasicDetails = {
			"firstName" : "",
			"lastName" : ""
		};
		$scope.Beneficiary.ContactDetails = {
			"homeNumber1" : "",
			"emailId" : ""
		};
		/** Member section model is bind to  scope variable 'Beneficiary'. Making this variable blank since we are displaying the same variable for all members -- End**/

		//Initially we are keeping the previous data to BeneficiaryTemp variable.
		//On cancelling, setting teh values back from temp variable to FNAObject mapping
		$scope.FNABeneficiaries[$scope.editIndex] = $scope.BeneficiaryTemp;
		globalService.setFNABeneficiaries($scope.FNABeneficiaries);
		$scope.defaultImageForMembers[$scope.editIndex].photo = $scope.BeneficiaryTemp.BasicDetails.photo;
		for (var i = 1; i <= 10; i++) {
			$("#detailsBtn" + (i)).attr("disabled", false);
			$("#selfdetailsBtn").attr("disabled", false);
		}
		$('.scrapButton').attr("disabled", false);
		$scope.benificiaryPhotoValidation = "true";
		if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
			$scope.refresh();
		}
	};

	//Function called on click of cancel button  of myself section
	$scope.cancelInsured = function() {
		$scope.cancelSelfDetails();
		UtilityService.disableProgressTab = false;
		$scope.FNAMyself = {};
		$scope.FNAMyself = JSON.parse(JSON.stringify($scope.InsuredTemp));
		for (var i = 1; i <= 10; i++) {
			$("#detailsBtn" + (i)).attr("disabled", false);
		}
		$scope.myselfPhotoValidation = "true";
		//$scope.insuredPhotoErrorCountUpdate();
	};

	//Update error count functionality

	$scope.updateErrorCount = function(arg1, arg2) {
		$rootScope.updateErrorCount(arg1, arg2);
		//for beneficiary DOB
		if ($scope.Beneficiary.BasicDetails != undefined && arg1 == 'aboutMeBeneficiary') {
			var dob = $scope.Beneficiary.BasicDetails.dob;
			if (LEDate(dob) > LEDate()) {
				$scope.errorCount = parseInt($scope.errorCount) + 1;
				$scope.refresh();
			}
		}
		//for self DOB
		if ($scope.FNAMyself.BasicDetails != undefined && arg1 == 'aboutMeInsured') {
			var dob = $scope.FNAMyself.BasicDetails.dob;
			if (LEDate(dob) > LEDate()) {
				$scope.errorCount = parseInt($scope.errorCount) + 1;
				$scope.refresh();
			}
		}
	};
	$scope.showErrorPopup = function(arg1, arg2) {
		$rootScope.showErrorPopup(arg1, arg2);
		//for beneficiary DOB
		if ($scope.Beneficiary.BasicDetails != undefined && arg1 == 'aboutMeBeneficiary') {
			var dob = $scope.Beneficiary.BasicDetails.dob;
			if (LEDate(dob) > LEDate()) {
				var error = {};
				error.message = translateMessages($translate, "fna.beneficiaryFututreDobValidationMessage");
				// DOB is a future date
				error.key = 'fnaBeneficiaryDOB';
				$scope.errorMessages.push(error);
			}

		}
		//for self DOB
		if ($scope.FNAMyself.BasicDetails != undefined && arg1 == 'aboutMeInsured') {
			var dob = $scope.FNAMyself.BasicDetails.dob;
			if (LEDate(dob) > LEDate()) {
				var error = {};
				//re-using error message for future DOB validation in beneficiaries
				error.message = translateMessages($translate, "fna.beneficiaryFututreDobValidationMessage");
				//DOB is a future date
				error.key = 'fnaInsuredDOB';
				$scope.errorMessages.push(error);
			}
		}

	};

	//Function called on click of Add/edit details button of Member
	$scope.addEdit = function(index, event) {
		$scope.addEditFamilyDetails(event);
		UtilityService.disableProgressTab = true;
		$scope.memberIndex = index;

		$scope.Beneficiary.BasicDetails = {
			"firstName" : "",
			"lastName" : ""
		};
		$scope.Beneficiary.ContactDetails = {
			"homeNumber1" : "",
			"emailId" : ""
		};
		if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
			$scope.refresh();
		}
		$scope.dynamicErrorCount = 0;
		$scope.dynamicErrorMessages = [];
		if ($scope.FNABeneficiaries[index] != "null" && typeof $scope.FNABeneficiaries[index] != "undefined") {
			$scope.Beneficiary = $scope.FNABeneficiaries[index];
			$scope.edit = true;
			$scope.BeneficiaryTemp = JSON.parse(JSON.stringify($scope.FNABeneficiaries[index]));
			$scope.editIndex = index;
			$scope.benificiaryPhotoValidation = "true";
			//for check the image in edit mode
			if ($scope.FNABeneficiaries[index].BasicDetails && $scope.FNABeneficiaries[index].BasicDetails.photo != "") {
				$scope.benificiaryPhotoValidation = "false";
			}

			$scope.refresh();
			for (var i = 1; i <= 10; i++) {
				$("#detailsBtn" + (i)).attr("disabled", "disabled");
				$("#selfdetailsBtn").attr("disabled", "disabled");
			}
			$("#detailsBtn" + (index + 1)).attr("disabled", false);
		} else {

			$scope.Beneficiary.BasicDetails = {
				"firstName" : "",
				"lastName" : ""
			};
			$scope.Beneficiary.ContactDetails = {
				"homeNumber1" : "",
				"emailId" : ""
			};
		}
		$('.scrapButton').attr("disabled", "disabled");
		$('.scrapButton').css("display", "none");
		$scope.refresh();
		$scope.updateErrorCount('aboutMeBeneficiary');
		//$rootScope.updateErrorCount('aboutMeBeneficiary');
	};

	//Function called on click of Add/edit details button of Insured
	$scope.aboutMe = function() {
		$scope.addSelfDetails(event);
		UtilityService.disableProgressTab = true;
		$scope.InsuredTemp = JSON.parse(JSON.stringify($scope.FNAMyself));
		//$scope.insuredPhotoErrorCountUpdate();
		for (var i = 1; i <= 10; i++) {
			$("#detailsBtn" + (i)).attr("disabled", "disabled");
		}
		if ($scope.InsuredTemp.BasicDetails.photo != FnaVariables.myselfPhoto) {
			$scope.myselfPhotoValidation = "false";
			$scope.refresh();
		}
		if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
			$scope.refresh();
		}
		$scope.updateErrorCount('aboutMeInsured');
		//$rootScope.updateErrorCount('aboutMeInsured');
	};

	//Enable/Disable tab on clicking Next/Back button of menu bar
	$scope.disableTab = function() {
		if (UtilityService.disableProgressTab) {
			for (var j = 0; j < $scope.pageSelections.length; j++) {
				if ($scope.pageSelections[j].pageType == "Product Recommendation") {
					$scope.pageSelections[j].isDisabled = "disabled";
				}
			}
		}
		$scope.FNAObject.FNA.pageSelections = $scope.pageSelections;
		FnaVariables.setFnaModel($scope.FNAObject);
	};

	//Function called on clicking Proceed button
	$scope.nextPage = function() {
		$scope.InsuredTemp = JSON.parse(JSON.stringify($scope.FNAMyself));
		if ($scope.InsuredTemp.BasicDetails.photo != FnaVariables.myselfPhoto) {
			$scope.myselfPhotoValidation = "false";
			$scope.refresh();
		}
		$scope.updateErrorCount('aboutMeInsured');
		//$rootScope.updateErrorCount('aboutMeInsured');
		$scope.errorCount = $scope.errorCount - $scope.dynamicErrorCount;
		//$scope.errorCount = $scope.errorCount + $scope.insuredPhotoErrorCount; ??
		$scope.disableTab();
		if ($scope.errorCount == 0) {

			for (var i = 0; i < FnaVariables.pages.length; i++) {
				if (FnaVariables.pages[i].url == "/setgoal") {
					$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
					break;
				}
			}

			globalService.setParty($scope.FNAMyself, "FNAMyself");
			globalService.setFNABeneficiaries($scope.FNABeneficiaries);
			var parties = globalService.getParties();
			$scope.FNAObject.FNA.parties = parties;

			FnaVariables.setFnaModel($scope.FNAObject);
			$scope.status = "Initial";
			var obj = new FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
			obj.save(function() {
				$location.path('/setgoal');
			});
		} else {
			$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "fna.selectionError"), translateMessages($translate, "general.productCancelOption"));
		}

	};

	//document Object mapping done for  capture/Browse file
	$scope.mapDocScopeToPersistence = function(document) {
		var docObject = {};
		var createdDate = UtilityService.getFormattedDate();
		docObject.id = null;
		docObject.parentId = ($rootScope.transactionId != null && $rootScope.transactionId != 0) ? $rootScope.transactionId : null;
		docObject.documentType = "Photo";
		docObject.documentName = document != null && document.documentName != null ? document.documentName : "";
		docObject.documentStatus = document != null && document.documentStatus != null ? document.documentStatus : "";
		docObject.date = createdDate;
		if ((rootConfig.isOfflineDesktop) || !(rootConfig.isDeviceMobile)) {
			docObject.documentObject = document;
		}
		return docObject;
	};

	// Function called on Image btn click
	$scope.captureFile = function(isInsured) {
		var docCount = 0;
		$scope.isInsured = isInsured;
		var currAgentId = $rootScope.agentId == null ? $rootScope.username : $rootScope.agentId;
		var currentDoc;
		var document;
		if (isInsured) {
			currentDoc = $scope.FNAMyself.BasicDetails.photo;
		} else if ( typeof $scope.Beneficiary.BasicDetails != "undefined") {
			currentDoc = $scope.Beneficiary.BasicDetails.photo;
		}
		if ( typeof currentDoc == "undefined" || currentDoc == "") {
			document = $scope.mapDocScopeToPersistence({});
		} else {
			document = $scope.mapDocScopeToPersistence(currentDoc);
		}
		var index = 0;
		DocumentService.captureFile(document, index, currAgentId, $scope.onFileSelectSuccess, $scope.operationError);
	};

	// Function called on browse buttton  click
	$scope.browseFile = function(isImage, isPhoto, fileObj, isInsured) {
		$scope.isImage = isImage;
		$scope.isPhoto = isPhoto;
		$scope.isSignature = false;
		$scope.isInsured = isInsured;
		var docCount = 0;
		var currAgentId = $rootScope.agentId == null ? $rootScope.username : $rootScope.agentId;
		if ((rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
			var currentDoc = {};
			currentDoc = $scope.LifeEngageProduct.Upload.Photograph[0];
			var document = $scope.mapDocScopeToPersistence(currentDoc);
		} else {
			var document = $scope.mapDocScopeToPersistence(fileObj);
		}
		if (!document.documentName) {
			var currAgentId = $rootScope.agentId == null ? $rootScope.username : $rootScope.agentId;
			var d = new Date();
			var n = d.getTime();
			var name = "document";
			var newFileName = "Agent1" + n + "_" + name;
			document.documentName = newFileName;
		}
		DocumentService.browseFile(document, currAgentId, $scope.onFileSelectSuccess, $scope.operationError);
	};

	//SuccessCallback function for capturing/browsing an image
	$scope.onFileSelectSuccess = function(document) {

		
		document.documentStatus = "Initial";

		globalService.setParty($scope.FNAMyself, "FNAMyself");
		globalService.setFNABeneficiaries($scope.FNABeneficiaries);
		var parties = globalService.getParties();
		$scope.FNAObject.FNA.parties = parties;

		var obj = new FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
		obj.save(function(data) {
			if (document.parentId == null) {
				$rootScope.transactionId = data;
				document.parentId = $rootScope.transactionId;
				$scope.status = "Initial";
			}

			if (document.documentType == "Photo") {
				if ($scope.isInsured) {
					if ((rootConfig.isOfflineDesktop) || !(rootConfig.isDeviceMobile)) {
						$scope.FNAMyself.BasicDetails.photo = document.documentPath.replace("data:image/jpeg;base64,", "");
						$scope.myselfPhotoValidation = "false";


					} else {
						$scope.FNAMyself.BasicDetails.photo = document.base64string;
						$scope.myselfPhotoValidation = "false";

					}
					//$scope.insuredPhotoErrorCountUpdate();

				} else {
					if ( typeof $scope.Beneficiary.BasicDetails == "undefined") {
						$scope.Beneficiary.BasicDetails = {};
					}
					if ((rootConfig.isOfflineDesktop) || !(rootConfig.isDeviceMobile)) {
						$scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.photo = document.documentPath.replace("data:image/jpeg;base64,", "");
						$scope.defaultImageForMembers[$scope.memberIndex].photo = document.documentPath.replace("data:image/jpeg;base64,", "");
						$scope.benificiaryPhotoValidation = "false";

					} else {
						$scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.photo = document.base64string;
						$scope.defaultImageForMembers[$scope.memberIndex].photo = document.base64string;
						$scope.benificiaryPhotoValidation = "false";

					}
                    $timeout(function(){
                         $scope.updateErrorCount($scope.isInsured?'aboutMeInsured':'aboutMeBeneficiary');
                    });
				}
			}
		});
		$rootScope.showHideLoadingImage(false, "");
	};

	$scope.hidePreView = function() {
		$('#dvPreview').css("display", "none");
	};

	$scope.benificiaryPhotoErrorCountUpdate = function() {
		$scope.dynamicErrorCount = $scope.dynamicErrorCount - 1;
	};

	$scope.insuredPhotoErrorCountUpdate = function() {
		$scope.dynamicErrorMessages = [];
		$scope.dynamicErrorCount = 0;
		$scope.insuredPhotoErrorCount = 0;

		if ($scope.FNAMyself.BasicDetails.photo == FnaVariables.myselfPhoto) {
			var error = {};
			error.message = translateMessages($translate, "fna.fnaInsuredPhotoRequiredValidationMessage");
			error.key = "fnaInsuredPhoto";
			$scope.dynamicErrorMessages.push(error);
			$scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
			$scope.insuredPhotoErrorCount = 1;
			$scope.updateErrorCount('aboutMeInsured');
			//$rootScope.updateErrorCount('aboutMeInsured');
		}
	};

	/*
	 * LOGIC FOR HANDLING THE MY FAMILY DRAG AND DROP, SLIDE, ADD PROFILE
	 * DETAILS FUNCIONALITIES
	 *
	 * LOGIC NOT HANDLED: WITH ONE TAB OPENED, IF WE TRY TO CLICK ANOTHER TAB,
	 * THE ALREADY OPENED TAB SHOULD CLOSE AND THE NEXT SHOULD BE OPENED
	 */

	/* Should be outside the ready function */

	var g_clickedSlideDwnBtn;
	var classToBePassed;
	var classtoBePassedFamily;
	var g_closestRelnText;
	var sub_len;

	//To hide the myself details section on save button click
	$scope.saveSelfDetails = function() {
		var clName = $('#selfdetailsBtn').attr("class");
		var translateEditDetailsSave = translateMessages($translate, "fna.editDetails");
		/* Change state from About Me to "Edit Details */
		if (clName == "addDetlBtn selectedState aboutMeButton") {

			$('#selfdetailsBtn').removeClass().addClass("editDetlBtn").html(translateEditDetailsSave);
			/* SHOW NAME /DOB and adjust the container height */
			$('.savedDetails').css("display", "block");
			/* $('.sel_myself_wrapper').css("min-height","240px"); */

			/* HIDE THE TEXT & Display the profile image */
			// $('.icon_myself_text').css("display","none");
			$('.icon_myself_text').css("display", "block");
			$('.boxshadow_selmyself').removeClass('').addClass("userpicture");

			$scope.toggleSlide(classToBePassed);
		}
		if (clName == "editDetlBtn aboutMeButton") { translateEditDetailsSave
			$('#selfdetailsBtn').removeClass().addClass("editDetlBtn").html(translateEditDetailsSave);
			$scope.toggleSlide(classToBePassed);
		}
	};

	//Function called on click of cancel button  of myself section
	$scope.cancelSelfDetails = function() {
		var chkflag = $('#selfdetailsBtn').attr("class");
		var translateEditDetails = translateMessages($translate, "fna.editDetails");
		if (chkflag == 'addDetlBtn selectedState aboutMeButton') {
			$('#selfdetailsBtn').removeClass().addClass("addDetlBtn");

			$('#selfdetailsBtn').html(translateEditDetails);
			classToBePassed = ".myfulldetails";
			/* Slide down and show the details tab */
			$scope.toggleSlide(classToBePassed);

		}

		if (chkflag == 'editDetlBtn aboutMeButton') {
			
			$('#selfdetailsBtn').removeClass().addClass("editDetlBtn");
			$('#selfdetailsBtn').html(translateEditDetails);
			classToBePassed = ".myfulldetails";
			/* Slide down and show the details tab */
			$scope.toggleSlide(classToBePassed);

		}
	};

	//Subfunction called on add/edit button click of Myself
	$scope.addSelfDetails = function(event) {
		var thisObj = event.currentTarget;
		g_clickedSlideDwnBtn = String(thisObj.className);

		/*
		 * Has only 1 class addDetlBtn, then on click, it should be toggled to
		 * "About me"
		 */
		if (g_clickedSlideDwnBtn == "addDetlBtn") {
			$(thisObj).addClass("selectedState aboutMeButton");
			var translateAboutMe = translateMessages($translate, "fna.aboutme");
			$(thisObj).html(translateAboutMe);
			$scope.refresh();
			classToBePassed = ".myfulldetails";
			/* Slide down and show the details tab */
			$scope.toggleSlide(classToBePassed);

		}
		/* The button is in About Me state now, toggle back to "Add details" */
		/*
		 * if(g_clickedSlideDwnBtn == 'addDetlBtn selectedState aboutMeButton'){
		 *
		 * $scope.toggleSlide(classToBePassed);
		 * $(thisObj).removeClass().addClass("addDetlBtn"); $(thisObj).html("Add
		 * Details"); }
		 */
		/* The button is Edit button now */
		if (g_clickedSlideDwnBtn == "editDetlBtn") {
			classToBePassed = ".myfulldetails";
			$scope.toggleSlide(classToBePassed);
			$(thisObj).removeClass().addClass("editDetlBtn aboutMeButton");
			var translateAboutMe = translateMessages($translate, "fna.aboutme");
			$(thisObj).html(translateAboutMe);
		}
		/*
		 * if(g_clickedSlideDwnBtn == "editDetlBtn aboutMeButton"){
		 * $scope.toggleSlide(classToBePassed);
		 * $(thisObj).removeClass("aboutMeButton"); $(thisObj).html("Edit
		 * Details"); }
		 */
	};

	// TODO - To check if its same as saveFamilyDetails and remove one
	$scope.cancelFamilyDetails = function() {
		/* ITEM TO WHICH THE IM AGE NEEDS TO BE SAVED */
		// var btntext = "#detailsBtn" + g_placeholder;
		var btntext = "#detailsBtn" + sub_len;
		var translateEditDetailsCancel = translateMessages($translate, "fna.editDetails");
		$(btntext).removeClass().addClass("editdetl_myfamily").html(translateEditDetailsCancel);
		$scope.toggleSlide(classtoBePassedFamily);
	};

	//Function called to Slide up the tab details on saving a member
	$scope.saveFamilyDetails = function() {
		/* ITEM TO WHICH THE IM AGE NEEDS TO BE SAVED */
		// var btntext = "#detailsBtn" + g_placeholder;
		var btntext = "#detailsBtn" + sub_len;
		var translateEditDetails = translateMessages($translate, "fna.editDetails");
		$(btntext).removeClass().addClass("editdetl_myfamily").html(translateEditDetails);

		$('.owl-wrapper .owl-item').eq(g_placeholder - 1).children().children(".savedetl_wrapper").children().css("display", "block");
		
		/*
		 * Find the list item corresponding to the button to change the image on
		 * save button click
		 */
		var equation = g_placeholder - 1;
		$('.owl-wrapper .owl-item').eq(equation).find('.nonmemberwrapper').addClass("kerry_lady").children().remove();
		/* Slide up the tab details */
		$scope.toggleSlide(classtoBePassedFamily);
	};

	//SubFunction called on click fo Add/Edit details of family member
	$scope.addEditFamilyDetails = function(event) {
		var thisObj = event.currentTarget;
		var thisObjId = thisObj.id;
		var thisObjClass = String(thisObj.className);
		/*
		 * Store the number extracted from the ID in a global placeholder as a
		 * flag
		 */
		sub_len = thisObjId.substring(10);
		// detailsBtn1 / 1
		g_placeholder = sub_len;
		// 1
		if (thisObjClass.indexOf("addetl_myfamily") >= 0) {
			var chklength = global_val_array[g_placeholder - 1].length;
			$scope.toggleSlideforFamily(thisObj, thisObjId, thisObjClass, chklength);
		} else if (thisObjClass.indexOf("editdetl_myfamily") >= 0) {
			$scope.toggleSlideforFamily(thisObj, thisObjId, thisObjClass);
		}
	};

	//Function to toggle the subsetcion of member details . Change the labels and styles
	$scope.toggleSlideforFamily = function(thisObject, thisId, thisClass, chklength) {
		/*
		 * Get the clicked button object, get its id, and class and extract the
		 * number to match the global placeholder, from the button id
		 */
		var thisObj = thisObject;
		var thisObjId = thisId;
		var thisObjClass = thisClass;
		classtoBePassedFamily = ".myfulltabdetails";
		var translateAboutMy = translateMessages($translate, "fna.aboutMy");
		var translateMemberMessage = "fna." + global_val_array[g_placeholder - 1];
		var translateMember = translateMessages($translate, translateMemberMessage);

		/* Need the button to be chnaged to "About my Son/Daughter/Wife etc */
		if (thisObjClass == "addetl_myfamily" && chklength > 0) {
			$(thisObj).removeClass("addetl_myfamily").addClass("aboutMeButton_myfamily");
			$(thisObj).html(translateAboutMy + translateMember + " ");
			$scope.refresh();
			$scope.toggleSlide(classtoBePassedFamily);
		}
		if (thisObjClass == "editdetl_myfamily") {
			$(thisObj).removeClass("editdetl_myfamily").addClass("aboutMeButton_myfamily");
			$(thisObj).html(translateAboutMy + translateMember + " ");
			$scope.refresh();
			$scope.toggleSlide(classtoBePassedFamily);
		}

	};

	$scope.toggleSlide = function(tabname) {
		/* CHECK FOR TOGGLED STATE */
		var chkFlag = $(tabname).css("display");
		if (chkFlag == "block") {
			$(tabname).slideUp("slow", function() {
			});
		} else {
			$(tabname).slideDown("slow", function() {
			});
		}
	};

	/** Fill member mock data for demo purpose**/
	$scope.fillMockDataBeneficiary = function() {
		var beneficairy = $scope.FNABeneficiaries[$scope.memberIndex];
		var className = $scope.FNABeneficiaries[$scope.memberIndex].classNameAttached;
		if (className == "Son" || className == "Father" || className == "Brother" || className == "Husband") {
			$scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.firstName = "George";
			$scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.lastName = "Smith";
		} else {
			$scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.firstName = "Linda";
			$scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.lastName = "Smith";
		}

		$scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.dob = "1990-11-06";
		$scope.FNABeneficiaries[$scope.memberIndex].OccupationDetails = {};
		$scope.FNABeneficiaries[$scope.memberIndex].OccupationDetails.description = "Business Concultant";
		$scope.FNABeneficiaries[$scope.memberIndex].ContactDetails = {};
		$scope.FNABeneficiaries[$scope.memberIndex].ContactDetails.homeNumber1 = "9876523567";

		$scope.FNABeneficiaries[$scope.memberIndex].ContactDetails.emailId = "smith@testmail.com";
		$scope.FNABeneficiaries[$scope.memberIndex].ContactDetails.currentAddress = {};
		$scope.FNABeneficiaries[$scope.memberIndex].ContactDetails.currentAddress.address = "Singapore City";
	};

	/** Fill insured mock data for demo purpose**/
	$scope.fillMockDataInsured = function() {

		if ($scope.FNAMyself.ContactDetails.homeNumber1 == "") {
			$scope.FNAMyself.ContactDetails.homeNumber1 = "9865432789";
		}
		if ($scope.FNAMyself.BasicDetails.firstName == "") {
			$scope.FNAMyself.BasicDetails.firstName = "John";
		}
		if ($scope.FNAMyself.BasicDetails.lastName == "") {
			$scope.FNAMyself.BasicDetails.lastName = "Smith";
		}
		if ($scope.FNAMyself.BasicDetails.dob == "") {
			$scope.FNAMyself.BasicDetails.dob = "1978-11-06";
		}
		if ($scope.FNAMyself.OccupationDetails.description == "") {
			$scope.FNAMyself.OccupationDetails.description = "Journalist";
		}
		if ($scope.FNAMyself.ContactDetails.emailId == "") {
			$scope.FNAMyself.ContactDetails.emailId = "johnSmith@gmail.com";
		}
		if ($scope.FNAMyself.ContactDetails.currentAddress.address == "") {
			$scope.FNAMyself.ContactDetails.currentAddress.address = "Singapore";
		}

	};
	$scope.initialLoad= function(){
		LEDynamicUI.paintUI(rootConfig.template, myFamilyDetailsUiJsonFile, "aboutMeInsured", "#aboutMeInsured", true, function() {
		$scope.callback();
	}, $scope, $compile);
	myFamilyDetailsUiJsonFile = FnaVariables.uiJsonFile.fnaBenificiaryFile;
	LEDynamicUI.paintUI(rootConfig.template, myFamilyDetailsUiJsonFile, "aboutMeBeneficiary", "#aboutMeBeneficiary", true, function() {
		$scope.callback();
	}, $scope, $compile);
	}
	$scope.$on('$viewContentLoaded', function(){
		$scope.initialLoad();
	  });
}]);