/*
 * Copyright 2015, LifeEngage 
 * Name:GLI_GoalController
 * For implementing GLI specific changes
 */
storeApp.controller('GLI_GoalController',['$rootScope', '$route', '$scope', '$location', '$compile', '$routeParams','DataService', 'LookupService', 'DocumentService', 'FnaVariables', 'GLI_FnaService','$translate', 'UtilityService', '$debounce', 'AutoSave', 'globalService','$controller',
function GLI_GoalController($rootScope, $route, $scope, $location, $compile, $routeParams,DataService, LookupService, DocumentService, FnaVariables, GLI_FnaService,$translate, UtilityService, $debounce, AutoSave, globalService,$controller){
$controller('GoalController',{$rootScope:$rootScope, $route:$route, $scope:$scope, $location:$location, $compile:$compile, $routeParams:$routeParams,DataService:DataService, LookupService:LookupService, DocumentService:DocumentService, FnaVariables:FnaVariables, FnaService:GLI_FnaService,$translate:$translate, UtilityService:UtilityService, $debounce:$debounce, AutoSave:AutoSave, globalService:globalService});
$scope.status = FnaVariables.getStatusOptionsFNA()[0].status;
$rootScope.selectedPage = "GoalController";
$scope.goalpref = [{
	"prioritynum" : "1",
		"prioritytext" : "Priority",		
	}, {
		"prioritynum" : "2",
		"prioritytext" : "Priority",		
	}, {
		"prioritynum" : "3",
		"prioritytext" : "Priority",		
	}, {
		"prioritynum" : "4",
		"prioritytext" : "Priority",		
	}, {
		"prioritynum" : "5",
		"prioritytext" : "Priority",
	},{
	"prioritynum" : "6",
	"prioritytext" : "Priority"
},{
	"prioritynum" : "7",
	"prioritytext" : "Priority"
}
/* FNA changes by LE Team
,{
	"prioritynum" : "8",
	"prioritytext" : "Priority"
}
*/
];

$scope.FNAObject.FNA.beneficiaryPhotos = $scope.beneficiaryPhotos;
/* FNA changes by LE Team
$scope.mandatoryGoals = [];
$scope.lifestageMandatoryGoals = [];
*/
$scope.currentPageType  = "Need Analysis";
$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;
UtilityService.disableProgressTab = false;

//enabling and disabling the tabs
GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);

$scope.removeGoal = function($scope, priority) {
	FnaVariables.goalsCount = FnaVariables.goalsCount - 1;
	FnaVariables.goalsTotal = Number(FnaVariables.goalsTotal) - Number(priority);
	var removedGoal;
	var index;
	for (var i = 0; i < $scope.FNAObject.FNA.goals.length; i++) {
		if ($scope.FNAObject.FNA.goals[i].priority == priority) {
			removedGoal = $scope.FNAObject.FNA.goals[i].goalName;

			$scope.FNAObject.FNA.goals.splice(i, 1);
			$scope.refresh();
		}
	}
	for (var j = 0; j < $scope.goals.length; j++) {
		if ($scope.goals[j].goalName == removedGoal) {
			// alert("b4"+$scope.goals[j].availability);
			$scope.goals[j].availability = Number($scope.goals[j].availability) + 1;
			// alert("aftr"+$scope.goals[j].availability);
			$scope.refresh();

		}
	}
	UtilityService.disableProgressTab = true;
	GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
};
$scope.setGoal = function($scope, priority, goal) {
	UtilityService.disableProgressTab = true;
	//GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
	var selectedGoal = {}
	for (var i = 0; i < $scope.goals.length; i++) {
		if ($scope.goals[i].goalicon == goal) {
			selectedGoal.goalName = $scope.goals[i].goalName;
			selectedGoal.translateID = $scope.goals[i].translateID;
			$scope.goals[i].availability = $scope.goals[i].availability - 1;
			selectedGoal.sliderUnit = $scope.goals[i].sliderUnit;
			selectedGoal.goalId = $scope.goals[i].goalId;
			selectedGoal.goalReportText = $scope.goals[i].goalReportText;
			$scope.refresh();

		}

	}
	FnaVariables.goalsCount = FnaVariables.goalsCount + 1;
	FnaVariables.goalsTotal = Number(FnaVariables.goalsTotal) + Number(priority);
	selectedGoal.priority = priority;
	selectedGoal.parameters = [];
	selectedGoal.productList = [];
	$scope.FNAObject.FNA.goals.push(selectedGoal);
	GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
};

$scope.$on('initiate-save', function (evt, obj) {
	//$scope.disableLePopCtrl = true;
	globalService.setParty($scope.FNAMyself, "FNAMyself");
	var parties = globalService.getParties();
	$scope.FNAObject.FNA.parties = parties;		
	FnaVariables.setFnaModel($scope.FNAObject);
	$scope.status = "Draft";
	var obj = new GLI_FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
	obj.save(function() {
		$rootScope.showHideLoadingImage(false, "");
		$rootScope.passwordLock  = false;
		$rootScope.isAuthenticated = false;
		$location.path('/login');
	});
});

//over writing function to rename status  initial to draft 
$scope.nextPage = function() {
	
	var lifeStageSize = $scope.lifeStageSpecificGoals.length;
    var goalSize = 	$scope.FNAObject.FNA.goals.length;
    var pendingGoals = [];
  	//$scope.unselectedGoals = angular.copy($scope.goals);
    //added for setting the unselected goals
    $scope.unselectedGoals= [];
  	var count = [];
  	var unselected =  true;
    for(var i=0 ; i < $scope.goals.length ; i++){
    	loop: for(var j=0 ; j < goalSize ; j++){
    		if($scope.goals[i].goalName == $scope.FNAObject.FNA.goals[j].goalName){
    			unselected =  true;
    			break loop;
    		}else{
    			unselected =  false;
    		}
    	}
    if(!unselected){
    	$scope.unselectedGoals.push($scope.goals[i]);
    }
    }
    $scope.FNAObject.FNA.unselectedGoals = $scope.unselectedGoals;
    
    //priorityInfo text
    for(var j=0;j<goalSize;j++)
	{
		var priority = $scope.FNAObject.FNA.goals[j].priority;
		var priorityinfoInput= "priorityinfo"+priority;
		var priorityInfoText = $('#'+priorityinfoInput).val();
		$scope.FNAObject.FNA.goals[j].priorityInfo = priorityInfoText;
	}
 
 /* FNA changes by LE Team
    //checking for all mandatory goals
	for(var i=0;i<lifeStageSize;i++)
	{		
		if($scope.lifestageMandatoryGoals[i] != undefined){
			
			var lifeStageGoalName =$scope.lifestageMandatoryGoals[i];
			var isGoalMandatory = true;
			if(isGoalMandatory)
			{
				var status=  false;
				for(var j=0;j<goalSize;j++)
				{				
					var goalName =$scope.FNAObject.FNA.goals[j].goalName;
					if(lifeStageGoalName == goalName)
					{					
						status = true;
						break;
					}
				}
				if(status == false)
					pendingGoals.push(lifeStageGoalName);			
			}	
		}
	}
*/
	
	//check for priorityOne goal
	var priorityOneStatus=  false;
	for(var j=0;j<goalSize;j++)
	{
		var priority = $scope.FNAObject.FNA.goals[j].priority;
		if(priority == "1")
		{					
			priorityOneStatus = true;
			break;
		}
	}
	$scope.disableTab();

	var match = FnaVariables.goalsCount
			* (Number(FnaVariables.goalsCount) + Number(1)) / 2;
	
	if (!priorityOneStatus)
	{
		$scope.errorMessage = "fna.priorityOneGoalCheckMessage";
		$scope.popup_setpref_container = true;		
		$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, $scope.errorMessage), translateMessages($translate, "illustrator.okMessageInCaps"));
	}
	else if (!(match == FnaVariables.goalsTotal && match != 0)){
		$scope.errorMessage = "fna.blankError";
		$scope.popup_setpref_container = true;		
		$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, $scope.errorMessage), translateMessages($translate, "illustrator.okMessageInCaps"));
	}
	else if (pendingGoals.length >0){		
		$scope.errorMessage = "fna.priorityGoalsCheckMessage";
		$scope.popup_setpref_container = true;		
		$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, $scope.errorMessage), translateMessages($translate, "illustrator.okMessageInCaps"));
	}
	else if (match == FnaVariables.goalsTotal && match != 0) {
		$scope.FNAObject.FNA.goals = $scope.sortGoals($scope.FNAObject.FNA.goals,
				'priority');
		for (var i = 0; i < FnaVariables.pages.length; i++) {
			if (FnaVariables.pages[i].url == "/fnaCalculator") {
				$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
				break;
			}
		}
		FnaVariables.setFnaModel($scope.FNAObject);

		$scope.status = "Draft";
		var obj = new GLI_FnaService.saveTransactions($scope, $rootScope,
				DataService, $translate, UtilityService, false);
		obj.save(function() {
			$location.path('/fnaCalculator');
		});
	} else {
		if (match == 0) {			
			$scope.errorMessage = "fna.priorityGoalsCheckMessage";
		}
		else if (pendingGoals.length >0){		
			$scope.errorMessage = "fna.priorityGoalsCheckMessage";
		}
		else if (!priorityOneStatus)
		{
			$scope.errorMessage = "fna.priorityOneGoalCheckMessage";
		}
		else {
			$scope.errorMessage = "fna.blankError";
		}		
		$scope.popup_setpref_container = true;		
		$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, $scope.errorMessage), translateMessages($translate, "illustrator.okMessageInCaps"));

	}
	$scope.refresh();
};

	//to get goals depending on lifestage selected
	$scope.getLifeStageSpecificGoals = function() {
		
		$scope.lifeStageSpecificGoals = [];
		var lifeStage = $scope.FNAObject.FNA.lifeStage.description;
		var isSenior = $scope.FNAMyself.BasicDetails.isSenior;
		for (var j = 0; j < $scope.lifeStageGoals.length; j++) {
			if (isSenior == "true") {
				if ($scope.lifeStageGoals[j].lifeStage == "Senior Citizen") {
					$scope.specificGoals = $scope.lifeStageGoals[j].goals;
					//FNA changes by LE Team
					//$scope.mandatoryGoals = $scope.lifeStageGoals[j].mandatoryGoals;	
				}
			} else {
				if ($scope.lifeStageGoals[j].lifeStage == lifeStage) {
					$scope.specificGoals = $scope.lifeStageGoals[j].goals;
					//FNA changes by LE Team 
					//$scope.mandatoryGoals = $scope.lifeStageGoals[j].mandatoryGoals;
					break;
				}
			}
		}
		if ($scope.specificGoals) {
			for (var j = 0; j < $scope.specificGoals.length; j++) {
				for (var i = 0; i < $scope.goals.length; i++) {
					if ($scope.goals[i].goalName == $scope.specificGoals[j]) {
						$scope.lifeStageSpecificGoals.push($scope.goals[i]);
						/* FNA changes by LE Team
						if($scope.mandatoryGoals[i] != undefined){
							$scope.lifestageMandatoryGoals.push($scope.mandatoryGoals[i]);
						}*/
					}
				}
			}
			$scope.goals = $scope.lifeStageSpecificGoals;
		}
	};


	/* FNA changes by LE Team
	Update Mandatory goals in UI
	$scope.updateMandatoryGoals = function(goalName){

		for(i=0;i<$scope.mandatoryGoals.length;i++){
			var retClass = "";
			if($scope.mandatoryGoals[i] == goalName){
				retClass = "mandatory";
				return retClass;
				break;
			}
			
		}
		
	}
	Update Mandatory goals in UI
	*/
	
		$scope.getClass = function(priorityValue) {
		var className = "priorityorder";
		var index = priorityValue - 1;
		for (var i = 0; i < $scope.FNAObject.FNA.goals.length; i++) {
			for (var j = 0; j < $scope.goals.length; j++) {
				if ($scope.FNAObject.FNA.goals[i].goalName == $scope.goals[j].goalName) {

					if ($scope.FNAObject.FNA.goals[i].priority == priorityValue) {
						className = " priorityset " + $scope.goals[j].goalicon;

						$("#" + $scope.FNAObject.FNA.goals[i].priority + "").children(".priority_number,.priority_txt").css("display", "none");
						$("#" + $scope.FNAObject.FNA.goals[i].priority + "").parent('.priorityGoal-holder').addClass('item-dropped');
						$scope.goalpref[index].translate = $scope.FNAObject.FNA.goals[i].translateID;
						return className;
					}

				}

			}

		}

		return className;
	};
	

	/* video plugin integration - start */
	
	$scope.playVideo 	= function(index) {
	    if (rootConfig.isDeviceMobile == "false") 
	    {
	        var myVideo 			= document.getElementById("le_prod_video");
	        myVideo.currentTime 	= 0;
	        $scope.show_hide_popup 	= true;	    
	    } 
	    else
	    {
	        var fileFullPath 		= "";
	        window.plugins.LEFileUtils.getApplicationPath(function(path) {
	            var documentsPath 	= path; //contains till the app package name
	            var filename 		= translateMessages($translate, $scope.lifeStageSpecificGoals[index].goalVideo);	        
	            fileFullPath 		= "file://" + documentsPath + "/" + filename;                     
	           window.plugins.videoPlayer.play(fileFullPath);
	        }, function(e) {
	         
	        });
	    }
	}
	
	$scope.getPriorityInfoText = function(priorityIndex)
	{
		var goalLength = 	$scope.FNAObject.FNA.goals.length;
		var priorityInfo= "";
		for(var j=0;j<goalLength;j++)
		{
			var priority = $scope.FNAObject.FNA.goals[j].priority;
			if(priority == priorityIndex )
			{					
				priorityInfo = $scope.FNAObject.FNA.goals[j].priorityInfo;
			}
		}
		return priorityInfo;
	}	
	
	$scope.changingStatus = function()
	{
		UtilityService.disableProgressTab = true;
		GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
		//$scope.initializeProgressbarNew();
		//$route.reload();
	}
	
	 $scope.$on("$destroy", function() {
                 if (this.$$destroyed) return;     
				 while (this.$$childHead) {      
				 this.$$childHead.$destroy();    
				 }     
				 if (this.$broadcast)
				{ 
					this.$broadcast('$destroy');   
				}    
				 this.$$destroyed = true;                 
				 //$timeout.cancel( timer );                           
    });

}]);