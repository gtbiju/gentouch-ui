/*
 *Copyright 2015, LifeEngage 
 */

storeApp.controller('AboutMeController', ['$rootScope', '$route', '$scope', 'DataService', 'FnaService','LookupService', 'DocumentService', 'ProductService', 
'$compile', '$routeParams', '$location', '$translate', 'FnaVariables', 'UtilityService','$debounce', 'PersistenceMapping', 'AutoSave', 'globalService',
function($rootScope, $route, $scope, DataService, FnaService, LookupService,DocumentService, ProductService, $compile, $routeParams, $location, $translate, 
FnaVariables, UtilityService,$debounce, PersistenceMapping, AutoSave, globalService) {


	// Set the page name for progressbar implementation - Required in each page
	// specific controller
	FnaVariables.selectedPage = "About Me";
	$scope.selectedpage = FnaVariables.selectedPage;
	$rootScope.moduleHeader = "fna.fnaHeading";
	$rootScope.moduleVariable = translateMessages($translate,
			$rootScope.moduleHeader);
	$scope.FNAObject = FnaVariables.getFnaModel();
	$scope.FNAMyself = globalService.getParty("FNAMyself");
	$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;
	$scope.custLifeStages = FnaVariables.custLifeStagesList;
	$scope.custGenderSelections = FnaVariables.custGenderSelectionsList;
	$scope.cutomizeBtnClass;
	$scope.message;
	$scope.popup_proceed_button = false;
	$scope.cancel = function() {
		$scope.popup_proceed_button = false;
		if($scope.lifeStageEditChanged){
			for ( var cuslifeStage in $scope.custLifeStages) {
				$scope.custLifeStages[cuslifeStage].lifestageSelected = "";
				if ($scope.custLifeStages[cuslifeStage].lifestage == previousLifeStageId) {
					$scope.custLifeStages[cuslifeStage].lifestageSelected = "_selected";
				}
			}
			
		   $scope.lifeStageClick($scope.previousLifeStage,previousLifeStageId);
		}
		if($scope.genderEditChanged){
			for ( var custGender in $scope.custGenderSelections) {
				$scope.custGenderSelections[custGender].genderSelected = "";
				if ($scope.custGenderSelections[custGender].gender == isPreviousGenderSeniorId) {
					$scope.custGenderSelections[custGender].genderSelected = "_selected";
				}
			}
		   $scope.genderSelectionClick(isPreviousGenderSeniorId);
		}
	};
	
	$scope.onGenderSelectionClick = function(genderSelectionObj){
		var id = genderSelectionObj.gender
		for ( var custGender in $scope.custGenderSelections) {
			$scope.custGenderSelections[custGender].genderSelected = "";
			if ($scope.custGenderSelections[custGender].gender == id) {
				$scope.custGenderSelections[custGender].genderSelected = "_selected";
			}
		}
		$scope.genderSelectionClick(id);
	};
	$scope.onLifeStageClick = function(lifeStageObj){
		var id = lifeStageObj.id;
		var lifestage = lifeStageObj.lifestage;
		for ( var cuslifeStage in $scope.custLifeStages) {
			$scope.custLifeStages[cuslifeStage].lifestageSelected = "";
			if ($scope.custLifeStages[cuslifeStage].lifestage == lifestage) {
				$scope.custLifeStages[cuslifeStage].lifestageSelected = "_selected";
			}
		}
		$scope.lifeStageClick(id,lifestage);
	};
	
	// Edit flow - set the lifeStage image class as selected or highlight the
	// image
	if ($scope.FNAObject.FNA.lifeStage
			&& $scope.FNAObject.FNA.lifeStage.id != "") {
		$scope.previousLifeStage = $scope.FNAObject.FNA.lifeStage.description;
		previousLifeStageId = $scope.FNAObject.FNA.lifeStage.id;
		for ( var cuslifeStage in $scope.custLifeStages) {
			$scope.custLifeStages[cuslifeStage].lifestageSelected = "";
			if ($scope.custLifeStages[cuslifeStage].lifestage == $scope.FNAObject.FNA.lifeStage.id) {
				$scope.custLifeStages[cuslifeStage].lifestageSelected = "_selected";
			}
		}
	}

	// Edit flow - set the gender image class as selected or highlight the image
	if ($scope.FNAMyself.BasicDetails.gender
			&& $scope.FNAMyself.BasicDetails.gender != ""
			&& $scope.FNAMyself.BasicDetails.genderId != "") {
		//changed for generali
		$scope.isPreviousGenderSenior = $scope.FNAMyself.BasicDetails.genderId;
		//changed for generali
		isPreviousGenderSeniorId = $scope.FNAMyself.BasicDetails.genderId;
		for ( var custGender in $scope.custGenderSelections) {
			$scope.custGenderSelections[custGender].genderSelected = "";
			if ($scope.custGenderSelections[custGender].gender == $scope.FNAMyself.BasicDetails.genderId) {
				$scope.custGenderSelections[custGender].genderSelected = "_selected";
			}
		}
	}

	var model = "FNAObject";
	$scope.genderEditChanged = false;
	$scope.lifeStageEditChanged = false;
	$scope.isPreviousGenderSenior;
	$scope.previousLifeStage;
	var previousLifeStageId;
	if ((rootConfig.autoSave.fna)) {
		AutoSave.setupWatchForScope(model, DataService, UtilityService, "",
				$scope, $debounce, $translate, $routeParams, FnaService);
	}

	// To change the selected tab style with highlighted background
	$scope.initalizeProgressbar = function() {
		UtilityService.disableProgressTab = false;
		for (var j = 0; j < $scope.pageSelections.length; j++) {
			if ($scope.pageSelections[j].pageType == "Customers Profile") {
				$scope.pageSelections[j].isSelected = "selected";
				$scope.pageSelections[j].isDisabled = "";
			} else {
				$scope.pageSelections[j].isSelected = "";

			}
		}
		$scope.FNAObject.FNA.pageSelections = $scope.pageSelections;
		FnaVariables.setFnaModel($scope.FNAObject);
	};
	//commented for generali
	//$scope.initalizeProgressbar();

	$scope.disableTab = function() {
		if (UtilityService.disableProgressTab) {
			for (var j = 0; j < $scope.pageSelections.length; j++) {
				if ($scope.pageSelections[j].pageType == "Product Recommendation"
						|| $scope.pageSelections[j].pageType == "Need Analysis") {
					$scope.pageSelections[j].isDisabled = "disabled";
				}
			}
		}
		$scope.FNAObject.FNA.pageSelections = $scope.pageSelections;
	};
	
	// Edit Flow - change class of already selected model data
	$scope.prePopulateLifeStage = function(isLast) {
		if (isLast) {
			var lifestageId = $scope.FNAObject.FNA.lifeStage.id;
			if ($scope.FNAObject.FNA.lifeStage
					&& $scope.FNAObject.FNA.lifeStage.id != ""
					&& $scope.FNAMyself.BasicDetails.genderId != "") {
				var cutomizeBtnClass = $scope.FNAMyself.BasicDetails.genderId
						+ '_' + $scope.FNAObject.FNA.lifeStage.id;
				$scope.cutomizeBtnClass = cutomizeBtnClass;
			}

		}
	}

	// Function called on click of Select Yourself image
	$scope.genderSelectionClick = function(genderId) {
		UtilityService.disableProgressTab = true;

		var gender = "Male";
		if (genderId.indexOf("female") >= 0) {
			gender = "Female";
		}
		var isSenior = "false";
		if (genderId.indexOf("senior") == 0) {
			isSenior = "true";
		}
		if ($scope.isPreviousGenderSenior && ($scope.isPreviousGenderSenior != isSenior)){
			$scope.genderEditChanged = true;
		}else{
			$scope.genderEditChanged = false;
		}
		// Set the values to fnaobject
		$scope.FNAMyself.BasicDetails.gender = gender;
		$scope.FNAMyself.BasicDetails.isSenior = isSenior;
		$scope.FNAMyself.BasicDetails.genderId = genderId;

		/** Set the insured to Parties in FNAModel * */
		$scope.FNAMyself.id = "myself";
		globalService.setParty($scope.FNAMyself, "FNAMyself");

		// To get the combined image eg: female_newlymarried or
		// senior_male_single
		var lifestageId = $scope.FNAObject.FNA.lifeStage.id;
		if (lifestageId && lifestageId != "") {
			var cutomizeBtnClass = genderId + '_' + lifestageId;
			$scope.cutomizeBtnClass = cutomizeBtnClass;
			$scope.refresh();
		}

	};

	// Function called on click of LifeStage image
	$scope.lifeStageClick = function(lifeStageId, lifeStageClass) {
		UtilityService.disableProgressTab = true;

		// Set the values to fnaobject
		$scope.FNAObject.FNA.lifeStage.description = lifeStageId;
		$scope.FNAObject.FNA.lifeStage.id = lifeStageClass;
		if ($scope.previousLifeStage && (lifeStageId != $scope.previousLifeStage)){
			$scope.lifeStageEditChanged = true;
		}else{
			$scope.lifeStageEditChanged = false;
		}
		// To get the combined image
		if ($scope.FNAMyself.BasicDetails.gender
				&& $scope.FNAMyself.BasicDetails.gender != ""
				&& $scope.FNAMyself.BasicDetails.genderId != "") {
			var genderSelect = $scope.FNAMyself.BasicDetails.genderId;

			var cutomizeBtnClass = genderSelect + '_' + lifeStageClass;
			$scope.cutomizeBtnClass = cutomizeBtnClass;
			$scope.refresh();
		}

	};

	$scope.removePreviousDetails = function() {
		$scope.FNAObject.FNA.selectedProduct = {};
		$scope.FNAObject.FNA.goals = [];
		$scope.popup_proceed_button = false;
		$scope.lifeStageEditChanged = false;
		$scope.genderEditChanged = false;
		$scope.nextPage();
	};
	$scope.nextPage = function() {

		var isNextPage = true;
		if ($scope.FNAMyself.BasicDetails.gender.length > 0
				&& $scope.FNAObject.FNA.lifeStage.description.length > 0) {
			for (var i = 0; i < FnaVariables.pages.length; i++) {
				if (FnaVariables.pages[i].url == "/MyFamilyDetails") {
					$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
					break;
				}
			}
			var beneficiaries = globalService.getFNABeneficiaries();
			for (var i = 0; i < beneficiaries.length; i++) {
				if (typeof beneficiaries[i] != "undefined"
						&& beneficiaries[i] != null) {
					if (($scope.FNAMyself.BasicDetails.gender == "Male" && beneficiaries[i].classNameAttached == "Husband")
							|| ($scope.FNAMyself.BasicDetails.gender == "Female" && beneficiaries[i].classNameAttached == "Wife")) {
						isNextPage = false;
						$scope.message = translateMessages($translate,
								"fna.fnaGenderSelectionValidationMessage");

					} else if ($scope.FNAObject.FNA.lifeStage.description == "Single"
							&& (beneficiaries[i].classNameAttached == "Husband" || beneficiaries[i].classNameAttached == "Wife")) {
						isNextPage = false;
						$scope.message = translateMessages($translate,
								"fna.fnaLifestageSelectionValidationMessage");
					}
				}
			}
			if($scope.FNAObject.FNA.goals && ($scope.FNAObject.FNA.goals.length > 0)){
				if($scope.lifeStageEditChanged || $scope.genderEditChanged){
					isNextPage = false;
					$scope.popup_proceed_button = true;
					$scope.message = translateMessages($translate, "fna.fnaLifestageModifyValidationMessage");
				}
			}
			if (isNextPage) {
				$scope.disableTab();

				/** Set the insured to Parties in FNAModel * */
				$scope.FNAMyself.id = "myself";
				globalService.setParty($scope.FNAMyself, "FNAMyself");
				var parties = globalService.getParties();
				$scope.FNAObject.FNA.parties = parties;

				FnaVariables.setFnaModel($scope.FNAObject);
				$scope.status = "Initial";
				var obj = new FnaService.saveTransactions($scope, $rootScope,
						DataService, $translate, UtilityService, false);
				obj.save(function() {
					$location.path('/MyFamilyDetails');
				});
			} else {
				$rootScope.lePopupCtrl.showWarning(translateMessages(
						$translate, "lifeEngage"), $scope.message,
						translateMessages($translate, "summaryConfirmButton"),
						$scope.removePreviousDetails, translateMessages(
								$translate, "general.productCancelOption",
								$scope.cancel, $scope.cancel));
			}

		} else {
			if ($scope.FNAMyself.BasicDetails.gender.length == 0
					&& $scope.FNAObject.FNA.lifeStage.description.length == 0) {
				$rootScope.lePopupCtrl.showError(translateMessages($translate,
						"lifeEngage"), translateMessages($translate,
						"fna.blankAboutMe"), translateMessages($translate,
						"general.productCancelOption"));
			} else if ($scope.FNAMyself.BasicDetails.gender.length == 0) {
				$rootScope.lePopupCtrl.showError(translateMessages($translate,
						"lifeEngage"), translateMessages($translate,
						"fna.blankYourself"), translateMessages($translate,
						"general.productCancelOption"));
			} else {
				$rootScope.lePopupCtrl.showError(translateMessages($translate,
						"lifeEngage"), translateMessages($translate,
						"fna.blankLifeStage"), translateMessages($translate,
						"general.productCancelOption"));
			}
		}

	};
}]);
