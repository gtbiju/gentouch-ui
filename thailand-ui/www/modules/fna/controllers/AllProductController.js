﻿/*
 *Copyright 2015, LifeEngage 
 */




'use strict';

storeApp.controller('AllProductController', ['$rootScope', '$scope', 'DataService', 'FnaService','LookupService', 'DocumentService', 'ProductService', 
                                             '$compile', '$routeParams', '$location', '$translate','IllustratorVariables', 'FnaVariables', 'UtilityService','$debounce', 'PersistenceMapping', 'AutoSave', 'globalService','UserDetailsService',
                                             function($rootScope, $scope, DataService, FnaService, LookupService,DocumentService, ProductService, $compile, $routeParams, $location, $translate, IllustratorVariables,
                                             FnaVariables, UtilityService,$debounce, PersistenceMapping, AutoSave, globalService,UserDetailsService) {

	FnaVariables.selectedPage = "Product Selection";
	$scope.selectedpage = "AllProducts";

	$scope. allproductsList;

	$scope.FNAObject = FnaVariables.getFnaModel();
	$rootScope.moduleHeader = "fna.fnaHeading";
	$rootScope.moduleVariable = translateMessages($translate,
			$rootScope.moduleHeader);
	$scope.selectedproduct = $scope.FNAObject.FNA.selectedProduct;
	$scope.productsListEven = [];
	$scope.productsListOdd = [];
	$scope.isRDSUser = UserDetailsService.getRDSUser();
		$scope.isWeb = true;
	if($scope.isRDSUser){
		$scope.isWeb = false;
	}
	var transactionObj;
	//ProductService.getAllActiveProducts(transactionObj, getAllProductsSuccess,
			//getAllProductsFailure);

	$scope.getAllProductsSuccess = function(data)  {
		$scope.allproductsList = data.Products;
		$rootScope.showHideLoadingImage(false);
		for (var x = 0; x < $scope.allproductsList.length; x += 2) {
			$scope.productsListEven.push($scope.allproductsList[x]);
		}
		for (var x = 1; x < $scope.allproductsList.length; x += 2) {
			$scope.productsListOdd.push($scope.allproductsList[x]);
		}
		$scope.refresh();
	}

	$scope.getAllProductsFailure = function()  {
	}

	$scope.getIllustration = function() {

		if ($scope.FNAObject.FNA.selectedProduct
				&& $scope.FNAObject.FNA.selectedProduct.id) {

			FnaVariables.setFnaModel($scope.FNAObject);

			var product = globalService.product;
			var productId = $scope.FNAObject.FNA.selectedProduct.id;
			product.ProductDetails.productCode = productId
			$scope.FNAObject.FNA.selectedProduct.productId = productId;
			globalService.setProduct(product);

			// To reset isInsured and isBenificiary for all parties to default
			// false value
			// Since for each illustrate button clisk -- new illustration is
			// created for FNA
			var parties = globalService.getFNAParties();
			$scope.FNAObject.FNA.parties = parties;
			FnaVariables.setFnaModel($scope.FNAObject);

			var obj = new FnaService.saveTransactions($scope, $rootScope,
					DataService, $translate, UtilityService, false);
			obj.save(function() {
				UtilityService.previousPage = 'FNA';
				$location.path('/fnaChooseParties/0/'+productId);
			});
		} else {
			 $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),$scope.message,translateMessages($translate, "chooseProductValidationMessage"),$scope.removePreviousDetails,translateMessages($translate, "fna.ok"));
		}

	}
	$scope.getSelectedProduct = function(product, event) {
		var thisObj = event.currentTarget;
		if (thisObj.checked) {
			$scope.FNAObject.FNA.selectedProduct = product;
		} else {
			$scope.FNAObject.FNA.selectedProduct = {};
		}
		$scope.selectedproduct = $scope.FNAObject.FNA.selectedProduct;
		FnaVariables.setFnaModel($scope.FNAObject);
		$scope.refresh();
	}
$scope.initialLoad= function(){
		ProductService.getAllActiveProducts(transactionObj, $scope.getAllProductsSuccess,
				$scope.getAllProductsFailure);
	}
	$scope.$on('$viewContentLoaded', function(){
		$scope.initialLoad();
	  });
}]);