/*
 *Copyright 2015, LifeEngage 
 */


storeApp.controller('FnaLandingController', ['$rootScope', '$scope', 'DataService', 'FnaService','LookupService', 'DocumentService', 'ProductService','$compile', 
'$routeParams', '$location', '$translate','IllustratorVariables', 'FnaVariables', 'UtilityService','$debounce', 'PersistenceMapping', 'AutoSave',
 'globalService','$timeout','UtilityVariables',
function($rootScope, $scope, DataService, FnaService, LookupService,DocumentService, ProductService, $compile, $routeParams, $location, $translate, 
IllustratorVariables,FnaVariables, UtilityService,$debounce, PersistenceMapping, AutoSave, globalService,$timeout,UtilityVariables) {

	$scope.selectedpage = "FNA Landing";

	$rootScope.moduleHeader = "fna.fnaHeading";
	$rootScope.moduleVariable = translateMessages($translate,
			$rootScope.moduleHeader);

	UtilityVariables.context = "FNA";

	$scope.val = "";

	// Get FNA details Succeess
	$scope.mapKeysforPersistence = function() {
		if (!(rootConfig.isDeviceMobile) && (FnaVariables.fnaId == null || FnaVariables.fnaId == 0)) {
			PersistenceMapping.creationDate = UtilityService
								.getFormattedDateTime();
			PersistenceMapping.Key13 = UtilityService
								.getFormattedDateTime();
		}
		PersistenceMapping.Key2 = (FnaVariables.fnaId != null && FnaVariables.fnaId != 0) ? FnaVariables.fnaId
								: "";
		PersistenceMapping.Key10 = "FullDetails";
		PersistenceMapping.Type = "FNA";
	};

	$scope.onGetListingsSuccess = function(data) {

		if (data[0].TransactionData.parties) {
			for (party in data[0].TransactionData.parties) {
				if (data[0].TransactionData.parties[party].BasicDetails
						&& data[0].TransactionData.parties[party].BasicDetails.photo
						&& data[0].TransactionData.parties[party].BasicDetails.photo != "") {
					data[0].TransactionData.parties[party].BasicDetails.photo = (data[0].TransactionData.parties[party].BasicDetails.photo)
							.replace(/ /g, '+');
				}
			}
			var parties = data[0].TransactionData.parties;
			globalService.setParties(parties);

		}

		if (data[0].Key16 != null && data[0].Key16 == "SUCCESS") {
			data[0].Key16 = "Synced";
		}
		FnaVariables.fnaKeys.Key12 = data[0].Key12;
		FnaVariables.transTrackingID = data[0].TransTrackingID;
		FnaVariables.fnaKeys.creationDate = data[0].creationDate;
		FnaVariables.fnaKeys.Key13 = data[0].Key13;
		FnaVariables.fnaKeys.modifiedDate = data[0].modifiedDate;
        FnaVariables.fnaKeys.key14 = data[0].modifiedDate;

		FnaVariables.setFnaModel({
			'FNA' : JSON.parse(JSON.stringify(data[0].TransactionData))
		});

	};
	// Get eApp details Error
	$scope.onGetListingsError = function() {
		alert("error");
	};
	if ($routeParams.fnaId == "0" && $routeParams.transactionId == "0") {

		FnaVariables.setFnaModel("");
	} else {
		PersistenceMapping.clearTransactionKeys();
		$scope.mapKeysforPersistence();
		var transactionObj = PersistenceMapping.mapScopeToPersistence({});

		DataService.getListingDetail(transactionObj,
				$scope.onGetListingsSuccess, $scope.onGetListingsError);
	}
	$scope.aboutMe = function() {
		for (var i = 0; i < FnaVariables.pages.length; i++) {
			if (FnaVariables.pages[i].url == "/aboutCompany/false/") {
				FnaVariables.getFnaModel().FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
				break;
			}
		}
		$location.path('/aboutCompany/false/');
	};
}]);
