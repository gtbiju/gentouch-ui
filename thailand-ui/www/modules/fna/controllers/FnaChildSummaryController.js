/*
 *Copyright 2015, LifeEngage 
 */

storeApp.controller('FnaChildSummaryController', ['$rootScope', '$scope', 'DataService', 'FnaService','LookupService', 'DocumentService', 'ProductService','$compile', 
'$routeParams', '$location', '$translate','IllustratorVariables', 'FnaVariables', 'UtilityService','$debounce', 'PersistenceMapping', 'AutoSave',
 'globalService','$timeout',
function($rootScope, $scope, DataService, FnaService, LookupService,DocumentService, ProductService, $compile, $routeParams, $location, $translate, 
IllustratorVariables,FnaVariables, UtilityService,$debounce, PersistenceMapping, AutoSave, globalService,$timeout) {


	$scope.childSelGoals = $scope.$parent.selectedGoals;
	$scope.imageTemp = $scope.$parent.insuredImage;
	$scope.anotherOptions = $scope.$parent.anotherOptions1;
	//$scope.chartDataChild = $scope.$parent.chartData;
	
	
	//Added for Generali begins
	$scope.FNAObject = FnaVariables.getFnaModel();
	//Added for Generali ends
	
	
	$scope.showDetail = function(index) {
		$scope.$parent.showDetail(index);
	}

}]);
