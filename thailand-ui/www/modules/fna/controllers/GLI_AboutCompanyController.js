/*
 *GLI_AboutCompanyController - For implementing Generali Specific changes
 */

storeApp.controller(
		'GLI_AboutCompanyController', [
			'$rootScope',
			'UtilityService',
			'$scope',
			'$location',
			'DataService',
			'FnaVariables',
			'GLI_FnaService',
			'$translate',
			'$routeParams',
			'globalService',
			'PersistenceMapping',
			'$controller', 
			'MediaService',
			function GLI_AboutCompanyController($rootScope, UtilityService, $scope,
												$location, DataService, FnaVariables,
												GLI_FnaService, $translate, $routeParams,
												globalService, PersistenceMapping, $controller,MediaService) {
			$controller('AboutCompanyController', {
				$rootScope : $rootScope,
				UtilityService : UtilityService,
				$scope : $scope,
				$location : $location,
				DataService : DataService,
				FnaVariables : FnaVariables,
				FnaService : GLI_FnaService,
				$translate : $translate,
				$routeParams : $routeParams,
				globalService : globalService,
				PersistenceMapping:PersistenceMapping
			});
							
			UtilityService.disableProgressTab = false;
			$scope.status = FnaVariables.getStatusOptionsFNA()[0].status;
			$rootScope.selectedPage = "AboutCompany";
			$scope.mapKeysforPersistence = function() {
				if (!(rootConfig.isDeviceMobile) && (FnaVariables.fnaId == null || FnaVariables.fnaId == 0)) {
					// PersistenceMapping.Key13 = formattedDate;
					PersistenceMapping.Key13 = UtilityService.getFormattedDate();
				}
				PersistenceMapping.Key2 = (FnaVariables.fnaId != null && FnaVariables.fnaId != 0) ? FnaVariables.fnaId : "";
				PersistenceMapping.Key10 = "FullDetails";
				PersistenceMapping.Type = "FNA";
			};
							
			$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;
			$scope.currentPageType  = "Introduction";
			//enabling and disabling the tabs
			GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
							
			$scope.initialLoad = function(){
				if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6']) {
					localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6');
                }
				if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6']) {
					localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6');
                }
				if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6']) {
					localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6');
                }
				if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6']) {
					localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6');
                }
				if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']) {
					localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
                }    
				if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4']) {
					localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4');
                }
				if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']) {
					localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
                }
				if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']) {
					localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
                }
				if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5']) {
					localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5');
                }
				if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5']) {
					localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5');
                }
				if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5']) {
					localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5');
                }
				if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5']) {
					localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5');
                }

				LEDynamicUI.getUIJson(rootConfig.FnaConfigJson, false, function(FnaConfigJson) {
					FnaVariables.goals = FnaConfigJson.goals;
					FnaVariables.lifeStageGoals = FnaConfigJson.lifeStageGoals;
					FnaVariables.familyMembers = FnaConfigJson.familyMembers;
					FnaVariables.riskList = FnaConfigJson.riskList;
					FnaVariables.aboutCompany = FnaConfigJson.aboutCompany;
					FnaVariables.custLifeStagesList = FnaConfigJson.custLifeStages;
					FnaVariables.custGenderSelectionsList = FnaConfigJson.custGenderSelections;			
					FnaVariables.pages = FnaConfigJson.pages;	
					FnaVariables.summaryGraph = FnaConfigJson.summaryGraph;
					FnaVariables.pageTypes = FnaConfigJson.pageTypes;	
					$scope.performanceDetails = FnaVariables.aboutCompany.performanceDetails;
					$scope.revenueDetails = FnaVariables.aboutCompany.revenueDetails;
					$scope.refresh();
				});
			}
							
			$scope.playVideo = function() {
				MediaService.playVideo($scope, 'lifestage.mp4');
			};
			// Get eApp details Error
			$scope.onGetListingsError = function() {
				alert("error");
			};
			
			$scope.$on("$destroy", function() {
               if (this.$$destroyed) return;     
               while (this.$$childHead) {      
            	   this.$$childHead.$destroy();    
               }     
               if (this.$broadcast) { 
            	   this.$broadcast('$destroy');   
               }    
               this.$$destroyed = true;                 
               //$timeout.cancel( timer );                           
            });
							
		} 
	])