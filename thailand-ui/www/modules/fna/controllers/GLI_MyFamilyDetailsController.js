/*
 *Copyright 2015, LifeEngage 
 */



/*Name:GLI_MyFamilyDetailsController
 * For implementing GLI specific changes
 */
storeApp.controller('GLI_MyFamilyDetailsController',['$timeout', '$route', '$rootScope', '$scope', '$window', '$location','$compile', '$routeParams', 'DataService', 'LookupService', 'DocumentService','FnaVariables', 'EappVariables','GLI_FnaService', 'UtilityService', 'PersistenceMapping','$translate', 'AutoSave', '$debounce', 'globalService', 'GLI_globalService', '$controller','DataLookupService','MediaService','IllustratorVariables','GLI_DataLookupService',
function GLI_MyFamilyDetailsController($timeout, $route, $rootScope, $scope, $window, $location,$compile, $routeParams, DataService, LookupService, DocumentService,FnaVariables,EappVariables, GLI_FnaService, UtilityService, PersistenceMapping,$translate, AutoSave, $debounce, globalService, GLI_globalService, $controller, DataLookupService, MediaService,IllustratorVariables,GLI_DataLookupService){
	$controller('MyFamilyDetailsController',{$timeout:$timeout, $route:$route, $rootScope:$rootScope, $scope:$scope, $window:$window, $location:$location,$compile:$compile, $routeParams:$routeParams, DataService:DataService, LookupService:LookupService, DocumentService:DocumentService,FnaVariables:FnaVariables,EappVariables:EappVariables, FnaService:GLI_FnaService, UtilityService:UtilityService, PersistenceMapping:PersistenceMapping,$translate:$translate, AutoSave:AutoSave, $debounce:$debounce, globalService:GLI_globalService, DataLookupService:GLI_DataLookupService,IllustratorVariables:IllustratorVariables});
    
	$scope.global_val_array = [];
	$rootScope.selectedPage = "MyFamilyDetails";
	$scope.alphanumericPattern = rootConfig.alphanumericPattern;
	$scope.alphanumericPatterng = rootConfig.alphanumericPatterng;//Defect 4118
	$scope.fnaImageUploadSize = rootConfig.FNAUploadImageSize;
	$scope.nationalityPattern = rootConfig.nationalityPattern;
	$scope.leadNewNamePattern = rootConfig.leadNewNamePattern;
    $scope.alphabetWithSpacePatternIllustration = rootConfig.alphabetWithSpacePatternIllustration;
	$scope.showDeleteButton = false;
    $scope.isInsuredClicked = true;
	$scope.myFamilyDetailsNoError=false; //Next button active/Disable
	$scope.SaveTrans=false;
	$scope.SaveTransBen=false;
    $scope.showSecondary=false;
    $scope.allowCodeUpdate=false;
	$scope.allowCodeUpdateBen=false;
    $scope.isBenefeciaryClicked = false;
    $scope.passportEappPattern=rootConfig.passportEappPattern;
    
    // Changes done by LETeam << Change starts here
    var currentTab;
    var j = 0;
    for (j; j < $scope.FNABeneficiaries.length ;j ++) {
    /* FNA Changes by LE Team Start (Bug: 3777) */
    /*
    if ($scope.FNAObject.FNA.lifeStage.description == "Single" && ($scope.FNABeneficiaries[j].classNameAttached == "Wife" || $scope.FNABeneficiaries[j].classNameAttached == "Husband")) {
		$scope.FNABeneficiaries.splice(i,1);
		}
    */
    if ($scope.FNAObject.FNA.lifeStage.description == "Single" && ($scope.FNABeneficiaries[j].classNameAttached == "Wife" || $scope.FNABeneficiaries[j].classNameAttached == "Husband" || $scope.FNABeneficiaries[j].classNameAttached == "Spouse")) {
		$scope.FNABeneficiaries.splice(j,1);
		}
    }
    /* FNA Changes by LE Team End (Bug: 3777) */
    var i = 0;
    var party = new globalService.getParties();
    for (i;  i < party.length ;i++) {
	if (i == 0 && party[0] != undefined && party[0].BasicDetails.dob != null) {
        if (party[0].OccupationDetails[0] == undefined || party[0].OccupationDetails[0] == "" ||  party[0].OccupationDetails[0].length < 1) {
            $scope.FNAMyself.OccupationDetails[0]="";
            $scope.isInitialLoad0 = false; 
            $scope.isInitialLoad1 = false;
        }        
        else {
	    $scope.FNAMyself.OccupationDetails = party[0].OccupationDetails;  
        $scope.isInitialLoad0 = true; 
        $scope.isInitialLoad1 = true; 
        }
        
	    $scope.FNAMyself.ContactDetails = party[0].ContactDetails;
        $scope.FNAMyself.BasicDetails.title = party[0].BasicDetails.title;
		$scope.FNAMyself.BasicDetails.nickName = party[0].BasicDetails.nickName;
        $scope.FNAMyself.BasicDetails.IDcard = party[0].BasicDetails.IDcard;
        $scope.FNAMyself.BasicDetails.gender = party[0].BasicDetails.gender;
        $scope.FNAMyself.BasicDetails.age = party[0].BasicDetails.age;
        $scope.FNAMyself.BasicDetails.dob = party[0].BasicDetails.dob;
        $scope.FNAMyself.BasicDetails.identityProof = party[0].BasicDetails.identityProof;   
        
        $scope.FNAMyself.BasicDetails.identityProof = party[0].BasicDetails.identityProof;
        if($scope.FNAMyself.BasicDetails.identityProof != undefined && $scope.FNAMyself.BasicDetails.identityProof=="CID") {
            $scope.FNAMyself.BasicDetails.IDcard = party[0].BasicDetails.IDcard;
        } else if ($scope.FNAMyself.BasicDetails.identityProof != undefined && $scope.FNAMyself.BasicDetails.identityProof=="PP") {
               $scope.passportNum = party[0].BasicDetails.IDcard;
                   } else if($scope.FNAMyself.BasicDetails.identityProof != undefined && $scope.FNAMyself.BasicDetails.identityProof=="DL") {
            $scope.driverLicenseNum = party[0].BasicDetails.IDcard;
        }else if($scope.FNAMyself.BasicDetails.identityProof != undefined && $scope.FNAMyself.BasicDetails.identityProof=="GID"){
                $scope.governmentIdNum = party[0].BasicDetails.IDcard;
           }        
	    } else {
           /*if (party[i].OccupationDetails == undefined || party[i].OccupationDetails[0] == undefined || party[i].OccupationDetails[0] == "" ||  party[i].OccupationDetails[0].length < 1) {
            $scope.Beneficiary.OccupationDetails= [];
            $scope.isInitialLoadBen0 = false; 
        $scope.isInitialLoadBen1 = false;    
        } else {
        if (party[i].OccupationDetails[0].industryValue != undefined &&  party[i].OccupationDetails[0].industryValue != "" ) {
            $scope.isInitialLoadBen0 = true;  
            } 
        if (party[i].OccupationDetails[1].industryValue != undefined &&  party[i].OccupationDetails[1].industryValue != "" ) {  
            $scope.isInitialLoadBen1 = true; 
                       }    
	    $scope.Beneficiary.OccupationDetails = party[i].OccupationDetails; 
         
        }*/
     
                $scope.Beneficiary.ContactDetails = party[i].ContactDetails;
                $scope.Beneficiary.BasicDetails = party[i].BasicDetails;  
        
        /* FNA changes made by LE Team for Bug 4307 */
         if ($scope.Beneficiary.BasicDetails != undefined && $scope.Beneficiary.BasicDetails.identityProof != undefined && $scope.Beneficiary.BasicDetails.identityProof=="PP") {
               $scope.passportNumBen = $scope.Beneficiary.BasicDetails.IDcard;
                   } else if($scope.Beneficiary.BasicDetails != undefined && $scope.Beneficiary.BasicDetails.identityProof != undefined && $scope.Beneficiary.BasicDetails.identityProof=="DL") {
            $scope.driverLicenseNumBen = $scope.Beneficiary.BasicDetails.IDcard;
        }else if($scope.Beneficiary.BasicDetails != undefined && $scope.Beneficiary.BasicDetails.identityProof != undefined && $scope.Beneficiary.BasicDetails.identityProof=="GID"){
                $scope.governmentIdNumBen = $scope.Beneficiary.BasicDetails.IDcard;
           }
       }
       /* FNA changes made by LE Team for Bug 4307 */    
    }
      // Changes done by LETeam << Change ends here
	var myFamilyDetailsUiJsonFile = FnaVariables.uiJsonFile.fnaAboutMeFile;
	$scope.memberClickEvent = {};
	if($scope.FNAObject.FNA.saveCheck == '' || typeof $scope.FNAObject.FNA.saveCheck == "undefined"){
		$scope.FNAObject.FNA.saveCheck = false;
	}
    if($scope.FNAObject.FNA.saveCheckDependents == '' || typeof $scope.FNAObject.FNA.saveCheckDependents == "undefined"){
		$scope.FNAObject.FNA.saveCheckDependents = false;
	}
    
	$scope.status = FnaVariables.getStatusOptionsFNA()[0].status;
	$scope.contactNo = "";
	$scope.myselfMandatoryFieldsAdded = true;
	$scope.choosePartFieldsAdded = true;
	$scope.currentPageType  = "Customers Profile";
	UtilityService.disableProgressTab = false;
      /*Changes done by LETeam Start - Bug 4178*/
    $scope.defaultBlankForDropDown=true;
    /*Changes done by LETeam End - Bug 4178*/
	var _errors = [];
	//enabling and disabling the tabs
	GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
	
	$scope.okPressed = function(){
	}
	$scope.modelChange = function(model) {
		$scope.FNAMyself.BasicDetails.countryofResidence = model;
	}
	//Function called to set the member details on dropping member,overwritten to enable tab click changes
	$scope.setMember = function(order, currentlyDragged) {
		UtilityService.disableProgressTab = true;
		//GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);

		/** Validate the occurance of a family member**/
		for (var i = 0; i < $scope.familyMembers.length; i++) {
			if ($scope.familyMembers[i].memberName == currentlyDragged) {
				$scope.familyMembers[i].availability = $scope.familyMembers[i].availability - 1;
				if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
					$scope.refresh();
				}

			}
		}
		GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
	};

	$scope.getLookUpDataFromCode =function(data, value) {
								return data.filter(function(data) {
									return angular.lowercase(data.code) == angular.lowercase(value)
								});
							} 
	$scope.setCategoryCode = function(model,occupationArray) {
								if(model){
									var _lastIndex=model.lastIndexOf('.');
									if(_lastIndex>0){
									   var parentModel=model.substring(0,_lastIndex);
									   var value=eval('$scope.' + model);
									   var scopeVar=eval('$scope.' + parentModel);	
									   if(occupationArray){
									   		var value = $scope.getLookUpDataFromCode(occupationArray,value);			   	
									    	if(value && value != "" && value.length > 0){
									   			scopeVar['occupationCategoryValue']=value[0].value;
									   			scopeVar['occupationCategory']=value[0].code;
									   			//scopeVar['natureofWork']=	"";
									    	}	
									   }			
									   			   
									}
								}
	}
	$scope.getValueCode = function(code_model,array_list){
			if(code_model){
				var _lastIndex=code_model.lastIndexOf('.');
				if(_lastIndex>0){
				   var parentModel=code_model.substring(0,_lastIndex);
				   var childModel = code_model.substring(_lastIndex+1,code_model.length);
				   var newChild =childModel+"Value";
				   var scopeVar=eval('$scope.' + parentModel);	
				   var inputCode = eval('$scope.' + code_model);	
				   var outputValue = $scope.getLookUpDataFromCode(array_list,inputCode);			   	
				   if(outputValue && outputValue != "" && outputValue.length > 0){
			   			scopeVar[newChild]=outputValue[0].value;
				   }
				   $scope.refresh();
				}
			}
		}
	$scope.fieldChange = function() {
		$scope.FNAObject.FNA.saveCheck = false;
        $scope.FNAObject.FNA.saveCheckDependents = false;
		UtilityService.disableProgressTab = true;
		GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
	};
     
         $scope.genderChange = function() {
        //$scope.FNABeneficiaries[];
		var chkFlag = "member";
           var sex;
           var z= 0;
		for(z;z<$scope.FNABeneficiaries.length;z++)
			{
		if ($scope.FNAMyself.BasicDetails.gender == "Male") {
			var arrs = $scope.FNABeneficiaries[z];
				$scope.FNABeneficiaries.splice(z,1);
			arrs.imageClass == "nonmember wife";
			$scope.FNABeneficiaries[z] = arrs;
				if ($scope.FNABeneficiaries[z].classNameAttached == "Spouse") {
					$scope.FNABeneficiaries[z].imageClass == "nonmember wife";
				}
		sex = "wife";
			}
                if ($scope.FNAMyself.BasicDetails.gender == "Female") {
			var arrs = $scope.FNABeneficiaries[z];
				$scope.FNABeneficiaries.splice(z,1);
			arrs.imageClass == "nonmember husband";
			$scope.FNABeneficiaries.push(arrs);
				if ($scope.FNABeneficiaries[z].classNameAttached == "Spouse") {
					$scope.FNABeneficiaries[z].imageClass == "nonmember husband";
				}
		sex = "husband";
			}
		}
			//$scope.updateDroppedContentGender(chkFlag,sex);
        
	};
	$scope.invalidNationality =function(errorKey){
		var error = {};
			$scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
		error.message = translateMessages(
				$translate,
				"lms.leadDetailsSectionnationalityRequiredValidationMessage");
		error.key = errorKey;
		_errors.push(error);
		
	}
	$scope.invalidResidentCountry =function(errorKey){
		var error = {};
			$scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
		error.message = translateMessages(
				$translate,
				"lms.leadDetailsSectionCountryofbirthRequiredValidationMessage");
		error.key = errorKey;
		_errors.push(error);
			
	}
// Changes done by LETeam << Change starts here
    $scope.formatLabel = function (model, options, type) {
        if (type == 'title' && model == 'MR') {
			return 'Mr.';
		} else if (type == 'title' && model == 'MRS') {
                   return 'Mrs.';
                   } else if (type == 'title' && model == 'MISS') {
                   return 'Miss';
                   } else if (type == 'title' && model == 'DR') {
                   return 'Dr.';
                   } else if (type == 'title' && model == 'MS') {
                   return 'Ms.';
                   }
        if (options && options.length >= 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code) {
                    return options[i].value;
                }
            }
        }
    };
    
    $scope.formatLabel2 = function (model, options, type) {
                    if (options && options.length >= 1) {
                        for (var i = 0; i < options.length; i++) {
                            if (model === options[i].code && ((type !== 'district' && !$scope.editMode) || (type !== 'district' && $scope.editMode && angular.equals(undefined, $scope.FNAMyself.ContactDetails.currentAddress.wardOrHamlet)) || (type !== 'district' && $scope.allowFormatLabel))) {
                                if (type == 'wardOrHamlet') {
                                    var splitWard=[];
                                    var optionValue=options[i].value;
                                    if(optionValue.indexOf(',')>0){
										$rootScope.splitInd=true;
       		   			                 optionValue=optionValue.substring(0,options[i].value.length-1);
       		   			                 splitWard=optionValue.split(',');
                                         $scope.FNAMyself.ContactDetails.currentAddress.wardOrHamletValue = splitWard[0];
                                    }else{
                                    $rootScope.splitInd=false;
                                    $scope.FNAMyself.ContactDetails.currentAddress.wardOrHamletValue = options[i].value;
                                    }
                                    $rootScope.updateAddress=true;                                    
                                }
                                return options[i].value;
                            }
                            if (type == 'district' && ($scope.allowFormatLabel || $rootScope.updateAddress)){
                                $scope.FNAMyself.ContactDetails.currentAddress.cityValue = options[0].value;
                                $scope.FNAMyself.ContactDetails.currentAddress.stateValue = options[1].value;
                                $scope.FNAMyself.ContactDetails.currentAddress.zipcodeValue = options[2].value;
                                $scope.FNAMyself.ContactDetails.currentAddress.city = options[0].code;
                                $scope.FNAMyself.ContactDetails.currentAddress.state = options[1].code;
                                $scope.FNAMyself.ContactDetails.currentAddress.zipcode = options[2].value;
                                $rootScope.updateAddress=false;
                                return options[0].value;
                            }

                            if(type == 'wardOrHamlet' && !$scope.allowFormatLabel && ($rootScope.updateAddress|| $scope.FNAMyself.ContactDetails.currentAddress.city)){
								if($rootScope.splitInd && !$scope.editMode){
                                    $rootScope.splitInd=false;
									return $scope.FNAMyself.ContactDetails.currentAddress.wardOrHamletValue+","+$scope.FNAMyself.ContactDetails.currentAddress.cityValue;
								}else{
									return $scope.FNAMyself.ContactDetails.currentAddress.wardOrHamletValue;
								}
							}else if(type == 'district' && ((!$scope.editMode && !$scope.allowFormatLabel)||($scope.editMode && !$scope.allowFormatLabel)||(!$scope.editMode && $scope.FNAMyself.ContactDetails.currentAddress.ward))){
								return model;
							}
                        }
                    }
                };
    $scope.validateCitizenId=function(){
		var idCardSubstr=$scope.FNAMyself.BasicDetails.IDcard.substring(0,12);
		var count=0;
		var len=$scope.FNAMyself.BasicDetails.IDcard.length+1;
		var rem=0;
		var quotient=0;
		var min=0;
		var nDigit=0;
                    
        for(var i=0;i<idCardSubstr.length;i++)
         {
			len=len-1;                            
			count=count+parseInt($scope.FNAMyself.BasicDetails.IDcard.substring(i,i+1),10)*len;
         }
                    
         quotient = Math.floor(count/11);
         rem=count%11;
         min=11-rem;
         nDigit=parseInt($scope.FNAMyself.BasicDetails.IDcard.substring(12,13),10);
                
         if(min>9){
             min=min%10;
         }
         if(nDigit!==min)
         {
            return true;
		 }
    };
    $scope.formatLabel2Ben = function (model, options, type) {
                    if (options && options.length >= 1) {
                        for (var i = 0; i < options.length; i++) {
                            if (model === options[i].code && ((type !== 'district' && !$scope.editMode) || (type !== 'district' && $scope.editMode && angular.equals(undefined, $scope.Beneficiary.CurrentAddress.wardOrHamlet)) || (type !== 'district' && $scope.allowFormatLabel))) {
                                if (type == 'wardOrHamlet') {
                                    var splitWard=[];
                                    var optionValue=options[i].value;
                                    if(optionValue.indexOf(',')>0){
										$rootScope.splitInd=true;
       		   			                 optionValue=optionValue.substring(0,options[i].value.length-1);
       		   			                 splitWard=optionValue.split(',');
                                         $scope.Beneficiary.CurrentAddress.wardOrHamletValue = splitWard[0];
                                    }else{
                                    $rootScope.splitInd=false;
                                    $scope.Beneficiary.CurrentAddress.wardOrHamletValue = options[i].value;
                                    }
                                    $rootScope.updateAddress=true;                                    
                                }
                                return options[i].value;
                            }
                            if (type == 'district' && ($scope.allowFormatLabel || $rootScope.updateAddress)){
                                $scope.Beneficiary.CurrentAddress.cityValue = options[0].value;
                                $scope.Beneficiary.CurrentAddress.stateValue = options[1].value;
                                $scope.Beneficiary.CurrentAddress.zipcodeValue = options[2].value;
                                $scope.Beneficiary.CurrentAddress.city = options[0].code;
                                $scope.Beneficiary.CurrentAddress.state = options[1].code;
                                $scope.Beneficiary.CurrentAddress.zipcode = options[2].value;
                                $rootScope.updateAddress=false;
                                return options[0].value;
                            }

                            if(type == 'wardOrHamlet' && !$scope.allowFormatLabel && ($rootScope.updateAddress|| $scope.Beneficiary.CurrentAddress.city)){
								if($rootScope.splitInd && !$scope.editMode){
                                    $rootScope.splitInd=false;
									return $scope.Beneficiary.CurrentAddress.wardOrHamletValue+","+$scope.Beneficiary.CurrentAddress.cityValue;
								}else{
									return $scope.Beneficiary.CurrentAddress.wardOrHamletValue;
								}
							}else if(type == 'district' && ((!$scope.editMode && !$scope.allowFormatLabel)||($scope.editMode && !$scope.allowFormatLabel)||(!$scope.editMode && $scope.Beneficiary.CurrentAddress.ward))){
								return model;
							}
                        }
                    }
                };
// Changes done by LETeam << Change ends here
	$scope.invalidCity =function(errorKey){
		var error = {};
			$scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
		error.message = translateMessages(
				$translate,
				"eapp.validCityErrorMessage");
		error.key = errorKey;
		_errors.push(error);
			
	}
   // Changes done by LETeam << Change starts here
    if($scope.FNAMyself.BasicDetails.identityProof=="PP" && ($scope.FNAMyself.BasicDetails.IDcard == "" || $scope.FNAMyself.BasicDetails.IDcard == undefined || $scope.FNAMyself.BasicDetails.IDcard == null)){
			$scope.FNAMyself.BasicDetails.IDcard = $scope.passportNum;
		}
		else if($scope.FNAMyself.BasicDetails.identityProof=="DL" && ($scope.FNAMyself.BasicDetails.IDcard == "" || $scope.FNAMyself.BasicDetails.IDcard == undefined || $scope.FNAMyself.BasicDetails.IDcard == null)){
			$scope.FNAMyself.BasicDetails.IDcard = $scope.driverLicenseNum;
		}
		else if($scope.FNAMyself.BasicDetails.identityProof=="GID" && ($scope.FNAMyself.BasicDetails.IDcard == "" || $scope.FNAMyself.BasicDetails.IDcard == undefined || $scope.FNAMyself.BasicDetails.IDcard == null)){
			$scope.FNAMyself.BasicDetails.IDcard = $scope.governmentIdNum;
		}
		
		if($scope.FNAMyself.BasicDetails.IDcard && $scope.FNAMyself.BasicDetails.identityProof=='CID' && $scope.validateCitizenId()){
           /*
            var error = {};
            error.message = translateMessages($translate, "lms.invalidCitizenId");
            error.key = 'IDcard';
            $scope.errorMessages.push(error);*/
		} 
   /* if($scope.Beneficiary.BasicDetails.identityProof != undefined && $scope.Beneficiary.BasicDetails.identityProof=="PP" && ($scope.Beneficiary.BasicDetails.IDcard == "" || $scope.Beneficiary.BasicDetails.IDcard == undefined || $scope.Beneficiary.BasicDetails.IDcard == null)){
			$scope.Beneficiary.BasicDetails.IDcard = $scope.passportNumBen;
		}
		else if($scope.Beneficiary.BasicDetails.identityProof != undefined && $scope.Beneficiary.BasicDetails.identityProof=="DL" && ($scope.Beneficiary.BasicDetails.IDcard == "" || $scope.Beneficiary.BasicDetails.IDcard == undefined || $scope.Beneficiary.BasicDetails.IDcard == null)){
			$scope.Beneficiary.BasicDetails.IDcard = $scope.driverLicenseNumBen;
		}
		else if($scope.Beneficiary.BasicDetails.identityProof != undefined && $scope.Beneficiary.BasicDetails.identityProof=="GID" && ($scope.Beneficiary.BasicDetails.IDcard == "" || $scope.Beneficiary.BasicDetails.IDcard == undefined || $scope.Beneficiary.BasicDetails.IDcard == null)){
			$scope.Beneficiary.BasicDetails.IDcard = $scope.governmentIdNumBen;
		}
		
		if($scope.Beneficiary.BasicDetails.IDcard != undefined && $scope.Beneficiary.BasicDetails.identityProof=='CID' && $scope.validateCitizenIdBen()){
            _errorCount++;
            var error = {};
            error.message = translateMessages($translate, "lms.invalidCitizenId");
            error.key = 'IDcard';
            _errors.push(error);
		} */
    
    /* $scope.validateCitizenIdBen=function(){
		var idCardSubstr=$scope.Beneficiary.BasicDetails.IDcard.substring(0,12);
		var count=0;
		var len=$scope.Beneficiary.BasicDetails.IDcard.length+1;
		var rem=0;
		var quotient=0;
		var min=0;
		var nDigit=0;
                    
        for(var i=0;i<idCardSubstr.length;i++)
         {
			len=len-1;                            
			count=count+parseInt($scope.Beneficiary.BasicDetails.IDcard.substring(i,i+1),10)*len;
         }
                    
         quotient = Math.floor(count/11);
         rem=count%11;
         min=11-rem;
         nDigit=parseInt($scope.Beneficiary.BasicDetails.IDcard.substring(12,13),10);
                
         if(min>9){
             min=min%10;
         }
         if(nDigit!==min)
         {
            return true;
		 }
    }; */
// Changes done by LETeam << Change ends here
	$scope.updateDynmaicErrMessage = function(erroKey){
		for(var i =0; i <$scope.dynamicErrorMessages.length ; i++ ){
			if($scope.dynamicErrorMessages[i].key == erroKey){
				$scope.dynamicErrorMessages.splice(i,1);
				$scope.dynamicErrorCount = $scope.dynamicErrorCount -1;
			}
		}
		$scope.updateErrorCount('aboutMeInsured');
	}
	// validating typeahead field
	$scope.validateField = function(model,list,fieldTobeValidated,errorKey){
		if(model != "" && model != null && model != undefined){
			var isFieldValid = GLI_FnaService.validateSmartSearch(model,list);
			if(isFieldValid==false){
				$scope.updateDynmaicErrMessage(errorKey);
				if(fieldTobeValidated == "nationality"){
					$scope.invalidNationality(errorKey);
				}
				else if(fieldTobeValidated == "birthAddressCountry"){
					$scope.invalidResidentCountry(errorKey);
				}
				else if(fieldTobeValidated == "city"){
					$scope.invalidCity(errorKey);
				}
				$scope.dynamicErrorMessages = _errors;
			}else{
				$scope.updateDynmaicErrMessage(errorKey);
			}

			$scope.updateErrorCount('aboutMeInsured');
		}else{
			$scope.updateDynmaicErrMessage(errorKey);
		}
	}
	
	if(typeof FnaVariables.choosePartyStatus !="undefined" && FnaVariables.choosePartyStatus == true)
    {
	 $scope.conditionalRequired = FnaVariables.choosePartyStatus;
    }
	
	if(typeof $scope.FNAMyself.isSaved == 'undefined')
	{
		$scope.FNAMyself.isSaved = false;
	}
		
	var translateEditDetails = "";
	if(!$scope.FNAMyself.isSaved)
	{
		translateEditDetails = translateMessages($translate, "fna.addDetails");
	}		
	else
		translateEditDetails = translateMessages($translate, "fna.editDetails");
	$('#txtHeaderInsured').html(translateEditDetails);
	$scope.memberIndex = -1;
	
	for(var i =0;i<$scope.FNABeneficiaries.length;i++)
	{
		if($scope.FNABeneficiaries[i])
			if($scope.FNABeneficiaries[i].memberIndex && $scope.FNABeneficiaries[i].memberIndex > 0)
			$scope.FNABeneficiaries[i].isSaved = true;
	}
	
	if($scope.FNAMyself.BasicDetails && $scope.FNAMyself.BasicDetails.photo)
	{
		if($scope.FNAMyself.BasicDetails.photo != FnaVariables.myselfPhoto){
			$scope.myselfPhotoValidation = "false";
			var fnaInsuredPhoto = translateMessages($translate, "fna.fnaInsuredPhoto");	
			$('#fnaInsuredPhoto').html(fnaInsuredPhoto);
			$scope.refresh();
		}
		else
		{
			var fnaInsuredPhoto = translateMessages($translate, "fna.fnaUploadPhoto");
			$('#fnaInsuredPhoto').html(fnaInsuredPhoto);
			$scope.refresh();
		}
	}
	else
	{
		var fnaInsuredPhoto = translateMessages($translate, "fna.fnaUploadPhoto");
		$('#fnaInsuredPhoto').html(fnaInsuredPhoto);
		$scope.refresh();
	}
	
	
	//function to add Contact Prefix
	$scope.addContactPrefix = function(data,memberType) {
		if(memberType == "myself")
		{
			if (data == "") {
				//var modelValue = $scope.LmsModel.Lead.ContactDetails.mobileNumber1;
				var modelValue = $scope.FNAMyself.ContactDetails.mobileNumber1;
				$scope.contactNo = modelValue.substring(
						rootConfig.mobileNoPrefix.length,
						modelValue.length);
			} else {
				$scope.FNAMyself.ContactDetails.mobileNumber1 = rootConfig.mobileNoPrefix + data;
			}
		}
		else
		{
			if (data == "") {
				//var modelValue = $scope.LmsModel.Lead.ContactDetails.mobileNumber1;
				var modelValue = $scope.Beneficiary.ContactDetails.homeNumber1;
				$scope.contactNo = modelValue.substring(
						rootConfig.mobileNoPrefix.length,
						modelValue.length);
			} else {
				$scope.Beneficiary.ContactDetails.homeNumber1 = rootConfig.mobileNoPrefix + data;
			}
		}
	}

	//Extended Function called on click of Add/edit details button of Member
    	$scope.addEdit = function(index, event) {
    		$scope.addEditFamilyDetails(event);
    		UtilityService.disableProgressTab = true;
    		$scope.memberIndex = index;

    		$scope.Beneficiary.BasicDetails = {
    			"firstName" : "",
    			"lastName" : ""
    		};
    		$scope.Beneficiary.ContactDetails = {
    			"homeNumber1" : "",
    			"mobileNumber1":"",
    			"emailId" : ""
    		};
    		if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
    			$scope.refresh();
    		}
    		$scope.dynamicErrorCount = 0;
    		$scope.dynamicErrorMessages = [];
    		if ($scope.FNABeneficiaries[index] != "null" && typeof $scope.FNABeneficiaries[index] != "undefined") {
    			$scope.Beneficiary = $scope.FNABeneficiaries[index];
    			$scope.edit = true;
    			$scope.BeneficiaryTemp = JSON.parse(JSON.stringify($scope.FNABeneficiaries[index]));
    			$scope.editIndex = index;
    			$scope.benificiaryPhotoValidation = "true";
    			//for check the image in edit mode
    			if ($scope.FNABeneficiaries[index].BasicDetails && $scope.FNABeneficiaries[index].BasicDetails.photo != "") {
    				$scope.benificiaryPhotoValidation = "false";
    			}

    			$scope.refresh();
                /* FNA Changes by LE Team Start (instead of 10, it will be 7 draggable circles, so changing it from 10 to 7) */
    			for (var i = 1; i <= 7; i++) {
                /* FNA Changes by LE Team Start */
    				$("#detailsBtn" + (i)).attr("disabled", "disabled");
    				$("#selfdetailsBtn").attr("disabled", "disabled");
    			}
    			$("#detailsBtn" + (index + 1)).attr("disabled", false);
    		} else {

    			$scope.Beneficiary.BasicDetails = {
    				"firstName" : "",
    				"lastName" : ""
    			};
    			$scope.Beneficiary.ContactDetails = {
    				"homeNumber1" : "",
    				"mobileNumber1":"",
    				"emailId" : ""
    			};
    		}
    		$('.scrapButton').attr("disabled", "disabled");
    		$('.scrapButton').css("display", "none");
    		$scope.refresh();
    		$scope.updateErrorCount('aboutMeBeneficiary');
    		//$rootScope.updateErrorCount('aboutMeBeneficiary');
    	};

	$scope.initialLoad= function(){
		/* FNA Changes by LE Team (Bug: 3734) Start */
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6']){
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
            '_UI_leadDetailsSection#leadDetailContent_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6']){
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
            '_UI_leadDetailsSection#popupContent_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6']){
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
            '_UI_popupTabsNavbar#leadDetail_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6']){
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
            '_UI_popupTabsNavbar#popupTabs_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
            '_UI_filterSection#filterSection_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4']){
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
            '_UI_FNAList#fnaList_4');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
            '_UI_HeaderDetails#HeaderDetails_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5']){
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
            '_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5']){
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
            '_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5']){
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
            '_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5']){
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
            '_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5');
        }
        /* NEW localStorage Definition STAGE-1 start */
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_Education#Education_1_7']){
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
            '_UI_Education#Education_1_7');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
            '_UI_callDetailsSection#popupContent_6');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_Protection#Protection_1_7']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
			'_UI_Protection#Protection_1_7');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsInvestment#fnaCalculatorTabsInvestment_5']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+
			'_UI_fnaCalculatorTabsInvestment#fnaCalculatorTabsInvestment_5');
		}
        /* NEW localStorage Definition STAGE-1 end */
        /* FNA Changes by LE Team (Bug: 3734) End */
        
        
		$scope.viewToBeCustomized = "aboutMeInsured";
		$scope.addContactPrefix($scope.contactNo,"myself");
		LEDynamicUI.getUIJson(
				rootConfig.FnaConfigJson,
				false,
				function(fnaConfig) {
					$scope.countryLists = fnaConfig.countryListsFNA;
					$scope.cityLists = fnaConfig.cityListsFNA;
					FnaVariables.myselfMandatoryFields = fnaConfig.myselfMandatoryFields;
					FnaVariables.choosepartyMandatoryFields = fnaConfig.choosepartyMandatoryFields;
				});
				var typeNameList = [
			{ "type": "OCODE", "isCacheable": true },
			{ "type": "INDUSTRY", "isCacheable": true },
			{ "type": "IDENTITY", "isCacheable": true },
			{ "type": "OCCUPATION", "isCacheable": true },
			{ "type": "JOB", "isCacheable": true },
			{ "type": "SUBJOB", "isCacheable": true },
			{ "type": "RELATIONSHIP", "isCacheable": true },
			{ "type": "INDUSTRY_LINEAR", "isCacheable": true },
			{ "type": "TITLE", "isCacheable": true },
			{ "type": "WARD", "isCacheable": true },
			{ "type": "OCCUPATION_LINEAR", "isCacheable": true },
			{ "type": "JOB_LINEAR", "isCacheable": true },
			{ "type": "NATUREOFWORK_LINEAR", "isCacheable": true },
			{ "type": "SUBJOB_LINEAR", "isCacheable": true }];
		GLI_DataLookupService.getMultipleLookUpData(typeNameList, function (lookUpList) {
			
			LEDynamicUI.paintUI(rootConfig.template, myFamilyDetailsUiJsonFile, "aboutMeInsured", "#aboutMeInsured", true, function() {
				$scope.initialCallback();
			}, $scope, $compile);
		}, function () {
			console.log("Error in multilookup data call");	
		});
	}
// Changes done by LETeam << Change starts here
   
	//Function called on click of Add/edit details button of Insured
	$scope.aboutMe = function(index, event) {
        $scope.index = index;
		$scope.event = event;      
      if($scope.FNAObject != undefined && $scope.FNAObject.FNA != undefined && $scope.FNAObject.FNA.saveCheckDependents != undefined &&  $scope.FNAObject.FNA.saveCheckDependents == false){
         
			 $scope.lePopupCtrl.showWarning(
						translateMessages($translate,
								"lifeEngage"),
						translateMessages($translate,
								"saveBeneficiaryData"),  
						translateMessages($translate,
								"fna.navok"),$scope.okPressed,translateMessages($translate,
								"general.navproceed"),$scope.aboutMeFn);
		} else {
            $scope.aboutMeFn(); 
        }	  
	};
    
    /* FNA Changes by LE Team Start */
    $scope.deleteBeneficiary = function(index, event) {		
        var id;
        var beneficiaryIndex;
		beneficiaryIndex = $scope.index;		
		$scope.index = index;
		$scope.event = event;
        var length=$scope.FNABeneficiaries.length;
        for (var i = 0; i < $scope.FNABeneficiaries.length; i++){
            if($scope.FNABeneficiaries[i]==undefined){                                
                $scope.FNABeneficiaries[i]=$scope.FNABeneficiaries[i+1];                
                length=length-1;                
            }
        } 
        for (var i = 0; i < $scope.FNABeneficiaries.length; i++){
            if($scope.FNABeneficiaries[i]!=undefined && $scope.FNABeneficiaries[i]==$scope.FNABeneficiaries[i+1])
            {
                delete $scope.FNABeneficiaries[i+1];
            }
            else if($scope.FNABeneficiaries[i]==undefined)
                $scope.FNABeneficiaries.length--;
        }        
		var beneficiaryLength = length;
        thisObjDeletionNew=thisObjDeletion.parentNode;
        var idNew=thisObjDeletionNew.id;
        if(beneficiaryIndex < (beneficiaryLength - 1)){            
			thisObjDeletion = thisObjDeletion.parentNode;
			//$(thisObjDeletion).removeClass().addClass("member ui-droppable");
			//$(thisObjDeletion).find('.relationText').html("Member");
			//$(thisObjDeletion).removeClass().addClass("owl-wrapper-outer owl-wrapper owl-item");
			//$(thisObjDeletion).parent().parent('.active').removeClass("active");
            id = thisObjDeletion.id;         
            thisObjDeletion.parentNode.parentNode.parentNode.childNodes[7].childNodes[0].childNodes[1].id=6;
            for(var i=id;i<=6;i++){
					if(+i+ +2 <=7){						
						thisObjDeletion.parentNode.parentNode.parentNode.childNodes[+i+ +2].childNodes[0].childNodes[1].id=i;						 
					}
			}
			for(var i=id;i<beneficiaryLength-1;i++){               
				$scope.FNABeneficiaries[+i+ +1].id=$scope.FNABeneficiaries[i].id;
			}
			$scope.FNABeneficiaries[id].id=6;
            thisObjDeletion.parentNode.parentNode.parentNode.appendChild(thisObjDeletion.parentNode.parentNode);
            
            $(thisObjDeletionNew).removeClass().addClass("member ui-droppable");
			$(thisObjDeletionNew).find('.relationText').html("Member");
			$(thisObjDeletionNew).parent().parent('.active').removeClass("active");
			$(thisObjDeletionNew).parent().parent().parent().children()[0].className='owl-item active';
            id = 6;
		}
		else{
			thisObjDeletion = thisObjDeletion.parentNode;
			$(thisObjDeletion).removeClass().addClass("member ui-droppable");
			$(thisObjDeletion).find('.relationText').html("Member");
			$(thisObjDeletion).parent().parent('.active').removeClass("active");
			$(thisObjDeletion).parent().parent().parent().children()[0].className='owl-item active';
            id = thisObjDeletion.id;
		}			
		
		$scope.removeMember(id);		
        //thisObjDeletion.parentNode.parentNode.parentNode.childNodes[7].childNodes[0].childNodes[1].id=6;
		$scope.global_val_array.splice(id, $scope.global_val_array.length);
        
        /* FNA Changes by LE Team Start (instead of 10, it will be 7 draggable circles, so changing it from 10 to 7) */
		for (var i = 1; i <= 7; i++) {
        /* FNA Changes by LE Team Start */
			$("#detailsBtn" + (i)).attr("disabled", false);
			$("#selfdetailsBtn").attr("disabled", false);
		}
		
		if(id == 0)
		{
			$scope.aboutMeFn();
			$(thisObjDeletion).closest('.owl-item').addClass('active');
			
		}
		else
		{
			var pos = id-1;
			for(var i = pos ; i >= 0; i--)
			{
				if(i == 0)
				{
					$scope.aboutMeFn();
					$(thisObjDeletion).closest('.owl-item').addClass('active');
				}				
				else
				{	
					if(typeof $scope.FNABeneficiaries[i] != "undefined")
					{
						/*var ind = i+1;
						
						$timeout(function() {
						    angular.element('.owl-item #'+i+' img').triggerHandler('click');
						}, 0);*/
						
						//var obj;
						// angular.element('.owl-item #'+i).trigger('click');
						/*$scope.addEdit(ind,event);*/
						var ind = i+1;
						//$scope.addEdit(ind,$scope.memberClickEvent);
						
						$scope.aboutMeFn();
						$(thisObjDeletion).closest('.owl-item').addClass('active');
						return true;
					}
				}
			}
		}		  
	};
    /* FNA Changes by LE Team End */
    
    /* FNA Changes by LE Team Start */
     $scope.validateCitizenIdBen=function(){
		var idCardSubstr=$scope.Beneficiary.BasicDetails.IDcard.substring(0,12);
		var count=0;
		var len=$scope.Beneficiary.BasicDetails.IDcard.length+1;
		var rem=0;
		var quotient=0;
		var min=0;
		var nDigit=0;
                    
        for(var i=0;i<idCardSubstr.length;i++)
         {
			len=len-1;                            
			count=count+parseInt($scope.Beneficiary.BasicDetails.IDcard.substring(i,i+1),10)*len;
         }
                    
         quotient = Math.floor(count/11);
         rem=count%11;
         min=11-rem;
         nDigit=parseInt($scope.Beneficiary.BasicDetails.IDcard.substring(12,13),10);
                
         if(min>9){
             min=min%10;
         }
         if(nDigit!==min)
         {
            return true;
		 }
    };
    /* FNA Changes by LE Team End */
    
// Changes done by LETeam << Change ends here
	$scope.aboutMeFn = function() {
		var index = $scope.index;
		var event = $scope.event;
        $scope.isInsuredClicked = true;
        $scope.isBenefeciaryClicked = false;
		$scope.showDeleteButton = false;
		$scope.contactNo = "";
		if($scope.memberIndex > -1)
		{
			if(typeof $scope.FNABeneficiaries[$scope.memberIndex]!= "undefined")
			$scope.cancelBeneficiaryOnSwitching();
		}
		//$scope.FNAMyself = globalService.getParty("FNAMyself");
		var myFamilyDetailsUiJsonFile = FnaVariables.uiJsonFile.fnaAboutMeFile;
		LEDynamicUI.paintUI(rootConfig.template, myFamilyDetailsUiJsonFile, "aboutMeInsured", "#aboutMeInsured", true, function() {
			$scope.myselfCallback();
		}, $scope, $compile);
		$scope.memberClickEvent = event;
		if(typeof event != "undefined" && event != null && event != "")
		{
			thisObjDeletion = event.target.parentNode.parentNode;
			var thisObjClassName = String(thisObjDeletion.className);
			var clickedNode = event.target.nodeName;
			var parentDiveNodes = $(clickedNode).parents('.owl-item');
			$(parentDiveNodes).removeClass("active");
			$(thisObjDeletion).closest('.owl-item').addClass('active');
		}
				
		$scope.addSelfDetails(event);
		//UtilityService.disableProgressTab = true;
		//GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
		$scope.memberIndex = -1;
		//$scope.insuredPhotoErrorCountUpdate();
        /* FNA Changes by LE Team Start (instead of 10, it will be 7 draggable circles, so changing it from 10 to 7) */
		for (var i = 1; i <= 7; i++) {
        /* FNA Changes by LE Team Start */
			$("#detailsBtn" + (i)).attr("disabled", "disabled");
		}
		
		
		if($scope.InsuredTemp.BasicDetails && $scope.InsuredTemp.BasicDetails.photo)
		{
			if($scope.InsuredTemp.BasicDetails.photo != FnaVariables.myselfPhoto){
				$scope.myselfPhotoValidation = "false";
				var fnaInsuredPhoto = translateMessages($translate, "fna.fnaInsuredPhoto");	
				$('#fnaInsuredPhoto').html(fnaInsuredPhoto);
				$scope.refresh();
			}
			else
			{
				var fnaInsuredPhoto = translateMessages($translate, "fna.fnaUploadPhoto");
				$('#fnaInsuredPhoto').html(fnaInsuredPhoto);
				$scope.refresh();
			}
		}
		else
		{
			var fnaInsuredPhoto = translateMessages($translate, "fna.fnaUploadPhoto");
			$('#fnaInsuredPhoto').html(fnaInsuredPhoto);
			$scope.refresh();
		}
		
		
		
		
		if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
			$scope.refresh();
		}
		var myselfFName = $scope.FNAMyself.BasicDetails.firstName;
		var myselfLName = $scope.FNAMyself.BasicDetails.lastName;
		
		if((typeof myselfFName =='undefined' || myselfFName == "null"))
			myselfFName = "";
		if((typeof myselfLName =='undefined' || myselfLName == "null"))
			myselfLName = "";
		if( $scope.FNAMyself.BasicDetails.firstName)
		if( $scope.FNAMyself.BasicDetails.firstName.length>25)
			{
			$('#txtMySelfLabel').addClass('fontSize13');
			}
		if((myselfFName == "" && myselfLName == ""))
		{
			$('#txtMySelfLabel').text(translateMessages($translate,"fna.myself"));
		}
		else
		{
			$('#txtMySelfLabel').text(myselfFName);
		}
		
		if(typeof $scope.FNAMyself.isSaved == 'undefined')
			$scope.FNAMyself.isSaved = false;
		var translateEditDetails = "";
		if(!$scope.FNAMyself.isSaved)
			translateEditDetails = translateMessages($translate, "fna.addDetails");
		else
			translateEditDetails = translateMessages($translate, "fna.editDetails");
		$('#txtHeaderInsured').html(translateEditDetails);
		$scope.addContactPrefix($scope.contactNo,'myself');
		/* FNA Changes By LE Team Starts -Bug: 4180, 4193 */
		$scope.showSecondary = false;
		/* FNA Changes By LE Team Ends -Bug: 4180, 4193 */
		GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
	};
	
	$scope.initilizeLabels = function()
	{		
		var myselfFName = $scope.FNAMyself.BasicDetails.firstName;
		var myselfLName = $scope.FNAMyself.BasicDetails.lastName;
		
		if((typeof myselfFName =='undefined' || myselfFName == "null"))
			myselfFName = "";
		if((typeof myselfLName =='undefined' || myselfLName == "null"))
			myselfLName = "";
		if( $scope.FNAMyself.BasicDetails.firstName)
		if( $scope.FNAMyself.BasicDetails.firstName.length>25)
			{
			$('#txtMySelfLabel').addClass('fontSize13');
			}
		if((myselfFName == "" && myselfLName == ""))
		{
			$('#txtMySelfLabel').text(translateMessages($translate,"fna.myself"));
		}
		else
		{
			$('#txtMySelfLabel').text(myselfFName);
		}
				
		for(var i= 0; i< $scope.FNABeneficiaries.length; i++)
		{
			var beneficiaryTxt = "detailsBtn" +( i + 1);
			if( $scope.FNABeneficiaries[i].BasicDetails)
			{
				var beneficiaryFName = $scope.FNABeneficiaries[i].BasicDetails.firstName;
				var beneficiaryLName = $scope.FNABeneficiaries[i].BasicDetails.lastName;
				
				if((typeof beneficiaryFName =='undefined' || beneficiaryFName == "null"))
					beneficiaryFName = "";
				if((typeof beneficiaryLName =='undefined' || beneficiaryLName == "null"))
					beneficiaryLName = "";
				if( $scope.FNABeneficiaries[i].BasicDetails.firstName>25)
							{
							$('#'+beneficiaryTxt).addClass('fontSize13');
							}
				
				if((beneficiaryFName == "" && beneficiaryLName == ""))
				{								
					$('#'+beneficiaryTxt).text(translateMessages($translate, "fna."+$scope.FNABeneficiaries[i].classNameAttached));
				}
				else
				{
					var beneficiaryFullname = beneficiaryFName;
					$('#'+beneficiaryTxt).text(beneficiaryFullname);
				}
			}
			else
			{					
				$('#'+beneficiaryTxt).text(translateMessages($translate, "fna."+$scope.FNABeneficiaries[i].classNameAttached));
			}
		}
	}
	
	$scope.checkNameChange = function(index)
	{
		if(index == -1)
		{
			var myselfFName = $scope.FNAMyself.BasicDetails.firstName;
			var myselfLName = $scope.FNAMyself.BasicDetails.lastName;
			
			if((typeof myselfFName =='undefined' || myselfFName == "null"))
				myselfFName = "";
			if((typeof myselfLName =='undefined' || myselfLName == "null"))
				myselfLName = "";
			
			if( $scope.FNAMyself.BasicDetails.firstName)
			if( $scope.FNAMyself.BasicDetails.firstName.length>25)
			{
			$('#txtMySelfLabel').addClass('fontSize13');
			}
			if((myselfFName == "" && myselfLName == ""))
			{
				$('#txtMySelfLabel').text(translateMessages($translate,"fna.myself"));
			}
			else
			{
				$('#txtMySelfLabel').text(myselfFName);
			}
		}
		else
		{
			var beneficiaryTxt = "detailsBtn" +( index + 1);
			if( $scope.FNABeneficiaries[index].BasicDetails)
			{
				var beneficiaryFName = $scope.FNABeneficiaries[index].BasicDetails.firstName;
				var beneficiaryLName = $scope.FNABeneficiaries[index].BasicDetails.lastName;
				
				if((typeof beneficiaryFName =='undefined' || beneficiaryFName == "null"))
					beneficiaryFName = "";
				if((typeof beneficiaryLName =='undefined' || beneficiaryLName == "null"))
					beneficiaryLName = "";
				if($scope.FNABeneficiaries[index].BasicDetails.firstName)
				if($scope.FNABeneficiaries[index].BasicDetails.firstName.length>25)
					{
					$('#'+beneficiaryTxt).addClass('fontSize13');
					}
				if((beneficiaryFName == "" && beneficiaryLName == ""))
				{					
					$('#'+beneficiaryTxt).text(translateMessages($translate, "fna."+$scope.FNABeneficiaries[index].classNameAttached));
				}
				else
				{
					var beneficiaryFullname = beneficiaryFName;
					$("#detailsBtn" + (index + 1)).text(beneficiaryFullname);
				}		
				
			}
			else
			{					
				$('#'+beneficiaryTxt).text(translateMessages($translate, "fna."+$scope.FNABeneficiaries[index].classNameAttached));
			}
			
			
			
		}	
	}
 /* Change by LETeam << starts here */
    $scope.formatLabelOccupation = function (model, options, type, index) {
        var x = parseInt(index);
        
        if (options && options.length >= 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code && ((type !== 'industry' && ($scope.FNAMyself.OccupationDetails[x].industry==="" || $scope.FNAMyself.OccupationDetails[x].industry===undefined)) || (type !== 'industry' && $scope.allowCodeUpdate)) && !$scope.SaveTrans) {
                    if (type == 'occupationCode') {
                        $scope.FNAMyself.OccupationDetails[x].id=x;
                        $scope.FNAMyself.OccupationDetails[x].occupationCodeValue = options[i].value;
                        $scope.updateCode = true;
                    }
                    return options[i].value;
                }

                if (options.length > 1 && type == 'industry' && ($scope.allowCodeUpdate || $scope.updateCode) && !$scope.SaveTrans) {
                    $scope.FNAMyself.OccupationDetails[x].industryValue = options[0].value;
                    $scope.FNAMyself.OccupationDetails[x].descriptionValue = options[1].value;
                    $scope.FNAMyself.OccupationDetails[x].jobValue = options[2].value;
                    $scope.FNAMyself.OccupationDetails[x].subJobValue = options[3].value;
                    $scope.FNAMyself.OccupationDetails[x].occupationClassValue = options[4].value;
                    $scope.FNAMyself.OccupationDetails[x].natureofWorkValue = options[5].value;

                    $scope.FNAMyself.OccupationDetails[x].industry = options[0].value;
                    $scope.FNAMyself.OccupationDetails[x].description = options[1].value;
                    $scope.FNAMyself.OccupationDetails[x].job = options[2].value;
                    $scope.FNAMyself.OccupationDetails[x].subJob = options[3].value;
                    $scope.FNAMyself.OccupationDetails[x].occupationClass = options[4].value;
                    $scope.FNAMyself.OccupationDetails[x].natureofWork = options[5].value;
                                       
                    if(!angular.equals(undefined,$scope.FNAMyself.OccupationDetails)){  
                        
                    for(var i=0;i<$scope.FNAMyself.OccupationDetails.length,i<x;i++){
                        if(!angular.equals(undefined,$scope.FNAMyself.OccupationDetails[i])){
                            if(!angular.equals(undefined,$scope.FNAMyself.OccupationDetails[i].occupationCode) && $scope.FNAMyself.OccupationDetails[i].occupationCode!==''){
						  if($scope.FNAMyself.OccupationDetails[x].occupationCode == $scope.FNAMyself.OccupationDetails[i].occupationCode){
							
                            $scope.FNAMyself.OccupationDetails[x]=[];
                            $scope.leadDetShowPopUpMsg = true;
                            $scope.enterMandatory = translateMessages($translate, "illustrator.duplicateOccupation");
                         
						  }
                        }
                        }
					   }
                    }
					$scope.refresh();
                                                           
                    $scope.updateCode = false;
                    return options[0].value;
                } else if (type == 'industry' && !$scope.allowCodeUpdate) {
					$scope.refresh();
                     if(!angular.equals(undefined, $scope.FNAMyself.OccupationDetails[x].industryValue)){
                        return $scope.FNAMyself.OccupationDetails[x].industryValue;
					}else{
						return $scope.FNAMyself.OccupationDetails[x].industry;
					}
				
				}
            
            if(!angular.equals(undefined, $scope.FNAMyself.OccupationDetails[x])){
                if (type == 'occupationCode' && ($scope.updateCode || $scope.FNAMyself.OccupationDetails[x].industry)) {
                    if(x===1){
                        $scope.showSecondary=true;
                    }/* FNA Changes By LE Team Starts -Bug: 4180, 4193 */
                    /*else{
                        $scope.showSecondary=false;
                    }*/
	                 /* FNA Changes By LE Team Ends -Bug: 4180, 4193 */
                    if(!angular.equals(undefined, $scope.FNAMyself.OccupationDetails[x].occupationCodeValue)){
                        return $scope.FNAMyself.OccupationDetails[x].occupationCodeValue;
					}else{
						return $scope.FNAMyself.OccupationDetails[x].occupationCode;
					}
					
                }                 
            }
            }
        }
    }

    $scope.formatLabelOccupationFromNOW = function (model, options, type, index) {
        var x = parseInt(index);
        
        if (options && options.length >= 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code && ((type !== 'industry' && ($scope.FNAMyself.OccupationDetails[x].occupationCode==="" || $scope.FNAMyself.OccupationDetails[x].occupationCode===undefined)) || (type !== 'industry' && $scope.allowCodeUpdate)) && !$scope.SaveTrans) {
                    if (type == 'natureOfWorkLinear') {
                        $scope.FNAMyself.OccupationDetails[x].id=x;
                        $scope.FNAMyself.OccupationDetails[x].natureofWorkValue = options[i].value;
                        $scope.updateCode = true;
                    }
                    return options[i].value;
                }

                if (options.length > 1 && type == 'occupationCode' && ($scope.allowCodeUpdate || $scope.updateCode) && !$scope.SaveTrans) {
                	$scope.FNAMyself.OccupationDetails[x].occupationCodeValue = options[0].value;
                    $scope.FNAMyself.OccupationDetails[x].industryValue = options[1].value;
                    $scope.FNAMyself.OccupationDetails[x].descriptionValue = options[2].value;
                    $scope.FNAMyself.OccupationDetails[x].jobValue = options[3].value;
                    $scope.FNAMyself.OccupationDetails[x].subJobValue = options[4].value;
                    $scope.FNAMyself.OccupationDetails[x].occupationClassValue = options[5].value;                   

                    $scope.FNAMyself.OccupationDetails[x].occupationCode = options[0].value;
                    $scope.FNAMyself.OccupationDetails[x].industry = options[1].value;
                    $scope.FNAMyself.OccupationDetails[x].description = options[2].value;
                    $scope.FNAMyself.OccupationDetails[x].job = options[3].value;
                    $scope.FNAMyself.OccupationDetails[x].subJob = options[4].value;
                    $scope.FNAMyself.OccupationDetails[x].occupationClass = options[5].value;
                    
                                       
                    if(!angular.equals(undefined,$scope.FNAMyself.OccupationDetails)){  
                        
                    for(var i=0;i<$scope.FNAMyself.OccupationDetails.length,i<x;i++){
                        if(!angular.equals(undefined,$scope.FNAMyself.OccupationDetails[i])){
                            if(!angular.equals(undefined,$scope.FNAMyself.OccupationDetails[i].occupationCode) && $scope.FNAMyself.OccupationDetails[i].occupationCode!==''){
						  if($scope.FNAMyself.OccupationDetails[x].occupationCode == $scope.FNAMyself.OccupationDetails[i].occupationCode){
							
                            $scope.FNAMyself.OccupationDetails[x]=[];
                            $scope.leadDetShowPopUpMsg = true;
                            $scope.enterMandatory = translateMessages($translate, "illustrator.duplicateOccupation");
                         
						  }
                        }
                        }
					   }
                    }
					$scope.refresh();
                                                           
                    $scope.updateCode = false;
                    return options[0].value;
                } else if (type == 'occupationCode' && !$scope.allowCodeUpdate) {
					$scope.refresh();
                     if(!angular.equals(undefined, $scope.FNAMyself.OccupationDetails[x].occupationCodeValue)){
                        return $scope.FNAMyself.OccupationDetails[x].occupationCodeValue;
					}else{
						return $scope.FNAMyself.OccupationDetails[x].occupationCode;
					}
				
				}
            
            if(!angular.equals(undefined, $scope.FNAMyself.OccupationDetails[x])){
                if (type == 'natureOfWorkLinear' && ($scope.updateCode || $scope.FNAMyself.OccupationDetails[x].occupationCode)) {
                    if(x===1){
                        $scope.showSecondary=true;
                    }/* FNA Changes By LE Team Starts -Bug: 4180, 4193 */
                    /*else{
                        $scope.showSecondary=false;
                    }*/
	                 /* FNA Changes By LE Team Ends -Bug: 4180, 4193 */
                    if(!angular.equals(undefined, $scope.FNAMyself.OccupationDetails[x].natureofWorkValue)){
                        return $scope.FNAMyself.OccupationDetails[x].natureofWorkValue;
					}else{
						return $scope.FNAMyself.OccupationDetails[x].natureofWork;
					}
					
                }                 
            }
            }
        }
    }

	$scope.formatLabelOccupationBen = function (model, options, type, index) {
        var x = parseInt(index);
        
        if (options && options.length >= 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code && ((type !== 'industry' && ($scope.Beneficiary.OccupationDetails[x].industry==="" || $scope.Beneficiary.OccupationDetails[x].industry===undefined)) || (type !== 'industry' && $scope.allowCodeUpdateBen)) && !$scope.SaveTransBen) {
                    if (type == 'occupationCode') {
                        $scope.Beneficiary.OccupationDetails[x].id=x;
                        $scope.Beneficiary.OccupationDetails[x].occupationCodeValue = options[i].value;
                        $scope.updateCodeBen = true;
                    }
                    return options[i].value;
                }

                if (options.length > 1 && type == 'industry' && ($scope.allowCodeUpdateBen || $scope.updateCodeBen) && !$scope.SaveTransBen) {
                    $scope.Beneficiary.OccupationDetails[x].industryValue = options[0].value;
                    $scope.Beneficiary.OccupationDetails[x].descriptionValue = options[1].value;
                    $scope.Beneficiary.OccupationDetails[x].jobValue = options[2].value;
                    $scope.Beneficiary.OccupationDetails[x].subJobValue = options[3].value;
                    $scope.Beneficiary.OccupationDetails[x].occupationClassValue = options[4].value;
                    $scope.Beneficiary.OccupationDetails[x].natureofWorkValue = options[5].value;

                    $scope.Beneficiary.OccupationDetails[x].industry = options[0].value;
                    $scope.Beneficiary.OccupationDetails[x].description = options[1].value;
                    $scope.Beneficiary.OccupationDetails[x].job = options[2].value;
                    $scope.Beneficiary.OccupationDetails[x].subJob = options[3].value;
                    $scope.Beneficiary.OccupationDetails[x].occupationClass = options[4].value;
                    $scope.Beneficiary.OccupationDetails[x].natureofWork = options[5].value;
                                       
                    if(!angular.equals(undefined,$scope.Beneficiary.OccupationDetails)){  
                        
                    for(var i=0;i<$scope.Beneficiary.OccupationDetails.length,i<x;i++){
                        if(!angular.equals(undefined,$scope.Beneficiary.OccupationDetails[i])){
                            if(!angular.equals(undefined,$scope.Beneficiary.OccupationDetails[i].occupationCode) && $scope.Beneficiary.OccupationDetails[i].occupationCode!==''){
						  if($scope.Beneficiary.OccupationDetails[x].occupationCode == $scope.Beneficiary.OccupationDetails[i].occupationCode){
							
                            $scope.Beneficiary.OccupationDetails[x]=[];
                            $scope.leadDetShowPopUpMsg = true;
                            $scope.enterMandatory = translateMessages($translate, "illustrator.duplicateOccupation");
                         
						  }
                        }
                        }
					   }
                    }
					$scope.refresh();
                                                           
                    $scope.updateCodeBen = false;
                    return options[0].value;
                } else if (type == 'industry' && !$scope.allowCodeUpdateBen) {
					$scope.refresh();
					if(!angular.equals(undefined, $scope.Beneficiary.OccupationDetails[x].industryValue)){
                        return $scope.Beneficiary.OccupationDetails[x].industryValue;
					}else{
						return $scope.Beneficiary.OccupationDetails[x].industry;
					}
				}
            
            if(!angular.equals(undefined, $scope.Beneficiary.OccupationDetails[x])){
                if (type == 'occupationCode' && ($scope.updateCodeBen || $scope.Beneficiary.OccupationDetails[x].industry)) {
                    if(x===1){
                        $scope.showSecondary=true;
                    }/* FNA Changes By LE Team Starts -Bug: 4180, 4193 */
                    /*else{
                       // $scope.showSecondary=false;
                    }*/
                    /* FNA Changes By LE Team Ends -Bug: 4180, 4193 */
                    if(!angular.equals(undefined, $scope.Beneficiary.OccupationDetails[x].occupationCodeValue)){
                        return $scope.Beneficiary.OccupationDetails[x].occupationCodeValue;
					}else{
						return $scope.Beneficiary.OccupationDetails[x].occupationCode;
					}
					
                }                 
            }
            }
        }
    }

    $scope.formatLabelOccupationBenFromNOW = function (model, options, type, index) {
        var x = parseInt(index);
        
        if (options && options.length >= 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code && ((type !== 'industry' && ($scope.Beneficiary.OccupationDetails[x].occupationCode==="" || $scope.Beneficiary.OccupationDetails[x].occupationCode===undefined)) || (type !== 'industry' && $scope.allowCodeUpdateBen)) && !$scope.SaveTransBen) {
                    if (type == 'natureOfWorkLinear') {
                        $scope.Beneficiary.OccupationDetails[x].id=x;                        
                        $scope.Beneficiary.OccupationDetails[x].natureofWorkValue = options[i].value;
                        $scope.updateCodeBen = true;
                    }
                    return options[i].value;
                }

                if (options.length > 1 && type == 'occupationCode' && ($scope.allowCodeUpdateBen || $scope.updateCodeBen) && !$scope.SaveTransBen) {
                	$scope.Beneficiary.OccupationDetails[x].occupationCodeValue = options[0].value;
                    $scope.Beneficiary.OccupationDetails[x].industryValue = options[1].value;
                    $scope.Beneficiary.OccupationDetails[x].descriptionValue = options[2].value;
                    $scope.Beneficiary.OccupationDetails[x].jobValue = options[3].value;
                    $scope.Beneficiary.OccupationDetails[x].subJobValue = options[4].value;
                    $scope.Beneficiary.OccupationDetails[x].occupationClassValue = options[5].value;
                    

                    $scope.Beneficiary.OccupationDetails[x].occupationCode = options[0].value;
                    $scope.Beneficiary.OccupationDetails[x].industry = options[1].value;
                    $scope.Beneficiary.OccupationDetails[x].description = options[2].value;
                    $scope.Beneficiary.OccupationDetails[x].job = options[3].value;
                    $scope.Beneficiary.OccupationDetails[x].subJob = options[4].value;
                    $scope.Beneficiary.OccupationDetails[x].occupationClass = options[5].value;
                   
                                       
                    if(!angular.equals(undefined,$scope.Beneficiary.OccupationDetails)){  
                        
                    for(var i=0;i<$scope.Beneficiary.OccupationDetails.length,i<x;i++){
                        if(!angular.equals(undefined,$scope.Beneficiary.OccupationDetails[i])){
                            if(!angular.equals(undefined,$scope.Beneficiary.OccupationDetails[i].occupationCode) && $scope.Beneficiary.OccupationDetails[i].occupationCode!==''){
						  if($scope.Beneficiary.OccupationDetails[x].occupationCode == $scope.Beneficiary.OccupationDetails[i].occupationCode){
							
                            $scope.Beneficiary.OccupationDetails[x]=[];
                            $scope.leadDetShowPopUpMsg = true;
                            $scope.enterMandatory = translateMessages($translate, "illustrator.duplicateOccupation");
                         
						  }
                        }
                        }
					   }
                    }
					$scope.refresh();
                                                           
                    $scope.updateCodeBen = false;
                    return options[0].value;
                } else if (type == 'occupationCode' && !$scope.allowCodeUpdateBen) {
					$scope.refresh();
					if(!angular.equals(undefined, $scope.Beneficiary.OccupationDetails[x].occupationCodeValue)){
                        return $scope.Beneficiary.OccupationDetails[x].occupationCodeValue;
					}else{
						return $scope.Beneficiary.OccupationDetails[x].occupationCode;
					}
				}
            
            if(!angular.equals(undefined, $scope.Beneficiary.OccupationDetails[x])){
                if (type == 'natureOfWorkLinear' && ($scope.updateCodeBen || $scope.Beneficiary.OccupationDetails[x].occupationCode)) {
                    if(x===1){
                        $scope.showSecondary=true;
                    }/* FNA Changes By LE Team Starts -Bug: 4180, 4193 */
                    /*else{
                       // $scope.showSecondary=false;
                    }*/
                    /* FNA Changes By LE Team Ends -Bug: 4180, 4193 */
                    if(!angular.equals(undefined, $scope.Beneficiary.OccupationDetails[x].natureofWorkValue)){
                        return $scope.Beneficiary.OccupationDetails[x].natureofWorkValue;
					}else{
						return $scope.Beneficiary.OccupationDetails[x].natureofWork;
					}
					
                }                 
            }
            }
        }
    }
    $scope.addOccupation = function () {        
        if ($scope.FNAMyself.OccupationDetails[0].occupationCode!=="" && !angular.equals(undefined, $scope.FNAMyself.OccupationDetails[0].occupationCode)) {
            $scope.showSecondary = true;
        }
        if($scope.FNAMyself.OccupationDetails[2]!==undefined){
        if ($scope.FNAMyself.OccupationDetails[2].occupationCode!=="" && !angular.equals(undefined, $scope.FNAMyself.OccupationDetails[2].occupationCode)) {
            $scope.showSecondary = true;
        }
        }
    }

    $scope.removeOccupation = function () {
        $scope.showSecondary = false;
        //$scope.FNAMyself.OccupationDetails[1].occupationCode='';
        $scope.FNAMyself.OccupationDetails[1]="";
        $scope.FNAMyself.OccupationDetails[3]="";
    }
    $scope.addOccupationBeneficiary = function () {        
        if ($scope.Beneficiary.OccupationDetails[0].occupationCode!=="" && !angular.equals(undefined, $scope.Beneficiary.OccupationDetails[0].occupationCode)) {
            $scope.showSecondary = true;
        }
        if($scope.Beneficiary.OccupationDetails[2]!==undefined){
        if ($scope.Beneficiary.OccupationDetails[2].occupationCode!=="" && !angular.equals(undefined, $scope.Beneficiary.OccupationDetails[2].occupationCode)) {
            $scope.showSecondary = true;
        }
        }
    }

    $scope.removeOccupationBeneficiary = function () {
        $scope.showSecondary = false;
        //$scope.Beneficiary.OccupationDetails[1].occupationCode='';
        $scope.Beneficiary.OccupationDetails[1]="";
        $scope.Beneficiary.OccupationDetails[3]="";
    }

    
// Changes done by LETeam << Change starts here
    //For clearing out district/ward fields on updating town/district fields
    $scope.markAsEdited = function (field) {        
    }
    
    $scope.clearFieldsBen = function (val) {
        if (val == 'occupationCodeBen' && $scope.FNAMyself.OccupationDetails[0].industry) {
            $scope.allowCodeUpdateBen = true;                                  
            $scope.Beneficiary.OccupationDetails[0]=[];
            $scope.updateErrorDynamically();
        } 
    };
    
        $scope.updateErrorDynamically = function () {
			
       
    }
      $scope.calculateAge = function (dob, id) {
        var age = "";
        if (dob != "" && dob != undefined &&
            dob != null) {
            var dobDate = new Date(dob);
            /*
             * In I.E it will accept date format in '/'
             * separator only
             */
            if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
                dobDate = new Date(dob.split("-").join(
                    "/"));
            }
            var todayDate = new Date();
            if (formatForDateControl(dob) >= formatForDateControl(new Date())) {
                //$scope.FNAMyself.BasicDetails.age = 0;
                age = 0;
            } else {
                var curd = new Date(todayDate
                    .getFullYear(), todayDate
                    .getMonth(), todayDate
                    .getDate());
                var cald = new Date(dobDate
                    .getFullYear(), dobDate
                    .getMonth(), dobDate.getDate());
                var dife = $scope.ageCalculation(curd,cald);

                if (age < 1) {
                    var datediff = curd.getTime() -
                        cald.getTime();
                    var days = (datediff / (24 * 60 * 60 * 1000)) / 365;
                    days = Number(Math.round(days +
                            'e3') +
                        'e-3');
                    age = 0;
                }
                if (dife[1] == "false") {
                    age = dife[0];
                } else {
                    age = dife[0] + 1;
                }

                if (dob == undefined || dob == "") {
                    age = "-";
                }
            }
        }
        if (id == 'aboutMe') {
            $scope.FNAMyself.BasicDetails.age = age;
        } else if (id == 'benefeciary') {
            $scope.Beneficiary.BasicDetails.age = age;
        }
         $scope.onFieldChange();
    };
    
     $scope.evaluateString = function (value) {
        var val;
        var variableValue = $scope;
        val = value.split('.');
        if (value.indexOf('$scope') != -1) {
            val = val.shift();
        }
        for (var i in val) {
            variableValue = variableValue[val[i]];
        }
        return variableValue;
    };
 /* Change by LETeam << starts here */
    $scope.validateDuplicate=function(index){       
            for(var i=0;i<$scope.FNAMyself.OccupationDetails.length,i<index;i++){ 
                if($scope.FNAMyself.OccupationDetails[i]){
                    if($scope.FNAMyself.OccupationDetails[index].occupationCode === $scope.FNAMyself.OccupationDetails[i].occupationCode){
							
                        $scope.FNAMyself.OccupationDetails[index]=[];
                        $scope.leadDetShowPopUpMsg = true;
                        $scope.enterMandatory = translateMessages($translate, "illustrator.duplicateOccupation");                 
						}
                    }
            }         
            $scope.refresh();
    }
    $scope.validateDuplicateBen=function(index){       
            for(var i=0;i<$scope.Beneficiary.OccupationDetails.length,i<index;i++){ 
                if($scope.Beneficiary.OccupationDetails[i]){
                    if($scope.Beneficiary.OccupationDetails[index].occupationCode === $scope.Beneficiary.OccupationDetails[i].occupationCode){
							
                        $scope.Beneficiary.OccupationDetails[index]=[];
                        $scope.leadDetShowPopUpMsg = true;
                        $scope.enterMandatory = translateMessages($translate, "illustrator.duplicateOccupation");                 
						}
                    }
            }         
            $scope.refresh();
    }	
    $scope.formatLabelOccupationLinear = function (model, options, type, index) {        
        var indexId = parseInt(index);
        $scope.setLinear=true;
        if (options && options.length >= 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code) {
                    if (type == 'occupationCodeLinear' && !angular.equals(undefined, $scope.FNAMyself.OccupationDetails[indexId].subJob) && !$scope.SaveTrans) {	
                        $scope.FNAMyself.OccupationDetails[indexId].occupationCodeValue = options[i].value;
                        $scope.FNAMyself.OccupationDetails[indexId].id=indexId;
						$scope.FNAMyself.OccupationDetails[indexId].occupationCode = options[i].value;
                        $scope.validateDuplicate(index);
                    } else if (type === 'industryLinear' && !$scope.SaveTrans && ($scope.FNAMyself.OccupationDetails[indexId].description==='' || $scope.FNAMyself.OccupationDetails[indexId].description===undefined)) {	
                        $scope.FNAMyself.OccupationDetails[indexId].industryValue = options[i].value;
                    } else if (type == 'occupationLinear' && !angular.equals(undefined, $scope.FNAMyself.OccupationDetails[indexId].industry) && !$scope.SaveTrans) {
                        $scope.FNAMyself.OccupationDetails[indexId].descriptionValue = options[i].value;
                    } else if (type == 'jobLinear' && !angular.equals(undefined, $scope.FNAMyself.OccupationDetails[indexId].description) && !$scope.SaveTrans) {
                        $scope.FNAMyself.OccupationDetails[indexId].jobValue = options[i].value;
                    }else if (type == 'subjobLinear' && !angular.equals(undefined, $scope.FNAMyself.OccupationDetails[indexId].job) && !$scope.SaveTrans) {
                        $scope.FNAMyself.OccupationDetails[indexId].subJobValue = options[i].value;
                    }else if(type=='occupationClassLinear'){
                        $scope.FNAMyself.OccupationDetails[indexId].occupationClassValue = options[i].value;
                    }
                                        
                    return options[i].value;
                }
            }
        }
    }
    $scope.formatLabelOccupationLinearBen = function (model, options, type, index) {        
        var indexId = parseInt(index);
        $scope.setLinear=true;
        if (options && options.length >= 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code) {
                    if (type == 'occupationCodeLinear' && !angular.equals(undefined, $scope.Beneficiary.OccupationDetails[indexId].subJob) && !$scope.SaveTransBen) {	
                        $scope.Beneficiary.OccupationDetails[indexId].occupationCodeValue = options[i].value;
                        $scope.Beneficiary.OccupationDetails[indexId].id=indexId;
						$scope.Beneficiary.OccupationDetails[indexId].occupationCode = options[i].value;
                        $scope.validateDuplicateBen(index);
                    } else if (type === 'industryLinear' && !$scope.SaveTransBen && ($scope.Beneficiary.OccupationDetails[indexId].description==='' || $scope.Beneficiary.OccupationDetails[indexId].description===undefined)) {	
                        $scope.Beneficiary.OccupationDetails[indexId].industryValue = options[i].value;
                    } else if (type == 'occupationLinear' && !angular.equals(undefined, $scope.Beneficiary.OccupationDetails[indexId].industry) && !$scope.SaveTransBen) {
                        $scope.Beneficiary.OccupationDetails[indexId].descriptionValue = options[i].value;
                    } else if (type == 'jobLinear' && !angular.equals(undefined, $scope.Beneficiary.OccupationDetails[indexId].description) && !$scope.SaveTransBen) {
                        $scope.Beneficiary.OccupationDetails[indexId].jobValue = options[i].value;
                    }else if (type == 'subjobLinear' && !angular.equals(undefined, $scope.Beneficiary.OccupationDetails[indexId].job) && !$scope.SaveTransBen) {
                        $scope.Beneficiary.OccupationDetails[indexId].subJobValue = options[i].value;
                    }else if(type=='occupationClassLinear'){
                        $scope.Beneficiary.OccupationDetails[indexId].occupationClassValue = options[i].value;
                    }
                                        
                    return options[i].value;
                }
            }
        }
    }
 /* Change by LETeam << ends here */ 	
    $scope.formatLabelOccupationPayor = function (model, options, type, index) {
        var y = parseInt(index);
        if (options && options.length >= 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code && ((type !== 'industry' && ($scope.FNAMyself.OccupationDetails[y].industry==="" || $scope.FNAMyself.OccupationDetails[y].industry===undefined)) || (type !== 'industry'))) {
                    if (type == 'occupationCode') {
                        $scope.FNAMyself.OccupationDetails[y].id=y;
                        $scope.FNAMyself.OccupationDetails[y].occupationCodeValue = options[i].value;
                       
                        $scope.updateCodePayor = true;
                    }
                    return options[i].value;
                }
                if (type == 'industry' && !$scope.isInitialLoad0) {
                    $scope.FNAMyself.OccupationDetails[y].industryValue = options[0].value;
                    $scope.FNAMyself.OccupationDetails[y].descriptionValue = options[1].value;
                    $scope.FNAMyself.OccupationDetails[y].jobValue = options[2].value;
                    $scope.FNAMyself.OccupationDetails[y].subJobValue = options[3].value;
                    $scope.FNAMyself.OccupationDetails[y].occupationClassValue = options[4].value;

                    $scope.FNAMyself.OccupationDetails[y].industry = options[0].value;
                    $scope.FNAMyself.OccupationDetails[y].description = options[1].value;
                    $scope.FNAMyself.OccupationDetails[y].job = options[2].value;
                    $scope.FNAMyself.OccupationDetails[y].subJob = options[3].value;
                    $scope.FNAMyself.OccupationDetails[y].occupationClass = options[4].value;                   
                    
                    if(!angular.equals(undefined,$scope.FNAMyself.OccupationDetails)){
                    for(var i=0;i<$scope.FNAMyself.OccupationDetails.length,i<y;i++){
						if(!angular.equals(undefined,$scope.FNAMyself.OccupationDetails[i])){
                            if($scope.FNAMyself.OccupationDetails[y].occupationCode == $scope.FNAMyself.OccupationDetails[i].occupationCode){
							
                            $scope.FNAMyself.OccupationDetails[y]=[];
                            $scope.leadDetShowPopUpMsg = true;
                            $scope.enterMandatory = translateMessages($translate, "illustrator.duplicateOccupation");

						  }
                        }
					   }
                    }
					$scope.refresh();
                    
                    $scope.updateCodePayor = false;
                    return options[0].value;
                }
            if(!angular.equals(undefined, $scope.FNAMyself.OccupationDetails[y])){
                if (type == 'occupationCode' && ($scope.FNAMyself.OccupationDetails[y].industry)) {
                    if(y===1){
                        $scope.showSecondaryPayor=true;
                    }else{
                        $scope.showSecondaryPayor=false;
                    }
                    if(!angular.equals(undefined,$scope.FNAMyself.OccupationDetails[y].occupationCodeValue)){
                        return $scope.FNAMyself.OccupationDetails[y].occupationCodeValue;
                    }else{
                        return $scope.FNAMyself.OccupationDetails[y].occupationCode;
                    }
                } else if (type == 'industry' && $scope.FNAMyself.OccupationDetails[y].occupationCode) {
                    $scope.updateErrorDynamically();
                    return model;
                }
            }
            }
        }
    }
    $scope.formatLabelOccupationPayor1 = function (model, options, type, index) {
        var y = parseInt(index);
        if (options && options.length >= 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code && ((type !== 'industry' && ($scope.FNAMyself.OccupationDetails[y].industry==="" || $scope.FNAMyself.OccupationDetails[y].industry===undefined)) || (type !== 'industry'))) {
                    if (type == 'occupationCode') {
                        $scope.FNAMyself.OccupationDetails[y].id=y;
                        $scope.FNAMyself.OccupationDetails[y].occupationCodeValue = options[i].value;
                       
                        $scope.updateCodePayor = true;
                    }
                    return options[i].value;
                }
                if (type == 'industry' && !$scope.isInitialLoad1 && $scope.FNAMyself.OccupationDetails.length > 1) {
                    $scope.FNAMyself.OccupationDetails[y].industryValue = options[0].value;
                    $scope.FNAMyself.OccupationDetails[y].descriptionValue = options[1].value;
                    $scope.FNAMyself.OccupationDetails[y].jobValue = options[2].value;
                    $scope.FNAMyself.OccupationDetails[y].subJobValue = options[3].value;
                    $scope.FNAMyself.OccupationDetails[y].occupationClassValue = options[4].value;

                    $scope.FNAMyself.OccupationDetails[y].industry = options[0].value;
                    $scope.FNAMyself.OccupationDetails[y].description = options[1].value;
                    $scope.FNAMyself.OccupationDetails[y].job = options[2].value;
                    $scope.FNAMyself.OccupationDetails[y].subJob = options[3].value;
                    $scope.FNAMyself.OccupationDetails[y].occupationClass = options[4].value;                   
                    
                    if(!angular.equals(undefined,$scope.FNAMyself.OccupationDetails)){
                    for(var i=0;i<$scope.FNAMyself.OccupationDetails.length,i<y;i++){
						if(!angular.equals(undefined,$scope.FNAMyself.OccupationDetails[i])){
                            if($scope.FNAMyself.OccupationDetails[y].occupationCode == $scope.FNAMyself.OccupationDetails[i].occupationCode){
							
                            $scope.FNAMyself.OccupationDetails[y]=[];
                            $scope.leadDetShowPopUpMsg = true;
                            $scope.enterMandatory = translateMessages($translate, "illustrator.duplicateOccupation");

						  }
                        }
					   }
                    }
					$scope.refresh();
                    
                    $scope.updateCodePayor = false;
                    return options[0].value;
                }
            if(!angular.equals(undefined, $scope.FNAMyself.OccupationDetails[y])){
                if (type == 'occupationCode' && ($scope.FNAMyself.OccupationDetails[y].industry)) {
                    if(y===1){
                        $scope.showSecondaryPayor=true;
                    }else{
                        $scope.showSecondaryPayor=false;
                    }
                    if(!angular.equals(undefined,$scope.FNAMyself.OccupationDetails[y].occupationCodeValue)){
                        return $scope.FNAMyself.OccupationDetails[y].occupationCodeValue;
                    }else{
                        return $scope.FNAMyself.OccupationDetails[y].occupationCode;
                    }
                } else if (type == 'industry' && $scope.FNAMyself.OccupationDetails[y].occupationCode) {
                    $scope.updateErrorDynamically();
                    return model;
                }
            }
            }
        }
    }
	$scope.formatLabelOccupationPayorBen = function (model, options, type, index) {
        var y = parseInt(index);
        if (options && options.length >= 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code && ((type !== 'industry' && ($scope.Beneficiary.OccupationDetails[y].industry==="" || $scope.Beneficiary.OccupationDetails[y].industry===undefined)) || (type !== 'industry'))) {
                    if (type == 'occupationCode') {
                        $scope.Beneficiary.OccupationDetails[y].id=y;
                        $scope.Beneficiary.OccupationDetails[y].occupationCodeValue = options[i].value;
                       
                        $scope.updateCodePayor = true;
                    }
                    return options[i].value;
                }
                if (type == 'industry'  && !$scope.isInitialLoadBen0) {
                    $scope.Beneficiary.OccupationDetails[y].industryValue = options[0].value;
                    $scope.Beneficiary.OccupationDetails[y].descriptionValue = options[1].value;
                    $scope.Beneficiary.OccupationDetails[y].jobValue = options[2].value;
                    $scope.Beneficiary.OccupationDetails[y].subJobValue = options[3].value;
                    $scope.Beneficiary.OccupationDetails[y].occupationClassValue = options[4].value;

                    $scope.Beneficiary.OccupationDetails[y].industry = options[0].value;
                    $scope.Beneficiary.OccupationDetails[y].description = options[1].value;
                    $scope.Beneficiary.OccupationDetails[y].job = options[2].value;
                    $scope.Beneficiary.OccupationDetails[y].subJob = options[3].value;
                    $scope.Beneficiary.OccupationDetails[y].occupationClass = options[4].value;                   
                    
                    if(!angular.equals(undefined,$scope.Beneficiary.OccupationDetails)){
                    for(var i=0;i<$scope.Beneficiary.OccupationDetails.length,i<y;i++){
						if(!angular.equals(undefined,$scope.Beneficiary.OccupationDetails[i])){
                            if($scope.Beneficiary.OccupationDetails[y].occupationCode == $scope.Beneficiary.OccupationDetails[i].occupationCode){
							
                            $scope.Beneficiary.OccupationDetails[y]=[];
                            $scope.leadDetShowPopUpMsg = true;
                            $scope.enterMandatory = translateMessages($translate, "illustrator.duplicateOccupation");

						  }
                        }
					   }
                    }
					$scope.refresh();
                    
                    $scope.updateCodePayor = false;
                    return options[0].value;
                }
            if(!angular.equals(undefined, $scope.Beneficiary.OccupationDetails[y])){
                if (type == 'occupationCode' && ($scope.Beneficiary.OccupationDetails[y].industry)) {
                    if(y===1){
                        $scope.showSecondaryPayorBen=true;
                    }else{
                        $scope.showSecondaryPayorBen=false;
                    }
                    if(!angular.equals(undefined,$scope.Beneficiary.OccupationDetails[y].occupationCodeValue)){
                        return $scope.Beneficiary.OccupationDetails[y].occupationCodeValue;
                    }else{
                        return $scope.Beneficiary.OccupationDetails[y].occupationCode;
                    }
                } else if (type == 'industry' && ((!FnaVariables.choosePartyStatus && $scope.Beneficiary.OccupationDetails[y].occupationCode) || (FnaVariables.choosePartyStatus && $scope.Beneficiary.OccupationDetails[y].occupationCode))) {
                    $scope.updateErrorDynamically();
                    return model;
                }
            }
            }
        }
    }
    $scope.formatLabelOccupationPayorBen1 = function (model, options, type, index) {
        var y = parseInt(index);
        if (options && options.length >= 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code && ((type !== 'industry' && ($scope.Beneficiary.OccupationDetails[y].industry==="" || $scope.Beneficiary.OccupationDetails[y].industry===undefined)) || (type !== 'industry'))) {
                    if (type == 'occupationCode') {
                        $scope.Beneficiary.OccupationDetails[y].id=y;
                        $scope.Beneficiary.OccupationDetails[y].occupationCodeValue = options[i].value;
                       
                        $scope.updateCodePayor = true;
                    }
                    return options[i].value;
                }
                if (type == 'industry' && !$scope.isInitialLoadBen1 && $scope.Beneficiary.OccupationDetails[1] != undefined) {
                    $scope.Beneficiary.OccupationDetails[y].industryValue = options[0].value;
                    $scope.Beneficiary.OccupationDetails[y].descriptionValue = options[1].value;
                    $scope.Beneficiary.OccupationDetails[y].jobValue = options[2].value;
                    $scope.Beneficiary.OccupationDetails[y].subJobValue = options[3].value;
                    $scope.Beneficiary.OccupationDetails[y].occupationClassValue = options[4].value;

                    $scope.Beneficiary.OccupationDetails[y].industry = options[0].value;
                    $scope.Beneficiary.OccupationDetails[y].description = options[1].value;
                    $scope.Beneficiary.OccupationDetails[y].job = options[2].value;
                    $scope.Beneficiary.OccupationDetails[y].subJob = options[3].value;
                    $scope.Beneficiary.OccupationDetails[y].occupationClass = options[4].value;                   
                    
                    if(!angular.equals(undefined,$scope.Beneficiary.OccupationDetails)){
                    for(var i=0;i<$scope.Beneficiary.OccupationDetails.length,i<y;i++){
						if(!angular.equals(undefined,$scope.Beneficiary.OccupationDetails[i])){
                            if($scope.Beneficiary.OccupationDetails[y].occupationCode == $scope.Beneficiary.OccupationDetails[i].occupationCode){
							
                            $scope.Beneficiary.OccupationDetails[y]=[];
                            $scope.leadDetShowPopUpMsg = true;
                            $scope.enterMandatory = translateMessages($translate, "illustrator.duplicateOccupation");

						  }
                        }
					   }
                    }
					$scope.refresh();
                    
                    $scope.updateCodePayor = false;
                    return options[0].value;
                }
            if(!angular.equals(undefined, $scope.Beneficiary.OccupationDetails[y])){
                if (type == 'occupationCode' && ($scope.Beneficiary.OccupationDetails[y].industry)) {
                    if(y===1){
                        $scope.showSecondaryPayorBen=true;
                    }else{
                        $scope.showSecondaryPayorBen=false;
                    }
                    if(!angular.equals(undefined,$scope.Beneficiary.OccupationDetails[y].occupationCodeValue)){
                        return $scope.Beneficiary.OccupationDetails[y].occupationCodeValue;
                    }else{
                        return $scope.Beneficiary.OccupationDetails[y].occupationCode;
                    }
                } else if (type == 'industry' && ((!FnaVariables.choosePartyStatus && $scope.Beneficiary.OccupationDetails[y].occupationCode) || (FnaVariables.choosePartyStatus && $scope.Beneficiary.OccupationDetails[y].occupationCode))) {
                    $scope.updateErrorDynamically();
                    return model;
                }
            }
            }
        }
    }
    $scope.populateIndependentLookupDateInScope2 = function (type, fieldName, model, setDefault, field, currentTab, successCallBack, errorCallBack,cacheable) {
		if (!cacheable){
						cacheable = false;
					}
        $scope[fieldName] = [{
            "key": "",
            "value": ""
					               }];
        
        var options = {};
        options.type = type;
        DataLookupService.getLookUpData(options, function (data) {
            $scope[fieldName] = data;
            if (setDefault) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isDefault == 1) {
                        if (type == "NATIONALITY" || type == "NATUREOFWORK_LINEAR") {
                            if ($scope.FNAMyself.BasicDetails.nationality == "") {
                                $scope.setValuetoScope(model, data[i].code);
                                break;
                            } else {
                                $scope.fromReset = false;
                            }
                        } else {
                            $scope.setValuetoScope(model, data[i].value);
                            break;
                        }
                    }
                }
            } else {
                var n = "$scope." + model;
                if (typeof "$scope." + model != "undefined")
                    var m = $scope.evaluateString(model);
                if (m) {
                    $scope.setValuetoScope(model, m);
                }

            }
            if (typeof field != "undefined" && typeof currentTab != "undefined") {
                setTimeout(function () {
                    if (typeof $scope.currentTab != "undefined") {
                        var data_Evaluvated = $scope.currentTab[field];
                        if (typeof data_Evaluvated != "undefined") {
                            dataEvaluvated.$render();
                        }
                    }
                }, 0);
            }
            if (typeof $scope.lmsSectionId != "undefined" && $scope.evaluateString($scope.lmsSectionId) != undefined) {
                var data_Evaluvated = $scope.lmsSectionId[fieldName];
                if (data_Evaluvated) {
                    data_Evaluvated.$render();
                }
            }
            $scope.refresh();
            if(successCallBack && successCallBack!=""){
                successCallBack();
            }
        }, function (errorData) {
            $scope[fieldName] = [];

        },cacheable);
    };

    $scope.populateIndependentLookupDateInScope = function (type, fieldName, model, setDefault, field, currentTab, successCallBack, errorCallBack,cacheable) {
    	/* Defect 4180 starts */
		if (!cacheable){
						cacheable = false;
					}
        $scope[fieldName] = [{
            "key": "",
            "value": ""
					               }];
		/* Defect 4180 ends */
        var options = {};
        options.type = type;
        DataLookupService.getLookUpData(options, function (data) {
            $scope[fieldName] = data;
            $rootScope[fieldName] = data;
            if (setDefault) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isDefault == 1) {
                        if (type == "NATIONALITY" || type == "NATUREOFWORK_LINEAR") {
                            if ($scope.FNAMyself.BasicDetails.nationality == "") {
                                $scope.setValuetoScope(model, data[i].value);
                                break;
                            } else {
                                $scope.fromReset = false;
                            }
                        } else {
                            $scope.setValuetoScope(model, data[i].value);
                            break;
                        }
                    }
                }
            } else {
                if (model) {
                    var n = "$scope." + model;
                    var m = $scope.evaluateString(model);
                    //var m = eval (n);
                    if (m) {
                        $scope.setValuetoScope(model, m);
                    }
                }
            }
            if (typeof field != "undefined" && typeof currentTab != "undefined") {
                setTimeout(function () {
                    var dataEvaluvated = $scope.currentTab[field];
                    //var dataEvaluvated=eval ('$scope.'+currentTab+'.'+field);
                    if (typeof dataEvaluvated != "undefined") {
                        dataEvaluvated.$render();
                        //eval ('$scope.'+currentTab+'.'+field).$render();

                    }

                }, 0);
            }
            if (typeof $scope.lmsSectionId != "undefined") {
                var data_Evaluvated = $scope.lmsSectionId[fieldName];
                if (data_Evaluvated) {
                    data_Evaluvated.$render();
                    // eval ('$scope.'+$scope.lmsSectionId+'.'+fieldName).$render(); }
                }
            }
            $scope.refresh();
            successCallBack();
        }, function (errorData) {
            $scope[fieldName] = [];

        },cacheable);
    }
	$scope.populateIndependentLookupDateInScope2Ben = function (type, fieldName, model, setDefault, field, currentTab, successCallBack, errorCallBack,cacheable) {
	if (!cacheable){
						cacheable = false;
					}
        $scope[fieldName] = [{
            "key": "",
            "value": ""
					               }];
        
        var options = {};
        options.type = type;
        DataLookupService.getLookUpData(options, function (data) {
            $scope[fieldName] = data;
            if (setDefault) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isDefault == 1) {
                        if (type == "NATIONALITY" || type == "NATUREOFWORK_LINEAR") {
                            if ($scope.Beneficiary.BasicDetails.nationality == "") {
                                $scope.setValuetoScope(model, data[i].code);
                                break;
                            } else {
                                $scope.fromReset = false;
                            }
                        } else {
                            $scope.setValuetoScope(model, data[i].value);
                            break;
                        }
                    }
                }
            } else {
                var n = "$scope." + model;
                if (typeof "$scope." + model != "undefined")
                    var m = $scope.evaluateString(model);
                if (m) {
                    $scope.setValuetoScope(model, m);
                }

            }
            if (typeof field != "undefined" && typeof currentTab != "undefined") {
                setTimeout(function () {
                    if (typeof $scope.currentTab != "undefined") {
                        var data_Evaluvated = $scope.currentTab[field];
                        if (typeof data_Evaluvated != "undefined") {
                            dataEvaluvated.$render();
                        }
                    }
                }, 0);
            }
            if (typeof $scope.lmsSectionId != "undefined" && $scope.evaluateString($scope.lmsSectionId) != undefined) {
                var data_Evaluvated = $scope.lmsSectionId[fieldName];
                if (data_Evaluvated) {
                    data_Evaluvated.$render();
                }
            }
            $scope.refresh();
            if(successCallBack && successCallBack!=""){
                successCallBack();
            }
        }, function (errorData) {
            $scope[fieldName] = [];

        },cacheable);
    };

    $scope.populateIndependentLookupDateInScopeBen = function (type, fieldName, model, setDefault, field, currentTab, successCallBack, errorCallBack,cacheable) {
	if (!cacheable){
						cacheable = false;
					}
        /* Defect 4180 Starts */
        $scope[fieldName] = [{
            "key": "",
            "value": ""
					               }];
		/* Defect 4180 Ends */
        var options = {};
        options.type = type;
        DataLookupService.getLookUpData(options, function (data) {
            $scope[fieldName] = data;
            $rootScope[fieldName] = data;
            if (setDefault) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isDefault == 1) {
                        if (type == "NATIONALITY") {
                            if ($scope.Beneficiary.BasicDetails.nationality == "") {
                                $scope.setValuetoScope(model, data[i].value);
                                break;
                            } else {
                                $scope.fromReset = false;
                            }
                        } else {
                            $scope.setValuetoScope(model, data[i].value);
                            break;
                        }
                    }
                }
            } else {
                if (model) {
                    var n = "$scope." + model;
                    var m = $scope.evaluateString(model);
                    //var m = eval (n);
                    if (m) {
                        $scope.setValuetoScope(model, m);
                    }
                }
            }
            if (typeof field != "undefined" && typeof currentTab != "undefined") {
                setTimeout(function () {
                    var dataEvaluvated = $scope.currentTab[field];
                    //var dataEvaluvated=eval ('$scope.'+currentTab+'.'+field);
                    if (typeof dataEvaluvated != "undefined") {
                        dataEvaluvated.$render();
                        //eval ('$scope.'+currentTab+'.'+field).$render();

                    }

                }, 0);
            }
            if (typeof $scope.lmsSectionId != "undefined") {
                var data_Evaluvated = $scope.lmsSectionId[fieldName];
                if (data_Evaluvated) {
                    data_Evaluvated.$render();
                    // eval ('$scope.'+$scope.lmsSectionId+'.'+fieldName).$render(); }
                }
            }
            $scope.refresh();
            successCallBack();
        }, function (errorData) {
            $scope[fieldName] = [];

        },cacheable);
    }
   // Changes done by LETeam << Change ends here
   
	//over writing function to rename status  initial to draft 
	$scope.saveBeneficiary = function() {
		if ($scope.errorCount == 0) {
			$scope.saveFamilyDetails();
             $scope.loadOccupationDetailsBen();             
			UtilityService.disableProgressTab = false;
			if ($scope.edit == true) {
				$scope.edit = false;
			} else {
				// Binding the model to Beneficiary scope variable. On saving setting the scope variable back to FNAObject model beneficary mapping
				//$scope.FNABeneficiaries[$scope.memberIndex] = $scope.Beneficiary;
				if($scope.memberIndex > -1)
				$scope.FNABeneficiaries[$scope.memberIndex] = angular.copy($scope.Beneficiary) ;
			}
			if (($scope.FNABeneficiaries[$scope.memberIndex].classNameAttached == "Son")
					|| ($scope.FNABeneficiaries[$scope.memberIndex].classNameAttached == "Father")
					|| ($scope.FNABeneficiaries[$scope.memberIndex].classNameAttached == "Brother")) {
				$scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.gender = "Male";
			} else {
				$scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.gender = "Female";
			}
	
			$scope.defaultImageForMembers[$scope.memberIndex].photo = $scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.photo;
			$scope.Beneficiary = {};
            /* FNA Changes by LE Team Start (instead of 10, it will be 7 draggable circles, so changing it from 10 to 7) */
			for (var i = 1; i <= 7; i++) {
            /* FNA Changes by LE Team Start */
				$("#detailsBtn" + (i)).attr("disabled", false);
				$("#selfdetailsBtn").attr("disabled", false);
			}
	
			globalService.setFNABeneficiaries($scope.FNABeneficiaries);
			var parties = globalService.getParties();
			$scope.FNAObject.FNA.parties = parties;
			var translateEditDetails = translateMessages($translate, "fna.editDetails");
			$('#txtHeaderInsured').html(translateEditDetails);
			FnaVariables.setFnaModel($scope.FNAObject);
			$scope.status = "Draft";
			var obj = new GLI_FnaService.saveTransactions($scope, $rootScope,
					DataService, $translate, UtilityService, false);
			obj.save(function () {
				/* FNA Changes By LE Team Starts -Bug: 4180, 4193 */
				$scope.SaveTransBen = false;
				/* FNA Changes By LE Team Ends -Bug: 4180, 4193 */
                });
			
			//onDelete
			} else {
			
			$rootScope.showHideLoadingImage(false);
			$scope.lePopupCtrl.showWarning(
					translateMessages($translate,
							"generaliIndonesia"),
					translateMessages($translate,
							"enterMandatory"),
					translateMessages($translate,
							"fna.ok"));
		}
	};
	
	$scope.$on('initiate-save', function (evt, obj) {
		//$scope.disableLePopCtrl = true;
		var parties = globalService.getParties();
		$scope.FNAObject.FNA.parties = parties;		
		FnaVariables.setFnaModel($scope.FNAObject);
		$scope.status = "Draft";
		var obj = new GLI_FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
		obj.save(function() {
			$rootScope.showHideLoadingImage(false, "");
			$rootScope.passwordLock  = false;
			$rootScope.isAuthenticated = false;
			$location.path('/login');
		});
	});
// Changes done by LETeam << Change starts here
   $scope.onSelectTypeHead = function (id) {
        $timeout(function () {
            $('#' + id).blur();
        }, 300);
    }

  $scope.setTitleCodeForFNA = function (model) {
        if ($scope.title && model && model !== "") {
            var filteredValue = UtilityService.getFilteredDataBasedOnInput(model, $scope.title, 'value');
            if (filteredValue && filteredValue.length) {
                $scope.FNAMyself.BasicDetails.title = filteredValue[0].code;
            }
            $scope.updateErrorDynamically();
        }
    };
 /* Change by LETeam << starts here */
   $scope.clearFields = function (val) {
        if (val == 'occupationCode' && $scope.FNAMyself.OccupationDetails[0].industry) {
            $scope.allowCodeUpdate = true;                                  
            $scope.FNAMyself.OccupationDetails[0]=[];
            $scope.updateErrorDynamically();
        } else if (val == 'occupationCodeNew' && $scope.FNAMyself.OccupationDetails[1].industry) {
            $scope.allowCodeUpdate = true;
            $scope.FNAMyself.OccupationDetails[1]=[];
            $scope.updateErrorDynamically();
        } else if (val == 'natureOfWorkLinear' && $scope.FNAMyself.OccupationDetails[2].occupationCode) {
            $scope.allowCodeUpdate = true;
            $scope.FNAMyself.OccupationDetails[2]=[]; 
            $scope.updateErrorDynamically();
        } else if (val == 'natureOfWorkLinear2' && $scope.FNAMyself.OccupationDetails[3].occupationCode) {
            $scope.allowCodeUpdate = true;
            $scope.FNAMyself.OccupationDetails[3]=[]; 
            $scope.updateErrorDynamically();
        }
    };
	$scope.clearFieldsBen = function (val) {
        if (val == 'occupationCode' && $scope.Beneficiary.OccupationDetails[0].industry) {
            $scope.allowCodeUpdateBen = true;                                  
            $scope.Beneficiary.OccupationDetails[0]=[];
            $scope.updateErrorDynamically();
        } else if (val == 'occupationCodeNew' && $scope.Beneficiary.OccupationDetails[1].industry) {
            $scope.allowCodeUpdateBen = true;
            $scope.Beneficiary.OccupationDetails[1]=[];
            $scope.updateErrorDynamically();
        } else if (val == 'natureOfWorkLinear' && $scope.Beneficiary.OccupationDetails[2].occupationCode) {
            $scope.allowCodeUpdate = true;
            $scope.Beneficiary.OccupationDetails[2]=[]; 
            $scope.updateErrorDynamically();
        } else if (val == 'natureOfWorkLinear2' && $scope.Beneficiary.OccupationDetails[3].occupationCode) {
            $scope.allowCodeUpdate = true;
            $scope.Beneficiary.OccupationDetails[3]=[]; 
            $scope.updateErrorDynamically();
        }
    };
	$scope.clearField=function(val,index){
        if (val == 'LinearInsured') {            
            $scope.FNAMyself.OccupationDetails[index]=[];
        }if (val == 'LinearOccupation') {                        
            var industry=$scope.FNAMyself.OccupationDetails[index].industry;
                       
            $scope.FNAMyself.OccupationDetails[index]=[];
            
            $scope.FNAMyself.OccupationDetails[index].industry = industry;          
        }else if (val == 'LinearJob') {
            var industry=$scope.FNAMyself.OccupationDetails[index].industry;
            var description=$scope.FNAMyself.OccupationDetails[index].description;            
            
            $scope.FNAMyself.OccupationDetails[index]=[];
            
            $scope.FNAMyself.OccupationDetails[index].industry = industry;
            $scope.FNAMyself.OccupationDetails[index].description = description;               
        }else if(val == 'LinearSubjob'){  
            var industry=$scope.FNAMyself.OccupationDetails[index].industry;
            var description=$scope.FNAMyself.OccupationDetails[index].description;
            var job=$scope.FNAMyself.OccupationDetails[index].job;
            
            $scope.FNAMyself.OccupationDetails[index]=[];
            
            $scope.FNAMyself.OccupationDetails[index].industry = industry;
            $scope.FNAMyself.OccupationDetails[index].description = description;    
            $scope.FNAMyself.OccupationDetails[index].job = job;
        }
    }
	$scope.clearFieldBen=function(val,index){
        if (val == 'LinearInsured') {            
            $scope.Beneficiary.OccupationDetails[index]=[];
        }if (val == 'LinearOccupation') {                        
            var industry=$scope.Beneficiary.OccupationDetails[index].industry;
                       
            $scope.Beneficiary.OccupationDetails[index]=[];
            
            $scope.Beneficiary.OccupationDetails[index].industry = industry;          
        }else if (val == 'LinearJob') {
            var industry=$scope.Beneficiary.OccupationDetails[index].industry;
            var description=$scope.Beneficiary.OccupationDetails[index].description;            
            
            $scope.Beneficiary.OccupationDetails[index]=[];
            
            $scope.Beneficiary.OccupationDetails[index].industry = industry;
            $scope.Beneficiary.OccupationDetails[index].description = description;               
        }else if(val == 'LinearSubjob'){  
            var industry=$scope.Beneficiary.OccupationDetails[index].industry;
            var description=$scope.Beneficiary.OccupationDetails[index].description;
            var job=$scope.Beneficiary.OccupationDetails[index].job;
            
            $scope.Beneficiary.OccupationDetails[index]=[];
            
            $scope.Beneficiary.OccupationDetails[index].industry = industry;
            $scope.Beneficiary.OccupationDetails[index].description = description;    
            $scope.Beneficiary.OccupationDetails[index].job = job;
        }
    }
 /* Change by LETeam << ends here */
    $scope.showMoreBene = function() {
        $scope.Beneficiary.BasicDetails.showMoreAboutMe = true;   
    };
    $scope.showLessBene = function() {        
        $scope.Beneficiary.BasicDetails.showMoreAboutMe = false;          
    };
    if ($scope.FNAMyself.BasicDetails.showMoreAboutMe == undefined) {
            $scope.FNAMyself.BasicDetails.showMoreAboutMe = false;
        }
    $scope.showMoreAboutMe = function() {        
            $scope.FNAMyself.BasicDetails.showMoreAboutMe = true;          
    };
    $scope.showLessAboutMe = function() {        
            $scope.FNAMyself.BasicDetails.showMoreAboutMe = false;          
    };
    
     $scope.checkGenderWithTitle = function() {
        /*if($scope.titleGenderPairInsured!==undefined && $scope.errorFlag!==true && $scope.FNAMyself.BasicDetails.gender!==undefined && $scope.FNAMyself.BasicDetails.gender!=="" && $scope.FNAMyself.BasicDetails.title!==undefined && $scope.FNAMyself.BasicDetails.title!==""){            
               var i = 0;
                if($scope.titleGenderPairInsured[0].code!=="Both" && $scope.titleGenderPairInsured!=="Both"){
                    if($scope.titleGenderPairInsured[0].code!==$scope.FNAMyself.BasicDetails.gender){
                     $scope.dynamicErrorCount= $scope.dynamicErrorCount+1;
                     var error = {};
                     error.message = translateMessages($translate, "illustrator.titleGenderMismatchInsured");
                     error.key = "title";
                     $scope.dynamicErrorMessages.push(error);
                     $rootScope.updateErrorCount($scope.isInsured?'aboutMeInsured':'aboutMeBeneficiary');				
                    } else {
						for ( i ; i < $scope.dynamicErrorMessages.length; i++) {
							if ($scope.dynamicErrorMessages[i].key == 'title') {
								$scope.dynamicErrorMessages.splice(i, 1);
                                $scope.dynamicErrorCount = $scope.dynamicErrorCount -1;
                                $scope.refresh();
							}
						}
						
					}
                }
        }*/
    }
    
     $scope.validateGenderTitle=function(){  
        $scope.errorFlag=false;
		if ($scope.isInsuredClicked) {
			if($scope.titleGenderPairInsured!==undefined && $scope.errorFlag!==true && $scope.FNAMyself.BasicDetails.gender!==undefined && $scope.FNAMyself.BasicDetails.gender!=="" && $scope.FNAMyself.BasicDetails.title!==undefined && $scope.FNAMyself.BasicDetails.title!==""){            
					if($scope.titleGenderPairInsured[0].code!=="Both" && $scope.titleGenderPairInsured!=="Both"){
						if($scope.titleGenderPairInsured[0].code!==$scope.FNAMyself.BasicDetails.gender && $scope.titleGenderPairInsured!==$scope.FNAMyself.BasicDetails.gender){
							$scope.errorFlag=true;
							$scope.leadDetShowPopUpMsg = true;
							 $scope.enterMandatory = translateMessages($translate, "fna.titleGenderMismatchInsured");
							
							$rootScope.showHideLoadingImage(false);
						}
					}
			}
        } else {
			if($scope.titleGenderPairPayer!==undefined && $scope.errorFlag!==true && $scope.Beneficiary.BasicDetails.gender!==undefined && $scope.Beneficiary.BasicDetails.gender!=="" && $scope.Beneficiary.BasicDetails.title!==undefined && $scope.Beneficiary.BasicDetails.title!==""){            
					if($scope.titleGenderPairPayer[0].code!=="Both" && $scope.titleGenderPairPayer!=="Both"){
						if($scope.titleGenderPairPayer[0].code!==$scope.Beneficiary.BasicDetails.gender && $scope.titleGenderPairPayer!==$scope.Beneficiary.BasicDetails.gender){
							$scope.errorFlag=true;
							$scope.leadDetShowPopUpMsg = true;
							/* FNA changes made by LE team for bug 4327 starts */
							$scope.enterMandatory = translateMessages($translate, "fna.titleGenderMismatchBeneficiary");
							/* FNA changes made by LE team for bug 4327 ends */
							$rootScope.showHideLoadingImage(false);
						}
					}
			}        
		}
		return $scope.errorFlag;
		/*
        if ($scope.FNAMyself.BasicDetails.gender != undefined && $scope.FNAMyself.BasicDetails.gender != "" && $scope.FNAMyself.BasicDetails.title != undefined && $scope.FNAMyself.BasicDetails.title != "") {
            if($scope.FNAMyself.BasicDetails.gender == 'Female')
        {
            if ($scope.FNAMyself.BasicDetails.title == 'MR') {
                $scope.errorFlag=true;
                    $scope.leadDetShowPopUpMsg = true;
                    $scope.enterMandatory = translateMessages($translate, "fna.titleGenderMismatchInsured");
                    
                    $rootScope.showHideLoadingImage(false);
            }
        }        
        else if (!(($scope.FNAMyself.BasicDetails.title == 'MR') || ($scope.FNAMyself.BasicDetails.title == 'DR')))
        {
                    $scope.errorFlag=true;
                    $scope.leadDetShowPopUpMsg = true;
                    $scope.enterMandatory = translateMessages($translate, "fna.titleGenderMismatchInsured");
                    
                    $rootScope.showHideLoadingImage(false);
                }
         if($scope.Beneficiary != undefined && $scope.Beneficiary.BasicDetails != undefined && $scope.Beneficiary.BasicDetails.gender != undefined && $scope.Beneficiary.BasicDetails.gender == 'Female')
        {
            if ($scope.Beneficiary.BasicDetails.title != undefined && $scope.Beneficiary.BasicDetails.title == 'MR') {
                $scope.errorFlag=true;
                    $scope.leadDetShowPopUpMsg = true;
                    $scope.enterMandatory = translateMessages($translate, "fna.titleGenderMismatchInsured");
                    
                    $rootScope.showHideLoadingImage(false);
            }
        }        
        else if ($scope.Beneficiary != undefined && $scope.Beneficiary.BasicDetails != undefined && $scope.Beneficiary.BasicDetails.title != undefined && !(($scope.Beneficiary.BasicDetails.title == 'MR') || ($scope.Beneficiary.BasicDetails.title == 'DR')))
        {
                    $scope.errorFlag=true;
                    $scope.leadDetShowPopUpMsg = true;
                    $scope.enterMandatory = translateMessages($translate, "fna.titleGenderMismatchInsured");
                    
                    $rootScope.showHideLoadingImage(false);
                } */
        
     }
    
 /* Change by LETeam << starts here */    var OccupationInsured=[];
    
    $scope.getInsuredOccupation=function(){    
        OccupationInsured=[];
        if(!angular.equals(undefined,OccupationInsured))
                        {
                            OccupationInsured.slice();
                        }
        
        for(var i=0;i<4;i++){
        if(!angular.equals(undefined,$scope.FNAMyself.OccupationDetails[i])){
            if(!angular.equals(undefined,$scope.FNAMyself.OccupationDetails[i].occupationCode) && $scope.FNAMyself.OccupationDetails[i].occupationCode!==""){
            var OccupationDetailsArr = {};
            OccupationDetailsArr={
                            "id":i,
							"occupationCode":$scope.FNAMyself.OccupationDetails[i].occupationCode,
                            "description":$scope.FNAMyself.OccupationDetails[i].description,
							"job":($scope.FNAMyself.OccupationDetails[i].job.indexOf('NA')===-1)?$scope.FNAMyself.OccupationDetails[i].job:"NA",
							"subJob":($scope.FNAMyself.OccupationDetails[i].subJob.indexOf('NA')===-1)?$scope.FNAMyself.OccupationDetails[i].subJob:"NA",
							"occupationClass":$scope.FNAMyself.OccupationDetails[i].occupationClass,
							"industry":$scope.FNAMyself.OccupationDetails[i].industry,
							"occupationCodeValue":$scope.FNAMyself.OccupationDetails[i].occupationCodeValue,
                            "descriptionValue":$scope.FNAMyself.OccupationDetails[i].descriptionValue,
							"jobValue":$scope.FNAMyself.OccupationDetails[i].jobValue,
							"subJobValue":$scope.FNAMyself.OccupationDetails[i].subJobValue,
							"occupationClassValue":$scope.FNAMyself.OccupationDetails[i].occupationClassValue,
							"industryValue":$scope.FNAMyself.OccupationDetails[i].industryValue,
							"natureofWork":$scope.FNAMyself.OccupationDetails[i].natureofWorkValue,
                            "natureofWorkValue":$scope.FNAMyself.OccupationDetails[i].natureofWorkValue
					};
                if(OccupationDetailsArr.id===3){
                    $scope.showSecondary=true;
                }
            OccupationInsured.push(OccupationDetailsArr);
            }
          }            
        }
        $scope.FNAMyself.OccupationDetails[2]="";
        $scope.FNAMyself.OccupationDetails[3]="";
       
    }
	var OccupationBeneficiary=[];
    
    $scope.getBeneficiaryOccupation=function(){    
        OccupationBeneficiary=[];
        if(!angular.equals(undefined,OccupationBeneficiary))
                        {
                            OccupationBeneficiary.slice();
                        }
        
        for(var i=0;i<4;i++){
        if(!angular.equals(undefined,$scope.Beneficiary.OccupationDetails[i])){
            if(!angular.equals(undefined,$scope.Beneficiary.OccupationDetails[i].occupationCode) && $scope.Beneficiary.OccupationDetails[i].occupationCode!==""){
            var OccupationDetailsArr = {};
            OccupationDetailsArr={
                            "id":i,
							"occupationCode":$scope.Beneficiary.OccupationDetails[i].occupationCodeValue,
                            "description":$scope.Beneficiary.OccupationDetails[i].description,
							"job":($scope.Beneficiary.OccupationDetails[i].job.indexOf('NA')===-1)?$scope.Beneficiary.OccupationDetails[i].job:"NA",
							"subJob":($scope.Beneficiary.OccupationDetails[i].subJob.indexOf('NA')===-1)?$scope.Beneficiary.OccupationDetails[i].subJob:"NA",
							"occupationClass":$scope.Beneficiary.OccupationDetails[i].occupationClass,
							"industry":$scope.Beneficiary.OccupationDetails[i].industry,
							"occupationCodeValue":$scope.Beneficiary.OccupationDetails[i].occupationCodeValue,
                            "descriptionValue":$scope.Beneficiary.OccupationDetails[i].descriptionValue,
							"jobValue":$scope.Beneficiary.OccupationDetails[i].jobValue,
							"subJobValue":$scope.Beneficiary.OccupationDetails[i].subJobValue,
							"occupationClassValue":$scope.Beneficiary.OccupationDetails[i].occupationClassValue,
							"industryValue":$scope.Beneficiary.OccupationDetails[i].industryValue,
							"natureofWork":$scope.Beneficiary.OccupationDetails[i].natureofWorkValue,
                            "natureofWorkValue":$scope.Beneficiary.OccupationDetails[i].natureofWorkValue
					};
                if(OccupationDetailsArr.id===3){
                    $scope.showSecondary=true;
                }
				//$scope.allowCodeUpdateBen = false;
				/* FNA Changes By LE Team Starts -Bug: 4180, 4193 */
		        //$scope.SaveTransBen = true;
		        /* FNA Changes By LE Team Ends -Bug: 4180, 4193 */
            OccupationBeneficiary.push(OccupationDetailsArr);
            }
          }            
        }
        $scope.Beneficiary.OccupationDetails[2]="";
        $scope.Beneficiary.OccupationDetails[3]="";
       
    }
    // Changes done by LETeam << Change ends here
	//over writing function to rename status  initial to draft
	$scope.saveInsured = function() {
		//if($scope.errorCount == 0 || $scope.errorCount == undefined){
           if(!$scope.validateGenderTitle()){
        $scope.FNAObject.FNA.saveCheck = true;
        $scope.FNAObject.FNA.saveCheckDependents = true;
        $scope.SaveTrans = true;
		$scope.allowCodeUpdate = false;
        /*for (var i = 0; i < $scope.FNABeneficiaries.length; i++) {           
            if ($scope.FNABeneficiaries[i] != undefined && $scope.FNABeneficiaries[i].classNameAttached != undefined && $scope.FNABeneficiaries[i].BasicDetails == undefined) {
               	$scope.FNABeneficiaries.splice(i, 1);
            }
        }*/
         if($scope.FNAMyself.BasicDetails.dob=="NaN-NaN-NaN" || angular.isUndefined($scope.FNAMyself.BasicDetails.dob) || $scope.FNAMyself.BasicDetails.dob==""){                
                $scope.FNAMyself.BasicDetails.dob="";
            }
        if($scope.Beneficiary.BasicDetails != undefined && $scope.Beneficiary.BasicDetails.dob != undefined && (angular.isUndefined($scope.Beneficiary.BasicDetails.dob) || $scope.Beneficiary.BasicDetails.dob=="NaN-NaN-NaN" || $scope.Beneficiary.BasicDetails.dob=="")){                
        $scope.Beneficiary.BasicDetails.dob="";
            }       
        if($scope.FNAMyself.BasicDetails.identityProof != undefined && $scope.FNAMyself.BasicDetails.identityProof=="CID") {
            $scope.FNAMyself.BasicDetails.IDcard = $scope.FNAMyself.BasicDetails.IDcard;
        } else if ($scope.FNAMyself.BasicDetails.identityProof != undefined && $scope.FNAMyself.BasicDetails.identityProof=="PP") {
               $scope.FNAMyself.BasicDetails.IDcard  = $scope.passportNum ;
                   } else if($scope.FNAMyself.BasicDetails.identityProof != undefined && $scope.FNAMyself.BasicDetails.identityProof=="DL") {
            $scope.FNAMyself.BasicDetails.IDcard  = $scope.driverLicenseNum;
        }else if($scope.FNAMyself.BasicDetails.identityProof != undefined && $scope.FNAMyself.BasicDetails.identityProof=="GID"){
                $scope.FNAMyself.BasicDetails.IDcard  = $scope.governmentIdNum;
           }

		/* FNA changes made by LE Team Start for Bug 4307 */
		         if($scope.FNABeneficiaries.length>0)  {

		        if($scope.Beneficiary.BasicDetails.identityProof != undefined && $scope.Beneficiary.BasicDetails.identityProof=="PP"){
					$scope.Beneficiary.BasicDetails.IDcard = $scope.passportNumBen;
				}
				else if($scope.Beneficiary.BasicDetails.identityProof != undefined && $scope.Beneficiary.BasicDetails.identityProof=="DL"){
					$scope.Beneficiary.BasicDetails.IDcard = $scope.driverLicenseNumBen;
				}
				else if($scope.Beneficiary.BasicDetails.identityProof != undefined && $scope.Beneficiary.BasicDetails.identityProof=="GID"){
					$scope.Beneficiary.BasicDetails.IDcard = $scope.governmentIdNumBen;
				}
			}
		/* FNA changes made by LE Team Start for Bug 4307 */
		$scope.getInsuredOccupation();
        $scope.FNAMyself.OccupationDetails=[];
        
        for(var i=0;i<OccupationInsured.length;i++){
            if(OccupationInsured[i].id!==undefined){
        		$scope.FNAMyself.OccupationDetails[i]=angular.copy(OccupationInsured[i]);
            } 
        }
        if($scope.FNAMyself.OccupationDetails[2]!==undefined){
        $scope.FNAMyself.OccupationDetails[2].slice();
        }
        if($scope.FNAMyself.OccupationDetails[3]!==undefined){
        $scope.FNAMyself.OccupationDetails[3].slice();
        }
        
        $scope.getBeneficiaryOccupation();
        $scope.Beneficiary.OccupationDetails=[];
        
        for(var i=0;i<OccupationBeneficiary.length;i++){
            if(OccupationBeneficiary[i].id!==undefined){
        $scope.Beneficiary.OccupationDetails[i]=angular.copy(OccupationBeneficiary[i]);
            }
        }
        if($scope.Beneficiary.OccupationDetails[2]!==undefined){
        $scope.Beneficiary.OccupationDetails[2].slice();
        }
        if($scope.Beneficiary.OccupationDetails[3]!==undefined){
        $scope.Beneficiary.OccupationDetails[3].slice();
        }
           
 		 /* Change by LETeam << ends here */
		$scope.InsuredTemp =  angular.copy($scope.FNAMyself);
		
		$scope.Beneficiary.isSaved = true;
		$scope.BeneficiaryTemp =  angular.copy($scope.Beneficiary);
		
		if($scope.memberIndex > -1)
			$scope.FNABeneficiaries[$scope.memberIndex] = angular.copy($scope.Beneficiary);

		$scope.InsuredTempForCancel = JSON.parse(JSON.stringify($scope.FNAMyself));
		$scope.updateErrorCount('aboutMeInsured');
		$scope.FNAMyself.isSaved = true;
		for(var i =0;i<$scope.FNABeneficiaries.length;i++)
		{
			if($scope.FNABeneficiaries[i])
				if($scope.FNABeneficiaries[i].memberIndex && $scope.FNABeneficiaries[i].memberIndex > 0)
					$scope.FNABeneficiaries[i].isSaved = true;
		}

		var translateEditDetails = translateMessages($translate, "fna.editDetails");
		$('#txtHeaderInsured').html(translateEditDetails);
		
		
		$scope.saveSelfDetails();
		UtilityService.disableProgressTab = false;

		globalService.setParty($scope.FNAMyself, "FNAMyself");
		globalService.setFNABeneficiaries($scope.FNABeneficiaries);
		var parties = globalService.getParties();
		$scope.FNAObject.FNA.parties = parties;		
		FnaVariables.setFnaModel($scope.FNAObject);
		$scope.status = "Draft";
        /* FNA Changes by LE Team Start (instead of 10, it will be 7 draggable circles, so changing it from 10 to 7) */
		for (var i = 1; i <= 7; i++) {
        /* FNA Changes by LE Team Start */
			$("#detailsBtn" + (i)).attr("disabled", false);
		}
		var obj = new GLI_FnaService.saveTransactions($scope, $rootScope,
				DataService, $translate, UtilityService, false);
		obj.save(function () {
			/* FNA Changes By LE Team Starts -Bug: 4180, 4193 */
			$scope.SaveTrans = false;
			/* FNA Changes By LE Team Ends -Bug: 4180, 4193 */
                });
        /* FNA Changes by LE Team Start */
        $rootScope.NotifyMessages(false, "Success",
				$translate);
        $scope.leadDetShowPopUpMsg = true;
        $scope.enterMandatory = translateMessages($translate, "fna.dependentSaveSuccess");
		/* FNA Changes by LE Team End */
		}else{
            $scope.leadDetShowPopUpMsg = false;
			$rootScope.showHideLoadingImage(false);
			/* FNA changes made by LE team for bug 4327 starts */
            if($scope.isInsuredClicked){
			$scope.lePopupCtrl.showWarning(
					translateMessages($translate,
					"lifeEngage"),
					translateMessages($translate,
					"fna.titleGenderMismatchInsured"),
					translateMessages($translate,
					"fna.ok"));
            }else{
                $scope.lePopupCtrl.showWarning(
					translateMessages($translate,
					"lifeEngage"),
					translateMessages($translate,
					"fna.titleGenderMismatchBeneficiary"),
					translateMessages($translate,
					"fna.ok"));
            }
			/* FNA changes made by LE team for bug 4327 ends */
		}
	  //  }
	};
    $scope.closePopup = function () {
            $scope.leadDetShowPopUpMsg = false;
        };
	
	//validate the occupation field and update the error count
	//changes made are:added the parameters dataInput-the value inputted,indicator-to check whteher its beneficairy or myself
	//before the occupation jobCode was set only for mySelf.
	//jobClass is added as its the model mapped in BI
	$scope.validateOccupation = function(value,dataInput,indicator) {
        $scope.dynamicErrorCount= 0;
        $scope.dynamicErrorMessages = [];
        occupationTaken = dataInput;
        if(occupationTaken != "")
        {
               _occupation = $scope.validateSmartSearch(dataInput,$scope.occupation);
               if(!_occupation)
               {
                     $scope.dynamicErrorCount= $scope.dynamicErrorCount+1;
                     var error = {};
                     error.message = translateMessages($translate, "lms.leadDetailsSectionoccupationRequiredValidationMessage");
                     error.key = "occupation";
                     $scope.dynamicErrorMessages.push(error);
                     $rootScope.updateErrorCount(value);
               }
               else
               {
            	   if(indicator=="Myself"){
            		   $scope.FNAMyself.OccupationDetails.jobCode=_occupation.jobCode;
            		   $scope.FNAMyself.OccupationDetails.jobClass=_occupation.jobCode;
            	   }else if(indicator=="Beneficary"){
            		   $scope.Beneficiary.OccupationDetails.jobCode=_occupation.jobCode;
            		   $scope.Beneficiary.OccupationDetails.jobClass=_occupation.jobCode;
            	   }
                    // $scope.FNAMyself.OccupationDetails.jobCode=_occupation.jobCode;
                     $rootScope.updateErrorCount(value);
               }     
        }
        else
        {
        	$rootScope.updateErrorCount(value);
        }
	};
 
	$scope.validateSmartSearch= function(value, list)
	 {
	        if((list != undefined && value!=undefined) && (list != undefined && value!=""))
	        {
	        	for(var i = 0 ; i < list.length ; i++)
	               {
	        			if(value.toLowerCase() == list[i].occupationName.toLowerCase())
	                     	{
	        					return list[i];
	                     	}
	               }      
	        }
	        return false;
	 }
	$scope.operationError = function() {
		 $rootScope.showHideLoadingImage(false, "");
	}
    
    $scope.browsePicture = function() {
    var docCount = 0;
		$scope.isInsured = true;
		var currAgentId = $rootScope.agentId == null ? $rootScope.username : $rootScope.agentId;
		var currentDoc;
		var document;
		if ($scope.isInsured) {
			currentDoc = $scope.FNAMyself.BasicDetails.photo;
		} else if ( typeof $scope.Beneficiary.BasicDetails != "undefined") {
			currentDoc = $scope.Beneficiary.BasicDetails.photo;
		}
		if ( typeof currentDoc == "undefined" || currentDoc == "") {
			document = $scope.mapDocScopeToPersistence({});
		} else {
			document = $scope.mapDocScopeToPersistence(currentDoc);
		}
		var index = 0;
	    var options = {
			type : "FNA"
		};
		DocumentService.browseFile(document, currAgentId, $scope.onFileSelectSuccess, $scope.operationError);
		$rootScope.showHideLoadingImage(false, "Uploading", $translate);        
    }
    
    $scope.clickPicture = function() {
    $rootScope.showHideLoadingImage(true, "Uploading", $translate);
		var docCount = 0;
		$scope.isInsured = true;
		var currAgentId = $rootScope.agentId == null ? $rootScope.username : $rootScope.agentId;
		var currentDoc;
		var document;
		if ($scope.isInsured) {
			currentDoc = $scope.FNAMyself.BasicDetails.photo;
		} else if ( typeof $scope.Beneficiary.BasicDetails != "undefined") {
			currentDoc = $scope.Beneficiary.BasicDetails.photo;
		}
		if ( typeof currentDoc == "undefined" || currentDoc == "") {
			document = $scope.mapDocScopeToPersistence({});
		} else {
			document = $scope.mapDocScopeToPersistence(currentDoc);
		}
		var index = 0;
	    var options = {
			type : "FNA"
		};
		DocumentService.captureFile(document, index, currAgentId, $scope.onFileSelectSuccess, $scope.operationError, options);
		$rootScope.showHideLoadingImage(false, "Uploading", $translate);
    }
    
	// Function called on Image btn click
	$scope.captureFile = function(isInsured) {
        if(rootConfig.isDeviceMobile && !rootConfig.isOfflineDesktop && isInsured) {
        $scope.lePopupCtrl.showWarning(
						translateMessages($translate,
								"lifeEngage"),
						translateMessages($translate,
								"uploadPicture"),  
						translateMessages($translate,
								"fna.browsePicture"),$scope.browsePicture,translateMessages($translate,
								"fna.clickPicture"),$scope.clickPicture);
        } 
        else {
        $rootScope.showHideLoadingImage(true, "Uploading", $translate);
		var docCount = 0;
		$scope.isInsured = isInsured;
		var currAgentId = $rootScope.agentId == null ? $rootScope.username : $rootScope.agentId;
		var currentDoc;
		var document;
		if (isInsured) {
			currentDoc = $scope.FNAMyself.BasicDetails.photo;
		} else if ( typeof $scope.Beneficiary.BasicDetails != "undefined") {
			currentDoc = $scope.Beneficiary.BasicDetails.photo;
		}
		if ( typeof currentDoc == "undefined" || currentDoc == "") {
			document = $scope.mapDocScopeToPersistence({});
		} else {
			document = $scope.mapDocScopeToPersistence(currentDoc);
		}
		var index = 0;
	    var options = {
			type : "FNA"
		};
		DocumentService.captureFile(document, index, currAgentId, $scope.onFileSelectSuccess, $scope.operationError, options);
		$rootScope.showHideLoadingImage(false, "Uploading", $translate);
        }		
	};

	// Function called on browse buttton  click
	$scope.browseFile = function(isImage, isPhoto, fileObj, isInsured) {
			if(fileObj.size>$scope.fnaImageUploadSize){
				$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
												 translateMessages($translate, "invalidSizeMesageFNA"),
												 translateMessages($translate, "fna.ok"),
					function() {
						$rootScope.selectedPage = "MyFamilyDetails";
						//$scope.selectedpage = true;
				});
			}
			else{
				$rootScope.showHideLoadingImage(true, "Uploading", $translate);
				$scope.isImage = isImage;
				$scope.isPhoto = isPhoto;
				$scope.isSignature = false;
				$scope.isInsured = isInsured;
				var docCount = 0;
				var currAgentId = $rootScope.agentId == null ? $rootScope.username : $rootScope.agentId;
				if ((rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
					var currentDoc = {};
					currentDoc = $scope.LifeEngageProduct.Upload.Photograph[0];
					var document = $scope.mapDocScopeToPersistence(currentDoc);
				} else {
					var document = $scope.mapDocScopeToPersistence(fileObj);
				}
				if (!document.documentName) {
					var currAgentId = $rootScope.agentId == null ? $rootScope.username : $rootScope.agentId;
					var d = new Date();
					var n = d.getTime();
					var name = "document";
					var newFileName = "Agent1" + n + "_" + name;
					document.documentName = newFileName;
				}
				DocumentService.browseFile(document, currAgentId, $scope.onFileSelectSuccess, $scope.operationError);
			}
	};

	//over writing function to rename status  initial to draft
	$scope.onFileSelectSuccess = function(document) {
		document.documentStatus = "Initial";
		globalService.setParty($scope.FNAMyself, "FNAMyself");
		globalService.setFNABeneficiaries($scope.FNABeneficiaries);
		var parties = globalService.getParties();
		$scope.FNAObject.FNA.parties = parties;
		var obj = new GLI_FnaService.saveTransactions($scope, $rootScope,DataService, $translate, UtilityService, false);
		obj.save(function(data) {
					if (document.parentId == null) {
						$rootScope.transactionId = data;
						document.parentId = $rootScope.transactionId;
						$scope.status = "Draft";
					}
					if (document.documentType == "Photo") {
						if ($scope.isInsuredClicked) {
							var fnaInsuredPhoto = translateMessages($translate, "fna.fnaInsuredPhoto");
							$('#fnaInsuredPhoto').html(fnaInsuredPhoto);
							if (eval(rootConfig.isOfflineDesktop)) {
								$scope.FNAMyself.BasicDetails.photo = document.documentPath
										.replace("data:image/jpeg;base64,", "");
								$scope.myselfPhotoValidation = "false";
							} else {
								$scope.FNAMyself.BasicDetails.photo = document.base64string;
								$scope.myselfPhotoValidation = "false";
							}
							if (!eval(rootConfig.isOfflineDesktop)
									&& !eval(rootConfig.isDeviceMobile)) {
								$scope.FNAMyself.BasicDetails.photo = document.documentPath
								.replace("data:image/jpeg;base64,", "");
								$scope.myselfPhotoValidation = "false";
							}
						} else if ($scope.isBenefeciaryClicked) {
							var fnaInsuredPhoto = translateMessages($translate, "fna.fnaInsuredPhoto");
							$('#fnaBeneficiaryPhoto').html(fnaInsuredPhoto);
							
							if (typeof $scope.Beneficiary.BasicDetails == "undefined") {
								$scope.Beneficiary.BasicDetails = {};
							}
							if (eval(rootConfig.isOfflineDesktop)) {
								$scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.photo = document.documentPath
										.replace("data:image/jpeg;base64,", "");
								$scope.defaultImageForMembers[$scope.memberIndex].photo = document.documentPath
										.replace("data:image/jpeg;base64,", "");
								$scope.benificiaryPhotoValidation = "false";
							} else {
								$scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.photo = document.base64string;
								$scope.defaultImageForMembers[$scope.memberIndex].photo = document.base64string;
                                if (!eval(rootConfig.isDeviceMobile))
                                {
                                    $scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.photo = document.documentPath
                                            .replace("data:image/jpeg;base64,", "");
                                    $scope.defaultImageForMembers[$scope.memberIndex].photo = document.documentPath
                                            .replace("data:image/jpeg;base64,", "");
                                }
                                    $scope.benificiaryPhotoValidation = "false";

							}
						}
						$timeout(function(){
                             $scope.updateErrorCount($scope.isInsured?'aboutMeInsured':'aboutMeBeneficiary');
                        });
						$rootScope.showHideLoadingImage(false, "");
					}
				});
		$rootScope.showHideLoadingImage(false, "");
	};
	$scope.addEditDetails =  function() {
		/* FNA Changes By LE Team Start -Bug: 4180, 4193 */
		 $scope.showSecondary = false;
		 $scope.allowCodeUpdateBen = false;		
		 /* FNA Changes By LE Team Ends -Bug: 4180, 4193 */
		$scope.FNAObject.FNA.saveCheck = true;
        $scope.FNAObject.FNA.saveCheckDependents = false;
        $scope.isBenefeciaryClicked = true;
        $scope.isInsuredClicked = false;		
		var index = $scope.index;
		var event = $scope.event;
		if($(event.currentTarget).hasClass('member')){
		}
		else{
		
		if($scope.memberIndex != index && $scope.memberIndex != -1)
		{
			$scope.cancelBeneficiaryOnSwitching();
		}
		else if ($scope.memberIndex == -1)
			$scope.cancelInsured();	
		$scope.contactNo = "";
		//$scope.FNABeneficiaries = globalService.getFNABeneficiaries();
		if ($scope.FNABeneficiaries[index].BasicDetails == undefined ) {
			$scope.passportNumBen= null;
			$scope.driverLicenseNumBen= null;
			 $scope.governmentIdNumBen = null;
		}
		  if ($scope.FNABeneficiaries[index].BasicDetails != undefined && $scope.FNABeneficiaries[index].BasicDetails.identityProof != undefined && $scope.FNABeneficiaries[index].BasicDetails.identityProof=="PP") {
               $scope.passportNumBen = $$scope.FNABeneficiaries[index].BasicDetails.IDcard;
                   } else if($scope.FNABeneficiaries[index].BasicDetails != undefined && $scope.FNABeneficiaries[index].BasicDetails.identityProof != undefined && $scope.FNABeneficiaries[index].BasicDetails.identityProof=="DL") {
            $scope.driverLicenseNumBen = $scope.FNABeneficiaries[index].BasicDetails.IDcard;
        }else if($scope.FNABeneficiaries[index].BasicDetails != undefined && $scope.FNABeneficiaries[index].BasicDetails.identityProof != undefined && $scope.FNABeneficiaries[index].BasicDetails.identityProof=="GID"){
                $scope.governmentIdNumBen = $scope.FNABeneficiaries[index].BasicDetails.IDcard;
           }			
		$scope.memberClickEvent = event;
		if(typeof event != "undefined" && event != null && event != "")
		{
			thisObjDeletion = event.target.parentNode.parentNode;
			var thisObjClassName = String(thisObjDeletion.className);
			var clickedNode = event.target.nodeName;
			var parentDiveNodes = $(clickedNode).parents('.owl-item');
			$(parentDiveNodes).removeClass("active");
			$(thisObjDeletion).closest('.owl-item').addClass('active');
			$scope.addEditFamilyDetails(event);
		}
		$scope.showDeleteButton = true;
		$scope.memberIndex = index;
		myFamilyDetailsUiJsonFile = FnaVariables.uiJsonFile.fnaBenificiaryFile;
		LEDynamicUI.paintUI(rootConfig.template, myFamilyDetailsUiJsonFile, "aboutMeInsured", "#aboutMeInsured", true, function() {
			$scope.memberCallback();
		}, $scope, $compile);
		
		
		//UtilityService.disableProgressTab = true;
		//GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
		if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
			$scope.refresh();
		}
		$scope.dynamicErrorCount = 0;
		$scope.dynamicErrorMessages = [];
		_errors = [];
		$scope.BeneficiaryTemp = angular.copy($scope.FNABeneficiaries[index]);
		$scope.Beneficiary.showMore = false;
            
		if ($scope.FNABeneficiaries[index] != "null" && typeof $scope.FNABeneficiaries[index] != "undefined") {
			$scope.Beneficiary = $scope.FNABeneficiaries[index];
			$scope.edit = true;
			$scope.editIndex = index;
			$scope.benificiaryPhotoValidation = "true";
			
			//for check the image in edit mode
			var fnaInsuredPhoto = "";
			if ($scope.FNABeneficiaries[index].BasicDetails && $scope.FNABeneficiaries[index].BasicDetails.photo != "") {
				$scope.benificiaryPhotoValidation = "false";
				fnaInsuredPhoto = translateMessages($translate, "fna.fnaInsuredPhoto");	
			}
			else
			{
				fnaInsuredPhoto = translateMessages($translate, "fna.fnaUploadPhoto");				
			}
			
			$('#fnaBeneficiaryPhoto').html(fnaInsuredPhoto);
			$scope.refresh();
			/* FNA Changes by LE Team Start (instead of 10, it will be 7 draggable circles, so changing it from 10 to 7) */
			for (var i = 1; i <= 7; i++) {
            /* FNA Changes by LE Team Start */
				$("#detailsBtn" + (i)).attr("disabled", "disabled");
				$("#selfdetailsBtn").attr("disabled", "disabled");
			}
			$("#detailsBtn" + (index + 1)).attr("disabled", false);			
		} else {			
			$scope.Beneficiary.BasicDetails = {
				"firstName" : "",
				"lastName" : "",
				"nationality" : "",
				"dob" : "",
				"gender":"",
				"age":"",
				"photo" : "",
				"identityProof" : "",
                "relationWithPrimaryInsured" : ""
			};
			$scope.Beneficiary.OccupationDetails = [];
		$scope.Beneficiary.OccupationDetails[0] = "";
		$scope.Beneficiary.OccupationDetails[1] = "";
		$scope.Beneficiary.OccupationDetails[2] = "";
		$scope.Beneficiary.OccupationDetails[3] = "";
			
			$scope.Beneficiary.ContactDetails = {
					"homeNumber1" : "",
					"mobileNumber1":"",
					"emailId" : "",
					"currentAddress" : {
							"address" : "",
							"addressLine1" : "",
							"city" : "",
							"state": "",
							"zipCode" : "",
							"addressLine2" : ""
						},
					"birthAddress" : {
						"country":""
					}
				};
		}
		
		if(typeof $scope.Beneficiary.BasicDetails == "undefined") {
			$scope.FNAObject.FNA.checkedState = "No";
			$scope.Beneficiary.BasicDetails = {
					"firstName" : "",
					"lastName" : "",
					"nationality" : "",
					"dob" : "",
					"gender":"",
					"age":"",
					"photo" : "",
					"identityProof" : "",
                    "relationWithPrimaryInsured" : ""
				};
			$scope.Beneficiary.OccupationDetails = [];
		$scope.Beneficiary.OccupationDetails[0] = "";
		$scope.Beneficiary.OccupationDetails[1] = "";
		$scope.Beneficiary.OccupationDetails[2] = "";
		$scope.Beneficiary.OccupationDetails[3] = "";
		
			$scope.Beneficiary.ContactDetails = {
					"homeNumber1" : "",
					"mobileNumber1":"",
					"emailId" : "",
					"currentAddress" : {
							"address" : "",
							"addressLine1" : "",
							"city" : "",
							"state": "",
							"zipCode" : "",
							"addressLine2" : ""
						},
					"birthAddress" : {
							"country":""
						}
				};		
		} else if ($scope.Beneficiary.CurrentAddress.address == "Yes"){
			$scope.FNAObject.FNA.checkedState = "Yes";
			$scope.Beneficiary.copyMySelfAddress = true;
		}
		else {
			$scope.FNAObject.FNA.checkedState = "No";
			$scope.Beneficiary.copyMySelfAddress = false;
		}
		if($scope.Beneficiary.CurrentAddress && $scope.Beneficiary.CurrentAddress.address && $scope.Beneficiary.CurrentAddress.address == "Yes")
			$scope.Beneficiary.copyMySelfAddress = true;
		else
			$scope.Beneficiary.copyMySelfAddress = false;
		$scope.Beneficiary.memberIndex = index;
		
		var beneficiaryTxt = "detailsBtn" +( index + 1);
            $scope.Beneficiary.BasicDetails.relationWithPrimaryInsured = translateMessages($translate,("fna."+$scope.FNABeneficiaries[index].classNameAttached));
		if( $scope.FNABeneficiaries[index].BasicDetails)
		{
			var beneficiaryFName = $scope.FNABeneficiaries[index].BasicDetails.firstName;
			var beneficiaryLName = $scope.FNABeneficiaries[index].BasicDetails.lastName;
			
			if((typeof beneficiaryFName =='undefined' || beneficiaryFName == "null"))
				beneficiaryFName = "";
			if((typeof beneficiaryLName =='undefined' || beneficiaryLName == "null"))
				beneficiaryLName = "";
			if($scope.FNABeneficiaries[index].BasicDetails.firstName)
			if($scope.FNABeneficiaries[index].BasicDetails.firstName.length>25)
			{
			$('#'+beneficiaryTxt).addClass('fontSize13');
			}
			if((beneficiaryFName == "" && beneficiaryLName == ""))
			{					
				$('#'+beneficiaryTxt).text(translateMessages($translate, "fna."+$scope.FNABeneficiaries[index].classNameAttached));
			}
			else
			{
				var beneficiaryFullname = beneficiaryFName;
				$("#detailsBtn" + (index + 1)).text(beneficiaryFullname);
			}		
			
		}
		else
		{					
			$('#'+beneficiaryTxt).text(translateMessages($translate, "fna."+$scope.FNABeneficiaries[index].classNameAttached));
		}
		//to set the gender
        /* FNA Changes By LE Team Start (Bug: 3777 & 3867) */
		for (var i = 0; i < $scope.familyMembers.length; i++) {
            if ($scope.FNABeneficiaries[index].classNameAttached.toLowerCase() == 'spouse') {
                if ($scope.familyMembers[i].memberName.toLowerCase() == 'husband') {
                    $scope.Beneficiary.BasicDetails.gender='Female';
                    break;
                } else if ($scope.familyMembers[i].memberName.toLowerCase() == 'wife') {
                    $scope.Beneficiary.BasicDetails.gender='Male';
                    break;
                }
            } else {
                if ($scope.familyMembers[i].memberName.toLowerCase() == $scope.FNABeneficiaries[index].classNameAttached.toLowerCase()) {
				    $scope.Beneficiary.BasicDetails.gender=$scope.familyMembers[i].gender;
				    break;
			     }
            }
		}
		/* FNA Changes By LE Team End */

		var translateEditDetails = "";
		
		if(typeof $scope.FNABeneficiaries[index].isSaved == 'undefined')
		{
			if($scope.FNABeneficiaries[index])
			$scope.FNABeneficiaries[index].isSaved = false;
		}
			
		$scope.addContactPrefix($scope.contactNo,'beneficiary');
		if(!$scope.FNABeneficiaries[index].isSaved)
			translateEditDetails = translateMessages($translate, "fna.addDetails");
		else
			translateEditDetails = translateMessages($translate, "fna.editDetails");
		$('#txtHeaderInsured').html(translateEditDetails);
		

		$scope.refresh();
		
		}
		//GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
		
	};
	//Function called on click of Add/edit details button of Member
	$scope.addEdit = function(index, event) {
		$scope.index = index;
		$scope.event = event;		 
		if($scope.FNAObject.FNA.saveCheck == false){
			
			 $scope.lePopupCtrl.showWarning(
						translateMessages($translate,
								"lifeEngage"),
						translateMessages($translate,
								"saveBeneficiaryData"),  
						translateMessages($translate,
								"fna.navok"),$scope.okPressed,translateMessages($translate,
								"general.navproceed"),$scope.addEditDetails);
		}else{
			$scope.addEditDetails();
		}
		
	};
    /* FNA changes by LE Team Start */
    $scope.clearField=function(val){                    
                   if(val=='id'){
                        $scope.FNAMyself.BasicDetails.IDcard="";
                    }
    };
    $scope.clearFieldBen=function(val){                    
                   if(val=='id'){
                        $scope.Beneficiary.BasicDetails.IDcard="";
                    }
    };
   $scope.validateFields = function (data) {
        $scope.dynamicErrorMessages = [];
        $scope.dynamicErrorCount = 0;
        var _errors = [];
        var _errorCount = 0;    
       if($scope.FNAMyself.BasicDetails.identityProof=="PP"){
			$scope.FNAMyself.BasicDetails.IDcard = $scope.passportNum;
		}
		else if($scope.FNAMyself.BasicDetails.identityProof=="DL"){
			$scope.FNAMyself.BasicDetails.IDcard =$scope.driverLicenseNum;
		}
		else if($scope.FNAMyself.BasicDetails.identityProof=="GID"){
			$scope.FNAMyself.BasicDetails.IDcard= $scope.governmentIdNum;
		}
		if($scope.FNAMyself.BasicDetails.IDcard && $scope.FNAMyself.BasicDetails.identityProof=='CID' && $scope.validateCitizenId()){
                    $scope.dynamicErrorCount= $scope.dynamicErrorCount+1;
                     var error = {};
                     error.message = translateMessages($translate, "lms.invalidCitizenId");
                     error.key = "IDcard";
                     $scope.dynamicErrorMessages.push(error);
                     $rootScope.updateErrorCount(data);
		}    
       else{
           $scope.dynamicErrorCount = 0;
           $scope.dynamicErrorMessages = _errors;
            $rootScope.updateErrorCount(data);
       }
}
	/* FNA changes made by LE Team for Bug 4307 */
   $scope.validateFieldsBen = function (data) {
        $scope.dynamicErrorMessages = [];
        $scope.dynamicErrorCount = 0;
        var _errors = [];
        var _errorCount = 0;    
       if($scope.Beneficiary.BasicDetails.identityProof=="PP"){
			$scope.Beneficiary.BasicDetails.IDcard = $scope.passportNumBen;
		}
		else if($scope.Beneficiary.BasicDetails.identityProof=="DL"){
			$scope.Beneficiary.BasicDetails.IDcard =$scope.driverLicenseNumBen;
		}
		else if($scope.Beneficiary.BasicDetails.identityProof=="GID"){
			$scope.Beneficiary.BasicDetails.IDcard= $scope.governmentIdNumBen;
		}
		if($scope.Beneficiary.BasicDetails.IDcard && $scope.Beneficiary.BasicDetails.identityProof=='CID' && $scope.validateCitizenIdBen()){
                    $scope.dynamicErrorCount= $scope.dynamicErrorCount+1;
                     var error = {};
                     error.message = translateMessages($translate, "lms.invalidCitizenId");
                     error.key = "IDcard";
                     $scope.dynamicErrorMessages.push(error);
                     $rootScope.updateErrorCount(data);
		}
       else{
           $scope.dynamicErrorCount = 0;
           $scope.dynamicErrorMessages = _errors;
            $rootScope.updateErrorCount(data);
       }
}
/* FNA changes made by LE Team for Bug 4307 */
$scope.validateCitizenId=function(){
		var idCardSubstr=$scope.FNAMyself.BasicDetails.IDcard.substring(0,12);
		var count=0;
		var len=$scope.FNAMyself.BasicDetails.IDcard.length+1;
		var rem=0;
		var quotient=0;
		var min=0;
		var nDigit=0;
                    
        for(var i=0;i<idCardSubstr.length;i++)
         {
			len=len-1;                            
			count=count+parseInt($scope.FNAMyself.BasicDetails.IDcard.substring(i,i+1),10)*len;
         }
                    
         quotient = Math.floor(count/11);
         rem=count%11;
         min=11-rem;
         nDigit=parseInt($scope.FNAMyself.BasicDetails.IDcard.substring(12,13),10);
                
         if(min>9){
             min=min%10;
         }
         if(nDigit!==min)
         {
            return true;
		 }
    };
    /* FNA changes by LE Team End */
	/* FNA Changes by LE Team Start */
	$scope.onFieldChange = function () {
        $rootScope.updateErrorCount($scope.viewToBeCustomized);        
    }
	/* FNA Changes by LE Team End */
	//Calculating age in years, month , days 
		$scope.ageCalculation = function(currDate, dobDate) {
			/* FNA Changes by LE Team Start */
			
			/* var y1 = date1.getFullYear(), m1 = date1.getMonth(), d1 = date1.getDate(),
			// y2 = date2.getFullYear(), m2 = date2.getMonth(), d2 = date2.getDate();
			// if (m1 < m2) {
				// y1--;
				// m1 += 12;
			// }
			 return [y1 - y2, m1 - m2, d1 - d2]; */
			 
			date1 = new Date(currDate);
			date2 = new Date(dobDate);
			var y1 = date1.getFullYear(),
            m1 = date1
            .getMonth(),
            d1 = date1.getDate(),
            y2 = date2
            .getFullYear(),
            m2 = date2.getMonth(),
            d2 = date2
            .getDate();

        var nextDOB;
        if (m1 < m2) {
            nextDOB = new Date(date2.setYear(date1
                .getFullYear()));
        } else if ((m1 == m2) && (d1 < d2)) {
            nextDOB = new Date(date2.setYear(date1
                .getFullYear()));
        } else {
            nextDOB = new Date(date2.setYear(date1
                .getFullYear() + 1));
        }
        var marginDate = new Date(date1.setMonth(date1
            .getMonth() + 6));

        if (m1 < m2) {
            y1--;
            m1 += 12;
        }

        if (nextDOB.getTime() > marginDate.getTime()) {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() -
                birthDate.getFullYear();
            var mnth = today.getMonth() -
                birthDate.getMonth();
            if (mnth < 0 ||
                (mnth === 0 && today.getDate() < birthDate
                    .getDate())) {
                ageVal--;
            }
            return [ageVal, "false"];
        } else {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() -
                birthDate.getFullYear();
            var mnth = today.getMonth() -
                birthDate.getMonth();
            if (mnth < 0 ||
                (mnth === 0 && today.getDate() < birthDate
                    .getDate())) {
                ageVal--;
            }
            return [ageVal, "true"];
        }
		/* FNA Changes by LE Team Start */
		};
//Function called on click of Add/edit details button of Insured
	$scope.nextPage = function(index, event) {
        $scope.index = index;
		$scope.event = event;
      if($scope.FNAObject != undefined && $scope.FNAObject.FNA != undefined && $scope.FNAObject.FNA.saveCheck == false && $scope.FNAObject.FNA.saveCheckDependents != undefined &&  $scope.FNAObject.FNA.saveCheckDependents == false){
         
			 $scope.lePopupCtrl.showWarning(
						translateMessages($translate,
								"lifeEngage"),
						translateMessages($translate,
								"saveBeneficiaryData"),  
						translateMessages($translate,
								"fna.navok"),$scope.okPressed,translateMessages($translate,
								"general.navproceed"),$scope.nextPageFn);
		} else {
            $scope.nextPageFn(); 
        }	  
	};
	//over writing function to rename status  initial to draft
	$scope.nextPageFn = function() {
		$scope.FNAObject.FNA.saveCheck = true;
		$scope.myselfMandatoryFieldsAdded = true;
	  	$scope.FNAObject.FNA.saveCheckDependents = true;	
		$scope.myselfMandatoryFields = FnaVariables.myselfMandatoryFields;
		$scope.choosepartyMandatoryFields = FnaVariables.choosepartyMandatoryFields;
		
	   var mandatoryFieldSize = $scope.myselfMandatoryFields.length;
	   for(var i=0;i<mandatoryFieldSize;i++)
	   {
		  /* 
		  if($scope.myselfMandatoryFields[i] == "BasicDetails.photo")
		   {
			   if($scope.FNAMyself.BasicDetails.photo == FnaVariables.myselfPhoto)
				{
				   $scope.myselfMandatoryFieldsAdded = false;
				   break;
				}   
		   }
		   else */
		   if( typeof eval('$scope.FNAMyself.'+$scope.myselfMandatoryFields[i]) == "undefined" || eval('$scope.FNAMyself.'+$scope.myselfMandatoryFields[i]) == "" || eval('$scope.FNAMyself.'+$scope.myselfMandatoryFields[i]) == null)
		   {
			   $scope.myselfMandatoryFieldsAdded = false;
			   break;
		   }
	   }
	   if($scope.myselfMandatoryFieldsAdded)
	   {
		   $scope.choosePartFieldsAdded = true;
		   FnaVariables.choosePartyStatus = true;
           
		   if(typeof FnaVariables.choosePartyStatus !="undefined" && FnaVariables.choosePartyStatus == true)
		   {
			   for(var i =0;i<$scope.FNABeneficiaries.length;i++)
			   {
				   if($scope.FNABeneficiaries[i])
				   {
					   
					   //
					   if($scope.FNABeneficiaries[i].BasicDetails)
					   {
						   var choosePartySize = $scope.choosepartyMandatoryFields.length;
						   for(var j=0;j<choosePartySize;j++)
						   {
							   if(typeof eval('$scope.FNABeneficiaries[i].'+$scope.choosepartyMandatoryFields[j]) == "undefined" || eval('$scope.FNABeneficiaries[i].'+$scope.choosepartyMandatoryFields[j]) == "" || eval('$scope.FNABeneficiaries[i].'+$scope.choosepartyMandatoryFields[j]) == null)
							   {
								   $scope.choosePartFieldsAdded = false;
								   break;
							   }
						   }
					   }
					   else
					   {
						   $scope.choosePartFieldsAdded = false;
						   break;
					   }
					   
				   }
			   }
			  
		   }
		   
		  if($scope.choosePartFieldsAdded)
		  {
			FnaVariables.choosePartyStatus = false;
			  
			$scope.FNAMyself.isSaved = true;
			for(var i =0;i<$scope.FNABeneficiaries.length;i++)
			{
				if($scope.FNABeneficiaries[i])
					if($scope.FNABeneficiaries[i].memberIndex && $scope.FNABeneficiaries[i].memberIndex > 0)
						$scope.FNABeneficiaries[i].isSaved = true;
			}
			
			
		/* FNA changes by LE Team End */	
			$scope.InsuredTemp = angular.copy($scope.FNAMyself);
			if($scope.InsuredTemp.BasicDetails.photo != FnaVariables.myselfPhoto){
				$scope.myselfPhotoValidation = "false";
				$scope.refresh();
			}
			$rootScope.updateErrorCount('aboutMeInsured');
			$scope.errorCount = $scope.errorCount - $scope.dynamicErrorCount;
			$scope.disableTab();
			if ($scope.errorCount == 0) {

				for (var i = 0; i < FnaVariables.pages.length; i++) {
					if (FnaVariables.pages[i].url == "/setgoal") {
						$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
						break;
					}
				}

				/* FNA changes made by LE Team Start for Bug 4307 */
				$scope.getInsuredOccupation();
		        $scope.FNAMyself.OccupationDetails=[];
		        
		        for(var i=0;i<OccupationInsured.length;i++){
		            if(OccupationInsured[i].id!==undefined){
		        		$scope.FNAMyself.OccupationDetails[i]=angular.copy(OccupationInsured[i]);
		            } 
		        }
		        if($scope.FNAMyself.OccupationDetails[2]!==undefined){
		        $scope.FNAMyself.OccupationDetails[2].slice();
		        }
		        if($scope.FNAMyself.OccupationDetails[3]!==undefined){
		        $scope.FNAMyself.OccupationDetails[3].slice();
		        }
		        
		      	 /* Change by LETeam << ends here */


				globalService.setParty($scope.FNAMyself, "FNAMyself");
				//To make the outline of beneficiary when image is not added
				for(var i=0 ;i< $scope.FNABeneficiaries.length ; i++){
					if($scope.FNABeneficiaries[i]){
						if(typeof $scope.FNABeneficiaries[i].BasicDetails == "undefined"){
						$scope.FNABeneficiaries[i].BasicDetails = {
								"firstName" : "",
								"lastName" : "",
								"nationality" : "",
								"dob" : "",
								"gender":"",
								"age":"",
								"photo" : "",
								"identityProof" : ""
							};
						$scope.FNABeneficiaries[i].OccupationDetails = {
								"description" : "",
								"occupationCategory": "",
								"occupationCategoryValue": "",
								"descriptionOthers" : "",
								"jobCode": "",
								"jobClass":"",
							};
						$scope.FNABeneficiaries[i].ContactDetails = {
								"homeNumber1" : "",
								"mobileNumber1":"",
								"emailId" : "",
								"currentAddress" : {
										"address" : "",
										"addressLine1" : "",
										"city" : "",
										"state": "",
										"zipCode" : "",
										"addressLine2" : "",
										"ward": ""
									},
								"birthAddress" : {
										"country":""
									}
							};	
					}
					}
					
				}
				globalService.setFNABeneficiaries($scope.FNABeneficiaries);
				var parties = globalService.getParties();
				$scope.FNAObject.FNA.parties = parties;

				FnaVariables.setFnaModel($scope.FNAObject);
				$scope.status = "Draft";
				var obj = new GLI_FnaService.saveTransactions($scope, $rootScope,
						DataService, $translate, UtilityService, false);
				obj.save(function() {
					$location.path('/setgoal');
				});
			} else {

				$scope.popup_setpref_container = true;
			}
	     }
		 else
		 {
			   $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "fna.dependentValidationMessageChng"), translateMessages($translate, "fna.ok"));
		 }
	   }
	   else
	   {
		   $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "fna.selectionError"), translateMessages($translate, "fna.ok"));
	   }


	};
    // Changes done by LETeam << Change starts here
   /* $scope.populateIndustryLookUp = function () {
        $scope.populateIndependentLookupDateInScope('INDUSTRY', 'industry', '', false, 'industry', currentTab, function () {
            $scope.populateOccupationLookUp();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateOccupationLookUp = function () {
        $scope.populateIndependentLookupDateInScope('OCCUPATION', 'occupation', '', false, 'occupation', currentTab, function () {
            $scope.populateOccupationListLookUp();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateOccupationListLookUp = function () {
        $scope.populateIndependentLookupDateInScope('JOB', 'job', '', false, 'job', currentTab, function () {
            $scope.populateOccupationClassLookUp();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateOccupationClassLookUp = function () {
        $scope.populateIndependentLookupDateInScope('SUBJOB', 'subjob', '', false, 'subjob', currentTab, function () {
            $scope.populateRelationshipLookUp();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateRelationshipLookUp = function () {
        $scope.populateIndependentLookupDateInScope('RELATIONSHIP', 'relationship', '', false, 'relationship', currentTab, function () {
            $scope.populateLinearIndustryLookUp();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateLinearIndustryLookUp = function () {
        $scope.populateIndependentLookupDateInScope('INDUSTRY_LINEAR', 'industryLinear', '', false, 'industryLinear', currentTab, function () {
            $scope.populateTitleGenderLookUp();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateTitleGenderLookUp = function () {
        $scope.populateIndependentLookupDateInScope('TITLE_GENDER', 'titleGenderRelationship', '', false, 'titleGenderRelationship', currentTab, function () {
            $scope.populateTitleLookUp();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateTitleLookUp = function () {
        $scope.populateIndependentLookupDateInScope('TITLE', 'title', '', false, 'title', currentTab, function () {
            $scope.populateNatureOfWorkLinearLookUp();
            $scope.refresh();
        }, function () {});
    }
    $scope.populateNatureOfWorkLinearLookUp = function () {
        $scope.populateIndependentLookupDateInScope('NATUREOFWORK_LINEAR', 'natureOfWorkLinear', '', false, 'natureOfWorkLinear', currentTab, function () {
            $scope.populateNatureOfWorkOccupationLinearLookUp();
            $scope.refresh();
        }, function () {});
    }
    $scope.populateNatureOfWorkOccupationLinearLookUp = function () {
        $scope.populateIndependentLookupDateInScope('NATUREOFWORKLINEAR_OCODE', 'natureOfWorkOccupationLinear', '', false, 'natureOfWorkOccupationLinear', currentTab, function () {
            $scope.populateNatureOfWorkLookUp();
            $scope.refresh();
        }, function () {});
    }
    $scope.populateNatureOfWorkLookUp = function() {
        $scope.populateIndependentLookupDateInScope('NATUREOFWORK', 'natureOfWork', '', false, 'natureOfWork', currentTab, function() {
            
            $scope.refresh();
        }, function () {});
    }

	
	$scope.populateIndustryLookUpBen = function () {
        $scope.populateIndependentLookupDateInScopeBen('INDUSTRY', 'industry', '', false, 'industry', currentTab, function () {
            $scope.populateOccupationLookUpBen();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateOccupationLookUpBen = function () {
        $scope.populateIndependentLookupDateInScopeBen('OCCUPATION', 'occupation', '', false, 'occupation', currentTab, function () {
            $scope.populateOccupationListLookUpBen();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateOccupationListLookUpBen = function () {
        $scope.populateIndependentLookupDateInScopeBen('JOB', 'job', '', false, 'job', currentTab, function () {
            $scope.populateOccupationClassLookUpBen();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateOccupationClassLookUpBen = function () {
        $scope.populateIndependentLookupDateInScopeBen('SUBJOB', 'subjob', '', false, 'subjob', currentTab, function () {
            $scope.populateRelationshipLookUpBen();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateRelationshipLookUpBen = function () {
        $scope.populateIndependentLookupDateInScopeBen('RELATIONSHIP', 'relationship', '', false, 'relationship', currentTab, function () {
            $scope.populateLinearIndustryLookUpBen();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateLinearIndustryLookUpBen = function () {
        $scope.populateIndependentLookupDateInScopeBen('INDUSTRY_LINEAR', 'industryLinear', '', false, 'industryLinear', currentTab, function () {
            $scope.populateTitleGenderLookUpBen();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateTitleGenderLookUpBen = function () {
        $scope.populateIndependentLookupDateInScopeBen('TITLE_GENDER', 'titleGenderRelationship', '', false, 'titleGenderRelationship', currentTab, function () {
            $scope.populateTitleLookUpBen();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateTitleLookUpBen = function () {
        $scope.populateIndependentLookupDateInScopeBen('TITLE', 'title', '', false, 'title', currentTab, function () {
            $scope.populateNatureOfWorkLinearLookUp();
            $scope.refresh();
        }, function () {});
    }

    $scope.populateNatureOfWorkLinearLookUp = function () {
        $scope.populateIndependentLookupDateInScopeBen('NATUREOFWORK_LINEAR', 'natureOfWorkLinear', '', false, 'natureOfWorkLinear', currentTab, function () {
            $scope.populateNatureOfWorkOccupationLinearLookUp();
            $scope.refresh();
        }, function () {});
    }
    $scope.populateNatureOfWorkOccupationLinearLookUp = function () {
        $scope.populateIndependentLookupDateInScopeBen('NATUREOFWORKLINEAR_OCODE', 'natureOfWorkOccupationLinear', '', false, 'natureOfWorkOccupationLinear', currentTab, function () {
            $scope.populateNatureOfWorkLookUp();
            $scope.refresh();
        }, function () {});
    }
    $scope.populateNatureOfWorkLookUp = function() {
        $scope.populateIndependentLookupDateInScopeBen('NATUREOFWORK', 'natureOfWork', '', false, 'natureOfWork', currentTab, function() {
            
            $scope.refresh();
        }, function () {});
    }*/
	 /* Change by LETeam << ends here */
	/*function initiatePopulationBeneficiary() {
        $scope.populateIndependentLookupDateInScope2Ben('WARD', 'wardOrHamlet', 'FNAMyself.ContactDetails.currentAddress.ward', false, 'wardOrHamlet', 'leadDetailsSection',$scope.initialPaintUI);
         $scope.populateIndustryLookUpBen();
		//$scope.initialPaintUI();		
    }
    function initiatePopulation() {
        $scope.populateIndependentLookupDateInScope2('WARD', 'wardOrHamlet', 'FNAMyself.ContactDetails.currentAddress.ward', false, 'wardOrHamlet', 'leadDetailsSection',$scope.initialPaintUI);
         $scope.populateIndustryLookUp();
		//$scope.initialPaintUI();		
    }*/
    $scope.setValuetoScope = function (model, value) {
        var _lastIndex = model.lastIndexOf('.');
        if (_lastIndex > 0) {
            var parentModel = model.substring(0, _lastIndex);
            var scopeVar = $scope.getValue(parentModel);
            var remain_parentModel = model.substring(_lastIndex + 1, model.length);
            scopeVar[remain_parentModel] = value;
        } else {
            $scope[model] = value;
        }
    }
    // Changes done by LETeam << Change ends here
	$scope.copyClientAddress= function(){
		if($scope.Beneficiary.CurrentAddress.address == "Yes"){
			$scope.Beneficiary.CurrentAddress.addressLine1 = $scope.FNAMyself.ContactDetails.currentAddress.addressLine1;
		   	$scope.Beneficiary.CurrentAddress.state = $scope.FNAMyself.ContactDetails.currentAddress.state;
			$scope.Beneficiary.CurrentAddress.stateValue = $scope.FNAMyself.ContactDetails.currentAddress.stateValue;
		   	$scope.Beneficiary.CurrentAddress.city = $scope.FNAMyself.ContactDetails.currentAddress.city;
			$scope.Beneficiary.CurrentAddress.cityValue = $scope.FNAMyself.ContactDetails.currentAddress.cityValue;
		   	$scope.Beneficiary.CurrentAddress.zipcode = $scope.FNAMyself.ContactDetails.currentAddress.zipcode;
			$scope.Beneficiary.CurrentAddress.zipcodeValue = $scope.FNAMyself.ContactDetails.currentAddress.zipcodeValue;
			$scope.Beneficiary.CurrentAddress.addressLine2 = $scope.FNAMyself.ContactDetails.currentAddress.addressLine2;
			$scope.Beneficiary.CurrentAddress.ward = $scope.FNAMyself.ContactDetails.currentAddress.ward;
			$scope.Beneficiary.CurrentAddress.wardOrHamletValue = $scope.FNAMyself.ContactDetails.currentAddress.wardOrHamletValue;
			$scope.Beneficiary.CurrentAddress.street = $scope.FNAMyself.ContactDetails.currentAddress.street;
			$scope.Beneficiary.CurrentAddress.streetName = $scope.FNAMyself.ContactDetails.currentAddress.streetName;
			$scope.Beneficiary.BasicDetails.nationality = $scope.FNAMyself.BasicDetails.nationality;
			$scope.Beneficiary.ContactDetails.birthAddress.country = $scope.FNAMyself.ContactDetails.birthAddress.country;
            $scope.Beneficiary.CurrentAddress.houseNo = $scope.FNAMyself.ContactDetails.currentAddress.houseNo;
			//$scope.Beneficiary.BasicDetails.identityProof = $scope.FNAMyself.BasicDetails.identityProof;
			$scope.FNAObject.FNA.checkedState = "Yes";
			$scope.Beneficiary.copyMySelfAddress = true;
		}
		else{
            $scope.Beneficiary.CurrentAddress = [];
			$scope.Beneficiary.CurrentAddress.addressLine1 = "";
		   	$scope.Beneficiary.CurrentAddress.state = "";
            $scope.Beneficiary.CurrentAddress.stateValue = "";
		   	$scope.Beneficiary.CurrentAddress.city = "";
            $scope.Beneficiary.CurrentAddress.cityValue = "";
		   	$scope.Beneficiary.CurrentAddress.zipCode = "";
            $scope.Beneficiary.CurrentAddress.zipcodeValue = "";
		   	$scope.Beneficiary.CurrentAddress.addressLine2 = "";
            $scope.Beneficiary.CurrentAddress.ward = "";
            $scope.Beneficiary.CurrentAddress.street = "";
            $scope.Beneficiary.CurrentAddress.wardOrHamletValue = "";
            $scope.Beneficiary.CurrentAddress.houseNo = "";
		   	$scope.Beneficiary.ContactDetails.birthAddress.country = "";
		   	$scope.Beneficiary.CurrentAddress.streetName  = "";
			$scope.FNAObject.FNA.checkedState = "No";
			$scope.Beneficiary.copyMySelfAddress = false;
		}
     }; 
	
	LEDynamicUI.getUIJson(rootConfig.FnaConfigJson, false, function(fnaConfig) {
		FnaVariables.occupation = fnaConfig.occupation;
		$scope.occupation = FnaVariables.occupation;						
	});

	/* ON CLICK OF THE DROPPED ITEM, IT SHOULD SHOW THE SCRAP POP UP */
	$scope.onMemberClick = function(event) {
		thisObjDeletion = event.target.parentNode.parentNode;
		var thisObjClassName = String(thisObjDeletion.className);
		var clickedNode = event.target.nodeName;
		var parentDiveNodes = $(clickedNode).parents('.owl-item');
		$(parentDiveNodes).removeClass("active");
		$(thisObjDeletion).closest('.owl-item').addClass('active');
		var id = thisObjDeletion.id;
		g_placeholder = JSON.parse(id) + 1;
		if (thisObjClassName.indexOf("nonmember") >= 0) {
			var individualOffset = findOffsetForScrap(thisObjDeletion, -10, 80);
			var topVal = individualOffset[0];
			var leftval = individualOffset[1];
			var chk = $('.scrapButton').css("display");
			if (chk == "none") {
				$('.scrapButton').fadeIn();
				$('.scrapButton').css({
					"display" : "block",
					"top" : topVal,
					"left" : leftval
				});
			} else if (chk == "block") {
				$('.scrapButton').css({
					"top" : topVal,
					"left" : leftval
				});
			}
		}
	};    
    
    
	//SubFunction called on delete of a member
	$scope.removeMember = function(id) {
		var removedMember;
		for (var i = 0; i < $scope.FNABeneficiaries.length; i++) {
			if ($scope.FNABeneficiaries[i] != null && $scope.FNABeneficiaries[i].id == id) {
				removedMember = $scope.FNABeneficiaries[i].imageClass
				.split(' ')[1];
				delete $scope.FNABeneficiaries[i];        
                $scope.defaultImageForMembers[i].photo = FnaVariables.photo;
				var btntext = "#detailsBtn" + g_placeholder;
				var translateEditDetails = translateMessages($translate, "fna.addDetails");
				$('#txtHeaderInsured').html(translateEditDetails);
				
				$(btntext).removeClass().addClass("addetl_myfamily").html("Add Details");
				var classtoBePassedFamily = ".myfulltabdetails";
				$scope.toggleSlide1(classtoBePassedFamily);
				if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
					$scope.refresh();
				}
                
				/*globalService.setFNABeneficiaries($scope.FNABeneficiaries);
				var parties = globalService.getParties();
				$scope.FNAObject.FNA.parties = parties;		
				FnaVariables.setFnaModel($scope.FNAObject);
				$scope.status = "Draft";
				for (var i = 1; i <= 10; i++) {
					$("#detailsBtn" + (i)).attr("disabled", false);
				}
				var obj = new GLI_FnaService.saveTransactions($scope, $rootScope,
						DataService, $translate, UtilityService, false);
				obj.save();*/
			}
		}
        
		//** Validate the occurance of the family member**/
		for (var j = 0; j < $scope.familyMembers.length; j++) {
			if ($scope.familyMembers[j].memberName == removedMember) {
				$scope.familyMembers[j].availability = Number($scope.familyMembers[j].availability) + 1;
				if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
					$scope.refresh();
				}

			}
		}
        
		UtilityService.disableProgressTab = true;
		GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);       
		//$scope.initializeProgressbarNew();
		//$route.reload();
	};
	//**Function called on delete of a member
    /* FNA changes by LE Team Start */
	$scope.onDelete = function(event) {
        $scope.lePopupCtrl.showWarning(
						translateMessages($translate,
								"lifeEngage"),
						translateMessages($translate,
								"fna.deleteBeneficiary"),  
						translateMessages($translate,
								"fna.deleteBeneficiaryYes"),$scope.deleteBeneficiary,translateMessages($translate,
								"fna.deleteBeneficiaryNo"),$scope.okPressed);

		/* thisObjDeletion = thisObjDeletion.parentNode;
			$(thisObjDeletion).removeClass().addClass("member ui-droppable");
			$(thisObjDeletion).find('.relationText').html("Member");
		$(thisObjDeletion).parent().parent('.active').removeClass("active");
	
		var id = thisObjDeletion.id;
		$scope.removeMember(id);
		$scope.global_val_array.splice(id, $scope.global_val_array.length);

		for (var i = 1; i <= 10; i++) {
			$("#detailsBtn" + (i)).attr("disabled", false);
			$("#selfdetailsBtn").attr("disabled", false);
		} */

		/* thisObjDeletion = thisObjDeletion.parentNode;
			$(thisObjDeletion).removeClass().addClass("member ui-droppable");
			$(thisObjDeletion).find('.relationText').html("Member");
		$(thisObjDeletion).parent().parent('.active').removeClass("active");
	
		var id = thisObjDeletion.id;
		$scope.removeMember(id);
		$scope.global_val_array.splice(id, $scope.global_val_array.length);
        /* FNA Changes by LE Team Start (instead of 10, it will be 7 draggable circles, so changing it from 10 to 7) */
		//for (var i = 1; i <= 7; i++) {
        /* FNA Changes by LE Team Start */
			//$("#detailsBtn" + (i)).attr("disabled", false);
			//$("#selfdetailsBtn").attr("disabled", false);
		//} 

		
	
	};
	/* FNA changes by LE Team End */
    
	$scope.getGlobalArrayValue = function($scope, returnVal) {
		for (var i = 0; i < returnVal.length; i++) {
			if (returnVal[i] != null && typeof returnVal[i] != "undefined") {
				$scope.global_val_array[i] = returnVal[i].classNameAttached;
			}
		}
	};

	$scope.getGlobalArrayValue($scope, $scope.FNABeneficiaries);
	
	//SubFunction called on click fo Add/Edit details of family member
	$scope.addEditFamilyDetails = function(event) {
		var thisObj = event.currentTarget;
		var thisObjId = thisObj.id;
		var thisObjClass = String(thisObj.className);
        $scope.isInsuredClicked = false;
        $scope.isBenefeciaryClicked = true;
        
		/*
		 * Store the number extracted from the ID in a global placeholder as a
		 * flag
		 */
		sub_len = thisObjId.substring(10);
		g_placeholder = sub_len;
		// 1
		if (thisObjClass.indexOf("addetl_myfamily") >= 0) {
			var chklength = $scope.global_val_array[g_placeholder - 1].length;
			$scope.toggleSlideforFamily(thisObj, thisObjId, thisObjClass, chklength);
		} else if (thisObjClass.indexOf("editdetl_myfamily") >= 0) {
			$scope.toggleSlideforFamily(thisObj, thisObjId, thisObjClass);
		}
	};
	//Function to toggle the subsetcion of member details . Change the labels and styles
	$scope.toggleSlideforFamily = function(thisObject, thisId, thisClass, chklength) {
		/*
		 * Get the clicked button object, get its id, and class and extract the
		 * number to match the global placeholder, from the button id
		 */
		var thisObj = thisObject;
		var thisObjId = thisId;
		var thisObjClass = thisClass;
		classtoBePassedFamily = ".myfulltabdetails";
		var translateAboutMy = translateMessages($translate, "fna.aboutMy");
		var translateMemberMessage = "fna." + global_val_array[g_placeholder - 1];
		var translateMember = translateMessages($translate, translateMemberMessage);

		/* Need the button to be chnaged to "About my Son/Daughter/Wife etc */
		if (thisObjClass == "addetl_myfamily" && chklength > 0) {
			$(thisObj).removeClass("addetl_myfamily").addClass("aboutMeButton_myfamily");
			$(thisObj).html(translateAboutMy + translateMember + " ");
			$scope.refresh();
			$scope.toggleSlide(classtoBePassedFamily);
		}
		if (thisObjClass == "editdetl_myfamily") {
			$(thisObj).removeClass("editdetl_myfamily").addClass("aboutMeButton_myfamily");
			$(thisObj).html(translateAboutMy + translateMember + " ");
			$scope.refresh();
			$scope.toggleSlide(classtoBePassedFamily);
		}

	};
	$scope.initialCallback = function() {
		if(!(rootConfig.isDeviceMobile)){
			$timeout(function(){
                             	$('.owl-wrapper .owl-item:first-child').addClass('active');
                        }, 100);
		
		}else{
			$('.owl-wrapper .owl-item:first-child').addClass('active');
		}
		

		if(typeof $scope.FNAMyself.isSaved == 'undefined')
			$scope.FNAMyself.isSaved = false;
		var translateEditDetails = "";
		if(!$scope.FNAMyself.isSaved)
			translateEditDetails = translateMessages($translate, "fna.addDetails");
		else
			translateEditDetails = translateMessages($translate, "fna.editDetails");
		$('#txtHeaderInsured').html(translateEditDetails);
		
				
		if($scope.FNAMyself.BasicDetails && $scope.FNAMyself.BasicDetails.photo)
		{
			if($scope.FNAMyself.BasicDetails.photo != FnaVariables.myselfPhoto){
				$scope.myselfPhotoValidation = "false";
				var fnaInsuredPhoto = translateMessages($translate, "fna.fnaInsuredPhoto");	
				$('#fnaInsuredPhoto').html(fnaInsuredPhoto);
				$scope.refresh();
			}
			else
			{
				var fnaInsuredPhoto = translateMessages($translate, "fna.fnaUploadPhoto");
				$('#fnaInsuredPhoto').html(fnaInsuredPhoto);
				$scope.refresh();
			}
		}
		else
		{
			var fnaInsuredPhoto = translateMessages($translate, "fna.fnaUploadPhoto");
			$('#fnaInsuredPhoto').html(fnaInsuredPhoto);
			$scope.refresh();
		}
		
		
		
		
		
		
		$scope.InsuredTempForCancel = JSON.parse(JSON.stringify($scope.FNAMyself));
		$scope.memberIndex = -1;
		$scope.initilizeLabels();
		
		$scope.InsuredTemp = angular.copy($scope.FNAMyself);
		
		$scope.updateErrorCount('aboutMeInsured');
	};
	$scope.memberCallback = function() {
		
		var fnaInsuredPhoto = "";
		if (!(rootConfig.isDeviceMobile)){
		}
		if ($scope.FNABeneficiaries[$scope.memberIndex].BasicDetails && $scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.photo != "") {
			$scope.benificiaryPhotoValidation = "false";
			fnaInsuredPhoto = translateMessages($translate, "fna.fnaInsuredPhoto");	
		}
		else
		{
			fnaInsuredPhoto = translateMessages($translate, "fna.fnaUploadPhoto");				
		}
		
		$('#fnaBeneficiaryPhoto').html(fnaInsuredPhoto);
		$scope.refresh();
		$scope.updateErrorCount('aboutMeInsured');
	};
	$scope.myselfCallback = function() {
		$scope.memberIndex = -1;
		if($scope.FNAMyself.BasicDetails && $scope.FNAMyself.BasicDetails.photo)
		{
			if($scope.FNAMyself.BasicDetails.photo != FnaVariables.myselfPhoto){
				$scope.myselfPhotoValidation = "false";
				var fnaInsuredPhoto = translateMessages($translate, "fna.fnaInsuredPhoto");	
				$('#fnaInsuredPhoto').html(fnaInsuredPhoto);
				$scope.refresh();
			}
			else
			{
				var fnaInsuredPhoto = translateMessages($translate, "fna.fnaUploadPhoto");
				$('#fnaInsuredPhoto').html(fnaInsuredPhoto);
				$scope.refresh();
			}
		}
		else
		{
			var fnaInsuredPhoto = translateMessages($translate, "fna.fnaUploadPhoto");
			$('#fnaInsuredPhoto').html(fnaInsuredPhoto);
			$scope.refresh();
		}

		$scope.updateErrorCount('aboutMeInsured');
	};
	$scope.myFamilyDragDrop = function() {
		$(".familymember").draggable({
			revert : false,
			helper : "clone",
			revertDuration : 100
		});

		$(".member").droppable({
			drop : function(event, ui) {
				/*
				 * Get the class name of the currently dragged
				 * item
				 */
				/* FNA Changes by LE Team Start */
				/* 
				var currentlyDragged = $(ui.draggable).attr(
				"class").split(' ')[1];	
				*/
				// var currentlyDragged = $(ui.draggable).attr("ng-class").split('}[')[0].split(' ')[3].replace(/'/g , "");
                var currentlyDragged = $(ui.draggable).attr("class").split(' ')[3];
				/* FNA Changes by LE Team Ends */
				var currentElement = event.target;
				/* Uppercasing the Class name */
				var classNameAttached = currentlyDragged.substr(0, 1).toUpperCase() + currentlyDragged.substr(1);
				/* FNA Changes by LE Team Start (Bug: 3777) */
				if (classNameAttached == "Wife" || classNameAttached == "Husband") {
					classNameAttached = "Spouse";
				}
				/* FNA Changes by LE Team End (Bug: 3777) */
				// SOn
				/*
				 * Get the class name of the currently dropped
				 * area ( member/non member)
				 */
				$scope.updateDroppedContent =  function(){
						if (chkFlag == "member") {
							$scope.FNABeneficiaries[Number($(currentElement).attr("id"))] = {};
							$scope.FNABeneficiaries[Number($(currentElement).attr("id"))].id = $(currentElement).attr("id");
							$scope.FNABeneficiaries[Number($(currentElement).attr("id"))].imageClass = "nonmember " + currentlyDragged;
							$scope.FNABeneficiaries[Number($(currentElement).attr("id"))].classNameAttached = classNameAttached;
							
							
							$scope.order = $(currentElement).attr("id");
							$(currentElement).removeClass().addClass('nonmember ' + currentlyDragged + ' ');
							$(currentElement).find('.relationText').html(classNameAttached);

							$scope.getGlobalArrayValue($scope, $scope.FNABeneficiaries);
							$scope.setMember($scope.order, currentlyDragged);
							
							$scope.FNABeneficiaries[Number($(currentElement).attr("id"))].isSaved = false;
							var num =  Number($(currentElement).attr("id"));
							var beneficiaryTxt = "detailsBtn" +(num + 1);
							$('#'+beneficiaryTxt).text(translateMessages($translate, "fna."+classNameAttached));
							$scope.isBeneficiaryDragged = true;
						}
					};
					var chkFlag = $(currentElement).attr("class").split(' ')[0];
					if($scope.FNAObject.FNA.saveCheck == false){
						
						 $scope.lePopupCtrl.showWarning(
									translateMessages($translate,
											"lifeEngage"),
									translateMessages($translate,
											"saveBeneficiaryData"),  
									translateMessages($translate,
											"fna.navok"),$scope.okPressed,translateMessages($translate,
											"general.navproceed"),$scope.updateDroppedContent);
					}else{
						$scope.updateDroppedContent();
					}
			}
		});
		
	};
	$scope.updateDroppedContentGender =  function(chkFlag,gender){
						if (chkFlag == "member") {
							$scope.FNABeneficiaries[Number($(currentElement).attr("id"))] = {};
							$scope.FNABeneficiaries[Number($(currentElement).attr("id"))].id = $(currentElement).attr("id");
							$scope.FNABeneficiaries[Number($(currentElement).attr("id"))].imageClass = "nonmember " + gender;
							$scope.FNABeneficiaries[Number($(currentElement).attr("id"))].classNameAttached = classNameAttached;
							
							
							$scope.order = $(currentElement).attr("id");
							$(currentElement).removeClass().addClass('nonmember ' + gender + ' ');
							$(currentElement).find('.relationText').html(classNameAttached);

							$scope.getGlobalArrayValue($scope, $scope.FNABeneficiaries);
							$scope.setMember($scope.order, gender);
							
							$scope.FNABeneficiaries[Number($(currentElement).attr("id"))].isSaved = false;
							var num =  Number($(currentElement).attr("id"));
							var beneficiaryTxt = "detailsBtn" +(num + 1);
							$('#'+beneficiaryTxt).text(translateMessages($translate, "fna."+classNameAttached));
							$scope.isBeneficiaryDragged = true;
						}
    };
	$scope.cancelFamilyDetails = function() {
		/* ITEM TO WHICH THE IM AGE NEEDS TO BE SAVED */
		var btntext = "#detailsBtn" + sub_len;
		var translateEditDetailsCancel = translateMessages($translate, "fna.editDetails");
		$('#txtHeaderInsured').html(translateEditDetails);
		classtoBePassedFamily = ".myfulltabdetails";
		$scope.toggleSlide(classtoBePassedFamily);
	};
	//Function called to Slide up the tab details on saving a member
	$scope.saveFamilyDetails = function() {
		/* ITEM TO WHICH THE IM AGE NEEDS TO BE SAVED */
		var btntext = "#detailsBtn" + sub_len;
		var translateEditDetails = translateMessages($translate, "fna.editDetails");
		$('#txtHeaderInsured').html(translateEditDetails);
		$('.owl-wrapper .owl-item').eq(g_placeholder - 1).children().children(".savedetl_wrapper").children().css("display", "block");
		/*
		 * Find the list item corresponding to the button to change the image on
		 * save button click
		 */
		var equation = g_placeholder - 1;
		$('.owl-wrapper .owl-item').eq(equation).find('.nonmemberwrapper').addClass("kerry_lady").children().remove();
		/* Slide up the tab details */
		$scope.toggleSlide(classtoBePassedFamily);
	};
	//Initialise function called once all members are [populated using ng-repeat
	$scope.initialiseFamilyDetails = function() {

		//To set the default base64string string so as to avoid the ?, if place-holder has no image
		if ( typeof $scope.FNAMyself.BasicDetails != "undefined") {
			if ( typeof $scope.FNAMyself.BasicDetails.photo == "undefined" || $scope.FNAMyself.BasicDetails.photo == "") {
				$scope.FNAMyself.BasicDetails.photo = FnaVariables.myselfPhoto;
			} else {
				$scope.FNAMyself.BasicDetails.photo = ($scope.FNAMyself.BasicDetails.photo).replace(/ /g, '+');
			}
		}
       
		for (var i = 0; i < $scope.FNABeneficiaries.length; i++) {
			if ($scope.FNABeneficiaries[i] != undefined && $scope.FNABeneficiaries[i].classNameAttached != undefined) {
                if ( typeof $scope.FNABeneficiaries[i] != "undefined" && $scope.FNABeneficiaries[i] != null) {
				$scope.FNABeneficiaries[i].id = i.toString();
				if ( typeof $scope.FNABeneficiaries[i].imageClass != "undefined") {
					$("#" + $scope.FNABeneficiaries[i].id + "").removeClass().addClass('' + $scope.FNABeneficiaries[i].imageClass + ' ');
					var classNameAttached = $scope.FNABeneficiaries[i].imageClass
					.split(' ')[1];
					// SOn
					var classNameAttached1 = classNameAttached.substr(0, 1).toUpperCase() + classNameAttached.substr(1);

				}

				//Changing Add Details to Edit Details
				if ( typeof $scope.FNABeneficiaries[i].BasicDetails != "undefined") {
					g_placeholder = i + 1;
					var btntext = "#detailsBtn" + g_placeholder;
					var translateEditDetails = translateMessages($translate, "fna.editDetails");
					$('#txtHeaderInsured').html(translateEditDetails);
				}
				if ( typeof $scope.FNABeneficiaries[i].BasicDetails != "undefined" && $scope.FNABeneficiaries[i].BasicDetails.photo) {
					$scope.FNABeneficiaries[i].BasicDetails.photo = ($scope.FNABeneficiaries[i].BasicDetails.photo).replace(/ /g, '+');
					$scope.defaultImageForMembers[i].photo = $scope.FNABeneficiaries[i].BasicDetails.photo;
				}
			}
			if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
				$scope.refresh();
			}
          }
		}
		for (var i = 0; i < $scope.FNABeneficiaries.length; i++) {
            if ($scope.FNABeneficiaries[i] != undefined && $scope.FNABeneficiaries[i].classNameAttached != undefined) {                
			for (var j = 0; j < $scope.familyMembers.length; j++) {
				if ( typeof $scope.FNABeneficiaries[i] != "undefined" && $scope.FNABeneficiaries[i] != null) {
					var classNameAttached = $scope.FNABeneficiaries[i].imageClass
					.split(' ')[1];
					if (classNameAttached == $scope.familyMembers[j].memberName) {
						$scope.familyMembers[j].availability = $scope.familyMembers[j].availability - 1;
						if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
							$scope.refresh();
						}
					}
				}

			}
            }
		}

		if ($scope.FNAMyself.BasicDetails.firstName != "" || $scope.FNAMyself.BasicDetails.lastName != "" || $scope.FNAMyself.BasicDetails.dob != "" || $scope.FNAMyself.OccupationDetails.occupationCategory != "" || $scope.FNAMyself.ContactDetails.emailId != "" || $scope.FNAMyself.ContactDetails.homeNumber1 != "" || $scope.FNAMyself.ContactDetails.currentAddress.address != "") {

			var translateEditDetailsEdit = translateMessages($translate, "fna.editDetails");
			$('#selfdetailsBtn').removeClass().addClass("editDetlBtn").html(translateEditDetailsEdit);
			$('.savedDetails').css("display", "block");
			if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
				$scope.refresh();
			}
		}

		if ($scope.FNAMyself.BasicDetails.photo == FnaVariables.myselfPhoto) {
			$scope.insuredPhotoErrorCount = 1;
		}
		
		for(var i= 0; i< $scope.FNABeneficiaries.length; i++)
		{
            if ($scope.FNABeneficiaries[i] != undefined && $scope.FNABeneficiaries[i].classNameAttached != undefined) {                
			var beneficiaryTxt = "detailsBtn" +( i + 1);
			if($scope.FNABeneficiaries[i].BasicDetails)
			{					
				var beneficiaryFName = $scope.FNABeneficiaries[i].BasicDetails.firstName;
				var beneficiaryLName = $scope.FNABeneficiaries[i].BasicDetails.lastName;
				
				if((typeof beneficiaryFName =='undefined' || beneficiaryFName == "null"))
					beneficiaryFName = "";
				if((typeof beneficiaryLName =='undefined' || beneficiaryLName == "null"))
					beneficiaryLName = "";
				
				if($scope.FNABeneficiaries[i].BasicDetails.firstName)
				if($scope.FNABeneficiaries[i].BasicDetails.firstName.length>25)
			{
			$('#'+beneficiaryTxt).addClass('fontSize13');
			}
				
				
				if((beneficiaryFName == "" && beneficiaryLName == ""))
				{					
					$('#'+beneficiaryTxt).text(translateMessages($translate, "fna."+$scope.FNABeneficiaries[i].classNameAttached));
				}
				else
				{
					var beneficiaryFullname = beneficiaryFName;
					$('#'+beneficiaryTxt).text(beneficiaryFullname);
				}
			}
			else
			{
				$('#'+beneficiaryTxt).text(translateMessages($translate, "fna."+$scope.FNABeneficiaries[i].classNameAttached));
			}
            }
		}
		
	};
	$scope.checkDob = function(dob,type) {
		
		var formattedDob ;
		if (dob != "" && dob != undefined
				&& dob != null) {
			var dobDate = new Date(dob);
			/*
			 * In I.E it will accept date format in '/'
			 * separator only
			 */
			if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
				dobDate = new Date(dob.split("-").join(
						"/"));
			}
			var todayDate = new Date();
			if (formatForDateControl(dob) >= formatForDateControl(new Date())) {
				formattedDob = formatForDateControl(new Date());
			} else {
				formattedDob = dob;
			}
			
			if(type == 'fnaBeneficiaryDOB')
				$scope.Beneficiary.BasicDetails.dob = formattedDob;
			else
				$scope.FNAMyself.BasicDetails.dob = formattedDob;
		}
	};
	
	$scope.cancelUnsavedData = function()
	{
		if($scope.memberIndex == -1)
			$scope.cancelInsured();
			
		else
			$scope.cancelBeneficiary();
			
	};
    
    
    $scope.addOccupationBen = function () {        
        if ($scope.Beneficiary.OccupationDetails[0].occupationCode!= "" && !angular.equals(undefined, $scope.Beneficiary.OccupationDetails[0].occupationCode)) {
            $scope.showSecondaryPayorBen = true;
            $scope.isInitialLoadBen1 = false;
            $scope.Beneficiary.OccupationDetails[1] = "";
        }        
    }

    $scope.removeOccupationBen = function () {
        $scope.showSecondaryPayorBen = false;
        //$scope.FNAMyself.OccupationDetails[1].occupationCode='';
        $scope.Beneficiary.OccupationDetails[1]=[];
//        $scope.Illustration.OccupationInsured.splice(1, 1);
    }
    /* Change by LETeam << ends here */
	//Function called on click of cancel button of member details section
	$scope.cancelBeneficiary = function() {
		$scope.contactNo = "";
		
		UtilityService.disableProgressTab = false;
		
		
		
		$scope.Beneficiary.BasicDetails = {
				"firstName" : "",
				"lastName" : "",
				"nationality" : "",
				"dob" : "",
				"gender":"",
				"age":"",
				"photo" : "",
				"identityProof" : ""
			};
 /* Change by LETeam << starts here */
		$scope.Beneficiary.OccupationDetails = [];
		$scope.Beneficiary.OccupationDetails[0] = "";
		$scope.Beneficiary.OccupationDetails[1] = "";
		$scope.Beneficiary.OccupationDetails[2] = "";
		$scope.Beneficiary.OccupationDetails[3] = "";
 /* Change by LETeam << ends here */		$scope.Beneficiary.ContactDetails = {
					"homeNumber1" : "",
					"mobileNumber1":"",
					"emailId" : "",
					"currentAddress" : {
							"address" : "",
							"addressLine1" : "",
							"city" : "",
							"state": "",
							"zipCode" : "",
							"addressLine2" : ""
						},
					"birthAddress" : {
						"country":""
					}
				};
		$scope.Beneficiary.copyMySelfAddress = false; 
		
		
		
		
		
		/** Member section model is bind to  scope variable 'Beneficiary'. Making this variable blank since we are displaying the same variable for all members -- End**/

		//Initially we are keeping the previous data to BeneficiaryTemp variable.
		//On cancelling, setting teh values back from temp variable to FNAObject mapping
		//Added undefined checked on $scope.BeneficiaryTemp.BasicDetails for the case when user clicks on cancel without filling data.
		if($scope.BeneficiaryTemp.BasicDetails != undefined){
		$scope.FNABeneficiaries[$scope.editIndex] = angular.copy($scope.BeneficiaryTemp);
		

		$scope.Beneficiary = angular.copy($scope.FNABeneficiaries[$scope.memberIndex]) ;
		}
		$scope.addContactPrefix($scope.contactNo,'beneficiary');
		globalService.setFNABeneficiaries($scope.FNABeneficiaries);
		if($scope.BeneficiaryTemp.BasicDetails.photo)
		$scope.defaultImageForMembers[$scope.editIndex].photo = $scope.BeneficiaryTemp.BasicDetails.photo;
        /* FNA Changes by LE Team Start (instead of 10, it will be 7 draggable circles, so changing it from 10 to 7) */
		for (var i = 1; i <= 7; i++) {
        /* FNA Changes by LE Team Start */
			$("#detailsBtn" + (i)).attr("disabled", false);
			$("#selfdetailsBtn").attr("disabled", false);
		}
		$('.scrapButton').attr("disabled", false);
		$scope.benificiaryPhotoValidation = "true";
		
		var beneficiaryTxt = "detailsBtn" +( $scope.editIndex + 1);
		if( $scope.FNABeneficiaries[$scope.editIndex].BasicDetails)
		{
			var beneficiaryFName = $scope.FNABeneficiaries[$scope.editIndex].BasicDetails.firstName;
			var beneficiaryLName = $scope.FNABeneficiaries[$scope.editIndex].BasicDetails.lastName;
			
			if((typeof beneficiaryFName =='undefined' || beneficiaryFName == "null"))
				beneficiaryFName = "";
			if((typeof beneficiaryLName =='undefined' || beneficiaryLName == "null"))
				beneficiaryLName = "";
			if($scope.FNABeneficiaries[$scope.editIndex].BasicDetails.firstName>25)
			{
			$('#'+beneficiaryTxt).addClass('fontSize13');
			}
			if((beneficiaryFName == "" && beneficiaryLName == ""))
			{					
				$('#'+beneficiaryTxt).text(translateMessages($translate, "fna."+$scope.FNABeneficiaries[$scope.editIndex].classNameAttached));
			}
			else
			{
				var beneficiaryFullname = beneficiaryFName;
				$('#'+beneficiaryTxt).text(beneficiaryFullname);
			}		
			
		}
		else
		{					
			$('#'+beneficiaryTxt).text(translateMessages($translate, "fna."+$scope.FNABeneficiaries[$scope.editIndex].classNameAttached));
		}
		//
		if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
			$scope.refresh();
		}
	};
	//Function called on click of cancel button  of myself section
	$scope.cancelInsured = function() {
		
		$scope.contactNo = "";
		UtilityService.disableProgressTab = false;
		$scope.FNAMyself = {};
		
		
		$scope.FNAMyself = angular.copy($scope.InsuredTemp);
		$scope.addContactPrefix($scope.contactNo,'myself');
        /* FNA Changes by LE Team Start (instead of 10, it will be 7 draggable circles, so changing it from 10 to 7) */
		for (var i = 1; i <= 7; i++) {
        /* FNA Changes by LE Team Start */
			$("#detailsBtn" + (i)).attr("disabled", false);
		}
		$scope.myselfPhotoValidation = "true";
		
		
		var myselfFName = $scope.FNAMyself.BasicDetails.firstName;
		var myselfLName = $scope.FNAMyself.BasicDetails.lastName;
		
		if((typeof myselfFName =='undefined' || myselfFName == "null"))
			myselfFName = "";
		if((typeof myselfLName =='undefined' || myselfLName == "null"))
			myselfLName = "";
		
		if( $scope.FNAMyself.BasicDetails.firstName)
		if( $scope.FNAMyself.BasicDetails.firstName.length>25)
		{
			$('#txtMySelfLabel').addClass('fontSize13');
		}	
		if((myselfFName == "" && myselfLName == ""))
		{
			$('#txtMySelfLabel').text(translateMessages($translate,"fna.myself"));
		}
		else
		{
			$('#txtMySelfLabel').text(myselfFName);
		}

		
	};
	
	//To toggle the display of myself/member block
	$scope. toggleSlide1 = function(tabname) {
		var chkFlag = $(tabname).css("display");
		if (chkFlag == "block") {
			$(tabname).slideUp("slow", function() {
			});
		}
	}
	

	$scope.updateErrorCount = function(arg1, arg2) {
		//FNA changes by LE Team Start
        //$scope.validateFields(arg1);
		$rootScope.updateErrorCount(arg1, arg2);
        if($scope.isInsuredClicked==true){
        $scope.validateFields(arg1);
        }
        else if($scope.isBenefeciaryClicked==true){
        $scope.validateFieldsBen(arg1);
        }
		//FNA changes by LE Team End		
		// }
		/*$rootScope.updateErrorCount(arg1, arg2);
		//for beneficiary DOB
		if ($scope.Beneficiary.BasicDetails != undefined && arg1 == 'aboutMeInsured') {
			var dob = $scope.Beneficiary.BasicDetails.dob;
			if (LEDate(dob) > LEDate()) {
				$scope.errorCount = parseInt($scope.errorCount) + 1;
				$scope.refresh();
			}
		}
		//for self DOB
		if ($scope.FNAMyself.BasicDetails != undefined && arg1 == 'aboutMeInsured') {
			var dob = $scope.FNAMyself.BasicDetails.dob;
			if (LEDate(dob) > LEDate()) {
				$scope.errorCount = parseInt($scope.errorCount) + 1;
				$scope.refresh();
			}
		}*/
		
		//$scope.refresh();
	};
	$scope.showErrorPopup = function(arg1, arg2) {
		$rootScope.showErrorPopup(arg1, arg2);
		//for beneficiary DOB
		if ($scope.Beneficiary.BasicDetails != undefined && arg1 == 'aboutMeInsured') {
			var dob = $scope.Beneficiary.BasicDetails.dob;
			if (LEDate(dob) > LEDate()) {
				var error = {};
				error.message = translateMessages($translate, "fna.beneficiaryFututreDobValidationMessage");
				// DOB is a future date
				error.key = 'fnaBeneficiaryDOB';
				$scope.errorMessages.push(error);
			}

		}
		//for self DOB
		if ($scope.FNAMyself.BasicDetails != undefined && arg1 == 'aboutMeInsured') {
			var dob = $scope.FNAMyself.BasicDetails.dob;
			if (LEDate(dob) > LEDate()) {
				var error = {};
				//re-using error message for future DOB validation in beneficiaries
				error.message = translateMessages($translate, "fna.beneficiaryFututreDobValidationMessage");
				//DOB is a future date
				error.key = 'fnaInsuredDOB';
				$scope.errorMessages.push(error);
			}
		}
		$scope.refresh();
	};
	//Subfunction called on add/edit button click of Myself
	$scope.addSelfDetails = function(event) {
		var thisObj = event.currentTarget;
		g_clickedSlideDwnBtn = String(thisObj.className);

		/*
		 * Has only 1 class addDetlBtn, then on click, it should be toggled to
		 * "About me"
		 */
		if (g_clickedSlideDwnBtn == "addDetlBtn") {
			$(thisObj).addClass("selectedState aboutMeButton");
			var translateAboutMe = translateMessages($translate, "fna.aboutme");
			$(thisObj).html(translateAboutMe);
			$scope.refresh();
			classToBePassed = ".myfulldetails";
			/* Slide down and show the details tab */
			$scope.toggleSlide(classToBePassed);

		}
		/* The button is in About Me state now, toggle back to "Add details" */
		
		/* The button is Edit button now */
		if (g_clickedSlideDwnBtn == "editDetlBtn") {
			classToBePassed = ".myfulldetails";
			$scope.toggleSlide(classToBePassed);
			$(thisObj).removeClass().addClass("editDetlBtn aboutMeButton");
			var translateAboutMe = translateMessages($translate, "fna.aboutme");
			$(thisObj).html(translateAboutMe);
		}
		
		
		
		var clickedNode = $('.my-self');

		$(clickedNode).closest('.owl-item').addClass('active');
	};
	
	$scope.cancelBeneficiaryOnSwitching = function() {
		$scope.contactNo = "";
		//$scope.cancelFamilyDetails();
		UtilityService.disableProgressTab = false;
		
		 $scope.FNAObject.FNA.checkedState = "No";
			$scope.Beneficiary.copyMySelfAddress = false;
			
			
			
			$scope.FNABeneficiaries[$scope.memberIndex].BasicDetails = {
					"firstName" : "",
					"lastName" : "",
					"nationality" : "",
					"dob" : "",
					"gender":"",
					"age":"",
					"photo" : "",
					"identityProof" : ""
				};
				$scope.FNABeneficiaries[$scope.memberIndex].OccupationDetails = {
						"description" : "",
						"descriptionOthers" : "",
						"jobCode": "",
						"jobClass":"",
					};
				
				$scope.FNABeneficiaries[$scope.memberIndex].ContactDetails = {
						"homeNumber1" : "",
						"mobileNumber1":"",
						"emailId" : "",
						"currentAddress" : {
								"address" : "",
								"addressLine1" : "",
								"city" : "",
								"state": "",
								"zipCode" : "",
								"addressLine2" : ""
							},
						"birthAddress" : {
							"country":""
						}
					};
				$scope.FNABeneficiaries[$scope.memberIndex].copyMySelfAddress = false; 
		 
		 
		 
		 
		//Initially we are keeping the previous data to BeneficiaryTemp variable.
		//On cancelling, setting teh values back from temp variable to FNAObject mapping
		//$scope.FNABeneficiaries[$scope.memberIndex] = $scope.BeneficiaryTemp;
		
	   $scope.FNABeneficiaries[$scope.memberIndex] = angular.copy($scope.BeneficiaryTemp);
	   
		
		
 
	   
	   
	   
	   //for gatting myself saved data
	   if($scope.memberIndex == -1)
	   $scope.FNAMyself = angular.copy($scope.InsuredTemp);
		
		globalService.setFNABeneficiaries($scope.FNABeneficiaries);
		if($scope.BeneficiaryTemp.BasicDetails && $scope.BeneficiaryTemp.BasicDetails.photo)
		$scope.defaultImageForMembers[$scope.editIndex].photo = angular.copy($scope.BeneficiaryTemp.BasicDetails.photo);
        /* FNA Changes by LE Team Start (instead of 10, it will be 7 draggable circles, so changing it from 10 to 7) */
		for (var i = 1; i <= 7; i++) {
        /* FNA Changes by LE Team Start */
			$("#detailsBtn" + (i)).attr("disabled", false);
			$("#selfdetailsBtn").attr("disabled", false);
		}
		$('.scrapButton').attr("disabled", false);
		$scope.benificiaryPhotoValidation = "true";
		
		var beneficiaryTxt = "detailsBtn" +( $scope.memberIndex + 1);
		if( $scope.FNABeneficiaries[$scope.memberIndex].BasicDetails)
		{
			var beneficiaryFName = $scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.firstName;
			var beneficiaryLName = $scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.lastName;
			
			if((typeof beneficiaryFName =='undefined' || beneficiaryFName == "null"))
				beneficiaryFName = "";
			if((typeof beneficiaryLName =='undefined' || beneficiaryLName == "null"))
				beneficiaryLName = "";
			if($scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.firstName)
			if($scope.FNABeneficiaries[$scope.memberIndex].BasicDetails.firstName.length>25)
			{
			$('#'+beneficiaryTxt).addClass('fontSize13');
			}
			if((beneficiaryFName == "" && beneficiaryLName == ""))
			{					
				$('#'+beneficiaryTxt).text(translateMessages($translate, "fna."+$scope.FNABeneficiaries[$scope.memberIndex].classNameAttached));
			}
			else
			{
				var beneficiaryFullname = beneficiaryFName;
				$('#'+beneficiaryTxt).text(beneficiaryFullname);
			}		
			
		}
		else
		{					
			$('#'+beneficiaryTxt).text(translateMessages($translate, "fna."+$scope.FNABeneficiaries[$scope.memberIndex].classNameAttached));
		}
		//
		if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
			$scope.refresh();
		}
	};
	
	//function to proceed to illustration - enhancement EN019
	$scope.proceedToIllustration=function(){
		//alert('in');
		/*FNA changes by LE Team >>> starts - Bug 4194*/
		UtilityService.previousPage = 'FNA';
		IllustratorVariables.fromFNAChoosePartyScreenFlow = false;		
		FnaVariables.flagFromCustomerProfileFNA = true;
		FnaVariables.initialLoadSIFromFNA = true;
		FnaVariables.insuredFromFNA = $scope.InsuredTemp;
		IllustratorVariables.transactionId = 0;
		IllustratorVariables.fnaId = FnaVariables.transTrackingID;
		IllustratorVariables.leadId = FnaVariables.leadId;	
        IllustratorVariables.leadName = FnaVariables.fnaKeys.Key22;	
		/*FNA changes by LE Team >>> ends - Bug 4194*/
        /* FNA Changes by LE Team Bug 4216 - starts */ 
        EappVariables.fromFNA = true;
        /* FNA Changes by LE Team Bug 4216 - End */ 
		FnaVariables.setFnaModel($scope.FNAObject);
		$location.path('/fnaAllProducts');
	};

	$scope.clearOccupDesc = function(model){
         if(model == "Lain lain kelas 1" || model == "Lain lain kelas 2" || model == "Lain lain kelas 3" || model == "Lain lain kelas 4"){
                if($scope.FNAMyself.OccupationDetails.descriptionOthers){
               	         $scope.FNAMyself.OccupationDetails.descriptionOthers = "";
               	}
         }
     }

    $scope.clearIndustryDesc = function(model){
    	if(model == 'Lainnya'){
          if($scope.FNAMyself.OccupationDetails.natureofWorkDesc){
          	 $scope.FNAMyself.OccupationDetails.natureofWorkDesc = "";
          }
    	}
    }

    $scope.clearOccupDescForBeneficiary = function(model){
             if(model == "Lain lain kelas 1" || model == "Lain lain kelas 2" || model == "Lain lain kelas 3" || model == "Lain lain kelas 4"){
                    if($scope.Beneficiary.OccupationDetails.descriptionOthers){
                   	         $scope.Beneficiary.OccupationDetails.descriptionOthers = "";
                   	}
             }
         }

    $scope.clearIndustryDescForBeneficiary = function(model){
        	if(model == 'Lainnya'){
              if($scope.Beneficiary.OccupationDetails.natureofWorkDesc){
              	 $scope.Beneficiary.OccupationDetails.natureofWorkDesc = "";
              }
        	}
        }
    
    $scope.clearStateOnChange = function(model){
        if(model== "" || model== undefined){
          $scope.FNAMyself.ContactDetails.currentAddress.state = "";
          $scope.FNAMyself.ContactDetails.currentAddress.zipCode = "";
        }
    }
// Changes done by LETeam << Change starts here
        if ($scope.FNAMyself.OccupationDetails[0] == undefined) {
        $scope.FNAMyself.OccupationDetails[0]="";
		}
		if ($scope.FNAMyself.OccupationDetails[1] == undefined) {
        $scope.FNAMyself.OccupationDetails[1]="";
		}
		if ($scope.FNAMyself.OccupationDetails[2] == undefined) {
        $scope.FNAMyself.OccupationDetails[2]="";
		}
		if ($scope.FNAMyself.OccupationDetails[3] == undefined) {
        $scope.FNAMyself.OccupationDetails[3]="";
		}
        if ($scope.Beneficiary != undefined) {
			if ($scope.Beneficiary.OccupationDetails == undefined) {
				$scope.Beneficiary.OccupationDetails = [];
			}
			if ($scope.Beneficiary.OccupationDetails[0] == undefined) {
			$scope.Beneficiary.OccupationDetails[0]="";
			}
			if ($scope.Beneficiary.OccupationDetails[1] == undefined) {
			$scope.Beneficiary.OccupationDetails[1]="";
			}
			if ($scope.Beneficiary.OccupationDetails[2] == undefined) {
			$scope.Beneficiary.OccupationDetails[2]="";
			}
			if ($scope.Beneficiary.OccupationDetails[3] == undefined) {
			$scope.Beneficiary.OccupationDetails[3]="";
			}
		}
       $scope.updateErrorDynamically();
       /* $scope.populateIndependentLookupDateInScope('OCODE', 'occupationCode', '', false, 'occupationCode', currentTab, function () {
            $scope.populateIndustryLookUp();            
        }, function () {

        }); 
     $scope.populateIndependentLookupDateInScopeBen('OCODE', 'occupationCode', '', false, 'occupationCode', currentTab, function () {
        }, function () {
        });*/
		
        //$scope.populateIndustryLookUpBen();
        
// Changes done by LETeam << Change ends here
    $scope.clearStateOnChangeBeneficiary = function(model){
        if(model== "" || model== undefined){
          $scope.Beneficiary.CurrentAddress.state = "";
          $scope.Beneficiary.CurrentAddress.zipCode = "";
        }
    }
	
	/* FNA Changes by LE Team Start */
	$scope.openPDF = function(pdfFile) {	
		if((rootConfig.isDeviceMobile) && (!rootConfig.isOfflineDesktop)){
            var fileFullPath = "";
            window.plugins.LEFileUtils.getApplicationPath(function (path) {
                fileFullPath = "file://" + path + "/" + pdfFile;
                cordova.exec(function () {}, function () {}, "PdfViewer", "showPdf", [fileFullPath]);
            }, function (e) {});
        }else if(((rootConfig.isDeviceMobile) && (rootConfig.isOfflineDesktop)) ||((!rootConfig.isDeviceMobile) && (!rootConfig.isOfflineDesktop))){
            MediaService.openPDF($scope, $window, pdfFile);
        }     
	}
	/* FNA Changes by LE Team Ends */
	 $scope.$on("$destroy", function() {
                 if (this.$$destroyed) return;     
				 while (this.$$childHead) {      
				 this.$$childHead.$destroy();    
				 }     
				 if (this.$broadcast)
				{ 
					this.$broadcast('$destroy');   
				}    
				 this.$$destroyed = true;                 
				 $timeout.cancel( timer );                           
     });


	
}]);
