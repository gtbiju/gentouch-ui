﻿/*
 *Copyright 2015, LifeEngage 
 */


'use strict';

storeApp.controller('FnaController', ['$rootScope', '$scope', 'DataService', 'FnaService','LookupService', 'DocumentService', 'ProductService','$compile', 
                                      '$routeParams', '$location', '$translate','IllustratorVariables', 'FnaVariables', 'UtilityService','$debounce', 'PersistenceMapping', 'AutoSave',
                                       'globalService','$timeout',
                                      function($rootScope, $scope, DataService, FnaService, LookupService,DocumentService, ProductService, $compile, $routeParams, $location, $translate, 
                                      IllustratorVariables,FnaVariables, UtilityService,$debounce, PersistenceMapping, AutoSave, globalService,$timeout) {


	$scope.selectedpage = "Master";
	$scope.fnaName = 'FNA Controller';

}]);