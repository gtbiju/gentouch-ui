storeApp.controller('GLI_FnaListController',['$rootScope', '$scope', '$translate', '$compile','$routeParams', 'DataService', 'FnaVariables', '$location', 'globalService','UtilityService', 'PersistenceMapping','$controller',
function GLI_FnaListController($rootScope, $scope, $translate, $compile,$routeParams, DataService, FnaVariables, $location, globalService,UtilityService, PersistenceMapping,$controller){
$controller('FnaListController',{$rootScope:$rootScope, $scope:$scope, $translate:$translate, $compile:$compile,$routeParams:$routeParams, DataService:DataService, FnaVariables:FnaVariables, $location:$location, globalService:globalService,UtilityService:UtilityService, PersistenceMapping:PersistenceMapping});

/* FNA Changes by LE team Start */
$rootScope.hideLanguageSetting = false;
$rootScope.enableRefresh=false;
/* FNA Changes by LE team End */
LEDynamicUI.getUIJson(rootConfig.FnaConfigJson, false, function(FnaConfigJson) {
	FnaVariables.goals = FnaConfigJson.goals;
	FnaVariables.pages = FnaConfigJson.pages;	
	FnaVariables.pageTypes = FnaConfigJson.pageTypes;
	
	FnaVariables.goals = FnaConfigJson.goals;
	FnaVariables.lifeStageGoals = FnaConfigJson.lifeStageGoals;
	FnaVariables.familyMembers = FnaConfigJson.familyMembers;
	FnaVariables.riskList = FnaConfigJson.riskList;
	FnaVariables.aboutCompany = FnaConfigJson.aboutCompany;
	FnaVariables.custLifeStagesList = FnaConfigJson.custLifeStages;
	FnaVariables.custGenderSelectionsList = FnaConfigJson.custGenderSelections;			
	FnaVariables.pages = FnaConfigJson.pages;	
	FnaVariables.summaryGraph = FnaConfigJson.summaryGraph;
	FnaVariables.pageTypes = FnaConfigJson.pageTypes;	
	$scope.performanceDetails = FnaVariables.aboutCompany.performanceDetails;
	$scope.revenueDetails = FnaVariables.aboutCompany.revenueDetails;
	$scope.refresh();
});
$scope.appDateFormat = rootConfig.appDateFormat;
$scope.isWeb  = false;
	if(rootConfig.isDeviceMobile==false){
		$scope.isWeb  = true;
	}
$scope.onGeFnatListingsSuccess = function(data) {

	if (data[0].TransactionData.parties) {
		for (party in data[0].TransactionData.parties) {
			if (data[0].TransactionData.parties[party].BasicDetails
					&& data[0].TransactionData.parties[party].BasicDetails.photo
					&& data[0].TransactionData.parties[party].BasicDetails.photo != "") {
				data[0].TransactionData.parties[party].BasicDetails.photo = (data[0].TransactionData.parties[party].BasicDetails.photo)
						.replace(/ /g, '+');
			}
		}
		var parties = data[0].TransactionData.parties;
		globalService.setParties(parties);

	}

	if (data[0].Key16 != null && data[0].Key16 == "SUCCESS") {
		data[0].Key16 = "Synced";
	}
	FnaVariables.fnaKeys.Key12 = data[0].Key12;
	FnaVariables.fnaKeys.Key13 = data[0].Key13;
	FnaVariables.fnaKeys.Key15 = data[0].Key15;
	FnaVariables.transTrackingID = data[0].TransTrackingID;
	FnaVariables.leadId = data[0].Key1;
	
	FnaVariables.setFnaModel({
		'FNA' : JSON.parse(JSON.stringify(data[0].TransactionData))
	});
	
	if(data[0].Key2 == ""){
		data[0].Key2 = 0;
	}
	$scope.FNAObject = FnaVariables.getFnaModel();
	globalService.setParties($scope.FNAObject.FNA.parties);
	FnaVariables.fnaId = data[0].Key2;
	$rootScope.transactionId = data[0].Id;
	FnaVariables.choosePartyStatus = false;
	$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
	$scope.refresh();
	if (data[0].Key15 == "Completed") 	
		$location.path('/fnaRecomendedProducts');
	/* FNA changes made by LE Team start */
	else if (data[0].Key15 == "FNA Report sent") 	
		$location.path('/fnaReport');
	/* FNA changes made by LE Team end */
	else 	{
		$rootScope.showHideLoadingImage(false, 'paintUIMessage', $translate);
		var lastVisitedPage = "/aboutCompany/" + data[0].Key2	+ "/"	+ data[0].Id + "/false/";
		for (var i = 0; i < FnaVariables.pages.length; i++) {
			if (FnaVariables.pages[i].pageName == FnaVariables.getFnaModel().FNA.lastVisitedPage ) {
				//lastVisitedPage = FnaVariables.pages[i].url + "/"+  data[0].Key2	+ "/"	+ data[0].Id + "/false/";
				lastVisitedPage = FnaVariables.pages[i].url;
				break;
			}
		}
		$location.path(lastVisitedPage);
    }
	
	$scope.refresh();

};
// Get  Error
$scope.onGetFnaListingsError = function() {
	//alert("error");
};


/* fna edit mode */
$scope.setFnaData = function(fnaData,fnaId,transId) 
{
	FnaVariables.setFnaModel({
		'FNA' : JSON
				.parse(JSON
						.stringify(fnaData.TransactionData))
	});
	$scope.FNAObject = FnaVariables.getFnaModel();
	globalService.setParties($scope.FNAObject.FNA.parties);
	FnaVariables.fnaId = fnaId;
	$rootScope.transactionId = transId;
};

$scope.showDob = function(dob)
{
	if(dob == "NaN-NaN-NaN")
		return "";
	else
		return dob;
}

$scope.editFNA = function(fnaId, transId,fnaData) {
	FnaVariables.transactionID = transId;
	$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
	$scope.refresh();
	DataService.getListingDetail(fnaData, $scope.onGeFnatListingsSuccess, $scope.onGetFnaListingsError);
}

if($routeParams.status=='status'){
	$rootScope.selectedPage = "fnaListing/status";
} else {
	$rootScope.selectedPage = "fnaListing/";
}

//repainting to add search values to scope
$scope.initialLoad= function(){
if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
	}    
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5');
	}
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5');
	}
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5');
	}
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5');
	}  
LEDynamicUI
.paintUI(rootConfig.template, $scope.listUiJsonFile, "FNAList",
		"#fnaList", true, function() {
			$scope.callback();
			PersistenceMapping.clearTransactionKeys();
			$scope.mapKeysforPersistence();
			var  searchCriteria = {
						 searchCriteriaRequest:{
				"command" : "ListingDashBoard",
				"modes" :['FNA'],
				"value" : ""
				}
			};
			var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
			transactionObj.Id = $routeParams.transactionId;
			transactionObj.Key2 = null;
			transactionObj.Key10 = "BasicDetails";
			transactionObj.filter = "Key15 <> 'Cancelled'";
			$scope.searchFNA=$rootScope.searchValue;
			$rootScope.searchValue="";
			$scope.statusFilter = FnaVariables.stausFilter;
			FnaVariables.stausFilter = "";
			$scope.searchFilter = FnaVariables.searchFilter;
			FnaVariables.searchFilter = "";
			DataService.getFilteredListing(transactionObj,
					$scope.onGetListingsSuccess,
					$scope.onGetListingsError);
		}, $scope, $compile);
}	
		
		 $scope.$on("$destroy", function() {
                 if (this.$$destroyed) return;     
				 while (this.$$childHead) {      
				 this.$$childHead.$destroy();    
				 }     
				 if (this.$broadcast)
				{ 
					this.$broadcast('$destroy');   
				}    
				 this.$$destroyed = true;                 
				 //$timeout.cancel( timer );                           
        });
    /* FNA changes by LE Team start */
    $scope.onGetListingsSuccess = function(data) {
		if (!rootConfig.isDeviceMobile) {
			$scope.fnaList = data.sort(function(a, b) {
				return LEDate(b.Key14)
							- LEDate(a.Key14);
			});
		} else {
            customSortForListing(data,function(data){
                    $scope.fnaList = data;
                            });
			//$scope.fnaList = data;
		}
		$scope.tableFNAList =  [].concat($scope.fnaList);
		$scope.refresh();
		$rootScope.showHideLoadingImage(false);
	}
    function customSortForListing(data, callBack) {                                
                                    data.sort(function(a,b){
                                       if (rootConfig.isDeviceMobile) {
                                            b.modifiedDate = b.modifiedDate ? b.modifiedDate : '1900-01-01 01:01:01';
                                            a.modifiedDate = a.modifiedDate ? a.modifiedDate : '1900-01-01 01:01:01';
                                        }
                                        return new Date(b.modifiedDate) - new Date(a.modifiedDate);
                                     });
                                
                                /*$timeout(function() {
                                   callBack(data);
                                },0);*/
                                 callBack(data);
                            }
    /* FNA changes by LE Team end */

}]);