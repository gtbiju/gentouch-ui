﻿/*
 *Copyright 2015, LifeEngage 
 */



'use strict';


storeApp.controller('GoalShortfallController', ['$rootScope', '$scope', 'DataService', 'FnaService','LookupService', 'DocumentService', 'ProductService','$compile', 
                                                '$routeParams', '$location', '$translate','IllustratorVariables', 'FnaVariables', 'UtilityService','$debounce', 'PersistenceMapping', 'AutoSave',
                                                 'globalService','$timeout','UtilityVariables',
                                                function($rootScope, $scope, DataService, FnaService, LookupService,DocumentService, ProductService, $compile, $routeParams, $location, $translate, 
                                                IllustratorVariables,FnaVariables, UtilityService,$debounce, PersistenceMapping, AutoSave, globalService,$timeout,UtilityVariables) {

	/* CHART STARTS */

	FnaVariables.selectedPage = "Summary";
	$scope.percentOption1 = 25;
	$scope.selectedColor = "#41ad48";
	$scope.anotherOptions1 = {
		animate : {
			duration : 0,
			enabled : false
		},
		barColor : '#3b9ac0',
		scaleColor : false,
		lineWidth : 54,
		lineCap : 'circle',
		trackColor : '#034f6d'
	};
	$scope.changeBgColor = function() {
		
		// $scope.anotherOptions1.barColor = $scope.selectedColor;
	};
	if($rootScope.theme == 'mainTheme'){
		$scope.anotherOptions1.barColor = '#3b9ac0';
		$scope.anotherOptions1.trackColor = '#034f6d';
	}else if($rootScope.theme == 'abcTheme'){
		$scope.anotherOptions1.barColor = '#ffd31b';
		$scope.anotherOptions1.trackColor = '#aa0724';
	}
	/* CHART ENDS */
	var model = "FNAObject";
	if ((rootConfig.autoSave.fna)) {
		AutoSave.setupWatchForScope(model, DataService, UtilityService, "",
				$scope, $debounce, $translate, $routeParams, FnaService);
	}
	$scope.fnaName = 'FNA Controller';
	$scope.allShortfall = 0;
	$scope.allPieChartPercent = 0;
	$scope.FNAObject = FnaVariables.getFnaModel();

	$scope.goals = [];
	$scope.goals = FnaVariables.goals;
	UtilityService.disableProgressTab = false;
	$scope.selectedGoals = [];
	$scope.selectedGoals.push({});
	$scope.FNAMyself = globalService.getParty("FNAMyself");
	if ($scope.FNAMyself.BasicDetails.photo == ""
			|| $scope.FNAMyself.BasicDetails.photo == undefined) {
		$scope.insuredImage = FnaVariables.myselfPhoto;
	} else {
		$scope.insuredImage = $scope.FNAMyself.BasicDetails.photo;
	}
	var count = 1;
	for (var k = 0; k < $scope.FNAObject.FNA.goals.length; k++) {
		if ($scope.FNAObject.FNA.goals[k].result != undefined) {
			$scope.selectedGoals.push($scope.FNAObject.FNA.goals[k]);
			$scope.selectedGoals[count++].localPriority = count;
		}
	}
	$scope.refresh();
	
	for (var i = 0; i < $scope.selectedGoals.length; i++) {
		for (var j = 0; j < $scope.goals.length; j++) {
			if ($scope.selectedGoals[i].goalName == $scope.goals[j].goalName) {
				$scope.selectedGoals[i].goalicon = $scope.goals[j].goalicon;
				$scope.allShortfall = Number($scope.allShortfall)
						+ Number($scope.selectedGoals[i].result.shortfall);
				$scope.allPieChartPercent = Number($scope.allPieChartPercent)
						+ Number($scope.selectedGoals[i].result.pieChartPercent);
				$scope.refresh();
			}
		}
	}

	var allChart = {};
	// allChart.priority=Number($scope.selectedGoals.length)+Number(1);
	allChart.localPriority = 1;
	allChart.goalicon = "all_text";
	allChart.result = {};
	allChart.result.pieChartPercent = $scope.allPieChartPercent;
	allChart.result.shortfall = $scope.allShortfall;
	$scope.selectedGoals[0] = allChart;

	$scope.nextPage = function() {
		for (var i = 0; i < FnaVariables.pages.length; i++) {
			if (FnaVariables.pages[i].url == "/questioner") {
				$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
				break;
			}
		}
		FnaVariables.setFnaModel($scope.FNAObject);
		var obj = new FnaService.saveTransactions($scope, $rootScope,
				DataService, $translate, UtilityService, false);
		obj.save(function() {
			$location.path('/questioner');
		});

	}

	$scope.chartData = [];
	$scope.legend_array = [];
	$scope.xaxis = "No of Years";
	$scope.yaxis = "Short Fall Amount";

	for (var i = 1; i < $scope.selectedGoals.length; i++) {
		if ($scope.selectedGoals[i].result != undefined) {
			$scope.chartData.push($scope.selectedGoals[i].result.graphOutput);
			$scope.legend_array.push($scope.selectedGoals[i].goalName);

		}
	}

	$scope.selectedGoals[0].result.graphOutput = $scope.chartData;

	$scope.id = "line_chart";

	$scope.type = "linechart";

	$scope.showDetail = function(index) {

		$scope.legend_array = [];
		$scope.legend_array.push($scope.selectedGoals[index].goalName);

		if (index == 0) {
			$scope.chartData = $scope.selectedGoals[index].result.graphOutput;
			$scope.legend_array = [];
			for (var i = 1; i < $scope.selectedGoals.length; i++) {
				$scope.legend_array.push($scope.selectedGoals[i].goalName);
			}
		} else {
			$scope.chartData = [];
			$scope.chartData
					.push($scope.selectedGoals[index].result.graphOutput);
		}
	}
}]);
