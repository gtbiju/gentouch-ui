/*
 *Copyright 2015, LifeEngage 
 */



'use strict';

storeApp.controller('AboutCompanyController', ['$rootScope', '$scope', 'DataService', 'FnaService', 'DocumentService', 'ProductService', '$compile', '$routeParams', '$location', '$translate', 'FnaVariables', 'UtilityService', 'PersistenceMapping', 'AutoSave', 'globalService','AgentService',
                                               function($rootScope, $scope, DataService, FnaService, DocumentService, ProductService, $compile, $routeParams, $location, $translate, FnaVariables, UtilityService, PersistenceMapping, AutoSave, globalService,AgentService) {

	$scope.fnaName = 'FNA Controller';
	
	$scope.performanceDetails = [];
	$scope.revenueDetails = [];
	FnaVariables.selectedPage = "About Company";
	$scope.selectedpage = FnaVariables.selectedPage;
	//To prepopulate data from LMS
	$scope.isFromLMS = $routeParams.isFromLMS;
	if ($scope.isFromLMS && $scope.isFromLMS == "true") {
		FnaVariables.setFnaModel("");
	}
	$scope.FNAObject = FnaVariables.getFnaModel();
	
	
	$scope.initialLoad = function(){
		LEDynamicUI.getUIJson(rootConfig.FnaConfigJson, false, function(FnaConfigJson) {
			FnaVariables.goals = FnaConfigJson.goals;
			FnaVariables.lifeStageGoals = FnaConfigJson.lifeStageGoals;
			FnaVariables.familyMembers = FnaConfigJson.familyMembers;
			FnaVariables.riskList = FnaConfigJson.riskList;
			FnaVariables.aboutCompany = FnaConfigJson.aboutCompany;
			FnaVariables.custLifeStagesList = FnaConfigJson.custLifeStages;
			FnaVariables.custGenderSelectionsList = FnaConfigJson.custGenderSelections;			
			FnaVariables.pages = FnaConfigJson.pages;	
			FnaVariables.pageTypes = FnaConfigJson.pageTypes;	
			$scope.performanceDetails = FnaVariables.aboutCompany.performanceDetails;
			$scope.revenueDetails = FnaVariables.aboutCompany.revenueDetails;
			$scope.refresh();
		});
	}
	
	$rootScope.moduleHeader = "fna.fnaHeading";
	$rootScope.moduleVariable = translateMessages($translate,
			$rootScope.moduleHeader);

	$scope.agent = {};
	$scope.achievementList = [];
	$scope.onRetrieveAgentProfileSuccess = function(data) {
		if ((rootConfig.isDeviceMobile)) {
			$scope.achievementList = data;
			/*FNA changes by LE Team>> dummy data starts */
			/*$scope.achievementList = [{"title":"1"},{"title":"2"},{"title":"3"},{"title":"4"},{"title":"5"},{"title":"6"},{"title":"7"},{"title":"8"},
			{"title":"9"},{"title":"10"},{"title":"11"},{"title":"12"},{"title":"13"},{"title":"14"}];*/
			/*FNA changes by LE Team>> dummy data ends */
			var leFileUtils = new LEFileUtils();
			leFileUtils.getApplicationPath(function(documentsPath) {
				if ((rootConfig.isOfflineDesktop)) {
					$scope.fileFullPath = "file://" + documentsPath;
				} else {
					$scope.fileFullPath = "file://" + documentsPath + "/";
				}
			});

		} else {
			$scope.agent = data.AgentDetails;
			$scope.achievementList = data.Achievements;
			$scope.FNAObject.FNA.AgentDetails = $scope.agent;
			$scope.fileFullPath = rootConfig.mediaURL;
		}
		
		if($scope.achievementList!=undefined){
			$scope.achievementBadgeCount = $scope.achievementList.length;
		}else{
			$scope.achievementBadgeCount = 0;
		}
		$scope.refresh();
	};


	PersistenceMapping.clearTransactionKeys();
	var transactionObj = PersistenceMapping.mapScopeToPersistence({});
	if ((rootConfig.isDeviceMobile)) {
		DataService.retrieveAgentDetails(transactionObj, function(data) {
			$scope.agent = data[0];
			$scope.FNAObject.FNA.AgentDetails = $scope.agent;
			DataService.retrieveAchievements(transactionObj, $scope.onRetrieveAgentProfileSuccess, function(error) {console.log(error)});
		}, $scope.onRetrieveAgentDetailsError);
	} else {
		AgentService.retrieveAgentProfile(transactionObj, $scope.onRetrieveAgentProfileSuccess, function(error) {console.log(error)});
	}
	
	$scope.nextPage = function() {
		try {
			for (var i = 0; i < FnaVariables.pages.length; i++) {
				if (FnaVariables.pages[i].url == "/aboutme") {
					$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
					break;
				}
			}

			// To prepopulate data from LMS
			if ($scope.isFromLMS && $scope.isFromLMS == "true") {
				globalService.setPartyFromParty("FNAMyself", "Lead");
				var parties = globalService.getParties();
				$scope.FNAObject.FNA.parties = parties;
			}

			FnaVariables.setFnaModel($scope.FNAObject);
			$location.path('/aboutme');
		} catch (exception) {
			alert("Error Output " + exception);
		}
	};
	
	$scope.$on('$viewContentLoaded', function(){
		$scope.initialLoad();
	});
}]);
