/*Name:GLI_GoalShortfallController
 * For implementing GLI specific changes
 */
storeApp.controller('GLI_GoalShortfallController',['$timeout', '$rootScope', '$scope','$location','$compile', '$translate', 'UtilityService', 'DataService', '$debounce', '$routeParams','GLI_FnaService', 'RuleService', 'AutoSave', 'globalService','FnaVariables','$controller',
function GLI_GoalShortfallController($timeout, $rootScope, $scope, $location,$compile, $translate,UtilityService,DataService,$debounce, $routeParams, GLI_FnaService, RuleService, AutoSave, globalService,FnaVariables,$controller){
$controller('GoalShortfallController',{$timeout:$timeout, $rootScope:$rootScope, $scope:$scope,$location:$location, $compile:$compile,$translate:$translate, UtilityService:UtilityService, DataService:DataService, $debounce:$debounce, $routeParams:$routeParams,FnaService:GLI_FnaService, RuleService:RuleService, AutoSave:AutoSave, globalService:globalService,FnaVariables:FnaVariables});

$scope.translateLabel = function(value) {
	return $translate.instant(value);
}

$scope.status = FnaVariables.getStatusOptionsFNA()[0].status;
/*Added*/
$scope.fnaName = 'FNA Controller';
$scope.allShortfall = 0;
$scope.goals = [];
$scope.goals = FnaVariables.goals;
$scope.summaryGraph = FnaVariables.summaryGraph;
$rootScope.selectedPage = "GoalShortfall";
/*Added*/

for ( var i = 1; i < $scope.selectedGoals.length; i++) {
	for ( var j = 0; j < $scope.goals.length; j++) {
		if ($scope.selectedGoals[i].goalName == $scope.goals[j].goalName) {
			$scope.selectedGoals[i].goalicon = $scope.goals[j].goalicon;
			$scope.allShortfall = Number($scope.allShortfall)
					+ Number($scope.selectedGoals[i].result.shortfall);
			$scope.allPieChartPercent = Number($scope.allPieChartPercent)
					+ Number($scope.selectedGoals[i].result.pieChartPercent);
			$scope.refresh();
		}
	}
}

$scope.selectedGoals.shift();
$scope.FNAObject.FNA.selectedGoals = $scope.selectedGoals;
allChart = {};
//for showing shortfall in thousands
for (var i=0;i<$scope.FNAObject.FNA.selectedGoals.length;i++){
	if(typeof $scope.FNAObject.FNA.selectedGoals[i].result.needGapfnaReport != "undefined" && $scope.FNAObject.FNA.selectedGoals[i].result.needGapfnaReport != ""){
		$scope.FNAObject.FNA.selectedGoals[i].result.needGapSummary = Math.round($scope.FNAObject.FNA.selectedGoals[i].result.needGapfnaReport/1000);
	}else if(typeof $scope.FNAObject.FNA.selectedGoals[i].result.futureNeedGapfnaReport != "undefined" && $scope.FNAObject.FNA.selectedGoals[i].result.futureNeedGapfnaReport != ""){
		$scope.FNAObject.FNA.selectedGoals[i].result.futureNeedGapSummary = Math.round($scope.FNAObject.FNA.selectedGoals[i].result.futureNeedGapfnaReport/1000);
	}
}
for (var i=0 ; i < $scope.FNAObject.FNA.selectedGoals.length ; i++){
	if(typeof $scope.FNAObject.FNA.selectedGoals[i].result.needGap != "undefined" && $scope.FNAObject.FNA.selectedGoals[i].result.needGap != ""){
		if($scope.FNAObject.FNA.selectedGoals[i].result.needGap < 0){
			$scope.FNAObject.FNA.selectedGoals[i].result.surplusShow = true;
		}else{
			$scope.FNAObject.FNA.selectedGoals[i].result.surplusShow = false;
		}
	}
	if(typeof $scope.FNAObject.FNA.selectedGoals[i].result.futureNeedGap != "undefined" && $scope.FNAObject.FNA.selectedGoals[i].result.futureNeedGap != ""){
		if($scope.FNAObject.FNA.selectedGoals[i].result.futureNeedGap < 0){
			$scope.FNAObject.FNA.selectedGoals[i].result.surplusShow = true;
		}else{
			$scope.FNAObject.FNA.selectedGoals[i].result.surplusShow = false;
		}
	}
}

$scope.id = "bar_chart";

$scope.type = "barChart";
$scope.showDetail = function(index) {
	
		
		$scope.labels = $scope.selectedGoals[index].result.labels;
		$scope.xAxisLabels = $scope.selectedGoals[index].result.xAxisLabels;
		
		$scope.needGapForSummary = $scope.selectedGoals[index].result.needGapForSummary;
		$scope.isCostofWaitingHidden = false;
		//$scope.isCarousel = true;
    
		if($scope.selectedGoals[index].goalId != "Retirement"){
           LEDynamicUI.getUIJson(rootConfig.FnaSummaryGraphJson, false, function(graphJson) {

			
			
			$scope.graphJson = 	graphJson;
			//FNA changes by LE Team
			if($scope.selectedGoals[index].goalId == "Protection" || $scope.selectedGoals[index].goalId == "Health" || $scope.needGapForSummary < 0 || $scope.selectedGoals[index].result.montlySavingsArr.length == 0)
			{
				//$scope.isCarousel = false;
				$scope.isCostofWaitingHidden = true;
				
				
				$scope.graphJson.Views[0].sections[0].subsections[0].controlGroups[0].controls.splice(0,1);		
				$scope.graphJson.Views[0].sections[0].subsections[0].controlGroups[0].isCarousel = false;
			}
			$scope.index = index;
			$scope.legend_array = [];
			$scope.legend_array.push($scope.selectedGoals[index].goalName);
			$scope.dataArray19 = [];

			//for(var i=0; i<Object.keys($scope.selectedGoals).length; i++)
			
			if(typeof $scope.selectedGoals[index].result.montlySavingsArr != "undefined"){
				for(var d=0; d<Object.keys($scope.selectedGoals[index].result.montlySavingsArr).length; d++)
				{		
					var accumlatedArray = [];
					var constant  = 1000;
                    /* FNA Changes by LE Team Start */
					//accumlatedArray.push(d,Math.round($scope.selectedGoals[index].result.montlySavingsArr[d]/Number(constant)));
                    accumlatedArray.push(d,Math.round($scope.selectedGoals[index].result.montlySavingsUnitArr[d]/Number(constant)));
                    /* FNA Changes by LE Team End */
					$scope.dataArray19.push(accumlatedArray);
                    /* FNA Changes by LE Team Start */
					/*for ( var j = 0; j < $scope.goals.length; j++) {
						if ($scope.selectedGoals[index].goalName == $scope.goals[j].goalName) {
							$scope.costOfDelayDesc = $scope.goals[j].goalCostOfDelay;
						}
					}*/
                    /* FNA Changes by LE Team End */
				}
			}
		      /* FNA Changes by LE Team Start */
				/* for ( var j = 0; j < $scope.goals.length; j++) {
					if ($scope.selectedGoals[index].goalName == $scope.goals[j].goalName) {
						if(typeof $scope.selectedGoals[index].result.needGap != "undefined" && $scope.selectedGoals[index].result.needGap != ""){
							if($scope.selectedGoals[index].result.needGap < 0){
								$scope.surplusShowDesc = $scope.goals[j].goalSurplus;
							}else{
								$scope.surplusShowDesc = $scope.goals[j].goalShortfall;
							}
						}
						if(typeof $scope.selectedGoals[index].result.futureNeedGap != "undefined" && $scope.selectedGoals[index].result.futureNeedGap != ""){
							if($scope.selectedGoals[index].result.futureNeedGap < 0){
								$scope.surplusShowDesc = $scope.goals[j].goalSurplus;
							}else{
								$scope.surplusShowDesc = $scope.goals[j].goalShortfall;
							}
						}
					}
				} */    
            /* FNA Changes by LE Team End */		
			if (index == 0) {
				$scope.chartData = $scope.dataArray19;
				$scope.legend_array = [];
				for (var i = 1; i < $scope.selectedGoals.length; i++) {
					$scope.legend_array.push($scope.selectedGoals[i].goalName);
				}
			} else {
				$scope.chartData = [];
				$scope.chartData
						.push($scope.dataArray19);
			}
			$('#fnaSummaryGraph').empty();
			
			LEDynamicUI.paintUIFromJsonObject(rootConfig.template,  $scope.graphJson , "fnaSummaryGraph", "#fnaSummaryGraph", true, function() {
				$timeout(function(){
					$('.illustrate_graph_container').removeClass('graph-visible');
				});
			}, $scope, $compile);
		
		}); 
        }
		
		if($scope.selectedGoals[index].goalId == "Retirement"){
        LEDynamicUI.getUIJson(rootConfig.FnaSummaryGraphRetirementJson, false, function(graphJson) {

			
			
			$scope.graphJson = 	graphJson;
			//FNA changes by LE Team
			if($scope.selectedGoals[index].goalId == "Protection" || $scope.selectedGoals[index].goalId == "Health" || $scope.needGapForSummary < 0 || $scope.selectedGoals[index].result.montlySavingsArr.length == 0)
			{
				//$scope.isCarousel = false;
				$scope.isCostofWaitingHidden = true;				
				
				$scope.graphJson.Views[0].sections[0].subsections[0].controlGroups[0].controls.splice(1,1);		
				$scope.graphJson.Views[0].sections[0].subsections[0].controlGroups[0].isCarousel = false;
			}
			$scope.index = index;
			$scope.legend_array = [];
			$scope.legend_array.push($scope.selectedGoals[index].goalName);
			$scope.dataArray19 = [];

			//for(var i=0; i<Object.keys($scope.selectedGoals).length; i++)
			
			if(typeof $scope.selectedGoals[index].result.montlySavingsArr != "undefined"){
				for(var d=0; d<Object.keys($scope.selectedGoals[index].result.montlySavingsArr).length; d++)
				{		
					var accumlatedArray = [];
					var constant  = 1000;
                    /* FNA Changes by LE Team Start */
					//accumlatedArray.push(d,Math.round($scope.selectedGoals[index].result.montlySavingsArr[d]/Number(constant)));
                    accumlatedArray.push(d,Math.round($scope.selectedGoals[index].result.montlySavingsUnitArr[d]/Number(constant)));
                    /* FNA Changes by LE Team End */
					$scope.dataArray19.push(accumlatedArray);
                    /* FNA Changes by LE Team Start */
					/*for ( var j = 0; j < $scope.goals.length; j++) {
						if ($scope.selectedGoals[index].goalName == $scope.goals[j].goalName) {
							$scope.costOfDelayDesc = $scope.goals[j].goalCostOfDelay;
						}
					}*/
                    /* FNA Changes by LE Team End */
				}
			}
		      /* FNA Changes by LE Team Start */
				/* for ( var j = 0; j < $scope.goals.length; j++) {
					if ($scope.selectedGoals[index].goalName == $scope.goals[j].goalName) {
						if(typeof $scope.selectedGoals[index].result.needGap != "undefined" && $scope.selectedGoals[index].result.needGap != ""){
							if($scope.selectedGoals[index].result.needGap < 0){
								$scope.surplusShowDesc = $scope.goals[j].goalSurplus;
							}else{
								$scope.surplusShowDesc = $scope.goals[j].goalShortfall;
							}
						}
						if(typeof $scope.selectedGoals[index].result.futureNeedGap != "undefined" && $scope.selectedGoals[index].result.futureNeedGap != ""){
							if($scope.selectedGoals[index].result.futureNeedGap < 0){
								$scope.surplusShowDesc = $scope.goals[j].goalSurplus;
							}else{
								$scope.surplusShowDesc = $scope.goals[j].goalShortfall;
							}
						}
					}
				} */    
            /* FNA Changes by LE Team End */		
			if (index == 0) {
				$scope.chartData = $scope.dataArray19;
				$scope.legend_array = [];
				for (var i = 1; i < $scope.selectedGoals.length; i++) {
					$scope.legend_array.push($scope.selectedGoals[i].goalName);
				}
			} else {
				$scope.chartData = [];
				$scope.chartData
						.push($scope.dataArray19);
			}
			$('#fnaSummaryGraph').empty();
			
			LEDynamicUI.paintUIFromJsonObject(rootConfig.template,  $scope.graphJson , "fnaSummaryGraphRetirement", "#fnaSummaryGraph", true, function() {
				$timeout(function(){
					$('.illustrate_graph_container').removeClass('graph-visible');
				});
			}, $scope, $compile);
		
		});
        }			
	}
$scope.showDetail(0);

$scope.$on('initiate-save', function (evt, obj) {
	//$scope.disableLePopCtrl = true;
	globalService.setParty($scope.FNAMyself, "FNAMyself");
	var parties = globalService.getParties();
	$scope.FNAObject.FNA.parties = parties;		
	FnaVariables.setFnaModel($scope.FNAObject);
	$scope.status = "Draft";
	var obj = new GLI_FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
	obj.save(function() {
		$rootScope.showHideLoadingImage(false, "");
		$rootScope.passwordLock  = false;
		$rootScope.isAuthenticated = false;
		$location.path('/login');
	});
});
//$scope.selectedGoals = [{"goalName":"Wealth Accumulation","translateID":"fna.wealthAccumulation","sliderUnit":"K","priority":"1","parameters":[{"name":"investmentPurposeSlider","unit":"NA","textBoxValue":0},{"name":"currentAgeSlider","unit":"NA","textBoxValue":27,"value":27},{"name":"investmentCostSlider","unit":"K","textBoxValue":5000,"value":5000000},{"name":"investmentRealiseSlider","unit":"NA","textBoxValue":60,"value":60},{"name":"infaltionRateSlider","unit":"NA","textBoxValue":10,"value":10},{"name":"investmentReturnRateSlider","unit":"NA","textBoxValue":12,"value":12},{"name":"investmentFundSlider","unit":"K","textBoxValue":50,"value":50000}],"productList":[],"priorityInfo":"","ruleName":"SavingsGoalCalculator.js","functionName":"SavingsGoalCalculator","goalicon":"options1","$$hashKey":"0J0","result":{"currentAgeSlider":27,"investmentCostSlider":5000000,"investmentRealiseSlider":60,"infaltionRateSlider":10,"investmentReturnRateSlider":12,"investmentFundSlider":50000,"shortfall":12121200,"ruleExecutionOutput":[{"amountRequired":116125772,"projectSavings":2104577,"futureNeedGap":114021195}]},"localPriority":2},{"goalName":"Retirement","translateID":"fna.retireName","sliderUnit":"K","priority":"2","parameters":[{"name":"currentAgeSlider","unit":"NA","textBoxValue":27,"value":27},{"name":"retirementApproxLifeSlider","unit":"NA","textBoxValue":70,"value":70},{"name":"retirementPensionAgeSlider","unit":"NA","textBoxValue":70,"value":70},{"name":"retirementMonthlyExpensesSlider","unit":"K","textBoxValue":10,"value":10000},{"name":"infaltionRateSlider","unit":"NA","textBoxValue":10,"value":10},{"name":"interestRateSlider","unit":"NA","textBoxValue":12,"value":12},{"name":"retirementPensionBalanceSlider","unit":"K","textBoxValue":15,"value":15000},{"name":"savingsRateSlider","unit":"NA","textBoxValue":3,"value":3},{"name":"retirementMonthlySavingsPensionSlider","unit":"K","textBoxValue":20,"value":20000},{"name":"retirementAnnualIncreaseSlider","unit":"NA","textBoxValue":5,"value":5},{"name":"retirementPensionRateSlider","unit":"NA","textBoxValue":10,"value":10}],"productList":[],"priorityInfo":"","ruleName":"RetirementGoalCalculator.js","functionName":"RetirementGoalCalculator","goalicon":"options2","$$hashKey":"0J3","result":{"currentAgeSlider":27,"retirementApproxLifeSlider":70,"retirementPensionAgeSlider":70,"retirementMonthlyExpensesSlider":10000,"infaltionRateSlider":10,"interestRateSlider":12,"retirementPensionBalanceSlider":15000,"savingsRateSlider":3,"retirementMonthlySavingsPensionSlider":20000,"retirementAnnualIncreaseSlider":5,"retirementPensionRateSlider":10,"shortfall":1232123,"ruleExecutionOutput":[{"corpusRequired":null,"corpusPlanned":0,"needGap":null}],"graphOutput":[[2014,null],[null,null]],"pieChartPercent":15},"localPriority":3},{"goalName":"Protection","translateID":"general.productType.Protection","sliderUnit":"K","priority":"3","parameters":[{"name":"protectionCurrentMonthlyExpensesSlider","unit":"K","textBoxValue":50,"value":50000},{"name":"protectionOutstandingLoansSlider","unit":"K","textBoxValue":100,"value":100000},{"name":"protectionLiquidAssetsSlider","unit":"K","textBoxValue":50,"value":50000},{"name":"protectionDependencyPeriodSlider","unit":"NA","textBoxValue":20,"value":20},{"name":"protectionLifeInsuranceCoverSlider","unit":"K","textBoxValue":400,"value":400000},{"name":"assumptionInfaltionRateSlider","unit":"NA","textBoxValue":10,"value":10},{"name":"protectionEstimatedReturnSlider","unit":"NA","textBoxValue":12,"value":12},{"name":"protectionTravelInMonthSlider","unit":"NA"},{"name":"protectionChoiceofRoomSlider","unit":"NA"},{"name":"protectionCostforCriticalIllnessSlider","unit":"NA","textBoxValue":20000000,"value":20000000},{"name":"protectionYearsForCoverageSlider","unit":"NA","textBoxValue":10,"value":10}],"productList":[],"priorityInfo":"","ruleName":"ProtectionGoalCalculator.js","functionName":"ProtectionGoalCalculator","goalicon":"options3","$$hashKey":"0J6","result":{"protectionCurrentMonthlyExpensesSlider":50000,"protectionOutstandingLoansSlider":100000,"protectionLiquidAssetsSlider":50000,"protectionDependencyPeriodSlider":20,"protectionLifeInsuranceCoverSlider":400000,"assumptionInfaltionRateSlider":10,"protectionEstimatedReturnSlider":12,"protectionCostforCriticalIllnessSlider":20000000,"protectionYearsForCoverageSlider":10,"shortfall":123213213,"ruleExecutionOutput":[{"corpusRequired":null,"corpusPlanned":0,"needGap":null}],"graphOutput":[[2014,null],[2054,null]],"pieChartPercent":15},"localPriority":4},{"goalName":"Wealth Transfer","translateID":"fna.wealthTransfer","sliderUnit":"K","priority":"4","parameters":[{"name":"wealthTransferAmountOfAssetSlider","unit":"K","textBoxValue":50000,"value":50000000},{"name":"wealthTransferAssetTransferSlider","unit":"NA","textBoxValue":5,"value":5},{"name":"wealthTransferTimeforAssetSlider","unit":"NA","textBoxValue":10,"value":10},{"name":"infaltionRateSlider","unit":"NA","textBoxValue":10,"value":10},{"name":"investmentRateSlider","unit":"NA","textBoxValue":12,"value":12},{"name":"savingsRateSlider","unit":"NA","textBoxValue":3,"value":3}],"productList":[],"priorityInfo":"","ruleName":"DisabilityGoalCalculator.js","functionName":"DisabilityGoalCalculator","goalicon":"options4","$$hashKey":"0J9","result":{"wealthTransferAmountOfAssetSlider":50000000,"wealthTransferAssetTransferSlider":5,"wealthTransferTimeforAssetSlider":10,"infaltionRateSlider":10,"investmentRateSlider":12,"savingsRateSlider":3,"shortfall":123213213,"ruleExecutionOutput":[{"corpusRequired":null,"corpusPlanned":0,"needGap":null}],"graphOutput":[[2014,null],[null,null]],"pieChartPercent":15},"localPriority":5},{"goalName":"Education","translateID":"education","sliderUnit":"K","priority":"5","parameters":[{"name":"educationChildAgeSlider","unit":"NA","textBoxValue":1,"value":1},{"name":"educationChildNameSlider","unit":"NA","textBoxValue":0},{"name":"infaltionRateSlider","unit":"NA","textBoxValue":10,"value":10},{"name":"educationInvestInterestRateSlider","unit":"NA","textBoxValue":12,"value":12},{"name":"savingsRateSlider","unit":"NA","textBoxValue":3,"value":3},{"name":"educationLevelOfEducation1Slider","unit":"NA"},{"name":"educationEntryAge1Slider","unit":"NA","textBoxValue":18,"value":18},{"name":"educationEducationCost1Slider","unit":"K","textBoxValue":30000,"value":30000000},{"name":"educationLivingCost1Slider","unit":"K","textBoxValue":0,"value":0},{"name":"educationLumpsum1Slider","unit":"NA","textBoxValue":100000,"value":100000},{"name":"monthlySavings1Slider","unit":"NA","textBoxValue":10000,"value":10000}],"productList":[],"priorityInfo":"","ruleName":"DisabilityGoalCalculator.js","functionName":"DisabilityGoalCalculator","goalicon":"options5","$$hashKey":"0JC","result":{"educationChildAgeSlider":1,"infaltionRateSlider":10,"educationInvestInterestRateSlider":12,"savingsRateSlider":3,"educationEntryAge1Slider":18,"educationEducationCost1Slider":30000000,"educationLivingCost1Slider":0,"educationLumpsum1Slider":100000,"monthlySavings1Slider":10000,"shortfall":1232323,"ruleExecutionOutput":[{"corpusRequired":null,"corpusPlanned":0,"needGap":null}],"graphOutput":[[2014,null],[null,null]],"pieChartPercent":15},"localPriority":6}];
//Extending nextpage function to set the status
$scope.nextPage = function() {
    /* FNA Changes made by LE Team Start */
    if(FnaVariables.fnaKeys.Key15 == FnaVariables.getStatusOptionsFNA()[0].status) {
		FnaVariables.fnaKeys.Key15  = FnaVariables.getStatusOptionsFNA()[1].status;
		$scope.status = FnaVariables.getStatusOptionsFNA()[1].status;
	}	
	else if(FnaVariables.fnaKeys.Key15 == FnaVariables.getStatusOptionsFNA()[2].status){
		FnaVariables.fnaKeys.Key15  = FnaVariables.getStatusOptionsFNA()[2].status;
		$scope.status = FnaVariables.getStatusOptionsFNA()[2].status;
	}
    /* FNA Changes made by LE Team End */
	for (var i = 0; i < FnaVariables.pages.length; i++) {
		
		// FNA changes by LE Team		
		if (FnaVariables.pages[i].url == "/fnaRecomendedProducts") {
			$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
			break;
		}
	}
	for (var i = 0; i < $scope.selectedGoals.length; i++) {
		for (var j = 0; j < $scope.goals.length; j++) {
			if ($scope.selectedGoals[i].goalName == $scope.goals[j].goalName) {
				$scope.selectedGoals[i].goalicon = $scope.goals[j].goalicon;
				$scope.allShortfall = Number($scope.allShortfall)
						+ Number($scope.selectedGoals[i].result.shortfall);
				$scope.allPieChartPercent = Number($scope.allPieChartPercent)
						+ Number($scope.selectedGoals[i].result.pieChartPercent);
				$scope.refresh();
			}
		}
	}

	//$scope.selectedGoals.shift();
	//$scope.FNAObject.FNA.selectedGoals = $scope.selectedGoals;
	
	$scope.status = "Draft";
	FnaVariables.setFnaModel($scope.FNAObject);
	var obj = new GLI_FnaService.saveTransactions($scope, $rootScope,
			DataService, $translate, UtilityService, false);
	obj.save(function() {
		// FNA changes by LE Team		
		//$location.path('/questioner');
		$location.path('/fnaRecomendedProducts');
	});

}

 $scope.$on("$destroy", function() {
                 if (this.$$destroyed) return;     
				 while (this.$$childHead) {      
				 this.$$childHead.$destroy();    
				 }     
				 if (this.$broadcast)
				{ 
					this.$broadcast('$destroy');   
				}    
				 this.$$destroyed = true;                 
				 $timeout.cancel( timer );                           
        });



}]);