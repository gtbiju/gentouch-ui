
/*Name:GLI_ProductListingController
 * For implementing GLI specific changes
 */
storeApp.controller('GLI_ProductListingController',['$timeout', '$rootScope', '$parse', '$window', '$location', '$scope','$translate', 'UtilityService', 'DataService', '$debounce', '$routeParams','GLI_FnaService', 'RuleService', 'AutoSave', 'globalService','FnaVariables','IllustratorVariables','GLI_globalService','$controller','MediaService',
	                                                function GLI_ProductListingController($timeout, $rootScope, $parse, $window, $location, $scope,$translate, UtilityService, DataService, $debounce, $routeParams,GLI_FnaService, RuleService, AutoSave, globalService,FnaVariables,IllustratorVariables,GLI_globalService,$controller,MediaService){
	                                                	$controller('ProductListingController',{$timeout:$timeout, $rootScope:$rootScope, $parse : $parse, $window: $window, $location:$location, $scope:$scope, $translate:$translate, UtilityService:UtilityService, DataService:DataService, $debounce:$debounce, $routeParams:$routeParams,FnaService:GLI_FnaService, RuleService:RuleService, AutoSave:AutoSave, globalService:globalService,FnaVariables:FnaVariables,IllustratorVariables:IllustratorVariables,GLI_globalService:GLI_globalService});
	
	if(FnaVariables.fnaKeys.Key15 == FnaVariables.getStatusOptionsFNA()[0].status) {
		FnaVariables.fnaKeys.Key15  = FnaVariables.getStatusOptionsFNA()[1].status;
		$scope.status = FnaVariables.getStatusOptionsFNA()[1].status;
	}	
	else if(FnaVariables.fnaKeys.Key15 == FnaVariables.getStatusOptionsFNA()[2].status){
		FnaVariables.fnaKeys.Key15  = FnaVariables.getStatusOptionsFNA()[2].status;
		$scope.status = FnaVariables.getStatusOptionsFNA()[2].status;
	}

	//$rootScope.populationBIdisableFlag = false;
	$rootScope.selectedPage = "ProductListing";
	$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;
	UtilityService.disableProgressTab = false;
	//enabling and disabling the tabs
	$scope.initializeProgressbarNew = function(){
		$scope.currentPageType  = "Product Recommendation";
		$scope.nextPageType = "FNA Report";
		
		var tabIndex ;
		for (var j = 0; j < $scope.pageSelections.length; j++) {
			if ($scope.pageSelections[j].pageType == $scope.currentPageType) {
				tabIndex = j;
				$scope.pageSelections[j].isSelected = "selected";
				$scope.pageSelections[j].isDisabled = "";
			} else {
				$scope.pageSelections[j].isSelected = "";
				if($scope.pageSelections[j].pageType == $scope.nextPageType){
					$scope.pageSelections[j].isDisabled = "";
				}else if(tabIndex >= 0 && j > tabIndex && UtilityService.disableProgressTab == true && $scope.pageSelections[j].pageType != $scope.nextPageType){
					$scope.pageSelections[j].isDisabled = "disabled";
				}
			}
		}


		$scope.FNAObject.FNA.pageSelections = $scope.pageSelections;
		FnaVariables.setFnaModel($scope.FNAObject);
		$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;
		$scope.$broadcast('ProgressbarReload');
		$scope.refresh();
	}
	$scope.initializeProgressbarNew();
	
	$scope.getneedId = function(goalName) {
		
		var needId = "";
		if (goalName == "Protection") {
			needId = "protection";
		} else if (goalName == "Education") {
			needId = "education";
		} else if (goalName == "Retirement") {
			needId = "retirement";
		} 
		/* FNA Changes by LE Team Starts */
		  else if (goalName == "Savings") {
			needId = "Savings";
		} else if (goalName == "Health") {
			needId = "Health";
		} 
		// else if (goalName == "Wealth Transfer") {
		//	needId = "wealth";
		//} else if (goalName == "Wealth Accumulation") {
		//	needId = "accumulation";
		//} 
		/* FNA Changes by LE Team Ends */
		
		return needId;
	};
	$scope.getLifeStage = function(lifestageDesc) {
		var lifeStageId = "";
		if (lifestageDesc == "Single") {
			lifeStageId = "0";
		} else if (lifestageDesc == "Married") {
			lifeStageId = "1";
		} else if (lifestageDesc == "Married-with-kids") {
			lifeStageId = "2";
		} else if (lifestageDesc == "Married-with-grown-up-kids") {
			lifeStageId = "3";
		} else if (lifestageDesc == "Golden-Age") {
			lifeStageId = "4";
		}
		return lifeStageId;
	};
	$scope.playArmsVideo = function() {
		MediaService.playVideo($scope,'arms.mp4');
	};
	$scope.getProductSelectionList = function(goalName, priority) {
        $scope.productDetailsList = [];
		$scope.mainInsuredRiderList = [];
		$scope.currentGoalName=goalName;
		$scope.additionalInsuredRiderList = [];
		var parentProduct = "";
		if(goalName){
			for (var i = 0; i < $scope.goalList.length; i++) {
				if ($scope.goalList[i].goalName == goalName && $scope.goalList[i].priority == priority) {
					
					for(var j=0; j< $scope.goalList[i].productList.length; j++){
						$scope.productDetailsList.push($scope.goalList[i].productList[j]);
						/* FNA Changes by LE team Start >> As Parent Child Product Recommendation is out of scope 
						if($scope.goalList[i].productList[j].insuredType == "" && $scope.goalList[i].productList[j].parentId == ""){
							parentProduct =  $scope.goalList[i].productList[j].productId;
							$scope.productDetailsList.push($scope.goalList[i].productList[j]);
						}
						else{
							if($scope.goalList[i].productList[j].parentId == parentProduct){
								if($scope.goalList[i].productList[j].insuredType == "Maininsured"){
									$scope.mainInsuredRiderList.push($scope.goalList[i].productList[j]);
								}
								else{
									if($scope.goalList[i].productList[j].insuredType == "Spouse"){
										var substring = "Spouse";
										if(($scope.goalList[i].productList[j].name).indexOf(substring) == -1){
											$scope.goalList[i].productList[j].name = "Spouse " + $scope.goalList[i].productList[j].name;
										}
									}else if($scope.goalList[i].productList[j].insuredType == "childInsured"){
										var substring = "Child";
										if(($scope.goalList[i].productList[j].name).indexOf(substring) == -1){
											$scope.goalList[i].productList[j].name = "Child " + $scope.goalList[i].productList[j].name;
										}
									}else if($scope.goalList[i].productList[j].insuredType == "parentsInsured"){
										var substring = "Parent";
										if(($scope.goalList[i].productList[j].name).indexOf(substring) == -1){
											$scope.goalList[i].productList[j].name = "Parent " + $scope.goalList[i].productList[j].name;
										}
									}
									$scope.additionalInsuredRiderList.push($scope.goalList[i].productList[j]);
								}
							}
						}  FNA Changes by LE team End */
					}	
					$scope.goalList[i].fnaReportProduct = $scope.productDetailsList;
					$scope.goalList[i].fnaReportMainInsured = $scope.mainInsuredRiderList;
					$scope.goalList[i].fnaReportAdditionalInsured = $scope.additionalInsuredRiderList;
				
					var riderList = [];
					GLI_globalService.setRiders(riderList);
					for(var y=0;y<$scope.mainInsuredRiderList.length;y++){
						var planNameMain = $scope.mainInsuredRiderList[y].planName;
						riderList.push(planNameMain);
					}
					for(var z=0;z<$scope.additionalInsuredRiderList.length;z++){
						var planNameAddnl = $scope.additionalInsuredRiderList[z].planName;
						riderList.push(planNameAddnl);
					}
					GLI_globalService.setRiders(riderList);
				
				}
				
			}
		}
        /* FNA Changes by LE Team Start */
        
        //Added path for product images, which is downloaded from Content admin
        if (rootConfig.isDeviceMobile) {
            var leFileUtils = new LEFileUtils();
            leFileUtils.getApplicationPath(function (documentsPath) {
                if (rootConfig.isOfflineDesktop) {
                    $scope.fileFullPath = "file://" + documentsPath;
                }
                else {
                    $scope.fileFullPath = "file://" + documentsPath + "/";
                }
            });
        }
        else {
            $scope.fileFullPath = rootConfig.mediaURL;
        }
        /* FNA Changes by LE Team Ends */
		$rootScope.showHideLoadingImage(false);
		$scope.refresh();
        };
	
	$scope.getSelectedGoals = function(successcallback){
		 for (var i = 0; i < $scope.goalList.length; i++) {
            for (var j = 0; j < $scope.goals.length; j++) {
                if ($scope.goals[j].goalName == $scope.goalList[i].goalName) {
                    var obj = {
                        "goalName": $scope.goals[j].goalName,
                        "goalicon": $scope.goals[j].goalicon,
                        "goalDesc": $scope.goals[j].goalDEsc,
                        "goalDescImage": $scope.goals[j].goalDescImage,
                        "priority": $scope.goalList[i].priority,
                        "translateID": $scope.goals[j].translateID
                    };
                    $scope.selectedGoals.push(obj);
                    if ($scope.goalList[i].priority == 1) {
                        goalfirst = $scope.goals[j].goalName;
                    }
                    
                }
            }
            if( i == ($scope.goalList.length-1)){
            	successcallback();
            }
            
        }
	}
	
	
	$scope.initialLoad = function(){
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
	}    
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
	}
	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5');
	}
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5');
	}
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5');
	}
    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5'])
	{
		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5');
	}  
				$scope.orderList = [{
					id : 1,
					value : "Priority(Ascending)"
				}, {
					id : 2,
					value : "Priority(Descending)"
				}, {
					id : 3,
					value : "Age(Ascending)"
				}];

				$scope.goals = FnaVariables.goals;

				$scope.selectedGoals = [];

				$scope.goalList = $scope.FNAObject.FNA.selectedGoals;

				$scope.goalPriorityNumber = 1;

				$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;

				$scope.selectedproduct = $scope.FNAObject.FNA.selectedProduct;
				
				$scope.getSelectedGoals(function(){
					   $scope.initalizeProgressbar();
	            	   $scope.currentGoalName = goalfirst;
	            	   $scope.getGoalProduct();
				});
				
	        }
	
	$scope.getAllProducts = function() {
		FnaVariables.setFnaModel($scope.FNAObject);
		/*bug 4194 starts */
		IllustratorVariables.transactionId =0;
		/*bug 4194 ends */
	//	var product = globalService.product();
	//	globalService.setProduct(product);
		//$rootScope.populationBIdisableFlag = true;
		$location.path('/fnaAllProducts');
	};
	
	$scope.getSelectedProduct = function(product) {
		$scope.isDependentOk = true;
			
		$scope.FNAObject.FNA.selectedProduct = product;
		for (var j = 0; j < $scope.FNAObject.FNA.goals.length; j++) {
			if ($scope.FNAObject.FNA.goals[j].goalName == $scope.currentGoalName) {
				$scope.FNAObject.FNA.productSelectedGoal = $scope.currentGoalName;
				$scope.FNAObject.FNA.selectedProduct.result = $scope.FNAObject.FNA.goals[j].result;
				$scope.isProductSelected = true;
			}
		}
		$scope.selectedproduct = $scope.FNAObject.FNA.selectedProduct;
		FnaVariables.setFnaModel($scope.FNAObject);
		$scope.FNABeneficiaries = globalService.getFNABeneficiaries();
		if($scope.FNABeneficiaries.length != 0){
			for (var z = 0; z < $scope.FNABeneficiaries.length; z++) {
				if($scope.FNABeneficiaries[z].BasicDetails != undefined){
					if($scope.FNABeneficiaries[z].BasicDetails.firstName == "" || $scope.FNABeneficiaries[z].BasicDetails.firstName == undefined || $scope.FNABeneficiaries[z].BasicDetails.dob == "" || $scope.FNABeneficiaries[z].BasicDetails.dob == undefined){
						$scope.isDependentOk = false;
						break;
					}
				}
				else{
					$scope.isDependentOk = false;
				}
			}
			if(!$scope.isDependentOk){
				$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "dependentValidationMessage"), translateMessages($translate,"fna.ok"), $scope.okBtnAction);
			}
			else{
				$scope.getIllustration(product);
			}
		}
		else{
			$scope.getIllustration(product);
		}
	};
	
	$scope.$on('initiate-save', function (evt, obj) {
		//$scope.disableLePopCtrl = true;
		globalService.setProduct(product);
		var parties = globalService.getDefaultParties();
		$scope.FNAObject.FNA.parties = parties;
		FnaVariables.setFnaModel($scope.FNAObject);
		var obj = new GLI_FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
		obj.save(function() {
			$rootScope.showHideLoadingImage(false, "");
			$rootScope.passwordLock  = false;
			$rootScope.isAuthenticated = false;
			$location.path('/login');
		});
	});
	
	$scope.getIllustration = function(product) {

		if ($scope.FNAObject.FNA.selectedProduct && $scope.FNAObject.FNA.selectedProduct.productId && $scope.isProductSelected) {
			FnaVariables.setFnaModel($scope.FNAObject);
			//$rootScope.populationBIdisableFlag = false;
			var product = globalService.product;
			product.ProductDetails.productCode = $scope.FNAObject.FNA.selectedProduct.productId;
			
			/*if ($scope.FNAObject.FNA.selectedProduct.result && $scope.FNAObject.FNA.selectedProduct.result.ruleExecutionOutput) {
				var value = Math.ceil(div(mul(Number($scope.FNAObject.FNA.selectedProduct.result.ruleExecutionOutput[0].corpusRequired), 10), 900), 1);
				product.policyDetails.committedPremium = Math.round(value);
			}*/
			globalService.setProduct(product);

			// To reset isInsured and isBenificiary for all parties to default
			// false value
			// Since for each illustrate button clisk -- new illustration is
			// created for FNA
			var parties = globalService.getDefaultParties();
			$scope.FNAObject.FNA.parties = parties;

			FnaVariables.setFnaModel($scope.FNAObject);
			var obj = new GLI_FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
			obj.save(function() {
				UtilityService.previousPage = 'FNA';
				$location.path('/fnaChooseParties/0/'+product.ProductDetails.productCode+'/0');
			});

		} else {
			$rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "chooseProductValidationMessage"), translateMessages($translate, "fna.ok"));
		}
	};
	
	/* FNA Changes by LE Team Start */
	/*
	$scope.openPDF = function(pdfFile) {	
		if($parse(rootConfig.isDeviceMobile)(this)){
			var fileFullPath = "";
	           window.plugins.LEFileUtils.getApplicationPath(function (path) {
	                   fileFullPath =  "file://"+path + "/" + pdfFile;
	                   cordova.exec(function(){}, function(){}, "PdfViewer", "showPdf", [fileFullPath]);
	                                                         
	                 },function(e){});
		}else if(!($parse(rootConfig.isDeviceMobile)(this)) && !($parse(rootConfig.isOfflineDesktop)(this))){
			MediaService.openPDF($scope, $window, pdfFile);
		}
	}
	*/
	$scope.openPDF = function (index, productCode, type) {
        var escape = false
            , pdfFile;
        var productsLength = $scope.productDetailsList.length;
        for (var k = 0; k < productsLength; k++) {
        	/* FNA Changes by LE Team: Bug 4224 */
            if ($scope.productDetailsList[k].productId == productCode) {
                for (var l = 0; l < $scope.productDetailsList.length; l++) {
                    if ($scope.productDetailsList[k].productId==1004) {
						pdfFile = "GenProLife_Brochure.pdf";
					} else if ($scope.productDetailsList[k].productId==1003) {
						pdfFile = "GenBumNan_Brochure.pdf";
					} else if ($scope.productDetailsList[k].productId==1010) {
						pdfFile = "GenCompleteHealth80_Brochure.pdf";
					} else if ($scope.productDetailsList[k].productId==1006) {
						pdfFile = "GenSave_Brochure.pdf";
					} else if ($scope.productDetailsList[k].productId==1097) {
						pdfFile = "GenCancerSuperProtection_Brochure.pdf";
					} else if ($scope.productDetailsList[k].productId==1220) {
						pdfFile = "WholeLife_Brochure.pdf";
					} else if ($scope.productDetailsList[k].productId==1306) {
						pdfFile = "GenSave10Plus_Brochure.pdf";
					} else if ($scope.productDetailsList[k].productId==1274) {
						pdfFile = "GenSave4Plus_Brochure.pdf";
					} else {
						pdfFile = "";
					}
                    escape = true;
                    break;
                }
                if (escape) {
                    break;
                }
            }
        }
        if((rootConfig.isDeviceMobile) && (!rootConfig.isOfflineDesktop)){
            var fileFullPath = "";
            window.plugins.LEFileUtils.getApplicationPath(function (path) {
                fileFullPath = "file://" + path + "/" + pdfFile;
                cordova.exec(function () {}, function () {}, "PdfViewer", "showPdf", [fileFullPath]);
            }, function (e) {});
        }else if(((rootConfig.isDeviceMobile) && (rootConfig.isOfflineDesktop)) ||((!rootConfig.isDeviceMobile) && (!rootConfig.isOfflineDesktop))){
            MediaService.openPDF($scope, $window, pdfFile);
        }
    }
	/* FNA Changes by LE Team End */
    
	$scope.playVideo = function(productId) {
        var videoFile = "";
		if (productId == 1004) {
            videoFile = "GenProLife.mp4";
        } else if (productId == 1003) {
            videoFile = "GENBUMNAN.mp4";
        } else if (productId == 1010) {
            videoFile = "GENCOMPLETE.mp4";
        } else if (productId == 1006) {
            videoFile = "GENPROCASH.mp4";
        }
        if(productId != 1097)
		MediaService.playVideo($scope,videoFile);
	}
                                                        
	$scope.okBtnAction = function() {
		FnaVariables.choosePartyStatus = true;
		$location.path('/MyFamilyDetails');															
	}
	 $scope.$on("$destroy", function() {
                 if (this.$$destroyed) return;     
				 while (this.$$childHead) {      
				 this.$$childHead.$destroy();    
				 }     
				 if (this.$broadcast)
				{ 
					this.$broadcast('$destroy');   
				}   
				 this.$$destroyed = true;                 
				 $timeout.cancel( timer );                           
    });
    /* FNA Changes by LE Team Start */
    $scope.showKeyFeatures = function(divID)
	{
		$scope.keyFeaturesOverlay = true;
		$(divID).css('display', 'inline');
	}
	$scope.hideKeyFeatures = function(divID)
	{
		$scope.keyFeaturesOverlay = false;
		$(divID).css('display', 'none');
	}
    /* FNA Changes by LE Team End */
}]);