/*
 *Copyright 2015, LifeEngage 
 */

storeApp.controller('FnaChildProductListingController', ['$rootScope', '$scope', 'DataService', 'FnaService','LookupService', 'DocumentService', 'ProductService','$compile', 
'$routeParams', '$location', '$translate','IllustratorVariables', 'FnaVariables', 'UtilityService','$debounce', 'PersistenceMapping', 'AutoSave',
 'globalService','$timeout',
function($rootScope, $scope, DataService, FnaService, LookupService,DocumentService, ProductService, $compile, $routeParams, $location, $translate, 
IllustratorVariables,FnaVariables, UtilityService,$debounce, PersistenceMapping, AutoSave, globalService,$timeout) {

	$scope.childSelGoals = $scope.$parent.selectedGoals;
	$scope.imageTemp = $scope.$parent.insuredImage;
	
	
	//Added for Generali begins
	$scope.FNAObject = FnaVariables.getFnaModel();
	//Added for Generali ends
	
	
	$scope.getProductSelectionListChild = function(goalName, priority) {
		$scope.$parent.getProductSelectionList(goalName, priority);
	}

}]);
