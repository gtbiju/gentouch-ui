/*
 *Copyright 2015, LifeEngage 
 */

storeApp.controller('FnaChildCalculatorController', ['$rootScope', '$scope', 'DataService', 'FnaService','LookupService', 'DocumentService', 'ProductService','$compile', 
'$routeParams', '$location', '$translate','IllustratorVariables', 'FnaVariables', 'UtilityService','$debounce', 'PersistenceMapping', 'AutoSave',
 'globalService','$timeout',
function($rootScope, $scope, DataService, FnaService, LookupService,DocumentService, ProductService, $compile, $routeParams, $location, $translate, 
IllustratorVariables,FnaVariables, UtilityService,$debounce, PersistenceMapping, AutoSave, globalService,$timeout) {


	$scope.childSelGoals = $scope.$parent.selectedGoals;
	$scope.imageTemp = $scope.$parent.insuredImage;
	
	
	//Added for Generali begins
	$scope.FNAObject = FnaVariables.getFnaModel();
	//Added for Generali ends

/* Updating Completed goal status in UI - Generali*/
    $scope.updateCompletedGoal = function(goal) {
        for(i=0;i<=$scope.$parent.uniquePengingGoalsCount;i++){
            var retClass = "";
            if($scope.$parent.uniquePengingGoals.indexOf(goal.goalName+goal.priority) == -1){
                if(goal.goalName.length >15 ){
					 retClass = "active long";
				}
				else{
					 retClass = "active";
				}
                return retClass;
                break;
            }                      
        }
    }
/* Updating Completed goal status in UI - Generali*/

	
	$scope.showDetail = function(index) {
		$scope.$parent.showDetail(index);
	}

}]);
