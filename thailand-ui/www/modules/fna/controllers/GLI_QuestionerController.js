/*
 *Copyright 2015, LifeEngage 
 */



/*Name:GLI_QuestionerController
 * For implementing GLI specific changes
 */
storeApp.controller('GLI_QuestionerController',['$timeout', '$route', 'globalService', '$rootScope', '$scope', '$location','$compile', '$routeParams', 'DataService', 'RuleService', 'LookupService','DocumentService', 'FnaVariables', 'GLI_FnaService', '$translate', 'UtilityService','$debounce', 'AutoSave','$controller',
function GLI_QuestionerController($timeout, $route, globalService, $rootScope, $scope, $location,$compile, $routeParams, DataService, RuleService, LookupService,DocumentService, FnaVariables, GLI_FnaService, $translate, UtilityService,$debounce, AutoSave,$controller){
	$controller('QuestionerController',{$timeout:$timeout, $route:$route, globalService:globalService, $rootScope:$rootScope, $scope:$scope, $location:$location,$compile:$compile, $routeParams:$routeParams, DataService:DataService, RuleService:RuleService, LookupService:LookupService,DocumentService:DocumentService, FnaVariables:FnaVariables, FnaService:GLI_FnaService, $translate:$translate, UtilityService:UtilityService,$debounce:$debounce, AutoSave:AutoSave});
	$rootScope.selectedPage = "Questioner";
	$scope.showIagree = false;
	$scope.disableOk = false;
	$scope.optionChange = false;
	$scope.status = FnaVariables.getStatusOptionsFNA()[0].status;
	$scope.currentPageType  = "Need Analysis";
	UtilityService.disableProgressTab = false;
	//enabling and disabling the tabs
	/*$scope.initializeProgressbarNew = function(){
		$scope.currentPageType  = "Need Analysis";
		$scope.FNAObject.FNA.pageSelections = GLI_FnaService.initalizeProgressbar($scope.currentPageType , $scope.pageSelections);
		FnaVariables.setFnaModel($scope.FNAObject);
		$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;
	}
	$scope.initializeProgressbarNew();
	*/
	
	GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
	
	//to show/hide the checkbox based on new risk profile selection 
	$scope.changeRiskProfile = function(data){
		
		if(data != $scope.risk){
			$scope.showIagree = true;
			$scope.disableOk = true;
		}
		else{
			$scope.showIagree = false;
			$scope.disableOk = false;
		}
			$scope.checkModel = false;
			$scope.FNAObject.FNA.riskProfile.level = data;
	};
	//For showing main insured and beneficiary photos
	$scope.FNAMyself = globalService.getParty("FNAMyself");
	if ($scope.FNAMyself.BasicDetails.photo == ""
		|| $scope.FNAMyself.BasicDetails.photo == undefined) {
		$scope.insuredImage = FnaVariables.myselfPhoto;
	} else {
		$scope.insuredImage = $scope.FNAMyself.BasicDetails.photo;
	}
	
	
	$scope.agreeClick = function(){
		if($scope.checkModel){
			$scope.disableOk = false;	
		}
		else{
			$scope.disableOk = true;	
		}
	}
	$scope.nextPage = function() {
		// displayOverlay('riskprofiler_popup_wrapper');
		/*$scope.riskprofiler_popup_wrapper = true;
		 */
		var transactionObj = {
			"UserInput" : []
		};
		$scope.showRisk = true;
		for (var i = 0; i < $scope.FNAObject.FNA.questionnaire.length; i++) {
			if ($scope.FNAObject.FNA.questionnaire[i].score == undefined) {

				$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "fna.questionRemaining"), translateMessages($translate, "fna.ok"));
				$scope.showRisk = false;
				break;
			}
			transactionObj.UserInput.push({
				"questionId" : $scope.FNAObject.FNA.questionnaire[i].questionId,
				"score" : $scope.FNAObject.FNA.questionnaire[i].score
			});
		}

		if ($scope.showRisk) {

			$scope.riskprofiler_popup_wrapper = true;
			try {
				var inputJson = JSON.stringify(transactionObj);
				var riskRuleSetObj = FnaVariables.riskCalculationRuleSetConfig;
				if($scope.optionChange == true || typeof $scope.FNAObject.FNA.riskProfile.level == "undefined" || $scope.FNAObject.FNA.riskProfile.level == ""){
					RuleService.calculateRisk(inputJson, riskRuleSetObj, function(output) {
						$scope.optionChange = false; 
						$scope.getRiskCalculatorSuccess(output);
					}, function(output) {
					});
				}
				else
				{
					$scope.risk = $scope.FNAObject.FNA.riskProfile.level;
				}
			} catch (exception) {
				alert("Error Output " + exception);
				errorCallback(exception);
			}
		}

	};
	//over writing function to show/hide the checkbox in risk profile page
	$scope.getRiskCalculatorSuccess = function(data) {
		$scope.FNAObject.FNA.riskProfile.level = data.risk;
		$scope.risk = data.risk;
		$scope.showIagree = false;
		$scope.refresh();
	};
	//New function to select and highlight option
	$scope.selectedOption = function(id, answerindex) {
		if (id == answerindex) {
			return "active";
		}
	};
	//overidding option click
	$scope.optionClick = function(index, id, score) {
		if($scope.optionChange == false && $scope.FNAObject.FNA.questionnaire[index].answerIndex != id){
			$scope.optionChange = true;
		}
		$scope.FNAObject.FNA.questionnaire[index].answerIndex = id;
		$scope.FNAObject.FNA.questionnaire[index].score = score;
		UtilityService.disableProgressTab = true;
		//GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
		var unansweredQuestion =0;	
			for (var i = 0; i < $scope.FNAObject.FNA.questionnaire.length; i++) {
			if ($scope.FNAObject.FNA.questionnaire[i].score == undefined) {
				unansweredQuestion++;
				break;
				}
			}
			if(unansweredQuestion == 0){
			$scope.questionCompleted = true;
			}
			else{
			$scope.questionCompleted = false;
			}
			GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
		
	};
		$scope.callback = function() {
		$rootScope.showHideLoadingImage(false);
			
			var unansweredQuestion =0;
			for (var i = 0; i < $scope.FNAObject.FNA.questionnaire.length; i++) {
			if ($scope.FNAObject.FNA.questionnaire[i].score == undefined) {
				unansweredQuestion++;
				break;
				}
			}
			if(unansweredQuestion == 0){
			$scope.questionCompleted = true;
			}
			else{
			$scope.questionCompleted = false;
			}

	};
	$scope.$on('initiate-save', function (evt, obj) {
		//$scope.disableLePopCtrl = true;
		FnaVariables.setFnaModel($scope.FNAObject);
		if(FnaVariables.fnaKeys.Key15 == FnaVariables.getStatusOptionsFNA()[0].status){
			FnaVariables.fnaKeys.Key15  = FnaVariables.getStatusOptionsFNA()[1].status;
		}
		$scope.status = FnaVariables.fnaKeys.Key15;
		var obj = new GLI_FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
		obj.save(function() {
			$rootScope.showHideLoadingImage(false, "");
			$rootScope.passwordLock  = false;
			$rootScope.isAuthenticated = false;
			$location.path('/login');
		});
	});
	//over writing function to rename status  final to complete
	$scope.riskSummary = function() {
		try {
			for (var i = 0; i < FnaVariables.pages.length; i++) {
				if (FnaVariables.pages[i].url == "/fnaRecomendedProducts") {
					$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
					break;
				}
			}
			FnaVariables.setFnaModel($scope.FNAObject);
			$scope.disableTab();
			//$scope.status = "Completed";
			if(FnaVariables.fnaKeys.Key15 == FnaVariables.getStatusOptionsFNA()[0].status){
				FnaVariables.fnaKeys.Key15  = FnaVariables.getStatusOptionsFNA()[1].status;
			}
			$scope.status = FnaVariables.fnaKeys.Key15;
			$scope.initalizeProgressbarNext();
			var obj = new GLI_FnaService.saveTransactions($scope, $rootScope,
			DataService, $translate, UtilityService, false);
			obj.save(function(){
				$scope.riskprofiler_popup_wrapper = false;
				 $timeout(function() { $location.path('/fnaRecomendedProducts'); }, 50);
			});
			
			// Provided a timeout in IOS version since Product ListingController
			// was getting loaded more than once and the PSM ouput was not
			// giving back all product details like collaterals
			// Not required since this is happening only in some iPad devices
			/*
			 * $timeout(function() { $location.path('/fnaRecomendedProducts'); },
			 * 50);
			 */
		} catch (exception) {
			alert("Error Output " + exception);
		}
	}
	
	 $scope.$on("$destroy", function() {
                 if (this.$$destroyed) return;     
				 while (this.$$childHead) {      
				 this.$$childHead.$destroy();    
				 }     
				 if (this.$broadcast)
				{ 
					this.$broadcast('$destroy');   
				}   
				 this.$$destroyed = true;                 
				 $timeout.cancel( timer );                           
    });

}]);