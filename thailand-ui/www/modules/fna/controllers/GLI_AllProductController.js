
/*Name:GLI_AllProductController
 * For implementing GLI specific changes
 */
storeApp.controller('GLI_AllProductController',['$rootScope',
												'$scope', 
												'$parse',
												'$window',
												'$location',
												'$translate',
												'$routeParams',
												'IllustratorVariables', 
												'ProductService',
												'FnaVariables',
												'UtilityService',
												'UserDetailsService',
												'GLI_IllustratorVariables',
												'$controller',
												'MediaService',
												'GLI_IllustratorService',
												'globalService',
												'FnaService',
												'DataService',
												'GLI_globalService',
   function GLI_AllProductController($rootScope, 
									 $scope, 
									 $parse, 
									 $window, 
									 $location, 
									 $translate, 
									 $routeParams, 
									 IllustratorVariables, 
									 ProductService, 
									 FnaVariables, 
									 UtilityService, 
									 UserDetailsService, 
									 GLI_IllustratorVariables, 
									 $controller, 
									 MediaService, 
									 GLI_IllustratorService, 
									 globalService, 
									 FnaService, 
									 DataService, 
									 GLI_globalService){
	  $controller('AllProductController',
			 {	$rootScope:$rootScope,
		  		$scope:$scope,
		  		$parse : $parse,
				$window: $window,
		  		$location:$location,
		  		$translate:$translate,
				$routeParams: $routeParams,
		  		IllustratorVariables:IllustratorVariables,
		  		ProductService:ProductService,
		  		FnaVariables:FnaVariables,
		  		UtilityService:UtilityService,
				UserDetailsService: UserDetailsService,
				IllustratorVariables: GLI_IllustratorVariables,
		  		globalService:globalService,
		  		FnaService:FnaService,
		  		DataService:DataService,
		  		GLI_globalService:GLI_globalService});
				
		$rootScope.hideLanguageSetting = false;
        $rootScope.enableRefresh=false;
	  	$scope.allproductsList = [];
	  	$rootScope.selectedPage = "fnaAllProducts";
		$scope.isRDSUser = UserDetailsService.getRDSUser();
		$scope.isTechomBank = false;
		$scope.isWeb = false;
		$scope.showProducts = false;
		
		/*
		if ($scope.isRDSUser) {
			$scope.isWeb = false;
		}
		*/
		if(rootConfig.isDeviceMobile==false) {
			$scope.isWeb  = true;
		}

		$scope.initialLoad= function() {
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6');
			}
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6');
			}
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6');
			}
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6');
			}
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
			}    
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4');
			}
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
			}
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
			}
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5');
			}
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5');
			}
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5');
			}
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5');
			}  
			
			$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
			$scope.productsListEven = [];
			$scope.productsListOdd = [];
			var transactionObj;
			$scope.isDeviceMobile = (rootConfig.isDeviceMobile);
			$scope.productsListOdd = [];
			
			if (rootConfig.isDeviceMobile) {
				transactionObj = {
                "CheckDate": new Date().getMonth() + 1 + "-" + new Date().getDate() + "-" + new Date().getFullYear()
                , "Products": {
                    "id": ""
                    , "code": ""
                    , "carrierId": 1
                    , "channelId": IllustratorVariables.channelId
					,"apiVersion":2
					}
				}
			} else {
				transactionObj = {
					"CheckDate": new Date().getMonth() + 1 + "-" + new Date().getDate() + "-" + new Date().getFullYear()
					, "Products": {
						"id": ""
						, "code": ""
						, "carrierCode": 1
						, "channelId": IllustratorVariables.channelId
						,"apiVersion":2
					}
				}
			}
			/* FNA Changes by LE team (This existing-line-of-code is commented out)
			var transactionObj = {
					"CheckDate": new Date().getMonth() + 1 + "-" + new Date().getDate() + "-" + new Date().getFullYear(),
					"Products": {
					"id": "",
					"code": "",
					"carrierCode": 1
					}
			}
			ProductService.getAllActiveProducts(transactionObj, $scope.getAllProductsSuccess,
					$scope.getAllProductsFailure);
			*/
			ProductService.getAllActiveProductsByFilter(transactionObj, $scope.getAllProductsSuccess, $scope.getAllProductsFailure);
		}
		$scope.getIllustration = function(product) {
			/* FNA Changes by LE Team Start (this line-of-code commented out) */
			/*
			$scope.FNAObject.FNA.selectedProduct = product;
			$scope.selectedproduct = $scope.FNAObject.FNA.selectedProduct;
			FnaVariables.setFnaModel($scope.FNAObject);
			
			$scope.isDependentOk = true;
			$scope.FNAObject.FNA.productSelectedGoal="";
			if ($scope.FNAObject.FNA.selectedProduct
					&& $scope.FNAObject.FNA.selectedProduct.id) {

				FnaVariables.setFnaModel($scope.FNAObject);

				var product = globalService.product;
				var productId = $scope.FNAObject.FNA.selectedProduct.id;
				product.ProductDetails.productCode = productId
				$scope.FNAObject.FNA.selectedProduct.productId = productId;
				var riderList = [];
				GLI_globalService.setRiders(riderList);
				globalService.setProduct(product);
				
				$scope.FNABeneficiaries = globalService.getFNABeneficiaries();
				if($scope.FNABeneficiaries.length != 0){
					for (var z = 0; z < $scope.FNABeneficiaries.length; z++) {
						if($scope.FNABeneficiaries[z].BasicDetails != undefined){
							if($scope.FNABeneficiaries[z].BasicDetails.firstName == "" || $scope.FNABeneficiaries[z].BasicDetails.firstName == undefined || $scope.FNABeneficiaries[z].BasicDetails.dob == "" || $scope.FNABeneficiaries[z].BasicDetails.dob == undefined){
								$scope.isDependentOk = false;
								break;
							}
						}
						else{
							$scope.isDependentOk = false;
						}
					}
					if(!$scope.isDependentOk){
						$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "dependentValidationMessage"), translateMessages($translate,"fna.ok"), $scope.okBtnAction);
					}
					else{
						// To reset isInsured and isBenificiary for all parties to default
						// false value
						// Since for each illustrate button clisk -- new illustration is
						// created for FNA
						var parties = globalService.getDefaultParties();
						$scope.FNAObject.FNA.parties = parties;
						FnaVariables.setFnaModel($scope.FNAObject);

						var obj = new FnaService.saveTransactions($scope, $rootScope,
								DataService, $translate, UtilityService, false);
						obj.save(function() {
							UtilityService.previousPage = 'FNA';
							$location.path('/fnaChooseParties/0/'+productId+'/0');
						});
					}
				}
				else{
					// To reset isInsured and isBenificiary for all parties to default
					// false value
					// Since for each illustrate button clisk -- new illustration is
					// created for FNA
					var parties = globalService.getDefaultParties();
					$scope.FNAObject.FNA.parties = parties;
					FnaVariables.setFnaModel($scope.FNAObject);

					var obj = new FnaService.saveTransactions($scope, $rootScope,
							DataService, $translate, UtilityService, false);
					obj.save(function() {
						UtilityService.previousPage = 'FNA';
						$location.path('/fnaChooseParties/0/'+productId+'/0');
					});
				}
			} else {
				 //$rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),$scope.message,translateMessages($translate, "chooseProductValidationMessage"),$scope.removePreviousDetails,translateMessages($translate, "fna.ok"));
				$rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),translateMessages($translate, "chooseProductValidationMessage"),translateMessages($translate, "fna.ok"));
			}
			*/
			/* New code Start */
			if(IllustratorVariables.fromChooseAnotherProduct == "false" || IllustratorVariables.fromChooseAnotherProduct == "" ){
				IllustratorVariables.fromChooseAnotherProduct = false;
			}
			if (IllustratorVariables.leadId !== "undefined" && IllustratorVariables.leadId && IllustratorVariables.leadId != 0 && !IllustratorVariables.fromChooseAnotherProduct) {
				if(!rootConfig.isDeviceMobile){
					IllustratorVariables.clearIllustrationVariables();
				}
				UtilityService.previousPage = 'FNA';
				$location.path('/Illustrator/' + product.productId + '/0/0/0');
				//$location.path('/fnaChooseParties/0/' + product.id + '/0');
			} else if(IllustratorVariables.fromChooseAnotherProduct){
				UtilityService.previousPage = 'IllustrationPersonal';
				$location.path('/Illustrator/' + product.productId + '/0/0/0');
			} else {
				IllustratorVariables.clearIllustrationVariables();
				IllustratorVariables.editFlowPackage = true;            
				// IllustratorVariables.setIllustratorModel($scope.Illustration);
				//$location.path('/Illustrator/' + product.id + '/0/' + IllustratorVariables.illustratorId + '/' + $rootScope.transactionId);
				// $location.path('/illustratorProduct/' + product.productId + '/0/' + IllustratorVariables.illustratorId + '/' + $rootScope.transactionId);
				$location.path('/Illustrator/' + product.productId + '/0/0/0');
			}
			/* New code End */
		/* FNA Changes by LE Team End */
		};
	  	$scope.playVideo = function(index, productCode, type) {
			/* FNA Changes by LE Team (this line-of-code is commented out) Start */
			/*
			var escapeVideo = false, videoFile;
			var productsLength = $scope.allproductsList.length;
			for (var k = 0; k < productsLength; k++) {
				if ($scope. allproductsList[k].code == productCode) {
					for (var l = 0; l < $scope.allproductsList[k].collaterals.length; l++) {
						if ($scope. allproductsList[k].collaterals[l].docType == type) {
							videoFile = $scope.allproductsList[k].collaterals[l].fileName;
							escapeVideo = true;
							break;
						}
					}
					if (escapeVideo) {
						break;
					}
				}
			}
			if(videoFile == "iPLAN_Video.mp4" || videoFile == "UB_Rich_Video.mp4"){
					MediaService.playVideo($scope,videoFile);
			}
			*/
			MediaService.playVideo($scope, 'iPLAN_Video.mp4');
			/* FNA Changes by LE Team End */
		};
		$scope.illustrationProductListing = rootConfig.illustrationProductListing;
		$scope.getAllProductsSuccess = function(data) {
	  		/* FNA Changes by LE Team Start (this line-of-code is commented out) */
	  		/*
			var sortProducts = data.Products;
			var sortedProducts = []
			for(var d=0;d<$scope.illustrationProductListing.count;d++){ 
			    for(var e=0; e<Object.keys(data.Products).length;e++){
					if(data.Products[e].id == eval($scope.illustrationProductListing.illustrationProductOrder[d].productId)){
						sortedProducts.push(data.Products[e]);
					}
			    }
			}
	  		
			$scope.allproductsList =[];
			$scope.productsListEven = [];
			$scope.productsListOdd = [];
			$scope.allproductsList = sortedProducts;
			
			$rootScope.showHideLoadingImage(false);
			for (var x = 0; x < $scope.allproductsList.length; x += 2) {
				$scope.productsListEven.push($scope.allproductsList[x]);
			}
			for (var x = 1; x < $scope.allproductsList.length; x += 2) {
				$scope.productsListOdd.push($scope.allproductsList[x]);
			}
			$scope.refresh();
			*/
			var applicableProducts = UserDetailsService.getApplicableProducts();
			for (prdIndx in data.Products) {
				var isProdSellable = false;
				for (var y = 0; y < applicableProducts.length; y++) {
					if (data.Products[prdIndx].code == applicableProducts[y].prdCode) {
						isProdSellable = true;
						break;
					}
				}
				if (!isProdSellable) {
					data.Products.splice(prdIndx);
				}
			}
			var sortedProducts = data.Products;
			var sortProducts = data.Products;
			var sortedProducts = [];
			for (var e = 0; e < Object.keys(data.Products).length; e++) {
				if (data.Products[e].id == "4") {
	                var genProlife = data.Products[e];
	            } else if (data.Products[e].id == "10") {
	                var compHealth = data.Products[e];  
	            } else if (data.Products[e].id == "97") {
	                var genCancer = data.Products[e];  
	            } else if (data.Products[e].id == "6") {
	                var genSave20P = data.Products[e];
	            } else if (data.Products[e].id == "3") {
	                  var genBumnan = data.Products[e];
	            } else if (data.Products[e].id == "220") {
	                var wholeLife = data.Products[e];
	            } else if (data.Products[e].id == "306") {
	                var genSave10P = data.Products[e];
	            } if (data.Products[e].id == "274") {
	                var genSave4P = data.Products[e];
	            }
			}
			sortedProducts.push(wholeLife);
			sortedProducts.push(genSave4P);
			sortedProducts.push(genSave10P);
	        sortedProducts.push(genProlife);
	        sortedProducts.push(compHealth);
	        sortedProducts.push(genCancer);
	        sortedProducts.push(genSave20P);
	        sortedProducts.push(genBumnan);

			$scope.allproductsList = [];
			$scope.productsListEven = [];
			$scope.productsListOdd = [];
			$scope.uniqueProductGroupTitle = [];
			$scope.allproductsList = sortedProducts;
			IllustratorVariables.productDetails = $scope.allproductsList;
			IllustratorVariables.productDetails = JSON.stringify(IllustratorVariables.productDetails[0]);
			//IllustratorVariables.productDetails = JSON.parse(IllustratorVariables.productDetails);
			//$rootScope.showHideLoadingImage(false);
			
			for (var x = 0; x < $scope.allproductsList.length; x += 2) {
				$scope.productsListEven.push($scope.allproductsList[x]);
			}
			for (var y = 1; y < $scope.allproductsList.length; y += 2) {
				$scope.productsListOdd.push($scope.allproductsList[y]);
			}

			//Get unique product groups
			for(var x = 0; x < $scope.allproductsList.length; x++){
				if($scope.allproductsList[x].productGroup != undefined){
					if($scope.uniqueProductGroupTitle.indexOf($scope.allproductsList[x].productGroup) == -1){
						$scope.uniqueProductGroupTitle.push($scope.allproductsList[x].productGroup);
					}
				}
			}
			
			//Added path for product images, which is downloaded from Content admin
			if (rootConfig.isDeviceMobile) {
				var leFileUtils = new LEFileUtils();
				leFileUtils.getApplicationPath(function (documentsPath) {
					if (rootConfig.isOfflineDesktop) {
						$scope.fileFullPath = "file://" + documentsPath;
					}
					else {
						$scope.fileFullPath = "file://" + documentsPath + "/";
					}
				});
			}
			else {
				$scope.fileFullPath = rootConfig.mediaURL;
			}
			
			$rootScope.showHideLoadingImage(false);
			$scope.refresh();
		}
		$scope.evaluateString = function (value) {
			var val;
			var variableValue = $scope;
			val = value.split('.');
			if (value.indexOf('$scope') != -1) {
				val = val.shift();
			}
			for (var i in val) {
				variableValue = variableValue[val[i]];
			}
			return variableValue;
		};
		$scope.openPDF = function(index, productCode, type) {
			var escape = false, pdfFile;
			var productsLength = $scope.allproductsList.length;
			for (var k = 0; k < productsLength; k++) {
				if ($scope. allproductsList[k].code == productCode) {
					for (var l = 0; l < $scope.allproductsList[k].collaterals.length; l++) {
						if ($scope. allproductsList[k].collaterals[l].docType == type) {
							pdfFile = $scope.allproductsList[k].collaterals[l].fileName;
							escape = true;
							break;
						}
					}
					if (escape) {
						break;
					}
				}
			}
			
        if((rootConfig.isDeviceMobile) && (!rootConfig.isOfflineDesktop)){
				var fileFullPath = "";
		           window.plugins.LEFileUtils.getApplicationPath(function (path) {
		                   fileFullPath =  "file://"+path + "/" + pdfFile;
		                   cordova.exec(function(){}, function(){}, "PdfViewer", "showPdf", [fileFullPath]);
		                 },function(e){});
        }else if(((rootConfig.isDeviceMobile) && (rootConfig.isOfflineDesktop)) ||((!rootConfig.isDeviceMobile) && (!rootConfig.isOfflineDesktop))){
				MediaService.openPDF($scope, $window, pdfFile);
			}
		}
		$scope.playVideo = function (index, productCode, type) {
			var escapeVideo = false
				, videoFile;
			var productsLength = $scope.allproductsList.length;
			for (var k = 0; k < productsLength; k++) {
				if ($scope.allproductsList[k].code == productCode) {
					for (var l = 0; l < $scope.allproductsList[k].collaterals.length; l++) {
						if ($scope.allproductsList[k].collaterals[l].docType == type) {
							videoFile = $scope.allproductsList[k].collaterals[l].fileName;
							escapeVideo = true;
							break;
						}
					}
					if (escapeVideo) {
						break;
					}
				}
			}
			if (videoFile == "GENBUMNAN.mp4" || videoFile == "GENCOMPLETE.mp4" || videoFile == "GenProLife.mp4" || videoFile == "GENPROCASH.mp4") {
				MediaService.playVideo($scope, videoFile);
			}
		};
		$scope.playArmsVideo = function() {
			MediaService.playVideo($scope,'arms.mp4');
		};
		/* FNA Changes by LE team Start (this existing line-of-code is commented out) */
		/* 
		$scope.okBtnAction = function() {
			$location.path('/MyFamilyDetails');															
		}
		*/
		/* FNA Changes by LE team End */
		
		/* Revamped product listing logic */	
		$scope.listSelectedProducts = function(type){
			$scope.showProducts = true;
			$scope.uniqueProductGroup = [];
			for(var x = 0; x < $scope.allproductsList.length; x++){
				if($scope.allproductsList[x].productGroup == type){
					$scope.uniqueProductGroup.push($scope.allproductsList[x]);
				}
			}
			$scope.slideCount=Math.ceil($scope.uniqueProductGroup.length/4);
		}
		$scope.backToProductsGroup = function(){
			$scope.showProducts = false;	
		}
		/* Revamped product listing logic */
		
		$scope.showKeyFeatures = function(divID) {
			$scope.keyFeaturesOverlay = true;
			$(divID).css('display', 'inline');
		}
		$scope.hideKeyFeatures = function(divID) {
			$scope.keyFeaturesOverlay = false;
			$(divID).css('display', 'none');
		}
		
		$scope.$on("$destroy", function() {
			/* FNA Changes by LE Team (this existing line-of-code is commented out) Start */
			/*
            if (this.$$destroyed) return;     
			while (this.$$childHead) {      
				this.$$childHead.$destroy();    
			}     
			if (this.$broadcast) { 
				this.$broadcast('$destroy');   
			}   
			this.$$destroyed = true;                 
			//$timeout.cancel( timer );  
			*/
			if (this != null && typeof this !== 'undefined') {
				if (this.$$destroyed) return;
				while (this.$$childHead) {
					this.$$childHead.$destroy();
				}
				if (this.$broadcast) {
					this.$broadcast('$destroy');
				}
				this.$$destroyed = true;
				//$timeout.cancel( timer );    
			}
        });
}]);