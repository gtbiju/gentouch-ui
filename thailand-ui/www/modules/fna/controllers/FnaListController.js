﻿
storeApp.controller('FnaListController', ['$rootScope', '$scope', 'DataService', 'FnaService','LookupService', 'DocumentService', 'ProductService','$compile', 
'$routeParams', '$location', '$translate','IllustratorVariables', 'FnaVariables', 'UtilityService','$debounce', 'PersistenceMapping', 'AutoSave',
 'globalService','$timeout','UtilityVariables',
function($rootScope, $scope, DataService, FnaService, LookupService,DocumentService, ProductService, $compile, $routeParams, $location, $translate, 
IllustratorVariables,FnaVariables, UtilityService,$debounce, PersistenceMapping, AutoSave, globalService,$timeout,UtilityVariables) {

	$rootScope.moduleHeader = "fna.fnaHeading";
	$rootScope.moduleVariable = translateMessages($translate,
			$rootScope.moduleHeader);
	$scope.fnaName = 'FNA Controller';
	$scope.namePlaceHolder = translateMessages($translate, "general.partyName");
	$scope.statusPlaceHolder = translateMessages($translate, "status");
	$scope.dobPlaceHolder = translateMessages($translate,
			"general.partyDateOfBirth");

	// $scope.fnaList=[{"TransactionData":{"Product":{"ProductDetails":{"familyType":"30","productType":""}},"Insured":{"BasicDetails":{"lastName":"Jose","firstName":"Tom"}}},"Key2":"Agent5","Key1":"C101","Key4":"03-01-2014","Key3":"PRD1","Key6":"627814","Type":"illustration","Key5":"","Key7":"Final","Id":""},{"TransactionData":{"Product":{"ProductDetails":{"familyType":"30","productType":""}},"Insured":{"BasicDetails":{"lastName":"Jose","firstName":"Tom"}}},"Key2":"Agent5","Key1":"C101","Key4":"03-01-2014","Key3":"PRD1","Key6":"627814","Type":"illustration","Key5":"","Key7":"Final","Id":""}];
	$scope.Status = $routeParams.status;
	$scope.filterOpen = false;
	$scope.tblhead1 = false;
	$scope.tblhead2 = false;
	$scope.tblhead3 = false;
	$scope.tblhead4 = false;
	$scope.tblhead5 = false;
	$scope.tblhead6 = false;
	$scope.predicate = '';
	$scope.reverse = false;
	$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
	$scope.refresh();

	$scope.mapKeysforPersistence = function() {
		PersistenceMapping.Key4 = ($routeParams.proposalId != null && $routeParams.proposalId != 0) ? $routeParams.proposalId
				: "";
		PersistenceMapping.Key5 = $scope.productId != null ? $scope.productId
				: "PRD1";
		PersistenceMapping.Key10 = "FullDetails";
		if (!(rootConfig.isDeviceMobile)) {
			if ($routeParams.fnaId == null || $routeParams.fnaId == 0) {
				//PersistenceMapping.Key13 = UtilityService.getFormattedDate();
				PersistenceMapping.creationDate = UtilityService
												.getFormattedDateTime();
			}
		}
		PersistenceMapping.dataIdentifyFlag = false;
		PersistenceMapping.Key15 = $scope.status != null ? $scope.status
				: "Saved";
		PersistenceMapping.Type = "FNA";
	}

	$scope.onGetListingsSuccess = function(data) {
		if (!rootConfig.isDeviceMobile) {
			$scope.fnaList = data.sort(function(a, b) {
				return LEDate(b.modifiedDate)
							- LEDate(a.modifiedDate);
			});
		} else {
			$scope.fnaList = data;
		}
		$scope.tableFNAList =  [].concat($scope.fnaList);
		$scope.refresh();
		$rootScope.showHideLoadingImage(false);
	}

	$scope.onGetListingsError = function(data) {

		$scope.refresh();
		$rootScope.showHideLoadingImage(false);
	}


	$scope.toggleFilterSection = function() {
		$scope.filterOpen = !($scope.filterOpen);
		if ($scope.filterOpen == false) {
			$scope.refreshFilter();
		}
	}

	$scope.refreshFilter = function() {
		$scope.Name = "";
		$scope.dob = "";
        <!--  FNA changes by LE Team >>>
        $scope.Key13 = "";
		$scope.statusFilter = "";
	}

	$scope.syncerror = function(statusMsg, errorMsg) {

		if (statusMsg == 'SyncError') {
			$rootScope.lePopupCtrl.showError(translateMessages($translate,
					"lifeEngage"), translateMessages($translate, errorMsg),
					translateMessages($translate, "general.ok"), function() {
						$scope.exitApp = true;
					});
		}

	};

	$scope.statusOptionsFNA = FnaVariables.getStatusOptionsFNA();

	$scope.createNew = function(fna) {
		var parties = [];
		globalService.setParties(parties);
		$rootScope.transactionId = 0;
		FnaVariables.clearFnaVariables();
		$location.path("/fnaLandingPage/0/0");
	}
	$scope.editFNA = function(fnaId, transId) {
		// alert(transId)
		if(fnaId == ""){
			fnaId=0;
			}
		FnaVariables.fnaId = fnaId;
		$rootScope.transactionId = transId;
		$location.path("/fnaLandingPage/" + fnaId + "/" + transId);
	}
	$scope.callback = function() {
		$rootScope.showHideLoadingImage(false);
	}
	$scope.orderValues = function(key, e) {
		$scope.predicate = key;
		$scope.reverse = !($scope.reverse);
	}
	$scope.listUiJsonFile = FnaVariables.uiJsonFile.fnaListFile;
	$scope.initialLoad= function(){
	LEDynamicUI
			.paintUI(rootConfig.template, $scope.listUiJsonFile, "FNAList",
					"#fnaList", true, function() {
						$scope.callback();
						PersistenceMapping.clearTransactionKeys();
						$scope.mapKeysforPersistence();
						var  searchCriteria = {
									 searchCriteriaRequest:{
							"command" : "ListingDashBoard",
							"modes" :['FNA'],
							"value" : ""
							}
						};
						var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
						transactionObj.Id = $routeParams.transactionId;
						transactionObj.Key2 = null;
						transactionObj.Key10 = "BasicDetails";
						$scope.searchFNA=$rootScope.searchValue;
						$rootScope.searchValue="";
						DataService.getFilteredListing(transactionObj,
								$scope.onGetListingsSuccess,
								$scope.onGetListingsError);
					}, $scope, $compile);
	}
	$scope.$on('$viewContentLoaded', function(){
		$scope.initialLoad();
	});

}]);

