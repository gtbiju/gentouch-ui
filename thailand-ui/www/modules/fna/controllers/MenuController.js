/*  menuController
 * define the supporting languages
 */
storeApp
		.controller(
				'MenuController',
				[
						'$routeParams',
						'$route',
						'$rootScope',
						'$scope',
						'$translate',
						'$location',
						'$compile',
						'UtilityService',
						'DataService',
						'LookupService',
						'DocumentService',
						'IllustratorVariables',
						'PersistenceMapping',
						'FnaVariables',
						'globalService',
						'UserDetailsService',
						'SyncService',
						function($routeParams, $route, $rootScope, $scope,
								$translate, $location, $compile,
								UtilityService, DataService, LookupService,
								DocumentService, IllustratorVariables,
								PersistenceMapping, FnaVariables,
								globalService, UserDetailsService,SyncService) {
							$rootScope.startupPopup = false;
							$scope.syncProgress = 0;
							$scope.isSycnCloseEnabled = false;
							$rootScope.syncDataTime = localStorage
									.getItem('syncTime');
							if (localStorage.getItem('syncTime') != undefined) {
								$rootScope.syncDataTime = localStorage
										.getItem('syncTime');
							} else {
								$rootScope.syncDataTime = "";
							}
							if (localStorage["locale"] != undefined) {
								$scope.selectedLanguage = localStorage["locale"];
							} else {
								$scope.selectedLanguage = rootConfig.locale;
							}
							$scope.popup_theme_switcher_container = false;
							$scope.themes = [ {
								"key" : "MainTheme",
								"value" : "Theme 1"
							}, {
								"key" : "ABCTHEME",
								"value" : "Theme 2"
							} ];
							$scope.onLanguageChange = function() {
								$translate.use($scope.selectedLanguage);
								localStorage["locale"] = $scope.selectedLanguage;
								$rootScope.moduleVariable = translateMessages($translate, $rootScope.moduleHeader);
								$scope.hideThemePopup();
							};
							$scope.selectedTheme = "MainTheme";
							$scope.onThemeChange = function(themeName) {
								if (!(angular.isDefined(themeName))) {
									themeName = $scope.selectedTheme;
								}
								UtilityService.initiateThemeChange(themeName);
								$scope.hideThemePopup();
							};
							$rootScope.alertObj = {};
							$rootScope.cancel = function() {
								$rootScope.alertObj.showPopup = false;
							};
							$rootScope.okClick = function() {
								$rootScope.alertObj.showPopup = false;
							};
							$rootScope.showMessagePopup = function(isShow,
									type, msg) {
								$rootScope.alertObj.showPopup = isShow;
								$rootScope.alertObj.confirmationType = type;
								$rootScope.alertObj.alertMessage = msg;
								$rootScope.refresh();
							};
							$scope.hideThemePopup = function() {
								$rootScope.isPopupDisplayed = !$rootScope.isPopupDisplayed;
								$scope.popup_theme_switcher_container = false;
							};
							$scope.logout = function() {
								$location.path('/login');
								$scope.isAuthenticated  = false;
							};
							$scope.settingpopup = function() {
								$rootScope.isPopupDisplayed = !$rootScope.isPopupDisplayed;
								$scope.popup_theme_switcher_container = true;
							};
							//SYNC STARTS
							// Sync Completely(To and From) and showthe progress bar
							$scope.syncCompletely = function() {
								// TODO move messages to resources
								$scope.syncProgress = 0;
								$scope.showSycnPopup();
								$scope.isSycnCloseEnabled = false;
								// Show the sync popup
								if (!checkConnection()) {
									$scope.syncMessage = "Network not available..";
									$scope.isSycnCloseEnabled = true;
									$scope.refresh();
								} else {
									//set TranstrackingID if not already set
									SyncService.setTransTrackingID($scope.setTransTrackingIdSuccess);
								}
							};
							$scope.setTransTrackingIdSuccess= function(){
								SyncService.initialize(rootConfig.syncConfig,$scope.syncSuccessCallback,$scope.syncProgressCallback);
								SyncService.runSyncThread();
							};
							$scope.syncProgressCallback = function(syncMessage,progress){
								// set the progress percentage and message
								$scope.syncProgress=progress;
								$scope.syncMessage=syncMessage;
								$scope.refresh();
							};
							$scope.syncSuccessCallback = function(syncFailed, syncWarning ,syncDocFailed,syncDocDownloadFailed){
								// The sync  completed for all modules
								//Call Email Service to mail BI.
                                // PersistenceMapping.clearTransactionKeys();
                                // var transactionObjForMailReq = PersistenceMapping.mapScopeToPersistence({});
                                // var userDetails = UserDetailsService.getUserDetailsModel();
                                // UtilityService.emailServiceCall(userDetails,transactionObjForMailReq);                                                             
                                //Email Service to mail BI ends.
                               //Start Archiving 
                            	$scope.syncMessage="Archiving...";
                            	$scope.syncProgress=99;
                            	$scope.refresh();
                            	SyncService.doArchiving($scope.archieveCompleted);
							};
							$scope.archieveCompleted = function(syncFailed,syncWarning,syncDocFailed,syncDocDownloadFailed) {
								if(!syncFailed && !syncWarning && !syncDocFailed && !syncDocDownloadFailed )
									{
									$scope.syncMessage = translateMessages($translate, "general.allRecordSynced");
									var syncTime = UtilityService.getFormattedDate();
									localStorage.setItem('syncTime',"Last synced on : " + syncTime);
									$rootScope.syncDataTime = localStorage.getItem('syncTime');
									}else{
										if (syncFailed) {
		                                    $scope.syncMessage =  translateMessages($translate, "general.errorDuringSync");
											localStorage.setItem('syncTime',"Sync Failed");
											$rootScope.syncDataTime = localStorage.getItem('syncTime');
										} else if(syncWarning || syncDocFailed || syncDocDownloadFailed ){
		                                    $scope.syncMessage = translateMessages( $translate, "general.syncCompletedWithErrors");
		                                    var syncTime = UtilityService.getFormattedDate();
											localStorage.setItem('syncTime',"Last synced on : " + syncTime);
											$rootScope.syncDataTime = localStorage.getItem('syncTime');
										}
									}
								$scope.syncProgress=100;
								$scope.isSycnCloseEnabled = true;
								$scope.refresh();
							};
							$scope.showSycnPopup = function() {// Show sync popup
								$rootScope.startupPopup = true;
								$scope.syncProgress = 0;
								$scope.refresh();
							};
							$scope.hideSyncPopup = function() {// Hide the sysnc popup
								$rootScope.startupPopup = false;
								if($scope.syncProgress==100)
								$route.reload();
								$scope.syncProgress = 0;
								$scope.syncMessage = "";
							};
				//SYNC ENDS

						} ]);
