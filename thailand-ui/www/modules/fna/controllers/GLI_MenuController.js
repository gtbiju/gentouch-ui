/*
 *Copyright 2015, LifeEngage 
 */



storeApp.controller('GLI_MenuController', ['$routeParams', '$route', '$rootScope', '$scope', '$translate', '$location', '$compile', 'UtilityService', 'DataService', 'LookupService', 'DocumentService', 'IllustratorVariables', 'PersistenceMapping', 'FnaVariables' ,'globalService','$controller', 'UserDetailsService','AgentService','GLI_DataService','EappVariables','GLI_EappVariables', function($routeParams, $route, $rootScope, $scope, $translate, $location, $compile, UtilityService, DataService, LookupService, DocumentService, IllustratorVariables, PersistenceMapping, FnaVariables, globalService, $controller, UserDetailsService,AgentService,GLI_DataService,EappVariables,GLI_EappVariables) {
	       $controller('MenuController', {$routeParams: $routeParams, $route:$route, $rootScope:$rootScope, $scope:$scope, $translate:$translate, $location:$location, $compile:$compile, UtilityService:UtilityService, DataService:DataService, LookupService:LookupService, DocumentService:DocumentService, IllustratorVariables:IllustratorVariables, PersistenceMapping:PersistenceMapping, FnaVariables:FnaVariables, globalService:globalService, UserDetailsService:UserDetailsService});
            if(rootConfig.isDeviceMobile==true || rootConfig.isOfflineDesktop==true){
                                $scope.enableSync=true;
                            }
			$scope.user=UserDetailsService.getUserDetailsModel();
			
			$scope.updateProgressColor;
			 

			//configuring settings button
			$scope.showSettingIconForList = rootConfig.showSettingIconFor;
			$rootScope.$on( "$routeChangeSuccess", function(event, next, current) {
					$scope.showSettingIcon = false; 
					for(var i =0;i < $scope.showSettingIconForList.length; i++){
					if(next.originalPath){
						if(next.originalPath.indexOf($scope.showSettingIconForList[i])>0){
						  $scope.showSettingIcon = true;
						break;
						}
					}
					}
			});
			$("ul.list-unstyled").on("click", ".init", function(event) {
			    $(this).closest("ul.list-unstyled").children('li:not(.init)').toggle();
				event.stopImmediatePropagation();
			});

			var allOptions = $("ul.list-unstyled").children('li:not(.init)');
			$("ul.list-unstyled").on("click", "li:not(.init)", function() {
			    allOptions.removeClass('selected');
			    $(this).addClass('selected');
			    $("ul.list-unstyled").children('.init').html($(this).html());
			    allOptions.toggle();
			});
			
			
			$scope.logoutpopupshow = function()
                {
                    
                     $scope.logoutpopup = true;
                      
                }

			$scope.logout = function() {
				$rootScope.isAuthenticated = false;
				var flow = localStorage.getItem('loginFlow');
				if(flow && flow == "internal") {
					$location.path('/login-internal');
				} else {
					if ((rootConfig.isDeviceMobile)){

						$location.path('/login');
						localStorage.removeItem('loginFlow');
						 $scope.logoutpopup = false;
					} else {
						$location.path('/login');
						localStorage.removeItem('loginFlow');
						 $scope.logoutpopup = false;
					}
				}
			};

			 $scope.closePopup = function () {
            $scope.logoutpopup = false;
            $scope.showPopUpServiceMsg=false;
            $scope.overlayTC = false;
        };    
			// overriding onLanguageChange() add broadcast 
			$scope.onLanguageChange = function() {
				$translate.use($scope.selectedLanguage);
				localStorage["locale"] = $scope.selectedLanguage;
				$rootScope.moduleVariable = translateMessages($translate, $rootScope.moduleHeader);
				$rootScope.$broadcast('lang-changed');
				$scope.hideThemePopup();
			};
	       
		   $scope.setLanguage = function(lang){
								if(lang !== $scope.selectedLanguage){
									$scope.selectedLanguage = lang;
									$scope.onLanguageChange();
								}
								$("ul.list-unstyled").children('li:not(.init)').toggle();
							};
							
		   $scope.syncProgressCallback = function(syncMessage,progress){
				// set the progress percentage and message
				$scope.syncProgress=progress;
				$scope.syncMessage=syncMessage;
				if($scope.syncProgress > 50){
					$scope.updateProgressColor = 'whiteTxt';
				}
				else{
					$scope.updateProgressColor = '';
				}
				$scope.refresh();
			};
			//Extended for implemeting SPAJ retrival on sync success
			$scope.archieveCompleted = function(syncFailed,syncWarning,syncDocFailed,syncDocDownloadFailed) {
				if(!syncFailed && !syncWarning && !syncDocFailed && !syncDocDownloadFailed )
					{
					$scope.syncMessage = translateMessages($translate, "general.allRecordSynced");
					var syncTime = UtilityService.getFormattedDate();
					localStorage.setItem('syncTime',"Last synced on : " + syncTime);
					$rootScope.syncDataTime = localStorage.getItem('syncTime');
					}else{
						if (syncFailed) {
                            $scope.syncMessage =  translateMessages($translate, "general.errorDuringSync");
							localStorage.setItem('syncTime',"Sync Failed");
							$rootScope.syncDataTime = localStorage.getItem('syncTime');
						} else if(syncWarning || syncDocFailed || syncDocDownloadFailed ){
                            $scope.syncMessage = translateMessages( $translate, "general.syncCompletedWithErrors");
                            var syncTime = UtilityService.getFormattedDate();
							localStorage.setItem('syncTime',"Last synced on : " + syncTime);
							$rootScope.syncDataTime = localStorage.getItem('syncTime');
						}
					}
				$scope.syncProgress=100;
				$scope.isSycnCloseEnabled = true;
				$scope.refresh();
			};
				
			 $scope.$on("$destroy", function() {
                 if (this.$$destroyed) return;     
				 while (this.$$childHead) {      
				 this.$$childHead.$destroy();    
				 }     
				 if (this.$broadcast)
				{ 
					this.$broadcast('$destroy');   
				}    
				 this.$$destroyed = true;                 
				 //$timeout.cancel( timer );                           
             });
    
            $scope.eappRefresh=function(){
                $rootScope.showHideLoadingImage(true, "");
                
                if(parseInt(IllustratorVariables.leadId)!==0 && IllustratorVariables.leadId!==undefined && IllustratorVariables.leadId!==""){
                    $location.path('MyAccount/eApp/');
				    $rootScope.fromMenuController=true;
                    $rootScope.fromMenuControllerLeadId=angular.copy(IllustratorVariables.leadId);    
                }else{                    
                    $location.path('MyAccount/eApp/');                    
                    $rootScope.searchValue = angular.copy(EappVariables.EappSearchValue);
                    EappVariables.searchKey = angular.copy(EappVariables.EappSearchKey);
                }
                            
                $rootScope.showHideLoadingImage(false, "");
                $scope.refresh();
            }
	
}]);