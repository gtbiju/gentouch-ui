
/*Name:GLI_ProductListingController
 * For implementing GLI specific changes
 */
storeApp.controller('GLI_ChoosePartiesController',['$rootScope', '$scope', '$location', '$compile','FnaVariables', 'UtilityService', '$translate', 'globalService', 'IllustratorVariables','EappVariables', '$routeParams', 'FnaService', 'GLI_FnaService', 'DataService','GLI_EappVariables', 'GLI_globalService', '$controller',
	                                                function GLI_ChoosePartiesController($rootScope, $scope, $location, $compile, FnaVariables, UtilityService, $translate, globalService, IllustratorVariables,EappVariables, $routeParams, FnaService, GLI_FnaService, DataService,GLI_EappVariables,GLI_globalService,$controller){
	                                                	$controller('ChoosePartiesController',{$rootScope:$rootScope, $scope:$scope, $location:$location, $compile:$compile, FnaVariables:FnaVariables, UtilityService:UtilityService, $translate:$translate, globalService:globalService, IllustratorVariables:IllustratorVariables,EappVariables:EappVariables, $routeParams:$routeParams, FnaService:GLI_FnaService, DataService:DataService,GLI_globalService:GLI_globalService});
	                                                	

			// Set the page name for progressbar implementation - Required in each page
			// specific controller
	        $rootScope.selectedPage = "ChooseParty";
			FnaVariables.selectedPage = "ChooseParties";
			$scope.status = FnaVariables.getStatusOptionsFNA()[0].status;
			$scope.selectedpage = FnaVariables.selectedPage;
			/* bug 4185 starts */
			$scope.isFemale=false;
			/* bug 4185 ends */
			$rootScope.lePopupCtrl = {
			};
			// Get the latest FNA Model from global service
			$scope.FNAObject = FnaVariables.getFnaModel();
			if (UtilityService.previousPage == 'FNA' || UtilityService.previousPage == 'IllustrationPersonal') {
				if(UtilityService.previousPage == 'FNA'){
					$scope.FNAObject.FNA.transactionId = $rootScope.transactionId;
				}else{
					$rootScope.transactionId = $scope.FNAObject.FNA.transactionId
				}
			}
			// Set the next page button name based on the next screen to be displayed
			/* FNA Changes by LE Team Start */
            /*
            if (UtilityService.previousPage == 'FNA'
					|| UtilityService.previousPage == 'IllustrationPersonal' || UtilityService.previousPage == 'LMS') {
				$scope.nextButtonLabel = "proceedtoIllustartionHeaderText";
				$rootScope.moduleVariable = translateMessages($translate, "illustrator.Illustrator");
			} else if (UtilityService.previousPage == 'Illustration') {
				$scope.nextButtonLabel = "proceedToEapp";
				$rootScope.moduleVariable = translateMessages($translate, "eapp_vt.eAppHeader");
			}
            */
	       $rootScope.moduleHeader = "fna.fnaHeading";
	       $rootScope.moduleVariable = translateMessages($translate, $rootScope.moduleHeader);
			
			/**
			 * Set the insured image and related params to scope variable to get in
			 * partial --Begin *
			 */
			
			if (UtilityService.previousPage == 'LMS'){
				$scope.FNAMyself = globalService.getParty("Lead");
			}else{
				$scope.FNAMyself = globalService.getParty("FNAMyself");
			 }
			
			if ($scope.FNAMyself.BasicDetails.photo == ""
					|| $scope.FNAMyself.BasicDetails.photo == undefined) {
				$scope.FNAMyself.BasicDetails.photo = FnaVariables.choosePartiesPhoto;
			}
			
			/**
			 * Set the Beneficiary image and related params to scope variable to get in
			 * partial --Begin *
			 */
			$scope.beneficiaries = globalService.getFNABeneficiaries();
			$scope.members = FnaVariables.images;
			$scope.beneficiaryData = [];
			$scope.additionalInsuredData = [];
			$scope.remainingBeneficiaries = [];// To set all beneficiaries back to
			// globalService
			var checkFlag = "";
			// To display only first 5 beneficiaries from family screen
			var beneficiaryCount = 0;// To display only first 5 beneficiaries from
			// family screen
			for (var i = 0; i < $scope.beneficiaries.length; i++) {
				if ($scope.beneficiaries[i] != null && i <= 10) {
					beneficiaryCount++;
					$scope.beneficiaryData[i] = angular.copy($scope.beneficiaries[i]);
					//$scope.additionalInsuredData[i] = JSON.parse(JSON.stringify($scope.beneficiaries[i]));
					// Set the image
					if ($scope.beneficiaries[i].BasicDetails == undefined) {
						$scope.beneficiaries[i].BasicDetails = {};
					}
			
					if ($scope.beneficiaries[i].BasicDetails.photo == undefined) {
						if($scope.beneficiaries[i].BasicDetails != undefined){
							$scope.beneficiaryData[i].BasicDetails.photo = FnaVariables.choosePartiesPhoto;
							//$scope.additionalInsuredData[i].BasicDetails.photo = FnaVariables.choosePartiesPhoto;
						}
					}
			
				} else if ($scope.beneficiaries[i] != null && i > 10) { // To set all
					// beneficiaries
					// back to
					// globalService
					$scope.remainingBeneficiaries.push($scope.beneficiaries[i]);
				}
			}
			/**
			 * Set the Beneficiary image and related params to scope variable to get in
			 * partial --End *
			 */
			
			// To prepopulate selected data on page load
			$scope.prePopulateData = function() {
				// $( ".myself" ).draggable({ revert: true, helper: "clone",
				// revertDuration: 100 });
				// To populate the already dragged insurance/proposer and beneficiary in
				// edit mode -- Proceed to eApp from illustration
				// From FNA -- Always new illustration will be created
				if (UtilityService.previousPage == 'Illustration'
						|| UtilityService.previousPage == 'IllustrationPersonal') {
					var parties = globalService.getParties();
					for (var i = 0; i < parties.length; i++) {
						var party = parties[i];
						if (party.isInsured) {
							
							var myEl = angular.element(document.querySelector('#insuredLst'));
							/* bug 4185 starts */
							var gender_party=party.BasicDetails.gender;
							if(gender_party == 'Female'){
								$scope.isFemale = true;
							}
							$('#insuredLst').children().append(
									'<span class="myself round-icon bs-80 '+[[party.imageClass]]+'" ng-class="{true: \'female-image\', false: \'male-image\'}['+[[$scope.isFemale]]+']"><img alt="" src="data:image/jpeg;base64,'+[ [ party.BasicDetails.photo ] ]+'" />'
											+ '<span class="myself_name">'
											+ [ [ party.BasicDetails.firstName ] ]
											+ '</span></span>');
							/*$('#insuredLst').children().append(
									'<span class="myself round-icon bs-80 '+[[party.imageClass]]+'" ng-class="{true: \'female-image\', false: \'male-image\'}[party.BasicDetails.gender== \'Female\']"><img alt="" src="data:image/jpeg;base64,'+[ [ party.BasicDetails.photo ] ]+'" />'
											+ '<span class="myself_name">'
											+ [ [ party.BasicDetails.firstName ] ]
											+ '</span></span>'); */
							$compile(myEl)($scope);
							/* bug 4185 ends */
						}
						if (party.isPayer) {
							/*$('#proposedLst').children().append(
									"<span class='myself round-icon bs-80' ng-class='{true: 'female-image', false: 'male-image'}[party.BasicDetails.gender== 'Female']'><img alt='' src='data:image/jpeg;base64,"
											+ [ [ party.BasicDetails.photo ] ] + "' />"
											+ "<span class='myself_name'>"
											+ [ [ party.BasicDetails.firstName ] ]
											+ "</span></span>");*/
							var myEl = angular.element(document.querySelector('#proposedLst'));
							/* bug 4185 starts */
							var gender_party=party.BasicDetails.gender;
							if(gender_party=='Female')
							{
								$scope.isFemale=true;
							}
							$('#proposedLst').children().append(
									'<span class="myself round-icon bs-80 '+[[party.imageClass]]+'" ng-class="{true: \'female-image\', false: \'male-image\'}['+[[$scope.isFemale]]+']"><img alt="" src="data:image/jpeg;base64,'+[ [ party.BasicDetails.photo ] ]+'" />'
											+ '<span class="myself_name">'
											+ [ [ party.BasicDetails.firstName ] ]
											+ '</span></span>');
							/*$('#proposedLst').children().append(
									'<span class="myself round-icon bs-80 '+[[party.imageClass]]+'" ng-class="{true: \'female-image\', false: \'male-image\'}[party.BasicDetails.gender== \'Female\']"><img alt="" src="data:image/jpeg;base64,'+[ [ party.BasicDetails.photo ] ]+'" />'
											+ '<span class="myself_name">'
											+ [ [ party.BasicDetails.firstName ] ]
											+ '</span></span>');*/
							$compile(myEl)($scope);
							/* bug 4185 ends */
						}
						if (party.isBeneficiary) {
			
							var dependentindex = party.id;// For delete flow of
							// beneficairy
			
							/*var myEl = angular.element(document
									.querySelector('#benefLst'));
							myEl
									.append('<li class="cpLstItem" show-scrap> <span class="dependent round-icon bs-50 '+[[party.imageClass]]+'" ng-class="{true: \'female-image\', false: \'male-image\'}[party.BasicDetails.gender== \'Female\']" id="'
											+ dependentindex
											+ '"> <img alt="" src="data:image/jpeg;base64,
											+ [ [ party.BasicDetails.photo ] ]
											+ '" />'
											+ '<span class="myself_name">'
											+ [ [ party.BasicDetails.firstName ] ]
											+ '</span>' + '</span></li>');
							$compile(myEl)($scope);*/
							var myEl = angular.element(document
									.querySelector('#benefLst'));
							/* bug 4185 starts */
							var gender_party=party.BasicDetails.gender;
							if(gender_party=='Female')
							{
								$scope.isFemale=true;
							}
							/* FNA changes made by LE Team for Bug 4303 starts */
							var itemStr = '<li class="cpLstItem" show-scrap> ';
									if(party.imageClass) {
										itemStr += '<span class="dependent round-icon bs-80 '+[[party.imageClass]]+'" id="'+ dependentindex+ '">';
									} else {
										itemStr += '<span class="myself round-icon bs-80 " ng-class="{true: \'female-image\', false: \'male-image\'}['+[[$scope.isFemale]]+']" id="'+ dependentindex+ '">';
									}
									itemStr += ' <img alt="" src="data:image/jpeg;base64,'+ [ [ party.BasicDetails.photo ] ] + '" />'
											+ '</span></li>';
							/* bug 4185 ends */			
									myEl.append(itemStr);
									
							/*myEl
									.append('<li class="cpLstItem" show-scrap> '
											+'<span class="dependent round-icon bs-80 '+[[party.imageClass]]+'" ng-class="{true: \'female-image\', false: \'male-image\'}[party.BasicDetails.gender== \'Female\']" id="'+ dependentindex+ '">'
											+' <img alt="" src="data:image/jpeg;base64,'+ [ [ party.BasicDetails.photo ] ] + '" />'
											+ '</span></li>');*/
							$compile(myEl)($scope);
			
						}
						/* FNA changes made by LE Team for Bug 4303 ends */
						if (party.isAdditionalInsured) {
							
							var dependentindex = party.id;// For delete flow of
							// beneficairy
			
							/*var myEl = angular.element(document
									.querySelector('#addnlInsuredLst'));
							myEl
									.append('<li class="cpLstItem" show-scrap> <span class="dependent round-icon bs-50 '+[[party.imageClass]]+'" ng-class="{true: \'female-image\', false: \'male-image\'}[party.BasicDetails.gender== \'Female\']" id="'
											+ dependentindex
											+ '"> <img alt=""  src="data:image/jpeg;base64,'
											+ [ [ party.BasicDetails.photo ] ]
											+ '" />'
											+ '<span class="myself_name" >'
											+ [ [ party.BasicDetails.firstName ] ]
											+ '</span>' + '</span></li>');
							$compile(myEl)($scope); */
							var myEl = angular.element(document
									.querySelector('#addnlInsuredLst'));
									
							var itemStr = '<li class="cpLstItem" show-scrap> ';
									if(party.imageClass) {
										itemStr += '<span class="dependent round-icon bs-50 '+[[party.imageClass]]+'" id="'+ dependentindex+ '">';
									} else {
										itemStr += '<span class="myself round-icon bs-50 " ng-class="{true: \'female-image\', false: \'male-image\'}[party.BasicDetails.gender== \'Female\']" id="'+ dependentindex+ '">';
									}
									itemStr += ' <img alt="" src="data:image/jpeg;base64,'+ [ [ party.BasicDetails.photo ] ] + '" />'
											+ '</span></li>';
											
									myEl.append(itemStr);
									
											
							/*myEl
									.append('<li class="cpLstItem" show-scrap> <span class="dependent round-icon bs-50 '+[[party.imageClass]]+'" ng-class="{true: \'female-image\', false: \'male-image\'}[party.BasicDetails.gender== \'Female\']" id="'+ dependentindex+ '"> <img alt="" src="data:image/jpeg;base64,'+ [ [ party.BasicDetails.photo ] ]
											+ '" />'
											+ '</span></li>');*/
							$compile(myEl)($scope);
			
						}
					}
				}
			
			};
			
			/** ON CLICK OF THE DROPPED ITEM, IT SHOULD SHOW THE SCRAP POP UP -- Begin * */
			$scope.thisObjDeletion;
			$scope.benef_id;
			$scope.currentlyRemovedId;
			
			$scope.onDelete = function() {
				var chkFlag = $('.cp_scrapButton').css("display");
				if (chkFlag == "block") {
			
					var childId = $($scope.thisObjDeletion).children();
					// Remove the binding from model
			
					if ($scope.benef_id == "insuredLst") {
						$scope.FNAMyself.isInsured = false;
						$scope.FNAMyself.fnaInsuredPartyId = "";
						for (var i = 0; i < $scope.beneficiaryData.length; i++) {
							$scope.beneficiaryData[i].isInsured = false;
							$scope.beneficiaryData[i].fnaInsuredPartyId = "";
						}
						if($scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured == "Yes"){
							$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "No";
						}
						$scope.proceedDisable = false;
			
					} else if ($scope.benef_id == "proposedLst") {
						$scope.FNAMyself.isPayer = false;
						$scope.FNAMyself.fnaPayerPartyId = "";
						for (var i = 0; i < $scope.beneficiaryData.length; i++) {
							$scope.beneficiaryData[i].isPayer = false;
							$scope.beneficiaryData[i].fnaPayerPartyId = "";
						}
						if($scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured == "Yes"){
							$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "No";
						}
						$scope.proceedDisable = false;
					} 
					else if ($scope.benef_id == "addnlInsuredLst"){
						
						if ($scope.currentlyRemovedId && $scope.currentlyRemovedId.length > 0
								&& $scope.currentlyRemovedId.indexOf("myself") > -1) {
							$scope.FNAMyself.isAdditionalInsured = false;
							$scope.FNAMyself.fnaBenfPartyId = "";
						} else {
							for (var i = 0; i < $scope.beneficiaryData.length; i++) {
								if ($scope.beneficiaryData[i].id == $scope.currentlyRemovedId) {
									$scope.beneficiaryData[i].isAdditionalInsured = false;
									$scope.beneficiaryData[i].fnaBenfPartyId = "";
									break;
								}
							}
						}
						
						for(var m=0;m<$scope.beneficiaryData.length;m++){
							if($scope.currentlyRemovedId == $scope.beneficiaryData[m].id){
								if(($scope.beneficiaryData[m].classNameAttached == 'Son' || $scope.beneficiaryData[m].classNameAttached == 'Daughter')&& $scope.addtnlInsuredChildren>0){
									$scope.addtnlInsuredChildren = $scope.addtnlInsuredChildren-1;
								}
							}
						}
						$scope.proceedDisable = false;
					}
					else {
//						/*FNA changes by LE Team -Fix for bug#4219 Starts */
						if ($scope.currentlyRemovedId
								&& $scope.currentlyRemovedId.length > 0
								&& $scope.currentlyRemovedId.indexOf("myself") > -1) {
							$scope.FNAMyself.isBeneficiary = false;
							$scope.FNAMyself.fnaBenfPartyId = "";

							/*FNA changes by LE Team -Fix for bug#4219 Ends */
						} else {
							for (var i = 0; i < $scope.beneficiaryData.length; i++) {
								if ($scope.beneficiaryData[i].id == $scope.currentlyRemovedId) {
									$scope.beneficiaryData[i].isBeneficiary = false;
									$scope.beneficiaryData[i].fnaBenfPartyId = "";
									break;
								}
							}
						}
						$scope.proceedDisable = false;
			
					}
					$scope.refresh();
			
					$($scope.thisObjDeletion).children().remove();
			
					if ($scope.benef_id == "benefLst") {
						$($scope.thisObjDeletion).remove();
					} else if($scope.benef_id == "addnlInsuredLst") {
						$($scope.thisObjDeletion).remove();
					}
				}
				$('.cp_scrapButton').css("display", "none");
				event.stopPropagation();
			
			};
			
			/** ON CLICK OF THE DROPPED ITEM, IT SHOULD SHOW THE SCRAP POP UP -- End * */
			$scope.myselfChoosePartyDragDrop = function() {
				$(".cp_draggable_section .myself").draggable({
					revert : true,
					helper : "clone",
					revertDuration : 0

				});
				$scope.choosePartyDrop();
			};
			
			$scope.dependantChoosePartyDragDrop = function() {
				$(".cp_draggable_section .dependent").draggable({
					revert : true,
					helper : "clone",
					revertDuration : 0
				});
				$scope.choosePartyDrop();
			};
			
			/** Function for droppable implementation of parties -- Begin * */
			$scope.choosePartyDrop = function() {
			
				var flagCountInsured = 0;
				var flagCountPropose = 0;
				var flagCountBenef = 0;
				var flagCountAddnlInsured = 0;
				$scope.addtnlInsuredChildren = 0;
				
				$(".cp_insured_section,.cp_beneficiary_section,.cp_proposer_section")
						.droppable(
								{
									hoverClass: 'drop-active',
									drop : function(event, ui) {
										var cloneItem = ui.draggable.clone();
										var currentlyDraggedClass = $(ui.draggable).attr('class').replace('ui-draggable','');
										var flag = $(this).find(".cp_prop_area").attr(
												"id");
			
										var flagCountInsured = $('#insuredLst')
												.children().children().length;
										var flagCountPropose = $('#proposedLst')
												.children().children().length;
										var flagCountBenef = $('#benefLst').children().length;
										var flagCountAddnlInsured = $('#addnlInsuredLst').children().length;
			
										if (flag == "insuredLst") {
			
											if (flagCountInsured > 0) {
												return;
											}
											/*FNA changes by LE Team >>> - starts*/
											if (flagCountInsured == 0) {
												var currentlyDragged = cloneItem[0].id;
												/*for(var y=0;y<$scope.beneficiaryData.length;y++){
													var currentlyDragged = cloneItem[0].id;
														if(currentlyDragged == $scope.beneficiaryData[y].id){
															if($scope.beneficiaryData[y].isAdditionalInsured == true){
																return;
															}
														}
												}
												if(currentlyDragged == "myself"){
													if($scope.FNAMyself.isAdditionalInsured == true){
														return;
													}
												}*/
											}
											/*FNA changes by LE Team >>> - ends*/
											/* FNA changes by LE Team for Bug 4283 starts */
											/*if ($scope.FNAMyself.id == currentlyDragged
													&& $scope.FNAMyself.isBeneficiary) {
												return;
											}*/
											/* FNA changes by LE Team for Bug 4283 ends */
											/*FNA changes by LE Team >>> - starts*/
											/*for (var i = 0; i < $scope.beneficiaryData.length; i++) {
												if ($scope.beneficiaryData[i].id == currentlyDragged
														&& $scope.beneficiaryData[i].isBeneficiary) {
													return;
												}
											}*/
											/*FNA changes by LE Team >>> - ends*/
                                            
											$('#insuredLst').children().append(
													cloneItem);
											/**
											 * To set the insured obj to
											 * illustration/eApp object -- Begin *
											 */
											$scope.setInsured($scope, currentlyDragged);
											/**
											 * To set the insured obj to
											 * illustration/eApp object -- End *
											 */
			
										} else if (flag == "proposedLst") {
											if (flagCountPropose > 0) {
												return;
											}
											if (flagCountPropose == 0) {
												$('#proposedLst').children().append(
														cloneItem);
			
												/**
												 * To set the Proposer obj to
												 * illustration/eApp object -- Begin *
												 */
												var currentlyDragged = cloneItem[0].id;
												$scope.setPayer($scope,
														currentlyDragged);
												/**
												 * To set the Proposer obj to
												 * illustration/eApp object -- End *
												 */
			
											}
										} else if (flag == "benefLst") {
											if (flagCountBenef >= 4) {
												return;
											}
											if (flagCountBenef < 4) {
												var currentlyDragged = cloneItem[0].id;
												
			
												/*if ($scope.FNAMyself.id == currentlyDragged
														&& ($scope.FNAMyself.isBeneficiary || $scope.FNAMyself.isInsured)) {*/
                                                    /* FNA changes by LE Team >>> Show Error message --Starts */
                                                   /* $rootScope.showHideLoadingImage(false, "Loading,please wait..");
                                                    $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
                                                        translateMessages($translate, "illustrator.insuredAndBeneficiarySame"),
                                                        translateMessages($translate, "illustrator.okMessageInCaps"));*/
                                                    /* FNA changes by LE Team >>> Show Error message --Ends */
													/*return;
												}
			
												for (var i = 0; i < $scope.beneficiaryData.length; i++) {
													if ($scope.beneficiaryData[i].id == currentlyDragged
															&& ($scope.beneficiaryData[i].isBeneficiary || $scope.beneficiaryData[i].isInsured)) {
														return;
													}
												}*/
			
												/**
												 * To set the beneficiary obj to
												 * illustration/eApp object -- Begin *
												 */
												var elementTemp = angular
														.element(document
																.querySelector('#benefLst'));
												/* FNA Changes by LE Team Start */
												/* show-scrap is replaced with show-delete */
												elementTemp
														.append("<li class='cpLstItem' show-delete> <span class='"+currentlyDraggedClass+"' id='"
																+ currentlyDragged
																+ "'>"
																+ cloneItem.html()
																+ "</span></li>");
												/* FNA Changes by LE Team End */
												$compile(elementTemp)($scope);
												$scope.setBeneficiary($scope,
														currentlyDragged);
												/**
												 * To set the beneficiary obj to
												 * illustration/eApp object -- Begin *
												 */
											}
			
										}
									}
								});
								if(UtilityService.previousPage == "Illustration") {
									$('.cp_insured_section, .cp_proposer_section').droppable("option", "disabled", true);
									$('.cp_insured_section, .cp_proposer_section').addClass('avoid-clicks');
								}
			};
			/** Function for droppable implementation of parties -- End * */
			
			$scope.$on('initiate-save', function (evt, obj) {
				var parties = globalService.getParties();
				globalService.setParties(parties);
				globalService.setProduct(data.Product);
				var obj = new GLI_FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
				obj.save(function() {
					$rootScope.showHideLoadingImage(false, "");
					$rootScope.passwordLock  = false;
					$rootScope.isAuthenticated = false;
					$location.path('/login');
				});
			});
                                                        
            $scope.goToProductRecommendationPage = function() {
            	/* FNA Changes by LE Team Bug 3800 - starts */ 
            	IllustratorVariables.fromIllustrationAfterSave = false;
            	/* FNA Changes by LE Team Bug 3800 - ends */ 
                $location.path('/fnaRecomendedProducts');
            }                                            
			
			/** Function called on click of Proceed button * */
			// navigate to illustration personal details page
			$scope.nextPage = function() {
				var flagCountInsured = $('#insuredLst')
				.children().children().length;
				var flagCountPropose = $('#proposedLst')
				.children().children().length;
				//var flagCountAddnlInsured = $('#addnlInsuredLst').children().length;
                		/*FNA changes by LE Team >>> starts - Bug 4194*/
				FnaVariables.flagFromCustomerProfileFNA = false;
				/*FNA changes by LE Team >>> ends - Bug 4194*/
				/*FNA changes by LE Team -Fix for bug#4219 Starts */
				var beneficiaryList = $('#benefLst').children();
				
				if (beneficiaryList.length == 0){
						$scope.FNAMyself.isBeneficiary = false;
						$scope.FNAMyself.fnaBenfPartyId = "";
					
				}else{
					if(!$scope.FNAMyself.isBeneficiary){
					for (var i = 0; i < beneficiaryList.length; i++) {
						$scope.FNAMyself.isBeneficiary = true;
						if(beneficiaryList[i].innerHTML.indexOf("myself") == -1){
							$scope.FNAMyself.isBeneficiary = false;
							$scope.FNAMyself.fnaBenfPartyId = "";
						}
					}
				  }
				}
				 
				 /*FNA changes by LE Team -Fix for bug#4219 Ends */
               
				/*FNA changes by LE Team >>> Starts */
                /*if(flagCountPropose == 0){*/
				if(flagCountInsured == 0){
				/*FNA changes by LE Team >>> Ends */
					$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"),translateMessages($translate,"fna.addInsured"),translateMessages($translate,"fna.ok"),"");
				} 
				/*else if(flagCountAddnlInsured != 0 && flagCountInsured == 0){
					$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"),translateMessages($translate,"fna.addPolicyHolder"),translateMessages($translate,"fna.ok"),"");
				}*/else{
				if (UtilityService.previousPage != 'LMS') {
				globalService.setParty($scope.FNAMyself, "FNAMyself");
				var fnaBeneficiaries = $scope.beneficiaryData;
				//var fnaAdditionalInsured = $scope.additionalInsuredData;
				if ($scope.remainingBeneficiaries != null
						&& $scope.remainingBeneficiaries.length > 0) {
					for (var i = 0; i < $scope.remainingBeneficiaries.length; i++) {
						fnaBeneficiaries.push($scope.remainingBeneficiaries[i]);
					}
				}
				globalService.setFNABeneficiaries(fnaBeneficiaries);
				$scope.FNAObject = FnaVariables.getFnaModel();
			
				if (UtilityService.previousPage == 'Illustration') {
					// Set key2 as fnaId if synced, else set transactionId as fnaId
					var illustrationId = $routeParams.illustrationTransId;
					var illustrationNum = $routeParams.illustrationNumber;
					
					//EappVariables.fnaId = FnaVariables.transTrackingID;
					//EappVariables.leadId =FnaVariables.leadId;
					//EappVariables.illustratorId =illustrationId;
					
					//setting the beneficiary to the variable used to disable the fields that is prepopulated in eapp
					//suppose new nenefiicary is added in choosepart screen before eapp screen
					//additional insured,proposer cannot be changed.
					EappVariables.prepopulatedBeneficiaryData=globalService.getSelectdBeneficiaries();
					if(FnaVariables.transTrackingID == ""){
						FnaVariables.transTrackingID = "0"
					}
					$location.path('/Eapp/0/0/' + illustrationId +'/0/'+FnaVariables.leadId+'/'+ FnaVariables.transTrackingID +'/'+ illustrationNum);
				} else if (UtilityService.previousPage == 'FNA' || UtilityService.previousPage == 'IllustrationPersonal') {
					$rootScope.isFromIllustration = false;
					if(UtilityService.previousPage == 'FNA'){
						$scope.FNAObject.FNA.transactionId = $rootScope.transactionId;
					}else{
						$rootScope.transactionId = $scope.FNAObject.FNA.transactionId
					}
					UtilityService.previousPage = 'FNA';
					var parties = globalService.getParties();
					$scope.FNAObject.FNA.parties = parties;
					FnaVariables.setFnaModel($scope.FNAObject);
			        
					$scope.dataNew = {
				              "Insured" : {},										
							  "Payer" : {},										
		                      "Beneficiaries" : [],
							  "Product" : {}	,
		                      "FNAGoal":{}
					};					
					for(var y=0; y< $scope.beneficiaryData.length; y++){
						
						var benef = {};
						if($scope.beneficiaryData[y].isBeneficiary){
							if($scope.beneficiaryData[y].BasicDetails != undefined){
								benef = $scope.beneficiaryData[y];
								/*FNA changes by LE Team >>> add additional paramters of age in months - Starts */
								benef.BasicDetails.ageInDays = getNumberOfDays(benef.BasicDetails.dob);
								/*FNA changes by LE Team >>> add additional paramters of age in months - Ends */
								$scope.dataNew.Beneficiaries.push(benef);
							}
						}
						/*if($scope.beneficiaryData[y].isAdditionalInsured){
							if($scope.beneficiaryData[y].BasicDetails != undefined){
								addnl = $scope.beneficiaryData[y];
								$scope.dataNew.AdditionalInsured.push(addnl);
							}
						}*/
					}					
					/*for(var z=0; z< $scope.additionalInsuredData.length; z++){
						
						var addnl = {};
						if($scope.additionalInsuredData[z].isBeneficiary){
							addnl = $scope.additionalInsuredData[z];
							$scope.dataNew.AdditionalInsured.push(addnl);
						}
					}*/
					$scope.riderList = GLI_globalService.getRiders();
					if($scope.riderList.length == 0){
						$scope.dataNew.populationBIdisableFlag = true;
					}else{
						$scope.dataNew.populationBIdisableFlag = false;
					}
					//$scope.dataNew.populationBIdisableFlag = $rootScope.populationBIdisableFlag;
					$scope.dataNew.Insured = globalService.getInsured();
					FnaVariables.flagFromCustomerProfileFNA = false;
					/*FNA changes by LE Team >>> add additional paramters of age in months - Starts */
					$scope.dataNew.Insured.BasicDetails.ageInDays = getNumberOfDays($scope.dataNew.Insured.BasicDetails.dob);
					/*FNA changes by LE Team >>> add additional paramters of age in months - Ends */
					/*if($scope.insuredNameSet == false || $scope.insuredNameSet == undefined){
						$scope.dataNew.Insured.BasicDetails.firstName = $scope.dataNew.Insured.BasicDetails.firstName + ' ' + $scope.dataNew.Insured.BasicDetails.lastName;
						$scope.insuredNameSet = true;
					}*/
					//$scope.dataNew.Insured.BasicDetails.mobileNumber = ($scope.dataNew.Insured.ContactDetails.homeNumber1)?($scope.dataNew.Insured.ContactDetails.homeNumber1):"";	
			       
					$scope.dataNew.Payer = globalService.getPayer();	
					/*FNA changes by LE Team >>> add additional paramters of age in months - Starts */
					$scope.dataNew.Payer.BasicDetails.ageInDays = getNumberOfDays($scope.dataNew.Payer.BasicDetails.dob);
					/*FNA changes by LE Team >>> add additional paramters of age in months - Ends */		 
					//$scope.dataNew.Proposer.BasicDetails.firstName = $scope.dataNew.Proposer.BasicDetails.firstName + ' ' + $scope.dataNew.Proposer.BasicDetails.lastName;
					//$scope.dataNew.Proposer.BasicDetails.mobileNumber = ($scope.dataNew.Proposer.ContactDetails.homeNumber1)?($scope.dataNew.Proposer.ContactDetails.homeNumber1):"";
				
					$scope.dataNew.Product = globalService.getProduct();	
					
					if(typeof $scope.FNAObject.FNA.productSelectedGoal !="undefined" && $scope.FNAObject.FNA.productSelectedGoal!=""){
					if(typeof $scope.FNAObject.FNA.goals!="undefined"){
						for(var i=0;i<$scope.FNAObject.FNA.goals.length;i++){
							if($scope.FNAObject.FNA.goals[i].goalName==$scope.FNAObject.FNA.productSelectedGoal){
								$scope.dataNew.FNAGoal=$scope.FNAObject.FNA.goals[i];
								break;
							}
						}
					}
					}else{
						if(typeof $scope.FNAObject.FNA.goals!="undefined"){
							for(var i=0;i<$scope.FNAObject.FNA.goals.length;i++){
								if($scope.FNAObject.FNA.goals[i].priority=="1"){
									$scope.dataNew.FNAGoal=$scope.FNAObject.FNA.goals[i];
									break;
								}
							}
						}
					}
					
					FnaService.checkValidationFNAtoBI($scope.dataNew, function(
								output) {
							getValidationSuccess(output, $scope.dataNew);
					}, function(output) {
							$scope.getValidationFailure(output);
						});
					
					
				} else {
					$location
							.path('/Illustrator/'
									+ $scope.FNAObject.FNA.selectedProduct.productId
									+ '/0/0/0');
				}
			
			}else{
				var productId = $routeParams.productId;
				$location
				.path('/Illustrator/'
						+ productId
						+ '/0/0/0');
			}
			}
			
			};
			
			
			
				
			function getValidationSuccess(data, dataNew) {
				var parties = globalService.getParties();
					//the first name and last name is appended for FNAVAlidation 
					//the same need to be splited as its need to be prepopulateed in illustration.
					//or else first name will have both first and last name in illustration when prepopulted from FNA
					//Commenting the below code since last in no mor enabled in LMS
					/*for(i=0;i<parties.length;i++){
						if(parties[i].BasicDetails.firstName !=""){
							var names=parties[i].BasicDetails.firstName.split(" ");
							parties[i].BasicDetails.firstName=names[0];
						}
					}*/
					//$scope.FNAObject.FNA.parties = parties;
					//FnaVariables.setFnaModel($scope.FNAObject);
					globalService.setParties(parties);
					
					globalService.setProduct(data.Product);
					
					/*FNA changes by LE Team -- set the goal details to be accessible in illustrator screen >> starts*/
					globalService.setFNAGoal(data.FNAGoal);
					/*FNA changes by LE Team -- set the goal details to be accessible in illustrator screen >> ends*/
					var obj = new GLI_FnaService.saveTransactions($scope, $rootScope,
							DataService, $translate, UtilityService, false);
					obj.save(function() {
			
						IllustratorVariables.selectedPage = "PersonalDetails";
						/* FNA Changes by LE Team Bug 3800 - starts */ 
						var transactionIdForIllustration = 0;
						if (IllustratorVariables.fromIllustrationAfterSave) {
							transactionIdForIllustration = IllustratorVariables.transactionId;
						}
						/* FNA Changes by LE Team Bug 3800 - ends */ 
						IllustratorVariables.clearIllustrationVariables();
						// Set key2 as fnaId if synced, else set transactionId as fnaId				
						IllustratorVariables.fnaId = FnaVariables.transTrackingID;
						IllustratorVariables.leadId = FnaVariables.leadId;	
                        IllustratorVariables.leadName = FnaVariables.fnaKeys.Key22;	
                        /*FNA changes by LE Team>> starts*/
                        IllustratorVariables.fromFNAChoosePartyScreenFlow = true;
                        /* FNA Changes by LE Team Bug 3800 - starts */ 
						if (IllustratorVariables.fromIllustrationAfterSave) {
							IllustratorVariables.transactionId = transactionIdForIllustration;
							IllustratorVariables.fromIllustrationAfterSave = false;
						}
						/* FNA Changes by LE Team Bug 3800 - ends */ 
                        /*FNA changes by LE Team>> ends*/
                        /* FNA Changes by LE Team Start */
                        IllustratorVariables.leadEmailId = FnaVariables.fnaKeys.Key23;	
                        /* FNA Changes by LE Team Bug 3800 - starts */ 
						/* FNA Changes by LE Team Bug 4166 - starts */ 
						EappVariables.fromFNA = true;
						/* Defect -3760 - starts*/
						var updatedBenArray = [];
						if($scope.FNAMyself.isBeneficiary){
							updatedBenArray.push($scope.FNAMyself);
						}
						for (var i=0;i<dataNew.Beneficiaries.length;i++) {
							var tempBeneObj = angular.copy(dataNew.Beneficiaries[i]);
							tempBeneObj.CustomerRelationship = {};
							/*bug 4217 starts*/
                           
								tempBeneObj.BasicDetails.nationalIDType=angular.copy(tempBeneObj.BasicDetails.identityProof);
								tempBeneObj.BasicDetails.identityProof=angular.copy(tempBeneObj.BasicDetails.IDcard);	
							
							
							/*bug 4217 ends*/
							tempBeneObj.CustomerRelationship.relationWithInsured = dataNew.Beneficiaries[i].classNameAttached;
							updatedBenArray.push(tempBeneObj);
						}						
						globalService.setCPInsured(dataNew.Insured);
						globalService.setCPPayer(dataNew.Payer);
						globalService.setCPPrivacyLaw(dataNew.PrivacyLaw);
						globalService.setCPBeneficiaries(updatedBenArray);
						/* Defect -3760 - ends*/
						/* FNA Changes by LE Team Bug 4166 - ends */ 
                        $location.path('/Illustrator/'
							+ $scope.FNAObject.FNA.selectedProduct.productId
							+ '/0/0/' + transactionIdForIllustration);
                        /* FNA Changes by LE Team Bug 3800 - ends */ 
                        
                        /* FNA Changes by LE Team End */
			
					});
			}
			
			$scope.oKClick = function(){	
			     $scope.proceedDisable = true;
			     $scope.refresh();
			}

			/*FNA changes by LE Team>> - starts*/
			$scope.closePopup = function () {
				$scope.choosePartyPopUpMsg = false;
				//$scope.showPopUpServiceMsg=false;
				$scope.overlayTC = false;
			};
			/*FNA changes by LE Team>> - ends*/
			
			$scope.getValidationFailure = function(data) {
				$scope.isValidationRuleExecuted = false;
				if (typeof data.ValidationResults != "undefined"
						|| data.ValidationResults != null) {
					$scope.isValidationRuleExecuted = true;
					$scope.validationMessage = translateMessages(
							$translate,
							"illustrator.validationFailed");
					$scope.validationResults = data.ValidationResults;
					$scope.choosePartyError = [];
					$scope.proceedDisable = true;
					for (var key in $scope.validationResults) {
							$scope.validationResults[key] = translateMessages($translate,"fna." +key);
							$scope.choosePartyError.push($scope.validationResults[key]);
					}
					/* FNA changes by LE Team>> - starts */
					$scope.overlayTC = true;
					$scope.choosePartyPopUpMsg = true;
					if($scope.choosePartyError.length == 3){
						$scope.choosePartyErrorMsg1 = $scope.choosePartyError[0];
						$scope.choosePartyErrorMsg2 = $scope.choosePartyError[1];
						$scope.choosePartyErrorMsg3 = $scope.choosePartyError[2];
					} else if($scope.choosePartyError.length == 2){
						$scope.choosePartyErrorMsg1 = $scope.choosePartyError[0];
						$scope.choosePartyErrorMsg2 = $scope.choosePartyError[1];
					} else {
						$scope.choosePartyErrorMsg1 = $scope.choosePartyError[0];
					}
					$scope.refresh();									
					$rootScope.showHideLoadingImage(false);
					/*$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"),$scope.choosePartyError,translateMessages($translate,"fna.ok"),$scope.oKClick);*/
					/* FNA changes by LE Team>> - ends */
				}
										
									}
			
			/** Set dragged payer details to illustration/eApp object * */
			$scope.setPayer = function($scope, currentlyDragged) {
			
				if (currentlyDragged == "myself") {
			
					$scope.FNAMyself.isPayer = true;
				//	$scope.FNAMyself.ContactDetails.mobileNumber1 = $scope.FNAMyself.ContactDetails.mobileNumber1;
					$scope.FNAMyself.fnaPayerPartyId = currentlyDragged;
			
					if (UtilityService.previousPage == 'Illustration') {// To change --
						// isPayorDifferentFromInsured
						// flag is
						// treated in
						// diff meanings
						// t implement
						// business
						// logics in
						// eApp and
						// illustration
						if ($scope.FNAMyself.isInsured) {
							$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "No";
							$scope.FNAMyself.CustomerRelationship.relationshipWithProposer = "Self";
						} else {
							$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "Yes";
							$scope.FNAMyself.CustomerRelationship.relationshipWithProposer = "Others";
						}
			
						var insuredParty = globalService.getInsured();
						insuredParty.CustomerRelationship.relationshipWithProposer = $scope.FNAMyself.CustomerRelationship.relationshipWithProposer;
						globalService.setInsured(insuredParty);
			
					} else {
						if ($scope.FNAMyself.isInsured) {
							$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "Yes";
						} else {
							$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "No";
						}
					}
			
				} else {
					for (var i = 0; i < $scope.beneficiaryData.length; i++) {
						if ($scope.beneficiaryData[i].id == currentlyDragged) {
							$scope.beneficiaryData[i].isPayer = true;
							/*if ($scope.beneficiaryData[i].ContactDetails) {
								$scope.beneficiaryData[i].ContactDetails.mobileNumber1 = $scope.beneficiaryData[i].ContactDetails.homeNumber1;
							}*/
							$scope.beneficiaryData[i].fnaPayerPartyId = currentlyDragged;
			
							if (!$scope.beneficiaryData[i].CustomerRelationship) {
								$scope.beneficiaryData[i].CustomerRelationship = {};
							}
							if ($scope.FNAMyself.isInsured) {
								$scope.beneficiaryData[i].CustomerRelationship.relationWithInsured = $scope.beneficiaryData[i].classNameAttached;
							}
							if (UtilityService.previousPage == 'Illustration') {// To
								// change
								// --
								// isPayorDifferentFromInsured
								// flag
								// is
								// treated
								// in
								// diff
								// meanings
								// t
								// implement
								// business
								// logics
								// in
								// eApp
								// and
								// illustration
								if ($scope.beneficiaryData[i].isInsured) {
									$scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured = "No";
									$scope.beneficiaryData[i].CustomerRelationship.relationshipWithProposer = "Self";
								} else {
									$scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured = "Yes";
									$scope.beneficiaryData[i].CustomerRelationship.relationshipWithProposer = "Others";
								}
			
								var insuredParty = globalService.getInsured();
								insuredParty.CustomerRelationship.relationshipWithProposer = $scope.beneficiaryData[i].CustomerRelationship.relationshipWithProposer;
								globalService.setInsured(insuredParty);
			
							} else {
								if ($scope.beneficiaryData[i].isInsured) {
									$scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured = "Yes";
								} else {
									$scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured = "No";
								}
							}
							break;
						}
			
					}
			
				}
			    $scope.proceedDisable = false;
				$scope.refresh();
			};
			
			/** Set dragegd insured details to illustration/eApp object * */
			$scope.setInsured = function($scope, currentlyDragged) {
			
				if (currentlyDragged == "myself") {
					$scope.FNAMyself.isInsured = true;
					//$scope.FNAMyself.ContactDetails.mobileNumber1 = $scope.FNAMyself.ContactDetails.homeNumber1;
					$scope.FNAMyself.fnaInsuredPartyId = currentlyDragged;
			
					if (UtilityService.previousPage == 'Illustration') {// To change --
						// isPayorDifferentFromInsured
						// flag is
						// treated in
						// diff meanings
						// t implement
						// business
						// logics in
						// eApp and
						// illustration
						if ($scope.FNAMyself.isPayer) {
							$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "No";
							$scope.FNAMyself.CustomerRelationship.relationshipWithProposer = "Self";
						} else {
							$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "Yes";
							$scope.FNAMyself.CustomerRelationship.relationshipWithProposer = "Others";
						}
						var payerParty = globalService.getPayer();
						payerParty.CustomerRelationship.isPayorDifferentFromInsured = $scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured;
						globalService.setPayer(payerParty);
					} else {
						if ($scope.FNAMyself.isPayer) {
							$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "Yes";
						} else {
							$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "No";
						}
					}
			
				} else {
					for (var i = 0; i < $scope.beneficiaryData.length; i++) {
						if ($scope.beneficiaryData[i].id == currentlyDragged) {
							$scope.beneficiaryData[i].isInsured = true;
							if ($scope.beneficiaryData[i].ContactDetails
									&& $scope.beneficiaryData[i].ContactDetails.homeNumber1) {
								$scope.beneficiaryData[i].ContactDetails.mobileNumber1 = $scope.beneficiaryData[i].ContactDetails.homeNumber1;
								$scope.beneficiaryData[i].ContactDetails.homeNumber1="";
							}
							$scope.beneficiaryData[i].fnaInsuredPartyId = currentlyDragged;
			
							if (!$scope.beneficiaryData[i].CustomerRelationship) {
								$scope.beneficiaryData[i].CustomerRelationship = {};
							}
			
							if (UtilityService.previousPage == 'Illustration') {// To
								// change
								// --
								// isPayorDifferentFromInsured
								// flag
								// is
								// treated
								// in
								// diff
								// meanings
								// t
								// implement
								// business
								// logics
								// in
								// eApp
								// and
								// illustration
								if ($scope.beneficiaryData[i].isPayer) {
									$scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured = "No";
									$scope.beneficiaryData[i].CustomerRelationship.relationshipWithProposer = "Self";
								} else {
									$scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured = "Yes";
									$scope.beneficiaryData[i].CustomerRelationship.relationshipWithProposer = "Others";
								}
			
								var payerParty = globalService.getPayer();
								payerParty.CustomerRelationship.isPayorDifferentFromInsured = $scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured;
								globalService.setPayer(payerParty);
			
							} else {
								if ($scope.beneficiaryData[i].isPayer) {
									$scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured = "Yes";
								} else {
									$scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured = "No";
								}
							}
			
							break;
						}
			
					}
				}
			    $scope.proceedDisable = false;
				$scope.refresh();
			};
			
			/** Set dragged beneficiry details to eApp object * */
			$scope.setBeneficiary = function($scope, currentlyDragged) {
			
				if (currentlyDragged == "myself") {
					$scope.FNAMyself.isBeneficiary = true;
					//$scope.FNAMyself.ContactDetails.mobileNumber1 = $scope.FNAMyself.ContactDetails.homeNumber1;
					$scope.FNAMyself.fnaBenfPartyId = currentlyDragged;
				} else {
					for (var i = 0; i < $scope.beneficiaryData.length; i++) {
						if ($scope.beneficiaryData[i].id == currentlyDragged) {
							$scope.beneficiaryData[i].isBeneficiary = true;
							$scope.beneficiaryData[i].fnaBenfPartyId = currentlyDragged;
							/*if ($scope.beneficiaryData[i].ContactDetails
									&& $scope.beneficiaryData[i].ContactDetails.homeNumber1) {
								$scope.beneficiaryData[i].ContactDetails.mobileNumber1 = $scope.beneficiaryData[i].ContactDetails.homeNumber1;
							}*/
							/*if ($scope.FNAMyself.isInsured) {
								if (!$scope.beneficiaryData[i].CustomerRelationship) {
									$scope.beneficiaryData[i].CustomerRelationship = {};
								}
								$scope.beneficiaryData[i].CustomerRelationship.relationWithInsured = $scope.beneficiaryData[i].classNameAttached;
							}*/
							break;
			
						}
			
					}
				}
				$scope.proceedDisable = false;
				$scope.refresh();
			};
			
			
			/** Set dragged beneficiry details to eApp object * */
			$scope.setAdditionalInsured = function($scope, currentlyDragged) {
			
				if (currentlyDragged == "myself") {
					$scope.FNAMyself.isAdditionalInsured = true;
					//$scope.FNAMyself.ContactDetails.mobileNumber1 = $scope.FNAMyself.ContactDetails.homeNumber1;
					$scope.FNAMyself.fnaAddInsuredPartyId = currentlyDragged;
				} else {
					for (var i = 0; i < $scope.beneficiaryData.length; i++) {
						if ($scope.beneficiaryData[i].id == currentlyDragged) {
							$scope.beneficiaryData[i].isAdditionalInsured = true;
							$scope.beneficiaryData[i].fnaAddInsuredPartyId = currentlyDragged;
						/*	if ($scope.beneficiaryData[i].ContactDetails
									&& $scope.beneficiaryData[i].ContactDetails.homeNumber1) {
								$scope.beneficiaryData[i].ContactDetails.mobileNumber1 = $scope.beneficiaryData[i].ContactDetails.homeNumber1;
							}*/
							if ($scope.FNAMyself.isInsured) {
								if (!$scope.beneficiaryData[i].CustomerRelationship) {
									$scope.beneficiaryData[i].CustomerRelationship = {};
								}
								$scope.beneficiaryData[i].CustomerRelationship.relationWithInsured = $scope.beneficiaryData[i].classNameAttached;
							}
							break;
			
						}
			
					}
				}
				
				$scope.proceedDisable = false;
				$scope.refresh();
			};
			
	
			
			 $scope.$on("$destroy", function() {
                 if (this.$$destroyed) return;     
				 while (this.$$childHead) {      
				 this.$$childHead.$destroy();    
				 }     
				 if (this.$broadcast)
				{ 
					this.$broadcast('$destroy');   
				}   
				 this.$$destroyed = true;                 
				 //$timeout.cancel( timer );                           
        });


}]);