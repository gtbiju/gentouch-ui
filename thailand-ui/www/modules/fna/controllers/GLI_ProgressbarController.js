storeApp.controller('GLI_ProgressbarController',['$timeout', 'FnaVariables', '$rootScope', '$scope', '$location','$compile', '$routeParams', 'DataService', 'LookupService', 'DocumentService','FnaVariables', 'GLI_FnaService', 'UtilityService', 'PersistenceMapping','$translate', 'AutoSave', '$debounce', 'globalService', 'GLI_globalService', '$controller',
function GLI_ProgressbarController($timeout, FnaVariables, $rootScope, $scope, $location,$compile, $routeParams, DataService, LookupService, DocumentService,FnaVariables, GLI_FnaService, UtilityService, PersistenceMapping,$translate, AutoSave, $debounce, globalService, GLI_globalService, $controller){
	$controller('ProgressbarController',{$timeout:$timeout, FnaVariables:FnaVariables,$rootScope:$rootScope, $scope:$scope, $location:$location,$compile:$compile, $routeParams:$routeParams, DataService:DataService, LookupService:LookupService, DocumentService:DocumentService,FnaVariables:FnaVariables, FnaService:GLI_FnaService, UtilityService:UtilityService, PersistenceMapping:PersistenceMapping,$translate:$translate, AutoSave:AutoSave, $debounce:$debounce, globalService:GLI_globalService});	
	
	
	
	// Function called onclick on any tab link in menu bar. In that case, we
	// move to the first page PageType as the selected tab
	$scope.tabClick = function(event, pageType) {
		$scope.event = event;
		$scope.pageType = pageType;
		/* FNA changes by LE Team Start */
		if(UtilityService.disableProgressTab && pageType=="Need Analysis" && $scope.FNAObject.FNA.saveCheck==false || UtilityService.disableProgressTab && pageType=="Need Analysis" && $scope.FNAObject.FNA.saveCheckDependents==false ){
			$rootScope.lePopupCtrl.showWarning(
						translateMessages($translate,
								"lifeEngage"),
						translateMessages($translate,
								"saveBeneficiaryData"),  
						translateMessages($translate,
								"fna.navok"),$scope.okPressed,translateMessages($translate,
								"general.navproceed"),$scope.addEditDetails);
		}
		/* FNA changes by LE Team End */
		else if (UtilityService.disableProgressTab) {
			 $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),translateMessages($translate, "fna.previousPageMessage") ,translateMessages($translate, "fna.ok"), $scope.navigationToTab,translateMessages($translate, "general.productCancelOption"),$scope.cancel,$scope.cancel);
		}		
		else{
			$scope.navigationToTab(event, pageType);
		}
	};	
	
	$scope.navigationToTab = function(){
		var thisObj = $scope.event.currentTarget;
		var locationUrl;
		for (var j = 0; j < $scope.pageTypes.length; j++) {
			if ($scope.pageType == $scope.pageTypes[j].pageType) {
				for (i = 0; i < $scope.pageSelections.length; i++) {
					if ($scope.pageType == $scope.pageSelections[i].pageType
							&& $scope.pageSelections[i].isDisabled == "") {
						if($scope.pageType == $scope.pageSelections[0].pageType){
							if( $rootScope.transactionId == ""){
								$rootScope.transactionId = 0;
							}
							locationUrl = $scope.pageTypes[j].url + "/" + FnaVariables.fnaId + "/" + $rootScope.transactionId + "/false";
						}else{
							locationUrl = $scope.pageTypes[j].url;
						}
						$location.path(locationUrl);
					}
				}
			}
		}
	};
    
    /* FNA changes made by LE Team Start */
    $scope.previousPage = function() {
        if(UtilityService.disableProgressTab && $scope.selectedPage=="My Family" && $scope.FNAObject.FNA.saveCheck==false || UtilityService.disableProgressTab && pageType=="Need Analysis" && $scope.FNAObject.FNA.saveCheckDependents==false ){
            $rootScope.lePopupCtrl.showWarning(
						translateMessages($translate,
								"lifeEngage"),
						translateMessages($translate,
								"saveBeneficiaryData"),  
						translateMessages($translate,
								"fna.navok"),$scope.cancel,translateMessages($translate,
								"general.navproceed"),$scope.previousPagefn);           
        }
		else if (UtilityService.disableProgressTab) {
			 $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),translateMessages($translate, "fna.previousPageMessage") ,translateMessages($translate, "fna.ok"), $scope.okClick,translateMessages($translate, "general.productCancelOption"),$scope.cancel,$scope.cancel);
		} else {
			if ($scope.pageIndex > 0) {
				var previousPage = $scope.pages[$scope.pageIndex - 1];
				$location.path(previousPage.url);
			}
		}
	};
    $scope.previousPagefn=function(){
        if ($scope.pageIndex > 0) {
				var previousPage = $scope.pages[$scope.pageIndex - 1];
				$location.path(previousPage.url);
			}
    }
	/* FNA changes made by LE Team End */
	$scope.$parent.$on('ProgressbarReload', function(event, args) {
		$scope.FNAObject = FnaVariables.getFnaModel();
		$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;
	});
	 $scope.$on("$destroy", function() {
                 if (this.$$destroyed) return;     
				 while (this.$$childHead) {      
				 this.$$childHead.$destroy();    
				 }     
				 if (this.$broadcast)
				{ 
					this.$broadcast('$destroy');   
				}   
				 this.$$destroyed = true;                 
				 $timeout.cancel( timer );                           
        });


	
}]);