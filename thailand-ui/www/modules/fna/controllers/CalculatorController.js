﻿/*
 *Copyright 2015, LifeEngage 
 */



'use strict';

storeApp.controller('CalculatorController', ['$rootScope', '$scope', 'DataService', 'FnaService','LookupService', 'DocumentService', 'ProductService','$compile', 
                                             '$routeParams', '$location', '$translate','IllustratorVariables', 'FnaVariables', 'UtilityService','$debounce', 'PersistenceMapping', 'AutoSave',
                                              'globalService','$timeout',
                                             function($rootScope, $scope, DataService, FnaService, LookupService,DocumentService, ProductService, $compile, $routeParams, $location, $translate, 
                                             IllustratorVariables,FnaVariables, UtilityService,$debounce, PersistenceMapping, AutoSave, globalService,$timeout) {

	FnaVariables.selectedPage = "Calculators";
	$scope.selectedpage = FnaVariables.selectedPage;
	// $scope.fnaName = 'FNA Controller';
	$scope.cancel = function() {
	}
	$scope.FNAObject = FnaVariables.getFnaModel();
	$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;
	$scope.FNAObject.FNA.activeGoalIndex = 0;
	$scope.highlightedGoal = $scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].priority;
	$scope.unitList = FnaVariables.unitList;
	$scope.FNAMyself = globalService.getParty("FNAMyself");
	$scope.goals = [];
	$scope.templateId
	$scope.showErrorCount = true;
	$scope.errorMessages = [];
	$scope.goals = FnaVariables.goals;
	$scope.selectedGoals = [];
	$scope.selectedGoals = $scope.FNAObject.FNA.goals;
	$scope.slideValue
	$scope.viewName
	UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
	$scope.showCalculations = false;
	$scope.currentIndexForPopUp
	$scope.currentIdForPopUp
	$scope.goalHeader = $scope.selectedGoals[0].translateID;
	$scope.selectedUnitGlobal = FnaVariables.selectedUnitGlobal;
	UtilityService.disableProgressTab = false;
	$scope.isDisplay = true;
	var model = "FNAObject";
	if ((rootConfig.autoSave.fna)) {
		AutoSave.setupWatchForScope(model, DataService, UtilityService, "",
				$scope, $debounce, $translate, $routeParams, FnaService);
	}
	// Get FNA details Succeess
	$scope.mapKeysforPersistence = function() {
		if (!(rootConfig.isDeviceMobile)
				&& (FnaVariables.fnaId == null || FnaVariables.fnaId == 0)) {
			// PersistenceMapping.Key13 = formattedDate;
				PersistenceMapping.creationDate = UtilityService
									.getFormattedDateTime();
		}
		PersistenceMapping.Key2 = (FnaVariables.fnaId != null && FnaVariables.fnaId != 0) ? FnaVariables.fnaId
				: "";
		// PersistenceMapping.Key10 = "FullDetails";
		// PersistenceMapping.Type = "FNA";
	};

	if ($scope.FNAMyself.BasicDetails.photo == ""
			|| $scope.FNAMyself.BasicDetails.photo == undefined) {
		$scope.insuredImage = FnaVariables.myselfPhoto;
	} else {
		$scope.insuredImage = $scope.FNAMyself.BasicDetails.photo;
	}
	for (var i = 0; i < $scope.FNAObject.FNA.goals.length; i++) {
		for (var j = 0; j < $scope.goals.length; j++) {
			if ($scope.FNAObject.FNA.goals[i].goalName == $scope.goals[j].goalName) {
				$scope.FNAObject.FNA.goals[i].ruleName = $scope.goals[j].ruleName;
				$scope.FNAObject.FNA.goals[i].functionName = $scope.goals[j].functionName;
				break;
			}
		}
	}

	$scope.callback = function() {
		$rootScope.showHideLoadingImage(false);
		$scope.refresh();
		$scope.testfun();
		$rootScope.updateErrorCount($scope.viewName);
	}

	$scope.testfun = function() {
		var totalWidth = 0;
		var thisObject = $('.sliderwidget');
		$('.sliderwidget').each(function() {
			totalWidth = totalWidth + $(this).outerWidth(true);
		});
		var chk = $(thisObject).closest('ul').hasClass("calculator_widget");
		if (chk) {
			var container_width = $('.calculator_widget_wrapper').width();

			if (totalWidth < container_width) {
				var customScrollWidth = container_width + 50;
				$(thisObject).closest('ul').width(customScrollWidth);
			} else {

				$(thisObject).closest('ul').width(totalWidth);
			}

		}
	}

	$scope.isselectedUnit = function(unit) {
		if (unit == $scope.selectedGoals[$scope.FNAObject.FNA.activeGoalIndex].sliderUnit) {
			return "selected";
		}
		return "";

	}

	for (var j = 0; j < $scope.goals.length; j++) {
		if ($scope.selectedGoals[$scope.FNAObject.FNA.activeGoalIndex].goalName == $scope.goals[j].goalName) {
			$scope.viewName = $scope.goals[j].goalCalculationUITemplate;

			// $scope.refresh();
		}

	}

	$scope.viewName1 = "myViewName1";

	// $("#" + $scope.viewName).html('');
	$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
	// $scope.refresh();
	var calculatorUiJsonFile = FnaVariables.uiJsonFile.goalViewsFile;

	/*
	 * $scope.paintCalculate = function(){
	 * LEDynamicUI.paintUI(rootConfig.template, calculatorUiJsonFile,
	 * $scope.viewName, "#" + $scope.viewName, true, function() {
	 * $scope.callback(); }, $scope, $compile); }
	 */
	$timeout(function() {
		
		LEDynamicUI.paintUI(rootConfig.template, calculatorUiJsonFile,
				$scope.viewName, "#" + $scope.viewName, true, function() {
					$scope.callback();
				}, $scope, $compile);
	}, 0);

	// if(typeof($('#'+$scope.viewName)) == "undefined"){

	
	// $timeout(function() {

	// LEDynamicUI.paintUI(rootConfig.template, calculatorUiJsonFile,
	// $scope.viewName, "#" + $scope.viewName, true, function() {
	// $scope.callback();
	// }, $scope, $compile);
	// }, 0);
	// }
	// else{
	
	// LEDynamicUI.paintUI(rootConfig.template, calculatorUiJsonFile,
	// $scope.viewName, "#" + $scope.viewName, true, function() {
	// $scope.callback();
	// }, $scope, $compile);

	// }

	/*
	 * $scope.mapScopeToPersistance = function (data) { var transactionData =
	 * angular.copy(data); var updatedDate = new Date(); var formattedDate =
	 * Number(updatedDate.getMonth() + 1) + "-" + Number(updatedDate.getDate() +
	 * 5) + "-" + updatedDate.getFullYear(); var transactionObj = {};
	 * transactionObj.Key1 = $rootScope.customerId != null ?
	 * $rootScope.customerId : "C101"; transactionObj.Key2 = $rootScope.agentId !=
	 * null ? $rootScope.agentId : "Agent5"; transactionObj.Key3 =
	 * $scope.productId != null ? $scope.productId : "PRD1"; transactionObj.Key4 =
	 * formattedDate; transactionObj.Key5 = formattedDate; transactionObj.Key6 =
	 * ($scope.illustrationId != null && $scope.illustrationId != 0) ?
	 * $scope.illustrationId : ""; transactionObj.Key7 = $scope.status != null ?
	 * $scope.status : "Saved"; //transactionObj.Key8 = ($scope.illustrationId !=
	 * null && $scope.illustrationId != 0) ? $scope.illustrationId : "";
	 * transactionObj.Key9 = ""; transactionObj.Key10 = "FullDetails";
	 * transactionObj.Type = "fna"; if ((rootConfig.isDeviceMobile)) {
	 * transactionObj.Id = $rootScope.transactionId != null &&
	 * $rootScope.transactionId != 0 ? $rootScope.transactionId : null;
	 * transactionObj.TransactionData = angular.toJson(transactionData); } else {
	 * transactionObj.TransTrackingID = ""; transactionObj.TransactionData =
	 * transactionData; } return transactionObj; }
	 */
	$scope.showDetail = function(index) {
		var priority = Number(index) + 1;
		$scope.FNAObject.FNA.activeGoalIndex = Number(index);
		$scope.highlightedGoal = $scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].priority;
		$scope.goalHeader = $scope.selectedGoals[index].translateID;
		if ($scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].result != undefined) {
			$scope.showCalculations = true;
			$scope.goalruleResults = $scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].result.ruleExecutionOutput[0];
		} else {
			$scope.goalruleResults = [];
			$scope.showCalculations = false;
		}
		for (var j = 0; j < $scope.goals.length; j++) {
			if ($scope.selectedGoals[$scope.FNAObject.FNA.activeGoalIndex].goalName == $scope.goals[j].goalName) {
				$scope.viewName = $scope.goals[j].goalCalculationUITemplate;
				// $scope.refresh();
			}
		}
		$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
		$scope.refresh();
		LEDynamicUI.paintUI(rootConfig.template,
				FnaVariables.uiJsonFile.goalViewsFile, $scope.viewName, "#"
						+ $scope.viewName, true, function() {
					$scope.callback();
				}, $scope, $compile);
	}

	$scope.goalObject = $scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex];

	$scope.changeAll = function(newVal, event) {
	
		var thisObj = event.currentTarget;
		g_currentlySelected[0] = thisObj;
		var thisObjDeno = $(thisObj).text();
		/*
		 * if (thisObjDeno == "K") {
		 * $('.unit_main_btn').removeClass("selected");
		 * $('.unit_main_btn').eq(0).addClass("selected"); } if (thisObjDeno ==
		 * "L") { $('.unit_main_btn').removeClass("selected");
		 * $('.unit_main_btn').eq(1).addClass("selected"); } if (thisObjDeno ==
		 * "C") { $('.unit_main_btn').removeClass("selected");
		 * $('.unit_main_btn').eq(2).addClass("selected"); }
		 */
		$scope.goalObject = $scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex];
		$scope.goalObject.sliderUnit = newVal;

		if ($scope.goalObject.parameters != undefined) {
			for (var i = 0; i < $scope.goalObject.parameters.length; i++) {
				if ($scope.goalObject.parameters[i].unit != "NA") {
					$scope.goalObject.parameters[i].textBoxValue = Number($scope.goalObject.parameters[i].value)
							/ Number(FnaVariables.unitList[newVal]);
					$scope.goalObject.parameters[i].unit = newVal;
				}

			}
		}
		$scope.errorMessages = [];
		var id2 = '#' + id + ' .truePattern' + ":not([class*='ng-hide'])";
		$(id2).each(
				function(index, item) {
					var error = {};
					error.message = translateMessages($translate, $(this).attr(
							'errormessage'));
					error.key = $(this).attr('errorkey');
					$scope.errorMessages.push(error);
				});

		var id3 = '#' + $scope.viewName + ' .trueValid'
				+ ":not([class*='ng-hide'])";
		$(id3).each(
				function(index, item) {
					var error = {};
					var messageTemp = translateMessages($translate, $(this)
							.attr('errormessage'));
					var maxLength = translateMessages($translate, $(this).attr(
							'maxlength'));
					var minLength = translateMessages($translate, $(this).attr(
							'minlength'));
					var unit = $(this).attr('unit');
					var unitModel = ((unit).replace(/FNAObject/g,
							"$scope.FNAObject"));
					var min;
					var max;
					if (eval(unitModel) != "NA") {
						min = Number(minLength)
								/ Number($scope.unitList[eval(unitModel)]);
						max = Number(maxLength)
								/ Number($scope.unitList[eval(unitModel)]);
						error.message = messageTemp + ' ' + min + ' and' + ' '
								+ max + eval(unitModel);
					} else {
						min = Number(minLength) + 1;
						max = maxLength;
						error.message = messageTemp + ' ' + min + ' and' + ' '
								+ max;
					}
					error.key = $(this).attr('errorkey');
					$scope.errorMessages.push(error);
				});
	}

	$scope.change = function(newVal) {
		var index = $scope.currentIndexForPopUp;
		var name = $scope.currentIdForPopUp;
		$scope.goalObject = $scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex];
		

		if ($scope.goalObject.parameters == undefined) {
			$scope.goalObject.parameters = [];
		}
		if ($scope.goalObject.parameters[index] == undefined) {
			$scope.goalObject.parameters[index] = {};
			$scope.goalObject.parameters[index].name = name;
		}
		$scope.goalObject.parameters[index].unit = newVal;
		// $scope.goalObject.parameters[index].value = $scope.slideValue;

		var value;
		$scope.goalObject.parameters[index].textBoxValue = Number($scope.goalObject.parameters[index].value)
				/ Number(FnaVariables.unitList[newVal]);
	}
	for (var i = 0; i < $scope.selectedGoals.length; i++) {
		for (var j = 0; j < $scope.goals.length; j++) {
			if ($scope.selectedGoals[i].goalName == $scope.goals[j].goalName) {
				$scope.selectedGoals[i].goalicon = $scope.goals[j].goalicon;
				// $scope.refresh();
			}
		}
		if ($scope.selectedGoals[i].priority == 1) {
			$scope.FNAObject.FNA.activeGoalIndex = i;
		}

	}
	/*
	 * $scope.PluginInit = function(value) { $scope.timeInMs = 0; var countUp =
	 * function() { if (value == true) { // animationTime: 100, set the
	 * animation duration // selectedAction:"selectOnly" Two options -
	 * switch/selectOnly // FastClick.attach(document.body);
	 * $('.arc_container_wrapper .arc_left').radialOptions({ animationTime :
	 * 100, selectedAction : "switch" }); } } $timeout(countUp, 0); }
	 */
	$scope.errorMessage

	/*
	 * $scope.nextPage=function(){$location.path('/fnasummary');
	 * $scope.pendingGoalsList=[]; var isCalculated; var isSelected =
	 * $scope.FNAObject.FNA.goals.length; for(var i=0;i<isSelected;i++){ var
	 * name={}; if(typeof $scope.FNAObject.FNA.goals[i].result == "undefined" ){
	 * name.translateID=$scope.FNAObject.FNA.goals[i].translateID;
	 * $scope.pendingGoalsList.push(name); } } //alert("here");
	 * isCalculated=$scope.pendingGoalsList.length;
	 * if($scope.isCalculated==isSelected){
	 * FnaVariables.setFnaModel($scope.FNAObject);
	 * $location.path('/fnasummary'); } else if(isCalculated>0){
	 * $('#proceedButton').show(); $scope.errorMessage="calculateRemaining"; var
	 * dynamic_height = $('body').height();
	 * $("body").removeClass("hide_scroll").addClass("hide_scroll");
	 * $('#popup_modal_bg').css('height',dynamic_height).fadeIn(300);
	 * preferencesPopUp(); } else { $scope.errorMessage="calculareAtleastOne";
	 * var dynamic_height = $('body').height();
	 * $("body").removeClass("hide_scroll").addClass("hide_scroll");
	 * $('#popup_modal_bg').css('height',dynamic_height).fadeIn(300);
	 * preferencesPopUp(); } }
	 */
	
	$scope.setRuleInputs  = function(goalObject) {
		goalObject.myselfDob = $scope.FNAObject.FNA.parties[0].BasicDetails.dob;
		return goalObject;
	
	}
	$scope.calculateGoal = function() {
		$rootScope
				.showHideLoadingImage(true, 'calculationProgress', $translate);
		$scope.refresh();
		var carrierId = 1;
		var goalObject = $scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex];
	//	goalObject.myselfDob = $scope.FNAObject.FNA.parties[0].BasicDetails.dob;
		goalObject = $scope.setRuleInputs(goalObject);
		UtilityService.disableProgressTab = true;
		$rootScope.updateErrorCount($scope.viewName);
		if ($scope.errorCount == 0) {
			try {
				PersistenceMapping.clearTransactionKeys();
				$scope.mapKeysforPersistence();
				// var transactionObj = PersistenceMapping
				// .mapScopeToPersistence({});
				FnaService.goalCalculation(carrierId, goalObject, function(
						output) {
					getProductSuccess(output, goalObject);
				}, function(output) {
					$scope.getProductFailure(output);
				});
			} catch (exception) {
				alert("Error Output " + exception);
				$scope.getProductFailure(exception);
				$rootScope.showHideLoadingImage(false);
			}
		} else {
			$scope.isDisplay = false;
			$scope.errorMessage = "fna.validationError";
			$rootScope.lePopupCtrl.showError(translateMessages($translate,
					"lifeEngage"), translateMessages($translate,
					$scope.errorMessage), translateMessages($translate,
					"fna.ok"));
			$rootScope.showHideLoadingImage(false);
		}
	}
	$scope.textboxfn = function(value, id, model) {
		UtilityService.disableProgressTab = true;
		var numericReqExp = "^[0-9]*$";
		numericReqExp = new RegExp(numericReqExp);
		$scope.templateId = $('form').attr('id');
		var newModel = ((model).replace(/FNAObject/g, "$scope.FNAObject"))
				.substring(0, (model).replace(/FNAObject/g, "$scope.FNAObject")
						.indexOf('.value'));
		var value1 = value;
		var unit;
		eval("unit =" + newModel + ".unit");
		if (unit != "NA" && numericReqExp.test(value) && value1 != "") {
			value1 = Number(value) * Number($scope.unitList[unit]);
		}
		if (value1.length > 1 && (value1.substring(0, 1) == 0)) {
			value1 = value1.substring(1, value1.length)
		}

		if (typeof value == "undefined" || value == "") {
			value1 = Number(value) * 0;
		}

		//var limit = value1;
		//eval(newModel + ".value =" + value1);
		eval(newModel + ".value =" +(" value1"));
		/*if(value1 = "<1 times")
		 limit = 0;
		else if(value1 = "1-2 times")
			 limit = 1;
		else if(value1 = ">2 times")
			 limit = 2;
		value1  = limit;
		*/
		$("#" + id).slider("value", parseInt(value1));
		$scope.showCalculations = false;
		$scope.calculatePendingGoals();
		$scope.refresh();
	}
	function getProductSuccess(data, goalObject) {

		$scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].result = data;
		$rootScope.showHideLoadingImage(false);

		if ($scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].result != undefined) {
			$scope.showCalculations = true;
			$scope.goalruleResults = $scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].result.ruleExecutionOutput[0];
		} else {
			$scope.goalruleResults = [];
			$scope.showCalculations = false;
		}
		$scope.calculatePendingGoals();
		$scope.refresh();

		// $rootScope.updateErrorCount("BeneficiaryDetailsSubTab");
		$rootScope.updateErrorCount($scope.viewName);
	}

	$scope.getProductFailure = function(data) {
	}
	function preferencesPopUp() {
		var flag = $('#popup_modal_bg').css("display");
		if (flag == "block") {
			$('.popup_setpref_container').css("display", "block");
			var top = ($(window).height() - $('.popup_setpref_container')
					.height()) / 2;
			var left = ($(window).width() - $('.popup_setpref_container')
					.width()) / 2;
			$('.popup_setpref_container').css({
				'top' : top + $(document).scrollTop(),
				'left' : left
			});
			$("body").removeClass("hide_scroll").addClass("hide_scroll");
		} else {
			$("body").removeClass("hide_scroll");
		}

	}

	$scope.redirect = function() {
		for (var i = 0; i < FnaVariables.pages.length; i++) {
			if (FnaVariables.pages[i].url == "/fnasummary") {
				$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
				break;
			}
		}
		FnaVariables.setFnaModel($scope.FNAObject);
		var obj = new FnaService.saveTransactions($scope, $rootScope,
				DataService, $translate, UtilityService, false);
		obj.save(function() {
			$location.path('/fnasummary');
		});

	}
	if ($scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].result != undefined) {
		$scope.showCalculations = true;
		$scope.goalruleResults = $scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].result.ruleExecutionOutput[0];
	} else {
		$scope.goalruleResults = [];
		$scope.showCalculations = false;
	}

	// $scope.goalruleResults = {"Existing Coverage":"$2,000","Target
	// Amount":"$70,000","Age of Retirement":"60 Years"};

	// $scope.goalruleResults = [{"Existing Coverage":"$2,000"},{"Target
	// Amount":"$70,000"},{"Age of Retirement":"60 Years"}];

	/* KLC - POP UP FOR SELECTION OF DENOMINATION */
	var g_currentlySelected = [];
	$(document.body).on('click', '.unit_select_popup_btn', function() {
		/*
		 * var thisObj = $(this); alert("in jquert "+$(this)) ;
		 * g_currentlySelected[0] = thisObj; var thisObjDeno =
		 * $(thisObj).text(); if(thisObjDeno == "K"){ $('.unit_selector_popup
		 * .denomination').removeClass("selected"); $('.unit_selector_popup
		 * .denomination').eq(0).addClass("selected"); } if(thisObjDeno == "L"){
		 * $('.unit_selector_popup .denomination').removeClass("selected");
		 * $('.unit_selector_popup .denomination').eq(1).addClass("selected"); }
		 * if(thisObjDeno == "C"){ $('.unit_selector_popup
		 * .denomination').removeClass("selected"); $('.unit_selector_popup
		 * .denomination').eq(2).addClass("selected"); } var individualOffset =
		 * findOffsetForScrap(thisObj , 30, -50); var topVal =
		 * individualOffset[0]; var leftval = individualOffset[1]; var chk =
		 * $('.unit_selector_popup').css("display");alert("chk "+chk) if(chk ==
		 * "none"){ $('.unit_selector_popup').fadeIn();
		 * $('.unit_selector_popup').css( { "display" : "block", "top" : topVal,
		 * "left" : leftval }); } if(chk == "block"){
		 * $('.unit_selector_popup').fadeOut();
		 * $('.unit_selector_popup').css("display","none"); }
		 */
	});

	/* UPON CLICKING K-L-C */
	$(document.body)
			.on(
					'click',
					'.unit_selector_popup .denomination',
					function() {
						$('.unit_selector_popup .denomination').removeClass(
								"selected");
						$(this).addClass("selected");
						var letter = $(this).text();
						$(g_currentlySelected[0]).html(" ").text(letter);
					});
	function findOffsetForScrap(thisObj, x, y) {
		var cordinateVal = $(thisObj).offset();
		var topVal = cordinateVal.top + x + "px";
	
		var leftVal = cordinateVal.left + y + "px";
	
		return [ topVal, leftVal ];
	}

	;
	/*
	 * FUNCTION TO CHECK THE "this" POSITION & IF IT IS > 800 , CHANGE THE
	 * POSITION & ARROW OF POP OVER
	 */
	function changeArrowPosn(cordinateVal) {
		var modifiedVal = cordinateVal - 420 + "px";
		var flag = $('.videopopover').children().last().attr("class");
		if (flag == "attacharrow") {
			$('.videopopover').children().last().removeClass().addClass(
					"attacharrow_right");
		}
		return modifiedVal;
	}

	;

	$scope.test = function(index, id, event) {
		
		var thisObj = event.currentTarget;
		g_currentlySelected[0] = thisObj;
		var thisObjDeno = $(thisObj).text();
		if (thisObjDeno == "K") {
			$('.unit_selector_popup .denomination').removeClass("selected");
			$('.unit_selector_popup .denomination').eq(0).addClass("selected");
		}
		if (thisObjDeno == "L") {
			$('.unit_selector_popup .denomination').removeClass("selected");
			$('.unit_selector_popup .denomination').eq(1).addClass("selected");
		}
		if (thisObjDeno == "C") {
			$('.unit_selector_popup .denomination').removeClass("selected");
			$('.unit_selector_popup .denomination').eq(2).addClass("selected");
		}
		var individualOffset = findOffsetForScrap(thisObj, 30, -50);
		var topVal = individualOffset[0];
		var leftval = individualOffset[1];
		var chk = $('.unit_selector_popup').css("display");
		if (chk == "none") {
			$('.unit_selector_popup').fadeIn();
			$('.unit_selector_popup').css({
				"display" : "block",
				"top" : topVal,
				"left" : leftval
			});
		}
		if (chk == "block") {
			$('.unit_selector_popup').fadeOut();
			$('.unit_selector_popup').css("display", "none");
		}
		$scope.currentIndexForPopUp = index;
		$scope.currentIdForPopUp = id;
	}

	$scope.calculatePendingGoals = function() {
		$scope.pengingGoals = " ";
		var count = 0;
		var count2 = 0;
		for (var i = 0; i < $scope.FNAObject.FNA.goals.length; i++) {
			if ($scope.FNAObject.FNA.goals[i].result == undefined) {
				count++;
			}
		}
		if (count != 0) {
			var pend = "";
			for (var i = 0; i < $scope.FNAObject.FNA.goals.length; i++) {
				if ($scope.FNAObject.FNA.goals[i].result == undefined) {
					count2++;
					if (count == 1) {
						pend = pend + "'"
								+ $scope.FNAObject.FNA.goals[i].goalName + "'.";
					} else if (count2 != count) {
						pend = pend + "'"
								+ $scope.FNAObject.FNA.goals[i].goalName
								+ "', ";
					} else {
						pend = pend + "and " + "'"
								+ $scope.FNAObject.FNA.goals[i].goalName + "'.";
					}
				}
			}
			$scope.pengingGoals = pend;
		}
	}

	$scope.disableTab = function() {
		if (UtilityService.disableProgressTab) {
			for (var j = 0; j < $scope.pageSelections.length; j++) {
				if ($scope.pageSelections[j].pageType == "Product Recommendation") {
					$scope.pageSelections[j].isDisabled = "disabled";
				}
			}
		}
		$scope.FNAObject.FNA.pageSelections = $scope.pageSelections;
	}
	$scope.calculatePendingGoals();
	$scope.nextPage = function() {
		$scope.pendingGoalsList = [];
		var isFirsPriorityCalculated = true;
		var isCalculated;
		var isSelected = $scope.FNAObject.FNA.goals.length;
		$scope.disableTab();
		if ($scope.FNAObject.FNA.goals[0].result == undefined) {
			var name = {};
			isFirsPriorityCalculated = false;
			name.translateID = $scope.FNAObject.FNA.goals[0].translateID;
			$scope.pendingGoalsList.push(name);
		}

		if (isFirsPriorityCalculated) {
			for (var i = 0; i < isSelected; i++) {
				var name = {};
				if ($scope.FNAObject.FNA.goals[i].result == undefined) {
					name.translateID = $scope.FNAObject.FNA.goals[i].translateID;
					$scope.pendingGoalsList.push(name);
				}
			}
		}

		isCalculated = Number(isSelected)
				- Number($scope.pendingGoalsList.length);
		if (isCalculated == 0 || !isFirsPriorityCalculated) {
			$('#proceedButton').hide();
		}
		if (isCalculated == isSelected) {
			for (var i = 0; i < FnaVariables.pages.length; i++) {
				if (FnaVariables.pages[i].url == "/fnasummary") {
					$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
					break;
				}
			}

			FnaVariables.setFnaModel($scope.FNAObject);
			$scope.status = "Initial";
			var obj = new FnaService.saveTransactions($scope, $rootScope,
					DataService, $translate, UtilityService, false);
			// obj.save();
			obj.save(function() {
				$location.path('/fnasummary');
			});
		} else if (isFirsPriorityCalculated == false) {
			$scope.isDisplay = true;
			$rootScope.lePopupCtrl.showError(translateMessages($translate,
					"lifeEngage"), translateMessages($translate,
					"fna.calculateAtleastOne"), translateMessages($translate,
					"fna.ok"));
		} else if (isCalculated > 0) {
			// $('#proceedButton').show();
			$scope.isDisplay = true;
			$scope.errorPopupMessage = [];
			$scope.errorPopupMessage.push(translateMessages($translate,
					"fna.calculateRemaining"));
			for (var i = 0; i < $scope.pendingGoalsList.length; i++)
				$scope.errorPopupMessage.push(translateMessages($translate,
						$scope.pendingGoalsList[i].translateID));
			$rootScope.lePopupCtrl
					.showWarning(translateMessages($translate, "lifeEngage"),
							$scope.errorPopupMessage, translateMessages(
									$translate, "general.productProceed"),
							$scope.redirect, translateMessages($translate,
									"general.productCancelOption"),
							$scope.cancel, $scope.cancel);

		}
	}
}]);