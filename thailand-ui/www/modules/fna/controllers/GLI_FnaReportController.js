storeApp.controller('GLI_FnaReportController', ['$rootScope', 'UtilityService', '$scope','$location','$compile','DataService','FnaVariables','GLI_FnaService','$translate','$routeParams','globalService','PersistenceMapping','$controller',
		function($rootScope, UtilityService, $scope, $location, $compile, DataService, FnaVariables, GLI_FnaService, $translate,$routeParams, globalService, PersistenceMapping, $controller) {
			$controller('FnaReportController', {
				$rootScope : $rootScope,
				$scope : $scope,
				$location : $location,
				$compile : $compile,
				DataService : DataService,
				FnaVariables : FnaVariables,
				FnaService : GLI_FnaService,
				$translate : $translate,
				$routeParams : $routeParams,
				globalService : globalService,
				PersistenceMapping : PersistenceMapping
			});
			
            /* FNA changes by LE Team starts*/
			$scope.isReportSection = true;
            $scope.emailRegexPattern = /^[A-Za-z0-9._%+-]+@[A-Za-z]+(\.([A-Za-z]){2,4})?\.([A-Za-z]{2,4})$/; 
			/* FNA changes by LE Team ends*/
            
			$rootScope.selectedPage = "FNAreport";
			//FnaVariables.fnaKeys.Key15  = FnaVariables.getStatusOptionsFNA()[1].status;
			$scope.status = FnaVariables.getStatusOptionsFNA()[1].status;
			$scope.riderIndex =  0;
			$scope.translateLabel = function(value) {
				 return $translate.instant(value);
				 ;
			}
			$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;
			$scope.currentPageType  = "FNA Report";
			UtilityService.disableProgressTab = false;
			//enabling and disabling the tabs
			GLI_FnaService.initalizeProgressbar($scope , $scope.currentPageType , $scope.pageSelections);
			
			var tempArray =[];
			var tempArray1 =[];
			var tempArray2 =[];
			for(var i=1 ; i< $scope.FNAObject.FNA.parties.length ; i++){
			
				if(i<6){
					tempArray1.push($scope.FNAObject.FNA.parties[i]);
				}
				else{
					tempArray2.push($scope.FNAObject.FNA.parties[i]);
				}
			}
			if(tempArray2.length==0){
				tempArray.push(tempArray1);
			}
			else{
				tempArray.push(tempArray1,tempArray2); 
			}
			$scope.customizedFnaPartiesObj = tempArray; 
			$scope.homeNumber = ($scope.FNAObject.FNA.parties[0].ContactDetails.mobileNumber1).replace(/(\d{3})(\d{3})/, "$1-$2-");
			//for setting the title
			for(var i=0 ; i< FnaVariables.custGenderSelectionsList.length ; i++){
				if($scope.FNAObject.FNA.parties[0].BasicDetails.gender == FnaVariables.custGenderSelectionsList[i].id){
					$scope.title = FnaVariables.custGenderSelectionsList[i].title;
				}
			}
			
			/*if($scope.FNAObject.FNA.parties[0].BasicDetails.lastName != "" && typeof  $scope.FNAObject.FNA.parties[0].BasicDetails.lastName != "undefined"){
                var title = translateMessages($translate,("fnaReport."+$scope.title));
                if($rootScope.moduleVariable=="วิเคราะห์ความต้องการทางการเงิน")
				        $scope.name = title + " " + $scope.FNAObject.FNA.parties[0].BasicDetails.firstName + " " + $scope.FNAObject.FNA.parties[0].BasicDetails.lastName;
                else
                    $scope.name = title + ". " + $scope.FNAObject.FNA.parties[0].BasicDetails.firstName + " " + $scope.FNAObject.FNA.parties[0].BasicDetails.lastName;
			}else{
                var title = translateMessages($translate,("fnaReport."+$scope.title));				
                if($rootScope.moduleVariable=="วิเคราะห์ความต้องการทางการเงิน")
                    $scope.name=title + " " + $scope.FNAObject.FNA.parties[0].BasicDetails.firstName;
                else
                    $scope.name=title + ". " + $scope.FNAObject.FNA.parties[0].BasicDetails.firstName;
			} */
			
			// FNA changes by LE Team Start regarding title
            if($scope.FNAObject.FNA.parties[0].BasicDetails.gender=="Male" && $scope.FNAObject.FNA.parties[0].BasicDetails.age>15){
                   var title= translateMessages($translate,("fnaReport."+"Mr"));
            }
            else if($scope.FNAObject.FNA.parties[0].BasicDetails.gender=="Female" && $scope.FNAObject.FNA.parties[0].BasicDetails.age>15 && $scope.FNAObject.FNA.lifeStage.description!=="Single"){
                   var title= translateMessages($translate,("fnaReport."+"Mrs"));
            }
            else if($scope.FNAObject.FNA.parties[0].BasicDetails.gender=="Female" && $scope.FNAObject.FNA.parties[0].BasicDetails.age>15 && $scope.FNAObject.FNA.lifeStage.description=="Single"){
                   var title= translateMessages($translate,("fnaReport."+"Miss"));
            }
            else if($scope.FNAObject.FNA.parties[0].BasicDetails.gender=="Female" && $scope.FNAObject.FNA.parties[0].BasicDetails.age<=15){
                   var title= translateMessages($translate,("fnaReport."+"Girl"));
            }
            else if($scope.FNAObject.FNA.parties[0].BasicDetails.gender=="Male" && $scope.FNAObject.FNA.parties[0].BasicDetails.age<=15){
                   var title= translateMessages($translate,("fnaReport."+"Boy"));
            }
            
            
			if($scope.FNAObject.FNA.parties[0].BasicDetails.lastName != "" && typeof  $scope.FNAObject.FNA.parties[0].BasicDetails.lastName != "undefined"){                
				    $scope.name = title + " " + $scope.FNAObject.FNA.parties[0].BasicDetails.firstName + " " + $scope.FNAObject.FNA.parties[0].BasicDetails.lastName;                
			}else{               
                    $scope.name=title + " " + $scope.FNAObject.FNA.parties[0].BasicDetails.firstName;               
			}
		 	// FNA changes by LE Team End regarding title
		 	
			for (var i = 0; i < FnaVariables.pages.length; i++) {
				if (FnaVariables.pages[i].url == "/fnaReport") {
					$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
					break;
				}
			}
			
			for(var i= 0;i< $scope.FNAObject.FNA.selectedGoals.length; i++){
				if($scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].goalName == rootConfig.savings){
					$scope.FNAObject.FNA.selectedGoals[i].result.investmentPurposeSlider =$scope.FNAObject.FNA.goals[i].parameters[0].textBoxValue;
				}else if($scope.FNAObject.FNA.goals[$scope.FNAObject.FNA.activeGoalIndex].goalName == rootConfig.education){
					$scope.FNAObject.FNA.selectedGoals[i].result.educationChildNameSlider =$scope.FNAObject.FNA.goals[i].parameters[1].textBoxValue;
				}
			}
			
			//for setting goalname in recommended product section
			for(var i= 0;i< $scope.FNAObject.FNA.selectedGoals.length; i++){
				if($scope.FNAObject.FNA.selectedGoals[i].result.educationChildNameSlider != "" && typeof $scope.FNAObject.FNA.selectedGoals[i].result.educationChildNameSlider != "undefined"){
					$scope.FNAObject.FNA.selectedGoals[i].newGoalName = $scope.FNAObject.FNA.selectedGoals[i].result.educationChildNameSlider + "'s " + translateMessages($translate,$scope.FNAObject.FNA.selectedGoals[i].translateID);
				}else{
					$scope.FNAObject.FNA.selectedGoals[i].newGoalName = translateMessages($translate,$scope.FNAObject.FNA.selectedGoals[i].translateID);
				}
			}
			//for setting goalname in priority section
			for(var i= 0;i< $scope.FNAObject.FNA.goals.length; i++){
				if($scope.FNAObject.FNA.goals[i].result){
					if($scope.FNAObject.FNA.goals[i].result.educationChildNameSlider){
						if($scope.FNAObject.FNA.goals[i].result.educationChildNameSlider != "" && typeof $scope.FNAObject.FNA.goals[i].result.educationChildNameSlider != "undefined"){
							/* FNA Changes by LE Team starts */
							$scope.FNAObject.FNA.goals[i].newGoalName = translateMessages($translate, "fnaReport.educationForChild") + ":" + " " + $scope.FNAObject.FNA.goals[i].result.educationChildNameSlider;
							/* FNA Changes by LE Team ends */
						}
					}/* FNA Changes by LE Team start */
                    else if ($scope.FNAObject.FNA.goals[i].goalId == "Health") {
						$scope.FNAObject.FNA.goals[i].newGoalName = "fnaReport.Health";
					}/* FNA Changes by LE Team end */
                    else{
						$scope.FNAObject.FNA.goals[i].newGoalName = translateMessages($translate,$scope.FNAObject.FNA.goals[i].translateID);
					}
				}
				else{
					if($scope.FNAObject.FNA.goals[i].goalName == rootConfig.goalName){
						$scope.FNAObject.FNA.goals[i].newGoalName = translateMessages($translate,($scope.FNAObject.FNA.goals[i].translateID + "s")) + $scope.FNAMyself.BasicDetails.firstName;
					}else{
						$scope.FNAObject.FNA.goals[i].newGoalName = translateMessages($translate,$scope.FNAObject.FNA.goals[i].translateID);
					}
				}
				if($scope.FNAObject.FNA.goals[i].goalName == rootConfig.protection){
					$scope.FNAObject.FNA.goals[i].newGoalName = translateMessages($translate,$scope.FNAObject.FNA.goals[i].goalName);
				}
				
			}
			
			for(var i= 0;i< $scope.FNAObject.FNA.unselectedGoals.length; i++){
				if($scope.FNAObject.FNA.unselectedGoals[i].goalName == rootConfig.protection){
					$scope.FNAObject.FNA.unselectedGoals[i].newGoalName = translateMessages($translate,"fnaReport.protection");
				}else{
					$scope.FNAObject.FNA.unselectedGoals[i].newGoalName = translateMessages($translate,$scope.FNAObject.FNA.unselectedGoals[i].translateID);
				}
			}
			 $scope.emailFNAReport = function() {
		    		$scope.emailToId = "";
		    		$scope.emailccId ="";
		    		var subject = translateMessages($translate,"fna.emailSubject");
		    		$scope.mailSubject = subject;
		    		$scope.pdfName = subject+".pdf";
		    		if ($scope.FNAMyself.ContactDetails.emailId != "" || $scope.FNAMyself.ContactDetails.emailId != undefined) {
		    			$scope.emailToId = $scope.FNAMyself.ContactDetails.emailId;
		    		}
					
					/* FNA changes by LE Team starts */
					if ($scope.FNAObject.FNA.AgentDetails.emailId != "" || $scope.FNAObject.FNA.AgentDetails.emailId != undefined) {
						$scope.emailccId = $scope.FNAObject.FNA.AgentDetails.emailId;
					}
					/* FNA changes by LE Team ends */
					
		    		$scope.showEmailPopup();
		    	}
			 
			 $scope.emailPopupOkClick = function (){		
					$scope.validateMailIds(function(emailIdStatus){
						if(emailIdStatus != false){
							$scope.emailpopup = false;
						    $rootScope.showHideLoadingImage(true,"Please Wait");
						    $scope.saveEmailSendFlagAndData($scope.showSuccessPopup,$scope.showErrorPopup);
						}
					});	
				}
			 
			 $scope.validateMailIds = function(emailCallback) {	
					$scope.emailToError ="";
					$scope.emailCcError ="";
					var status = true;
					/* FNA Changes by LE Team Start */
                    // var EMAIL_REGEXP = rootConfig.emailPattern;
                    var EMAIL_REGEXP = $scope.emailRegexPattern;
                    /* FNA Changes by LE Team End */
					if($scope.emailToId==""|| $scope.emailToId == undefined){
							$scope.emailToError = translateMessages(
									$translate, "fna.requiredEmailValidationMessage");
							status = false;
					}else{
							var emailIdsToValidate = [];
							emailIdsToValidate.push($scope.emailToId);
							if($scope.emailccId !=""){
								emailIdsToValidate.push($scope.emailccId);
							}
							for(var t = 0;t < emailIdsToValidate.length; t++){
								var toEmailId = emailIdsToValidate[t];
								if(toEmailId.charAt(toEmailId.length - 1) == ";")
								{
									toEmailId = toEmailId.substring(0, (toEmailId.length-1));
								}
								
								var tomailIds = toEmailId.split(';');		
								for (var idCount = 0; idCount < tomailIds.length; idCount++) {		 
						            /*if(!EMAIL_REGEXP.test(tomailIds[idCount])){	
						            	if(t === 0){
						            		$scope.emailToError =  translateMessages(
													$translate, "fna.validToEmailValidationMessage");
						            	}else{
						            		$scope.emailCcError =  translateMessages(
													$translate, "fna.validCcEmailValidationMessage");
						            	}													
										status = false;
										break;
									}*/      			
								}
							}
					}
					emailCallback(status);
				}
			 //extended for including language
			 $scope.saveEmailSendFlagAndData = function(successCallback,errorCallBack) {
				 
					var emailObj = {};
					emailObj.toMailIds = $scope.emailToId;
					emailObj.fromMailId = $scope.emailfromId;
					emailObj.ccMailIds = $scope.emailccId; 
					emailObj.mailSubject =  $scope.mailSubject;
					if($scope.FNAObject.FNA.parties[0].BasicDetails.lastName != "" && typeof $scope.FNAObject.FNA.parties[0].BasicDetails.lastName != "undefined"){
						emailObj.leadName = $scope.FNAObject.FNA.parties[0].BasicDetails.firstName + " " + $scope.FNAObject.FNA.parties[0].BasicDetails.lastName;
					}else{
						emailObj.leadName = $scope.FNAObject.FNA.parties[0].BasicDetails.firstName;
					}
					//emailObj.pdfName =  $scope.pdfName; 
					var selectedLanguage = (localStorage["locale"] == "in_ID") ? "in" : "en";
					emailObj.language = selectedLanguage; 
					GLI_FnaService.saveEmailTransactions($scope, $rootScope, $translate, emailObj, DataService, successCallback, errorCallBack);		
			}
			
			 $scope.showSuccessPopup = function(online){
				    $rootScope.showHideLoadingImage(false);
			        $scope.emailToId = "";
			        $scope.emailccId = "";
					$scope.mailSubject = "";
					$scope.pdfName = "";   
					
					/* FNA changes by LE Team starts */
					if ((rootConfig.isDeviceMobile) && !checkConnection()) {	
							$rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "illustrator.emailOkButtonMessageOffline"), translateMessages($translate,"fna.ok"));
					} else {
							$rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "illustrator.emailOkButtonMessage"), translateMessages($translate,"fna.ok"));									
					}
					/* FNA changes by LE Team ends */
					
					$scope.refresh();
			    }
			//For populatind the address
				if($scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine2  != "" && 
							$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine1 != "" &&
							$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.city != ""  && $scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.state != "" &&
							$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.zipCode != ""){
				$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.address = $scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine2 + ", " + 
							$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine1 + ", " + 
							$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.city + ", " + $scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.state + ", " +
							$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.zipCode;
				}else if($scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine2  != "" && 
						$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine1 != "" &&
						$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.city != ""  && $scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.state != ""){
					$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.address = $scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine2 + ", " + 
					$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine1 + ", " + 
					$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.city + ", " + $scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.state;
				}else if($scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine2  != "" && 
						$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine1 != "" &&
						$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.city != ""){
					$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.address = $scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine2 + ", " + 
					$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine1 + ", " + 
					$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.city;
				}else if($scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine2  != "" && $scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine1 != ""){
					$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.address = $scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine2 + ", " + 
					$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine1;
				}else{
					$scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.address = $scope.FNAObject.FNA.parties[0].ContactDetails.currentAddress.addressLine2;
				}
			
			$scope.onRetrieveAgentProfileSuccess = function(data) {
				$scope.agent = data.AgentDetails;
				$scope.refresh();
				$scope.goalIndex = -1;
                
				// FNA Changes by LE team // 
				LEDynamicUI.paintUI(rootConfig.template, "FnaReport.json", "FNASummaryForReport", "#FNASummaryForReport", true,  function() {
					//$scope.onPaintUISuccess();
				}, $scope, $compile);
				
			};
            
            /* FNA changes by LE Team Regarding date manipulation starts */
			$scope.arrangeDateforFNAReportGeneration = function() {
				
				var locale = rootConfig.locale;
				var fnaReportDateTemp = angular.copy(new Date($scope.FNAObject.FNA.fnaCompletion));
                if(!(rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)){
                    $scope.FNAObject.FNA.AgentDetails.mobileNumber = $scope.agent.mobileNumber;
                }				
				$scope.yearForReport = fnaReportDateTemp.getFullYear() + rootConfig.thaiYearDifference;
				$scope.monthkeyForReport = "monthKeyMap"+"."+(fnaReportDateTemp.getMonth() + 1);
				$scope.dateForReport=fnaReportDateTemp.getDate();
				$scope.hoursForReport = fnaReportDateTemp.getHours() < 10 ? "0" + fnaReportDateTemp.getHours() : fnaReportDateTemp.getHours();
				$scope.minutesForReport=fnaReportDateTemp.getMinutes() < 10 ? "0" + fnaReportDateTemp.getMinutes() : fnaReportDateTemp.getMinutes();
				$scope.agentNameForReport = $scope.FNAObject.FNA.AgentDetails.agentName;
                $scope.mobileNumber = ($scope.FNAObject.FNA.AgentDetails.mobileNumber).replace(/(\d{3})(\d{3})/, "$1-$2-");
                
			}
			
			/* FNA changes by LE Team Regarding date manipulation ends */
            
            
			var onNgRepeatFinished = $rootScope.$on('ngRepeatFinished', function() {
				$scope.dataArray19 = [];
				$scope.onPaintUISuccess();
			});
			$scope.onPaintUISuccess = function(data)
			{
				onNgRepeatFinished();
			  $scope.goalIndex = $scope.goalIndex +1;
                  
              /* FNA changes by LE Team starts */
			  $scope.arrangeDateforFNAReportGeneration();
			  /* FNA changes by LE Team ends */
			  
			  if($scope.goalIndex < $scope.FNAObject.FNA.selectedGoals.length)
			  {
				  if(typeof $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.needGap != "undefined" && $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.needGap != ""){
					  if($scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.needGap == "Infinity" || $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.needGap < 0 ){
						  $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.surplus = abs($scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.needGap);
						  var parentModel="showShortfall_"+$scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].goalId + $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].priority; 
						  var scopeVar=eval('$scope');
						  scopeVar[parentModel]=false;
						  var parentModel="showSurplus_"+$scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].goalId + $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].priority; 
						  var scopeVar=eval('$scope');
						  scopeVar[parentModel]=true;
						  $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.needGapfnaReport = 0;
					  }else{
						  var parentModel="showShortfall_"+$scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].goalId + $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].priority; 
						  var scopeVar=eval('$scope');
						  scopeVar[parentModel]=true;
						  var parentModel="showSurplus_"+$scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].goalId + $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].priority; 
						  var scopeVar=eval('$scope');
						  scopeVar[parentModel]=false;
						   if(typeof $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.montlySavingsArr != "undefined"){
							   $scope.dataArray19Temp = [];
						  for(var d=0; d<Object.keys($scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.montlySavingsArr).length; d++)
							{	
							  var constant = 1000;	
								var accumlatedArray = [];
								if(d==0){
									var xaxisLabel = translateMessages(
													$translate, "fna.now");
								}else{
									var xaxisLabel = "+" + d + translateMessages(
													$translate, "fna.year")
								}
								accumlatedArray.push(xaxisLabel,Math.round($scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.montlySavingsArr[d]/Number(constant)));
								$scope.dataArray19Temp.push(accumlatedArray);
							}
						   }
					  }
				 }else if(typeof $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.futureNeedGap != "undefined" && $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.futureNeedGap != ""){
					 if($scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.futureNeedGap == "Infinity" || $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.futureNeedGap < 0 ){
						 $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.surplus = abs($scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.futureNeedGap);
						 var parentModel="showShortfall_"+$scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].goalId + $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].priority; 
						 var scopeVar=eval('$scope');
						 scopeVar[parentModel]=false;
						 var parentModel="showSurplus_"+$scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].goalId + $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].priority; 
						 var scopeVar=eval('$scope');
						 scopeVar[parentModel]=true;
						 $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.futureNeedGapfnaReport = 0;
					 }else{
						 var parentModel="showShortfall_"+$scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].goalId + $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].priority; 
						 var scopeVar=eval('$scope');
						 scopeVar[parentModel]=true;
						 var parentModel="showSurplus_"+$scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].goalId + $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].priority; 
						 var scopeVar=eval('$scope');
						 scopeVar[parentModel]=false;
						 if(typeof $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.montlySavingsArr != "undefined"){
						 $scope.dataArray19Temp = [];
						 for(var d=0; d<Object.keys($scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.montlySavingsArr).length; d++)
						 {		
							var accumlatedArray = [];
							var constant = 1000;
							if(d==0){
									var xaxisLabel = translateMessages(
													$translate, "fna.now");
								}else{
									var xaxisLabel = "+" + d + translateMessages(
													$translate, "fna.year")
								}
							accumlatedArray.push(xaxisLabel,Math.round($scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].result.montlySavingsArr[d]/Number(constant)));
							$scope.dataArray19Temp.push(accumlatedArray);
						}
						 }
					 }
				 }
				  $scope.dataArray19[$scope.goalIndex] = angular.copy($scope.dataArray19Temp);  
				  
				  
				LEDynamicUI.paintUI(rootConfig.template,'FnaReport.json',$scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].goalId,"#"+ $scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].goalId+"_"+$scope.FNAObject.FNA.selectedGoals[$scope.goalIndex].priority,true,  function() {
					$scope.onPaintUISuccess();
				}, $scope, $compile);
			  }
			
			
			}
			$scope.downloadFNAReport = function(){
				$rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
				GLI_FnaService.downloadFNAReport($scope, DataService, $scope.pdfDownloadSuccessPopup, $scope.pdfDownloadErrorPopup);
			}
			
			 $scope.$on("$destroy", function() {
                 if (this.$$destroyed) return;     
				 while (this.$$childHead) {      
				 this.$$childHead.$destroy();    
				 }     
				 if (this.$broadcast)
				{ 
					this.$broadcast('$destroy');   
				} 
				 onNgRepeatFinished();
				 this.$$destroyed = true;                 
				 //$timeout.cancel( timer );                           
            });

			
			
} ]);