/*
 *Copyright 2015, LifeEngage 
 */




'use strict';


storeApp.controller('QuestionerController', ['$rootScope', '$route', '$scope', 'DataService', 'FnaService','LookupService', 'DocumentService', 'ProductService','$compile', 
                                             '$routeParams', '$location', '$translate','IllustratorVariables', 'FnaVariables', 'UtilityService','$debounce', 'PersistenceMapping', 'AutoSave',
                                              'globalService','$timeout','UtilityVariables','RuleService',

                                              function($rootScope, $route, $scope, DataService, FnaService, LookupService,DocumentService, ProductService, $compile, $routeParams, $location, $translate, 
                                             IllustratorVariables,FnaVariables, UtilityService,$debounce, PersistenceMapping, AutoSave, globalService,$timeout,UtilityVariables,RuleService) {

	function questionerOptionClick(event) {
		var thisObj = event.currentTarget;
		var thisId = thisObj.id;
		var thisObjClassName = String(thisObj.className);
		if (thisObjClassName.indexOf("option_qn_general_gray") >= 0) {
			optionsToggleSelect("option_qn_general_gray", "option_qn_general_gray_sel", thisObj, thisId);
		} else if (thisObjClassName.indexOf("option_qn_general") >= 0) {
			optionsToggleSelect("option_qn_general", "option_qn_general_sel", thisObj, thisId);
		}
	};
	function optionsToggleSelect(opt_gen, classTobeRemoved, thisObj, thisId) {
		$("#" + thisId).children().removeClass(classTobeRemoved);
		$(thisObj).addClass(classTobeRemoved);
	}

	;
	$scope.riskprofiler_popup_wrapper = false;
	$scope.cancel = function() {
		$scope.riskprofiler_popup_wrapper = false;
	};
	$scope.cancelQuestionnaire = function() {
	};
	var model = "FNAObject";
	if ((rootConfig.autoSave.fna)) {
		AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, FnaService);
	}
	// $scope.fnaName = 'FNA Controller';
	FnaVariables.selectedPage = "Questioner";
	$scope.selectedpage = FnaVariables.selectedPage;
	$scope.showRisk = true;

	$scope.viewName = 'fnaRiskQuestionnaire';
	$scope.FNAObject = FnaVariables.getFnaModel();
	$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;
	$scope.riskList = FnaVariables.riskList;

	$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;

	$scope.initalizeProgressbar = function() {
		UtilityService.disableProgressTab = false;

		for (var j = 0; j < $scope.pageSelections.length; j++) {
			if ($scope.pageSelections[j].pageType == "Need Analysis") {
				$scope.pageSelections[j].isSelected = "selected";
				$scope.pageSelections[j].isDisabled = "";
			} else {
				$scope.pageSelections[j].isSelected = "";

			}
		}
		$scope.FNAObject.FNA.pageSelections = $scope.pageSelections;
		FnaVariables.setFnaModel($scope.FNAObject);

	};
	//Commented for generali
	//$scope.initalizeProgressbar();
	

	$scope.callback = function() {
		$rootScope.showHideLoadingImage(false);

	};

	$scope.rightSelectedOption = function(id, answerindex) {
		if (id == answerindex) {
			return "option_qn_general_gray_sel";
		}
	};

	$scope.leftSelectedOption = function(id, answerindex) {
		if (id == answerindex) {
			return "option_qn_general_sel";
		}
	};

	$scope.optionClick = function(index, id, score, event) {
		$scope.FNAObject.FNA.questionnaire[index].answerIndex = id;
		$scope.FNAObject.FNA.questionnaire[index].score = score;
		
		UtilityService.disableProgressTab = true;
		questionerOptionClick(event);
	};
	
	
	
	$scope.nextPage = function() {
		// displayOverlay('riskprofiler_popup_wrapper');
		/*$scope.riskprofiler_popup_wrapper = true;
		 */
		var transactionObj = {
			"UserInput" : []
		};
		$scope.showRisk = true;
		for (var i = 0; i < $scope.FNAObject.FNA.questionnaire.length; i++) {
			if ($scope.FNAObject.FNA.questionnaire[i].score == undefined) {

				$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "fna.questionRemaining"), translateMessages($translate, "fna.ok"));
				$scope.showRisk = false;
				break;
			}
			transactionObj.UserInput.push({
				"questionId" : $scope.FNAObject.FNA.questionnaire[i].questionId,
				"score" : $scope.FNAObject.FNA.questionnaire[i].score
			});
		}

		if ($scope.showRisk) {

			$scope.riskprofiler_popup_wrapper = true;
			try {
				var inputJson = JSON.stringify(transactionObj);
				var riskRuleSetObj = FnaVariables.riskCalculationRuleSetConfig;
				RuleService.calculateRisk(inputJson, riskRuleSetObj, function(output) {
					//var result = output;
					$scope.getRiskCalculatorSuccess(output);
				}, function(output) {
				});
			} catch (exception) {
				alert("Error Output " + exception);
				errorCallback(exception);
			}
		}

	};

	$scope.getRiskCalculatorSuccess = function(data) {
		$scope.FNAObject.FNA.riskProfile.level = data.risk;
		$scope.refresh();
	};

	$scope.errorCallback = function() {
	};

	$scope.isRiskSelected = function(data) {
		if ($scope.FNAObject.FNA.riskProfile.level == data) {
			return "_selected";
		}
		return "";
	};

	$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
	$scope.refresh();
	var questionnaireUiJsonFile = FnaVariables.uiJsonFile.questionareViewFile;

	$timeout(function() {
		LEDynamicUI.paintUI(rootConfig.template, questionnaireUiJsonFile, $scope.viewName, "#" + $scope.viewName, true, function() {
			$scope.callback();
		}, $scope, $compile);
	}, 0);

	
	$scope.disableTab = function() {
		if (UtilityService.disableProgressTab) {
			for (var j = 0; j < $scope.pageSelections.length; j++) {
				if ($scope.pageSelections[j].pageType == "Product Recommendation") {
					$scope.pageSelections[j].isDisabled = "disabled";
				}
			}
		}
		$scope.FNAObject.FNA.pageSelections = $scope.pageSelections;
		FnaVariables.setFnaModel($scope.FNAObject);
	};
	$scope.riskSummary = function() {
		try {
			for (var i = 0; i < FnaVariables.pages.length; i++) {
				if (FnaVariables.pages[i].url == "/fnaRecomendedProducts") {
					$scope.FNAObject.FNA.lastVisitedPage = FnaVariables.pages[i].pageName;
					break;
				}
			}
			FnaVariables.setFnaModel($scope.FNAObject);
			$scope.disableTab();
			$scope.status = "Final";
			$scope.initalizeProgressbarNext();
			var obj = new FnaService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, false);
			obj.save();
			$scope.riskprofiler_popup_wrapper = false;
			$timeout(function() { $location.path('/fnaRecomendedProducts'); },
					  50);
			// Provided a timeout in IOS version since Product ListingController
			// was getting loaded more than once and the PSM ouput was not
			// giving back all product details like collaterals
			// Not required since this is happening only in some iPad devices
			/*
			 * $timeout(function() { $location.path('/fnaRecomendedProducts'); },
			 * 50);
			 */
		} catch (exception) {
			alert("Error Output " + exception);
		}
	};
	// To change the selected tab style with highlighted background
	$scope.initalizeProgressbarNext = function() {
		$scope.FNAObject = FnaVariables.getFnaModel();
		$scope.pageSelections = $scope.FNAObject.FNA.pageSelections;
		UtilityService.disableProgressTab = false;
		for (var j = 0; j < $scope.pageSelections.length; j++) {
			if ($scope.pageSelections[j].pageType == "Product Recommendation") {
				$scope.pageSelections[j].isSelected = "selected";
				$scope.pageSelections[j].isDisabled = "";
			} else {
				$scope.pageSelections[j].isSelected = "";

			}
		}
		$scope.FNAObject.FNA.pageSelections = $scope.pageSelections;
		FnaVariables.setFnaModel($scope.FNAObject);
	};
}]);