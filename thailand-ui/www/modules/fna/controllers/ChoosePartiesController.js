'use strict';

storeApp.controller('ChoosePartiesController', ['$rootScope', '$scope', 'DataService', 'FnaService','LookupService', 'DocumentService', 'ProductService','$compile', 
                                                '$routeParams', '$location', '$translate','IllustratorVariables', 'FnaVariables', 'EappVariables','UtilityService','$debounce', 'PersistenceMapping', 'AutoSave',
                                                 'globalService','$timeout',
                                                function($rootScope, $scope, DataService, FnaService, LookupService,DocumentService, ProductService, $compile, $routeParams, $location, $translate, 
                                                IllustratorVariables,FnaVariables, EappVariables, UtilityService,$debounce, PersistenceMapping, AutoSave, globalService,$timeout) {

	// Set the page name for progressbar implementation - Required in each page
	// specific controller
	FnaVariables.selectedPage = "ChooseParties";
	$scope.selectedpage = FnaVariables.selectedPage;
	$rootScope.lePopupCtrl = {
	};
	// Get the latest FNA Model from global service
	$scope.FNAObject = FnaVariables.getFnaModel();

	// Set the next page button name based on the next screen to be displayed
	if (UtilityService.previousPage == 'FNA'
			|| UtilityService.previousPage == 'IllustrationPersonal' || UtilityService.previousPage == 'LMS') {
		$scope.nextButtonLabel = "proceedtoIllustartionHeaderText";
		$rootScope.moduleVariable = translateMessages($translate, "Illustrator");
	} else if (UtilityService.previousPage == 'Illustration') {
		$scope.nextButtonLabel = "proceedToEapp";
		$rootScope.moduleVariable = translateMessages($translate, "e-Application");
	}

	/**
	 * Set the insured image and related params to scope variable to get in
	 * partial --Begin *
	 */
	
	if (UtilityService.previousPage == 'LMS'){
		$scope.FNAMyself = globalService.getParty("Lead");
	}else{
		$scope.FNAMyself = globalService.getParty("FNAMyself");
	 }
	
	if ($scope.FNAMyself.BasicDetails.photo == ""
			|| $scope.FNAMyself.BasicDetails.photo == undefined) {
		$scope.FNAMyself.BasicDetails.photo = FnaVariables.choosePartiesPhoto;
	}

	/**
	 * Set the Beneficiary image and related params to scope variable to get in
	 * partial --Begin *
	 */
	$scope.beneficiaries = globalService.getFNABeneficiaries();
	$scope.members = FnaVariables.images;
	$scope.beneficiaryData = [];
	$scope.remainingBeneficiaries = [];// To set all beneficiaries back to
	// globalService
	var checkFlag = "";
	// To display only first 5 beneficiaries from family screen
	var beneficiaryCount = 0;// To display only first 5 beneficiaries from
	// family screen
	for (var i = 0; i < $scope.beneficiaries.length; i++) {
		if ($scope.beneficiaries[i] != null && i < 5) {
			beneficiaryCount++;
			$scope.beneficiaryData[i] = $scope.beneficiaries[i];
			// Set the image
			if ($scope.beneficiaries[i].BasicDetails == undefined) {
				$scope.beneficiaries[i].BasicDetails = {};
			}

			if ($scope.beneficiaries[i].BasicDetails.photo == undefined) {
				$scope.beneficiaryData[i].BasicDetails.photo = FnaVariables.choosePartiesPhoto;
			}

		} else if ($scope.beneficiaries[i] != null && i > 4) { // To set all
			// beneficiaries
			// back to
			// globalService
			$scope.remainingBeneficiaries.push($scope.beneficiaries[i]);
		}
	}
	/**
	 * Set the Beneficiary image and related params to scope variable to get in
	 * partial --End *
	 */

	// To prepopulate selected data on page load
	$scope.prePopulateData = function() {
		// $( ".myself" ).draggable({ revert: true, helper: "clone",
		// revertDuration: 100 });
		// To populate the already dragged insurance/proposer and beneficiary in
		// edit mode -- Proceed to eApp from illustration
		// From FNA -- Always new illustration will be created
		if (UtilityService.previousPage == 'Illustration'
				|| UtilityService.previousPage == 'IllustrationPersonal') {
			var parties = globalService.getParties();
			for (var i = 0; i < parties.length; i++) {
				var party = parties[i];
				if (party.isInsured) {

					$('#insuredLst').children().append(
							"<span class='myself'><img alt='' src='data:image/jpeg;base64,"
									+ [ [ party.BasicDetails.photo ] ] + "' />"
									+ "<span class='myself_name'>"
									+ [ [ party.BasicDetails.firstName ] ]
									+ "</span></span>");
				}
				if (party.isPayer) {
					$('#proposedLst').children().append(
							"<span class='myself'><img alt='' src='data:image/jpeg;base64,"
									+ [ [ party.BasicDetails.photo ] ] + "' />"
									+ "<span class='myself_name'>"
									+ [ [ party.BasicDetails.firstName ] ]
									+ "</span></span>");
				}
				if (party.isBeneficiary) {

					var dependentindex = party.id;// For delete flow of
					// beneficairy

					var myEl = angular.element(document
							.querySelector('#benefLst'));
					myEl
							.append("<li class='cpLstItem' show-scrap> <span class='dependent' id='"
									+ dependentindex
									+ "'> <img alt='' src='data:image/jpeg;base64,"
									+ [ [ party.BasicDetails.photo ] ]
									+ "' />"
									+ "<span class='myself_name'>"
									+ [ [ party.BasicDetails.firstName ] ]
									+ "</span>" + "</span></li>");
					$compile(myEl)($scope);

				}
			}
		}

	};

	/** ON CLICK OF THE DROPPED ITEM, IT SHOULD SHOW THE SCRAP POP UP -- Begin * */
	$scope.thisObjDeletion;
	$scope.benef_id;
	$scope.currentlyRemovedId;

	$scope.onDelete = function() {
		var chkFlag = $('.cp_scrapButton').css("display");
		if (chkFlag == "block") {

			var childId = $($scope.thisObjDeletion).children();
			// Remove the binding from model

			if ($scope.benef_id == "insuredLst") {
				$scope.FNAMyself.isInsured = false;
				$scope.FNAMyself.fnaInsuredPartyId = "";
				for (var i = 0; i < $scope.beneficiaryData.length; i++) {
					$scope.beneficiaryData[i].isInsured = false;
					$scope.beneficiaryData[i].fnaInsuredPartyId = "";
				}

			} else if ($scope.benef_id == "proposedLst") {
				$scope.FNAMyself.isPayer = false;
				$scope.FNAMyself.fnaPayerPartyId = "";
				for (var i = 0; i < $scope.beneficiaryData.length; i++) {
					$scope.beneficiaryData[i].isPayer = false;
					$scope.beneficiaryData[i].fnaPayerPartyId = "";
				}
			} else {

				if ($scope.currentlyRemovedId
						&& $scope.currentlyRemovedId.length > 0
						&& $scope.currentlyRemovedId.indexOf("myself") > -1) {
					$scope.FNAMyself.isBeneficiary = false;
					$scope.FNAMyself.fnaBenfPartyId = "";
				} else {
					for (var i = 0; i < $scope.beneficiaryData.length; i++) {
						if ($scope.beneficiaryData[i].id == $scope.currentlyRemovedId) {
							$scope.beneficiaryData[i].isBeneficiary = false;
							$scope.beneficiaryData[i].fnaBenfPartyId = "";
							break;
						}
					}
				}

			}
			$scope.refresh();

			$($scope.thisObjDeletion).children().remove();

			if ($scope.benef_id == "benefLst") {
				$($scope.thisObjDeletion).remove();
			}
		}
		$('.cp_scrapButton').css("display", "none");
		event.stopPropagation();

	};

	/** ON CLICK OF THE DROPPED ITEM, IT SHOULD SHOW THE SCRAP POP UP -- End * */
	$scope.myselfChoosePartyDragDrop = function() {
		$(".cp_draggable_section .myself").draggable({
			revert : true,
			helper : "clone",
			revertDuration : 0
		});
		$scope.choosePartyDrop();
	};

	$scope.dependantChoosePartyDragDrop = function() {
		$(".cp_draggable_section .dependent").draggable({
			revert : true,
			helper : "clone",
			revertDuration : 0
		});
		$scope.choosePartyDrop();
	};

	/** Function for droppable implementation of parties -- Begin * */
	$scope.choosePartyDrop = function() {

		var flagCountInsured = 0;
		var flagCountPropose = 0;
		var flagCountBenef = 0;
		$(".cp_insured_section,.cp_beneficiary_section,.cp_proposer_section")
				.droppable(
						{
							hoverClass: 'drop-active',
							drop : function(event, ui) {
								var cloneItem = ui.draggable.clone();
								var flag = $(this).find(".cp_prop_area").attr(
										"id");

								var flagCountInsured = $('#insuredLst')
										.children().children().length;
								var flagCountPropose = $('#proposedLst')
										.children().children().length;
								var flagCountBenef = $('#benefLst').children().length;

								if (flag == "insuredLst") {

									if (flagCountInsured > 0) {
										return;
									}

									if (flagCountInsured == 0) {
										var currentlyDragged = cloneItem[0].id;
									}

									if ($scope.FNAMyself.id == currentlyDragged
											&& $scope.FNAMyself.isBeneficiary) {
										return;
									}
									for (var i = 0; i < $scope.beneficiaryData.length; i++) {
										if ($scope.beneficiaryData[i].id == currentlyDragged
												&& $scope.beneficiaryData[i].isBeneficiary) {
											return;
										}
									}

									$('#insuredLst').children().append(
											cloneItem);
									/**
									 * To set the insured obj to
									 * illustration/eApp object -- Begin *
									 */
									$scope.setInsured($scope, currentlyDragged);
									/**
									 * To set the insured obj to
									 * illustration/eApp object -- End *
									 */

								} else if (flag == "proposedLst") {
									if (flagCountPropose > 0) {
										return;
									}
									if (flagCountPropose == 0) {
										$('#proposedLst').children().append(
												cloneItem);

										/**
										 * To set the Proposer obj to
										 * illustration/eApp object -- Begin *
										 */
										var currentlyDragged = cloneItem[0].id;
										$scope.setPayer($scope,
												currentlyDragged);
										/**
										 * To set the Proposer obj to
										 * illustration/eApp object -- End *
										 */

									}
								} else if (flag == "benefLst") {
									if (flagCountBenef > 5) {
										return;
									}
									if (flagCountBenef <= 5) {
										var currentlyDragged = cloneItem[0].id;

										if ($scope.FNAMyself.id == currentlyDragged
												&& ($scope.FNAMyself.isBeneficiary || $scope.FNAMyself.isInsured)) {
											return;
										}

										for (var i = 0; i < $scope.beneficiaryData.length; i++) {
											if ($scope.beneficiaryData[i].id == currentlyDragged
													&& ($scope.beneficiaryData[i].isBeneficiary || $scope.beneficiaryData[i].isInsured)) {
												return;
											}
										}

										/**
										 * To set the beneficiary obj to
										 * illustration/eApp object -- Begin *
										 */
										var elementTemp = angular
												.element(document
														.querySelector('#benefLst'));
										elementTemp
												.append("<li class='cpLstItem' show-scrap> <span class='dependent' id='"
														+ currentlyDragged
														+ "'>"
														+ cloneItem.html()
														+ "</span></li>");
										$compile(elementTemp)($scope);
										$scope.setBeneficiary($scope,
												currentlyDragged);
										/**
										 * To set the beneficiary obj to
										 * illustration/eApp object -- Begin *
										 */
									}

								}

							}
						});
	};
	/** Function for droppable implementation of parties -- End * */

	/** Function called on click of Proceed button * */
	// navigate to illustration personal details page
	$scope.nextPage = function() {
		if (UtilityService.previousPage != 'LMS') {
		globalService.setParty($scope.FNAMyself, "FNAMyself");
		var fnaBeneficiaries = $scope.beneficiaryData;
		if ($scope.remainingBeneficiaries != null
				&& $scope.remainingBeneficiaries.length > 0) {
			for (var i = 0; i < $scope.remainingBeneficiaries.length; i++) {
				fnaBeneficiaries.push($scope.remainingBeneficiaries[i]);
			}
		}
		globalService.setFNABeneficiaries(fnaBeneficiaries);
		$scope.FNAObject = FnaVariables.getFnaModel();

		if (UtilityService.previousPage == 'Illustration') {
			// Set key2 as fnaId if synced, else set transactionId as fnaId
			var illustrationId = $routeParams.illustrationTransId;
			
			EappVariables.fnaId = FnaVariables.transTrackingID;
			EappVariables.leadId =FnaVariables.leadId;
			EappVariables.illustratorId =illustrationId;
			$location.path('/Eapp/0/0/' + illustrationId + '/0');
		} else if (UtilityService.previousPage == 'FNA') {

			var parties = globalService.getParties();
			$scope.FNAObject.FNA.parties = parties;
			FnaVariables.setFnaModel($scope.FNAObject);
            
			$scope.dataNew = {
			              "Insured" : {},										
						  "Proposer" : {},										
                          "Beneficiaries" : [],
                          "Product" : {}						  
			};
			//alert(angular.toJson(globalService.getSelectdBeneficiaries()));
			$scope.dataNew.Insured = globalService.getInsured();
			$scope.dataNew.Insured.BasicDetails.firstName = $scope.dataNew.Insured.BasicDetails.firstName + ' ' + $scope.dataNew.Insured.BasicDetails.lastName;
			$scope.dataNew.Insured.BasicDetails.dob = $scope.dataNew.Insured.BasicDetails.dob;
			$scope.dataNew.Insured.BasicDetails.gender = $scope.dataNew.Insured.BasicDetails.gender;
			$scope.dataNew.Insured.BasicDetails.mobileNumber = ($scope.dataNew.Insured.ContactDetails.homeNumber1)?($scope.dataNew.Insured.ContactDetails.homeNumber1):"";	
           
			$scope.dataNew.Proposer = globalService.getPayer();			 
			$scope.dataNew.Proposer.BasicDetails.firstName = $scope.dataNew.Proposer.BasicDetails.firstName + ' ' + $scope.dataNew.Proposer.BasicDetails.lastName;
			$scope.dataNew.Proposer.BasicDetails.dob = $scope.dataNew.Proposer.BasicDetails.dob;
			$scope.dataNew.Proposer.BasicDetails.gender = $scope.dataNew.Proposer.BasicDetails.gender;
			$scope.dataNew.Proposer.BasicDetails.mobileNumber = ($scope.dataNew.Proposer.ContactDetails.homeNumber1)?($scope.dataNew.Proposer.ContactDetails.homeNumber1):"";
		
			$scope.dataNew.Product = globalService.getProduct();	
			$scope.dataNew.Product.ProductDetails.productCode = $scope.dataNew.Product.ProductDetails.productCode;
			$scope.dataNew.Product.ProductDetails.committedPremium = $scope.dataNew.Product.policyDetails.committedPremium;
			$scope.dataNew.Product.ProductDetails.surrenderOption = $scope.dataNew.Product.policyDetails.surrenderOption;
			
			
			//getValidationSuccess();
			FnaService.checkValidationFNAtoBI($scope.dataNew, function(
						output) {
					getValidationSuccess(output, $scope.dataNew);
				}, function(output) {
					$scope.getValidationFailure(output);
				});
			
			
		} else {
			$location
					.path('/Illustrator/'
							+ $scope.FNAObject.FNA.selectedProduct.productId
							+ '/0/0/0');
		}

	}else{
		var productId = $routeParams.productId;
		$location
		.path('/Illustrator/'
				+ productId
				+ '/0/0/0');
	}
	
	};
	
	
	
		
	function getValidationSuccess(data, dataNew) {
			var obj = new FnaService.saveTransactions($scope, $rootScope,
					DataService, $translate, UtilityService, false);
			obj.save(function() {

				IllustratorVariables.selectedPage = "PersonalDetails";
				IllustratorVariables.clearIllustrationVariables();
				// Set key2 as fnaId if synced, else set transactionId as fnaId				
				IllustratorVariables.fnaId = FnaVariables.transTrackingID;
				IllustratorVariables.leadId = FnaVariables.leadId;				
				$location.path('/Illustrator/'
						+ $scope.FNAObject.FNA.selectedProduct.productId
						+ '/0/0/0');

			});
	}
	
	$scope.oKClick = function(){	
	     $scope.proceedDisable = true;
	     $scope.refresh();
	}
	
	$scope.getValidationFailure = function(data) {
								$scope.isValidationRuleExecuted = false;
								if (typeof data.ValidationResults != "undefined"
										|| data.ValidationResults != null) {
									$scope.isValidationRuleExecuted = true;
									$scope.validationMessage = translateMessages(
											$translate,
											"illustrator.validationFailed");
									$scope.validationResults = data.ValidationResults;	
									$scope.proceedDisable = true;
										$scope.refresh();									
									$rootScope.showHideLoadingImage(false);
										$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"),$scope.validationResults,translateMessages($translate,"fna.ok"),$scope.oKClick);
								}
								
							}

	/** Set dragged payer details to illustration/eApp object * */
	$scope.setPayer = function($scope, currentlyDragged) {

		if (currentlyDragged == "myself") {

			$scope.FNAMyself.isPayer = true;
			$scope.FNAMyself.ContactDetails.mobileNumber1 = $scope.FNAMyself.ContactDetails.homeNumber1;
			$scope.FNAMyself.fnaPayerPartyId = currentlyDragged;

			if (UtilityService.previousPage == 'Illustration') {// To change --
				// isPayorDifferentFromInsured
				// flag is
				// treated in
				// diff meanings
				// t implement
				// business
				// logics in
				// eApp and
				// illustration
				if ($scope.FNAMyself.isInsured) {
					$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "No";
					$scope.FNAMyself.CustomerRelationship.relationshipWithProposer = "Self";
				} else {
					$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "Yes";
					$scope.FNAMyself.CustomerRelationship.relationshipWithProposer = "Others";
				}

				var insuredParty = globalService.getInsured();
				insuredParty.CustomerRelationship.relationshipWithProposer = $scope.FNAMyself.CustomerRelationship.relationshipWithProposer;
				globalService.setInsured(insuredParty);

			} else {
				if ($scope.FNAMyself.isInsured) {
					$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "Yes";
				} else {
					$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "No";
				}
			}

		} else {
			for (var i = 0; i < $scope.beneficiaryData.length; i++) {
				if ($scope.beneficiaryData[i].id == currentlyDragged) {
					$scope.beneficiaryData[i].isPayer = true;
					if ($scope.beneficiaryData[i].ContactDetails) {
						$scope.beneficiaryData[i].ContactDetails.mobileNumber1 = $scope.beneficiaryData[i].ContactDetails.homeNumber1;
					}
					$scope.beneficiaryData[i].fnaPayerPartyId = currentlyDragged;

					if (!$scope.beneficiaryData[i].CustomerRelationship) {
						$scope.beneficiaryData[i].CustomerRelationship = {};
					}
					if ($scope.FNAMyself.isInsured) {
						$scope.beneficiaryData[i].CustomerRelationship.relationWithInsured = $scope.beneficiaryData[i].classNameAttached;
					}
					if (UtilityService.previousPage == 'Illustration') {// To
						// change
						// --
						// isPayorDifferentFromInsured
						// flag
						// is
						// treated
						// in
						// diff
						// meanings
						// t
						// implement
						// business
						// logics
						// in
						// eApp
						// and
						// illustration
						if ($scope.beneficiaryData[i].isInsured) {
							$scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured = "No";
							$scope.beneficiaryData[i].CustomerRelationship.relationshipWithProposer = "Self";
						} else {
							$scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured = "Yes";
							$scope.beneficiaryData[i].CustomerRelationship.relationshipWithProposer = "Others";
						}

						var insuredParty = globalService.getInsured();
						insuredParty.CustomerRelationship.relationshipWithProposer = $scope.beneficiaryData[i].CustomerRelationship.relationshipWithProposer;
						globalService.setInsured(insuredParty);

					} else {
						if ($scope.beneficiaryData[i].isInsured) {
							$scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured = "Yes";
						} else {
							$scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured = "No";
						}
					}
					break;
				}

			}

		}
        $scope.proceedDisable = false;
		$scope.refresh();
	};

	/** Set dragegd insured details to illustration/eApp object * */
	$scope.setInsured = function($scope, currentlyDragged) {

		if (currentlyDragged == "myself") {
			$scope.FNAMyself.isInsured = true;
			$scope.FNAMyself.ContactDetails.mobileNumber1 = $scope.FNAMyself.ContactDetails.homeNumber1;
			$scope.FNAMyself.fnaInsuredPartyId = currentlyDragged;

			if (UtilityService.previousPage == 'Illustration') {// To change --
				// isPayorDifferentFromInsured
				// flag is
				// treated in
				// diff meanings
				// t implement
				// business
				// logics in
				// eApp and
				// illustration
				if ($scope.FNAMyself.isPayer) {
					$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "No";
					$scope.FNAMyself.CustomerRelationship.relationshipWithProposer = "Self";
				} else {
					$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "Yes";
					$scope.FNAMyself.CustomerRelationship.relationshipWithProposer = "Others";
				}
				var payerParty = globalService.getPayer();
				payerParty.CustomerRelationship.isPayorDifferentFromInsured = $scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured;
				globalService.setPayer(payerParty);
			} else {
				if ($scope.FNAMyself.isPayer) {
					$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "Yes";
				} else {
					$scope.FNAMyself.CustomerRelationship.isPayorDifferentFromInsured = "No";
				}
			}

		} else {
			for (var i = 0; i < $scope.beneficiaryData.length; i++) {
				if ($scope.beneficiaryData[i].id == currentlyDragged) {
					$scope.beneficiaryData[i].isInsured = true;
					if ($scope.beneficiaryData[i].ContactDetails
							&& $scope.beneficiaryData[i].ContactDetails.homeNumber1) {
						$scope.beneficiaryData[i].ContactDetails.mobileNumber1 = $scope.beneficiaryData[i].ContactDetails.homeNumber1;
					}
					$scope.beneficiaryData[i].fnaInsuredPartyId = currentlyDragged;

					if (!$scope.beneficiaryData[i].CustomerRelationship) {
						$scope.beneficiaryData[i].CustomerRelationship = {};
					}

					if (UtilityService.previousPage == 'Illustration') {// To
						// change
						// --
						// isPayorDifferentFromInsured
						// flag
						// is
						// treated
						// in
						// diff
						// meanings
						// t
						// implement
						// business
						// logics
						// in
						// eApp
						// and
						// illustration
						if ($scope.beneficiaryData[i].isPayer) {
							$scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured = "No";
							$scope.beneficiaryData[i].CustomerRelationship.relationshipWithProposer = "Self";
						} else {
							$scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured = "Yes";
							$scope.beneficiaryData[i].CustomerRelationship.relationshipWithProposer = "Others";
						}

						var payerParty = globalService.getPayer();
						payerParty.CustomerRelationship.isPayorDifferentFromInsured = $scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured;
						globalService.setPayer(payerParty);

					} else {
						if ($scope.beneficiaryData[i].isPayer) {
							$scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured = "Yes";
						} else {
							$scope.beneficiaryData[i].CustomerRelationship.isPayorDifferentFromInsured = "No";
						}
					}

					break;
				}

			}
		}
        $scope.proceedDisable = false;
		$scope.refresh();
	};

	/** Set dragged beneficiry details to eApp object * */
	$scope.setBeneficiary = function($scope, currentlyDragged) {

		if (currentlyDragged == "myself") {
			$scope.FNAMyself.isBeneficiary = true;
			$scope.FNAMyself.ContactDetails.mobileNumber1 = $scope.FNAMyself.ContactDetails.homeNumber1;
			$scope.FNAMyself.fnaBenfPartyId = currentlyDragged;
		} else {
			for (var i = 0; i < $scope.beneficiaryData.length; i++) {
				if ($scope.beneficiaryData[i].id == currentlyDragged) {
					$scope.beneficiaryData[i].isBeneficiary = true;
					$scope.beneficiaryData[i].fnaBenfPartyId = currentlyDragged;
					if ($scope.beneficiaryData[i].ContactDetails
							&& $scope.beneficiaryData[i].ContactDetails.homeNumber1) {
						$scope.beneficiaryData[i].ContactDetails.mobileNumber1 = $scope.beneficiaryData[i].ContactDetails.homeNumber1;
					}
					if ($scope.FNAMyself.isInsured) {
						if (!$scope.beneficiaryData[i].CustomerRelationship) {
							$scope.beneficiaryData[i].CustomerRelationship = {};
						}
						$scope.beneficiaryData[i].CustomerRelationship.relationWithInsured = $scope.beneficiaryData[i].classNameAttached;
					}
					break;

				}

			}
		}
		$scope.proceedDisable = false;
		$scope.refresh();
	};
	


}]);