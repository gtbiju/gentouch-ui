/*
 * Copyright 2015, LifeEngage 
 */
storeApp.controller('MainController', ['$rootScope', '$scope', 'AuthService', '$location', 'DataService', '$window', '$filter', '$translate',
function($rootScope, $scope, AuthService, $location, DataService, $window, $filter, translate) {

	$rootScope.loadingMsg = '';
	$rootScope.page_loadr = false;
	$rootScope.majorVersion = rootConfig.majorVersion;
	$rootScope.minorVersion = rootConfig.minorVersion;
	$rootScope.msg = "Hi suspended msg";
	$rootScope.isAuthenticated = false;

	$scope.slider = [false, false, false, false, false, false, false];
	$scope.showSlide = false;
	$scope.slideTog = function(show) {
		$scope.showSlide = !show;
	};
	$scope.selectSlider = function(index) {
		for (var i = 0; i < $scope.slider.length; i++) {
			if (i != index) {
				$scope.slider[i] = false;
			} else {
				$scope.slider[i] = true;
			}
		}
		$scope.slideTog($scope.showSlide);
	};
	// Show hide the loading images across all ajax requests
	$rootScope.showHideLoadingImage = function(isShow, loadingMessage, $translate) {
		if (isShow) {
			$rootScope.page_loadr = true;
			$rootScope.loadingMsg = translateMessages($translate, loadingMessage);
		} else {
			$rootScope.loadingMsg = '';
			$rootScope.page_loadr = false;
		}

	};
/* 	var footer = $(".footer");
  footer.css({ "top": footer.position().top, "bottom": "auto"}); */
	$rootScope.notifyMsg = false;
	$rootScope.notifyerror = false;
	$rootScope.notifyMessage = '';

	$rootScope.HideNotifyMsg = function() {
		$rootScope.notifyMsg = false;
		$rootScope.notifyMessage = '';
		$scope.refresh();
	};
	$rootScope.NotifyMessages = function(isError, message, $translate, additionalInfo) {
		$rootScope.notifyMsg = true;
		$rootScope.notifyerror = isError;
		$rootScope.notifyMessage = translateMessages($translate, message, additionalInfo);
		setTimeout(function() {
			$rootScope.HideNotifyMsg();
		}, 3000);
	};
    
    Object.getPrototypeOf($rootScope).refresh = function (delay, callback) {
        var time;
        if (!delay) {
            time = 0;
        }
        $timeout(function () {
            if (callback) {
                callback();
            }
        }, time);
    };
    Object.getPrototypeOf($scope).refresh = function (delay, callback) {
        var time;
        if (!delay) {
            time = 0;
        }
        $timeout(function () {
            if (callback) {
                callback();
            }
        }, time);
    };

	$scope.convertertranslate = function(name) {

		return $filter('translate')(name);

	};

}]);
