
storeApp.controller('LandingController', ['$rootScope', '$scope', '$timeout', '$routeParams', '$route', '$location', 'DataService','$translate','FnaVariables', 'UtilityService', 'IllustratorVariables', 'PersistenceMapping', 'globalService', 'AgentService', 'DataWipeService', 'commonConfig', 
function($rootScope, $scope, $timeout,$routeParams, $route, $location, DataService,$translate,FnaVariables, UtilityService, IllustratorVariables, PersistenceMapping, globalService, AgentService, DataWipeService, commonConfig) {

     //$rootScope.showHideLoadingImage(true,'paintUIMessage', $translate);
    
	$scope.hideInDesktop = ((rootConfig.isDeviceMobile) && (/Android/i
			.test(navigator.userAgent))) ? false : true;// rootConfig.isDeviceMobile;
	
	if(rootConfig.isDeviceMobile && !rootConfig.hasContentAdmin) $rootScope.showHideLoadingImage(false);
	
	$scope.lmsvar = {
		"count" : 0,
		"mode" : "",
		"subModes" : {}
	};
	$scope.eAppvar = {
		"count" : 0,
		"mode" : "",
		"subModes" : {}
	};
	$scope.fnavar = {
		"count" : 0,
		"mode" : "",
		"subModes" : {}
	};
	$scope.illustrationvar = {
		"count" : 0,
		"mode" : "",
		"subModes" : {}
	};
	$scope.dayCount = 0;
	$scope.weekCount = 0;
	$scope.monthCount = 0;
    $scope.isSearched = false;
    $scope.zeroResult = false;
    $scope.isLoggedIn = false; 

    $scope.onRetrieveAgentProfileError = function() {
		$rootScope.lePopupCtrl.showError(translateMessages($translate,
			"lifeEngage"), translateMessages($translate,
			"general.agentProfileRetrieveError"), translateMessages($translate,
			"fna.ok"), $scope.okClick);
		$rootScope.showHideLoadingImage(false, "");
	};

	$scope.onRetrieveAgentProfileSuccess = function(data) {
		DataService.saveAgentProfile(data, $scope.loadLandingPage, $scope.onRetrieveAgentProfileError);
	};

	$scope.retrieveAgentProfile = function() {
		PersistenceMapping.clearTransactionKeys();
		var transactionObj = PersistenceMapping.mapScopeToPersistence({});
		AgentService.retrieveAgentProfile(transactionObj, $scope.onRetrieveAgentProfileSuccess, $scope.onRetrieveAgentProfileError);
	};
	
	$scope.contentDownloader = function() {

		// le Content Admin starts
		(function downloadContentFiles(mandatory, callAgain) {
			$scope.serviceURL = rootConfig.contentAdminURL;
			var options = {
				contentManagerServiceurl : $scope.serviceURL,
				debugMode : true,
				appContext : rootConfig.appContext
			};
			if (mandatory) {
				// alert("manadatory download");
				options.contentPreference = "mandatory";
			}
			// alert("Nonmandatory : "+options.contentPreference)
			var le = new LEContentManager(options);
			var options = {
				syncMode : "all",
				jsonFile : "initialContent.json",
				headers : {
					"nwsAuthToken" : "e03364fd-a8c6-7878-3d44-cff2a9d8e333"
				}
			};
			le
					.syncData(
							function(type, data) {
							},
							function(successFileList, errorFileList) {

								var message = "";
								if (successFileList.length > 0) {
									message = successFileList.length
											+ " files successfully downloaded. "
											+ JSON.stringify(successFileList)
											+ ". ";
									DataService.initializeDb(successFileList);
									if ((rootConfig.isDeviceMobile)
											&& !(rootConfig.isOfflineDesktop)) {
										for (i = 0; i < successFileList.length; i++) {
											if (successFileList[i]
													.indexOf(".db") >= 0
													&& successFileList[i] != "LifeEngageData.db") {
											
												window.sqlitePlugin
														.closeDatabase(
																successFileList[i],
																function() {
																	
																},
																function() {
																	
																});
											}
										}
									}
								}
								if (errorFileList.length > 0) {
									message = message + errorFileList.length
											+ " files failed to download. "
											+ JSON.stringify(errorFileList)
											+ ".";

									$scope.mandatoryDownloadFailed(mandatory);
								} else {

									if (callAgain) {
										/*
										 * Loading Landinpage after manadatory
										 * file download
										 */
										//$scope.loadLandingPage();
										/*
										 * Going to download Non mandatory
										 * content files
										 */
									    $scope.loadConfigfile(function(){$scope.retrieveAgentProfile()});
										downloadContentFiles("", false)
									}
									
								}

							}, function(message, data) {
								$scope.mandatoryDownloadFailed(mandatory);
								if (typeof message != undefined
										&& typeof data != undefined) {
								
								} else if (typeof message != undefined
										&& typeof data == undefined) {
								
								} else if (typeof message == undefined
										&& typeof data != undefined) {
									
								}

							}, options);

			$rootScope.showHideLoadingImage(false, "");
			// le Content Admin ends
		})("mandatory", true)
	};
	$scope.okClick = function() {
		$location.path('/login');
	};
	$scope.mandatoryDownloadFailed = function(mandatory) {
		if (mandatory == "mandatory") {
			$rootScope.showHideLoadingImage(false, "");//generali specific change.
			$rootScope.lePopupCtrl.showError(translateMessages($translate,
					"lifeEngage"), translateMessages($translate,
					"general.retryContentAdmin"), translateMessages($translate,
					"fna.ok"), $scope.okClick);
			// $rootScope.showMessagePopup(true, "error", translateMessages(
			// $translate, "general.retryContentAdmin"));
			// $location.path('/login');
			$rootScope.showHideLoadingImage(false, "");
			// $scope.cancel = function()
			// {$location.path('/login');$rootScope.alertObj.showPopup =
			// false;};
			
		}
	};
	$scope.loopRootConfig = function(successcallback){
var url = rootConfig[rootConfig.environment+'_URL'];
		//var url = eval ('rootConfig.' + rootConfig.environment
		//		+ '_URL');
		var urlkeys=Object.keys(url);
		var lastKey=urlkeys[(urlkeys.length)-1];

		for ( var key in url) {
			rootConfig[key] = url[key];
			if( key == lastKey){
				
				successcallback();
			}
		}
	};
    $scope.loadConfigfile = function(successCallBack) {
		var configUrl = null;
		var resourceUrl = null;
		if ((rootConfig.isDeviceMobile)) {
			var leFileUtils = new LEFileUtils();
			leFileUtils.getApplicationPath(function(documentsPath) {
				if ((rootConfig.isOfflineDesktop)) {
					configUrl = documentsPath + "appconfig.json";
					resourceUrl = documentsPath + "resources.json";
				} else {
					configUrl = documentsPath + "/" + "appconfig.json";
					resourceUrl = documentsPath + "/" + "resources.json";
				}
				$scope.getFromApplicationPath(configUrl, function(jsonObject) {
					//config = eval (jsonObject);
					if(rootConfig.isOfflineDesktop){
						config = JSON.parse(jsonObject.trim());
					} else {
						config = jsonObject;
					}
					$.extend(true, rootConfig, config);
					$rootScope.languages = rootConfig.languageList;
					// url selection(DEV, SIT, DEMO) based on environment
					// variable
					// merging appconfig.json and rootconfig.json
					$scope.loopRootConfig(function(){
						 $scope.getFromApplicationPath(resourceUrl,
		                        function(jsonObject) {
		                           // resources = eval (jsonObject);
								   	if(rootConfig.isOfflineDesktop){
										resources = JSON.parse(jsonObject);
                                    } else {
                                        resources = jsonObject;
                                    }
		                            $translate.refresh();
		                            //Calling landing page after config and resource files loading
		                            $timeout(function () {
						successCallBack();
					    }, 200);
		                });
					});
				});
                
               
               
                //Call should be made after completing above all.
             /*   if (!rootConfig.hasContentAdmin || !(rootConfig.isDeviceMobile)) {
                    $scope.loadLandingPage();
                }*/
            //});
			});
		} else {
			$scope.getFromRemote("appconfig.json", false,
					function(jsonObject) {
						//config = eval (jsonObject);
						config = JSON.parse(jsonObject);
						/*if(rootConfig.isOfflineDesktop){
							config = JSON.parse(jsonObject.trim());
						} else {
								config = jsonObject;
						}*/
						$.extend(true, rootConfig, config);
						$rootScope.languages = rootConfig.languageList;
						// url selection(DEV, SIT, DEMO) based on environment
						// variable
						// merging appconfig.json and rootconfig.json
						var url = rootConfig[rootConfig.environment+'_URL'];
						//var url = eval ('rootConfig.' + rootConfig.environment
							//	+ '_URL');
						for ( var key in url) {
							rootConfig[key] = url[key];
						}
					});
			$scope.getFromRemote("resources.json", false, function(jsonObject) {
				//resources = eval (jsonObject);
				//resources = JSON.parse(jsonObject);
					/*if(rootConfig.isOfflineDesktop){
										resources = JSON.parse(jsonObject);
                                    } else {
                                        resources = jsonObject;
                                    }*/
				
                 $translate.refresh();
				//Calling landing page after config and resource files loading
				successCallBack();
			});
	         
	            //Call should be made after completingt above all.
	           /* if (!rootConfig.hasContentAdmin || !(rootConfig.isDeviceMobile)) {
	                $scope.loadLandingPage();
	            }*/
		}
	};
	$scope.getFromRemote = function(jsonFile, callmode, successCallBack,
			errorCallback) {
		var self = this;
		$.ajax({
			type : "GET",
			url : rootConfig.contentAdminDownLoadURL + jsonFile,
			dataType : "text",
			success : function(jsonObject) {
				successCallBack(jsonObject);

			},
			async : callmode,
			error : function(error) {
				self._prepareErrorCallBack('Error_106',
						'Error while fetching config JSON from remote location . '
								+ JSON.stringify(error), errorCallback);
			}
		});
	};
	$scope.mapKeysforPersistence = function() {

		if (!(rootConfig.isDeviceMobile)) {
					PersistenceMapping.creationDate = UtilityService
							.getFormattedDateTime();
		}
		PersistenceMapping.dataIdentifyFlag = false;

	}
	$scope.loadLandingPage = function() {
		$rootScope.showHideLoadingImage(false, "");
		$rootScope.moduleVariable = translateMessages($translate,
				"fna.landingPageHeading");
        if (rootConfig.isPrecomiledTemplate) {
				LEDynamicUI.loadPrecompiledTemplate("preCompiled.json" ,function(){
					if (typeof rootConfig != "undefined" && rootConfig.isDebug) {
						console.log("preCompiledTemplate loaded successfully");
					}
				},function(err){
					if (typeof rootConfig != "undefined" && rootConfig.isDebug) {
						console.log("preCompiledTemplate_lifeengage load failed  : " + err);
					}
				});
        }
		UtilityService.previousPage = '';
		// To set the globalservice variables to null
		var parties = [];
		globalService.setParties(parties);
		var product = {
			"ProductDetails" : {},
			"policyDetails" : {}
		};
		globalService.setProduct(product);
		IllustratorVariables.illustratorId = "0";
		IllustratorVariables.setIllustratorModel(null);
		IllustratorVariables.setIllustrationAttachmentModel(null);
		if (!(rootConfig.isDeviceMobile)) {
			isSync = false;
			PersistenceMapping.clearTransactionKeys();
			$scope.mapKeysforPersistence();
			var searchCriteria = {
				searchCriteriaRequest : {
					"command" : "LandingDashBoardCount",
					"modes" : rootConfig.modules,
					"value" : ""
				}
			};

			var transactionObj = PersistenceMapping
					.mapScopeToPersistence(searchCriteria);
			delete transactionObj.Id;
			transactionObj.filter = "Key15  <> 'Cancelled'";
			DataService.retrieveByFilterCount(transactionObj,
					$scope.onGetListingsSuccess, $scope.onGetListingsError);
		} else {

			PersistenceMapping.clearTransactionKeys();
			PersistenceMapping.dataIdentifyFlag = false;
			$scope.mapKeysforPersistence();
			var searchCriteria = {
				searchCriteriaRequest : {
					"command" : "LandingDashBoardCount",
					"modes" : rootConfig.modules,
					"value" : ""
				}
			};
			var transactionObj = PersistenceMapping
					.mapScopeToPersistence(searchCriteria);
			delete transactionObj.Id;
			$scope.searchValue = "";
			transactionObj.filter = "Key15  <> 'Cancelled'";
			DataService.retrieveByFilterCount(transactionObj,
					$scope.onGetListingsSuccess, $scope.onGetListingsError);
		}
		/* Commenting since it is needed for Generali implementation*/
		/*if (!$scope.hideInDesktop) {
			PersistenceMapping.clearTransactionKeys();
			var transactionObj = PersistenceMapping.mapScopeToPersistence({});
			transactionObj.Id = $routeParams.transactionId;
			DataService.getLMSCount(transactionObj, $scope.onGetCountSuccess,
					$scope.onGetCountError);
		}*/

	};

	$scope.SearchHome = function(searchlead) {
		PersistenceMapping.clearTransactionKeys();
		if ((rootConfig.isDeviceMobile)) {
			PersistenceMapping.dataIdentifyFlag = false;
		}
		$scope.mapKeysforPersistence();
		if (searchlead !== undefined && searchlead !== "") {
			var searchCriteria = {
				searchCriteriaRequest : {
					"command" : "LandingDashBoardCount",
					"modes" : rootConfig.modules,
					"value" : searchlead
				}
			};
			var transactionObj = PersistenceMapping
					.mapScopeToPersistence(searchCriteria);
			transactionObj.filter = "Key15  <> 'Cancelled'";
			DataService.retrieveByFilterCount(transactionObj,
					$scope.onGetSearchListingsSuccess,
					$scope.onGetListingsError);
		}
	};

	$scope.onGetSearchListingsSuccess = function(data) {
		$scope.lmsvar = {
			"count" : 0,
			"mode" : "",
			"subModes" : {}
		};
		$scope.eAppvar = {
			"count" : 0,
			"mode" : "",
			"subModes" : {}
		};
		$scope.fnavar = {
			"count" : 0,
			"mode" : "",
			"subModes" : {}
		};
		$scope.illustrationvar = {
			"count" : 0,
			"mode" : "",
			"subModes" : {}
		};
		if (!(rootConfig.isDeviceMobile)) {
			data = data[0].TransactionData.SearchCriteriaResponse;
		}
		$scope.summary = data;
		if ($scope.summary.length != 0) {
			for (var i = 0; i < $scope.summary.length; i++) {
				if ($scope.summary[i].mode === 'eApp') {
					$scope.eAppvar = $scope.summary[i];
				} else if ($scope.summary[i].mode === 'LMS') {
					$scope.lmsvar = $scope.summary[i];
				} else if ($scope.summary[i].mode === 'FNA') {
					$scope.fnavar = $scope.summary[i];
				} else if ($scope.summary[i].mode === 'illustration') {
					$scope.illustrationvar = $scope.summary[i];
				} else {
					$scope.eAppvar.count = 0;
					$scope.lmsvar.count = 0;
					$scope.fnavar.count = 0;
					$scope.illustrationvar.count = 0;
				}

			}
            $scope.consolidatedCount = parseInt($scope.eAppvar.count) + parseInt($scope.lmsvar.count) + parseInt($scope.fnavar.count) + parseInt($scope.illustrationvar.count);
            $scope.isSearched = true;
            $scope.zeroResult = false;
		}
        if($scope.summary.length == 0){
            $scope.zeroResult = true;
            $scope.isSearched = false;
        }
		$scope.refresh();
	};
	// Get eApp details Error
	$scope.onGetListingsError = function(data) {
		$rootScope.NotifyMessages(true, resources.onGetListingsError);
		$rootScope.showHideLoadingImage(false);
		$scope.refresh();
	};

	$scope.onGetListingsSuccess = function(data) {
		if (!(rootConfig.isDeviceMobile)) {
			data = data[0].TransactionData.SearchCriteriaResponse;

		}
		$scope.summary = data;
		if ($scope.summary.length != 0) {
			for (var i = 0; i < $scope.summary.length; i++) {
				if ($scope.summary[i].mode === 'eApp') {
					$scope.eAppvar = $scope.summary[i];
				} else if ($scope.summary[i].mode === 'LMS') {
					$scope.lmsvar = $scope.summary[i];
				} else if ($scope.summary[i].mode === 'FNA') {
					$scope.fnavar = $scope.summary[i];
				} else if ($scope.summary[i].mode === 'illustration') {
					$scope.illustrationvar = $scope.summary[i];
				} else {
					$scope.eAppvar.count = 0;
					$scope.lmsvar.count = 0;
					$scope.fnavar.count = 0;
					$scope.illustrationvar = 0;
				}

			}
		}
		$scope.refresh();
		$rootScope.showHideLoadingImage(false);
	};

	$scope.List = function(app, status, searchvalue) {
		 if(typeof status== "undefined" || status == ""){status="status"}
		switch (app) {
		case "lead":
			$rootScope.searchValue = searchvalue;
			$location.path("/lms");
			break;
		case "fna":
			$rootScope.searchValue = searchvalue;
			$location.path("/fnaListing/" + status + "");
			break;
		case "illustration":
			$rootScope.searchValue = searchvalue;
			$location.path("/MyAccount/Illustration/0/0");
			break;
		case "eApp":
			$rootScope.searchValue = searchvalue;
			$location.path("/MyAccount/eApp/0/0");
			break;
		default:
			break;
		}

	};
	$scope.createNew = function(app) {
		switch (app) {
		case "Lead":
			$location.path("/lms");
			break;
		case "FNA":
			$rootScope.transactionId = 0;
			FnaVariables.clearFnaVariables();
			$location.path("/fnaLandingPage/0/0");
			break;
		case "illustration":
			$rootScope.transactionId = 0;
			IllustratorVariables.clearIllustrationVariables();
			$location.path("/products/0/0/0/0/false");
			break;
		case "eApp":
			$location.path("/Eapp/0/0/0/0");
			break;
		default:
			break;
		}
	};
	$scope.getFromApplicationPath = function(jsonFile,
									successCallBack, errorCallback) {
		var self = this;
								self._prepareErrorCallBack = function(
										errorCode, errorMessage) {
									if(rootConfig.isDebug){
										console.log(errorMessage);
		}
								};
		if (rootConfig && rootConfig.isOfflineDesktop) {
			var fs = require('fs');
									fs.readFile(jsonFile, 'utf8', function(err,
											data) {
				if (err) {
											self._prepareErrorCallBack(
													'Error_106',
							'Error while fetching config JSON from remote location'
															+ jsonFile,
													errorCallback);
				}
				successCallBack(data);
			});
		} else {
									url:
											rootConfig.contentAdminDownLoadURL
													+ jsonFile,
											$
													.getJSON(jsonFile, function(data){ //alert(data)
														
													})
													.done(
															function(
																	jsonObject,
																	textStatus) {
				successCallBack(jsonObject);
			})
					.fail(
															function(jqxhr,
																	settings,
																	exception) {
																self
																		._prepareErrorCallBack(
																				'Error_106',
										'Error while fetching config JSON from remote location . '
																						+ JSON
																								.stringify(error),
										errorCallback);
							});

		}
	};
	
	//To be committed in LE
	$scope.initialLoad = function(){
	if (localStorage["prevPath"].indexOf("/login") > 0) {
		
		
       /* if((rootConfig.isDeviceMobile) && rootConfig.hasContentAdmin) {
            if(checkConnection()) {
                $rootScope.showHideLoadingImage(true, translateMessages($translate,
                "general.downloadingMandatory"));
                $scope.contentDownloader();
            } else {
                $scope.loadConfigfile(function() {$scope.loadLandingPage()});
            }
        } else {//Web , contentadmin false of device case - need to merge config files
            $scope.loadConfigfile(function() {$scope.loadLandingPage()});
        }*/
        
        if((rootConfig.isDeviceMobile) && rootConfig.hasContentAdmin && checkConnection()) {
            $rootScope.showHideLoadingImage(true, translateMessages($translate,
            "general.downloadingMandatory"));
            $scope.contentDownloader();	
    } else if((rootConfig.isDeviceMobile) && !(rootConfig.hasContentAdmin) && checkConnection()){
		$scope.loadConfigfile(function(){$scope.retrieveAgentProfile()});
	} else {//Web , contentadmin false of device case - need to merge config files
	
        $scope.loadConfigfile(function() {$scope.loadLandingPage()});
	}
        
    } else {
        $scope.loadLandingPage();
    }
	}
	
   //To be committed in LE
	$scope.$on('$viewContentLoaded', function(){
		$scope.initialLoad();
	  });

	$scope.onGetCountSuccess = function(count) {
		$scope.dayCount = count.day;
		$scope.weekCount = count.week;
		$scope.monthCount = count.month;
		$scope.totalCount = $scope.dayCount + $scope.weekCount
				+ $scope.monthCount;
		$scope.refresh();
		// $route.reload();
	};

	$scope.onGetCountError = function() {
	};

	$scope.resetFilter = function() {
		$scope.searchValue = "";
		$scope.loadLandingPage();
	};

}]);