﻿﻿﻿﻿'use strict';

storeApp.controller('MyAccountController', ['$rootScope', '$scope', '$compile', '$routeParams', 'DataService', 'LookupService', 'DocumentService', '$translate', 'UtilityService', 'IllustratorVariables', 'EappVariables', '$location', 'PersistenceMapping', 'FnaVariables', 'globalService', 'IllustratorService', '$route',
function($rootScope, $scope, $compile, $routeParams, DataService, LookupService, DocumentService, $translate, UtilityService, IllustratorVariables, EappVariables, $location, PersistenceMapping, FnaVariables, globalService, IllustratorService, $timeout, $route) {
	
	IllustratorVariables.setIllustratorModel(null);
	IllustratorVariables.setIllustrationAttachmentModel(null);
	EappVariables.clearEappVariables();
	var syncWarning = false;
	$scope.tblhead1 = false;
	$scope.tblhead2 = false;
	$scope.tblhead3 = false;
	$scope.tblhead4 = false;
	$scope.tblhead5 = false;
	$scope.tblhead6 = false;
	$scope.tblhead7 = false;
	$scope.dvDeleteConfirm = false;
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.selectedProposal = [];
	$rootScope.transactionId = '';
	$scope.insuredNameFilter="";
	$scope.proposerNameFilter="";
	$scope.productNameFilter="";
	$scope.productTypeFilter="";
	$scope.statusFilter="";
	$scope.filterOpenIllustration = false;
	$scope.filterOpenEapp = false;
	
	$scope.emailpopup = false;
	$scope.emailToId=="";
	$scope.emailccId=="";
	$scope.emailfromId = rootConfig.emailFromId;
	$scope.illustartionsToMail =[];	
	
	IllustratorVariables.leadId = $routeParams.leadId;
	IllustratorVariables.fnaId =  $routeParams.fnaId;
	// To set the globalservice variables to null
	if(IllustratorVariables.leadId==0 && IllustratorVariables.fnaId==0 ){
	var parties = [];
	globalService.setParties(parties);
	var product = {
		"ProductDetails" : {},
		"policyDetails" : {}
	};
	globalService.setProduct(product);
	}
	
	$scope.toggleFilterIllustration = function(){
		$scope.filterOpenIllustration = !($scope.filterOpenIllustration);
		if($scope.filterOpenIllustration == false){
		$scope.resetFilter();
	}
	}
	$scope.toggleFilterEapp = function(){
		$scope.filterOpenEapp = !($scope.filterOpenEapp);
		if($scope.filterOpenEapp == false){
		$scope.resetFilter();
	}
	}
	$scope.resetFilter= function() {
		$scope.insuredNameFilter="";
		$scope.proposerNameFilter="";
		$scope.productNameFilter="";
	    $scope.productTypeFilter="";
		$scope.statusFilter="";
	};
	

	$scope.statusOptionsIllustration = IllustratorVariables.getStatusOptions();
	$scope.statusOptionsEapp = EappVariables.getStatusOptionsEapp();
	
	IllustratorVariables.illustratorId = "0";
	$scope.tabClick = function(tab) {
		switch (tab) {
			case 'myPolicies':
				$scope.myPolicies = "active";
				$scope.myIllustrations = "";
				$scope.myApplications = "";
				break;
			case 'myIllustrations':
                $rootScope.enableRefresh=false;
				$scope.myPolicies = "";
				$scope.myIllustrations = "active";
				$scope.myApplications = "";
				$rootScope.moduleHeader = "illustrator.Illustrator";
				$rootScope.moduleVariable = translateMessages($translate, $rootScope.moduleHeader);
				break;
			case 'myApplications':
                $rootScope.enableRefresh=true;
                if(rootConfig.isDeviceMobile){
                    $rootScope.displayRefreshForWeb=false;    
                }else{
                    $rootScope.displayRefreshForWeb=true;
                }
				$scope.myPolicies = "";
				$scope.myIllustrations = "";
				$scope.myApplications = "active";
				$rootScope.moduleHeader = "eApplication";
				$rootScope.moduleVariable = translateMessages($translate, $rootScope.moduleHeader);
				break;
			default:
				
		}
	}
	if ($routeParams.type == "Illustration") {
		$scope.tabClick('myIllustrations');
	} else if ($routeParams.type == "eApp") {
		UtilityService.previousPage = '';
		$scope.tabClick('myApplications');
	}

	// Show Coose Parties screen only for device.Not in
	// web
	/*
	 * if ((rootConfig.isDeviceMobile)) { $scope.nextPageToProceed =
	 * "default.htm#/fnaChooseParties/0"; }else{ UtilityService.previousPage =
	 * 'Illustration'; $scope.nextPageToProceed = "default.htm#/Eapp/0/0/0/0"; }
	 */
	$scope.mapKeysforPersistence = function() {
		PersistenceMapping.Key4 = ($routeParams.proposalId != null && $routeParams.proposalId != 0) ? $routeParams.proposalId : "";
		PersistenceMapping.Key5 = $scope.productId != null ? $scope.productId : "1";
		if (isSync) {
			PersistenceMapping.Key10 = "FullDetails";
		} else {
			PersistenceMapping.Key10 = "BasicDetails";
		}
		if (!(rootConfig.isDeviceMobile)) {
									PersistenceMapping.creationDate = UtilityService
											.getFormattedDateTime();
		}
		PersistenceMapping.Key15 = $scope.status != null ? $scope.status : "Saved";
		PersistenceMapping.Type = nextSyncType;
		PersistenceMapping.dataIdentifyFlag = false;
	}

	$scope.orderValues = function(key, e) {
		// if ($(e.target).hasClass("sorter_arrow_dwn"))
		// {
		// $(e.target).toggleClass("sorter_arrow_dwn
		// sorter_arrow_up");
		// } else {
		// $(e.target).toggleClass("sorter_arrow_up
		// sorter_arrow_dwn");
		// }
		$scope.predicate = key;
		$scope.reverse = !($scope.reverse);
	}

	$scope.proceedToeApp = function(illustrationObject) {
		var updatedDate = new Date();
		var formattedDate = Number(updatedDate.getMonth() + 1) + "-" + Number(updatedDate.getDate() + 5) + "-" + updatedDate.getFullYear();
		DataService.getListingDetail(illustrationObject, $scope.onGetListingDetailSuccess, $scope.onGetListingDetailError);

	}
	$scope.updateMinWidthSetting = function(filteredLength) {

		if (filteredLength > 8) {
			$scope.minWidthSetting = "min_width_th";
		} else {
			$scope.minWidthSetting = "";
		}
	}

	$scope.prepopulateeAppData = function(illustrationTransId) {
		UtilityService.previousPage = 'Illustration';
		// Overwrite insured,payer and product details
		// of FNA party with illustration party since
		// illustartion has more fields. Also, there is
		// a chance that insured and proposer are
		// updated in illustration -- Begin
		// An FNA can have multiple illustrations. Hence
		// the insured,proposer and beneficairy selected
		// should be populated from illustration rather
		// than FNA(this will have last created
		// illustraion details)
		// var insured = globalService.getInsured();
		var insured = $scope.Illustration.Insured;
		if (!insured.IncomeDetails) {
			insured.IncomeDetails = {};
			insured.IncomeDetails.annualIncome = "";
		}
		var beneficiaries = $scope.Illustration.Beneficiaries;
		if (!insured.ContactDetails) {
			insured.ContactDetails = {};
		}
		if (!insured.ContactDetails.currentAddress) {
			insured.ContactDetails.currentAddress = {};
		}
		insured.ContactDetails.currentAddress.country = insured.BasicDetails.countryofResidence;
		insured.ContactDetails.currentAddress.state = insured.BasicDetails.state;
		if (insured.CustomerRelationship && insured.CustomerRelationship.isPayorDifferentFromInsured) {
			if (insured.CustomerRelationship.isPayorDifferentFromInsured == "Yes") {
				insured.CustomerRelationship.relationshipWithProposer = "Self";
			} else if (insured.CustomerRelationship.isPayorDifferentFromInsured == "No") {
				insured.CustomerRelationship.relationshipWithProposer = "Others";
			}
		}
		// var payer = globalService.getPayer();
		var payer = $scope.Illustration.Payer;
		if (!payer.ContactDetails) {
			payer.ContactDetails = {};
		}
		if (!payer.ContactDetails.currentAddress) {
			payer.ContactDetails.currentAddress = {};
		}
		payer.ContactDetails.currentAddress.country = $scope.Illustration.Payer.BasicDetails.countryofResidence;
		payer.ContactDetails.currentAddress.state = $scope.Illustration.Payer.BasicDetails.state;
		payer.ContactDetails.currentAddress.zipCode = $scope.Illustration.Payer.BasicDetails.zipCode;
		// To DO - change the illustartion flag - Toggle
		// isPayorDifferentFromInsured since
		// isPayorDifferentFromInsured flag is used
		// differently for business logic implmtn in
		// eapp and illustration
		if ($scope.Illustration.Payer.CustomerRelationship && $scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == "Yes") {
			payer.CustomerRelationship.isPayorDifferentFromInsured = "No";
			//payer.CustomerRelationship.relationshipWithProposer = "Self";
			if ($scope.fnaId == "" || $scope.fnaId == null) {
				insured.CustomerRelationship.relationshipWithProposer = "Self";
			}
		} else if ($scope.Illustration.Payer.CustomerRelationship && $scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == "No") {
			payer.CustomerRelationship.isPayorDifferentFromInsured = "Yes";
			payer.CustomerRelationship.relationshipWithProposer = "Others";
			if ($scope.fnaId == "" || $scope.fnaId == null) {
				insured.CustomerRelationship.relationshipWithProposer = "Others";
			}
		}
		// An FNA can have multiple illustration. Hence
		// the insured,proposer & beneficairy selected
		// should be populated from illustration
		// To set the images from fna parties so as to
		// display in Chooseparties screen
		if ($scope.fnaId != "" && $scope.fnaId != null) {
			globalService.setFNADefaultParties();
			globalService.setInsured(insured);
			globalService.setPayer(payer);
			globalService.setBenfFromIllustrn(beneficiaries);
		} else {
			var parties = [];
			globalService.setParties(parties);
			globalService.setPartyFromillustrtn(insured, true);
			globalService.setPartyFromillustrtn(payer, false);
		}

		var product = $scope.Illustration.Product;
		// Set the values of extra product fields in
		// eApp model
		// To change after confirmation got on the value
		// of sum assured.
		if ($scope.Illustration.IllustrationOutput.PolicyDetails && $scope.Illustration.IllustrationOutput.PolicyDetails.productCode == 24) {
			product.ProductDetails.sumAssured = 65000;
		} else {
			product.ProductDetails.sumAssured = $scope.Illustration.IllustrationOutput.sumAssured;
		}
		product.ProductDetails.initialPremium = $scope.Illustration.Product.policyDetails.committedPremium;
		product.ProductDetails.premiumTerm = $scope.Illustration.Product.policyDetails.premiumPayingTerm;
		product.ProductDetails.policyTerm = $scope.Illustration.Product.policyDetails.policyTerm;
		if ($scope.Illustration.Product.RiderDetails && $scope.Illustration.Product.RiderDetails.length > 0) {
			product.ProductDetails.rider = "Yes";
		} else {
			product.ProductDetails.rider = "No";
		}
		globalService.setProduct(product);
		// Overwrite insured, payer and product details
		// of FNA party with illustration party since
		// illustartion has more fields. Also, there is
		// a chance that insured and proposer are
		// updated in illustration -- End

		// On creating a new illustration from Landing
		// page(Clicking Create New illsutartion
		// button), fnaid won't be there, for which we
		// dont have to display chooseparties screen
		// Even from web - If has fna - go to
		// chooseparties rather than Eapp
		if ($scope.fnaId != "" && $scope.fnaId != null) {
			$location.path('/fnaChooseParties/' +illustrationTransId+"/"+product.id );
			$scope.refresh();
		} else {
			EappVariables.fnaId = IllustratorVariables.fnaId;
			EappVariables.leadId =IllustratorVariables.leadId;
			EappVariables.illustratorId =IllustratorVariables.illustratorId;
			$location.path('/Eapp/0/0/' + illustrationTransId + '/0');
			$scope.refresh();
		}

	}

	$scope.onGetListingDetailSuccess = function(data) {
		IllustratorVariables.setIllustratorModel(data[0].TransactionData);
		$scope.Illustration = IllustratorVariables.getIllustratorModel();
		$scope.fnaId = data[0].Key2;
		FnaVariables.fnaId = 0;
		IllustratorVariables.leadId=data[0].Key1;
		IllustratorVariables.fnaId=data[0].Key2;
		IllustratorVariables.illustratorId = data[0].TransTrackingID;
		$scope.IllustrationId = (IllustratorVariables.illustratorId != "" && IllustratorVariables.illustratorId != 0) ? IllustratorVariables.illustratorId : data[0].Id;

		if ($scope.fnaId != "" && $scope.fnaId != null) {// Get
			// FNA
			// details

			// On creating a new illustration from
			// Landing page(Clicking Create New
			// illsutartion button), fnaid won't be
			// there, for which we dont have to display
			// chooseparties screen
			if ((rootConfig.isDeviceMobile)) {// Get
				// the
				// related
				// FNA
				// from
				// db
				DataService.getRelatedFNA($scope.fnaId, function(fnaData) {
					if (fnaData && fnaData[0] && fnaData[0].TransactionData) {
						FnaVariables.setFnaModel({
							'FNA' : JSON.parse(JSON.stringify(fnaData[0].TransactionData))
						});
						$scope.FNAObject = FnaVariables.getFnaModel();
						globalService.setParties($scope.FNAObject.FNA.parties);
					}
					$scope.prepopulateeAppData($scope.IllustrationId);
				}, $scope.onGetListingsError);
			} else {

				PersistenceMapping.clearTransactionKeys();
				PersistenceMapping.Type = "FNA";
				var transactionObj = PersistenceMapping.mapScopeToPersistence({});
				transactionObj.TransTrackingID = $scope.fnaId;

				DataService.getListingDetail(transactionObj, function(fnaData) {
					FnaVariables.setFnaModel({
						'FNA' : JSON.parse(JSON.stringify(fnaData[0].TransactionData))
					});
					$scope.FNAObject = FnaVariables.getFnaModel();
					if (fnaData[0].TransactionData.parties) {
						var parties = fnaData[0].TransactionData.parties;
						globalService.setParties(parties);
					}
					$scope.prepopulateeAppData($scope.IllustrationId);
				}, $scope.onGetListingsError);

			}
		} else {
			$scope.prepopulateeAppData($scope.IllustrationId);
		}

	}
	$scope.onGetListingDetailError = function() {
		alert('error');
	}
	var isSync = false;
	var nextSyncType;
	$scope.titles = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];

	$scope.startsWith = function(state, viewValue) {
		return state.substr(0, viewValue.length).toLowerCase() == viewValue.toLowerCase();
	}
	$scope.clientId = 'Agent1';//to be committed in LE


	$scope.Proposals = [];
	$scope.Illustartions = [];
	$scope.onGetListingsSuccess = function(data) {
		if ((rootConfig.isDeviceMobile)) {
			$.each(data, function(i, obj) {
				obj.TransactionData.ProposalStatus = obj.Key15;
			});
		}
		for(var i=0;i<data.length;i++){
			if(data[i].Key4 == "" || typeof data[i].Key4 == "undefined"){
			data[i].Key4 = "-";
			}
			if(data[i].Id == "" || typeof data[i].Id == "undefined"){
			data[i].Id = "-";
			}
			}
		$scope.Proposals = data;
		$scope.tableProposals =  [].concat($scope.Proposals);
		$scope.refresh();
	}
	$scope.onGeteAppListingSuccess = function(data) {
		
		//commented for eapp listing screen breaking issue in IOS
		/*if ((rootConfig.isDeviceMobile)) {
			$.each(data, function(i, obj) {
				obj.TransactionData.ProposalStatus = obj.Key15;
			});
		}*/
		for(var i=0;i<data.length;i++){
			if(data[i].Key4 == "" || typeof data[i].Key4 == "undefined"){
			data[i].Key4 = "-";
			}
			if(data[i].Id == "" || typeof data[i].Id == "undefined"){
			data[i].Id = "-";
			}
			}
			if (!rootConfig.isDeviceMobile) {
				$scope.Proposals = data.sort(function(a, b) {
					return LEDate(b.modifiedDate)
						- LEDate(a.modifiedDate);
				});
			} else {
					$scope.Proposals = data;
			}
		
		$scope.tableProposals =  [].concat($scope.Proposals);
		$rootScope.showHideLoadingImage(false);
		$scope.refresh();
	}
	$scope.onGetIllustrationSuccessForListing = function(data) {
		if ((rootConfig.isDeviceMobile)) {
			$.each(data, function(i, obj) {
				obj.TransactionData.IllustrationStatus = obj.Key15;
			});
		}
		if(IllustratorVariables.leadId!=0){// Filtering for Lead based BI 
		
			for (var i=0;i<data.length;i++){
			if(data[i].Key1 == IllustratorVariables.leadId){
				$scope.Illustartions.push(data[i]);
			}
		}
		
		}else if(IllustratorVariables.fnaId!=0){
		
			for (var i=0;i<data.length;i++){
				if(data[i].Key2 == IllustratorVariables.fnaId){
					$scope.Illustartions.push(data[i]);
				}
			}
			$scope.tableIllustrations = [].concat($scope.Illustartions);
			
	    }else{	
			
		$scope.Illustartions = data;
		$scope.tableIllustrations = [].concat($scope.Illustartions);
		
		}
		if (!rootConfig.isDeviceMobile) {
			$scope.Illustartions.sort(function(a, b) {
				return LEDate(b.modifiedDate)- LEDate(a.modifiedDate);
			});
		}
		$rootScope.showHideLoadingImage(false);
		$scope.refresh();
	}
	$scope.onGetIllustrationListingsSuccess = function(data) {
		if ((rootConfig.isDeviceMobile)) {
			$.each(data, function(i, obj) {
				obj.TransactionData.IllustrationStatus = obj.Key15;
			});
		}
		$scope.Illustartions = data;
		$scope.tableIllustrations = [].concat($scope.Illustartions);
		$scope.refresh();
	}
	$scope.onGetFnaListingsSuccess = function(data) {
		/*
		 * if ((rootConfig.isDeviceMobile)) { $.each(data, function (i, obj) {
		 * obj.TransactionData.fnaStatus = obj.Key7; }); } $scope.FNAObject.FNA =
		 * data;
		 */
		$rootScope.showHideLoadingImage(false);
		$scope.refresh();
	}
	$scope.onGetListingsError = function(data) {
		$scope.Proposals = data;
		$scope.tableProposals =  [].concat($scope.Proposals);
		$rootScope.showHideLoadingImage(false);
		$scope.refresh();
	}
	$scope.toggleSelection = function toggleSelection(proposal) {
		var idx = $scope.selectedProposal.indexOf(proposal);

		// is currently selected
		if (idx > -1) {
			$scope.selectedProposal.splice(idx, 1);
			
		}

		// is newly selected
		else {
			$scope.selectedProposal.push(proposal);
			
			
		}
	};
	$scope.deletePopup = function(type) {
		$scope.deleteType = type;
		PersistenceMapping.clearTransactionKeys();
		$scope.mapKeysforPersistence();
		if (PersistenceMapping.mapScopeToPersistence(JSON.stringify($scope.selectedProposal[0].TransactionData))) {
			// if($scope.mapScopeToPersistance(JSON.stringify($scope.selectedProposal[0].TransactionData))){
			// displayOverlay('dvDeleteConfirm');
			$scope.dvDeleteConfirm = true;
		}

	}

	$scope.removePagePopup = function() {
		$scope.dvDeleteConfirm = false;
		// hideOverlay();
		// $rootScope.showHideLoadingImage(true,
		// 'deleteMessage', $translate);
	}

	$scope.cancel = function() {
		$scope.dvDeleteConfirm = false;
		// hideOverlay();

	}
    
    $scope.syncError = function(statusMsg,errorMsg) {
        
        if(statusMsg == 'SyncError')
        {
		$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate,errorMsg),translateMessages($translate, "general.ok"),function(){
													$scope.exitApp = true;
													});
        }
	}

	$scope.deleteTransactions = function() {
		PersistenceMapping.clearTransactionKeys();
		$scope.mapKeysforPersistence();
		var transactionObjDeleted = {};
		if ((rootConfig.isDeviceMobile)) {
			transactionObjDeleted.Id = $scope.selectedProposal[0].Id;
		} else {
			transactionObjDeleted = PersistenceMapping.mapScopeToPersistence();
			
		for(var i in $scope.selectedProposal){
			  $scope.selectedProposal[i].Key15="Cancelled"; }
			 
			transactionObjDeleted = $scope.selectedProposal;
		}

		// var transactionObjDeleted =
		// $scope.mapScopeToPersistance(JSON.stringify($scope.selectedProposal[0].TransactionData));
		if ((rootConfig.isDeviceMobile)) {
			transactionObjDeleted.Id = $scope.selectedProposal[0].Id;
		}
		// transactionObjDeleted.TransactionData.Key7 =
		// "Cancelled";
		transactionObjDeleted.Key1 = "";
		transactionObjDeleted.Key15 = "Cancelled";
		if (transactionObjDeleted.Type == "eApp") {
			transactionObjDeleted.Key4 = $scope.selectedProposal[0].Key4;
		}
		if (transactionObjDeleted.Type == "illustration") {
			transactionObjDeleted.Key3 = $scope.selectedProposal[0].Key3;
		}
		if (transactionObjDeleted.Type == "FNA") {
			transactionObjDeleted.Key2 = $scope.selectedProposal[0].Key2;
		}
		$rootScope.showHideLoadingImage(true, 'deleteMessage', $translate);
		if ((rootConfig.isDeviceMobile)) {
			DataService.saveTransactions(transactionObjDeleted, $scope.ondelteSuccessCallBack, $scope.ondelteError);
		} else {
			DataService.deleteTransactions(transactionObjDeleted, $scope.ondelteSuccessCallBack, $scope.ondelteError);
		}

		$scope.removePagePopup();

	}

	$scope.ondelteSuccessCallBack = function() {
		if ((rootConfig.isDeviceMobile)) {
			$scope.selectedProposal.splice(0, 1);

			if ($scope.selectedProposal.length > 0) {

				$scope.deleteTransactions();
			} else {

				$rootScope.showHideLoadingImage(true, 'deleteMessage', $translate);
				// var transactionObj =
				// $scope.mapScopeToPersistance({});
				// transactionObj.Type = "eApp";
				var transactionObj = {};

				PersistenceMapping.clearTransactionKeys();
				$scope.mapKeysforPersistence();
				transactionObj = PersistenceMapping.mapScopeToPersistence({});
				// transactionObj =
				// $scope.mapScopeToPersistance({});
				// transactionObj.Id
				// =$routeParams.transactionId;
				transactionObj.Type = $scope.deleteType;
				transactionObj.Key4 = "";
				transactionObj.Id = "";
				transactionObj.Key10 = "BasicDetails";
				transactionObj.filter = "Key15  <> 'Cancelled'";
				if (transactionObj.Type == 'eApp') {
					DataService.getDetailsForListing(transactionObj, function(data) {
						if ((rootConfig.isDeviceMobile)) {
							$.each(data, function(i, obj) {
								obj.TransactionData.ProposalStatus = obj.Key15;
							});
						}
						$scope.Proposals = data;
						$rootScope.showHideLoadingImage(false);
						$scope.refresh();
					}, $scope.onGetListingsError);

				} else if (transactionObj.Type == 'illustration') {
					DataService.getDetailsForListing(transactionObj, $scope.onGetIllustrationSuccessForListing, $scope.onGetListingsError);

				}
			}
		} else {
			if (document.location.href.indexOf('Illustration') < 0) {
				for (var i in $scope.selectedProposal) {
					
					for (var k in $scope.Proposals) {
						
						if ($scope.Proposals[k] == $scope.selectedProposal[i]) {
							$scope.Proposals.splice(k, 1)
						}
					}
				}
			} else {
				for (var i in $scope.selectedProposal) {
					
					for (var k in $scope.Illustartions) {
						
						if ($scope.Illustartions[k] == $scope.selectedProposal[i]) {
							$scope.Illustartions.splice(k, 1)
						}
					}
				}
			}
			$scope.selectedProposal.splice(0,$scope.selectedProposal.length);
			setTimeout(function() {
				$rootScope.showHideLoadingImage(false);
				$rootScope.refresh();
			});

		}
		// alert("ondelteSuccessCallBack");
	}
	$scope.ondelteError = function() {
		$rootScope.showHideLoadingImage(false);
		$scope.refresh();
	}
	$scope.initialLoad = function(){
		// Calling the dataservice to get the product object
		if ($scope.clientId != 0) {//to be committed in LE

			isSync = false;
			$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
			PersistenceMapping.clearTransactionKeys();
			$scope.mapKeysforPersistence();
			if($routeParams.type=='Illustration'){
				 var  searchCriteria = {
								 searchCriteriaRequest:{
								"command" : "ListingDashBoard",
								"modes" :['illustration'],
								"value" : ""
								}
							};
			PersistenceMapping.Type='illustration';
			var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
			transactionObj.Type = "illustration";
			$scope.searchIllustrationKey = $rootScope.searchValue;
			$rootScope.searchValue="";
			DataService.getFilteredListing(transactionObj, $scope.onGetIllustrationSuccessForListing, $scope.onGetListingsError);
			}else if($routeParams.type=='eApp'){
					 var  searchCriteria = {
										 searchCriteriaRequest:{
								"command" : "ListingDashBoard",
								"modes" :['eApp'],
								"value" : ""
								}
							};
			PersistenceMapping.Type='eApp';
			var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
			// var transactionObj =
			// $scope.mapScopeToPersistance({});
			transactionObj.Id = $routeParams.transactionId;
			transactionObj.Type = "eApp";
			transactionObj.filter = "Key15  <> 'Cancelled'";
			$scope.searchProposalKey = $rootScope.searchValue;
			$rootScope.searchValue="";	
			DataService.getFilteredListing(transactionObj, $scope.onGeteAppListingSuccess, $scope.onGetListingsError);	
			}

		}
	}
	
	$scope.createNewIllustration = function() {
		$rootScope.transactionId=0;
		if(IllustratorVariables.leadId==0 && IllustratorVariables.fnaId==0 ){
		IllustratorVariables.clearIllustrationVariables();
		}
		$location.path('/products/0/0/0/0/false');
	}
	
		//Copy Functionality Implementation
		
		//To check the length of proposals
		$scope.checkProposals = function() {
			if($scope.selectedProposal.length > 0){
				var index = 0;
				$scope.cloneList(index);
			}else{
				$rootScope.lePopupCtrl.showError(translateMessages(
						$translate, "lifeEngage"), translateMessages(
								$translate, "selectProposal"),
						translateMessages($translate, "illustrator.ok"),
						$scope.rootedOK);
			}
		}
		
		//To listing the corresponding proposal
		$scope.cloneList = function(index) {
				var noOfProposals = $scope.selectedProposal.length;
				if( noOfProposals > index){
					IllustratorService.cloneListing(index,$scope,$scope.selectedProposal[index]);
				}else{
					$rootScope.showHideLoadingImage(false);
					$scope.initialLoad();//has to be changed with LE functionality
				}
				
		}
		
		$scope.onListingSuccess = function(index,data) {
			$scope.updateListingData(data, function(transactionObj){
				IllustratorService.cloneSave($scope,transactionObj,function(data){
					index++;
					$scope.cloneList(index)
				},function(){
				});
			});
		}
		
		$scope.updateListingData = function(data,successcallBack) {
			if(data[0].TransactionData != null || data[0].TransactionData != ""){
				data[0].TransactionData.IllustrationOutput = "";
			}
			data[0].Id = '';
			PersistenceMapping.mapPersistenceToScope(data);
			IllustratorVariables.setIllustratorModel(data[0].TransactionData);
			$scope.Illustration = IllustratorVariables.getIllustratorModel();
			$scope.mapKeysforPersistenceChild(data);
			var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
			successcallBack(transactionObj);
			
		}
		
		$scope.mapKeysforPersistenceChild = function(data) {
			PersistenceMapping.Type = "illustration";
			PersistenceMapping.Key5 = data[0].Key5;
		}
		
		$scope.showEmailPopup = function() {
			$scope.emailpopup = true;
			$scope.refresh();
		}
		
		$scope.fetchIllustrations = function(){	
			if ($scope.selectedProposal.length == 0) {
				$rootScope.lePopupCtrl.showError("",translateMessages($translate,"emailSelectAtlestOneWarning"),
				translateMessages($translate, "general.ok"));
			} else {
				IllustratorService.fetchIllustrationsForEmail($scope.selectedProposal, DataService, 
				$scope.doValidationForEmailSending, $scope.fetchError);		
			}
		}
		
		$scope.fetchError = function() {
			$rootScope.lePopupCtrl.showError(translateMessages(
					$translate, "lifeEngage"), translateMessages(
					$translate, "emailBIErrorMessage"), translateMessages($translate,
					"fna.ok"));
			
		}
		// two validations before opening email popup 1.Chosen BIs should belong to same proposer  2. Status of chosen BIs should be FINAL
		$scope.doValidationForEmailSending = function(transactions) {
			$scope.emailToId = "";
			$scope.emailccId ="";
			var testSameProposer = true;
			var testBIStatusFinal = true; 
			if (transactions.length > 0) {
				$scope.illustartionsToMail = transactions;
				var proposer_email = transactions[0].TransactionData.Payer.ContactDetails.emailId;
				var proposer_firstname = transactions[0].TransactionData.Payer.BasicDetails.firstName;
				//var proposer_lastname = $scope.selectedProposal[0].TransactionData.Payer.BasicDetails.lastName;
				var proposer_dob = transactions[0].TransactionData.Payer.BasicDetails.dob;
				var status = $scope.selectedProposal[0].Key15;			
					if(status != "Final"){
						testBIStatusFinal = false;
						$scope.showValidationErrorforEmail(testSameProposer,testBIStatusFinal)
						//return false;
					}				
				//for ( var obj in transactions) {
				for(var i=1;i < transactions.length;i++) {
					if(transactions[i].TransactionData.Payer.BasicDetails.firstName == proposer_firstname
					//&& transactions[i].TransactionData.Payer.BasicDetails.lastName == proposer_lastname
					&& transactions[i].TransactionData.Payer.BasicDetails.dob == proposer_dob){
						if(!$scope.selectedProposal[i].Key15 == status){
							testBIStatusFinal = false;
							break;
						} 
					} else {
						testSameProposer = false;
						break;
					}								
				};
			} 
			if(testSameProposer && testBIStatusFinal) {
				$scope.emailToId = proposer_email;
				//cc id needs to be populated from User Details object agent - mailid - yet to implement maild id return from service
				$scope.emailpopupError ="";
				$scope.showEmailPopup();
			} else {
				$scope.showValidationErrorforEmail(testSameProposer,testBIStatusFinal);
			}
		}
		
		$scope.showValidationErrorforEmail = function(proposerSame,statusFinal) {
			if(!proposerSame){
				$rootScope.lePopupCtrl.showError("",translateMessages($translate,"emailBIValidationErrorProposerCheck")
				,translateMessages($translate, "general.ok"));
			} else if (!statusFinal){
				$rootScope.lePopupCtrl.showError("",translateMessages($translate,"emailBIValidationErrorSatusCheck")
				,translateMessages($translate, "general.ok"));
			}
		}
		
		$scope.emailPopupOkClick = function (){		
			if($scope.validateMailIds()) {
			    $scope.emailpopup = false;
			    $rootScope.showHideLoadingImage(true,"Please Wait");
				$scope.saveEmailSendFlagAndData($scope.showSuccessPopup,$scope.showErrorPopup);
			}
		}
		$scope.validateMailIds = function() {		
			var EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
			if($scope.emailToId==""|| $scope.emailToId == undefined){
					$scope.emailpopupError = "Please enter To mail id";
				return false;
			}
			var tomailIds = $scope.emailToId.split(',');		
			for ( var obj in tomailIds) {		 
	            if(!EMAIL_REGEXP.test(tomailIds[obj])){
					$scope.emailpopupError = "Please enter valid To mail id";
					return false;
					break;
				}      			
			}
			if($scope.emailccId != undefined && $scope.emailccId !="") {
				var ccMailids = $scope.emailccId.split(',');
				for ( var obj in ccMailids) {		 
					if(!EMAIL_REGEXP.test(ccMailids[obj])){
						$scope.emailpopupError = "Please enter valid CC mail id";
						return false;
						break;
					}      			
				}			
			}
			return true;
		}
		
		$scope.saveEmailSendFlagAndData = function(successCallback,errorCallBack) {
			if ($scope.selectedProposal.length > 0) {			 	 
				var emailObj = {};
				emailObj.toMailIds = $scope.emailToId;
				emailObj.fromMailId = $scope.emailfromId;
				emailObj.ccMailIds = $scope.emailccId; 
				IllustratorService.setEmailBIflagAndSaveTransactions(emailObj,$scope.illustartionsToMail,
						DataService,successCallback,errorCallBack);
			}		
		}
		$scope.showSuccessPopup = function(online){
		    $rootScope.showHideLoadingImage(false);
	        //$scope.emailpopup = false;
	        $scope.emailToId ="";
	        $scope.emailccId="";        
	        $.each($scope.selectedProposal, function(i, obj) {
	            $scope.toggleSelection(obj);             
	        });
	        $scope.selectedProposal=[];
	        $scope.illustartionsToMail =[];
	        if(!online){
	            $rootScope.lePopupCtrl.showSuccess(translateMessages(
	                $translate, "lifeEngage"), translateMessages(
	                $translate, "emailOkButtonMessageOffline"), translateMessages($translate,
	                "fna.ok"));             
	        } else if (online){
	            $rootScope.lePopupCtrl.showSuccess(translateMessages(
	                $translate, "lifeEngage"), translateMessages(
	                $translate, "emailOkButtonMessageOnline"), translateMessages($translate,
	                "fna.ok"));              
	        }
	        $scope.refresh();
	    }
		$scope.showErrorPopup = function() {
			$scope.emailpopup = false;
			$rootScope.lePopupCtrl.showError(translateMessages(
					$translate, "lifeEngage"), translateMessages(
					$translate, "emailBIErrorMessage"), translateMessages($translate,
					"fna.ok"));
		}
		$scope.cancelEmailPopup = function(){
			$.each($scope.selectedProposal, function(i, obj) {
				$scope.toggleSelection(obj);			 
			});
			$scope.selectedProposal=[];
			$scope.illustartionsToMail =[];
			$scope.emailpopup = false;
		}
		
		$scope.$on('$viewContentLoaded', function(){
			$scope.initialLoad();
		  });
		
	
}]);
