function GLI_SendEmailController($timeout, $rootScope, $scope, $compile,
		$routeParams, DataService, LookupService, DocumentService,
		FnaVariables, $location, UtilityService, PersistenceMapping, $translate) {

	// Click on Send Button
	$scope.onClickSend = function() {

		if (navigator.network.connection.type == Connection.NONE) {
			$rootScope.lePopupCtrl.showError(translateMessages($translate,
					"lifeEngage"), translateMessages($translate,
					"Your email has been sent."), translateMessages($translate,
					"fna.ok"));
		}

		else {
			$rootScope.lePopupCtrl.showError(translateMessages($translate,
					"lifeEngage"), translateMessages($translate,
					"Your email request has been placed successfully."),
					translateMessages($translate, "fna.ok"));

		}
	}

	// Click on Cancel Button//to do while click on cancel button clear all selected solutions.
	$scope.onClickCancel = function() {
		$scope.$parent.showEmailPopup = false;

	}
}
