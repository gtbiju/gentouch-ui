storeApp.controller('GLI_LandingController', GLI_LandingController);
GLI_LandingController.$inject = ['$rootScope',
    'GLI_IllustratorVariables',
    '$scope',
    '$routeParams',
    'DataService',
    '$translate',
    '$location',
    'GLI_FnaVariables',
    'FnaVariables',
    'GLI_EappVariables',
    'EappVariables',
    'UtilityService',
    'IllustratorVariables',
    'PersistenceMapping',
    'globalService',
    '$controller',
    'LmsVariables',
    'UserDetailsService',
    'AgentService',
    'LoginService',
    'GLI_DataService',
    'PushNotificationService',
    'GLI_LmsVariables',
    'commonConfig',
    'DataWipeService'
];

function GLI_LandingController($rootScope, GLI_IllustratorVariables, $scope, $routeParams,
    DataService, $translate, $location, GLI_FnaVariables, FnaVariables, GLI_EappVariables, EappVariables,
    UtilityService, IllustratorVariables, PersistenceMapping,
    globalService, $controller, LmsVariables, UserDetailsService, AgentService, LoginService, GLI_DataService, PushNotificationService, GLI_LmsVariables, commonConfig, DataWipeService) {
    $controller('LandingController', {
        $rootScope: $rootScope,
        IllustratorVariables: GLI_IllustratorVariables,
        $scope: $scope,
        $routeParams: $routeParams,
        DataService: GLI_DataService,
        $translate: $translate,
        $location: $location,
        UtilityService: UtilityService,
        PersistenceMapping: PersistenceMapping,
        globalService: globalService,
        LmsVariables: GLI_LmsVariables,
        FnaVariables: GLI_FnaVariables,
        EappVariables: GLI_EappVariables,
        DataWipeService: DataWipeService
    });
    $scope.showDropDown = false;
    $scope.searchResultArray = [];
    $scope.searchResult = false;
    UtilityService.leadPage = '';
    $scope.listingData = {};
    $rootScope.selectedPage = "landingPage";
    $rootScope.inGeofenceArea = false;
    LmsVariables.isFromLeadListing = false;
    $scope.reportingAgentsList = [];
    $scope.mappedBranchesList = [];
    $scope.applicableProductList = [];
    $scope.logoutpopup = false;
    $rootScope.moduleHeader = "";
    $rootScope.hideLanguageSetting = true;
    $rootScope.enableRefresh = false;
    if (typeof ($rootScope.isHierarchyLoginCheck) == 'undefined') {
        $rootScope.isHierarchyLoginCheck = '';
    }
    $('#wrapper').addClass('landingPage');
    if (!$rootScope.reloadAfterSync) {
        $rootScope.hideCustomOverlay = false;
    }
    $rootScope.isAgent = false;

    /*
     * Overrided function
     */
    $scope.loadLandingPage = function () {
        $rootScope.showHideLoadingImage(false, "");
        $rootScope.moduleVariable = "";
        if (rootConfig.isPrecomiledTemplate) {
            LEDynamicUI.loadPrecompiledTemplate("preCompiler.json", function () {
                if (typeof rootConfig != "undefined" && rootConfig.isDebug) {
                    console.log("preCompiledTemplate loaded successfully");
                }
            }, function (err) {
                if (typeof rootConfig != "undefined" && rootConfig.isDebug) {
                    console.log("preCompiledTemplate_lifeengage load failed  : " + err);
                }
            });
        }
        UtilityService.previousPage = '';
        // To set the globalservice variables to null
        var parties = [];
        globalService.setParties(parties);
        var product = {
            "ProductDetails": {},
            "policyDetails": {}
        };
        globalService.setProduct(product);
        IllustratorVariables.illustratorId = "0";
        IllustratorVariables.setIllustratorModel(null);
        IllustratorVariables.setIllustrationAttachmentModel(null);
        $scope.agentDetail = UserDetailsService.getUserDetailsModel();
        if (!(rootConfig.isDeviceMobile)) {
            if (UserDetailsService.getAgentRetrieveData().AgentDetails.agentType == rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType == rootConfig.agentTypeBranchAdmin) {
                $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
                $rootScope.HamburgerForAgent = true;
                $location.path("/MyAccount/eApp/0/0");
                $scope.refresh();
            } else {
                $rootScope.HamburgerForAgent = false;
                isSync = false;
                PersistenceMapping.clearTransactionKeys();
                $scope.mapKeysforPersistence();
                var searchCriteria = {
                    searchCriteriaRequest: {
                        "command": "LandingDashBoardCount",
                        "modes": rootConfig.modules,
                        "value": ""
                    }
                };
                var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
                delete transactionObj.Id;
                transactionObj.filter = "Key15  <> 'Cancelled'";
                DataService.retrieveByFilterCount(transactionObj, $scope.onGetListingsSuccess, $scope.onGetListingsError);
            }
        } else {
            $rootScope.HamburgerForAgent = false;
            PersistenceMapping.clearTransactionKeys();
            PersistenceMapping.dataIdentifyFlag = false;
            $scope.mapKeysforPersistence();
            var searchCriteria = {
                searchCriteriaRequest: {
                    "command": "LandingDashBoardCount",
                    "modes": rootConfig.modules,
                    "value": ""
                }
            };
            var transactionObj = PersistenceMapping
                .mapScopeToPersistence(searchCriteria);
            delete transactionObj.Id;
            $scope.searchValue = "";
            transactionObj.filter = "Key15  <> 'Cancelled'";
            DataService.retrieveByFilterCount(transactionObj,
                $scope.onGetListingsSuccess, $scope.onGetListingsError);
        }
    };
    /* $scope.logout = function() {
    	$scope.logoutpopup = true;
    	$rootScope.isAuthenticated = false;
    	var flow = localStorage.getItem('loginFlow');
    	if(flow && flow == "internal") {
    		$location.path('/login-internal');
    	} else {
    		if (eval(rootConfig.isDeviceMobile)){
    			
    			$location.path('/login');
    			localStorage.removeItem('loginFlow');
    			 

    		} else {
    			 
    			$location.path('/login');
    			localStorage.removeItem('loginFlow');
    			 
    		}
    	}
    };*/
    $scope.onGetListingsSuccess = function (data) {
        $scope.lmsStaticCountVar = {
            "count": "0",
            "mode": rootConfig.lmsLandingScrnStatus
        }
        $scope.fnaStaticCountVar = {
            "count": "0",
            "mode": rootConfig.fnaLandingScrnStatus
        }
        $scope.IlltnStaticCountVar = {
            "count": "0",
            "mode": rootConfig.illtnLandingScrnStatus
        }
        $scope.eAppStaticCountVar = {
            "count": "0",
            "mode": rootConfig.eAppLandingScrnStatus
        }

        $scope.lmsStatusCountList = angular.copy(rootConfig.statusCountListLMS);
        $scope.donutCountListLMS = angular.copy($scope.lmsStatusCountList);
        $scope.fnaStatusCountList = angular.copy(rootConfig.statusCountListFNA);
        $scope.eAppStatusCountList = angular.copy(rootConfig.statusCountListeApp);
        $scope.biStatusCountList = angular.copy(rootConfig.statusCountListBI);
        if (!($scope.searchResult)) {
            $scope.listingData = angular.copy(data);
        } else {
            $scope.searchResult = false;
        }
        if (!(rootConfig.isDeviceMobile)) {
            data = data[0].TransactionData.SearchCriteriaResponse;

        }
        $scope.summary = data;
        if ($scope.summary.length != 0) {
            for (var i = 0; i < $scope.summary.length; i++) {
                if ($scope.summary[i].mode === 'eApp') {
                    $scope.eAppvar = $scope.summary[i];
                } else if ($scope.summary[i].mode === 'LMS') {
                    $scope.lmsvar = $scope.summary[i];
                } else if ($scope.summary[i].mode === 'FNA') {
                    $scope.fnavar = $scope.summary[i];
                } else if ($scope.summary[i].mode === 'illustration') {
                    $scope.illustrationvar = $scope.summary[i];
                } else {
                    $scope.eAppvar.count = 0;
                    $scope.lmsvar.count = 0;
                    $scope.fnavar.count = 0;
                    $scope.illustrationvar = 0;
                }

            }

            if ($scope.lmsvar.subModes && $scope.lmsvar.subModes.length > 0) {
                var openStatusArr = rootConfig.lmsOpenStatus.split(",");
                /*               var openStatusCount = 0;
                for (var i = 0; i < openStatusArr.length; i++) {
                    for (var n = 0; n < $scope.lmsvar.subModes.length; n++) {
                        if ($scope.lmsvar.subModes[n].mode == openStatusArr[i]) {
                            //$scope.lmsvar.subModes.remove(n);
                            if ((rootConfig.isDeviceMobile)) {
                                openStatusCount += $scope.lmsvar.subModes[n].count;
                            } else {
                                if (typeof ($scope.lmsvar.subModes[n].count) == "string") {
                                    openStatusCount += Number($scope.lmsvar.subModes[n].count);
                                } else {
                                    openStatusCount += $scope.lmsvar.subModes[n].count;
                                }
                            }
                            $scope.lmsvar.subModes.splice(n, 1);
                            break;
                        }
                    }
                }
                var openStatusMode = {
                    "count": openStatusCount,
                    "mode": "Open"
                };
                $scope.lmsvar.subModes.push(openStatusMode);
*/
                for (var j = 0; j < $scope.lmsvar.subModes.length; j++) {
                    if ($scope.lmsvar.subModes[j].mode == rootConfig.lmsLandingScrnStatus) {
                        $scope.lmsStaticCountVar.count = $scope.lmsvar.subModes[j].count;
                        $scope.lmsStaticCountVar.mode = $scope.lmsvar.subModes[j].mode;
                        break;
                    }
                }

                $scope.calculateStatusCount($scope.lmsStatusCountList, $scope.lmsvar);
            }
            if ($scope.fnavar.count > 0 && $scope.fnavar.subModes && $scope.fnavar.subModes.length > 0) {
                for (var k = 0; k < $scope.fnavar.subModes.length; k++) {
                    if ($scope.fnavar.subModes[k].mode == rootConfig.fnaLandingScrnStatus) {
                        $scope.fnaStaticCountVar = $scope.fnavar.subModes[k];
                        break;
                    }
                }

                $scope.calculateStatusCount($scope.fnaStatusCountList, $scope.fnavar);
            }
            if ($scope.illustrationvar.count > 0 && $scope.illustrationvar.subModes && $scope.illustrationvar.subModes.length > 0) {
                for (var l = 0; l < $scope.illustrationvar.subModes.length; l++) {
                    if ($scope.illustrationvar.subModes[l].mode == rootConfig.illtnLandingScrnStatus) {
                        $scope.IlltnStaticCountVar = $scope.illustrationvar.subModes[l];
                        break;
                    }
                }
                $scope.calculateStatusCount($scope.biStatusCountList, $scope.illustrationvar);
            }
            if ($scope.eAppvar.count > 0 && $scope.eAppvar.subModes && $scope.eAppvar.subModes.length > 0) {
                for (var m = 0; m < $scope.eAppvar.subModes.length; m++) {
                    if ($scope.eAppvar.subModes[m].mode == rootConfig.eAppLandingScrnStatus) {
                        $scope.eAppStaticCountVar = $scope.eAppvar.subModes[m];
                        break;
                    }
                }
                $scope.calculateStatusCount($scope.eAppStatusCountList, $scope.eAppvar);
            }
        }
        $scope.refresh();

        $rootScope.showHideLoadingImage(false);
    };

    $scope.getCurrentMonthandYear = function () {
        var currentMonth = new Date().getMonth();
        if (currentMonth < 9) {
            currentMonth = '0' + (currentMonth + 1);
        } else {
            currentMonth = (currentMonth + 1).toString();
        }
        var currentYear = new Date().getFullYear().toString();

        return {
            month: currentMonth,
            year: currentYear
        }
    };

    $scope.retrieveAgentProfile = function () {
        PersistenceMapping.clearTransactionKeys();
        var transactionObj = PersistenceMapping.mapScopeToPersistence({});
        AgentService.retrieveAgentProfile(transactionObj, $scope.onRetrieveAgentProfileSuccess, $scope.onRetrieveAgentProfileError);
        /* GLI_DataService.retrieveSPAJCount(transactionObj.Key11, function (count) {
            transactionObj.Key18 = (rootConfig.maxSPAJCount) - count;
            AgentService.retrieveAgentProfile(transactionObj, $scope.onRetrieveAgentProfileSuccess, $scope.onRetrieveAgentProfileError);
        }, $scope.onRetrieveAgentProfileError); */
    };
    //function to calculate status count for each module
    $scope.calculateStatusCount = function (
        statusCountList, varList) {
        statusCountList.count = varList.count;
        for (var loc = 0; loc < statusCountList.subModes.length; loc++) {
            $scope.positionFlag = false
            for (var i = 0; i < varList.subModes.length; i++) {
                if (varList.subModes[i].mode == statusCountList.subModes[loc].mode) {
                    $scope.position = i;
                    $scope.positionFlag = true;
                    break;
                }
            }
            if (($scope.positionFlag)) {
                statusCountList.subModes[loc].count = varList.subModes[$scope.position].count;
            } else {
                statusCountList.subModes[loc].count = 0;
            }
        }

        if (varList.mode == "LMS") {
            $scope.lmsStatusCountList = statusCountList;
            $scope.donutCountListLMS = angular.copy($scope.lmsStatusCountList);
            $scope.donutCountListLMS.subModes = [];
            angular.forEach($scope.lmsStatusCountList.subModes, function (value, key) {
                if (value.mode == "New") {
                    $scope.donutCountListLMS.subModes.push(value);
                }
            });
        } else if (varList.mode == "FNA") {
            $scope.fnaStatusCountList = statusCountList;
        } else if (varList.mode == "eApp") {
            $scope.eAppStatusCountList = statusCountList;
        } else if (varList.mode == "illustration") {
            $scope.biStatusCountList = statusCountList;
        }
        if ($scope.showDropDown) {
            if (statusCountList.count > 1) {
                statusCountList.mode = statusCountList.mode + "s";
            }
            $scope.searchResultArray.push(statusCountList);
        } else {
            $scope.showDropDown = false;
        }
    };


    //extending searchHome
    $scope.SearchHome = function (searchLead) {
        if (searchLead !== undefined && searchLead !== "") {
            if ($scope.showDropDown == false) {
                $scope.showDropDown = true;
                $scope.searchResult = true;
                $scope.searchResultArray = [];
                PersistenceMapping.clearTransactionKeys();
                if ((rootConfig.isDeviceMobile)) {
                    PersistenceMapping.dataIdentifyFlag = false;
                }
                $scope.mapKeysforPersistence();
                var searchCriteria = {
                    searchCriteriaRequest: {
                        "command": "LandingDashBoardCount",
                        "modes": rootConfig.modules,
                        "value": searchLead
                    }
                };
                var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
                DataService.retrieveByFilterCount(transactionObj, $scope.onGetSearchListingsSuccess, $scope.onGetListingsError);
            }
        } else {
            $scope.showDropDown = false;
            $scope.onGetListingsSuccess($scope.listingData);
        }
    };

    //extending success call of search home
    $scope.onGetSearchListingsSuccess = function (data) {
        if (data.length == 0) {
            $scope.showDropDown = false;
        }
        $scope.lmsvar = {
            "count": 0,
            "mode": "",
            "subModes": {}
        };
        $scope.eAppvar = {
            "count": 0,
            "mode": "",
            "subModes": {}
        };
        $scope.fnavar = {
            "count": 0,
            "mode": "",
            "subModes": {}
        };
        $scope.illustrationvar = {
            "count": 0,
            "mode": "",
            "subModes": {}
        };
        $scope.onGetListingsSuccess(data); //To be committed in LE Code 
    };



    // Setting the status to Draft on creating a new illustration
    $scope.createNew = function (app) {
        $rootScope.isTabsEnabled = false;
        if (app == "Lead") {

            if ($scope.reportingAgentsList) {
                $scope.normalFlow(app);
                /*if ($scope.reportingAgentsList.length == 0) {
                    $rootScope.lePopupCtrl.showError(
                        translateMessages($translate,
                            "lifeEngage"),
                        translateMessages($translate,
                            "lms.cannotCreateNewLead"),
                        translateMessages($translate,
                            "lms.ok"));
                } else {
                    $scope.normalFlow(app);
                }*/
            } else {
                $rootScope.showHideLoadingImage(false);
                $rootScope.lePopupCtrl.showError(
                    translateMessages($translate,
                        "lifeEngage"),
                    translateMessages($translate,
                        "lms.cannotCreateNewLead"),
                    translateMessages($translate,
                        "lms.ok"));
                $scope.cancelPopup();
            }
        } else {
            $scope.normalFlow(app);
        }
    };

    $scope.normalFlow = function (app) {
        $rootScope.isTabsEnabled = false;
        switch (app) {
            case "Lead":
                $rootScope.showHideLoadingImage(true, 'loadingMsg', $translate);
                LmsVariables.editMode = false;
                $rootScope.transactionId = 0;
                LmsVariables.actualMeetingDate = "";
                LmsVariables.actualMeetingTime = "";
                LmsVariables.plannedMeetingDate = "";
                LmsVariables.plannedMeetingTime = "";
                LmsVariables.submissionDate = "";
                LmsVariables.clearLmsVariables();
                LmsVariables.setNewLmsModel($rootScope.username);
                $location.path('/lms/clientprofile');
                break;
            case "Product":
                IllustratorVariables.illustrationStatus = "";
                $location.path('/products/0/0/0/0/true');
                break;
            case "selfTools":
                $location.path('/selfTools/selfTools');
                break;
            case "aboutGenerali":
                $location.path('/selfTools/aboutGenerali');
                break;
            case "Illustration":
                $location.path('/products/0/0/0/0/false');
                break;
            case "Proposal":
                $location.path('MyAccount/Illustration/0/0');
                break;
            default:
                $location.path("/lms");
                break;
        }
    };

    $scope.$on("$destroy", function () {
        if (!!plot)
            plot.destroy();
        if (!!plot2)
            plot2.destroy();
    });

    $scope.List = function (app, status, searchvalue, searchKey) {
        $rootScope.showHideLoadingImage(true, 'loadingMsg', $translate);
        if (typeof status == "undefined" || status == "") {
            status = "status"
        }
        switch (app) {
            case "lead":
            case "Lead":
            case "Leads":
                $location.path("/lms");
                if ($rootScope.searchValue == null || $rootScope.searchValue == "") {
                    $rootScope.searchValue = status;
                    LmsVariables.searchKey = searchKey;
                }
                break;
            case "fna":
            case "FNA":
            case "FNAs":
                FnaVariables.stausFilter = searchvalue;
                FnaVariables.searchFilter = searchKey;
                $location.path("/fnaListing/" + status + "");
                if ($rootScope.searchValue == null || $rootScope.searchValue == "") {
                    FnaVariables.stausFilter = searchvalue;
                }
                break;
            case "illustration":
            case "Illustration":
            case "Illustrations":
                IllustratorVariables.leadId = "0";
                IllustratorVariables.leadName = "";
                IllustratorVariables.leadEmailId = "";
                if (UtilityService.leadPage == "") {
                    UtilityService.leadPage = 'GenericListing';
                }
                $location.path("/MyAccount/Illustration/");
                if (IllustratorVariables.searchBiValue == null || IllustratorVariables.searchBiValue == "") {
                    IllustratorVariables.searchBiValue = searchvalue;
                    IllustratorVariables.searchBiKey = searchKey;
                }
                break;
            case "eApp":
            case "eApps":
                if (searchvalue == rootConfig.statusCountListeAppMemo) {
                    EappVariables.EappSearchValueMemo = searchvalue;
                } else {
                    EappVariables.EappSearchValueMemo = "";
                }
                EappVariables.EappSearchValue = searchvalue;
                EappVariables.EappSearchKey = searchKey;
                $location.path("/MyAccount/eApp/0/0");
                if ($rootScope.searchValue == null || $rootScope.searchValue == "") {
                    $rootScope.searchValue = searchvalue;
                    EappVariables.searchKey = searchKey;
                }
                break;
            default:
                break;
        }

    };
    $scope.initialLoad = function () {
        $scope.responseFlag = true;
        if (localStorage["prevPath"].indexOf("/login") > 0) {
            if ((rootConfig.isDeviceMobile) && rootConfig.hasContentAdmin && checkConnection()) {
                $rootScope.showHideLoadingImage(true, translateMessages($translate,
                    "general.downloadingMandatory"));
                $scope.contentDownloader();
            } else if ((rootConfig.isDeviceMobile) && !(rootConfig.hasContentAdmin) && checkConnection()) {
                $scope.loadConfigfile(function () {
                    $scope.retrieveAgentProfile()
                });
            } else {
                // TO DO - Modify code for mobile part
                if ((rootConfig.isDeviceMobile)) {
                    PersistenceMapping.clearTransactionKeys();
                    var transactionObj = PersistenceMapping.mapScopeToPersistence({});
                    GLI_DataService.retrieveAgentDetailsOffline(transactionObj, function (data) {
                        if (rootConfig.AgentStatusForDateWipe.indexOf(data.AgentDetails.status) >= 0) {
                            DataWipeService.deleteAgentSpecificConfigFiles(data.AgentDetails.agentCode, commonConfig().DATA_WIPE_REASON.UNAUTHORIZED_USER,
                                function () {
                                    $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
                                        translateMessages($translate, "dataWipe.agentTerminatedDataWipeMessage"),
                                        translateMessages($translate, "fna.ok"),
                                        $scope.okClick);
                                    $rootScope.showHideLoadingImage(false, "");
                                },
                                function (error) {
                                    $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
                                        translateMessages($translate, "dataWipe.agentTerminatedMessage"),
                                        translateMessages($translate, "fna.ok"),
                                        $scope.okClick);
                                    $rootScope.showHideLoadingImage(false, "");
                                });
                        } else {
                            $scope.loadAgentProfile(data, function (data) {
                                $scope.loadLandingPage();
                            }, function () {});
                        }

                    }, $scope.onRetrieveAgentProfileError);

                } else {
                    //Web , contentadmin false of device case - need to merge config files
                    $scope.loadConfigfile(function () {
                        PersistenceMapping.clearTransactionKeys();
                        var transactionObj = PersistenceMapping.mapScopeToPersistence({});
                        transactionObj.Key18 = 0;
                        transactionObj.Key11 = $rootScope.username;
                        AgentService.retrieveAgentProfile(transactionObj, function (data) {
                            $scope.retrieveAgentProfileSuccessCallBack(data);
                        }, $scope.onRetrieveAgentProfileError);
                    });
                }
            }
        } else {
            if ((rootConfig.isDeviceMobile) && rootConfig.hasContentAdmin && checkConnection()) {
                $rootScope.showHideLoadingImage(true, translateMessages($translate,
                    "general.downloadingMandatory"));
                $scope.contentDownloader();
            } else if ((rootConfig.isDeviceMobile) && !(rootConfig.hasContentAdmin) && checkConnection()) {
                $scope.loadConfigfile(function () {
                    $scope.retrieveAgentProfile()
                });
            } else {
                // TO DO - Modify code for mobile part
                if ((rootConfig.isDeviceMobile)) {
                    PersistenceMapping.clearTransactionKeys();
                    var transactionObj = PersistenceMapping.mapScopeToPersistence({});
                    GLI_DataService.retrieveAgentDetailsOffline(transactionObj, function (data) {
                        if (rootConfig.AgentStatusForDateWipe.indexOf(data.AgentDetails.status) >= 0) {
                            DataWipeService.deleteAgentSpecificConfigFiles(data.AgentDetails.agentCode, commonConfig().DATA_WIPE_REASON.UNAUTHORIZED_USER,
                                function () {
                                    $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
                                        translateMessages($translate, "dataWipe.agentTerminatedDataWipeMessage"),
                                        translateMessages($translate, "fna.ok"),
                                        $scope.okClick);
                                    $rootScope.showHideLoadingImage(false, "");
                                },
                                function (error) {
                                    $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
                                        translateMessages($translate, "dataWipe.agentTerminatedMessage"),
                                        translateMessages($translate, "fna.ok"),
                                        $scope.okClick);
                                    $rootScope.showHideLoadingImage(false, "");
                                });
                        } else {
                            $scope.loadAgentProfile(data, function (data) {
                                $scope.loadLandingPage();
                            }, function () {});
                        }

                    }, $scope.onRetrieveAgentProfileError);

                } else {
                    //Web , contentadmin false of device case - need to merge config files
                    $scope.loadConfigfile(function () {
                        PersistenceMapping.clearTransactionKeys();
                        var transactionObj = PersistenceMapping.mapScopeToPersistence({});
                        transactionObj.Key18 = 0;
                        transactionObj.Key11 = $rootScope.username;
                        AgentService.retrieveAgentProfile(transactionObj, function (data) {
                            $scope.retrieveAgentProfileSuccessCallBack(data);
                        }, $scope.onRetrieveAgentProfileError);
                        $scope.loadLandingPage();
                    });
                }
            }
            /*
            $scope.agentDetail = UserDetailsService.getUserDetailsModel();
            if ($scope.agentDetail.agentType == "BranchUser") {
                $scope.reportingAgentsList = UserDetailsService.getReportingAgents();
            } else if ($scope.agentDetail.agentType == "IOIS") {
                $scope.mappedBranchesList = UserDetailsService.getMappedBranches();
            }
            //For Getting the Product List from User details
            $scope.applicableProductList = UserDetailsService.getApplicableProducts();
            $scope.loadLandingPage();
			*/
        }
    }

    $scope.retrieveAgentDetailsOfflineSuccess = function (data) {
        UserDetailsService.setAgentRetrieveData(data);
        if (data.AgentDetails.agentType == "BranchUser") {
            GLI_DataService.retrieveReportingAgentsDetails({
                    "parentAgentCode": data.AgentDetails.agentCode
                },
                function (reportingAgentData) {
                    $scope.reportingAgentsList = reportingAgentData;
                    UserDetailsService.setReportingAgents($scope.reportingAgentsList);

                    $scope.loadConfigfile(function () {
                        $scope.loadAgentProfile(data);
                        $scope.loadLandingPage()
                    });
                },
                $scope.onRetrieveReportingAgentsError);
        } else if (data.AgentDetails.agentType == "IOIS") {
            GLI_DataService.retrieveMappedBranchesDetails({
                    "parentAgentCode": data.AgentDetails.agentCode
                },
                function (branchList) {
                    $scope.mappedBranchesList = branchList;
                    UserDetailsService.setMappedBranches($scope.mappedBranchesList);
                    //$scope.registerGeofence();
                    GLI_DataService.retrieveReportingRM({
                        "parentAgentCode": data.AgentDetails.agentCode
                    }, function (reportingRMList) {
                        UserDetailsService.setReportingRM(reportingRMList);
                        $scope.loadConfigfile(function () {
                            $scope.loadAgentProfile(data);
                            $scope.loadLandingPage()
                        });
                    }, function () {
                        $scope.loadConfigfile(function () {
                            $scope.loadAgentProfile(data);
                            $scope.loadLandingPage()
                        });
                    });
                },
                $scope.onMappedBranchesSaveError);
        } else if (data.AgentDetails.agentType == "RM" || data.AgentDetails.agentType == "ASM" || data.AgentDetails.agentType == "AM") {
            GLI_DataService.retrieveHierarchyResponse({
                    "AgentCode": data.AgentDetails.agentCode
                },
                function (hierarchyResponse) {
                    $scope.hierarchyDataResponse = hierarchyResponse;
                    $scope.responseFlag = false;
                    UserDetailsService.setHierarchyData($scope.hierarchyDataResponse);
                    //$scope.registerGeofence();
                    $scope.hierarchyMapping(hierarchyResponse[0].TransactionData);
                    $scope.loadConfigfile(function () {});
                },
                $scope.onMappedBranchesSaveError);
        } else {
            $scope.loadConfigfile(function () {
                $scope.loadAgentProfile(data);
                $scope.loadLandingPage()
            });
        }
    }

    $scope.retrieveAgentProfileSuccessCallBack = function (data) {
        $scope.hierarchyLogin = false;
        $rootScope.showHideLoadingImage(false, "");
        $rootScope.hierarchyLogin = false;
        if (data.Response != undefined) {
            if (data.Response.ResponsePayload.Transactions[0].TransactionData.AgentDetails.agentType == "AM" || data.Response.ResponsePayload.Transactions[0].TransactionData.AgentDetails.agentType == "ASM" || data.Response.ResponsePayload.Transactions[0].TransactionData.AgentDetails.agentType == "RM") {
                $scope.responseFlag = false;
                $rootScope.moduleVariable = "";
                $scope.hierarchyLogin = true;
                $rootScope.hierarchyLogin = true;
                $rootScope.isHierarchyLoginCheck = true;
                $scope.reportingTo = "";
                $scope.searchCriteria = {
                    searchCriteriaRequest: {
                        "command": "LeadManagerialLevelRequest",
                        "userDetails": [{
                            "userType": "",
                            "userId": ""
                        }],
                    },
                };
                $scope.hierarchyAgentRetrieveProfile(data);
            }

        } else {
            $rootScope.showHideLoadingImage(false);
            $rootScope.isHierarchyLoginCheck = false;
            $scope.responseFlag = true;
            UserDetailsService.setAgentRetrieveData(data);
            $rootScope.agentName = data.AgentDetails.agentName;
            $scope.agentCode = data.AgentDetails.agentCode;

            /*  Mapped branches save for Agent User Login
             * Save reporting Mapped branches in MappedBranches Table
             * Only applicable to Agent user login
             */

            if (data.AgentDetails.agentType == "IOIS") {
                $scope.mappedBranchesList = data.AgentDetails.mappedBranches;

                $scope.mappedBranchesList.forEach(function (branches) {
                    branches["parentAgentCode"] = $scope.agentCode;
                });
                delete data.AgentDetails.mappedBranches;
            }



            /*  Reporting Agents save for RM User Login
             * Save reporting Agents Details in ReportingAgents Table
             * Only applicable to RM user login
             */
            if (data.AgentDetails.agentType == "BranchUser") {
                $scope.reportingAgentsList = data.AgentDetails.reportingAgents;
                /*  adding RM user ID in every Agent Object - for DB save purpose*/
                $scope.reportingAgentsList.forEach(function (agents) {
                    agents["parentAgentCode"] = $scope.agentCode;
                });
                delete data.AgentDetails.reportingAgents;
            }

            //Hardcoding channel since agent profile service is not ready with channels
            if (!data.AgentDetails.channel) {
                data.AgentDetails.channel = "Agency";
            }
            for (var channelIndx in rootConfig.channels) {
                if (rootConfig.channels[channelIndx].channel == data.AgentDetails.channel) {
                    IllustratorVariables.channelId = rootConfig.channels[channelIndx].channelId;
                }
            }
            /*Saving Applicable Prodcuct List from the Agent Details Servie */
            /*TODO :- This Product assignment needs to be removed once more than one product available - same applicable in GLI_MyAccountController*/
            //data.AgentDetails.products = [{"prdCode":"GS20P","prdName":"GenSave20Plus"},{"prdCode":"GP2025","prdName":"GenProLife"},{"prdCode":"CHS8080","prdName":"GenCompleteHealth"},{"prdCode":"GB8","prdName":"GenBumnan"},{"prdCode":"4N85","prdName":"GenCancer Super Protection"},{"prdCode":"1P05","prdName":"Wholelife"},{"prdCode":"4P10","prdName":"GenSave10Plus"},{"prdCode":"4P04","prdName":"GenSave4Plus"}];
            if (data.AgentDetails.products) {
                if (data.AgentDetails.products.length > 0) {
                    $scope.applicableProductList = data.AgentDetails.products;
                    /*  adding RM user ID in every Product Object - for DB save purpose*/
                    $scope.applicableProductList.forEach(function (agents) {
                        agents["parentAgentCode"] = $scope.agentCode;
                    });
                    delete data.AgentDetails.products;
                    UserDetailsService.setApplicableProducts($scope.applicableProductList);
                } else {
                    delete data.AgentDetails.products;
                }
            }

            if (data.AgentDetails.mappedBranches) {
                delete data.AgentDetails.mappedBranches;
            }
            if (data.AgentDetails.reportingAgents) {
                delete data.AgentDetails.reportingAgents;
            }
            if (data.AgentDetails.reportingRMs) {
                delete data.AgentDetails.reportingRMs;
            }
            if (data.AgentDetails.reportingASMs) {
                delete data.AgentDetails.reportingASMs;
            }

            $rootScope.showHideLoadingImage(false, "");
            $scope.loadAgentProfile(data, function (data) {
                $scope.loadLandingPage();
            });
        }
    }

    $scope.evaluateTotalLeads = function (value1, value2, value3) {
        return (parseInt(value1) + parseInt(value2) + parseInt(value3));
    }

    $scope.evaluateInMillion = function (value) {
        var data = 0;
        if (value != 0 && value != undefined) {
            var data = parseInt(value);
            data = data / 1000000;
            value = data.toFixed(2);
        }
        return (value);
    }
    $scope.backToLoginPage = function () {
        $location.path('/login');
    };
    $scope.onNotificationGCM = function (data) {
        $scope.saveDeviceToken("GCM", data);
    };
    $scope.onRegisterDeviceSuccess = function (data) {
        //Production certificate platform type -APNS
        $scope.saveDeviceToken("APNS", data);
        //Developmentcertificate platform type -APNS_SANDBOX
        //$scope.saveDeviceToken("APNS_SANDBOX",data);
    };
    //Save Device Id from GCM to Server
    $scope.saveDeviceToken = function (type, data) {
        var registerRequest = PushNotificationRegisterRequest();
        registerRequest.registerDeviceRequest.DeviceUserID = $rootScope.username;
        registerRequest.registerDeviceRequest.Application_Name = rootConfig.appName;
        switch (type) {
            case "GCM":
                registerRequest.registerDeviceRequest.Device_ID = data.regid;
                registerRequest.registerDeviceRequest.PushMsgRegisteredID = data.regid;
                registerRequest.registerDeviceRequest.DeviceType = "GCM";
                registerRequest.registerDeviceRequest.MobileAppBundleID = rootConfig.androidPackageName;
                break;
            case "APNS":
                registerRequest.registerDeviceRequest.Device_ID = data;
                registerRequest.registerDeviceRequest.PushMsgRegisteredID = data;
                registerRequest.registerDeviceRequest.DeviceType = "APNS";
                registerRequest.registerDeviceRequest.MobileAppBundleID = rootConfig.iOSAppBundleIdentifier;
                break;
            case "APNS_SANDBOX":
                registerRequest.registerDeviceRequest.Device_ID = data;
                registerRequest.registerDeviceRequest.PushMsgRegisteredID = data;
                registerRequest.registerDeviceRequest.DeviceType = "APNS_SANDBOX";
                registerRequest.registerDeviceRequest.MobileAppBundleID = rootConfig.iOSAppBundleIdentifier;
                break;
        }
        var transactionObj = {};
        transactionObj.Type = "PushNotification";
        transactionObj.TransactionData = registerRequest;
        PushNotificationService.saveDeviceToken(transactionObj, function () {
            localStorage.deviceId = data.regid
        }, function () {
            localStorage.deviceId = null
        });
    };
    $scope.onRetrieveAgentProfileSuccess = function (data) {
        if (rootConfig.isDeviceMobile && typeof data.AgentDetails.agentType != "undefined" && (data.AgentDetails.agentType == rootConfig.agentTypeGAO || data.AgentDetails.agentType == rootConfig.agentTypeBranchAdmin)) {
            GLI_DataService.deleteUserDetailsTableForAnAgent(data.AgentDetails.agentCode, function (successData) {
                $rootScope.showHideLoadingImage(false);
                $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.GAONotSupportInDevice"), translateMessages($translate, "fna.ok"), $scope.backToLoginPage);
            }, function (errorData) {
                $rootScope.showHideLoadingImage(false);
                $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.GAONotSupportInDevice"), translateMessages($translate, "fna.ok"), $scope.backToLoginPage);
            });
        } else {
            $scope.hierarchyLogin = false;
            $rootScope.hierarchyLogin = false;
            //Push Notification - Accessing Device Token
            if ((rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
                if (/Android/i.test(navigator.userAgent)) {
                    PushNotificationService.registerAndroidDeviceForDeviceToken($scope.onNotificationGCM, function () {
                        localStorage.deviceId = null
                    });
                } else if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
                    PushNotificationService.registeriOSDeviceForDeviceToken($scope.onRegisterDeviceSuccess, function () {
                        localStorage.deviceId = null
                    });
                }
                var myVar = setInterval(function () {
                    myTimer();
                }, 30000);

                function myTimer() {
                    if (localStorage.deviceId == null || localStorage.deviceId == undefined) {
                        $scope.alertMessage = translateMessages($translate, "deviceRegistrationError");
                        $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), $scope.alertMessage, translateMessages($translate, "lms.ok"));
                    }
                    clearInterval(myVar);
                }
            }
            if (data.Response != undefined) {
                var saveReq = angular.copy(data.Response.ResponsePayload.Transactions[0].TransactionData);
                if (saveReq.AgentDetails.mappedBranches) {
                    delete saveReq.AgentDetails.mappedBranches;
                }
                if (saveReq.AgentDetails.reportingAgents) {
                    delete saveReq.AgentDetails.reportingAgents;
                }
                if (saveReq.AgentDetails.reportingRMs) {
                    delete saveReq.AgentDetails.reportingRMs;
                }
                if (saveReq.AgentDetails.reportingASMs) {
                    delete saveReq.AgentDetails.reportingASMs;
                }
                if (saveReq.AgentDetails.products) {
                    delete saveReq.AgentDetails.products;
                }
                $scope.loadAgentProfile(data.Response.ResponsePayload.Transactions[0].TransactionData);
                DataService.saveAgentProfile(saveReq, function () {
                    $rootScope.showHideLoadingImage(false, "");
                }, $scope.onRetrieveAgentProfileError);
            } else {
                $rootScope.showHideLoadingImage(false);
                $rootScope.isHierarchyLoginCheck = false;
                UserDetailsService.setAgentRetrieveData(data);
                $scope.responseFlag = true;
                /* var currentDate = new Date();
				var currentDateFormatted = addPrefixInDate(currentDate.getMonth() + 1) + "/" + addPrefixInDate(currentDate.getDate()) + "/" + currentDate.getFullYear();
				var ExpDate = data.AgentDetails.agentLicenseExpDate;
				var agentExpDate = ExpDate.slice(4, 6) + "/" + ExpDate.slice(6, 8) + "/" + ExpDate.slice(0, 4);
				if (data.AgentDetails.isEligble && data.AgentDetails.isEligble == "False" && $rootScope.isFirstTime) {
					$rootScope.isFirstTime = false;
					$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.agentCertified"),
						translateMessages($translate, "fna.ok"));
				}
				dayDiff = dateDiffInDays(new Date(currentDateFormatted), new Date(agentExpDate));
				if (dayDiff <= 7) {
					var dayMsg = "";
					if (dayDiff == 1) {
						dayMsg = dayDiff + " day. "
					} else {
						dayMsg = dayDiff + " days. "
					}
					$rootScope.LicenceExpireMsg = translateMessages($translate, "LicenceExpireMsg1") + dayMsg + translateMessages($translate, "LicenceExpireMsg2");
				}*/
                $rootScope.agentName = data.AgentDetails.agentName;
                $scope.agentCode = data.AgentDetails.agentCode;
                //Hardcoding channel since agent profile service is not ready with channels
                if (!data.AgentDetails.channel) {
                    data.AgentDetails.channel = "Agency";
                }
                for (var channelIndx in rootConfig.channels) {
                    if (rootConfig.channels[channelIndx].channel == data.AgentDetails.channel) {
                        IllustratorVariables.channelId = rootConfig.channels[channelIndx].channelId;
                    }
                }
                //data.AgentDetails.products = [{"prdCode":"GS20P","prdName":"GenSave20Plus"},{"prdCode":"GP2025","prdName":"GenProLife"},{"prdCode":"CHS8080","prdName":"GenCompleteHealth"},{"prdCode":"GB8","prdName":"GenBumnan"},{"prdCode":"4N85","prdName":"GenCancer Super Protection"},{"prdCode":"1P05","prdName":"Wholelife"},{"prdCode":"4P10","prdName":"GenSave10Plus"},{"prdCode":"4P04","prdName":"GenSave4Plus"}];
                if (data.AgentDetails.products) {
                    if (data.AgentDetails.products.length > 0) {
                        $scope.applicableProductList = data.AgentDetails.products;
                        /*  adding RM user ID in every Product Object - for DB save purpose*/
                        $scope.applicableProductList.forEach(function (agents) {
                            agents["parentAgentCode"] = $scope.agentCode;
                        });
                        delete data.AgentDetails.products;
                        GLI_DataService.saveApplicableProductDetails($scope.applicableProductList, $scope.onApplicableProductsSaveSucess, $scope.onApplicableProductsSaveError);
                        UserDetailsService.setApplicableProducts($scope.applicableProductList);
                    } else {
                        delete data.AgentDetails.products;
                    }
                }
                /*  Need to remove the below deletions since it is not needed for FGLI	*/
                if (data.AgentDetails.products) {
                    delete data.AgentDetails.products;
                }
                if (data.AgentDetails.mappedBranches) {
                    delete data.AgentDetails.mappedBranches;
                }
                if (data.AgentDetails.reportingAgents) {
                    delete data.AgentDetails.reportingAgents;
                }
                if (data.AgentDetails.reportingRMs) {
                    delete data.AgentDetails.reportingRMs;
                }
                if (data.AgentDetails.reportingASMs) {
                    delete data.AgentDetails.reportingASMs;
                }
                DataService.saveAgentProfile(data, function () {
                    $scope.loadAgentProfile(data, function (data) {
                        $scope.loadLandingPage();
                    });
                }, $scope.onRetrieveAgentProfileError);
            }
        }
    };

    $scope.onsaveSPAJDetailsSucess = function (data) {

    }

    $scope.onApplicableProductsSaveSucess = function (data) {

    };

    $scope.onApplicableProductsSaveError = function (data) {

    };

    $scope.checkIfStartsWith = function (string, prefix) {
        return string.slice(0, prefix.length) == prefix;
    }

    $scope.getBooleanValue = function (data) {
        return !!JSON.parse(String(data).toLowerCase());
    }
    $scope.loadAgentProfile = function (data, successcallback) {
        if (data.AgentDetails && data.AgentDetails.status && data.AgentDetails.status != "") {
            $scope.isRDS = false;
            var agentDetail = UserDetailsService.getUserDetailsModel();
            if (data.AgentDetails && data.AgentDetails.isRDSUser) {
                $scope.isRDS = $scope.getBooleanValue(data.AgentDetails.isRDSUser);
            }
            if (!(rootConfig.isDeviceMobile) && $scope.isRDS) {
                UserDetailsService.setRDSUser(true);
                if (data && data.AgentDetails) {
                    $rootScope.agentName = data.AgentDetails.agentName;
                }
                var _agentDetail = data["AgentDetails"];

                for (var i = 0; i < rootConfig.agentDetailFields.length; i++) {
                    agentDetail[rootConfig.agentDetailFields[i]] = _agentDetail[rootConfig.agentDetailFields[i]];
                }
                UserDetailsService.setRDSUserDetailsModel(agentDetail);
            } else {
                UserDetailsService.setRDSUser(false);
                if (data && data.AgentDetails) {
                    $rootScope.agentName = data.AgentDetails.agentName;
                }
                var _agentDetail = data["AgentDetails"];

                for (var i = 0; i < rootConfig.agentDetailFields.length; i++) {
                    agentDetail[rootConfig.agentDetailFields[i]] = _agentDetail[rootConfig.agentDetailFields[i]];
                }
                UserDetailsService.setUserDetailsModel(agentDetail);
            }
            $scope.refresh();
            if (successcallback) {
                successcallback(data);
            }
        }
    };

    // Verify whether a new version of app is available
    $scope.isAppExpired = function (successcallback) {
        var configuration = JSON.parse(localStorage.getItem('configuration'));
        var appExpired = false;
        if (parseInt(configuration.appMajorVersion) < parseInt(rootConfig.majorVersion)) {
            appExpired = true;
        } else if (parseInt(configuration.appMajorVersion) == parseInt(rootConfig.majorVersion) &&
            parseInt(configuration.appMinorVersion) < parseInt(rootConfig.minorVersion)) {
            appExpired = true;
        }
        if (appExpired) {
            rootConfig.minorVersion = configuration.appMinorVersion;
            rootConfig.majorVersion = configuration.appMajorVersion;
            $scope.userDetails = UserDetailsService.getUserDetailsModel();
            var currentDate = new Date();
            $scope.userDetails.user.lastLoggedInDate = new Date(currentDate.setDate(currentDate.getDate() - rootConfig.maxPermittedOfflineDuration));
            UserDetailsService.setUserDetailsModel($scope.userDetails);
            LoginService.saveToken($scope.userDetails.user, function (res) {}, function (err) {});
        }
        successcallback(appExpired);
    }

    $scope.backToLogin = function () {
        if ((rootConfig.isDeviceMobile)) {
            var url = rootConfig.downloadNewVersionURL;
            window.open(url, "_system");
            $location.path('/login');
        }
    };

    //extended to comment showHideLoadingImage
    $scope.contentDownloader = function () {
        // le Content Admin starts
        (function downloadContentFiles(mandatory, callAgain) {
            $scope.serviceURL = rootConfig.contentAdminURL;
            var options = {
                contentManagerServiceurl: $scope.serviceURL,
                debugMode: true,
                appContext: rootConfig.appContext
            };
            if (mandatory) {
                options.contentPreference = "mandatory";
            } else {
                options.contentPreference = "optional";
            }
            var le = new LEContentManager(options);
            var options = {
                syncMode: "all",
                jsonFile: "initialContent.json",
                headers: {
                    "nwsAuthToken": "e03364fd-a8c6-7878-3d44-cff2a9d8e333"
                }
            };
            le
                .syncData(
                    function (type, data) {},
                    function (successFileList, errorFileList) {

                        var message = "";
                        if (successFileList.length > 0) {
                            message = successFileList.length +
                                " files successfully downloaded. " +
                                JSON.stringify(successFileList) +
                                ". ";
                            DataService.initializeDb(successFileList);
                            if ((rootConfig.isDeviceMobile) &&
                                !(rootConfig.isOfflineDesktop)) {
                                for (i = 0; i < successFileList.length; i++) {
                                    if (successFileList[i]
                                        .indexOf(".db") >= 0 &&
                                        successFileList[i] != "LifeEngageData.db") {

                                        window.sqlitePlugin
                                            .closeDatabase(
                                                successFileList[i],
                                                function () {

                                                },
                                                function () {

                                                });
                                    }
                                }
                            }
                        }
                        if (errorFileList.length > 0) {
                            message = message + errorFileList.length +
                                " files failed to download. " +
                                JSON.stringify(errorFileList) +
                                ".";

                            $scope.mandatoryDownloadFailed(mandatory);
                        } else {

                            if (callAgain) {
                                /*
                                 * Loading Landinpage after manadatory
                                 * file download
                                 */
                                /* Going to download Non mandatory
                                 * content files
                                 */
                                $scope.loadConfigfile(function () {
                                    //Verify the app expiry version
                                    $scope.isAppExpired(function (appExpired) {
                                        if (!appExpired) {
                                            $scope.retrieveAgentProfile();
                                            downloadContentFiles("", false);
                                        } else {
                                            $rootScope.showHideLoadingImage(false, "");
                                            $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.newVersionReleased"),
                                                translateMessages($translate, "fna.ok"), $scope.backToLogin);
                                        }
                                    });
                                });
                            }

                        }

                    },
                    function (message, data) {
                        $scope.mandatoryDownloadFailed(mandatory);
                        if (typeof message != undefined &&
                            typeof data != undefined) {

                        } else if (typeof message != undefined &&
                            typeof data == undefined) {

                        } else if (typeof message == undefined &&
                            typeof data != undefined) {

                        }

                    }, options);
        })("mandatory", true)
    };

    //GeoFencing code
    $scope.showLocalNotification = function (notificationOptions) {
        NOTIFICATIONS.schedule(notificationOptions, function (res) {

        }, $scope);

        $scope.cancelAndUnregister = function (id, event, callback) {
            NOTIFICATIONS.cancel(id, function () {

                NOTIFICATIONS.un(event, callback);
            });
        }
        $scope.clickFn = function (id, json) {

            $scope.cancelAndUnregister(id, "click", $scope.clickFn);
            $scope.cancelAndUnregister(id, "trigger", $scope.triggerFn);
        }
        $scope.triggerFn = function (notification, state) {
            var message = 'Notification with ID is triggered: ' + notification.id + ' state: ' + state;

            NOTIFICATIONS.clear(notification.id, function () {

            }, 'Notification triggered', 'Close');
            $scope.cancelAndUnregister(notification.id, "trigger", $scope.triggerFn);
        }
        NOTIFICATIONS.on("click", $scope.clickFn);
    };

    /*On Enter Search*/
    $scope.onEnterClickEvent = function (searchValue) {
        if (event.keyCode == 13) {
            $scope.SearchHome(searchValue);
        }
    }

    $("form").submit(function (e) {
        e.preventDefault();
    });
};