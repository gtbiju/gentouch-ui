/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:GLI_MainController
 CreatedDate:20/07/2015
 Description:GLI_MainController 
 */

storeApp
		.controller(
				'GLI_MainController',
				[		
				 	'$route',
					'$rootScope',
					'$scope',
					'$compile',
					'$window',	
					'$routeParams',
					'DataService',
					'LookupService',
					'DocumentService',
					'$translate',
					'UtilityService',
					'GLI_DataService',
					'IllustratorVariables',
					'IllustratorService',
					'EappVariables',
					'$location',
					'PersistenceMapping',
					'FnaVariables',
					'globalService',
					'$controller',
					'LmsVariables',
					'AgentService',
					'UserDetailsService',
					'GLI_IllustratorService',
					'ProductService',
					'GLI_globalService',
					'GLI_EappVariables', 
					'MediaService', 
					'$filter',
					'EappService',
					'GLI_EappService',
					'GLI_IllustratorVariables',
					'AuthService',
					'$filter',	
					function GLI_MainController($route, $rootScope, $scope, $compile, $window,  $routeParams, DataService, LookupService,
												DocumentService, $translate, UtilityService, GLI_DataService, IllustratorVariables, 
												IllustratorService, EappVariables, $location, PersistenceMapping, FnaVariables, 
												globalService, $controller, LmsVariables, AgentService, UserDetailsService, 
												GLI_IllustratorService, ProductService, GLI_globalService, GLI_EappVariables, MediaService,
												$filter, EappService, GLI_EappService, GLI_IllustratorVariables, AuthService) {
						$controller('MainController', {
							$route : $route,
							$rootScope : $rootScope,
							$scope : $scope,	
							$compile : $compile,
							$window: $window,
							$routeParams : $routeParams,
							DataService : GLI_DataService,
							LookupService : LookupService,
							DocumentService : DocumentService,
							$translate : $translate,
							UtilityService : UtilityService,
							IllustratorVariables : IllustratorVariables,
							IllustratorVariables : GLI_IllustratorVariables,
							EappVariables : EappVariables,
							$location:$location,
							PersistenceMapping : PersistenceMapping,
							globalService : globalService,
							UserDetailsService: UserDetailsService,
							LmsVariables : LmsVariables,
							GLI_globalService:GLI_globalService,
							GLI_EappVariables:GLI_EappVariables,
							$filter:$filter,
							AuthService : AuthService
						});

						$rootScope.isAuthenticated = false;
                        $rootScope.enableRefresh=false;
						
						Object.getPrototypeOf($rootScope).refresh = function(delay) {
							var time;
							if(!delay) {
								time=0;
							}
							setTimeout(function(){
								$rootScope.$apply()
							}, time);
						}
						
						Object.getPrototypeOf($scope).refresh=function(delay){
							var self = this;
							var time;
							if(!delay){
								time=0;
							}
							setTimeout(function(){
								self.$apply()
							}, time);
						}
						
						$scope.getNavigationFlag = function(successCallBack){
							$scope.ignoreList = rootConfig.navigationIgnoreList.List
							var flag = false;
							for(i=0;i<$scope.ignoreList.length;i++) {
								if($scope.ignoreList[i] == $rootScope.selectedPage){
									flag =  true;
									break;
								}								   
							}								
							successCallBack(flag);
						}
						
						$scope.playSelfToolVideo = function(video){  
							var fileFullPath ="";
							window.plugins.LEFileUtils.getApplicationPath(function (path) {
								fileFullPath =  "file://"+path + "/" + video;
				            }, function(e) { });
				            if(fileFullPath!=""){
				            	$scope.srcVideo = fileFullPath;
				            	//$scope.refresh();
			                } 
						}
							$scope.learningSystem = function()
							{
								// var url = "http://www.gen-training.com/"; 
								var url = "https://training.generali.co.th/index.html";
								
								if (!rootConfig.isDeviceMobile) 
								{   
									window.open(url,'_blank');
								}
								else
								{
									window.open(url, '_system');        
								}
							}	
						$scope.selectSlider = function(selectedPage) {
							if($rootScope.selectedPage != undefined){
								if($rootScope.selectedPage != "") {
									if($rootScope.selectedPage == "MyAccount/Illustration/0/0"){
										$rootScope.selectedPage = "MyAccount/Illustration"
									}
									if(selectedPage == "MyAccount/eApp/0/0"){
										selectedPage = "MyAccount/eApp/"
									}
									if($rootScope.selectedPage != selectedPage) {
									    IllustratorVariables.illustrationFromKMC = false;
									    IllustratorVariables.illustrationFromIllusPersonal = false;
									 	$scope.selectedPage =  selectedPage;
									 	$scope.getNavigationFlag(function(flag){
									 	    if(!flag){
												$scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
																			   translateMessages($translate, "pageNavigationText"),  
																			   translateMessages($translate, "fna.navok"), 
																			   $scope.okPressed, 
																			   translateMessages($translate, "general.navproceed"), 
																			   $scope.proceedPressed);
									 	    } else {	
//												$rootScope.showHideLoadingImage(true,'loadingMsg', $translate);								
									 	    	$scope.proceedPressed()
									 	    }
										}, function(){
											
										});
									} else if(selectedPage = "landingPage") {
										$rootScope.$broadcast("landingReload");
									}
								}
							}
						}	
							
						$scope.back = function(){
							if(LmsVariables.selectedPage == "Listing Page"){
								$location.path("/landingPage");
								$scope.refresh();
							} else {
								$location.path("/lms");
								$scope.refresh();
							}
						}
							
						$scope.okPressed = function(){
							$scope.slideTog($scope.showSlide);
						}
                            
						$scope.proceedPressed = function(){	
                            if($scope.selectedPage == 'selfTools'){
                               $rootScope.enableRefresh=false;
							   $scope.selectedPage = 'selfTools/selfTools';
							}
							else if($scope.selectedPage == 'aboutGenerali'){
                               $rootScope.enableRefresh=false;
							   $scope.selectedPage = 'selfTools/aboutGenerali';
							}							
							baseUrl  = "/"+$scope.selectedPage;
						    /** UtilityService.leadPage is set to generic listing to hide the goto product list buton
							*   Button will be displayed only in lead specific screen
							*/
						    if($scope.selectedPage.indexOf('Illustration')>-1){
						    	UtilityService.leadPage = 'GenericListing';
							}
							$location.path(baseUrl);
							//$scope.refresh();
							$rootScope.refresh();
						}
                            
						$scope.openPdfFAQ = function(pdfFile) {	
							if((rootConfig.isDeviceMobile) && (!rootConfig.isOfflineDesktop)){
								var fileFullPath = "";
                                window.plugins.LEFileUtils.getApplicationPath(function (path) {
                                    fileFullPath = "file://" + path + "/" + pdfFile;
                                    cordova.exec(function () {}, function () {}, "PdfViewer", "showPdf", [fileFullPath]);
                                }, function (e) {});
                            } else if(((rootConfig.isDeviceMobile) && (rootConfig.isOfflineDesktop)) ||((!rootConfig.isDeviceMobile) && (!rootConfig.isOfflineDesktop))){
                                MediaService.openPDF($scope, $window, pdfFile);
                            }     
	                    }
							
						$scope.openPDF = function(fileName){					          
							var fileFullPath = "";
							window.plugins.LEFileUtils.getApplicationPath(function (path) {
								fileFullPath =  "file://"+path + "/" + fileName;
								cordova.exec(function(){}, function(){}, "PdfViewer", "showPdf", [fileFullPath]);
							},function(e){});
   
						}
						
					  	/***********************************************************
						calls the UpdateErrorCount method of scope
						pass the time as initial argument
						******************************************************************/	
						Object.getPrototypeOf($scope).delayUpdateErrorCount = function() {
							var self = this;
							var time = Array.prototype.splice.call(arguments, 0);
							var args = time.splice(1);
							setTimeout(function(){
								self.updateErrorCount.apply(this,args)
							}, time);
						}

						//Checks for user is online 
                        $scope.checkUserOnline = function() {
                        	var userOnline = true;
                            $scope.OfflineshowPopUpMsg = false;
                            var page = 'salesactivity';
                            if((rootConfig.isDeviceMobile)&& !(rootConfig.isOfflineDesktop)) {
        	                    if (navigator.network.connection.type == Connection.NONE) {
                                    userOnline = false;
                                    $scope.OfflineshowPopUpMsg = true;
                                    $scope.offlineErrorMsg = translateMessages($translate, "salesActivity.connectToInternetMsg");
                                } else {                                        
                                    $scope.selectSlider(page);
                                }
                            } else if(rootConfig.isOfflineDesktop) {
                                if (!navigator.onLine) {
                                    userOnline = false;
                                    $scope.OfflineshowPopUpMsg = true;
                                    $scope.offlineErrorMsg = translateMessages($translate, "salesActivity.connectToInternetMsg");
                                } else {
                                    $scope.selectSlider(page);
                                }
                            } else {
                                $scope.selectSlider(page);
                            }
                        };

                        $scope.closePopup = function () {
                            $scope.OfflineshowPopUpMsg = false;
                        };
						
						/* Moved from GLI_MyAccountController
                         * Because $emit is not getting caught when proceeding
                         * from FNA Module -> Illustration Module to eApp Module Starts.
                         */
                        
                        $rootScope.$on('saveCallback', function(event, iResult){                         	
                            PersistenceMapping.clearTransactionKeys();
							PersistenceMapping.Type = "illustration";
							var transactionObj = PersistenceMapping.mapScopeToPersistence({});
							transactionObj.TransTrackingID = iResult.transTrackingID;
                            if(rootConfig.isDeviceMobile || rootConfig.isOfflineDesktop){
                                 
                            } else {
                                transactionObj.Key3=iResult.illustratorId;
                            }                                
                            //receive the data as second parameter
                            $scope.proceedToeApp(transactionObj);
                            $rootScope.refresh(); 
                        });
                        
                        $scope.proceedToeApp = function(illustrationObject) {
                            //$rootScope.showHideLoadingImage(false);   
							//if(illustrationObject.Key5 != "1"){
							$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
							var updatedDate = new Date();
							var formattedDate = Number(updatedDate.getMonth() + 1) + "-" + Number(updatedDate.getDate() + 5) + "-" + updatedDate.getFullYear();
							if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
								illustrationObject.Key11=$rootScope.username;
								DataService.getListingDetail(illustrationObject, $scope.checkIllustrationStatus, $scope.onGetListingDetailError);
							}else{
								DataService.getListingDetail(illustrationObject, $scope.checkIllustrationStatus, $scope.onGetListingDetailError);
							}
                            $rootScope.showHideLoadingImage(false);                                    
                            //}else{
                            //$scope.lePopupCtrl.showWarning(translateMessages($translate,"lifeEngage"),translateMessages($translate,"CIB1DiscontinuedMsg"),translateMessages($translate,"fna.ok"));
                            //}
						}
                        
                        $scope.checkIllustrationStatus=function(data){
                            if(data!==undefined){
                                if(data[0].Key15!=='Confirmed'){
                                    IllustratorVariables.illustrationStatus = "Confirmed";
							        GLI_IllustratorService.onFieldChange($scope,IllustratorVariables,$rootScope);
							        IllustratorVariables.setIllustratorModel(data[0].TransactionData);
							        $scope.Illustration = IllustratorVariables.getIllustratorModel();
							        IllustratorVariables.transTrackingID = data[0].TransTrackingID;
                                    IllustratorVariables.illustratorKeys.Key5=data[0].Key5;
									IllustratorVariables.agentForGAO = data[0].Key11;
									IllustratorVariables.GAOId = data[0].Key36;
									IllustratorVariables.agentNameForGAO = data[0].Key38;
									IllustratorVariables.agentType= data[0].Key37;
									IllustratorVariables.GAOOfficeCode=data[0].Key35;
                                    $scope.productId=data[0].Key5;
							        $rootScope.transactionId = data[0].Id;
                                    var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
                                    obj.save(function (isAutoSave) {    
                                        $scope.checkAgentStatus(data); 
                                    });
                                }else{											
									$scope.checkAgentStatus(data);
							    }
                            }
                        };
                        
                        $scope.manipulateInsuredData=function(insured){
							if (!insured.IncomeDetails) {
								insured.IncomeDetails = {};
								insured.IncomeDetails.annualIncome = "";
							}
							if (!insured.ContactDetails) {
								insured.ContactDetails = {};
							}
							if (!insured.ContactDetails.currentAddress) {
								insured.ContactDetails.currentAddress = {};
							}
							if(!insured.ContactDetails.birthAddress) {
								insured.ContactDetails.birthAddress = {};
							}								
							if(!insured.BasicDetails.nationality){
								insured.BasicDetails.nationality="";
							}
							if(!insured.ContactDetails.officeNumber){
								insured.ContactDetails.officeNumber={};
							}
							if(!insured.ContactDetails.officeNumber.number){
								insured.ContactDetails.officeNumber.number="";
							}
							
							if(insured.BasicDetails.identityProof==='PP' || insured.BasicDetails.identityProof==='DL' || insured.BasicDetails.identityProof==='GID'){
								insured.BasicDetails.identityProof = "";
								insured.BasicDetails.IDcard="";
							}
							
							if(insured.BasicDetails.identityProof==='CID'){
								if(!rootConfig.isDeviceMobile){
									insured.BasicDetails.IDcard = insured.IdentityDetails.IDcard;
									insured.BasicDetails.identityProof = "";
								}else{
									insured.BasicDetails.IDcard = insured.BasicDetails.IDcard;
									insured.BasicDetails.identityProof = "";
								}	
								
							}
							
							//Will not prepopulate home number from FNA to Eapp -sit bug 1824 fix
							if(insured.ContactDetails.homeNumber1){
								insured.ContactDetails.homeNumber1="";
							}							
                            if(insured.ContactDetails.currentAddress.addressLine1){
								insured.ContactDetails.currentAddress.buildingName=insured.ContactDetails.currentAddress.addressLine1;                                    
							}
							if(insured.BasicDetails.countryofResidence){
								//SIT Bug fix 701 to pre populate country from BI in personal details
								insured.ContactDetails.currentAddress.country = insured.BasicDetails.countryofResidence;
								insured.BasicDetails.countryofResidence="";//SIT Bug fix:984 to clear birthplace field in personal details
							}
                            if(insured.BasicDetails.isInsuredSameAsPayer){
                                if(insured.BasicDetails.illustrationInsSameAsPayer===undefined || insured.BasicDetails.illustrationInsSameAsPayer===""){
                                    insured.BasicDetails.illustrationInsSameAsPayer=insured.BasicDetails.isInsuredSameAsPayer;
                                }
                            }
                            //Occupation Nature of work
                            if(insured.OccupationDetails){
                                if(insured.OccupationDetails.length===1){
                                    if(insured.OccupationDetails[0].occupationCode!=="" && insured.OccupationDetails[0].occupationCode!==undefined){
                                        insured.OccupationDetails[0].natureofWork=insured.OccupationDetails[0].occupationCode;
                                    }                                        
                                    insured.OccupationDetails[0].id=insured.OccupationDetails[0].id;
                                }
                                if(insured.OccupationDetails.length===2){
                                    if(insured.OccupationDetails[0].occupationCode!=="" && insured.OccupationDetails[0].occupationCode!==undefined){
                                        insured.OccupationDetails[0].natureofWork=insured.OccupationDetails[0].occupationCode;
                                    } 
                                    insured.OccupationDetails[0].id=insured.OccupationDetails[0].id;
                                    if(insured.OccupationDetails[1].occupationCode!=="" && insured.OccupationDetails[1].occupationCode!==undefined){
                                        insured.OccupationDetails[1].natureofWork=insured.OccupationDetails[1].occupationCode;
                                    }
                                    insured.OccupationDetails[1].id=insured.OccupationDetails[1].id;
                                } 
                                if(insured.OccupationDetails.length >2){
                                	for(var i=0; i <insured.OccupationDetails.length; i++){
                                		 if(insured.OccupationDetails[i].occupationCode!=="" && insured.OccupationDetails[i].occupationCode!==undefined){
                                        	insured.OccupationDetails[i].natureofWork=insured.OccupationDetails[i].occupationCode;
                                    	} 
                                    	insured.OccupationDetails[i].id=insured.OccupationDetails[i].id;
                                	}
                                }       
                            }
							if(insured.CustomerRelationship===undefined){
                                insured.CustomerRelationship={};
                            }
                            if(insured.CustomerRelationship.relationshipWithProposer=="" || insured.CustomerRelationship.relationshipWithProposer==undefined){
                                insured.CustomerRelationship.isPayorDifferentFromInsured='Yes';
                            }
							return insured;
						}
						
						$scope.manipulatePayerData=function(payer){
							if (!payer.ContactDetails) {
								payer.ContactDetails = {};
							}
							if (!payer.ContactDetails.currentAddress) {
								payer.ContactDetails.currentAddress = {};
							}
							payer.ContactDetails.currentAddress.country = $scope.Illustration.Payer.BasicDetails.countryofResidence;
							
							if (!payer.ContactDetails.birthAddress) {
								payer.ContactDetails.birthAddress = {};
							}
							if(payer.BasicDetails.countryofResidence){
							    //SIT Bug fix 701 to pre populate country from BI in personal details
								payer.ContactDetails.currentAddress.country = payer.BasicDetails.countryofResidence;
								payer.BasicDetails.countryofResidence = "";
							}
							//Will not prepopulate home number from FNA to Eapp -sit bug 1824 fix
							if(payer.ContactDetails.homeNumber1){
								payer.ContactDetails.homeNumber1="";
							}							
							if(!payer.ContactDetails.officeNumber){
								payer.ContactDetails.officeNumber={};
							}
							if(!payer.ContactDetails.officeNumber.number){
								payer.ContactDetails.officeNumber.number="";
							}		
							
							if(payer.BasicDetails.identityProof==='PP' || payer.BasicDetails.identityProof==='DL' || payer.BasicDetails.identityProof==='GID'){
								payer.BasicDetails.identityProof = "";
								payer.BasicDetails.IDcard="";
							}
							
							if(payer.BasicDetails.identityProof==='CID'){
								if(!rootConfig.isDeviceMobile){
									payer.BasicDetails.IDcard = payer.IdentityDetails.IDcard;
									payer.BasicDetails.identityProof = "";
								}else{
									payer.BasicDetails.IDcard = payer.BasicDetails.IDcard;
									payer.BasicDetails.identityProof = "";
								}
							}
							//if(payer.OccupationDetails && payer.OccupationDetails.description && payer.OccupationDetails.description!=""){
							//payer.OccupationDetails.occupationCategory = payer.OccupationDetails.description;
							//insured.OccupationDetails.occupationCategoryValue="";
							//}
							// To DO - change the illustartion flag - Toggle isPayorDifferentFromInsured since isPayorDifferentFromInsured 
							// flag is used differently for business logic implmtn in eapp and illustration
							if ($scope.Illustration.Payer.CustomerRelationship && $scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == "Yes") {
								payer.CustomerRelationship.isPayorDifferentFromInsured = "No";
							} else if ($scope.Illustration.Payer.CustomerRelationship && $scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == "No") {
								payer.CustomerRelationship.isPayorDifferentFromInsured = "Yes";
							}
                            //Occupation Nature of work
                            if(payer.OccupationDetails){
                                if(payer.OccupationDetails.length===1){
                                    if(payer.OccupationDetails[0].occupationCode!=="" && payer.OccupationDetails[0].occupationCode!==undefined){
                                        payer.OccupationDetails[0].natureofWork=payer.OccupationDetails[0].occupationCode;
                                    }                                        
                                    payer.OccupationDetails[0].id=payer.OccupationDetails[0].id;
                                }
                                if(payer.OccupationDetails.length===2){
                                    if(payer.OccupationDetails[0].occupationCode!=="" && payer.OccupationDetails[0].occupationCode!==undefined){
                                        payer.OccupationDetails[0].natureofWork=payer.OccupationDetails[0].occupationCode;
                                    } 
                                    payer.OccupationDetails[0].id=payer.OccupationDetails[0].id;
                                    if(payer.OccupationDetails[1].occupationCode!=="" && payer.OccupationDetails[1].occupationCode!==undefined){
                                        payer.OccupationDetails[1].natureofWork=payer.OccupationDetails[1].occupationCode;
                                    }
                                    payer.OccupationDetails[1].id=payer.OccupationDetails[1].id;
                                } 
                                if(payer.OccupationDetails.length >2){
                                	for(var i=0; i <payer.OccupationDetails.length; i++){
                                		 if(payer.OccupationDetails[i].occupationCode!=="" && payer.OccupationDetails[i].occupationCode!==undefined){
                                        	payer.OccupationDetails[i].natureofWork=payer.OccupationDetails[i].occupationCode;
                                    	} 
                                    	payer.OccupationDetails[i].id=payer.OccupationDetails[i].id;
                                	}
                                }        
                            }
							return payer;
						}
                        
                        $scope.manipulateInsuredRelationshipData=function(insured,payer){
							if(payer.CustomerRelationship){
                                if(payer.CustomerRelationship.relationWithInsured){
                                    insured.CustomerRelationship.relationshipWithProposer=payer.CustomerRelationship.relationWithInsured;
                                }
                            }
							return insured;
						};
						
						$scope.mapIdForOccupationInsured=function(insured){
                            //Occupation Nature of work
                            var tempInsuredObject=angular.copy(insured.OccupationDetails);
                            if(tempInsuredObject.length>1){
                                for(var i=0;i<tempInsuredObject.length;i++){
                                    if(parseInt(tempInsuredObject[i].id)===0 || parseInt(tempInsuredObject[i].id)===2){
                                        insured.OccupationDetails[0]=tempInsuredObject[i];
                                    }
                                    if(parseInt(tempInsuredObject[i].id)===1 || parseInt(tempInsuredObject[i].id)===3){
                                        insured.OccupationDetails[1]=tempInsuredObject[i];
                                    }
                                }
                            }

                            //discard the redumdent details
                            for(var i=insured.OccupationDetails.length - 1; i>=0; i--){
                            	if (undefined !== insured.OccupationDetails[i] && (insured.OccupationDetails[i] == ""  || 
                            		(undefined !== insured.OccupationDetails[i].id && i!==parseInt(insured.OccupationDetails[i].id)))){
                            		insured.OccupationDetails.splice(i,1);
                            	}
                            }
                            return insured;
                        }
                        
                        $scope.mapIdForOccupationPayer=function(payer){
                            //Occupation Nature of work
                            var tempPayerObject=angular.copy(payer.OccupationDetails);
                            if(tempPayerObject.length>1){
                                for(var i=0;i<tempPayerObject.length;i++){
                                    if(parseInt(tempPayerObject[i].id)===0 || parseInt(tempPayerObject[i].id)===2){
                                        payer.OccupationDetails[0]=tempPayerObject[i];
                                    }
                                    if(parseInt(tempPayerObject[i].id)===1 || parseInt(tempPayerObject[i].id)===3){
                                        payer.OccupationDetails[1]=tempPayerObject[i];
                                    }
                                }
                            }

                            
                            for(var i=payer.OccupationDetails.length - 1; i>=0; i--){
                            	if (undefined !== payer.OccupationDetails[i] && (payer.OccupationDetails[i] == ""  || 
                            		(undefined !== payer.OccupationDetails[i].id && i!==parseInt(payer.OccupationDetails[i].id)))){
                            		payer.OccupationDetails.splice(i,1);
                            	}
                            }
                            return payer;
                        }
                        
                        $scope.prepopulateeAppData = function(illustrationTransId,illustrationNumber) {
							UtilityService.previousPage = 'Illustration';
							// Overwrite insured,payer and product details of FNA party with illustration party since
							// illustartion has more fields. Also, there is a chance that insured and proposer are
							// updated in illustration -- Begin An FNA can have multiple illustrations. Hence
							// the insured,proposer and beneficairy selected should be populated from illustration rather
							// than FNA(this will have last created illustraion details)
							// var insured = globalService.getInsured();
							
							/*
							 * manipulateInsuredData,manipulatePayerData,manipulateProductData are functions which is used to manipulate
							 * the data of insured,proposer,product accordingly.
							 * Example:the 'isPayorDifferentFromInsured' is set depending on the data from BI correclty.
							 * The same functions is used in edit flow also to disable the fileds in Eapp, that is coming from BI
							 */
							var insured = $scope.Illustration.Insured;
							insured=$scope.manipulateInsuredData(insured);
							//if(insured.CustomerRelationship.relationShipWithPO == 'Child'){
							//insured.CustomerRelationship.relationShipWithPO = "Child/Guarded Child";
							//}
							var beneficiaries = $scope.Illustration.Beneficiaries;
							var payer = $scope.Illustration.Payer;
							payer=$scope.manipulatePayerData(payer);
                            insured=$scope.manipulateInsuredRelationshipData(insured,payer);
                            insured=$scope.mapIdForOccupationInsured(insured);
                            payer=$scope.mapIdForOccupationPayer(payer);
							
							
							// An FNA can have multiple illustration. Hence
							// the insured,proposer & beneficairy selected
							// should be populated from illustration
							// To set the images from fna parties so as to
							// display in Chooseparties screen
							//And also if in choose party screen ,they are not selected,then as per BI, is should be set
							if (( $scope.fnaId != "" && $scope.fnaId != null ) || ($scope.leadId != "" && $scope.leadId != null)) {
								//debugger;
								globalService.setDefaultParties();
								//globalService.setInsured(insured);
								//prepopulation is not happening in eApp if nothing is choosen in choose party,But insured and proposer are 
								//inputeed in BI. So added function setInsuredFromFNA to set the data correctly.
								//globalService.setPayer(payer);
								globalService.setInsuredFromFNA(insured);
								globalService.setPayerFromFNA(payer);
								globalService.setBenfFromIllustrn(beneficiaries);
							} else {
								var parties = [];
								//var partie = globalService.getParties(); 
								globalService.setParties(parties);
								globalService.setPartyFromillustrtn(insured, true);
								globalService.setPartyFromillustrtn(payer, false);
							}

							var product = angular.copy($scope.Illustration.Product);
							//product=$scope.manipulateProductData(product);
							// Set the values of extra product fields in
							// eApp model
							// To change after confirmation got on the value
							// of sum assured.
							
							//Need to review UB Rich temp fix 
							if($scope.Illustration.IllustrationOutput.PolicyDetails && $scope.Illustration.IllustrationOutput.PolicyDetails.productCode == 9){
								product.policyDetails.singleTopUpPremium = $scope.Illustration.IllustrationOutput.PolicyDetails.singleTopUp;
							}
							
//							if ($scope.Illustration.IllustrationOutput.PolicyDetails && $scope.Illustration.IllustrationOutput.PolicyDetails.productCode == 24) {
//								product.ProductDetails.sumAssured = 65000;
//							} else {
								product.ProductDetails.sumAssured = $scope.Illustration.IllustrationOutput.sumAssured;
							
							product.ProductDetails.initialPremium = $scope.Illustration.Product.policyDetails.premiumAmount;
							product.ProductDetails.premiumTerm = $scope.Illustration.Product.policyDetails.premiumTerm;
							product.ProductDetails.policyTerm = $scope.Illustration.Product.policyDetails.policyTerm;
							product.ProductDetails.totalInitialPremium = $scope.Illustration.IllustrationOutput.totalPremium;
							if ($scope.Illustration.Product.RiderDetails && $scope.Illustration.Product.RiderDetails.length > 0) {
								product.ProductDetails.rider = "Yes";
							} else {
								product.ProductDetails.rider = "No";
							}
//							for(var i=0;i<$scope.Illustration.IllustrationOutput.selectedRiderTbl.length;i++){
//								for(var j=0;j<product.RiderDetails.length;j++){
//									for(var k=0;k<rootConfig.uniqueRiderName.length;k++){
//										if(product.RiderDetails[j].uniqueRiderName == rootConfig.uniqueRiderName[k] && product.RiderDetails[j].uniqueRiderName == $scope.Illustration.IllustrationOutput.selectedRiderTbl[i].uniqueRiderName && $scope.Illustration.IllustrationOutput.selectedRiderTbl[i].productName !== "Dental" && $scope.Illustration.IllustrationOutput.selectedRiderTbl[i].productName !== "OutPatient" ){
//											product.RiderDetails[j].sumInsured = "";
//											product.RiderDetails[j].initialPremium = product.RiderDetails[j].riderPremium;
//											product.RiderDetails[j].riderPlanName = $scope.Illustration.IllustrationOutput.selectedRiderTbl[i].productName;
//											if($scope.Illustration.IllustrationOutput.VGHRider[$scope.Illustration.IllustrationOutput.selectedRiderTbl[i].laNum].isRequired ==="Yes" && $scope.Illustration.IllustrationOutput.VGHRider[$scope.Illustration.IllustrationOutput.selectedRiderTbl[i].laNum].chosenRiderPlanCd){
//												product.RiderDetails[j].shortName = $scope.Illustration.IllustrationOutput.VGHRider[$scope.Illustration.IllustrationOutput.selectedRiderTbl[i].laNum].chosenRiderPlanCd.substring(0,4);
//											}
//											break;
//										}
//									}
//								}
//								
//								
//							}
							globalService.setProduct(product);
							
							// Overwrite insured, payer and product details
							// of FNA party with illustration party since
							// illustartion has more fields. Also, there is
							// a chance that insured and proposer are
							// updated in illustration -- End
							
							//This variable is set to disable the fileds prepopulated from BI and FNA(if done)in Eapp 
                            // starts here 
                            EappVariables.prepopulatedInsuredData=insured;
                            EappVariables.prepopulatedProposerData=payer;
                            EappVariables.prepopulatedBeneficiaryData=$scope.Illustration.Beneficiaries;
                            EappVariables.illustrationOutputData=$scope.Illustration.IllustrationOutput;
							EappVariables.leadName=IllustratorVariables.leadName;
                            // Ends here 

								if(illustrationNumber == ""){
									illustrationNumber = 0;
								}
							// On creating a new illustration from Landing
							// page(Clicking Create New illsutartion
							// button), fnaid won't be there, for which we
							// dont have to display chooseparties screen
							// Even from web - If has fna - go to
							// chooseparties rather than Eapp
								
							// Currently Commenting this section for THAILAND implementation because 
						    // when proceeding from Illustration to eApp choose party screen is not needed.
								
							/*if ($scope.fnaId != "" && $scope.fnaId != null) {
								$location.path('/fnaChooseParties/' +illustrationTransId+"/"+product.id+"/"+illustrationNumber);
								$scope.refresh();
							} else {
								//EappVariables.fnaId = IllustratorVariables.fnaId;
								//EappVariables.leadId =IllustratorVariables.leadId;
							   //EappVariables.illustratorId =IllustratorVariables.illustratorId;
								if(IllustratorVariables.fnaId == ""){
									IllustratorVariables.fnaId = 0;
								}
								if(IllustratorVariables.leadId == ""){
									IllustratorVariables.leadId = 0;
								}
                                $rootScope.showHideLoadingImage(true, 'proceedingToEapp', $translate);
								$location.path('/Eapp/0/0/' + illustrationTransId + '/0/'+IllustratorVariables.leadId+'/'+IllustratorVariables.fnaId+'/'+illustrationNumber);
								$rootScope.refresh();
							}*/
								
							// Added for THAILAND implementation
							if(IllustratorVariables.fnaId == ""){
								IllustratorVariables.fnaId = 0;
							}
							if(IllustratorVariables.leadId == ""){
								IllustratorVariables.leadId = 0;
							}
                            $rootScope.showHideLoadingImage(true, 'proceedingToEapp', $translate);
							$location.path('/Eapp/0/0/' + illustrationTransId + '/0/'+IllustratorVariables.leadId+'/'+IllustratorVariables.fnaId+'/'+illustrationNumber);
							$rootScope.refresh();
							
							//$rootScope.showHideLoadingImage(false); 	
						};
						
						$scope.loadAgentProfile = function() {
							var data = $scope.agentDetails;
							var agentDetail=UserDetailsService.getUserDetailsModel();
							if( data && data.AgentDetails ){
								$rootScope.agentName = data.AgentDetails.agentName;
							}
							var _agentDetail=data["AgentDetails"];				
								
							for (var i = 0; i < rootConfig.agentDetailFields.length; i++) {
								agentDetail[rootConfig.agentDetailFields[i]]=_agentDetail[rootConfig.agentDetailFields[i]];
							}
							UserDetailsService.setUserDetailsModel(agentDetail);
							$scope.refresh();
						};
                        
                        // Method for navigating to eapp click success callback
						$scope.navigateToEapp = function(eAppData, illustrationData){
                            GLI_EappVariables.agentForGAO = illustrationData[0].Key11;
							GLI_EappVariables.GAOId = illustrationData[0].Key36;
							GLI_EappVariables.agentNameForGAO = illustrationData[0].Key38;
							GLI_EappVariables.agentType= illustrationData[0].Key37;
							GLI_EappVariables.GAOOfficeCode = illustrationData[0].Key35;
							GLI_EappVariables.CreatedAuthor=UserDetailsService.getAgentRetrieveData().AgentDetails.agentType;
							$scope.illustrationKey = "";
							if(illustrationData[0].Key1 == "" && illustrationData[0].Key2 != "" ){
								illustrationData[0].Key1 = illustrationData[0].Key2;
								illustrationData[0].Key2 = "";
							}
							if (eAppData && eAppData[0] && eAppData[0].TransactionData) {
								var id1 = 0;
                                var id3 = 0;
                                if(eAppData[0].Key4 == '-' || eAppData[0].Key4 == ""){
                                	eAppData[0].Key4 = 0;
                                 }
                                $scope.LifeEngageProduct = EappVariables.getEappModel();
                                //This variable is set to disable the fileds prepopulated in Eapp 
                                // starts here 
                                $scope.Illustration = illustrationData[0].TransactionData;
                                $scope.LifeEngageProduct.Product.ProductDetails.IllustrationStatus = illustrationData[0].Key15;
								$scope.Illustration.Product.ProductDetails.IllustrationStatus = illustrationData[0].Key15;
                                var insuredData=$scope.manipulateInsuredData($scope.Illustration.Insured);
                                var payerData=$scope.manipulatePayerData($scope.Illustration.Payer);
                                insuredData=$scope.manipulateInsuredRelationshipData(insuredData,payerData);
                                insuredData=$scope.mapIdForOccupationInsured(insuredData);
                                payerData=$scope.mapIdForOccupationPayer(payerData);
                                EappVariables.prepopulatedInsuredData=insuredData;
                                EappVariables.prepopulatedProposerData=payerData;
                                EappVariables.prepopulatedBeneficiaryData=$scope.Illustration.Beneficiaries;
                                EappVariables.illustrationOutputData=$scope.Illustration.IllustrationOutput;
								EappVariables.leadName=illustrationData[0].Key22;
                                // Ends here 
                                var product = angular.copy($scope.Illustration.Product);
                                globalService.setProduct(product);
                                EappVariables.setEappModel($scope.LifeEngageProduct);
                                EappVariables.LastVisitedUrl = $scope.LifeEngageProduct.LastVisitedUrl;
                                if(eAppData[0].Key2 == ""){
                                	eAppData[0].Key2 = 0;
                                }
								if(illustrationData[0].Key3 == ""){
									illustrationData[0].Key3 = 0;
								}
								if($scope.isRDSUser){
									$scope.illustrationKey = illustrationData[0].Key24;
								} else {
									$scope.illustrationKey = illustrationData[0].Key3;
								}
								if(!eAppData[0].Id){
									eAppData[0].Id = 0;
								}
								if(eAppData[0].Key1== ""){
									eAppData[0].Key1 = 0;
								}
                                $location.path('/Eapp/'+eAppData[0].Key5+"/"+eAppData[0].Key4+"/"+eAppData[0].Key3+"/"+eAppData[0].Id+"/"+eAppData[0].Key1+"/"+eAppData[0].Key2+"/"+$scope.illustrationKey);
                            	$rootScope.refresh();
							} else {
								GLI_EappVariables.fromLead=true;
								IllustratorVariables.setIllustratorModel(illustrationData[0].TransactionData);
								$scope.Illustration = IllustratorVariables.getIllustratorModel();
								$scope.fnaId = illustrationData[0].Key2;
								FnaVariables.fnaId = 0;
								IllustratorVariables.leadId=illustrationData[0].Key1;
								IllustratorVariables.fnaId=illustrationData[0].Key2;
								$scope.Illustration.Product.ProductDetails.IllustrationStatus = illustrationData[0].Key15;
								IllustratorVariables.illustratorId = illustrationData[0].TransTrackingID;
								$scope.IllustrationId = (IllustratorVariables.illustratorId != "" && IllustratorVariables.illustratorId != 0) ? IllustratorVariables.illustratorId : illustrationData[0].Id;
								IllustratorVariables.leadName=illustrationData[0].Key22;
								$scope.leadId = illustrationData[0].Key1;
								if($scope.isRDSUser){
									$scope.illustrationKey = illustrationData[0].Key24;
								} else {
									$scope.illustrationKey = illustrationData[0].Key3;
								}
								if ($scope.fnaId != "" && $scope.fnaId != null) {
									// Get FNA details
									// On creating a new illustration from Landing page(Clicking Create New illsutartion button), 
									//fnaid won't be there, for which we don't have to display chooseparties screen
									if ((rootConfig.isDeviceMobile)) {
										// Get the related FNA from db
										DataService.getRelatedFNA($scope.fnaId, function(fnaData) {
											if(fnaData && fnaData[0] && fnaData[0].TransactionData) {
												FnaVariables.setFnaModel({
													'FNA' : JSON.parse(JSON.stringify(fnaData[0].TransactionData))
												});
												$scope.FNAObject = FnaVariables.getFnaModel();
												FnaVariables.transTrackingID = fnaData[0].TransTrackingID;
												FnaVariables.leadId = fnaData[0].Key1;
												globalService.setParties($scope.FNAObject.FNA.parties);
											}
											$scope.prepopulateeAppData($scope.IllustrationId,$scope.illustrationKey);
										}, $scope.onGetListingsError);
									} else {
										PersistenceMapping.clearTransactionKeys();
										PersistenceMapping.Type = "FNA";
										var transactionObj = PersistenceMapping.mapScopeToPersistence({});
										transactionObj.TransTrackingID = $scope.fnaId;
										DataService.getListingDetail(transactionObj, function(fnaData) {
											if (fnaData && fnaData[0] && fnaData[0].TransactionData) {
												FnaVariables.setFnaModel({
													'FNA' : JSON.parse(JSON.stringify(fnaData[0].TransactionData))
												});
												$scope.FNAObject = FnaVariables.getFnaModel();
												FnaVariables.transTrackingID = fnaData[0].TransTrackingID;
												FnaVariables.leadId = fnaData[0].Key1;
												if (fnaData[0].TransactionData.parties) {
													var parties = fnaData[0].TransactionData.parties;
													globalService.setParties(parties);
												}
											}
											$scope.prepopulateeAppData($scope.IllustrationId, $scope.illustrationKey);
										}, $scope.onGetListingsError);
									}
								} else {
									if ($scope.leadId != "" && $scope.leadId != null) {
										if ((rootConfig.isDeviceMobile)) {
											GLI_DataService.getRelatedLMS($scope.leadId, function(leadData) {
												if (leadData && leadData[0] && leadData[0].TransactionData) {
													var parties = [];
													var party = new globalService.party();
													party.BasicDetails = leadData[0].TransactionData.Lead.BasicDetails;
													party.ContactDetails = leadData[0].TransactionData.Lead.ContactDetails;
//													party.OccupationDetails = leadData[0].TransactionData.Lead.OccupationDetails;
													party.id = "myself";
													party.type = "Lead";
													party.isInsured = false;
													party.isPayer = false;
													party.isBeneficiary = false;
													party.isAdditionalInsured = false;
													party.fnaBenfPartyId = "";
													party.fnaInsuredPartyId = "";
													party.fnaPayerPartyId = "";
													parties.push(party);
													globalService.setParties(parties)
												}
												$scope.prepopulateeAppData($scope.IllustrationId,$scope.illustrationKey);
											}, $scope.onGetListingsError);
										} else {
											PersistenceMapping.clearTransactionKeys();
											var searchCriteria = {
													 searchCriteriaRequest:{
											"command" : "RelatedTransactions",
											"modes" :['CHOOSEPARTY'],
											"value" : IllustratorVariables.leadId
											}
											};
											PersistenceMapping.Type = "LMS";
											var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
											transactionObj.TransTrackingID = IllustratorVariables.leadId;
												DataService.getFilteredListing(transactionObj, function(leadData) {
													if (leadData && leadData[0] && leadData[0].TransactionData) {
														var parties = [];
														var party = new globalService.party();
														party.BasicDetails = leadData[0].TransactionData.Lead.BasicDetails;
														party.ContactDetails = leadData[0].TransactionData.Lead.ContactDetails;
	//													party.OccupationDetails = leadData[0].TransactionData.Lead.OccupationDetails;
														party.id = "myself";
														party.type = "Lead";
														party.isInsured = false;
														party.isPayer = false;
														party.isBeneficiary = false;
														party.isAdditionalInsured = false;
														party.fnaBenfPartyId = "";
														party.fnaInsuredPartyId = "";
														party.fnaPayerPartyId = "";
														parties.push(party);
														globalService.setParties(parties)
													}
													$scope.prepopulateeAppData($scope.IllustrationId,$scope.illustrationKey);
												});
										}
									}
									$scope.prepopulateeAppData($scope.IllustrationId,$scope.illustrationKey);
								}
							}
						}
                        
                        $scope.onGetListingDetailSuccess = function(data) {
							var illustrationData = data;
							if ((rootConfig.isDeviceMobile)){
								GLI_DataService.getRelatedEappForBI(illustrationData[0].TransTrackingID, function(eAppData){
									$scope.navigateToEapp(eAppData, illustrationData);
								});
							} else {
								PersistenceMapping.clearTransactionKeys();
								var  searchCriteria = {
										 searchCriteriaRequest:{
								"command" : "RelatedTransactions",
								"modes" :['eApp'],
								"value" : data[0].TransTrackingID
								}
								};
								PersistenceMapping.Type = "eApp";
								var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
								transactionObj.Key1 = data.TransTrackingID;
								transactionObj.filter = "Key15 <> 'Cancelled'";
								DataService.getFilteredListing(transactionObj, function(eAppData) {
									$scope.navigateToEapp(eAppData, illustrationData);
								});
							}
						};
						
						//Validation to check whether any of the Insured Bday in coming 14 days
						 $scope.checkFourteenDays= function(){
                            //Not required for Thailand
							$scope.onGetListingDetailSuccess($scope.eAppData);						
						};
						
						$scope.okBtnAction = function() {
							$scope.eAppData[0].Key15 = "Draft";
							IllustratorVariables.illustrationStatus = "Draft";
							GLI_IllustratorService.onFieldChange($scope,IllustratorVariables,$rootScope);
							IllustratorVariables.setIllustratorModel($scope.eAppData[0].TransactionData);
							$scope.Illustration = IllustratorVariables.getIllustratorModel();
							IllustratorVariables.transTrackingID = $scope.eAppData[0].TransTrackingID;
							$rootScope.transactionId = $scope.eAppData[0].Id;
							var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
							obj.save(function(isAutoSave) {
								//debugger;
								$rootScope.showHideLoadingImage(false);
								$location.path('/Illustrator/'+$scope.eAppData[0].Key5+'/0/0/'+$scope.eAppData[0].Id);
							});
						}
						
						$scope.cancelBtnAction = function() {
							
						}
						 
						//Proceed action from Active Agent to check the Illustration date
						$scope.setAgentInfoAndProceed = function(data){
							 $rootScope.showHideLoadingImage(false);
							 $scope.setAgentInfo(data);
							 var illuDate = $scope.eAppData[0].TransactionData.Product.premiumSummary.validatedDate ;
							 var isExpired = GLI_IllustratorService.checkExpiryDate(illuDate);
							 if (isExpired){
								$rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
																   translateMessages( $translate, "illustrator.expiryDateMessage"), 
																   translateMessages( $translate, "illustrator.okMessageInCaps"), 
																   $scope.okBtnAction, 
																   translateMessages( $translate, "illustrator.cancelMessageInCaps"), 
																   $scope.cancelBtnAction);
							 } else {
								 $scope.checkFourteenDays();
							 }
						};
						
						$scope.setAgentInfo = function(data) {
							var agentData = {
								"agentId" : "",
								"agentName" : "",
								"agencyName" : "",
								"agentHomeNumber" : "",
								"agentMobileNumber" : "",
								"agentEmailID" : "",
								"licenseNumber" : "",
								"securityNo":""
							};
							agentData.agentId = data.AgentDetails.agentCode;
							agentData.agentName = data.AgentDetails.agentName;
							agentData.agencyName = data.AgentDetails.agencyOffice;
							agentData.agentHomeNumber = "";
							agentData.agentMobileNumber = data.AgentDetails.phone;
							agentData.licenseNumber = data.AgentDetails.licenseNumber;
							agentData.securityNo = data.AgentDetails.securityNo;
							agentData.agentEmailID = data.AgentDetails.emailId;
							EappVariables.agentData = agentData;
						}
						
						//OK button action from the Terminated/Suspended Agent
						$scope.proceedWithRetrievedAgentData = function(){
							$scope.setAgentInfoAndProceed($scope.retrievedAgentDetails);
						}
						
						$scope.onRetrieveAgentProfileSuccess = function(data) {
							if(data.AgentDetails.mappedBranches){
								delete data.AgentDetails.mappedBranches;
							}
							if(data.AgentDetails.reportingAgents){
								delete data.AgentDetails.reportingAgents;
							}
							if(data.AgentDetails.reportingRMs){
								delete data.AgentDetails.reportingRMs;
							}
							if(data.AgentDetails.reportingASMs){
								delete data.AgentDetails.reportingASMs;
							}
							$scope.retrievedAgentDetails = data;
							EappVariables.agentType = data.AgentDetails.agentType;
						     if(data.AgentDetails.status == 'Active' || data.AgentDetails.status == 'INFORCE'){
						    	 $scope.setAgentInfoAndProceed($scope.retrievedAgentDetails);
						     } else if(data.AgentDetails.status=='Terminate'){
						    	$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), 
						    									 translateMessages($translate, "illustrator.suspendedOrTerminatedWarning"), 
						    									 translateMessages($translate, "fna.ok"), 
						    									 $scope.proceedWithRetrievedAgentData);
						    	$rootScope.showHideLoadingImage(false, "");
						     } else if(data.AgentDetails.status=='Suspend'){
						    	$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), 
						    									 translateMessages($translate, "illustrator.suspendedOrTerminatedWarning"), 
						    									 translateMessages($translate, "fna.ok"), 
						    									 $scope.proceedWithRetrievedAgentData);
						    	$rootScope.showHideLoadingImage(false, "");
						     } else {
									$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), 
																	 translateMessages($translate, "notActiveMsg"), 
																	 translateMessages($translate, "fna.ok"), 
																	 $scope.okClick);
									$rootScope.showHideLoadingImage(false, "");
							 }
							$rootScope.agentName = data.AgentDetails.agentName;
							$scope.agentDetails =data;
							if(data.AgentDetails.agentType == "IOIS"){
								$scope.mappedBranchesList =data.AgentDetails.mappedBranches;
								if($scope.mappedBranchesList){
									$scope.mappedBranchesList.forEach(function (branches) { 
									   branches["parentAgentCode"] = $scope.agentCode;
									});
								delete data.AgentDetails.mappedBranches;
								}
							}	
							if(data.AgentDetails.agentType == "BranchUser"){
								$scope.reportingAgentsList =data.AgentDetails.reportingAgents;
								/*  adding RM user ID in every Agent Object - for DB save purpose*/
								$scope.reportingAgentsList.forEach(function (agents) { 
								   agents["parentAgentCode"] = $scope.agentCode;
								});
								delete data.AgentDetails.reportingAgents;
							}
							/*TODO :- This Product assignment needs to be removed once more than one product available - same applicable in GLI_Landing Controller*/
							/*if(data.AgentDetails.products.length == 0){
								data.AgentDetails.products =[{"prdCode": "CIB1","prdName": "Critical Illness ENdowment"}];
							}*/
							if(data.AgentDetails.products){	
								if(data.AgentDetails.products.length > 0){
									$scope.applicableProductList = data.AgentDetails.products;
									/*  adding RM user ID in every Product Object - for DB save purpose*/
									$scope.applicableProductList.forEach(function (agents) { 
									   agents["parentAgentCode"] = $scope.agentCode;
									});
									delete data.AgentDetails.products;                   
								}else{
									delete data.AgentDetails.products;     
								}
							}
							if(data.AgentDetails.mappedBranches){
								delete data.AgentDetails.mappedBranches;
							}
							if(data.AgentDetails.reportingAgents){
								delete data.AgentDetails.reportingAgents;
							}
							if(data.AgentDetails.reportingRMs){
								delete data.AgentDetails.reportingRMs;
							}
							if(data.AgentDetails.reportingASMs){
								delete data.AgentDetails.reportingASMs;
							}
							if ((rootConfig.isDeviceMobile)){
								DataService.saveAgentProfile(data,function(){
									$scope.loadAgentProfile(data);
								},function(){

								});										
							}
						};
                        
                        $scope.onRetrieveAgentProfileError = function() {
							$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), 
															 translateMessages($translate, "general.agentProfileRetrieveError"), 
															 translateMessages($translate, "fna.ok"), 
															 $scope.okClick);
							$rootScope.showHideLoadingImage(false, "");
                        };
                        
                        $scope.checkAgentStatus = function(data){
							//EappVariables.eAppProceedingDate = UtilityService.getFormattedDate();
							$scope.eAppData = data;
							if ((rootConfig.isDeviceMobile) || (rootConfig.isOfflineDesktop)) {
								if(checkConnection()){
									PersistenceMapping.clearTransactionKeys();
									var transactionObj = PersistenceMapping.mapScopeToPersistence({});
									AgentService.retrieveAgentProfile(transactionObj, $scope.onRetrieveAgentProfileSuccess, $scope.onRetrieveAgentProfileError);
								}
								else{
									PersistenceMapping.clearTransactionKeys();
									var transactionObj = PersistenceMapping.mapScopeToPersistence({});
									DataService.retrieveAgentDetails(transactionObj, function(data) {
										$scope.retrievedAgentDetails = data[0];
										if(data[0].status == 'Active' || data[0].status == 'INFORCE'){
											 $rootScope.showHideLoadingImage(false);
											 var agentData = {
												"agentId" : "",
												"agentName" : "",
												"agencyName" : "",
												"agentHomeNumber" : "",
												"agentMobileNumber" : "",
												"agentEmailID" : "",
												"licenseNumber":"",
												"securityNo":""
											};
											agentData.agentId = data[0].agentCode;
											agentData.agentName = data[0].agentName;
											agentData.agencyName = data[0].agencyOffice;
											agentData.agentHomeNumber = "";
											agentData.agentMobileNumber = data[0].phone;
											agentData.agentEmailID = data[0].emailId;
											agentData.licenseNumber = data[0].licenseNumber;
											agentData.securityNo = data[0].securityNo;
											EappVariables.agentData = agentData;
											// $scope.setAgentInfo(data);
											var illuDate = $scope.eAppData[0].TransactionData.Product.premiumSummary.validatedDate ;
											var isExpired = GLI_IllustratorService.checkExpiryDate(illuDate);
											if (isExpired) {		
												$rootScope.lePopupCtrl.showWarning(translateMessages( $translate, "lifeEngage"),
																				   translateMessages( $translate, "illustrator.expiryDateMessage"), 
																				   translateMessages( $translate, "illustrator.okMessageInCaps"), 
																				   $scope.okBtnAction, 
																				   translateMessages( $translate, "illustrator.cancelMessageInCaps"), 
																				   $scope.cancelBtnAction);
											 } else {
												$scope.onGetListingDetailSuccess($scope.eAppData);
											 }
										 } else if(data[0].status=='Terminate'){
												$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), 
																				 translateMessages($translate, "suspendedOrTerminatedWarning"), 
																				 translateMessages($translate, "fna.ok"), 
																				 $scope.proceedWithRetrievedAgentData);
												$rootScope.showHideLoadingImage(false, "");
										 } else if(data[0].status=='Suspend'){
												$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), 
																				 translateMessages($translate, "suspendedOrTerminatedWarning"), 
																				 translateMessages($translate, "fna.ok"), 
																				 $scope.proceedWithRetrievedAgentData);
												$rootScope.showHideLoadingImage(false, "");
										 } else{
												$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), 
																				 translateMessages($translate, "notActiveMsg"), 
																				 translateMessages($translate, "fna.ok"), 
																				 $scope.okClick);
												$rootScope.showHideLoadingImage(false, "");
										 }
												//$scope.FNAObject.FNA.AgentDetails = $scope.agent;
									}, $scope.onRetrieveAgentDetailsError);
									$rootScope.showHideLoadingImage(false);
								}
							} else {
								PersistenceMapping.clearTransactionKeys();
								var transactionObj = PersistenceMapping.mapScopeToPersistence({});
								if($scope.isRDSUser) {
									transactionObj.Key11 = $scope.eAppData[0].TransactionData.AgentInfo.agentCode;
								}
								AgentService.retrieveAgentProfile(transactionObj, $scope.onRetrieveAgentProfileSuccess, $scope.onRetrieveAgentProfileError);
							}
						}
							
		} 
]);