/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name : GLI_MyAccountController
 */

storeApp
		.controller(
				'GLI_MyAccountController',
				[		
						'$route',
						'$rootScope',
						'$scope',
						'$compile',
						'$window',
						'$routeParams',
						'DataService',
						'LookupService',
						'DocumentService',
						'$translate',
						'UtilityService',
						'GLI_DataService',
						'IllustratorVariables',
						'IllustratorService',
						'EappVariables',
						'LmsService',
						'GLI_LmsService',
						'$location',
						'PersistenceMapping',
						'FnaVariables',
						'globalService',
						'$controller',
						'LmsVariables',
						'AgentService',
						'UserDetailsService', 
						'GLI_IllustratorService', 
						'ProductService', 
						'GLI_globalService', 
						'GLI_EappVariables', 
						'MediaService', 
						'$filter', 
						'EappService',
						'GLI_EappService',
						function($route, $rootScope, $scope, $compile,$window,
								$routeParams, DataService, LookupService,
								DocumentService, $translate, UtilityService,GLI_DataService,
								IllustratorVariables, IllustratorService,EappVariables,LmsService, GLI_LmsService,$location,
								PersistenceMapping, FnaVariables,
								globalService, $controller,LmsVariables,AgentService,UserDetailsService,GLI_IllustratorService,ProductService,GLI_globalService,GLI_EappVariables,MediaService,$filter,EappService,GLI_EappService) {
							$controller('MyAccountController', {
								$route : $route,
								$rootScope : $rootScope,
								$scope : $scope,
								$compile : $compile,
								$window: $window,
								$routeParams : $routeParams,
								DataService : GLI_DataService,
								LookupService : LookupService,
								DocumentService : DocumentService,
								$translate : $translate,
								UtilityService : UtilityService,
								IllustratorVariables : IllustratorVariables,
								EappVariables : EappVariables,
								LmsService:GLI_LmsService,
								$location : $location,
								PersistenceMapping : PersistenceMapping,
								FnaVariables : FnaVariables,
								globalService : globalService,
								LmsVariables : LmsVariables,
								GLI_globalService:GLI_globalService,
								GLI_EappVariables:GLI_EappVariables
								
								
							});
							$rootScope.hideLanguageSetting = false;
							$scope.appDateTimeFormat = rootConfig.appDateTimeFormat;
							$scope.appDateFormat = rootConfig.appDateFormat;
							$scope.DeleteEappButtonDisable=true;
							$scope.barLimit = 10;
							$scope.dynamicSearchKey = "Proposal number";
							$scope.dynamicSearchProposalKey = "";
							$scope.isRDSUser = UserDetailsService.getRDSUser();
                                                GLI_IllustratorService.setSelectedProposalData();
                            $scope.fileList = rootConfig.selfToolsDocList;
                            $scope.bothSigMissing = false;
							$scope.clientSigMissing = false;
							$scope.agentSigMissing = false;
                            
							if($routeParams.type=='Illustration' && ($routeParams.leadId === undefined || $routeParams.leadId === '0')){
						        $rootScope.selectedPage = "MyAccount/Illustration/0/0";
						    }else{
								$rootScope.selectedPage = "leadSpecificListing";
							}
							if($routeParams.type=='eApp'){
							 	$rootScope.selectedPage = "MyAccount/eApp/0/0";
							}
						    if(!$routeParams.type) {					    	
						    	$rootScope.selectedPage = "selfTools";
                                $scope.subTab = $routeParams.subTab;
						    }
						    
						    
							//$scope.showEmailPopup = false;
							$scope.canClick = true;
							// For checking the visibility of SendEmail Button
							if ($scope.Illustartions.length == 0) {
								$scope.canClick = true;
								$scope.refresh();
							} else {
								$scope.canClick = false;
								$scope.refresh();
							}
							$scope.getLeadDetailsForIllustration = function(){
								data = IllustratorVariables.leadData;
								LmsService.getLead(function(data) {
									var parties = [];
									var party = new globalService.party();
									var dataOfLead = LmsVariables.getLmsModel().Lead;
									party.ContactDetails.mobileNumber1 = dataOfLead.ContactDetails.mobileNumber1;
									//party.ContactDetails.emailID = dataOfLead.ContactDetails.emailID;
									//party.OccupationDetails= dataOfLead.OccupationDetails.occupationCategory;
									party.ContactDetails= dataOfLead.ContactDetails;
									party.BasicDetails.firstName = dataOfLead.BasicDetails.fullName;
									party.ContactDetails.homeNumber1 = dataOfLead.ContactDetails.homeNumber1; // data.Key4;
									party.ContactDetails.emailId = data.Key20;
									party.BasicDetails.lastName = data.Key3;
	
									party.BasicDetails.dob = dataOfLead.BasicDetails.dob;
									party.BasicDetails.gender = dataOfLead.BasicDetails.gender;
									party.ContactDetails.currentAddress.houseNo = dataOfLead.ContactDetails.currentAddress.addressLine1;
									party.ContactDetails.currentAddress.street = dataOfLead.ContactDetails.currentAddress.streetName;
									party.ContactDetails.currentAddress.district = dataOfLead.ContactDetails.currentAddress.city;
									party.ContactDetails.currentAddress.city = dataOfLead.ContactDetails.currentAddress.state;
									party.ContactDetails.currentAddress.zipCode = dataOfLead.ContactDetails.currentAddress.zipCode;
									//Comment this country mapping since country is mapping to basicdetails
									//party.ContactDetails.birthAddress.country = dataOfLead.ContactDetails.currentAddress.country;
									// occupationCategory For occupation code  & occupationCategoryValue for occupation category name
//									party.OccupationDetails.occupationCategory = dataOfLead.OccupationDetails.occupationCategory; 
//									party.OccupationDetails.occupationCategoryValue = dataOfLead.OccupationDetails.occupationCategoryValue;
//									party.OccupationDetails.description = dataOfLead.OccupationDetails.description;
//									party.OccupationDetails.jobCode = dataOfLead.OccupationDetails.jobCode;
//									party.OccupationDetails.jobClass = dataOfLead.OccupationDetails.jobCode;
//									party.OccupationDetails.descriptionOthers = dataOfLead.OccupationDetails.descriptionOthers;
									
									party.BasicDetails.incomeRange = dataOfLead.BasicDetails.incomeRange;
									party.BasicDetails.nationality = dataOfLead.BasicDetails.nationality;
									party.BasicDetails.countryofResidence = dataOfLead.ContactDetails.currentAddress.country;
									party.BasicDetails.identityProof = dataOfLead.BasicDetails.identityProof;
									
									
									if(dataOfLead.BasicDetails.gender == 'NotMentioned'){
										//dataOfLead.BasicDetails.gender = "";
										party.BasicDetails.gender= "";
									} else {
										party.BasicDetails.gender= dataOfLead.BasicDetails.gender;
									}
									
									//party.BasicDetails.dob =data.Key7; to be used
									party.BasicDetails.dob =dataOfLead.BasicDetails.dob;
									party.type = "Lead";
									parties.push(party);
									globalService.setParties(parties);
									createNewIllustrationContinue();
								});
							}
							// Setting the status to draft on creating a new
							// illustration
							/*$scope.createNewIllustration = function() {
								IllustratorVariables.clearIllustrationVariables();
								IllustratorVariables.illustrationFromKMC= true;
								IllustratorVariables.setIllustratorModel(null);
								IllustratorVariables
										.setIllustrationAttachmentModel(null);
								IllustratorVariables.fnaId = "";
								IllustratorVariables.leadEmailId = LmsVariables.lmsKeys.Key20;
								IllustratorVariables.leadName = LmsVariables.lmsKeys.Key2;	
								IllustratorVariables.illustrationStatus = "Draft";
								$location.path('/products/0/0/0/0/false');
							}*/
							$scope.createNewIllustration = function() {
								$rootScope.declinedRCCADB = false;
								$rootScope.declinedRCCAI = false;
								$rootScope.declinedRCCADD = false;
								if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
									$scope.markAsComplete_unlockEapp_newIllustrate('illustration');
								}else{
									/* FNA changes by LE Team for Bug 4166 starts */
									EappVariables.fromFNA=false;
									/* FNA changes by LE Team for Bug 4166 ends */
									 /*FNA changes by LE Team >>> starts - Bug 4194*/
									FnaVariables.flagFromCustomerProfileFNA = false;
									 /*FNA changes by LE Team >>> ends - Bug 4194*/
									$scope.selectedProposal = [];
									createNewIllustrationContinue();
								}
							};
							//common function to create new illustration
							function createNewIllustrationContinue(){
								$rootScope.transactionId = 0;
								IllustratorVariables.isChoosePartyEnabled = false;
								if (IllustratorVariables.fnaId == 0) {
									IllustratorVariables.clearIllustrationVariables();
								}
                                GLI_IllustratorService.setSelectedProposalData();
                                if($scope.selectedProposal && $scope.selectedProposal.length) {
                                  GLI_IllustratorService.setSelectedProposalData($scope.selectedProposal);  
                                }
								$location.path('/products/0/0/0/0/false');
							};
							$scope.statuspopover = function(index,key) {
                                
                                
                                
                                if(key == "Pending Submission"){
                                    
                                

								var top = angular.element('#pending-submission-'+index).offset().top - 200;
								if(angular.isDefined($scope.prevIndex)){

									$scope.closestatus($scope.prevIndex);

								}
								
								
								
								$scope.prevIndex = index;		
								angular.element("#myPopup_"+index).css({'top':  top+'px'});
                                var element = document.getElementById("myPopup_"+index);
                                    element.classList.add("popup");
                                }
							};
                            
                             $scope.closestatus = function(index) {
								
                                var close = document.getElementById("myPopup_"+index);
								     close.classList.remove("popup");
							};
							$scope.OverlayPopoverFunction = function() {
								$rootScope.alertObj.showPopup = true;
								$rootScope.refresh();
								$scope.overflowClass = true;
							};
							$scope.closeOverlay = function() {
								$scope.overflowClass = false;
							};
							
							$scope.translateErrorMesgs = function(msg) {
								$scope.translatedMsg = translateMessages(
										$translate, msg);
								return $scope.translatedMsg;
							}
							
							//copy functionality
							$scope.proceedToCopy = function() {
								$scope.checkProposals();
							}
							
							$scope.eappEditPage   = function(data) {
								
							angular.element(document.querySelector(".site-overlay")).addClass("hideSplitOne");
							var divElement = angular.element(document.querySelector('.hideSplitOne'));
							var htmlElement = angular.element('<span class="loadmsg">Loading..<span>');
							divElement.append(htmlElement);
							$compile(divElement)($scope); 

							setTimeout(function () {
								angular.element(document.querySelector(".site-overlay")).removeClass("hideSplitOne");
							},2000)
								
								
								if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO){
									if(data.Key36==""){
										$scope.lockEappToGAO(data);
									}else if(data.Key36!=UserDetailsService.getAgentRetrieveData().AgentDetails.agentCode){
										$rootScope.showHideLoadingImage(false);
										$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"),translateMessages($translate,"eapp.unlockDiffAgentError"),translateMessages($translate,"fna.ok"));
									}else {
										/*FNA changes made by LE Team for Bug 4166 Starts */
										EappVariables.fromFNA=false;
										/*FNA changes made by LE Team for Bug 4166 Ends */
										proceedEappEditPage(data);
									}
								}else{
									/*FNA changes made by LE Team for Bug 4166 Starts */
									EappVariables.fromFNA=false;
									/*FNA changes made by LE Team for Bug 4166 Ends */
									proceedEappEditPage(data);
								}
							};
                            
                            $scope.mapIdForOccupationInsured=function(insured){
                                //Occupation Nature of work
                                var tempInsuredObject=angular.copy(insured.OccupationDetails);
                                if(tempInsuredObject.length>=1){
                                    insured.OccupationDetails=[];
                                    for(var i=0;i<tempInsuredObject.length;i++){
                                        if(parseInt(tempInsuredObject[i].id)===0 || parseInt(tempInsuredObject[i].id)===2){
                                            insured.OccupationDetails[0]=tempInsuredObject[i];
                                        }
                    
                                        if(parseInt(tempInsuredObject[i].id)===1 || parseInt(tempInsuredObject[i].id)===3){
                                            insured.OccupationDetails[1]=tempInsuredObject[i];
                                        }
                                    }
                                }
                                
                                return insured;
                            }
                            
                            $scope.mapIdForOccupationPayer=function(payer){
                                //Occupation Nature of work
                                var tempPayerObject=angular.copy(payer.OccupationDetails);
                                if(tempPayerObject.length>=1){
                                    payer.OccupationDetails=[];
                                    for(var i=0;i<tempPayerObject.length;i++){
                                        if(parseInt(tempPayerObject[i].id)===0 || parseInt(tempPayerObject[i].id)===2){
                                            payer.OccupationDetails[0]=tempPayerObject[i];
                                        }
                    
                                        if(parseInt(tempPayerObject[i].id)===1 || parseInt(tempPayerObject[i].id)===3){
                                            payer.OccupationDetails[1]=tempPayerObject[i];
                                        }
                                    }
                                }
                                
                                return payer;
                            }
                            
							function proceedEappEditPage(data){
								$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
								$scope.refresh();
								$rootScope.fromMemo=false;
								GLI_EappVariables.memoCount = data.Key30;
								GLI_EappVariables.agentForGAO = data.Key11;
								GLI_EappVariables.GAOId = data.Key36;
								GLI_EappVariables.agentNameForGAO = data.Key38;
								GLI_EappVariables.agentType= data.Key37;
								GLI_EappVariables.GAOOfficeCode = data.Key35;
								GLI_EappVariables.CreatedAuthor=UserDetailsService.getAgentRetrieveData().AgentDetails.agentType;
								if(data.Key4 == '-' || data.Key4 == ""){
									  data.Key4 = 0;
								 }
                               if(data.Key1 == ""){
                                    data.Key1 = 0;
                               }
                               if(data.Key2 == ""){
                                    data.Key2 = 0;
                               }
							   if(data.Key3 == ""){
                                    data.Key3 = 0;
                               }
                               if((rootConfig.isDeviceMobile)){
	                               GLI_DataService.getRelatedBIForEapp(data.Key3,function(IllustrationData){
	                                	//This variable is set to disable the fileds prepopulated in Eapp 
		                                // starts here 
										if( IllustrationData && IllustrationData[0] ){
											$scope.Illustration = IllustrationData[0].TransactionData;
											var insuredData=$scope.manipulateInsuredData($scope.Illustration.Insured);
											var payerData=$scope.manipulatePayerData($scope.Illustration.Payer);
                                            insuredData=$scope.manipulateInsuredRelationshipData(insuredData,payerData);
                                            insuredData=$scope.mapIdForOccupationInsured(insuredData);
                                            payerData=$scope.mapIdForOccupationPayer(payerData);
											EappVariables.prepopulatedInsuredData=insuredData;
											EappVariables.prepopulatedProposerData=payerData;
											EappVariables.prepopulatedBeneficiaryData=$scope.Illustration.Beneficiaries;
                                            EappVariables.illustrationOutputData=$scope.Illustration.IllustrationOutput;
											if(IllustratorVariables.leadName==""){
												IllustratorVariables.leadName = IllustrationData[0].Key22;
											}
											EappVariables.leadName=IllustratorVariables.leadName;
										}
		                                // Ends here 
	                     
	                                	$scope.LifeEngageProduct = EappVariables.getEappModel();
	                                    EappVariables.LastVisitedUrl = $scope.LifeEngageProduct.LastVisitedUrl;
	                                    if(IllustrationData[0].Key3==""){
											IllustrationData[0].Key3= 0;
										}
	                                    $location.path('/Eapp/'+data.Key5+"/"+data.Key4+"/"+data.Key3+"/"+data.Id+"/"+data.Key1+"/"+data.Key2+"/"+IllustrationData[0].Key3);
	                                    
										$scope.refresh();
										// id3 always 0 id4 is Epp Id
	                               });
                               }
                               else {
									if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
										data.Key11=$rootScope.username;
										DataService.getListingDetail(data, $scope.onGetEappListingsSuccess, $scope.onGetListingsError);
									}else{
										DataService.getListingDetail(data, $scope.onGetEappListingsSuccess, $scope.onGetListingsError);
									}
                               }
							};
							
							$scope.onGetEappListingsSuccess = function(data){
								if( data[0].TransactionData ){
									$scope.Illustration = data[0].TransactionData;
									var insuredData=$scope.manipulateInsuredData($scope.Illustration.Insured);
									var payerData=$scope.manipulatePayerData($scope.Illustration.Payer);
                                    insuredData=$scope.manipulateInsuredRelationshipData(insuredData,payerData);
                                    insuredData=$scope.mapIdForOccupationInsured(insuredData);
                                    payerData=$scope.mapIdForOccupationPayer(payerData);
									EappVariables.prepopulatedInsuredData=insuredData;
									EappVariables.prepopulatedProposerData=payerData;
									EappVariables.prepopulatedBeneficiaryData=$scope.Illustration.Beneficiaries;
									EappVariables.leadName=IllustratorVariables.leadName;
								}
                                // Ends here 
								if(data[0].Key1 == ""){
									data[0].Key1 = 0;
								}
								if(data[0].Key2 == ""){
									data[0].Key2 = 0;
								}
								if(data[0].Key3 == ""){
									data[0].Key3 = 0;
								}								
								if(data[0].Id == "-" || data[0].Id == ""){
									data[0].Id = 0;
								}
                            	$scope.LifeEngageProduct = EappVariables.getEappModel();
                                EappVariables.LastVisitedUrl = $scope.LifeEngageProduct.LastVisitedUrl;
                                $location.path('/Eapp/'+data[0].Key5+"/"+data[0].Key4+"/"+data[0].Key3+"/"+data[0].Id+"/"+data[0].Key1+"/"+data[0].Key2+"/"+data[0].Key3);
                                
								$scope.refresh();
							}
							
							$scope.checkProposals = function(selectedProposal) {
                                /*$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);*/
								if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
										$scope.selectedProposal[0].Key11=$scope.selectedProposal[0].Key36;
								}
								var index = 0;
								if($scope.selectedProposal.length > 0){
									if($scope.selectedProposal.length > 1){
//										$rootScope.lePopupCtrl.showError(translateMessages(
//												$translate, "lifeEngage"), translateMessages(
//												$translate, "selectOneProposal"),
//												translateMessages($translate, "illustrator.ok"),
//												$scope.rootedOK);
                                        $scope.leadDetShowPopUpMsg = true;
                                        $scope.enterMandatory = translateMessages($translate, "selectOneProposal");
									}else{
                                        if($scope.selectedProposal[0].Key15 != "Draft"){
                                            $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
                                            $scope.cloneList(index);
                                        }else{
                                            $scope.cloneList(index);
                                        }
									}
								}else{
//									$rootScope.lePopupCtrl.showError(translateMessages(
//											$translate, "lifeEngage"), translateMessages(
//											$translate, "selectProposal"),
//											translateMessages($translate, "illustrator.ok"),
//											$scope.rootedOK);
                                    $scope.leadDetShowPopUpMsg = true;
                                    $scope.enterMandatory = translateMessages($translate, "selectProposal");
								}
							}
							$scope.cloneList = function(index) {
								var noOfProposals = $scope.selectedProposal.length;
								if( noOfProposals > index){
									PersistenceMapping.mapPersistenceToScope($scope.selectedProposal);
									IllustratorService.cloneListing(index,$scope,$scope.selectedProposal[index]);
								}else{
									$scope.selectedProposal = [];
									$rootScope.showHideLoadingImage(false);
									$scope.initialLoad();//has to be changed with LE functionality
								}
								
						   }
							
							
							$scope.illustrationStatusChecking = function(status) {						
								var illustrationStatusForSubmission = (status == "Confirmed") ? false: true;								
								//'Proceed to eApp' button is disabled for the current release
								illustrationStatusForSubmission = true;
								return illustrationStatusForSubmission;
							}	
							
							$scope.okBtnAction = function() {
								$scope.eAppData[0].Key15 = "Draft";
								IllustratorVariables.illustrationStatus = "Draft";
								GLI_IllustratorService.onFieldChange($scope,IllustratorVariables,$rootScope);
								IllustratorVariables.setIllustratorModel($scope.eAppData[0].TransactionData);
								$scope.Illustration = IllustratorVariables.getIllustratorModel();
								IllustratorVariables.transTrackingID = $scope.eAppData[0].TransTrackingID;
								$rootScope.transactionId = $scope.eAppData[0].Id;
								var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
								obj.save(function(isAutoSave) {
									$rootScope.showHideLoadingImage(false);
									$location.path('/Illustrator/'+$scope.eAppData[0].Key5+'/0/0/'+$scope.eAppData[0].Id);
								});
							}
							
							$scope.cancelBtnAction = function() {
								
							}						
							
							$scope.orderValues = function(key, e) {
								if(key=="Key3"){
									for (var indx in $scope.Illustartions){
										if($scope.Illustartions[indx].Key3 != ""){
											if($scope.Illustartions[indx].Key3=parseInt($scope.Illustartions[indx].Key3));
										}
									}
								}
								/* 
									$scope.predicate = key;
									$scope.reverse = !($scope.reverse); 
								*/
								if ($scope.predicate == key) {
									$scope.reverse = !($scope.reverse);
								} else {
									$scope.tblhead1 = false;
									$scope.tblhead2 = false;
									$scope.tblhead3 = false;
									$scope.tblhead4 = false;
									$scope.tblhead5 = false;
									$scope.tblhead6 = false;
									$scope.tblhead7 = false;
									$scope.tblhead8 = false;
									$scope.tblhead9 = false;
									$scope.tblhead10 = false;
									$scope.tblhead11 = false;
									$scope.tblhead12 = false;
									$scope.reverse = true;
								}
								$scope.predicate = key;
							}
							$scope.toggleSorter = function(id){
								/*var leftModel = '$scope.tblhead'+id;
								var rightModel = '!$scope.tblhead'+id;
								var rightValue = eval (rightModel);
								eval (leftModel + "=" + rightValue);*/
								var rightValue = !$scope['tblhead'+id];
								$scope['tblhead'+id] = rightValue;
							}
							
							$scope.manipulateInsuredData=function(insured){
								if (!insured.IncomeDetails) {
									insured.IncomeDetails = {};
									insured.IncomeDetails.annualIncome = "";
								}
								if (!insured.ContactDetails) {
									insured.ContactDetails = {};
								}
								if (!insured.ContactDetails.currentAddress) {
									insured.ContactDetails.currentAddress = {};
								}
								if(!insured.ContactDetails.birthAddress) {
									insured.ContactDetails.birthAddress = {};
								}								
								if(!insured.BasicDetails.nationality){
									insured.BasicDetails.nationality="";
								}
								if(!insured.ContactDetails.officeNumber){
									insured.ContactDetails.officeNumber={};
								}
								if(!insured.ContactDetails.officeNumber.number){
									insured.ContactDetails.officeNumber.number="";
								}
								//Will not prepopulate home number from FNA to Eapp -sit bug 1824 fix
								if(insured.ContactDetails.homeNumber1){
									insured.ContactDetails.homeNumber1="";
								}
								if(insured.BasicDetails.identityProof==='PP' || insured.BasicDetails.identityProof==='DL' || insured.BasicDetails.identityProof==='GID'){
									insured.BasicDetails.identityProof = "";
									insured.BasicDetails.IDcard="";
								}
							
								if(insured.BasicDetails.identityProof==='CID'){
									if(!rootConfig.isDeviceMobile){
										insured.BasicDetails.IDcard = insured.IdentityDetails.IDcard;
										insured.BasicDetails.identityProof = "";
									}else{
										insured.BasicDetails.IDcard = insured.BasicDetails.IDcard;
										insured.BasicDetails.identityProof = "";
									}	

								}
//								if(insured.BasicDetails.identityProof){
//									insured.BasicDetails.identityProof = "";
//								}
								
                                if(insured.ContactDetails.currentAddress.addressLine1){
									insured.ContactDetails.currentAddress.buildingName=insured.ContactDetails.currentAddress.addressLine1;                                    
								}
								if(insured.BasicDetails.countryofResidence){
									//SIT Bug fix 701 to pre populate country from BI in personal details
									insured.ContactDetails.currentAddress.country = insured.BasicDetails.countryofResidence;
									insured.BasicDetails.countryofResidence="";//SIT Bug fix:984 to clear birthplace field in personal details
								}
                                
                                if(insured.BasicDetails.isInsuredSameAsPayer){
                                    if(insured.BasicDetails.illustrationInsSameAsPayer===undefined || insured.BasicDetails.illustrationInsSameAsPayer===""){
                                        insured.BasicDetails.illustrationInsSameAsPayer=insured.BasicDetails.isInsuredSameAsPayer;
                                    }
                                }
								
                                //Occupation Nature of work
                                if(insured.OccupationDetails){
                                    if(insured.OccupationDetails.length>=1){
                                        for(var z=0;z<insured.OccupationDetails.length;z++){
                                            if(insured.OccupationDetails[z].occupationCode!=="" && insured.OccupationDetails[z].occupationCode!==undefined){
                                                insured.OccupationDetails[z].natureofWork=insured.OccupationDetails[z].occupationCode;
                                                insured.OccupationDetails[z].id=insured.OccupationDetails[z].id;
                                            } 
                                        }
                                        
                                    }        
                                }
                                
								if(insured.CustomerRelationship===undefined){
                                    insured.CustomerRelationship={};
                                }
                                if(insured.CustomerRelationship.relationshipWithProposer=="" || insured.CustomerRelationship.relationshipWithProposer==undefined){
                                    insured.CustomerRelationship.isPayorDifferentFromInsured='Yes';
                                }
								return insured;
							}
							
							$scope.manipulatePayerData=function(payer){
								
								if (!payer.ContactDetails) {
									payer.ContactDetails = {};
								}
								if (!payer.ContactDetails.currentAddress) {
									payer.ContactDetails.currentAddress = {};
								}
								payer.ContactDetails.currentAddress.country = $scope.Illustration.Payer.BasicDetails.countryofResidence;
								
								if (!payer.ContactDetails.birthAddress) {
									payer.ContactDetails.birthAddress = {};
								}
								if(payer.BasicDetails.countryofResidence){
								    //SIT Bug fix 701 to pre populate country from BI in personal details
									payer.ContactDetails.currentAddress.country = payer.BasicDetails.countryofResidence;
									payer.BasicDetails.countryofResidence = "";
								}
								//Will not prepopulate home number from FNA to Eapp -sit bug 1824 fix
								if(payer.ContactDetails.homeNumber1){
									payer.ContactDetails.homeNumber1="";
								}
								
								if(payer.BasicDetails.identityProof==='PP' || payer.BasicDetails.identityProof==='DL' || payer.BasicDetails.identityProof==='GID'){
									payer.BasicDetails.identityProof = "";
									payer.BasicDetails.IDcard="";
								}
							
								if(payer.BasicDetails.identityProof==='CID'){
									if(!rootConfig.isDeviceMobile){
										payer.BasicDetails.IDcard = payer.IdentityDetails.IDcard;
										payer.BasicDetails.identityProof = "";
									}else{
										payer.BasicDetails.IDcard = payer.BasicDetails.IDcard;
										payer.BasicDetails.identityProof = "";
									}
								}
								
//								if(payer.BasicDetails.identityProof){
//									payer.BasicDetails.identityProof = "";
//								}
								if(!payer.ContactDetails.officeNumber){
									payer.ContactDetails.officeNumber={};
								}
								if(!payer.ContactDetails.officeNumber.number){
									payer.ContactDetails.officeNumber.number="";
								}								
								
//								if(payer.OccupationDetails && payer.OccupationDetails.description && payer.OccupationDetails.description!=""){
//									payer.OccupationDetails.occupationCategory = payer.OccupationDetails.description;
//									//insured.OccupationDetails.occupationCategoryValue="";
//								}
								// To DO - change the illustartion flag - Toggle
								// isPayorDifferentFromInsured since
								// isPayorDifferentFromInsured flag is used
								// differently for business logic implmtn in
								// eapp and illustration
								if ($scope.Illustration.Payer.CustomerRelationship && $scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == "Yes") {
									payer.CustomerRelationship.isPayorDifferentFromInsured = "No";
								} else if ($scope.Illustration.Payer.CustomerRelationship && $scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == "No") {
									payer.CustomerRelationship.isPayorDifferentFromInsured = "Yes";
								}

                                //Occupation Nature of work
                                if(payer.OccupationDetails){
                                    if(payer.OccupationDetails.length>=1){
                                        for(var z=0;z<payer.OccupationDetails.length;z++){
                                            if(payer.OccupationDetails[z].occupationCode!=="" && payer.OccupationDetails[z].occupationCode!==undefined){
                                                payer.OccupationDetails[z].natureofWork=payer.OccupationDetails[z].occupationCode;
                                                payer.OccupationDetails[z].id=payer.OccupationDetails[z].id;
                                            } 
                                        }
                                        
                                    }        
                                }
                                
								return payer;
							}
                            
                            $scope.manipulateInsuredRelationshipData=function(insured,payer){
								if(payer.CustomerRelationship){
                                    if(payer.CustomerRelationship.relationWithInsured){
                                        insured.CustomerRelationship.relationshipWithProposer=payer.CustomerRelationship.relationWithInsured;
                                    }
                                }
								return insured;
							}
							
							/*$scope.manipulateProductData=function(product){
								// Set the values of extra product fields in
								// eApp model
								// To change after confirmation got on the value
								// of sum assured.
								if ($scope.Illustration.IllustrationOutput.PolicyDetails && $scope.Illustration.IllustrationOutput.PolicyDetails.productCode == 24) {
									product.ProductDetails.sumAssured = 65000;
								} else {
									product.ProductDetails.sumAssured = $scope.Illustration.IllustrationOutput.sumAssured;
								}
								product.ProductDetails.initialPremium = $scope.Illustration.Product.policyDetails.committedPremium;
								product.ProductDetails.premiumTerm = $scope.Illustration.Product.policyDetails.premiumPayingTerm;
								product.ProductDetails.policyTerm = $scope.Illustration.Product.policyDetails.policyTerm;
								if ($scope.Illustration.Product.RiderDetails && $scope.Illustration.Product.RiderDetails.length > 0) {
									product.ProductDetails.rider = "Yes";
								} else {
									product.ProductDetails.rider = "No";
								}
								return product;
							}*/
							
							$scope.prepopulateeAppData = function(illustrationTransId,illustrationNumber) {
								UtilityService.previousPage = 'Illustration';
								// Overwrite insured,payer and product details
								// of FNA party with illustration party since
								// illustartion has more fields. Also, there is
								// a chance that insured and proposer are
								// updated in illustration -- Begin
								// An FNA can have multiple illustrations. Hence
								// the insured,proposer and beneficairy selected
								// should be populated from illustration rather
								// than FNA(this will have last created
								// illustraion details)
								// var insured = globalService.getInsured();
								
								/*
								 * manipulateInsuredData,manipulatePayerData,manipulateProductData are functions which is used to manipulate
								 * the data of insured,proposer,product accordingly.
								 * Example:the 'isPayorDifferentFromInsured' is set depending on the data from BI correclty.
								 * The same functions is used in edit flow also to disable the fileds in Eapp, that is coming from BI
								 */
								var insured = $scope.Illustration.Insured;
								insured=$scope.manipulateInsuredData(insured);
//								if(insured.CustomerRelationship.relationShipWithPO == 'Child'){
//									insured.CustomerRelationship.relationShipWithPO = "Child/Guarded Child";
//								}
								var beneficiaries = $scope.Illustration.Beneficiaries;
								
								var payer = $scope.Illustration.Payer;
								payer=$scope.manipulatePayerData(payer);
                                insured=$scope.manipulateInsuredRelationshipData(insured,payer);
                                insured=$scope.mapIdForOccupationInsured(insured);
                                payer=$scope.mapIdForOccupationPayer(payer);
								
								
								// An FNA can have multiple illustration. Hence
								// the insured,proposer & beneficairy selected
								// should be populated from illustration
								// To set the images from fna parties so as to
								// display in Chooseparties screen
								//And also if in choose party screen ,they are not selected,then as per BI, is should be set
								if (( $scope.fnaId != "" && $scope.fnaId != null ) || ($scope.leadId != "" && $scope.leadId != null)) {
									//debugger;
									globalService.setDefaultParties();
									//globalService.setInsured(insured);//prepopulation is not happening in eApp if nothing is choosen in choose party,But insured and proposer are inputeed in BI.
																		//So added function setInsuredFromFNA to set the data correctly.
									//globalService.setPayer(payer);
									globalService.setInsuredFromFNA(insured);
									globalService.setPayerFromFNA(payer);
									globalService.setBenfFromIllustrn(beneficiaries);
								} else {
									
									var parties = [];
									//var partie = globalService.getParties(); 
									globalService.setParties(parties);
									globalService.setPartyFromillustrtn(insured, true);
									globalService.setPartyFromillustrtn(payer, false);
								}

								var product = angular.copy($scope.Illustration.Product);
								//product=$scope.manipulateProductData(product);
								// Set the values of extra product fields in
								// eApp model
								// To change after confirmation got on the value
								// of sum assured.
								
								//Need to review UB Rich temp fix 
								if($scope.Illustration.IllustrationOutput.PolicyDetails && $scope.Illustration.IllustrationOutput.PolicyDetails.productCode == 9){
									product.policyDetails.singleTopUpPremium = $scope.Illustration.IllustrationOutput.PolicyDetails.singleTopUp;
								}
								
//								if ($scope.Illustration.IllustrationOutput.PolicyDetails && $scope.Illustration.IllustrationOutput.PolicyDetails.productCode == 24) {
//									product.ProductDetails.sumAssured = 65000;
//								} else {
									product.ProductDetails.sumAssured = $scope.Illustration.IllustrationOutput.sumAssured;
								
								product.ProductDetails.initialPremium = $scope.Illustration.Product.policyDetails.premiumAmount;
								product.ProductDetails.premiumTerm = $scope.Illustration.Product.policyDetails.premiumTerm;
								product.ProductDetails.policyTerm = $scope.Illustration.Product.policyDetails.policyTerm;
								product.ProductDetails.totalInitialPremium = $scope.Illustration.IllustrationOutput.totalPremium;
								if ($scope.Illustration.Product.RiderDetails && $scope.Illustration.Product.RiderDetails.length > 0) {
									product.ProductDetails.rider = "Yes";
								} else {
									product.ProductDetails.rider = "No";
								}
//								for(var i=0;i<$scope.Illustration.IllustrationOutput.selectedRiderTbl.length;i++){
//									for(var j=0;j<product.RiderDetails.length;j++){
//										for(var k=0;k<rootConfig.uniqueRiderName.length;k++){
//											if(product.RiderDetails[j].uniqueRiderName == rootConfig.uniqueRiderName[k] && product.RiderDetails[j].uniqueRiderName == $scope.Illustration.IllustrationOutput.selectedRiderTbl[i].uniqueRiderName && $scope.Illustration.IllustrationOutput.selectedRiderTbl[i].productName !== "Dental" && $scope.Illustration.IllustrationOutput.selectedRiderTbl[i].productName !== "OutPatient" ){
//												product.RiderDetails[j].sumInsured = "";
//												product.RiderDetails[j].initialPremium = product.RiderDetails[j].riderPremium;
//												product.RiderDetails[j].riderPlanName = $scope.Illustration.IllustrationOutput.selectedRiderTbl[i].productName;
//												if($scope.Illustration.IllustrationOutput.VGHRider[$scope.Illustration.IllustrationOutput.selectedRiderTbl[i].laNum].isRequired ==="Yes" && $scope.Illustration.IllustrationOutput.VGHRider[$scope.Illustration.IllustrationOutput.selectedRiderTbl[i].laNum].chosenRiderPlanCd){
//													product.RiderDetails[j].shortName = $scope.Illustration.IllustrationOutput.VGHRider[$scope.Illustration.IllustrationOutput.selectedRiderTbl[i].laNum].chosenRiderPlanCd.substring(0,4);
//												}
//												break;
//											}
//										}
//									}
//									
//									
//								}
								globalService.setProduct(product);
								
								// Overwrite insured, payer and product details
								// of FNA party with illustration party since
								// illustartion has more fields. Also, there is
								// a chance that insured and proposer are
								// updated in illustration -- End
								
								//This variable is set to disable the fileds prepopulated from BI and FNA(if done)in Eapp 
                                // starts here 
                                EappVariables.prepopulatedInsuredData=insured;
                                EappVariables.prepopulatedProposerData=payer;
                                EappVariables.prepopulatedBeneficiaryData=$scope.Illustration.Beneficiaries;
                                EappVariables.illustrationOutputData=$scope.Illustration.IllustrationOutput;
								EappVariables.leadName=IllustratorVariables.leadName;
                                // Ends here 

									if(illustrationNumber == ""){
										illustrationNumber = 0;
									}
								// On creating a new illustration from Landing
								// page(Clicking Create New illsutartion
								// button), fnaid won't be there, for which we
								// dont have to display chooseparties screen
								// Even from web - If has fna - go to
								// chooseparties rather than Eapp
									
								// Currently Commenting this section for THAILAND implementation because 
							    // when proceeding from Illustration to eApp choose party screen is not needed.
									
								/*if ($scope.fnaId != "" && $scope.fnaId != null) {
									$location.path('/fnaChooseParties/' +illustrationTransId+"/"+product.id+"/"+illustrationNumber);
									$scope.refresh();
								} else {
									//EappVariables.fnaId = IllustratorVariables.fnaId;
									//EappVariables.leadId =IllustratorVariables.leadId;
								   //EappVariables.illustratorId =IllustratorVariables.illustratorId;
									if(IllustratorVariables.fnaId == ""){
										IllustratorVariables.fnaId = 0;
									}
									if(IllustratorVariables.leadId == ""){
										IllustratorVariables.leadId = 0;
									}
                                    $rootScope.showHideLoadingImage(true, 'proceedingToEapp', $translate);
									$location.path('/Eapp/0/0/' + illustrationTransId + '/0/'+IllustratorVariables.leadId+'/'+IllustratorVariables.fnaId+'/'+illustrationNumber);
									$rootScope.refresh();
								}*/
									
								// Added for THAILAND implementation
								if(IllustratorVariables.fnaId == ""){
									IllustratorVariables.fnaId = 0;
								}
								if(IllustratorVariables.leadId == ""){
									IllustratorVariables.leadId = 0;
								}
                                $rootScope.showHideLoadingImage(true, 'proceedingToEapp', $translate);
								$location.path('/Eapp/0/0/' + illustrationTransId + '/0/'+IllustratorVariables.leadId+'/'+IllustratorVariables.fnaId+'/'+illustrationNumber);
								$rootScope.refresh();
								
								//$rootScope.showHideLoadingImage(false); 	
							}

                            //Ok button action from the Insured DOB warning popup
							$scope.moveToEapp = function(){
								$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
								$scope.onGetListingDetailSuccess($scope.eAppData);
							}

							//Validation to check whether any of the Insured Bday in coming 14 days
							 $scope.checkFourteenDays= function(){
                                //Not required for Thailand
								$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate); 
								$scope.onGetListingDetailSuccess($scope.eAppData);						
							}
							 
							//Proceed action from Active Agent to check the Illustration date
							$scope.setAgentInfoAndProceed = function(data){
								 $rootScope.showHideLoadingImage(false);
								 $scope.setAgentInfo(data);
								 var illuDate = $scope.eAppData[0].TransactionData.Product.premiumSummary.validatedDate ;
								 var isExpired = GLI_IllustratorService.checkExpiryDate(illuDate);
								 $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
								 if (isExpired){
									$rootScope.lePopupCtrl.showWarning(translateMessages( $translate, "lifeEngage"),
																	   translateMessages( $translate, "illustrator.expiryDateMessage"), 
																	   translateMessages( $translate, "illustrator.okMessageInCaps"), 
																	   $scope.okBtnAction, 
																	   translateMessages( $translate, "illustrator.cancelMessageInCaps"), 
																	   $scope.cancelBtnAction);
								 } else {
									 $scope.checkFourteenDays();
								 }
							}

							//OK button action from the Terminated/Suspended Agent
							$scope.proceedWithRetrievedAgentData = function(){
								$scope.setAgentInfoAndProceed($scope.retrievedAgentDetails);
							}
							
							$scope.onRetrieveAgentProfileSuccess = function(data) {
								if(data.AgentDetails.mappedBranches){
									delete data.AgentDetails.mappedBranches;
								}
								if(data.AgentDetails.reportingAgents){
									delete data.AgentDetails.reportingAgents;
								}
								if(data.AgentDetails.reportingRMs){
									delete data.AgentDetails.reportingRMs;
								}
								if(data.AgentDetails.reportingASMs){
									delete data.AgentDetails.reportingASMs;
								}
								$scope.retrievedAgentDetails = data;
								EappVariables.agentType = data.AgentDetails.agentType;
							    if(data.AgentDetails.status == 'Active' || data.AgentDetails.status == 'INFORCE'){
							    	$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
							    	$scope.setAgentInfoAndProceed($scope.retrievedAgentDetails);
							    } else if(data.AgentDetails.status=='Terminate'){
							    	$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), 
							    									 translateMessages($translate, "illustrator.suspendedOrTerminatedWarning"), 
							    									 translateMessages($translate, "fna.ok"), 
							    									 $scope.proceedWithRetrievedAgentData);
							    	$rootScope.showHideLoadingImage(false, "");
							    } else if(data.AgentDetails.status=='Suspend'){
							    	$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), 
							    									 translateMessages($translate, "illustrator.suspendedOrTerminatedWarning"), 
							    									 translateMessages($translate, "fna.ok"), 
							    									 $scope.proceedWithRetrievedAgentData);
							    	$rootScope.showHideLoadingImage(false, "");
							    } else {
										$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), 
																		 translateMessages($translate, "notActiveMsg"), 
																		 translateMessages($translate, "fna.ok"), 
																		 $scope.okClick);
										$rootScope.showHideLoadingImage(false, "");
								}
								$rootScope.agentName = data.AgentDetails.agentName;
								$scope.agentDetails =data;
								if(data.AgentDetails.agentType == "IOIS"){
									$scope.mappedBranchesList =data.AgentDetails.mappedBranches;
									if($scope.mappedBranchesList){
										$scope.mappedBranchesList.forEach(function (branches) { 
											   branches["parentAgentCode"] = $scope.agentCode;
										});
										delete data.AgentDetails.mappedBranches;
									}
								}
								if(data.AgentDetails.agentType == "BranchUser"){
									$scope.reportingAgentsList =data.AgentDetails.reportingAgents;
									/*  adding RM user ID in every Agent Object - for DB save purpose*/
										$scope.reportingAgentsList.forEach(function (agents) { 
										   agents["parentAgentCode"] = $scope.agentCode;
										});
									delete data.AgentDetails.reportingAgents;
								}
								/*TODO :- This Product assignment needs to be removed once more than one product available - same applicable in GLI_Landing Controller*/
								/*if(data.AgentDetails.products.length == 0){
									data.AgentDetails.products =[{"prdCode": "CIB1","prdName": "Critical Illness ENdowment"}];
								}*/
								if(data.AgentDetails.products){	
									if(data.AgentDetails.products.length > 0){
										$scope.applicableProductList = data.AgentDetails.products;
										/*  adding RM user ID in every Product Object - for DB save purpose*/
										$scope.applicableProductList.forEach(function (agents) { 
										   agents["parentAgentCode"] = $scope.agentCode;
										});
										delete data.AgentDetails.products;                   
									}else{
										delete data.AgentDetails.products;     
									}
								}
								if(data.AgentDetails.mappedBranches){
									delete data.AgentDetails.mappedBranches;
								}
								if(data.AgentDetails.reportingAgents){
									delete data.AgentDetails.reportingAgents;
								}
								if(data.AgentDetails.reportingRMs){
									delete data.AgentDetails.reportingRMs;
								}
								if(data.AgentDetails.reportingASMs){
									delete data.AgentDetails.reportingASMs;
								}
								if ((rootConfig.isDeviceMobile)){
									DataService.saveAgentProfile(data,function(){
										$scope.loadAgentProfile(data);
									},function(){

									});										
								}
							};
							
							// Method for navigating to eapp click success callback
							$scope.navigateToEapp = function(eAppData, illustrationData){
								$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
								$scope.refresh();
                                GLI_EappVariables.agentForGAO = illustrationData[0].Key11;
								GLI_EappVariables.GAOId = illustrationData[0].Key36;
								GLI_EappVariables.agentNameForGAO = illustrationData[0].Key38;
								GLI_EappVariables.agentType= illustrationData[0].Key37;
								GLI_EappVariables.GAOOfficeCode = illustrationData[0].Key35;
								GLI_EappVariables.CreatedAuthor=UserDetailsService.getAgentRetrieveData().AgentDetails.agentType;
								$scope.illustrationKey = "";
								if(illustrationData[0].Key1 == "" && illustrationData[0].Key2 != "" ){
									illustrationData[0].Key1 = illustrationData[0].Key2;
									illustrationData[0].Key2 = "";
								}
								if (eAppData && eAppData[0] && eAppData[0].TransactionData) {
									var id1 = 0;
	                                var id3 = 0;
	                                if(eAppData[0].Key4 == '-' || eAppData[0].Key4 == ""){
	                                	eAppData[0].Key4 = 0;
	                                 }
	                                $scope.LifeEngageProduct = EappVariables.getEappModel();
	                                //This variable is set to disable the fileds prepopulated in Eapp 
	                                // starts here 
	                                $scope.Illustration = illustrationData[0].TransactionData;
	                                $scope.LifeEngageProduct.Product.ProductDetails.IllustrationStatus = illustrationData[0].Key15;
									$scope.Illustration.Product.ProductDetails.IllustrationStatus = illustrationData[0].Key15;
	                                var insuredData=$scope.manipulateInsuredData($scope.Illustration.Insured);
	                                var payerData=$scope.manipulatePayerData($scope.Illustration.Payer);
                                    insuredData=$scope.manipulateInsuredRelationshipData(insuredData,payerData);
                                    insuredData=$scope.mapIdForOccupationInsured(insuredData);
                                    payerData=$scope.mapIdForOccupationPayer(payerData);
	                                EappVariables.prepopulatedInsuredData=insuredData;
	                                EappVariables.prepopulatedProposerData=payerData;
	                                EappVariables.prepopulatedBeneficiaryData=$scope.Illustration.Beneficiaries;
                                    EappVariables.illustrationOutputData=$scope.Illustration.IllustrationOutput;
									EappVariables.leadName=illustrationData[0].Key22;
	                                // Ends here 
	                                var product = angular.copy($scope.Illustration.Product);
	                                globalService.setProduct(product);
	                                EappVariables.setEappModel($scope.LifeEngageProduct);
	                                EappVariables.LastVisitedUrl = $scope.LifeEngageProduct.LastVisitedUrl;
	                                if(eAppData[0].Key2 == ""){
	                                	eAppData[0].Key2 = 0;
	                                }
									if(illustrationData[0].Key3 == ""){
										illustrationData[0].Key3 = 0;
									}
									if($scope.isRDSUser){
										$scope.illustrationKey = illustrationData[0].Key24;
									} else{
										$scope.illustrationKey = illustrationData[0].Key3;
									}
									if(!eAppData[0].Id){
										eAppData[0].Id = 0;
									}
									if(eAppData[0].Key1== ""){
										eAppData[0].Key1 = 0;
									}
	                                $location.path('/Eapp/'+eAppData[0].Key5+"/"+eAppData[0].Key4+"/"+eAppData[0].Key3+"/"+eAppData[0].Id+"/"+eAppData[0].Key1+"/"+eAppData[0].Key2+"/"+$scope.illustrationKey);
	                            	$rootScope.refresh();
								} else {
									IllustratorVariables.setIllustratorModel(illustrationData[0].TransactionData);
									$scope.Illustration = IllustratorVariables.getIllustratorModel();
									$scope.fnaId = illustrationData[0].Key2;
									FnaVariables.fnaId = 0;
									IllustratorVariables.leadId=illustrationData[0].Key1;
									IllustratorVariables.fnaId=illustrationData[0].Key2;
									$scope.Illustration.Product.ProductDetails.IllustrationStatus = illustrationData[0].Key15;
									IllustratorVariables.illustratorId = illustrationData[0].TransTrackingID;
									$scope.IllustrationId = (IllustratorVariables.illustratorId != "" && IllustratorVariables.illustratorId != 0) ? IllustratorVariables.illustratorId : illustrationData[0].Id;
									IllustratorVariables.leadName=illustrationData[0].Key22;
									$scope.leadId = illustrationData[0].Key1;
									if($scope.isRDSUser){
										$scope.illustrationKey = illustrationData[0].Key24;
									} else{
										$scope.illustrationKey = illustrationData[0].Key3;
									}
									if ($scope.fnaId != "" && $scope.fnaId != null) {
										// Get FNA details
										// On creating a new illustration from Landing page(Clicking Create New illsutartion button), 
										//fnaid won't be there, for which we dont have to display chooseparties screen
										if ((rootConfig.isDeviceMobile)) {
											// Get the related FNA from db
											DataService.getRelatedFNA($scope.fnaId, function(fnaData) {
												if (fnaData && fnaData[0] && fnaData[0].TransactionData) {
													FnaVariables.setFnaModel({
														'FNA' : JSON.parse(JSON.stringify(fnaData[0].TransactionData))
													});
													$scope.FNAObject = FnaVariables.getFnaModel();
													FnaVariables.transTrackingID = fnaData[0].TransTrackingID;
													FnaVariables.leadId = fnaData[0].Key1;
													globalService.setParties($scope.FNAObject.FNA.parties);
												}
												$scope.prepopulateeAppData($scope.IllustrationId,$scope.illustrationKey);
											}, $scope.onGetListingsError);
										} else {
											PersistenceMapping.clearTransactionKeys();
											PersistenceMapping.Type = "FNA";
											var transactionObj = PersistenceMapping.mapScopeToPersistence({});
											transactionObj.TransTrackingID = $scope.fnaId;
											DataService.getListingDetail(transactionObj, function(fnaData) {
												if (fnaData && fnaData[0] && fnaData[0].TransactionData) {
													FnaVariables.setFnaModel({
														'FNA' : JSON.parse(JSON.stringify(fnaData[0].TransactionData))
													});
													$scope.FNAObject = FnaVariables.getFnaModel();
													FnaVariables.transTrackingID = fnaData[0].TransTrackingID;
													FnaVariables.leadId = fnaData[0].Key1;
													if (fnaData[0].TransactionData.parties) {
														var parties = fnaData[0].TransactionData.parties;
														globalService.setParties(parties);
													}
												}
												$scope.prepopulateeAppData($scope.IllustrationId, $scope.illustrationKey);
											}, $scope.onGetListingsError);
										}
									} else {
										if ($scope.leadId != "" && $scope.leadId != null) {
											if ((rootConfig.isDeviceMobile)) {
												GLI_DataService.getRelatedLMS($scope.leadId, function(leadData) {
													if (leadData && leadData[0] && leadData[0].TransactionData) {
														var parties = [];
														var party = new globalService.party();
														party.BasicDetails = leadData[0].TransactionData.Lead.BasicDetails;
														party.ContactDetails = leadData[0].TransactionData.Lead.ContactDetails;
//														party.OccupationDetails = leadData[0].TransactionData.Lead.OccupationDetails;
														party.id = "myself";
														party.type = "Lead";
														party.isInsured = false;
														party.isPayer = false;
														party.isBeneficiary = false;
														party.isAdditionalInsured = false;
														party.fnaBenfPartyId = "";
														party.fnaInsuredPartyId = "";
														party.fnaPayerPartyId = "";
														parties.push(party);
														globalService.setParties(parties)
													}
													$scope.prepopulateeAppData($scope.IllustrationId,$scope.illustrationKey);
												}, $scope.onGetListingsError);
											} else {
												PersistenceMapping.clearTransactionKeys();
												var searchCriteria = {
														searchCriteriaRequest:{
															"command" : "RelatedTransactions",
															"modes" :['CHOOSEPARTY'],
															"value" : IllustratorVariables.leadId
														}
												};
												PersistenceMapping.Type = "LMS";
												var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
												transactionObj.TransTrackingID = IllustratorVariables.leadId;
													DataService.getFilteredListing(transactionObj, function(leadData) {
														if (leadData && leadData[0] && leadData[0].TransactionData) {
															var parties = [];
															var party = new globalService.party();
															party.BasicDetails = leadData[0].TransactionData.Lead.BasicDetails;
															party.ContactDetails = leadData[0].TransactionData.Lead.ContactDetails;
	//														party.OccupationDetails = leadData[0].TransactionData.Lead.OccupationDetails;
															party.id = "myself";
															party.type = "Lead";
															party.isInsured = false;
															party.isPayer = false;
															party.isBeneficiary = false;
															party.isAdditionalInsured = false;
															party.fnaBenfPartyId = "";
															party.fnaInsuredPartyId = "";
															party.fnaPayerPartyId = "";
															parties.push(party);
															globalService.setParties(parties)
														}
														$scope.prepopulateeAppData($scope.IllustrationId,$scope.illustrationKey);
													});
											}
										}
										$scope.prepopulateeAppData($scope.IllustrationId,$scope.illustrationKey);
									}
								}
							}
							
							//extending to cuztomize
							$scope.onGetListingDetailSuccess = function(data) {
								var illustrationData = data;
								if ((rootConfig.isDeviceMobile)){
									GLI_DataService.getRelatedEappForBI(illustrationData[0].TransTrackingID,function(eAppData){
										$scope.navigateToEapp(eAppData, illustrationData);
									});
								} else{
									PersistenceMapping.clearTransactionKeys();
									var searchCriteria = {
											searchCriteriaRequest:{
												"command" : "RelatedTransactions",
												"modes" :['eApp'],
												"value" : data[0].TransTrackingID
											}
									};
									PersistenceMapping.Type = "eApp";
									var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
									transactionObj.Key1 = data.TransTrackingID;
									transactionObj.filter = "Key15 <> 'Cancelled'";
                                    $scope.statusFilter = $rootScope.searchValue;
									$rootScope.searchValue="";	
									DataService.getFilteredListing(transactionObj, function(eAppData) {
										$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
										$scope.navigateToEapp(eAppData, illustrationData);
									});
								}
							}

							//This will return the remaining days for Insured Birthday
							$scope.checkDaysForBday = function(dob, successCallback) {
								if(dob != "" && dob != undefined && dob != null){
									var dobDate = new Date(dob);
									if((dobDate == "NaN" || dobDate == "Invalid Date")) {
										dobDate = new Date(formatForDateControl(dob).split("-").join("/"));
									}
									var birthdayMonthAndDate = [];
									birthdayMonthAndDate = [dobDate.getDate(),dobDate.getMonth()+1];
									var currentDate = new Date();
									var thisYearBday = new Date(currentDate.getFullYear(),birthdayMonthAndDate[1]-1,birthdayMonthAndDate[0]);
									if( currentDate.getTime() > thisYearBday.getTime()) {
										thisYearBday.setFullYear(thisYearBday.getFullYear()+1);
									}
									var timeToBday = thisYearBday.getTime()- currentDate.getTime();
									var daysToBday = Math.floor(timeToBday/(1000*60*60*24)) + 1;
									successCallback(daysToBday);
								}
							};
							
							$scope.loadAgentProfile = function() {
								var data = $scope.agentDetails;
								var agentDetail=UserDetailsService.getUserDetailsModel();
								if( data && data.AgentDetails ){
									$rootScope.agentName = data.AgentDetails.agentName;
								}
								var _agentDetail=data["AgentDetails"];				
									
								for (var i = 0; i < rootConfig.agentDetailFields.length; i++) {
									agentDetail[rootConfig.agentDetailFields[i]]=_agentDetail[rootConfig.agentDetailFields[i]];
								}
								
								
								UserDetailsService.setUserDetailsModel(agentDetail);
								$scope.refresh();
							};
							
							$scope.onRetrieveAgentProfileError = function() {
									$rootScope.lePopupCtrl.showError(translateMessages($translate,
										"lifeEngage"), translateMessages($translate,
										"general.agentProfileRetrieveError"), translateMessages($translate,
										"fna.ok"), $scope.okClick);
									$rootScope.showHideLoadingImage(false, "");
							};
							
							/*$scope.checkAgentStatus = function(data){
								//EappVariables.eAppProceedingDate = UtilityService.getFormattedDate();
								$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
								$scope.eAppData = data;
								if ((rootConfig.isDeviceMobile) || (rootConfig.isOfflineDesktop)) {
									if(checkConnection()){
										PersistenceMapping.clearTransactionKeys();
										var transactionObj = PersistenceMapping.mapScopeToPersistence({});
										AgentService.retrieveAgentProfile(transactionObj, $scope.onRetrieveAgentProfileSuccess, $scope.onRetrieveAgentProfileError);
									} else {
										PersistenceMapping.clearTransactionKeys();
										var transactionObj = PersistenceMapping.mapScopeToPersistence({});
										DataService.retrieveAgentDetails(transactionObj, function(data) {
											$scope.retrievedAgentDetails = data[0];
											if(data[0].status == 'Active' || data[0].status == 'INFORCE'){
												 $rootScope.showHideLoadingImage(false);
												 var agentData = {
													"agentId" : "",
													"agentName" : "",
													"agencyName" : "",
													"agentHomeNumber" : "",
													"agentMobileNumber" : "",
													"agentEmailID" : "",
													"licenseNumber":"",
													"securityNo":""
												};
												agentData.agentId = data[0].agentCode;
												agentData.agentName = data[0].agentName;
												agentData.agencyName = data[0].agencyOffice;
												agentData.agentHomeNumber = "";
												agentData.agentMobileNumber = data[0].phone;
												agentData.agentEmailID = data[0].emailId;
												agentData.licenseNumber = data[0].licenseNumber;
												agentData.securityNo = data[0].securityNo;
												EappVariables.agentData = agentData;
												// $scope.setAgentInfo(data);
												 var illuDate = $scope.eAppData[0].TransactionData.Product.premiumSummary.validatedDate ;
												 var isExpired = GLI_IllustratorService.checkExpiryDate(illuDate);
												 if (isExpired) {		
													$rootScope.lePopupCtrl.showWarning(translateMessages( $translate, "lifeEngage"),
																					   translateMessages( $translate, "illustrator.expiryDateMessage"), 
																					   translateMessages( $translate, "illustrator.okMessageInCaps"), 
																					   $scope.okBtnAction, 
																					   translateMessages( $translate, "illustrator.cancelMessageInCaps"), 
																					   $scope.cancelBtnAction);
												 } else {
													$scope.onGetListingDetailSuccess($scope.eAppData);
												 }
											 } else if(data[0].status=='Terminate'){
													$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), 
																					 translateMessages($translate, "suspendedOrTerminatedWarning"), 
																					 translateMessages($translate, "fna.ok"), 
																					 $scope.proceedWithRetrievedAgentData);
													$rootScope.showHideLoadingImage(false, "");
											 } else if(data[0].status=='Suspend'){
													$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), 
																					 translateMessages($translate, "suspendedOrTerminatedWarning"), 
																					 translateMessages($translate, "fna.ok"), 
																					 $scope.proceedWithRetrievedAgentData);
													$rootScope.showHideLoadingImage(false, "");
											 } else{
													$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), 
																					 translateMessages($translate, "notActiveMsg"), 
																					 translateMessages($translate, "fna.ok"), 
																					 $scope.okClick);
													$rootScope.showHideLoadingImage(false, "");
											 }
										}, $scope.onRetrieveAgentDetailsError);
										$rootScope.showHideLoadingImage(false);
									}
								} else{
									PersistenceMapping.clearTransactionKeys();
									var transactionObj = PersistenceMapping.mapScopeToPersistence({});
									if($scope.isRDSUser) {
										transactionObj.Key11 = $scope.eAppData[0].TransactionData.AgentInfo.agentCode;
									}
									$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
									AgentService.retrieveAgentProfile(transactionObj, $scope.onRetrieveAgentProfileSuccess, $scope.onRetrieveAgentProfileError);
								}
                            
							}*/
							
							$scope.setAgentInfo = function(data) {
								var agentData = {
									"agentId" : "",
									"agentName" : "",
									"agencyName" : "",
									"agentHomeNumber" : "",
									"agentMobileNumber" : "",
									"agentEmailID" : "",
									"licenseNumber" : "",
									"securityNo":""
								};
								agentData.agentId = data.AgentDetails.agentCode;
								agentData.agentName = data.AgentDetails.agentName;
								agentData.agencyName = data.AgentDetails.agencyOffice;
								agentData.agentHomeNumber = "";
								agentData.agentMobileNumber = data.AgentDetails.phone;
								agentData.licenseNumber = data.AgentDetails.licenseNumber;
								agentData.securityNo = data.AgentDetails.securityNo;
								agentData.agentEmailID = data.AgentDetails.emailId;
								EappVariables.agentData = agentData;
							}
							
							$scope.proceedToeAppNow = function() {
								$rootScope.showHideLoadingImage(false);
							}
							
							if($rootScope.fromFNAtoIllustration && IllustratorVariables.fromFNAChoosePartyScreenFlow){								
								PersistenceMapping.clearTransactionKeys();
								PersistenceMapping.Type = "illustration";
								var transactionObj = PersistenceMapping.mapScopeToPersistence({});
								transactionObj.TransTrackingID = IllustratorVariables.transTrackingID;
                                if(rootConfig.isDeviceMobile || rootConfig.isOfflineDesktop){
                                    
                                }else{
                                    transactionObj.Key3=IllustratorVariables.illustratorId;
                                }                                
                                //receive the data as second parameter
                                $scope.proceedToeApp(transactionObj);
                                $rootScope.refresh(); 
							}
							
                            /* $rootScope.$on('saveCallback', function(event,iResult){
                                PersistenceMapping.clearTransactionKeys();
								PersistenceMapping.Type = "illustration";
								var transactionObj = PersistenceMapping.mapScopeToPersistence({});
                                
								transactionObj.TransTrackingID = iResult.transTrackingID;
                                if(rootConfig.isDeviceMobile || rootConfig.isOfflineDesktop){
                                    
                                }else{
                                    transactionObj.Key3=iResult.illustratorId;
                                }                                
                                //receive the data as second parameter
                                $scope.proceedToeApp(transactionObj);
                                $rootScope.refresh(); 
                            }); */
                            
                            $scope.closePopupForErrorMsg = function () {
								$scope.errorMessageForInsured = undefined;	
								$scope.errorMessageForAgent = undefined;
								$scope.showErrorForSignature = false;					
		
							};
								$scope.proceedToeApp = function(illustrationObject) {
								$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
								var updatedDate = new Date();
								var formattedDate = Number(updatedDate.getMonth() + 1) + "-" + Number(updatedDate.getDate() + 5) + "-" + updatedDate.getFullYear();
								if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
									illustrationObject.Key11 = $rootScope.username;
									DataService.getListingDetail(illustrationObject, $scope.checkIllustrationStatus, $scope.onGetListingDetailError);
								} else {
									DataService.getListingDetail(illustrationObject, $scope.checkIllustrationStatus, $scope.onGetListingDetailError);
								}
                                $rootScope.showHideLoadingImage(false);                                    
							}
                            
                            $scope.checkIllustrationStatus=function(data){
                                if(data!==undefined){
                                    if(data[0].Key15!=='Confirmed'){
                                    	$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
                                    	$scope.refresh();
                                        IllustratorVariables.illustrationStatus = "Confirmed";
								        GLI_IllustratorService.onFieldChange($scope,IllustratorVariables,$rootScope);
								        IllustratorVariables.setIllustratorModel(data[0].TransactionData);
								        $scope.Illustration = IllustratorVariables.getIllustratorModel();
								        IllustratorVariables.transTrackingID = data[0].TransTrackingID;
                                        IllustratorVariables.illustratorKeys.Key5=data[0].Key5;
										IllustratorVariables.agentForGAO = data[0].Key11;
										IllustratorVariables.GAOId = data[0].Key36;
										IllustratorVariables.agentNameForGAO = data[0].Key38;
										IllustratorVariables.agentType= data[0].Key37;
										IllustratorVariables.GAOOfficeCode=data[0].Key35;
                                        $scope.productId=data[0].Key5;
								        $rootScope.transactionId = data[0].Id;
								        signature="Signature";
										outputFormat="image/png";
								        if (data[0].TransactionData.Requirements.length == 0) {
											 $scope.bothSigMissing = true;
											 $scope.clientSigMissing = false;
											 $scope.agentSigMissing = false; 
											 $scope.showErrorOnSignatureValidation(data);
										} else if (data[0].TransactionData.Requirements.length == 1) {
											IllustratorVariables.illustratorId = data[0].Key3;
											if(rootConfig.isDeviceMobile){
												IllustratorService.getSignature(IllustratorVariables.transTrackingID,signature,"",function(sigdata){
													if(sigdata && sigdata[0]){
														IllustratorVariables.RequirementFile=[];
														IllustratorVariables.RequirementFile.push(sigdata[0]);
														IllustratorService.convertImgToBase64(sigdata[0].base64string, function(base64Img){
															$scope.signatureBase64string=base64Img;
															var signatureType = sigdata[0].fileName.split("_")[0];
															if((signatureType == "ClientSignature" && $scope.signatureBase64string == undefined) ||
																 (signatureType == "AgentSignature" && $scope.signatureBase64string != undefined)){
																$scope.bothSigMissing = false;
																$scope.clientSigMissing = true;
												 				$scope.agentSigMissing = false;
															}else if((signatureType == "ClientSignature" && $scope.signatureBase64string != undefined) || 
																	(signatureType == "AgentSignature" && $scope.signatureBase64string == undefined)){
																$scope.bothSigMissing = false;
																$scope.clientSigMissing = false;
												 				$scope.agentSigMissing = true;
															}
															$scope.showErrorOnSignatureValidation(data);															
														}, outputFormat);
													}
													
												});
											} else{
												IllustratorService.getSignature(IllustratorVariables,signature,$scope,function(sigdata){
													IllustratorVariables.RequirementFile=[];
													IllustratorVariables.RequirementFile.push(sigdata);
													$scope.signatureBase64string=sigdata.base64string;
													$scope.signaturerequirement=sigdata.requirementName;
													var padCheckFirstload = $scope.signaturerequirement!=null ?$scope.signaturerequirement:"";
													var padFirstKey = padCheckFirstload.split('_');
													if((padFirstKey[0]=="Client" && $scope.signatureBase64string == undefined) || (padFirstKey[0]=="Agent" && $scope.signatureBase64string != undefined)) {
														$scope.bothSigMissing = false;
														$scope.clientSigMissing = true;
												 		$scope.agentSigMissing = false;
													} else if ((padFirstKey[0]=="Client" && $scope.signatureBase64string != undefined) || (padFirstKey[0]=="Agent" && $scope.signatureBase64string == undefined)) {
														$scope.bothSigMissing = false;
														$scope.clientSigMissing = false;
												 		$scope.agentSigMissing = true;
													} 														
													$scope.showErrorOnSignatureValidation(data);												
												});	
											}										
										} else if (data[0].TransactionData.Requirements.length == 2) {
											IllustratorVariables.illustratorId = data[0].Key3;
											if (rootConfig.isDeviceMobile){
												IllustratorService.getSignature(IllustratorVariables.transTrackingID,signature,"",function(sigdata){
													if(sigdata.length > 0){
														IllustratorVariables.RequirementFile = sigdata;
														IllustratorService.convertImgToBase64(sigdata[0].base64string, function(base64Img){
															$scope.signatureBase64stringFirst=base64Img;
															var signatureTypeFirst = sigdata[0].fileName.split("_")[0];
															IllustratorService.convertImgToBase64(sigdata[1].base64string, function(base64Img){
																$scope.signatureBase64stringSecond=base64Img;
																var signatureTypeSecond = sigdata[1].fileName.split("_")[0];
																if ($scope.signatureBase64stringFirst == undefined && $scope.signatureBase64stringSecond == undefined) {
																	$scope.bothSigMissing = true;
																	$scope.clientSigMissing = false;
												 					$scope.agentSigMissing = false; 
																} else if ((signatureTypeFirst == "ClientSignature" && $scope.signatureBase64stringFirst == undefined && signatureTypeSecond == "AgentSignature" && $scope.signatureBase64stringSecond != undefined) || 
																		(signatureTypeFirst == "AgentSignature" && $scope.signatureBase64stringFirst != undefined && signatureTypeSecond =="ClientSignature" && $scope.signatureBase64stringSecond == undefined)) {
																	$scope.bothSigMissing = false;
																	$scope.clientSigMissing = true;
												 					$scope.agentSigMissing = false;
																} else if ((signatureTypeFirst == "ClientSignature" && $scope.signatureBase64stringFirst != undefined && signatureTypeSecond == "AgentSignature" && $scope.signatureBase64stringSecond == undefined) || 
																		(signatureTypeFirst=="AgentSignature" && $scope.signatureBase64stringFirst == undefined && signatureTypeSecond =="ClientSignature" && $scope.signatureBase64stringSecond != undefined)) {
																	$scope.bothSigMissing = false;
																	$scope.clientSigMissing = false;
												 					$scope.agentSigMissing = true;
																} 
																$scope.showErrorOnSignatureValidation(data);
															}, outputFormat);																																													
														}, outputFormat);
													}													
												});
											} else {
												IllustratorService.getSignature(IllustratorVariables,signature,$scope,function(sigdataFirst){
													IllustratorVariables.RequirementFile=[];
													IllustratorVariables.RequirementFile.push(sigdataFirst);
													$scope.signatureBase64stringFirst=sigdataFirst.base64string;
													$scope.signaturerequirementFirst=sigdataFirst.requirementName;
													IllustratorService.getSignature1(IllustratorVariables,signature,$scope,function(sigdataSecond){
														IllustratorVariables.RequirementFile=[];
														IllustratorVariables.RequirementFile.push(sigdataSecond);
														$scope.signatureBase64stringSecond=sigdataSecond.base64string;
														$scope.signaturerequirementSecond=sigdataSecond.requirementName;
														var padCheckFirstload = $scope.signaturerequirementFirst!=null ?$scope.signaturerequirementFirst:"";
														var padFirstKey = padCheckFirstload.split('_');
														var padCheckSecondload = $scope.signaturerequirementSecond!=null ?$scope.signaturerequirementSecond:"";
														var padSecondKey = padCheckSecondload.split('_');

														if ($scope.signatureBase64stringFirst == undefined && $scope.signatureBase64stringSecond == undefined) {
															$scope.bothSigMissing = true;
															$scope.clientSigMissing = false;
										 					$scope.agentSigMissing = false; 
														} else if ((padFirstKey[0]=="Client" && $scope.signatureBase64stringFirst == undefined && padSecondKey[0]=="Agent" && $scope.signatureBase64stringSecond != undefined) || 
																(padFirstKey[0]=="Agent" && $scope.signatureBase64stringFirst != undefined && padSecondKey[0]=="Client" && $scope.signatureBase64stringSecond == undefined)) {
															$scope.bothSigMissing = false;
															$scope.clientSigMissing = true;
										 					$scope.agentSigMissing = false;
														} else if ((padFirstKey[0]=="Client" && $scope.signatureBase64stringFirst != undefined && padSecondKey[0]=="Agent" && $scope.signatureBase64stringSecond == undefined) || 
																(padFirstKey[0]=="Agent" && $scope.signatureBase64stringFirst == undefined && padSecondKey[0]=="Client" && $scope.signatureBase64stringSecond != undefined)) {
															$scope.bothSigMissing = false;
															$scope.clientSigMissing = false;
										 					$scope.agentSigMissing = true;
														} 
															
														$scope.showErrorOnSignatureValidation(data);
														
													});
												});	
											}												
										} 										
                                    }else{											
										$scope.checkAgentStatus(data);
								    }
                                }
                            }

                            $scope.showErrorOnSignatureValidation = function(data){
                            	if ($scope.bothSigMissing && !$scope.clientSigMissing && !$scope.agentSigMissing) {
											$rootScope.showHideLoadingImage(false, "");
											$scope.showErrorForSignature = true;
											$scope.errorMessageForInsured = translateMessages($translate, "eapp_vt.checkSignForSIInsured"); 
											$scope.errorMessageForAgent = translateMessages($translate, "eapp_vt.checkSignForSIAgent");
										} else if (!$scope.bothSigMissing && $scope.clientSigMissing && !$scope.agentSigMissing) {
											$rootScope.showHideLoadingImage(false, "");										
											$scope.showErrorForSignature = true;
											$scope.errorMessageForInsured = translateMessages($translate, "eapp_vt.checkSignForSIInsured"); 
										} else if (!$scope.bothSigMissing && !$scope.clientSigMissing && $scope.agentSigMissing) {
											$rootScope.showHideLoadingImage(false, "");							   			
							   				$scope.showErrorForSignature = true;			
											$scope.errorMessageForAgent = translateMessages($translate, "eapp_vt.checkSignForSIAgent"); 
										}
										else {
											$rootScope.showHideLoadingImage(false, "");
	                                        var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
	                                        obj.save(function (isAutoSave) {   
	                                        	$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
	                                            $scope.checkAgentStatus(data); 
	                                        });
                                    	}
                                    	$scope.refresh();
                            }

							$scope.selectedRiderCount = function(data){
								if(data && data.length>0){
									$rootScope.showHideLoadingImage(false);
									if(data[0].TransactionData && data[0].TransactionData.IllustrationOutput && data[0].TransactionData.IllustrationOutput.selectedRiderTbl){
										if(data[0].TransactionData.IllustrationOutput.selectedRiderTbl.length > rootConfig.riderLimit){
											$rootScope.lePopupCtrl.showError(translateMessages($translate,
													"lifeEngage"), translateMessages($translate,
													"illustrator.riderCountValidation"), translateMessages($translate,
													"fna.ok"));
										}else{											
											$scope.checkAgentStatus(data);
										}
										
									}else{											
											$scope.checkAgentStatus(data);
										}
									}									
								}													
							
							function customSortForListing(data, callBack) {
                                if(!$scope.sortApplied) {
                                    data.sort(function(a,b){
                                       if (rootConfig.isDeviceMobile) {
                                            b.modifiedDate = b.modifiedDate ? b.modifiedDate : '1900-01-01 01:01:01';
                                            a.modifiedDate = a.modifiedDate ? a.modifiedDate : '1900-01-01 01:01:01';
                                        }
                                        return new Date(b.modifiedDate) - new Date(a.modifiedDate);
                                     });
                                }
                                /*$timeout(function() {
                                   callBack(data);
                                },0);*/
								 callBack(data);
                            }

							
							/*
							 * Overrided function
							 */
							$scope.onGetIllustrationSuccessForListing = function(data) {
								$scope.Illustartions = [];
								customSortForListing(data,function(){
									if ((rootConfig.isDeviceMobile)) {
										$.each(data, function(i, obj) {
											obj.TransactionData.IllustrationStatus = obj.Key15;
										});
									}
								});
								/*if ((rootConfig.isDeviceMobile)) {
									$.each(data, function(i, obj) {
										obj.TransactionData.IllustrationStatus = obj.Key15;
									});
								}*/
								for (var i=0;i<data.length;i++){
								   if(data[i].Key21 && data[i].Key21 != ""){
	                                  data[i].Key21 = parseInt(data[i].Key21);
                                   }
								   else
                                   {
                                   	  data[i].Key21 = parseInt("0");
                                   }
								   if(data[i].Key25 && data[i].Key25 != ""){
	                                  data[i].Key25 = parseInt(data[i].Key25);
                                   }
								   else
                                   {
                                   	  data[i].Key25 = parseInt("0");
                                   }
								   /*if(data[i].Key19 && data[i].Key19 != ""){
									   if(IllustratorVariables.channelId == "9084"){
										   data[i].Key19 =  translateMessages($translate, "illustrator.eximBankProductNameForProdListingScreen");
							           }else if(IllustratorVariables.channelId  == "9085"){
							        	   data[i].Key19 =  translateMessages($translate, "illustrator.techomBankProductNameForProdListingScreen");
							           }else{
							        	   data[i].Key19 =  translateMessages($translate, "illustrator.commonBankProductNameForProdListingScreen");
							           }
								   }*/
								}
								if(IllustratorVariables.leadId!=undefined && IllustratorVariables.leadId!=0){// Filtering for Lead based BI 
									$scope.showNewIlustration=true;
									for (var i=0;i<data.length;i++){
										if(data[i].Key1 == IllustratorVariables.leadId){
											$scope.Illustartions.push(data[i]);
											if( i == 0 ){
												IllustratorVariables.leadName = data[i].Key22;
												IllustratorVariables.leadEmailId = data[i].Key23;
											}
										}
										
									}
								
								}else if(IllustratorVariables.fnaId!=undefined && IllustratorVariables.fnaId!=0){
								
									for (var i=0;i<data.length;i++){
										if(data[i].Key2 == IllustratorVariables.fnaId){
											$scope.Illustartions.push(data[i]);
										}
									}
									$scope.tableIllustrations = [].concat($scope.Illustartions);
							    }else{	
									
								$scope.Illustartions = data;
								$scope.tableIllustrations = [].concat($scope.Illustartions);
								}
								if (!(rootConfig.isDeviceMobile)) {
									$scope.Illustartions = $scope.Illustartions.sort(function(a, b) {
										return LEDate(b.Key14)
												- LEDate(a.Key14)
									});
								}
								$rootScope.showHideLoadingImage(false);
								$scope.refresh();
							}
							
							$scope.toggleSelection = function toggleSelection(proposal) {
								$scope.hidebtnMakeCopy=false;
								var idx = $scope.selectedProposal.indexOf(proposal);

								// is currently selected
								if (idx > -1) {
									$scope.selectedProposal.splice(idx, 1);
									
								}

								// is newly selected
								else {
									//removed for bugfix 1221
									/* if (!(rootConfig.isDeviceMobile)) {
										$scope.selectedProposal = [];
									} */
									$scope.selectedProposal=[];
									$scope.selectedProposal.push(proposal);
									
									
								}

								if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin)
								{   
									   	
								$scope.hidebtnMakeCopy=true;
								}								
							};
							
							// For alert message based on sync status
							$scope.deletePopup = function(type) {
									$scope.deleteType = type;
										PersistenceMapping.clearTransactionKeys();
										$scope.mapKeysforPersistence();
										if (PersistenceMapping
												.mapScopeToPersistence(JSON
														.stringify($scope.selectedProposal[0].TransactionData))) {
											$scope.dvDeleteConfirm = true;
											$scope.confirmDeleteMessage = true;
                                            $scope.statusdelete = true;
											
											for (var i = 0; i < $scope.selectedProposal.length; i++) {
												if($scope.deleteType=="eApp"){
													for (var j = 0; j < rootConfig.proposalStatus.length; j++){
														if (($scope.selectedProposal[i].Key15).indexOf(rootConfig.proposalStatus[j]) != -1 || ($scope.selectedProposal[i].Key7).indexOf(rootConfig.proposalStatus[j]) != -1 ) {
															$scope.confirmDeleteMessage = true;
															break;
														}
													}
												}
												if($scope.deleteType=="illustration"){
													if ($scope.selectedProposal[i].Key15 == "Confirmed") {
														$scope.confirmDeleteMessage = false;
													}
												}
                                                
                                                if($scope.deleteType=="eApp"){
													if ($scope.selectedProposal[i].Key15 != "Pending Submission") {
														$scope.confirmDeleteMessage = false;
													}
												}
                                                
                                                
                                                if($scope.deleteType=="eApp"){
													if ($scope.selectedProposal.length >= 1) {
                                                        if($scope.selectedProposal[i].Key15 != "Pending Submission"){
														$scope.statusdelete = false;
                                                        }
													}
												}
                                                
											}
											if($scope.deleteType=="eApp"){
												if($scope.selectedProposal.length == 1){
                                                    if($scope.confirmDeleteMessage){
                                                        $scope.alertMessage = "deleteWarningText";
                                                    }
                                                    else{
                                                        $scope.alertMessage = "eappsubmitdeletemsg";
                                                    }
                                                }
                                                
                                                if($scope.selectedProposal.length > 1){
                                                    if($scope.confirmDeleteMessage){
                                                        $scope.alertMessage = "deleteWarningText";
                                                        
                                                    }
                                                    
                                                    else{
                                                        $scope.alertMessage = "eappcombinationselectedstatus";
                                                    }
                                                }
													
                                                    
                                                    
                                                    
//												else{
//													$scope.dvDeleteConfirm = false;
//													$scope.confirmDeleteMessage = false;
//													$rootScope.lePopupCtrl.showError(translateMessages(
//																$translate, "lifeEngage"),translateMessages($translate,"eapp_vt.deleteErrorWarningMsg")
//													,translateMessages($translate, "general.ok"));
//												}
											}else{
												if ($scope.confirmDeleteMessage) {
													$scope.alertMessage = "illustrationWarningText";
//                                                    $scope.lePopupCtrl.showWarning(
//						                                  translateMessages($translate,
//								                                        "lifeEngage"),
//						                                  translateMessages($translate,
//								                                "illustrationWarningText"),  
//						                                  translateMessages($translate,
//								                          "okConfirmButtonText"),$scope.deleteTransactions,translateMessages($translate,
//								                          "biCancelButtonTextEapp"),$scope.okPressed); 
												} else {
													$scope.alertMessage = "illustrationConfirmWarningText";
//                                                    $rootScope.lePopupCtrl.showSuccess(translateMessages($translate,"lifeEngage"),translateMessages($translate,"illustrationConfirmWarningText"),translateMessages($translate,"fna.ok"),$scope.okPressed);
												}
											}
										}
								
							};
					$scope.ondelteSuccessCallBack = function() {
						if ((rootConfig.isDeviceMobile)) {
							$scope.selectedProposal.splice(0, 1);
							if ($scope.selectedProposal.length > 0) {
										$scope.deleteTransactions();
							} 
							else {
								$rootScope.showHideLoadingImage(true, 'deleteMessage', $translate);		
								var transactionObj = {};
								PersistenceMapping.clearTransactionKeys();
								$scope.mapKeysforPersistence();
								transactionObj = PersistenceMapping.mapScopeToPersistence({});				
								transactionObj.Type = $scope.deleteType;
								transactionObj.Key4 = "";
								transactionObj.Id = "";
								transactionObj.Key10 = "BasicDetails";
								transactionObj.filter = "Key15  <> 'Cancelled'";
								if (transactionObj.Type == 'eApp') {
										var  searchCriteria = {
														 searchCriteriaRequest:{
												"command" : "ListingDashBoard",
												"modes" :['eApp'],
												"value" : ""
												}
											};
									PersistenceMapping.Type='eApp';
									var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);									
									transactionObj.Id = $routeParams.transactionId;
									transactionObj.Type = "eApp";
									transactionObj.filter = "Key15  <> 'Cancelled'";
									$scope.statusFilter = $rootScope.searchValue;
									$rootScope.searchValue="";	
									//transactionObj.Key11 = "00007766";
									DataService.getFilteredListing(transactionObj, $scope.onGeteAppListingSuccess, $scope.onGetListingsError);
					

								} else if (transactionObj.Type == 'illustration') {					
										var  searchCriteria = {
														 searchCriteriaRequest:{
														"command" : "ListingDashBoard",
														"modes" :['illustration'],
														"value" : ""
														}
													};
										PersistenceMapping.Type='illustration';
										var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
										transactionObj.Type = "illustration";
										transactionObj.filter = "Key15  <> 'Cancelled'";
										$scope.searchIllustrationKey = $rootScope.searchValue;
										$rootScope.searchValue="";
										$scope.searchIllustrationValue =  IllustratorVariables.searchBiValue;
										IllustratorVariables.searchBiValue = "";
										$scope.searchKeyValue = IllustratorVariables.searchBiKey;
										IllustratorVariables.searchBiKey = "";
										//transactionObj.Key11 = "00007766";
										DataService.getFilteredListing(transactionObj, $scope.onGetIllustrationSuccessForListing, $scope.onGetListingsError);
									
									}
								}
						}
						else {
							if (document.location.href.indexOf('Illustration') < 0) {
								for (var i in $scope.selectedProposal) {					
									for (var k in $scope.Proposals) {						
										if ($scope.Proposals[k] == $scope.selectedProposal[i]) {
											$scope.Proposals.splice(k, 1)
										}
									}
								}
							} 
							else {
								for (var i in $scope.selectedProposal) {					
									for (var k in $scope.Illustartions) {
										if ($scope.Illustartions[k] == $scope.selectedProposal[i]) {
											$scope.Illustartions.splice(k, 1)
										}
									}
								}
							}
							$scope.selectedProposal.splice(0,$scope.selectedProposal.length);
							setTimeout(function() {
								$rootScope.showHideLoadingImage(false);
								$rootScope.refresh();
							});
						}
		
					}

					$scope.deleteTransactions = function() {
                    		PersistenceMapping.clearTransactionKeys();
                    		$scope.mapKeysforPersistence();
                    		var transactionObjDeleted = {};
                    		if ((rootConfig.isDeviceMobile)) {
                    			transactionObjDeleted.Id = $scope.selectedProposal[0].Id;
                    		} else {
                    			transactionObjDeleted = PersistenceMapping.mapScopeToPersistence();

                    		for(var i in $scope.selectedProposal){
                    			if($scope.selectedProposal[i].Key15)
                    				$scope.selectedProposal[i].Key15="Cancelled";
                    		}

                    			transactionObjDeleted = $scope.selectedProposal;
                                
                                if (transactionObjDeleted[0].Type == "illustration") {
                    			     if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
									   transactionObjDeleted[0].Key11=transactionObjDeleted[0].Key36;
								    }
                    		    }
                    		}

                    		// var transactionObjDeleted =
                    		// $scope.mapScopeToPersistance(JSON.stringify($scope.selectedProposal[0].TransactionData));
                    		if ((rootConfig.isDeviceMobile)) {
                    			transactionObjDeleted.Id = $scope.selectedProposal[0].Id;
                    		}
                    		// transactionObjDeleted.TransactionData.Key7 =
                    		// "Cancelled";
							
                    		transactionObjDeleted.Key1 = "";
                    		transactionObjDeleted.Key15 = "Cancelled";
                    		if (transactionObjDeleted.Type == "eApp") {
                    			transactionObjDeleted.Key4 = $scope.selectedProposal[0].Key4;
                    		}
                    		if (transactionObjDeleted.Type == "illustration") {
                    			transactionObjDeleted.Key3 = $scope.selectedProposal[0].Key3;
                                if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
									transactionObjDeleted.Key11=transactionObjDeleted.Key36;
								}
                    		}
                    		if (transactionObjDeleted.Type == "FNA") {
                    			transactionObjDeleted.Key2 = $scope.selectedProposal[0].Key2;
                    		}
                    		$rootScope.showHideLoadingImage(true, 'deleteMessage', $translate);
                    		if ((rootConfig.isDeviceMobile)) {
                    			DataService.saveTransactions(transactionObjDeleted, $scope.ondelteSuccessCallBack, $scope.ondelteError);
                    		} else {
                    			DataService.deleteTransactions(transactionObjDeleted, $scope.ondelteSuccessCallBack, $scope.ondelteError);
                    		}

                    		$scope.removePagePopup();

                    	}

							$scope.onSaveSuccess = function(data) {
								$rootScope.showHideLoadingImage(false);
								$route.reload();
							}
							//overrided function for sending email in illustration listing screen
							
							$scope.fetchIllustrations = function(){
                                $rootScope.showHideLoadingImage(true, "Loading..");
                                if($scope.isUserOnline()) {                              
								    if ($scope.selectedProposal.length != 0) {
									   IllustratorService.fetchIllustrationsForEmail($scope.selectedProposal, DataService, 
									   $scope.emailStatusCheck, $scope.fetchError);		
								    }
                                }else{
                                    $scope.emailpopup = false; 
                                    $scope.leadDetShowPopUpMsg = true;
                                    $scope.enterMandatory = translateMessages($translate, "illustrator.emailBINoConnectionErrorMessage");

                                }
							}
							
                            $scope.isUserOnline = function(){
								var userOnline = true;
								if ((rootConfig.isDeviceMobile)&& !(rootConfig.isOfflineDesktop)){
									if (navigator.network.connection.type == Connection.NONE) {
										userOnline = false;
									}
								}
								if ((rootConfig.isOfflineDesktop)) {
									if (!navigator.onLine) {
										userOnline = false;
									}
								}
								return userOnline;
							}
							$scope.fetchError = function() {
                                $scope.leadDetShowPopUpMsg = true;
                                $scope.enterMandatory = translateMessages($translate, "emailBIErrorMessage");
								
							}
							
							$scope.emailStatusCheck = function(transactions) {
								$scope.dratCheck = true;
								var testSameProposer = true;
								if (transactions.length > 0) {
									for(var i=0;i<transactions.length;i++) {
										if($scope.selectedProposal[i].Key15=='Draft'){
											$scope.dratCheck = false;
											break;
										}else if($scope.selectedProposal[0].Key1 != $scope.selectedProposal[i].Key1){
											testSameProposer = false;
											break;
										}	
									}
									
									if(!$scope.dratCheck){
										$scope.showValidationErrorforEmail(true,false);
									}else if(!testSameProposer){
										$scope.showValidationErrorforEmail(false,true);
									}else{
										$scope.doValidationForEmailSending(transactions);
									}
								}
								
							}
							
							// two validations before opening email popup 1.Chosen BIs should belong to same lead  2. Status of chosen BIs should be FINAL
							$scope.doValidationForEmailSending = function(transactions) {
								$scope.emailToId = "";
								$scope.emailccId =""; 
								if (transactions.length > 0) {
									$scope.illustartionsToMail = transactions;									
									//$scope.leadName = $scope.selectedProposal[0].Key22;	
									//$scope.emailToId = $scope.selectedProposal[0].Key23;
									
									$scope.leadName =  transactions[0].Key22 ;
									if(transactions[0].Key23!=undefined && transactions[0].Key23!=""){
										$scope.emailToId = transactions[0].Key23;
									}
									else if(transactions[0].Key20!=undefined && transactions[0].Key20!=""){
										$scope.emailToId = transactions[0].Key20;
									}
									$scope.userDetails = UserDetailsService.getUserDetailsModel();
									$scope.emailccId = $scope.userDetails.emailId;
									$scope.agentName = $scope.userDetails.agentName;
									$scope.agentEmailId = $scope.userDetails.emailId;
                                    
                                    var productLabel="";
                                    if(parseInt(transactions[0].Key5)==1006){
                                        productLabel="GenSave20Plus_emailSubject";
                                    } else if(parseInt(transactions[0].Key5)==1004){
                                        productLabel="GenProLife_emailSubject";
                                    } else if(parseInt(transactions[0].Key5)==1003){
                                        productLabel="GenBumnan_emailSubject";
                                    } else if(parseInt(transactions[0].Key5)==1010){
                                        productLabel="GenCompleteHealth_emailSubject";
                                    }
                                    
                                    if(parseInt(transactions[0].Key5)==1010 || parseInt(transactions[0].Key5)==1006){
									   $scope.emailSubject = translateMessages($translate, "illustrator.GeneraliThailandEmailSubject")+translateMessages($translate,productLabel)+" "+translateMessages($translate, "illustrator.GeneraliThailandEmailSubject1")+" "+transactions[0].Key6;
                                    }else{
	                                    $scope.emailSubject = translateMessages($translate, "illustrator.GeneraliThailandEmailSubject")+translateMessages($translate,productLabel)+" "+transactions[0].Key30+" "+translateMessages($translate, "illustrator.GeneraliThailandEmailSubject1")+" "+transactions[0].Key6; 
                                    }
									//cc id needs to be populated from User Details object agent - mailid - yet to implement maild id return from service
									$scope.emailpopupError ="";
									$scope.emailToError ="";
									$scope.emailCcError ="";
									$scope.emailSubjectError = "";
									if(IllustratorVariables.leadEmailId!=undefined && IllustratorVariables.leadEmailId!=""){
										$scope.emailToId = IllustratorVariables.leadEmailId;
								    }
									else if(transactions[0].Key23!=undefined && transactions[0].Key23!=""){
										$scope.emailToId = transactions[0].Key23;
								    }else if(transactions[0].Key20!=undefined && transactions[0].Key20!=""){
										$scope.emailToId = transactions[0].Key20;
								    }  else{
										$scope.emailToId = "";
									}
									$scope.showEmailPopup();
								} 
							}
							
							$scope.showEmailPopup = function() {
								$scope.emailpopup = true;
								$scope.refresh();
							}
							
							$scope.showValidationErrorforEmail = function(proposerSame,statusFinal) {
								if(!proposerSame){
//									$rootScope.lePopupCtrl.showError(translateMessages(
//												$translate, "lifeEngage"),translateMessages($translate,"illustrator.emailValidationMessageMultileadSelection")
//									,translateMessages($translate, "general.ok"));
                                    $scope.leadDetShowPopUpMsg = true;
                                    $scope.enterMandatory = translateMessages($translate, "illustrator.emailValidationMessageMultileadSelection");
                                    $rootScope.showHideLoadingImage(false);
                                    $scope.refresh();
								} else if (!statusFinal){
//									$rootScope.lePopupCtrl.showError(translateMessages(
//												$translate, "lifeEngage"),translateMessages($translate,"illustrator.emailBIValidationErrorSatusCheck")
//									,translateMessages($translate, "general.ok"));
                                    $scope.leadDetShowPopUpMsg = true;
                                    $scope.enterMandatory = translateMessages($translate, "illustrator.emailBIValidationErrorSatusCheck");
                                    $rootScope.showHideLoadingImage(false);
                                    $scope.refresh();
								}
							}
							
							$scope.emailPopupOkClick = function (){	
								$scope.validateMailIds(function(emailIdStatus){
									if(emailIdStatus){
										$scope.emailpopup = false;
									    $rootScope.showHideLoadingImage(true,"Please Wait");
										$scope.saveEmailSendFlagAndData($scope.showSuccessPopup,$scope.showErrorPopup);
									}
								});							
							}
							
							
							$scope.validateMailIds = function(emailCallback) {	
								$scope.emailToError ="";
								$scope.emailCcError ="";
								$scope.emailSubjectError = "";
								var status = true;
								var re = rootConfig.emailPattern;
						        var EMAIL_REGEXP = new RegExp(re);
						        if($scope.emailToId==""|| $scope.emailToId == undefined){
										$scope.emailToError = translateMessages(
												$translate, "illustrator.validEmailValidationMessage");
										status = false;
								} else if($scope.emailSubject && $scope.emailSubject == ""){
									$scope.emailSubjectError = translateMessages($translate, "illustrator.validEmailValidationMessage");;
								} else {
										var emailIdsToValidate = [];
										emailIdsToValidate.push($scope.emailToId);
										if($scope.emailccId !=""){
											if($scope.emailccId !=undefined){
												emailIdsToValidate.push($scope.emailccId);
											}
										}
										for(var t = 0;t < emailIdsToValidate.length; t++){
											var toEmailId = emailIdsToValidate[t];
											if(toEmailId.charAt(toEmailId.length - 1) == ";")
											{
												toEmailId = toEmailId.substring(0, (toEmailId.length-1));
											}
											
											var tomailIds = toEmailId.split(';');		
											for (var idCount = 0; idCount < tomailIds.length; idCount++) {		 
									            if(!EMAIL_REGEXP.test(tomailIds[idCount])){	
									            	if(t === 0){
									            		$scope.emailToError =  translateMessages(
																$translate, "illustrator.validEmailValidationMessage");
									            	}else{
									            		$scope.emailCcError =  translateMessages(
																$translate, "illustrator.validEmailValidationMessage");
									            	}													
													status = false;
													break;
												}      			
											}
										}
								}
								emailCallback(status);
							}
							
							$scope.saveEmailSendFlagAndData = function(successCallback,errorCallBack) {
								if ($scope.selectedProposal.length > 0) {			 	 
									var emailObj = {};				
									emailObj.toMailIds = $scope.emailToId;
									if($scope.emailccId){
										emailObj.ccMailIds = $scope.emailccId; 
									}else{
										emailObj.ccMailIds = "";
									}
									if($scope.agentName){
										emailObj.agentName = $scope.agentName;
									}else{
										emailObj.agentName = "";
									}
									if($scope.agentEmailId){
										emailObj.agentEmailId = $scope.agentEmailId;
									}else{
										emailObj.agentEmailId = "";
									}
									
									emailObj.leadName = $scope.leadName;
									if ($scope.emailSubject) {
										emailObj.mailSubject = $scope.emailSubject;
									} else {
										emailObj.mailSubject = "";
									}
									//var selectedLanguage = (localStorage["locale"] == "vt_VT") ? "vt" : "en";
									//var selectedLanguage = (localStorage["locale"] == "th_TH") ? "th" : "en";
									 var selectedLanguage = "th";
									emailObj.language = selectedLanguage; 
									IllustratorService.setEmailBIflagAndSaveTransactions(emailObj,$scope.illustartionsToMail,
											DataService,successCallback,errorCallBack);
								}
								
							}
							$scope.showSuccessPopup = function(){
								$rootScope.showHideLoadingImage(false);
								$scope.emailpopup = false;
								$scope.emailToId ="";
								$scope.emailccId="";
								$scope.leadName = "";	
								$scope.agentName = "";
								$scope.agentEmailId = "";
								$.each($scope.selectedProposal, function(i, obj) {
									$scope.toggleSelection(obj);			 
								});
								$scope.selectedProposal=[];
								$scope.illustartionsToMail =[];
								
								if (!(rootConfig.isDeviceMobile)){
//									$rootScope.lePopupCtrl.showSuccess(translateMessages($translate,"lifeEngage"),
//													translateMessages($translate,"illustrator.emailOkButtonMessage"),
//													translateMessages($translate,"fna.ok"),$scope.initialLoad);
                                    $scope.leadDetShowPopUpMsg = true;
                                    $scope.enterMandatory = translateMessages($translate, "illustrator.emailOkButtonMessage");
								}
								else{
									if(checkConnection())
									{								
//										$rootScope.lePopupCtrl.showSuccess(translateMessages($translate,"lifeEngage"),
//													translateMessages($translate,"illustrator.emailOkButtonMessage"),
//													translateMessages($translate,"fna.ok"),$scope.initialLoad);
                                        $scope.leadDetShowPopUpMsg = true;
                                        $scope.enterMandatory = translateMessages($translate, "illustrator.emailOkButtonMessage");
									}
									else
									{
										$scope.emailpopup = false;
										$scope.leadDetShowPopUpMsg = true;
                                        $scope.enterMandatory = translateMessages($translate, "illustrator.emailBINoConnectionErrorMessage");
									}
								}
								$scope.refresh();
							}
							
							$scope.showErrorPopup = function(status) {
								//var errorMessage = "httpErrorDescription"+status;
								var httpErrorMessage = "illustrator."+errorMessage;
								$rootScope.showHideLoadingImage(false);
								$scope.emailpopup = false;
								$scope.emailToId ="";
								$scope.emailccId="";
								$scope.leadName = "";	
								$scope.agentName = "";
								$scope.agentEmailId = "";	
								$.each($scope.selectedProposal, function(i, obj) {
									$scope.toggleSelection(obj);			 
								});
								$scope.selectedProposal=[];
								$scope.illustartionsToMail =[];
								
								if(checkConnection())
								{
										var emailErrorMessage = translateMessages($translate, "illustrator.emailSendingErrorMessage");
										//+ translateMessages($translate, httpErrorMessage);
										$rootScope.lePopupCtrl.showSuccess(translateMessages(
												$translate, "lifeEngage"), emailErrorMessage, translateMessages($translate,
												"fna.ok"));
												$scope.refresh();
								}
								else
								{
									$scope.emailpopup = false;
									$scope.leadDetShowPopUpMsg = true;
                                    $scope.enterMandatory = translateMessages($translate, "illustrator.emailBINoConnectionErrorMessage");
								}
								$scope.refresh();
								
							}
							$scope.cancelEmailPopup = function(){
								$rootScope.showHideLoadingImage(false);
								$.each($scope.selectedProposal, function(i, obj) {
									$scope.toggleSelection(obj);			 
								});
								$scope.selectedProposal=[];
								$scope.illustartionsToMail =[];
								$scope.emailpopup = false;
								$scope.emailToId ="";
								$scope.emailccId="";
								$scope.leadName = "";	
								$scope.agentName = "";
								$scope.agentEmailId = "";	
							}
							
							$scope.proceedToProduct = function() {
							   
							   if($scope.Illustartions){
								  
								   if($scope.Illustartions[0].Key1 != ""){  // Fetch Lead ID
									  	GLI_DataService.getRelatedFNAForLMS($scope.Illustartions[0].Key1, function(fnaData) {  // FNA exists
											if (fnaData && fnaData[0] && fnaData[0].TransactionData && fnaData[0].Key15 == "Complete") {
													$location.path('/fnaRecomendedProducts');
											}
											else{
												$location.path('/fnaAllProducts');
											}												
										}); 											
								   }
								   else{  // FNA not Created
									   $location.path('/fnaAllProducts');
								   }
								   $scope.refresh();
							   }
								
							}
							
							// For copy a illustration from listing screen
							
							
							$scope.updateListingData = function(data,successcallBack) {
								if (data && data.length > 0 && data[0].Key15 && data[0].Key15 != "Draft"){
									
									if(data[0].TransactionData != null || data[0].TransactionData != ""){
										data[0].TransactionData.IllustrationOutput = "";
									}
									if( data[0].TransactionData && data[0].TransactionData.Product &&
									data[0].TransactionData.Product.policyDetails && 
									data[0].TransactionData.Product.policyDetails.policyNumber){
										data[0].TransactionData.Product.policyDetails.policyNumber = "";
									}
									//Code Added for Vietnam to Change the Current Date
									if(data[0].TransactionData && data[0].TransactionData.Payer &&
									data[0].TransactionData.Payer.BasicDetails && 
									data[0].TransactionData.Payer.BasicDetails.currentDate){
										data[0].TransactionData.Payer.BasicDetails.currentDate = "";
										var date = new Date();
										$scope.FromDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
										data[0].TransactionData.Payer.BasicDetails.currentDate = $scope.FromDate;
									}
									if(data[0].TransactionData.Requirements &&  data[0].TransactionData.Requirements.length >0){
										data[0].TransactionData.Requirements = [];
									}
									data[0].Id = '';
									data[0].Key3 = "";
                                    data[0].Key14=UtilityService.getFormattedDate();
                                    data[0].Key13=UtilityService.getFormattedDate();
                                    if(!rootConfig.isDeviceMobile && !rootConfig.isOfflineDesktop){
                                    data[0].Key12 = "";             
                                    }else{
                                        data[0].Key12 = UtilityService.getFormattedDate();             
                                    }
//									data[0].Key25 = "";
									data[0].Key24 = "";									
									PersistenceMapping.mapPersistenceToScope(data);
									//PersistenceMapping.clearTransactionKeys();
									IllustratorVariables.setIllustratorModel(data[0].TransactionData);
									$scope.Illustration = IllustratorVariables.getIllustratorModel();                         
									$scope.mapKeysforPersistenceChild(data);
                                    $scope.Illustration.Product.premiumSummary=data[0].TransactionData.Product.premiumSummary;
									var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
									successcallBack(transactionObj);
								}else{
//									$scope.lePopupCtrl.showWarning(translateMessages(
//											$translate, "lifeEngage"), translateMessages(
//													$translate, "IllustrationDraft"),
//											translateMessages($translate, "illustrator.ok"),
//											$scope.rootedOK);
                                    $scope.leadDetShowPopUpMsg = true;
                                    $scope.enterMandatory = translateMessages($translate, "IllustrationDraft");
								}
							}
							
							//extended to add search values
							$scope.initialLoad = function(){
								$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
								$scope.showGAORelatedFields = false;
								$scope.showNotBranchAdminRelatedFields=false;
								if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO){
									$rootScope.hideLanguageSetting = true;
									$scope.showGAORelatedFields = true;
									$scope.showNotBranchAdminRelatedFields=true;
								}
								if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
									$rootScope.hideLanguageSetting = true;
									$scope.showGAORelatedFields = true;
									$scope.showNotBranchAdminRelatedFields=false;
								}
								$scope.isDeviceMobile = rootConfig.isDeviceMobile;
								$scope.emailPattern = rootConfig.emailPattern;
								if($rootScope.selectedPage == "selfTools"){
									$rootScope.showHideLoadingImage(false);
								}
								// formatting Date on Tab navigation 
								$rootScope.stringFormattedDate	= function(){								 	
								}
								// Calling the dataservice to get the product object
								if ($scope.clientId != 0) {
									isSync = false;
									PersistenceMapping.clearTransactionKeys();
									$scope.mapKeysforPersistence();
									if($routeParams.type=='Illustration'){
										var  searchCriteria = {
											 searchCriteriaRequest:{
											"command" : "ListingDashBoard",
											"modes" :['illustration'],
											"value" : ""
											}
										};
										PersistenceMapping.Type='illustration';
										var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
										transactionObj.Type = "illustration";
										if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
											transactionObj.Key35 = UserDetailsService.getAgentRetrieveData().AgentDetails.unit;
											transactionObj.Key36 = transactionObj.Key11;
											transactionObj.Key37 = UserDetailsService.getAgentRetrieveData().AgentDetails.agentType;
										}
										transactionObj.filter = "Key15  <> 'Cancelled'";
										$scope.searchIllustrationKey = $rootScope.searchValue;
										$rootScope.searchValue="";
										$scope.searchIllustrationValue =  IllustratorVariables.searchBiValue;
										IllustratorVariables.searchBiValue = "";
										$scope.searchKeyValue = IllustratorVariables.searchBiKey;
										IllustratorVariables.searchBiKey = "";
										DataService.getFilteredListing(transactionObj, $scope.onGetIllustrationSuccessForListing, $scope.onGetListingsError);
									}else if($routeParams.type=='eApp'){
                                        
										var  searchCriteria = {
													 searchCriteriaRequest:{
											"command" : "ListingDashBoard",
											"modes" :['eApp'],
											"value" : ""
											}
										};
										PersistenceMapping.Type='eApp';
										var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
										transactionObj.Id = $routeParams.transactionId;
										transactionObj.Type = "eApp";
										if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
											transactionObj.Key35 = UserDetailsService.getAgentRetrieveData().AgentDetails.unit;
											transactionObj.Key36 = transactionObj.Key11;
											transactionObj.Key37 = UserDetailsService.getAgentRetrieveData().AgentDetails.agentType;
										}
										transactionObj.filter = "Key15  <> 'Cancelled'";
										$scope.statusFilter = $rootScope.searchValue;
										$rootScope.searchValue="";
										$scope.dynamicSearchKeyDashboard=EappVariables.searchKey;
										EappVariables.searchKey="";
										if(EappVariables.EappSearchValueMemo == rootConfig.statusCountListeAppMemo){
											$scope.memoStatusFilter = EappVariables.EappSearchValueMemo;
										}else{
											$scope.memoStatusFilter="";
										}
										//$rootScope.showHideLoadingImage(false);
										DataService.getFilteredListing(transactionObj, $scope.onGeteAppListingSuccess, $scope.onGetListingsError);	
									}
								}
						  		$scope.accordion();
                       		}
							
							$scope.getFormattedPendingRemarks = function(remarks,successCallBack){
								var str = remarks.split("|");
								var formatedRemarks = "";
								for(var i=0; i<str.length-1; i=i+3){
									if(str[i+2] && str[i+2]!=""){
									formatedRemarks = formatedRemarks+" "+str[i]+" "+str[i+1]+" "+str[i+2];
									} else{
										formatedRemarks = formatedRemarks+" "+str[i]+" "+str[i+1];
									}
								}
								successCallBack(formatedRemarks);
							}
							
							$scope.onGeteAppListingSuccess = function(data) {
								//commented for eapp listing screen breaking issue in IOS
								/*if ((rootConfig.isDeviceMobile)) {
									$.each(data, function(i, obj) {
										obj.TransactionData.ProposalStatus = obj.Key15;
									});
								}*/
								for(var i=0;i<data.length;i++){
									if(data[i].Key4 == "" || typeof data[i].Key4 == "undefined"){
										data[i].Key4 = "-";
									}
									if(data[i].Id == "" || typeof data[i].Id == "undefined"){
										data[i].Id = "-";
									}
									
									//Setting Eapp Listing page valuea as Pending instead of PendingUploaded
									if(data[i].Key7 != "" && data[i].Key7 == "PendingUploaded"){
									     data[i].Key7 = "Memo";
									}
									if(data[i].Key7 != "" && data[i].Key7 != "Submitted"){
									     data[i].Key15 = data[i].Key7;
									}
									//Converting from string to integer for sorting
									if(data[i].Key23 != ""){
										if(data[i].Key23.indexOf('e') > 0){
                                        	data[i].Key23 = Number(data[i].Key23);
										}
										data[i].Key23 = parseInt(data[i].Key23);
									}
								}
								$scope.proposalCorrespLead = [];
                                if($rootScope.fromMenuController){
									IllustratorVariables.leadId=angular.copy($rootScope.fromMenuControllerLeadId);
									$rootScope.fromMenuControllerLeadId="";
									$rootScope.fromMenuController=false;
								}
								if(IllustratorVariables.leadId!=undefined && IllustratorVariables.leadId!=0){// Filtering for Lead based BI 
									for (var i=0;i<data.length;i++){
										if(data[i].Key1 == IllustratorVariables.leadId){
											$scope.proposalCorrespLead.push(data[i]);//UAT bug fix 396 not able to navigate to eapp when clicking leads eapp
										}
									}
								}else{	
									$scope.proposalCorrespLead = data;
								}
								$scope.Proposals = $filter('orderBy')($scope.proposalCorrespLead, 'Key14', true);//SIT bug fix 1013 sorting using created/modified date.
								$scope.tableProposals =  [].concat($scope.Proposals);
								$rootScope.showHideLoadingImage(false);
								$scope.refresh();
							};
							
							
							$scope.mapKeysforPersistenceChild = function(data) {
								PersistenceMapping.Type = "illustration";
								PersistenceMapping.Key15 = "Draft";
								PersistenceMapping.Key3 = "";
								PersistenceMapping.TransTrackingID = UtilityService.getTransTrackingID();
			                    PersistenceMapping.Key5 = data[0].Key5;
			                    PersistenceMapping.Key16 = "";
			                    PersistenceMapping.Key19 = data[0].Key19;
			                    
			                    
			                    GLI_IllustratorService.clearingIllustrationData($scope,IllustratorVariables,$rootScope);
							}
							// After downloading, set the document object from
                          	// LifeEngage to EappAttachment. Delete document
                          	// metadata contents from LifeEngage scope object
                          	$scope.onGetServerDocSuccess = function(successcallback) {
                          		if ($scope.Illustration.Upload && $scope.Illustration.Upload.Signature && !angular.equals($scope.Illustration.Upload.Signature[0], {})) {
                          			if(typeof $scope.Illustration.Upload.Signature[0] == "undefined"){
                          				$scope.IllustrationAttachment = IllustratorVariables.getIllustrationAttachmentModel();
                          			}
                          			var tempPhoto = angular.copy($scope.Illustration.Upload.Signature[0]);
                          			if(typeof tempPhoto!= "undefined") {									
											if (tempPhoto.documentName != "") {
												var docDetails = {
													Documents : {
														documentName : tempPhoto.documentName
													}
												};
												PersistenceMapping.clearTransactionKeys();
												IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
												var docreq1 = PersistenceMapping.mapScopeToPersistence(docDetails);
												var arrayIndex = 0;
												DocumentService.startDownloadForEachTransaction(arrayIndex, docreq1, function(docDetails) {
													var page1 = docDetails;
													$scope.Illustration.Upload.Signature[0] = page1;
													$scope.Illustration.Upload.Signature[0].pages[0].base64string = page1.pages[0].base64string.replace(/\\/g, '');
													$scope.IllustrationAttachment.Upload.Signature = $scope.Illustration.Upload.Signature;
													// To set the doc
													// status so as to
													// to avoid
													// unnecessary
													// uploaddoc service
													// call
													$scope.IllustrationAttachment.Upload.Signature[0].documentStatus = "Synced";
													IllustratorVariables.setIllustrationAttachmentModel($scope.IllustrationAttachment);
													delete $scope.Illustration.Upload;
													successcallback();
												});
											} else {
											    successcallback();
											}
								    } else {
									    successcallback();
									    }
                          		}
                          		else{
                          			successcallback();
                          		}
                          	}
							$scope.mapKeysfromPersistence = function(successcallback) {
								$rootScope.customerId = PersistenceMapping.Key1;
                          		$rootScope.agentId = PersistenceMapping.Key11;
                          		IllustratorVariables.illustrationCreationDate = PersistenceMapping.Key13;
                          		// $scope.status =PersistenceMapping.Key15 ;
                          		$scope.Illustration = PersistenceMapping.transactionData;
                          		IllustratorVariables.transTrackingID = PersistenceMapping.TransTrackingID;
                          		// After Sync From Server, Country and State
                          		// values are coming as String, but Number is
                          		// expected in the CustomDDL
								$scope.IllustrationAttachment = IllustratorVariables.getIllustrationAttachmentModel();
                          		if ((rootConfig.isDeviceMobile)) {
                          			DataService.getDocumentsForTransaction($rootScope.transactionId, function(docData) {
                          				if (docData.length != 0) {
                          					var Signature = [];
                          					// Can
                          					// have
                          					// more
                          					// than
                          					// one
                          					// Signature
                          					for (var i = 0; i < docData.length; i++) {
                          						if (docData[i].DocumentType == 'Signature') {
                          							Signature.push(JSON.parse(docData[i].DocumentObject));
                          						}
                          					}
                          					if (Signature.length > 0) {
                          						$scope.IllustrationAttachment.Upload.Signature = Signature;
                          						IllustratorVariables.setIllustrationAttachmentModel($scope.IllustrationAttachment);
                          						Signature = [];
                          					}
                          				}
                          				successcallback();
                          			}, function(error) {// errorCallback(error);
                          			});

                          		} else {
                          			$scope.onGetServerDocSuccess(function(){
                          				successcallback();
                          			});
                          		}
                          		$scope.refresh();
                          	}
							
							$scope.getLeadDetails  = function(successcallback ){
								LmsVariables.lmsKeys.Key1 = IllustratorVariables.leadId;
								LmsVariables.lmsKeys.Id = "";
								LmsService.getLead(function(leaddata){
									IllustratorVariables.leadName = leaddata.Key22;
	                          		IllustratorVariables.leadEmailId = leaddata.Key20;
	                          		successcallback();
								},successcallback);
							}
							
							$scope.navigateToBI = function(illustationdata){
								if(illustationdata.Key5 == ""){
									illustationdata.Key5 = '0';
									}
									if(illustationdata.Key3 == ""){
										illustationdata.Key3 = '0';
									}
									if(illustationdata.Id == ""){
										illustationdata.Id = '0';
									}
								if((rootConfig.isDeviceMobile)){
									if(illustationdata.Key15 == "Completed"){
										//Checking whether the Completed illustration has an eApp
										$rootScope.haseApp = false;
										GLI_DataService.getRelatedEappForBI(illustationdata.TransTrackingID,function(eAppData){
											if (eAppData
											&& eAppData[0]
											&& eAppData[0].TransactionData) {
												$rootScope.haseApp = true;
												$rootScope.premiumSummaryOutput = true;
												$scope.IllustrationAttachment = IllustratorVariables.getIllustrationAttachmentModel();
												$location.path('/illustration/'+illustationdata.Key5+'/0/'+illustationdata.Key3+'/'+illustationdata.Id);
											}
											else{
												$rootScope.haseApp = false;
												$rootScope.premiumSummaryOutput = false;
												$location.path('/illustratorProduct/'+illustationdata.Key5+'/0/'+illustationdata.Key3+'/'+illustationdata.Id);	
											}
										},function(){});
									}
									else if(illustationdata.Key15 == "Confirmed"){
										$rootScope.premiumSummaryOutput = true;
										$scope.IllustrationAttachment = IllustratorVariables.getIllustrationAttachmentModel();
										$location.path('/illustration/'+illustationdata.Key5+'/0/'+illustationdata.Key3+'/'+illustationdata.Id);
									}
								}
								else{
									$rootScope.haseApp = false;
									if(illustationdata.Key15 == "Completed"){
										//Checking whether a Completed illustration has an eApp
										PersistenceMapping.clearTransactionKeys();
										var  searchCriteria = {
												 searchCriteriaRequest:{
										"command" : "RelatedTransactions",
										"modes" :['eApp'],
										"value" : illustationdata.TransTrackingID
										}
										};
										PersistenceMapping.Type = "eApp";
										var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
										transactionObj.Key1 = illustationdata.TransTrackingID;
										transactionObj.filter = "Key15 <> 'Cancelled'";

										DataService.getFilteredListing(transactionObj, function(eAppData) {
											if (eAppData
											&& eAppData[0]
											&& eAppData[0].TransactionData) {
												
												$rootScope.haseApp = true;
												$location.path('/illustration/'+illustationdata.Key5+'/0/'+illustationdata.Key3+'/0');
											}
											else{
												$rootScope.haseApp = false;
												$location.path('/illustratorProduct/'+illustationdata.Key5+'/0/'+illustationdata.Key3+'/0');
											}
										},function(){});
									}
									
									else if(illustationdata.Key15 == "Confirmed"){
										$location.path('/illustration/'+illustationdata.Key5+'/0/'+illustationdata.Key3+'/0');
									}
								}
								$scope.refresh();
							}
							
							$scope.onGetListingsSuccess = function(data){
								
								for(var i=0;i<data.length;i++){
									if(data[i].Key4 == "" || typeof data[i].Key4 == "undefined"){
									data[i].Key4 = "-";
									}
									if(data[i].Id == "" || typeof data[i].Id == "undefined"){
									data[i].Id = "-";
									}
									}
								/*if(data[0].TransactionData.Requirements && data[0].TransactionData.Requirements.length >0){
									for(var i=0;i<data[0].TransactionData.Requirements.length;i++){
                                        if(data[0].TransactionData.Requirements[i].Documents[0]!==undefined){
										data[0].TransactionData.Requirements[i].Documents[0].fileName = (data[0].TransactionData.Requirements[i].Documents[0].pages[0]!==undefined)?data[0].TransactionData.Requirements[i].Documents[0].pages[0].fileName:data[0].TransactionData.Requirements[i].Documents[0].fileName;
                                        }
									}
								}*/
								IllustratorVariables.setIllustratorModel(data[0].TransactionData);
								IllustratorVariables.illustratorId = data[0].Key3;
								IllustratorVariables.leadId = data[0].Key1;
								IllustratorVariables.fromChooseAnotherProduct = data[0].Key7;
                          		IllustratorVariables.fnaId = data[0].Key2;
                          		IllustratorVariables.illustratorKeys.Key12 = data[0].Key12;
                          		IllustratorVariables.transTrackingID = data[0].TransTrackingID;
                          		IllustratorVariables.illustrationStatus = data[0].Key15;
                          		IllustratorVariables.leadName = data[0].Key22;
	                          	IllustratorVariables.leadEmailId = data[0].Key23;
								IllustratorVariables.agentForGAO = data[0].Key11;
								IllustratorVariables.GAOId = data[0].Key36;
								IllustratorVariables.agentNameForGAO = data[0].Key38;
								IllustratorVariables.agentType= data[0].Key37;
								IllustratorVariables.GAOOfficeCode=data[0].Key35;
                          		PersistenceMapping.mapPersistenceToScope(data);
                          		$scope.mapKeysfromPersistence(function(){
                          			var productId = data[0].TransactionData.Product.ProductDetails.productId;
                          			var transactionObj = {
                          					"CheckDate" : new Date().getMonth() + 1 + "-" + new Date().getDate() + "-" + new Date().getFullYear(),
                          					"Products" : {
                          						"id" : productId,
                          						"code" : "",
                          						"carrierCode" : 1,
												"apiVersion":2
                          					}
                          			}
                          			ProductService.getProduct(transactionObj, function(product) {
                          				IllustratorVariables.productDetails = JSON.parse(JSON.stringify(product));
                          				var personalDetailsJsonName;
                          				var pdfTemplateDetails = [];
                          				var eAppPdfTemplateDetails = [];
                          				for (template in IllustratorVariables.productDetails.templates) {
                          					if (IllustratorVariables.productDetails.templates[template].templateType == '22001') {
                          						personalDetailsJsonName = IllustratorVariables.productDetails.templates[template].templateName + '.json';
                          					} else if (IllustratorVariables.productDetails.templates[template].templateType == '22002') {
                          						IllustratorVariables.illustrationInputTemplate = IllustratorVariables.productDetails.templates[template].templateName + '.json';
                          					} else if (IllustratorVariables.productDetails.templates[template].templateType == '22003') {
                          						IllustratorVariables.illustrationOutputTemplate = IllustratorVariables.productDetails.templates[template].templateName + '.json';
                          					}
                          					else if (IllustratorVariables.productDetails.templates[template].templateType == '22004') {

                          						var objPdfTemplate = {};

                          						objPdfTemplate.templateId = IllustratorVariables.productDetails.templates[template].templateId;
                          						objPdfTemplate.language = IllustratorVariables.productDetails.templates[template].language;
                          						objPdfTemplate.templateName = IllustratorVariables.productDetails.templates[template].templateName;
                          						eAppPdfTemplateDetails.push(objPdfTemplate);

                          					}
                          					else if (IllustratorVariables.productDetails.templates[template].templateType == '22005') {

                          						var objPdfTemplate = {};

                          						objPdfTemplate.templateId = IllustratorVariables.productDetails.templates[template].templateId;
                          						objPdfTemplate.language = IllustratorVariables.productDetails.templates[template].language;
                          						objPdfTemplate.templateName = IllustratorVariables.productDetails.templates[template].templateName;
                          						pdfTemplateDetails.push(objPdfTemplate);
                          					}
                          					else if (IllustratorVariables.productDetails.templates[template].templateType == '22006') {
                          						$scope.Illustration.Product.templates.eAppInputTemplate = IllustratorVariables.productDetails.templates[template].templateName + '.json';
                          					}
                          					$scope.Illustration.Product.templates.illustrationPdfTemplate = pdfTemplateDetails;
                          				}
                          				$scope.navigateToBI(data[0]);

                          			}, function(data) {
                          				alert("Error in get product Service " + data);
                          			});
                          			
                          				
                          		});
								
							}
							//Handle navigation based to status
							$scope.navigateOnEdit = function(transaction){
								if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO){
									if(transaction.Key36!=UserDetailsService.getAgentRetrieveData().AgentDetails.agentCode){
										$rootScope.showHideLoadingImage(false);
										$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"),translateMessages($translate,"eapp.unlockDiffAgentErrorSingleIllustration"),translateMessages($translate,"fna.ok"));
									}else {
										$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
										$scope.refresh();
										navigateOnEditContinue(transaction);
									}
								}else{
									$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
									$scope.refresh();
									navigateOnEditContinue(transaction);
								}
							};
							function navigateOnEditContinue(transaction){
								$rootScope.editIllustrator=true;
                                if(!rootConfig.isDeviceMobile && !rootConfig.isOfflineDesktop){
                                    transaction.Key12="";
                                }
								IllustratorVariables.illustrationConfirmedDate = transaction.Key14;
								if($scope.isRDSUser){
									IllustratorVariables.illustrationNumberRDS = transaction.Key24;
								}
								if(transaction.Key2 != "" && typeof transaction.Key2 != "undefined"){
									$rootScope.isFromIllustration = false;
								}else{
									$rootScope.isFromIllustration = true;
								}
								IllustratorVariables.fromFNAChoosePartyScreenFlow = false;
								 /*FNA changes by LE Team >>> starts - Bug 4194*/
								FnaVariables.flagFromCustomerProfileFNA = false;
								 /*FNA changes by LE Team >>> ends - Bug 4194*/
								if(UtilityService.leadPage==''){
									UtilityService.leadPage = 'GenericListing';
								}
								if(transaction.Key15=="Draft"){
									if(transaction.Key5 == ""){
										transaction.Key5 = '0';
									}
									if(transaction.Key3 == ""){
										transaction.Key3 = '0';
									}
									if(transaction.Id == ""){
										transaction.Id = '0';
									}
									if((rootConfig.isDeviceMobile)){
										$location.path('/Illustrator/'+transaction.Key5+'/0/'+transaction.Key3+'/'+transaction.Id);
									}
									else{
										$location.path('/Illustrator/'+transaction.Key5+'/0/'+transaction.Key3+'/0');
									}
								}else if(transaction.Key15=="Completed" || transaction.Key15=="Confirmed"){
									if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
										transaction.Key11=transaction.Key36;
									}
									DataService.getListingDetail(transaction, $scope.onGetListingsSuccess, $scope.onGetListingsError);
								}
							};
							//delete Eapp
							$scope.toggleSelectionEapp=function toggleSelectionEapp(proposal){
								var idx = $scope.selectedProposal.indexOf(proposal);
								// is currently selected
								if (idx > -1) {
									$scope.selectedProposal.splice(idx, 1);
								}
								// is newly selected
								else {
									$scope.selectedProposal.push(proposal);
								}
								if($scope.selectedProposal.length==1){
									for (var j = 0; j < rootConfig.proposalStatus.length; j++){
										if(($scope.selectedProposal[0].Key15).indexOf(rootConfig.proposalStatus[j]) != -1 || ($scope.selectedProposal[0].Key7).indexOf(rootConfig.proposalStatus[j]) != -1 ){
											$scope.DeleteEappButtonDisable=false;
											break;
										}else{
											$scope.DeleteEappButtonDisable=true;
										}
									}
								}else if($scope.selectedProposal.length<1){
									$scope.DeleteEappButtonDisable=true;
								}else{
									$scope.DeleteEappButtonDisable=false;
								}
							}
							
							$scope.setIllustrationNumberForRDSUser = function(illustrationId, illustrationNumberRDS){
								$scope.isRDSUser = UserDetailsService.getRDSUser();
								if($scope.isRDSUser){
									return illustrationNumberRDS;
								}
								else{
									return illustrationId;
								}
							}
//							$scope.PopulateIllustrationData=function(){
//								for (var i=0;i<$scope.tableIllustrations.length;i++){
//								    $scope.tableIllustrations[i].Illustration=JSON.parse(unescape($scope.tableIllustrations[i].TransactionData));
//									$scope.tableIllustrations[i].Key27=$scope.tableIllustrations[i].Illustration.Insured.BasicDetails.nickName;
//								  }
//							}
							
							//Products Page Landing
							  $scope.linkSubTab = function (tab) {
							        if (tab == "Product") {
							        	$location.path('/products/0/0/0/0/true');
							        	$rootScope.refresh();
							        }
							  }
							 //Accordion View
							  $scope.accordion= function (){
							var acc = document.getElementsByClassName("formsSelfTools");
							var i;
							  
							for (i = 0; i < acc.length; i++) {
							acc[i].addEventListener("click", function() {
							this.classList.toggle("active");
							var panel = this.nextElementSibling;
								if (panel.style.display === "block") {
									panel.style.display = "none";
									this.lastElementChild.src='css/generali-theme/img/icon/scroll_arrow_down.png';
								} else {
								  panel.style.display = "block";
								  this.lastElementChild.src='css/generali-theme/img/icon/scroll_arrow_right2.png';
								   }
								 });
							   } 
							 }
							  
           /* $scope.proceedToPrint = function() {
				$scope.dratCheck = true;
				if ($scope.selectedProposal.length > 0) {
					for(var i=0;i<$scope.selectedProposal.length;i++) {
						if($scope.selectedProposal[i].Key15=='Draft'){
							$scope.dratCheck = false;
							break;
						}
					}
				    if(!$scope.dratCheck){
					   $scope.showValidationErrorforPrint();
				    }else{
                        $scope.printPDFfile(); 
                    }
				}								
            }*/
            
            $scope.showValidationErrorforPrint=function(){
                $scope.leadDetShowPopUpMsg = true;
                $scope.enterMandatory = translateMessages($translate, "illustrator.printSIValidationErrorStatusCheck");
                $rootScope.showHideLoadingImage(false);
                $scope.refresh();
            }
            
            $scope.syncProgressCallback = function(currentIndex,totalCount){
    			// set the progress percentage and message
    			//$scope.syncProgress=progress;
    			$rootScope.showHideLoadingImage(false, "");
    			//$scope.syncMessage= translateMessages($translate,"uploadingDocuments")+currentIndex + translateMessages($translate,"uploadOf")+totalCount + translateMessages($translate,"documents") ;
    			/*if(currentIndex==totalCount){
    				$rootScope.showHideLoadingImage(true,'paintUIMessage', $translate);
    				$scope.hideSyncPopup();
    			}*/
    			$scope.refresh();
	    	};
	    	$scope.syncModuleProgress = function(syncMessage,progress){
    			// set the progress percentage and message
    			$rootScope.showHideLoadingImage(false, "");
    			$scope.syncMessage=syncMessage;
    			$scope.refresh();
	    	};
	    	
	    	
	    	$scope.proceedToPrint = function() {
	    		 $rootScope.showHideLoadingImage(true, "Loading.."); 
	        	if ($scope.selectedProposal.length != 0) {
					if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
						$scope.selectedProposal[0].Key11=$scope.selectedProposal[0].Key36;
					}
	        		DataService.getListingDetail($scope.selectedProposal[0], function (data) {
	        			$scope.checkIllustrationPrintStatus(data);
	        		}, function(){
	        			$rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
						$rootScope.showHideLoadingImage(false);
						$scope.refresh();
	        		});
				}
	        }
	        
	        $scope.checkIllustrationPrintStatus = function(transactions){
	        	$scope.dratCheck = true;
	        	if (transactions.length > 0) {
					for(var i=0;i<transactions.length;i++) {
						if(transactions[i].Key15=='Draft'){
							$scope.dratCheck = false;
							break;
						}	
					}
					if(!$scope.dratCheck){
						$scope.showValidationErrorforPrint();
					}else{
						 $scope.printPDFfile(transactions); 
					}
				}
	        }
            
            $scope.printPDFfile = function (data) { 
            	$rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
    			$scope.$apply();
    			IllustratorVariables.illustratorId = data[0].Key3;
    	        IllustratorVariables.fnaId = data[0].Key2;
    	        IllustratorVariables.fromChooseAnotherProduct = data[0].Key7;
    	        IllustratorVariables.illustratorKeys.Key12 = data[0].Key12;
    	        IllustratorVariables.leadId = data[0].Key1;
    	        IllustratorVariables.leadName = data[0].Key22;
    	        IllustratorVariables.leadEmailId = data[0].Key23;
    	        IllustratorVariables.illustrationNumberRDS = data[0].Key24;
    	        $scope.productId = data[0].Key5;
    			$scope.Illustration = data[0].TransactionData;
    	        $rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
    	        PersistenceMapping.clearTransactionKeys();
    	        IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
    	        PersistenceMapping.dataIdentifyFlag = false;
    	        PersistenceMapping.Type = "illustration";
    	        var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
    	        transactionObj.TransactionData.Product.templates.selectedLanguage = "th";
    	        if (rootConfig.isDeviceMobile) {
    	            if (checkConnection()) {
    					$rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
    					var transactionsToSync = [];
    					transactionsToSync.push({
    	                    "Key" : "illustration",
    	                    "Value" : data[0].Id,
    						"syncType": "indSyncBI"
    	                });
    	                IllustratorService.individualSync($scope, IllustratorVariables, transactionsToSync, function() {
    						GLI_DataService.getListings(transactionObj,function(BIData){
    							transactionObj.Key3 = BIData[0].Key3;
    							$rootScope.showHideLoadingImage(true, "Loading..");
    							if((transactionObj.Key19=="GENBUMNAN8")||(transactionObj.Key19=="GenBumnan")){
    								$scope.templateId = "1004";
    					 		} else if((transactionObj.Key19=="COMPLETE HEALTH SOLUTION 8080")||(transactionObj.Key19=="GenCompleteHealth")){
    					        	$scope.templateId = "1005";
    					        } else if(transactionObj.Key19=="GenProLife"){
                                    $scope.templateId = "1008";
       							} else if(transactionObj.Key19=="GenSave20Plus"){
                                    $scope.templateId = "1010";
       							} else if((transactionObj.Key19=="Wholelife") ||(transactionObj.Key19 == "Gen Protection 5")){
                                    $scope.templateId = "1029";
       							} else if(transactionObj.Key19=="GenCancerSuperProtection"){
    								$scope.templateId = "1025";
    							} else if(transactionObj.Key19=="GenSave10Plus"){
    								$scope.templateId = "1031";
    							} else if(transactionObj.Key19=="GenSave4Plus"){
    								$scope.templateId = "1033";
    							}
    							$rootScope.showHideLoadingImage(true, "Loading..");
    							DataService.downloadPDF(transactionObj, $scope.templateId, function(data) {      
    								if($('body').hasClass('ipad')){
    									$rootScope.showHideLoadingImage(false);
    									$scope.refresh();
    								} else {
    									$rootScope.showHideLoadingImage(false);
    									$rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"),translateMessages($translate,"illustrator.pdfDownloadTransactionsSuccess"),translateMessages($translate,"illustrator.popUpClose"));
    									$scope.refresh();
    								}
    							},function(error){
    								$rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
    								$rootScope.showHideLoadingImage(false);
    								$scope.refresh();
    							});
    						},function(){
    							$rootScope.showHideLoadingImage(false);

    						});
    	                },function(data){
    						$scope.documentError(data);
    					},$scope.syncProgressCallback,$scope.syncModuleProgress);
    	            } else {
    	                $rootScope.showHideLoadingImage(false);
    	                $scope.leadDetShowPopUpMsg = true;
                        $scope.enterMandatory = translateMessages($translate, "illustrator.userNotOnline");
    	                $scope.refresh();
    	            }
    	        } else {
					$rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
					$scope.$apply();
    	        	if((transactionObj.Key19=="GENBUMNAN8")||(transactionObj.Key19=="GenBumnan")){
						$scope.templateId = "1004";
			 		} else if((transactionObj.Key19=="COMPLETE HEALTH SOLUTION 8080")||(transactionObj.Key19=="GenCompleteHealth")){
			        	$scope.templateId = "1005";
			        } else if(transactionObj.Key19=="GenProLife"){
                        $scope.templateId = "1008";
					} else if(transactionObj.Key19=="GenSave20Plus"){
                        $scope.templateId = "1010";
					} else if((transactionObj.Key19=="Wholelife") ||(transactionObj.Key19 == "Gen Protection 5")){
                        $scope.templateId = "1029";
					} else if(transactionObj.Key19=="GenCancerSuperProtection"){
						$scope.templateId = "1025";
					} else if(transactionObj.Key19=="GenSave10Plus"){
						$scope.templateId = "1031";
					} else if(transactionObj.Key19=="GenSave4Plus"){
						$scope.templateId = "1033";
					}
					$rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
    	            DataService.downloadPDF(transactionObj, $scope.templateId, function (data) {
    	                $rootScope.showHideLoadingImage(false);
    	                $scope.refresh();
    	            }, function (error) {
    	                $rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
    	                $rootScope.showHideLoadingImage(false);
    	                $scope.refresh();
    	            });
    	        }							
        }
            
        $scope.showValidationErrorforDownload=function(){
            $rootScope.showHideLoadingImage(false, "Loading,please wait..");                
            $scope.leadDetShowPopUpMsg = true;
            $scope.enterMandatory = translateMessages($translate, "illustrator.downloadSIValidationErrorStatusCheck");
            $scope.refresh();
        }
        
        $scope.proceedToDownload = function() {
            $rootScope.showHideLoadingImage(true, "Loading..");
        	if ($scope.selectedProposal.length != 0) {
				if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
					$scope.selectedProposal[0].Key11=$scope.selectedProposal[0].Key36;
				}
        		DataService.getListingDetail($scope.selectedProposal[0], function (data) {
        			$scope.checkIllustrationDownloadStatus(data);
        		}, function(){
        			$rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
					$rootScope.showHideLoadingImage(false);
					$scope.refresh();
        		});
			}
        }
        
        $scope.checkIllustrationDownloadStatus = function(transactions){
        	$scope.dratCheck = true;
        	if (transactions.length > 0) {
				for(var i=0;i<transactions.length;i++) {
					if(transactions[i].Key15=='Draft'){
						$scope.dratCheck = false;
						break;
					}	
				}
				if(!$scope.dratCheck){
					$scope.showValidationErrorforDownload();
				}else{
					 $scope.downloadApplicationPDF(transactions); 
				}
			}
        }

        $scope.increaseLimit = function () {
        $scope.barLimit += 10;
        
      }
         $scope.playVideo = function (videofile) {
        MediaService.playVideo($scope, videofile);
    };
    
    		$scope.openPDF = function (value) {
    		MediaService.openPDF($scope, $window, value);
    }
    		
    		$scope.openPopUp = function() {  
    			$scope.overlayTC=true;
    			$scope.showInformation = true;        
    			}

    		$scope.closeTC = function () {
    	        $scope.showInformation = false;
    	        $scope.overlayTC = false;
    	};
        
        $scope.hospitalNetworkLink = function()
        {
            var url = rootConfig.hospitalNetworkURL; 
             
            if (!rootConfig.isDeviceMobile) 
            {   
                window.open(url,'_blank');
            }
            else
            {
                window.open(url, '_system');        
            }
        }

		$scope.downloadApplicationPDF = function (data) {
			$rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
			$scope.$apply();
			IllustratorVariables.illustratorId = data[0].Key3;
	        IllustratorVariables.fnaId = data[0].Key2;
	        IllustratorVariables.fromChooseAnotherProduct = data[0].Key7;
	        IllustratorVariables.illustratorKeys.Key12 = data[0].Key12;
	        IllustratorVariables.leadId = data[0].Key1;
	        IllustratorVariables.leadName = data[0].Key22;
	        IllustratorVariables.leadEmailId = data[0].Key23;
	        IllustratorVariables.illustrationNumberRDS = data[0].Key24;
	        $scope.productId = data[0].Key5;
			$scope.Illustration = data[0].TransactionData;
	        $rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
	        PersistenceMapping.clearTransactionKeys();
	        IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
	        PersistenceMapping.dataIdentifyFlag = false;
	        PersistenceMapping.Type = "illustration";
	        var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
	        transactionObj.TransactionData.Product.templates.selectedLanguage = "th";
	        if (rootConfig.isDeviceMobile) {
	            if (checkConnection()) {
					$rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
					var transactionsToSync = [];
					transactionsToSync.push({
	                    "Key" : "illustration",
	                    "Value" : data[0].Id,
						"syncType": "indSyncBI"
	                });
	                IllustratorService.individualSync($scope, IllustratorVariables, transactionsToSync, function() {
						GLI_DataService.getListings(transactionObj,function(BIData){
							transactionObj.Key3 = BIData[0].Key3;
							$rootScope.showHideLoadingImage(true, "Loading..");
							if((transactionObj.Key19=="GENBUMNAN8")||(transactionObj.Key19=="GenBumnan")){
								$scope.templateId = "1004";
					 		} else if((transactionObj.Key19=="COMPLETE HEALTH SOLUTION 8080")||(transactionObj.Key19=="GenCompleteHealth")){
					        	$scope.templateId = "1005";
					        } else if(transactionObj.Key19=="GenProLife"){
                                $scope.templateId = "1008";
   							} else if(transactionObj.Key19=="GenSave20Plus"){
                                $scope.templateId = "1010";
   							} else if((transactionObj.Key19=="Wholelife") ||(transactionObj.Key19 == "Gen Protection 5")){
                                $scope.templateId = "1029";
   							} else if(transactionObj.Key19=="GenCancerSuperProtection"){
								$scope.templateId = "1025";
							} else if(transactionObj.Key19=="GenSave10Plus"){
								$scope.templateId = "1031";
							} else if(transactionObj.Key19=="GenSave4Plus"){
								$scope.templateId = "1033";
							}
							$rootScope.showHideLoadingImage(true, "Loading..");
							DataService.downloadPDF(transactionObj, $scope.templateId, function(data) {      
								if($('body').hasClass('ipad')){
									$rootScope.showHideLoadingImage(false);
									$scope.refresh();
								}
								else{
									$rootScope.showHideLoadingImage(false);
									$rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"),translateMessages($translate,"illustrator.pdfDownloadTransactionsSuccess"),translateMessages($translate,"illustrator.popUpClose"));
									$scope.refresh();
								}
							},function(error){
								$rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
								$rootScope.showHideLoadingImage(false);
								$scope.refresh();
							});
						},function(){
							$rootScope.showHideLoadingImage(false);

						});
	                },function(data){
						$scope.documentError(data);
					},$scope.syncProgressCallback,$scope.syncModuleProgress);
	            } else {
	                $rootScope.showHideLoadingImage(false);
	                $scope.leadDetShowPopUpMsg = true;
                    $scope.enterMandatory = translateMessages($translate, "illustrator.userNotOnline");
	                $scope.refresh();
	            }
	        } else {
	        	if((transactionObj.Key19=="GENBUMNAN8")||(transactionObj.Key19=="GenBumnan")){
					$scope.templateId = "1004";
		 		} else if((transactionObj.Key19=="COMPLETE HEALTH SOLUTION 8080")||(transactionObj.Key19=="GenCompleteHealth")){
		        	$scope.templateId = "1005";
		        } else if(transactionObj.Key19=="GenProLife"){
		        	$scope.templateId = "1008";
				} else if(transactionObj.Key19=="GenSave20Plus"){
					$scope.templateId = "1010";
				} else if((transactionObj.Key19=="Wholelife") ||(transactionObj.Key19 == "Gen Protection 5")){
					$scope.templateId = "1029";
				} else if(transactionObj.Key19=="GenCancerSuperProtection"){
					$scope.templateId = "1025";
				} else if(transactionObj.Key19=="GenSave10Plus"){
					$scope.templateId = "1031";
				} else if(transactionObj.Key19=="GenSave4Plus"){
					$scope.templateId = "1033";
				}
	            DataService.downloadPDF(transactionObj, $scope.templateId, function (data) {
	                $rootScope.showHideLoadingImage(false);
	                $scope.refresh();
	            }, function (error) {
	                $rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
	                $rootScope.showHideLoadingImage(false);
	                $scope.refresh();
	            });
	        }							
        };
		$scope.closePopup = function () {
			$scope.leadDetShowPopUpMsg = false;
		};
		//Proceed to Memo page
		$scope.proceedToMemo = function(data){
			//if(parseInt(data.Key30)>0){ //memo count
				$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
				GLI_EappVariables.memoCount=data.Key30;
				GLI_EappVariables.leadName=data.Key6;
				GLI_EappVariables.EappKeys.Key21 = data.Key21;
				PersistenceMapping.clearTransactionKeys();
				PersistenceMapping.Key4 = (data.Key4=="-")?data.Key4="":data.Key4;
				PersistenceMapping.Key24 = data.Key24;
				PersistenceMapping.Id = data.Id;
				PersistenceMapping.TransTrackingID = data.TransTrackingID;
				PersistenceMapping.Type = "eApp";
				GLI_EappVariables.EappKeys.Key1 = (data.Key1=="")?0:data.Key1;
				GLI_EappVariables.EappKeys.Key2 = (data.Key2=="")?0:data.Key2;
				GLI_EappVariables.EappKeys.Key3 = (data.Key3=="")?0:data.Key3;
				GLI_EappVariables.EappKeys.Key4 = (data.Key4=="-"||data.Key4=="")?0:data.Key4;
				GLI_EappVariables.EappKeys.Key5 = data.Key5;
				GLI_EappVariables.EappKeys.Key24 = data.Key24;
				GLI_EappVariables.EappKeys.Id = data.Id;
				var transactionObj = PersistenceMapping.mapScopeToPersistence();
				DataService.getListingDetail(transactionObj, function(EappData){
					EappVariables.setEappModel(EappData[0].TransactionData);
                    $rootScope.enableRefresh=false;
					$location.path('/memo');
					$scope.refresh();
				}, function(){
					$rootScope.showHideLoadingImage(false);
				});
			//}
			//else{
			//	$rootScope.showHideLoadingImage(false);
			//	$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"), translateMessages($translate,"eapp.memoCountMessage"), translateMessages($translate,"fna.ok"));
			//}
		};
		//GAO request pop up
		$scope.GAOpopup = function() {
			if($scope.selectedProposal.length > 0){
				for(var i=0;i<$scope.selectedProposal.length;i++){
					var flagCheck=false;
					if($scope.selectedProposal[i].Key15!=rootConfig.EappStausPendingSubmission){ //Pending Submission
						flagCheck=true;
						break;
					}
				}
				if(flagCheck){
					$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"),translateMessages($translate,"general.GAOeappListMessage"),translateMessages($translate, "fna.ok"), $scope.cancelBtnAction);
				}else{
					$rootScope.lePopupCtrl.showWarning(translateMessages( $translate, "lifeEngage"),
					translateMessages( $translate, "illustrator.GAOpopupmessage"), translateMessages( $translate, "illustrator.confirmgao"), $scope.confirmGAOClick, translateMessages( $translate, "illustrator.cancelgao"), $scope.cancelBtnAction,$scope.cancelBtnAction);
				}
			}
		};
		//Function on confirm button click on GAO Request pop up
		$scope.confirmGAOClick = function(){
			$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
			if(rootConfig.isDeviceMobile){
				$scope.GAORequstSave();
			}else{
				var transData=[];
				for(var i=0;i<$scope.selectedProposal.length;i++){
					var transactionObj = {
						"Id":"",
						"GAOOffice":"",
						"GAOCode":"",
						"Type":"",
						"Action":""
					};
					transactionObj.Id=$scope.selectedProposal[i].Key4;
					transactionObj.Type = "eApp";
					transactionObj.Action = "assignGAO";
					transactionObj.GAOOffice = UserDetailsService.getAgentRetrieveData().AgentDetails.unit;
					transactionObj.AgentName = UserDetailsService.getAgentRetrieveData().AgentDetails.agentName;
					transData.push(transactionObj);
				}
				PersistenceMapping.clearTransactionKeys();
				DataService.requestGAOSupport(transData, function () {
					PersistenceMapping.clearTransactionKeys();
					$scope.mapKeysforPersistence();
					var  searchCriteria = {
						 searchCriteriaRequest:{
							"command" : "ListingDashBoard",
							"modes" :['eApp'],
							"value" : ""
						}
					};
					PersistenceMapping.Type='eApp';
					var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
					transactionObj.Type = "eApp";
					transactionObj.filter = "Key15  <> 'Cancelled'";
					DataService.getFilteredListing(transactionObj, $scope.onGeteAppListingSuccess, $scope.onGetListingsError);
				}, function (error) {
					$rootScope.showHideLoadingImage(false);
					$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"), translateMessages($translate,"eapp.GAORequestFailed"), translateMessages($translate,"fna.ok"));
				});
			}
		};
		//Function to update Key35 with Agent GAO code 
		$scope.GAORequstSave = function(){
			PersistenceMapping.clearTransactionKeys();
			$scope.mapKeysforPersistence();
			var transactionObjDeleted = {};
			transactionObjDeleted.Id = $scope.selectedProposal[0].Id;
			transactionObjDeleted.Key35 = UserDetailsService.getAgentRetrieveData().AgentDetails.unit;
			transactionObjDeleted.Key38 = UserDetailsService.getAgentRetrieveData().AgentDetails.agentName;
			DataService.saveTransactions(transactionObjDeleted, $scope.onupdateGAOSuccessCallBack, $scope.onupdateGAOErrorCallBack);
		};
		//SuccessCallBack of Function to update Key35 with Agent GAO code 
		$scope.onupdateGAOSuccessCallBack = function(data){
			$scope.selectedProposal.splice(0, 1);
			if ($scope.selectedProposal.length > 0) {
				$scope.GAORequstSave();
			}else{
				PersistenceMapping.clearTransactionKeys();
				$scope.mapKeysforPersistence();
				var  searchCriteria = {
					 searchCriteriaRequest:{
						"command" : "ListingDashBoard",
						"modes" :['eApp'],
						"value" : ""
					}
				};
				PersistenceMapping.Type='eApp';
				var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);									
				transactionObj.Id = $routeParams.transactionId;
				transactionObj.Type = "eApp";
				transactionObj.filter = "Key15  <> 'Cancelled'";
				DataService.getFilteredListing(transactionObj, $scope.onGeteAppListingSuccess, $scope.onGetListingsError);
			}
		};
		//ErrorCallBack Function to update Key35 with Agent GAO code 
		$scope.onupdateGAOErrorCallBack = function(error){
			$rootScope.showHideLoadingImage(false);
			$scope.refresh();
		};
		//Function on clicking Mark as complete,unlock - first check if the agents are the same,then proceed
		$scope.markAsComplete_unlockEapp_newIllustrate = function(action){
			$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
			if(action=="illustration"){
				$rootScope.showHideLoadingImage(false);
				$scope.enterMandatory = translateMessages($translate, "eapp.GAOcreateNewIllustrationMessage");
				$scope.agentDetailsShowPopUpMsg=true;
				$scope.agentDetailsDescShowPopUpMsg=false;
				$scope.refresh();
			}else{
				$scope.agentDetailsShowPopUpMsg=false;
				var flagToCheck=false;
				for(var i=0;i<$scope.selectedProposal.length;i++){
					if($scope.selectedProposal[i].Key36!=UserDetailsService.getAgentRetrieveData().AgentDetails.agentCode && $scope.selectedProposal[i].Key36!=""){
						flagToCheck=true;
						break;
					}
				}
				if(flagToCheck){ // to check if the agent marking/unlocking is same as the agent whos tagged to
					if(action=="markAsComplete"){
						$rootScope.showHideLoadingImage(false);
						$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"), translateMessages($translate,"eapp.markAsCompletedDiffAgentError"), translateMessages($translate,"fna.ok"));
					}else if(action=="unlock"){
						$rootScope.showHideLoadingImage(false);
						$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"),translateMessages($translate,"eapp.unlockDiffAgentError"),translateMessages($translate,"fna.ok"));
					}
				}else{
					if(action=="markAsComplete"){
						 $rootScope.showHideLoadingImage(false);
						 $rootScope.lePopupCtrl.showQuestion(translateMessages($translate,"lifeEngage"),translateMessages($translate,"eapp.confirmMarkAsDeleteMessage"),translateMessages($translate,"Yes"),confirmMarkAsDelete,translateMessages($translate,
						"No"),cancel,'','',cancel);
					}else if(action=="unlock"){
						var transData=[];
						for(var i=0;i<$scope.selectedProposal.length;i++){
							var transactionObj = {
								"Id":"",
								"GAOOffice":"",
								"GAOCode":"",
								"Type":"",
								"Action":""
							};
							transactionObj.Id=$scope.selectedProposal[i].Key4;
							transactionObj.Type = "eApp";
							transactionObj.Action = "unlock";
							transData.push(transactionObj);
							if(i==$scope.selectedProposal.length-1)
								callGAOSpecificService(transData,"unlock");
						}
					}
				}
			}
		};
		//Function on close
		function cancel(){
			$rootScope.showHideLoadingImage(false);
		};
		//Function to create obj for mark as complete
		function confirmMarkAsDelete(){
			$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
			var transData=[];
			for(var i=0;i<$scope.selectedProposal.length;i++){
				var transactionObj = {
					"Id":"",
					"GAOOffice":"",
					"GAOCode":"",
					"Type":"",
					"Action":""
				};
				transactionObj.Id=$scope.selectedProposal[i].Key4;
				transactionObj.Type = "eApp";
				transactionObj.Action = "marked complete";
				transData.push(transactionObj);
				if(i==$scope.selectedProposal.length-1)
					callGAOSpecificService(transData,"markAsComplete");
			}
		};
		//Generaic function call for Mark as delete,unlock,lock
		function callGAOSpecificService(transData,action){
			PersistenceMapping.clearTransactionKeys();
			DataService.requestGAOSupport(transData, function () {
				if(action=="unlock"){
					$rootScope.showHideLoadingImage(false);
					$rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"),translateMessages($translate,"eapp.unlockSuccessMessage"),translateMessages($translate,"fna.ok"),reloadAfterAction);
				}else if(action=="markAsComplete"){
					reloadAfterAction();
				}else if(action=="lock"){
					$rootScope.showHideLoadingImage(false);
					$rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"),translateMessages($translate,"eapp.lockSuccessMessage"),translateMessages($translate,"fna.ok"),directToEapp);
				}
			}, function (error) {
				$rootScope.showHideLoadingImage(false);
				$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"), translateMessages($translate,"eapp.GAORequestFailed"), translateMessages($translate,"fna.ok"));
			});
		};
		//Function to reload the eapps after marking as complete or unlock
		function reloadAfterAction(){
			$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
			PersistenceMapping.clearTransactionKeys();
			$scope.mapKeysforPersistence();
			var  searchCriteria = {
				 searchCriteriaRequest:{
					"command" : "ListingDashBoard",
					"modes" :['eApp'],
					"value" : ""
				}
			};
			PersistenceMapping.Type='eApp';
			var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
			transactionObj.Type = "eApp";
			transactionObj.Key35 = UserDetailsService.getAgentRetrieveData().AgentDetails.unit;
			transactionObj.Key36 = transactionObj.Key11;
			transactionObj.Key37 = UserDetailsService.getAgentRetrieveData().AgentDetails.agentType;
			transactionObj.filter = "Key15  <> 'Cancelled'";
			DataService.getFilteredListing(transactionObj, $scope.onGeteAppListingSuccess, $scope.onGetListingsError);
		};
		//Function on edit button click - to assign an eapp to GAO
		$scope.lockEappToGAO = function(data){
			$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
			if(data.Key36!=UserDetailsService.getAgentRetrieveData().AgentDetails.agentCode && data.Key36!==""){
				$rootScope.showHideLoadingImage(false);
				$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"),translateMessages($translate,"eapp.unlockDiffAgentErrorSingle"),translateMessages($translate,"fna.ok"));
			}else if(data.Key36==UserDetailsService.getAgentRetrieveData().AgentDetails.agentCode){
				$scope.editData=data;
				directToEapp();
			}else{
				var transData=[];
				var transactionObj = {
					"Id":"",
					"GAOOffice":"",
					"GAOCode":"",
					"Type":"",
					"Action":""
				};
				transactionObj.Id=data.Key4;
				transactionObj.Type = "eApp";
				transactionObj.GAOCode = UserDetailsService.getAgentRetrieveData().AgentDetails.agentCode;
				transactionObj.Action = "lock";
				transData.push(transactionObj);
				$scope.editData=data;
				callGAOSpecificService(transData,"lock");
			}
		};
		//Function to redirect to eapp page
		function directToEapp(){
			$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
			proceedEappEditPage($scope.editData);
			delete $scope.editData;
		};
		//Function to view the docs
		$scope.viewDocs = function (proposal){
			/* sample docsArray
			 docsArray =[{base64:'',imgName:''},{base64:'',imgName:''}]
			 imgName may be file name or serial no
			 base64 is base64 string of the image
			  */
			/* if(data.Key36!=UserDetailsService.getAgentRetrieveData().AgentDetails.agentCode){
				$rootScope.showHideLoadingImage(false);
				$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"),translateMessages($translate,"eapp.unlockDiffAgentErrorSingle"),translateMessages($translate,"fna.ok"));
			}else{ */
			    $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
				var requirementObject = CreateRequirementFile();
				requirementObject.RequirementFile.requirementName = "GAO";
				requirementObject.RequirementFile.documentName = "GAO";
				PersistenceMapping.clearTransactionKeys();
				//EappService.mapKeysforPersistence();
				GLI_EappService.mapKeysforPersistenceGAO();
				var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
				transactionObj.Key4 = proposal.Key4;
				DataService.getDocumentsForRequirement(transactionObj, function(data){
					var docsArray =[];
					if(data && data.length>0){
						$scope.pdfImage = "css/generali-theme/img/dummy_img_pdf.png";
						for(var i=0;i<data.length;i++){
							var obj={base64:'',fileName:'',type:'',pdfBase64:''};
							var fileType = data[i].fileName.split(".")[1];
							if(fileType=="pdf"){
								obj.base64=$scope.pdfImage;
								obj.fileName=data[i].fileName;
								obj.type=fileType;
								var base64Data=data[i].base64string.split(',');
								var pdfBase64Data="data:application/pdf;base64," + base64Data[1];
								obj.pdfBase64=pdfBase64Data;
							}else if(fileType=="docx"){
								obj.base64=$scope.pdfImage;
								obj.fileName=data[i].fileName;
								obj.type=fileType;
								var base64Data=data[i].base64string.split(',');
								var pdfBase64Data="data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64," + base64Data[1];
								obj.pdfBase64=pdfBase64Data;
							}else if(fileType=="doc"){
								obj.base64=$scope.pdfImage;
								obj.fileName=data[i].fileName;
								obj.type=fileType;
								var base64Data=data[i].base64string.split(',');
								var pdfBase64Data="data:application/msword;base64," + base64Data[1];
								obj.pdfBase64=pdfBase64Data;
							}else{
								obj.base64=data[i].base64string;
								obj.fileName=data[i].fileName;
								obj.type=fileType;
							}
							docsArray.push(obj);
							if(i==data.length -1){
								$rootScope.showHideLoadingImage(false);
								navigateToViewDocs(docsArray);
							}
						}
					}else{
						$rootScope.showHideLoadingImage(false);
					}
				}, function(){
					$rootScope.showHideLoadingImage(false);
				});
			//}
		};
		function navigateToViewDocs(docsArray){
			var docUrl = $window.location.pathname+"#/gaoDocs";
			var windowObj = $window.open(docUrl,"popup","width=1300,height=800,left=10,top=150");
			windowObj.docsArray= docsArray;
		};
		//Function to fetch the agent details
		$scope.showAgentDetails = function(agentCode){
			$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
			$scope.agentDetailsDescShowPopUpMsg=false;
			PersistenceMapping.clearTransactionKeys();
			var transactionObj = PersistenceMapping.mapScopeToPersistence({});
			transactionObj.Key11 = agentCode;
			AgentService.retrieveAgentProfile(transactionObj,successRetrieveAgent,errorRetrieveAgent);
		};
		//success call back of retrieveAgentProfile
		function successRetrieveAgent(data){
			$scope.statusCheck=(data.AgentDetails.status!=rootConfig.AgentStatusForGAO)?true:false;
			$scope.dataCheck=(new Date()>new Date(data.AgentDetails.licExpDate))?true:false;
			if( $scope.statusCheck || $scope.dataCheck){
				$scope.disableConfirmButtonForAgent=true;
			}else{
				$scope.disableConfirmButtonForAgent=false;
			}
			
			if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO){
				if(data.AgentDetails.unit && data.AgentDetails.unit!=null) {
					var userCheck = $rootScope.username;
				if(userCheck.indexOf(data.AgentDetails.unit)==-1){
					$scope.disableConfirmButtonForAgent=true;
				}else{
					$scope.disableConfirmButtonForAgent=false;
				}
			}else {
				$scope.disableConfirmButtonForAgent=true;
			   }
			}
			
			UserDetailsService.setAgentForGAO(data);
			$scope.agentFullName=data.AgentDetails.agentName;
			$scope.agentManagerName=data.AgentDetails.managerName;
			$scope.GAOName=data.AgentDetails.gaoName;
			$scope.agentStatus=data.AgentDetails.status;
			var dateArray=data.AgentDetails.licExpDate.split('-');
			var dateThai = new Date(parseInt(dateArray[0]) + rootConfig.thaiYearDifference, dateArray[1] - 1, dateArray[2]);
			$scope.agentLiscenseExpiryDate= $filter('date')(new Date(dateThai),$scope.appDateFormat); 
			$scope.agentDetailsDescShowPopUpMsg=true;
			$rootScope.showHideLoadingImage(false);
		};
		//error call back of retrieveAgentProfile
		function errorRetrieveAgent(data){
			$rootScope.showHideLoadingImage(false);
		};
		//Function to close the pop up
		$scope.closePopUpGAO = function(){
			$scope.agentDetailsShowPopUpMsg=false;
			$scope.agentCodeForGAO="";
		};
		//Function on clicking the confirm button in agent pop up
		$scope.confirmNewIllustration = function(){
			IllustratorVariables.agentForGAO = UserDetailsService.getAgentForGAO().AgentDetails.agentCode;
			IllustratorVariables.agentNameForGAO = UserDetailsService.getAgentForGAO().AgentDetails.agentName;
			createNewIllustrationContinue();
		};
							
		if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
			$rootScope.disableRefreshForAdmin=true;
		}else{
			$rootScope.disableRefreshForAdmin=false;
		}
}]);
						
			
		
