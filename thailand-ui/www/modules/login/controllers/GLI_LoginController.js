/*
 * Copyright 2015, LifeEngage 
 */
/* 
 * Name:GLI_LoginController
 * For implementing Login specific changes
 */

storeApp
    .controller(
        'GLI_LoginController', GLI_LoginController);
	GLI_LoginController.$inject = ['$rootScope',
			                       '$scope',
			                       '$http',
			                       'AuthService',
			                       '$location',
								   '$compile',
								   '$timeout',
			                       '$translate',
			                       'UserDetailsService',
			                       'DataService',
			                       'GLI_LoginService',
			                       '$controller',
			                       'LoginService',
			                       'DataWipeService',
			                       'commonConfig',
					       'GLI_DataLookupService'];

function GLI_LoginController($rootScope, $scope, $http, AuthService, $location, $compile, $timeout,$translate,
							 UserDetailsService, DataService, GLI_LoginService, $controller, LoginService, 
							 DataWipeService, commonConfig, GLI_DataLookupService) {
							    $controller('LoginController', {
							        $rootScope: $rootScope,
							        $scope: $scope,
							        $http: $http,
							        AuthService: AuthService,
							        $location: $location,
							        $translate: $translate,
							        UserDetailsService: UserDetailsService,
							        DataService: DataService,
							        LoginService: GLI_LoginService,
							        LoginService: LoginService,
							        DataWipeService: DataWipeService,
									DataLookupService:GLI_DataLookupService
							    });
	$scope.rememberme = false;
    $scope.isLoggedIn = true;
    $rootScope.isFirstTime = true;
    $rootScope.isWeb = !rootConfig.isDeviceMobile;
    $scope.logoutpopup = false;
	GLI_DataLookupService.clearDataLookupCache();
    $rootScope.isAuthenticated = false;

    if (JSON.parse(localStorage.getItem('loginDetails')) !== null) {
        $scope.loginObject = JSON.parse(localStorage.getItem('loginDetails'));
        if ($scope.loginObject !== null &&
            $scope.loginObject !== undefined) {
            $scope.rememberme = true;
            if(rootConfig.isDeviceMobile) {
            	$scope.username = Decrypt_text($scope.loginObject.leuname);
            	$scope.password = Decrypt_text($scope.loginObject.lepwd);
            } else {
            	$scope.username = $scope.loginObject.leuname;
            	$scope.password = $scope.loginObject.lepwd;
            }
        }
    }
    $scope.rootedOK = function () {
        if ($scope.exitApp) {
            navigator.app.exitApp();
        }
    };
    
    if (localStorage["locale"] == undefined) {      
        localStorage["locale"] = rootConfig.locale;        
    }
    
    $scope.checkValidConnection = function(value){
    	if((rootConfig.isDeviceMobile) || (rootConfig.isOfflineDesktop)){
    		if(!($scope.isUserOnline())) {
				$scope.showPopUpMsg = true;
                $scope.loginmessage = translateMessages($translate, "firsttimeloginErrorMsg");
			} else {
				$scope.linkTo(value);
			}
    	} else {
    		$scope.linkTo(value);
    	}
    }
    
    $scope.checkifEligibleToLogin = function () {
		
    	angular.element(document.querySelector(".site-overlay")).addClass("hideSplitOne"); 
		var divElement = angular.element(document.querySelector('.hideSplitOne')); 
		var htmlElement = angular.element('<span class="loadmsg">Loading..<span>');
		divElement.append(htmlElement);
		$compile(divElement)($scope); 
		$timeout(function () {
			angular.element(document.querySelector(".site-overlay")).removeClass("hideSplitOne");
		},1000)
		
        if ($scope.username != "" && $scope.password != "" && $scope.username != undefined && $scope.password != undefined) {
            if (rootConfig.isDeviceMobile && !rootConfig.isOfflineDesktop) {
                createKeyIfNotExists(function () {
                    var filesToBeEncrypted = getFilesToBeEncrypted();
                    window.plugins.LEEncryption.encryptDB(filesToBeEncrypted, function (data) {
						DataService.dbInit(function(){
								$scope.CheckforRootedDevice(function () {
                                $scope.CheckForAppExpiry(function () {
                                    $scope.login();
                                })
                            });
						});
                        
                    });
                });
            } else if ((rootConfig.isDeviceMobile) &&
                (rootConfig.isOfflineDesktop)) {
					DataService.dbInit(function(){
						$scope.CheckForAppExpiry(function () {
						$scope.login();
                });
					});
                
            } else {
                $scope.login();
            }
        } else {
                // paste  your existing  logic
                /*$rootScope.showHideLoadingImage(false, "");
                $rootScope.isAuthenticated = false;
                $scope.loginError = translateMessages(
                $translate,
                "loginCredentialEmptyErrorMsg");*/
                $scope.showPopUpMsg = true;
                $scope.loginmessage = translateMessages($translate, "loginmessage.loginCredentialEmptyErrorMsg");
        }
    };

    $scope.closePopup = function () {
        $scope.showPopUpMsg = false;
        $scope.showPopUpServiceMsg=false;
        $scope.overlayTC = false;
    };

    $scope.linkTo = function(link) {
        $rootScope.isAuthenticated = true;
        $rootScope.isLoggedOut = true;
        $rootScope.isInLoginPage = false;
        $location.path(link);
        $scope.$apply();
    };  

    
    $scope.login = function () {
        $rootScope.showHideLoadingImage(true, 'loadingMsg', $translate);
        $scope.UpdateFirstLoginDateOnUpgrade();
        if ((rootConfig.isDeviceMobile) &&
            !(rootConfig.isOfflineDesktop)) {
            if (!$scope
                .isUserOnline()) {
                LoginService
                    .checkAgentIdExistingInDb({
                            userId: $scope.username,
                            password: $scope.password
                        },
                        function (
                            data) {
                            if (data.length > 0) {
                                $rootScope.showHideLoadingImage(true, 'loadingMsg', $translate);
                                var localPassword = Decrypt_text(data[0].password);
                                if ($scope.password == localPassword) {
                                    $rootScope.username = $scope.username;
                                    LoginService
                                        .fetchToken(
                                            $scope.username,
                                            function (
                                                res) {
                                                var currentDate = new Date();
                                                var offlineDueDate = new Date(
                                                    currentDate
                                                    .setDate(currentDate
                                                        .getDate() -
                                                        rootConfig.maxPermittedOfflineDuration));
                                                var lastLoggedInDate = res[0].lastLoggedInDate;
                                                if (new Date(
                                                        lastLoggedInDate) > offlineDueDate) {
                                                    $rootScope.isAuthenticated = true;
                                                    $scope.userDetails.options.headers.Token = res[0].token;
                                                    UserDetailsService
                                                        .setUserDetailsModel($scope.userDetails);
                                                    if ($scope.rememberme == true) {
                                                        var loginDetails = {
                                                            'leuname': Encrypt_text($scope.username),
                                                            'lepwd': Encrypt_text($scope.password)
                                                        };
                                                        localStorage
                                                            .setItem(
                                                                'loginDetails',
                                                                JSON
                                                                .stringify(loginDetails));
                                                    } else {
                                                        localStorage
                                                            .removeItem('loginDetails');
                                                    }
                                                    $location
                                                        .path('/landingPage');
                                                     $scope.logoutpopup = false;    
                                                    $scope
                                                        .refresh();
                                                } else {
                                                    $rootScope.isAuthenticated = false;
                                                    $rootScope.lePopupCtrl
                                                        .showSuccess(translateMessages(
                                                                $translate, "lifeEngage"),
                                                            translateMessages($translate,
                                                                "tokenExpiryMessage"),
                                                            translateMessages($translate,
                                                                "ok"), terminateLogin);
                                                    $rootScope.showHideLoadingImage(false, "");
                                                    $scope.refresh();
                                                }
                                            },
                                            function (err) {
                                            	$rootScope.isAuthenticated = false;
                                            	$scope.showPopUpMsg = true;
                                                $scope.loginmessage = translateMessages($translate, "loginmessage.loginCredentialErrorMsg");
                                                $rootScope.showHideLoadingImage(false, "");
                                                $scope.refresh();
                                            });
                                } else {
                                    $rootScope.isAuthenticated = false;
                                    $scope.showPopUpMsg = true;
                                    /*$scope.loginmessage = translateMessages($translate, "loginmessage.loginCredentialErrorMsg");*/
                                    $scope.loginmessage = translateMessages($translate, "loginmessage.loginCredentialErrorMsg");
                                    $rootScope.showHideLoadingImage(false, "");
                                    $scope.refresh();
                                }
                            } else {
                                if ($scope.isUserOnline()) {
                                    $scope
                                        .authenticateUserOnline();
                                } else {
                                    $rootScope.showHideLoadingImage(false, "");
                                    $rootScope.isAuthenticated = false;
                                    $scope.showPopUpMsg = true;
                                    $scope.loginmessage = translateMessages($translate, "firsttimeloginErrorMsg");
                                    $scope.refresh();
                                }
                            }
                        },
                        function (err) {
                            $rootScope.showHideLoadingImage(false, "");
                            $rootScope.isAuthenticated = false;
                            $scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "firsttimeloginErrorMsg");
                        });
            } else {
                $rootScope.showHideLoadingImage(true, 'loadingMsg', $translate);
                $scope.authenticateUserOnline();
            }
        } else {
            $scope.authenticateUserOnline();
        }
    };

    $scope.showforgotPasswordPopup = function () {
        $rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "forgotPasswordAlert"),
            translateMessages($translate,
                "lms.ok"));
    }

    $scope.authenticateUserOnline = function () {
        AuthService
            .login({
                    username: $scope.username,
                    password: $scope.password
                },
                function (res, status) {
                    $rootScope.username = $scope.username;
                    $rootScope.isAuthenticated = true;
                    $scope.userDetails.user.userId = $scope.username;
                    $scope.userDetails.user.password = Encrypt_text($scope.password);
                    $scope.userDetails.user.token = res;
                    $scope.userDetails.user.lastLoggedInDate = new Date();
                    $scope.userDetails.options.headers.Token = res;
                    UserDetailsService.setUserDetailsModel($scope.userDetails);
                    LoginService
                        .saveToken(
                            $scope.userDetails.user,
                            function (
                                res) {

                                if ($scope.rememberme == true) {
                                    if (rootConfig.isDeviceMobile) {
                                    var loginDetails = {
                                        'leuname': Encrypt_text($scope.username),
                                        'lepwd': Encrypt_text($scope.password)
                                    };} else {
                                          var loginDetails = {
                                        'leuname': $scope.username,
                                        'lepwd': $scope.password
                                    };
                                    }
                                    localStorage
                                        .setItem(
                                            'loginDetails',
                                            JSON
                                            .stringify(loginDetails));
                                } else {
                                    localStorage
                                        .removeItem('loginDetails');
                                }
                                if (status == 220) {
                                    //$rootScope.suspendedmsg = translateMessages($translate,"suspendedFooterMsg");
                                    $rootScope.showHideLoadingImage(false, "");
                                    $rootScope.lePopupCtrl
                                        .showSuccess(translateMessages(
                                                $translate, "lifeEngage"),
                                            translateMessages($translate,
                                                "suspendedMsg"),
                                            translateMessages($translate,
                                                "ok"), suspendedLogin);
                                } else {
                                    $location
                                        .path('/landingPage');
                                        // .path('/consentPage');
                                         $scope.logoutpopup = false;
                                }

                                $scope
                                    .refresh();

                            },
                            function (
                                err) {

                            });
                },
                function (data, status) {
                    $rootScope.isAuthenticated = false;
                    $rootScope.showHideLoadingImage(false, "");
                    $scope.refresh();
                    if (status) {
                        if (status == 401) {
							$scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "loginmessage.loginCredentialErrorMsg");
                        } else if (status == 400) {
							$scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "resourceNotFoundErrMsg");
                        } else if (status == 500) {
							$scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "resourceUnAvailableErrMsg");
                        } else if (status == 480) {
							$scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "loginmessage.unauthorizedaccess");
                        } else if (status == 481) {
							$scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "loginmessage.agentLicenseExpired");
                        } else if (status == 482) {
							$scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "loginmessage.agentLicenseExpired");
                        }
                        else if (status == 502) {
							$scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "loginmessage.accountlocked");
                        }
                        else if (status == 471) {
							$scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "loginmessage.usernotfound");
                        }
                        else if (status == 475) {
                            $scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "loginmessage.passwordExpired");
                        } 
                         else {
                            if ($scope.username != "" && $scope.password != "" && $scope.username != undefined && $scope.password != undefined) {
                                $rootScope.showHideLoadingImage(false, "");
                                $scope.loginError = translateMessages($translate,
                                    "loginCredentialErrorMsg");
                            } else {
                                $rootScope.showHideLoadingImage(false, "");
                                $scope.loginError = translateMessages($translate,
                                    "loginCredentialEmptyErrorMsg");
                            }
                        }
                    } else {
                        $scope.loginError = translateMessages($translate,
                            "LE_SYNC_ERR_104");
                    }
                })

        $scope.refresh();
    };

    $scope.ManageBruteForceLoginAttempt = function () {
        $scope.wrongPasswordAttemptCount = $scope.wrongPasswordAttemptCount + 1;
        if (rootConfig.isDeviceMobile && rootConfig.allowDataWipeOnBruteForceLogin) {
            if ($scope.wrongPasswordAttemptCount <= rootConfig.wrongPasswordMessageAlert) {
                $scope.loginError = "";

            } else if ($scope.wrongPasswordAttemptCount < rootConfig.wrongPasswordAttemptLimit && $scope.wrongPasswordAttemptCount > rootConfig.wrongPasswordMessageAlert) {
                $scope.errorMessageRemainingForWrongPasswordAttempt($scope.wrongPasswordAttemptCount);
            }
            if ($scope.wrongPasswordAttemptCount == rootConfig.wrongPasswordAttemptLimit) {
                $rootScope.showHideLoadingImage(true, 'loadingMsg', $translate);
                DataWipeService.deleteAgentSpecificConfigFiles($scope.username, commonConfig().DATA_WIPE_REASON.BRUTE_FORCE_LOGIN, function () {
                    $rootScope.showHideLoadingImage(true, 'loadingMsg', $translate);
                    $scope.errorMessageForWrongPasswordAttemptReached();
                }, function (error) {
                    $scope.errorMessageForWrongPasswordAttemptReached();
                });
            } else {
                $scope.loginError = translateMessages($translate, "loginCredentialErrorMsg");
                $rootScope.showHideLoadingImage(false, "");
            }
        } else {
            $scope.loginError = translateMessages($translate, "loginCredentialErrorMsg");
            $rootScope.showHideLoadingImage(false, "");
        }
    };

    $scope.errorMessageForWrongPasswordAttemptReached = function () {
        $scope.loginError = "";
        $rootScope.showHideLoadingImage(false, "");
        $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
            translateMessagesWithParams($translate, "dataWipe.wrongPasswordAttemptLimitReached", {
                "limit": rootConfig.wrongPasswordAttemptLimit
            }),
            translateMessages($translate, "fna.ok"),
            $scope.okClick);
        $scope.wrongPasswordAttemptCount = 0;
    };

    $scope.errorMessageRemainingForWrongPasswordAttempt = function (wrongPasswordAttempt) {
        $scope.remainingWrongPasswordLeft = rootConfig.wrongPasswordAttemptLimit - wrongPasswordAttempt;
        $scope.loginError = "";
        $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
            translateMessagesWithParams($translate, "dataWipe.wrongPasswordRemainingAttempts", {
                "remainingAttempts": $scope.remainingWrongPasswordLeft
            }),
            translateMessages($translate, "fna.ok"),
            $scope.okClick);
    };

    //Logout Controller extended
    $scope.logout = function () {
        $rootScope.isAuthenticated = false;
        $location.path('/login');
    };
    $scope.$on("$destroy", function () {
        if (this != null && typeof this !== 'undefined') {
            if (this.$$destroyed) return;
            while (this.$$childHead) {
                this.$$childHead.$destroy();
            }
            if (this.$broadcast) {
                this.$broadcast('$destroy');
            }
            this.$$destroyed = true;
        }
    });

    function suspendedLogin() {
        $location.path('/landingPage');
         $scope.logoutpopup = false;
        $scope.refresh();
    }

    function terminateLogin() {
        $location.path('/login');
        $scope.refresh();
    }

    function createKeyIfNotExists(success) {
        var options = [];
        var obj = {};
        obj.appName = rootConfig.appName;
        options.push(obj);
        window.plugins.LEEncryption.checkKeyAvailability(options,
            function (data) {
                if (data == "key exists") {
                    success();
                } else {
                    AuthService.fetchKey({
                            username: $scope.username,
                            password: $scope.password
                        }, function (data) {

                            if (data.StatusData.Status == "SUCCESS") {
                                var KeyArray = [];
                                var keyObj = {};
                                keyObj.key = data.encryptionKey;
                                keyObj.appName = rootConfig.appName;
                                KeyArray.push(keyObj);
                                window.plugins.LEEncryption.insertKey(KeyArray,
                                    function (data) {
                                        if (data == "key entered successfully") {}
                                        success();
                                    },
                                    function (data) {
                                        console.log("failure to insert key. " + data);
                                    }
                                );
                            } else {
                                //else condition
                            }
                        },
                        function (data, status) {
                            $rootScope.isAuthenticated = false;
                            
                            if (status == 401) {
                                $scope.loginError = translateMessages(
                                    $translate,
                                    "loginCredentialErrorMsg");
                            } else if (status == 404) {
                                $scope.loginError = translateMessages(
                                    $translate,
                                    "resourceNotFoundErrMsg");
                            } else if (status == 500) {
                                $scope.loginError = translateMessages(
                                    $translate,
                                    "resourceUnAvailableErrMsg");
                            } else if (status == 0) {
                                $scope.loginError = translateMessages(
                                    $translate,
                                    "firsttimeloginErrorMsg");
                            } else {
                                $scope.loginError = translateMessages(
                                    $translate,
                                    "loginCredentialErrorMsg");
                            }
                            $rootScope.showHideLoadingImage(false, "");
                        });
                }
            },
            function (error) {
                console.log("Failed to check key availability. " + error);
            }
        );
    };
    $scope.pushEncryptionFilesToArray = function (fileArray, files) {
        for (var i = 0; i < fileArray.length; i++) {
            var fileDesc = {};
            fileDesc.fileName = fileArray[i];
            files.push(fileDesc);
        }
        return files;
    };

    function getFilesToBeEncrypted() {
        var filesToBeEncrypted = [];
        var filesArray = ["core-products.db", 
                          "LifeEngageData.db", 
                          "code-lookup.db", 
                          "RequirementDB.db", 
                          "GB8Products.db", 
                          "CHS8080Products.db", 
                          "GP2025Products.db", 
                          "GS20PProducts.db", 
                          "4N85Products.db", 
                          "1P05Products.db",
                          "4P10Products.db",
                          "4P04Products.db"];
        filesToBeEncrypted = $scope.pushEncryptionFilesToArray(filesArray, filesToBeEncrypted);
        var options = [];
        var obj = {};
        obj.appName = rootConfig.appName;
        obj.fileArray = filesToBeEncrypted;
        options.push(obj);
        return options;
    };
};