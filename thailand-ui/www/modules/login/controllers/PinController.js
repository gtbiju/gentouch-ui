/*
 * Copyright 2015, LifeEngage 
 */
/*storeApp.controller('PinController', ['$rootScope', '$scope',  '$window',  '$translate',
function($rootScope, $scope, $window,  translate) {*/
storeApp.controller('PinController', ['$rootScope', '$scope','AuthService', '$location','$translate','DataService',
function($rootScope, $scope,AuthService, $location,  translate,DataService) {
	if ((rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop) && (rootConfig.hasPinSecurity)
			&& (/Android/i.test(navigator.userAgent))) {
		window
				.addEventListener(
						'load',
						function() {

							document.addEventListener('backbutton',
									function(e) {
										e.preventDefault();/*
															 * Preventing Back
															 * button
															 */
									});
                            
							$scope.pinValue = [];
							$scope.hidePinBox = false;
							$scope.pinMessage = "";
							$scope.firstTime = true;
							$scope.tempPin = "";
							CDVPLUGIN.SystemCheck
									.PinCheck(
											{
												firstTime : true
											},
											function(res) {
												$scope.pinMessage = "This is your first login. Please set your PIN.";
												$scope.firstTime = true;
											},
											function(err) {
												$scope.pinMessage = "Please enter your PIN.";
												$scope.firstTime = false;
											});

						}, false);

		$scope.pinValuePush = function(val) {
			$scope.pinMessage = "";
			if ($scope.pinValue.length <= 3)
				$scope.pinValue.push(val);
			if ($scope.pinValue.length == 4) {
				if (!$scope.firstTime) {

					var pin = $scope.pinValue.join('');
					CDVPLUGIN.SystemCheck.PinCheck({
						pinNumber : pin
					}, function(res) {
						$scope.hidePinBox = true;
						$scope.refresh();
					}, function(err) {
						$scope.pinMessage = "Wrong PIN. Please Try Again.";
						$scope.pinValue = [];
						$scope.refresh();

					});
				} else {

					if ($scope.tempPin == "") {

						$scope.tempPin = $scope.pinValue.join('');
						$scope.pinMessage = "Please re-enter PIN";
						$scope.pinValue = [];
						$scope.refresh();

					} else {

						if ($scope.tempPin == $scope.pinValue.join('')) {

							var pin = $scope.pinValue.join('');
							CDVPLUGIN.SystemCheck.PinCheck({
								pinNumber : pin
							}, function(res) {
								$scope.firstTime = false;
								$scope.hidePinBox = true;
								$scope.refresh();
							}, function(err) {

							});

						} else {

							$scope.tempPin = $scope.pinValue.join('');
							$scope.pinMessage = "The entered PINs doesnt match. Pls re-enter.";
							$scope.tempPin = "";
							$scope.pinValue = [];
							$scope.refresh();

						}
					}

				}

			}

		};
		$scope.pinValuePop = function() {
			$scope.pinValue.pop();
			if ($scope.pinValue.length == 0)
				$scope.pinMessage = "Please enter your PIN.";
			$scope.refresh();

		};
		document
				.addEventListener(
						"resume",
						function() {
							if (!$scope.firstTime)
								$scope.pinMessage = "Please enter your PIN.";
							else
								$scope.pinMessage = "This is your first login. Please set your PIN.";

							$scope.pinValue = [];
							$scope.tempPin = "";
							$scope.hidePinBox = false;
							$scope.refresh();
						});
	} else {
		$scope.hidePinBox = true;
	}

}]);