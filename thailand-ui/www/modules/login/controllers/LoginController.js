storeApp
		.controller(
				'LoginController',
				[
						'$rootScope',
						'$scope',
						'$http',
						'AuthService',
						'$location',
						'$translate',
						'UserDetailsService',
						'LoginService',
						'DataService',                        
						'DataWipeService',
						'commonConfig',

						function LoginController($rootScope, $scope, $http,
								AuthService, $location, $translate,
								UserDetailsService, LoginService, DataService, DataWipeService, commonConfig) {
							$scope.majorVersion = rootConfig.majorVersion;
							$scope.minorVersion = rootConfig.minorVersion;
							//$scope.isLoginAllowed = true;
							// OK button functionality of rooted device display
							// popup
							$scope.rootedOK = function() {
								$rootScope.showHideLoadingImage(false, "");
								if ($scope.exitApp) {
									navigator.app.exitApp();
								}
							};
							$rootScope.lePopupCtrl = {

							};
							$scope.userDetails = UserDetailsService
									.getUserDetailsModel();

							$rootScope.isAuthenticated = false;
							$scope.username = '';
							$scope.password = '';

							if (JSON
									.parse(localStorage.getItem('loginDetails')) !== null) {
								$scope.loginObject = JSON.parse(localStorage
										.getItem('loginDetails'));
								if ($scope.loginObject !== null
										&& $scope.loginObject !== undefined){
									$scope.username = $scope.loginObject.leuname;
								}
							}

							$scope.handleKeypress = function(key) {
								if (key == "13") {
									$scope.login();
								}
							};
							
							//Added for ensuring login call will be made after all eligibility checks
							$scope.checkifEligibleToLogin = function() { 
								$rootScope.showHideLoadingImage(true,
										"Loading..");
								if((rootConfig.isDeviceMobile)&& !(rootConfig.isOfflineDesktop)) {
									//$scope.login();
									$scope.CheckforRootedDevice(function() {
										$scope.CheckForAppExpiry(function() {
											$scope.login();
										})
									});
								} else if ((rootConfig.isDeviceMobile)&& (rootConfig.isOfflineDesktop)){
									$scope.CheckForAppExpiry(function() {$scope.login()});
								} else
									$scope.login();
							};
								
							//Checks for user is online 
							$scope.isUserOnline = function(){
								var userOnline = true;
								if ((rootConfig.isDeviceMobile)&& !(rootConfig.isOfflineDesktop)){
									if (navigator.network.connection.type == Connection.NONE) {
										userOnline = false;
									}
								}
								if ((rootConfig.isOfflineDesktop)) {
									if (!navigator.onLine) {
										userOnline = false;
									}
								}
								return userOnline;
							}
							
							$scope.login = function() {
																
								$scope.UpdateFirstLoginDateOnUpgrade();

								if (!$scope.isUserOnline()) {
									LoginService
											.checkAgentIdExistingInDb(
													{
														userId : $scope.username,
														password : $scope.password
													},
													function(data) {
														if (data.length > 0) {
															var localPassword = Decrypt_text(data[0].password);
															if ($scope.password == localPassword) {
																$rootScope.username = $scope.username;
																LoginService
																		.fetchToken(
																				$scope.username,
																				function(
																						res) {
																					var currentDate = new Date();
																					var offlineDueDate = new Date(
																							currentDate
																									.setDate(currentDate
																											.getDate()
																											- rootConfig.maxPermittedOfflineDuration));
																					var lastLoggedInDate = res[0].lastLoggedInDate;
																					if (new Date(
																							lastLoggedInDate) > offlineDueDate) {
																						$rootScope.isAuthenticated = true;
																						$scope.userDetails.options.headers.Token = res[0].token;
																						UserDetailsService
																								.setUserDetailsModel($scope.userDetails);
																						if ($scope.rememberme == true) {
																							var loginDetails = {
																								'leuname' : $scope.username,
																								'lepwd' : $scope.password
																							};
																							localStorage
																									.setItem(
																											'loginDetails',
																											JSON
																													.stringify(loginDetails));
																						} else {
																							localStorage
																									.removeItem('loginDetails');
																						}
																						$location
																								.path('/landingPage');
																								$scope.refresh();
																					} else {
																						$rootScope.isAuthenticated = false;
																						$scope.loginError =translateMessages($translate, 
																								"tokenExpiryMessage");
																						$rootScope
																								.showHideLoadingImage(
																										false,
																										"");
																										$scope.refresh();
																					}
																								},
																				function(
																						err) {

																				});
															} else {
																$rootScope.isAuthenticated = false;
																$scope.loginError = translateMessages($translate, 
																		"loginCredentialErrorMsg");
																$rootScope
																		.showHideLoadingImage(
																				false,
																				"");
																$scope.refresh();
															}
														} else {
															$rootScope.isAuthenticated = false;
															$scope.loginError = translateMessages($translate, 
																	"firsttimeloginErrorMsg");
															$rootScope
																	.showHideLoadingImage(
																			false,
																			"");
															$scope.refresh();
														}
													}, function(err) {
														$rootScope.isAuthenticated = false;
														$scope.loginError = translateMessages($translate, 
																"firsttimeloginErrorMsg");
														$rootScope
																.showHideLoadingImage(
																		false,
																		"");
																		$scope.refresh();
													});
										} else {
											$scope.authenticateUserOnline();
										}
								};

							$scope.authenticateUserOnline = function() {
								AuthService
										.login(
												{
													username : $scope.username,
													password : $scope.password
												},
												function(res) {
													$rootScope.username = $scope.username;
													$rootScope.isAuthenticated = true;
													$scope.userDetails.user.userId = $scope.username;
													$scope.userDetails.user.password = Encrypt_text($scope.password);
													$scope.userDetails.user.token = res;
													$scope.userDetails.user.lastLoggedInDate = new Date();
													$scope.userDetails.options.headers.Token = res;
													UserDetailsService
															.setUserDetailsModel($scope.userDetails);
													LoginService
															.saveToken(
																	$scope.userDetails.user,
																	function(
																			res) {

																		if ($scope.rememberme == true) {
																			var loginDetails = {
																				'leuname' : $scope.username
																				//'lepwd' : $scope.password
																			};
																			localStorage
																					.setItem(
																							'loginDetails',
																							JSON
																									.stringify(loginDetails));
																		} else {
																			localStorage
																					.removeItem('loginDetails');
																		}
																		$location
																				.path('/landingPage');
																		$scope
																				.refresh();

																	},
																	function(
																			err) {

																	});
												},
												function(data, status) {
													$rootScope.isAuthenticated = false;
													$rootScope.showHideLoadingImage(false, "");
													$scope.refresh();
												
													if(status){
														if(status == 401) {
															$scope.loginError = translateMessages($translate,"loginCredentialErrorMsg");
														} else if (status == 400) {
															$scope.loginError = translateMessages($translate,"resourceNotFoundErrMsg");
														} else if (status == 500) {
															$scope.loginError = translateMessages($translate,"resourceUnAvailableErrMsg");
														} else {
															$scope.loginError = translateMessages($translate, 
																	"loginCredentialErrorMsg");
														}
													}
											})
												
												$rootScope.showHideLoadingImage(false, "");
										$scope.refresh();
							};

							$scope.logout = function() {
								$rootScope.isAuthenticated = false;
								$location.path('/login');
							};
							$rootScope.$on("$locationChangeStart", function(e,
									currentLocation, previousLocation) {
								localStorage["prevPath"] = previousLocation;

							});
							
							$scope.CheckforRootedDevice = function(successcallback) {
								// Checking is the device rooted or
								// jailbroken through Cordove
								// plugin
								if (rootConfig.rootCheckEnabled) {
									CDVPLUGIN.SystemCheck
											.RootCheck(
													{
														action : 'RootCheck'
													},
													function(res) {
														if (res.rootedStatus) {
															$scope.alertMessage = translateMessages(
																	$translate,
																	res.message);
															if (res.message == "rootediOS") {
															    $scope.isRootediOS = true;
															    if  (rootConfig.allowDataWipeInRootedDevice){
															        DataWipeService.deleteAllConfigFiles(commonConfig().DATA_WIPE_REASON.ROOTED_DEVICE, function(){
                                                                        $scope.rootedCheckFailureMessageForCustomerIos();
                                                                    }, function(error){
                                                                       
                                                                        $scope.rootedCheckFailureMessageForCustomerAndroid();
                                                                    });
															    } else {
															        $scope.rootedCheckFailureMessageForCustomerIos();
															    }
															} else {
															    $scope.exitApp = true;
															    if  (rootConfig.allowDataWipeInRootedDevice){
															        DataWipeService.deleteAllConfigFiles(commonConfig().DATA_WIPE_REASON.ROOTED_DEVICE,function(){
                                                                        $scope.rootedCheckFailureMessageForCustomerAndroid();
                                                                    }, function(error){
                                                                       
                                                                        $scope.rootedCheckFailureMessageForCustomerAndroid();
                                                                    });
															    } else {
															       $scope.rootedCheckFailureMessageForCustomerAndroid();
															    }
															}
														} else {
															successcallback();
														}
													}, function(err) {
														errorcallback();
													});
								} else {
									successcallback();
								}
								CDVPLUGIN.SystemCheck.DeleteCheck(
										{
											action : 'DeleteCheck'
										},
										function(res) {
											if(rootConfig.isDebug){
												console.log("Success");
											}
										},
										function(err) {
											if(rootConfig.isDebug){
												console.log("Error in deleting pdf files");
											}
										});
							};
							
							$scope.rootedCheckFailureMessageForCustomerIos = function () {
								$rootScope.showHideLoadingImage(false, "");
							    $rootScope.lePopupCtrl.showBlockingPopUp(
                                    translateMessages($translate,
                                            "lifeEngage"),
                                    translateMessagesWithParams($translate,
                                            "dataWipe.rootedDeviceErrorMessage", {"appName" : rootConfig.appName})
                                );
							};
							$scope.rootedCheckFailureMessageForCustomerAndroid = function () {
								$rootScope.showHideLoadingImage(false, "");
                            	$rootScope.lePopupCtrl.showError(
                                    translateMessages($translate,
                                            "lifeEngage"),
                                    translateMessagesWithParams($translate,
                                            "dataWipe.rootedDeviceErrorMessage", {"appName" : rootConfig.appName}),
                                    translateMessages(
                                            $translate,
                                            "fna.ok"),
                                    $scope.rootedOK
                                );
                            };
							$scope.rootedCheckErrorHandler = function() {
								$rootScope.showHideLoadingImage(false, "");
								$scope.loginError = translateMessages(
										$translate, "rootedCheckError");
							};
							
							$scope.UpdateFirstLoginDateOnUpgrade = function(){
								if ((rootConfig.isDeviceMobile)) {

									// Clearing PrecompiledHandlebarTemplate
									// from local storage and
									// updating first login date on app upgrade
									// Begin
									var configuration = JSON.parse(localStorage
											.getItem('configuration'));
									if (!configuration) {

										configuration = JSON
												.parse('{"appMajorVersion": "'
														+ rootConfig.majorVersion
														+ '", "appMinorVersion": "'
														+ rootConfig.minorVersion
														+ '"}');
										localStorage.setItem('configuration',
												JSON.stringify(configuration));
									} else {
										var appUpgrade = false;
										if (parseInt(configuration.appMajorVersion) < parseInt(rootConfig.majorVersion)) {
											appUpgrade = true;
										} else if (parseInt(configuration.appMajorVersion) == parseInt(rootConfig.majorVersion)
												&& parseInt(configuration.appMinorVersion) < parseInt(rootConfig.minorVersion)) {
											appUpgrade = true;
										}
									}
									if (appUpgrade) {
										configuration = JSON
												.parse('{"appMajorVersion": "'
														+ rootConfig.majorVersion
														+ '", "appMinorVersion": "'
														+ rootConfig.minorVersion
														+ '"}');
										localStorage.setItem('configuration',
												JSON.stringify(configuration));
										var firstLoginDate = new Date();
										localStorage.setItem('firstLoginDate',
												firstLoginDate);
										localStorage
												.removeItem("lePrecompiledHandlebarTemplate");
										for (var i = 0; i < localStorage.length; i++) {
											var propertyName = localStorage
													.key(i);
											var pattern = 'lePage_';
											if (propertyName.indexOf(pattern) >= 0) {
												localStorage
														.removeItem(propertyName);
												i--;
											}
										}
									}
								}
							};
							
							$scope.CheckForAppExpiry = function(successCallback){
								
								if (!localStorage.firstLoginDate) {

									var firstLoginDate = new Date();
									localStorage.setItem('firstLoginDate',
											firstLoginDate);
									successCallback();
								} else {

									var firstLoginDate = new Date(
											localStorage
													.getItem('firstLoginDate'));
									var currentDate = new Date();

									// Copy date parts of the timestamps,
									// discarding the time parts.
									var firstLogin = new Date(
											firstLoginDate.getFullYear(),
											firstLoginDate.getMonth(),
											firstLoginDate.getDate());
									var currentLogin = new Date(currentDate
											.getFullYear(), currentDate
											.getMonth(), currentDate
											.getDate());

									// Do the math.
									var millisecondsPerDay = 1000 * 60 * 60 * 24;
									var millisBetween = currentLogin
											.getTime()
											- firstLogin.getTime();
									var daysDifference = millisBetween
											/ millisecondsPerDay;

									if (rootConfig.appExpiryDuration != ""
											&& daysDifference > rootConfig.appExpiryDuration) {

										$scope.exitApp = false;
										$scope.alertMessage = translateMessages($translate,"appExpiryErrMsg");
										$rootScope.lePopupCtrl.showError(
												translateMessages(
														$translate,
														"lifeEngage"),
												$scope.alertMessage,
												translateMessages(
														$translate,
														"fna.ok"),
												$scope.rootedOK);
										//$scope.isLoginAllowed = false;

										return;
									} else {
										successCallback();
									}
								}
							
							}
						}
				])
