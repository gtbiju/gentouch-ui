/*
 * Copyright 2015, LifeEngage 
 */
storeApp
    .controller(
        'CustomerRegistrationCtrl', CustomerRegistrationCtrl);
		
CustomerRegistrationCtrl.$inject = ['$rootScope',
                        '$scope',
                        '$http',
                        'AuthService',
                        '$location',
                        '$translate',
                        'UserDetailsService',
                        'DataService',
                        'GLI_LoginService',
                        '$controller',
                        'LoginService',
                        'DataWipeService',
                        'commonConfig',
						'GLI_DataService', 
						'$window', 
						'$filter', 
						'UtilityService',
						'$timeout',
						'PersistenceMapping'];
						
function CustomerRegistrationCtrl($rootScope,
                        $scope,
                        $http,
                        AuthService,
                        $location,
                        $translate,
                        UserDetailsService,
                        DataService,
                        GLI_LoginService,
                        $controller,
                        LoginService,
                        DataWipeService,
                        commonConfig,
						GLI_DataService, 
						$window, 
						$filter, 
						UtilityService,
						$timeout,
						PersistenceMapping) {
        $controller('LoginController', {
        $rootScope: $rootScope,
        $scope: $scope,
        $http: $http,
        AuthService: AuthService,
        $location: $location,
        $translate: $translate,
        UserDetailsService: UserDetailsService,
        DataService: DataService,
        LoginService: GLI_LoginService,
        LoginService: LoginService,
        DataWipeService: DataWipeService
    });
		$scope.agentCode = "";
        $scope.licenseNumber = "";
        $scope.identificationId = "";
        $scope.agentDob = "";
        $scope.errorFlag = false;
        $scope.serviceErrorFlag=false;
        $scope.registration = true;
        $scope.verification = true;
		 $scope.errorMessageService = [];
        $scope.confirmAgentCode = "";
        //adedd for fix releated for pentest starts
         $scope.confirmLicenseNumber = "";
         $scope.confirmIdentificationId = "";
         $scope.confirmDobAgent = "";
        //adedd for fix releated for pentest ends
        $scope.email = "";
        $scope.password = "";
        $scope.confirmPassword = "";
        $scope.showPassword = false;
        $scope.toggle = true;
        $scope.toggleText = '';
        $scope.passwordMatchInd = true;
        $scope.passwordRegex = rootConfig.passwordRegexPattern;
		$scope.agentCodeRegex = /^[a-zA-Z0-9_]*$/;
        $scope.emailRegex = /^[A-Za-z0-9._%+-]+@[A-Za-z]+(\.([A-Za-z]){2,4})?\.([A-Za-z]{2,4})$/;
		$scope.dobRegex = rootConfig.dobPattern;
        $rootScope.isAuthenticated = false;
        $scope.OfflineshowPopUpMsg = false;
		$scope.errorMsgExtract=[];
        var userDetailsValidation=false;
        var regPasswordValidation=false;
        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        /*
         * Update error count on blur.
         */
        $scope.onBlur = function () {
            $rootScope.updateErrorCount('myForm');
        }

        $timeout(function () {
            $rootScope.updateErrorCount('myForm');
        }, 50);


        //Checks for user is online 
        $scope.isUserOnline = function(){
            var userOnline = true;
            if((rootConfig.isDeviceMobile)&& !(rootConfig.isOfflineDesktop)){
            	if (navigator.network.connection.type == Connection.NONE) {
                    userOnline = false;
                }
            }
            if((rootConfig.isOfflineDesktop)) {
                if (!navigator.onLine) {
                    userOnline = false;
                }
            }
            return userOnline;
        }

        $scope.registerConfirm = function () {
            if ($scope.errorFlag) {
                $scope.showPopUpMsg = true;
                $scope.popUpErrorMsg = translateMessages($translate, "registration.popUpErrorMsg")
                $scope.errorMessage.push($scope.popUpErrorMsg);
				$scope.showPopUpServiceMsg = false;
                $scope.overlayTC = true;
            }
            else if($scope.serviceErrorFlag){
			     $scope.showPopUpMsg = false;
            	 $scope.showPopUpServiceMsg = true;
            	 $scope.overlayTC = true;
            }
        };

        $scope.showTermsConditionsPopup = function () {
            $scope.showTerms = true;
            $scope.overlayTC = true;
        }

        $scope.$watch('toggle', function () {
            $scope.toggleText = $scope.toggle ? 'แสดงรหัสผ่าน' : 'ซ่อนรหัสผ่าน';
        });

        $scope.closeTC = function () {
            $scope.showTerms = false;
            $scope.overlayTC = false;
        };

        $scope.closePopup = function () {
        	$scope.showPopUpMsg = false;
			$scope.showPopUpServiceMsg=false;
            $scope.overlayTC = false;
            $scope.OfflineshowPopUpMsg = false;
        };

        $scope.verifyDcocuments = function (a, b) {
            $scope.showPassword = !$scope.showPassword;
        }

        $scope.toggleSelection = function toggleSelection() {
            if ($scope.termsCheckbox) {
                $scope.termsCheckboxFlag = true;
            } else if (!$scope.termsCheckbox) {
                $scope.termsCheckboxFlag = false;
            }
        };

        $scope.validateUserDetails = function () {
            $scope.errorFlag = false;
            $scope.errorMessage = [];
            $scope.registrationErrorMessage = [];

            //Validate Agent Code
            if (!$scope.agentCode || angular.isUndefined($scope.agentCode)) {
                $scope.errorFlag = true;
                $scope.registrationErrorMessage = translateMessages($translate, "registration.agentCodeEmptyMsg");
                $scope.errorMessage.push($scope.registrationErrorMessage);
            } else if ($scope.agentCode.length < 8) {
                $scope.errorFlag = true;
                $scope.registrationErrorMessage = translateMessages($translate, "registration.agentCodeMinMsg");
                $scope.errorMessage.push($scope.registrationErrorMessage);
            } else if ($scope.agentCode.length > 8) {
                $scope.errorFlag = true;
                $scope.registrationErrorMessage = translateMessages($translate, "registration.agentCodeMaxMsg");
                $scope.errorMessage.push($scope.registrationErrorMessage);
            }

            //Validate License Number
            if (!$scope.licenseNumber || angular.isUndefined($scope.licenseNumber)) {
                $scope.errorFlag = true;
                $scope.registrationErrorMessage = translateMessages($translate, "registration.licenseNoEmptyMsg");
                $scope.errorMessage.push($scope.registrationErrorMessage);
            } else if ($scope.licenseNumber.length < 5) {
                $scope.errorFlag = true;
                $scope.registrationErrorMessage = translateMessages($translate, "registration.licenseNoMinMsg");
                $scope.errorMessage.push($scope.registrationErrorMessage);
            } else if ($scope.licenseNumber.length > 15) {
                $scope.errorFlag = true;
                $scope.registrationErrorMessage = translateMessages($translate, "registration.licenseNoMaxMsg");
                $scope.errorMessage.push($scope.registrationErrorMessage);
            }

            //Validate Identification ID
            if (!$scope.identificationId || angular.isUndefined($scope.identificationId)) {
                $scope.errorFlag = true;
                $scope.registrationErrorMessage = translateMessages($translate, "registration.identificationIdEmptyMsg");
                $scope.errorMessage.push($scope.registrationErrorMessage);
            } else if ($scope.identificationId.length < 13) {
                $scope.errorFlag = true;
                $scope.registrationErrorMessage = translateMessages($translate, "registration.identificationIdMinMsg");
                $scope.errorMessage.push($scope.registrationErrorMessage);
            } else if ($scope.identificationId.length > 13) {
                $scope.errorFlag = true;
                $scope.registrationErrorMessage = translateMessages($translate, "registration.identificationIdMaxMsg");
                $scope.errorMessage.push($scope.registrationErrorMessage);
            }
            
            //Agent DOB
            $scope.agentDob = angular.element('#agentDob').val();

            //Validate Agent Date of Birth
            if (!$scope.agentDob || angular.isUndefined($scope.agentDob)) {
                $scope.errorFlag = true;
                $scope.registrationErrorMessage = translateMessages($translate, "registration.agentDobEmptyMsg");
                $scope.errorMessage.push($scope.registrationErrorMessage);
            }

            if ($scope.errorFlag) {
                return false;
            } else {
				return true;
            }
        };

        $scope.validateRegistrationDetails = function () {
            $scope.errorFlag = false;
            $scope.errorMessage = [];
            $scope.registrationErrorMessage = [];

            //Validate Password    
            if (!$scope.password || angular.isUndefined($scope.password)) {
                $scope.errorFlag = true;
                $scope.registrationErrorMessage = translateMessages($translate, "registration.passwordEmptyMsg");
                $scope.errorMessage.push($scope.registrationErrorMessage);
            } else if ($scope.password.length < 8 || !$scope.passwordRegex.test($scope.password)) {
                $scope.errorFlag = true;
                $scope.errorMessage.push("registration.passwordInvalidMsg");
            }

            //Validate Confirm Password    
            if (!$scope.confirmPassword || angular.isUndefined($scope.confirmPassword)) {
                $scope.errorFlag = true;
                $scope.registrationErrorMessage = translateMessages($translate, "registration.confirmPasswordEmptyMsg");
                $scope.errorMessage.push($scope.registrationErrorMessage);
            }

            //Validate Email address
            if ($scope.email) {
                if (!$scope.emailRegex.test($scope.email) || $scope.email.length < 8) {
                    $scope.errorFlag = true;
                    $scope.registrationErrorMessage = translateMessages($translate, "registration.mailInvalidMsg");
                    $scope.errorMessage.push($scope.registrationErrorMessage);
                }
            }

            //Validate if Password matches confirm password
            if ($scope.password && $scope.confirmPassword) {
                $scope.passwordMatchInd = false;
                if (!angular.equals($scope.password, $scope.confirmPassword)) {
                    $scope.errorFlag = true;
                    $scope.registrationErrorMessage = translateMessages($translate, "registration.passwordMatchMsg");
                    $scope.errorMessage.push($scope.registrationErrorMessage);
                }
            }

            //Validate Terms and Condition Checkbox
            if (!$scope.termsCheckboxFlag) {
                $scope.errorFlag = true;
                $scope.registrationErrorMessage = translateMessages($translate, "registration.termsConditionCheckMsg");
                $scope.errorMessage.push($scope.registrationErrorMessage);
            }

            if ($scope.errorFlag) {
                return false;
            } else {
            	$scope.registrationSuccessfull=true;
            	return true;
            }
        };

        $scope.registerUser = function () {
        	 if((rootConfig.isDeviceMobile) || (rootConfig.isOfflineDesktop)){
        		if($scope.isUserOnline()) {
        			if ($scope.validateUserDetails()) {
	                	var dobAgent = ExternalToInternal($scope.agentDob); 
	                    var transObj = $scope.mapScopeToPersistance();
	                    var userDetails = {
	                    	"userDetails": {
	                           "agentCode": $scope.agentCode,
	                           "licenseNumber": $scope.licenseNumber,
	                           "identificationId": $scope.identificationId,
	                           "agentDob": dobAgent  
	                    	}
	                    };
	                    transObj.TransactionData = userDetails;
	                    transObj.Type = "UserDetails";
	                    $rootScope.showHideLoadingImage(true, "Loading..");
	                    GLI_DataService.getTransactions(transObj, $scope.getTransationSuccess, $scope.getTransationError);
        			} else {
        				$scope.registerConfirm();
        			}
        		} else {
        			$scope.OfflineshowPopUpMsg = true;
        			$scope.firsttimeloginErrorMsg = translateMessages($translate, "firsttimeloginErrorMsg");
        		}
        	 } else {
        		 if ($scope.validateUserDetails()) {
        			 var dobAgent = ExternalToInternal($scope.agentDob); 
        			 var transObj = $scope.mapScopeToPersistance();
				     var userDetails = {
				    	"userDetails": {
				    		"agentCode": $scope.agentCode,
				    		"licenseNumber": $scope.licenseNumber,
				    		"identificationId": $scope.identificationId,
				    		"agentDob": dobAgent  
				    	}
                    };
                    transObj.TransactionData = userDetails;
                    transObj.Type = "UserDetails";
                    $rootScope.showHideLoadingImage(true, "Loading..");
                    GLI_DataService.getTransactions(transObj, $scope.getTransationSuccess, $scope.getTransationError);
        		} else {
        			$scope.registerConfirm();
        		}
        	 }
        };
        
        $scope.createPassword = function () {
        	if((rootConfig.isDeviceMobile) || (rootConfig.isOfflineDesktop)){
        		if($scope.isUserOnline()) {
        			if ($scope.validateRegistrationDetails()) {
        				var transObj = $scope.mapScopeToPersistance();
                        var createPassword = {
                        	"createPassword": {
                               "password": $scope.password,
                               "emailId": $scope.email
                        	}
	                    };
	                    transObj.TransactionData = createPassword;
	                    transObj.Type = "CreatePassword";
	                    GLI_DataService.getTransactions(transObj, $scope.getTransationSuccess, $scope.getTransationError);
        			} else {
        				$scope.registerConfirm();
        			} 
        		} else {
        			$scope.OfflineshowPopUpMsg = true;
        			$scope.firsttimeloginErrorMsg = translateMessages($translate, "firsttimeloginErrorMsg");
	            }	
        	} else {
        		if ($scope.validateRegistrationDetails()) {
        			var transObj = $scope.mapScopeToPersistance();
				    var createPassword = {
				    	"createPassword": {
				    		"password": $scope.password,
				    		"emailId": $scope.email
				        }
				   };
				   transObj.TransactionData = createPassword;
				   transObj.Type = "CreatePassword";
				   GLI_DataService.getTransactions(transObj, $scope.getTransationSuccess, $scope.getTransationError);
	            } else {
	            	$scope.registerConfirm();
	            } 
        	}
        };
        
    	$scope.getTransationSuccess = function(data) {
    		$scope.errorMessageService = [];
    		$rootScope.showHideLoadingImage(false, "Loading..");
    		if(data[0] !== undefined && data[0].TransactionData !== null){
    			var statusCode = data[0].TransactionData.StatusData.StatusCode;
       	       	var errorMsg=  data[0].TransactionData.StatusData.StatusMessage;
       	       
       		    //Authorization is successful
       		    if(statusCode==="100" && $scope.registrationSuccessfull){
       		    	//$location.path('/login');
					$scope.checkifEligibleToLogin_CusReg();
			    } else if(statusCode === "100"){
       			  	$scope.successMsg = $scope.convertertranslate("userRegnSucc");
                    $scope.activeResendBtn = false;
					$scope.confirmAgentCode = $scope.agentCode;
				    $rootScope.showHideLoadingImage(false, "");
                    $location.path('/customerregistration');
                    $scope.registration = false;
                    $scope.verification = false;
                    $scope.refresh();
                    $timeout(function () {
                    	$rootScope.updateErrorCount('myForm');
                    }, 50);
       		   	} else if (statusCode === "205"){
					$scope.serviceErrorFlag = true;	
					$scope.errorMessageService.push(translateMessages($translate,"licenseDateExpired"));
					$scope.registerConfirm();
				} else if (statusCode === "206"){
					$scope.serviceErrorFlag = true;	
					$scope.errorMessageService.push(translateMessages($translate,"agentStatusNotActive"));
					$scope.registerConfirm();
				} else if (statusCode === "207"){
					$scope.serviceErrorFlag = true;	
					$scope.errorMessageService.push(translateMessages($translate,"usernotfound"));
					$scope.registerConfirm();
				} else if (statusCode === "208"){
					$scope.serviceErrorFlag = true;	
					$scope.errorMessageService.push(translateMessages($translate,"agentCodeInvalidMsg"));
					$scope.registerConfirm();
				} else if(statusCode.length>3){
       		   		$scope.serviceErrorFlag = true;						 
       		   		$scope.errorMsgExtract=[];
       		   		$scope.errorMessageService=[];
       		   		if(statusCode.indexOf('-')>0){
       		   			statusCode=statusCode.substring(0,statusCode.length-1);
       		   			$scope.errorMsgExtract=statusCode.split('-');
       		   			for(var i=0;i<=$scope.errorMsgExtract.length;i++){
							if($scope.errorMsgExtract[i] === "202"){
								$scope.errorMsgExtract[i] = translateMessages($translate,"licenseNoInvalidMsg");
								$scope.errorMessageService.push($scope.errorMsgExtract[i]);
							} else if($scope.errorMsgExtract[i] === "203"){
								$scope.errorMsgExtract[i] = translateMessages($translate,"identificationIDInvalidMsg");
								$scope.errorMessageService.push($scope.errorMsgExtract[i]);
							} else if($scope.errorMsgExtract[i] === "204"){
								$scope.errorMsgExtract[i] = translateMessages($translate,"agentDobInvalidMsg");
								$scope.errorMessageService.push($scope.errorMsgExtract[i]);
							}
       		   			}
       		   		} 
					else {
       		   			$scope.errorMsgExtract[0]=errorMsg;
       		   		}
       		   		$scope.errorMessageService = $scope.errorMsgExtract;
				    $scope.registerConfirm();
       		   	} 
				else{
   				   $scope.errorMessage.push();
       		   	}
       		    $scope.$apply();
    		}
       };
   	   
       $scope.getTransationError = function(data) {
             $rootScope.showHideLoadingImage(false, "Loading..");
   			 $rootScope.serviceFailed=true;
			 if (rootConfig.isDeviceMobile && !checkConnection()) {
				$scope.message = translateMessages($translate, "networkValidationErrorMessage");
			 } else {
				$scope.message = translateMessages($translate, "regServerError");
			 }
			 $scope.$emit('tokenEvent', { message: $scope.message });
			 if (data == "Error in ajax callE") {
				 $scope.onServerError=true;
				 $scope.serverErrorMessage= translateMessages($translate, "serverErrorMessage");
			 }  else {
			        //showHideLoadingImage(false);
			 }
			 $rootScope.$apply();
       };
       
       $scope.mapScopeToPersistance = function (type) {
    	   PersistenceMapping.clearTransactionKeys();
           var transactionObj = PersistenceMapping.mapScopeToPersistence({});
		   var dobAgent = ExternalToInternal($scope.agentDob);
		   transactionObj.Key1= $scope.agentCode;
		   transactionObj.Key2=$scope.licenseNumber;
		   transactionObj.Key3=$scope.identificationId;
	       transactionObj.Key4=dobAgent;
	       transactionObj.Key5=$scope.password;
	       transactionObj.Key6=$scope.email;
           return transactionObj;
      };
      $scope.checkifEligibleToLogin_CusReg = function () {
		$scope.username = $scope.agentCode;
        if ($scope.username != "" && $scope.password != "" && $scope.username != undefined && $scope.password != undefined) {
            if (rootConfig.isDeviceMobile && !rootConfig.isOfflineDesktop) {
                createKeyIfNotExists_CusReg(function () {
                    var filesToBeEncrypted = getFilesToBeEncrypted();
                    window.plugins.LEEncryption.encryptDB(filesToBeEncrypted, function (data) {
                        if ((rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
                            $scope.CheckforRootedDevice(function () {
                                $scope.CheckForAppExpiry(function () {
                                    $scope.login_CusReg();
                                })
                            });
                        } else
                            $scope.login_CusReg();
                    });
                });
            } else if ((rootConfig.isDeviceMobile) &&
                (rootConfig.isOfflineDesktop)) {
                $scope.CheckForAppExpiry(function () {
                    $scope.login_CusReg();
                });
            } else {
                $scope.login_CusReg();
            }
        } else {
                $scope.showPopUpMsg = true;
                $scope.loginmessage = translateMessages($translate, "loginCredentialEmptyErrorMsg");
        }
    };
	function createKeyIfNotExists_CusReg(success) {
        var options = [];
        var obj = {};
        obj.appName = rootConfig.appName;
        options.push(obj);
        window.plugins.LEEncryption.checkKeyAvailability(options,
            function (data) {
                if (data == "key exists") {
                    success();
                } else {
                    AuthService.fetchKey({
                            username: $scope.username,
                            password: $scope.password
                        }, function (data) {

                            if (data.StatusData.Status == "SUCCESS") {
                                var KeyArray = [];
                                var keyObj = {};
                                keyObj.key = data.encryptionKey;
                                keyObj.appName = rootConfig.appName;
                                KeyArray.push(keyObj);
                                window.plugins.LEEncryption.insertKey(KeyArray,
                                    function (data) {
                                        if (data == "key entered successfully") {}
                                        success();
                                    },
                                    function (data) {
                                        
                                    }
                                );
                            } else {
                                //else condition
                            }
                        },
                        function (data, status) {
                            $rootScope.isAuthenticated = false;

                            if (status == 401) {
                                $scope.loginError = translateMessages(
                                    $translate,
                                    "loginCredentialErrorMsg");
                            } else if (status == 404) {
                                $scope.loginError = translateMessages(
                                    $translate,
                                    "resourceNotFoundErrMsg");
                            } else if (status == 500) {
                                $scope.loginError = translateMessages(
                                    $translate,
                                    "resourceUnAvailableErrMsg");
                            } else if (status == 0) {
                                $scope.loginError = translateMessages(
                                    $translate,
                                    "firsttimeloginErrorMsg");
                            } else {
                                $scope.loginError = translateMessages(
                                    $translate,
                                    "loginCredentialErrorMsg");
                            }
                            $rootScope.showHideLoadingImage(false, "");
                        });
                }
            },
            function (error) {
               
            }
        );
    };
	function getFilesToBeEncrypted() {
        var filesToBeEncrypted = [];
        var filesArray = ["core-products.db",
                          "LifeEngageData.db", 
                          "code-lookup.db", 
                          "RequirementDB.db", 
                          "GB8Products.db", 
                          "CHS8080Products.db", 
                          "GP2025Products.db", 
                          "GS20PProducts.db", 
                          "4N85Products.db", 
                          "1P05Products.db",
                          "4P10Products.db",
                          "4P04Products.db"];
        filesToBeEncrypted = $scope.pushEncryptionFilesToArray(filesArray, filesToBeEncrypted);
        var options = [];
        var obj = {};
        obj.appName = rootConfig.appName;
        obj.fileArray = filesToBeEncrypted;
        options.push(obj);
        return options;
    };
	$scope.pushEncryptionFilesToArray = function (fileArray, files) {
        for (var i = 0; i < fileArray.length; i++) {
            var fileDesc = {};
            fileDesc.fileName = fileArray[i];
            files.push(fileDesc);
        }
        return files;
    };
	 $scope.login_CusReg = function () {
        $rootScope.showHideLoadingImage(true, "Loading..");
        $scope.UpdateFirstLoginDateOnUpgrade();
        if ((rootConfig.isDeviceMobile) &&
            !(rootConfig.isOfflineDesktop)) {
            if (!$scope
                .isUserOnline()) {
                LoginService
                    .checkAgentIdExistingInDb({
                            userId: $scope.username,
                            password: $scope.password
                        },
                        function (
                            data) {
                            if (data.length > 0) {
                                $rootScope.showHideLoadingImage(true, "Loading..");
                                var localPassword = Decrypt_text(data[0].password);
                                if ($scope.password == localPassword) {
                                    $rootScope.username = $scope.username;
                                    LoginService
                                        .fetchToken(
                                            $scope.username,
                                            function (
                                                res) {
                                                var currentDate = new Date();
                                                var offlineDueDate = new Date(
                                                    currentDate
                                                    .setDate(currentDate
                                                        .getDate() -
                                                        rootConfig.maxPermittedOfflineDuration));
                                                var lastLoggedInDate = res[0].lastLoggedInDate;
                                                if (new Date(
                                                        lastLoggedInDate) > offlineDueDate) {
                                                    $rootScope.isAuthenticated = true;
                                                    $scope.userDetails.options.headers.Token = res[0].token;
                                                    UserDetailsService
                                                        .setUserDetailsModel($scope.userDetails);
                                                    if ($scope.rememberme == true) {
                                                        var loginDetails = {
                                                            'leuname': $scope.username,
                                                            'lepwd': $scope.password
                                                        };
                                                        localStorage
                                                            .setItem(
                                                                'loginDetails',
                                                                JSON
                                                                .stringify(loginDetails));
                                                    } else {
                                                        localStorage
                                                            .removeItem('loginDetails');
                                                    }
                                                    $location
                                                        .path('/landingPage');
                                                     $scope.logoutpopup = false;    
                                                    $scope
                                                        .refresh();
                                                } else {
                                                    $rootScope.isAuthenticated = false;
                                                    $rootScope.lePopupCtrl
                                                        .showSuccess(translateMessages(
                                                                $translate, "lifeEngage"),
                                                            translateMessages($translate,
                                                                "tokenExpiryMessage"),
                                                            translateMessages($translate,
                                                                "ok"), terminateLogin);
                                                    $rootScope.showHideLoadingImage(false, "");
                                                    $scope.refresh();
                                                }
                                            },
                                            function (
                                                err) {

                                            });
                                } else {
                                    $rootScope.isAuthenticated = false;
                                    $scope.loginError = translateMessages(
                                        $translate,
                                        "loginCredentialErrorMsg");
                                    $rootScope.showHideLoadingImage(false, "");
                                    $scope.refresh();
                                }
                            } else {
                                if ($scope.isUserOnline()) {
                                    $scope
                                        .authenticateUserOnline();
                                } else {
                                    $rootScope.showHideLoadingImage(false, "");
                                    $rootScope.isAuthenticated = false;
                                    $scope.loginError = translateMessages(
                                        $translate,
                                        "firsttimeloginErrorMsg");
                                    $scope.refresh();
                                }
                            }
                        },
                        function (
                            err) {
                            $rootScope.showHideLoadingImage(false, "");
                            $rootScope.isAuthenticated = false;
                            $scope.loginError = translateMessages(
                                $translate,
                                "firsttimeloginErrorMsg");
                        });
            } else {
                $rootScope.showHideLoadingImage(true, "Loading..");
                $scope
                    .authenticateUserOnline();
            }
        } else {
            $scope.authenticateUserOnline();
        }
    };
	$scope.authenticateUserOnline = function () {
        AuthService
            .login({
                    username: $scope.username,
                    password: $scope.password
                },
                function (res, status) {
                    $rootScope.username = $scope.username;
                    $rootScope.isAuthenticated = true;
                    $scope.userDetails.user.userId = $scope.username;
                    $scope.userDetails.user.password = Encrypt_text($scope.password);
                    $scope.userDetails.user.token = res;
                    $scope.userDetails.user.lastLoggedInDate = new Date();
                    $scope.userDetails.options.headers.Token = res;
                    UserDetailsService.setUserDetailsModel($scope.userDetails);
                    LoginService
                        .saveToken(
                            $scope.userDetails.user,
                            function (
                                res) {

                                if ($scope.rememberme == true) {
                                    var loginDetails = {
                                        'leuname': $scope.username,
                                        'lepwd': $scope.password
                                    };
                                    localStorage
                                        .setItem(
                                            'loginDetails',
                                            JSON
                                            .stringify(loginDetails));
                                } else {
                                    localStorage
                                        .removeItem('loginDetails');
                                }
                                if (status == 220) {
                                    //$rootScope.suspendedmsg = translateMessages($translate,"suspendedFooterMsg");
                                    $rootScope.showHideLoadingImage(false, "");
                                    $rootScope.lePopupCtrl
                                        .showSuccess(translateMessages(
                                                $translate, "lifeEngage"),
                                            translateMessages($translate,
                                                "suspendedMsg"),
                                            translateMessages($translate,
                                                "ok"), suspendedLogin);
                                } else {
                                    $location
                                        .path('/landingPage');
                                         $scope.logoutpopup = false;
                                }

                                $scope
                                    .refresh();

                            },
                            function (
                                err) {

                            });
                },
                function (data, status) {
                    $rootScope.isAuthenticated = false;
                    $rootScope.showHideLoadingImage(false, "");
                    $scope.refresh();
                    if (status) {
                        if (status == 401) {
							$scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "loginmessage.loginCredentialErrorMsg");
                        } else if (status == 400) {
							$scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "resourceNotFoundErrMsg");
                        } else if (status == 500) {
							$scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "resourceUnAvailableErrMsg");
                        } else if (status == 480) {
							$scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "loginmessage.unauthorizedaccess");
                        } else if (status == 481) {
							$scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "loginmessage.agentLicenseExpired");
                        } else if (status == 482) {
							$scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "loginmessage.agentLicenseExpired");
                        }
                        else if (status == 502) {
							$scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "loginmessage.accountlocked");
                        }
                        else if (status == 471) {
							$scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "loginmessage.usernotfound");
                        }
                        else if (status == 475) {
                            $scope.showPopUpMsg = true;
                            $scope.loginmessage = translateMessages($translate, "loginmessage.passwordExpired");
                        } 
                         else {
                            if ($scope.username != "" && $scope.password != "" && $scope.username != undefined && $scope.password != undefined) {
                                $rootScope.showHideLoadingImage(false, "");
                                $scope.loginError = translateMessages($translate,
                                    "loginCredentialErrorMsg");
                            } else {
                                $rootScope.showHideLoadingImage(false, "");
                                $scope.loginError = translateMessages($translate,
                                    "loginCredentialEmptyErrorMsg");
                            }
                        }
                    } else {
                        $scope.loginError = translateMessages($translate,
                            "LE_SYNC_ERR_104");
                    }
                })

        $scope.refresh();
    };

};
