/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:Task List Controller
 CreatedDate:11/12/2013
 Description:Task List Module.
 */

storeApp.controller('TaskListController',
		[
				'$rootScope',
				'$scope',
				'$filter',
				'DataService',
				'IllustratorVariables',
				'LookupService',
				'DocumentService',
				'ProductService',
				'$compile',
				'$routeParams',
				'$location',
				'$translate',
				'$debounce',
				'UtilityService',
				'PersistenceMapping',
				'AutoSave',
				'globalService',
				function($rootScope, $scope, $filter, DataService,
						IllustratorVariables, LookupService, DocumentService,
						ProductService, $compile, $routeParams, $location,
						$translate, $debounce, UtilityService,
						PersistenceMapping, AutoSave, globalService) {

					$rootScope.showHideLoadingImage(true, 'loadingMessage',
							$translate);
					$rootScope.moduleHeader = "tasklistHeader";

					$scope.tasks = "";
					var todoSelected = "";
					var endDate = "";
					$rootScope.moduleVariable = translateMessages($translate,
							$rootScope.moduleHeader);

					$scope.getClass = function(event) {
						if (event == "Birthday") {
							return "my_tasklist_icon birthday_icon";
						} else {
							return "my_tasklist_icon calendar_icon";
						}
					};

					$scope.onGetListingsError = function(dbObject,
							todoSelected, currentDate, endDate, viewallDate) {
						$rootScope.showHideLoadingImage(false);
						$scope.refresh();
					};

					$scope.onGetListingsSuccess = function(dbObject,
							todoSelected, currentDate, endDate, viewallDate) {
						var dateTemp = currentDate.split("-");
						var currentYear = dateTemp[0];
						var taskMonth = [];

						for (var i = 0; i < dbObject.length; i++) {
							if (dbObject[i].Key7 != ""
									&& dbObject[i].Key7 != null) {
								var fnaDOB = dbObject[i].Key7;
								var fnaTempDOB = fnaDOB.split("-");
								var fnaDateDOB = "-" + fnaTempDOB[1] + "-"
										+ fnaTempDOB[2];
								var currentDOBYear = currentYear + fnaDateDOB;
								dbObject[i].Key7 = currentDOBYear;

								var taskJson = {
									"leadId" : dbObject[i].Key1,
									"name" : dbObject[i].Key2,
									"contact" : dbObject[i].Key4,
									"event" : "",
									"eventDate" : "",
									"eventTime" : ""
								};

								taskJson.event = "Birthday";
								taskJson.eventDate = dbObject[i].Key7;
								taskMonth.push(taskJson);
							}
							if (dbObject[i].Key19 != ""
									&& dbObject[i].Key19 != null) {
								var setLmsDate = new Array();
								var setLmsDate = dbObject[i].Key19.split(" ");
								var eventDate = setLmsDate[0];
								var eventTime = setLmsDate[1];

								var taskJson = {
									"leadId" : dbObject[i].Key1,
									"name" : dbObject[i].Key2,
									"contact" : dbObject[i].Key4,
									"event" : "",
									"eventDate" : eventDate,
									"eventTime" : eventTime
								};

								taskJson.event = "Meeting";
								taskMonth.push(taskJson);
							}
						}

						$scope.tasks = taskMonth;
						$scope.refresh();

					};



					$scope.callback = function() {
						$rootScope.showHideLoadingImage(false);
					};

					$scope.calenderViewListings = function(todoSelected) {

						PersistenceMapping.clearTransactionKeys();
						var transactionObj = PersistenceMapping
								.mapScopeToPersistence({});
						var today = new Date();
						var yyyy = today.getFullYear();
						var dd = today.getDate();
						var mm = today.getMonth() + 1;
						if (mm < 10) {
							mm = '0' + mm;
						}
						var currentDate = yyyy + '-' + mm + '-' + dd;
						var viewallDate;
						if (todoSelected == "Day") {
							$("#tab_day").addClass("active");
							$("#tab_week").removeClass("active");
							$("#tab_month").removeClass("active");
							$("#tab_all").removeClass("active");
						}
						if (todoSelected == "Week") {
							var date1 = new Date();
							endDate = new Date(date1
									.setTime(date1.getTime() + 6 * 86400000));
							var year = endDate.getFullYear();
							var day = endDate.getDate();
							var month = endDate.getMonth() + 1;
							endDate = year + '-' + month + '-' + day;
							$("#tab_day").removeClass("active");
							$("#tab_week").addClass("active");
							$("#tab_month").removeClass("active");
							$("#tab_all").removeClass("active");
						}
						if (todoSelected == "Month") {
							var date2 = new Date();
							endDate = new Date(date2
									.setTime(date2.getTime() + 30 * 86400000));
							var year = endDate.getFullYear();
							var day = endDate.getDate();
							var month = endDate.getMonth() + 1;
							endDate = year + '-' + month + '-' + day;
							$("#tab_day").removeClass("active");
							$("#tab_week").removeClass("active");
							$("#tab_month").addClass("active");
							$("#tab_all").removeClass("active");
						}
						if (todoSelected == "All") {
							var date3 = new Date();
							var date4 = new Date();
							viewallDate = new Date(date3.setTime(date3
									.getTime() - 90 * 86400000));
							endDate = new Date(date4
									.setTime(date4.getTime() + 90 * 86400000));
							var year = endDate.getFullYear();
							var day = endDate.getDate();
							var month = endDate.getMonth() + 1;
							endDate = year + '-' + month + '-' + day;
							$("#tab_day").removeClass("active");
							$("#tab_week").removeClass("active");
							$("#tab_month").removeClass("active");
							$("#tab_all").addClass("active");
						}

						DataService.getCalenderListings(currentDate,
								todoSelected, endDate, viewallDate,
								transactionObj, $scope.onGetListingsSuccess,
								$scope.onGetListingsError);
					};

					LEDynamicUI.paintUI(rootConfig.template,
							rootConfig.taskListUIJson, "taskList", "#taskList",
							true, function(callbackId, data) {

								$scope.callback();
								var tabName = $routeParams.tabName;
								PersistenceMapping.clearTransactionKeys();
								var transactionObj = PersistenceMapping
										.mapScopeToPersistence({});
								transactionObj.Id = $routeParams.transactionId;
								var viewallDate;

								var today = new Date();
								var yyyy = today.getFullYear();
								var dd = today.getDate();
								var mm = today.getMonth() + 1;
								if (mm < 10) {
									mm = '0' + mm;
								}
								var currentDate = yyyy + '-' + mm + '-' + dd;

								if (tabName == "Day") {
									$("#tab_day").addClass("active");
									$("#tab_week").removeClass("active");
									$("#tab_month").removeClass("active");
									$("#tab_all").removeClass("active");
								}
								if (tabName == "Week") {
									var date1 = new Date();
									endDate = new Date(date1.setTime(date1
											.getTime() + 6 * 86400000));
									var year = endDate.getFullYear();
									var day = endDate.getDate();
									var month = endDate.getMonth() + 1;
									endDate = year + '-' + month + '-' + day;
									$("#tab_day").removeClass("active");
									$("#tab_week").addClass("active");
									$("#tab_month").removeClass("active");
									$("#tab_all").removeClass("active");
								}
								if (tabName == "Month") {
									var date2 = new Date();
									endDate = new Date(date2.setTime(date2
											.getTime() + 30 * 86400000));
									var year = endDate.getFullYear();
									var day = endDate.getDate();
									var month = endDate.getMonth() + 1;
									endDate = year + '-' + month + '-' + day;
									$("#tab_day").removeClass("active");
									$("#tab_week").removeClass("active");
									$("#tab_month").addClass("active");
									$("#tab_all").removeClass("active");
								}
								if (todoSelected == "All") {
									var date3 = new Date();
									var date4 = new Date();
									viewallDate = new Date(date3.setTime(date3
											.getTime() - 90 * 86400000));
									endDate = new Date(date4.setTime(date4
											.getTime() + 90 * 86400000));
									$("#tab_day").removeClass("active");
									$("#tab_week").removeClass("active");
									$("#tab_month").removeClass("active");
									$("#tab_all").addClass("active");
								}
								DataService.getCalenderListings(currentDate,
										tabName, endDate, viewallDate,
										transactionObj,
										$scope.onGetListingsSuccess,
										$scope.onGetListingsError);

							}, $scope, $compile);

				} ]);