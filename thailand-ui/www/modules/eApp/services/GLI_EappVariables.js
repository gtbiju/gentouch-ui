/*
 * Created on 17/12/2015
 * GLI_EappVariables
 */


angular
		.module('lifeEngage.GLI_EappVariables', [])
		.service(
				"GLI_EappVariables",['$http','EappVariables',
				function($http,EappVariables) {
					EappVariables.prepopulatedInsuredData;
					EappVariables.prepopulatedProposerData;
					EappVariables.prepopulatedBeneficiaryData;
                    EappVariables.illustrationOutputData;
					EappVariables.additionalInsuredQuestionnaire;
					EappVariables.questions;
					EappVariables.QuestionModel;
					EappVariables.agentData;
					EappVariables.agentType;
					EappVariables.leadName;
					/* FNA changes by LE Team for bug 4166 starts */
					EappVariables.fromFNA = false;
					/* FNA changes by LE Team for bug 4166 ends */
					EappVariables.clearEappVariables= function() {
						EappVariables.EappKeys={};
						EappVariables.EappKeys.Key1 = "";
						EappVariables.EappKeys.Key2 = "";
						EappVariables.EappKeys.Key3 = "";
						EappVariables.EappKeys.Key4 = "";
						EappVariables.EappKeys.Key5 = 0;
						EappVariables.EappKeys.Key7 = "";
						EappVariables.EappKeys.Key21 = "";
						EappVariables.EappKeys.Key26 = "";
						EappVariables.EappKeys.Id = "";
						EappVariables.EappKeys.TransTrackingID = "";
						EappVariables.QuestionModel="";
						EappVariables.Signature=[];
						EappVariables.getDocTypeFormIdForPending=[];
						EappVariables.additionalQuestionsCount=0;
						EappVariables.EappModel={};
					};
					return EappVariables;
				}]);