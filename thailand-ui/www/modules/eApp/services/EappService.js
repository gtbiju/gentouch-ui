angular.module('lifeEngage.EappService', []).factory("EappService",['$rootScope', '$translate', 'PersistenceMapping', 'globalService', 'EappVariables', 'DataService', 'UtilityService','DataLookupService','DocumentService','SyncService' ,function($rootScope, $translate, PersistenceMapping, globalService, EappVariables, DataService, UtilityService,DataLookupService,DocumentService,SyncService) {
	var EappService = new Object();
	var docUploadIndex="";
	EappService.mapKeysfromPersistence = function() {
		EappVariables.setEappModel(PersistenceMapping.transactionData);
		Object.keys(PersistenceMapping).map(function(value, index) {
			if (value.indexOf('Key') > -1) {
				EappVariables.EappKeys[value] = PersistenceMapping[value];
			}
		});

	};
	EappService.mapKeysforPersistence = function() {
		PersistenceMapping.Key1 = EappVariables.EappKeys.Key1;
		PersistenceMapping.Key2 = EappVariables.EappKeys.Key2;
		PersistenceMapping.Key3 = EappVariables.EappKeys.Key3;
		PersistenceMapping.Key4 = EappVariables.EappKeys.Key4;
		PersistenceMapping.Key5 = EappVariables.EappKeys.Key5;
		if (EappVariables.EappModel) {
			var fullName = EappVariables.EappModel.Proposer.BasicDetails.firstName;
			if (EappVariables.EappModel.Proposer.BasicDetails.lastName){
				fullName = fullName + ' ' + EappVariables.EappModel.Proposer.BasicDetails.lastName;
			}
			PersistenceMapping.Key6 = fullName;
			PersistenceMapping.Key8 = (EappVariables.EappModel.Proposer.ContactDetails.mobileNumber1) ? EappVariables.EappModel.Proposer.ContactDetails.mobileNumber1 : "";
			PersistenceMapping.Key19 = EappVariables.EappModel.Product.ProductDetails.productName;
		}
		PersistenceMapping.Key10 = "FullDetails";
		PersistenceMapping.Key12 = "";
		if ((rootConfig.isDeviceMobile)) {
			if (EappVariables.EappKeys.Id == null || EappVariables.EappKeys.Id == 0) {
								PersistenceMapping.creationDate = UtilityService
										.getFormattedDateTime();
								EappVariables.EappKeys.creationDate=UtilityService
										.getFormattedDateTime();
							}else{
								PersistenceMapping.creationDate=EappVariables.EappKeys.creationDate;
			}
		} else {
							if (EappVariables.EappKeys.Key4==null || EappVariables.EappKeys.Key4==0 || EappVariables.EappKeys.Key4==0) {
							  PersistenceMapping.creationDate = UtilityService
									.getFormattedDateTime();
							  EappVariables.EappKeys.creationDate=UtilityService
										.getFormattedDateTime();
							}else{
								PersistenceMapping.creationDate=EappVariables.EappKeys.creationDate;
				}
		}
		if (EappVariables.EappKeys.TransTrackingID == null || EappVariables.EappKeys.TransTrackingID == 0) {
			PersistenceMapping.TransTrackingID = UtilityService.getTransTrackingID();
			EappVariables.EappKeys.TransTrackingID = PersistenceMapping.TransTrackingID;
		} else {
			PersistenceMapping.TransTrackingID = EappVariables.EappKeys.TransTrackingID;
		}
		PersistenceMapping.Key15 = EappVariables.EappKeys.Key15 != null ? EappVariables.EappKeys.Key15 : "Saved";
		PersistenceMapping.Key16 = EappVariables.EappKeys.Key16 != null ? EappVariables.EappKeys.Key16 : "";
		PersistenceMapping.Type = "eApp";
	};
	/*********************************************************************************************************************************************
	Pre-populate Eapp model with Data 
	Now this function is used to pre-populate data from global service if illustration is present.
	The same function can be used to pre-populate any data like user information from third party service when we integrate it with CRM or so
	************************************************************************************************************************************/
	EappService.prepopulateData=function()
	{
		if(EappVariables.EappKeys.Key3 !=0)
		{
			EappVariables.EappModel.Insured = globalService.getInsured();
			//EappVariables.EappModel.Proposer = globalService.getPayer();
			EappVariables.EappModel.Proposer = angular.copy(globalService.getPayer());
			EappVariables.EappModel.Payer = globalService.getPayer();
			EappVariables.EappModel.Product = globalService.getProduct();
			EappVariables.EappModel.Beneficiaries = globalService.getSelectdBeneficiaries();
		}
	
	}

EappService.mapDocScopeToPersistence = function(transactionObject) {
        var requirementObject = {};
        requirementObject.identifier = (EappVariables.EappKeys.TransTrackingID !== null && $rootScope.transactionId != 0) ? EappVariables.EappKeys.TransTrackingID : null;

        if ((rootConfig.isDeviceMobile)) {
            requirementObject.requirementName = transactionObject != null && transactionObject.requirementName != null ? transactionObject.requirementName : "";
			requirementObject.documentName = transactionObject != null && transactionObject.documentName != null ? transactionObject.documentName : "";
            requirementObject.fileName = transactionObject != null && transactionObject.fileName != null ? transactionObject.fileName : " ";
            requirementObject.status = transactionObject != null && transactionObject.status != null ? transactionObject.status : " ";
            requirementObject.lastSyncDate = transactionObject != null && transactionObject.lastSyncDate != null ? transactionObject.lastSyncDate : " ";
            requirementObject.base64String = transactionObject != null && transactionObject.base64String != null ? transactionObject.base64String : " ";
			requirementObject.description = transactionObject != null && transactionObject.description != null ? transactionObject.description : " ";;
         
        }
        return requirementObject;
    }

	EappService.copyUploadedFile = function(fileName, index, requirementName, requirementType, documentType,description,successCallback) {
		
		if((rootConfig.isDeviceMobile)){
	
	     DocumentService.copyUploadedFile(fileName,function(filePath) {
					var srcUrl = '';
					
	             if ((!rootConfig.isOfflineDesktop)) {
                    srcUrl = filePath;
                } else {
                    srcUrl = "../" + filePath.path;
                }
				EappService.saveRequirementFiles(srcUrl, index, requirementName, requirementType, description, documentType, function() {
                    successCallback();
                });
	     });
	  }
		
		else{
			  //srcUrl = "../" + filePath.path;
			EappService.saveRequirementFiles(fileName, index, requirementName, requirementType, description, documentType, function() {
                    successCallback();
                });
		}
		
	}
	EappService.getRequirementFilePath = function(filedata, index, requirementName, requirementType, documentType, successCallback, errorCallback) {
    if ((rootConfig.isDeviceMobile)) { 
	var path="";
        if (requirementType == "Signature") {
            DocumentService.base64AsFile(filedata, function(filePath) {
                successCallback(filePath);
            }, function(error) {});
        } else {
		    if(filedata.path){
			path = filedata.path;
			
			}else{
			path = filedata;
			}
            DocumentService.copyFile(path,function(filePath) {
            	var fileName=filedata.name;
            	/*var data1=filePath.split("/");
            	var fileName=data1[data1.length-1];*/
                var description = "";
                var srcUrl = '';
                if (!rootConfig.isOfflineDesktop) {
                    srcUrl = filePath;
                } else {
                    srcUrl = "../" + filePath.path;
                }

               EappService.saveTransaction(function() {
	 
				EappService.saveRequirementFiles(srcUrl, index, requirementName, requirementType, description, documentType, function() {
                    successCallback();
                },'',fileName);
	 
	 }, this.onSaveError, false);
            }, errorCallback,filedata);

        }
    } else {
        var docCount = 0;
        var currAgentId = $rootScope.agentId == null ? $rootScope.username : $rootScope.agentId;
        var document = EappService.mapDocScopePersistence(filedata);
        if (!document.documentName) {
            var d = new Date();
            var n = d.getTime();
            var name = "document";
            var newFileName = "Agent1" + n + "_" + name;
            document.documentName = newFileName;
        }
        DocumentService.browseFile(document, currAgentId, function(filePath, outStr, fr) {
            var description = "";
            var fileName=document.fileObj.name;
            filePath = document.documentPath;
            EappVariables.DocumentPath = filePath;
            EappService.saveRequirementFiles(filePath, index, requirementName, requirementType, description,documentType, function() {
                successCallback();
            },'',fileName);
        }, function(error) {
		if(errorCallback){
			errorCallback(error);
			}
        });
    }
};
	EappService.saveFileReqIE=function(filedata, index, requirementName, requirementType, documentType, successCallback){
	
	 var description = "";
	 EappVariables.DocumentPath = filedata;
	 EappService.saveRequirementFiles(filedata, index, requirementName, requirementType, description,documentType, function() {
                successCallback();
            });
	};
	EappService.saveRequirement = function(filePath, requirementType, partyIdentifier, isMandatory, docArray, documentTypes, successCallback, errorCallback) {
    var requirementObj = Requirement();
    var currentDate = new Date();
    var timeStamp = currentDate.getTime();
    var index = "";
    var description = "";
    var docObject = [];

    if ((requirementType !== "Signature") || (EappVariables.Signature.length == 0)) {
        	var docIndex = 0;
            requirementObj.requirementType = requirementType;
            requirementObj.requirement = requirementType;
            requirementObj.requirementName = "Agent1" + "_" + requirementType + "_" + timeStamp;
            requirementObj.partyIdentifier = partyIdentifier;
            requirementObj.isMandatory = isMandatory;
			 do {
            if (requirementType == "Signature") {
                requirementObj.Documents[docIndex].documentName = "Agent1" + "_" + partyIdentifier + "_" + requirementType + "_" + partyIdentifier + "_" + timeStamp;
                requirementObj.Documents[docIndex].documentType = requirementType;
                requirementObj.Documents[docIndex].documentProofSubmitted = "Signature";
                requirementObj.Documents[docIndex].documentStatus = "";
                requirementObj.Documents[docIndex].date = currentDate;
            } else {
                    var reqObj = {};
                    reqObj.documentProofSubmitted = "";
					if(documentTypes[docIndex].documentOptions[0]){
						reqObj.documentName = 'Agent1_' + requirementObj.requirementType + '_' + partyIdentifier + "_" + documentTypes[docIndex] + '_' + docIndex + "_" + timeStamp;
                    }else{
                    	reqObj.documentName = 'Agent1_' + requirementObj.requirementType + '_' + partyIdentifier +"_"+"document"+docIndex+"_"+ timeStamp;
					}
					reqObj.documentStatus = "";
                    reqObj.documentOptions = docArray[docIndex].documentOptions;
                    reqObj.isMandatory = docArray[docIndex].isMandatory;
                    if (docArray[docIndex].isMandatory == "true") {
                        reqObj.isMandatory = true;
                    } else {
                        reqObj.isMandatory = false;
                    }
                    if (docArray[docIndex].documentOptions &&
                        docArray[docIndex].documentOptions.length == 1) {
                        reqObj.documentProofSubmitted = docArray[docIndex].documentOptions[0];
                    }
                    docObject.push(reqObj);
                requirementObj.Documents = docObject;
            }

            docIndex++;
      } while (docArray.length > docIndex);
        EappVariables.EappModel.Requirements.push(requirementObj);
    }
	
		if (requirementType == "Signature") {
           if (EappVariables.Signature.length > 0) {
               requirementObj.requirementName = EappVariables.Signature[0].requirementName;
           }
		   if(rootConfig.isDeviceMobile){
           EappService.saveTransaction(function() {
               EappService.saveRequirementFiles(filePath, index, requirementObj.requirementName, requirementType, description, documentTypes, successCallback);
           }, this.onSaveError, false);
		   }else{
			EappService.saveRequirementFiles(filePath, index, requirementObj.requirementName, requirementType, description, documentTypes, successCallback);
		   }
       } else {
           if(successCallback && successCallback !="" ) {
               successCallback();
           }
       }
};
	EappService.saveRequirementFiles = function(filePath, index, requirementName, requirementType, description, documentType, successCallback,fileName) {
		var requirementObject = CreateRequirementFile();
		if(fileName){
			var fileType = fileName.split(".")[1];
	    }
	    var fileName = "";
	    var currentDate = new Date();
	    var timeStamp = currentDate.getTime();
	    if (requirementType == "Signature") {
	        fileName = "Signature"
	    } else {
	        fileName = filePath.replace(/^.*[\\\/]/, '');
	        if(fileType== "pdf")
	    		fileName = "pdf";
	    }
	    requirementObject.RequirementFile.identifier = EappVariables.EappKeys.TransTrackingID;
	    if (typeof EappVariables.Signature !== "undefined" && EappVariables.Signature &&
	        EappVariables.Signature.length > 0 && documentType == "Signature") {
	        requirementObject.RequirementFile.requirementName = EappVariables.Signature[0].requirementName;
	        requirementObject.RequirementFile.documentName = EappVariables.Signature[0].documentName;
	        requirementObject.RequirementFile.fileName = EappVariables.Signature[0].fileName;
	    } else {
	        for (var i = 0; i < EappVariables.EappModel.Requirements.length; i++) {
	            if (EappVariables.EappModel.Requirements[i].requirementName == requirementName) {
	                requirementObject.RequirementFile.requirementName = EappVariables.EappModel.Requirements[i].requirementName
	                for (var j = 0; j < EappVariables.EappModel.Requirements[i].Documents.length; j++) {
	                    if (EappVariables.EappModel.Requirements[i].Documents[j].documentProofSubmitted == documentType) {
						    if(EappVariables.EappModel.Requirements[i].Documents[j].documentName ==""){
							 EappVariables.EappModel.Requirements[i].Documents[j].documentName = 'Agent1_' + requirementType +"_"+documentType+"_"+ timeStamp;
							
							}
	                        requirementObject.RequirementFile.documentName = EappVariables.EappModel.Requirements[i].Documents[j].documentName;
	                    }
	                }
	            }
	        }
			
	        requirementObject.RequirementFile.fileName = "Agent1" + "_" + requirementType + "_" + documentType + "_" +  fileName + "_" + timeStamp + '.jpg';
	        if(fileType== "pdf"){
	        	requirementObject.RequirementFile.fileName = "Agent1" + "_" + requirementType + "_" + documentType + "_" + fileName + "_" + timeStamp + '.pdf';
			}
	    }
	    if(EappVariables.EappKeys.Key7 == "Pending"){
	    	requirementObject.RequirementFile.status = "Resubmitted";
	    } else {
	    	requirementObject.RequirementFile.status = "Saved";
	    }
	    if (!(rootConfig.isDeviceMobile)) {
	        requirementObject.RequirementFile.contentType = "base64";
	    }
	    requirementObject.RequirementFile.base64string = filePath;
	    EappVariables.DocumentPath = requirementObject.RequirementFile.base64string;
	    requirementObject.RequirementFile.description = description;
	    if (requirementType !== "Signature") {
	    	if(EappVariables.Requirement) {
	    		EappVariables.Requirement.push(requirementObject.RequirementFile);
	    		if(!(rootConfig.isOfflineDesktop) && !(rootConfig.isDeviceMobile)){
                    //var pages=[];
                    var obj={};
                     for(var i=0;i<EappVariables.EappModel.Requirements.length;i++){
                       if(EappVariables.EappModel.Requirements[i].requirementName==requirementObject.RequirementFile.requirementName){
                          obj.fileName=requirementObject.RequirementFile.fileName;
                          obj.documentStatus=requirementObject.RequirementFile.status;
                           for(var j=0;j<EappVariables.EappModel.Requirements[i].Documents.length;j++){
                                if(EappVariables.EappModel.Requirements[i].Documents[j].documentName==requirementObject.RequirementFile.documentName){
                                    if(EappVariables.EappModel.Requirements[i].Documents[j].pages && EappVariables.EappModel.Requirements[i].Documents[j].pages.length>=0){
                                        EappVariables.EappModel.Requirements[i].Documents[j].pages.push(obj);
                                    }else{
                                        EappVariables.EappModel.Requirements[i].Documents[j].pages=[];
                                        EappVariables.EappModel.Requirements[i].Documents[j].pages.push(obj);
                                        if(EappVariables.EappKeys.Key7 == "Pending"){
											 EappVariables.EappModel.Requirements[i].Documents[j].documentStatus = "Resubmitted";
										}
                                    }
                                     
                                }
                            }
                        }
                     }
                 }
	    	} else {
	            EappVariables.Requirement =[];
	            EappVariables.Requirement.push(requirementObject.RequirementFile);
	        }
	    }
	    
	    //Modified for Generali. Changing key values for Resubmission
		if(EappVariables.EappKeys.Key7 == "Pending"){
			EappVariables.EappKeys.Key7 = "PendingUploaded";
		}
		
		  EappService.saveTransaction(function() {
	    if ((rootConfig.isDeviceMobile)) {
		
             DataService.saveRequirementFiles(requirementObject.RequirementFile, function() {
	            successCallback();
	        }, function(e){
			});
	    } else {
	       // PersistenceMapping.clearTransactionKeys();
	       // EappVariables.EappKeys.Key15 = EappVariables.getStatusOptionsEapp()[1].status;
	        EappService.mapKeysforPersistence();
	        var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
			//transactionObj.Key15="New"
	        DataService.saveRequirementFiles(transactionObj, function() {
	            successCallback();
	        }, function(e){
				
			});
	    }}, this.onSaveError, false);
	   
	};
	
EappService.getSyncStatus = function(requirementObj, transtrackingId, successCallback){
    
    DataService.getStatusForRequirement(requirementObj, transtrackingId, function(data) {
        var obj={};
        if (data[0].totalCount == 0) {
            obj.noData = true;
            obj.unsyncedStatus = false;
            obj.syncedStatus = false
            successCallback(obj);
        } else {
            if(data[0].unSyncedCount == 0){
                obj.noData = false;
                obj.unsyncedStatus=false;
                obj.syncedStatus = true;
            successCallback(obj);
            }else{
                obj.noData = false;
                obj.unsyncedStatus=true;
                obj.syncedStatus = false
            successCallback(obj);
            }
            
        }
    }, function(data){
                    //error callback
    });
};
    
EappService.getuploadStatus = function(requirementObj, transtrackingId, successCallback){
	
			if(!(rootConfig.isOfflineDesktop) && !(rootConfig.isDeviceMobile)){        
				var mandatoryDocCount = 0;
			    var mandatoryDocList = [];
			    for(var x=0 ; x< requirementObj.Documents.length; x++){
			        if(requirementObj.Documents[x].isMandatory) {
			            mandatoryDocCount += 1;
			            mandatoryDocList.push(requirementObj.Documents[x].documentName);
			        }
			    }
			
			    if (mandatoryDocCount === 0) {
			        for(var i=0;i<requirementObj.Documents.length;i++){
			            if(requirementObj.Documents[i].pages){
			                if(requirementObj.Documents[i].pages.length>0){
			                    var docCount =0;
			                    for(var j=0;j<requirementObj.Documents[i].pages.length;j++){		
			                        if(requirementObj.Documents[i].pages[j].documentStatus=="Saved"){
			                            docCount++;  
			                        }
			                    }
			                }else if(requirementObj.Documents[i].documentName=="GAO" && requirementObj.Documents[i].documentUploadStatus==true) {
					var docCount =0;
			            	docCount++;
			            }
			            }
			        }
			        if(docCount>0){
			            successCallback(true);
			        }else{
			            successCallback(false);
			        }
			    } else if (mandatoryDocCount > 0) {
			        var manDocUploadCount =0;
			         for(var i=0;i<requirementObj.Documents.length;i++) {
                        for(var k=0; k<mandatoryDocList.length;k++) {
                            if(requirementObj.Documents[i].documentName==mandatoryDocList[k]) {
                                if((requirementObj.Documents[i].pages) && (requirementObj.Documents[i].pages.length >0)) {
                                    for(var j=0; j<requirementObj.Documents[i].pages.length; j++) {
                                        if(requirementObj.Documents[i].pages[j].documentStatus=="Saved") {
                                            manDocUploadCount ++;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

			        if(manDocUploadCount==mandatoryDocCount) {
			            successCallback(true);
			        } else {
			            successCallback(false);
			        }
			    }
			}
			else{
				var mandatoryDocCount = 0;
				var mandatoryDocList = [];
				    for(var i=0 ; i< requirementObj.Documents.length; i++){
				        if(requirementObj.Documents[i].isMandatory) {
				            mandatoryDocCount += 1;
				            mandatoryDocList.push(requirementObj.Documents[i].documentName);
				        }
				    }
				   
				    if (mandatoryDocCount === 0) {
						DataService.getRequirementsForTransId(requirementObj, transtrackingId, function(data) {
				            if (data[0].count > 0) {
				                successCallback(true);
				            } else {
				                successCallback(false);
				            }
				        }, function(data){
				            //error callback
				        });
				    } else if (mandatoryDocCount > 0) {
				        DataService.getRequirementsUploadedForTransId(mandatoryDocList, transtrackingId, function(data) {
				            if (data[0].count == mandatoryDocCount) {
				               successCallback(true);
				            } else {
				               successCallback(false);
				            }
				        }, function(data){
				                //error callback
				        });
				    }
			}

};

EappService.getSignature = function(transtrackingId,Signature,successCallback) {
 DataService.getSignature(transtrackingId,Signature,function(sigdata){
  	successCallback(sigdata);
  });
};

/*EappService.deleteRequirementFiles = function(docData, status, successCallback, errorCallback) {
    var transactionObject = {};
    if (docData) {
        transactionObject.status = status;
        DataService.updateRequirementFiles(transactionObject, successCallback, errorCallback);
    }
};*/

EappService.convertImgToBase64=function(url, callback, outputFormat){
	if((rootConfig.isOfflineDesktop && (rootConfig.isDeviceMobile)) || (!rootConfig.isOfflineDesktop && !(rootConfig.isDeviceMobile))){
		 var img = new Image();
		img.crossOrigin = 'Anonymous';
		img.onload = function(){
			var canvas = document.createElement('CANVAS'),
			ctx = canvas.getContext('2d'), dataURL;
			canvas.height = this.height;
			canvas.width = this.width;
			ctx.drawImage(this, 0, 0);
			dataURL = canvas.toDataURL(outputFormat);
			callback(dataURL);
			canvas = null; 
		};
		img.src = url;
	} else{
		var fileName = url.replace(/^.*[\\\/]/, '');
        DocumentService.readFile(fileName, function(dataURL){
            var base64String = "data:image/jpeg;base64," + dataURL;
            callback(base64String);
        }, function(error){
           
        });
	}
};

EappService.saveSignatureAsImage =function(signdata, successCallback){
 	DocumentService.saveSignature(signdata,function(sigPath){
 		successCallback(sigPath);
 	});
};

EappService.runRequirementRule = function(transactionObj, successCallback, requirementRuleError) {
		DataService.runRequirementRule(transactionObj, function(data){
			EappService.requirementRuleSuccess(data, successCallback, requirementRuleError);
		}, requirementRuleError);
};

EappService.requirementRuleSuccess = function(data, successCallback, errorCallback) {
    var currentDate = new Date();
    var timeStamp = currentDate.getTime();
    var filePath = "";
    var documentType = "";
    var onSuccess; 
    if (data.Requirements) {
        for (var i = 0; i < data.Requirements.length; i++) {
            if (i == data.Requirements.length-1) {
                onSuccess = successCallback;
            }
            var requirementObj = Requirement();
            if(data.Requirements[i].document) {
            documentType =data.Requirements[i].document;
            }
            EappService.saveRequirement(filePath, data.Requirements[i].requirementType, data.Requirements[i].partyIdentifier,
                data.Requirements[i].ismandatory, data.Requirements[i].document, documentType, onSuccess, errorCallback);
        }
    } else {
		successCallback();
	}
    return data;

};
	
EappService.getStatusForDocuments = function(requirementObj,transtrackingId, successCallback) {
	var transactionObj ={};
   transactionObj.documentList = requirementObj.Documents;
   transactionObj.transtrackingId=transtrackingId;
   DataService.getStatusForDocuments(transactionObj,
        function(docData) {
            successCallback(docData);
        });
};

EappService.getDocumentsForRequirement = function(requirementObj, documentSelected, transtrackingId, successCallback) {
    var transactionObj = {};
    transactionObj.requirementName = requirementObj.requirementName;
    for (var i = 0; i < requirementObj.Documents.length; i++) {
        if (requirementObj.Documents[i].documentProofSubmitted == documentSelected) {
            transactionObj.documentName = requirementObj.Documents[i].documentName;
        }
    }
    transactionObj.transtrackingId = transtrackingId;
    if ((rootConfig.isDeviceMobile)) {
        DataService.getDocumentsForRequirement(transactionObj,
            function(docData) {
                EappVariables.Requirement = docData;
                successCallback();
            });
    } else {
        var requirementObject = CreateRequirementFile();
        requirementObject.RequirementFile.identifier = transtrackingId;
        requirementObject.RequirementFile.requirementName = transactionObj.requirementName;
        requirementObject.RequirementFile.documentName = transactionObj.documentName;
       // PersistenceMapping.clearTransactionKeys();
       // EappVariables.EappKeys.Key15 = EappVariables.getStatusOptionsEapp()[0].status;
        EappService.mapKeysforPersistence();
        var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
        DataService.getDocumentsForRequirement(transactionObj, function(docData) {
            EappVariables.Requirement = docData;
            successCallback();
        });
    }

};

EappService.updateRequirementFiles = function(
		requirmentObj, successCallback, errorCallback,
		deleteTypeFlag) {
 var fileName="";
 var tableName="RequirementFiles";
 if(requirmentObj[0]){
   requirmentObj = requirmentObj[0];
 
 }
 if(requirmentObj.base64string){
		fileName = requirmentObj.base64string.replace(
				/^.*[\\\/]/, '');
	}
 
	if (rootConfig.isDeviceMobile) {
		/*var whereClause = "identifier IN (" + "'"
				+ EappVariables.EappKeys.TransTrackingID
				+ "'" + ") and requirementName IN(" + "'"
				+ requirmentObj.requirementName + "'"
				+ ")and documentName IN (" + "'"
				+ requirmentObj.documentName + "'"
				+ ") and fileName IN(" + "'"
				+ requirmentObj.fileName + "'" + ")";*/
		var fileObject = {};
		fileObject.fileName = fileName;
		fileObject.filePath = "Uploads";
		fileObject.isErrorIfFileNotExists = false;
		window.plugins.LEFileUtils
				.deleteFile(
						[ fileObject ],
						function() {
							requirmentObj.status = "Deleted";
							EappService
									.saveTransaction(
											function() {
												if (deleteTypeFlag == "ImageDelete") {
													DataService
															.saveRequirementFiles(
																	requirmentObj,
																	function() {
	    		successCallback();
																	},
																	errorCallback);
												} else if (deleteTypeFlag == "RowDelete") {
													DataService.deleteRequirementFiles(tableName,EappVariables.EappKeys.TransTrackingID,
															requirmentObj.fileName,
															function() {
														successCallback();
													},function() {
 														errorCallback();
 													});
 	 											}
 											},
											function() {
												console
														.log("Transaction Saved failed"
																+ error);
											}, false);

						}, errorCallback);
	} else {
		var RequirementFilesObj = {};
	     PersistenceMapping.clearTransactionKeys();
	     EappService.mapKeysforPersistence();
		RequirementFilesObj.RequirementFile = requirmentObj;
		var transactionObj = PersistenceMapping
				.mapScopeToPersistence(RequirementFilesObj);
		DataService.saveRequirementFiles(transactionObj,
				function() {
	         successCallback();
	     }, errorCallback);
	     
	 }
 };
 
 EappService.deleteRequirementInWeb = function(requirmentObj,pageToDelete,documents, successCallback, errorCallback) {
	 var RequirementFileToDelete={};
	 var requirementObject={};
	  PersistenceMapping.clearTransactionKeys();
	     EappService.mapKeysforPersistence();
		requirementObject.requirementName =requirmentObj.requirementName;
		requirementObject.fileName=pageToDelete.fileName;
		requirementObject.documentName=documents.documentName;
		requirementObject.contentType=pageToDelete.contentType;
		requirementObject.documentStatus="Saved";
		requirementObject.base64String="";
		requirementObject.status="Deleted";
		requirementObject.identifier=requirmentObj.partyIdentifier;
		RequirementFileToDelete.RequirementFile = requirementObject;
		var transactionObj = PersistenceMapping.mapScopeToPersistence(RequirementFileToDelete);
		   DataService.saveRequirementFiles(transactionObj, function(){
	         successCallback();
	     }, errorCallback);
	 
	 };

 EappService.updateCommentsForRequirementFiles= function(requirmentObj, successCallback, errorCallback){
		 DataService.saveRequirementFiles(requirmentObj, function(){
    		successCallback();
		}, errorCallback);
	
 };
 
EappService.mapDocScopePersistence = function(document) {
			var docObject = {};
			var newDate = new Date();
			var createdDate = Number(newDate.getMonth() + 1)
										+ "-"
										+ newDate.getDate()
										+ "-"
										+ newDate.getFullYear();
			docObject.id = document !== null
										&& document.id !== null ? document.id
										: null;
			docObject.parentId = ($rootScope.transactionId !== null
										&& $rootScope.transactionId !== 0 && $rootScope.transactionId !== "-") ? 
										$rootScope.transactionId
										: null;
			docObject.date = createdDate;
				if (document != null) {
					docObject.fileObj = document;
				}			
			return docObject;
	};

EappService.lookUpDocuments = function(successCallback,cacheable) {
	if (!cacheable){
					cacheable = false;
				}
    var options = {};
    options.type = "DOCUMENT_TYPE_TILE";
    DataLookupService.getLookUpData(options, function(data) {
        if (data) {
            EappVariables.DocumentType = data;
            successCallback();
        }
    }, function(errorData) {
       
    },cacheable);

};

    EappService.saveTransaction = function(saveSuccess, saveError, isAutoSave) {
		this.onSaveSuccess = function(data) {
			if ((rootConfig.isDeviceMobile)) {
				EappVariables.EappKeys.Id = data;
			} else {
				EappVariables.EappKeys.Key4 = data.Key4;
			}
			if (saveSuccess) {
				saveSuccess(data);
			}
		};
		this.onSaveError = function(data) {
			if (saveError) {
				saveError(data);
			}
		};
		PersistenceMapping.clearTransactionKeys();		
		EappService.mapKeysforPersistence();		
		var transactionObj = PersistenceMapping.mapScopeToPersistence(EappVariables.EappModel);
		var source =(rootConfig.isDeviceMobile)=="false"? "TABLET": "DESKTOP";
		transactionObj.TransactionData.source = source;
		if(EappVariables.EappKeys.Id && EappVariables.EappKeys.Id !=0 && EappVariables.EappKeys.Id != ""){
			transactionObj.Id = EappVariables.EappKeys.Id;
		} 
		
		DataService.saveTransactions(transactionObj, this.onSaveSuccess, this.onSaveError);
	};
	
	
	EappService.saveRequirementOnly = function(requirementToSaveForWeb, saveSuccess, saveError, isAutoSave) {
		this.onSaveSuccess = function(data) {
				EappVariables.EappKeys.Key4 = data.Key4;			
					if (saveSuccess) {
						saveSuccess(data);
					}
		};
		this.onSaveError = function(data) {
			if (saveError) {
				saveError(data);
			}
		};
		PersistenceMapping.clearTransactionKeys();		
		EappService.mapKeysforPersistence();
		var transactionObj;
		var Requirement = [];
		RequirementFile={};
		Requirement.push(requirementToSaveForWeb);
		//Requirement =EappVariables.EappModel.Requirements
		RequirementFile.Requirements =Requirement;	
		

		transactionObj = PersistenceMapping.mapScopeToPersistence(RequirementFile);	
		//transactionObj = PersistenceMapping.mapScopeToPersistence(EappVariables.EappModel.Requirements);
		//transactionObj.TransactionData = [];
		//transactionObj.TransactionData.Requirements = EappVariables.EappModel.Requirements;
		if(EappVariables.EappKeys.Id && EappVariables.EappKeys.Id !=0 && EappVariables.EappKeys.Id != ""){
			transactionObj.Id = EappVariables.EappKeys.Id;
		} 
		
		DocumentService.saveRequirementOnly(transactionObj, this.onSaveSuccess, this.onSaveError);
	};
	
	EappService.getEapp = function(getEappSuccess, getEappError) {
		this.onListingSuccess = function(data) {
			PersistenceMapping.mapPersistenceToScope(data);
			EappVariables.EappKeys.TransTrackingID=data[0].TransTrackingID;
			EappVariables.EappKeys.creationDate = data[0].creationDate;
			EappVariables.EappKeys.modifiedDate = data[0].modifiedDate;
			EappService.mapKeysfromPersistence();
			if (getEappSuccess) {
				getEappSuccess();
			}
		};
		this.onListingError = getEappError;
		PersistenceMapping.clearTransactionKeys();
		PersistenceMapping.Key4 = EappVariables.EappKeys.Key4;
		PersistenceMapping.Key24 = EappVariables.EappKeys.Key24;
		PersistenceMapping.Id = EappVariables.EappKeys.Id;
		PersistenceMapping.TransTrackingID = EappVariables.EappKeys.TransTrackingID;
		PersistenceMapping.Type = "eApp";
		var transactionObj = PersistenceMapping.mapScopeToPersistence();
		DataService.getListingDetail(transactionObj, this.onListingSuccess, this.onListingError);
	};

	EappService.runSTPRule = function(stpRuleSuccess, stpRuleError) {
		PersistenceMapping.clearTransactionKeys();
		EappService.mapKeysforPersistence();
		var transactionObj = PersistenceMapping.mapScopeToPersistence(EappVariables.EappModel);
		if ((rootConfig.isDeviceMobile)) {
		    transactionObj.TransactionData = JSON.parse(transactionObj.TransactionData);
		}
		DataService.runSTPRule(transactionObj, stpRuleSuccess, stpRuleError);
	};

	EappService.downloadApplicationPdf = function(successCallback, errorCallback) {
		if (EappVariables.EappModel.Product.templates && EappVariables.EappModel.Product.templates.eAppPdfTemplate) {
			var eAppTemplateId = EappVariables.EappModel.Product.templates.eAppPdfTemplate;
		}
		if (!eAppTemplateId) {
			var eAppTemplateId = 0;
		}
		PersistenceMapping.clearTransactionKeys();
		EappService.mapKeysforPersistence();
		var transactionObj = PersistenceMapping.mapScopeToPersistence(EappVariables.EappModel);
		if ((rootConfig.isDeviceMobile)) {
			var objsToSync = [];
			objsToSync.push(EappVariables.EappKeys.Id);
			var syncConfig = [{
				"module" : "eApp",
				"operation" : "syncTo",
				"selectedIds" : objsToSync
			}];
			SyncService.initialize(syncConfig, function() {
				DataService.downloadPDF(transactionObj, eAppTemplateId, successCallback, errorCallback);
			});
			SyncService.runSyncThread();
		} else {
			DataService.downloadPDF(transactionObj, eAppTemplateId, successCallback, errorCallback);
		}
	};

 EappService.getAllRequirements = function(transTrackingId, successCallback) {
		DataService.getRequirementsForTransaction(
            transTrackingId,
            function(docData) {
                successCallback(docData);
            },
            function(error) {
				
            });
  };
  EappService.getRequirementFilesToDelete= function(transtrackingId,documentName,successCallback,errorCallback){
  
  DataService.getRequirementFilesToDelete(transtrackingId,documentName,function(data){
  successCallback(data);
  
  },errorCallback);
  
  }

	
 EappService.requirementRuleError = function(data) {
        $rootScope.lePopupCtrl.showError(translateMessages($translate, "ruleFailed"));
};

	var successCallback = function() {
		
    };
    var errorCallback = function(err) {
		
    };
	return EappService;
}]);
