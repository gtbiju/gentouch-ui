
angular.module('lifeEngage.GLI_EappService', []).factory("GLI_EappService",['$http', '$location','$rootScope', '$translate', 'PersistenceMapping', 'RuleService', 'GLI_RuleService', 'globalService', 'EappVariables', 'DataService', 'UtilityService','DataLookupService','GLI_globalService','EappService', 'DocumentService', 'SyncService','RequirementService','GLI_EvaluateService','UserDetailsService','GLI_EappVariables',
		function($http, $location,$rootScope, $translate, PersistenceMapping, RuleService, GLI_RuleService, globalService, EappVariables, DataService, UtilityService,DataLookupService,GLI_globalService,EappService, DocumentService, SyncService,RequirementService,GLI_EvaluateService,UserDetailsService,GLI_EappVariables) {
			var premiumFrequency;
			EappService.GLIprepopulateData=function(sucesscallback)
					{
						if(EappVariables.EappKeys.Key3 !=0 || EappVariables.EappKeys.Key3 =="")
						{
					EappVariables.EappModel.Insured = globalService.getInsuredEapp();
					// Temporary Fix. Need To Change
					EappVariables.prepopulatedInsuredData.partyId = EappVariables.EappModel.Insured.partyId;
					EappVariables.EappModel.Insured = angular.copy(EappVariables.prepopulatedInsuredData);
					EappVariables.EappModel.Proposer = globalService.getPayer();
//					EappVariables.EappModel.Proposer.ContactDetails.mobileNumber1 = "";
					EappVariables.EappModel.Proposer.BasicDetails.countryofResidence = "";
//					EappVariables.EappModel.Proposer.BasicDetails.nationality = "";
					//EappVariables.EappModel.Payer = globalService.getPayer();
					EappVariables.EappModel.Product = globalService.getProductForEapp();
					/* FNA changes by LE Team for Bug 4166 starts */
					if (!EappVariables.fromFNA) {
						EappVariables.EappModel.Beneficiaries = globalService.getSelectdBeneficiariesGLI();
					} else {
						EappVariables.EappModel.Beneficiaries = globalService.getCPBeneficiaries();
					}					
					//clear the beneficiary relation with proposer.
					if(EappVariables.EappModel.Beneficiaries && !EappVariables.fromFNA){
						/* FNA changes by LE Team for Bug 4166 ends */
						for(beneficary in EappVariables.EappModel.Beneficiaries){
							if(EappVariables.EappModel.Beneficiaries[beneficary].CustomerRelationship){
								if(EappVariables.EappModel.Beneficiaries[beneficary].CustomerRelationship.relationWithInsured){
									EappVariables.EappModel.Beneficiaries[beneficary].CustomerRelationship.relationWithInsured="";
								}
							}
						}
					}
					EappVariables.EappModel.AgentInfo = EappVariables.agentData;
							
							LEDynamicUI.getUIJson(rootConfig.eAppConfigJson, false, function(eAppConfigJson) {		
								
						var questions = JSON.stringify(eAppConfigJson.questions);
						var proposerObj = JSON.stringify(EappVariables.EappModel.Proposer);
						EappVariables.EappModel.Insured.Questionnaire = eAppConfigJson.InsuredQuestionnaire;
						EappVariables.EappModel.Payer.Questionnaire = eAppConfigJson.PayerQuestionnaire;
                        EappVariables.prepopulatedProposerData.Questionnaire = EappVariables.EappModel.Payer.Questionnaire;
						var modifiedProposerObj = proposerObj.substring(0,proposerObj.length-1)+','+questions.substring(1,questions.length);
						EappVariables.EappModel.Proposer = JSON.parse(modifiedProposerObj);
						EappVariables.EappModel.Proposer.Questionnaire = eAppConfigJson.ProposerQuestionnaire;
								for(var i=0;i<EappVariables.EappModel.Beneficiaries.length;i++){
							EappVariables.EappModel.Beneficiaries[i].Questions = eAppConfigJson.questions.Questions;
								}
								
								EappService.mapDataFromIllustration(EappVariables.EappModel,sucesscallback);
					});
				}
			}
			EappService.mapDataFromIllustration = function(eappModel,sucesscallback){
				premiumFrequency=eappModel.Product.policyDetails.premiumFrequency;
				EappVariables.EappModel.Payment.RenewalPayment.paymentFrequency = premiumFrequency;
				LEDynamicUI.getUIJson(
											rootConfig.eAppConfigJson,
											false,
											function(eAppConfigJson) {// populating premium frequency
												for(var key in eAppConfigJson["paymentFrequency"]){
													if(key == premiumFrequency){
														EappVariables.EappModel.Payment.RenewalPayment.paymentFrequency =eAppConfigJson["paymentFrequency"][key];
														break;
													}
												}
												
												// populating relationship with additional insured	
												sucesscallback();
												});
			}
			EappService.getRelationShipWithPolicyHolder=function(){
				var RelationShipWithPH=[{
					"Male":[{
						"code":"Husband",
						"value" : translateMessages($translate,"eapp.husbands")
					},
					{
						"code":"Son",
						"value" : translateMessages($translate,"eapp.son")
					},
					{
						"code":"Father",
						"value" : translateMessages($translate,"eapp.father")
					},
					{
						"code":"Siblings",
						"value" : translateMessages($translate,"eapp.sibling")
					},
					{
						"code":"Others",
						"value" : translateMessages($translate,"eapp.others")
					}],
					"Female":[{
						"code":"Wife",
						"value" : translateMessages($translate,"eapp.wife")
					},
					{
						"code":"Daughter",
						"value" : translateMessages($translate,"eapp.daughter")
					},
					{
						"code":"Mother",
						"value" : translateMessages($translate,"eapp.mother")
					},
					{
						"code":"Siblings",
						"value" : translateMessages($translate,"eapp.sibling")
					},
					{
						"code":"Others",
						"value" : translateMessages($translate,"eapp.others")
					}]
				}]
				return JSON.parse(JSON.stringify(RelationShipWithPH));
			};
			
			EappService.getCopyDataFrom=function(){
				var CopyFrom=[
				    {
				    	"code":"Insured",
						"value" : translateMessages($translate,"Insured")
					},
					{
				    	"code":"PolicyHolder",
						"value" : translateMessages($translate,"policyHolder")
					},
					{
						"code":"Additional Insured 1",
						"value" : translateMessages($translate,"illustrator.additionalInsuredDetails1")
					},
					{
						"code":"Additional Insured 2",
						"value" : translateMessages($translate,"illustrator.additionalInsuredDetails2")
					},
					{
						"code":"Additional Insured 3",
						"value" : translateMessages($translate,"illustrator.additionalInsuredDetails3")
					},
					{
						"code":"Additional Insured 4",
						"value" : translateMessages($translate,"illustrator.additionalInsuredDetails4")
					}]
				return JSON.parse(JSON.stringify(CopyFrom));
			};
			
			EappService.mapKeysforPersistence = function() {
				// PersistenceMapping.dataIdentifyFlag=false;
				PersistenceMapping.Key1 = EappVariables.EappKeys.Key1;
				if(EappVariables.EappKeys.Key2 == ""){
					EappVariables.EappKeys.Key2 = 0;
					PersistenceMapping.Key2 = EappVariables.EappKeys.Key2;
				}
				else {
					PersistenceMapping.Key2 = EappVariables.EappKeys.Key2;
				}
				PersistenceMapping.Key3 = EappVariables.EappKeys.Key3;
                PersistenceMapping.Key29 = EappVariables.EappModel.Product.policyDetails.premiumPeriod;
				PersistenceMapping.Key4 = EappVariables.EappKeys.Key4;
				PersistenceMapping.Key5 = EappVariables.EappKeys.Key5;
				if (EappVariables.EappModel) {
					if(EappVariables.EappModel.Insured){
						var fullName = EappVariables.EappModel.Insured.BasicDetails.firstName;
						if (EappVariables.EappModel.Insured.BasicDetails.lastName)
							fullName = fullName + ' ' + EappVariables.EappModel.Insured.BasicDetails.lastName;
						PersistenceMapping.Key6 = fullName;
						PersistenceMapping.Key8 = (EappVariables.EappModel.Insured.ContactDetails.mobileNumber1) ? EappVariables.EappModel.Insured.ContactDetails.mobileNumber1 : "";
					}
					if(EappVariables.EappModel.Product){
						PersistenceMapping.Key19 = EappVariables.EappModel.Product.ProductDetails.productName;
                     }
				}
				PersistenceMapping.Key7 =EappVariables.EappKeys.Key7==rootConfig.EappStausPendingSubmission?"":EappVariables.EappKeys.Key7;
                PersistenceMapping.Key14 = UtilityService.getFormattedDate();
				PersistenceMapping.Key10 = "FullDetails";
				PersistenceMapping.Key12 = "";
				if ((rootConfig.isDeviceMobile)) {
					if (EappVariables.EappKeys.Id == null || EappVariables.EappKeys.Id == 0) {
						EappVariables.EappKeys.Key13 = UtilityService.getFormattedDate();
						PersistenceMapping.Key13 = EappVariables.EappKeys.Key13;
					}else{
                        
                        
						PersistenceMapping.Key13 =  EappVariables.EappKeys.Key13;
//                        PersistenceMapping.Key13 = UtilityService.getFormattedDate();
                    }
				} else {
					PersistenceMapping.Key13 = UtilityService.getFormattedDate();
				}
				if (EappVariables.EappKeys.TransTrackingID == null || EappVariables.EappKeys.TransTrackingID == 0) {
					PersistenceMapping.TransTrackingID = UtilityService.getTransTrackingID();
					EappVariables.EappKeys.TransTrackingID = PersistenceMapping.TransTrackingID;
				} else {
					PersistenceMapping.TransTrackingID = EappVariables.EappKeys.TransTrackingID;
				}
				PersistenceMapping.Key15 = EappVariables.EappKeys.Key15 != null ? EappVariables.EappKeys.Key15 : "Pending Submission";
				PersistenceMapping.Key16 = EappVariables.EappKeys.Key16 != null ? EappVariables.EappKeys.Key16 : "";
				PersistenceMapping.Key18 = EappVariables.EappKeys.Key18 != null ? EappVariables.EappKeys.Key18 : "";
				PersistenceMapping.Key21 = EappVariables.EappKeys.Key21 != null ? EappVariables.EappKeys.Key21 : "";
				PersistenceMapping.Key24 = EappVariables.EappKeys.Key24 != null ? EappVariables.EappKeys.Key24 : "";
				PersistenceMapping.Key25 = EappVariables.EappKeys.Key25 != null ? EappVariables.EappKeys.Key25 : "";
				PersistenceMapping.Key26 = EappVariables.EappKeys.Key26 != null ? EappVariables.EappKeys.Key26 : "";
				PersistenceMapping.Key34 = EappVariables.EappKeys.Key34 != null ? EappVariables.EappKeys.Key34 : "";
				PersistenceMapping.Type = "eApp";
				PersistenceMapping.Key22 = EappVariables.eAppProceedingDate;


				if(EappVariables.EappModel.Product){
					if(EappVariables.EappModel.Product.premiumSummary.totalPremium != undefined){
						PersistenceMapping.Key23 = EappVariables.EappModel.Product.premiumSummary.totalPremium;
					}else{
						PersistenceMapping.Key23 = "";
					}
                    
                    if(EappVariables.EappModel.Product.premiumSummary.sumInsured != undefined){
						PersistenceMapping.Key27 = EappVariables.EappModel.Product.premiumSummary.sumInsured;
					}else{
						PersistenceMapping.Key27 = "";
					}
				}
					if(EappVariables.EappModel){
                    if(EappVariables.EappModel.ApplicationNo ==undefined || EappVariables.EappModel.ApplicationNo == ""){
                        EappVariables.EappModel.ApplicationNo = 0;
                    }
					PersistenceMapping.Key21 = EappVariables.EappModel.ApplicationNo;
                    
				}
			};	
			EappService.requirementRuleSuccess = function(data, successCallback, errorCallback) {
				
				var currentDate = new Date();
				var timeStamp = currentDate.getTime();
				var isGaoAvailable=false;
				var filePath = "";
				var documentType = "";
				var onSuccess; 
				var requirementList = EappVariables.EappModel.Requirements;
				var GAORequirement = requirementList.filter(function(item){
					return item.requirementType == "GAO";
				});
				var signatureExists = EappVariables.EappModel.Requirements.filter(function(item){
					return item.requirementType == "Signature";
				})
				if(signatureExists && signatureExists.length >0){
					EappVariables.EappModel.Requirements = [];
					EappVariables.EappModel.Requirements = angular.copy(signatureExists);
				}	

				 if(GAORequirement && GAORequirement.length >0)
				 {
				    				
					for(var val=0;val<EappVariables.EappModel.Requirements.length;val++)
					{
						if(EappVariables.EappModel.Requirements[val].requirementType=="GAO")
						{
							isGaoAvailable=true;
						}
					}
					if(!isGaoAvailable)
					{
					EappVariables.EappModel.Requirements.push(angular.copy(GAORequirement[0]));
					}
				 }	

				if (data.Requirements) {
					for (var i = 0; i < data.Requirements.length; i++) {
						if (i == data.Requirements.length-1) {
							onSuccess = successCallback;
						}
						var requirementObj = Requirement();
						if(data.Requirements[i].Documents) {
							documentType =data.Requirements[i].Documents;
						}
						EappService.saveRequirement(filePath, data.Requirements[i].requirementType, data.Requirements[i].partyIdentifier,
							data.Requirements[i].isMandatory, data.Requirements[i].Documents, documentType, data.Requirements[i].SerialNumber, onSuccess, errorCallback);
					}
				} else {
					successCallback();
				}
				return data;

			};
	
			EappService.convertToCanonicalInput = function(hierarchicalObj) {
				var canonicalObj = {};
				if (hierarchicalObj.Insured !== null && typeof hierarchicalObj.Insured != "undefined") {
					var insuredGender =  getGender(hierarchicalObj.Insured.BasicDetails.gender);
					
					var insuredAgeDays;
					
					if((hierarchicalObj.Insured.BasicDetails) && (hierarchicalObj.Insured.BasicDetails.age)) {
					     if(hierarchicalObj.Insured.BasicDetails.age <= 1) {
					        insuredAgeDays = getAgeDays(hierarchicalObj.Insured.BasicDetails.dob);	
					     }else{
						 insuredAgeDays = getAgeDays(hierarchicalObj.Insured.BasicDetails.dob);
						 }
					}					
					else{
						insuredAgeDays = getAgeDays(hierarchicalObj.Insured.BasicDetails.dob);
					}
					var insuredObj = {
						"name" : hierarchicalObj.Insured.BasicDetails.firstName,
						"age" : insuredAgeDays,
						"medicalPlanChosen" : "",
						"healthUnitsChosen" : "",
						"partyIdentifier":"Ins",
						"sex" : insuredGender
					};
				}
				var productObj = {
						"sumAssured" : "",
				};
				if (hierarchicalObj.Product.policyDetails !== null && typeof hierarchicalObj.Product.policyDetails != "undefined") {
					if(hierarchicalObj.Product.policyDetails.sumInsured){
						productObj.sumAssured = hierarchicalObj.Product.policyDetails.sumInsured;
					}
					else{
						productObj.sumAssured = hierarchicalObj.Product.premiumSummary.sumInsured;
					}
					
				}
				
				if (hierarchicalObj.Proposer !== null && typeof hierarchicalObj.Proposer != "undefined") {
					var proposerAgeDays;
					var proposerGender = getGender(hierarchicalObj.Proposer.BasicDetails.gender);
					if((hierarchicalObj.Proposer.BasicDetails) && (hierarchicalObj.Proposer.BasicDetails.age) && (hierarchicalObj.Proposer.BasicDetails.age <=1)){
						proposerAgeDays = getAgeDays(hierarchicalObj.Proposer.BasicDetails.dob);
					}else{
						proposerAgeDays = getAgeDays(hierarchicalObj.Proposer.BasicDetails.dob);
					}
					var proposerObj = {
						"name" : hierarchicalObj.Proposer.BasicDetails.firstName,
						"age" : proposerAgeDays,
						"sex" : proposerGender
					};
				}
				var additionalInsured0 = {
						"name" : "",
						"age" : "",
						"medicalPlanChosen" : "",
						"partyIdentifier":"",
						"healthUnitsChosen" : "",
						"TermLifeSA":""
				};
				var additionalInsured1 = {
						"name" : "",
						"age" : "",
						"medicalPlanChosen" : "",
						"partyIdentifier":"",
						"healthUnitsChosen" : "",
						"TermLifeSA":""
				};
				var additionalInsured2 = {
						"name" : "",
						"age" : "",
						"medicalPlanChosen" : "",
						"partyIdentifier":"",
						"healthUnitsChosen" : "",
						"TermLifeSA":""
				};
				var additionalInsured3 = {
						"name" : "",
						"age" : "",
						"medicalPlanChosen" : "",
						"partyIdentifier":"",
						"healthUnitsChosen" : "",
						"TermLifeSA":""
				};
				
				var riderArray = hierarchicalObj.Product.RiderDetails;
				for ( var i = 0; i < riderArray.length; i++) {
					if(riderArray[i].shortName=='HealthPlan'){
						if(riderArray[i].insured =='Maininsured'){
							insuredObj.healthUnitsChosen = riderArray[i].healthUnitsOrPlanType;
						}
					}
					else if(riderArray[i].shortName=='MedicalPlan'){
						if(riderArray[i].insured =='Maininsured'){
							if((riderArray[i].healthUnitsOrPlanType)){
								insuredObj.medicalPlanChosen =(riderArray[i].healthUnitsOrPlanType).substring((riderArray[i].healthUnitsOrPlanType).length - 1);
							}
						}
					}
					
					else if(riderArray[i].shortName=='AdditionalInsuredTermLifePlan'){
						if(riderArray[i].insured =='Maininsured'){
							insuredObj.TermLifeSA = riderArray[i].sumInsured;
						}
					}
				}
				canonicalObj.InsuredDetails = insuredObj;
				canonicalObj.ProductDetails = productObj;
				canonicalObj.ProposerDetails = proposerObj;
				return canonicalObj;
			};
			
			EappService.convertToCanonicalInputOccupation = function(hierarchicalObj,data){
				var canonicalObj = {};
				var proposerObj = {};
				var insuredObj = {};
				var additionalInsuredObj0 = {};
				var additionalInsuredObj1 = {};
				var additionalInsuredObj2 = {};
				var additionalInsuredObj3 = {};
				if(data.person && data.person=='ProposerDetails'){
					proposerObj = {
						"partyIdentifier" : "proposer",
						"industry" : hierarchicalObj.Proposer.OccupationDetails.natureofWork,
						"occupation" : hierarchicalObj.Proposer.OccupationDetails.occupationCategory
					}
					
				}
				else if(data.person && data.person=='InsuredDetails'){
					insuredObj = {
							"partyIdentifier" : "ins",
							"industry" : hierarchicalObj.Insured.OccupationDetails.natureofWork,
							"occupation" : hierarchicalObj.Insured.OccupationDetails.occupationCategory
						}
				}
				
				canonicalObj.InsuredDetails = insuredObj;
				canonicalObj.ProposerDetails = proposerObj;
				return canonicalObj;
			};
			
			EappService.generateMedCategory = function($scope,successcallback,errorCallback) {
				var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.LifeEngageProduct);
				if ((rootConfig.isDeviceMobile)) {
					var transactionData = $.parseJSON(transactionObj.TransactionData)
					transactionObj.TransactionData = transactionData;
				} 
				transactionObj.TransactionData = EappService.convertToCanonicalInput(transactionObj.TransactionData);
				var fileName=rootConfig.medicalCategoryRule+'.js';				
				var databaseName=rootConfig.medicalCategoryRuleDB;
				var validationFunctionName=rootConfig.medicalCategoryRule;
				if(typeof transactionObj.TransactionData =="object"){
					var inputJson = '{ "data": {"OfflineDbName": "' + databaseName
				+ '", "RuleSetId": "1","RuleSetName": "'
				+ fileName + '", "FunctionName": "'
				+ validationFunctionName + '","variables": '
				+ JSON.stringify(transactionObj.TransactionData)
				+ '} }';
				}				
				
				if ((rootConfig.isDeviceMobile)) {
					LERuleEngine
					.execute(
							inputJson,
							function(output) {
								var result = $.parseJSON(output);
								successcallback(result);
							},function(){alert("Error in med category generation");$rootScope.showHideLoadingImage(false);});
				}
				else {
				    var medCategoryRuleInfo = {};
					medCategoryRuleInfo.offlineDB = rootConfig.medicalCategoryRuleDB;
					medCategoryRuleInfo.ruleName = rootConfig.medicalCategoryRule;
					medCategoryRuleInfo.ruleGroup = "13";
					
				    GLI_RuleService.runCustomRule(transactionObj, function(output) {
								successcallback(output);
							}, errorCallback, medCategoryRuleInfo, "generateMedCategory" );
				}
				
			};
			
			EappService.runOccupationRule = function($scope,data,successcallback) {
				var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.LifeEngageProduct);
				if ((rootConfig.isDeviceMobile)) {
					var transactionData = $.parseJSON(transactionObj.TransactionData)
					transactionObj.TransactionData = transactionData;
				} 
				transactionObj.TransactionData = EappService.convertToCanonicalInputOccupation(transactionObj.TransactionData,data);
				var fileName=rootConfig.occupationRule+'.js';
				var databaseName=rootConfig.medicalCategoryRuleDB;
				var validationFunctionName=rootConfig.occupationRule;
				if(typeof transactionObj.TransactionData =="object"){
					var inputJson = '{ "data": {"OfflineDbName": "' + databaseName
				+ '", "RuleSetId": "1","RuleSetName": "'
				+ fileName + '", "FunctionName": "'
				+ validationFunctionName + '","variables": '
				+ JSON.stringify(transactionObj.TransactionData)
				+ '} }';
				}
				LERuleEngine
				.execute(
						inputJson,
						function(output) {
							var result = $.parseJSON(output);						
							if(result[data.person] && result[data.person].questionnaire){
								successcallback(result[data.person].questionnaire);
							}
							else{
								successcallback(null);
							}
						},function(){alert("Error in running occupation rule");$rootScope.showHideLoadingImage(false);});
			};
			
			EappService.convertToCanonicalInsuranceEngagementLetter = function(hierarchicalObj,successCanonicalInput){
				var canonicalObj = {};
				var productObj = {};
				var insuredObj = {};
				var beneficiary0 = {
						"partyIdentifier" : "",
						"relationWithInsured":""
						
				};
				var beneficiary1 = {
						"partyIdentifier" : "",
						"relationWithInsured":""
						
				};
				var beneficiary2 = {
						"partyIdentifier" : "",
						"relationWithInsured":""
						
				};
				var beneficiary3 = {
						"partyIdentifier" : "",
						"relationWithInsured":""
						
				};
				var beneficiary4 = {
						"partyIdentifier" : "",
						"relationWithInsured":""
						
				};
				var beneficiary5 = {
						"partyIdentifier" : "",
						"relationWithInsured":""
						
				};
				var beneficiary6 = {
						"partyIdentifier" : "",
						"relationWithInsured":""
						
				};
				var beneficiary7 = {
						"partyIdentifier" : "",
						"relationWithInsured":""
						
				};
				var beneficiary8 = {
						"partyIdentifier" : "",
						"relationWithInsured":""
						
				};
				var beneficiary9 = {
						"partyIdentifier" : "",
						"relationWithInsured":""
						
				};
				productObj = {
						"productCode" : hierarchicalObj.Product.ProductDetails.productCode
				}
				insuredObj = {
						"maritalStatus" : "",
						"partyIdentifier" : UtilityService.getUUID()
				}
				// if(hierarchicalObj.Proposer.CustomerRelationship.isPayorDifferentFromInsured!="Yes"){
				// 	insuredObj.maritalStatus=hierarchicalObj.Proposer.BasicDetails.maritalStatus;
				// }else{
				// 	insuredObj.maritalStatus = hierarchicalObj.Insured.BasicDetails.maritalStatus;
				// }
					
				for(var index in hierarchicalObj.Beneficiaries){
					var beneficiaryObj = hierarchicalObj.Beneficiaries[index];
					var beneficiaryIndex = "beneficiary"+index;
					var evaledBeneficiary =eval(beneficiaryIndex);
					if(beneficiaryObj.CustomerRelationship){
						evaledBeneficiary['relationWithInsured'] = beneficiaryObj.CustomerRelationship.relationWithInsured;
					}
					evaledBeneficiary['partyIdentifier'] = beneficiaryObj.BeneficiaryId;
				}
				canonicalObj.InsuredDetails = insuredObj;
				canonicalObj.ProductDetails = productObj;
				canonicalObj.Beneficiary1 = beneficiary0;
				canonicalObj.Beneficiary2 = beneficiary1;
				canonicalObj.Beneficiary3 = beneficiary2;
				canonicalObj.Beneficiary4 = beneficiary3;
				canonicalObj.Beneficiary5 = beneficiary4;
				canonicalObj.Beneficiary6 = beneficiary5;
				canonicalObj.Beneficiary7 = beneficiary6;
				canonicalObj.Beneficiary8 = beneficiary7;
				canonicalObj.Beneficiary9 = beneficiary8;
				canonicalObj.Beneficiary10 = beneficiary9;
				successCanonicalInput(canonicalObj);
			};
			/**
			 * Method to validate the entered text for typehead.
			 * 
			 */

			EappService.validateSmartSearch = function(value, list){

				if ((list != undefined && value!=undefined) && (list != undefined && value!="")){
					for ( var i = 0 ; i < list.length ; i++ ){
						if (value.toLowerCase() == list[i].value.toLowerCase()){
							return list[i].value;
						}
					}	
				 }
				 return false;
			};
			
			//Function to return FormID and DocType for Pending cases
				EappService.getFormIDAndDocType = function(requirementType, successCallback){
					for(var g = 0; g < EappVariables.getDocTypeFormIdForPending.length;g++){
						if(requirementType == EappVariables.getDocTypeFormIdForPending[g].documentName){
							successCallback(EappVariables.getDocTypeFormIdForPending[g].FormID,EappVariables.getDocTypeFormIdForPending[g].DocType);
							g = EappVariables.getDocTypeFormIdForPending.length;
						}
					}
				};
				
				EappService.saveRequirement = function(filePath, requirementType, partyIdentifier, isMandatory, docArray, documentTypesActual, SerialNumber, successCallback, errorCallback) {
					var requirementObj = Requirement();
					var currentDate = new Date();
					var timeStamp = currentDate.getTime();
					var index = "";
					var description = "";
					var signatureType = "";
					var docObject = [];
					var signatureEditFlow=false;
					var signatureEditIndex=0;
					if(documentTypesActual.indexOf("Signature") >= 0 && (EappVariables.Signature) && (typeof EappVariables.Signature !=undefined)){
						var value=documentTypesActual.split("#");
						documentTypes = value[0];
						signatureType = value[1];
						if ((rootConfig.isDeviceMobile)){
						for(var i=0;i<EappVariables.Signature.length;i++){
							if(EappVariables.Signature[i].documentName.indexOf("Signature") >= 0 ){
								if(EappVariables.Signature[i].documentName.split("_")[2].localeCompare(signatureType)==0){
										signatureEditFlow=true;
										signatureEditIndex=i;
								}
							}
					}
					}
					} else{
						documentTypes=documentTypesActual;
						signatureType ="Agent1";
						
					}

					if ((requirementType !== "Signature") || !signatureEditFlow) {
							var docIndex = 0;
							requirementObj.requirementType = requirementType;
							requirementObj.requirement = requirementType;
							requirementObj.requirementName = signatureType + "_" +partyIdentifier + "_"+ requirementType + "_" + timeStamp;
							requirementObj.partyIdentifier = partyIdentifier;
							requirementObj.isMandatory = (isMandatory) ? "true" : "false";
							requirementObj.SerialNumber = SerialNumber;
							//requirementObj.requirementSubType = signatureType;
							 do {
							if (requirementType == "Signature") {
								requirementObj.Documents[docIndex].documentName = signatureType +"_" + requirementType +"_"+signatureType+"_"+ timeStamp;
								requirementObj.Documents[docIndex].documentType = requirementType;
								requirementObj.Documents[docIndex].documentProofSubmitted = "Signature";
								requirementObj.Documents[docIndex].documentStatus = "";
								requirementObj.Documents[docIndex].FormID = "Signature";
								requirementObj.Documents[docIndex].DocType = "Signature";
								requirementObj.Documents[docIndex].date = currentDate;
								requirementObj.requirementSubType = signatureType;
								
							} else {
									var reqObj = {};
									reqObj.documentProofSubmitted = "";
									if(documentTypes[docIndex].documentOptions[0]){
									reqObj.documentName = 'Agent1_' + requirementObj.requirementType + '_' + partyIdentifier + documentTypes[docIndex].documentOptions[0] + '_' + docIndex + "_" + timeStamp;
									}else{
									 reqObj.documentName = 'Agent1_' + requirementObj.requirementType + '_' + partyIdentifier +"_"+"document"+docIndex+"_"+ timeStamp;
									}
									reqObj.documentStatus = "";
									reqObj.documentUploadStatus = "";
									reqObj.documentOptions = docArray[docIndex].documentOptions;
									reqObj.isMandatory = docArray[docIndex].isMandatory;									
									//Adding FormID and DocType from rule for initial submit case
									reqObj.FormID = docArray[docIndex].FormID;
									reqObj.DocType = docArray[docIndex].DocType;
									reqObj.isDisabled = docArray[docIndex].isDisabled;
										
									if (docArray[docIndex].isMandatory == true) {
										reqObj.isMandatory = true;
									} else {
										reqObj.isMandatory = false;
									}
									if (docArray[docIndex].documentOptions &&
										docArray[docIndex].documentOptions.length == 1) {
										reqObj.documentProofSubmitted = docArray[docIndex].documentOptions[0];
									}
									docObject.push(reqObj);
								requirementObj.Documents = docObject;
								
							}

							docIndex++;
					  } while (docArray.length > docIndex);
						EappVariables.EappModel.Requirements.push(requirementObj);
					}
					
					if (requirementType == "Signature") {
						   
							 for(var i=0;i<EappVariables.Signature.length;i++){
									if(EappVariables.Signature[i].documentName.indexOf("Signature") >= 0 ){
										if(EappVariables.Signature[i].documentName.split("_")[2].localeCompare(signatureType)==0){
										requirementObj.requirementName = EappVariables.Signature[i].requirementName;
										}
									}
								}
						   if((rootConfig.isDeviceMobile)){
									EappService.saveTransaction(function() {
								 EappService.saveRequirementFiles(filePath, index, requirementObj.requirementName, requirementType, description, documentTypesActual, successCallback);
							   }, this.onSaveError, false);
							   }else{
								 EappService.saveRequirementFiles(filePath, index, requirementObj.requirementName, requirementType, description, documentTypesActual, successCallback, errorCallback);
							   }
					
					   } else {
						   if(successCallback && successCallback !="" ) {
							   successCallback();
						   }
					   }
};
			
			EappService.saveRequirementFiles = function(filePath, index, requirementName, requirementType, description, documentTypeActual, successCallback,errorCallback,fileName) {
			var requirementToSaveForWeb;
				var requirementObject = CreateRequirementFile();
				if(fileName){
					var fileType = fileName.split(".").pop().toLowerCase();
			    }
				var fileName = "";
				var currentDate = new Date();
				var timeStamp = currentDate.getTime();
				var signatureType = "";
				var  documentType="";
				var signatureEditFlow=false;
				var signatureEditIndex=0;
                if(documentTypeActual.indexOf("Signature") >= 0 ){
					var value=documentTypeActual.split("#");
                     documentType = value[0];
                     signatureType = value[1];
					 for(var i=0;i<EappVariables.Signature.length;i++){
							if(EappVariables.Signature[i].documentName.indexOf("Signature") >= 0 ){
								if(EappVariables.Signature[i].documentName.split("_")[2].localeCompare(signatureType)==0){
										signatureEditFlow=true;
										signatureEditIndex=i;
								}
							}
					}
                } else{
					documentType=documentTypeActual	
				}
               if (requirementType == "Signature") {
                 fileName = "Signature"
               } else {
                 fileName = filePath.replace(/^.*[\\\/]/, '');
                 if(fileType== "pdf")
     	    		fileName = "pdf";
				 else if(fileType== "doc")
					fileName = "doc";
				 else if(fileType== "docx")
					fileName = "docx";
               }

				/*if (requirementType == "Signature") {
					fileName = "Signature"
				} else {
				fileName = filePath.replace(/^.*[\\\/]/, '');
				}*/
				requirementObject.RequirementFile.identifier = EappVariables.EappKeys.TransTrackingID;
				if (typeof EappVariables.Signature != "undefined" && EappVariables.Signature &&
					signatureEditFlow && documentType == "Signature") {
					requirementObject.RequirementFile.requirementName = EappVariables.Signature[signatureEditIndex].requirementName;
					requirementObject.RequirementFile.documentName = EappVariables.Signature[signatureEditIndex].documentName;
					requirementObject.RequirementFile.fileName = EappVariables.Signature[signatureEditIndex].fileName;
					requirementObject.RequirementFile.DocType = "Signature";
					requirementObject.RequirementFile.FormID = "Signature";
					//requirementObject.RequirementFile.fileType = EappVariables.Signature[signatureEditIndex].fileType;
				} else {
				for (var i = 0; i < EappVariables.EappModel.Requirements.length; i++) {
						if (EappVariables.EappModel.Requirements[i].requirementName == requirementName) {
						    requirementToSaveForWeb = angular.copy(EappVariables.EappModel.Requirements[i]);
							requirementObject.RequirementFile.requirementName = EappVariables.EappModel.Requirements[i].requirementName
							for (var j = 0; j < EappVariables.EappModel.Requirements[i].Documents.length; j++) {
								if (EappVariables.EappModel.Requirements[i].Documents[j].documentProofSubmitted == documentType) {
									if(EappVariables.EappModel.Requirements[i].Documents[j].documentName ==""){
										if(documentType == "Signature"){
											EappVariables.EappModel.Requirements[i].Documents[j].documentName='Agent1_' + requirementType +"_"+signatureType+"_"+ timeStamp;
											//requirementObject.RequirementFile.fileType = signatureType;
										}else{
										EappVariables.EappModel.Requirements[i].Documents[j].documentName = 'Agent1_' + requirementType +"_"+documentType+"_"+ timeStamp;
										}
							
									}/*else{
										if(documentType == "Signature"){
											requirementObject.RequirementFile.fileType = signatureType; 
										}
									}*/
									requirementObject.RequirementFile.documentName = EappVariables.EappModel.Requirements[i].Documents[j].documentName;
									//Assigns DocType and FormID based on the case whether it is direct flow or Resubmission
									if(EappVariables.EappKeys.Key7 == "Pending" || EappVariables.EappKeys.Key7 == "PendingUploaded"){
										EappService.getFormIDAndDocType(documentTypeActual ,function(returnedFormId,returnedDocType){
											requirementObject.RequirementFile.FormID = returnedFormId;
											requirementObject.RequirementFile.DocType = returnedDocType;
											EappVariables.EappModel.Requirements[i].Documents[j].FormID = returnedFormId;
											EappVariables.EappModel.Requirements[i].Documents[j].DocType = returnedDocType;
										});
									}else{										
										if(EappVariables.EappModel.Requirements[i].Documents[j].DocType && EappVariables.EappModel.Requirements[i].Documents[j].FormID){
												requirementObject.RequirementFile.DocType = EappVariables.EappModel.Requirements[i].Documents[j].DocType;
												requirementObject.RequirementFile.FormID = EappVariables.EappModel.Requirements[i].Documents[j].FormID;
											}else{
												EappService.getFormIDAndDocType(documentTypeActual ,function(returnedFormId,returnedDocType){
													requirementObject.RequirementFile.FormID = returnedFormId;
													requirementObject.RequirementFile.DocType = returnedDocType;
													EappVariables.EappModel.Requirements[i].Documents[j].FormID = returnedFormId;
													EappVariables.EappModel.Requirements[i].Documents[j].DocType = returnedDocType;
												});
											}
									}
								}
							}
						}
				}
				var getFilepartyUUID = UtilityService.getUUID();
				requirementObject.RequirementFile.fileName = signatureType + "_" + requirementType + "_" + "Document" + "_" +  getFilepartyUUID + "_" + timeStamp + '.jpg';
				if(fileType== "pdf") {
					requirementObject.RequirementFile.fileName = signatureType + "_" + requirementType + "_" + "Document" + "_" + getFilepartyUUID + "_" + timeStamp + '.pdf';
				}else if(fileType== "doc"){
					requirementObject.RequirementFile.fileName = signatureType + "_" + requirementType + "_" + "Document" + "_" + getFilepartyUUID + "_" + timeStamp + '.doc';
				}else if(fileType== "docx"){
					requirementObject.RequirementFile.fileName = signatureType + "_" + requirementType + "_" + "Document" + "_" + getFilepartyUUID + "_" + timeStamp + '.docx';
				}
			}
			if(EappVariables.EappKeys.Key7 == "Pending"){
		    	requirementObject.RequirementFile.status = "Resubmitted";
		    } else {
		    	requirementObject.RequirementFile.status = "Saved";
		    }
			if (!(rootConfig.isDeviceMobile)) {
				requirementObject.RequirementFile.contentType = "base64";
			}
			requirementObject.RequirementFile.base64string = filePath;
			EappVariables.DocumentPath = requirementObject.RequirementFile.base64string;
			requirementObject.RequirementFile.description = description;
			if (requirementType !== "Signature") {
				if(EappVariables.Requirement) {
					EappVariables.Requirement.push(requirementObject.RequirementFile);
						if(!(rootConfig.isOfflineDesktop) && !(rootConfig.isDeviceMobile)){
                    //var pages=[];
                    var obj={};
                     for(var i=0;i<EappVariables.EappModel.Requirements.length;i++){
                       if(EappVariables.EappModel.Requirements[i].requirementName==requirementObject.RequirementFile.requirementName){
					   requirementToSaveForWeb = angular.copy(EappVariables.EappModel.Requirements[i]);
                          obj.fileName=requirementObject.RequirementFile.fileName;
                          obj.documentStatus=requirementObject.RequirementFile.status;
                           for(var j=0;j<EappVariables.EappModel.Requirements[i].Documents.length;j++){
                                if(EappVariables.EappModel.Requirements[i].Documents[j].documentName==requirementObject.RequirementFile.documentName){
                                    if(EappVariables.EappModel.Requirements[i].Documents[j].pages && EappVariables.EappModel.Requirements[i].Documents[j].pages.length>=0){
                                        EappVariables.EappModel.Requirements[i].Documents[j].pages.push(obj);
										 if(EappVariables.EappKeys.Key7 == "Pending" || EappVariables.EappKeys.Key7 == "PendingUploaded"){
											 EappVariables.EappModel.Requirements[i].Documents[j].documentStatus = "Resubmitted";
											// EappVariables.EappModel.Requirements[i].uploadStatus = true;
										}
                                    }else{
                                        EappVariables.EappModel.Requirements[i].Documents[j].pages=[];
                                        EappVariables.EappModel.Requirements[i].Documents[j].pages.push(obj);
                                        if(EappVariables.EappKeys.Key7 == "Pending" || EappVariables.EappKeys.Key7 == "PendingUploaded"){
											 EappVariables.EappModel.Requirements[i].Documents[j].documentStatus = "Resubmitted";
											// EappVariables.EappModel.Requirements[i].uploadStatus = true;
										}
                                    }
                                     
                                }
                            }
                        }
                     }
                 }
				} else {
					EappVariables.Requirement =[];
					EappVariables.Requirement.push(requirementObject.RequirementFile);
				}
			}
			//Modified for Generali. Changing key values for Resubmission
			if(EappVariables.EappKeys.Key7 == "Pending"){
				EappVariables.EappKeys.Key7 = "PendingUploaded";
			}
			if((rootConfig.isDeviceMobile)) {
			    EappService.saveTransaction(function(){
				    DataService.saveRequirementFiles(requirementObject.RequirementFile, function() {
						successCallback();
				    }, function(e){
				    	console.log(e);
				    });
                }, this.onSaveError, false);				
			} else {
			    EappService.saveRequirementOnly(requirementToSaveForWeb, function(){
				        PersistenceMapping.clearTransactionKeys();
						//To change afetr getting the requirement from Jasna
						//EappVariables.EappKeys.Key15 = "New"; // Not needed for GLI
						EappService.mapKeysforPersistence();
						var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
						DataService.saveRequirementFiles(transactionObj, function() {
							successCallback();
						}, function(e){
							console.log(e);
						});				
				}, errorCallback, false);
			}
	};
	
	
	EappService.downloadApplicationPdf = function(successCallback, errorCallback) {
		if (EappVariables.EappModel.Product.templates && EappVariables.EappModel.Product.templates.eAppPdfTemplate) {
			var eAppTemplateId = EappVariables.EappModel.Product.templates.eAppPdfTemplate;
		}
		if (!eAppTemplateId) {
			var eAppTemplateId = 0;
		}
		PersistenceMapping.clearTransactionKeys();
		EappService.mapKeysforPersistence();
		var transactionObj = PersistenceMapping.mapScopeToPersistence(EappVariables.EappModel);
		if ((rootConfig.isDeviceMobile)) {
				var transactionData = $
						.parseJSON(transactionObj.TransactionData)
				transactionObj.TransactionData = transactionData;
			}
			
		if ((rootConfig.isDeviceMobile)) {
			var objsToSync = [];
			objsToSync.push(EappVariables.EappKeys.Id);
			var syncConfig = [{
				"module" : "eApp",
				"operation" : "syncTo",
				"selectedIds" : objsToSync
			}];
			SyncService.initialize(syncConfig, function() {
				DataService.downloadPDF(transactionObj, eAppTemplateId, successCallback, errorCallback);
			});
			SyncService.runSyncThread();
		} else {
			DataService.downloadPDF(transactionObj, eAppTemplateId, successCallback, errorCallback);
		}
	};

	

	/* Individual Sync */
    EappService.individualSync = function($scope, EappVariables, transactionsToSync, successCallback, errorCallback,syncProgressCallback,syncModuleProgress) {
		PersistenceMapping.clearTransactionKeys();
		EappService.mapKeysforPersistence($scope, EappVariables);
		var syncConfig = [];
		if( transactionsToSync && transactionsToSync.length > 0){
			for ( var i = 0; i <  transactionsToSync.length ; i++){
				var objsToSync = [];
				objsToSync.push(transactionsToSync[i].Value);
				var transactionObj = {
					"module": transactionsToSync[i].Key,
					"operation": "syncTo",
					"selectedIds": objsToSync
				}
				syncConfig.push(transactionObj);
			}
		}
		//call selected sync                                                                                
		SyncService.initialize(syncConfig, function(syncFailed, syncWarning, syncDocFailed, syncDocDownloadFailed,syncModuleProgress) {
			if(!syncFailed && !syncWarning && !syncDocFailed && !syncDocDownloadFailed ) {
				successCallback();
			} else {
				if(syncDocFailed || syncDocDownloadFailed){
					errorCallback("docUploadFailed");
				}else{
					errorCallback();					
				}
			}
		}, function(syncMessage,progress){
				syncModuleProgress(syncMessage,progress);
			}, function(data,type){
			//Sync From
			SyncService.runSyncThread();
		}, function(data,type){
			var statusCode = "";
			if(data.serverAnswer && data.serverAnswer.Response && data.serverAnswer.Response.ResponsePayload && data.serverAnswer.Response.ResponsePayload.Transactions){
		
		
			if(data.serverAnswer.Response.ResponsePayload.Transactions.length > 0){
            	statusCode = data.serverAnswer.Response.ResponsePayload.Transactions[0].Key16 ;
                if( statusCode == "SUCCESS"){                          
                    //Sync To                	
                    SyncService.runSyncThread();
                }else{
                    errorCallback( data.serverAnswer.Response.ResponsePayload.Transactions[0] );
                }
            } else {
            	successCallback();
            } 
        } else {
            errorCallback();
        }  
			
		},syncProgressCallback);		
		SyncService.runSyncThread();
	};
            
    EappService.customIndividualSyncForPdf = function($scope, EappVariables, transactionsToSync, successCallback, errorCallback,syncProgressCallback,syncModuleProgress) {
		PersistenceMapping.clearTransactionKeys();
		EappService.mapKeysforPersistence($scope, EappVariables);
		var syncConfig = [];
		if( transactionsToSync && transactionsToSync.length > 0){
			for ( var i = 0; i <  transactionsToSync.length ; i++){
				var objsToSync = [];
				objsToSync.push(transactionsToSync[i].Value);
				var transactionObj = {
					"module": transactionsToSync[i].Key,
					"operation": "syncTo",
					"selectedIds": objsToSync
				}
				syncConfig.push(transactionObj);
			}
		}
		//call selected sync                                                                                
		SyncService.initialize(syncConfig, function(syncFailed, syncWarning, syncDocFailed, syncDocDownloadFailed,syncModuleProgress) {
			if(!syncFailed && !syncWarning && !syncDocFailed && !syncDocDownloadFailed ) {
				successCallback();
			} else {
				if(syncDocFailed || syncDocDownloadFailed){
					errorCallback("docUploadFailed");
				}else{
					errorCallback();					
				}
			}
		}, function(syncMessage,progress){
				syncModuleProgress(syncMessage,progress);
			}, function(data,type){
			//Sync From
			SyncService.runSyncThread();
		}, function(data,type){
			var statusCode = "";
			if(data.serverAnswer && data.serverAnswer.Response && data.serverAnswer.Response.ResponsePayload && data.serverAnswer.Response.ResponsePayload.Transactions){
		
		
			if(data.serverAnswer.Response.ResponsePayload.Transactions.length > 0){
            	statusCode = data.serverAnswer.Response.ResponsePayload.Transactions[0].Key16 ;
                if( statusCode == "SUCCESS"){                          
                    //Sync To                	
                    successCallback();
                }else{
                    errorCallback( data.serverAnswer.Response.ResponsePayload.Transactions[0] );
                }
            } else {
            	successCallback();
            } 
        } else {
            errorCallback();
        }  
			
		},syncProgressCallback);		
		SyncService.runSyncThread();
	};
	
    		//save call in questionnaire sections while navigating between inner tabs(health details,life style,others)
    		EappService.saveEappQuestionnaireTransaction = function(saveSuccess, saveError, isAutoSave) {
    			this.onSaveSuccess = function(data) {
    				if ((rootConfig.isDeviceMobile)) {
    					EappVariables.EappKeys.Id = data;
    				} else {
    					EappVariables.EappKeys.Key4 = data.Key4;
    				}
    				if (saveSuccess) {
    					saveSuccess(data);
    				}
    			};
    			this.onSaveError = function(data) {
    				if (saveError) {
    					saveError(data);
    				}
    			};
    			PersistenceMapping.clearTransactionKeys();		
    			EappService.mapKeysforPersistence();		
    			var transactionObj = PersistenceMapping.mapScopeToPersistence(EappVariables.EappModel);
    			if(EappVariables.EappKeys.Id && EappVariables.EappKeys.Id !=0 && EappVariables.EappKeys.Id != ""){
    				transactionObj.Id = EappVariables.EappKeys.Id;
    			} 
    			
    			DataService.saveTransactions(transactionObj, this.onSaveSuccess, this.onSaveError);
    		};
    		
    		//To solve pre-populated data getting cleared issue on tab navigation(country of residence & Nationality) before save
    		
			EappService.populateIndependentLookupDate = function(type, fieldName,model,setDefault,field,currentTab,$scope,cacheable) {
				if (!cacheable){
						cacheable = false;
					}
                if (!$scope[fieldName] || $scope[fieldName].length != 0) {
                    $scope[fieldName] = [ {
                           "key" : "",
                           "value" : "loading ...."
                    } ];
                    var options = {};
                    options.type = type;
                    DataLookupService.getLookUpData(options, function(data) {
                           $scope[fieldName] = data;
                           if(setDefault){
                              for ( var i = 0; i < data.length; i++) {
                                 if(data[i].isDefault==1){
                                    $scope.setValuetoScope(model,data[i].value);break;
                                 }
                              }
                           }else{ 
       						var n = "$scope." + model;
     						//var m = eval (n);
							var m = GLI_EvaluateService.evaluateString($scope,model);
     						if(m){
     							$scope.setValuetoScope(model,m);
 							}
     						
     					 }
                           if(typeof field !="undefined" && typeof currentTab !="undefined"){
   							setTimeout(function(){
								       //var data_Evaluvated=$scope.currentTab[field];
										//var dataEvaluvated=eval ('$scope.'+currentTab+'.'+field);
										var dataEvaluvated=GLI_EvaluateService.evaluateString($scope,'$scope.'+currentTab+'.'+field);
											if(typeof dataEvaluvated!="undefined"){
												dataEvaluvated.$render();
											}
										
									},0);
   						}
     					 if(typeof $scope.lmsSectionId != "undefined"){
							  //var data_Evaluvated=$scope.lmsSectionId[fieldName];
							  var data_Evaluvated=GLI_EvaluateService.evaluateString($scope,'$scope.'+$scope.lmsSectionId+'.'+fieldName);
     						 if(data_Evaluvated){
                         		  data_Evaluvated.$render();
                         	  }
							  }
     					 
     					if($scope.viewToBeCustomized) {
     						 setTimeout(function(){
								var data_Evaluvated=GLI_EvaluateService.evaluateString($scope,$scope.viewToBeCustomized+'.'+'premiumFrequency')
						 if(data_Evaluvated!=undefined){
							data_Evaluvated.$render();
						}
                      		  	/*if(eval ('$scope.'+$scope.viewToBeCustomized+'.'+fieldName)){
                          		  eval ('$scope.'+$scope.viewToBeCustomized+'.'+fieldName).$render();
                          	  	}*/
     						 },0);
							  }
     					 
     					 $scope.refresh();
                    }, function(errorData) {
                           $scope[fieldName] = [];
                     
                    },cacheable);
             }
       };
    			
        		//Function to count no.of additional questions selected
                EappService.updateAdditionalQuestionCount = function(val,questionID, currentArr, successcallback) {
                	if(val=="Yes")
                    {
                		for(var i=0;i<currentArr.length;i++)
                    	{
                    		if(currentArr[i]==questionID)
                    		{
                    			currentArr.splice(i,1);
                    		}
                        }
                        currentArr.push(questionID);
                     		
                    }
                	else if(val=="No")
                    {
                    	for(var i=0;i<currentArr.length;i++)
                    	{
                    		if(currentArr[i]==questionID)
                    		{
                    			currentArr.splice(i,1);
                    		}
                        }
                    }
                   
                    else if(val=="Other")
                    {
                    	
                    	for(var i=0;i<currentArr.length;i++)
                    	{
                    		if(currentArr[i]==questionID)
                    		{
                    			currentArr.splice(i,1);
                    		}
                        }
                    	currentArr.push(questionID);
                    		
                    }
                      successcallback(currentArr);
                };

        		
        		//Function to count no.of additional questions selected
        		EappService.updateMedicalQuestionCount = function(val,questionID, medicalArr, successcallback) {
        			if(val=="Yes")
                    {
        				for(var i=0;i<medicalArr.length;i++)
                    	{
                    		if(medicalArr[i]==questionID)
                    		{
                    			medicalArr.splice(i,1);
                    		}
                        }
        				medicalArr.push(questionID);
                    }
                	else if(val=="No")
                    {
                    	for(var i=0;i<medicalArr.length;i++)
                    	{
                    		if(medicalArr[i]==questionID)
                    		{
                    			medicalArr.splice(i,1);
                    		}
                        }
                    }
                      successcallback(medicalArr);
        		};
        		

        		        		
    			/***Function to check Requirement List******/
				EappService.checkRequirementNull = function(reqList,pageId,successCallback) {
			

					if(pageId == "Payment" ){
						var reqCount = 0;
						for(var i =0; i < reqList.length; i++){
							
							if(reqList[i].requirementType == "GAO" || reqList[i].requirementType == "Signature"){
								 reqCount = 0;
							} else {
								reqCount++;
							}
						}

						successCallback(reqCount);
					}						
				};
				
		//get eapp from DB before save -performance custaomization changes to append the questionnaires
				EappService.getEappBeforeSaveEapp = function(getEappSuccess, getEappError) {
					this.onListingSuccess = function(data) {
					    if(data && data.length>0){
						EappVariables.setEappModel(data[0].TransactionData);
						  if (getEappSuccess) {
							getEappSuccess();
						  }
					    }
					};
					this.onListingError = getEappError;
					PersistenceMapping.clearTransactionKeys();
					PersistenceMapping.Key4 = EappVariables.EappKeys.Key4;
					PersistenceMapping.Key24 = EappVariables.EappKeys.Key24;
					PersistenceMapping.Id = EappVariables.EappKeys.Id;
					PersistenceMapping.TransTrackingID = EappVariables.EappKeys.TransTrackingID;
					PersistenceMapping.Type = "eApp";
					var transactionObj = PersistenceMapping.mapScopeToPersistence();
					DataService.getListingDetail(transactionObj, this.onListingSuccess, this.onListingError);
				};
				
				EappService.uploadIndividualReq = function(requirementIndex,requirementFilesArray, curRequirements, successCallback,errorCallback){
					var requirementFile = {};
					  var modifiedObjectsToPush = [];
						  for(var j = 0; j < curRequirements.length; j++){
							if (curRequirements[j].requirementName == requirementFilesArray[requirementIndex].requirementName) {
								for (var k = 0; k < curRequirements[j].Documents.length; k++) {
									if (curRequirements[j].Documents[k].documentName == requirementFilesArray[requirementIndex].documentName) {
										modifiedObjectsToPush.push(curRequirements[j]);
										break;
									}
								}
							}
						  }
						RequirementFile={};
						RequirementFile.Requirements =modifiedObjectsToPush;		
						PersistenceMapping.dataIdentifyFlag = false;
						transactionObj = PersistenceMapping.mapScopeToPersistence(RequirementFile);		
						DocumentService.saveRequirementOnly(transactionObj, function(){
							requirementFile.RequirementFile = requirementFilesArray[requirementIndex];
									RequirementService.readFile(requirementFilesArray[requirementIndex],function(base64FileContent){
										requirementFile.RequirementFile.base64string = base64FileContent;
										PersistenceMapping.dataIdentifyFlag = false;
										var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementFile);
										
										RequirementService.uploadDocumentFile(transactionObj,function(result) {
											if (result.ResponsePayload.Transactions[0].StatusData
													&& result.ResponsePayload.Transactions[0].StatusData.Status == "SUCCESS") {
													var statusUpdate = {};
													statusUpdate.status = 'Synced';
													statusUpdate.Id = transactionObj.Id;
													DataService.updateFileStatusSynced(
																	statusUpdate,function(){
																		requirementIndex++;
																		if( requirementIndex < requirementFilesArray.length){
																			EappService.uploadIndividualReq(requirementIndex, requirementFilesArray, curRequirements, function(){
																				successCallback();
																			},function(){errorCallback();});
																		}else{
																			successCallback();
																		}
																	},
																function(){errorCallback();});
											}else{
												errorCallback();
											}
										});
									});
						},function(){ errorCallback();});
							
				};
				
				
				
				EappService.uploadAllResubmittedDocs = function(transactionObj,successCallback,errorCallback){
					var reqArrayToCheck = angular.copy(transactionObj.TransactionData);
					reqArrayToCheck = JSON.parse(reqArrayToCheck);
					DataService.getAllDocsToResubmit("Resubmitted",transactionObj.TransTrackingID,function(requirementFilesArray){
						var reqFileIndex = 0
						if ( requirementFilesArray && requirementFilesArray != null && requirementFilesArray.length > 0) {
							var requirementIndex = 0;
							EappService.uploadIndividualReq(requirementIndex, requirementFilesArray, reqArrayToCheck.Requirements, function(){
								successCallback();
							},function(){errorCallback();});
						}else{
							successCallback();
						}
						
					},function(){
						errorCallback();
					});
				};
		//Memo mail same as SI logic		
		/*EappService.getEmailObj = function(transactionObjList,objectToAdd, successcallback){
			var objsToSync = [];//used in case of user is online and flag online_emailBI is set true
			var isComplete = false;
			$.each(transactionObjList, function(i, obj) {
				if(obj) {
				   obj.Key18 = true;                               
				   obj.TransactionData.Email = objectToAdd;
				   if ((rootConfig.isDeviceMobile)) {
					   obj.TransactionData =  angular.toJson(obj.TransactionData);
				   }
				   else{
					   obj.TransactionData =  obj.TransactionData; 
					   obj.TransactionData.Insured.BasicDetails.fullName =  obj.Key6; 
				   }
				   
				   //If online mail sending needed keeping list of ids for selected sync
				   objsToSync.push(obj.Id);
				   if(i == (transactionObjList.length-1)){
					isComplete= true;
				   }
				}
			}); 
			if(isComplete){
				successcallback(objsToSync);
			}
		};
		EappService.setEmailEappflagAndSaveTransactions = function(objectToAdd,transactionObjList,successCallback,errorCallback) {
			EappService.getEmailObj(transactionObjList,objectToAdd,function(objsToSync){
				DataService.saveTransactionObjectsMultiple(transactionObjList,function() {
					var syncConfig = [{
						"module" :"eApp",
						"operation" : "syncTo",
						"selectedIds" : objsToSync                                  
					}];
					if ((rootConfig.isDeviceMobile) && checkConnection()) {
						if(objsToSync.length > 0) {
							//call selected sync                                                                                
							SyncService.initialize(syncConfig,function(){
								//Call Email Service to mail BI.                                
								PersistenceMapping.clearTransactionKeys();
								var transactionObjForMailReq = PersistenceMapping.mapScopeToPersistence({});
								transactionObjForMailReq.Type = "eApp";
								var userDetails = UserDetailsService.getUserDetailsModel();
								UtilityService.emailServiceCall(userDetails,transactionObjForMailReq);
								successCallback(true);
							},function(){
								errorCallback;
							});                                                                                     
							SyncService.runSyncThread();
						}                                   
					}else{
						PersistenceMapping.clearTransactionKeys();
						var transactionObjForMailReq = PersistenceMapping.mapScopeToPersistence({});
						transactionObjForMailReq.Type = "eApp";
						var userDetails = UserDetailsService.getUserDetailsModel();
						UtilityService.emailServiceCall(userDetails,transactionObjForMailReq);
						successCallback(true);
					}
				},errorCallback);
			});
		};*/
		
		EappService.saveTransaction = function(saveSuccess, saveError, isAutoSave) {
		this.onSaveSuccess = function(data) {
			if ((rootConfig.isDeviceMobile)) {
				EappVariables.EappKeys.Id = data;
			} else {
				EappVariables.EappKeys.Key4 = data.Key4;
			}
			if (saveSuccess) {
				saveSuccess(data);
			}
		};
		this.onSaveError = function(data) {
			if (saveError) {
				saveError(data);
			}
		};
		PersistenceMapping.clearTransactionKeys();		
		EappService.mapKeysforPersistence();		
		var transactionObj = PersistenceMapping.mapScopeToPersistence(EappVariables.EappModel);
		var source =(rootConfig.isDeviceMobile)=="false"? "TABLET": "DESKTOP";
		transactionObj.TransactionData.source = source;
		if(transactionObj != undefined 
		   && transactionObj.TransactionData != undefined 
		   && transactionObj.TransactionData.Insured != undefined 
		   && transactionObj.TransactionData.Insured.Declaration != undefined 
		   && transactionObj.TransactionData.Insured.Declaration.dateandTime != undefined
		   && transactionObj.TransactionData.Insured.Declaration.dateandTime != ""){
			transactionObj.TransactionData.Insured.Declaration.dateandTime = transactionObj.TransactionData.Insured.Declaration.dateandTime + " 00:00:00";
		}
		if(EappVariables.EappKeys.Id && EappVariables.EappKeys.Id !=0 && EappVariables.EappKeys.Id != ""){
			transactionObj.Id = EappVariables.EappKeys.Id;
		} 
		if(transactionObj.Key15==""||transactionObj.Key15==rootConfig.eAppLandingScrnStatus)
			transactionObj.Key30="0";
		else
			transactionObj.Key30=GLI_EappVariables.memoCount;
		if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
			var key11Data=transactionObj.Key11;
			transactionObj.Key11=GLI_EappVariables.agentForGAO;
			if(transactionObj.Key15!=null && transactionObj.Key15!="Submitted") {
				transactionObj.Key35=GLI_EappVariables.GAOOfficeCode;
				transactionObj.Key36=key11Data;
				transactionObj.Key37=UserDetailsService.getAgentRetrieveData().AgentDetails.agentType;
				transactionObj.Key38=GLI_EappVariables.agentNameForGAO;
			}else{
				/* Changes done for eApp report starts*/
				if(transactionObj.Key15!=null && transactionObj.Key15=="Submitted") {
					transactionObj.Key32=key11Data;
					transactionObj.Key33=UserDetailsService.getAgentRetrieveData().AgentDetails.agentType;
				}
				/* Changes done for eApp report ends*/
				transactionObj.Key35="";
				transactionObj.Key36="";
				transactionObj.Key37="";
				transactionObj.Key38="";
			}			
		}else if(transactionObj.Key15!=null && transactionObj.Key15!="Submitted") {
			transactionObj.Key35=(typeof GLI_EappVariables.GAOOfficeCode!="undefined")?GLI_EappVariables.GAOOfficeCode:"";
			transactionObj.Key36=(typeof GLI_EappVariables.GAOId!="undefined")?GLI_EappVariables.GAOId:"";
			transactionObj.Key37=(typeof GLI_EappVariables.agentType!="undefined")?GLI_EappVariables.agentType:"";
			transactionObj.Key38=(typeof GLI_EappVariables.agentNameForGAO!="undefined")?GLI_EappVariables.agentNameForGAO:"";
		}
		var valCheck = transactionObj.Key11;
		var isNum = /^\d+$/.test(valCheck);
		if(transactionObj.Key11!=null) {
			if(transactionObj.Key11 == transactionObj.Key36 || isNum==false) {
				transactionObj.Key11=transactionObj.Key4.substring(0,8);
			}
		}
		
		DataService.saveTransactions(transactionObj, this.onSaveSuccess, this.onSaveError);
	};
	
	EappService.getRequirementFilePath = function(filedata, index, requirementName, requirementType, documentType, successCallback, errorCallback) {
		if ((rootConfig.isDeviceMobile)) { 
		var path="";
			if (requirementType == "Signature") {
				DocumentService.base64AsFile(filedata, function(filePath) {
					successCallback(filePath);
				}, function(error) {});
			} else {
				if(filedata.path){
				path = filedata.path;
				
				}else{
				path = filedata;
				}
				DocumentService.copyFile(path,function(filePath) {
					var fileName=filedata.name;
					/*var data1=filePath.split("/");
					var fileName=data1[data1.length-1];*/
					var description = "";
					var srcUrl = '';
					if (!rootConfig.isOfflineDesktop) {
						srcUrl = filePath;
					} else {
						srcUrl = "../" + filePath.path;
					}
					if (fileName == "" || fileName == undefined) {
						var data1=srcUrl.split("/");
						fileName=data1[data1.length-1];
					}
					if(requirementType==rootConfig.agentTypeGAO){
						EappVariables.EappModel.LastVisitedUrl=$rootScope.lastURL;
						EappVariables.EappModel.LastVisitedIndex=$rootScope.lastURLindex;
					}
				   EappService.saveTransaction(function() {
				   
					EappService.saveRequirementFiles(srcUrl, index, requirementName, requirementType, description, documentType, function() {
						successCallback();
					},'',fileName);
		 
		 }, this.onSaveError, false);
				}, errorCallback,filedata);

			}
		} else {
			var docCount = 0;
			var currAgentId = $rootScope.agentId == null ? $rootScope.username : $rootScope.agentId;
			var document = EappService.mapDocScopePersistence(filedata);
			if (!document.documentName) {
				var d = new Date();
				var n = d.getTime();
				var name = "document";
				var newFileName = "Agent1" + n + "_" + name;
				document.documentName = newFileName;
			}
			DocumentService.browseFile(document, currAgentId, function(filePath, outStr, fr) {
				var description = "";
				var fileName=document.fileObj.name;
				filePath = document.documentPath;
				EappVariables.DocumentPath = filePath;
				EappService.saveRequirementFiles(filePath, index, requirementName, requirementType, description,documentType, function() {
					successCallback();
				},'',fileName);
			}, function(error) {
			if(errorCallback){
				errorCallback(error);
				}
			});
		}
	};
	EappService.mapKeysforPersistenceGAO = function() {
		PersistenceMapping.Key1 = EappVariables.EappKeys.Key1;
		PersistenceMapping.Key2 = EappVariables.EappKeys.Key2;
		PersistenceMapping.Key3 = EappVariables.EappKeys.Key3;
		PersistenceMapping.Key4 = EappVariables.EappKeys.Key4;
		PersistenceMapping.Key5 = EappVariables.EappKeys.Key5;
		PersistenceMapping.Key10 = "FullDetails";
		PersistenceMapping.Key12 = "";
		if ((rootConfig.isDeviceMobile)) {
			if (EappVariables.EappKeys.Id == null || EappVariables.EappKeys.Id == 0) {
								PersistenceMapping.creationDate = UtilityService
										.getFormattedDateTime();
								EappVariables.EappKeys.creationDate=UtilityService
										.getFormattedDateTime();
							}else{
								PersistenceMapping.creationDate=EappVariables.EappKeys.creationDate;
			}
		} else {
							if (EappVariables.EappKeys.Key4==null || EappVariables.EappKeys.Key4==0 || EappVariables.EappKeys.Key4==0) {
							  PersistenceMapping.creationDate = UtilityService
									.getFormattedDateTime();
							  EappVariables.EappKeys.creationDate=UtilityService
										.getFormattedDateTime();
							}else{
								PersistenceMapping.creationDate=EappVariables.EappKeys.creationDate;
				}
		}
		if (EappVariables.EappKeys.TransTrackingID == null || EappVariables.EappKeys.TransTrackingID == 0) {
			PersistenceMapping.TransTrackingID = UtilityService.getTransTrackingID();
			EappVariables.EappKeys.TransTrackingID = PersistenceMapping.TransTrackingID;
		} else {
			PersistenceMapping.TransTrackingID = EappVariables.EappKeys.TransTrackingID;
		}
		PersistenceMapping.Key15 = EappVariables.EappKeys.Key15 != null ? EappVariables.EappKeys.Key15 : "Saved";
		PersistenceMapping.Key16 = EappVariables.EappKeys.Key16 != null ? EappVariables.EappKeys.Key16 : "";
		PersistenceMapping.Type = "eApp";
	};
	
	EappService.checkRequirementGAOSignature = function(reqList,pageId,successCallback) {
		if(pageId == "GAO" ){
			var reqCount = 0;
			for(var i =0; i < reqList.length; i++){
				if(reqList[i].requirementType != "GAO" && reqList[i].requirementType != "Signature"){
					reqCount++;
				} 
			}
			successCallback(reqCount);
		}						
	};
	
	
			return EappService;
		}]);