﻿/*
 *Copyright 2015, LifeEngage 
 */



function Navigation($scope, viewName, direction) {
	if ($scope.confirmForPaymentAuth == true) {
		var stepCount = 0;
		$scope.confirmForPaymentAuth = false;
	} else {
		var stepCount = 1;
	}
	var navObj = {};
	navObj.showErrorcount = "Yes";
	if (direction == "forward") {
		for ( var i in $scope.tabs) {
			tabs = $scope.tabs[i];
			if (tabs.viewName == viewName) {
				if (tabs.subTabs.length > 0) {
					navObj.nextView = $scope.tabs[subtabindex + 1].subTabs[0].viewName;
					navObj.mainTab = tabs.viewName;
					navObj.showPrevButton = $scope.tabs[subtabindex + 1].subTabs[0].showPrevButton;
					navObj.showNextButton = $scope.tabs[subtabindex + 1].subTabs[0].showNextButton;
					return navObj;
				} else {
					nextMainTab = $scope.tabs[parseInt(i) + 1];
					if (nextMainTab.subTabs.length > 0) {
						navObj.nextView = nextMainTab.subTabs[0].viewName;
						navObj.mainTab = nextMainTab.viewName;
						navObj.showPrevButton = nextMainTab.subTabs[0].showPrevButton;
						navObj.showNextButton = nextMainTab.subTabs[0].showNextButton;
						return navObj;
					} else {
						navObj.nextView = $scope.tabs[parseInt(i) + 1].viewName;
						navObj.mainTab = navObj.nextView;
						navObj.showPrevButton = $scope.tabs[parseInt(i) + 1].showPrevButton;
						navObj.showNextButton = $scope.tabs[parseInt(i) + 1].showNextButton;
						return navObj;
					}
				}
			} else {

				if (tabs.subTabs.length > 0) {
					for ( var j = 0; j < tabs.subTabs.length; j++) {
						if (tabs.subTabs[j].viewName == viewName) {
							if (typeof tabs.subTabs[j + 1] != 'undefined') {

								var exp = eval('$scope.'
										+ tabs.subTabs[j + 1].ngShow);
								if (exp == true || exp == 'undefined'
										|| typeof exp == 'undefined') {
									navObj.nextView = tabs.subTabs[j + 1].viewName;
									navObj.mainTab = tabs.viewName;
									navObj.showPrevButton = tabs.subTabs[j + 1].showPrevButton;
									navObj.showNextButton = tabs.subTabs[j + 1].showNextButton;
									return navObj;
								} else {

									var navObj = Navigation($scope,
											tabs.subTabs[j + 1].viewName,
											direction);
									return navObj;
								}
							}

							else {
								nextMainTab = $scope.tabs[parseInt(i) + 1];
								if (nextMainTab.subTabs.length > 0) {
									navObj.nextView = nextMainTab.subTabs[0].viewName;
									navObj.mainTab = nextMainTab.viewName;
									navObj.showPrevButton = nextMainTab.showPrevButton;
									navObj.showNextButton = nextMainTab.showNextButton;
									return navObj;
								} else {
									navObj.nextView = $scope.tabs[parseInt(i) + 1].viewName;
									navObj.mainTab = navObj.nextView;
									navObj.showPrevButton = $scope.tabs[parseInt(i) + 1].showPrevButton;
									navObj.showNextButton = $scope.tabs[parseInt(i) + 1].showNextButton;
									return navObj;
								}

							}

						}

					}

				}

			}
		}
	} else {

		for ( var i in $scope.tabs) {
			tabs = $scope.tabs[i];
			if (tabs.viewName == viewName) {

				if (tabs.subTabs.length > 0) {

					navObj.nextView = $scope.tabs[subtabindex - 1].subTabs[0].viewName;
					navObj.mainTab = tabs.viewName;
					navObj.showPrevButton = tabs.showPrevButton;
					navObj.showNextButton = tabs.showNextButton;
					return navObj;
				} else {

					prevMainTab = $scope.tabs[parseInt(i) - stepCount];

					if (prevMainTab.subTabs.length > 0) {

						navObj.nextView = prevMainTab.subTabs[parseInt(prevMainTab.subTabs.length) - 1].viewName;
						navObj.mainTab = prevMainTab.viewName;
						navObj.showPrevButton = prevMainTab.showPrevButton;
						navObj.showNextButton = prevMainTab.showNextButton;
						return navObj;

					} else {

						navObj.nextView = $scope.tabs[parseInt(i) - stepCount].viewName;
						navObj.mainTab = navObj.nextView;
						navObj.showPrevButton = $scope.tabs[parseInt(i)
								- stepCount].showPrevButton;
						navObj.showNextButton = $scope.tabs[parseInt(i)
								- stepCount].showNextButton;
						return navObj;

					}
				}
			} else {

				if (tabs.subTabs.length > 0) {
					for ( var j = 0; j < tabs.subTabs.length; j++) {

						if (tabs.subTabs[j].viewName == viewName) {
							if (typeof tabs.subTabs[j - 1] != 'undefined') {
								var exp = eval('$scope.'
										+ tabs.subTabs[j - 1].ngShow);
								if (exp == true || exp == 'undefined'
										|| typeof exp == 'undefined') {
									navObj.nextView = tabs.subTabs[j - 1].viewName;
									navObj.mainTab = tabs.viewName;
									navObj.showPrevButton = tabs.subTabs[j - 1].showPrevButton;
									navObj.showNextButton = tabs.subTabs[j - 1].showNextButton;
									return navObj;
								} else {
									var navObj = Navigation($scope,
											tabs.subTabs[j - 1].viewName,
											direction);
									return navObj;
								}
							} else {
								prevMainTab = $scope.tabs[parseInt(i) - 1];
								if (prevMainTab.subTabs.length > 0) {
									var exp = eval('$scope.'
											+ prevMainTab.subTabs[parseInt(prevMainTab.subTabs.length) - 1].ngShow);
									if (exp == true || exp == 'undefined'
											|| typeof exp == 'undefined') {
										navObj.nextView = prevMainTab.subTabs[parseInt(prevMainTab.subTabs.length) - 1].viewName;
										navObj.mainTab = prevMainTab.viewName;
										navObj.showPrevButton = prevMainTab.subTabs[parseInt(prevMainTab.subTabs.length) - 1].showPrevButton;
										navObj.showNextButton = prevMainTab.subTabs[parseInt(prevMainTab.subTabs.length) - 1].showNextButton;
										return navObj;
									} else {
										var navObj = Navigation(
												$scope,
												prevMainTab.subTabs[parseInt(prevMainTab.subTabs.length) - 1].viewName,
												direction);
										return navObj;
									}

								} else {
									navObj.nextView = prevMainTab.viewName;
									navObj.mainTab = navObj.nextView;
									navObj.showPrevButton = prevMainTab.showPrevButton;
									navObj.showNextButton = prevMainTab.showNextButton;
									return navObj;
								}
							}
						}
					}
				}
			}
		}
	}
}
