/*
 *Copyright 2015, LifeEngage 
 */
/*Name:SummaryController
 * For implementing GLI specific changes
 */
storeApp.controller('GLI_SummaryAgentController', GLI_SummaryAgentController);
GLI_SummaryAgentController.$inject = ['$timeout', '$route', 'globalService', '$rootScope', '$scope', '$location', '$compile', '$routeParams', 'DataService', 'RuleService', 'LookupService', 'DocumentService', 'FnaVariables', 'GLI_FnaService', '$translate', 'UtilityService', '$debounce', 'AutoSave', '$controller', 'EappService', 'EappVariables', 'UserDetailsService', 'PersistenceMapping', 'GLI_DataService', 'GLI_EappService', 'GLI_RuleService', 'AgentService', 'GLI_EappVariables', '$filter'];

function GLI_SummaryAgentController($timeout, $route, globalService, $rootScope, $scope, $location, $compile, $routeParams, DataService, RuleService, LookupService, DocumentService, FnaVariables, GLI_FnaService, $translate, UtilityService, $debounce, AutoSave, $controller, EappService, EappVariables, UserDetailsService, PersistenceMapping, GLI_DataService, GLI_EappService, GLI_RuleService, AgentService, GLI_EappVariables, $filter) {
    $controller('SummaryController', {
        $timeout: $timeout,
        $route: $route,
        globalService: globalService,
        $rootScope: $rootScope,
        $scope: $scope,
        $location: $location,
        $compile: $compile,
        $routeParams: $routeParams,
        DataService: DataService,
        RuleService: RuleService,
        LookupService: LookupService,
        DocumentService: DocumentService,
        FnaVariables: FnaVariables,
        FnaService: GLI_FnaService,
        $translate: $translate,
        UtilityService: UtilityService,
        $debounce: $debounce,
        AutoSave: AutoSave
    });

    /* var unload=$scope.$parent.$on('Declaration_unload', function(event) {
        	var data=$scope.$parent;
            angular.element( document.querySelector( '#Declaration' ) ).empty();
            $scope.$destroy();
            $scope.$parent=data;
            load();
            unload(); 
		});*/

    $scope.isBanca = false;
    $scope.acrConfirmed = false;
    $scope.signDisabled = false;
    $scope.alphanumericPattern = rootConfig.alphanumericPattern;
    var signature = "Signature";
    $scope.notJuvenile = true;
    $scope.juvenileChange = true;
    var outputFormat = "image/png";
    $scope.isSPAJNoAllocated = false;

    $scope.isRDSUser = UserDetailsService.getRDSUser();
    $scope.lastVisitedDate = "";
    $scope.appDateTimeFormat = rootConfig.appDateTimeFormat;
    $scope.setSaveproposalDisableFlag = false;
    $scope.showErrorCount = true;
    /*if(!(rootConfig.isDeviceMobile) &&(typeof $scope.DocumentType == "undefined")){
			$scope.DocumentType="Signature#AgentSignature";
    	 }*/
    $scope.isDeviceWeb = true;
    $scope.spajNumber = EappVariables.EappKeys.Key21;
    if (!$scope.spajNumber) {
        $scope.spajNumber = "******";
    } else {
        $scope.isSPAJNoAllocated = true;
    }
    if ((rootConfig.isDeviceMobile)) {
        //disabling canvas based on the declaration check
        $scope.agentStatementCanvas = true;
        //disabling summary page buttons based on the signature check
        $scope.agentStatementButtonDisable = false;
    } else {
        $scope.agentStatementCanvas = false;
        $scope.agentStatementButtonDisable = false;
    }
    $rootScope.selectedPage = "Summary";
    var signaturePadAgent;
    //to translte rider isnured type
    $scope.translate = function (riderType) {
        return $translate.instant("eapp." + riderType);
    }
    /**
     * to translate client declarations
     */

    $scope.translateBasedOnProduct = function (textModel) {
        if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == 20 || $scope.LifeEngageProduct.Product.ProductDetails.productCode == 9 || $scope.LifeEngageProduct.Product.ProductDetails.productCode == 4) {
            return $translate.instant(textModel + "sameProducts");
        } else if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == 1 || $scope.LifeEngageProduct.Product.ProductDetails.productCode == 5) {
            return $translate.instant(textModel + $scope.LifeEngageProduct.Product.ProductDetails.productCode);
        }
    }

    $scope.translateSubDescriptionSummary = function (translatetext) {
        if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == 20 || $scope.LifeEngageProduct.Product.ProductDetails.productCode == 9 || $scope.LifeEngageProduct.Product.ProductDetails.productCode == 4) {
            return $translate.instant(translatetext + "sameProducts");
        } else {
            return $translate.instant(translatetext + '1');
        }
    }
    /**
     * to translate client declarations end
     */

    $scope.checkCommissionRate = function () {
        if ($scope.LifeEngageProduct.Declaration.Questions[23].option == 'Yes') {
            $scope.LifeEngageProduct.AgentInfo.commissionRateOne = rootConfig.eappCommssionRateOne;
            $scope.LifeEngageProduct.AgentInfo.commissionRateTwo = rootConfig.eappCommssionRateTwo;
            if ($scope.agentDetail.agentType == 'IOIS') {
                $scope.isBanca = true;
            } else {
                $scope.isBanca = false;
            }
        } else {
            $scope.LifeEngageProduct.AgentInfo.commissionRateOne = 100;
            $scope.LifeEngageProduct.AgentInfo.commissionAgent = "";
            $scope.LifeEngageProduct.AgentInfo.commissionRateTwo = "";
        }
    }
    //To disable specific button to address issue on different flows
function removeDisableSpecificElementsInScope () {
        $('#Declaration #agentSectionPlace').removeProp("disabled");
        $('#Declaration #agentAgree').removeAttr("disabled");
    };

    function disableSpecificElementsInScope () {
        $('#Declaration button#okBtnPolicyHolderACRD').attr("disabled", "disabled");
        $('#Declaration button#clearBtnPolicyHolderACR').attr("disabled", "disabled");
    };



    $scope.disableSpecificElementsOnConfirmation = function () {
        if (!$scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature) {
            /*$('input[type="text"]').attr("disabled", "disabled");
            $('input[type="date"]').attr("disabled", "disabled");
            $('select').not('.languageSelector').attr("disabled", "disabled");
            $('input[type="radio"]').attr("disabled", "disabled");
            $('.iradio').addClass('disabled');
            $('input[type="checkbox"]').attr("disabled", "disabled");
            $('.icheckbox').addClass('disabled');*/
        }
    };

    $scope.Initialize = function () {
        $scope.$parent.disableProceedButtonCommom = false;
        $('#agentSummaryDate').attr("disabled", "disabled");
        $scope.dateDisable = true;
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            //EappVariables.setEappModel($scope.LifeEngageProduct);
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
        }
        $scope.LifeEngageProduct = EappVariables.getEappModel();
        //agent type check
        $scope.agentDetail = UserDetailsService.getUserDetailsModel();

        //$scope.eAppParentobj.PreviousPage   = "Declaration_unload";
        $scope.eAppParentobj.PreviousPage = "";
        /*if($scope.LifeEngageProduct.Declaration.Questions[6].details != ""){
        	$scope.lastVisitedDate = $scope.LifeEngageProduct.Declaration.Questions[6].details;
        }*/
        $scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature = true;
        if (typeof $scope.DocumentType == "undefined") {
            $scope.DocumentType = "Signature#AgentSignatureSummary";
        }

        $scope.LifeEngageProduct.Declaration.agentSignDate = new Date();
        if ($scope.LifeEngageProduct.Declaration.agentSignDate && $scope.LifeEngageProduct.Declaration.agentSignDate != "") {
            $scope.signedDate = new Date();
            if (!$scope.isRDSUser) {
                $scope.spajAgentDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.agentSignDate), $scope.appDateTimeFormat);
            } else {
                $scope.agentSignDate = $scope.LifeEngageProduct.Declaration.agentSignDate;
            }
        }

        //function call to show/hide questiannoare tab
        if (typeof $scope.LifeEngageProduct.showHideQuestaionnaireTab == "undefined") {
            $scope.LifeEngageProduct.showHideQuestaionnaireTab = summayQuestionnaireTabShowHide();
            $scope.$parent.LifeEngageProduct.showHideQuestaionnaireTab = $scope.LifeEngageProduct.showHideQuestaionnaireTab;
        }

        if (!$scope.LifeEngageProduct.Declaration.Questions[23].option) {
            $scope.LifeEngageProduct.Declaration.Questions[23].option = 'No';
            $scope.LifeEngageProduct.AgentInfo.commissionRateOne = 100;
        }
        $timeout(function () {
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission" && EappVariables.EappKeys.Key15 != "Confirmed") {
                $scope.setSaveproposalDisableFlag = false;
                $scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature = true;
                UtilityService.disableAllActionElements();
            }else if (typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) {
                $scope.setSaveproposalDisableFlag = false;
                $scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature = true;
                UtilityService.disableAllActionElements();
            } else {
                $scope.setSaveproposalDisableFlag = true;
                UtilityService.removeDisableSpecificElements('Declaration');
                if (!$scope.agentStatementButtonDisable) {
                    removeDisableSpecificElementsInScope();
                }
            }
            $('#agentSummaryDate').attr("disabled", "disabled");
            $scope.checkInsuredNotJuvenile();

            UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
            $scope.updateErrorCount($scope.selectedTabId);
            $scope.showErrorCount = true;

            if (EappVariables.EappKeys.TransTrackingID) {
                $scope.getSignature($scope.DocumentType.split("#")[1], function (base64String) {
                    $scope.loadSignature(base64String);
                });
            }
            /*if($scope.lastVisitedDate != ""){
	    			$scope.LifeEngageProduct.Declaration.Questions[6].details = $scope.lastVisitedDate;
	    		}*/

            $scope.disableSpecificElementsOnConfirmation();
            if (rootConfig.isDeviceMobile == "false" && $scope.isRDSUser) {
                $scope.isDeviceWeb = false;
            }
        }, 50);

        $timeout(function () {
            if ($scope.agentDetail.agentType == 'IOIS') {
                $scope.isBanca = true;
            } else {
                $scope.isBanca = false;
            }
        }, 0);

        //For disabling and enabling the tabs.And also to know the last visited page.---starts
        $scope.LifeEngageProduct.LastVisitedUrl = "4,4,'Declaration',true,'Declaration',''";
        $scope.eAppParentobj.nextPage = "5,0,'Payment',false,'tabsinner16',''";
        $scope.eAppParentobj.CurrentPage = "";
        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[1] < 4 && subIndex[0] <= $scope.LifeEngageProduct.LastVisitedUrl[0]) {
                $scope.LifeEngageProduct.LastVisitedIndex = "4,4";
            }
        }

        if ($scope.LifeEngageProduct.Declaration.clientdeclarationAgreed != "Yes" && (rootConfig.isDeviceMobile)) {
            $scope.agentStatementCanvas = true;
        }
        //For disabling and enabling the tabs.And also to know the last visited page.---ends
        $scope.agentDetail = UserDetailsService.getUserDetailsModel();
        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        $scope.updateErrorCount($scope.selectedTabId);
        $scope.showErrorCount = true;
        if ($scope.LifeEngageProduct.Declaration.isFinalSubmit) {
            $scope.isSTPSuccess = true;
            $scope.isSTPExecuted = true;
            $scope.STPMessage = translateMessages($translate, "stpRulePassed");
        } else {
            $scope.isSTPSuccess = false;
            $scope.isSTPExecuted = false;
        }
        $scope.proposalId = EappVariables.EappKeys.Key4;
    };
    var signatureString;
    $scope.loadSignature = function (base64) {
        if (base64 && base64 != '') {
            signatureString = base64;
        }
        if (angular.element('#agentStatementCanvas')[0]) {
            signaturePadAgent = new SignaturePad(angular.element('#agentStatementCanvas')[0].querySelector("canvas"));
            signaturePadAgent.onEnd = function () {
                $scope.signDisabled = false;
                $scope.acrConfirmed = false;
                $('#Declaration button#okBtnPolicyHolderACRD').removeAttr("disabled");
                $('#Declaration button#clearSig').removeAttr("disabled");
            };
            var dpr = window.devicePixelRatio;
            window.devicePixelRatio = 1;
            if (signaturePadAgent) {
                signaturePadAgent.clear();
                if (signatureString)
                    signaturePadAgent.fromDataURL(signatureString);
            } else {
                if (!(rootConfig.isDeviceMobile) && $scope.signaturePad) {
                    $scope.signaturePad.clear();
                }
            }
            window.devicePixelRatio = dpr;

            if (signatureString) {
                $scope.setSaveproposalDisableFlag = false;
                $scope.agentStatementButtonDisable = true;
                $scope.agentStatementCanvas = true;
                $scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature = false;
            } else {
                if ($scope.LifeEngageProduct.Declaration.agentSignDate && $scope.LifeEngageProduct.Declaration.agentSignDate != '') {
                    $scope.agentStatementButtonDisable = false;
                    $scope.agentStatementCanvas = false;
                }
            }
            if (!$scope.agentStatementButtonDisable) {
                removeDisableSpecificElementsInScope();
            } else {
                disableSpecificElementsInScope();
            }
            signature = '';
            $scope.refresh();
        } else {
            $timeout($scope.loadSignature(), 10);

        }
        setTimeout(function () {
            //added signatureString condition to fix the issue of signature getting cleared when status is payment done
            if ($scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature && !signatureString) {
                $scope.clearSignature('agentStatementCanvas');
                $scope.acrConfirmed = false;
                $('#Declaration button#okBtnPolicyHolderACRD').attr("disabled", "disabled");

            } else {
                disableSpecificElementsInScope();
                $scope.acrConfirmed = true;
                $scope.signDisabled = true;
                $('#Declaration button#confirmSig').attr("disabled", "disabled");
                $('#Declaration button#clearSig').attr("disabled", "disabled");
                $scope.$parent.refresh();
            }
        }, 10);
    }


    $scope.setDefaultDuration = function () {
        if ($scope.LifeEngageProduct.Declaration.Questions[0].option == 'Yes') {
            $scope.LifeEngageProduct.Declaration.Questions[0].options[0].details = 0;
        } else {
            $scope.LifeEngageProduct.Declaration.Questions[0].options[0].details = '';
        }
        $scope.refresh();
        setTimeout(function () {
            $scope.updateErrorCount('Declaration');
            $scope.refresh();
        }, 0);
    }
    $scope.checkInsuredNotJuvenile = function () {

        var today = new Date();
        var DOB = new Date($scope.LifeEngageProduct.Insured.BasicDetails.dob);
        var todayYear = today.getFullYear();
        var DOBYear = DOB.getFullYear();
        if ((todayYear - DOBYear) > 18) {
            $scope.notJuvenile = true;
        } else if ((todayYear - DOBYear) == 18) {
            if ((today.getMonth() - DOB.getMonth()) > 0) {
                $scope.notJuvenile = true;
            } else if ((today.getMonth() - DOB.getMonth()) == 0) {
                if ((today.getDate() - DOB.getDate()) >= 0) {
                    $scope.notJuvenile = true;
                } else {
                    $scope.notJuvenile = false;
                }
            } else {
                $scope.notJuvenile = false;
            }
        } else {
            $scope.notJuvenile = false;
        }

    }
    $scope.clearSignature = function (signatureId) {
        $scope.LifeEngageProduct.Declaration.confirmAgentSignature = true;

        if (signatureId == "agentStatementCanvas") {
            $scope.agentStatementButtonDisable = false;
            if (!$scope.LifeEngageProduct.Declaration.agentDeclarationDate && $scope.LifeEngageProduct.Declaration.agentDeclarationDate == '')
                $scope.agentStatementCanvas = false;
            if (signaturePadAgent) {
                signaturePadAgent.clear();
            } else {
                if (!(rootConfig.isDeviceMobile) && $scope.signaturePad) {
                    $scope.signaturePad.clear();
                }
            }
        }
        $scope.refresh();

    };

    $scope.getSignature = function (pageName, successCallback) {
        var isSignSaved = false;
        if ((rootConfig.isDeviceMobile)) {
            EappService.getSignature(EappVariables.EappKeys.TransTrackingID, signature, function (sigdata) {
                EappVariables.Signature = sigdata;
                if (sigdata.length > 0) {
                    for (var i = 0; i < sigdata.length; i++) {
                        if (sigdata[i].documentName.indexOf("Signature") >= 0) {
                            signatureType = sigdata[i].documentName.split("_")[2];
                            if (signatureType.localeCompare(pageName) == 0) {
                                isSignSaved = true;
                                if ($scope.setSaveproposalDisableFlag) {
                                    $scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature = false;
                                    $scope.disableSpecificElementsOnConfirmation();
                                }
                                $scope.$parent.refresh();
                                EappService.convertImgToBase64(sigdata[i].base64string, successCallback, outputFormat);
                            } else {
                                if (i == (sigdata.length - 1)) {
                                    successCallback()
                                }
                            }
                        }
                    }
                } else {
                    successCallback();
                }
            });
        } else {
            var requirementObject = CreateRequirementFile();
            requirementObject.RequirementFile.identifier = EappVariables.EappKeys.TransTrackingID;
            for (var i = 0; i < EappVariables.EappModel.Requirements.length; i++) {
                if (EappVariables.EappModel.Requirements[i].requirementType === "Signature") {
                    for (var j = 0; j < EappVariables.EappModel.Requirements[i].Documents.length; j++) {
                        var value = $scope.DocumentType.split("#");
                        if (EappVariables.EappModel.Requirements[i].requirementSubType == pageName) {
                            isSignSaved = true;
                            requirementObject.RequirementFile.documentName = EappVariables.EappModel.Requirements[i].Documents[j].documentName;
                            requirementObject.RequirementFile.requirementName = EappVariables.EappModel.Requirements[i].requirementName;
                            PersistenceMapping.clearTransactionKeys();
                            EappService.mapKeysforPersistence();
                            var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
                            DataService.getDocumentsForRequirement(transactionObj, getDocsSuccess, $scope.errorCallback);
                        }
                    }
                }
                if (isSignSaved) {
                    break;
                }
            }
            if (!isSignSaved) {
                $scope.clearSignature('agentStatementCanvas');
                $scope.LifeEngageProduct.Declaration.confirmAgentSignature = true;
                $('#Declaration button#okBtnPolicyHolderACRD').attr("disabled", "disabled");
                signaturePadAgent = new SignaturePad(angular.element('#agentStatementCanvas')[0].querySelector("canvas"));
                signaturePadAgent.onEnd = function () {
                    $scope.signDisabled = false;
                    $('#Declaration button#okBtnPolicyHolderACRD').removeAttr("disabled");
                    $('#Declaration button#clearSig').removeAttr("disabled");
                };
            }
        }
    }
    $scope.errorCallback = function () {

    }
    function getDocsSuccess (sigdata) {
        if (sigdata[0].documentName.indexOf("Signature") >= 0) {
            var signatureType = sigdata[0].documentName.split("_")[2];
            if (signatureType.localeCompare("AgentSignatureSummary") == 0) {
                if ($scope.setSaveproposalDisableFlag) {
                    $scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature = false;
                    $scope.disableSpecificElementsOnConfirmation();
                }
                $scope.$parent.refresh();
                EappService.convertImgToBase64(sigdata[0].base64string, function (base64String) {
                    $scope.loadSignature(base64String);
                }, outputFormat);
            }
        }
        $scope.signaturePad.fromDataURL(sigdata[0].base64string);
    }
    $scope.onConfirmError = function () {
        $("#agentSummaryButton").removeAttr('disabled');
        $rootScope.showHideLoadingImage(false);
    }
    $scope.confirmProposal = function (sigData) {
        if ($scope.LifeEngageProduct.Insured.IncomeDetails && !($scope.LifeEngageProduct.Insured.IncomeDetails.annualIncome)) {
            $scope.LifeEngageProduct.Insured.IncomeDetails.annualIncome = "";
        }
        // added for prev button in payment
        // authorisation
        $scope.confirmForPaymentAuth = true;

        var requirementType = "Signature";
        var index = "";
        var requirementName = "Signature";
        var partyIdentifier = "";
        var isMandatory = "";
        var docArray = [];
        var documentType = $scope.DocumentType;

        if ((rootConfig.isDeviceMobile)) {
            EappService.getRequirementFilePath(sigData, index, requirementName, requirementType, documentType, function (filePath) {
                if ((rootConfig.isDeviceMobile) || rootConfig.isOfflineDesktop) {
                    filePath = "../" + filePath;
                }
                EappService.saveRequirement(filePath, requirementType, partyIdentifier, isMandatory, docArray, documentType, function () {
                    if ($scope.canvasId == "agentStatementCanvas") {
                        /*$scope.agentStatementCanvas=true;
                        $scope.agentStatementButtonDisable =true;*/
                        $scope.acrConfirmed = true;
                        $scope.signDisabled = true;
                        $('#Declaration button#confirmSig').attr("disabled", "disabled");
                        $('#Declaration button#clearSig').attr("disabled", "disabled");
                        $('input[type="date"]').attr("disabled", "disabled");
                    }
                    $("#agentSummaryButton").removeAttr('disabled');
                    $rootScope.showHideLoadingImage(false);
                    $scope.refresh();
                }, $scope.onConfirmError);
            }, $scope.onConfirmError);
            $scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature = false;
        } else {
            var documentTypeChk = "AgentSignatureSummary";
            for (var o = $scope.LifeEngageProduct.Requirements.length - 1; o >= 0; o--) {
                if (documentTypeChk === $scope.LifeEngageProduct.Requirements[o].requirementSubType) {
                    $scope.LifeEngageProduct.Requirements.splice(o, 1);
                }
            }
            EappService.saveRequirement(sigData, requirementType, partyIdentifier, isMandatory, docArray, documentType, function () {
                $scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature = false;
                $scope.signDisabled = false;
                $scope.acrConfirmed = false;
                if ($scope.canvasId == "agentStatementCanvas") {
                    /*$scope.agentStatementCanvas=true;
                    $scope.agentStatementButtonDisable =true;*/
                    $scope.acrConfirmed = true;
                    $scope.signDisabled = true;
                    $('#Declaration button#confirmSig').attr("disabled", "disabled");
                    $('#Declaration button#clearSig').attr("disabled", "disabled");
                    $('input[type="date"]').attr("disabled", "disabled");
                }
                $("#agentSummaryButton").removeAttr('disabled');
                $rootScope.showHideLoadingImage(false);
            }, function () {
                $scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature = false;
                $scope.onConfirmError();
            });
        }
        $scope.disableSpecificElementsOnConfirmation();
    };

    $rootScope.validationBeforesave = function (value, successcallback) {
        $scope.validateBeforeSaveDetails(value, successcallback);
    };

    $scope.validateBeforeSaveDetails = function (value, successcallback) {

        /*$scope.LifeEngageProduct.LastVisitedUrl	= "5,0,'Payment',false,'tabsinner16',''";
        if ((rootConfig.isDeviceMobile)){
        	successcallback();
        }else{
        	$scope.$parent.LifeEngageProduct.Declaration.confirmSignature=false;
        	successcallback();
        }*/

        /** To validate partner code & commission code for ACR **/
        if ($scope.LifeEngageProduct.AgentInfo.commissionAgent && $scope.LifeEngageProduct.Declaration.Questions[23].option == 'Yes') {
            PersistenceMapping.clearTransactionKeys();
            var transactionObj = PersistenceMapping.mapScopeToPersistence({});
            //transactionObj.Key11=  $scope.LifeEngageProduct.AgentInfo.agentId;
            transactionObj.Key11 = $scope.LifeEngageProduct.AgentInfo.commissionAgent;
            AgentService.retrieveAgentProfile(transactionObj, $scope.onRetrieveAgentProfileSuccess, $scope.onRetrieveAgentProfileError);

        } else {
            if ($scope.agentDetail.agentType == 'IOIS') {
                $scope.LifeEngageProduct.Declaration.confirmAgentSignature = $scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature;
                $scope.partnerCodeTrue = false;
                if ($scope.LifeEngageProduct.AgentInfo.partnerCode) {
                    $scope.mappedBranches = UserDetailsService.getMappedBranches();
                    for (var i = 0; i < $scope.mappedBranches.length; i++) {
                        if ($scope.mappedBranches[i].branchCode == $scope.LifeEngageProduct.AgentInfo.partnerCode) {
                            $scope.partnerCodeTrue = true;
                            $scope.LifeEngageProduct.LastVisitedUrl = "5,0,'Payment',false,'tabsinner16',''";
                            if ((rootConfig.isDeviceMobile)) {
                                $scope.editEappQuestions();
                            } else {
                                $scope.editEappQuestions();
                            }
                            break;
                        } else {
                            $scope.partnerCodeTrue = false;
                        }
                    }
                }
                if ($scope.partnerCodeTrue == false) {
                    // when partner code is not matching
                    $('#Declaration button#confirmSig').removeAttr("disabled");
                    $('#Declaration button#clearSig').removeAttr("disabled");
                    $('input[type="date"]').removeAttr("disabled");
                    $("#agentSummaryButton").removeAttr('disabled');
                    $rootScope.showHideLoadingImage(false);
                    $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.partnerCodeInvalid"), translateMessages($translate, "fna.ok"));

                }
            } else {
                $scope.LifeEngageProduct.Declaration.confirmAgentSignature = $scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature;
                $scope.LifeEngageProduct.LastVisitedUrl = "5,0,'Payment',false,'tabsinner16',''";
                if ((rootConfig.isDeviceMobile)) {
                    $scope.editEappQuestions();
                } else {
                    $scope.editEappQuestions();
                }
            }
        }
    }

    $scope.onRetrieveAgentProfileSuccess = function (data) {
        $rootScope.showHideLoadingImage(false);
        if (data.AgentDetails.length == 0) {
            $('#Declaration button#confirmSig').removeAttr("disabled");
            $('#Declaration button#clearSig').removeAttr("disabled");
            $('input[type="date"]').removeAttr("disabled");
            $("#agentSummaryButton").removeAttr('disabled');
            $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.commissionCodeInvalid"), translateMessages($translate, "fna.ok"));
        } else {
            $scope.LifeEngageProduct.AgentInfo.commissionAgentName = data.AgentDetails.agentName;
            if ($scope.agentDetail.agentType == 'IOIS' && data.AgentDetails.status == "Active") {
                if (data.AgentDetails.agentType == "IOIS") {
                    $scope.partnerCodeTrue = false;
                    $scope.mappedBranches = UserDetailsService.getMappedBranches();
                    for (var i = 0; i < $scope.mappedBranches.length; i++) {
                        if ($scope.mappedBranches[i].branchCode == $scope.LifeEngageProduct.AgentInfo.partnerCode) {
                            $scope.partnerCodeTrue = true;
                            $scope.LifeEngageProduct.LastVisitedUrl = "5,0,'Payment',false,'tabsinner16',''";
                            if ((rootConfig.isDeviceMobile)) {
                                $scope.editEappQuestions();
                            } else {
                                $scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature = false;
                                $scope.editEappQuestions();
                            }
                            break;
                        } else {
                            $scope.partnerCodeTrue = false;
                        }
                    }
                    if ($scope.partnerCodeTrue == false) {
                        // when partner code is not matching
                        $("#agentSummaryButton").removeAttr('disabled');
                        $rootScope.showHideLoadingImage(false);
                        $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.partnerCodeInvalid"), translateMessages($translate, "fna.ok"));
                    }
                } else {
                    $("#agentSummaryButton").removeAttr('disabled');
                    $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.commissionCodeInvalid"), translateMessages($translate, "fna.ok"));
                }
            } else if ($scope.agentDetail.agentType == 'IOIS' && data.AgentDetails.status != "Active") {
                $("#agentSummaryButton").removeAttr('disabled');
                $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.commissionCodeInvalid"), translateMessages($translate, "fna.ok"));
            } else {
                $scope.LifeEngageProduct.LastVisitedUrl = "5,0,'Payment',false,'tabsinner16',''";
                if ((rootConfig.isDeviceMobile)) {
                    $scope.editEappQuestions();
                } else {
                    $scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature = false;
                    $scope.editEappQuestions();
                }
            }
        }
    }
    $scope.editEappQuestions = function () {
        if ($scope.eAppParentobj.nextPage.indexOf('SPAJsummary') >= 0) {
            $rootScope.showHideLoadingImage(true, 'calculationProgress', $translate);
            $scope.Save();
            //added if condition for the fix of fields getting disabled in other screens while proceeding without signing
            if ($scope.$parent.LifeEngageProduct.Declaration.confirmSignature == false && $scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature == false) {
                $('input[type="text"]').attr("disabled", "disabled");
                $('select').not('.languageSelector').attr("disabled", "disabled");
                $('input[type="radio"]').not('input[name="language"]').prop("disabled", true);
                $('.iradio').not('[id = "languageColor"]').addClass('disabled');
                $('input[type="checkbox"]').attr("disabled", "disabled");
                $('.icheckbox').addClass('disabled');
            }
        } else {
            $scope.LifeEngageProduct.Declaration.confirmAgentSignature = $scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature;
            $scope.$parent.LifeEngageProduct = angular.copy($scope.LifeEngageProduct); //Fix for: ACR signature is missing
            $scope.Save();
            if ($scope.$parent.LifeEngageProduct.Declaration.confirmSignature == false && $scope.$parent.LifeEngageProduct.Declaration.confirmAgentSignature == false) {
                $('input[type="text"]').attr("disabled", "disabled");
                $('select').not('.languageSelector').attr("disabled", "disabled");
                $('input[type="radio"]').not('input[name="language"]').prop("disabled", true);
                $('.iradio').not('[id = "languageColor"]').addClass('disabled');
                $('input[type="checkbox"]').attr("disabled", "disabled");
                $('.icheckbox').addClass('disabled');
            }
        }
    };

    $scope.onRetrieveAgentProfileError = function (data) {
        $("#agentSummaryButton").removeAttr('disabled');
        $rootScope.showHideLoadingImage(false);
        $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.commisionAgntError"), translateMessages($translate, "fna.ok"));
    }
    var load = $scope.$parent.$on('Declaration', function (event, args) {

        $scope.selectedTabId = args[0];
        if ($scope.LifeEngageProduct.LastVisitedIndex && args[1]) {
            $scope.LifeEngageProduct.LastVisitedIndex = args[1];
        }
        $scope.Initialize();
        $rootScope.validationBeforesave = function (value, successcallback) {
            $scope.validateBeforeSaveDetails(value, successcallback);
        };

    });
    $scope.confirmDeclaration = function (canvasId) {
        $("#agentSummaryButton").attr("disabled", "disabled");
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
        $scope.canvasId = canvasId;
        $scope.LifeEngageProduct.Declaration.agentSignDate = new Date();
        $scope.updateErrorCount('Declaration');
        if (canvasId == "agentStatementCanvas") {
            if ((rootConfig.isDeviceMobile)) {
                if ($scope.errorCount > 0) {
                    $("#agentSummaryButton").removeAttr('disabled');
                    $rootScope.showHideLoadingImage(false);
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                        translateMessages($translate, "enterMandatoryFieldsForeApp"),
                        translateMessages($translate, "fna.ok"));
                } else {
                    if (signaturePadAgent && !signaturePadAgent.isEmpty()) {
                        $scope.LifeEngageProduct.Declaration.agentSignDate = UtilityService.getFormattedDate();
                        $scope.spajAgentDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.agentSignDate), $scope.appDateTimeFormat);
                        $scope.confirmProposal(signaturePadAgent.toDataURL());
                    } else {
                        $("#agentSummaryButton").removeAttr('disabled');
                        $rootScope.showHideLoadingImage(false);
                        $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.declarationAgreedMessage"), translateMessages($translate, "fna.ok"));
                    }
                }
            }
            //Bug fix for Web - Able to proceed without entering sign
            else {
                if ($scope.errorCount > 0) {
                    $("#agentSummaryButton").removeAttr('disabled');
                    $rootScope.showHideLoadingImage(false);
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                        translateMessages($translate, "enterMandatoryFieldsForeApp"),
                        translateMessages($translate, "fna.ok"));
                } else {
                    if (signaturePadAgent && !signaturePadAgent.isEmpty()) {
                        $scope.LifeEngageProduct.Declaration.agentSignDate = UtilityService.getFormattedDate();
                        $scope.spajAgentDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.agentSignDate), $scope.appDateTimeFormat);
                        $scope.confirmProposal(signaturePadAgent.toDataURL());
                    } else {
                        $("#agentSummaryButton").removeAttr('disabled');
                        $rootScope.showHideLoadingImage(false);
                        $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.declarationAgreedMessage"), translateMessages($translate, "fna.ok"));
                    }
                }

            }
        }
    };

    $scope.updateErrorCount = function (id) {
        $rootScope.updateErrorCount(id);
        $scope.selectedTabId = id;
        $scope.eAppParentobj.errorCount = $scope.errorCount;
    }
    $scope.showErrorPopup = function (id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
    }
    $scope.summaryEdit = function (linkTab, linkSubTab) {
        // isRootNav will check for whether the nav
        // click is coming from direct
        // main root tabs/subtabs
        if (!$scope.isSPAJNoAllocated) {
            $scope.isRootNav = false;
            $scope.refresh();
            $("#registration_tabs a[rel='" + linkTab + "']")
                .parent('div').trigger('click');

            $('#' + linkTab).find(
                "li a[rel='" + linkSubTab +
                "']").parent('li').trigger(
                'click');
        }

    };

    //Used to set the date for RDS user in Summary screen
    $scope.setDateForRDSUser = function (date) {
        if ($scope.isRDSUser) {
            var temp = getFormattedDateSummary(date);
            $scope.LifeEngageProduct.Declaration.agentSignDate = temp;
        }
    };

    /*
     * Used to show or hide the questionnaire tab under summary based on additonal questions selected or not.---starts
     */
    function summayQuestionnaireTabShowHide() {
        $scope.arrayIndex = 0;
        var questionnaireCheckValues = [$scope.LifeEngageProduct.isProposerForeigner,
			                                $scope.LifeEngageProduct.hasInsuredQuestions,
			                                $scope.LifeEngageProduct.hasBeneficialOwnerForm];

        if ($scope.LifeEngageProduct.hasInsuranceEngagementLetter == true || $scope.LifeEngageProduct.hasInsuranceEngagementLetter == "true") {
            return true;
        } else {
            var returnValue;
            loopArrayQuestionnaire(questionnaireCheckValues[$scope.arrayIndex], function (dataReturn) {
                returnValue = dataReturn;
            });
            return returnValue;
        }
    };

    function loopArrayQuestionnaire(arrayValue, successcallback) {
        var questionnaireCheckValues = [$scope.LifeEngageProduct.isProposerForeigner,
			                                $scope.LifeEngageProduct.hasInsuredQuestions,
			                                $scope.LifeEngageProduct.hasBeneficialOwnerForm];
        checkLength(arrayValue, function (returnValue) {
            if (returnValue) {
                successcallback(true);
            } else {
                if ($scope.arrayIndex == questionnaireCheckValues.length - 1) {
                    successcallback(false);
                } else {
                    $scope.arrayIndex++;
                    loopArrayQuestionnaire(questionnaireCheckValues[$scope.arrayIndex], successcallback);
                }
            }
        });
    };

    /*
     * Used to show or hide the questionnaire tab under summary based on additonal questions selected or not.---ends
     */

    /*$scope.ShowDate = function(model, format) {
    	var returnVal;
    	var parentModel = model.split('.');
    	var isParentModel = parentModel[0]+"."+parentModel[1];
    	if(eval ("$scope."+isParentModel)){
    		if(!(isEmptyOrNull(format))){
    		
    			for (var i = 0; i < $scope.LifeEngageProduct.Beneficiaries.length; i++) {
    					if(i==format){
    						returnVal=$scope.LifeEngageProduct.Beneficiaries[i].BasicDetails.dob;
    						break;
    					}
    			}
    		}
    		else{
    			eval ("returnVal=$scope." + model);
    		}
    		returnVal = new Date(Date.parse(returnVal))
    	
    		return returnVal != 'Invalid Date' ? (parseInt(returnVal
    			.getDate()))
    			+ "-"
    			+ (parseInt(returnVal.getMonth()) + 1)
    			+ "-" + returnVal.getFullYear()
    			: "";
    	}
    };*/

    /*$scope.showOptions = function(model) {
    	if(model){
    		var returnVal;
    		var parentModel = model.split('.');
    		var isParentModel = parentModel[0]+"."+parentModel[1];
    		var isParentModel1 = parentModel[0]+"."+parentModel[1];
    		var isParentModel2 = parentModel[0]+"."+parentModel[1]+"."+parentModel[2];

    		if(eval ("$scope."+isParentModel1) && eval ("$scope."+isParentModel2)) {
    			if(eval ("$scope."+model)!=undefined){
    		   		eval ("returnVal=$scope." + model);
    			    	if(returnVal == 'Yes'){
    				    	return true;
    			   		}
    			    else{
    						return false;
    			  		}
    		   	}
    		   	else{
    		   		return false;
    		   	}
    		}	
    	}			  
    		
    };*/


    var agentStatementCanvasWatch = $scope.$watch('LifeEngageProduct.Declaration.clientdeclarationAgreed', function (value) {
        if ($scope.selectedTabId == "Declaration") {
            if (value == 'Yes' && !$scope.LifeEngageProduct.Declaration.agentSignDate && $scope.LifeEngageProduct.Declaration.agentSignDate == '') {
                $scope.agentStatementCanvas = false;
            } else {
                if (signaturePadAgent) {
                    signaturePadAgent.clear();
                    $scope.agentStatementCanvas = true;
                } else {
                    if (!(rootConfig.isDeviceMobile) && $scope.signaturePad) {
                        $scope.signaturePad.clear();
                    }
                }
            }
            $scope.refresh();
        }
    }, true);


    $scope.$on("$destroy", function () {
        if (this != null && typeof this !== 'undefined') {
            agentStatementCanvasWatch();
            if (this.$$destroyed) return;
            while (this.$$childHead) {
                this.$$childHead.$destroy();
            }
            if (this.$broadcast) {
                this.$broadcast('$destroy');
            }
            this.$$destroyed = true;
            //$timeout.cancel( timer );  
        }
    })
}