﻿'use strict';

storeApp.controller('HomeController', [
		'$rootScope',
		'$scope',
		'$compile',
		'$routeParams',
		'DataService',
		'LookupService',
		'DocumentService',
		function($rootScope, $scope, $compile, $routeParams, DataService,
				LookupService, DocumentService) {
			$scope.prevButtonName = 'Home Controller';

		} ]);

/*
 * Recursively merge properties of two objects
 */
function MergeRecursive(obj1, obj2) {
	for ( var p in obj2) {
		try {
			// Property in destination object set; update its value.
			if (obj2[p].constructor == Object) {
				obj1[p] = MergeRecursive(obj1[p], obj2[p]);
			} else {
				obj1[p] = obj2[p];
			}
		} catch (e) {
			// Property in destination object not set; create it and set its
			// value.
			obj1[p] = obj2[p];
		}
	}
	return obj1;
}