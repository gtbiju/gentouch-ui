'use strict';
storeApp.controller('GLI_PaymentSplitController', GLI_PaymentSplitController);
GLI_PaymentSplitController.$inject = ['$rootScope', '$scope', 'DataService', 'GLI_DataService', 'LookupService', 'DocumentService','$sce', 'ProductService', '$compile', '$routeParams','$window', '$location', '$translate', '$debounce', 'UtilityService', 'PersistenceMapping', 'AutoSave', 'globalService', 'EappVariables', 'EappService', 'PaymentService', '$timeout', 'GLI_EappService', 'UserDetailsService'];

function GLI_PaymentSplitController($rootScope, $scope, DataService, GLI_DataService, LookupService, DocumentService,$sce, ProductService, $compile, $routeParams,$window, $location, $translate, $debounce, UtilityService, PersistenceMapping, AutoSave, globalService, EappVariables, EappService, PaymentService, $timeout, GLI_EappService, UserDetailsService) {
    $scope.paymentinfo;
	
    $scope.numberPattern = rootConfig.numberPattern;
    $scope.alphanumericPattern = rootConfig.alphanumericPattern;
    $scope.showModel = false;
    $scope.isSingleTopup = false;
    $scope.payerNames = [];
    $scope.popupMessage = "";
    $scope.supportingbanks = ["BCA", "BNI", "Niaga"];
    $scope.creditCardDetail = {};
    $scope.creditCardDetail.creditCardNumberone = "";
    $scope.creditCardDetail.creditCardNumbertwo = "";
    $scope.creditCardDetail.creditCardNumberthree = "";
    $scope.creditCardDetail.creditCardNumberfour = "";
    $scope.mandiriCreditCardNumber1 = "";
    $scope.mandiriCreditCardNumber2 = "";
    $scope.mandiriCreditCardNumber3 = "";
    $scope.mandiriCreditCardNumber4 = "";
    $scope.onlineMessage = "";
    $rootScope.selectedPage = "step2Payment";
    $scope.isDeviceMobile = rootConfig.isDeviceMobile;
    $scope.submitPayment = false;
	$scope.isPopupDisplayed = false;
	$scope.showErrorCount = true;
    $scope.dynamicErrorMessages = [];
    $scope.dynamicErrorCount = 0;
	$scope.splitoneinput=true;
	$scope.splittwoinput=false;
	$scope.singlepaymentInput=true;
	$scope.payConfirmDetails=false;
	$scope.splittwoPayDetails=false;
	$scope.payConfirmaction=false;
	$scope.splitTwopayConfirmDetails=false;
	$rootScope.proceedpaymentbtn=false;
	$rootScope.proceedstep3=false;
	$rootScope.splitOptionSelected;
	$rootScope.showSplitTwo=false;
	$scope.DynamicGenerateETRpopup=false;	
	$scope.manualGenerateETRpopup=false;
	$scope.disablepaynow=false;
	$scope.splitTwodisablepaynow=false;
	$scope.payment3ProceedDisable = false;
	$scope.showSplitOne=false;
	$rootScope.showSplitTwo=false;
	$scope.showProceedForGAOBranchAdmin=false;	
	$scope.payCheque=false;
	$scope.updateETRnumber=false;
	$scope.paynowEmail=false;
	$scope.paynowEmailWeb=false;
	$scope.updateETRBtn=false;	
	$('#paymentButtonETRBck').attr('disabled', 'disabled');	
	$scope.LifeEngageProduct.ETRgeneration = '';
	$rootScope.etrGenerationSelected = false;
	
	var now = new Date();
	var currentDate= now.toUTCString();
	$scope.eAppParentobj.CurrentPage = "step2Payment";
    $scope.statusCheck="";
	$scope.paymentDone=false;
	navigator.onLine;
	$scope.alphabeticPattern = rootConfig.alphabeticPattern;
	
	// add class to active the split tab
	var splitone = angular.element(document.querySelector(".splittabspayone")).addClass("splittabspayoneactive");
	var splitoneSingle = angular.element(document.querySelector(".tabspayone")).addClass("tabspayoneactive");
	var splitTwoActive = angular.element(document.getElementsByClassName("button.splittabspaytwoactive"));
	angular.element(document.querySelector("#step2Payment .accordion-groups")).addClass("step2paymentvalidate");
	angular.element(document.querySelector(".paddingLeftprem")).addClass("paymentTypeWordbreak");
	   
		
	//condition for payment save order
	if($scope.LifeEngageProduct.Payment.PaymentInfo){
		 $scope.LifeEngageProduct.Payment.PaymentInfo = $scope.LifeEngageProduct.Payment.PaymentInfo.sort(function (a,b) {
			 if(a.paySplit===""){
				 a.paySplit="T";
			 }
			 if(b.paySplit===""){
				 b.paySplit="T";
			 }
			 var va = (a.paySplit === null) ? "" : "" + a.paySplit,
						vb = (b.paySplit === null) ? "" : "" + b.paySplit;
			 return va > vb ? 1 : ( va === vb ? 0 : -1 );
		});
	}
	//disable popuptime after click
		angular.element(document).on('click', function () {
		  angular.element(document.querySelector(".bootstrap-timepicker-widget.dropdown-menu.open")).addClass("closeTimepopup");
		});
		
		$(".step2paymentvalidate").click(function() {
		angular.element(document.querySelectorAll("#PaysinTime")).blur();
		});

		$("#PaysinTime").click( function(e) {
			e.stopPropagation(); // this stops the event from bubbling up to the body
			 angular.element(document.querySelector(".bootstrap-timepicker-widget.dropdown-menu.open")).removeClass("closeTimepopup");
			 angular.element(document.querySelector("#PaysinTime")).focus();
		});
		
		$("#PaysinTimeGAO").click( function(e) {
			e.stopPropagation(); // this stops the event from bubbling up to the body
			 angular.element(document.querySelector(".bootstrap-timepicker-widget.dropdown-menu.open")).removeClass("closeTimepopup");
		});
		
	//disable popup date after click
		// angular.element(document).on('click', function () {
		//   angular.element(document.querySelector(".datepicker")).addClass("closeDatepopup");
		// });

		// $("#payinDates").click( function(e) {
		// 	e.stopPropagation(); // this stops the event from bubbling up to the body
		// 	 angular.element(document.querySelector(".datepicker")).removeClass("closeDatepopup");
		// });
	//Redirect from step3 to document upload 
	$rootScope.redirectDocupload = function () {
		//EappVariables.EappKeys.Key34 = "PaymentCompleted";
		$scope.save();
		$scope.LifeEngageProduct.LastVisitedUrl = "5,0,'Payment',false,'tabsinner16',''"; 
	    $scope.eAppParentobj.CurrentPage = "";
	        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
	            $scope.LifeEngageProduct.LastVisitedIndex = "5,0";
	            $scope.$parent.LifeEngageProduct.LastVisitedIndex = "5,0";
	        }	
		angular.element(document.querySelector("#PaymentDetailsSubTab .selector-icon li:nth-child(3)")).addClass("activated");
		angular.element(document.querySelector("#PaymentDetailsSubTab .selector-text li:nth-child(3)")).addClass("activated");
		$scope.paintView(6,0,'DocumentUpload',true,'DocumentUpload','');
	}
	// redirect back button to step 1
	$rootScope.step1ScreenLoad = function () {
		angular.element(document.querySelector("#PaymentDetailsSubTab .selector-icon li:nth-child(1)")).removeClass("activated");
		angular.element(document.querySelector("#PaymentDetailsSubTab .selector-text li:nth-child(1)")).removeClass("activated");
		$scope.paintView(5,1,'Payment',true,'tabsinner16','');
	}	
	//redirect step 2 to step 3
	$rootScope.step3ScreenLoad = function () {
		if ($scope.LifeEngageProduct.ETRgeneration == 'manualETR' && $scope.LifeEngageProduct.splitOneamount > 0) {
			var arrayObj = {"tr_no":""};
			arrayObj.tr_no= $scope.LifeEngageProduct.splitTwoEtrNumber;
			$scope.etrSuccessCallback(arrayObj);
		}
		angular.element(document.querySelector("#PaymentDetailsSubTab .selector-icon li:nth-child(3)")).removeClass("activated");
		angular.element(document.querySelector("#PaymentDetailsSubTab .selector-text li:nth-child(3)")).removeClass("activated");
		
		angular.element(document.querySelector("#PaymentDetailsSubTab .selector-icon li:nth-child(2)")).addClass("activated");
		angular.element(document.querySelector("#PaymentDetailsSubTab .selector-text li:nth-child(2)")).addClass("activated");
		EappVariables.EappKeys.Key34 = "PaymentCompleted";
		/*EappService.saveTransaction(function () {
			$rootScope.showHideLoadingImage(false);
			
		}, $scope.onConfirmError, false);
		*/

		$rootScope.showHideLoadingImage(true, 'eapp.SavingInprogress', $translate);
		EappService.saveTransaction($scope.onSaveCallback,$scope.onConfirmError, false);
		$rootScope.showHideLoadingImage(false);
		$scope.paintView(5,3,'step3Payment',true,'tabsinner16','');
		$rootScope.etrGenerationSelected=false;
	}
	
	
	

	$rootScope.splitTwoScreenLoad = function () {	
			if ($scope.LifeEngageProduct.ETRgeneration == 'manualETR') {
				var arrayObj = {"tr_no":""};
				arrayObj.tr_no= $scope.LifeEngageProduct.splitTwoEtrNumber;
				$scope.etrSuccessCallback(arrayObj);
		}
			angular.element(document.querySelector(".site-overlay")).addClass("hideSplitOne");
			//$scope.initailPaintView(5,1,'Payment',true,'tabsinner16','');
			$timeout(function () {
				$scope.initailPaintView(5,2,'step2Payment',true,'tabsinner18','');
				$rootScope.showSplitTwo();
				angular.element(document.querySelector(".site-overlay")).removeClass("hideSplitOne");
				$scope.validateSplitPaymnet();
				if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
				    $scope.customPaymentOption=angular.copy($rootScope.PaymentOption);        
				    $scope.customPaymentOption.splice(5,1);    
				    $scope.customPaymentOption.splice(2,1);   
				    $scope.customPaymentOption.splice(1,1);
				    $scope.customPaymentOption.splice(0,1);    
				}else{
				    $scope.customPaymentOption=angular.copy($rootScope.PaymentOption);      
				}
				$scope.LifeEngageProduct.ETRgeneration = '';				
				$rootScope.etrGenerationSelected = false;
				$('#step2Payment select').removeAttr("disabled");
		},500);
	}

	$scope.validateSplitPaymnet = function () {
		// validate total amount to enter remaining amount value in split tab
			if($scope.LifeEngageProduct.Product.policyDetails.premiumFrequency == 'Monthly' && $rootScope.splitOptionSelected==true)
			{
				if($scope.LifeEngageProduct.Payment.RenewalPayment.paymentType != 'CreditCard'){
					$scope.LifeEngageProduct.splitOneAmountValidate = $scope.LifeEngageProduct.Product.premiumSummary.totalPremium*2;
				} else {
					$scope.LifeEngageProduct.splitOneAmountValidate = $scope.LifeEngageProduct.Product.premiumSummary.totalPremium;
				}
			}
			if($scope.LifeEngageProduct.Product.policyDetails.premiumFrequency !='Monthly' && $rootScope.splitOptionSelected==true)
			{
				$scope.LifeEngageProduct.splitOneAmountValidate = $scope.LifeEngageProduct.Product.premiumSummary.totalPremium;
			}
		
		//Condition for split payment display
		if($scope.LifeEngageProduct.Payment.PaymentInfo.length > 0) {
			if($scope.LifeEngageProduct.Payment.PaymentInfo[0].splitPaymentTst=="No" || $scope.LifeEngageProduct.Payment.PaymentInfo[1].splitPaymentTst == "No"){
				$rootScope.splitOptionSelected=false;
				if($scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit == "SplitOne") {
				//if(($scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit == "SplitOne") && ($scope.LifeEngageProduct.Payment.PaymentInfo[0].premiumAmt==$scope.LifeEngageProduct.Product.premiumSummary.totalPremium)){
				angular.element(document.querySelector(".tabspayone")).removeClass("tabspayoneactive");
				$scope.singlepaymentInput=false;
				$scope.paymentDone=true;
				$scope.splittwoinput=false;
				angular.element(document.querySelector(".splittabspaytwo")).removeClass("splittabspaytwoactive");
				$rootScope.proceedstep3=true;
				$scope.paymentbackbutton = false;
				}
			}
		}
		
		if($scope.LifeEngageProduct.Payment.PaymentInfo.length > 0) {
			
			/*// the parsing logic in Product and Payment value is different - hence trimming the case where decimal value has no numeric factor 
			if (typeof $scope.LifeEngageProduct.Payment.PaymentInfo[0] !='undefined' && $scope.LifeEngageProduct.Payment.PaymentInfo[0].premiumAmt != null && $scope.LifeEngageProduct.Payment.PaymentInfo[0].premiumAmt != ""){
			 	if (String($scope.LifeEngageProduct.Payment.PaymentInfo[0].premiumAmt).endsWith(".00")){
			 		var premium = String($scope.LifeEngageProduct.Payment.PaymentInfo[0].premiumAmt);
			 		if (premium.substring(0, premium.indexOf(".00")) == $scope.LifeEngageProduct.Product.premiumSummary.totalPremium) {			 		
			 			$scope.LifeEngageProduct.Payment.PaymentInfo[0].premiumAmt = premium.substring(0, premium.indexOf(".00"));
			 		}
			 	}
			}*/

			if($scope.LifeEngageProduct.Payment.PaymentInfo[0].splitPaymentTst=="Yes" || $scope.LifeEngageProduct.Payment.PaymentInfo[1].splitPaymentTst=="Yes"){
				$rootScope.splitOptionSelected=true;
				if((typeof $scope.LifeEngageProduct.Payment.PaymentInfo[0] !='undefined' && typeof $scope.LifeEngageProduct.Payment.PaymentInfo[1] !='undefined' && $scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit == "SplitOne" && $scope.LifeEngageProduct.Payment.PaymentInfo[1].paySplit == "SplitTwo") || $scope.LifeEngageProduct.Payment.PaymentInfo[0].premiumAmt==$scope.LifeEngageProduct.Product.premiumSummary.totalPremium){
					/*Disabled the split one*/
					angular.element(document.querySelector(".splittabspayone")).removeClass("splittabspayoneactive");
					$scope.splitoneinput=false;
					/*Disabled the split two*/
					$scope.splittwoinput=false;
					angular.element(document.querySelector(".splittabspaytwo")).removeClass("splittabspaytwoactive");
					/*message*/
					$scope.paymentDone=true;
					$rootScope.proceedstep3=true;
					$scope.paymentbackbutton = false;
					}
				else if(typeof $scope.LifeEngageProduct.Payment.PaymentInfo[0] !='undefined' && $scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit == "SplitOne" && $scope.LifeEngageProduct.splitOneAmountValidate==$scope.LifeEngageProduct.Payment.PaymentInfo[0].premiumAmt){
					/*Disabled the split one*/
					angular.element(document.querySelector(".splittabspayone")).removeClass("splittabspayoneactive");
					$scope.splitoneinput=false;
					/*Disabled the split two*/
					$scope.splittwoinput=false;
					angular.element(document.querySelector(".splittabspaytwo")).removeClass("splittabspaytwoactive");
					/*message*/
					$scope.paymentDone=true;
					$rootScope.proceedstep3=true;
					$scope.paymentbackbutton = false;
				}
				else if(typeof $scope.LifeEngageProduct.Payment.PaymentInfo[0] !='undefined' && $scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit == "SplitOne" && $scope.LifeEngageProduct.Payment.PaymentInfo[1].splitPaymentTst=="" ){
					/*Disabled the split one*/
					angular.element(document.querySelector(".splittabspayone")).removeClass("splittabspayoneactive");
					$scope.splitoneinput=false;
					/*Enabled the split two*/
					$scope.splittwoinput=true;
					$scope.LifeEngageProduct.splitOneamount = $scope.LifeEngageProduct.splitOneAmountValidate-$scope.LifeEngageProduct.Payment.PaymentInfo[0].premiumAmt;
					$scope.LifeEngageProduct.splitOneOption='';
					angular.element(document.querySelector(".splittabspaytwo")).addClass("splittabspaytwoactive");
					$scope.updateETRBtn=false;	
					$('#paymentButtonETRBck').attr('disabled', 'disabled');	
					$scope.LifeEngageProduct.ETRgeneration = '';
					$rootScope.etrGenerationSelected = false;
				} else {
					/*Enabled the split one*/
					angular.element(document.querySelector(".splittabspayone")).addClass("splittabspayoneactive");
					$scope.splitoneinput=true;
					$scope.LifeEngageProduct.splitOneamount=''
					$scope.LifeEngageProduct.splitOneOption='';
					/*Disabled the split two*/
					$scope.splittwoinput=false;
				}
			}
			
		}
	}

	$scope.validateSplitPaymnet();
	//split one tab display	
	$scope.showSplitOne = function () {	
		$scope.splitoneinput=true;
		$scope.splittwoinput=false;
		$scope.splittwoPayDetails=false;
		$scope.payConfirmDetails=false;
		angular.element(document.querySelector(".splittabspayone")).addClass("splittabspayoneactive");
		angular.element(document.querySelector(".splittabspaytwo")).removeClass("splittabspaytwoactive");
	}
	//Show showSplitOnepayment tab display
	$scope.showSplitOnepayment = function () {
		$scope.singlepaymentInput=true;
		$scope.payConfirmDetails=false;
		$scope.splittwoPayDetails=false;
	}
	//Get premium amount details to display in split one single payment
	if($scope.LifeEngageProduct.Product.policyDetails.premiumFrequency == 'Monthly' && $rootScope.splitOptionSelected==false)
	{
		if($scope.LifeEngageProduct.Payment.RenewalPayment.paymentType != 'CreditCard'){
			$scope.LifeEngageProduct.splitOneamount = $scope.LifeEngageProduct.Product.premiumSummary.totalPremium*2;
		} else {
			$scope.LifeEngageProduct.splitOneamount = $scope.LifeEngageProduct.Product.premiumSummary.totalPremium;
		}
	}
	if($scope.LifeEngageProduct.Product.policyDetails.premiumFrequency!='Monthly' && $rootScope.splitOptionSelected==false)
	{
		$scope.LifeEngageProduct.splitOneamount = $scope.LifeEngageProduct.Product.premiumSummary.totalPremium;
	}
		

	//split one confirm action
	$scope.paysplitOneconfirm = function () {
		//split no condition
		if((UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO && ($scope.LifeEngageProduct.splitOneOption=="PayInCash" || $scope.LifeEngageProduct.splitOneOption=="PayInCheque"))|| UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
			$scope.showProceedForGAOBranchAdmin=true;
			$scope.splitTwoConfirmGAO=true;
			$scope.proccedForGAOBranchADmin=true;
			$timeout(function () {
             $scope.updateErrorCount($scope.selectedTabId);
         }, 50);
		}else{
			$scope.showProceedForGAOBranchAdmin=false;
		}
		if($rootScope.splitOptionSelected==false){
			//no network for online payment
			if(navigator.onLine==false){
				if(($scope.LifeEngageProduct.splitOneOption=="SendPaymentURL") || ($scope.LifeEngageProduct.splitOneOption=="CreditCard") || ($scope.LifeEngageProduct.splitOneOption=="Ebanking")){
					
					$scope.payConfirmDetails=false;
					$rootScope.lePopupCtrl.showSuccess(translateMessages($translate,
						"successHeader"), translateMessages($translate,
						"eapp_vt.checkOnlineForPayment"), translateMessages($translate,
						"fna.ok"));					
				}
			}else{
				// paynow and payment status condition
				if(($scope.LifeEngageProduct.splitOneOption!='')&&($scope.LifeEngageProduct.splitOneOption!=undefined)&&($scope.LifeEngageProduct.splitOneOption!="PayInCash")&&($scope.LifeEngageProduct.splitOneOption!="PayInCheque"))
				{
					$scope.payConfirmDetails=true;
					$scope.splitoneinput=false;
					$scope.singlepaymentInput=false;
					$scope.showErrorCount=false;
					
				}
			}
			
			if(($scope.LifeEngageProduct.splitOneOption=="PayInCash")&&($scope.LifeEngageProduct.splitOneamount!='')){
				$scope.splittwoPayDetails=true;
				$('#btnGenerateEtr').attr('disabled', 'disabled');
				$('#btnupdateEtrdisable').attr('disabled', 'disabled');
				$scope.splitoneinput=false;
				$scope.splittwoinput=false;
				$scope.singlepaymentInput=false;
				$scope.payConfirmDetails=false;
				$scope.payCheque=false;

				if(navigator.onLine==false){
					$scope.updateETRBtn=true;
				}
				//$scope.LifeEngageProduct.splitOneOption="";
					$scope.LifeEngageProduct.paymentBankname="";
					$scope.LifeEngageProduct.splitTwobankCharge="";
					$scope.LifeEngageProduct.splitTwoPayinDate="";
					$scope.LifeEngageProduct.splitTwoPayinTime="";
					$scope.LifeEngageProduct.splitTwoEtrNumber="";
					
			}
			if(($scope.LifeEngageProduct.splitOneOption=="PayInCheque")&&($scope.LifeEngageProduct.splitOneamount!='')){
				$scope.splittwoPayDetails=true;
				$('#btnGenerateEtr').attr('disabled', 'disabled');
				$('#btnupdateEtrdisable').attr('disabled', 'disabled');
				$scope.splitoneinput=false;
				$scope.splittwoinput=false;
				$scope.singlepaymentInput=false;
				$scope.payConfirmDetails=false;
				$scope.payCheque=true;
				if(navigator.onLine==false){
					$scope.updateETRBtn=true;
				}
				//$scope.LifeEngageProduct.splitOneOption="";
					$scope.LifeEngageProduct.paymentBankname="";
					$scope.LifeEngageProduct.splitTwobankCharge="";
					$scope.LifeEngageProduct.splitTwoPayinDate="";
					$scope.LifeEngageProduct.splitTwoPayinTime="";
					$scope.LifeEngageProduct.splitTwoEtrNumber="";
					$scope.LifeEngageProduct.splitTwoBranchName="";
					$scope.LifeEngageProduct.splitTwoChequeNumber="";
					$scope.LifeEngageProduct.splitTwoIssueBank="";
					$scope.LifeEngageProduct.splitTwoIssueBranch="";
					
			}
			
		}
		//split yes condition
		if($rootScope.splitOptionSelected==true)
		{
			// generate ETR condition		
			if(($scope.LifeEngageProduct.splitOneOption=="PayInCash")&&($scope.LifeEngageProduct.splitOneamount!='')&&($scope.LifeEngageProduct.splitOneamount<=$scope.LifeEngageProduct.splitOneAmountValidate)){
				$('#btnGenerateEtr').attr('disabled', 'disabled');
				$('#btnupdateEtrdisable').attr('disabled', 'disabled');
				$scope.splitoneinput=false;
				$scope.splittwoinput=false;
				$scope.singlepaymentInput=false;
				$scope.payConfirmDetails=false;
				$scope.payCheque=false;
				$scope.splitTwopayConfirmDetails=false;
				$scope.splittwoPayDetails=true;
				if(navigator.onLine==false){
					$scope.updateETRBtn=true;
				}
				//$scope.LifeEngageProduct.splitOneOption="";
					$scope.LifeEngageProduct.paymentBankname="";
					$scope.LifeEngageProduct.splitTwobankCharge="";
					$scope.LifeEngageProduct.splitTwoPayinDate="";
					$scope.LifeEngageProduct.splitTwoPayinTime="";
					$scope.LifeEngageProduct.splitTwoEtrNumber="";
									
			}
			if(($scope.LifeEngageProduct.splitOneOption=="PayInCheque")&&($scope.LifeEngageProduct.splitOneamount!='')&&($scope.LifeEngageProduct.splitOneamount<=$scope.LifeEngageProduct.splitOneAmountValidate)){
				$('#btnGenerateEtr').attr('disabled', 'disabled');
				$('#btnupdateEtrdisable').attr('disabled', 'disabled');
				$scope.splittwoPayDetails=true;
				$scope.splitoneinput=false;
				$scope.splittwoinput=false;
				$scope.singlepaymentInput=false;
				$scope.payConfirmDetails=false;
				$scope.splitTwopayConfirmDetails=false;
				$scope.payCheque=true;
				if(navigator.onLine==false){
					$scope.updateETRBtn=true;
				}
				//$scope.LifeEngageProduct.splitOneOption="";
					$scope.LifeEngageProduct.paymentBankname="";
					$scope.LifeEngageProduct.splitTwobankCharge="";
					$scope.LifeEngageProduct.splitTwoPayinDate="";
					$scope.LifeEngageProduct.splitTwoPayinTime="";
					$scope.LifeEngageProduct.splitTwoEtrNumber="";
					$scope.LifeEngageProduct.splitTwoBranchName="";
					$scope.LifeEngageProduct.splitTwoChequeNumber="";
					$scope.LifeEngageProduct.splitTwoIssueBank="";
					$scope.LifeEngageProduct.splitTwoIssueBranch="";
			}
			//no network for online payment
			if(navigator.onLine==false){
				if(($scope.LifeEngageProduct.splitOneOption=="SendPaymentURL") || ($scope.LifeEngageProduct.splitOneOption=="CreditCard") || ($scope.LifeEngageProduct.splitOneOption=="Ebanking")){
					
					$scope.payConfirmDetails=false;
					$rootScope.lePopupCtrl.showSuccess(translateMessages($translate,
						"successHeader"), translateMessages($translate,
						"eapp_vt.checkOnlineForPayment"), translateMessages($translate,
						"fna.ok"));					
				}
			}
			// paynow and payemnt status condition
			else
			{ 
				if(($scope.LifeEngageProduct.splitOneamount!=undefined)&&($scope.LifeEngageProduct.splitOneamount<=$scope.LifeEngageProduct.splitOneAmountValidate)&&($scope.LifeEngageProduct.splitOneamount!='')&&($scope.LifeEngageProduct.splitOneOption!='')&&($scope.LifeEngageProduct.splitOneOption!=undefined)&&($scope.LifeEngageProduct.splitOneOption!="PayInCash")&&($scope.LifeEngageProduct.splitOneOption!="PayInCheque"))
				{
					$scope.payConfirmDetails=true;
					$scope.splitoneinput=false;
					$scope.splitTwopayConfirmDetails=false;
					$scope.splittwoinput=false;
					$scope.singlepaymentInput=false;
					$scope.showErrorCount=false;
					
				}
			}
			
		}	
		$scope.LifeEngageProduct.splitOneamount;
		$scope.LifeEngageProduct.splitOneOption;
		$scope.updateErrorCount($scope.selectedTabId);
	}
		//split two confirm action
	$scope.paysplitTwoconfirm = function () {
		if((UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO && ($scope.LifeEngageProduct.splitOneOption=="PayInCash" || $scope.LifeEngageProduct.splitOneOption=="PayInCheque"))|| UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
			$scope.showProceedForGAOBranchAdmin=true;
			$scope.splitTwoConfirmGAO=false;
			$scope.proccedForGAOBranchADmin=true;
			$timeout(function () {
			 $scope.updateErrorCount($scope.selectedTabId);
		 }, 50);
		}else{
			$scope.showProceedForGAOBranchAdmin=false;
		}
		//split yes condition
		if($rootScope.splitOptionSelected==true)
		{
			// generate ETR condition		
			if(($scope.LifeEngageProduct.splitOneOption=="PayInCash")&&($scope.LifeEngageProduct.splitOneamount!='')&&($scope.LifeEngageProduct.splitOneamount<=$scope.LifeEngageProduct.splitOneAmountValidate)){
				
				$('#btnGenerateEtr').attr('disabled', 'disabled');
				$('#btnupdateEtrdisable').attr('disabled', 'disabled');
				$('#step2Payment input[id="paybankCharge"]').removeAttr("disabled");
				$('#step2Payment input[id="payinDates"]').removeAttr("disabled");
				$('#step2Payment input[type="time"]').removeAttr("disabled");
				$('#ETRgeneration input').removeAttr("disabled");
				$scope.splitoneinput=false;
				$scope.splittwoinput=false;
				$scope.singlepaymentInput=false;
				$scope.payConfirmDetails=false;
				$scope.payCheque=false;
				$scope.splitTwopayConfirmDetails=false;
				$scope.splittwoPayDetails=true;
				//$scope.LifeEngageProduct.splitOneOption="";
					$scope.LifeEngageProduct.paymentBankname="";
					$scope.LifeEngageProduct.splitTwobankCharge="";
					$scope.LifeEngageProduct.splitTwoPayinDate="";
					$scope.LifeEngageProduct.splitTwoPayinTime="";
					$scope.LifeEngageProduct.splitTwoEtrNumber="";
					if(navigator.onLine==false){
					$scope.updateETRBtn=true;
				}
									
			}
			if(($scope.LifeEngageProduct.splitOneOption=="PayInCheque")&&($scope.LifeEngageProduct.splitOneamount!='')&&($scope.LifeEngageProduct.splitOneamount<=$scope.LifeEngageProduct.splitOneAmountValidate)){
				
				$('#btnGenerateEtr').attr('disabled', 'disabled');
				$('#btnupdateEtrdisable').attr('disabled', 'disabled');
				$('#step2Payment input[id="paybankCharge"]').removeAttr("disabled");
				$('#step2Payment input[id="payinDates"]').removeAttr("disabled");
				$('#step2Payment input[id="chequeNumber"]').removeAttr("disabled");
				$('#step2Payment input[type="time"]').removeAttr("disabled");
				$('#ETRgeneration input').removeAttr("disabled");
				$scope.splittwoPayDetails=true;
				$scope.splitoneinput=false;
				$scope.splittwoinput=false;
				$scope.singlepaymentInput=false;
				$scope.payConfirmDetails=false;
				$scope.splitTwopayConfirmDetails=false;
				$scope.payCheque=true;
			//	$scope.LifeEngageProduct.splitOneOption="";
					$scope.LifeEngageProduct.paymentBankname="";
					$scope.LifeEngageProduct.splitTwobankCharge="";
					$scope.LifeEngageProduct.splitTwoPayinDate="";
					$scope.LifeEngageProduct.splitTwoPayinTime="";
					$scope.LifeEngageProduct.splitTwoEtrNumber="";
					$scope.LifeEngageProduct.splitTwoBranchName="";
					$scope.LifeEngageProduct.splitTwoChequeNumber="";
					$scope.LifeEngageProduct.splitTwoIssueBank="";
					$scope.LifeEngageProduct.splitTwoIssueBranch="";
					if(navigator.onLine==false){
					$scope.updateETRBtn=true;
				}
				
			}
			//no network for online payment
			if(navigator.onLine==false){
				if(($scope.LifeEngageProduct.splitOneOption=="SendPaymentURL") || ($scope.LifeEngageProduct.splitOneOption=="CreditCard") || ($scope.LifeEngageProduct.splitOneOption=="Ebanking")){
					$scope.payConfirmDetails=false;
					$rootScope.lePopupCtrl.showSuccess(translateMessages($translate,
						"successHeader"), translateMessages($translate,
						"eapp_vt.checkOnlineForPayment"), translateMessages($translate,
						"fna.ok"));					
				}
			}
			// paynow and payemnt status condition
			else
			{
				if(($scope.LifeEngageProduct.splitOneamount!=undefined)&&($scope.LifeEngageProduct.splitOneamount<=$scope.LifeEngageProduct.splitOneAmountValidate)&&($scope.LifeEngageProduct.splitOneamount!='')&&($scope.LifeEngageProduct.splitOneOption!='')&&($scope.LifeEngageProduct.splitOneOption!=undefined)&&($scope.LifeEngageProduct.splitOneOption!="PayInCash")&&($scope.LifeEngageProduct.splitOneOption!="PayInCheque"))
				{
					$scope.splitTwopayConfirmDetails=true;
					$scope.splitoneinput=false;
					$scope.payConfirmDetails=false;
					$scope.splittwoinput=false;
					$scope.singlepaymentInput=false;
					$scope.showErrorCount=false;
					
				}
			}
		}	
		$scope.LifeEngageProduct.splitOneamount;
		$scope.LifeEngageProduct.splitOneOption;
		$scope.updateErrorCount($scope.selectedTabId);
	}
	//cancel email popup
	$scope.cancelMailPopup = function () {
	$scope.paynowEmail=false;
	$scope.paynowEmailWeb=false;
		angular.element(document.querySelector(".site-overlay")).removeClass("modal-backdrop fade in");
		$("#paynowEmail .modal").modal({
            backdrop: true,
			keyboard: true
        });	
		
	}	
	
	//cancel email field popup
	$scope.cancelEmailPopup = function () {
		$scope.emailpopup=false;
		angular.element(document.querySelector(".site-overlay")).removeClass("modal-backdrop fade in");
		$("#updateManualETRAction .modal").modal({
            backdrop: true,
			keyboard: true
        });
		angular.element(document.querySelector(".paynowbtn")).removeClass("paynowbtndisable");
		angular.element(document.querySelector(".paynowbtntwo")).removeClass("paynowbtnTwodisable");
		$scope.disablepaynow=false;
		$scope.paymentStatusBtn=false;	
	}
	// pay now action

	
	$scope.splitPaymentAction = function () {
		$scope.splitoneinput=false;
		$scope.showSplitOne = false;
		$scope.showSplitOnepayment = false;
		$scope.singlepaymentInput=false;
		$rootScope.proceedstep3=false;
		if($scope.payConfirmDetails==true){
			angular.element(document.querySelector(".paynowbtn")).addClass("paynowbtndisable");
			$scope.disablepaynow=true;
			$scope.paymentStatusBtn=true;	
		}
		if($scope.splitTwopayConfirmDetails==true){
			angular.element(document.querySelector(".paynowbtntwo")).addClass("paynowbtnTwodisable");
			$scope.splitTwodisablepaynow=true;
			$scope.splittwopaymentStatusBtn=true;	
			$scope.paymentStatusBtn=false;	
			$scope.disablepaynow=false;
		}
		$scope.paymentbackbutton = false;
		$scope.paymentStatusBtn = true;
		$('#ProceedPaymentSummaryButton').attr('disabled', true);
		
		//paynow email popup start
		if($scope.LifeEngageProduct.splitOneOption=="SendPaymentURL"){

			//popup email button
			if(rootConfig.isDeviceMobile) 
			{
				$scope.paynowEmail=true;
				$scope.emailpopup=false;
				angular.element(document.querySelector(".site-overlay")).addClass("modal-backdrop fade in");
				angular.element(document.querySelector("#paynowEmail")).addClass("modal le-popup-control hide fade auto-height animated fadeInDownBig in");
				angular.element(document.querySelector("#paynowEmail .span5")).addClass("etrInputValue");
				$("#paynowEmail .modal").modal({
					backdrop: 'static',
					keyboard: false
				});
			}
			else
			{
								
				$scope.paynowEmailWeb=true;
				$scope.emailpopup=false;
				angular.element(document.querySelector(".site-overlay")).addClass("modal-backdrop fade in");
				angular.element(document.querySelector("#paynowEmailWeb")).addClass("modal le-popup-control hide fade auto-height animated fadeInDownBig in");
				angular.element(document.querySelector("#paynowEmailWeb .span5")).addClass("etrInputValue");
				$("#paynowEmailWeb .modal").modal({
					backdrop: 'static',
					keyboard: false
				});
				
			}
			// popup email template with input field	
			$scope.paynowEmailPopup	= function () {
				$scope.emailToId = $scope.LifeEngageProduct.Insured.ContactDetails.emailId;
				$scope.emailSubject = "Payment URL for " +  $scope.LifeEngageProduct.Product.ProductDetails.productName + " " + $scope.LifeEngageProduct.Insured.BasicDetails.fullName;
				$scope.emailpopup=true;
				$scope.paynowEmail=false;
				$scope.paynowEmailWeb=false;
				angular.element(document.querySelector(".site-overlay")).addClass("modal-backdrop fade in");
				angular.element(document.querySelector(".popup_setpref_container_previous")).addClass("modal le-popup-control hide fade auto-height animated fadeInDownBig in");
				angular.element(document.querySelector(".popup_setpref_container_previous .span5")).addClass("etrInputValue");
				$("#popup_setpref_container_previous .modal").modal({
					backdrop: 'static',
					keyboard: false
				});
				
			}
			// update the email details
			$scope.updateEmailPopup = function () {
				var errorId = 0;
				$rootScope.showHideLoadingImage(true, 'eapp.PaymentInProgress',$translate);
				if ($scope.LifeEngageProduct.splitOneOption != null) {
				if ($scope.LifeEngageProduct.splitOneOption=="SendPaymentURL") {
					var transPayment = PaymentInfo();
					transPayment.PaymentInfo.payment_no = $scope.LifeEngageProduct.ApplicationNo;
					transPayment.PaymentInfo.payment_purpose = "N";
					if ($scope.LifeEngageProduct.Payer.BasicDetails.firstName != null) {
						transPayment.PaymentInfo.first_name = $scope.LifeEngageProduct.Payer.BasicDetails.firstName;
					}
					if ($scope.LifeEngageProduct.Payer.BasicDetails.lastName != null && $scope.LifeEngageProduct.Payer.BasicDetails.lastName!="") {
						transPayment.PaymentInfo.last_name = $scope.LifeEngageProduct.Payer.BasicDetails.lastName;
					}else{
						transPayment.PaymentInfo.last_name =$scope.LifeEngageProduct.Payer.BasicDetails.firstName;
					}
					if($scope.sendbyWhatsapp!=undefined && ($scope.sendbyWhatsapp=="Whatsapp"  || $scope.sendbyWhatsapp=="Linkapp")){
						transPayment.PaymentInfo.email = $scope.LifeEngageProduct.Insured.ContactDetails.emailId;
						}else if ($scope.emailToId != undefined && $scope.emailToId !=""){
							transPayment.PaymentInfo.email =$scope.emailToId;
					}else {
							transPayment.PaymentInfo.email = $scope.LifeEngageProduct.Insured.ContactDetails.emailId;
					}
						transPayment.PaymentInfo.premium = $scope.LifeEngageProduct.splitOneamount+"00";
						transPayment.PaymentInfo.company = "GTL";
						transPayment.PaymentInfo.grace_due_date = getFormattedDate();
						transPayment.PaymentInfo.allow_to_pay_shortage = 0;
						transPayment.PaymentInfo.source_id = "04";
						transPayment.PaymentInfo.sms_allowed = "Y";						
						transPayment.PaymentInfo.telephone = $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber1;
						PersistenceMapping.clearTransactionKeys();
						//PersistenceMapping.Key1 ="sendByUrl";
						PersistenceMapping.Key1 = ($scope.LifeEngageProduct.splitOneOption=="SendPaymentURL") ? "sendByUrl" : "card";
					}
				}
				var transactionObj = PersistenceMapping
				.mapScopeToPersistence(transPayment);
				if ((rootConfig.isDeviceMobile)) {
					var transactionData = $
						.parseJSON(transactionObj.TransactionData)
						transactionObj.TransactionData = transactionData;
				}
				$scope.statusCheck="PaidNow";
				$scope.emailpopup=false;
				$scope.paynowEmail=false;
				angular.element(document.querySelector(".site-overlay")).removeClass("modal-backdrop fade in");
				$("#paynowEmail .modal").modal({
		            backdrop: true,
					keyboard: true
		        });	
				
				$scope.paynowEmailWeb=false;
				angular.element(document.querySelector(".site-overlay")).removeClass("modal-backdrop fade in");
				$("#paynowEmailWeb .modal").modal({
		            backdrop: true,
					keyboard: true
		        });	
				
				if($scope.sendbyWhatsapp!=undefined && $scope.sendbyWhatsapp=="Whatsapp"){
					PaymentService.makePayment(transactionObj,$scope.whatsappSuccessCallback,$scope.errorInPaymentCallback);		
				}else if($scope.sendbyWhatsapp!=undefined && $scope.sendbyWhatsapp=="Linkapp"){
					PaymentService.makePayment(transactionObj,$scope.linkappSuccessCallback,$scope.errorInPaymentCallback);
				}else{
					PaymentService.makePayment(transactionObj,$scope.validationSuccessCallback,$scope.errorInPaymentCallback);
				}
				
				
				// success popup
				/*$rootScope.lePopupCtrl.showSuccess(translateMessages($translate,
						"successHeader"), translateMessages($translate,
						"eapp.paymentsuccessful"), translateMessages($translate,
						"eapp.messageok"));
				LEDynamicUI.paintUI(rootConfig.template, "step2Payment.json",
						"callDetailsSection", "#popupContent", true, function() {
							$scope.callback();
						}, $scope, $compile);*/
			}	
		} //paynow email popup end	
		else
		{
			
			var errorId = 0;
			$rootScope.showHideLoadingImage(true, 'eapp.PaymentInProgress',$translate);
			if ($scope.LifeEngageProduct.splitOneOption != null) {
				if ($scope.LifeEngageProduct.splitOneOption=="SendPaymentURL" || $scope.LifeEngageProduct.splitOneOption=="CreditCard" || $scope.LifeEngageProduct.splitOneOption=="Ebanking") {
					var transPayment = PaymentInfo();
					transPayment.PaymentInfo.payment_no = $scope.LifeEngageProduct.ApplicationNo;
					transPayment.PaymentInfo.payment_purpose = "N";
					if ($scope.LifeEngageProduct.Payer.BasicDetails.firstName != null) {
						transPayment.PaymentInfo.first_name = $scope.LifeEngageProduct.Payer.BasicDetails.firstName;
					}
					if ($scope.LifeEngageProduct.Payer.BasicDetails.lastName != null && $scope.LifeEngageProduct.Payer.BasicDetails.lastName!="") {
						transPayment.PaymentInfo.last_name = $scope.LifeEngageProduct.Payer.BasicDetails.lastName;
					}else
						transPayment.PaymentInfo.last_name =$scope.LifeEngageProduct.Payer.BasicDetails.firstName;
						transPayment.PaymentInfo.email = $scope.LifeEngageProduct.Insured.ContactDetails.emailId;
						transPayment.PaymentInfo.premium = $scope.LifeEngageProduct.splitOneamount+"00";
						transPayment.PaymentInfo.company = "GTL";						
						transPayment.PaymentInfo.grace_due_date = getFormattedDate();
						transPayment.PaymentInfo.allow_to_pay_shortage = 0;
						transPayment.PaymentInfo.source_id = "04";
						transPayment.PaymentInfo.sms_allowed = "Y";						
						transPayment.PaymentInfo.telephone = $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber1;
						PersistenceMapping.clearTransactionKeys();
						//PersistenceMapping.Key1 ="sendByUrl";
						PersistenceMapping.Key1 = ($scope.LifeEngageProduct.splitOneOption=="SendPaymentURL") ? "sendByUrl" : "card";
				}
			}
			var transactionObj = PersistenceMapping
				.mapScopeToPersistence(transPayment);
				if ((rootConfig.isDeviceMobile)) {
					var transactionData = $
						.parseJSON(transactionObj.TransactionData)
						transactionObj.TransactionData = transactionData;
				}
				$scope.statusCheck="PaidNow";
				PaymentService.makePayment(transactionObj,$scope.validationSuccessCallback,$scope.errorInPaymentCallback);
				
				// success popup
				/*$rootScope.lePopupCtrl.showSuccess(translateMessages($translate,
						"successHeader"), translateMessages($translate,
						"eapp.paymentsuccessful"), translateMessages($translate,
						"eapp.messageok"));
				LEDynamicUI.paintUI(rootConfig.template, "step2Payment.json",
						"callDetailsSection", "#popupContent", true, function() {
							$scope.callback();
						}, $scope, $compile);*/
		}
	}

	function getFormattedDate() {
		var todayDate = new Date();
		var yyyy = todayDate.getFullYear().toString();
		var mm = (todayDate.getMonth() + 1).toString();						
		var dd = todayDate.getDate().toString();
		return yyyy + (mm[1] ? mm : "0" + mm[0]) + (dd[1] ? dd : "0" + dd[0]);
	}

	//payment status action
	$scope.paymentStatus = function () {
		PersistenceMapping.clearTransactionKeys();
		PersistenceMapping.Key20 ="N";
		PersistenceMapping.Key21 =$scope.LifeEngageProduct.ApplicationNo;
		var transactionObj = PersistenceMapping.mapScopeToPersistence({});
		if ((rootConfig.isDeviceMobile)) {
			var transactionData = $
					.parseJSON(transactionObj.TransactionData)
			transactionObj.TransactionData = transactionData;
		}
		
		PaymentService.getPaymentStatus(transactionObj,$scope.statusSuccessCallback,$scope.errorInPaymentCallback);
	}
	// payment status success call back
	$scope.statusSuccessCallback = function (data) {
	    var msg = "";
	    $rootScope.showHideLoadingImage(false);
	    var requirementObject = paymentSaveObject();
        if(data!=='undefined' && data!=""){
			var newDate = new Date();
			var createdDate = newDate.getFullYear() + "-" + Number(newDate.getMonth() + 1) + "-" + newDate.getDate();
        	var dataObject = data.results [ data.results.length - 1];	
			if (dataObject && dataObject.status=="1") {
				if($scope.statusCheck=="PaidNow"){
			
				msg = translateMessages($translate, "eapp.paymentsuccessful");
					if($scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit!="SplitOne") 
					{
	            	   	requirementObject.PaymentInfo.paymentMethod=$scope.LifeEngageProduct.splitOneOption;
						requirementObject.PaymentInfo.paySplit = "SplitOne";
						if(dataObject.status=="1") {
							requirementObject.PaymentInfo.status ="Success";
						}
						else {
							requirementObject.PaymentInfo.status ="Failed";
						}
						requirementObject.PaymentInfo.transDate="";
						requirementObject.PaymentInfo.etrNumber ="";  //Transaction ID from Response
						requirementObject.PaymentInfo.etrGenerationMode ="";
						if(dataObject.paid_channel=="creditcard") {
							requirementObject.PaymentInfo.paymentType ="CreditCard";
						}else {
							requirementObject.PaymentInfo.paymentType =dataObject.paid_channel;
						}
						if(dataObject.datetime!=null){
							requirementObject.PaymentInfo.paymentDateTim =dataObject.datetime;
						}
						requirementObject.PaymentInfo.creditCardNo =dataObject.creditcard_no;
						requirementObject.PaymentInfo.approvalCode =dataObject.approval_code;

						// The parsing logic in Product and Payment value is different - hence trimming the case where decimal value has no numeric factor
						if(dataObject.premium != "" && dataObject.premium != null && dataObject.premium.endsWith(".00") &&
							 dataObject.premium.substring(0, dataObject.premium.indexOf(".00")) == $scope.LifeEngageProduct.splitOneAmountValidate) {
								requirementObject.PaymentInfo.premiumAmt = dataObject.premium.substring(0, dataObject.premium.indexOf(".00"));
						} else {
							requirementObject.PaymentInfo.premiumAmt =dataObject.premium;
						}
						requirementObject.PaymentInfo.bankName ="";
						if(dataObject.paid_channel=="creditcard")
						{
							var percentCharge = dataObject.premium * (1.34/100);
							requirementObject.PaymentInfo.bankCharge=percentCharge;
							requirementObject.PaymentInfo.merchantId =rootConfig.MerchantIdn;
						}
						else
						{
							requirementObject.PaymentInfo.bankCharge="";
							requirementObject.PaymentInfo.merchantId="";
						}
						requirementObject.PaymentInfo.chequeNumber ="";
						requirementObject.PaymentInfo.chequeDate ="";
						requirementObject.PaymentInfo.etrDate ="";
						requirementObject.PaymentInfo.cash ="";
						requirementObject.PaymentInfo.receiptNumber =dataObject.transaction_id;
						requirementObject.PaymentInfo.notes ="";
						requirementObject.PaymentInfo.etrGeneration="";
						
						if(splitone && $rootScope.splitOptionSelected==true) {
	 					//condition for proceed button based on total amount paid
							if(dataObject.premium === $scope.LifeEngageProduct.splitOneAmountValidate){
								$rootScope.proceedpaymentbtn=false;
								$rootScope.proceedstep3=true;
							}
							else
							{
								$rootScope.proceedpaymentbtn=true;
								$rootScope.proceedstep3=false;
							}
														 							//split two tab display	
	 					$rootScope.showSplitTwo = function () {
								
							$scope.showErrorCount = true;
	 						$scope.splittwoinput=true;
	 						$scope.splittwoPayDetails=false;
	 						$scope.EtrPayDetails=false;
	 						$scope.payConfirmDetails=false;
	 						$scope.splitTwopayConfirmDetails=false;
	 						$scope.LifeEngageProduct.splitOneamount=$scope.LifeEngageProduct.splitOneAmountValidate - dataObject.premium;
	 				//		$scope.LifeEngageProduct.splitOneOption="";
	 						$scope.LifeEngageProduct.paymentBankname="";
	 						$scope.LifeEngageProduct.splitTwobankCharge="";
	 						$scope.LifeEngageProduct.splitTwoPayinDate="";
	 						$scope.LifeEngageProduct.splitTwoPayinTime="";
	 						$scope.showSplitOne=false;
	 						$rootScope.proceedpaymentbtn=false;
							$rootScope.proceedstep3=true;
	 						$rootScope.splitTwo = angular.element(document.querySelector(".splittabspaytwo")).addClass("splittabspaytwoactive");
	 						$rootScope.splitOneRemove = angular.element(document.querySelector(".splittabspayone")).removeClass("splittabspayoneactive");
	 					}
	 					
						}
						if(splitone && $rootScope.splitOptionSelected==false) {
						$rootScope.proceedstep3=true;
						$rootScope.proceedpaymentbtn=false;
						}
						
						if($rootScope.splitOptionSelected == false) 
						{
							requirementObject.PaymentInfo.splitPaymentTst ="No";
						}
						else if($rootScope.splitOptionSelected == true) 
						{
							requirementObject.PaymentInfo.splitPaymentTst ="Yes";
						}
						requirementObject.PaymentInfo.skipPaymentTst=$scope.LifeEngageProduct.skipPaymentSelection;
						$scope.LifeEngageProduct.Payment.PaymentInfo[0] = requirementObject.PaymentInfo;
						$scope.LifeEngageProduct.paymentSignStatus = "Inprogress";
						if (rootConfig.isDeviceMobile) 
						{
							EappVariables.setEappModel($scope.LifeEngageProduct);
						}
						$scope.statusCheck="PayStopped";
						//EappVariables.EappKeys.Key34 = "PaymentCompleted";  
						/*EappService.saveTransaction(function () {
							$rootScope.showHideLoadingImage(false);
							
						}, $scope.onConfirmError, false);*/
						
						$rootScope.showHideLoadingImage(true, 'Saving Inprogress', $translate);
						EappService.saveTransaction($scope.onSaveCallback,$scope.onConfirmError, false);
						$rootScope.showHideLoadingImage(false);
						$('#ProceedPaymentSummaryButton').attr('disabled', false);
						$scope.lePopupCtrl.paymentSuccess(translateMessages($translate, "lifeEngage"), msg, translateMessages($translate, "fna.ok"));
					} 
					else if ($scope.LifeEngageProduct.Payment.PaymentInfo[1].paySplit!="SplitTwo" && $scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit!="" && $scope.LifeEngageProduct.Payment.PaymentInfo[0].receiptNumber != dataObject.transaction_id) 
					{
	        	       	requirementObject.PaymentInfo.paymentMethod=$scope.LifeEngageProduct.splitOneOption;
						requirementObject.PaymentInfo.paySplit = "SplitTwo";
						if(dataObject.status=="1") 
						{
							requirementObject.PaymentInfo.status ="Success";
						}
						else
						{
							requirementObject.PaymentInfo.status ="Failed";
						}
						requirementObject.PaymentInfo.transDate="";
						requirementObject.PaymentInfo.etrNumber ="";  //Transaction ID from Response
						requirementObject.PaymentInfo.etrGenerationMode ="";
						if(dataObject.paid_channel && dataObject.paid_channel == "creditcard")
						{
							requirementObject.PaymentInfo.paymentType ="CreditCard";
						} else {
							requirementObject.PaymentInfo.paymentType = dataObject.paid_channel;
						}	
						if (dataObject.datetime!=null) {
							requirementObject.PaymentInfo.paymentDateTim = dataObject.datetime;
						}
						requirementObject.PaymentInfo.creditCardNo = dataObject.creditcard_no;
						requirementObject.PaymentInfo.approvalCode = dataObject.approval_code;
						requirementObject.PaymentInfo.premiumAmt = dataObject.premium;
						requirementObject.PaymentInfo.bankName ="";
						if(dataObject.paid_channel=="creditcard") {
							var percentCharge = dataObject.premium * (1.34/100);
							requirementObject.PaymentInfo.bankCharge = percentCharge;
							requirementObject.PaymentInfo.merchantId = rootConfig.MerchantIdn;
						} else {
							requirementObject.PaymentInfo.bankCharge="";
							requirementObject.PaymentInfo.merchantId = "";
						}
						requirementObject.PaymentInfo.chequeNumber ="";
						requirementObject.PaymentInfo.chequeDate ="";
						requirementObject.PaymentInfo.etrDate ="";
						requirementObject.PaymentInfo.cash ="";
						requirementObject.PaymentInfo.receiptNumber = dataObject.transaction_id;
						requirementObject.PaymentInfo.notes ="";
						requirementObject.PaymentInfo.etrGeneration="";
						
						
							$rootScope.proceedstep3=true;
							$rootScope.proceedpaymentbtn=false;
						
						
						if($rootScope.splitOptionSelected == false) 
						{
							requirementObject.PaymentInfo.splitPaymentTst ="No";
						}
						else if($rootScope.splitOptionSelected == true) 
						{
							requirementObject.PaymentInfo.splitPaymentTst ="Yes";
						} 
						requirementObject.PaymentInfo.skipPaymentTst=$scope.LifeEngageProduct.skipPaymentSelection;
						$scope.LifeEngageProduct.Payment.PaymentInfo[1] = requirementObject.PaymentInfo;
						$scope.LifeEngageProduct.paymentSignStatus = "Inprogress";
						if (rootConfig.isDeviceMobile) 
						{
							EappVariables.setEappModel($scope.LifeEngageProduct);
						}
					//	EappVariables.EappKeys.Key34 = "PaymentCompleted";
						/*EappService.saveTransaction(function () 
						{
							$rootScope.showHideLoadingImage(false);
						
						}, $scope.onConfirmError, false);*/
						
						$rootScope.showHideLoadingImage(true, 'eapp.SavingInprogress', $translate);
						EappService.saveTransaction($scope.onSaveCallback,$scope.onConfirmError, false);
						$rootScope.showHideLoadingImage(false);
						$scope.statusCheck="PayStopped";
						$('#ProceedPaymentSummaryButton').attr('disabled', false);
						$scope.lePopupCtrl.paymentSuccess(translateMessages($translate, "lifeEngage"), msg, translateMessages($translate, "fna.ok"));
                    
					} else {}
				}
				else 
				{
					msg = translateMessages($translate, "eapp.paymentsuccessful");
					$scope.lePopupCtrl.paymentSuccess(translateMessages($translate, "lifeEngage"), msg, translateMessages($translate, "fna.ok"));                
				}
			  
			} else {
				   msg = translateMessages($translate, "eapp.payemntFailMessage");
				   $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), msg, translateMessages($translate, "fna.ok"));
			}
		} else {
		   msg = translateMessages($translate, "eapp.payemntFailMessage");
		   $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), msg, translateMessages($translate, "fna.ok"));
		}
	};


	//generate Manual E-TR popup
	$scope.ETRgenerateManual = function () {
		$scope.splittwoPayDetails=true;
		$scope.manualGenerateETRpopup=false;
		$scope.updateETRnumber=false;
		$scope.DynamicGenerateETRpopup=false;
		if($scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit=="SplitOne" && $rootScope.splitOptionSelected==true){
			$rootScope.proceedstep3=false;
			$rootScope.proceedpaymentbtn=true;	
			}
			if($scope.LifeEngageProduct.Payment.PaymentInfo[1].paySplit=="SplitTwo" && $rootScope.splitOptionSelected==false){
			$rootScope.proceedstep3=true;
			$rootScope.proceedpaymentbtn=false;	
			}
		angular.element(document.querySelector(".site-overlay")).removeClass("modal-backdrop fade in");
		$("#generateManualETRAction .modal").modal({
            backdrop: true,
			keyboard: true
        });
		$('#btnGenerateEtr').attr('disabled', 'disabled');
		$('#bankName').attr('disabled','disabled');
        $('#step2Payment input[type="text"]').attr('disabled', 'disabled');
		$('#step2Payment input[type="time"]').attr('disabled', 'disabled');
		$('#step2Payment select').attr('disabled', 'disabled');
		var arrayObj = {"tr_no":""};
		arrayObj.tr_no= $scope.LifeEngageProduct.splitTwoEtrNumber;
		$scope.etrSuccessCallback(arrayObj);
	}

	//generate Dynamic E-TR popup
	$scope.ETRgenerateDynamic = function () {
		$scope.splittwoPayDetails = true;
		$scope.manualGenerateETRpopup = false;
		$scope.updateETRnumber = false;
		$scope.DynamicGenerateETRpopup = false;
		if ($rootScope.splitOptionSelected == false && $scope.LifeEngageProduct.splitTwoEtrNumber) {
			saveOnAutoEtrSplitoneOption();
		}
		else if ($rootScope.splitOptionSelected == true && $scope.LifeEngageProduct.splitTwoEtrNumber) {
			var arrayObj = { "tr_no": "", "tr_date": "" };
			arrayObj.tr_no = $scope.LifeEngageProduct.splitTwoEtrNumber;
			arrayObj.tr_date = $scope.LifeEngageProduct.splitPaymentTime;
			$scope.etrSuccessCallback(arrayObj);
		}
		else {
			$rootScope.showHideLoadingImage(false);
			$rootScope.proceedstep3 = false;
			$scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.paymentInvalidErrorMsg"), translateMessages($translate, "fna.ok"));
			angular.element(document.querySelector(".site-overlay")).removeClass("modal-backdrop fade in");
			$("#generateDynamicETRAction .modal").modal({
				backdrop: true,
				keyboard: true
			});
		}
		
		if ($scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit == "SplitOne" && $rootScope.splitOptionSelected == true) {
			$rootScope.proceedstep3 = false;
			$rootScope.proceedpaymentbtn = true;
		}
		if ($scope.LifeEngageProduct.Payment.PaymentInfo[1].paySplit == "SplitTwo" && $rootScope.splitOptionSelected == false) {
			$rootScope.proceedstep3 = true;
			$rootScope.proceedpaymentbtn = false;
		}

		angular.element(document.querySelector(".site-overlay")).removeClass("modal-backdrop fade in");
		$("#generateDynamicETRAction .modal").modal({
			backdrop: true,
			keyboard: true
		});
		$('#btnGenerateEtr').attr('disabled', 'disabled');
		$('#bankName').attr('disabled', 'disabled');
		$('#step2Payment input[type="text"]').attr('disabled', 'disabled');
		$('#step2Payment select').attr('disabled', 'disabled');
		$('#step2Payment input[type="time"]').attr('disabled', 'disabled');
		$("#ETRgeneration :input").attr("disabled", true);
	
	}
	//cancel Manual E-TR popup
	$scope.cancelETRnoManual = function () {
		$scope.splittwoPayDetails=true;
		$scope.updateETRnumber=false;
		$scope.manualGenerateETRpopup=false;
		$scope.DynamicGenerateETRpopup=false;
		$scope.LifeEngageProduct.splitTwoEtrNumber="";
		//$scope.cancelProceedbtn=true;
		$rootScope.proceedstep3=false;
		$rootScope.proceedpaymentbtn=false;
		angular.element(document.querySelector(".site-overlay")).removeClass("modal-backdrop fade in");
		$("#updateManualETRAction .modal").modal({
            backdrop: true,
			keyboard: true
        });
	}
	
	//update Manual E-TR popup
	$scope.updateETRnoManual = function () {
		$scope.splittwoPayDetails=true;
		$scope.updateETRnumber=false;
		$scope.manualGenerateETRpopup=false;
		$scope.DynamicGenerateETRpopup=false;
		if($scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit=="SplitOne" && $rootScope.splitOptionSelected==true){
			$rootScope.proceedstep3=false;
			$rootScope.proceedpaymentbtn=true;	
			}
			if($scope.LifeEngageProduct.Payment.PaymentInfo[1].paySplit=="SplitTwo" && $rootScope.splitOptionSelected==false){

			$rootScope.proceedstep3=true;
			$rootScope.proceedpaymentbtn=false;	
			}
		angular.element(document.querySelector(".site-overlay")).removeClass("modal-backdrop fade in");
		$("#updateManualETRAction .modal").modal({
            backdrop: true,
			keyboard: true
        });
		$('#btnGenerateEtr').attr('disabled', 'disabled');
		$('#bankName').attr('disabled','disabled');
        $('#step2Payment input[type="text"]').attr('disabled', 'disabled');
		$('#step2Payment select').attr('disabled', 'disabled');
	$('#step2Payment input[type="time"]').attr('disabled', 'disabled');
		var etrGenerateNumber = $scope.LifeEngageProduct.splitTwoEtrNumber;
		if($scope.LifeEngageProduct.Payment.PaymentInfo.length > 0) {
			for (i = 0; i <= $scope.LifeEngageProduct.Payment.PaymentInfo.length; i++) {
	            if ($scope.LifeEngageProduct.Payment.PaymentInfo[i].etrNumber == $scope.etrUpdateNo) {
	            	$scope.LifeEngageProduct.Payment.PaymentInfo[i].paySplit="Update";
	                break;
	            }
			}
		}
		var arrayObj = {"tr_no":""};
		arrayObj.tr_no= $scope.LifeEngageProduct.splitTwoEtrNumber;
		$scope.etrSuccessCallback(arrayObj);
	}
	


	// update manual ETR number popup box
	$scope.updateETRno = function () {
		if(($scope.LifeEngageProduct.paymentBankname!=undefined)&&($scope.LifeEngageProduct.splitTwobankCharge!=undefined)&&($scope.LifeEngageProduct.splitTwoPayinDate!=undefined)&&($scope.LifeEngageProduct.splitTwoPayinTime!=undefined))
	{	
		angular.element(document.querySelector(".site-overlay")).addClass("modal-backdrop fade in");
		angular.element(document.querySelector("#updateManualETRAction")).addClass("modal le-popup-control hide fade auto-height animated fadeInDownBig in");
		angular.element(document.querySelector("#updateManualETRAction .span5")).addClass("etrInputValue");
		$scope.etrUpdateNo = $scope.LifeEngageProduct.splitTwoEtrNumber;
		$("#updateManualETRAction .modal").modal({
            backdrop: 'static',
            keyboard: false
        });
		//check internet connection
		if(navigator.onLine==false){
			$scope.updateETRnumber=true;
		}
	}
	}
	//Generate E-TR 
	$scope.generateETR = function () {
	if(($scope.LifeEngageProduct.paymentBankname!=undefined)&&($scope.LifeEngageProduct.splitTwobankCharge!=undefined)&&($scope.LifeEngageProduct.splitTwoPayinDate!=undefined)&&($scope.LifeEngageProduct.splitTwoPayinTime!=undefined))
	{	
		if(splitone && $rootScope.splitOptionSelected==true){
			$scope.showErrorCount=false;
			
			$scope.splittwoPayDetails=true;
		}	
		
		/*if((splitone && $rootScope.splitOptionSelected==false)||($rootScope.splitTwo && $rootScope.splitOptionSelected==true)){
			$rootScope.proceedstep3=true;
			$scope.splittwoPayDetails=true;
			$rootScope.proceedpaymentbtn=false;
		}*/
		var newDate = new Date();
		var createdDate = newDate.getFullYear() + "-" + Number(newDate.getMonth() + 1) + "-" + newDate.getDate();
		var externalDateConvert = InternalToEtr(createdDate);
		var transPayment=paymentETRTransaction();
		var externalDateConvertDOB = InternalToEtr($scope.LifeEngageProduct.Insured.BasicDetails.dob);
		        transPayment.source_of_data = "Gentouch";
				transPayment.payor_name = $scope.LifeEngageProduct.Payer.BasicDetails.firstName + " " + " " + $scope.LifeEngageProduct.Payer.BasicDetails.lastName;
				if ($scope.LifeEngageProduct.Payer.ContactDetails.emailId != undefined) {
				transPayment.payor_email = $scope.LifeEngageProduct.Payer.ContactDetails.emailId;}
				else {
					transPayment.payor_email = "";
				}
				transPayment.mobile = $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber1;
                // autoetr new fix - transPayment.payor_phone = $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber1;
				transPayment.payment_date = externalDateConvert;
				transPayment.pay_for = 0;
				if( $scope.LifeEngageProduct.Proposer.BasicDetails.isInsuredSameAsPayer=="Yes"){
					transPayment.payor_role = 1;
				} else {
					transPayment.payor_role = 3;
				}
				
				transPayment.bankName = $scope.LifeEngageProduct.paymentBankname;
				transPayment.bankBranchName =  $scope.LifeEngageProduct.splitTwoBranchName;
				transPayment.bankAccountNumber = $scope.LifeEngageProduct.splitTwoAccountNumber;
				transPayment.branchCode = $scope.LifeEngageProduct.splitTwoBranchCode;
				transPayment.bankBranchCode = $scope.LifeEngageProduct.splitTwoBankBranchCode;
				transPayment.splitPayment = $rootScope.splitOptionSelected;
				transPayment.bankCharge = $scope.LifeEngageProduct.splitTwobankCharge;
				
				transPayment.product_id = $scope.LifeEngageProduct.Product.ProductDetails.planCode;
				transPayment.application_no = $scope.LifeEngageProduct.ApplicationNo;
				transPayment.application_date = externalDateConvert;
			    transPayment.policy_holder = $scope.LifeEngageProduct.Insured.BasicDetails.firstName + " " + " " + $scope.LifeEngageProduct.Insured.BasicDetails.lastName;
			//  transPayment.DOB_policy_holder=externalDateConvert;
			    transPayment.DOB_policy_holder = externalDateConvertDOB;

                // autoetr new fix - transPayment.policy_holder_email = $scope.LifeEngageProduct.Insured.ContactDetails.emailId;
				// autoetr new fix - transPayment.policy_holder_phone = $scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber1;
				
                // autoetr new fix - transPayment.application_date = externalDateConvert;
			    

               // autoetr new fix -  transPayment.DOB_policy_holder = externalDateConvertDOB;
			//	transPayment.sum_assured= parseInt($scope.LifeEngageProduct.Product.premiumSummary.totalPremium);
			//	transPayment.premium= parseInt($scope.LifeEngageProduct.splitOneamount);
			    transPayment.sum_assured = parseInt($scope.LifeEngageProduct.Product.policyDetails.sumInsured);
				transPayment.premium = parseInt($scope.LifeEngageProduct.Product.premiumSummary.totalPremium);
				if($scope.LifeEngageProduct.splitOneOption == "PayInCash")	{
				    transPayment.cash_payment = parseInt($scope.LifeEngageProduct.splitOneamount);
				}else{
					transPayment.cash_payment = 0;
				}
				if($scope.LifeEngageProduct.splitOneOption=="PayInCheque")	{
	        		var arryObj = {"cheque_bank": "", "cheque_branch": "","chequeNumber": "","cheque_date": "","amount": "" };
	        		arryObj.cheque_bank = $scope.LifeEngageProduct.splitTwoIssueBank;
	        		arryObj.cheque_branch = $scope.LifeEngageProduct.splitTwoIssueBranch;
	        		arryObj.chequeNumber = $scope.LifeEngageProduct.splitTwoChequeNumber;
	        		arryObj.cheque_date = externalDateConvert;
	        		arryObj.amount = parseInt($scope.LifeEngageProduct.splitOneamount);
	        		transPayment.cheques.push(arryObj);
	        	}
				PersistenceMapping.clearTransactionKeys();
				var transactionObj = PersistenceMapping.mapScopeToPersistence(transPayment);
				//transPayment.agent_code=PersistenceMapping.Key11;
				if ((rootConfig.isDeviceMobile)) {
					var transactionData = $.parseJSON(transactionObj.TransactionData)
					transactionObj.TransactionData = transactionData;
				}
				transactionObj.TransactionData.agent_code=transactionObj.Key11;
				if(navigator.onLine==true){
				
			PaymentService.getETRNumber(transactionObj,$scope.etrSuccessCallbackone,$scope.errorInPaymentCallback);
				$rootScope.showHideLoadingImage(true, 'eapp.GenerateETRInProgress',$translate);
				}
		
		//check internet connection
		if(navigator.onLine==false){
			//manual ETR generate popup box
			angular.element(document.querySelector(".site-overlay")).addClass("modal-backdrop fade in");
			angular.element(document.querySelector("#generateManualETRAction")).addClass("modal le-popup-control hide fade auto-height animated fadeInDownBig in");
			angular.element(document.querySelector("#generateManualETRAction .span5")).addClass("etrInputValue");
			
			$("#generateManualETRAction .modal").modal({
	            backdrop: 'static',
	            keyboard: false
	        });
			$scope.manualGenerateETRpopup=true;
			}
		}		
	}
	    /**
		 * function for  save on auto generate splitone payment option flow
		 */
		function saveOnAutoEtrSplitoneOption () {
			$rootScope.showHideLoadingImage(false);
			var requirementObject = paymentSaveObject();
			var newDate = new Date();
			var createdDate = newDate.getFullYear() + "-" + Number(newDate.getMonth() + 1) + "-" + newDate.getDate();
			var tran_date;
			var tr_no = $scope.LifeEngageProduct.splitTwoEtrNumber;
			var tr_date = $scope.LifeEngageProduct.splitPaymentTime;
			if (tr_no && tr_no != null && tr_no != "") {
				if ($scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit != "SplitOne") {
					requirementObject.PaymentInfo.paymentMethod = $scope.LifeEngageProduct.splitOneOption;
					requirementObject.PaymentInfo.paySplit = "SplitOne";
					if ($scope.LifeEngageProduct.splitOneOption == "PayInCash") {
						requirementObject.PaymentInfo.paymentType = "Cash";
					} else if ($scope.LifeEngageProduct.splitOneOption == "PayInCheque") {
						requirementObject.PaymentInfo.paymentType = "PersonalCheck";
					}
					requirementObject.PaymentInfo.etrNumber = $scope.LifeEngageProduct.splitTwoEtrNumber;
					requirementObject.PaymentInfo.etrGenerationMode = $scope.LifeEngageProduct.ETRgeneration;
					requirementObject.PaymentInfo.paymentDateTime = createdDate;
					var timevalue = $scope.LifeEngageProduct.splitTwoPayinTime.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
					if (timevalue != null) {
						if (navigator.userAgent.indexOf("Android") > 0) {
							requirementObject.PaymentInfo.paymentDateTim = $scope.LifeEngageProduct.splitTwoPayinDate + " " + "12:00:00";
						} else {
							var paymentConvertTime = $scope.time24Format(timevalue);
							requirementObject.PaymentInfo.paymentDateTim = $scope.LifeEngageProduct.splitTwoPayinDate + " " + paymentConvertTime.toString().trim();
						}
					}
					requirementObject.PaymentInfo.etrDate = createdDate;
					requirementObject.PaymentInfo.premiumAmt = $scope.LifeEngageProduct.splitOneamount;
					requirementObject.PaymentInfo.cash = $scope.LifeEngageProduct.splitOneamount;
					if (UserDetailsService.getAgentRetrieveData().AgentDetails.agentType == rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType == rootConfig.agentTypeBranchAdmin) {
						requirementObject.PaymentInfo.transDate = requirementObject.PaymentInfo.paymentDateTim;
					} else {
						if (tr_date != null) {
							tran_date = tr_date.replace(/T/g, " ");
							tran_date = tran_date.replace(/Z/g, "");
							requirementObject.PaymentInfo.transDate = tran_date;
						}
					}
					//Pay in Cash and Cheque
					requirementObject.PaymentInfo.bankName = $scope.LifeEngageProduct.paymentBankname;
					requirementObject.PaymentInfo.bankCode = $scope.LifeEngageProduct.splitTwoBranchCode.trim();
					requirementObject.PaymentInfo.branchName = $scope.LifeEngageProduct.splitTwoBranchName.trim();
					requirementObject.PaymentInfo.bankCharge = $scope.LifeEngageProduct.splitTwobankCharge.trim();
					requirementObject.PaymentInfo.bankAccNo = $scope.LifeEngageProduct.splitTwoAccountNumber.trim();
					if ($scope.LifeEngageProduct.splitOneOption == "PayInCheque") {
						requirementObject.PaymentInfo.chequeNumber = $scope.LifeEngageProduct.splitTwoChequeNumber;
						requirementObject.PaymentInfo.chequeDate = $scope.LifeEngageProduct.splitTwoPayinDate;
						requirementObject.PaymentInfo.issBankName = $scope.LifeEngageProduct.splitTwoIssueBank.trim();
						requirementObject.PaymentInfo.issBranchName = $scope.LifeEngageProduct.splitTwoIssueBranch.trim();
						requirementObject.PaymentInfo.issBranchCode = $scope.LifeEngageProduct.splitTwoIssueBranchCode.trim();
						requirementObject.PaymentInfo.branchCode = "";
					} else {
						requirementObject.PaymentInfo.chequeNumber = "";
						requirementObject.PaymentInfo.chequeDate = "";
						requirementObject.PaymentInfo.issBankName = "";
						requirementObject.PaymentInfo.issBranchName = "";
						requirementObject.PaymentInfo.issBranchCode = "";
						requirementObject.PaymentInfo.branchCode = $scope.LifeEngageProduct.splitTwoBankBranchCode;
					}
					requirementObject.PaymentInfo.creditCardNo = "";
					requirementObject.PaymentInfo.approvalCode = "";
					requirementObject.PaymentInfo.receiptNumber = "";
					requirementObject.PaymentInfo.notes = "";
					requirementObject.PaymentInfo.status = "";
					requirementObject.PaymentInfo.merchantId = "";
					if (navigator.onLine == true) {
						if (UserDetailsService.getAgentRetrieveData().AgentDetails.agentType == rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType == rootConfig.agentTypeBranchAdmin) {
							requirementObject.PaymentInfo.etrGeneration = "Manual";
						} else {
							if ($scope.LifeEngageProduct.ETRgeneration == "manualETR") {
								requirementObject.PaymentInfo.etrGeneration = "Manual";
							} else {
								requirementObject.PaymentInfo.etrGeneration = "Online";
							}
						}
					} else {
						requirementObject.PaymentInfo.etrGeneration = "Manual";
					}
					//proceed button enable and disable
					if (splitone && $rootScope.splitOptionSelected == true) {
						$rootScope.proceedstep3 = false;
						$rootScope.proceedpaymentbtn = true;
					}
					if (splitone && $rootScope.splitOptionSelected == false) {
						$rootScope.proceedstep3 = true;
						$rootScope.proceedpaymentbtn = false;
					}
					if ($rootScope.splitOptionSelected == false) {
						requirementObject.PaymentInfo.splitPaymentTst = "No";
					} else if ($rootScope.splitOptionSelected == true) {
						requirementObject.PaymentInfo.splitPaymentTst = "Yes";
					}
					requirementObject.PaymentInfo.skipPaymentTst = $scope.LifeEngageProduct.skipPaymentSelection;
					$scope.LifeEngageProduct.Payment.PaymentInfo[0] = requirementObject.PaymentInfo;
					$scope.LifeEngageProduct.paymentSignStatus = "Inprogress";
					if (rootConfig.isDeviceMobile) {
						EappVariables.setEappModel($scope.LifeEngageProduct);
					}
					$rootScope.showHideLoadingImage(true, 'eapp.SavingInprogress', $translate);
					EappService.saveTransaction($scope.onSaveCallback, $scope.onConfirmError, false);
					$rootScope.showHideLoadingImage(false);
					$('#ProceedPaymentSummaryButton').attr('disabled', false);
				}
			}
			else {
				$rootScope.showHideLoadingImage(false);
				$rootScope.proceedstep3 = false;
				$scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.paymentInvalidErrorMsg"), translateMessages($translate, "fna.ok"));
				angular.element(document.querySelector(".site-overlay")).removeClass("modal-backdrop fade in");
				$("#generateDynamicETRAction .modal").modal({
					backdrop: true,
					keyboard: true
				});
			}
		}
		/* END */
		
		$scope.etrSuccessCallbackone = function (data) 
		{
			$rootScope.showHideLoadingImage(false);    
			$scope.LifeEngageProduct.splitTwoEtrNumber = data.tr_no;
			$scope.LifeEngageProduct.splitPaymentTime = data.tr_date;
			if(data.tr_no && data.tr_no!=null && data.tr_no!=""){
				if(navigator.onLine==true && (UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeNormal || 
					UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGM || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeSM)){
					$scope.DynamicGenerateETRpopup=true;
					angular.element(document.querySelector(".site-overlay")).addClass("modal-backdrop fade in");
					angular.element(document.querySelector("#generateDynamicETRAction")).addClass("modal le-popup-control hide fade auto-height animated fadeInDownBig in");
					angular.element(document.querySelector("#generateDynamicETRAction .span5")).addClass("etrInputValue");
					$("#generateDynamicETRAction .modal").modal({
						backdrop: 'static',
						keyboard: false
					});
				}
			}
			else {
				
			     $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.paymentInvalidErrorMsg"), translateMessages($translate, "fna.ok"));
					
			}
			
		}
		
		$scope.etrSuccessCallback = function (data) {
		  $rootScope.showHideLoadingImage(false);    
		  var requirementObject = paymentSaveObject();
		  var newDate = new Date();
		  var createdDate = newDate.getFullYear() + "-" + Number(newDate.getMonth() + 1) + "-" + newDate.getDate();
		  var tran_date;
		  $scope.LifeEngageProduct.splitTwoEtrNumber = data.tr_no;
				if(data.tr_no && data.tr_no!=null && data.tr_no!=""){
					if($scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit!="SplitOne") {
		        	requirementObject.PaymentInfo.paymentMethod=$scope.LifeEngageProduct.splitOneOption;
		        	requirementObject.PaymentInfo.paySplit = "SplitOne";
		        	if($scope.LifeEngageProduct.splitOneOption=="PayInCash"){
		        		requirementObject.PaymentInfo.paymentType ="Cash";
		        	}else if($scope.LifeEngageProduct.splitOneOption=="PayInCheque"){
		        		requirementObject.PaymentInfo.paymentType ="PersonalCheck";
		        	}
		        	requirementObject.PaymentInfo.etrNumber =$scope.LifeEngageProduct.splitTwoEtrNumber;
		        	requirementObject.PaymentInfo.etrGenerationMode =$scope.LifeEngageProduct.ETRgeneration;
		        	requirementObject.PaymentInfo.paymentDateTime =createdDate;
		        	
		        	/*var paymentConvertTime = $scope.time24Format($scope.LifeEngageProduct.splitTwoPayinTime);
		        	var paymentConvertTime = $scope.LifeEngageProduct.splitTwoPayinTime.toString();
		        	paymentConvertTime = paymentConvertTime.split(' ');
		        	var paymentFinConverstion = paymentConvertTime[4];
		            requirementObject.PaymentInfo.paymentDateTim = $scope.LifeEngageProduct.splitTwoPayinDate + " " + paymentFinConverstion;
		        	*/
		        	
		        	var timevalue = $scope.LifeEngageProduct.splitTwoPayinTime.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
		        	
		        	if(timevalue!=null ) {
		        	//	timevalue = timevalue.replace(/AM|PM/g,"");
		        		if(navigator.userAgent.indexOf("Android") > 0 ) {
		        			requirementObject.PaymentInfo.paymentDateTim = $scope.LifeEngageProduct.splitTwoPayinDate + " " + "12:00:00";
		        		}else {
		        		var paymentConvertTime = $scope.time24Format(timevalue);
		        		requirementObject.PaymentInfo.paymentDateTim = $scope.LifeEngageProduct.splitTwoPayinDate + " " +paymentConvertTime.toString().trim();
		        		}
		        	}
		        	
		        	requirementObject.PaymentInfo.etrDate =createdDate;
		        	requirementObject.PaymentInfo.premiumAmt =$scope.LifeEngageProduct.splitOneamount;
		        	requirementObject.PaymentInfo.cash =$scope.LifeEngageProduct.splitOneamount;
		        	if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
		            	requirementObject.PaymentInfo.transDate=requirementObject.PaymentInfo.paymentDateTim;
		        	}else {
		            	if(data.tr_date !=null){
			        		tran_date = data.tr_date.replace(/T/g," ");
			        		tran_date = tran_date.replace(/Z/g,"");
			            	requirementObject.PaymentInfo.transDate=tran_date;
			        	}
			    	}
		        	
		    
		        	//Pay in Cash and Cheque
		        	requirementObject.PaymentInfo.bankName =$scope.LifeEngageProduct.paymentBankname;
		        	requirementObject.PaymentInfo.bankCode =$scope.LifeEngageProduct.splitTwoBranchCode.trim();
		        	requirementObject.PaymentInfo.branchName =$scope.LifeEngageProduct.splitTwoBranchName.trim();
		        	requirementObject.PaymentInfo.bankCharge=$scope.LifeEngageProduct.splitTwobankCharge.trim();
		        	requirementObject.PaymentInfo.bankAccNo=$scope.LifeEngageProduct.splitTwoAccountNumber.trim();
		        	
		        	if($scope.LifeEngageProduct.splitOneOption=="PayInCheque"){
		        		requirementObject.PaymentInfo.chequeNumber =$scope.LifeEngageProduct.splitTwoChequeNumber;
		        		requirementObject.PaymentInfo.chequeDate =$scope.LifeEngageProduct.splitTwoPayinDate;
		        		requirementObject.PaymentInfo.issBankName=$scope.LifeEngageProduct.splitTwoIssueBank.trim();
			        	requirementObject.PaymentInfo.issBranchName=$scope.LifeEngageProduct.splitTwoIssueBranch.trim();
			        	requirementObject.PaymentInfo.issBranchCode=$scope.LifeEngageProduct.splitTwoIssueBranchCode.trim();
			        	requirementObject.PaymentInfo.branchCode="";
			       }else {
		        		requirementObject.PaymentInfo.chequeNumber ="";
		        		requirementObject.PaymentInfo.chequeDate ="";
		        		requirementObject.PaymentInfo.issBankName="";
			        	requirementObject.PaymentInfo.issBranchName="";
			        	requirementObject.PaymentInfo.issBranchCode="";
			            requirementObject.PaymentInfo.branchCode=$scope.LifeEngageProduct.splitTwoBankBranchCode;	
		        	}
		        	requirementObject.PaymentInfo.creditCardNo ="";
		        	requirementObject.PaymentInfo.approvalCode ="";
		        	requirementObject.PaymentInfo.receiptNumber ="";
		        	requirementObject.PaymentInfo.notes ="";
		        	requirementObject.PaymentInfo.status ="";
		        	requirementObject.PaymentInfo.merchantId ="";
		        
		        	
		        	
		        	if(navigator.onLine==true) {
						if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
							requirementObject.PaymentInfo.etrGeneration="Manual";
						}else{
							if($scope.LifeEngageProduct.ETRgeneration == "manualETR") {
								requirementObject.PaymentInfo.etrGeneration="Manual";
							} else {
								requirementObject.PaymentInfo.etrGeneration="Online";
							}
						}
		        	}else{
	        			requirementObject.PaymentInfo.etrGeneration="Manual";
		        	}
		        			
				//proceed button enable and disable
		        	if(splitone && $rootScope.splitOptionSelected==true){
						
							$rootScope.proceedstep3=false;
							$rootScope.proceedpaymentbtn=true;
						
				$rootScope.showSplitTwo = function () {	
					$scope.splittwoinput=true;
					$scope.showErrorCount = true;
					$scope.splittwoPayDetails=false;
					$scope.EtrPayDetails=false;
					$scope.payConfirmDetails=false;
					$scope.splitTwopayConfirmDetails=false;
					$scope.LifeEngageProduct.splitOneamount=$scope.LifeEngageProduct.splitOneAmountValidate - $scope.LifeEngageProduct.splitOneamount;
				//	$scope.LifeEngageProduct.splitOneOption="";
					$scope.LifeEngageProduct.paymentBankname="";
					$scope.LifeEngageProduct.splitTwobankCharge="";
					$scope.LifeEngageProduct.splitTwoPayinDate="";
					$scope.LifeEngageProduct.splitTwoPayinTime="";
					$scope.LifeEngageProduct.splitTwoEtrNumber="";
					$scope.showSplitOne=false;
					$rootScope.proceedpaymentbtn=false;
					$rootScope.splitTwo = angular.element(document.querySelector(".splittabspaytwo")).addClass("splittabspaytwoactive");
					$rootScope.splitOneRemove = angular.element(document.querySelector(".splittabspayone")).removeClass("splittabspayoneactive");
				}
					}
					
					if(splitone && $rootScope.splitOptionSelected==false){
						
							$rootScope.proceedstep3=true;
							$rootScope.proceedpaymentbtn=false;
					}
					
					if($rootScope.splitOptionSelected == false) {
						requirementObject.PaymentInfo.splitPaymentTst ="No";
					}else if($rootScope.splitOptionSelected == true) {
						requirementObject.PaymentInfo.splitPaymentTst ="Yes";
					}
					requirementObject.PaymentInfo.skipPaymentTst=$scope.LifeEngageProduct.skipPaymentSelection;
					$scope.LifeEngageProduct.Payment.PaymentInfo[0] = requirementObject.PaymentInfo;
					$scope.LifeEngageProduct.paymentSignStatus = "Inprogress";
					if (rootConfig.isDeviceMobile) {
	                        EappVariables.setEappModel($scope.LifeEngageProduct);
	                    }
	           //     EappVariables.EappKeys.Key34 = "PaymentCompleted";
					
	            	/*EappService.saveTransaction(function () {$rootScope.showHideLoadingImage(false);
	                	
	            	}, $scope.onConfirmError, false);*/
					
					$rootScope.showHideLoadingImage(true, 'eapp.SavingInprogress', $translate);
					EappService.saveTransaction($scope.onSaveCallback,$scope.onConfirmError, false);
					$rootScope.showHideLoadingImage(false);
	            	$('#ProceedPaymentSummaryButton').attr('disabled', false);
	          } else if ($scope.LifeEngageProduct.Payment.PaymentInfo[1].paySplit!="SplitTwo") {
	            	requirementObject.PaymentInfo.paymentMethod=$scope.LifeEngageProduct.splitOneOption;
					requirementObject.PaymentInfo.paySplit = "SplitTwo";
	        		if($scope.LifeEngageProduct.splitOneOption=="PayInCash"){
	        			requirementObject.PaymentInfo.paymentType ="Cash";
	        		}else if($scope.LifeEngageProduct.splitOneOption=="PayInCheque"){
	        			requirementObject.PaymentInfo.paymentType ="PersonalCheck";
	        		}
	        		requirementObject.PaymentInfo.etrNumber =$scope.LifeEngageProduct.splitTwoEtrNumber;
	        		requirementObject.PaymentInfo.etrGenerationMode =$scope.LifeEngageProduct.ETRgeneration;
	        		requirementObject.PaymentInfo.paymentDateTime =createdDate;
/*	        		var paymentConvertTime = $scope.LifeEngageProduct.splitTwoPayinTime.toString();
		        	paymentConvertTime = paymentConvertTime.split(' ');
		        	var paymentFinConverstion = paymentConvertTime[4];
		        	requirementObject.PaymentInfo.paymentDateTim = $scope.LifeEngageProduct.splitTwoPayinDate + " " + paymentFinConverstion;
*/		        	
		        	var timevalue = $scope.LifeEngageProduct.splitTwoPayinTime.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
		        	if(timevalue!=null) {
		        	//	timevalue = timevalue.replace(/AM|PM/g,"");
		        		var paymentConvertTime = $scope.time24Format(timevalue);
		        		requirementObject.PaymentInfo.paymentDateTim = $scope.LifeEngageProduct.splitTwoPayinDate + " " +paymentConvertTime.toString().trim();
		        	}
		        	
	        		requirementObject.PaymentInfo.etrDate =createdDate;
	        		requirementObject.PaymentInfo.premiumAmt =$scope.LifeEngageProduct.splitOneamount;
	        		requirementObject.PaymentInfo.cash =$scope.LifeEngageProduct.splitOneamount;

	        		if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
		            	requirementObject.PaymentInfo.transDate=requirementObject.PaymentInfo.paymentDateTim;
		        	}else {
		            	if(data.tr_date !=null){
			        		tran_date = data.tr_date.replace(/T/g," ");
			        		tran_date = tran_date.replace(/Z/g,"");
			            	requirementObject.PaymentInfo.transDate=tran_date;
			        	}
			    	}
		        	//Pay in Cash and Cheque
		        	requirementObject.PaymentInfo.bankName =$scope.LifeEngageProduct.paymentBankname.trim();
		        	requirementObject.PaymentInfo.bankCode =$scope.LifeEngageProduct.splitTwoBranchCode.trim();
		        	requirementObject.PaymentInfo.branchName =$scope.LifeEngageProduct.splitTwoBranchName.trim();
		        	requirementObject.PaymentInfo.bankCharge=$scope.LifeEngageProduct.splitTwobankCharge.trim();
		        	requirementObject.PaymentInfo.bankAccNo=$scope.LifeEngageProduct.splitTwoAccountNumber.trim();
		        	if($scope.LifeEngageProduct.splitOneOption=="PayInCheque"){
		        		requirementObject.PaymentInfo.chequeNumber =$scope.LifeEngageProduct.splitTwoChequeNumber;
		        		requirementObject.PaymentInfo.chequeDate =$scope.LifeEngageProduct.splitTwoPayinDate;
		        		requirementObject.PaymentInfo.issBankName=$scope.LifeEngageProduct.splitTwoIssueBank.trim();
			        	requirementObject.PaymentInfo.issBranchName=$scope.LifeEngageProduct.splitTwoIssueBranch.trim();
			        	requirementObject.PaymentInfo.issBranchCode=$scope.LifeEngageProduct.splitTwoIssueBranchCode.trim();
			        	requirementObject.PaymentInfo.branchCode="";
		        		
		        	}else {
		        		requirementObject.PaymentInfo.chequeNumber ="";
		        		requirementObject.PaymentInfo.chequeDate ="";
		        		requirementObject.PaymentInfo.issBankName="";
			        	requirementObject.PaymentInfo.issBranchName="";
			        	requirementObject.PaymentInfo.issBranchCode="";
			            requirementObject.PaymentInfo.branchCode=$scope.LifeEngageProduct.splitTwoBankBranchCode;	
		        	}
		        	requirementObject.PaymentInfo.creditCardNo ="";
		        	requirementObject.PaymentInfo.approvalCode ="";
		        	requirementObject.PaymentInfo.receiptNumber ="";
		        	requirementObject.PaymentInfo.notes ="";
		        	requirementObject.PaymentInfo.status ="";
		        	requirementObject.PaymentInfo.merchantId ="";
		        
		        	
		        	
	        		if(navigator.onLine==true) {
	        			if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
							requirementObject.PaymentInfo.etrGeneration="Manual";
						}else{
							if($scope.LifeEngageProduct.ETRgeneration == "manualETR") {
								requirementObject.PaymentInfo.etrGeneration="Manual";
							} else {
								requirementObject.PaymentInfo.etrGeneration="Online";
							}
						}
	        		}else{
	        			requirementObject.PaymentInfo.etrGeneration="Manual";
	        		}
	        
	        	
						$rootScope.proceedstep3=true;
						$rootScope.proceedpaymentbtn=false;
						
					        		
				if($rootScope.splitOptionSelected == false) {
					requirementObject.PaymentInfo.splitPaymentTst ="No";
				}else if($rootScope.splitOptionSelected == true) {
					requirementObject.PaymentInfo.splitPaymentTst ="Yes";
				} 
				requirementObject.PaymentInfo.skipPaymentTst=$scope.LifeEngageProduct.skipPaymentSelection;
				$scope.LifeEngageProduct.Payment.PaymentInfo[1] = requirementObject.PaymentInfo;
				$scope.LifeEngageProduct.paymentSignStatus = "Inprogress";
				if (rootConfig.isDeviceMobile) {
                    EappVariables.setEappModel($scope.LifeEngageProduct);
				}
				
	            /*EappService.saveTransaction(function () {$rootScope.showHideLoadingImage(false);
   		            }, $scope.onConfirmError, false);*/
				$rootScope.showHideLoadingImage(true, 'eapp.SavingInprogress', $translate);
				EappService.saveTransaction($scope.onSaveCallback,$scope.onConfirmError, false);
				$rootScope.showHideLoadingImage(false);
	            	$('#ProceedPaymentSummaryButton').attr('disabled', false);
	        }
		}
		else{
		    $rootScope.showHideLoadingImage(false);
	        $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.paymentInvalidErrorMsg"), translateMessages($translate, "fna.ok"));
			angular.element(document.querySelector(".site-overlay")).removeClass("modal-backdrop fade in");
			$("#generateDynamicETRAction .modal").modal({
								backdrop: true,
								keyboard: true
							});
		}
	}	    	
		 $scope.errorInPaymentCallback = function (data) {
		        $rootScope.showHideLoadingImage(false);
		        $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.paymentInvalidErrorMsg"), translateMessages($translate, "fna.ok"));
		        
		        angular.element(document.querySelector(".site-overlay")).removeClass("modal-backdrop fade in");
				$("#generateManualETRAction .modal").modal({
		            backdrop: true,
					keyboard: true
		        });
		        
		    };
	
	//latest code end		
	
		    
		    
		    
		    
/*    $scope.checkForBtnName = function () {
        return 'payNowOffline'
    }
    $scope.updatePaymentOptions = function () {
        $scope.paymentOptions = [];
        if ($scope.paymentinfo.paymentMethod == "OnlineOnline") {
            $scope.paymentOptions = $scope.onlinePaymentOptions;
        } else if ($scope.paymentinfo.paymentMethod == "Offline") {
            $scope.paymentOptions = $scope.offlinePaymentOptions;
        }
        $scope.paymentinfo.paymentType = "";
        $scope.updateErrorCount();
    }
    $scope.subTypeCheck = function () {
        for (i = 0; i <= $scope.paymentOptions.length; i++) {
            if ($scope.paymentinfo.paymentType == $scope.paymentOptions[i].code) {
                $scope.subPaymentType = $scope.paymentOptions[i].type;
                $scope.submitPayment = true;
                break;
            }
        }
    }*/
    /*$scope.onPaymentTypeChange = function () {
        $scope.subTypeCheck();
        if ($scope.paymentinfo.paymentType == "virtual_account") {
            $scope.paymentinfo.accountIdentifier = "00074-10-" + $scope.spajNo;
            $scope.paymentinfo.payerName = "PT. AJ. Generali Indonesia";
        } else if ($scope.paymentinfo.paymentType == "credit_card" && $scope.renewalPayemntType == "CreditCard") {
            $scope.paymentinfo.cardNumber = $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardNumber;
            if ($scope.LifeEngageProduct.Payment.RenewalPayment && $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName)
                $scope.paymentinfo.payerName = $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName.trim();
        } else if ($scope.paymentinfo.paymentType == "bank_auto_debit" && $scope.renewalPayemntType == "DebitAccount") {
            $scope.paymentinfo.bankName = $scope.LifeEngageProduct.Payment.RenewalPayment.bankName;
            $scope.paymentinfo.branchName = $scope.LifeEngageProduct.Payment.RenewalPayment.branchName;
            $scope.paymentinfo.payerName = $scope.LifeEngageProduct.Payment.RenewalPayment.accHolderName;
            $scope.paymentinfo.accountIdentifier = $scope.LifeEngageProduct.Payment.RenewalPayment.accountNumber;
        } else {
            $scope.paymentinfo.accountIdentifier = "";
            $scope.paymentinfo.cardNumber = "";
            $scope.paymentinfo.payerName = "";
        }
        $scope.paymentinfo.receiptNumber = "";
        $scope.paymentinfo.notes = "";
        $timeout(function () {
            $scope.updateErrorCount($scope.selectedTabId);
        }, 0);

    }*/
    /*$scope.updateMandiriCreditCardData = function () {
        if (typeof $scope.mandiriCreditCardNumber1 == 'undefined' || typeof $scope.mandiriCreditCardNumber2 == 'undefined' || typeof $scope.mandiriCreditCardNumber3 == 'undefined' || typeof $scope.mandiriCreditCardNumber4 == 'undefined') {

        } else {
            var creditCardStr = $scope.mandiriCreditCardNumber1 + $scope.mandiriCreditCardNumber2 + $scope.mandiriCreditCardNumber3 + $scope.mandiriCreditCardNumber4;
            if (creditCardStr) {
                $scope.paymentinfo.cardNumber = creditCardStr;
            }
        }
    };
    function updateCreditCardData() {
        if (typeof $scope.creditCardDetail.creditCardNumberone == 'undefined' || typeof $scope.creditCardDetail.creditCardNumbertwo == 'undefined' || typeof $scope.creditCardDetail.creditCardNumberthree == 'undefined' || typeof $scope.creditCardDetail.creditCardNumberfour == 'undefined') {

        } else {
            var creditCardStr = $scope.creditCardDetail.creditCardNumberone + $scope.creditCardDetail.creditCardNumbertwo + $scope.creditCardDetail.creditCardNumberthree + $scope.creditCardDetail.creditCardNumberfour;
            if (creditCardStr) {
                $scope.paymentinfo.cardNumber = creditCardStr;
            }
        }

    };*/

   /* $scope.updateErrorCount = function (id) {
        var _errorCount = 0;
        var _errors = [];

			if ($scope.LifeEngageProduct.splitOneamount == "") {
            _errorCount++;
            var error = {};
            error.message = translateMessages($translate, "eapp.splitOneAmountInput");
            error.key = 'amount';
           _errors.push(error);
        }
		
       $scope.dynamicErrorCount = _errorCount;
        if (!$scope.dynamicErrorMessages) {
            $scope.dynamicErrorMessages = [];
        }
        $scope.dynamicErrorMessages = _errors;

        $timeout(function () {
        }, 0);
        $scope.selectedTabId = id;
    }*/
    $scope.onChangeAmount = function () {
        alert($scope.amountCash);
    }

    function initPaymentDetails() {
        if ($scope.LifeEngageProduct.Payment && $scope.LifeEngageProduct.Payment.PaymentInfo) {
            $scope.paymentinfo = $scope.LifeEngageProduct.Payment.PaymentInfo
        } else {
            $scope.paymentinfo = EappVariables.EappModel.Payment.PaymentInfo;
        }
        if ($scope.totalPremium && $scope.LifeEngageProduct.Payment.PaymentInfo.status != "Payment done") {
            $scope.paymentinfo.cash = $scope.totalPremium;
        }
        $scope.paymentOptions = rootConfig.paymentDetail.PaymentMethods;
        $scope.proposalNo = EappVariables.EappKeys.Key21;
    }

   /* $scope.isEligible = function (option, product, amount, renewalType) {
        var _isEligible = false;
        if (option.supportingProducts.indexOf(product) != -1 && (!option.renewal || option.renewal.indexOf(renewalType) != -1) &&
            option.minAmount <= amount && (option.maxAmount >= amount || option.maxAmount == -1)) {
            _isEligible = true;
        }
        return _isEligible;
    }*/

    $scope.successCallback = function (data) {
        $scope.paymentinfo.transactionRefNumber = data.PaymentInfo.transactionId;
        $scope.LifeEngageProduct.Payment.PaymentInfo = $scope.paymentinfo;
        if (rootConfig.isDeviceMobile && rootConfig.isDeviceMobile != "false") {
            var payWindow = window.open(data.PaymentInfo.redirect_url, '_blank', 'location=yes');
            payWindow.addEventListener('exit', function (event) {
                $scope.onPayWindowClose();
            });
        } else {
            $scope.LifeEngageProduct.Payment.PaymentInfo = $scope.paymentinfo;
            EappService.saveTransaction(function () {
                $scope.onSaveSuccess(data.PaymentInfo.redirect_url);
            }, $scope.onConfirmError, false);
        }
    };
    /** Function to call redirection on desktop */
    $scope.onSaveSuccess = function (url) {
        window.location.href = url;
    };
    $scope.onSaveError = function () {};

    /* $scope.createChargeData = function () {
         var chargeData = {
             "TransactionData": {
                 "PaymentInfo": {
                     "policyholder_name": "",
                     "contract_no": 0,
                     "purpose_of_payment": "",
                     "amount": ""
                 }
             }
         };
         chargeData.TransactionData.PaymentInfo.policyholder_name = $scope.LifeEngageProduct.Proposer.BasicDetails.firstName + "" + $scope.LifeEngageProduct.Proposer.BasicDetails.lastName;
         chargeData.TransactionData.PaymentInfo.contract_no = EappVariables.EappKeys.Key21;
         chargeData.TransactionData.PaymentInfo.purpose_of_payment = "Insurance Premium";
         chargeData.TransactionData.PaymentInfo.amount = $scope.amount;

         return chargeData;

     }*/

    $scope.paymentSubmit = function () {
        if ($scope.paymentinfo.status == "Payment Done") {
            $scope.save();
        }
    }


    
    $scope.validationSuccessCallback = function (data) {
        var msg = translateMessages($translate, "eapp.paymentsuccessful");
        msg = msg.replace("{{proposalno}}", $scope.proposalNo);
        $rootScope.showHideLoadingImage(false);
        if (rootConfig.isDeviceMobile) {
              var newDate = new Date();
              
	            if ($scope.LifeEngageProduct.splitOneOption=="CreditCard" || $scope.LifeEngageProduct.splitOneOption=="Ebanking") {
                    var chargeDataWeb = [{
                        "key": "policyOwner",
                        "value": $scope.LifeEngageProduct.Proposer.BasicDetails.firstName
                    },  {
                        "key": "amount",
                        "value": $scope.LifeEngageProduct.splitOneamount
                    }, {
                        "key": "fromfunction",
                        "value": "GenPay"
                    }];
                    var url = data.info.url;
                    $window.open(url, '_blank');
                    /*payWindow.onload = function () {
                        payWindow.init(chargeDataWeb, url);
                    }*/
                }
	            else if ($scope.LifeEngageProduct.splitOneOption=="SendPaymentURL") {
	        //    	$scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), msg, translateMessages($translate, "fna.ok"), $scope.save);
	            }
        } else {
            
                if ($scope.LifeEngageProduct.splitOneOption=="CreditCard" || $scope.LifeEngageProduct.splitOneOption=="Ebanking") {
                	var newDate = new Date();
                  //  $scope.paymentinfo.date = UtilityService.getFormattedDate(newDate);
               //     $scope.paymentinfo.premiumAmt =  $scope.LifeEngageProduct.splitOneamount;
                   /* EappVariables.EappModel.Payment.PaymentInfo = $scope.paymentinfo;
                    EappService.saveTransaction(successCallBack, errorCallBack, false);*/
                    
                    var chargeDataWeb = [{
                        "key": "policyOwner",
                        "value": $scope.LifeEngageProduct.Proposer.BasicDetails.firstName
                    },  {
                        "key": "amount",
                        "value": $scope.LifeEngageProduct.splitOneamount
                    }, {
                        "key": "fromfunction",
                        "value": "OMMI"
                    }];
                    var url = data.info.url;
                    $window.open(url, '_blank');
                    /*payWindow.onload = function () {
                        payWindow.init(chargeDataWeb, url);
                    }*/
                } 
                /*else if ($scope.LifeEngageProduct.splitOneOption=="SendPaymentURL") {
	            	$scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), msg, translateMessages($translate, "fna.ok"), $scope.save);
	            }*/ 
                else if ($scope.LifeEngageProduct.splitOneOption=="SendPaymentURL") {
	           // 	$scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), msg, translateMessages($translate, "fna.ok"), $scope.save);
	            }

                //onbeforeunload

                /*if($scope.paymentinfo.paymentType == "cards" || $scope.paymentinfo.paymentType == "pos" || $scope.paymentinfo.paymentType == "manual_bank_deposit"){
                		$scope.lePopupCtrl.showWarning(translateMessages($translate,"lifeEngage"),translateMessages($translate,"eapp_vt.paymentErrorMsg1"),translateMessages($translate,"fna.ok"));
                }
                else{
                	$scope.lePopupCtrl.showWarning(translateMessages($translate,"lifeEngage"),translateMessages($translate,"eapp_vt.paymentErrorMsg"),translateMessages($translate,"fna.ok"));
                }*/
          
        }
    }

    
   
    
    function validateOnlinePayment() {
        PersistenceMapping.clearTransactionKeys();
        var transactionObj = PersistenceMapping.mapScopeToPersistence();
        transactionObj.ApplicationNo = EappVariables.EappKeys.Key21;
        transactionObj.PremiumAmount = $scope.totalPremium;
        transactionObj.PaymentOption = "Credit Card";
        transactionObj.Type = "PaymentValidation";
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);

        PaymentService.validateOnlinePayment(transactionObj, $scope.validationSuccessCallback, $scope.errorInPaymentCallback);
    }

    function validateRecieptNo() {
        PersistenceMapping.clearTransactionKeys();
        var transactionObj = PersistenceMapping.mapScopeToPersistence();
        transactionObj.ApplicationNo = EappVariables.EappKeys.Key21;
        transactionObj.Key1 = $scope.paymentinfo.receiptNumber;
        transactionObj.PremiumAmount = $scope.paymentinfo.cash;
        transactionObj.Key3 = transactionObj.Key11;
        transactionObj.Key11 = "";
        transactionObj.Key14 = "";
        transactionObj.Type = "PaymentValidation";
        switch ($scope.paymentinfo.paymentType) {
            case 'cash':
                transactionObj.PaymentOption = 'Cash';
                break;
            case 'manual_bank_deposit':
                transactionObj.PaymentOption = 'Manual bank deposit';
                break;
            case 'pos':
                transactionObj.PaymentOption = 'POS';
                break;
            default:
                break;
        }
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
        PaymentService.validateReciptNumber(transactionObj, $scope.validationSuccessCallback, $scope.errorInPaymentCallback);
    }

    $scope.paynowOffline = function () {
        if ($scope.LifeEngageProduct.Product.ProductDetails.productCode != "1") {
            if ($scope.paymentinfo.paymentType == "virtual_payment") {
                $scope.save();
            } else {
                if ($scope.errorCount > 0) {
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "enterMandatoryIllustration"), translateMessages($translate, "fna.ok"));
                } else {
                    if ($scope.paymentinfo.cash !== "") {
                        if ($scope.paymentinfo.cash < $scope.totalPremium) {
                            var amountLessThanPremiumErrorMsg = translateMessages($translate, "eapp_vt.amountLessThanPremiumErrorMsg");
                            amountLessThanPremiumErrorMsg = amountLessThanPremiumErrorMsg.replace("{{amount}}", $scope.totalPremium);
                            $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), amountLessThanPremiumErrorMsg, translateMessages($translate, "fna.ok"));
                        } else {
                            validateRecieptNo();
                        }
                    }
                }
            }
        } else {
            $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "CIB1DiscontinuedMsg"), translateMessages($translate, "fna.ok"));
        }
    }
    $scope.onPayWindowClose = function (payWindow) {
        validateRecieptNo();
    };
    $scope.paynow = function () {
        if ($scope.LifeEngageProduct.Product.ProductDetails.productCode != "1") {
            if (!$scope.paymentinfo.paymentType && $scope.paymentinfo.paymentType == "") {
                return;
                //Show some popup
            }
            if ($scope.errorCount > 0) {
                $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "enterMandatory"), translateMessages($translate, "fna.ok"));
            } else {
                if ($scope.isUserOnline()) {
                    if ($scope.LifeEngageProduct.Proposer.BasicDetails.fullName) {
                        $scope.policyOwnerName = $scope.LifeEngageProduct.Proposer.BasicDetails.fullName;
                    } else {
                        $scope.policyOwnerName = $scope.LifeEngageProduct.Proposer.BasicDetails.firstName + " " + $scope.LifeEngageProduct.Proposer.BasicDetails.lastName;
                    }
                    $scope.policyNumber = EappVariables.EappKeys.Key21;
                    $scope.paymentDes = "opt1";
                    $scope.amountOnline = $scope.totalPremium;
                    var chargeData = [{
                        "key": "policyOwner",
                        "value": $scope.policyOwnerName
                    }, {
                        "key": "policyNumber",
                        "value": $scope.policyNumber
                    }, {
                        "key": "paymentDes",
                        "value": $scope.paymentDes
                    }, {
                        "key": "amount",
                        "value": $scope.amountOnline
                    }, {
                        "key": "fromfunction",
                        "value": "OMMI"
                    }];
                    if (rootConfig.isDeviceMobile) {
                        $scope.doPaymentOnDevice(chargeData);
                    } else {
                        validateOnlinePayment();
                    }
                }
            }
        } else {
            $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "CIB1DiscontinuedMsg"), translateMessages($translate, "fna.ok"));
        }
    };

    $scope.doPaymentOnDevice = function (chargeData) {
        var url = rootConfig.paymentOnlineURL;
        var index = 0;
        var payment = 'var form =document.createElement("form");form.setAttribute("method","post");form.setAttribute("id","paymentForm");form.setAttribute("action","' + url + '");';
        var filedString;
        for (var data in chargeData) {
            filedString = "";
            filedString += ' var hiddenField' + index + ' = document.createElement("input");';
            filedString += 'hiddenField' + index + '.setAttribute("name", "' + chargeData[data].key + '");';
            filedString += 'hiddenField' + index + '.setAttribute("value", "' + chargeData[data].value + '");';
            filedString += 'hiddenField' + index + '.setAttribute("type", "hidden");';

            filedString += 'form.appendChild(hiddenField' + index + ');';
            payment += filedString;
            index++;
        };
        payment += 'document.body.appendChild(form);document.querySelector("#paymentForm").submit();';
        var payWindow = window.open('pay.html', '_blank', 'location=yes');

        payWindow.addEventListener('loadstop', function (event) {
            payWindow.executeScript({
                code: payment
            });
            payment = "";
        });
        payWindow.addEventListener('loaderror', function (event) {});

        payWindow.addEventListener('exit', function (event) {
            $scope.statusSuccesspayment();
        });
    }
    $scope.doPaymentOnWeb = function (chargeData) {
        var url = rootConfig.paymentOnlineURL;
        var payWindow = window.open('pay.html', '_blank', 'location=yes');
        var form = payWindow.document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("id", "paymentForm");
        form.setAttribute("action", url);
        var index = 0;
        for (var data in chargeData) {

            var hiddenField = payWindow.document.createElement("input");
            hiddenField.setAttribute("name", chargeData[data].key);
            hiddenField.setAttribute("value", chargeData[data].value);
            hiddenField.setAttribute("type", "hidden");

            form.appendChild(hiddenField);
            index++;
        };
        payWindow.document.body.appendChild(form);
        payWindow.document.querySelector("#paymentForm").submit();

    }

    $scope.statusSuccesspayment = function () {
        validateOnlinePayment();
    }
    
   
    /*$scope.populatePayorList = function () {
        $scope.payerNames = [];
        var name = $scope.mergeName($scope.LifeEngageProduct.Proposer.BasicDetails.firstName, $scope.LifeEngageProduct.Proposer.BasicDetails.lastName);
        $scope.payerNames.push(name);
        if ($scope.LifeEngageProduct.Proposer.CustomerRelationship &&
            $scope.LifeEngageProduct.Proposer.CustomerRelationship.isPayorDifferentFromInsured &&
            $scope.LifeEngageProduct.Proposer.CustomerRelationship.isPayorDifferentFromInsured == "Yes") {
            var name = $scope.mergeName($scope.LifeEngageProduct.Insured.BasicDetails.firstName, $scope.LifeEngageProduct.Insured.BasicDetails.lastName);
            if ($scope.payerNames.indexOf(name) == -1) {
                $scope.payerNames.push(name);
            }

        }
        if ($scope.LifeEngageProduct.Beneficiaries) {
            for (var index = 0; index < $scope.LifeEngageProduct.Beneficiaries.length; index++) {
                var name = $scope.mergeName($scope.LifeEngageProduct.Beneficiaries[index].BasicDetails.firstName, $scope.LifeEngageProduct.Beneficiaries[index].BasicDetails.lastName);
                if ($scope.payerNames.indexOf(name) == -1) {
                    $scope.payerNames.push(name);
                }
            }
        }
        if ($scope.LifeEngageProduct.Payment.FinancialInfo.premiumPayer == "Other") {
            if ($scope.LifeEngageProduct.Proposer.AdditionalQuestioniare && typeof ($scope.LifeEngageProduct.Proposer.AdditionalQuestioniare.Questions[51].details) != "undefined") {
                var name = $scope.LifeEngageProduct.Proposer.AdditionalQuestioniare.Questions[51].details;
                if ($scope.payerNames.indexOf(name) == -1) {
                    $scope.payerNames.push(name);
                }
            }

        }

    };*/

    $scope.mergeName = function (fn, ln) {
        var name = fn;
        if (ln) {
            name = name + " " + ln;
        }
        return name.trim();
    }
    
    
	$scope.time24Format = function (time) {
		// var time = '02:05';
		var hours = Number(time.match(/^(\d+)/)[1]);
		var minutes = Number(time.match(/:(\d+)/)[1]);
		if(time.match(/\s(.*)$/) && time.match(/\s(.*)$/) != null){
			var AMPM = time.match(/\s(.*)$/)[1];
			if (AMPM == "PM" && hours < 12) hours = hours + 12;
			if (AMPM == "AM" && hours == 12) hours = hours - 12;
		}
		var sHours = hours.toString();
		var sMinutes = minutes.toString();
		if (hours < 10) sHours = "0" + sHours;
		if (minutes < 10) sMinutes = "0" + sMinutes;
		var sSecond = "00";
		var totalTime = sHours + ":" + sMinutes + ":" + sSecond;
		return totalTime.trim();
	}

    /*$scope.checkRenewalPayment = function () {
        $scope.renewalPayemntType = $scope.LifeEngageProduct.Payment.RenewalPayment.paymentType;
        if ($scope.renewalPayemntType == "CreditCard") {
            $scope.paymentinfo.cardNumber = $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardNumber;
            if ($scope.LifeEngageProduct.Payment.RenewalPayment && $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName)
                $scope.paymentinfo.payerName = $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName.trim();
        } else if ($scope.renewalPayemntType == "DebetAccount") {
            $scope.paymentinfo.bankName = $scope.LifeEngageProduct.Payment.RenewalPayment.bankName;
            $scope.paymentinfo.branchName = $scope.LifeEngageProduct.Payment.RenewalPayment.branchName;
            $scope.paymentinfo.payerName = $scope.LifeEngageProduct.Payment.RenewalPayment.accHolderName;
            $scope.paymentinfo.accountIdentifier = $scope.LifeEngageProduct.Payment.RenewalPayment.accountNumber;
        }
    }*/

    $scope.closeModel = function () {
        $scope.showModel = false;
    }

    $scope.isReadOnly = function () {
        return false;
    }
    $scope.save = function () {
        EappService.saveTransaction($scope.processedToDocument, $scope.onConfirmError, false);
    }

    $scope.processedToDocument = function (data) {
        if ($scope.LifeEngageProduct.Payment.PaymentInfo.status == "Payment done") {
            $('#Payment button[id="btnpayNow"]').attr('disabled', true);
        }
        $rootScope.showHideLoadingImage(true, "Loading. Please Wait...", $translate);
        $scope.selectedTabId = "Payment";
        GLI_EappService.checkRequirementNull($scope.LifeEngageProduct.Requirements, $scope.selectedTabId, function (returnReq) {
            if (returnReq == 0) {
                $scope.runRuleToGetRequirement();
            } else {
                $scope.isRequirementRuleExecuted = false;
                $scope.LifeEngageProduct.LastVisitedUrl = "6,0,'DocumentUpload',false,'tabsinner18',''";
                $scope.saveProposal();
                $rootScope.validationBeforesave = function (value, successcallback) {
                    successcallback();
                };
            }
        });
    };

    $scope.errorCallback = function (data) {
        $rootScope.showHideLoadingImage(false);
    };

   

    $scope.isUserOnline = function () {
        var userOnline = true;
        if ((rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
            if (navigator.network.connection.type == Connection.NONE) {
                userOnline = false;
            }
        }
        if ((rootConfig.isOfflineDesktop)) {
            if (!navigator.onLine) {
                userOnline = false;
            }
        }
        return userOnline;
    }


    $scope.Initialize = function () {
		if(typeof EappVariables.EappKeys.Key34 != "undefined" && (EappVariables.EappKeys.Key34== "PaymentCompleted" || EappVariables.EappKeys.Key34=="DocuploadCompleted")) {
			angular.element(document.querySelector(".site-overlay")).addClass("hideSplitOne");
			$timeout(function () {
				$scope.initailPaintView(5,3,'step3Payment',true,'tabsinner16','');
				angular.element(document.querySelector(".site-overlay")).removeClass("hideSplitOne");
			},500)
			$scope.initailPaintView(5,3,'step3Payment',true,'tabsinner16','');
			return;
		}
		
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_Payment#tabDetails_1']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_Payment#tabDetails_1');
		}
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step2Payment#tabDetails_1']){
   			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step2Payment#tabDetails_1');
        } 
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step3Payment#tabDetails_1']){
   			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step3Payment#tabDetails_1');
		}
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_BeneficiarySubTab#tabDetails_1']){
   			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_BeneficiarySubTab#tabDetails_1');
        }
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_financilaDetailsSubTab#tabDetails_1']){
           	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_financilaDetailsSubTab#tabDetails_1');
        }
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_mainInsuredSubTab#tabDetails_1']){
           	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_mainInsuredSubTab#tabDetails_1');
        }
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
           	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
        }
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6']){
           	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6');
        }
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PolicyHolderSubTab#tabDetails_1']){
           	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PolicyHolderSubTab#tabDetails_1');
        }		
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6']){
   			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6');
   		} 
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6']){
   			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6');
   		}
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6']){
   			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6');
   		}
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6']){
 			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6');
   		}	
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6');
		}
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6');
		}
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6']){
   			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6');
   		}
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
   			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
   		} 
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
   			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
   		}
    	
    	$scope.viewToBeCustomized = 'Step2Payment';
    	$scope.filterData=rootConfig.BranchAdminPaymentOption;
		if($rootScope.splitOptionSelected && $rootScope.splitOptionSelected==true){
			$scope.validationSplitAmt();
		}
		if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
			$scope.disableIRT=true;
		}else{
			$scope.disableIRT=false;
		}
		
		if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
			$scope.paymentTypeFilter=true;
		}else{
			$scope.paymentTypeFilter=false;
		}
    	 $timeout(function () {
             $scope.updateErrorCount($scope.selectedTabId);
             if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission") {
                 UtilityService.disableAllActionElements();
             }else if (typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34=== "PaymentCompleted") {
                UtilityService.disableAllActionElements();
            }
         }, 50);
    	
    	 
    	 UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
         angular.element('#' + $scope.selectedTabId + "_a").trigger('click');
       $timeout(function () {
            $scope.updateErrorCount('Step2Payment');
            $scope.refresh();
        }, 50)
    	 
    	
     	/* $scope.LifeEngageProduct.Payment.PaymentInfo = $scope.LifeEngageProduct.Payment.PaymentInfo.sort(function (a,b) {
         	
        	 var va = (a.paySplit === null) ? "" : "" + a.paySplit,
        		        vb = (b.paySplit === null) ? "" : "" + b.paySplit;
        	 return va > vb ? 1 : ( va === vb ? 0 : -1 );
        });
    	
   
    	
    	 LEDynamicUI.paintUI(rootConfig.template, rootConfig.illustratorUIJson, "HeaderDetails", "#HeaderDetails", true, function () {
             //$scope.loadIllustration();
             if ($scope.viewToBeCustomized)
                 $rootScope.updateErrorCount($scope.viewToBeCustomized);
             $rootScope.showHideLoadingImage(false);
             $scope.refresh();
         }, $scope, $compile);
  
     
     
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            //EappVariables.setEappModel($scope.LifeEngageProduct);
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
        }
        
        $scope.LifeEngageProduct.Payment.PaymentInfo = $scope.LifeEngageProduct.Payment.PaymentInfo.sort(function (a,b) {
        	
        	 var va = (a.paySplit === null) ? "" : "" + a.paySplit,
        		        vb = (b.paySplit === null) ? "" : "" + b.paySplit;
        	 return va > vb ? 1 : ( va === vb ? 0 : -1 );
        });
            
    
    if ($scope.LifeEngageProduct.Beneficiaries.length > 0) {
        $scope.LifeEngageProduct.Beneficiaries = $scope.LifeEngageProduct.Beneficiaries.sort(function (a, b) {
            return parseInt(a.BeneficiaryId) - parseInt(b.BeneficiaryId)
        });
    }


        if($scope.LifeEngageProduct.Payment.PaymentInfo > 0) {
			if($scope.LifeEngageProduct.Payment.PaymentInfo[0].paymentType === "SplitOne"){
				//splitOptionSelected = false;
			}else if ($scope.LifeEngageProduct.Payment.PaymentInfo[1].paymentType === "SplitTwo") {
				//splitOptionSelected = false;
			}
		}
        
        
        //Getting the illustration status
        if (rootConfig.isDeviceMobile) {
            GLI_DataService.getRelatedBIForEapp(EappVariables.EappKeys.Key3, function (IllustrationData) {
                $scope.LifeEngageProduct.Product.ProductDetails.IllustrationStatus = angular.copy(IllustrationData[0].Key15);
                $scope.updateErrorCount($scope.selectedTabId);
            });
        } else {
            var transactionObj = {};
            PersistenceMapping.clearTransactionKeys();
            transactionObj = PersistenceMapping.mapScopeToPersistence({});
            transactionObj.Key1 = EappVariables.EappKeys.Key1;
            transactionObj.Key2 = EappVariables.EappKeys.Key2;
            transactionObj.Key3 = EappVariables.EappKeys.Key24;
            transactionObj.Key4 = EappVariables.EappKeys.Key4;
            transactionObj.Key5 = EappVariables.EappKeys.Key5;
            transactionObj.Key7 = EappVariables.EappKeys.Key7;
            transactionObj.Key15 = EappVariables.EappKeys.Key15;
            transactionObj.Key21 = EappVariables.EappKeys.Key21;
            transactionObj.Key24 = EappVariables.EappKeys.Key24;
            transactionObj.Key26 = EappVariables.EappKeys.Key26;
            transactionObj.Type = "illustration";
            DataService.getListingDetail(transactionObj, function (IllustrationData) {
                $scope.LifeEngageProduct.Product.ProductDetails.IllustrationStatus = angular.copy(IllustrationData[0].Key15);
                $scope.updateErrorCount($scope.selectedTabId);
            }, function () {
                alert("Error");
            });
        }
        $scope.updateErrorCount($scope.selectedTabId);
        //For disabling and enabling the tabs.And also to know the last visited page.---starts
        //$rootScope.selectedPage = "ProposerDetails";
        $scope.LifeEngageProduct.LastVisitedUrl = "5,0,'Payment',false,'tabsinner16',''";
        $scope.eAppParentobj.nextPage = "6,0,'DocumentUpload',false,'tabsinner18',''";
        $scope.eAppParentobj.CurrentPage = "";
        if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission" && EappVariables.EappKeys.Key15 != "Confirmed") {
            UtilityService.disableAllActionElements();
        } else {
            UtilityService.removeDisableElements('Payment');
        }
        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[0] <= $scope.LifeEngageProduct.LastVisitedUrl[0]) {
                $scope.LifeEngageProduct.LastVisitedIndex = "5,0";
            }
        }
        $scope.prodPremium = 0;
        $scope.riderPremium = 0;
        // calculating total premium
        if ($scope.LifeEngageProduct.Product.ProductDetails.initialPremium) {
            $scope.prodPremium = $scope.LifeEngageProduct.Product.ProductDetails.initialPremium;
        }
        if ($scope.LifeEngageProduct.Product.RiderDetails.length > 0) {
            for (i = 0; i < $scope.LifeEngageProduct.Product.RiderDetails.length; i++) {
                $scope.riderPremium = $scope.LifeEngageProduct.Product.RiderDetails[i].initialPremium;
            }
        }
        //$scope.totalPremium = $scope.prodPremium + $scope.riderPremium;
        $scope.totalPremium = $scope.LifeEngageProduct.Product.ProductDetails.totalInitialPremium;
        //For disabling and enabling the tabs.And also to know the last visited page.---ends

        $scope.eAppParentobj.PreviousPage = "";
        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        angular.element('#' + $scope.selectedTabId + "_a").trigger('click');
        initPaymentDetails();
        $timeout(function () {
            if ($scope.LifeEngageProduct.Payment.PaymentInfo.status == "Payment done") {
                $('#step2Payment input[type="radio"]').attr('disabled', false);
				$('input[type="text"]').prop("disabled", false);
                $('#step2Payment input[type="text"]').attr('disabled', 'disabled');
		$('#step2Payment input[type="time"]').attr('disabled', 'disabled');
                $('#step2Payment .iradio').attr('disabled', 'disabled');
            } else {
                $('#step2Payment input[type="radio"]').attr('disabled', false);
                $('#step2Payment input[type="text"]').removeAttr("disabled");
                $('#step2Payment .iradio').removeAttr("disabled");
            }
        }, 0);
        // formatting Date on Tab navigation 
        $rootScope.stringFormattedDate = function () {}
    */
	
	}
	
	//Function to proceed for GAO and Branch admin when payment mode is cash or cheque
	$scope.proceedForGAOBrannchAdmin = function(){
		var message=translateMessages( $translate, "eapp.GAOBranchAdminPayemtMessagePart1")+"'"+$scope.LifeEngageProduct.splitTwoEtrNumber+"'."+translateMessages( $translate, "eapp.GAOBranchAdminPayemtMessagePart2");
		$rootScope.lePopupCtrl.showWarning(translateMessages( $translate, "lifeEngage"),
					message, translateMessages( $translate, "eapp.confirmbtn"), function(){
						if($scope.paymentDone!=true){
							var arrayObj = {"tr_no":""};
							arrayObj.tr_no= $scope.LifeEngageProduct.splitTwoEtrNumber;
							$scope.etrSuccessCallback(arrayObj);
							//$rootScope.step3ScreenLoad ();
							if($rootScope.splitOptionSelected && $rootScope.splitOptionSelected==true && $scope.splitTwoConfirmGAO==true){
								//$rootScope.splitTwoScreenLoad();
								$scope.initailPaintView(5,2,'step2Payment',true,'tabsinner18','');
								$rootScope.showSplitTwo();
								angular.element(document.querySelector(".site-overlay")).removeClass("hideSplitOne");
								$scope.splitTwoConfirmGAO=false;
								$scope.proccedForGAOBranchADmin=false;
							}else{
								angular.element(document.querySelector("#PaymentDetailsSubTab .selector-icon li:nth-child(3)")).removeClass("activated");
								angular.element(document.querySelector("#PaymentDetailsSubTab .selector-text li:nth-child(3)")).removeClass("activated");
								
								angular.element(document.querySelector("#PaymentDetailsSubTab .selector-icon li:nth-child(2)")).addClass("activated");
								angular.element(document.querySelector("#PaymentDetailsSubTab .selector-text li:nth-child(2)")).addClass("activated");
								EappVariables.EappKeys.Key34 = "PaymentCompleted";
								$scope.paintView(5,3,'step3Payment',true,'tabsinner16','');
							}
						}else{
							$rootScope.step3ScreenLoad();
						}
					}, translateMessages( $translate, "btnCancel"), function(){},function(){});
	};
	
    $scope.updateErrorCount = function (id) {
       $rootScope.updateErrorCount(id);
        $scope.selectedTabId = id;
        $scope.eAppParentobj.errorCount = $scope.errorCount;
		
//		$scope.onValueChangeETRSelection($scope.LifeEngageProduct.ETRgeneration);
		if($scope.eAppParentobj.errorCount==0)
		{	
			if ($scope.LifeEngageProduct.ETRgeneration == 'manualETR') {
				$rootScope.etrGenerationSelected = true;
				$('#btnGenerateEtr').attr('disabled', 'disabled');

				if(splitone && $rootScope.splitOptionSelected==true){
					$rootScope.proceedstep3=false;
					// $rootScope.proceedpaymentbtn=true;	
				} 

				if(($scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit=="SplitOne" && $scope.LifeEngageProduct.Payment.PaymentInfo[1].paySplit=="T")
				 || $rootScope.splitOptionSelected==false){
					$rootScope.proceedstep3=true;
					// $rootScope.proceedpaymentbtn=false;	
				}
			} else if ($scope.LifeEngageProduct.ETRgeneration == "autoETR") {
				$('#btnGenerateEtr').removeAttr('disabled');
				$rootScope.proceedstep3=false;		
			}
					
			$scope.ETRButtonEnable=false;
			$scope.ETRUpdateButtonEnable=false;
			$scope.ETRProceedButtonEnable=false;
		}
		else
		{
			$scope.ETRButtonEnable=true;
			$scope.ETRUpdateButtonEnable=true;
			$scope.ETRProceedButtonEnable=true;
			$rootScope.etrGenerationSelected = false;	
			$rootScope.proceedstep3=false;		
		}

		//$scope.refresh();
		
   }
    $scope.showErrorPopup = function (id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
    }

    //PreliminaryDataController Load Event and Load Event Handler
    $scope.$parent.$on('step2Payment', function (event, args) {
        $scope.selectedTabId = "step2Payment";
        $scope.Initialize();
        $rootScope.validationBeforesave = function (value, successcallback) {
            successcallback();
        };
    });

    $scope.runRuleToGetRequirement = function () {
    	$rootScope.showHideLoadingImage(true, 'reqRuleProgress', $translate);
        var transactionObj = RequirementRule();
      //Proposer Details
        if ($scope.LifeEngageProduct.Proposer) {
            //Proposer Questionnaire sections
        	if($scope.LifeEngageProduct.Proposer.Questionnaire.Other){
        		if ($scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions.length > 0) {

        			if ($scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[0]) {
        				transactionObj.QuestionDetails.FatcaQuestions1Ans = $scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[0].details;
        			}
        			if ($scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[1]) {
        				transactionObj.QuestionDetails.FatcaQuestions11Ans = $scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[1].details;
        			}
        			if ($scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[2]) {
        				transactionObj.QuestionDetails.FatcaQuestions12Ans = $scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[2].details;
        			}
        			if ($scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[3]) {
        				transactionObj.QuestionDetails.FatcaQuestions13Ans = $scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[3].details;
        			}
        			if ($scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[4]) {
        				transactionObj.QuestionDetails.FatcaQuestions2Ans = $scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[4].details;
        			}
        			if ($scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[5]) {
        				transactionObj.QuestionDetails.FatcaQuestions3Ans = $scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[5].details;
        			}
        			if ($scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[6]) {
        				transactionObj.QuestionDetails.FatcaQuestions4Ans = $scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[6].details;
        			}

        		}
        	}
        }
        //Insured Details
        if ($scope.LifeEngageProduct.Insured !== undefined && $scope.LifeEngageProduct.Insured.BasicDetails !== undefined) {
        		transactionObj.InsuredDetails.partyIdentifier = UtilityService.getUUID();
        		EappVariables.EappModel.Insured.BasicDetails.UUID = transactionObj.InsuredDetails.partyIdentifier;
                transactionObj.InsuredDetails.age = calculateAge($scope.LifeEngageProduct.Insured.BasicDetails.dob);
				transactionObj.InsuredDetails.insuredNationality = $scope.LifeEngageProduct.Insured.BasicDetails.nationality;
                transactionObj.InsuredDetails.nationality = $scope.LifeEngageProduct.Insured.BasicDetails.nationality;
                transactionObj.InsuredDetails.isInsuredSameAsPayer = $scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer;
        }
        
       //Payer Details
        if ($scope.LifeEngageProduct.Insured !== undefined && $scope.LifeEngageProduct.Insured.CustomerRelationship !== undefined) {
        		transactionObj.PayerDetails.partyIdentifier = UtilityService.getUUID();
        		EappVariables.EappModel.Payer.BasicDetails.UUID = transactionObj.PayerDetails.partyIdentifier;
                transactionObj.PayerDetails.payerRelationshipWithInsured = $scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer;
        }
        
       //Product Details
	   
        if ($scope.LifeEngageProduct.Product !== undefined && $scope.LifeEngageProduct.Product.policyDetails !== undefined) {
                transactionObj.ProductDetails.sumAssured = $scope.LifeEngageProduct.Product.policyDetails.sumInsured;
        }
        
       //Bank Details
        if ($scope.LifeEngageProduct.Payment) {
        	/*if($scope.LifeEngageProduct.Payment.PaymentInfo){
                transactionObj.BankDetails.modeOfPayment = $scope.LifeEngageProduct.Payment.PaymentInfo.paymentType;
				transactionObj.PaymentDetails.ETRNumber = "Online";
        	}*/
        	if($scope.LifeEngageProduct.Payment.RenewalPayment){
                transactionObj.BankDetails.renewalMode = $scope.LifeEngageProduct.Payment.RenewalPayment.paymentFrequency;
                transactionObj.BankDetails.renewalMethod = $scope.LifeEngageProduct.Payment.RenewalPayment.paymentType;
        	}
//        	if($scope.LifeEngageProduct.Payment.PaymentInfo){
//                transactionObj.BankDetails.modeOfPayment = $scope.LifeEngageProduct.Payment.PaymentInfo.paymentType;
//                //transactionObj.PaymentDetails.ETRNumber = $scope.LifeEngageProduct.Payment.PaymentInfo.etrGeneration;
//				transactionObj.PaymentDetails.ETRNumber = "Online";
//        	}
        
        	if(($scope.LifeEngageProduct.Payment.PaymentInfo[0].splitPaymentTst!=undefined && $scope.LifeEngageProduct.Payment.PaymentInfo[0].splitPaymentTst=="Yes") ||  ($scope.LifeEngageProduct.Payment.PaymentInfo[1].splitPaymentTst!=undefined && $scope.LifeEngageProduct.Payment.PaymentInfo[1].splitPaymentTst=="Yes") ){
        		transactionObj.BankDetails.modeOfPayment = $scope.LifeEngageProduct.Payment.PaymentInfo[0].paymentType;
        		transactionObj.BankDetails.modeOfPayment2 = $scope.LifeEngageProduct.Payment.PaymentInfo[1].paymentType;
        		transactionObj.BankDetails.ETRNumber = $scope.LifeEngageProduct.Payment.PaymentInfo[0].etrGeneration;
        		transactionObj.BankDetails.ETRNumber2 = $scope.LifeEngageProduct.Payment.PaymentInfo[1].etrGeneration;
        		transactionObj.BankDetails.isSplitPayment = "Yes";
        	}else {
        		transactionObj.BankDetails.modeOfPayment = $scope.LifeEngageProduct.Payment.PaymentInfo[0].paymentType;
        		transactionObj.BankDetails.ETRNumber = $scope.LifeEngageProduct.Payment.PaymentInfo[0].etrGeneration;
        		transactionObj.BankDetails.modeOfPayment2 = "";
        		transactionObj.BankDetails.ETRNumber2 = "";
        		transactionObj.BankDetails.isSplitPayment = "No";
        	}
        	
        }

        //Application Number
        if($scope.LifeEngageProduct.ApplicationNo.length == 11){
        	transactionObj.ProductDetails.PhysicalApplicationNumber = "No";
        } else{
        	transactionObj.ProductDetails.PhysicalApplicationNumber = "Yes";
        }
        
        EappService.runRequirementRule(transactionObj, function (data) {
			
            $scope.isRequirementRuleExecuted = false;
            $scope.LifeEngageProduct.LastVisitedUrl = "6,0,'DocumentUpload',false,'tabsinner18',''";
            $scope.saveProposal();
            $rootScope.validationBeforesave = function (value, successcallback) {
                successcallback();				
				 $rootScope.loadDocumentSection(); 
            };
        }, $scope.requirementRuleError);	
    };

    $scope.requirementRuleError = function () {
        $rootScope.showHideLoadingImage(false);
        EappService.requirementRuleError();
    };
    $rootScope.validationBeforesave = function (value, successcallback) {
        successcallback();
    };

    //calculating age for Additional Insured
    function calculateAge(dob) {
        if (dob !== "" && dob != undefined && dob !== null && dob !== "Invalid Date") {
            var dobDate = new Date(dob);
            if ((dobDate === "NaN" || dobDate === "Invalid Date")) {
                dobDate = new Date(dob.split("-").join("/"));
            }
            var todayDate = new Date();
            var yyyy = todayDate.getFullYear().toString();
            var mm = (todayDate.getMonth() + 1).toString();
            var dd = todayDate.getDate().toString();
            var formatedDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);

            if (dobDate > todayDate) {
                return;
            } else {
                var age = todayDate.getFullYear() - dobDate.getFullYear();
                if (todayDate.getMonth() < dobDate.getMonth() - 1 || (dobDate.getMonth() - 1 === todayDate.getMonth() && todayDate.getMonth() < dobDate.getDate())) {
                    age--;
                }
                if (dob === undefined || dob === "") {
                    age = "-";
                }
                return age;
            }
        }
    };
    
    
    $scope.onFieldChange = function () {
        $rootScope.updateErrorCount($scope.viewToBeCustomized);
        GLI_IllustratorService.onFieldChange($scope, IllustratorVariables, $rootScope);
        $scope.updateErrorDynamically();
    }
    
    
    $scope.updateErrorDynamically = function () {     
        var date = new Date();
        $scope.dynamicErrorCount = 0;
        $scope.dynamicErrorMessages = [];
        var currDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
        
              
                if(($scope.LifeEngageProduct.splitOneamount==="" || $scope.LifeEngageProduct.splitOneamount===undefined)){                                           
                    var error = {};
                    error.message = translateMessages($translate, "eapp.splitOneAmountInput");
                    error.key = "amount";
                    $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                    $scope.dynamicErrorMessages.push(error);    
                }
        
                
       
        for (index in $scope.dynamicErrorMessages) {
            for (var newindex = Number(index) + 1; newindex < $scope.dynamicErrorMessages.length; newindex++) {
                if ($scope.dynamicErrorMessages[index].key == $scope.dynamicErrorMessages[newindex].key) {
                    $scope.updateDynmaicErrMessageByIndex(newindex);
                    newindex = Number(index);

                }
            }
        }
        if ($scope.viewToBeCustomized != undefined) {
            $rootScope.updateErrorCount($scope.viewToBeCustomized, true);
        }
        $scope.refresh();
    }
    
    $scope.convertPaymentMethod=function(value){
        //Converting Title code to type
        for (var i = 0; i < $rootScope.PaymentOption.length; i++) {
            if (value === $rootScope.PaymentOption[i].code) {
                value = $rootScope.PaymentOption[i].value;
            }
        }
        
        return value;
    }
    
    $scope.dynamicValidations=function() {
        
        $scope.dynamicErrorMessages = [];
        $scope.dynamicErrorCount = 0;
        var _errors = [];
        var _errorCount = 0;
        
         
          if($scope.LifeEngageProduct.splitTwobankCharge!=undefined && $scope.LifeEngageProduct.splitTwobankCharge!="") {
        	  var splitTwoBankCharge = $scope.LifeEngageProduct.splitTwobankCharge.toString();
        	  if(splitTwoBankCharge && splitTwoBankCharge.length >4 || splitTwoBankCharge && splitTwoBankCharge.length <= 0){
               _errorCount++;
               var error = {};
               $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
               error.message = translateMessages($translate,"eapp_vt.eAppEtrBankChargeInvalid");            
               error.key = "paybankCharge";
               _errors.push(error);
               $scope.dynamicErrorMessages = _errors;
        }
      }
	   if($scope.LifeEngageProduct.splitTwoChequeNumber!=undefined && $scope.LifeEngageProduct.splitTwoChequeNumber!="") {
        	  var splitTwoChequeNumber = $scope.LifeEngageProduct.splitTwoChequeNumber.toString();
        	  if(splitTwoChequeNumber && splitTwoChequeNumber.length >15 || splitTwoChequeNumber && splitTwoChequeNumber.length < 3){
               _errorCount++;
               var error = {};
               $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
               error.message = translateMessages($translate,"eapp_vt.eAppEtrChequeNumberInvalid");            
               error.key = "chequeNumber";
               _errors.push(error);
               $scope.dynamicErrorMessages = _errors;
        }
      }
      if($scope.payCheque==false || $scope.payCheque==true){
	  if($scope.LifeEngageProduct.splitOneamount!=undefined && $scope.LifeEngageProduct.splitOneamount!=""){
		 if($scope.LifeEngageProduct.splitOneamount > 100000){
			 _errorCount++;
               var error = {};
               $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
               error.message = translateMessages($translate,"eapp.amountBelowOneLakh");            
               error.key = "amount";
               _errors.push(error);
               $scope.dynamicErrorMessages = _errors;
			
		 }
	  }
	  
	} 	          
       	
	    $scope.dynamicErrorCount = _errorCount;
        if (!$scope.dynamicErrorMessages) {
            $scope.dynamicErrorMessages = [];
        }
        $scope.dynamicErrorMessages = _errors;   
        $timeout(function () {
            $rootScope.updateErrorCount($scope.selectedTabId);
        }, 0);
        $scope.selectedTabId = id;
        $scope.refresh();
		
	}
	   

    $scope.validationSplitAmt=function() {
    
    $scope.dynamicErrorMessages = [];
    $scope.dynamicErrorCount = 0;
    var _errors = [];
    var _errorCount = 0;
    
    if($scope.splitOptionSelected==true && $scope.splitoneinput==true) {
    	var splitPaymentAmt = $scope.LifeEngageProduct.splitOneamount;
    	  if(splitPaymentAmt && splitPaymentAmt > $scope.LifeEngageProduct.Product.premiumSummary.totalPremium){
           _errorCount++;
           var error = {};
           $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
           error.message = translateMessages($translate,"eapp_vt.eappSplitNegativeAmt");            
           error.key = "splitoneamount";
           _errors.push(error);
           $scope.dynamicErrorMessages = _errors;
    }
    }
      
    $scope.dynamicErrorCount = _errorCount;
    if (!$scope.dynamicErrorMessages) {
        $scope.dynamicErrorMessages = [];
    }
    $scope.dynamicErrorMessages = _errors;   
    $scope.updateErrorCount('step2Payment');
    $scope.refresh();
}

$scope.updateBranchDetails=function(){
    if($scope.LifeEngageProduct.splitTwoIssueBank!==undefined && $scope.LifeEngageProduct.splitTwoIssueBank!==""){
        for (var i = 0; i < $rootScope.issueBranchPayment.length; i++) {
            if($scope.LifeEngageProduct.splitTwoIssueBank===$rootScope.issueBranchPayment[i].code){
                $scope.LifeEngageProduct.splitTwoIssueBranch = $rootScope.issueBranchPayment[i].value.trim();
            }
        }   
        
        for (var i = 0; i < $rootScope.issueBranchCodePayment.length; i++) {
            if($scope.LifeEngageProduct.splitTwoIssueBank===$rootScope.issueBranchCodePayment[i].code){
                $scope.LifeEngageProduct.splitTwoIssueBranchCode = $rootScope.issueBranchCodePayment[i].value.trim();
            }
        }
    }else{
        $scope.LifeEngageProduct.splitTwoIssueBranch="";
        $scope.LifeEngageProduct.splitTwoIssueBranchCode="";
    }
}

$scope.updateReceiveBranchDetails=function(){
    if($scope.LifeEngageProduct.paymentBankname!==undefined && $scope.LifeEngageProduct.paymentBankname!==""){
        for (var i = 0; i < $rootScope.receiveBranchPayment.length; i++) {
            if($scope.LifeEngageProduct.paymentBankname===$rootScope.receiveBranchPayment[i].code){
                $scope.LifeEngageProduct.splitTwoBranchName = $rootScope.receiveBranchPayment[i].value.trim();
            }
        } 
        
        for (var i = 0; i < $rootScope.receiveBranchCodePayment.length; i++) {
            if($scope.LifeEngageProduct.paymentBankname===$rootScope.receiveBranchCodePayment[i].code){
                $scope.LifeEngageProduct.splitTwoBranchCode = $rootScope.receiveBranchCodePayment[i].value.trim();
            }
        }
        
        for (var i = 0; i < $rootScope.PaymentBankName.length; i++) {
            if($scope.LifeEngageProduct.paymentBankname===$rootScope.PaymentBankName[i].code){
                var accountNumberSplit=$rootScope.PaymentBankName[i].value;
                var splitAccountNumber=[];
                if(accountNumberSplit.indexOf('(')>0){				    
       		        splitAccountNumber=accountNumberSplit.split('(');
                    $scope.LifeEngageProduct.splitTwoAccountNumber = splitAccountNumber[1].replace(")","");
                    $scope.LifeEngageProduct.splitTwoAccountNumber=$scope.LifeEngageProduct.splitTwoAccountNumber.replace(/-/g,"");
                }else{
                    $scope.LifeEngageProduct.splitTwoAccountNumber = "";   
                } 
            }
        } 
        
        if($scope.LifeEngageProduct.splitOneOption && $scope.LifeEngageProduct.splitOneOption=="PayInCash"){        	
        	for (var i = 0; i < $rootScope.payinCashBranchCodePayment.length; i++) {
                if($scope.LifeEngageProduct.paymentBankname===$rootScope.payinCashBranchCodePayment[i].code){
                    $scope.LifeEngageProduct.splitTwoBankBranchCode = $rootScope.payinCashBranchCodePayment[i].value.trim();
                }
            } 
        }else{
            $scope.LifeEngageProduct.splitTwoBankBranchCode="";
        }
        
    }else{
        $scope.LifeEngageProduct.splitTwoBranchName="";     
        $scope.LifeEngageProduct.splitTwoBranchCode="";
        $scope.LifeEngageProduct.splitTwoBankBranchCode="";
        $scope.LifeEngageProduct.splitTwoAccountNumber = "";  
    }
}


$scope.whatsappSuccessCallback = function (data) {
    var msg = translateMessages($translate, "eapp.paymentsuccessful");
    msg = msg.replace("{{proposalno}}", $scope.proposalNo);
    $rootScope.showHideLoadingImage(false);
    var urlMsg = data.TransactionData.paymentUrl;
    UtilityService.sendWhatsApp(urlMsg);

}

$scope.linkappSuccessCallback = function (data) {
    var msg = translateMessages($translate, "eapp.paymentsuccessful");
    msg = msg.replace("{{proposalno}}", $scope.proposalNo);
    $rootScope.showHideLoadingImage(false);
    var urlMsg = data.TransactionData.paymentUrl;
   // UtilityService.sendLinkApp(urlMsg);
	//$location.url("line://msg/text/?"+urlMsg);
	location.href="line://msg/text/?"+urlMsg;

}

$scope.onValueChangeETRSelection = function(value) {

	if (value == "manualETR") {
		$scope.disableIRT = true;
	} else if (value == "autoETR") {
		$scope.disableIRT = false;
	}
	$scope.refresh();
	$timeout(function () {
			 $scope.updateErrorCount($scope.selectedTabId);
			 if($scope.eAppParentobj.errorCount==0 || ($scope.eAppParentobj.errorCount==1 && $scope.LifeEngageProduct.splitTwoEtrNumber == undefined)) {
				if (value == "manualETR") {
					$scope.disableIRT = true;
					$rootScope.etrGenerationSelected = true;

					$('#btnGenerateEtr').attr('disabled', 'disabled');									
				} else if (value == "autoETR") {
					$scope.disableIRT = false;
					$('#btnGenerateEtr').removeAttr('disabled');
					$rootScope.etrGenerationSelected = false;	
					$scope.LifeEngageProduct.splitTwoEtrNumber = '';			
				}
				if ($scope.eAppParentobj.errorCount!=0) {
				 	$rootScope.etrGenerationSelected = false;
				 }
			} 
			if ($scope.eAppParentobj.errorCount > 0) {
				if (value == "autoETR") {
					$scope.LifeEngageProduct.splitTwoEtrNumber = '';			
				}
			}
		 }, 50);	 
}

$scope.onSaveCallback = function() {
	$rootScope.showHideLoadingImage(false);
}
$scope.onConfirmError = function() {
	$rootScope.showHideLoadingImage(false);
}

$scope.paynowWhatsapp = function() {
	$scope.sendbyWhatsapp="Whatsapp";
	$scope.updateEmailPopup();
}


$scope.paynowLinkapp = function() {
	$scope.sendbyWhatsapp="Linkapp";
	$scope.updateEmailPopup();
}

if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
    $scope.customPaymentOption=angular.copy($rootScope.PaymentOption);        
    $scope.customPaymentOption.splice(5,1);    
    $scope.customPaymentOption.splice(2,1);   
    $scope.customPaymentOption.splice(1,1);
    $scope.customPaymentOption.splice(0,1);    
}else{
    $scope.customPaymentOption=angular.copy($rootScope.PaymentOption);      
}

} 