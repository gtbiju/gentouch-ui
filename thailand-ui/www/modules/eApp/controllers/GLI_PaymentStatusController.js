'use strict';
storeApp.controller('GLI_PaymentStatusController', GLI_PaymentStatusController);
GLI_PaymentStatusController.$inject = ['$rootScope', '$scope', 'DataService', 'GLI_DataService', 'LookupService', 'DocumentService', 'ProductService', '$compile', '$routeParams','$window', '$location', '$translate', '$debounce', 'UtilityService', 'PersistenceMapping', 'AutoSave', 'globalService', 'EappVariables', 'EappService', 'PaymentService', '$timeout', 'GLI_EappService', 'UserDetailsService'];

function GLI_PaymentStatusController($rootScope, $scope, DataService, GLI_DataService, LookupService, DocumentService, ProductService, $compile, $routeParams,$window, $location, $translate, $debounce, UtilityService, PersistenceMapping, AutoSave, globalService, EappVariables, EappService, PaymentService, $timeout, GLI_EappService, UserDetailsService) {
    $scope.paymentinfo;
	
    $scope.numberPattern = rootConfig.numberPattern;
    $scope.alphanumericPattern = rootConfig.alphanumericPattern;
    $scope.showModel = false;
    $scope.isSingleTopup = false;
    $scope.payerNames = [];
    $scope.popupMessage = "";
    $scope.supportingbanks = ["BCA", "BNI", "Niaga"];
    $scope.creditCardDetail = {};
    $scope.creditCardDetail.creditCardNumberone = "";
    $scope.creditCardDetail.creditCardNumbertwo = "";
    $scope.creditCardDetail.creditCardNumberthree = "";
    $scope.creditCardDetail.creditCardNumberfour = "";
    $scope.mandiriCreditCardNumber1 = "";
    $scope.mandiriCreditCardNumber2 = "";
    $scope.mandiriCreditCardNumber3 = "";
    $scope.mandiriCreditCardNumber4 = "";
    $scope.onlineMessage = "";
    $rootScope.selectedPage = "step1Payment";
    $scope.isDeviceMobile = rootConfig.isDeviceMobile;
    $scope.submitPayment = false;
	$scope.isPopupDisplayed = false;
	//my latest code start

	$scope.payConfirmDetails=false;
	$scope.singlePayConfirmDetails=false;
	$scope.splitoneinput=true;
	$scope.splittwoinput=false;
	$scope.splittwoPayDetails=false;
	$scope.EtrPayDetails=false;
	$scope.singlepaymentInput=true;
	$scope.payConfirmaction=false;
	
	//$scope.modifiedPaymentList = [];
	$rootScope.proceedpaymentbtn=false;
	$rootScope.proceedstep3=false;
	$rootScope.splitOptionSelected;
	$rootScope.showSplitTwo=false;
	navigator.onLine;

	
	//get premium amount details to display in split one
	
    $scope.updateErrorCount = function (id) {
        updateCreditCardData();
        var _errorCount = 0;
        var _errors = [];
        if ($scope.paymentinfo && $scope.paymentinfo.paymentType && $scope.paymentinfo.paymentType == "credit_card") {
            if (!$scope.paymentinfo.payerName || $scope.paymentinfo.payerName == "") {
                _errorCount++;
                var error = {};
                error.message = translateMessages($translate, "eapp.nameOfCreditcardHolderRequiredValidationMessage");
                error.key = 'ccpayerName';
                _errors.push(error);
            }
            if (!$scope.paymentinfo.cardNumber || $scope.paymentinfo.cardNumber == "" || $scope.paymentinfo.cardNumber.length != 16) {
                _errorCount++;
                var error = {};
                error.key = 'creditCardNumber_1';
                error.message = translateMessages($translate, "eapp.creditCardNumberRequiredValidationMessage");
                _errors.push(error);
            }

        }
			if ($scope.LifeEngageProduct.splitOneamount == "") {
            _errorCount++;
            var error = {};
            error.message = translateMessages($translate, "eapp.splitOneAmountInput");
            error.key = 'amount';
            _errors.push(error);
        }
		
        if ($scope.LifeEngageProduct.Product.ProductDetails.IllustrationStatus != "Confirmed") {
            _errorCount++;
            var error = {};
            error.message = translateMessages($translate, "illustrationNotSigned");
            error.key = 'BISign';
            _errors.push(error);
        }
        if ($scope.LifeEngageProduct.Declaration.confirmSignature != false || EappVariables.EappKeys.Key15 == "New" || EappVariables.EappKeys.Key15 == undefined || EappVariables.EappKeys.Key15 == "undefined" || EappVariables.EappKeys.Key15 == "") {
            _errorCount++;
            var error = {};
            error.message = translateMessages($translate, "declarationNotSigned");
            error.key = 'declarationSign';
            _errors.push(error);
        }
//        if ($scope.LifeEngageProduct.Declaration.confirmAgentSignature != false) {
//            _errorCount++;
//            var error = {};
//            error.message = translateMessages($translate, "ACRNotSigned");
//            error.key = 'ACRSign';
//            _errors.push(error);
//        }
        $scope.dynamicErrorCount = _errorCount;
        if (!$scope.dynamicErrorMessages) {
            $scope.dynamicErrorMessages = [];
        }
        $scope.dynamicErrorMessages = _errors;

        $timeout(function () {
            $rootScope.updateErrorCount($scope.selectedTabId);
        }, 0);
        $scope.selectedTabId = id;
    }

    $scope.successCallback = function (data) {
        $scope.paymentinfo.transactionRefNumber = data.PaymentInfo.transactionId;
        $scope.LifeEngageProduct.Payment.PaymentInfo = $scope.paymentinfo;
        if (rootConfig.isDeviceMobile && rootConfig.isDeviceMobile != "false") {
            var payWindow = window.open(data.PaymentInfo.redirect_url, '_blank', 'location=yes');
            payWindow.addEventListener('exit', function (event) {
                $scope.onPayWindowClose();
            });
        } else {
            $scope.LifeEngageProduct.Payment.PaymentInfo = $scope.paymentinfo;
            EappService.saveTransaction(function () {
                $scope.onSaveSuccess(data.PaymentInfo.redirect_url);
            }, $scope.onConfirmError, false);
        }
    };
    /** Function to call redirection on desktop */
    $scope.onSaveSuccess = function (url) {
        window.location.href = url;
    };
   

    $scope.statusSuccesspayment = function () {
        validateOnlinePayment();
    }
    
    $scope.closeModel = function () {
        $scope.showModel = false;
    }

    $scope.isReadOnly = function () {
        return false;
    }
    $scope.save = function () {
        if (!$scope.paymentinfo && $scope.paymentinfo == "") {
            return;
            //Show some popup
        }

        $scope.paymentinfo.status = "Payment done";
        EappVariables.EappKeys.Key15 = "PaymentDone";
        EappVariables.EappModel.Payment.PaymentInfo = $scope.paymentinfo;
        EappService.saveTransaction($scope.processedToDocument, $scope.onConfirmError, false);
    }

    $scope.processedToDocument = function (data) {
        if ($scope.LifeEngageProduct.Payment.PaymentInfo.status == "Payment done") {
            $('#Payment button[id="btnpayNow"]').attr('disabled', true);
        }
        $rootScope.showHideLoadingImage(true, "Loading. Please Wait...", $translate);
        $scope.selectedTabId = "Payment";
        GLI_EappService.checkRequirementNull($scope.LifeEngageProduct.Requirements, $scope.selectedTabId, function (returnReq) {
            if (returnReq == 0) {
                $scope.runRuleToGetRequirement();
            } else {
                $scope.isRequirementRuleExecuted = false;
                $scope.LifeEngageProduct.LastVisitedUrl = "6,0,'DocumentUpload',false,'tabsinner18',''";
                $scope.saveProposal();
                $rootScope.validationBeforesave = function (value, successcallback) {
                    successcallback();
                };
            }
        });
    };

    $scope.errorCallback = function (data) {
        $rootScope.showHideLoadingImage(false);
    };


    $scope.Initialize = function () {
    	
    	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_Payment#tabDetails_1']){
    		localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_Payment#tabDetails_1');
        }
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step2Payment#tabDetails_1']){
           	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step2Payment#tabDetails_1');
        } 
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step3Payment#tabDetails_1']){
           	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step3Payment#tabDetails_1');
        }
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_BeneficiarySubTab#tabDetails_1']){
           	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_BeneficiarySubTab#tabDetails_1');
        }
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_financilaDetailsSubTab#tabDetails_1']){
           	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_financilaDetailsSubTab#tabDetails_1');
        }
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_mainInsuredSubTab#tabDetails_1']){
           	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_mainInsuredSubTab#tabDetails_1');
        }
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
           	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
        }
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6']){
           	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6');
        }
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PolicyHolderSubTab#tabDetails_1']){
           	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PolicyHolderSubTab#tabDetails_1');
        }		
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6']){
   			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6');
   		} 
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6']){
   			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6');
   		}
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6']){
   			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6');
   		}
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6']){
   			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6');
   		}		
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6');
		}
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6');
		}
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6']){
   			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6');
   		}
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
   			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
   		} 
   		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
   			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
   		}
    	
    	$scope.modifiedPaymentList =[];
    	if($scope.LifeEngageProduct.Payment.PaymentInfo && $scope.LifeEngageProduct.Payment.PaymentInfo.length>0){
    		$scope.LifeEngageProduct.Payment.PaymentInfo = $scope.LifeEngageProduct.Payment.PaymentInfo.sort(function (a,b) {
    			if(a.paySplit===""){
    				a.paySplit="T";
   			 	}
   			 	if(b.paySplit===""){
   			 		b.paySplit="T";
   			 	}
   			 	var va = (a.paySplit === null) ? "" : "" + a.paySplit,
   					vb = (b.paySplit === null) ? "" : "" + b.paySplit;
   			 	return va > vb ? 1 : ( va === vb ? 0 : -1 );
    		});
    	}
    	
    	if($scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit=="SplitOne") {
    		var arryObj = {"paySplit": "", "premiumAmt": "","payMethod": "","etrNumber": "","paymentDateTime": "", "bankName": "" }
    		if($scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit == "SplitOne"){
    			arryObj.paySplit="Split One";
    		}
    		arryObj.premiumAmt = $scope.LifeEngageProduct.Payment.PaymentInfo[0].premiumAmt;
    		if($scope.LifeEngageProduct.Payment.PaymentInfo[0].paymentType=="Cash") {
    			arryObj.payMethod = "Pay in by Cash";
    			arryObj.etrNumber = $scope.LifeEngageProduct.Payment.PaymentInfo[0].etrNumber;
    		} else if($scope.LifeEngageProduct.Payment.PaymentInfo[0].paymentType!='undefined' && $scope.LifeEngageProduct.Payment.PaymentInfo[0].paymentType=="PersonalCheck") {
    			arryObj.payMethod = "Pay in by Cheque";
    			arryObj.etrNumber = $scope.LifeEngageProduct.Payment.PaymentInfo[0].etrNumber;
    		} else {
    			arryObj.payMethod = $scope.LifeEngageProduct.Payment.PaymentInfo[0].paymentType;
    			arryObj.etrNumber = $scope.LifeEngageProduct.Payment.PaymentInfo[0].receiptNumber;
    		}
    		//arryObj.paymentDateTime = InternalToPaymentString($scope.LifeEngageProduct.Payment.PaymentInfo[0].paymentDateTim);
    		arryObj.paymentDateTime = $scope.LifeEngageProduct.Payment.PaymentInfo[0].paymentDateTim;
    		arryObj.bankName = $scope.LifeEngageProduct.Payment.PaymentInfo[0].bankName;
    		$scope.modifiedPaymentList.push(arryObj);
    	}
		
    	if($scope.LifeEngageProduct.Payment.PaymentInfo[1].paySplit=="SplitTwo") {
    		var arryObj = {"paySplit": "", "premiumAmt": "","payMethod": "","etrNumber": "","paymentDateTime": "", "bankName": "" }
    		if($scope.LifeEngageProduct.Payment.PaymentInfo[1].paySplit == "SplitTwo"){
    			arryObj.paySplit="Split Two";
    		}
    		arryObj.premiumAmt = $scope.LifeEngageProduct.Payment.PaymentInfo[1].premiumAmt;
    		if($scope.LifeEngageProduct.Payment.PaymentInfo[1].paymentType!='undefined' && $scope.LifeEngageProduct.Payment.PaymentInfo[1].paymentType=="Cash") {
    			arryObj.payMethod = "Pay in by Cash";
    			arryObj.etrNumber = $scope.LifeEngageProduct.Payment.PaymentInfo[1].etrNumber;
    		}else if ($scope.LifeEngageProduct.Payment.PaymentInfo[1].paymentType!='undefined' && $scope.LifeEngageProduct.Payment.PaymentInfo[1].paymentType=="PersonalCheck") {
    			arryObj.payMethod = "Pay in by Cheque";
    			arryObj.etrNumber = $scope.LifeEngageProduct.Payment.PaymentInfo[1].etrNumber;
    		}else{
    			arryObj.payMethod = $scope.LifeEngageProduct.Payment.PaymentInfo[1].paymentType;
    			arryObj.etrNumber = $scope.LifeEngageProduct.Payment.PaymentInfo[1].receiptNumber;
    		}
    		arryObj.paymentDateTime = $scope.LifeEngageProduct.Payment.PaymentInfo[1].paymentDateTim;
    		arryObj.bankName = $scope.LifeEngageProduct.Payment.PaymentInfo[1].bankName;
    		$scope.modifiedPaymentList.push(arryObj);
    	}
    	
		if ((typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34=== "PaymentCompleted") && (EappVariables.EappKeys.Key15==="Pending Submission" || EappVariables.EappKeys.Key15===undefined)) {            
            $('button#paymentButtonDisable').removeAttr("disabled");
        }else{
            $('button#paymentButtonDisable').attr("disabled", "disabled");
        }
	    	/*
			for (var i = 0;i < $scope.LifeEngageProduct.Payment.PaymentInfo.length-1; i++) {
				var arryObj = {
	                "paySplit": "",
	                "premiumAmt": "",
	                "paymentType": "",
	                "etrNumber": "",
	                "paymentDateTime": "",
	                "bankName": ""
	            }
				arryObj.paySplit = $scope.LifeEngageProduct.Payment.PaymentInfo[i].paySplit;
				arryObj.premiumAmt = $scope.LifeEngageProduct.Payment.PaymentInfo[i].premiumAmt;
				arryObj.paymentType =$scope.LifeEngageProduct.Payment.PaymentInfo[i].paymentType;
				arryObj.etrNumber =$scope.LifeEngageProduct.Payment.PaymentInfo[i].etrNumber;
				arryObj.paymentDateTime = $scope.LifeEngageProduct.Payment.PaymentInfo[i].paymentDateTime;
				arryObj.bankName = $scope.LifeEngageProduct.Payment.PaymentInfo[i].bankName;
				$scope.modifiedPaymentList.push(arryObj);
			}
	    	
	    	LEDynamicUI.paintUI(rootConfig.template, "step3Payment.json", "step3Payment", "#step3Payment", true, function () {
	        	$scope.callback();
	            PersistenceMapping.clearTransactionKeys();
	            var searchCriteria = {
		            searchApplNumber: {
		                "applicationNum": "ListingDashBoard",
		            }
	            };
	         	var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
	         	transactionObj.Id = $routeParams.transactionId;
	         	DataService.getFilteredListing(transactionObj, $scope.onGetListingsSuccess, $scope.onGetListingsError);
	         }, $scope, $compile);
	    	
	         if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
	            //EappVariables.setEappModel($scope.LifeEngageProduct);
	            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
	         }
	         //Getting the illustration status
	         if (rootConfig.isDeviceMobile) {
	            GLI_DataService.getRelatedBIForEapp(EappVariables.EappKeys.Key3, function (IllustrationData) {
	                $scope.LifeEngageProduct.Product.ProductDetails.IllustrationStatus = angular.copy(IllustrationData[0].Key15);
	                $scope.updateErrorCount($scope.selectedTabId);
	            });
	         } else {
	         	var transactionObj = {};
	            PersistenceMapping.clearTransactionKeys();
	            transactionObj = PersistenceMapping.mapScopeToPersistence({});
	            transactionObj.Key1 = EappVariables.EappKeys.Key1;
	            transactionObj.Key2 = EappVariables.EappKeys.Key2;
	            transactionObj.Key3 = EappVariables.EappKeys.Key24;
	            transactionObj.Key4 = EappVariables.EappKeys.Key4;
	            transactionObj.Key5 = EappVariables.EappKeys.Key5;
	            transactionObj.Key7 = EappVariables.EappKeys.Key7;
	            transactionObj.Key15 = EappVariables.EappKeys.Key15;
	            transactionObj.Key21 = EappVariables.EappKeys.Key21;
	            transactionObj.Key24 = EappVariables.EappKeys.Key24;
	            transactionObj.Key26 = EappVariables.EappKeys.Key26;
	            transactionObj.Type = "illustration";
	            DataService.getListingDetail(transactionObj, function (IllustrationData) {
	                $scope.LifeEngageProduct.Product.ProductDetails.IllustrationStatus = angular.copy(IllustrationData[0].Key15);
	                $scope.updateErrorCount($scope.selectedTabId);
	            }, function () {
	                alert("Error");
	            });
	        }
	        $scope.updateErrorCount($scope.selectedTabId);
	        //For disabling and enabling the tabs.And also to know the last visited page.---starts
	        //$rootScope.selectedPage = "ProposerDetails";
	        $scope.LifeEngageProduct.LastVisitedUrl = "5,0,'Payment',false,'tabsinner16',''";
	        $scope.eAppParentobj.nextPage = "6,0,'DocumentUpload',false,'tabsinner18',''";
	        $scope.eAppParentobj.CurrentPage = "";
	        if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission" && EappVariables.EappKeys.Key15 != "Confirmed") {
	            UtilityService.disableAllActionElements();
	        } else {
	            UtilityService.removeDisableElements('Payment');
	        }
	        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
	            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
	            if (subIndex[0] <= $scope.LifeEngageProduct.LastVisitedUrl[0]) {
	                $scope.LifeEngageProduct.LastVisitedIndex = "5,0";
	            }
	        }
	        $scope.prodPremium = 0;
	        $scope.riderPremium = 0;
	        // calculating total premium
	        if ($scope.LifeEngageProduct.Product.ProductDetails.initialPremium) {
	            $scope.prodPremium = $scope.LifeEngageProduct.Product.ProductDetails.initialPremium;
	        }
	        if ($scope.LifeEngageProduct.Product.RiderDetails.length > 0) {
	            for (i = 0; i < $scope.LifeEngageProduct.Product.RiderDetails.length; i++) {
	                $scope.riderPremium = $scope.LifeEngageProduct.Product.RiderDetails[i].initialPremium;
	            }
	        }
	        //$scope.totalPremium = $scope.prodPremium + $scope.riderPremium;
	        $scope.totalPremium = $scope.LifeEngageProduct.Product.ProductDetails.totalInitialPremium;
	        //For disabling and enabling the tabs.And also to know the last visited page.---ends
	        $scope.eAppParentobj.PreviousPage = "";
	        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
	        angular.element('#' + $scope.selectedTabId + "_a").trigger('click');
	        initPaymentDetails();
	        $timeout(function () {
	            if ($scope.LifeEngageProduct.Payment.PaymentInfo.status == "Payment done") {
	                $('#step2Payment input[type="radio"]').attr('disabled', true);
	                $('#step2Payment input[type="text"]').attr('disabled', 'disabled');
	                $('#step2Payment .iradio').attr('disabled', 'disabled');
	            } else {
	                $('#step2Payment input[type="radio"]').attr('disabled', false);
	                $('#step2Payment input[type="text"]').removeAttr("disabled");
	                $('#step2Payment .iradio').removeAttr("disabled");
	            }
	        }, 0);
	        // formatting Date on Tab navigation 
	        $rootScope.stringFormattedDate = function () {}
	    */
	}
    
    $scope.onGetListingsSuccess = function (dbObject) {
        var dataNew = [];
        for (var i = 0; i < dbObject.length; i++) {
            var requiredData = {};
            requiredData.Id = dbObject[i].Id;
            requiredData.Key1 = dbObject[i].Key1;
            requiredData.Key2 = dbObject[i].Key2;
            requiredData.Key3 = dbObject[i].Key3;
            requiredData.Key4 = dbObject[i].Key4;
            requiredData.Key5 = dbObject[i].Key5;
            requiredData.Key6 = dbObject[i].Key6;
            requiredData.Key7 = dbObject[i].Key7;
            requiredData.Key8 = dbObject[i].Key8;
            requiredData.Key14 = dbObject[i].Key14;
            requiredData.Key11 = dbObject[i].Key11;
            requiredData.Key15 = dbObject[i].Key15;
            requiredData.Key16 = dbObject[i].Key16;
            requiredData.Key17 = dbObject[i].Key17;
            requiredData.Key18 = dbObject[i].Key18;
            if (dbObject[i].Key15 == 'Successful Sale') {
                requiredData.Key19 = "";
            } else {
				requiredData.Key19 = dbObject[i].Key19;
			}
			if(rootConfig.isDeviceMobile){
				requiredData.modifiedDate=dbObject[i].modifiedDate;
			}
            requiredData.Key20 = dbObject[i].Key20;
            requiredData.Key21 = dbObject[i].Key21;
            requiredData.Key22 = dbObject[i].Key22;
			requiredData.Key26 = dbObject[i].Key26;
            requiredData.Type = dbObject[i].Type;
            requiredData.fullName = dbObject[i].Key2 + " " + dbObject[i].Key3;
            requiredData.illustrationCount = dbObject[i].illustrationCount;
            requiredData.eAppCount = dbObject[i].eAppCount;
            requiredData.fnaCount = dbObject[i].fnaCount;
            requiredData.TransTrackingID = dbObject[i].TransTrackingID;
            dataNew.push(requiredData);
        }
		
		if (!rootConfig.isDeviceMobile) {
            $scope.leads = dataNew.sort(function (a, b) {
                return new Date(b.Key14) -
                    new Date(a.Key14);
            });
        } else {
        	customSortForListing(dataNew,function(data){
        		$scope.leads=data;
            });
        }
        LmsVariables.setRefLead($scope.leads);
        $scope.filterLeads();
        $scope.refresh();
        $rootScope.showHideLoadingImage(false);
    };
    
    $scope.showErrorPopup = function (id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
    }

    //PreliminaryDataController Load Event and Load Event Handler
    $scope.$parent.$on('step3Payment', function (event, args) {
        $scope.selectedTabId = "step2Payment";
        $scope.Initialize();
        $rootScope.validationBeforesave = function (value, successcallback) {
            successcallback();
        };
    });

    $rootScope.validationBeforesave = function (value, successcallback) {
        successcallback();
    };

    //calculating age for Additional Insured
    function calculateAge(dob) {
        if (dob !== "" && dob != undefined && dob !== null && dob !== "Invalid Date") {
            var dobDate = new Date(dob);
            if ((dobDate === "NaN" || dobDate === "Invalid Date")) {
                dobDate = new Date(dob.split("-").join("/"));
            }
            var todayDate = new Date();
            var yyyy = todayDate.getFullYear().toString();
            var mm = (todayDate.getMonth() + 1).toString();
            var dd = todayDate.getDate().toString();
            var formatedDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
            if (dobDate > todayDate) {
                return;
            } else {
                var age = todayDate.getFullYear() - dobDate.getFullYear();
                if (todayDate.getMonth() < dobDate.getMonth() - 1 || (dobDate.getMonth() - 1 === todayDate.getMonth() && todayDate.getMonth() < dobDate.getDate())) {
                    age--;
                }
                if (dob === undefined || dob === "") {
                    age = "-";
                }
                return age;
            }
        }
    };
    
    $scope.convertPaymentMethod=function(value){
        //Converting Payment option code to type
        for (var i = 0; i < $rootScope.PaymentOption.length; i++) {
            if (value === $rootScope.PaymentOption[i].code) {
                value = $rootScope.PaymentOption[i].value;
            }
        }
        
        if(value==='Pay in by Cheque' || value==='Pay in by Cash'){
            return translateMessages($translate, value);
        }else{
            return value;
        }
    }
    
    $scope.convertPaymentBank=function(value){
        //Converting Bank code to type
        for (var i = 0; i < $rootScope.PaymentBankName.length; i++) {
            if (value === $rootScope.PaymentBankName[i].code) {
                value = $rootScope.PaymentBankName[i].value;
            }
        }
        
        return value;
    }
    
    $scope.convertSplitHeader=function(value){
        if(value==='Split One'){
            return translateMessages($translate, "eapp.splitOne"); 
        }else if(value==='Split Two'){
            return translateMessages($translate, "eapp.splitTwo"); 
        }        
    }

} //