/*Name:GLI_EappNavigationController
 * For implementing GLI specific changes
 */
storeApp.controller('GLI_EappNavigationController', GLI_EappNavigationController);
GLI_EappNavigationController.$inject = ['$rootScope', '$scope', 'DataLookupService', 'DataService', 'LookupService', 'DocumentService', 'ProductService', '$compile', '$routeParams', '$location', '$anchorScroll', '$translate', '$debounce', 'UtilityService', 'PersistenceMapping', 'AutoSave', 'globalService', 'EappVariables', 'EappService', 'GLI_EappService', 'GLI_EappVariables', 'GLI_DataService', 'AgentService', 'UserDetailsService', '$controller', 'GLI_EvaluateService', '$timeout','GLI_DataLookupService'];

function GLI_EappNavigationController($rootScope, $scope, DataLookupService, DataService, LookupService, DocumentService, ProductService, $compile, $routeParams, $location, $anchorScroll, $translate, $debounce, UtilityService, PersistenceMapping, AutoSave, globalService, EappVariables, EappService, GLI_EappService, GLI_EappVariables, GLI_DataService, AgentService, UserDetailsService, $controller, GLI_EvaluateService, $timeout,GLI_DataLookupService) {
    $controller('EappNavigationController', {
        $rootScope: $rootScope,
        $scope: $scope,
        DataService: DataService,
        LookupService: LookupService,
        DocumentService: DocumentService,
        ProductService: ProductService,
        $compile: $compile,
        $routeParams: $routeParams,
        $location: $location,
        $translate: $translate,
        $debounce: $debounce,
        UtilityService: UtilityService,
        PersistenceMapping: PersistenceMapping,
        AutoSave: AutoSave,
        globalService: globalService,
        EappVariables: EappVariables,
        EappService: EappService,
        DataService: GLI_DataService,
        GLI_EappService: GLI_EappService,
		DataLookupService:GLI_DataLookupService,
        GLI_EappVariables: GLI_EappVariables
    });
    $rootScope.enableRefresh=false;
    $rootScope.hideLanguageSetting = false;
    $scope.eAppParentobj = {};
    $scope.eAppParentobj.errorCount = 0;
    $scope.eAppParentobj.nextPage = "";
    $scope.eAppParentobj.CurrentPage = "";
    $scope.eAppParentobj.PreviousPage = "";
    $scope.eAppQuestionnaire = rootConfig.eAppQuestionnaire;
    $scope.eAppProposerQuestionnaire = rootConfig.eAppProposerQuestionnaire;
    $scope.mainInsuredMedcategory = 'NM';
    $scope.addInsured1MedCategory = 'NM';
    $scope.addInsured2MedCategory = 'NM';
    $scope.addInsured3MedCategory = 'NM';
    $scope.addInsured4MedCategory = 'NM';
    $scope.totalDocCount = 0;
    $scope.saveFromSignatureConfirm = "false";
    $scope.disableProceedButtonCommom = false;
    $scope.docCountFlagForSubmit = rootConfig.GVLNonSubmitFlag;
    $scope.displayARMSSection = false;
    $scope.modifiedRidersList = [];
    $scope.isSectionVisible = false;
    //$scope.popupTabs = [ '', '','',''];
    //$scope.carouselTabs = ['','','','','','','','','',''];
    //$scope.popupTabs[0] = 'active'; 
    // $scope.carouselTabs[0] = 'active';
    $scope.applicationNoDisabled = false;
    $scope.questionnaireTimeoutInclusion = false;
    $rootScope.etrGenerationSelected = false;

    $scope.translate = function(riderType) {
        return $translate.instant("eapp." + riderType);
    }
    var currentTab;
    $rootScope.moduleHeader = translateMessages($translate, "eapp_vt.eAppHeader");
    $rootScope.moduleVariable = translateMessages($translate, $rootScope.moduleHeader);
    $scope.totalFundOption = 100;
    $rootScope.docview = false; // TO DO
    $rootScope.tabview = false; // TO DO
    $rootScope.enableSignaturPad = false;
    $scope.isRDSUser = UserDetailsService.getRDSUser();
    $scope.namePattern = rootConfig.namePattern;
    $scope.Initialize = function() {
        $scope.isPainted = false;
        $scope.eAppTabNavStyle = [];
        $scope.innertabs = [];
        $scope.innertabs[0] = 'active';
        $scope.innertabsVertical = [];
        $scope.innertabsVertical[0] = 'selected';
        $scope.showProposal = false;
        $scope.isPopupDisplayed = false;
        $scope.isAutoSave = false;
        $scope.isNotAutoSave = false;
        $scope.eAppUIJson = rootConfig.eAppJson;
        GLI_EappVariables.clearEappVariables();
        EappVariables.EappKeys.Key5 = $routeParams.productId;
        EappVariables.EappKeys.Key4 = $routeParams.proposalId == 0 ? "" : $routeParams.proposalId;
        EappVariables.EappKeys.Id = $routeParams.transactionId;
        EappVariables.EappKeys.Key3 = $routeParams.illustrationId == 0 ? "" : $routeParams.illustrationId;
        EappVariables.EappKeys.Key1 = $routeParams.lmsId == 0 ? "" : $routeParams.lmsId;
        EappVariables.EappKeys.Key2 = $routeParams.fnaId == 0 ? "" : $routeParams.fnaId;
        EappVariables.EappKeys.Key24 = $routeParams.illustrationNum == 0 ? "" : $routeParams.illustrationNum;
        $rootScope.transactionId = $routeParams.transactionId;
        $('#ApplicationDate').attr('disabled', true);
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
    };

    var agentDetail = UserDetailsService.getUserDetailsModel();
    $scope.setButtonDisabled = "true";
    if (agentDetail.isEligble && agentDetail.isEligble == "False") {
        $scope.userStatus = "false";
    } else {
        $scope.userStatus = "true";
    }
    $scope.initialLoadContinue = function() {
        LEDynamicUI.getUIJson(rootConfig.eAppConfigJson, false, function(eAppConfigJson) {
            GLI_EappVariables.eAppInputTemplate = eAppConfigJson.eAppInputTemplate;
            GLI_EappVariables.additionalinsuredQuestionnaire = eAppConfigJson.questionnaire;
            GLI_EappVariables.previouspolicyHistory = eAppConfigJson.PreviouspolicyHistory;
            GLI_EappVariables.familyHistory = eAppConfigJson.questionnaire.FamilyHealthHistory;

            GLI_EappVariables.InsuredQuestionnaire = eAppConfigJson.InsuredQuestionnaire;
            GLI_EappVariables.PayerQuestionnaire = eAppConfigJson.PayerQuestionnaire;
            GLI_EappVariables.ProposerQuestionnaire = eAppConfigJson.ProposerQuestionnaire;

            if ((rootConfig.isDeviceMobile)) {
                if ($routeParams.proposalId > 0 || $routeParams.transactionId > 0 || EappVariables.EappKeys &&
                    ((EappVariables.EappKeys.Key4 && EappVariables.EappKeys.Key4 != "") || (EappVariables.EappKeys.Id && EappVariables.EappKeys.Id != "0"))) {
                    getEapp();
                } else {
                    GLIprepopulateData();
                }
            } else {
                /* FNA Changes by LE Team Bug 4216 - starts */
                if (EappVariables.fromFNA && EappVariables.EappKeys != undefined) {
                    EappVariables.EappKeys.Key4 = "";
                }
                
                if (EappVariables.fromLead  && EappVariables.EappKeys != undefined) {
                    EappVariables.EappKeys.Key4 = "";
                    EappVariables.fromLead=false;
                }
                if ($routeParams.proposalId != 0 || $routeParams.transactionId > 0 || EappVariables.EappKeys &&
                    ((EappVariables.EappKeys.Key4 && EappVariables.EappKeys.Key4 != "") || (EappVariables.EappKeys.Id && EappVariables.EappKeys.Id != "0"))) {
                    /* FNA Changes by LE Team Bug 4216 - End */
                    getEapp();
                } else {
                    GLIprepopulateData();
                }
            }
        });
    }

    function GLIprepopulateData() {

        $scope.Initialize();

        if (EappVariables.getEappModel().Insured.Questionnaire == undefined || EappVariables.getEappModel().Insured.Questionnaire.LifestyleDetails == undefined || EappVariables.getEappModel().Insured.Questionnaire.LifestyleDetails.Questions.length < 43) {
            EappVariables.getEappModel().Insured.Questionnaire = GLI_EappVariables.InsuredQuestionnaire;
            EappVariables.getEappModel().Payer.Questionnaire = GLI_EappVariables.PayerQuestionnaire;
            EappVariables.getEappModel().Proposer.Questionnaire = GLI_EappVariables.ProposerQuestionnaire;
        }
        $scope.LifeEngageProduct = EappVariables.getEappModel();
        //if($scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer==='Yes'){
        //$scope.innertabs[1] = '';
        //}

        EappService.GLIprepopulateData(function() {
            if ($scope.isRDSUser && EappVariables.EappModel.Product && EappVariables.EappModel.Product.policyDetails) {
                EappVariables.EappKeys.Key21 = EappVariables.EappModel.Product.policyDetails.policyNumber;
            }
            if (EappVariables.EappModel.Product.templates !== undefined) {
                if (EappVariables.EappModel.Product.templates.eAppInputTemplate === undefined || EappVariables.EappModel.Product.templates.eAppInputTemplate === "") {
                    EappVariables.EappModel.Product.templates.eAppInputTemplate = GLI_EappVariables.eAppInputTemplate;
                }
            }
            /* FNA changes made by LE Team starts */
           if($scope.LifeEngageProduct.Insured.CurrentAddress && $scope.LifeEngageProduct.Insured.classNameAttached!=undefined && ($scope.LifeEngageProduct.Insured.ContactDetails.isPermanentAddressSameAsCurrentAddress==undefined || $scope.LifeEngageProduct.Insured.ContactDetails.isPermanentAddressSameAsCurrentAddress==='No')){
                 $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress=$scope.LifeEngageProduct.Insured.CurrentAddress;
            }            
            /* FNA changes made by LE Team ends */
            $scope.isFinalSubmit = $scope.LifeEngageProduct.Declaration.isFinalSubmit;
            $scope.paintInitialView();
        }); // For Pre-populating Data in to the eApp Model

    }

    function getEapp() {
        $scope.Initialize();
        EappService.getEapp(function() {
            if (EappVariables.getEappModel().Insured.Questionnaire == undefined || EappVariables.getEappModel().Insured.Questionnaire.LifestyleDetails == undefined || EappVariables.getEappModel().Insured.Questionnaire.LifestyleDetails.Questions.length < 43) {
                EappVariables.getEappModel().Insured.Questionnaire = GLI_EappVariables.InsuredQuestionnaire;
                EappVariables.getEappModel().Payer.Questionnaire = GLI_EappVariables.PayerQuestionnaire;
                EappVariables.getEappModel().Proposer.Questionnaire = GLI_EappVariables.ProposerQuestionnaire;
            }
            $scope.LifeEngageProduct = EappVariables.getEappModel();
            $scope.LifeEngageProduct.ApplicationNo = EappVariables.EappKeys.Key21;
            //                        $scope.LifeEngageProduct.traineeAgentCode = EappVariables.EappModel.traineeAgentCode;

            /* FNA changes made by LE Team starts */
            if($scope.LifeEngageProduct.Insured.CurrentAddress && $scope.LifeEngageProduct.Insured.classNameAttached!=undefined && ($scope.LifeEngageProduct.Insured.ContactDetails.isPermanentAddressSameAsCurrentAddress==undefined || $scope.LifeEngageProduct.Insured.ContactDetails.isPermanentAddressSameAsCurrentAddress==='No')){
                 $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress=$scope.LifeEngageProduct.Insured.CurrentAddress;
            }            
            /* FNA changes made by LE Team ends */

            if (EappVariables.EappKeys.Key15 != 'Pending Submission') {
                PersistenceMapping.Key22 = EappVariables.eAppProceedingDate;
            }
            if ($scope.LifeEngageProduct.traineeAgentCode !== undefined && $scope.LifeEngageProduct.traineeAgentCode !== '') {
                $scope.LifeEngageProduct.Insured.BasicDetails.TrineeAgent = 'Yes';
            }
            if (EappVariables.EappKeys.Key34 == 'DataEntryCompleted' || EappVariables.EappKeys.Key15 == 'Submitted') {
                $scope.disableapplicatioNo = true;
            } else {
                $scope.disableapplicatioNo = false;
            }
            $scope.mapIdForOccupationInsured();
            $scope.mapIdForOccupationPayer();
            $scope.isFinalSubmit = $scope.LifeEngageProduct.Declaration.isFinalSubmit;
            if ($scope.LifeEngageProduct.previouspolicyHistory && $scope.LifeEngageProduct.previouspolicyHistory.length > 0) {
                GLI_EappVariables.previouspolicyHistory = $scope.LifeEngageProduct.previouspolicyHistory;
            }
            if ($scope.LifeEngageProduct.familyHistory && $scope.LifeEngageProduct.familyHistory.length > 0) {
                GLI_EappVariables.familyHistory = $scope.LifeEngageProduct.familyHistory;
            }
            if ($scope.LifeEngageProduct.Insured.Questionnaire && $scope.LifeEngageProduct.Insured.Questionnaire.LifestyleDetails && $scope.LifeEngageProduct.Insured.Questionnaire.LifestyleDetails.Questions.length < 43) {
                GLI_EappVariables.getEappModel().Insured.Questionnaire = $scope.LifeEngageProduct.Insured.Questionnaire;
            }
            if ($scope.LifeEngageProduct.Payer.Questionnaire && $scope.LifeEngageProduct.Payer.Questionnaire.LifestyleDetails && $scope.LifeEngageProduct.Payer.Questionnaire.LifestyleDetails.Questions.length < 43) {
                GLI_EappVariables.getEappModel().Payer.Questionnaire = $scope.LifeEngageProduct.Payer.Questionnaire;
            }
            if ($scope.LifeEngageProduct.Proposer.Questionnaire && $scope.LifeEngageProduct.Proposer.Questionnaire.Other && $scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions.length != 8) {
                GLI_EappVariables.getEappModel().Proposer.Questionnaire = $scope.LifeEngageProduct.Proposer.Questionnaires;
            }

            if (EappVariables.EappKeys.Key34 == 'DataEntryCompleted') {
                $scope.LifeEngageProduct.LastVisitedUrl = "5,0,'Payment',false,'tabsinner16',''";
            }
            // ----------------------Performance Customization starts--------------
            if ((rootConfig.isDeviceMobile)) {
                var LastVisitedURL = $scope.LifeEngageProduct.LastVisitedUrl;
                if (LastVisitedURL && LastVisitedURL != "") {
                    var nextPage = LastVisitedURL.split(",");
                    var index = parseInt(nextPage[0]);
                    var subindex = parseInt(nextPage[1]);
                    var tabid = nextPage[2].split("'").join('');
                    var isInnerTab = $scope.stringToBool(nextPage[3]);
                    var selectedTab = nextPage[4].split("'").join('');
                    var json = nextPage[5].split("'").join('');
                }
                EappVariables.QuestionModel = "";
                if (tabid == "MainInsuredQuestionnaire") {
                    EappVariables.QuestionModel = angular.copy($scope.LifeEngageProduct.Insured.Questionnaire);
                } else if (tabid == "PolicyHolderQuestionnaire") {
                    EappVariables.QuestionModel = angular.copy($scope.LifeEngageProduct.Payer.Questionnaire);
                } else if (tabid == "eAppFatcaQuestionnaireJson") {
                    EappVariables.QuestionModel = angular.copy($scope.LifeEngageProduct.Proposer.Questionnaire);
                }
                // In summary need not delete the scope data in questionnaire
                if (index != undefined && index != "4" && index != 4 && index != "5" && index != 5 && index != "6" && index != 6) {
                    /*if ($scope.LifeEngageProduct.Insured.Questionnaire) {
                    	$scope.additionalQuestions = angular.copy($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.Questions);
                    	delete $scope.LifeEngageProduct.Insured["Questionnaire"];
                    	if ((index == 1 || index == "1") && (subindex == 2 || subindex == "2")) {
                    		$scope.LifeEngageProduct.Insured.Questionnaire = {};
                    		$scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare = {};
                    		$scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.Questions = {};
                    		$scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.Questions = angular.copy($scope.additionalQuestions);
                    		delete $scope.additionalQuestions;
                    	}
                    }*/
                    EappVariables.setEappModel($scope.LifeEngageProduct);
                    $scope.paintInitialView();
                } else {
                    EappVariables.QuestionModel = "";
                    $scope.paintInitialView();
                }
            } else {
                $scope.paintInitialView();
            }
            // -----------------------Performance Customization ends--------------
            // $scope.paintInitialView();
        }, '');


    }
    $scope.initialLoad = function() {
        $rootScope.showHideLoadingImage(false);
        $rootScope.selectedPage = "/MyAccount/eApp/0/0/EappVariables.EappKeys.Key3";
        $scope.clearPrecompileLocalStorage();
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
		 var typeNameList = [
			{ "type": "STATE", "isCacheable": true },
			{ "type": "DISTRICT", "isCacheable": true },
			{ "type": "DOCUMENT_IDENTITY", "isCacheable": true },
			{ "type": "DOCUMENT_IDENTITY_PAYER", "isCacheable": true },
			{ "type": "PAYMENTRECORDS", "isCacheable": true },
			{ "type": "FATCA", "isCacheable": true },
			{ "type": "HEALTHRECORD", "isCacheable": true },
			{ "type": "PREVIOUSPOLICY", "isCacheable": true },
			{ "type": "CONSENTLETTER", "isCacheable": true },
			{ "type": "FINANCIALRECORDS", "isCacheable": true },
			{ "type": "FORIGNERQUESTIONNAIRE", "isCacheable": true },
			{ "type": "OTHERS", "isCacheable": true },
			{ "type": "PHYSICALAPPFORMS", "isCacheable": true },
			{ "type": "COMPANYNAME", "isCacheable": true },
			{ "type": "INSURANCECOMPANY", "isCacheable": true },
			{ "type": "PROVINCECODE", "isCacheable": true },
			{ "type": "ISSUEBANK", "isCacheable": true },
			{ "type": "ISSUEBRANCH", "isCacheable": true },
			{ "type": "ISSUEBRANCHCODE", "isCacheable": true },
			{ "type": "RECEIVEBRANCH", "isCacheable": true },
			{ "type": "RECEIVEBRANCHCODE", "isCacheable": true },
			{ "type": "PAYINCASHBRANCHCODE", "isCacheable": true },
			{ "type": "PBOCC_CLASS", "isCacheable": true },
			{ "type": "PAYMENTBANKNAME", "isCacheable": true },
			{ "type": "PAYMENTOPTION", "isCacheable": true },
			{ "type": "NATUREOFWORK", "isCacheable": true },
			{ "type": "IDENTITYTYPE", "isCacheable": true },
			{ "type": "BENEFICIARYIDENTITY", "isCacheable": true },
			{ "type": "CONVENIENTCONTACT", "isCacheable": true },
			{ "type": "MARITALSTATUS", "isCacheable": true },
			{ "type": "BANKNAME", "isCacheable": true },
			{ "type": "NATIONALITY", "isCacheable": true },
			{ "type": "WARD", "isCacheable": true },
			{ "type": "IDENTITY", "isCacheable": true },
			{ "type": "TITLE", "isCacheable": true },
            { "type": "TITLE_GENDER", "isCacheable": true },
			{ "type": "RELATIONSHIP", "isCacheable": true },
			{ "type": "DOCUMENT_TYPE_TILE", "isCacheable": true },
			{ "type": "BENEFICIARYRELATIONSIP", "isCacheable": true },
			{ "type": "GAO", "isCacheable": true }];
			

		GLI_DataLookupService.getMultipleLookUpData(typeNameList, function (lookUpList) {
            $scope.populateIndependentLookupDateInScope('STATE', 'state', '', false, 'state', currentTab, function() {
                $scope.populateDistrictLookUp();
            }, function() {},true); 
        }, function () {
            console.log("Error in multilookup data call");  
        });
}

    $scope.stringToBool = function(val) {
        return (val + '').toLowerCase() === 'true';
    }
	$scope.backupload = function () {
		$scope.showBackBtn = false;
        $rootScope.docview = true;
        $rootScope.tabview = false;
		$scope.paintView(6,0,'DocumentUpload',true,'DocumentUpload','');
	}
    $scope.backToDocumentUpload = function() {
        $scope.showBackBtn = false;
        $rootScope.docview = true;
        $rootScope.tabview = false;
		
		if(EappVariables.EappKeys.Key34=='' || EappVariables.EappKeys.Key34==undefined || EappVariables.EappKeys.Key34==null){
//			$scope.LifeEngageProduct.LastVisitedUrl = "1,1,'mainInsuredSubTab',true,'mainInsuredSubTab',''";
			$scope.LifeEngageProduct.LastVisitedUrl = $rootScope.lastURL;
		}else if(EappVariables.EappKeys.Key34==='DataEntryCompleted'){
			$scope.LifeEngageProduct.LastVisitedUrl = "5,0,'Payment',false,'tabsinner16',''";
		}else if(EappVariables.EappKeys.Key34==='PaymentCompleted'){
			$scope.LifeEngageProduct.LastVisitedUrl = "6,0,'DocumentUpload',false,'tabsinner18',''";
		}
		
        $scope.Save();
        //$scope.$emit('DocumentUpload',[ "DocumentUpload" ]);
        $timeout(function() {
            if ($scope.LifeEngageProduct.totalDocCount == 0 && !$rootScope.GAOTileClick && EappVariables.EappKeys.Key15 != "Submitted") {
                EappVariables.EappKeys.Key34 = "DocuploadCompleted";
                $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
                $scope.Save();
            }
        }, 100)
        $scope.$emit('DocReturn');
    }

    $scope.saveProceed = function() {
        if ($scope.eAppParentobj.errorCount > 0) {
            $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "enterMandatoryIllustration"), translateMessages($translate, "fna.ok"));
        } else {
            $scope.isNotAutoSave = "true";
            //$scope.Save();
            $scope.onSaveSuccess(); //Fix for bug document upload and payment getting disabled for submitted eapp
        }
    }

    $scope.validateAgentCode = function() {
        var transObj = PersistenceMapping.mapScopeToPersistence({});
        transObj.Key1 = $scope.LifeEngageProduct.traineeAgentCode;
        transObj.Type = "AgentCodeValidation";
        GLI_DataService.AgentCodeValidation(transObj, $scope.getTransationSuccess, $scope.getTransationError);
    };

    $scope.validateApplicationNo = function() {
        var transObj = PersistenceMapping.mapScopeToPersistence({});
        transObj.Key4 = EappVariables.EappKeys.Key4;
        transObj.Key21 = ($scope.LifeEngageProduct.ApplicationNo === undefined || $scope.LifeEngageProduct.ApplicationNo === "") ? EappVariables.EappModel.ApplicationNo : $scope.LifeEngageProduct.ApplicationNo;
        transObj.Type = "isSpajExisting";
        GLI_DataService.isSpajExisting(transObj, $scope.getapplicationNoSuccess, $scope.getTransationError);
    };

    $scope.getapplicationNoSuccess = function(data) {
        
        $rootScope.showHideLoadingImage(false, "Loading..");
        if (data !== undefined && data.TransactionData !== null) {
            var statusCode = data.Key16;
            if (statusCode === "EXISTS") {
                $rootScope.lePopupCtrl.showError(
                    translateMessages($translate,
                        "lifeEngage"),
                    translateMessages($translate,
                        "eapp_vt.ApplicationAlreadyExists"),
                    translateMessages($translate,
                        "fna.ok"));

                $("#summaryProceedButton").removeAttr("disabled");
                $('#InsuredApplicationNo').removeAttr("disabled");
            } else {
                EappVariables.EappKeys.Key34 = "DataEntryCompleted";
                $scope.disableapplicatioNo = true;
                if ($scope.eAppParentobj.nextPage.indexOf('Payment') >= 0) {
                    $rootScope.showHideLoadingImage(true, 'calculationProgress', $translate);
                    $scope.Save();
                } else {
                    $scope.Save();
                }
            }
        }
    }
    $scope.getTransationSuccess = function(data) {
        $scope.errorMessageService = [];
        $rootScope.showHideLoadingImage(false, "Loading..");
        if (data !== undefined && data.TransactionData !== null) {

            var statusCode = data.TransactionData.StatusData.StatusCode;
            var errorMsg = data.TransactionData.StatusData.StatusMessage;

            if (statusCode === "100") {
                $scope.agentStatus = false;
            } else if (statusCode !== "100") {
                $scope.agentStatus = true;
                $scope.lePopupCtrl.showWarning(
                    translateMessages($translate,
                        "lifeEngage"),
                    translateMessages($translate,
                        "eapp_vt.invalidAgenCode"),
                    translateMessages($translate,
                        "fna.ok"));
                return;
            }
        }

        $scope.saveProposal(false, 'mainInsuredSubTab');
    }

    $scope.getTransationError = function(data) {
        $rootScope.showHideLoadingImage(false, "Loading..");
        $rootScope.serviceFailed = true;
        if (rootConfig.isDeviceMobile && !checkConnection()) {
            $scope.message = translateMessages($translate, "networkValidationErrorMessage");
        } else {
            $scope.message = translateMessages($translate, "regServerError");
        }
        $scope.$emit('tokenEvent', {
            message: $scope.message
        });
        if (data == "Error in ajax callE") {
            $scope.onServerError = true;
            $scope.serverErrorMessage = translateMessages($translate, "serverErrorMessage");
            //showHideLoadingImage(false);
        } else {
            //showHideLoadingImage(false);

        }
        //                   	$rootScope.$apply();
    };


    $scope.mapIdForOccupation = function() {
        //Occupation Nature of work
        //            if($scope.LifeEngageProduct.Insured.OccupationDetails){
        //                if($scope.LifeEngageProduct.Insured.OccupationDetails.length===1){                    
        //                    $scope.LifeEngageProduct.Insured.OccupationDetails[0].id=0;
        //                }
        //                if($scope.LifeEngageProduct.Insured.OccupationDetails.length===2){                    
        //                    $scope.LifeEngageProduct.Insured.OccupationDetails[0].id=0;
        //                    $scope.LifeEngageProduct.Insured.OccupationDetails[1].id=1;
        //                }        
        //            }

        if ($scope.LifeEngageProduct.Payer.OccupationDetails) {
            if ($scope.LifeEngageProduct.Payer.OccupationDetails.length === 1) {
                $scope.LifeEngageProduct.Payer.OccupationDetails[0].id = 0;
            }
            if ($scope.LifeEngageProduct.Payer.OccupationDetails.length === 2) {
                $scope.LifeEngageProduct.Payer.OccupationDetails[0].id = 0;
                $scope.LifeEngageProduct.Payer.OccupationDetails[1].id = 1;
            }
        }
    }

    $scope.mapIdForOccupationInsured = function() {
        //Occupation Nature of work
        var tempInsuredObject = angular.copy(EappVariables.EappModel.Insured.OccupationDetails);
        if (tempInsuredObject.length > 1) {
            for (var i = 0; i < tempInsuredObject.length; i++) {
                if (parseInt(tempInsuredObject[i].id) === 0 || parseInt(tempInsuredObject[i].id) === 2) {
                    $scope.LifeEngageProduct.Insured.OccupationDetails[0] = tempInsuredObject[i];
                }

                if (parseInt(tempInsuredObject[i].id) === 1 || parseInt(tempInsuredObject[i].id) === 3) {
                    $scope.LifeEngageProduct.Insured.OccupationDetails[1] = tempInsuredObject[i];
                }
            }
        }
    }

    $scope.mapIdForOccupationPayer = function() {
        //Occupation Nature of work
        var tempPayerObject = angular.copy(EappVariables.EappModel.Payer.OccupationDetails);
        if (tempPayerObject.length > 1) {
            for (var i = 0; i < tempPayerObject.length; i++) {
                if (parseInt(tempPayerObject[i].id) === 0 || parseInt(tempPayerObject[i].id) === 2) {
                    $scope.LifeEngageProduct.Payer.OccupationDetails[0] = tempPayerObject[i];
                }

                if (parseInt(tempPayerObject[i].id) === 1 || parseInt(tempPayerObject[i].id) === 3) {
                    $scope.LifeEngageProduct.Payer.OccupationDetails[1] = tempPayerObject[i];
                }
            }
        }
    }

    $scope.getPayerOccupation = function() {
        var OccupationPayer = [];
        if (!angular.equals(undefined, OccupationPayer)) {
            OccupationPayer.slice();
        }
        for (var i = 0; i < 2; i++) {
            if (!angular.equals(undefined, $scope.LifeEngageProduct.Payer.OccupationDetails[i])) {
                if (!angular.equals(undefined, $scope.LifeEngageProduct.Payer.OccupationDetails[i].natureofWork) && $scope.LifeEngageProduct.Payer.OccupationDetails[i].natureofWork !== "") {
                    var OccupationDetailsArr = {};
                    OccupationDetailsArr = {
                        "id": i,
                        "natureofWork": $scope.LifeEngageProduct.Payer.OccupationDetails[i].natureofWork,
                        "annualIncome": $scope.LifeEngageProduct.Payer.OccupationDetails[i].annualIncome,
                        "companyName": ""
                    };
                    //                        OccupationDetailsArr={
                    //                            "id":i,
                    //							"natureofWork":$scope.LifeEngageProduct.Payer.OccupationDetails[i].natureofWork,
                    //                            "annualIncome":$scope.LifeEngageProduct.Payer.OccupationDetails[i].annualIncome,
                    //                            "companyName":$scope.LifeEngageProduct.Payer.OccupationDetails[i].companyName
                    //					   };
                    OccupationPayer.push(OccupationDetailsArr);
                }
            }
        }
        $scope.LifeEngageProduct.Payer.OccupationDetails = [];
        for (var i = 0; i < OccupationPayer.length; i++) {
            if (OccupationPayer[i].id !== undefined) {
                $scope.LifeEngageProduct.Payer.OccupationDetails[i] = angular.copy(OccupationPayer[i]);
            }
        }
    }

    $scope.savePayment = function(index, subindex, id, isInnerTab, selectedTab, json) {
        $scope.saveProposal();
        $scope.initailPaintView(index, subindex, id, isInnerTab, selectedTab, json);
    }

    $scope.onChangeTraineeAgent = function(value) {
        $scope.LifeEngageProduct.Insured.BasicDetails.TrineeAgent = value;
        $scope.LifeEngageProduct.traineeAgentCode = "";
        //            EappVariables.EappKeys.Key1 = ""; 

        $scope.refresh();
    }

    $scope.onUpdateTraineeAgent = function(value) {
        $scope.LifeEngageProduct.traineeAgentCode = value;
        $scope.agentStatus = true;
    }
    //$scope.agentStatus = true;
    $scope.saveProposal = function(isAutoSave, id) {
       $scope.LifeEngageProduct.tsaAllComp = $scope.LifeEngageProduct.Insured.Questionnaire.LifestyleDetails.Questions[3].details;
       $scope.LifeEngageProduct.tsaCIDD = $scope.LifeEngageProduct.Insured.Questionnaire.LifestyleDetails.Questions[4].details; 
        /*Personal Details values are not retained after editing START*/
//        $scope.LifeEngageProduct.Insured=angular.copy(EappVariables.EappModel.Insured);
//        $scope.LifeEngageProduct.Payer=angular.copy(EappVariables.EappModel.Payer);
        $scope.LifeEngageProduct.Insured.BasicDetails=angular.copy(EappVariables.EappModel.Insured.BasicDetails);
        $scope.LifeEngageProduct.Insured.ContactDetails=angular.copy(EappVariables.EappModel.Insured.ContactDetails);
        $scope.LifeEngageProduct.Insured.OccupationDetails=angular.copy(EappVariables.EappModel.Insured.OccupationDetails);
        $scope.LifeEngageProduct.Insured.HealthDetails=angular.copy(EappVariables.EappModel.Insured.HealthDetails);
        $scope.LifeEngageProduct.Insured.CustomerRelationship=angular.copy(EappVariables.EappModel.Insured.CustomerRelationship);
        
        $scope.LifeEngageProduct.Payer.BasicDetails=angular.copy(EappVariables.EappModel.Payer.BasicDetails);
        $scope.LifeEngageProduct.Payer.ContactDetails=angular.copy(EappVariables.EappModel.Payer.ContactDetails);        
        $scope.LifeEngageProduct.Payer.OccupationDetails=angular.copy(EappVariables.EappModel.Payer.OccupationDetails);
        $scope.LifeEngageProduct.Payer.HealthDetails=angular.copy(EappVariables.EappModel.Payer.HealthDetails);
        
        $scope.LifeEngageProduct.Beneficiaries=angular.copy(EappVariables.EappModel.Beneficiaries);
        $scope.LifeEngageProduct.Payment.RenewalPayment=angular.copy(EappVariables.EappModel.Payment.RenewalPayment);
        $scope.LifeEngageProduct.Payment.RefundPayment=angular.copy(EappVariables.EappModel.Payment.RefundPayment);
        /*Personal Details values are not retained after editing END*/
      
        $scope.LifeEngageProduct.Insured.tsaAllComp = $scope.LifeEngageProduct.tsaAllComp;
	$scope.LifeEngageProduct.Insured.tsaCIDD = $scope.LifeEngageProduct.tsaCIDD;
	
        $rootScope.addTickVisiblity = false;
        if ($scope.secId == "additionalInformationPayer") {
            $rootScope.addpayer = true;
        }

        if ($scope.secId == "previousPolicies") {
            $rootScope.previousTickVisiblity = true;
        }

        if ($scope.secId && $scope.secId != 'previousPolicies' && $scope.secId != "additionalInformationPayer" &&
            $rootScope.selectedPage != 'FatcaQuestionnaire') {
            tabid = "MainInsuredQuestionnaire";
            selectedTab = "MainInsuredQuestionnaire";
            subindex = 0;


            if ($scope.secId == 'LifeStyleQuestions') {
                $rootScope.lifeStyleTickVisiblity = true;
            } else if ($scope.secId == 'LifeStyleQuestionsPayer') {
                $rootScope.lifeStylePayerTickVisiblity = true;
            } else if ($scope.secId == 'IllnessTreatmentHistoryPayer') {
                $rootScope.illnessPayerTickVisiblity = true;
            } else if ($scope.secId == 'IllnessTreatmentHistory') {
                $rootScope.illnessTickVisiblity = true;

            } else if ($scope.secId == 'healthDDRider') {
                $rootScope.healthTickVisiblity = true;
            } else if ($scope.secId == 'additionalInformation') {
                $rootScope.addTickVisiblity = true;
                /* if($rootScope.isPreviousPolicyNavigation==true){											
               	      $rootScope.previousTickVisiblity=true;
                       }	*/

            }
        }

        if ($scope.LifeEngageProduct.Insured.Declaration == undefined) {
            $scope.LifeEngageProduct.Insured.Declaration = {};
        }
        if ($scope.LifeEngageProduct.Insured.Declaration && $scope.LifeEngageProduct.Insured.Declaration.dateandTime && $scope.LifeEngageProduct.Insured.Declaration.dateandTime.indexOf('/') == 2) {
            $scope.LifeEngageProduct.Insured.Declaration.dateandTime = ExternalToInternal($scope.LifeEngageProduct.Insured.Declaration.dateandTime);
        }
        //$scope.LifeEngageProduct.Insured.Declaration.dateandTime="1999-10-12";
        //            EappVariables.EappKeys.Key1= ($scope.LifeEngageProduct.traineeAgentCode!=='' && $scope.LifeEngageProduct.traineeAgentCode!==undefined)?$scope.LifeEngageProduct.traineeAgentCode:"";

        if ($scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer === 'Yes') {

            $scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer = "";
            $scope.LifeEngageProduct.Payer = angular.copy($scope.LifeEngageProduct.Insured);
            if ($scope.LifeEngageProduct.Payer.Questionnaire != undefined || $scope.LifeEngageProduct.Payer.Questionnaire.AdditionalQuestioniare != undefined) {
                $scope.LifeEngageProduct.Payer.Questionnaire.AdditionalQuestioniare.ShortQuestions = GLI_EappVariables.PayerQuestionnaire.AdditionalQuestioniare.ShortQuestions;
            }
        }

        $scope.mapIdForOccupation();

        EappVariables.prepopulatedProposerData = angular.copy($scope.LifeEngageProduct.Payer);
//        EappVariables.setEappModel($scope.LifeEngageProduct);
        //$scope.eAppParentobj.errorCount = 0;            
        $scope.clearPrecompileLocalStorage();
        if (id == 'DeclarationAndAuthorizationTab') {
            $scope.updateErrorCount('DeclarationAndAuthorizationTab');
            $("#summaryProceedButton").attr("disabled", "disabled");
        } else if (id == 'Declaration') {
            $("#agentSummaryButton").attr("disabled", "disabled");
        } else if (id == 'mainInsuredSubTab') {


        } else if (id == 'BeneficiarySubTab') {

            if ($rootScope.beneficiarySelected) {
                $scope.disableProceedButtonCommom = true;
            } else {

                $scope.lePopupCtrl.showWarning(
                    translateMessages($translate,
                        "lifeEngage"),
                    translateMessages($translate,
                        "eapp_vt.atleastOneBeneficiary"),
                    translateMessages($translate,
                        "fna.ok"));
                return;

            }

        } else {
            $scope.disableProceedButtonCommom = true;
        }
        $rootScope.showHideLoadingImage(true, 'saveTransactionMessage', $translate);
        $timeout(function() {

            if ($scope.eAppParentobj.CurrentPage == "eappshortquestionnaire") {
                $scope.eAppParentobj.errorCount = 0;
            }

            if ($scope.eAppParentobj.errorCount > 0) {
                $scope.disableProceedButtonCommom = false;
                var isDisabledDeclarationAndAuth = $("#summaryProceedButton").is(':disabled');
                if (isDisabledDeclarationAndAuth) {
                    $("#summaryProceedButton").removeAttr('disabled');
                }
                var isDisabledACR = $("#agentSummaryButton").is(':disabled');
                if (isDisabledACR) {
                    $("#agentSummaryButton").removeAttr('disabled');
                }

                $rootScope.showHideLoadingImage(false);
                if ($scope.eAppParentobj.CurrentPage.indexOf("Questionnaire") > 0) {
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.answerMandatoryQuestions"), translateMessages($translate, "fna.ok"));
                } else {
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "enterMandatoryIllustration"), translateMessages($translate, "fna.ok"));
                }

                $scope.Save();
            } else if ($rootScope.errorFlagTitle && id == 'PolicyHolderSubTab') {
                $scope.disableProceedButtonCommom = false;
                var isDisabledDeclarationAndAuth = $("#summaryProceedButton").is(':disabled');
                if (isDisabledDeclarationAndAuth) {
                    $("#summaryProceedButton").removeAttr('disabled');
                }
                var isDisabledACR = $("#agentSummaryButton").is(':disabled');
                if (isDisabledACR) {
                    $("#agentSummaryButton").removeAttr('disabled');
                }
                $rootScope.showHideLoadingImage(false);

                $rootScope.lePopupCtrl.showError(
                    translateMessages($translate, "lifeEngage"),
                    translateMessages($translate, "illustrator.titleGenderMismatchPayer"),
                    translateMessages($translate, "lms.ok"));
                $rootScope.showHideLoadingImage(false);
                $scope.Save();
            } else if (id == 'mainInsuredSubTab' && $scope.LifeEngageProduct.Insured.BasicDetails.TrineeAgent === 'Yes' && ($scope.agentStatus === undefined || $scope.agentStatus === '' || $scope.agentStatus)) {
                if ($scope.LifeEngageProduct.Insured.BasicDetails.TrineeAgent === 'Yes' && $scope.LifeEngageProduct.traineeAgentCode !== '' && $scope.LifeEngageProduct.traineeAgentCode !== undefined && $scope.eAppParentobj.errorCount === 0) {
                    if ($scope.isUserOnline()) {
                        $scope.validateAgentCode();
                        if (!$scope.agentStatus) {
                            return;
                        }
                    } else {
                        $scope.emailpopup = false;
                        $scope.leadDetShowPopUpMsg = true;
                        $scope.enterMandatory = translateMessages($translate, "eapp_vt.checkOnlineForPayment");
                    }
                } else if ($scope.LifeEngageProduct.Insured.BasicDetails.TrineeAgent === 'Yes' && ($scope.LifeEngageProduct.traineeAgentCode === '' || $scope.LifeEngageProduct.traineeAgentCode === undefined)) {
                    $rootScope.showHideLoadingImage(false);
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "enterMandatoryIllustration"), translateMessages($translate, "fna.ok"));
                }
            } else if (id == 'eAppFatcaQuestionnaireJson' && (UserDetailsService.getAgentRetrieveData().AgentDetails.agentType == rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType == rootConfig.agentTypeBranchAdmin) && ($scope.LifeEngageProduct.ApplicationNo == '0' || $scope.LifeEngageProduct.ApplicationNo == '')) {
                $rootScope.showHideLoadingImage(false);
                $scope.disableProceedButtonCommom = false;
                $rootScope.lePopupCtrl.showError(
                    translateMessages($translate, "lifeEngage"),
                    translateMessages($translate, "eapp.branchAdminApplicationNumberRequiredMessage"),
                    translateMessages($translate, "lms.ok"));
            } else {

                $scope.isAutoSave = isAutoSave;
                $scope.isNotAutoSave = "true";
                if (rootConfig.isDeviceMobile){
                    if (EappVariables.EappModel.Requirements != undefined && EappVariables.EappModel.Requirements.length > 0) {
                        $scope.LifeEngageProduct.Requirements = EappVariables.EappModel.Requirements;
                    }
                }
                
                 EappVariables.setEappModel($scope.LifeEngageProduct);
                
                $rootScope.validationBeforesave("proceed", function() {
                    $scope.editEappQuestions(function() {

                        if (id == 'DeclarationAndAuthorizationTab') {

                            var applicationNumber = $("#InsuredApplicationNo").val();
                            //	var ApplicationDateVal =$('#ApplicationDate').val();

                            //$scope.LifeEngageProduct.Insured.Declaration.dateandTime = ExternalToInternal($scope.LifeEngageProduct.Insured.Declaration.dateandTime);
                     if(navigator.onLine == true) {
                            if (applicationNumber == '' || applicationNumber == '0') {
                                $rootScope.showHideLoadingImage(false, "");
                                $scope.lePopupCtrl.showWarning(
                                    translateMessages($translate,
                                        "lifeEngage"),
                                    translateMessages($translate,
                                        "eapp_vt.summaryApplicationConfirmPopup"),
                                    translateMessages($translate,
                                        "eapp_vt.summaryConfirm"), $scope.SPAJAllocation, translateMessages($translate,
                                        "eapp_vt.summaryCancel"), $scope.summaryCancelBtn)

                            } else {

                                if (EappVariables.EappKeys.Key34 == 'DataEntryCompleted' || EappVariables.EappKeys.Key15 == 'Submitted') {
                                    $('#InsuredApplicationNo').attr('disabled', true);
                                    $scope.LifeEngageProduct.ApplicationNo = EappVariables.EappKeys.Key21;
                                }
                                $scope.LifeEngageProduct.ApplicationNo = applicationNumber;
                                $scope.validateApplicationNo();
                            }
                        }else {
                        	$rootScope.showHideLoadingImage(false);
                        	$scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.checkOnlineForPayment"), translateMessages($translate, "fna.ok"));
                        	$("#summaryProceedButton").removeAttr('disabled');
                        }

                   }else {
                            $scope.Save();
                        }

                    });

                });

            }
        }, 100);

    };

    $scope.tabNavWithError = function(index, subindex, id, isInnerTab, selectedTab, json) {
        if ($scope.innertabs[subindex - 1] === 'activated' || $scope.innertabs[subindex - 1] === 'active') {
            $scope.initailPaintView(index, subindex, id, isInnerTab, selectedTab, json);
        }
    }

    $scope.tabNavForQuestionnaire = function(index, subindex, id, isInnerTab, selectedTab, json) {
        if ($scope.eAppParentobj.errorCount == 0) {
            $scope.questionnaireTimeoutInclusion = true;
            $scope.initailPaintView(index, subindex, id, isInnerTab, selectedTab, json);
        }
    }

    $scope.editEappQuestions = function(successcallback) {
        if ((rootConfig.isDeviceMobile)) {
            $scope.LifeEngageProduct = angular.copy(EappVariables.getEappModel());
            var lastVisitURL = angular.copy($scope.LifeEngageProduct.LastVisitedUrl);
            if (lastVisitURL && lastVisitURL != "") {
                var nextPage = lastVisitURL.split(",");
                var index = parseInt(nextPage[0]);
                var subindex = parseInt(nextPage[1]);
                var tabid = nextPage[2].split("'").join('');
            }
            if (index == "4" || index == 4 || index == "5" || index == 5 || index == "6" || index == 6) {
                successcallback();
            } else {
                if ((EappVariables.EappKeys.Id != 0 && EappVariables.EappKeys.Id != "0" && EappVariables.EappKeys.Id != null) || ($rootScope.transactionId != 0 && $rootScope.transactionId != "0")) {
                    // EappVariables.EappKeys.Id=$rootScope.transactionId;
                    EappService.getEappBeforeSaveEapp(function() {
                        if (!$scope.LifeEngageProduct.Insured.Questionnaire) {
                            $scope.LifeEngageProduct.Insured.Questionnaire = angular.copy(EappVariables.EappModel.Insured.Questionnaire);
                            $scope.EditAdditionalInsured(index, subindex, function() {
                                successcallback();
                            });
                        }
                        if (!$scope.LifeEngageProduct.Payer.Questionnaire) {
                            $scope.LifeEngageProduct.Payer.Questionnaire = angular.copy(EappVariables.EappModel.Payer.Questionnaire);
                            $scope.EditAdditionalInsured(index, subindex, function() {
                                successcallback();
                            });
                        } else {
                            if ((index == 1 || index == "1") && (subindex == 2 || subindex == "2")) {
                                $scope.AdditionalQuestions = angular.copy($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare);
                                $scope.LifeEngageProduct.Insured.Questionnaire = angular.copy(EappVariables.EappModel.Insured.Questionnaire);
                                $scope.LifeEngageProduct.Payer.Questionnaire = angular.copy(EappVariables.EappModel.Payer.Questionnaire);

                                $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare = angular.copy($scope.AdditionalQuestions);
                                //delete $scope.AdditionalQuestions;
                                $scope.EditAdditionalInsured(index, subindex, function() {
                                    successcallback();
                                });
                            } else {
                                $scope.EditAdditionalInsured(index, subindex, function() {
                                    successcallback();
                                });
                            }
                        }
                    }, function() {
                    	
                    });
                } else {
                    successcallback();
                }
            }
        } else {
            successcallback();
        }
    };

    $scope.isUserOnline = function() {
        var userOnline = true;
        if ((rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
            if (navigator.network.connection.type == Connection.NONE) {
                userOnline = false;
            }
        }
        if ((rootConfig.isOfflineDesktop)) {
            if (!navigator.onLine) {
                userOnline = false;
            }
        }
        return userOnline;
    }
    $scope.summaryCancelBtn = function() {
        $("#summaryProceedButton").removeAttr('disabled');
        //$scope.LifeEngageProduct.Insured.Declaration.dateandTime = InternalToExternal($scope.LifeEngageProduct.Insured.Declaration.dateandTime);

    }

    $scope.SPAJAllocation = function(sucess) {

        // SPAJ allocation logic
        if ($scope.eAppParentobj.nextPage == "5,0,'Payment',true,'tabsinner16',''" && (!EappVariables.EappKeys.Key21 || EappVariables.EappKeys.Key21 == '' || EappVariables.EappKeys.Key21 == '0')) {
            if (!$scope.isUserOnline()) {
                // PersistenceMapping.clearTransactionKeys();
                // var transactionObj = PersistenceMapping.mapScopeToPersistence({});
                // GLI_DataService.retrieveSPAJCount(transactionObj.Key11, function(count){
                // 	if(count>0){
                // 		var agentCode = transactionObj.Key11;// TODO: previously it was $scope.agentDetail.agentCode; but,sometimes getting undefind dirty fix changed to KEy11
                // 		GLI_DataService.retrieveSPAJNo(agentCode,function(data){
                // 			if(!EappVariables.EappKeys.Key21 || EappVariables.EappKeys.Key21=='' || EappVariables.EappKeys.Key21=='0'){
                // 				EappVariables.EappKeys.Key21 = data[0].SPAJNo;
                // 				$scope.spajNo = data[0].SPAJNo;
                // 			}
                // 			$scope.isProposalNoAllocated = true;
                // 			$scope.$parent.showProceed=true;
                // 			$scope.proposalNumber = EappVariables.EappKeys.Key21;
                // 			$scope.refresh();
                // 			sucess();
                // 		},$scope.onRetrieveAgentProfileError);
                $("#summaryProceedButton").removeAttr('disabled');
                $("#InsuredApplicationNo").removeAttr('disabled');
                $rootScope.showHideLoadingImage(false, "");
                $rootScope.lePopupCtrl.showError(translateMessages($translate,
                    "lifeEngage"), translateMessages($translate,
                    "eapp_vt.checkOnlineForPayment"), translateMessages($translate,
                    "fna.ok"));
                $("#summaryProceedButton").removeAttr('disabled');

            } else {
                if (EappVariables.EappModel.ApplicationNo == "0" || EappVariables.EappModel.ApplicationNo == '') {

                    PersistenceMapping.clearTransactionKeys();
                    var transactionObj = PersistenceMapping.mapScopeToPersistence({});
                    transactionObj.Key18 = 1;
                    GLI_DataService.retrieveSPAJNumbers(transactionObj, function(data) {
                        if (!EappVariables.EappKeys.Key21 || EappVariables.EappKeys.Key21 == '' || EappVariables.EappKeys.Key21 == '0') {
                            EappVariables.EappKeys.Key21 = data.spajNos[0];
                            EappVariables.EappModel.ApplicationNo = data.spajNos[0];
                            $scope.LifeEngageProduct.ApplicationNo = data.spajNos[0];
                            $('#InsuredApplicationNo').attr('disabled', true);
                            $scope.proposalNumber = EappVariables.EappKeys.Key21;
                            $scope.isProposalNoAllocated = true;
                            $scope.$parent.showProceed = true;
                            EappVariables.EappKeys.Key34 = "DataEntryCompleted";
                            $scope.lePopupCtrl.showWarning(
                                translateMessages($translate,
                                    "lifeEngage"),
                                translateMessages($translate,
                                    "proposalAccept") + EappVariables.EappModel.ApplicationNo,
                                translateMessages($translate,
                                    "fna.ok"));
                        }
                        if ($scope.eAppParentobj.nextPage.indexOf('Payment') >= 0) {
                            $rootScope.showHideLoadingImage(true, 'calculationProgress', $translate);
                        }

                        if ($scope.isUserOnline()) {
                            $scope.Save();
                        } else {
                            $scope.emailpopup = false;
                            $scope.leadDetShowPopUpMsg = true;
                            $scope.enterMandatory = translateMessages($translate, "eapp_vt.checkOnlineForPayment");
                            $("#summaryProceedButton").removeAttr('disabled');                            
                        }
                    }, $scope.onRetrieveAgentProfileError);

                } else if (EappVariables.EappModel.ApplicationNo !== undefined) {
                    if (EappVariables.EappModel.ApplicationNo.length == 7 && EappVariables.EappModel.ApplicationNo.indexOf(0) == '0') {

                        $scope.validateApplicationNo();
                    } else {
                        $("#summaryProceedButton").removeAttr('disabled');
                        $("#InsuredApplicationNo").removeAttr('disabled');
                        $rootScope.showHideLoadingImage(false, "");
                        $rootScope.lePopupCtrl.showError(translateMessages($translate,
                            "lifeEngage"), translateMessages($translate,
                            "eapp_vt.ApplicationNotValid"), translateMessages($translate,
                            "fna.ok"));

                    }

                } else {
                    $("#summaryProceedButton").removeAttr('disabled');
                    $("#InsuredApplicationNo").removeAttr('disabled');
                    $rootScope.showHideLoadingImage(false, "");
                    $rootScope.lePopupCtrl.showError(translateMessages($translate,
                        "lifeEngage"), translateMessages($translate,
                        "eapp_vt.ApplicationNotValid"), translateMessages($translate,
                        "fna.ok"));

                }


            }
            // },$scope.onRetrieveAgentProfileError);
        } else {

            if (EappVariables.EappKeys.Key34 == 'DataEntryCompleted' || EappVariables.EappKeys.Key15 == 'Submitted') {
                $('#InsuredApplicationNo').attr('disabled', true);
                $scope.LifeEngageProduct.ApplicationNo = EappVariables.EappKeys.Key21;
            }

            if ($scope.eAppParentobj.nextPage.indexOf('Payment') >= 0) {
                $rootScope.showHideLoadingImage(true, 'calculationProgress', $translate);
            }

            if ($scope.isUserOnline()) {
                $scope.Save();
            } else {
                $scope.emailpopup = false;
                $scope.leadDetShowPopUpMsg = true;
                $scope.enterMandatory = translateMessages($translate, "eapp_vt.checkOnlineForPayment");
                $("#summaryProceedButton").removeAttr('disabled');
            }
        }
    };

    //$rootScope.enableSignature = false;
    $scope.downloadApplicationPDF = function() {
        if(EappVariables.EappKeys.Key34 != undefined && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null || EappVariables.EappKeys.Key15 == 'Submitted'){
        	//$rootScope.showHideLoadingImage(false);  
        } else {
            $rootScope.enableSignaturPad = true;
	        $timeout(function() {
	        	//$rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
	        	//$scope.$apply();
	            $('#signaturePadMainInsured canvas').removeAttr('disabled');
	            $('#signaturePadPolicyHolder canvas').removeAttr('disabled');
	            $('#signaturePadAgent canvas').removeAttr('disabled');
	            $('#signaturePadWitness canvas').removeAttr('disabled');
	            $('#DeclarationAndAuthorizationTab button#clearBtnMainInsured').removeAttr('disabled');
	            $('#DeclarationAndAuthorizationTab button#clearBtnPolicyHolder').removeAttr('disabled');
	            $('#DeclarationAndAuthorizationTab button#clearBtnAgent').removeAttr('disabled');
	            $('#DeclarationAndAuthorizationTab button#clearBtnWitness').removeAttr('disabled');
	            $('#DeclarationAndAuthorizationTab button#summaryProceedButton').removeAttr('disabled');
	            $('.signature-pad canvas').removeClass("pointer-None");
	        }, 50)
        }

        //$scope.LifeEngageProduct.Insured.Declaration.dateandTime="1999-10-12";
        //$scope.LifeEngageProduct.Payer.Declaration.dateandTime="1999-10-12";
        if ($scope.LifeEngageProduct.Insured.Declaration.dateandTime && $scope.LifeEngageProduct.Insured.Declaration.dateandTime.indexOf('/') == 2) {
            $scope.LifeEngageProduct.Insured.Declaration.dateandTime = ExternalToInternal($scope.LifeEngageProduct.Insured.Declaration.dateandTime);
        }

        if ($scope.LifeEngageProduct.Payer.Declaration !== undefined) {
            if ($scope.LifeEngageProduct.Payer.Declaration.dateandTime !== undefined) {
                if ($scope.LifeEngageProduct.Payer.Declaration.dateandTime.indexOf('undefined') > -1 || $scope.LifeEngageProduct.Payer.Declaration.dateandTime.indexOf('NaN') > -1) {
                    $scope.LifeEngageProduct.Payer.Declaration.dateandTime = "";
                }
            }
        }

        if ($scope.LifeEngageProduct.Insured.Declaration.dateandTime.indexOf('undefined') > -1 || $scope.LifeEngageProduct.Insured.Declaration.dateandTime.indexOf('NaN') > -1) {
            $scope.LifeEngageProduct.Insured.Declaration.dateandTime = "";
        }

        EappVariables.setEappModel($scope.LifeEngageProduct);

        var eAppTemplateId;
        if (EappVariables.EappModel.Product.templates && EappVariables.EappModel.Product.templates.eAppPdfTemplate) {
            eAppTemplateId = EappVariables.EappModel.Product.templates.eAppPdfTemplate;
        }

        if (eAppTemplateId === undefined || eAppTemplateId === "") {
            eAppTemplateId = 0;
        }
        PersistenceMapping.clearTransactionKeys();
        EappService.mapKeysforPersistence($scope, EappVariables);
        PersistenceMapping.dataIdentifyFlag = false;
        PersistenceMapping.Type = "eApp";
        var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.LifeEngageProduct);
        //        $scope.Save();
        $rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
        if ((rootConfig.isDeviceMobile)) {
            if (checkConnection()) {
                $rootScope.enableSignature = true;
                var transactionsToSync = [];
                transactionsToSync.push({
                    "Key": "eApp",
                    "Value": transactionObj.Id,
                    "syncType": "eApp"
                });
                EappService.customIndividualSyncForPdf($scope, EappVariables, transactionsToSync, function() {
                    GLI_DataService.getListings(transactionObj, function(BIData) {
                        transactionObj.Key3 = BIData[0].Key3;
                        transactionObj.Key4 = BIData[0].Key4;
                        transactionObj.TransactionData.Product.templates.selectedLanguage = "th";
                        DataService.downloadPDF(transactionObj, eAppTemplateId, function(data) {

                            $rootScope.showHideLoadingImage(false);
                            $rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "illustrator.pdfDownloadTransactionsSuccess"), translateMessages($translate, "illustrator.popUpClose"));
                            $scope.refresh();

                        }, function(error) {
                            $rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
                            $rootScope.showHideLoadingImage(false);
                            $scope.refresh();
                        });
                        $rootScope.showHideLoadingImage(false);
                        $rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "illustrator.pdfDownloadTransactionsSuccess"), translateMessages($translate, "illustrator.popUpClose"));
                        $scope.refresh();
                    }, function() {
                        $rootScope.showHideLoadingImage(false);
                    });
                }, function(data) {
                    $scope.documentError(data);
                }, $scope.syncProgressCallback, $scope.syncModuleProgress);

            } else {
                $rootScope.enableSignature = false;
                $rootScope.showHideLoadingImage(false);
                $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.NotInOnlinePopup"), translateMessages($translate, "fna.ok"));
            }
        } else {
            $rootScope.enableSignature = true;
            EappService.downloadApplicationPdf($scope.pdfSuccessCallback, $scope.pdfErrorCallback);

        }

    };
    $scope.pdfErrorCallback = function() {
        $rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
        $rootScope.showHideLoadingImage(false);
        $scope.refresh();
    };
    $scope.pdfSuccessCallback = function() {
        $rootScope.showHideLoadingImage(false);
        $scope.refresh();
    };

    $scope.syncProgressCallback = function(currentIndex, totalCount) {
        // set the progress percentage and message
        //$scope.syncProgress=progress;
        $rootScope.showHideLoadingImage(false, "");

        $scope.refresh();
    };
    $scope.syncModuleProgress = function(syncMessage, progress) {
        // set the progress percentage and message
        $rootScope.showHideLoadingImage(false, "");
        $scope.syncMessage = syncMessage;
        $scope.refresh();
    }

    $scope.diableAppDate = function() {
        var applicationNumber = $("#InsuredApplicationNo").val();
        $scope.LifeEngageProduct.ApplicationNo = applicationNumber;
        if ($scope.LifeEngageProduct.ApplicationNo.length == 7) {
            $('#ApplicationDate').attr('disabled', false);
        } else {
            $('#ApplicationDate').attr('disabled', true);
        }

        $scope.updateErrorCount('DeclarationAndAuthorizationTab');
        $rootScope.radioValidations();
    };

    $scope.EditAdditionalInsured = function(index, subindex, successcallback) {
        EappVariables.setEappModel($scope.LifeEngageProduct);
        successcallback();
    }
    $scope.onSaveError = function(msg) {
        $scope.disableProceedButtonCommom = false;
        var isDisabledDeclarationAndAuth = $("#summaryProceedButton").is(':disabled');
        if (isDisabledDeclarationAndAuth) {
            $("#summaryProceedButton").removeAttr('disabled');
        }
        var isDisabledACR = $("#agentSummaryButton").is(':disabled');
        if (isDisabledACR) {
            $("#agentSummaryButton").removeAttr('disabled');
        }
        $rootScope.showHideLoadingImage(false);
        if (!$scope.isAutoSave) {
            $rootScope.NotifyMessages(true, msg);
        }
    }

    $scope.clearPrecompileLocalStorage = function() {

        if (localStorage['GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_Payment#tabDetails_1']) {
            localStorage.removeItem('GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_Payment#tabDetails_1');
        }
        if (localStorage['GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_step2Payment#tabDetails_1']) {
            localStorage.removeItem('GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_step2Payment#tabDetails_1');
        }
        if (localStorage['GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_step3Payment#tabDetails_1']) {
            localStorage.removeItem('GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_step3Payment#tabDetails_1');

        }
        if (localStorage['GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_BeneficiarySubTab#tabDetails_1']) {
            localStorage.removeItem('GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_BeneficiarySubTab#tabDetails_1');
        }
        if (localStorage['GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_financilaDetailsSubTab#tabDetails_1']) {
            localStorage.removeItem('GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_financilaDetailsSubTab#tabDetails_1');
        }
        if (localStorage['GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_mainInsuredSubTab#tabDetails_1']) {
            localStorage.removeItem('GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_mainInsuredSubTab#tabDetails_1');
        }
        if (localStorage['GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_HeaderDetails#HeaderDetails_6']) {
            localStorage.removeItem('GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_HeaderDetails#HeaderDetails_6');
        }
        if (localStorage['GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_popupTabsStartbar#popupTabs_6']) {
            localStorage.removeItem('GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_popupTabsStartbar#popupTabs_6');
        }
        if (localStorage['GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_PolicyHolderSubTab#tabDetails_1']) {
            localStorage.removeItem('GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_PolicyHolderSubTab#tabDetails_1');
        }
        if (localStorage['GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_eAppFatcaQuestionnaireJson#tabDetails_1']) {
            localStorage.removeItem('GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_eAppFatcaQuestionnaireJson#tabDetails_1');
        }
        if (localStorage['GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_PersonalDetails_10#PersonalDetails_6']) {
            localStorage.removeItem('GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_PersonalDetails_10#PersonalDetails_6');
        }
        if (localStorage['GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_PersonalDetails_3#PersonalDetails_6']) {
            localStorage.removeItem('GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_PersonalDetails_3#PersonalDetails_6');
        }
        if (localStorage['GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_PersonalDetails_4#PersonalDetails_6']) {
            localStorage.removeItem('GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_PersonalDetails_4#PersonalDetails_6');
        }
        if (localStorage['GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_PersonalDetails_6#PersonalDetails_6']) {
            localStorage.removeItem('GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_PersonalDetails_6#PersonalDetails_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6');
		}
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6');
		}
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6');
        }
        if (localStorage['GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_filterSection#filterSection_6']) {
            localStorage.removeItem('GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_filterSection#filterSection_6');
        }
        if (localStorage['GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_callDetailsSection#popupContent_6']) {
            localStorage.removeItem('GeneraliThailand_' + rootConfig.majorVersion + '.' + rootConfig.minorVersion + '_UI_callDetailsSection#popupContent_6');
        }
    }
    $scope.Save = function() {
        
        $scope.getPayerOccupation();
        $scope.clearPrecompileLocalStorage();
        // var transObj = PersistenceMapping.mapScopeToPersistence({});;
        // transObj.Key21 = $scope.LifeEngageProduct.ApplicationNo;
        EappService.saveTransaction($scope.onSaveSuccess, $scope.onSaveError, $scope.isAutoSave);
    };

    $scope.EditAdditionalInsuredAfterSaveSaveBtnClickSuccess = function(index, subindex, tabid, isInnerTab, selectedTab, json, data) {
        EappVariables.setEappModel($scope.LifeEngageProduct);
        $rootScope.transactionId = data;
        if ((rootConfig.isDeviceMobile)) {
            EappVariables.EappKeys.Id = data;
        } else {
            EappVariables.EappKeys.Id = data.Key1;
        }

        $scope.isNotAutoSave = false;
        $rootScope.showHideLoadingImage(false);
        if ($scope.LifeEngageProduct.LastVisitedUrl && $scope.LifeEngageProduct.LastVisitedUrl != "") {
            $scope.paintView(index, subindex, tabid, isInnerTab, selectedTab, json);
        }
    }
    $scope.onSaveBtnClickSuccess = function(data) {
        $rootScope.showHideLoadingImage(false);
        //fix for save and proceed button getting disabled after coming back grom agent declaration screen
        if (EappVariables.EappKeys.Key15 == "Confirmed" && $scope.LifeEngageProduct.Declaration.confirmSignature == true) {
            $scope.LifeEngageProduct.Declaration.confirmSignature = true;
        } else {
            $scope.LifeEngageProduct.Declaration.confirmSignature = false;
        }
        //copied the code inside $scope.savesuccess
        if (!$scope.isNotAutoSave || $scope.saveFromSignatureConfirm === "true") {
            $scope.saveFromSignatureConfirm = "false";
            $rootScope.showHideLoadingImage(false);
        } else {
            var nextPage, index, subindex, tabid, isInnerTab, selectedTab, json;
            if ($scope.LifeEngageProduct.LastVisitedUrl && $scope.LifeEngageProduct.LastVisitedUrl != "") {
                nextPage = ($scope.LifeEngageProduct.LastVisitedUrl).split(",");
                index = parseInt(nextPage[0]);
                subindex = parseInt(nextPage[1]);
                tabid = nextPage[2].split("'").join('');
                isInnerTab = $scope.stringToBool(nextPage[3]);
                selectedTab = nextPage[4].split("'").join('');
                json = nextPage[5].split("'").join('');
            }
            // Optimize the code for performance improvement - Begin
            // Delete all questionaire from the scope model and keep only the required questuionaire model as a single object
            if ((rootConfig.isDeviceMobile)) {
                EappVariables.QuestionModel = "";
                if ($scope.LifeEngageProduct.LastVisitedUrl && tabid == "MainInsuredQuestionnaire") {
                    EappVariables.QuestionModel = angular.copy($scope.LifeEngageProduct.Insured.Questionnaire);
                    //delete $scope.LifeEngageProduct.Insured["Questionnaire"];
                } else if ($scope.LifeEngageProduct.LastVisitedUrl && tabid == "PolicyHolderQuestionnaire") {
                    EappVariables.QuestionModel = angular.copy($scope.LifeEngageProduct.Payer.Questionnaire);
                    //delete $scope.LifeEngageProduct.Payer["Questionnaire"];
                } else if ($scope.LifeEngageProduct.LastVisitedUrl && tabid == "eAppFatcaQuestionnaireJson") {
                    EappVariables.QuestionModel = angular.copy($scope.LifeEngageProduct.Proposer.Questionnaire);
                }

                // In summary need not delete the scope data in questionnaire
                if (index != "4" && index != 4 && index != "5" && index != 5 && index != "6" && index != 6) {
                    if ($scope.LifeEngageProduct.Insured.Questionnaire) {
                        $scope.additionalQuestions = angular.copy($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare);

                        if ((index == 1 || index == "1") && (subindex == 2 || subindex == "2")) {
                            //$scope.LifeEngageProduct.Insured.Questionnaire = {};
                            $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare = {};
                            $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.Questions = {};
                            $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare = angular.copy($scope.additionalQuestions);
                            //delete $scope.additionalQuestions;
                            $scope.EditAdditionalInsuredAfterSaveSaveBtnClickSuccess(index, subindex, tabid, isInnerTab, selectedTab, json, data);
                        } else {
                            $scope.EditAdditionalInsuredAfterSaveSaveBtnClickSuccess(index, subindex, tabid, isInnerTab, selectedTab, json, data);
                        }
                    } else {
                        $scope.EditAdditionalInsuredAfterSaveSaveBtnClickSuccess(index, subindex, tabid, isInnerTab, selectedTab, json, data);
                    }
                } else {
                    EappVariables.setEappModel($scope.LifeEngageProduct);
                    $rootScope.transactionId = data;
                    EappVariables.EappKeys.Id = data;
                    $scope.isNotAutoSave = false;
                    $rootScope.showHideLoadingImage(false);
                    if ($scope.LifeEngageProduct.LastVisitedUrl && $scope.LifeEngageProduct.LastVisitedUrl != "") {
                        $scope.paintView(index, subindex, tabid, isInnerTab, selectedTab, json);
                    }
                }
            } else {
                $scope.isNotAutoSave = false;
                $rootScope.showHideLoadingImage(false);
                if ($scope.LifeEngageProduct.LastVisitedUrl && $scope.LifeEngageProduct.LastVisitedUrl != "") {
                    $scope.paintView(index, subindex, tabid, isInnerTab, selectedTab, json);
                }
            }
        }
        $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "successMessage"), translateMessages($translate, "fna.ok"));
        $scope.refresh();
    }
    $scope.onSaveBtnClickError = function() {
        $rootScope.showHideLoadingImage(false);
        $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "saveTransactionsError"), translateMessages($translate, "fna.ok"));
        $scope.refresh();
    }
    $scope.saveBtnClick = function(isAutoSave) {
        
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
        $scope.isAutoSave = isAutoSave;
        $scope.isNotAutoSave = "true";
        EappService.saveTransaction($scope.onSaveBtnClickSuccess, $scope.onSaveBtnClickError, isAutoSave);
        $rootScope.validationBeforesave("save", function() {
            $scope.editEappQuestions(function() {
                $scope.SPAJAllocation(function() {
                    if ($scope.eAppParentobj.nextPage.indexOf('Payment') >= 0) {
                        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
                        EappService.saveTransaction($scope.onSaveBtnClickSuccess, $scope.onSaveBtnClickError, isAutoSave);
                    } else {
                        EappService.saveTransaction($scope.onSaveBtnClickSuccess, $scope.onSaveBtnClickError, isAutoSave);
                    }
                });
            });
        });
    };
    $scope.getView = function(modifiedeAppUIJson, id, successCallBack) {
        changeViewIdLoop: for (i = 0; i < modifiedeAppUIJson.Views.length; i++) {
            if (modifiedeAppUIJson.Views[i].id == id) {
                successCallBack(modifiedeAppUIJson.Views[i]);
                break changeViewIdLoop;
            }
        }
    }

    $scope.populateIndependentLookupDateInScope = function(type, fieldName, model, setDefault, field, currentTab, successCallBack, errorCallBack,cacheable) {
        $scope[fieldName] = [{
            "key": "",
            "value": "loading ...."
        }];
		if (!cacheable){
					cacheable = false;
				}
        var options = {};
        options.type = type;
        DataLookupService.getLookUpData(options, function(data) {
            $scope[fieldName] = data;
            $rootScope[fieldName] = data;
            if (setDefault) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isDefault == 1) {
                        if (type == "NATIONALITY") {
                            if ($scope.LmsModel.Lead.BasicDetails.nationality == "") {
                                $scope.setValuetoScope(model, data[i].value);
                                break;
                            } else {
                                $scope.fromReset = false;
                            }
                        } else {
                            $scope.setValuetoScope(model, data[i].value);
                            break;
                        }
                    }
                }
            } else {
                if (model) {
                    var n = "$scope." + model;
                    //var m = eval (n);
                    var m = GLI_EvaluateService.evaluateString($scope, model);
                    if (m) {
                        $scope.setValuetoScope(model, m);
                    }
                }
            }
            if (typeof field != "undefined" && typeof currentTab != "undefined") {
                setTimeout(function() {
                    // var data_Evaluvated=$scope.currentTab[field];
                    //var dataEvaluvated=eval ('$scope.'+currentTab+'.'+field);
                    var dataEvaluvated = GLI_EvaluateService.evaluateString($scope, '$scope.' + currentTab + '.' + field);
                    if (typeof dataEvaluvated != "undefined") {
                        dataEvaluvated.$render();
                    }

                }, 0);
            }



            if (typeof $scope.lmsSectionId != "undefined") {
                // var data_Evaluvated=$scope.lmsSectionId[fieldName];
                // var data_Evaluvated=$scope.evaluateString($scope.lmsSectionId+'.'+fieldName);
                var data_Evaluvated = GLI_EvaluateService.evaluateString($scope, '$scope.' + $scope.lmsSectionId + '.' + fieldName);
                if (data_Evaluvated) {
                    data_Evaluvated.$render();
                }
            }

            /* if($scope.viewToBeCustomized) {
											 setTimeout(function(){
					                 		  	if(eval ('$scope.'+$scope.viewToBeCustomized+'.'+fieldName)){
					                     		  eval ('$scope.'+$scope.viewToBeCustomized+'.'+fieldName).$render();
					                     	  	}
											 },0);
										  }*/

            $scope.refresh();
            successCallBack();
        }, function(errorData) {
            $scope[fieldName] = [];

        },cacheable);
    }
    $scope.populateDistrictLookUp = function() {
        $scope.populateIndependentLookupDateInScope('DISTRICT', 'district', '', false, 'district', currentTab, function() {
            $scope.populatewardOrHamletLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }
    $scope.populatewardOrHamletLookUp = function() {
        $scope.populateIndependentLookupDateInScope('WARD', 'wardOrHamlet', '', false, 'wardOrHamlet', currentTab, function() {
            $scope.populatetypeOfIdentityLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }
    //		$scope.populateOccuapationListLookUp = function(){
    //		$scope.populateIndependentLookupDateInScope('OCCUPATION_LIST','occupationList','',false,'occupationList',currentTab,function(){
    //			$scope.populatetypeOfIdentityLookUp();            
    //			//$scope.initialLoadContinue();
    //			$scope.refresh();
    //			},function(){
    //		});
    //		}

    $scope.populatetypeOfIdentityLookUp = function() {
        $scope.populateIndependentLookupDateInScope('IDENTITY', 'identityTypeArray', '', false, 'identityTypeArray', currentTab, function() {
            $scope.populateRelationshipLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }
    $scope.populateRelationshipLookUp = function() {
        $scope.populateIndependentLookupDateInScope('RELATIONSHIP', 'relationship', '', false, 'relationship', currentTab, function() {
            $scope.populateDocumentTypeLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }
    $scope.populateDocumentTypeLookUp = function() {
        $scope.populateIndependentLookupDateInScope('DOCUMENT_IDENTITY', 'documentType', '', false, 'documentType', currentTab, function() {
            $scope.populateDocumentTypePayerLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateDocumentTypePayerLookUp = function() {
        $scope.populateIndependentLookupDateInScope('DOCUMENT_IDENTITY_PAYER', 'documentTypePayer', '', false, 'documentTypePayer', currentTab, function() {
            $scope.populatePaymentRecordsLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populatePaymentRecordsLookUp = function() {
        $scope.populateIndependentLookupDateInScope('PAYMENTRECORDS', 'paymentRecords', '', false, 'paymentRecords', currentTab, function() {
            $scope.populateFatcaDocumentLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateFatcaDocumentLookUp = function() {
        $scope.populateIndependentLookupDateInScope('FATCA', 'fatcaDocument', '', false, 'fatcaDocument', currentTab, function() {
            $scope.populateHealthRecordDocumentLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateHealthRecordDocumentLookUp = function() {
        $scope.populateIndependentLookupDateInScope('HEALTHRECORD', 'healthRecord', '', false, 'healthRecord', currentTab, function() {
            $scope.populatePreviousPolicyDocumentLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populatePreviousPolicyDocumentLookUp = function() {
        $scope.populateIndependentLookupDateInScope('PREVIOUSPOLICY', 'previousPolicy', '', false, 'previousPolicy', currentTab, function() {
            $scope.populateConsentLetterLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateConsentLetterLookUp = function() {
        $scope.populateIndependentLookupDateInScope('CONSENTLETTER', 'consentLetter', '', false, 'consentLetter', currentTab, function() {
            $scope.populateFinancialRecordsLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateFinancialRecordsLookUp = function() {
        $scope.populateIndependentLookupDateInScope('FINANCIALRECORDS', 'financialRecords', '', false, 'financialRecords', currentTab, function() {
            $scope.populateForignQuestionnaireLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateForignQuestionnaireLookUp = function() {
        $scope.populateIndependentLookupDateInScope('FORIGNERQUESTIONNAIRE', 'forignQuestionnaire', '', false, 'forignQuestionnaire', currentTab, function() {
            $scope.populateGuardianDocumentLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateGuardianDocumentLookUp = function() {
        $scope.populateIndependentLookupDateInScope('OTHERS', 'otherDocument', '', false, 'otherDocument', currentTab, function() {
            $scope.populatePhysicalAppFormLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populatePhysicalAppFormLookUp = function() {
        $scope.populateIndependentLookupDateInScope('PHYSICALAPPFORMS', 'physicalAppForms', '', false, 'physicalAppForms', currentTab, function() {
            $scope.populateCompanyNameLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateCompanyNameLookUp = function() {
        $scope.populateIndependentLookupDateInScope('COMPANYNAME', 'companyName', '', false, 'companyName', currentTab, function() {
            $scope.populateInsuranceCompanyLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateInsuranceCompanyLookUp = function() {
        $scope.populateIndependentLookupDateInScope('INSURANCECOMPANY', 'insuranceCompany', '', false, 'insuranceCompany', currentTab, function() {
            $scope.populateProvinceCodeLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }
    $scope.populateProvinceCodeLookUp = function() {
        $scope.populateIndependentLookupDateInScope('PROVINCECODE', 'provinceCode', '', false, 'provinceCode', currentTab, function() {
            $scope.populateIssueBankPaymentLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }
    $scope.populateIssueBankPaymentLookUp = function() {
        $scope.populateIndependentLookupDateInScope('ISSUEBANK', 'issueBankPayment', '', false, 'issueBankPayment', currentTab, function() {
            $scope.populateIssueBranchPaymentLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }
    $scope.populateIssueBranchPaymentLookUp = function() {
        $scope.populateIndependentLookupDateInScope('ISSUEBRANCH', 'issueBranchPayment', '', false, 'issueBranchPayment', currentTab, function() {
            $scope.populateIssueBranchCodePaymentLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }
    $scope.populateIssueBranchCodePaymentLookUp = function() {
        $scope.populateIndependentLookupDateInScope('ISSUEBRANCHCODE', 'issueBranchCodePayment', '', false, 'issueBranchCodePayment', currentTab, function() {
            $scope.populateReceiveBranchPaymentLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateReceiveBranchPaymentLookUp = function() {
        $scope.populateIndependentLookupDateInScope('RECEIVEBRANCH', 'receiveBranchPayment', '', false, 'receiveBranchPayment', currentTab, function() {
            $scope.populateReceiveBranchCodePaymentLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }
    $scope.populateReceiveBranchCodePaymentLookUp = function() {
        $scope.populateIndependentLookupDateInScope('RECEIVEBRANCHCODE', 'receiveBranchCodePayment', '', false, 'receiveBranchCodePayment', currentTab, function() {
            $scope.populatePayincashBranchCodePaymentLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }
    $scope.populatePayincashBranchCodePaymentLookUp = function() {
        $scope.populateIndependentLookupDateInScope('PAYINCASHBRANCHCODE', 'payinCashBranchCodePayment', '', false, 'payinCashBranchCodePayment', currentTab, function() {
            $scope.populatePbOccClassLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populatePbOccClassLookUp = function() {
        $scope.populateIndependentLookupDateInScope('PBOCC_CLASS', 'pbOccClass', '', false, 'pbOccClass', currentTab, function() {
            $scope.populateTitleLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }
    $scope.populateTitleLookUp = function() {
        $scope.populateIndependentLookupDateInScope('TITLE', 'title', '', false, 'title', currentTab, function() {
            $scope.populateTitleGenderLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateTitleGenderLookUp = function() {
        $scope.populateIndependentLookupDateInScope('TITLE_GENDER', 'titleGenderRelationship', '', false, 'titleGenderRelationship', currentTab, function() {
            $scope.populatePaymentBankNameLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populatePaymentBankNameLookUp = function() {
        $scope.populateIndependentLookupDateInScope('PAYMENTBANKNAME', 'PaymentBankName', '', false, 'PaymentBankName', currentTab, function() {
            $scope.populatePaymentOptionLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populatePaymentOptionLookUp = function() {
        $scope.populateIndependentLookupDateInScope('PAYMENTOPTION', 'PaymentOption', '', false, 'PaymentOption', currentTab, function() {
            $scope.populateNatureOfWorkLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateNatureOfWorkLookUp = function() {
        $scope.populateIndependentLookupDateInScope('NATUREOFWORK', 'natureOfWork', '', false, 'natureOfWork', currentTab, function() {
            $scope.populateIdentityTypeEappLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateIdentityTypeEappLookUp = function() {
        $scope.populateIndependentLookupDateInScope('IDENTITYTYPE', 'identityTypeApp', '', false, 'identityTypeApp', currentTab, function() {
            $scope.populateIdentityTypeBeneficiaryLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateIdentityTypeBeneficiaryLookUp = function() {
        $scope.populateIndependentLookupDateInScope('BENEFICIARYIDENTITY', 'identityTypeBeneficiary', '', false, 'identityTypeBeneficiary', currentTab, function() {
            $scope.populateCopyFromLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateCopyFromLookUp = function() {
        $scope.populateIndependentLookupDateInScope('CONVENIENTCONTACT', 'convenientContact', '', false, 'convenientContact', currentTab, function() {
            $scope.populateMaritalStatusLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateMaritalStatusLookUp = function() {
        $scope.populateIndependentLookupDateInScope('MARITALSTATUS', 'maritalStatus', '', false, 'maritalStatus', currentTab, function() {
            $scope.populateBankNameLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateBankNameLookUp = function() {
        $scope.populateIndependentLookupDateInScope('BANKNAME', 'bankname', '', false, 'bankname', currentTab, function() {
            $scope.populateNationalityLookUp();
            $scope.refresh();
        }, function() {

        },true);
    }

    $scope.populateNationalityLookUp = function() {
        $scope.populateIndependentLookupDateInScope('NATIONALITY', 'nationalityLsit', '', false, 'nationalityLsit', currentTab, function() {
            $scope.initialLoadContinue();
            $scope.refresh();
        }, function() {},true);
    }

    $scope.validateOccupationClassPBRider = function() {
        if (EappVariables.prepopulatedProposerData.OccupationDetails.length < 2) {
            if (EappVariables.prepopulatedProposerData.OccupationDetails[0] !== undefined) {
                $scope.pbPlanClass = $scope.pbPlanClass1;
            }
        } else if (EappVariables.prepopulatedProposerData.OccupationDetails[0] !== undefined && EappVariables.prepopulatedProposerData.OccupationDetails[1] !== undefined) {
            if (!isNaN($scope.pbPlanClass1) && !isNaN($scope.pbPlanClass2)) {
                if ($scope.pbPlanClass1 > $scope.pbPlanClass2) {
                    $scope.pbPlanClass = $scope.pbPlanClass1;
                } else {
                    $scope.pbPlanClass = $scope.pbPlanClass2;
                }
            } else if (isNaN($scope.pbPlanClass1) && isNaN($scope.pbPlanClass2)) {
                if ($scope.pbPlanClass1 == "D") {
                    $scope.pbPlanClass = $scope.pbPlanClass1;
                } else if ($scope.pbPlanClass2 == "D") {
                    $scope.pbPlanClass = $scope.pbPlanClass2;
                } else {
                    $scope.pbPlanClass = $scope.pbPlanClass1;
                }
            } else {
                if (isNaN($scope.pbPlanClass1)) {
                    $scope.pbPlanClass = $scope.pbPlanClass1;
                }

                if (isNaN($scope.pbPlanClass2)) {
                    $scope.pbPlanClass = $scope.pbPlanClass2;
                }
            }
        }

        return $scope.pbPlanClass;
    }

    $scope.mapProductDetails = function() {
        if (EappVariables.illustrationOutputData !== undefined) {
            //BPM mapping - PremTerm    
            $scope.LifeEngageProduct.Product.policyDetails.premiumPeriod = EappVariables.illustrationOutputData.premiumPeriodFinal;

            //BPM mapping - Base class
            if (Array.isArray(EappVariables.illustrationOutputData.BaseOccCode)) {
                $scope.LifeEngageProduct.Product.policyDetails.BaseOccCode = EappVariables.illustrationOutputData.BaseOccCode[0];
            } else {
                $scope.LifeEngageProduct.Product.policyDetails.BaseOccCode = EappVariables.illustrationOutputData.BaseOccCode;
            }

            //BPM mapping - Product Plan code
            $scope.LifeEngageProduct.Product.ProductDetails.planCode = EappVariables.illustrationOutputData.planCodeField;

            //BPM mapping - Basic Plan
            if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == '4' && $scope.LifeEngageProduct.Product.policyDetails.premiumPeriod == '25') {
                $scope.LifeEngageProduct.Product.policyDetails.basicPlan = translateMessages($translate, "eapp_vt.basicPlan1004_25");
            }
            if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == '4' && $scope.LifeEngageProduct.Product.policyDetails.premiumPeriod == '20') {
                $scope.LifeEngageProduct.Product.policyDetails.basicPlan = translateMessages($translate, "eapp_vt.basicPlan1004_20");
            }
            if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == '6') {
                $scope.LifeEngageProduct.Product.policyDetails.basicPlan = translateMessages($translate, "eapp_vt.basicPlan1006");
            }
            if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == '3') {
                $scope.LifeEngageProduct.Product.policyDetails.basicPlan = translateMessages($translate, "eapp_vt.basicPlan1003");
            }
            if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == '10') {
                $scope.LifeEngageProduct.Product.policyDetails.basicPlan = translateMessages($translate, "eapp_vt.basicPlan1010");
            }
			if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == '97') {
                $scope.LifeEngageProduct.Product.policyDetails.basicPlan = translateMessages($translate, "eapp_vt.basicPlan1097");
            }
            if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == '220') {
                $scope.LifeEngageProduct.Product.policyDetails.basicPlan = translateMessages($translate, "eapp_vt.basicPlan1220");
            }
            if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == '306') {
                $scope.LifeEngageProduct.Product.policyDetails.basicPlan = translateMessages($translate, "eapp_vt.basicPlan1306");
            }
            if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == '274') {
                $scope.LifeEngageProduct.Product.policyDetails.basicPlan = translateMessages($translate, "eapp_vt.basicPlan1274");
            }
            
            $scope.LifeEngageProduct.Product.policyDetails.hasPBRider = false;

            //BPM mapping - Rider Plan Code
            if (EappVariables.illustrationOutputData.selectedRiderTbl !== undefined) {
                if (EappVariables.illustrationOutputData.selectedRiderTbl.length > 0) {
                    for (var i = 0; i < EappVariables.illustrationOutputData.selectedRiderTbl.length; i++) {
                        if (EappVariables.illustrationOutputData.selectedRiderTbl[i].planName === 'WP') {
                            $scope.wpPlanCode = EappVariables.illustrationOutputData.selectedRiderTbl[i].planCode;
                        }
                        if (EappVariables.illustrationOutputData.selectedRiderTbl[i].planName === 'PB') {
                            $scope.LifeEngageProduct.Product.policyDetails.hasPBRider = true;
                            $scope.pbPlanCode = (parseInt(EappVariables.illustrationOutputData.selectedRiderTbl[i].planCode) === 9430) ? translateMessages($translate, "eapp_vt.PBRiderPlanCode") : EappVariables.illustrationOutputData.selectedRiderTbl[i].planCode;
                        }
                        if (EappVariables.illustrationOutputData.selectedRiderTbl[i].planName === 'ADD') {
                            $scope.addPlanCode = EappVariables.illustrationOutputData.selectedRiderTbl[i].planCode;
                        }
                        if (EappVariables.illustrationOutputData.selectedRiderTbl[i].planName === 'RCC ADD') {
                            $scope.rccaddPlanCode = EappVariables.illustrationOutputData.selectedRiderTbl[i].planCode;
                        }
                        if (EappVariables.illustrationOutputData.selectedRiderTbl[i].planName === 'ADB') {
                            $scope.adbPlanCode = EappVariables.illustrationOutputData.selectedRiderTbl[i].planCode;
                        }
                        if (EappVariables.illustrationOutputData.selectedRiderTbl[i].planName === 'RCC ADB') {
                            $scope.rccadbPlanCode = EappVariables.illustrationOutputData.selectedRiderTbl[i].planCode;
                        }
                        if (EappVariables.illustrationOutputData.selectedRiderTbl[i].planName === 'AI') {
                            $scope.aiPlanCode = EappVariables.illustrationOutputData.selectedRiderTbl[i].planCode;
                        }
                        if (EappVariables.illustrationOutputData.selectedRiderTbl[i].planName === 'RCC AI') {
                            $scope.rccaiPlanCode = EappVariables.illustrationOutputData.selectedRiderTbl[i].planCode;
                        }
                        if (EappVariables.illustrationOutputData.selectedRiderTbl[i].planName === 'HB(A)') {
                            $scope.hbPlanCode = EappVariables.illustrationOutputData.selectedRiderTbl[i].planCode;
                        }
                        if (EappVariables.illustrationOutputData.selectedRiderTbl[i].planName === 'HS Extra') {
                            $scope.hsPlanCode = EappVariables.illustrationOutputData.selectedRiderTbl[i].planCode;
                        }
						if (EappVariables.illustrationOutputData.selectedRiderTbl[i].planName === 'WPInvasiveRider') {
                            $scope.wpicPlanCode = EappVariables.illustrationOutputData.selectedRiderTbl[i].planCode;
                        }						
						if (EappVariables.illustrationOutputData.selectedRiderTbl[i].planName === 'CancerRider' || EappVariables.illustrationOutputData.selectedRiderTbl[i].planName === 'Cancer Rider') {
                            $scope.cncPlanCode = EappVariables.illustrationOutputData.selectedRiderTbl[i].planCode;
                        }
                        if (EappVariables.illustrationOutputData.selectedRiderTbl[i].planName === 'DD_2551') {
                            $scope.ddPlanCode = (parseInt(EappVariables.illustrationOutputData.selectedRiderTbl[i].planCode) === 9412) ? translateMessages($translate, "eapp_vt.DDRiderPlanCode") : EappVariables.illustrationOutputData.selectedRiderTbl[i].planCode;
                        }
                        if (EappVariables.illustrationOutputData.selectedRiderTbl[i].planName === 'OPD') {
                            $scope.opdPlanCode = EappVariables.illustrationOutputData.selectedRiderTbl[i].planCode;
                        }
                    }
                }
            }

            //BPM mapping - Rider Plan Code for CompleteHealth
            if ($scope.LifeEngageProduct.Product.RiderDetails !== undefined && $scope.LifeEngageProduct.Product.ProductDetails.productCode == "10") {
                if ($scope.LifeEngageProduct.Product.RiderDetails.length > 0) {
                    for (var i = 0; i < $scope.LifeEngageProduct.Product.RiderDetails.length; i++) {
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName.indexOf('CI') > -1) {
                            $scope.ciPlanCode = translateMessages($translate, "eapp_vt.CIExtraPlanCode");
                        }
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName.indexOf('HS') > -1) {
                            $scope.hsPlanCode = translateMessages($translate, "eapp_vt.HSExtraPlanCode");
                        }
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName.indexOf('HB') > -1) {
                            $scope.hbPlanCode = translateMessages($translate, "eapp_vt.HBExtraPlanCode");
                        }
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName.indexOf('ADD') > -1) {
                            $scope.addPlanCode = translateMessages($translate, "eapp_vt.ADDExtraPlanCode");
                        }
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName.indexOf('OPD') > -1) {
                            $scope.opdPlanCode = translateMessages($translate, "eapp_vt.OPDPlanCode");
                        }
                    }
                }
            }
			//BPM mapping - Rider Plan Code for Cancer care
			if ($scope.LifeEngageProduct.Product.RiderDetails !== undefined && $scope.LifeEngageProduct.Product.ProductDetails.productCode == "97") {
                if ($scope.LifeEngageProduct.Product.RiderDetails.length > 0) {
                    for (var i = 0; i < $scope.LifeEngageProduct.Product.RiderDetails.length; i++) {
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName.indexOf('Cancer Rider') > -1) {
                            $scope.cncPlanCode = translateMessages($translate, "eapp_vt.CNCExtraPlanCode");
                        }                        
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName.indexOf('WP InvasiveRider') > -1) {
                            $scope.wpicPlanCode = translateMessages($translate, "eapp_vt.WPICExtraPlanCode");
                        }
                    }
                }
            }

            //BPM mapping - Rider classes
            if ($scope.LifeEngageProduct.Product.RiderDetails !== undefined) {
                if ($scope.LifeEngageProduct.Product.RiderDetails.length > 0) {
                    for (var i = 0; i < $scope.LifeEngageProduct.Product.RiderDetails.length; i++) {
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName.indexOf('WP') > -1) {
                            $scope.LifeEngageProduct.Product.RiderDetails[i].planCode = $scope.wpPlanCode;
                            if (Array.isArray(EappVariables.illustrationOutputData.WPOccCode)) {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.WPOccCode[0];
                            } else {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.WPOccCode;
                            }
                        }
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName.indexOf('PB') > -1) {
                            $scope.LifeEngageProduct.Product.RiderDetails[i].planCode = $scope.pbPlanCode;
                            $scope.LifeEngageProduct.Product.policyDetails.hasPBRider = true;
                            if (Array.isArray(EappVariables.illustrationOutputData.PBOccCode)) {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.PBOccCode[0];
                            } else {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.PBOccCode;
                            }

                            if ($scope.LifeEngageProduct.Product.RiderDetails[i].occClass === undefined || $scope.LifeEngageProduct.Product.RiderDetails[i].occClass === "" || $scope.LifeEngageProduct.Product.RiderDetails[i].occClass === null) {
                                if (EappVariables.prepopulatedProposerData.OccupationDetails[0] !== undefined) {
                                    if (EappVariables.prepopulatedProposerData.OccupationDetails.length > 1) {
                                        for (var j = 0; j < $rootScope.pbOccClass.length; j++) {
                                            if (EappVariables.prepopulatedProposerData.OccupationDetails[0].occupationCode === $rootScope.pbOccClass[j].code) {
                                                $scope.pbPlanClass1 = $rootScope.pbOccClass[j].value;
                                            }
                                            if (EappVariables.prepopulatedProposerData.OccupationDetails[1].occupationCode === $rootScope.pbOccClass[j].code) {
                                                $scope.pbPlanClass2 = $rootScope.pbOccClass[j].value;
                                            }
                                        }
                                    } else {
                                        for (var j = 0; j < $rootScope.pbOccClass.length; j++) {
                                            if (EappVariables.prepopulatedProposerData.OccupationDetails[0].occupationCode === $rootScope.pbOccClass[j].code) {
                                                $scope.pbPlanClass1 = $rootScope.pbOccClass[j].value;
                                            }
                                        }
                                    }
                                }

                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = $scope.validateOccupationClassPBRider();
                            }
                        }
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName === 'ADD') {
                            $scope.LifeEngageProduct.Product.RiderDetails[i].planCode = $scope.addPlanCode;
                            if (Array.isArray(EappVariables.illustrationOutputData.ADDOccCode)) {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.ADDOccCode[0];
                            } else {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.ADDOccCode;
                            }
                        }
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName === 'ADB') {
                            $scope.LifeEngageProduct.Product.RiderDetails[i].planCode = $scope.adbPlanCode;
                            if (Array.isArray(EappVariables.illustrationOutputData.ADBOccCode)) {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.ADBOccCode[0];
                            } else {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.ADBOccCode;
                            }
                        }
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName === 'AI') {
                            $scope.LifeEngageProduct.Product.RiderDetails[i].planCode = $scope.aiPlanCode;
                            if (Array.isArray(EappVariables.illustrationOutputData.AIOccCode)) {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.AIOccCode[0];
                            } else {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.AIOccCode;
                            }
                        }
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName === 'RCC ADD') {
                            $scope.LifeEngageProduct.Product.RiderDetails[i].planCode = $scope.rccaddPlanCode;
                            if (Array.isArray(EappVariables.illustrationOutputData.ADDOccCode)) {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.ADDOccCode[0];
                            } else {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.ADDOccCode;
                            }
                        }
                        //CompleteHealth
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName === 'ADD Extra') {
                            $scope.LifeEngageProduct.Product.RiderDetails[i].planCode = $scope.addPlanCode;
                            if (Array.isArray(EappVariables.illustrationOutputData.ADDOccCode)) {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.ADDOccCode[0];
                            } else {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.ADDOccCode;
                            }
                        }
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName === 'RCC ADB') {
                            $scope.LifeEngageProduct.Product.RiderDetails[i].planCode = $scope.rccadbPlanCode;
                            if (Array.isArray(EappVariables.illustrationOutputData.ADBOccCode)) {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.ADBOccCode[0];
                            } else {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.ADBOccCode;
                            }
                        }
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName === 'RCC AI') {
                            $scope.LifeEngageProduct.Product.RiderDetails[i].planCode = $scope.rccaiPlanCode;
                            if (Array.isArray(EappVariables.illustrationOutputData.AIOccCode)) {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.AIOccCode[0];
                            } else {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.AIOccCode;
                            }
                        }
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName.indexOf('CI') > -1) {
                            $scope.LifeEngageProduct.Product.RiderDetails[i].planCode = $scope.ciPlanCode;
                            if (Array.isArray(EappVariables.illustrationOutputData.CIOccCode)) {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.CIOccCode[0];
                            } else {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.CIOccCode;
                            }
                        }
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName.indexOf('HS') > -1) {
                            $scope.LifeEngageProduct.Product.RiderDetails[i].planCode = $scope.hsPlanCode;
                            if (Array.isArray(EappVariables.illustrationOutputData.HSOccCode)) {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.HSOccCode[0];
                            } else {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.HSOccCode;
                            }
                        }
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName.indexOf('HB') > -1) {
                            $scope.LifeEngageProduct.Product.RiderDetails[i].planCode = $scope.hbPlanCode;
                            if (Array.isArray(EappVariables.illustrationOutputData.HBOccCode)) {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.HBOccCode[0];
                            } else {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.HBOccCode;
                            }
                        }
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName === "DD") {
                            $scope.LifeEngageProduct.Product.RiderDetails[i].planCode = $scope.ddPlanCode;
                            if (Array.isArray(EappVariables.illustrationOutputData.DDOccCode)) {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.DDOccCode[0];
                            } else {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.DDOccCode;
                            }
                        }
						if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName === "WP InvasiveRider") {
                            $scope.LifeEngageProduct.Product.RiderDetails[i].planCode = $scope.wpicPlanCode;
                            if (Array.isArray(EappVariables.illustrationOutputData.WPICOccCode)) {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.WPICOccCode[0];
                            } else {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.WPICOccCode;
                            }
                        }						
						if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName === "Cancer Rider") {
                            $scope.LifeEngageProduct.Product.RiderDetails[i].planCode = $scope.cncPlanCode;
                            if (Array.isArray(EappVariables.illustrationOutputData.CNCOccCode)) {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.CNCOccCode[0];
                            } else {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.CNCOccCode;
                            }
                        }
                        if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName.indexOf('OPD') > -1) {
                            $scope.LifeEngageProduct.Product.RiderDetails[i].planCode = $scope.opdPlanCode;
                            if (Array.isArray(EappVariables.illustrationOutputData.HSOccCode)) {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.HSOccCode[0];
                            } else {
                                $scope.LifeEngageProduct.Product.RiderDetails[i].occClass = EappVariables.illustrationOutputData.HSOccCode;
                            }
                        }
                    }
                }
            }
            //BPM mapping - Rider classes and Base class --END--
        }

    }
    $scope.paintInitialView = function() {
        $scope.LifeEngageProduct = EappVariables.getEappModel();

        //FIX FOR LOADING FROM EAPP LISTING SCREEN(Load directly doupload page) WHEN NO UPLOAD HAS BEEN MADE FROM DOCUPLOAD SCREEN.
        //$scope.LifeEngageProduct.Product.templates.eAppInputTemplate="eAppProductJson.json";
        if (EappVariables.EappKeys.Key15 === "" || EappVariables.EappKeys.Key15 === undefined) {
            if ($scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer == "No") {
                $scope.LifeEngageProduct.Payer = angular.copy(EappVariables.prepopulatedProposerData);

                if ($scope.LifeEngageProduct.Payer.BasicDetails.nationality === undefined || $scope.LifeEngageProduct.Payer.BasicDetails.nationality === "") {
                    $scope.LifeEngageProduct.Payer.BasicDetails.nationality = 'THA';
                    EappVariables.prepopulatedProposerData.BasicDetails.nationality = 'THA';
                }

            }

            //Province code mapping for LA - currentAddress   
            if ($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress !== undefined) {
                if ($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.state !== undefined && $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.state !== "") {
                    for (var i = 0; i < $rootScope.provinceCode.length; i++) {
                        if ($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.state === $rootScope.provinceCode[i].code) {
                            $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.provinceCode = $rootScope.provinceCode[i].value;
                        }
                    }
                }
            }
            $scope.LifeEngageProduct.traineeAgentCode = "";
            $scope.mapProductDetails();
            $scope.Save();
        }
        var LastVisitedURL;
        if ($scope.LifeEngageProduct.Payment.PaymentInfo !== undefined) {
            if ($scope.LifeEngageProduct.Payment.PaymentInfo.status == 'Payment done') {
                LastVisitedURL = "6,0,'DocumentUpload',false,'tabsinner18',''";
            } else {
                LastVisitedURL = $scope.LifeEngageProduct.LastVisitedUrl;
            }
        } else {
            LastVisitedURL = $scope.LifeEngageProduct.LastVisitedUrl;
        }

        //var LastVisitedURL = "5,0,'Payment',false,'tabsinner16',''";
        LEDynamicUI.paintUI(rootConfig.template, 'eAppTabNavigation.json', "buyOnlineTabs", "#buyOnlineTabs", true, function(callbackId, data) {
            if (LastVisitedURL && LastVisitedURL != "") {
                var nextPage = LastVisitedURL.split(",");
                var index = parseInt(nextPage[0]);
                var subindex = parseInt(nextPage[1]);
                var tabid = nextPage[2].split("'").join('');
                var isInnerTab = $scope.stringToBool(nextPage[3]);
                var selectedTab = nextPage[4].split("'").join('');
                var json = nextPage[5].split("'").join('');
                $scope.LifeEngageProduct.LastVisitedIndex = index + ',' + subindex;
                if (EappVariables.EappKeys.Key7 == 'PendingUW' && $rootScope.fromMemo == false) {
                    $scope.paintView(4, 0, 'SPAJsummary', false, 'SPAJsummary', '');
                } else {
                    $scope.paintView(index, subindex, tabid, isInnerTab, selectedTab, json);
                }
            } else {
                $scope.LifeEngageProduct.LastVisitedIndex = "1,1";
                $scope.paintView(1, 1, 'mainInsuredSubTab', false, 'mainInsuredSubTab', '');

                //$scope.LifeEngageProduct.LastVisitedIndex = "3,0";
                //$scope.paintView(3,0,'MainInsuredQuestionnaire',false,'HealthDetailsSubTab','eAppQuestionnaire');
            }
            if (!$scope.isFinalSubmit) { // disable final if not final submit happaned
                // $scope.eAppTabNavStyle[$scope.eAppTabNavStyle.length
                // -1] = 'disabled';
            }
        }, $scope, $compile);
    }

    // This function is specific for summary page:written to call medical category rules on load.
    $scope.loadpaintView = function(index, subindex, id, isInnerTab, selectedTab, json) {
        
        if (id != "MainInsuredQuestionnaire" || id != "PolicyHolderQuestionnaire") {            
           var summaryActive=$("#registration_tabs li:nth-child(4)").hasClass("activated");
           if(summaryActive)
            {
            $scope.secId = '';
            }
         }       

        if ($scope.eAppTabNavStyle[index - 1] == 'activated' || $scope.eAppTabNavStyle[index - 1] == 'activate') {
            if (isInnerTab) {
                if ($scope.innertabs[subindex - 1] == 'active' || $scope.innertabs[subindex - 1] == 'activated') {
                    $rootScope.showHideLoadingImage(true, 'calculationProgress', $translate);
                    $scope.customizationQuestionnaire(index, subindex, id, isInnerTab, selectedTab, json);
                } else {
                    return;
                }
            } else {
                $rootScope.showHideLoadingImage(true, 'calculationProgress', $translate);
                $scope.customizationQuestionnaire(index, subindex, id, isInnerTab, selectedTab, json);
            }
        } else {
            return;
        }
    };

    $scope.customizeLoadpaintView = function(index, subindex, id, isInnerTab, selectedTab, json) {
        if ($scope.innertabs[subindex - 1] === 'activated' || $scope.innertabs[subindex - 1] === 'active') {
            $scope.loadpaintView(index, subindex, id, isInnerTab, selectedTab, json);
        }
    }

    // call the paint function depending on the tabs that are active when clicked on the tab.
    $scope.initailPaintView = function(index, subindex, id, isInnerTab, selectedTab, json) {

        //Fix - If error exist in subtab, navigation is not allowed - Defect 3270
        //			if($scope.eAppParentobj.errorCount!=undefined){
        //				if($scope.eAppParentobj.errorCount != 0){
        //					return;
        //				}
        //			}

        if (id == "MainInsuredQuestionnaire") {
            $scope.secId = 'LifeStyleQuestions';
            $scope.tabActive = "LifeStyleQuestions-0";
        } else if (id == "PolicyHolderQuestionnaire") {
            $scope.secId = 'LifeStyleQuestionsPayer';
            $scope.tabActive = "LifeStyleQuestionsPayer-0";
        } else {                 
            if(id=="Payment")
            {
               var paymentActive=$("#registration_tabs li:nth-child(5)").hasClass("activated");  
                 if(paymentActive)
                 {
                     $scope.secId = ""; 
                 }
            }
            else
            {
            $scope.secId = "";            
            }
         }          
        

        if (subindex != 0) {
            $scope.innertabs[subindex - 1] = 'active';
        } else {
            $scope.innertabs[0] = 'active';
        }

        if (id == "step2Payment") {
            if ($rootScope.skipOptionSelected == true) {
                //	$scope.paintView(6,0,'DocumentUpload',true,'DocumentUpload','');
                //$scope.paintView(6,0,'DocumentUpload',true,'DocumentUpload','');
                $rootScope.redirectDocupload();
                return;

            }
        }

        if (id == "DocumentUpload") {
            $rootScope.docview = true;
            $rootScope.tabview = false;
        }

        if (id == "Payment" && EappVariables.EappKeys.Key34 == 'DataEntryCompleted') {
            $scope.eAppTabNavStyle[4] = 'activate';
        }

        if ($scope.eAppTabNavStyle[index - 1] == 'activated' || $scope.eAppTabNavStyle[index - 1] == 'activate') {
            if (isInnerTab) {
                if ($scope.eAppParentobj.FromSummaryFlag && $scope.eAppParentobj.FromSummaryFlag == true) {
                    $scope.eAppParentobj.FromSummaryFlag = false;
                    for (var i = 0; i < $scope.innertabs.length; i++) {
                        $scope.innertabs[i] = 'activated';
                    }
                    if ($scope.innertabs[subindex - 1] == 'active' || $scope.innertabs[subindex - 1] == 'activated') {
                        $scope.customizationQuestionnaire(index, subindex, id, isInnerTab, selectedTab, json);
                    } else {
                        return;
                    }
                } else {
                    if ($scope.innertabs[subindex - 1] == 'active' || $scope.innertabs[subindex - 1] == 'activated') {
                        $scope.customizationQuestionnaire(index, subindex, id, isInnerTab, selectedTab, json);
                    } else {
                        return;
                    }
                }
            } else {
                $scope.customizationQuestionnaire(index, subindex, id, isInnerTab, selectedTab, json);
            }
        } else {
            return;
        }
    }

    // -----------------------Performance Customization--------------
    $scope.customizationQuestionnaire = function(index, subindex, id, isInnerTab, selectedTab, json) {
        if ((rootConfig.isDeviceMobile)) {
            $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
            EappService.getEappBeforeSaveEapp(function() {
                EappVariables.QuestionModel = "";
                if (id == "MainInsuredQuestionnaire") {
                    EappVariables.QuestionModel = angular.copy(EappVariables.EappModel.Insured.Questionnaire);
                } else if (id == "PolicyHolderQuestionnaire") {
                    EappVariables.QuestionModel = angular.copy(EappVariables.EappModel.Payer.Questionnaire);
                   // delete EappVariables.EappModel.Payer["Questionnaire"];
                } else if (id == "eAppFatcaQuestionnaireJson") {
                    EappVariables.QuestionModel = angular.copy(EappVariables.EappModel.Proposer.Questionnaire);
                }
                if (index == "4" || index == 4 || index == "6" || index == 6) {
                    $scope.paintView(index, subindex, id, isInnerTab, selectedTab, json);
                } else {
                    if (EappVariables.EappModel.Insured.Questionnaire !== undefined && EappVariables.EappModel.Insured.Questionnaire.AdditionalQuestioniare !== undefined) {
                        $scope.additionalQuestions = angular.copy(EappVariables.EappModel.Insured.Questionnaire.AdditionalQuestioniare);
                   //     delete EappVariables.EappModel.Insured["Questionnaire"];
                        if ((index == 1 || index == "1") && (subindex == 2 || subindex == "2")) {
                            EappVariables.EappModel.Insured.Questionnaire = {};
                            EappVariables.EappModel.Insured.Questionnaire.AdditionalQuestioniare = {};
                            EappVariables.EappModel.Insured.Questionnaire.AdditionalQuestioniare.Questions = {};
                            EappVariables.EappModel.Insured.Questionnaire.AdditionalQuestioniare = angular.copy($scope.additionalQuestions);
                          //  delete $scope.additionalQuestions;
                            $scope.EditAdditionalInsuredTabClick(index, subindex, id, isInnerTab, selectedTab, json);
                        } else {
                            $scope.EditAdditionalInsuredTabClick(index, subindex, id, isInnerTab, selectedTab, json);
                        }
                    } else {
                        $scope.EditAdditionalInsuredTabClick(index, subindex, id, isInnerTab, selectedTab, json);
                    }
                }
                // $scope.paintView(index,subindex,id,isInnerTab,selectedTab,json);
            }, function() {
            	
            });
        } else {
            $scope.paintView(index, subindex, id, isInnerTab, selectedTab, json);
        }
    }

    $scope.EditAdditionalInsuredTabClick = function(index, subindex, id, isInnerTab, selectedTab, json) {
        $scope.paintView(index, subindex, id, isInnerTab, selectedTab, json);
    }

    $scope.backNavigation = false;
    $scope.paintView = function(index, subindex, id, isInnerTab, selectedTab, json) {
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
        var urlIndex;
        var lastVisitedIndex;
        var lastVisitedSubIndex;
        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "" && $scope.LifeEngageProduct.LastVisitedIndex.length > 0) {
            urlIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(",");
            // index
            if (parseInt(urlIndex[0]) > index) {
                $scope.backNavigation = true;
            } else {
                $scope.backNavigation = false;
            }
            // subindex
            lastVisitedIndex = parseInt(urlIndex[0]);
            lastVisitedSubIndex = parseInt(urlIndex[1]);
        }

        // Fix for Questionnaire landing and making tab active from product details tab - Defect 3270
        if (id == "MainInsuredQuestionnaire") {
            $scope.secId = 'LifeStyleQuestions';
            $scope.tabActive = "LifeStyleQuestions-0";
        } else if (id == "PolicyHolderQuestionnaire") {
            $scope.secId = 'LifeStyleQuestionsPayer';
            $scope.tabActive = "LifeStyleQuestionsPayer-0";
        }

        if (subindex != 0) {
            $scope.innertabs[subindex - 1] = 'active';
        } else {
            $scope.innertabs[0] = 'active';
        }

        $scope.paintTabAndActivate = function() {
            // for main tabs active or disabling
            for (var i = 0; i < $scope.eAppTabNavStyle.length; i++) {
                $scope.tabPosition = index;
                if (i < index) {
                    if (index == 6 && $scope.setDocUploadDirectly && $scope.setDocUploadDirectly == true) {
                        if (i < lastVisitedIndex) {
                            $scope.eAppTabNavStyle[i] = 'activated';
                        } else {
                            $scope.eAppTabNavStyle[i] = '';
                        }
                    } else {
                        $scope.eAppTabNavStyle[i] = 'activated';
                    }
                } else {
                    if (lastVisitedIndex != "" && i < lastVisitedIndex) {
                        if (i > 0) {
                            $scope.eAppTabNavStyle[i] = 'activated';
                            //$scope.eAppTabNavStyle[i] = ''; 
                        }
                    } else if ($scope.eAppTabNavStyle[i] != 'disabled') {
                        $scope.eAppTabNavStyle[i] = '';
                    }
                }
            }
            if ($scope.setDocUploadDirectly)
                $scope.setDocUploadDirectly = false;
            if (UserDetailsService.getAgentRetrieveData().AgentDetails.agentType == rootConfig.agentTypeNormal && EappVariables.EappKeys.Key15 != "Submitted") {
                $scope.eAppTabNavStyle[$scope.eAppTabNavStyle.length - 1] = 'activate';
            }
            $scope.eAppTabNavStyle[index - 1] = 'activate'; // activate the current tab clicked

            $scope.paintSuccess = function() {
                if (typeof $scope.eAppParentobj.PreviousPage != "undefined" && $scope.eAppParentobj.PreviousPage != "") {
                    $scope.$emit($scope.eAppParentobj.PreviousPage);
                }
                $scope.$emit(id, [selectedTab]);
                if ([selectedTab] == "SPAJsummary") {
                    var productDivId = "SummaryProductDetails";
                    var eAppJSON = $scope.LifeEngageProduct.Product.templates.eAppInputTemplate;
                    LEDynamicUI.paintUI(rootConfig.template, eAppJSON, productDivId, "#ProductDetailsSummary", true, function(callBackId) {
                        $scope.refresh();
                        $rootScope.updateErrorCount(callBackId);
                        $scope.disableProceedButtonCommom = false;
                        var isDisabledDeclarationAndAuth = $("#summaryProceedButton").is(':disabled');
                        if (isDisabledDeclarationAndAuth) {
                            $("#summaryProceedButton").removeAttr('disabled');
                        }
                        var isDisabledACR = $("#agentSummaryButton").is(':disabled');
                        if (isDisabledACR) {
                            $("#agentSummaryButton").removeAttr('disabled');
                        }
                        $rootScope.showHideLoadingImage(false);
                    }, $scope, $compile);
                }
                $(".tabs").tabs();
                $scope.tabActivate();
            };

            $scope.tabActivate = function() {
                for (var i = 0; i < $scope.innertabs.length; i++) {
                    $scope.innertabs[i] = '';
                }

                if (isInnerTab || subindex < lastVisitedSubIndex) {
                    var newSubIndex;
                    if (index > lastVisitedIndex) {
                        newSubIndex = subindex;
                    } else {
                        newSubIndex = lastVisitedSubIndex;
                    }
                    for (var i = 0; i <= newSubIndex - 1; i++) {
                        $scope.innertabs[i] = 'activated';
                    }
                }

                // used to enable all the subtabs if navigated back.
                if ($scope.backNavigation) {
                    for (var i = 0; i < $scope.innertabs.length; i++) {
                        $scope.innertabs[i] = 'activated';
                    }
                }

                if (subindex != 0) {
                    $scope.innertabs[subindex - 1] = 'active';
                } else {
                    $scope.innertabs[0] = 'active';
                }

                // TO DO:change the logic generic
                // this is used to enable the second tab
                // if its there in summary.Need to
                // change the logic.//dirty fix
                // if (id == 'SPAJsummary' && $scope.LifeEngageProduct.showHideQuestaionnaireTab) {
                // 	$scope.innertabs[1] = 'activated';
                // }

                if ($scope.id != id) {
                    $scope.id = id;
                    // angular.element('#' + id +'_Tab').trigger('click');
                }
                $scope.disableProceedButtonCommom = false;
                var isDisabledDeclarationAndAuth = $("#summaryProceedButton").is(':disabled');
                if (isDisabledDeclarationAndAuth) {
                    $("#summaryProceedButton").removeAttr('disabled');
                }
                var isDisabledACR = $("#agentSummaryButton").is(':disabled');
                if (isDisabledACR) {
                    $("#agentSummaryButton").removeAttr('disabled');
                }
                if ($scope.questionnaireTimeoutInclusion == true) {
                    $timeout(function() {
                        $rootScope.showHideLoadingImage(false);
                        $rootScope.refresh();
                    }, 1000);
                } else {
                    $rootScope.showHideLoadingImage(false);
                    $rootScope.refresh();
                }
                //fix for web tab alone is painted issue.
                if (subindex == 0) {
                    $scope.innertabs[0] = 'active';
                }
                $scope.questionnaireTimeoutInclusion = false;
            }

            $scope.isPainted = true;
            if (angular.element('#' + id).text().trim().length == 0) {
                $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
                if ($scope.LifeEngageProduct.Product.templates == undefined) {
                    $scope.LifeEngageProduct.Product.templates = EappVariables.getEappModel().Product.templates;
                }
                //$scope.LifeEngageProduct.Product.templates.eAppInputTemplate="eAppProductJson.json";
                var productJsonArray = [id + '.json', $scope.LifeEngageProduct.Product.templates.eAppInputTemplate];
                //$scope.LifeEngageProduct.Product.templates.eAppInputTemplate = 'eAppProductJson.json';
                if (typeof $scope.modifiedeAppUIJson1 != 'undefined' && $scope.modifiedeAppUIJson1 != "") {
                    LEDynamicUI.paintUIFromJsonObject(rootConfig.template, $scope.modifiedeAppUIJson1, id, '#tabDetails', true, $scope.paintSuccess, $scope, $compile);
                    $scope.modifiedeAppUIJson1 = "";
                    $scope.refresh();
                } else {
                    LEDynamicUI.paintUI(rootConfig.template, productJsonArray, id, '#tabDetails', true, $scope.paintSuccess, $scope, $compile);
                }
            } else {
                $scope.modifiedeAppUIJson1 = "";
                if (typeof $scope.eAppParentobj.PreviousPage != "undefined" && $scope.eAppParentobj.PreviousPage != "") {
                    var substringData = $scope.eAppParentobj.PreviousPage.substr(0, $scope.eAppParentobj.PreviousPage.indexOf('_'));
                    if (substringData != selectedTab) {
                        $scope.$emit($scope.eAppParentobj.PreviousPage);
                    }
                }
                $scope.$emit(id, [selectedTab, $scope.LifeEngageProduct.LastVisitedIndex]);
                $scope.tabActivate();
                // $scope.$emit(id, [selectedTab]);
            }
        }

        if (json != "" && typeof json != "undefined") {
            if (json == "eAppQuestionnaire") {
                $scope.eAppQuestionnaire = rootConfig.eAppQuestionnaire;
            } else if (json == "eAppProposerQuestionnaire") {
                $scope.eAppQuestionnaire = rootConfig.eAppProposerQuestionnaire;
            } else if (json == "eAppSummaryQuestionnaire") {
                $scope.eAppQuestionnaire = rootConfig.eAppSummaryQuestionnaire;
            }
            LEDynamicUI.getUIJson(id + '.json', false, function(modifiedeAppUIJson) {
                /* $scope.getView(modifiedeAppUIJson,id,function(modifiedQuestionnaireView) { */
                LEDynamicUI.getUIJson($scope.eAppQuestionnaire, false, function(eAppQuestionnaire) {
                    /* modifiedQuestionnaireView.sections = angular.copy(eAppQuestionnaire.sections); */
                    modifiedeAppUIJson.Views[0].sections = angular.copy(eAppQuestionnaire.sections);
                    $scope.modifiedeAppUIJson1 = modifiedeAppUIJson;
                    $scope.paintTabAndActivate();
                });
                /* }) */
            });
        } else {
            $scope.paintTabAndActivate();
        }
    };

    $scope.mapRelationshipWithProposer = function() {
        if ($scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer === 'Yes') {

            $scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer = "SELF";
            $scope.LifeEngageProduct.Payer = angular.copy($scope.LifeEngageProduct.Insured);
            if ($scope.LifeEngageProduct.Payer.Questionnaire != undefined || $scope.LifeEngageProduct.Payer.Questionnaire.AdditionalQuestioniare != undefined) {
                $scope.LifeEngageProduct.Payer.Questionnaire.AdditionalQuestioniare.ShortQuestions = GLI_EappVariables.PayerQuestionnaire.AdditionalQuestioniare.ShortQuestions;
            }
        } else {
            $scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer = "";
            // $scope.LifeEngageProduct.Payer = {};
        }
    }
    $scope.restrictnavigation = true;
    $scope.isDeviceRestriction=true;
    $scope.ispreviousPolicyToFatcaNavigation = false;
    $scope.onSaveSuccess = function(data) {

        if (!$scope.isNotAutoSave || $scope.saveFromSignatureConfirm === "true") {
            $scope.saveFromSignatureConfirm = "false";
            $scope.disableProceedButtonCommom = false;
            var isDisabledDeclarationAndAuth = $("#summaryProceedButton").is(':disabled');
            if (isDisabledDeclarationAndAuth) {
                $("#summaryProceedButton").removeAttr('disabled');
            }
            var isDisabledACR = $("#agentSummaryButton").is(':disabled');
            if (isDisabledACR) {
                $("#agentSummaryButton").removeAttr('disabled');
            }
            $rootScope.showHideLoadingImage(false);
        } else {

            var nextPage, index, subindex, tabid, isInnerTab, selectedTab, json;

            if ($scope.secId == "additionalInformationPayer") {
                $("#headertabs_payer2").removeClass("QuestionnaireCheckbox");
                $("#headerText_payer2").removeClass("diableCheckMark");
            } else if ($scope.secId == 'previousPolicies') {
                $("#headertabs_4").removeClass("QuestionnaireCheckbox");
                $("#headerText_4").removeClass("diableCheckMark");
            }

            $scope.LifeEngageProduct.Insured.Questionnaire = angular.copy(EappVariables.getEappModel().Insured.Questionnaire);
            $scope.LifeEngageProduct.Payer.Questionnaire = angular.copy(EappVariables.getEappModel().Payer.Questionnaire);

            var insuredData = $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions;
            var payerData = $scope.LifeEngageProduct.Payer.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions;

            if (insuredData[0].details == 'No' && insuredData[1].details == 'No' && insuredData[2].details == 'No' &&
                insuredData[3].details == 'No' && insuredData[7].details == 'No' && insuredData[4].details == 'No' &&
                (insuredData[5].details == 'Yes' || insuredData[6].details == 'Yes')) {

                $scope.LifeEngageProduct.isInsured = false;
                // $scope.LifeEngageProduct.isFatca=true;
                // $rootScope.shorteappFatca = true;

            }

            if (insuredData[0].details == 'No' && insuredData[1].details == 'No' && insuredData[2].details == 'No' &&
                insuredData[3].details == 'No' && insuredData[7].details == 'No' && insuredData[4].details == 'No' && insuredData[5].details == 'No' &&
                insuredData[6].details == 'No') {
                $scope.LifeEngageProduct.isInsured = false;
                // $scope.LifeEngageProduct.isFatca=false;

            }

            if (insuredData[0].details == 'Yes' || insuredData[1].details == 'Yes' || insuredData[2].details == 'Yes' ||
                insuredData[3].details == 'Yes' || insuredData[7].details == 'Yes' || insuredData[4].details == 'Yes') {

                $scope.LifeEngageProduct.isInsured = true;
                $rootScope.shorteappInsured = true;
                // $scope.LifeEngageProduct.isFatca=true;
                // $rootScope.shorteappFatca = true;

            }

            if (payerData[0].details == 'No' && payerData[1].details == 'No' && payerData[7].details == 'No' && payerData[2].details == 'No' &&
                payerData[3].details == 'No' && payerData[4].details == 'No') {
                $scope.LifeEngageProduct.isPayer = false;

            } else {
                $scope.LifeEngageProduct.isPayer = true;
                $rootScope.shorteappPayer = true;
                // $scope.LifeEngageProduct.isFatca=true;
                // $rootScope.shorteappFatca = true;
            }

            $scope.LifeEngageProduct.isFatca = true;
            $rootScope.shorteappFatca = true;

            if ($scope.secId &&
                $scope.secId != "additionalInformationPayer" &&
                $rootScope.selectedPage != 'FatcaQuestionnaire') {
                tabid = "MainInsuredQuestionnaire";
                selectedTab = "MainInsuredQuestionnaire";
                subindex = 0;
				
				if($scope.secId == "previousPolicies" || $scope.secId == "IllnessTreatmentHistoryPayer" || $scope.secId == "additionalInformationPayer" || $scope.secId == "LifeStyleQuestionsPayer"){
					tabid = "PolicyHolderQuestionnaire";
					//subindex = 2;
					//selectedTab = 'PolicyHolderQuestionnaire';
				}
				
                $scope.isLifeStyleAvail = false;
                $scope.istreatmentAvail = false;
                $scope.isPayerLifeStyleAvail = false;
                 
                $scope.directIllnessPayer = false;
                $scope.directLifeStylePayer = false;

                if (payerData[0].details == 'No' && payerData[1].details == 'No' && payerData[2].details == 'No' && payerData[7].details == 'No' &&
                    (payerData[3].details == 'Yes' || payerData[4].details == 'Yes')) {
                    $scope.directIllnessPayer = true;
                }

                 if (payerData[0].details == 'Yes' || payerData[1].details == 'Yes'|| payerData[2].details == 'Yes' || payerData[7].details == 'Yes') {
                    $scope.directLifeStylePayer = true;
                }

                //Check the landing to lifestyle if selected
                if ($scope.LifeEngageProduct.isInsured && (insuredData[0].details == 'Yes' || insuredData[1].details == 'Yes' || insuredData[2].details == 'Yes' || insuredData[7].details == 'Yes') && $scope.eAppParentobj.CurrentPage == 'eappshortquestionnaire') {
                    $scope.restrictnavigation = false;
                    $scope.isDeviceRestriction=false;
                    $scope.popupTabsNav('LifeStyleQuestions', 0,true);
                    //$scope.errorMsgQuestionnaire(false,true);
                    $scope.isLifeStyleAvail = true;
                    $scope.disableProceedButtonCommom = false;
                    $scope.LifeEngageProduct.Insured.Questionnaire = angular.copy(EappVariables.getEappModel().Insured.Questionnaire);
                }
                //once landed redirect either to illness or health				 	
                else if ($scope.LifeEngageProduct.isPayer && (!$scope.LifeEngageProduct.isInsured) && (payerData[0].details == 'Yes' || payerData[1].details == 'Yes' || payerData[2].details == 'Yes' || payerData[7].details == 'Yes') && $scope.eAppParentobj.CurrentPage == 'eappshortquestionnaire') {
                    $scope.restrictnavigation = false;
                    $scope.isDeviceRestriction=false;
                    $scope.eAppParentobj.nextPage = "3,2,'PolicyHolderQuestionnaire',true,'PolicyHolderQuestionnaire','eAppProposerQuestionnaire'";
                    $scope.popupTabsNav('LifeStyleQuestionsPayer', 0,true);
                    $scope.isPayerLifeStyleAvail = true;
                    $scope.disableProceedButtonCommom = false;
                    $scope.LifeEngageProduct.Payer.Questionnaire = angular.copy(EappVariables.getEappModel().Payer.Questionnaire);

                }

                //&& (!$scope.LifeEngageProduct.isInsured) 
                else if ($scope.LifeEngageProduct.isPayer && (!$scope.isPayerLifeStyleAvail) && $scope.secId == 'LifeStyleQuestionsPayer') {

                    $("#headertabs_payer0").removeClass("QuestionnaireCheckbox");
                    $("#headerText_payer0").removeClass("diableCheckMark");

                    $scope.restrictnavigation = false;
                    $scope.isDeviceRestriction=false;
                    $scope.disableProceedButtonCommom = false;
                    $scope.LifeEngageProduct.Payer.Questionnaire = angular.copy(EappVariables.getEappModel().Payer.Questionnaire);
                    if (payerData[3].details != 'Yes' && payerData[4].details != 'Yes') {
                        $scope.popupTabsNav('additionalInformationPayer', 2,true);
                    } else if (payerData[3].details == 'Yes' || payerData[4].details == 'Yes' ) {
                        $scope.popupTabsNav('IllnessTreatmentHistoryPayer', 1,true);
                    } else {
                        $scope.popupTabsNav('additionalInformationPayer', 2,true);
                    }
                } else if ($scope.LifeEngageProduct.isInsured && (!$scope.isLifeStyleAvail) && $scope.secId == 'LifeStyleQuestions') {

                    $("#headertabs_0").removeClass("QuestionnaireCheckbox");
                    $("#headerText_0").removeClass("diableCheckMark");

                    $scope.restrictnavigation = false;
                    $scope.isDeviceRestriction=false;
                    $scope.disableProceedButtonCommom = false;
                    $scope.LifeEngageProduct.Insured.Questionnaire = angular.copy(EappVariables.getEappModel().Insured.Questionnaire);
                    if (insuredData[3].details != 'Yes' && insuredData[4].details != 'Yes') {
                        $scope.popupTabsNav('additionalInformation', 3,true);
                    } else if (insuredData[3].details == 'Yes'|| insuredData[4].details == 'Yes') {
                        $scope.popupTabsNav('IllnessTreatmentHistory', 1,true);
                        $scope.istreatmentAvail = true;
                        //$scope.errorMsgQuestionnaire(true,false);
                    } else {
                        $scope.popupTabsNav('additionalInformation', 3,true);
                    }
                } else if ((insuredData[3].details == 'Yes' || insuredData[4].details == 'Yes')   && (!$scope.istreatmentAvail) && (!$scope.isLifeStyleAvail) && $scope.LifeEngageProduct.isInsured && $scope.secId == 'IllnessTreatmentHistory') {
                    $("#headertabs_1").removeClass("QuestionnaireCheckbox");
                    $("#headerText_1").removeClass("diableCheckMark");

                    $scope.restrictnavigation = false;
                    $scope.isDeviceRestriction=false;

                    if ($rootScope.healthRiderAvailable && insuredData[3].details == 'Yes') {
                        $scope.popupTabsNav('healthDDRider', 2,true);
                        $rootScope.editPageNavigation = ['healthDDRider', 1];
                        //$scope.errorMsgQuestionnaire(true,false);
                    } else {
                        $scope.popupTabsNav('additionalInformation', 3,true);
                    }
                    $scope.disableProceedButtonCommom = false;
                    $scope.LifeEngageProduct.Insured.Questionnaire = angular.copy(EappVariables.getEappModel().Insured.Questionnaire);

                } else if ((payerData[3].details == 'Yes' || payerData[4].details == 'Yes') && (!$scope.isPayerLifeStyleAvail) && $scope.LifeEngageProduct.isPayer && $scope.secId == 'IllnessTreatmentHistoryPayer') {
                    $("#headertabs_payer1").removeClass("QuestionnaireCheckbox");
                    $("#headerText_payer1").removeClass("diableCheckMark");

                    $scope.restrictnavigation = false;
                    $scope.isDeviceRestriction=false;
                    $scope.popupTabsNav('additionalInformationPayer', 2,true);
                    $scope.disableProceedButtonCommom = false;
                    $scope.LifeEngageProduct.Payer.Questionnaire = angular.copy(EappVariables.getEappModel().Payer.Questionnaire);

                } else if ($scope.secId == 'healthDDRider') {
                    $("#headertabs_2").removeClass("QuestionnaireCheckbox");
                    $("#headerText_2").removeClass("diableCheckMark");


                    $scope.restrictnavigation = false;
                    $scope.isDeviceRestriction=false;
                    $scope.popupTabsNav('additionalInformation', 3,true);
                    $scope.disableProceedButtonCommom = false;
                    $scope.LifeEngageProduct.Insured.Questionnaire = angular.copy(EappVariables.getEappModel().Insured.Questionnaire);

                } else if ($scope.secId == 'additionalInformation') {
                    $("#headertabs_3").removeClass("QuestionnaireCheckbox");
                    $("#headerText_3").removeClass("diableCheckMark");

                    $scope.restrictnavigation = false;
                    $scope.isDeviceRestriction=false;
                    if ($rootScope.isPreviousPolicyNavigation == true) {
                        $scope.popupTabsNav('previousPolicies', 4,true);
                    } else {
                        $scope.ispreviousPolicyToFatcaNavigation = true;
                        $scope.restrictnavigation = true;
                        $scope.isDeviceRestriction=true;

                        if ($scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer == 'No' &&
                            $scope.LifeEngageProduct.Product.policyDetails.hasPBRider &&
                            $scope.LifeEngageProduct.isPayer) {
                            index = 3;
                            subindex = 2;
                            tabid = 'PolicyHolderQuestionnaire';
                            selectedTab = 'PolicyHolderQuestionnaire';
                            isInnerTab = true;
                            json = 'eAppProposerQuestionnaire';
                            $scope.secId = '';

                            if ($scope.directIllnessPayer) {
                                $rootScope.editPageNavigation = ['IllnessTreatmentHistoryPayer', 1];
                            }

                             else if($scope.directLifeStylePayer){
                            $rootScope.editPageNavigation = ['LifeStyleQuestionsPayer', 0];                            
                        }

                        } else if ($scope.LifeEngageProduct.isFatca) {
                            $scope.ispreviousPolicyToFatcaNavigation = true;
                            $scope.restrictnavigation = true;
                            $scope.isDeviceRestriction=true;
                            nextPage = ($scope.eAppParentobj.nextPage).split(",");
                            index = 3;
                            subindex = 3;
                            tabid = 'eAppFatcaQuestionnaireJson';
                            selectedTab = 'FatcaQuestionnaire';
                            isInnerTab = true;
                            json = '';
                            $scope.secId = '';

                        } else {

                            index = 4;
                            subindex = 1;
                            tabid = 'SPAJsummary';
                            selectedTab = 'SPAJsummary';
                            isInnerTab = true;
                            json = '';
                            $scope.secId = '';

                        }
                    }
                    $scope.disableProceedButtonCommom = false;
                    $scope.LifeEngageProduct.Insured.Questionnaire = angular.copy(EappVariables.getEappModel().Insured.Questionnaire);
                } else if ($scope.secId == 'previousPolicies') {
                    $scope.ispreviousPolicyToFatcaNavigation = true;
                    $scope.restrictnavigation = true;
                    $scope.isDeviceRestriction=true;
                    if ($scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer == 'No' && $scope.LifeEngageProduct.Product.policyDetails.hasPBRider && $scope.LifeEngageProduct.isPayer) {
                        index = 3;
                        subindex = 2;
                        tabid = 'PolicyHolderQuestionnaire';
                        selectedTab = 'PolicyHolderQuestionnaire';
                        isInnerTab = true;
                        json = 'eAppProposerQuestionnaire';
                        $scope.secId = '';

                        if ($scope.directIllnessPayer) {
                            $rootScope.editPageNavigation = ['IllnessTreatmentHistoryPayer', 1];
                        }
                        else if($scope.directLifeStylePayer){
                            $rootScope.editPageNavigation = ['LifeStyleQuestionsPayer', 0];                            
                        }

                    } else if ($scope.LifeEngageProduct.isFatca) {

                        $scope.ispreviousPolicyToFatcaNavigation = true;
                        $scope.restrictnavigation = true;
                        $scope.isDeviceRestriction=true;
                        nextPage = ($scope.eAppParentobj.nextPage).split(",");
                        index = 3;
                        subindex = 3;
                        tabid = 'eAppFatcaQuestionnaireJson';
                        selectedTab = 'FatcaQuestionnaire';
                        isInnerTab = true;
                        json = '';
                        $scope.secId = '';

                    } else {

                        index = 4;
                        subindex = 1;
                        tabid = 'SPAJsummary';
                        selectedTab = 'SPAJsummary';
                        isInnerTab = true;
                        json = '';
                        $scope.secId = '';

                    }
                    $scope.LifeEngageProduct.Insured.Questionnaire = angular.copy(EappVariables.getEappModel().Insured.Questionnaire);
                    $scope.LifeEngageProduct.PreviouspolicyHistory = angular.copy(EappVariables.getEappModel().PreviouspolicyHistory);
                } else if ($scope.secId == "additionalInformationPayer" && !$scope.LifeEngageProduct.isFatca) {

                    index = 4;
                    subindex = 1;
                    tabid = 'SPAJsummary';
                    selectedTab = 'SPAJsummary';
                    isInnerTab = true;
                    json = '';
                    $scope.secId = '';

                }

            } else if ($scope.eAppParentobj.nextPage && $scope.eAppParentobj.nextPage != "") {

                if ($scope.eAppParentobj.CurrentPage == "eappshortquestionnaire" || $rootScope.selectedPage =="eappshortquestionnaire") {
                    $scope.istreatmentAvailiablity = false;
                    $scope.isLifeStyleAvailiablity = false;
                    $scope.isPayerLifeStyleAvailiablity = false;
                    if ($scope.LifeEngageProduct.isInsured && (insuredData[0].details == 'Yes' || insuredData[1].details == 'Yes' || insuredData[2].details == 'Yes' || insuredData[7].details == 'Yes')) {
                        $rootScope.editPageNavigation = ['LifeStyleQuestions', 0];
                        $scope.isLifeStyleAvailiablity = true;
                        $scope.errorMsgQuestionnaire(false, true);
                    } else if ($scope.LifeEngageProduct.isPayer && (!$scope.LifeEngageProduct.isInsured) && (payerData[0].details == 'Yes' || payerData[1].details == 'Yes' || payerData[2].details == 'Yes' || payerData[7].details == 'Yes')) {
                        $scope.eAppParentobj.nextPage = "3,2,'PolicyHolderQuestionnaire',true,'PolicyHolderQuestionnaire','eAppProposerQuestionnaire'";
                        $rootScope.editPageNavigation = ['LifeStyleQuestionsPayer', 0];
                        $scope.isPayerLifeStyleAvailiablity = true;
                    } else if ($scope.LifeEngageProduct.isInsured && (!$scope.isLifeStyleAvailiablity) && (insuredData[3].details == 'Yes' || insuredData[4].details == 'Yes' || $scope.secId == 'LifeStyleQuestions')) {
                        $rootScope.editPageNavigation = ['IllnessTreatmentHistory', 1];
                        $scope.istreatmentAvailiablity = true;
                        $scope.errorMsgQuestionnaire(true, false);
                    } else if (insuredData[3].details == 'Yes' && ($scope.healthRiderAvailable) && (!$scope.istreatmentAvailiablity) && (!$scope.isLifeStyleAvailiablity) && $scope.LifeEngageProduct.isInsured && $scope.secId == 'IllnessTreatmentHistory') {
                        $rootScope.editPageNavigation = ['healthDDRider', 1];
                        $scope.errorMsgQuestionnaire(true, false);
                    } else if ($scope.LifeEngageProduct.isPayer && (!$scope.LifeEngageProduct.isInsured) && (payerData[3].details == 'Yes' || payerData[4].details == 'Yes') && (!$scope.isPayerLifeStyleAvailiablity)) {
                        $scope.eAppParentobj.nextPage = "3,2,'PolicyHolderQuestionnaire',true,'PolicyHolderQuestionnaire','eAppProposerQuestionnaire'";
                        $rootScope.editPageNavigation = ['IllnessTreatmentHistoryPayer', 1];
                    } else if ($rootScope.selectedPage == 'FatcaQuestionnaire') {
                        $scope.LifeEngageProduct.Proposer.Questionnaire = angular.copy(EappVariables.getEappModel().Proposer.Questionnaire);
                    } else if ($scope.secId == "additionalInformationPayer") {
                        $scope.LifeEngageProduct.Payer.Questionnaire = angular.copy(EappVariables.getEappModel().Payer.Questionnaire);
                    }
                    // 	else if($scope.LifeEngageProduct.isInsured &&  insuredData[5].details=='Yes' && insuredData[0].details!='Yes'
                    // && insuredData[1].details!='Yes' && insuredData[3].details!='Yes'  && insuredData[4].details!='Yes'  && insuredData[2].details!='Yes'  ){
                    // 	$scope.eAppParentobj.nextPage = "3,3,'eAppFatcaQuestionnaireJson',true,'FatcaQuestionnaire',''";

                    // }
                    else if (!$scope.LifeEngageProduct.isInsured && !$scope.LifeEngageProduct.isPayer && $scope.LifeEngageProduct.isFatca) {

                        $scope.eAppParentobj.nextPage = "3,3,'eAppFatcaQuestionnaireJson',true,'FatcaQuestionnaire',''";

                    } else if ($scope.LifeEngageProduct.isFatca) {

                        $scope.eAppParentobj.nextPage = "3,3,'eAppFatcaQuestionnaireJson',true,'FatcaQuestionnaire',''";

                    } else {
                        $scope.eAppParentobj.nextPage = "4,1,'SPAJsummary',true,'SPAJsummary',''";
                    }

                    // if(!$scope.LifeEngageProduct.isInsured && !$scope.LifeEngageProduct.isPayer && $scope.eAppParentobj.CurrentPage  =='eappshortquestionnaire')
                    // {						
                    // 	$scope.eAppParentobj.nextPage = "4,1,'SPAJsummary',true,'SPAJsummary',''";
                    // }	
                }
                
                if ($rootScope.selectedPage == 'FatcaQuestionnaire') {
                      $scope.LifeEngageProduct.Proposer.Questionnaire = angular.copy(EappVariables.getEappModel().Proposer.Questionnaire);
                }
                if($rootScope.selectedPage == 'PolicyHolderQuestionnaire'){
                      $scope.LifeEngageProduct.Payer.Questionnaire = angular.copy(EappVariables.getEappModel().Payer.Questionnaire);
                }
                if($rootScope.selectedPage == "HealthDetailsQuestionnaire"){

                       $scope.LifeEngageProduct.Insured.Questionnaire = angular.copy(EappVariables.getEappModel().Insured.Questionnaire);
                       $scope.LifeEngageProduct.PreviouspolicyHistory = angular.copy(EappVariables.getEappModel().PreviouspolicyHistory);
                }

                $scope.restrictnavigation = true;
                nextPage = ($scope.eAppParentobj.nextPage).split(",");
                index = parseInt(nextPage[0]);
                subindex = parseInt(nextPage[1]);
                tabid = nextPage[2].split("'").join('');
                selectedTab = nextPage[4].split("'").join('');

                if ($scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer == "Yes" && index == 1 && subindex == 2) {

                    $scope.LifeEngageProduct.Payer = angular.copy($scope.LifeEngageProduct.Insured);
                    if ($scope.LifeEngageProduct.Payer.Questionnaire != undefined || $scope.LifeEngageProduct.Payer.Questionnaire.AdditionalQuestioniare != undefined) {
                        $scope.LifeEngageProduct.Payer.Questionnaire.AdditionalQuestioniare.ShortQuestions = GLI_EappVariables.PayerQuestionnaire.AdditionalQuestioniare.ShortQuestions;
                    }

                    tabid = "BeneficiarySubTab";
                    selectedTab = "BeneficiarySubTab";
                    subindex = 7;
                    //index, subindex,id, isInnerTab, selectedTab, json
                }

                isInnerTab = $scope.stringToBool(nextPage[3]);


                json = nextPage[5].split("'").join('');
            }

            if ($scope.eAppParentobj.CurrentPage == "MainInsuredQuestionnaire" || $scope.eAppParentobj.CurrentPage == "eappshortquestionnaire" || $scope.eAppParentobj.CurrentPage == "PolicyHolderQuestionnaire") {

                //Insured
                if (tabid == "MainInsuredQuestionnaire") {
                    if ($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[0].details == 'Yes') {
                        $rootScope.yesNoValidationChkOnSave(['Insured', 'LifestyleDetails', 'Yes', [],[], 0, 7], []);
                    } else {
                        $rootScope.yesNoValidationChkOnSave(['Insured', 'LifestyleDetails', 'No', [],[], 0, 7], []);
                    }

                    if ($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[1].details == 'Yes') {
                        $rootScope.yesNoValidationChkOnSave(['Insured', 'LifestyleDetails', 'Yes', [],[], 11, 13, 18, 23], []);
                    } else {
                        $rootScope.yesNoValidationChkOnSave(['Insured', 'LifestyleDetails', 'No', [],[], 11, 13, 18, 23], []);
                    }

                    if ($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[7].details == 'Yes') {
                        $rootScope.yesNoValidationChkOnSave(['Insured', 'LifestyleDetails', 'Yes', [],[], 28], []);
                    } else {
                        $rootScope.yesNoValidationChkOnSave(['Insured', 'LifestyleDetails', 'No', [],[], 28], []);
                    }

                    if ($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[2].details == 'Yes') {
                        $rootScope.yesNoValidationChkOnSave(['Insured', 'LifestyleDetails', 'Yes', [],[], 32, 36], []);
                    } else {
                        $rootScope.yesNoValidationChkOnSave(['Insured', 'LifestyleDetails', 'No', [],[], 32, 36], []);
                    }

                    if ($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[4].details == 'Yes') {
                        $rootScope.yesNoValidationChkOnSave(['Insured', 'Other', 'Yes', [],[], 273, 275, 281], []);
                    } else {
                        $rootScope.yesNoValidationChkOnSave(['Insured', 'Other', 'No', [],[], 273, 275, 281], []);
                    }

                    if ($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[3].details == 'Yes') {
                        $rootScope.yesNoValidationChkOnSave(['Insured', 'Other', 'Yes', ['Yes', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14], ['Insured',1,7,25,51,77,91,105,127,149,159,185,215,225,235],0, 6, 24, 50, 76, 90, 104, 126, 148, 158, 184, 214, 224, 234, 248, 253, 258, 263, 268, 287, 293], ['Insured', 'HealthDetails', 'Yes', ['Yes', 15, 16, 17, 18], ['Insured',1,33,70,107], 0, 32, 69, 106]);
                    } else {
                        $rootScope.yesNoValidationChkOnSave(['Insured', 'Other', 'No', ['No', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],[], 0, 6, 24, 50, 76, 90, 104, 126, 148, 158, 184, 214, 224, 234, 248, 253, 258, 263, 268, 287, 293], ['Insured', 'HealthDetails', 'No', ['No', 15, 16, 17, 18], [], 0, 32, 69, 106]);
                    }
                }

                //Payer
                if (tabid == 'PolicyHolderQuestionnaire') {
                    if ($scope.LifeEngageProduct.Payer.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[0].details == 'Yes') {
                        $rootScope.yesNoValidationChkOnSave(['Payer', 'LifestyleDetails', 'Yes', [], [],0, 7], []);
                    } else {
                        $rootScope.yesNoValidationChkOnSave(['Payer', 'LifestyleDetails', 'No', [], [],0, 7], []);
                    }

                    if ($scope.LifeEngageProduct.Payer.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[1].details == 'Yes') {
                        $rootScope.yesNoValidationChkOnSave(['Payer', 'LifestyleDetails', 'Yes', [], [],11, 13, 18, 23], []);
                    } else {
                        $rootScope.yesNoValidationChkOnSave(['Payer', 'LifestyleDetails', 'No', [], [],11, 13, 18, 23], []);
                    }

                    if ($scope.LifeEngageProduct.Payer.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[7].details == 'Yes') {
                        $rootScope.yesNoValidationChkOnSave(['Payer', 'LifestyleDetails', 'Yes', [],[], 28], []);
                    } else {
                        $rootScope.yesNoValidationChkOnSave(['Payer', 'LifestyleDetails', 'No', [], [],28], []);
                    }

                    if ($scope.LifeEngageProduct.Payer.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[2].details == 'Yes') {
                        $rootScope.yesNoValidationChkOnSave(['Payer', 'LifestyleDetails', 'Yes', [],[], 32, 36], []);
                    } else {
                        $rootScope.yesNoValidationChkOnSave(['Payer', 'LifestyleDetails', 'No', [],[], 32, 36], []);
                    }

                    if ($scope.LifeEngageProduct.Payer.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[4].details == 'Yes') {
                        $rootScope.yesNoValidationChkOnSave(['Payer', 'Other', 'Yes', [], [],273, 275, 281], []);
                    } else {
                        $rootScope.yesNoValidationChkOnSave(['Payer', 'Other', 'No', [],[], 273, 275, 281], []);
                    }

                    if ($scope.LifeEngageProduct.Payer.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[3].details == 'Yes') {
                        $rootScope.yesNoValidationChkOnSave(['Payer', 'Other', 'Yes', ['Yes', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14], ['Payer',1,7,25,51,77,91,105,127,149,159,185,215,225,235], 0, 6, 24, 50, 76, 90, 104, 126, 148, 158, 184, 214, 224, 234, 248, 253, 258, 263, 268, 287, 293], []);
                    } else {
                        $rootScope.yesNoValidationChkOnSave(['Payer', 'Other', 'No', ['No', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14], [], 0, 6, 24, 50, 76, 90, 104, 126, 148, 158, 184, 214, 224, 234, 248, 253, 258, 263, 268, 287, 293], []);
                    }
                }
            }

            // Optimize the code for performance improvement - Begin
            // Delete all questionaire from the scope model and keep only the required questuionaire model as a single object
            if ((rootConfig.isDeviceMobile)) {

                EappVariables.QuestionModel = "";
                if ($scope.eAppParentobj.nextPage && tabid == "MainInsuredQuestionnaire") {

                    EappVariables.QuestionModel = angular.copy($scope.LifeEngageProduct.Insured.Questionnaire);
                   // delete $scope.LifeEngageProduct.Insured["Questionnaire"];

                } else if ($scope.eAppParentobj.nextPage && tabid == "PolicyHolderQuestionnaire") {
                    EappVariables.QuestionModel = angular.copy($scope.LifeEngageProduct.Payer.Questionnaire);
                   // delete $scope.LifeEngageProduct.Payer["Questionnaire"];
                } else if ($scope.eAppParentobj.nextPage && tabid == "eAppFatcaQuestionnaireJson") {
                    EappVariables.QuestionModel = angular.copy($scope.LifeEngageProduct.Proposer.Questionnaire);
                }
                // In summary need not delete the scope data in questionnaire
                if (index != undefined && index != "4" && index != 4 && index != "5" && index != 5 && index != "6" && index != 6) {
                    if ($scope.LifeEngageProduct.Insured.Questionnaire !== undefined && $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare !== undefined) {
                        $scope.additionalQuestions = angular.copy($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.Questions);

                        if ((index == 1 || index == "1") && (subindex == 2 || subindex == "2")) {
                            //$scope.LifeEngageProduct.Insured.Questionnaire = {};
                            //$scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare = {};
                            $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.Questions = {};
                            $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.Questions = angular.copy($scope.additionalQuestions);
                           // delete $scope.additionalQuestions;
                           
                        if($scope.eAppParentobj.CurrentPage == 'eappshortquestionnaire' || $scope.eAppParentobj.CurrentPage == 'MainInsuredQuestionnaire' ||$scope.eAppParentobj.CurrentPage == 'PolicyHolderQuestionnaire'||$scope.eAppParentobj.CurrentPage=='eAppFatcaQuestionnaireJson') {
                        EappVariables.setEappModel($scope.LifeEngageProduct);
                        $rootScope.transactionId = data;
                        EappVariables.EappKeys.Id = data;
                        $scope.isNotAutoSave = false;
                        $rootScope.showHideLoadingImage(false);
                        if ($scope.eAppParentobj.nextPage && $scope.eAppParentobj.nextPage != "" && $scope.restrictnavigation) {
                        $scope.paintView(index, subindex, tabid, isInnerTab, selectedTab, json);}
                        }
                        
                         else
                        {
                            $scope.EditAdditionalInsuredAfterSave(index, subindex, tabid, isInnerTab, selectedTab, json, data);
                        }
                    }
                        
                        
                        
                         else {

                        if($scope.eAppParentobj.CurrentPage == 'eappshortquestionnaire' ||  $scope.eAppParentobj.CurrentPage == 'MainInsuredQuestionnaire' ||  $scope.eAppParentobj.CurrentPage == 'PolicyHolderQuestionnaire'||$scope.eAppParentobj.CurrentPage=='eAppFatcaQuestionnaireJson' ) {
                        EappVariables.setEappModel($scope.LifeEngageProduct);
                        $rootScope.transactionId = data;
                        EappVariables.EappKeys.Id = data;
                        $scope.isNotAutoSave = false;
                        $rootScope.showHideLoadingImage(false);
                        if ($scope.eAppParentobj.nextPage && $scope.eAppParentobj.nextPage != "" && $scope.restrictnavigation) {
                        $scope.paintView(index, subindex, tabid, isInnerTab, selectedTab, json);}
                        }
                        else{
                            $scope.EditAdditionalInsuredAfterSave(index, subindex, tabid, isInnerTab, selectedTab, json, data);
                        }
                        }
                    } else {
                        if($scope.eAppParentobj.CurrentPage == 'eappshortquestionnaire' ||  $scope.eAppParentobj.CurrentPage == 'MainInsuredQuestionnaire' ||  $scope.eAppParentobj.CurrentPage == 'PolicyHolderQuestionnaire'||$scope.eAppParentobj.CurrentPage=='eAppFatcaQuestionnaireJson') {
                        EappVariables.setEappModel($scope.LifeEngageProduct);
                        $rootScope.transactionId = data;
                        EappVariables.EappKeys.Id = data;
                        $scope.isNotAutoSave = false;
                        $rootScope.showHideLoadingImage(false);
                        if ($scope.eAppParentobj.nextPage && $scope.eAppParentobj.nextPage != "" && $scope.restrictnavigation) {
                        $scope.paintView(index, subindex, tabid, isInnerTab, selectedTab, json);}
                    }
                    else
                    {
                        $scope.EditAdditionalInsuredAfterSave(index, subindex, tabid, isInnerTab, selectedTab, json, data);
                    }
                    }
                } else {
                    EappVariables.setEappModel($scope.LifeEngageProduct);
                    $rootScope.transactionId = data;
                    EappVariables.EappKeys.Id = data;
                    $scope.isNotAutoSave = false;
                    $rootScope.showHideLoadingImage(false);
                    if ($scope.eAppParentobj.nextPage && $scope.eAppParentobj.nextPage != "" && $scope.restrictnavigation) {
                        $scope.paintView(index, subindex, tabid, isInnerTab, selectedTab, json);
                    }
                }
            } else {
                $scope.isNotAutoSave = false;
                $rootScope.showHideLoadingImage(false);
                if ($scope.eAppParentobj.nextPage && $scope.eAppParentobj.nextPage != "" && $scope.restrictnavigation) {                 
                     $scope.paintView(index, subindex, tabid, isInnerTab, selectedTab, json);           
				}
            }
        }
    };

    $scope.EditAdditionalInsuredAfterSave = function(index, subindex, tabid, isInnerTab, selectedTab, json, data) {
        EappVariables.setEappModel($scope.LifeEngageProduct);
        $rootScope.transactionId = data;
        if ((rootConfig.isDeviceMobile)) {
            EappVariables.EappKeys.Id = data;
        } else {
            EappVariables.EappKeys.Id = data.Key1;
        }

        $scope.isNotAutoSave = false;
        $rootScope.showHideLoadingImage(false);
        if ($scope.eAppParentobj.nextPage && $scope.eAppParentobj.nextPage != "") {
            $scope.paintView(index, subindex, tabid, isInnerTab, selectedTab, json);

            if (tabid == "MainInsuredQuestionnaire") {
                $scope.secId = 'LifeStyleQuestions';
                $scope.tabActive = "LifeStyleQuestions-0";
            } else if (tabid == "PolicyHolderQuestionnaire") {
                $scope.secId = 'LifeStyleQuestionsPayer';
                $scope.tabActive = "LifeStyleQuestionsPayer-0";
            }
        }

        $scope.disableProceedButtonCommom = false;
        var isDisabledDeclarationAndAuth = $("#summaryProceedButton").is(':disabled');
        if (isDisabledDeclarationAndAuth) {
            $("#summaryProceedButton").removeAttr('disabled');
        }
        var isDisabledACR = $("#agentSummaryButton").is(':disabled');
        if (isDisabledACR) {
            $("#agentSummaryButton").removeAttr('disabled');
        }
    }

    $scope.confirmSubmitDocuments = function() {

        //$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"),translateMessages($translate,"eapp.confirmDocument"),translateMessages($translate,"fna.ok"),$scope.submitDocuments);
        $scope.lePopupCtrl.showWarning(
            translateMessages($translate,
                "lifeEngage"),
            translateMessages($translate,
                "eapp.confirmDocument"),
            translateMessages($translate,
                "Yes"), $scope.submitDocuments, translateMessages($translate,
                "No"), $scope.cancelBtnAction)

    }

    /* Submitting Documents */
    $scope.submitDocuments = function() {
        $('button#submitBtn-DocUpload').attr("disabled", "disabled");
        $('button#paymentButtonDisable').attr("disabled", "disabled");
        $('button#ProceedPaymentSummaryButton').attr("disabled", "disabled");
        if (rootConfig.isDeviceMobile) {
            $scope.syncProgress = 0;
            $scope.showSycnPopup();
            $scope.isSycnCloseEnabled = false;
        }
        var agentDetail = UserDetailsService.getUserDetailsModel();
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
        if (EappVariables.EappKeys.Key7 == "Memo") {
            $('button#submitBtn-DocUpload').removeAttr("disabled");
            $rootScope.showHideLoadingImage(false);
            $scope.hideSyncPopup();
            $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp.resubmitValidationMessage"), translateMessages($translate, "fna.ok"), $scope.okClick);
        } else if (EappVariables.EappKeys.Key7 == "PendingUploaded") {
            if ((!(rootConfig.isDeviceMobile)) || ((rootConfig.isDeviceMobile) && checkConnection())) {
                $scope.documentResubmission();
            } else {
                $('button#submitBtn-DocUpload').removeAttr("disabled");
                $scope.hideSyncPopup();
                $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "agentIsOnline"), translateMessages($translate, "fna.ok"), $scope.okClick);
                $rootScope.showHideLoadingImage(false, "");
            }
        } else if (EappVariables.EappKeys.Key15 != "Submitted") {
            GLI_EappVariables.memoCount = "0";
            $scope.initialDocumentSubmission();
        }
    }

    $scope.submitDocumentsInitially = function() {
        EappVariables.EappKeys.Key21=(parseInt(EappVariables.EappKeys.Key21)===0 || EappVariables.EappKeys.Key21==="")?$scope.LifeEngageProduct.ApplicationNo:EappVariables.EappKeys.Key21;
        PersistenceMapping.clearTransactionKeys();
        var transactionObj = PersistenceMapping.mapScopeToPersistence(EappVariables.EappModel);

        AgentService.retrieveAgentProfile(transactionObj, $scope.onRetrieveAgentProfileSuccess, $scope.onRetrieveAgentProfileError);
    }

    $scope.initialDocumentSubmission = function() {
        if ((!(rootConfig.isDeviceMobile)) || ((rootConfig.isDeviceMobile) && checkConnection())) {
            $scope.submitDocumentsInitially();
        } else {
            $('button#submitBtn-DocUpload').removeAttr("disabled");
            $scope.hideSyncPopup();
            $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "agentIsOnline"), translateMessages($translate, "fna.ok"), $scope.okClick);
            $rootScope.showHideLoadingImage(false, "");
        }
    };
    $scope.getKeyfromMainKey = function(mainKey, successcallBack) {
        var key = [];
        var illustrationStatus = "";
        if (!isNaN(mainKey)) {
            key.push(mainKey);
        } else {
            newElement = mainKey.split("##");
            key.push(newElement[0]);
            if (newElement.length > 1) {
                illustrationStatus = newElement[1];
            }
        }
        successcallBack(key, illustrationStatus);
    }
    $scope.syncRelatedTransactions = function(relatedTrans, successCallbackSync) {
        var illustrationId = "";
        var illustrationStatus = "";
        var transactionsToSync = [];

        if (relatedTrans.hasOwnProperty('LMS')) {
            transactionsToSync.push({
                "Key": 'LMS',
                "Value": relatedTrans.LMS
            });
            delete relatedTrans.LMS;
        }

        if (relatedTrans.hasOwnProperty('FNA')) {
            transactionsToSync.push({
                "Key": 'FNA',
                "Value": relatedTrans.FNA
            });
            delete relatedTrans.FNA;
        }

        angular.forEach(relatedTrans, function(mainKey, value) {
            var splitIndex = 0;
            $scope.getKeyfromMainKey(mainKey, function(key, illustrationStatus) {
                if (key.length > 0) {
                    if (value == "illustration") {
                        key[0] = parseInt(key[0]);
                        illustrationId = key[0];
                    }
                    transactionsToSync.push({
                        "Key": value,
                        "Value": key[0]
                    });
                }
                if (Object.keys(relatedTrans)[Object.keys(relatedTrans).length - 1] == value) {
                    if (illustrationStatus && illustrationStatus == "REJECTED") {
                        $rootScope.showHideLoadingImage(false, "");
                        $scope.hideSyncPopup();
                        $('button#submitBtn-DocUpload').removeAttr("disabled");
                        var errMsg = "illustrationRejectedErrorDuringSync";
                        $rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, errMsg), translateMessages($translate, "fna.ok"), $scope.okClick);
                    } else {
                        if (transactionsToSync.length != 0) {
                            GLI_EappService.individualSync($scope, EappVariables, transactionsToSync, function() {
                                successCallbackSync(illustrationId);
                            }, function(data) {
                                $scope.documentError(data);
                            }, $scope.syncProgressCallback, $scope.syncModuleProgress);
                        }
                    }
                }
            });
        });
    };

    $scope.updateTransactionAfterSync = function(id, successCallBack) {
        EappVariables.EappKeys.Key15 = "Submitted";
        EappVariables.eAppProceedingDate = UtilityService.getFormattedDate();
        EappService.saveTransaction(function() {
            successCallBack();
        }, function(error) {
            $rootScope.showHideLoadingImage(false, "");
        });
    }

    $scope.flowAfterInsuredAgeValidation = function() {
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
        $scope.proceedAfterInsuredAgeValidation($scope.agentDataRetrieved);
    }

    $scope.onRetrieveAgentProfileSuccess = function(data) {
        $scope.agentDataRetrieved = data;
        if (data.AgentDetails.status == rootConfig.AgentStatusForGAO) {
            $scope.flowAfterInsuredAgeValidation();
            /* var diff = $scope.updateAge($scope.LifeEngageProduct.Insured.BasicDetails.dob);
            // To Do
            if(diff != $scope.LifeEngageProduct.Insured.BasicDetails.age){
            	$('button#submitBtn-DocUpload').removeAttr("disabled");
            	$scope.hideSyncPopup();
            	$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"),translateMessages($translate,"changedInsuredAge"),translateMessages($translate,"fna.ok"),$scope.flowAfterInsuredAgeValidation);
            	$rootScope.showHideLoadingImage(false, "");
            }else{
            		$scope.flowAfterInsuredAgeValidation();
            } */
        }

        //Condition to check if the Agent status is expired or suspended
        else if (data.AgentDetails.status == 'TERMINATE' || data.AgentDetails.status == 'SUSPEND') {
            $('button#submitBtn-DocUpload').removeAttr("disabled");
            $scope.hideSyncPopup();
            $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "expiredAgentProfileRetrieveErrorMsg"), translateMessages($translate, "fna.ok"), $scope.okClick);
            $rootScope.showHideLoadingImage(false, "");
        } else {
            $('button#submitBtn-DocUpload').removeAttr("disabled");
            $scope.hideSyncPopup();
            $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "notActiveMsg"), translateMessages($translate, "fna.ok"), $scope.okClick);
            $rootScope.showHideLoadingImage(false, "");
        }
    };
    //Call to sync whether related LMS and BI is Synced
    $scope.proceedAfterInsuredAgeValidation = function(data) {

        $rootScope.showHideLoadingImage(true, "");
        PersistenceMapping.clearTransactionKeys();
        EappVariables.EappKeys.Key18 = "true";
        EappVariables.EappKeys.Key15 = "Submitted";
        EappService.mapKeysforPersistence();
        PersistenceMapping.dataIdentifyFlag = false;
        PersistenceMapping.Type = "eApp";
        var source = (rootConfig.isDeviceMobile != "false") ? "TABLET" : "DESKTOP";
        var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.LifeEngageProduct);
        transactionObj.TransactionData.source = source;
        if ($scope.LifeEngageProduct.ApplicationNo !== undefined && $scope.LifeEngageProduct.ApplicationNo !== "" && $scope.LifeEngageProduct.ApplicationNo !== 0) {
            EappVariables.EappKeys.Key21 = $scope.LifeEngageProduct.ApplicationNo;
        }        
        // Email
        var emailObj = {};
        emailObj.toMailIds = transactionObj.TransactionData.Insured.ContactDetails.emailId;
        emailObj.ccMailIds = data.AgentDetails.emailId;
        emailObj.mailSubject = "";
        //var selectedLanguage = (localStorage["locale"] == "in_ID") ? "in": "en";
        //Selecting the Vietnam language from config file as per request from GVL
        emailObj.language = rootConfig.GVLPDFLanguage;
        emailObj.leadName = transactionObj.TransactionData.Insured.BasicDetails.firstName + " " + transactionObj.TransactionData.Insured.BasicDetails.lastName;
        EappVariables.EappModel.Email = emailObj;
        if ((rootConfig.isDeviceMobile)) {
            // EappService.saveTransaction(function()
            // {
            GLI_DataService.getRelatedTransactionsToSync(transactionObj.Key1, transactionObj.Key2, transactionObj.Key3, function(relatedTrans) {
                if (relatedTrans && Object.keys(relatedTrans).length > 0) {
                    $scope.syncRelatedTransactions(relatedTrans, function(key3) {
                        if (key3 != "" && EappVariables.EappKeys.Key3 != "") {
                            // call to getKey4ForDoc to get Key3 since it is not available as response from individualsync
                            DataService.getRelatedBIForEapp(EappVariables.EappKeys.Key3, function(illustrationData) {
                                if (illustrationData && illustrationData.length > 0 && illustrationData[0].Key3 && illustrationData[0].Key3 != "") {
                                    EappVariables.EappKeys.Key24 = illustrationData[0].Key3;
                                    /*if( illustrationData[0].Key15 && illustrationData[0].Key15 != "Confirmed"){
                                    	$scope.hideSyncPopup();
                                    	$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"),translateMessages($translate,"illustrator.expiryDateMessage"),translateMessages($translate,"fna.ok"),$scope.okClick);
                                    	$rootScope.showHideLoadingImage(false, "");
                                    }else{*/
                                    EappVariables.eAppProceedingDate = UtilityService.getFormattedDate();
                                    EappService.saveTransaction(function() {
                                        $scope.triggerToIndividualSync(transactionObj.Id);
                                    });
                                    //}
                                }
                            }, function() {});
                        } else {
                            $scope.updateTransactionAfterSync(transactionObj.Id, function() {
                                $scope.triggerToIndividualSync(transactionObj.Id);
                            });
                        }
                    });
                } else {
                    if (EappVariables.EappKeys.Key3 != "") {
                        DataService.getRelatedBIForEapp(EappVariables.EappKeys.Key3, function(illustrationData) {
                            /*if(illustrationData && illustrationData.length > 0 && illustrationData[0].Key15 && illustrationData[0].Key15 != "Confirmed"){
											 $rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"),translateMessages($translate,"illustrator.expiryDateMessage"),translateMessages($translate,"fna.ok"),$scope.okClick);
											 $rootScope.showHideLoadingImage(false, "");
											 $scope.hideSyncPopup();
								 }else{*/
                            if (illustrationData[0].Key3 && illustrationData[0].Key3 != "" && EappVariables.EappKeys.Key24 == "") {
                                EappVariables.EappKeys.Key24 = illustrationData[0].Key3;
                            }
                            $scope.updateTransactionAfterSync(transactionObj.Id, function() {
                                $scope.triggerToIndividualSync(transactionObj.Id);
                            });
                            //}
                        });
                    } else {
                        $scope.updateTransactionAfterSync(transactionObj.Id, function() {
                            $scope.triggerToIndividualSync(transactionObj.Id);
                        });
                    }
                }
            }, function(error) {
                $('button#submitBtn-DocUpload').removeAttr("disabled");
                $scope.hideSyncPopup();
                $scope.errorCallback();
            });
        } else {
            EappVariables.EappKeys.Key15 = "Submitted";
            EappVariables.eAppProceedingDate = UtilityService.getFormattedDate();
            EappService.saveTransaction(function() {
            /*    var transactionObj = PersistenceMapping.mapScopeToPersistence(EappVariables.EappModel);
                transactionObj.Key4 = EappVariables.EappKeys.Key4;
                if ($scope.isRDSUser) {
                	transactionObj.Key18 = "";
                } else {
                transactionObj.Key18 = "true";
                //}
                transactionObj.Key21 = EappVariables.EappKeys.Key21;
                transactionObj.Key24 = EappVariables.EappKeys.Key24;
                transactionObj.Type = "eApp";
                EappVariables.EappKeys.Key15 = "Submitted";
                transactionObj.Key15 = "Submitted";*/
            	
            	PersistenceMapping.clearTransactionKeys();		
        		EappService.mapKeysforPersistence();		
        		var transactionObj = PersistenceMapping.mapScopeToPersistence(EappVariables.EappModel);
        		if(EappVariables.EappKeys.Id && EappVariables.EappKeys.Id !=0 && EappVariables.EappKeys.Id != ""){
        			transactionObj.Id = EappVariables.EappKeys.Id;
        		} 
        		if(transactionObj.Key15==""||transactionObj.Key15==rootConfig.eAppLandingScrnStatus)
        			transactionObj.Key30="0";
        		else
        			transactionObj.Key30=GLI_EappVariables.memoCount;
        		if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
        			var key11Data=transactionObj.Key11;
        			transactionObj.Key11=GLI_EappVariables.agentForGAO;
        			if(transactionObj.Key15!=null && transactionObj.Key15!="Submitted") {
        				transactionObj.Key35="";
        				transactionObj.Key36="";
        				transactionObj.Key37="";
        				transactionObj.Key38="";
        			}
                    /* Changes done for eApp report starts*/
                    if(transactionObj.Key15!=null && transactionObj.Key15=="Submitted") {
                        transactionObj.Key32=key11Data;
                        transactionObj.Key33=UserDetailsService.getAgentRetrieveData().AgentDetails.agentType;
                    }
                /* Changes done for eApp report ends*/
        		}else{
        			transactionObj.Key36=(typeof GLI_EappVariables.GAOId!="undefined")?GLI_EappVariables.GAOId:"";
        			transactionObj.Key37=(typeof GLI_EappVariables.agentType!="undefined")?GLI_EappVariables.agentType:"";
        			transactionObj.Key38=(typeof GLI_EappVariables.agentNameForGAO!="undefined")?GLI_EappVariables.agentNameForGAO:"";
        			transactionObj.Key35=(typeof GLI_EappVariables.GAOOfficeCode!="undefined")?GLI_EappVariables.GAOOfficeCode:"";
        		}
        		var valCheck = transactionObj.Key11;
        		var isNum = /^\d+$/.test(valCheck);
        		if(transactionObj.Key11!=null) {
        			if(transactionObj.Key11 == transactionObj.Key36 || isNum==false) {
        				transactionObj.Key11=transactionObj.Key4.substring(0,8);
        			}
        		}
        		transactionObj.Key15 = "Submitted";
        		if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
        		transactionObj.Key39 = "byWeb";
        		}
                GLI_DataService.generateBPMEmailPDF(transactionObj, function(data) {
                    if (data == 200) {
                        EappVariables.EappKeys.Key7 = "Submitted";
                    }
                    $rootScope.showHideLoadingImage(false, "");
                    $scope.hideSyncPopup();
                    /*if($scope.isRDSUser){
                    	$rootScope.lePopupCtrl.showSuccess(translateMessages($translate,"lifeEngage"),translateMessages($translate,"bpmSuccessTrigger"),translateMessages($translate,"fna.ok"),$scope.confirmClick);
                    }else{*/
                    $rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "bpmSuccessAndEmailTrigger"), translateMessages($translate, "fna.ok"), $scope.confirmClick);
                    //}
                }, function(error) {
                    $('button#submitBtn-DocUpload').removeAttr("disabled");
                    $scope.hideSyncPopup();
                    $scope.errorCallback();
                });
            }, function(error) {
                $('button#submitBtn-DocUpload').removeAttr("disabled");
                $scope.hideSyncPopup();
                $scope.errorCallback();
            });
        }

    }
    //Call to trigger BPM, Email and PDF
    $scope.callToBPMEmailPdf = function(keyData, successCallback, errorCallback) {
        EappVariables.EappKeys.Key4 = keyData;
        var transactionObj = PersistenceMapping.mapScopeToPersistence(EappVariables.EappModel);
        transactionObj.Key4 = keyData;
        transactionObj.Key18 = "true";
        transactionObj.Key21 = EappVariables.EappKeys.Key21;
        transactionObj.Key24 = EappVariables.EappKeys.Key24;
        transactionObj.Type = "eApp";
        transactionObj.Key15 = "Submitted";
        transactionObj.Key22 = UtilityService.getFormattedDate();
        GLI_DataService.generateBPMEmailPDF(transactionObj, function(data) {
            if (data == 200) {
                var dataObj = {};
                dataObj.Key16 = "Synced";
                dataObj.Key15 = "Submitted";
                dataObj.Key7 = "Submitted";
                GLI_DataService.updateTransactionAfterBPMEmailPDF(dataObj, transactionObj.Id, function() {
                    successCallback();
                }, function(error) {
                    errorCallback();
                });
            }
        }, function(error) {
            $('button#submitBtn-DocUpload').removeAttr("disabled");
            $scope.hideSyncPopup();
            errorCallback();
        });
    };

    //function to call IndividualSync
    $scope.triggerToIndividualSync = function(transactionObjId) {
        var transactionsToSync = [];
        transactionsToSync.push({
            "Key": "eApp",
            "Value": transactionObjId
        });
        EappService.individualSync($scope, EappVariables, transactionsToSync, function() {
            DataService.getKey4ForDoc(transactionObjId, 'Key4', function(keyData) {
                $scope.callToBPMEmailPdf(keyData, function() {
                    $scope.hideSyncPopup();
                    $rootScope.showHideLoadingImage(false, "");
                    $rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "bpmSuccessAndEmailTrigger"), translateMessages($translate, "fna.ok"), $scope.confirmClick);
                }, function() {
                    $scope.errorCallback();
                });
            });
        }, function(data) {
            $scope.documentError(data);
        }, $scope.syncProgressCallback, $scope.syncModuleProgress);
    };

    $scope.confirmClick = function() {
        $location.path('MyAccount/eApp/0/0');
        $rootScope.showHideLoadingImage(false, "");
        $scope.refresh();
    };
    $scope.errorCallback = function() {
        $rootScope.showHideLoadingImage(false, "");
        $rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "unexpectErrorCallback"), translateMessages($translate, "fna.ok"), $scope.okClick);
        $scope.refresh();
    }

    $scope.okClick = function() {
        $rootScope.showHideLoadingImage(false, "");
    }

    $scope.documentError = function(transacData) {
        // To do: Need to check if this is required
        /* if(EappVariables.EappKeys.Key7 == "PendingUW")
        	EappVariables.EappKeys.Key7 = "PendingUploaded"; */
        $scope.hideSyncPopup();
        $('button#submitBtn-DocUpload').removeAttr("disabled");
        $rootScope.showHideLoadingImage(false);
        if (transacData && transacData == "docUploadFailed") {
            $rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.docSyncFailedTryLater"), translateMessages($translate, "fna.ok"), $scope.okClick);
        } else if (transacData && transacData.Key16 && transacData.Key16 == "REJECTED" && transacData.Type && transacData.Type != "") {
            var errMsg = transacData.Type + "RejectedErrorDuringSync";
            $rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, errMsg), translateMessages($translate, "fna.ok"), $scope.okClick);
        } else if (transacData && transacData.Key16 && transacData.Key16 == "FAILURE" && transacData.Type && transacData.Type != "") {
            var failureMsg = transacData.Type + "FailureErrorDuringSync";
            $rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, failureMsg), translateMessages($translate, "fna.ok"), $scope.okClick);
        } else {
            $rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.errorDuringSync"), translateMessages($translate, "fna.ok"), $scope.okClick);
        }
    };

    $scope.onRetrieveAgentProfileError = function() {
        $('button#submitBtn-DocUpload').removeAttr("disabled");
        $scope.hideSyncPopup();
        $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.agentProfileRetrieveError"), translateMessages($translate, "fna.ok"), $scope.okClick);
        $rootScope.showHideLoadingImage(false, "");
        $("#summaryProceedButton").removeAttr("disabled");
    };

    $scope.updateAge = function(dob) {
        if (dob != "" && dob != undefined && dob != null) {
            var dobDate = new Date(dob);
            if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
                dobDate = new Date(formatForDateControl(dob).split("-").join("/"));
            }
            var todayDate = new Date();
            if (dobDate > todayDate) {
                $('button#submitBtn-DocUpload').removeAttr("disabled");
                $scope.hideSyncPopup();
                $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "suspendedAgentProfileRetrieveError"), translateMessages($translate, "fna.ok"), $scope.okClick);
                $rootScope.showHideLoadingImage(false, "");
            } else {
                var curd = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate());
                var cald = new Date(dobDate.getFullYear(), dobDate.getMonth(), dobDate.getDate());
                var dife = $scope.ageCalculation(curd, cald);
                var datediff = curd.getTime() - cald.getTime();
                var days = (datediff / (24 * 60 * 60 * 1000)) / 365;
                days = Number(Math.round(days + 'e3') + 'e-3');
                var diff;
                if (dife[1] == "false") {
                    diff = dife[0];
                } else {
                    diff = dife[0] + 1;
                }
                return diff;
            }
        }
    };

    //Calculating age in years, month , days 
    $scope.ageCalculation = function(currDate, dobDate) {
        date1 = new Date(currDate);
        date2 = new Date(dobDate);
        var y1 = date1.getFullYear(),
            m1 = date1.getMonth(),
            d1 = date1.getDate(),
            y2 = date2.getFullYear(),
            m2 = date2.getMonth(),
            d2 = date2.getDate();
        var nextDOB;
        if (m1 < m2) {
            nextDOB = new Date(date2.setYear(date1.getFullYear()));
        } else if ((m1 == m2) && (d1 < d2)) {
            nextDOB = new Date(date2.setYear(date1.getFullYear()));
        } else {
            nextDOB = new Date(date2.setYear(date1.getFullYear() + 1));
        }
        var marginDate = new Date(date1.setMonth(date1.getMonth()));
        if (m1 < m2) {
            y1--;
            m1 += 12;
        }
        if (nextDOB.getTime() > marginDate.getTime()) {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() - birthDate.getFullYear();
            var mnth = today.getMonth() - birthDate.getMonth();
            if (mnth < 0 || (mnth === 0 && today.getDate() < birthDate.getDate())) {
                ageVal--;
            }
            return [ageVal, "false"];
        } else {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() - birthDate.getFullYear();
            var mnth = today.getMonth() - birthDate.getMonth();
            if (mnth < 0 || (mnth === 0 && today.getDate() < birthDate.getDate())) {
                ageVal--;
            }
            return [ageVal, "false"];
        }
    };

    $scope.showSycnPopup = function() { // Show sync popup
        $rootScope.startupPopupDoc = true;
        $scope.syncProgress = 0;
        $scope.refresh();
    };
    $scope.hideSyncPopup = function() { // Hide the sysnc popup
        $rootScope.startupPopupDoc = false;
        $scope.syncProgress = 0;
        $scope.syncMessage = "";
    };

    $scope.syncProgressCallback = function(currentIndex, totalCount) {
        // set the progress percentage and message
        //$scope.syncProgress=progress;
        $rootScope.showHideLoadingImage(false, "");
        $scope.syncMessage = translateMessages($translate, "uploadingDocuments") + currentIndex + translateMessages($translate, "uploadOf") + totalCount;
        if (currentIndex == totalCount) {
            $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
            $scope.hideSyncPopup();
        }
        $scope.refresh();
    };

    $scope.syncModuleProgress = function(syncMessage, progress) {
        // set the progress percentage and message
        $rootScope.showHideLoadingImage(false, "");
        $scope.syncMessage = syncMessage;
        $scope.refresh();
    }
    /*Document Resubmission*/
    $scope.documentResubmission = function() {
        //if(EappVariables.EappKeys.Key7 == "PendingUploaded" || EappVariables.EappKeys.Key7 == "Memo"){
        if ((rootConfig.isDeviceMobile)) {
            PersistenceMapping.clearTransactionKeys();
            EappService.mapKeysforPersistence();
            var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.LifeEngageProduct);
            var transactionsToSync = [];
            transactionsToSync.push({
                "Key": "eApp",
                "Value": transactionObj.Id,
                "syncType": "eApp"
            });
            //GLI_EappService.individualSync($scope, transactionsToSync, $scope.documentSuccess, $scope.documentError);
            EappVariables.EappKeys.Key7 = "PendingUW";
            EappVariables.eAppProceedingDate = UtilityService.getFormattedDate();
            EappService.saveTransaction(function() {
                EappService.individualSync($scope, EappVariables, transactionsToSync, function() {
                    DataService.getKey4ForDoc(transactionObj.Id, 'Key4', function(keyData) {
                        EappVariables.EappKeys.Key4 = keyData;
                        //$scope.callBPMResubmission(transactionObj.Id);
                        $scope.saveOfflineAfterSubmit(transactionObj.Id);
                    });
                }, function(data) {
                    $scope.documentError(data);
                }, $scope.syncProgressCallback, $scope.syncModuleProgress);
            }, function() {});
        } else {
            var transactionObjForMailReq = PersistenceMapping.mapScopeToPersistence({});
            transactionObjForMailReq.Type = "eApp";
            EappVariables.eAppProceedingDate = UtilityService.getFormattedDate();
            EappVariables.EappKeys.Key7 = "PendingUW";
            EappVariables.EappKeys.Key35 = "";
            EappVariables.EappKeys.Key36 = "";
            EappVariables.EappKeys.Key37 = "";
            EappVariables.EappKeys.Key38 = "";
            EappService.saveTransaction(function() {
                $scope.callBPMResubmission();
            }, function(error) {
                $('button#submitBtn-DocUpload').removeAttr("disabled");
                $scope.hideSyncPopup();
                $scope.errorCallback();
                $scope.errorCallback();
            });
        }
        /* }else{
        	$('button#submitBtn-DocUpload').removeAttr("disabled");
        	$scope.hideSyncPopup();
        	$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"),translateMessages($translate,"notUploadedDocForResubmission"),translateMessages($translate,"fna.ok"),$scope.okClick);
        	$rootScope.showHideLoadingImage(false, "");
        } */
    }

    $scope.saveOfflineAfterSubmit = function(transId) {
        EappVariables.eAppProceedingDate = UtilityService.getFormattedDate();
        EappVariables.EappKeys.Key18 = "";
        var dataObj = {};
        dataObj.Key18 = "";
        dataObj.Key15 = "Submitted";
        dataObj.Key16 = "Synced";
        dataObj.Key7 = "PendingUW";
        GLI_DataService.updateTransactionAfterBPMEmailPDF(dataObj, transId, function() {
            $scope.callBPMResubmission();
        }, function(error) {
            errorCallback();
        });
    }

    $scope.documentResubmissionSuccess = function() {
        $rootScope.showHideLoadingImage(false, "");
        $rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "bpmSuccessTrigger"), translateMessages($translate, "fna.ok"),
            function() {
                $location.path('MyAccount/eApp/0/0');
                $rootScope.showHideLoadingImage(false, "");
                $scope.refresh();
            });
    }

    $scope.callBPMResubmission = function(transId) {
        //$rootScope.showHideLoadingImage(false, "");
        var bpmKeyToUpdate = EappVariables.EappKeys.Key4;
        GLI_DataService.callToUpdateBPMUpload(bpmKeyToUpdate, true, function() {
            $scope.documentResubmissionSuccess();
        }, function(error) {
            $scope.hideSyncPopup();
            $('button#submitBtn-DocUpload').removeAttr("disabled");
            $scope.errorCallback();
        });
        //$scope.documentResubmissionSuccess();
    };

    /* 	$scope.paymentChildScope = {};
    	$scope.paynowClicked = function(){
    	$scope.$broadcast('payNowClickedEvent');
    } */

    $scope.$on("$destroy", function() {
        if (this != null && typeof this !== 'undefined') {
            if (this.$$destroyed)
                return;
            while (this.$$childHead) {
                this.$$childHead.$destroy();
            }
            if (this.$broadcast) {
                this.$broadcast('$destroy');
            }
            this.$$destroyed = true;
            //$timeout.cancel( timer );    
        }
    });

    // $scope.secId = 'LifeStyleQuestions';
    // $scope.tabActive = "LifeStyleQuestions-0";

    $scope.popupTabsNav = function(rel, index,isSaveProposal) {
        
        var prevTabindex= $scope.tabActive.split("-")[1];
   if(!isSaveProposal)     
   {
    if(rel=="LifeStyleQuestions" || rel=="IllnessTreatmentHistory"|| rel == "healthDDRider" || rel=="additionalInformation" || rel == 'previousPolicies')
    {
        
              if($('#headerText_'+prevTabindex).hasClass("diableCheckMark")) {
                    return;
                }
                else if($scope.eAppParentobj.errorCount > 0)
                {
                    return;                    
                }
    }

     if(rel=="LifeStyleQuestionsPayer" || rel == "IllnessTreatmentHistoryPayer" || rel=="additionalInformationPayer")   
            {
                $scope.updateErrorCount('PolicyHolderQuestionnaire');
				$scope.refresh();
                if ($("#headerText_payer"+prevTabindex).hasClass("diableCheckMark")){
                        return;
                    }                    
                    else if($scope.eAppParentobj.errorCount > 0){
                    return;                    
                }
            }
        }

             $scope.secId = "";
             $scope.tabActive = "";
             $scope.tabActive = rel + '-' + index;
             $scope.secId = rel;

       if (rel == "healthDDRider" || rel == "IllnessTreatmentHistory" || rel == "IllnessTreatmentHistoryPayer") {
            $scope.errorMsgQuestionnaire(true, false);
        } else {
            $scope.errorMsgQuestionnaire(false, true);
        }


        if (rel == 'previousPolicies') {
            if ($rootScope.checkPrevious != undefined) {
                $rootScope.checkPrevious(true);
            }
        } else {
            if ($rootScope.checkPrevious != undefined) {
                $rootScope.checkPrevious(false);
            }
        }	
       
     
        	if(rel == "LifeStyleQuestionsPayer" || rel == "IllnessTreatmentHistoryPayer" || rel=="additionalInformationPayer"){
             $timeout(function () {
                  
				$scope.updateErrorCount('PolicyHolderQuestionnaire');
				$scope.refresh();

                    if($scope.eAppParentobj.errorCount > 0 )
                    {
                        for(var j=0;j<=2;j++){
                        $('#headertabs_payer'+j).addClass("QuestionnaireCheckbox");
                        }
                    }
                    else
                    {
                          for(var j=0;j<=2;j++){
                          if ($("#headerText_payer"+j).hasClass("diableCheckMark"))
                          $('#headertabs_payer'+j).addClass("QuestionnaireCheckbox");                            
                            else                            
                        $('#headertabs_payer'+j).removeClass("QuestionnaireCheckbox");                            
                    }
                }

			}, 50)
        }

		if(rel == "LifeStyleQuestions" || rel == "healthDDRider" || rel == "IllnessTreatmentHistory" || rel=="additionalInformation"){
            $timeout(function(){
                
					$scope.updateErrorCount('MainInsuredQuestionnaire')
					$scope.refresh();
                    if($scope.eAppParentobj.errorCount > 0)
                    {
                        for(var i=0;i<=4;i++)
                        $('#headertabs_'+i).addClass("QuestionnaireCheckbox");
                        return;
                    }
                      else
                    {
                         for(var i=0;i<=4;i++){
                       if($('#headerText_'+i).hasClass("diableCheckMark")){
                           $('#headertabs_'+i).addClass("QuestionnaireCheckbox");
                      }
                      else{
                        $('#headertabs_'+i).removeClass("QuestionnaireCheckbox");
                      }
                    }
                    }
			},50)
        }
        
             	
     
    };

    $scope.isSectionVisible_tab = [];

    $scope.initailPaintViewDocUpload = function(index, subindex, id, isInnerTab, selectedTab, json) {
        $scope.setDocUploadDirectly = true;
        $scope.paintView(index, subindex, id, isInnerTab, selectedTab, json);
    };

    $scope.errorMsgQuestionnaire = function(flag1, flag2) {

        if ($scope.secId == "healthDDRider") {

            if ($rootScope.HealthYesNoflag && $rootScope.HealthYesNoflag.length > 0) {
                if ($rootScope.HealthYesNoflag[0] == 'Yes') {
                    for (var i = 1; i < $rootScope.HealthYesNoflag.length; i++) {
                        var radioQuestion = "radioQuestion" + $rootScope.HealthYesNoflag[i];
						var optionchk = $scope.questionOptionsCheck($rootScope.checkHealthOptionFlag[0],$rootScope.checkHealthOptionFlag[i],true);
                        if(optionchk)
							$scope[radioQuestion] = flag1;
						else
							$scope[radioQuestion] = flag2;
                    }

                    // IllnessTreatmentHistory fields true			
                    if ($rootScope.illnessYesNoflag && $rootScope.illnessYesNoflag.length > 0) {
                        for (var i = 1; i < $rootScope.illnessYesNoflag.length; i++) {
                            var radioQuestion = "radioQuestion" + $rootScope.illnessYesNoflag[i];
                            $scope[radioQuestion] = flag1;
                        }
                    }

                } else {

                    for (var i = 1; i < $rootScope.HealthYesNoflag.length; i++) {
                        var radioQuestion = "radioQuestion" + $rootScope.HealthYesNoflag[i];
                        $scope[radioQuestion] = flag1;
                    }
                }
            }
        } else if ($scope.secId == "IllnessTreatmentHistory") {

            if ($rootScope.illnessYesNoflag && $rootScope.illnessYesNoflag.length > 0) {
                if ($rootScope.illnessYesNoflag[0] == 'Yes') {
                    for (var i = 1; i < $rootScope.illnessYesNoflag.length; i++) {
                        var radioQuestion = "radioQuestion" + $rootScope.illnessYesNoflag[i];
						var optionchk = $scope.questionOptionsCheck($rootScope.checkIllnessOptionflag[0],$rootScope.checkIllnessOptionflag[i]);
						if(optionchk)
							$scope[radioQuestion] = flag1;
						else
							$scope[radioQuestion] = flag2;
                    }

                    // Making Health fields true			
                    if ($rootScope.HealthYesNoflag && $rootScope.HealthYesNoflag.length > 0) {
                        for (var i = 1; i < $rootScope.HealthYesNoflag.length; i++) {
                            var radioQuestion = "radioQuestion" + $rootScope.HealthYesNoflag[i];
                            $scope[radioQuestion] = flag1;
                        }
                    }

                } else {
                    for (var i = 1; i < $rootScope.illnessYesNoflag.length; i++) {
                        var radioQuestion = "radioQuestion" + $rootScope.illnessYesNoflag[i];
                        $scope[radioQuestion] = flag1;
                    }
                }
            }
        } else {
            if ($rootScope.illnessYesNoflag && $rootScope.illnessYesNoflag.length > 0) {
                for (var i = 1; i < $rootScope.illnessYesNoflag.length; i++) {
                    var radioQuestion = "radioQuestion" + $rootScope.illnessYesNoflag[i];
                    
						if($scope.secId != "" && $scope.secId != undefined && $scope.secId == "IllnessTreatmentHistoryPayer"){
                        var optionchk = $scope.questionOptionsCheck($rootScope.checkIllnessOptionflag[0],$rootScope.checkIllnessOptionflag[i]);
						
						if(optionchk)
							$scope[radioQuestion] = flag1;
						else
							$scope[radioQuestion] = flag2;
						
						}else{
                            $scope[radioQuestion] = flag2;
						}
                }
            }

            if ($rootScope.HealthYesNoflag && $rootScope.HealthYesNoflag.length > 0) {
                for (var i = 1; i < $rootScope.HealthYesNoflag.length; i++) {
                    var radioQuestion = "radioQuestion" + $rootScope.HealthYesNoflag[i];
                    $scope[radioQuestion] = flag2;
                }
            }
        }
    }
	
$rootScope.illnessYesNoflag = []; 
$rootScope.HealthYesNoflag = []; 
$rootScope.checkIllnessOptionflag = []; 
$rootScope.checkHealthOptionFlag = []; 

$rootScope.yesNoValidationChkOnSave = function(qArray1, qArray2){
	
	if(qArray1.length > 0){
		if(qArray1[2] == 'Yes'){
			$rootScope.illnessYesNoflag = qArray1[3];
			$rootScope.checkIllnessOptionflag = qArray1[4];
			for(var t=5; t<qArray1.length; t++){
                if($scope.LifeEngageProduct[qArray1[0]].Questionnaire[qArray1[1]].Questions[qArray1[t]].detailsQuestionInfo  == "")
                {
				$scope.LifeEngageProduct[qArray1[0]].Questionnaire[qArray1[1]].Questions[qArray1[t]].details = "Yes";
                $scope.LifeEngageProduct[qArray1[0]].Questionnaire[qArray1[1]].Questions[qArray1[t]].detailsQuestionInfo  = "Yes";
                }                
			}
		}else{
			$rootScope.illnessYesNoflag = qArray1[3]; 
			$rootScope.checkIllnessOptionflag = qArray1[4];
			for(var t=5; t<qArray1.length; t++){
				$scope.LifeEngageProduct[qArray1[0]].Questionnaire[qArray1[1]].Questions[qArray1[t]].details = "No";   
                $scope.LifeEngageProduct[qArray1[0]].Questionnaire[qArray1[1]].Questions[qArray1[t]].detailsQuestionInfo  = "";
			}
		}	
	}
	
	//HealthDetails
if($rootScope.shortEappHealthRiderAvailable){
    
	if(qArray2.length > 0){
		if(qArray2[2] == 'Yes'){
			$rootScope.HealthYesNoflag = qArray2[3]; 
			$rootScope.checkHealthOptionFlag = qArray2[4]; 
			for(var t=5; t<qArray2.length; t++){
                if($scope.LifeEngageProduct[qArray2[0]].Questionnaire[qArray2[1]].Questions[qArray2[t]].detailsQuestionInfo  =="")
                {
				$scope.LifeEngageProduct[qArray2[0]].Questionnaire[qArray2[1]].Questions[qArray2[t]].details = "Yes";
                $scope.LifeEngageProduct[qArray2[0]].Questionnaire[qArray2[1]].Questions[qArray2[t]].detailsQuestionInfo  = "Yes";
                }
			}
		}else{
			$rootScope.HealthYesNoflag = qArray2[3];
			$rootScope.checkHealthOptionFlag = qArray2[4]; 			
			for(var t=5; t<qArray2.length; t++){
				$scope.LifeEngageProduct[qArray2[0]].Questionnaire[qArray2[1]].Questions[qArray2[t]].details = "No";
			}
		}	
	}
}
	
	EappVariables.getEappModel().Insured.Questionnaire = angular.copy($scope.LifeEngageProduct.Insured.Questionnaire);
	EappVariables.getEappModel().Payer.Questionnaire = angular.copy($scope.LifeEngageProduct.Payer.Questionnaire);
}

$scope.questionOptionsCheck = function(beneficiary,position,isHealth){
	
    if(beneficiary!=undefined && position!=undefined)
    {
        var optionchk=[];
        if(isHealth)
        {
        optionchk = $scope.LifeEngageProduct[beneficiary].Questionnaire.HealthDetails.Questions[position].options;
        }
        else
        {
        optionchk = $scope.LifeEngageProduct[beneficiary].Questionnaire.Other.Questions[position].options;    
        }	    
	
	return optionchk.some(function(ee){ 
		return ee.details == "true" 
	});
    }
}
};