'use strict';
storeApp.controller('ProposerController', ['$rootScope', '$scope', 
                                           'DataService', 'LookupService', 
                                           'DocumentService', 'ProductService', 
                                           '$compile', '$routeParams', '$location', 
                                           '$translate', '$debounce', 'UtilityService', 
                                           'PersistenceMapping', 'AutoSave', 'globalService',
                                           'EappVariables', 'EappService','$timeout',
function($rootScope, $scope, DataService, LookupService, DocumentService,
        ProductService, $compile, $routeParams, $location, $translate, 
        $debounce, UtilityService, PersistenceMapping, AutoSave, globalService, 
        EappVariables, EappService,$timeout) {
    
    $scope.mobnumberPattern = rootConfig.mobnumberPattern;
    $scope.namePattern = rootConfig.namePattern;
    $scope.zipPattern  = rootConfig.zipPattern;
    $scope.numberPattern  = rootConfig.numberPattern;
    $scope.emailPattern  = rootConfig.emailPattern;
    $scope.showErrorCount = true;
    $scope.innertabsVertical = [];
	$scope.innertabsVertical[0] = 'selected';
    // Controller Initialize method
    $scope.Initialize = function() {
        $scope.LifeEngageProduct = EappVariables.getEappModel();
        UtilityService.InitializeErrorDisplay($scope,$rootScope, $translate);
        angular.element('#'+$scope.selectedTabId+"_a").trigger('click');
        $scope.updateErrorCount($scope.selectedTabId,0);
		var model = "LifeEngageProduct";
		if ((rootConfig.autoSave.eApp) && EappVariables.EappKeys.Key15 != 'Final') {
			if(AutoSave.destroyWatch){
				AutoSave.destroyWatch();
			}
			AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
		} else if(AutoSave.destroyWatch){
			AutoSave.destroyWatch();
		}
		
        $scope.IsProposerPermAddSame = function(id) {
            if ($scope.LifeEngageProduct.Proposer.ContactDetails.isPermanentAddressSameAsCurrentAddress) {
                $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress = $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress;
            } else {
                $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress = {};
            }
        };
    }
    $scope.updateErrorCount=function(id,index)
    {
        $rootScope.updateErrorCount(id);
        $scope.selectedTabId=id;
        var proposerDob = $scope.LifeEngageProduct.Proposer.BasicDetails.dob;
        if (LEDate(proposerDob) >= LEDate()) {
            $scope.errorCount = parseInt($scope.errorCount) + 1;
        }
        // TODO change this to one variable
        if( (id=="tabsinner6") && $scope.LifeEngageProduct.Proposer.ContactDetails.methodOfCommunication.isEmail =="No" && $scope.LifeEngageProduct.Proposer.ContactDetails.methodOfCommunication.isSMS =="No" && $scope.LifeEngageProduct.Proposer.ContactDetails.methodOfCommunication.isPost=="No"){
            $scope.errorCount = parseInt($scope.errorCount) + 1;
        }
         for ( var i = 0; i < $scope.innertabsVertical.length; i++) {
									if (i == index) {
										$scope.innertabsVertical[i] = 'selected';
									} else {
										$scope.innertabsVertical[i] = '';
									}
								}
    }
    $scope.showErrorPopup = function(id)
    {
        $rootScope.showErrorPopup($scope.selectedTabId);
        var proposerDob = $scope.LifeEngageProduct.Proposer.BasicDetails.dob;
        if (LEDate(proposerDob) <= LEDate()) {
            var error = {};
            error.message = translateMessages($translate,"proposerDOBFutureDateValidationMessage");
            error.key = 'proposerDOB';
            $scope.errorMessages.push(error);
        }
        // TODO change this to one variable
        if(($scope.selectedTabId=="tabsinner6") && $scope.LifeEngageProduct.Proposer.ContactDetails.methodOfCommunication.isEmail =="No" && $scope.LifeEngageProduct.Proposer.ContactDetails.methodOfCommunication.isSMS =="No" && $scope.LifeEngageProduct.Proposer.ContactDetails.methodOfCommunication.isPost=="No"){
            var error = {};
            error.message = translateMessages($translate,"proposerMethodOfCommunicationRequiredValidationMessage");
            error.key = 'methodOfCommunication1';
            $scope.errorMessages.push(error);
        }
    }
    // ProposerController Load Event and Load Event Handler
   /* $scope.$parent.$on('ProposerDetailsSubTab', function(event, args)
     {
        $scope.selectedTabId = args[0];
        $scope.Initialize();
     });*/
}]);
