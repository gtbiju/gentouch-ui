'use strict';
/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:GLI_QuestionnaireInsuredController
 CreatedDate:23/12/2015
 Description:GLI_QuestionnaireInsuredController 
 */

storeApp.controller('GLI_QuestionnaireInsuredController', GLI_QuestionnaireInsuredController);
GLI_QuestionnaireInsuredController.$inject = ['$rootScope', '$scope', 'DataService', 'LookupService', 'DocumentService', 'ProductService', '$compile', '$routeParams', '$location', '$translate', '$debounce', 'UtilityService', 'PersistenceMapping', 'AutoSave', 'globalService', 'EappVariables', 'EappService', 'GLI_EappVariables', '$timeout', 'GLI_EappService', 'GLI_EvaluateService'];

function GLI_QuestionnaireInsuredController($rootScope, $scope, DataService, LookupService, DocumentService, ProductService, $compile, $routeParams, $location, $translate, $debounce, UtilityService, PersistenceMapping, AutoSave, globalService, EappVariables, EappService, GLI_EappVariables, $timeout, GLI_EappService, GLI_EvaluateService) {
    $rootScope.selectedPage = "HealthDetailsQuestionnaire";
    $scope.selectedTabId = 'MainInsuredQuestionnaire';


    $scope.lifeQuestionFlag = false;
    $scope.IllnessTreatmentHistoryFlag = true;
    $scope.healthDDRiderFlag = true;


    $scope.LifeEngageProduct.LastVisitedUrl = "";
    $scope.eAppParentobj.nextPage = "";
    $scope.eAppParentobj.CurrentPage = "";
    $scope.mobnumberPattern = rootConfig.mobnumberPattern;
    $scope.namePattern = rootConfig.namePattern;
    $scope.zipPattern = rootConfig.zipPattern;
    $scope.numberPattern = rootConfig.numberPattern;
    $scope.emailPattern = rootConfig.emailPattern;
    $scope.eappSpecialCharacters = rootConfig.eappSpecialCharacters;
    $scope.eappSpecialCharactersTextArea = rootConfig.eappSpecialCharactersTextArea;
    $scope.alphanumericWithSixSpecialChara = rootConfig.alphanumericWithSixSpecialChara;
    $scope.twoDecimalPattern = rootConfig.twoDecimalPattern;
    $scope.showErrorCount = true;
    $scope.dynamicErrorMessages = [];
    $scope.dynamicErrorCount = 0;
    $scope.mainInsuredMedcategory = 'NM';
    $scope.addInsured1MedCategory = 'NM';
    $scope.addInsured2MedCategory = 'NM';
    $scope.addInsured3MedCategory = 'NM';
    $scope.addInsured4MedCategory = 'NM';
    $scope.IsPreviouspolicySection = false;
    $scope.IsFamilyHistorySection = false;
    $scope.showQuestion16 = false;
    $scope.IsSubQuestionAvailable=false;
	$scope.RidersInIllustration = [];
	$scope.healthRiderAvailable = false;
    //$rootScope.shorteappsubtab = true;
	$rootScope.healthRiderAvailable=$scope.healthRiderAvailable;   
    $scope.isDecweight=false;
	$scope.alphabetWithSpacePatternIllustration =  rootConfig.alphabetWithSpacePatternIllustration;
	$scope.alphaNumericEappCustom =  rootConfig.alphaNumericEappCustom;	
	$scope.PreviousPolicyQuestionnaire ={
		"PreviouspolicyHistory":[]
	};
	$scope.previousPolicyAddButton = 'false';
	$scope.disableField1 = true;
	$scope.disableField2 = true;
	$scope.Question2dropdown = {
		"id": "",
		"number" : ""
		};
	$scope.Question2textbox = "";
	$scope.policyUniqueNumberCheck = false;

$scope.item={
  "id": "",
  "label": "" 
};

  $scope.items = [{
  id: 1,
  label: '1' 
}, {
  id: 2,
  label: '2'
}, 
{
  id:3,
  label: '3'
}, 
{
  id: 4,
  label: '4'
}, {
  id:5,
  label: '5'
},
 {
  id: 6,
  label: '6'
}, {
  id: 7,
  label: '7'
}, {
  id: 8,
  label: '8'
}, {
  id: 9,
  label: '9'
}
];

    $scope.SubQuestionSubCategoriesVisiblity=false;
    $scope.alphanumericPattern = rootConfig.alphanumericPattern;
    if (!$rootScope.MainInsuredQuestionnaireNoToAll) {
        $rootScope.MainInsuredQuestionnaireNoToAll = "No";
    }
    if ($rootScope.MainInsuredQuestionnaireNoToAll == "No") {
        $scope.NoToAll = "No"
    } else {
        $scope.NoToAll = "Yes";
    }

    var unload = $scope.$parent.$on('HealthDetailsSubTab_unload', function (event) {
        var data = $scope.$parent;
        angular.element(document.querySelector('#MainInsuredQuestionnaire')).empty();
        $scope.$destroy();
        $scope.$parent = data;
        load();
        unload();
    });

    
 
    
    $scope.showDeclaration = function () {
        return true;
    }

    $scope.returnQuestionNo = function (n) {
        return n + ".";
    }
    
    $scope.returnQuestionErrNo = function (n) {
        return n;
    }

    $scope.returnQuestionNoChara = function (n) {
        return "6a.";
    }

    $scope.showQuestion17 = function () {
        if ($scope.LifeEngageProduct.Insured.BasicDetails.gender == 'Female') {
            return true;
        } else {
            return false;
        }
    }
    
    $scope.showIfAdditionalInsured = function () {
        return 'collapsed';
    }

    //To clear text field on clicking 'No' and unchecking the 'No To all' checkbox
    $scope.clearField = function (model) {
        if (model.option == "No") {
            if (model.details) {
                model.details = "";
            }
            if (model.options) {
                model.options = [];
            }
        }
        if (model.option == "Yes") {
            if ($scope.QuestionnaireObj.HealthDetails.Questions[27].option == "Yes") {
                $scope.QuestionnaireObj.HealthDetails.Questions[27].option = "No";
            }
        }
    }

    $scope.noToAll = function () {
        if ($scope.QuestionnaireObj.HealthDetails.Questions[27].option == "Yes") {
            for (var i = 0; i <= 26; i++) {
                $scope.QuestionnaireObj.HealthDetails.Questions[i].option = "No";
            }
            if ($scope.LifeEngageProduct.Insured.BasicDetails.age >= 16) {
                $scope.QuestionnaireObj.HealthDetails.Questions[23].option = "";
            }
            if ($scope.LifeEngageProduct.Insured.BasicDetails.gender == "Male") {
                $scope.QuestionnaireObj.HealthDetails.Questions[24].option = "";
                $scope.QuestionnaireObj.HealthDetails.Questions[25].option = "";
            }
        } else {
            for (var i = 0; i <= 26; i++) {
                $scope.QuestionnaireObj.HealthDetails.Questions[i].option = "";
            }
            $rootScope.MainInsuredQuestionnaireNoToAll = "No";
        }
        $timeout(function () {
            $scope.updateErrorCount('MainInsuredQuestionnaire');
            $scope.refresh();
        }, 50)
    }

    if ($scope.LifeEngageProduct.Insured.BasicDetails.age < 16) {
        $scope.showQuestion16 = true;
    }
    //Controller Initialize method
    $rootScope.hideInsuredLine = true;
	
    function initialize() {
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_eAppFatcaQuestionnaireJson#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_eAppFatcaQuestionnaireJson#tabDetails_1');
        }
        if($rootScope.editPageNavigation!=undefined && $rootScope.editPageNavigation.length>0){
            $scope.popupTabsNav($rootScope.editPageNavigation[0],$rootScope.editPageNavigation[1],true);
            $rootScope.editPageNavigation = undefined;
        }
        $scope.$parent.disableProceedButtonCommom = false;
		$scope.RidersInIllustration = $scope.LifeEngageProduct.Product.RiderDetails;
		for(var i = 0;i<$scope.RidersInIllustration.length;i++){
			if($scope.RidersInIllustration[i].riderPlanName == "HS Extra" 
				|| $scope.RidersInIllustration[i].riderPlanName == "HB(A)"  
				|| $scope.RidersInIllustration[i].riderPlanName == "CI Extra"
				|| $scope.RidersInIllustration[i].riderPlanName == "DD") {
				$scope.healthRiderAvailable = true;
				$rootScope.healthRiderAvailable=$scope.healthRiderAvailable;
                $scope.LifeEngageProduct.Product.policyDetails.hasDDRider=true;
			}
		}
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            //EappVariables.setEappModel($scope.LifeEngageProduct);
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
        }
        $scope.showErrorCount = true;
        $scope.IsPreviouspolicySection = false;
        $scope.IsFamilyHistorySection = false;
        if(!rootConfig.isDeviceMobile){
        	if(EappVariables.getEappModel().Insured.Questionnaire == undefined || EappVariables.getEappModel().Insured.Questionnaire.LifestyleDetails == undefined || EappVariables.getEappModel().Insured.Questionnaire.LifestyleDetails.Questions.length < 43){
        		EappVariables.getEappModel().Insured.Questionnaire = GLI_EappVariables.InsuredQuestionnaire;
        		EappVariables.getEappModel().Payer.Questionnaire = GLI_EappVariables.PayerQuestionnaire;
        		EappVariables.getEappModel().Proposer.Questionnaire = GLI_EappVariables.ProposerQuestionnaire;
        	}
        }
        $scope.LifeEngageProduct = angular.copy(EappVariables.getEappModel());

        if($('#Questionnaire .selector-icon li').length == '1'){
            $rootScope.hideInsuredLine = false;
        } else {
            $rootScope.hideInsuredLine = true;
        }
        
        $scope.eAppParentobj.PreviousPage = "HealthDetailsSubTab_unload";
        $scope.PreviouspolicyHistoryList = [];
        $scope.FamilyHistoryList = [];
        $scope.remainingPreviousPolicyHistory = [];
        if ((rootConfig.isDeviceMobile)) {
            $scope.LifeEngageProduct.Insured.Questionnaire = [];
        }
        $scope.disableBtns = false;
        $timeout(function () {
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission") {
                 //UtilityService.disableAllActionElements();
                $scope.disableBtns = true;
            }
        }, 50); 

        if ((rootConfig.isDeviceMobile)) {
            $scope.QuestionnaireObj = angular.copy(EappVariables.QuestionModel);
			
			$scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory = GLI_EappVariables.previouspolicyHistory;
			
			if($scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[2] != undefined && $scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[2].orderId !=''){
				$scope.previousPolicyAddButton = 'true';
			}
			if($scope.QuestionnaireObj.AdditionalQuestioniare.Other!=undefined && $scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions[0].detailsOther != "" && $scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions[0].detailsOther != undefined){
				if($scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions[0].details == 'Replace existing policy'){
					$scope.addPolicyNumbers();
					$scope.Question2dropdown.number = $scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions[0].detailsOther;
					$scope.Question2dropdown.id = parseInt($scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions[0].unit);
				}else{
					$scope.Question2textbox = $scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions[0].detailsOther;
				}
			}
			
			if($scope.QuestionnaireObj.Other.Questions[274].details != "" && $scope.QuestionnaireObj.Other.Questions[274].details != undefined && $scope.QuestionnaireObj.Other.Questions[274].detailsOther != "" && $scope.QuestionnaireObj.Other.Questions[274].unit != ""){
				$scope.QuestionnaireObj.Other.Questions[274].details.id = $scope.QuestionnaireObj.Other.Questions[274].unit;
				$scope.QuestionnaireObj.Other.Questions[274].details.label = $scope.QuestionnaireObj.Other.Questions[274].detailsOther;
			}

        if($scope.QuestionnaireObj.Other.Questions[274].details ==""){
            var unit="";
            $scope.QuestionnaireObj.Other.Questions[274].details = $scope.item;
            if($scope.QuestionnaireObj.Other.Questions[274].unit!="")
            {
            unit=parseInt($scope.QuestionnaireObj.Other.Questions[274].unit);
            }
            else{
            unit=$scope.QuestionnaireObj.Other.Questions[274].unit;
            }
            $scope.QuestionnaireObj.Other.Questions[274].details.id = unit;
		    $scope.QuestionnaireObj.Other.Questions[274].details.label = $scope.QuestionnaireObj.Other.Questions[274].detailsOther;
	
            }

			
        } else {
            $scope.QuestionnaireObj = angular.copy($scope.LifeEngageProduct.Insured.Questionnaire);
			
			$scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory = $scope.LifeEngageProduct.PreviouspolicyHistory;
			
			if($scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[2] != undefined && $scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[2].orderId !=''){
				$scope.previousPolicyAddButton = 'true';
			}
			if($scope.QuestionnaireObj.AdditionalQuestioniare.Other!=undefined && $scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions[0].detailsOther != "" && $scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions[0].detailsOther != undefined){
				if($scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions[0].details == 'Replace existing policy'){
					$scope.addPolicyNumbers();
					$scope.Question2dropdown.number = $scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions[0].detailsOther;
					$scope.Question2dropdown.id = parseInt($scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions[0].unit);
				}else{
					$scope.Question2textbox = $scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions[0].detailsOther;
				}
			}
			
			if($scope.QuestionnaireObj.Other.Questions[274].details != "" && $scope.QuestionnaireObj.Other.Questions[274].details != undefined && $scope.QuestionnaireObj.Other.Questions[274].detailsOther != "" && $scope.QuestionnaireObj.Other.Questions[274].unit != ""){
				$scope.QuestionnaireObj.Other.Questions[274].details.id = $scope.QuestionnaireObj.Other.Questions[274].unit;
				$scope.QuestionnaireObj.Other.Questions[274].details.label = $scope.QuestionnaireObj.Other.Questions[274].detailsOther;
			}

           if($scope.QuestionnaireObj.Other.Questions[274].details == ""){
            
            $scope.QuestionnaireObj.Other.Questions[274].details = $scope.item;
            var unit="";
            if($scope.QuestionnaireObj.Other.Questions[274].unit!="")
            {
            unit=parseInt($scope.QuestionnaireObj.Other.Questions[274].unit);
            }
            else{
            unit=$scope.QuestionnaireObj.Other.Questions[274].unit;
            }
        
            $scope.QuestionnaireObj.Other.Questions[274].details.id =unit;
		    $scope.QuestionnaireObj.Other.Questions[274].details.label = $scope.QuestionnaireObj.Other.Questions[274].detailsOther;
	        }


			
        }
        if (EappVariables.EappKeys.Key15 == "Confirmed" && $scope.$parent.LifeEngageProduct.Declaration.confirmSignature == true) {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = true;
        } else {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
        }
		
		 $timeout(function () {
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission") {
                UtilityService.disableAllActionElements();
				  $scope.spanDisabled();
            }else if (typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) {
                UtilityService.disableAllActionElements();
				 $scope.spanDisabled();
            }
        }, 0);
		
        //For disabling and enabling the tabs.And also to know the last visited page.---starts
        //$scope.LifeEngageProduct.LastVisitedUrl = "3,1,'MainInsuredQuestionnaire',true,'HealthDetailsSubTab','eAppQuestionnaire'";
        $scope.LifeEngageProduct.LastVisitedUrl = "3,6,'eappshortquestionnaire',true,'eappshortquestionnaire',''";
        $scope.eAppParentobj.CurrentPage = "MainInsuredQuestionnaire";
       if ($scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer==='No') {
            $scope.eAppParentobj.nextPage = "3,2,'PolicyHolderQuestionnaire',true,'LifeStyleSubTab','eAppProposerQuestionnaire'";
        } else if ($scope.LifeEngageProduct.Proposer) {
            $scope.eAppParentobj.nextPage = "3,3,'eAppFatcaQuestionnaireJson',true,'FatcaQuestionnaire',''";
        } else {
            $scope.eAppParentobj.nextPage = "4,1,'SPAJsummary',true,'SPAJsummary',''";
        }
        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[1] < 1 && subIndex[0] <= $scope.LifeEngageProduct.LastVisitedUrl[0]) {
                $scope.LifeEngageProduct.LastVisitedIndex = "3,1";
            }
        }

        //For disabling and enabling the tabs.And also to know the last visited page.---ends
        var model = "LifeEngageProduct";
        if ((rootConfig.autoSave.eApp) && (typeof EappVariables.EappKeys.Key15 == "undefined" || EappVariables.EappKeys.Key15 == "New" || EappVariables.EappKeys.Key15 == "Confirmed")) {
            if (AutoSave.destroyWatch) {
                AutoSave.destroyWatch();
            }
            AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
        } else if (AutoSave.destroyWatch) {
            AutoSave.destroyWatch();
        }

        // For previous policy history display in questionnaire others tab.	
        // $scope.PreviouspolicyHistory = GLI_EappVariables.previouspolicyHistory;
        // if ($scope.QuestionnaireObj.LifestyleDetails.Questions[0].option == "Yes") {
        //     if ($scope.LifeEngageProduct.PreviouspolicyHistory && $scope.LifeEngageProduct.PreviouspolicyHistory.length > 0) {
        //         $scope.remainingPreviousPolicyHistory = [];
        //         for (var i = 0; i < $scope.LifeEngageProduct.PreviouspolicyHistory.length; i++) {
        //             if ($scope.LifeEngageProduct.PreviouspolicyHistory[i].partyType == "Primary") {
        //                 $scope.IsPreviouspolicySection = true;
        //                 $scope.PreviouspolicyHistoryList.push($scope.LifeEngageProduct.PreviouspolicyHistory[i]);
        //             } else {
        //                 if ($scope.LifeEngageProduct.PreviouspolicyHistory[i].partyType != "Primary" && $scope.LifeEngageProduct.PreviouspolicyHistory[i].partyId != 0) {
        //                     $scope.remainingPreviousPolicyHistory.push($scope.LifeEngageProduct.PreviouspolicyHistory[i]);
        //                 }
        //             }
        //         }
        //     } else {
        //         var previouspolicyHistoryObj = angular.copy($scope.PreviouspolicyHistory);
        //         previouspolicyHistoryObj.partyType = "Primary";
        //         previouspolicyHistoryObj.partyId = "0";
        //         $scope.PreviouspolicyHistoryList.push(previouspolicyHistoryObj);
        //     }
        // } else {
        //     $scope.IsPreviouspolicySection = false;
        // }

        // //Family History in Questionnaire
        // $scope.FamilyHistory = GLI_EappVariables.familyHistory;
        // if ($scope.QuestionnaireObj.Other.Questions[0].option == "Yes") {
        //     if (($scope.QuestionnaireObj.FamilyHealthHistory && $scope.QuestionnaireObj.FamilyHealthHistory.length) > 0) {
        //         for (var i = 0; i < $scope.QuestionnaireObj.FamilyHealthHistory.length; i++) {
        //             $scope.IsFamilyHistorySection = true;
        //             $scope.FamilyHistoryList.push($scope.QuestionnaireObj.FamilyHealthHistory[i]);
        //         }
        //     } else {
        //         var familyHistoryObj = angular.copy($scope.FamilyHistory);
        //         $scope.FamilyHistoryList.push(familyHistoryObj);
        //     }
        // } else {
        //     $scope.IsFamilyHistorySection = false;
        // }

        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        angular.element('#' + $scope.selectedTabId + "_a").trigger('click');
		
		/*
		 if($scope.LifeEngageProduct.Product.LifeStyleQuestionsTab){
		  $("#headerText_0").removeClass("diableCheckMark");
		 }
		 if($scope.LifeEngageProduct.Product.IllnessTreatmentHistoryTab){
		  $("#headerText_1").removeClass("diableCheckMark");
		 }
	     if($scope.LifeEngageProduct.Product.healthDDRiderTab){
		  $("#headerText_2").removeClass("diableCheckMark");
		}
		if($scope.LifeEngageProduct.Product.previousPoliciesTab){
		$("#headerText_4").removeClass("diableCheckMark");
		}
		if($scope.LifeEngageProduct.Product.addTab){
		 	
		 $("#headerText_3").removeClass("diableCheckMark");
		}
			*/
        
        $scope.LifeEngageProduct.Insured.Questionnaire=$scope.QuestionnaireObj;
         var lifeStyleLength=$scope.LifeEngageProduct.Insured.Questionnaire.LifestyleDetails.Questions.length;
         var illnessLength=$scope.LifeEngageProduct.Insured.Questionnaire.Other.Questions.length;            
         var healthDDLength=$scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions.length;
        var AddQuestionlength=$scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.Questions.length;
        
		//$scope.errorMsgQuestionnaire();
       
        var prevPolicylength=$scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.Other.Questions.length;
        if($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.Other.Questions[prevPolicylength-1].details=="Yes")
        {
		$("#headerText_4").removeClass("diableCheckMark");
        $("#headertabs_4").removeClass("QuestionnaireCheckbox");
		}
        
         if($scope.LifeEngageProduct.Insured.Questionnaire.LifestyleDetails.Questions[lifeStyleLength-1].details=="Yes")
         {
		  $("#headerText_0").removeClass("diableCheckMark");
          $("#headertabs_0").removeClass("QuestionnaireCheckbox");
		 }
             
		 if($scope.LifeEngageProduct.Insured.Questionnaire.Other.Questions[illnessLength-1].details=="Yes")
         {
		  $("#headerText_1").removeClass("diableCheckMark");
          $("#headertabs_1").removeClass("QuestionnaireCheckbox");
		 }
        
	     if($scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[healthDDLength-1].details=="Yes")
         {
		  $("#headerText_2").removeClass("diableCheckMark");
          $("#headertabs_2").removeClass("QuestionnaireCheckbox");
		 }
        
		if($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.Questions[AddQuestionlength-1].details=="Yes")
        {		 	
		 $("#headerText_3").removeClass("diableCheckMark");
         $("#headertabs_3").removeClass("QuestionnaireCheckbox");
		}
        
        if($scope.QuestionnaireObj.LifestyleDetails.Questions[28].details=='Yes' && $scope.QuestionnaireObj.LifestyleDetails.Questions[30].details=="")
         {
         $scope.isIncWeight =true;           
         }
        else
         {
         $scope.isIncWeight =false;         
         }		
        
        if($scope.QuestionnaireObj.LifestyleDetails.Questions[0].details=='Yes')
        {    
        $rootScope.isPreviousPolicyNavigation=true;
        }
        else {        
        $rootScope.isPreviousPolicyNavigation=false;
        }
        $scope.prevErorrCountUpdate();
        $timeout(function () {
            $scope.updateErrorCount('MainInsuredQuestionnaire');
            $scope.refresh();
        }, 50)
    }

    $scope.checkDate = function (value, index, data) {
        var _errorCount = 0;
        var today = new Date();
        var dateGiven = new Date(value);
        if (dateGiven > today) {
            _errorCount++;
            var error = {};
            error.message = translateMessages($translate, "lms.callDetailsSectionmeetingNextMeetingDateErrorValidationMessage");
            // meetingDateLesserThanFollowup
            error.key = data + index.toString();
            _errors.push(error);
        }
    }

    $scope.updateDynmaicErrMessage = function (errorKey) {
        for (var i = 0; i < $scope.dynamicErrorMessages.length; i++) {
            if ($scope.dynamicErrorMessages[i].key == errorKey) {
                $scope.dynamicErrorMessages.splice(i, 1);
                $scope.dynamicErrorCount = $scope.dynamicErrorCount - 1;
            }
        }
    }

    $scope.updateErrorDynamically = function () {
        if ($scope.FamilyHistoryList && Object.keys($scope.FamilyHistoryList).length > 0) {
            $scope.dynamicErrorMessages = [];
            $scope.dynamicErrorCount = 0;
            for (var i = 0; i < Object.keys($scope.FamilyHistoryList).length; i++) {
                if ($scope.FamilyHistoryList[i].diseaseOnsetAge == "" && $scope.FamilyHistoryList[i].ageAtDeath == "") {
                    var error = {};
                    error.message = translateMessages($translate, "eapp_vt.diseaseOnsetAgeRequiredMessage");
                    error.key = "diseaseAge" + i;
                    $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                    $scope.errorCount = $scope.errorCount + 1;
                    $scope.dynamicErrorMessages.push(error);
                } else if ($scope.FamilyHistoryList[i].diseaseOnsetAge != "" && $scope.FamilyHistoryList[i].ageAtDeath == "") {
                    $scope.updateDynmaicErrMessage("diseaseAge" + i);
                } else if ($scope.FamilyHistoryList[i].diseaseOnsetAge == "" && $scope.FamilyHistoryList[i].ageAtDeath != "") {
                    $scope.updateDynmaicErrMessage("diseaseAge" + i);
                } else {
                    $scope.updateDynmaicErrMessage("diseaseAge" + i);
                }
            }
        }
    }






    $scope.checkAgeAndDeath = function () {
        if ($scope.FamilyHistoryList && Object.keys($scope.FamilyHistoryList).length > 0) {
            $scope.dynamicErrorMessages = [];
            $scope.dynamicErrorCount = 0;
            for (var i = 0; i < Object.keys($scope.FamilyHistoryList).length; i++) {
                if ($scope.FamilyHistoryList[i].diseaseOnsetAge == "" && $scope.FamilyHistoryList[i].ageAtDeath == "") {
                    var error = {};
                    error.message = translateMessages($translate, "eapp_vt.diseaseOnsetAgeRequiredMessage");
                    error.key = "diseaseAge" + i;
                    $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                    $scope.errorCount = $scope.errorCount + 1;
                    $scope.dynamicErrorMessages.push(error);
                } else if ($scope.FamilyHistoryList[i].diseaseOnsetAge != "" && $scope.FamilyHistoryList[i].ageAtDeath == "") {
                    $scope.updateDynmaicErrMessage("diseaseAge" + i);
                } else if ($scope.FamilyHistoryList[i].diseaseOnsetAge == "" && $scope.FamilyHistoryList[i].ageAtDeath != "") {
                    $scope.updateDynmaicErrMessage("diseaseAge" + i);
                } else {
                    $scope.updateDynmaicErrMessage("diseaseAge" + i);
                }
            }
        }
    }

    $scope.IsPreviouspolicy = function (value) {
//        if (value == "Yes") {
//            $scope.IsPreviouspolicySection = true;
//            if ($scope.PreviouspolicyHistoryList.length <= 0) {
//                $scope.addPreviousPolicyHistory()
//            }
//        } else {
//            $scope.IsPreviouspolicySection = false;
//            $scope.PreviouspolicyHistoryList = [];
//            $scope.updateErrorCount($scope.selectedTabId);
//            //$scope.$apply();
//            $scope.refresh();
//        }
    }



$scope.SubQuestionSubCategoriesVisiblity=function(value)
{
$scope.SubQuestionSubCategoriesVisiblity=true;

}







    $scope.addPreviousPolicyHistory = function () {
//        if ($scope.PreviouspolicyHistoryList.length < 2) {
//            $scope.addNew = true;
//            $scope.showErrorCount = true;
//            var previouspolicyHistoryObj = angular.copy($scope.PreviouspolicyHistory);
//            previouspolicyHistoryObj.partyType = "Primary";
//            previouspolicyHistoryObj.partyId = "0";
//            previouspolicyHistoryObj.insured = $scope.LifeEngageProduct.Insured.BasicDetails.firstName + " " + $scope.LifeEngageProduct.Insured.BasicDetails.lastName;
//            switch ($scope.PreviouspolicyHistoryList.length) {
//                case 0:
//                    previouspolicyHistoryObj.orderId = "1";
//                    break;
//                case 1:
//
//                    previouspolicyHistoryObj.orderId = "2";
//                    break;
//                case 2:
//                    previouspolicyHistoryObj.orderId = "3";
//                    break;
//                default:
//                    break;
//            }
//            $scope.PreviouspolicyHistoryList.push(previouspolicyHistoryObj);
//            setTimeout(function () {
//                $scope.updateErrorCount($scope.selectedTabId);
//                $scope.refresh();
//            }, 50)
//        }
    };

    $scope.deletePreviousPolicyHistory = function (index) {
//        if (index == 'No') {
//            $scope.PreviouspolicyHistoryList = [];
//            $scope.LifeEngageProduct.PreviouspolicyHistory = [];
//        } else if (!isNaN(index)) {
//            if ($scope.PreviouspolicyHistoryList.length > 1) {
//                $scope.PreviouspolicyHistoryList.splice(index, 1);
//                setTimeout(function () {
//                    $scope.updateErrorCount($scope.selectedTabId);
//                    $scope.refresh();
//                }, 50);
//            }
//        }
    };

    $scope.IsFamilyHistory = function (value) {
        if (value == "Yes") {
            $scope.IsFamilyHistorySection = true;
            if ($scope.FamilyHistoryList.length <= 0) {
                $scope.addFamilyHistory()
            }
        } else {
            $scope.IsFamilyHistorySection = false;
            $scope.FamilyHistoryList = [];
            $scope.updateErrorCount($scope.selectedTabId);
            if ($scope.dynamicErrorCount > 0) {
                for (var j = 0; j < $scope.dynamicErrorMessages.length; j++) {
                    $scope.updateDynmaicErrMessage($scope.dynamicErrorMessages[j].key);
                }
            }
            $scope.dynamicErrorCount = 0;
            $scope.dynamicErrorMessages = [];
            //$scope.$apply();
            $scope.refresh();
        }
    }

    $scope.addFamilyHistory = function () {
        if ($scope.FamilyHistoryList.length < 3) {
            $scope.addNew = true;
            $scope.showErrorCount = true;
            var familyHistoryObj = angular.copy($scope.FamilyHistory);
            switch ($scope.FamilyHistoryList.length) {
                case 0:
                    familyHistoryObj.orderId = 1;
                    break;
                case 1:
                    familyHistoryObj.orderId = 2;
                    $scope.errorCount = $scope.errorCount - 1;
                    $scope.dynamicErrorCount = 0;
                    break;
                case 2:
                    familyHistoryObj.orderId = 3;
                    $scope.errorCount = $scope.errorCount - 2;
                    $scope.dynamicErrorCount = 0;
                    break;
                default:
                    break;
            }
            $scope.FamilyHistoryList.push(familyHistoryObj);
            setTimeout(function () {
                $scope.updateErrorCount($scope.selectedTabId);
                $scope.updateErrorDynamically();
                $scope.refresh();
            }, 0)
        }
    };

    $scope.updateDynmaicErrorMessages = function (errorKey, index) {
        if ($scope.dynamicErrorMessages[index].key == errorKey) {
            $scope.dynamicErrorMessages.splice(index, 1);
            $scope.dynamicErrorCount = $scope.dynamicErrorCount - 1;
            $scope.errorCount = $scope.errorCount - 1;
        }
    }

    $scope.deleteFamilyHistory = function (index) {
        if (index == 'No') {
            $scope.FamilyHistoryList = [];
            $scope.LifeEngageProduct.Insured.Questionnaire.FamilyHealthHistory = [];
        } else if (!isNaN(index)) {
            if ($scope.FamilyHistoryList.length > 1) {
                $scope.FamilyHistoryList.splice(index, 1);
                setTimeout(function () {
                    $scope.updateErrorCount($scope.selectedTabId);
                    if ($scope.dynamicErrorCount > 0) {
                        $scope.updateDynmaicErrorMessages($scope.dynamicErrorMessages[index - 1].key, index - 1);
                    }
                    $scope.refresh();
                }, 50);
            }
        }
    };

    $scope.updateErrorCount = function (id) {
        $rootScope.updateErrorCount(id);
        $scope.selectedTabId = id;
        $scope.eAppParentobj.errorCount = $scope.errorCount;
    }
    $scope.showErrorPopup = function (id) {
        if ($scope.selectedTabId == "HealthDetailsSubTab") {
            $scope.selectedTabId = "tabsinnerMainInsuredHealthDetails";
        }
        $rootScope.showErrorPopup($scope.selectedTabId);
    }

    $scope.clearText = function (model) {
        if (typeof GLI_EvaluateService.evaluateString($scope, model) != undefined) {
            var parentModel = GLI_EvaluateService.evaluateString($scope, "$scope." + model.substring(0, model.lastIndexOf('.')));
            var subModel = model.substring(model.lastIndexOf('.') + 1, model.length);
            parentModel[subModel] = "";
            $scope.refresh();
        }
    }
    //HealthDetailsController Load Event and Load Event Handler
    var load = $scope.$parent.$on('MainInsuredQuestionnaire', function (event, args) {
        if (typeof $scope.selectedTabId == "undefined") {
            $scope.selectedTabId = args[0];
            
        }      
        $scope.selectedSectionId = "MainInsured";
        initialize();
        $rootScope.validationBeforesave = function (value, successcallback) {
            if (value == "save") {
                $scope.LifeEngageProduct.LastVisitedIndex = "3,1";
            }
            if (rootConfig.isDeviceMobile) {
                EappVariables.setEappModel($scope.LifeEngageProduct);
            }
            $scope.validateBeforeSaveDetails(value, successcallback);
        };
    });

    function questionnaireBeforeSaveFn(successcallback) {
        //Add previousPolicy history
//        $scope.LifeEngageProduct.PreviouspolicyHistory = [];
//        if ($scope.PreviouspolicyHistoryList.length > 0) {
//            angular.forEach($scope.PreviouspolicyHistoryList, function (value) {
//                $scope.LifeEngageProduct.PreviouspolicyHistory.push(value);
//            });
//        }
//        //Add Family History
//        $scope.LifeEngageProduct.Insured.Questionnaire.FamilyHealthHistory = [];
//        if ($scope.FamilyHistoryList.length > 0) {
//            angular.forEach($scope.FamilyHistoryList, function (value) {
//                $scope.LifeEngageProduct.Insured.Questionnaire.FamilyHealthHistory.push(value);
//            });
//        }
        /*if ($scope.remainingPreviousPolicyHistory.length > 0) {
            angular.forEach($scope.remainingPreviousPolicyHistory, function(value){                	
        	$scope.LifeEngageProduct.PreviouspolicyHistory.push(value);
            });
           
        }*/
        successcallback();
    }

    /*	 $scope.$on("$destroy", function() {
                     if (this.$$destroyed) return;     
    				 while (this.$$childHead) {      
    				 this.$$childHead.$destroy();    
    				 }     
    				 if (this.$broadcast)
    				{ 
    					this.$broadcast('$destroy');   
    				}    
    				 this.$$destroyed = true;                 
    				 $timeout.cancel( timer );                           
            });*/

    $rootScope.validationBeforesave = function (value, successcallback) {
        $scope.validateBeforeSaveDetails(value, successcallback);
    };

    $scope.validateBeforeSaveDetails = function (value, successcallback) {
		
		if($scope.QuestionnaireObj.Other.Questions[274].details != undefined && $scope.QuestionnaireObj.Other.Questions[274].details.label != "" && $scope.QuestionnaireObj.Other.Questions[274].details.id != ""){
			$scope.QuestionnaireObj.Other.Questions[274].detailsOther = $scope.QuestionnaireObj.Other.Questions[274].details.label;
			$scope.QuestionnaireObj.Other.Questions[274].unit = $scope.QuestionnaireObj.Other.Questions[274].details.id;
		}
		
        if($scope.Question2dropdown != undefined && $scope.Question2dropdown.number != undefined && $scope.Question2dropdown.number != ""){
           $scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions[0].detailsOther = $scope.Question2dropdown.number;
           $scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions[0].unit = $scope.Question2dropdown.id;
        } else if($scope.Question2textbox != "" && $scope.Question2textbox != undefined){
           $scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions[0].detailsOther = $scope.Question2textbox;
        }
        
        if( $rootScope.lifeStyleTickVisiblity){
            var length= $scope.QuestionnaireObj.LifestyleDetails.Questions.length;
            $scope.QuestionnaireObj.LifestyleDetails.Questions[length-1].details="Yes";
                $rootScope.lifeStyleTickVisiblity=undefined;
        }
        
        if($rootScope.illnessTickVisiblity){
             var length= $scope.QuestionnaireObj.Other.Questions.length;
             $scope.QuestionnaireObj.Other.Questions[length-1].details="Yes";
                       $rootScope.illnessTickVisiblity=undefined;
        }
            
        
        if($rootScope.healthTickVisiblity){
            var length= $scope.QuestionnaireObj.HealthDetails.Questions.length;
            $scope.QuestionnaireObj.HealthDetails.Questions[length-1].details="Yes";
                $rootScope.healthTickVisiblity=undefined;
        }
        
        if($rootScope.previousTickVisiblity){
             var length= $scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions.length;
             $scope.QuestionnaireObj.AdditionalQuestioniare.Other.Questions[length-1].details="Yes";
                       $rootScope.previousTickVisiblity=undefined;
        }
        
        if($rootScope.addTickVisiblity){
            var length= $scope.QuestionnaireObj.AdditionalQuestioniare.Questions.length;
            $scope.QuestionnaireObj.AdditionalQuestioniare.Questions[length-1].details="Yes";
                $rootScope.addTickVisiblity=undefined;
        }
        
        $scope.LifeEngageProduct.Insured.Questionnaire = angular.copy($scope.QuestionnaireObj);
        $scope.LifeEngageProduct.PreviouspolicyHistory = angular.copy($scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory);
            
    
        if ((rootConfig.isDeviceMobile)) {
            EappService.getEapp(function () {
                EappVariables.setEappModel($scope.LifeEngageProduct);
                questionnaireBeforeSaveFn(function () {
                    successcallback();
                });
            }, function () {
               
            });
        } else {
			EappVariables.setEappModel($scope.LifeEngageProduct);
            questionnaireBeforeSaveFn(function () {
                successcallback();
            });
        }
    }

    $scope.callUpdateErrorCount = function (param) {
        var str = 'tabsinner' + $scope.selectedSectionId + param;
        $scope.updateErrorCount(str);
    }

    //For counting no.of AdditionalQuestions selected
    $scope.callAdditionalQuestionCount = function (val, questionID, isMedicalQuestion) {
        var curentArr = [];
        if ($scope.LifeEngageProduct.hasInsuredQuestions && ($scope.LifeEngageProduct.hasInsuredQuestions != "0" || $scope.LifeEngageProduct.hasInsuredQuestions != 0) && $scope.LifeEngageProduct.hasInsuredQuestions != "") {
            curentArr = JSON.parse($scope.LifeEngageProduct.hasInsuredQuestions).selectedArr;
        }
        EappService.updateAdditionalQuestionCount(val, questionID, curentArr, function (modifiedArr) {
            var modifiedJson = {
                "selectedArr": []
            };
            modifiedJson.selectedArr = modifiedArr;
            $scope.LifeEngageProduct.hasInsuredQuestions = JSON.stringify(modifiedJson);
        });

        //Counting medical questions only
        /*if(isMedicalQuestion ==true){
        	var medicalArr=[];
        	if( $scope.LifeEngageProduct.hasMedicalQuestions && ($scope.LifeEngageProduct.hasMedicalQuestions!="0" || $scope.LifeEngageProduct.hasMedicalQuestions!=0) && $scope.LifeEngageProduct.hasMedicalQuestions!=""){
        		medicalArr = JSON.parse($scope.LifeEngageProduct.hasMedicalQuestions).selectedArr;
            }
            EappService.updateMedicalQuestionCount(val,questionID, medicalArr, function(modifiedArr){
            	var modifiedJson = {"selectedArr" : []};
         	    modifiedJson.selectedArr = modifiedArr;
         	    $scope.LifeEngageProduct.hasMedicalQuestions = JSON.stringify(modifiedJson);   
            });
   		}*/
    }

    /*$scope.checkErrorCountOnTabNav = function($event,param){
    	var currentTab=$scope.selectedTabId;
    	var clickedTab = 'tabsinner'+ $scope.selectedSectionId + param;
    	//$scope.updateErrorCount(currentTab);
    	var currentTabErrorCount=$scope.errorCount;
    	//$scope.updateErrorCount(clickedTab);
    	var clickedTabErrorCount=$scope.errorCount;
    	if(currentTabErrorCount>0){
    		if(clickedTabErrorCount==0){
    			$scope.selectedTabId=clickedTab;
    			//$scope.updateErrorCount(clickedTab);
    		}else{
    			$scope.selectedTabId=currentTab;
    			//$scope.updateErrorCount(currentTab);
    			$event.stopPropagation();
    			$scope.lePopupCtrl.showWarning(translateMessages($translate,"lifeEngage"),
    					translateMessages($translate,"enterMandatoryFieldsForeApp"),
    					translateMessages($translate,"fna.ok"));
    		}
    	}else{
    		$scope.selectedTabId=clickedTab;
    		//$scope.updateErrorCount(clickedTab);
    	}
    }*/

    $scope.evaluateModel = function (value) {
        var model = 'MainInsuredQuestionnaire' + '.' + value;
        if (GLI_EvaluateService.evaluateString($scope, model)) {
            return true;
        } else {
            return false;
        }
    }

    $scope.evaluatePreviousPolicyModel = function (name, index, value) {
        var model = 'MainInsuredQuestionnaire' + '.' + name + index.toString() + value;
        if (GLI_EvaluateService.evaluateString($scope, model)) {
            return true;
        } else {
            return false;
        }
    }

    $scope.evaluateFamilyHistoryModel = function (name, index, value) {
        var model = 'MainInsuredQuestionnaire' + '.' + name + index.toString() + value;
        if (GLI_EvaluateService.evaluateString($scope, model)) {
            return true;
        } else {
            return false;
        }
    }

    //To clear the models when main option is selected as 'No'
    $scope.clearModels = function () {
        /*var idList = arguments;
		for(i=0;i<idList.length;i++){
			idList[i] = $scope.selectedSectionId + idList[i] ;
			$('#'+idList[i] input[type='text']).val('');
			$('#'+idList[i] textarea).val('');
		}
		
		$scope.$apply();*/
    }

    
$scope.ClearFieldsValues = function (carouselName,model,length,start,end,isCarousel){
        
	model=model.split(".");
    var data=[];  
    angular.forEach(model,function(value,key) {    
    switch(key)
    {
    case 0:
        data= $scope[value];
        break;
    case 1:    
        data=data[value]; 
        break;
    case 2:		
		var strlen=value.split("[")[key-2].split("]")[key-2]; 	
		data=data[strlen]; 
        break;        
    } 
 });
    if(isCarousel)
	{		
	 for(var i = start; i<= end;i++)
	{
      data[i].details='';        
	}	
	}
	else
	{
		for(var i = start; i< (start+length);i++)
	{
      data[i].details='';        
	}
}

if(carouselName!="")
{
    var datePatternValidation = "datePatternValidation"+"_"+ carouselName;    
    var patternShow = "patternShow" + "_" + carouselName;
    $scope[datePatternValidation] ="";
    $scope[patternShow]=false;    
}


 angular.forEach($scope.carouselDateFields,function(index,value){        
    var datePatternValidation = "datePatternValidation"+"_"+ index;    
    var patternShow = "patternShow" + "_" + index;
     $scope[datePatternValidation] ="";
     $scope[patternShow]=false;
 });


       $timeout(function () {
            $scope.updateErrorCount('MainInsuredQuestionnaire');
            $scope.refresh();
        }, 50)
}









$scope.ClearFieldsValuesForTexboxAlone = function (name,model,start,end,exceptionStart,exceptionEnd){
    
   
	model=model.split(".");
    var data=[];  
    angular.forEach(model,function(value,key) {    
    switch(key)
    {
    case 0:
        data= $scope[value];
        break;
    case 1:    
        data=data[value]; 
        break;
    case 2:		
		var strlen=value.split("[")[key-2].split("]")[key-2]; 	
		data=data[strlen]; 
        break;        
    } 
 });
    
	if(end!=undefined){
		for(var i = start; i<= end;i++) {
			data[i].details='';        
		}
	} else {
		data[start].details='';
	}
	if(exceptionStart!=undefined){
		for(var i = exceptionStart; i<= exceptionEnd;i++) {
			data[i].details='';        
		}
	}

  var datePatternValidation = "datePatternValidation"+"_"+name;    
    var patternShow = "patternShow" + "_" + name;
    $scope[datePatternValidation] ="";
     $scope[patternShow]=false;


       $timeout(function () {
            $scope.updateErrorCount('MainInsuredQuestionnaire');
            $scope.refresh();
        }, 50)

}


$scope.Q1=["HyperTensionDetailsSection","HeartDiseaseDetailsSection","CoronaryHeartDiseaseDetailsSection","CardiovascularDiseaseDetailsSection","CerebroVascularDiseaseDetailsSection","PartialOrTotalParalysisDetailsSection","DiabeticMellitusDetailsSection","ThyroidDiseaseDetailsSection"];

$scope.Q2=["CancerDiseaseDetailsSection","EnlargedLymphNodeDiseaseDetailsSection","TumorDiseaseDetailsSection","cystDetailsSection"];

$scope.Q3=["PancreatitisDetailsSection","KidneyDiseaseDetailsSection","JaundiceDiseaseDetailsSection","SplenomegalyDetailsSection","PepticUlcerDetailsSection","LiverDetailsSection","AlcoholismDetailsSection"];

$scope.Q4=["LungDiseasePneumoniaDetailsSection","TuberculosisDetailsSection","AsthmaDiseaseDetailsSection","pulmonaryDiseaseDetailsSection","EmphysemaDetailsSection","SleepApneaDetailsSection"];

$scope.Q5=["ImpairedVisionDetailsSection","RetinaDiseaseDetailsSection","GlaucomaDetailsSection"];

$scope.Q6=["ParkinsonsDetailsSection","AlzheimersDetailsSection","EpilepsyDetailsSection"];

$scope.Q7=["ArthritisDetailsSection","GoutDetailsSection","SclerodermaDetailsSection","SLEDetailsSection","BloodDiseaseDetailsSection"];

$scope.Q8=["PsychosisDetailsSection","NeurosisDetailsSection","DepressionDetailsSection","DownSyndromeDetailsSection","PhysicalDisabilitySection"];

$scope.Q9=["AIDSDiseaseDetailsSection","VenerealDiseaseDetailsSection"];

$scope.Q10=["ChestPainDetailsSection","PalpitationDetailsSection","AbnormalFatigueDetailsSection","MuscularWeaknessDetailsSection","AbnormalPhysicalMovementDetailsSection","SensoryNeuronDetailsSection"];

$scope.Q11=["ChronicStomachAcheDetailsSection","HematemesisOrHematocheziaDetailsSection","DropsyDetailsSection","ChronicDiarrheaDetailsSection","HematuriaDetailsSection","ChronicCoughDetailsSection","HemoptysisDetailsSection"];

$scope.Q12=["PalpableTumorDetailsSection","HeadacheDetailsSection",""];

$scope.Q13=["JointPainsDetailsSection","BruisesDetailsSection"];

$scope.Q14=["ImpairedVisionDetailsSection2","SlowDevelopmentDetailsSection","AttemptedToHurtDetailsSection"];

$scope.Q16=["OtitisMediaDiseaseHealthDetailsSection","ChronicTonsillitisDiseaseHealthDetailsSection","SinusitisDiseaseHealthDetailsSection","MigraineDiseaseHealthDetailsSection","AllergyHealthDiseaseDetailsSection","ChronicBronchitisDiseaseDetailsSection"];

$scope.Q17=["gastroEsophagealRefluxDiseaseHealthDetailsSection","urinaryTractStoneDiseaseHealthDetailsSection","cholecystitisDiseaseHealthDetailsSection","herniaDiseaseHealthDetailsSection","hemorrhoidHealthDiseaseDetailsSection","analFistulaDiseaseDetailsSection"];

$scope.Q18=["endometroisisDiseaseHealthDetailsSection","spondylolisthesisDiseaseHealthDetailsSection","herniatedVertebralDiscDiseaseHealthDetailsSection","herniatedNucleusPulposusDiseaseHealthDetailsSection","degenerativeOsteoarthritisDiseaseDetailsSection","chronicTendinitisDiseaseDetailsSection","peripheraNeuritisDiseaseDetailsSection"];

$scope.Q19=["autisticDiseaseHealthDetailsSection","attDefHyperActdisorderDiseaseHealthDetailsSection"];
$scope.carouselDateFields=[];
$scope.tab=function(model,questions,start,end,length,exceptionStart,expectionEnd){
    	
	var tabCheckIndex=2;
	var tabIndex=1;
	 
	if(model == "Other"){
		for(var i=0; i < $scope.QuestionnaireObj.Other.Questions[start-1].options.length; i++){
			$scope.QuestionnaireObj.Other.Questions[start-1].options[i].details = "";
		}
	}else if(model == "health"){
		for(var i=0; i < $scope.QuestionnaireObj.HealthDetails.Questions[start-1].options.length; i++){
			$scope.QuestionnaireObj.HealthDetails.Questions[start-1].options[i].details = "";
		}
	}else{
		for(var i=0; i < $scope.QuestionnaireObj.LifestyleDetails.Questions[start-1].options.length; i++){
			$scope.QuestionnaireObj.LifestyleDetails.Questions[start-1].options[i].details = "";
		}
	}

			angular.forEach($scope.indexArrayList,function(value,key) {    
                					
				var model="";
				$scope.tabName=value;		
              			
			    if(questions.indexOf(value) > -1)
				{					
                    
       
                if($('.'+value).children("div .col-xs-3").children().attr('name')!=undefined) {
                      $scope.carouselGetDateName =  $('.'+value).children("div .col-xs-3").children().attr('name');
                    }

                     if($scope.carouselDateFields.indexOf($scope.carouselGetDateName) == -1){
                         $scope.carouselDateFields.push($scope.carouselGetDateName);
                         }       

					$scope.indexArrayList[value]=false;                
				}
                });	

				 angular.forEach($scope.selectedTabList,function(value,key) {    
				 var carouseltabcheckbox = value;
				 carouseltabcheckbox=carouseltabcheckbox.split("_")[tabCheckIndex];
				 if(questions.indexOf(carouseltabcheckbox) > -1)
				 {			     
				 $(value).removeClass("checkmarkActive");
				 var carouseltabindex=value.split("_")[tabIndex];
				 var carouseltabName=value.split("_")[tabCheckIndex];
				 $("#carousel_"+carouseltabindex+"_"+carouseltabName).removeClass("active");			  
				 }
				 });
				if(model=="Other") {
					model='QuestionnaireObj.Other.Questions';	
				} else if (model=="health") {
					model='QuestionnaireObj.HealthDetails.Questions';	
				} else if (model=="life") {
					model='QuestionnaireObj.LifestyleDetails.Questions';	
				}
                
				$scope.ClearFieldsValues("",model,length,start,end,true);
				if(exceptionStart!=0 || expectionEnd!=0)
				{					
				$scope.ClearFieldsValues("",model,length,exceptionStart,expectionEnd,true);
				}
					
			$timeout(function(){
					$scope.updateErrorCount('MainInsuredQuestionnaire')
					$scope.refresh();
				},50)		
}

$scope.isLifeStyle=false;
$scope.isOther=true;
$scope.isHealth=true;


$scope.selectedTabList = [];

$scope.disableElement = function(index, status, rel, model, questionNumber,radioQtnNo,carouselName,length,start,end,isCarousel){
    	
        var modelData=model;
	if($scope.tabedIndex == index +'-'+ rel){
			model=model.split(".");
			var tab = model[1];
			
			if ($('#tabIndex'+'_'+index+ "_" + rel).hasClass("checkmarkActive")) {  
				$('#tabIndex'+'_'+index+ "_" +rel).removeClass("checkmarkActive");
				$scope.carouselId = "";
				
				if($scope.indexArrayList.indexOf(rel) == -1){					
                    $scope.indexArrayList[rel] = false;
					$scope.indexArrayList.push(rel);
                } else {
					$scope.indexArrayList[rel] = false;
                    var totalCount=0;
                    for(var i=0;i<$scope.indexArrayList.length;i++){
                        if($scope.indexArrayList[rel]){
                            totalCount++;
                        }
                    }
                    if(totalCount==1){
                        var radioQuestion="radioQuestion" + radioQtnNo;
                        $scope[radioQuestion] = false;
                  
                    }
                  
                          
                    }
                    	
                       $timeout(function(){
                           $scope.updateErrorCount('MainInsuredQuestionnaire')
                           $scope.refresh();
                       },50)
					   
					if (tab=="HealthDetails") {
						$scope.QuestionnaireObj.HealthDetails.Questions[questionNumber].options[index].details = '';
						$scope.isHealth=true;
						$scope.isLifeStyle=false;
						$scope.isOther=false;
					} else if (tab=="LifestyleDetails") {
						$scope.QuestionnaireObj.LifestyleDetails.Questions[questionNumber].options[index].details = '';
						$scope.isLifeStyle=true;
						$scope.isOther=false;
						$scope.isHealth=false;	
					} else if (tab=="Other") {			
						$scope.QuestionnaireObj.Other.Questions[questionNumber].options[index].details = '';
						$scope.isOther=true;
						$scope.isLifeStyle=false;
						$scope.isHealth=false;	
				}
				
				}else{
					$('#tabIndex'+'_'+index+ "_" + rel).addClass("checkmarkActive");
					$scope.carouselId = rel;
					if($scope.indexArrayList.indexOf(rel) == -1){
                   		$scope.indexArrayList[rel] = true;
						$scope.indexArrayList.push(rel);
                	} else {
						$scope.indexArrayList[rel] = true;
					}
					
					if (tab=="HealthDetails") {
						$scope.QuestionnaireObj.HealthDetails.Questions[questionNumber].options[index].details = 'true';
						$scope.isHealth=true;
						$scope.isLifeStyle=false;
						$scope.isOther=false;
					} else if(tab=="LifestyleDetails") {
						$scope.QuestionnaireObj.LifestyleDetails.Questions[questionNumber].options[index].details = 'true';
						$scope.isLifeStyle=true;
						$scope.isOther=false;
						$scope.isHealth=false;	
					} else if(tab=="Other") {			
						$scope.QuestionnaireObj.Other.Questions[questionNumber].options[index].details = 'true';
						$scope.isOther=true;
						$scope.isLifeStyle=false;
						$scope.isHealth=false;	
					}
				
				}
                $scope.ClearFieldsValues(carouselName,modelData,length,start,end,isCarousel);
				
			} else {
				$('#tabIndex'+'_'+index+ "_" +rel).addClass("QuestionnaireCheckbox");
				$('#'+rel).addClass("QuestionnaireCheckbox");
				
				var tabId='#tabIndex'+'_'+index+ "_" + rel;
				//$scope.carouselId = rel;
				$scope.selectedTabList.push(tabId);
				
				if($scope.indexArrayList.indexOf(rel) == -1){
                    $scope.indexArrayList[rel] = true;
					$scope.indexArrayList.push(rel);
                } else {
					$scope.indexArrayList[rel] = true;
				}
			}
			if($scope.isOther){
			 for(var i=0;i<$scope.QuestionnaireObj.Other.Questions[questionNumber].options.length;i++){
					if($scope.QuestionnaireObj.Other.Questions[questionNumber].options[i].details == "true"){ 
						var  radioQuestion="radioQuestion" + radioQtnNo;
					   $scope[radioQuestion] = true;
					   return;
					}else{
						var  radioQuestion="radioQuestion" + radioQtnNo;
					   $scope[radioQuestion] = false;
					}
               }
			}
			if($scope.isHealth){
			 for(var i=0;i<$scope.QuestionnaireObj.HealthDetails.Questions[questionNumber].options.length;i++){
					if($scope.QuestionnaireObj.HealthDetails.Questions[questionNumber].options[i].details == "true"){ 
					   var  radioQuestion="radioQuestion" + radioQtnNo;
					   $scope[radioQuestion] = true;
					   return;
					}else{
						var  radioQuestion="radioQuestion" + radioQtnNo;
					   $scope[radioQuestion] = false;
					}
               }
			}
			$timeout(function(){
					$scope.updateErrorCount('MainInsuredQuestionnaire')
					$scope.refresh();
				},50)
		}	

$scope.indexArrayList = [];



$scope.setNavigationScope=function(){
	
	$rootScope.isPreviousPolicyNavigation=true;
	
}


$scope.removeNavigationScope=function(){
	$rootScope.isPreviousPolicyNavigation=false;
	
}


$scope.carosuelTab = function(rel, index, questionNumber,model,radioQtnNo){
		 	var  radioQuestion="radioQuestion" + radioQtnNo;
                   $scope[radioQuestion] = true;
		if($scope.indexArrayList.indexOf(rel) == -1){
			$scope.indexArrayList[rel] = true;
			$scope.indexArrayList.push(rel);
    	} else {
			$scope.indexArrayList[rel] = true;
		}
		
		$timeout(function(){
					$scope.updateErrorCount('MainInsuredQuestionnaire')
					$scope.refresh();
				},50)
	
		var carouselTabSection = 'carousel'+'-'+index+'-'+rel;
		$scope.carouselTabSection = carouselTabSection;
		model=model.split(".");
		var tab = model[1];
		$scope.carouselId = "";
		for (var i = 0; i <= 200; i++) {
			if (i == index) {				
				
				$scope.tabedIndex = index +'-'+ rel;
				$('#tabIndex'+'_'+i+'_'+rel).removeClass("QuestionnaireCheckbox");	
				$('#tabIndex'+'_'+index+'_'+rel).addClass("checkmarkActive");
				var tabIndexid='#tabIndex'+'_'+index+ "_" + rel;				
				$scope.selectedTabList.push(tabIndexid);
				if(tab=="HealthDetails")
					$scope.QuestionnaireObj.HealthDetails.Questions[questionNumber].options[index].details = 'true';
			
			    else if(tab=="LifestyleDetails")
					$scope.QuestionnaireObj.LifestyleDetails.Questions[questionNumber].options[index].details = 'true';
			 
				else if(tab=="Other")			
					$scope.QuestionnaireObj.Other.Questions[questionNumber].options[index].details = 'true';
						
			} else {
				
				$scope.tabedIndex = index +'-'+ rel;
				$('#tabIndex'+'_'+i+'_'+rel).addClass("QuestionnaireCheckbox");
			}
		}

		$scope.carouselId = rel;
	}	

$scope.displayCheckMark=function(start,len){
	$scope.isValid=false;
	if($scope.isBothWeight || $scope.isValidField){
		$scope.isValid=true;
	}
	$timeout(function () {
	if( $scope.errorCount || ($scope.isValid)){       
		$('#headerText_'+start).addClass("diableCheckMark");
			for(var i=0;i<=len;i++){
				if(i!=start)
					$('#headertabs_'+i).addClass("QuestionnaireCheckbox");
			}
	}
       else{
        // $('#headerText_'+start).removeClass("diableCheckMark");	
		for(var i=0;i<=len;i++){
				if(i!=start)
					$('#headertabs_'+i).removeClass("QuestionnaireCheckbox");
			}
	}
	}, 50)
}

$scope.displayCarouselErrorMessage=function(isCarousel,name){    
	if(isCarousel){		
        $scope[name] = false;
    }
      $timeout(function () {
            $scope.updateErrorCount('MainInsuredQuestionnaire');
            $scope.refresh();
        }, 50)
}



$scope.patternShow=false;

$scope.validateDate = function(input,name) 
{    
    var datePatternValidation = "datePatternValidation"+"_"+name;    
    var patternShow = "patternShow" + "_" + name;
   
    $scope[datePatternValidation]="";
    $scope[patternShow]=false;
	$scope.isValidField=false;
   
   if(input!=undefined)
    {
     var inputData = input.split('/');
       if(inputData.length>3){
      $scope[datePatternValidation] ="ng-invalid ng-invalid-pattern";
      $scope[patternShow]=true;
	   $scope.isValidField=true;
		}
 else
 {
    if(inputData.length > 2){
    input= inputData[1]+'/' + inputData[0] + '/' + inputData[2];
    }
    var date = new Date(input);   
    
    if(date.getFullYear() >= 2500 && date.getFullYear() <= 2699)
    {     
    var datePattern = date.getDate() === +inputData[0] && date.getMonth() + 1 === +inputData[1] &&  date.getFullYear() === +inputData[2];
    if(!datePattern)	
     {   
     $scope[datePatternValidation] ="ng-invalid ng-invalid-pattern";
     $scope[patternShow]=true;
	    $scope.isValidField=true;
	 
     }
    }		
       else
        {
            $scope[datePatternValidation]="ng-invalid ng-invalid-pattern";
            $scope[patternShow]=true; 
			    $scope.isValidField=true;
        }
 }
 
	//   if(datePattern){

	// 		var day = (inputData[0].length < 2) ? 0+''+inputData[0] : inputData[0];	
	// 		var month = (inputData[1].length < 2) ? 0+''+inputData[1] : inputData[1];	
	// 		input= day+'/' + month + '/' + inputData[2];
	// 		$scope.QuestionnaireObj.Other.Questions[2].details = input;
	// 	}
    }
            $timeout(function () {
            $scope.updateErrorCount('MainInsuredQuestionnaire');
            $scope.refresh();
        }, 50)
}


$scope.checkWeightMandatory=function(){    
$scope.isIncWeight=true;
$scope.isBothWeight=false;
$scope.isDecweight=false;
}

$scope.mandatoryFieldCheck= function(Question){
$scope.isBothWeight=false;
$scope.isIncWeight=false;
$scope.isDecweight=false;

var radio =$scope.QuestionnaireObj.LifestyleDetails.Questions[28].details;
var incrWeight=$scope.QuestionnaireObj.LifestyleDetails.Questions[29].details;
var decWeight=$scope.QuestionnaireObj.LifestyleDetails.Questions[30].details;

if(radio=='Yes' && (incrWeight=='' || incrWeight== undefined) && (decWeight=='' || decWeight==undefined ))
$scope.isIncWeight=true;

else if(radio=='Yes' && (incrWeight!='' && incrWeight!=undefined) && (decWeight!='' && decWeight!=undefined))
 $scope.isBothWeight=true;    

     $timeout(function () {
            $scope.updateErrorCount('MainInsuredQuestionnaire');
            $scope.refresh();
        }, 50)
}

$scope.formatLabel = function (model, options, type) {
	

        if (options && options.length > 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code) {
                    return options[i].value;
                }
            }
        }
		    $timeout(function () {
            $scope.updateErrorCount('MainInsuredQuestionnaire');
            $scope.refresh();
        }, 50)
}
 $scope.onSelectTypeHead = function (id) {
	 
        $timeout(function () {
            $('#' + id).blur();
        }, 300);		
    }
	
$scope.addPolicyNumbers = function() {
	$scope.policyNumbers = [];
	for(var x=0; x<3; x++){
		if($scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[x] != undefined){
			if($scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[x].orderId != undefined && $scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[x].orderId !=''){
			
			var found = $scope.policyNumbers.some(function (el) {
				return el.number === $scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[x].orderId;
			});
			if(!found){
				$scope.policyUniqueNumberCheck = false;
				$scope.policyNumbers.push({
					"id" : x,
					"number" : $scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[x].orderId
					})
				}else{ 
					//$scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[x].orderId = '';
					$scope.policyUniqueNumberCheck = true;
					$scope.prevErorrCountUpdate();
					return;
				}	
			}
		}	
	}
	$timeout(function () {
            $scope.updateErrorCount('MainInsuredQuestionnaire');
            $scope.refresh();
    }, 50) 
}


$scope.checkCarouselSelect=function(questionNumber,radioQtnNo,isHealth){
    
	var  radioQuestion="radioQuestion" + radioQtnNo;
	$scope[radioQuestion]=false;

if(isHealth)
{
if($scope.QuestionnaireObj.HealthDetails.Questions[questionNumber-1].details == 'Yes'){
		for(var i=0;i<$scope.QuestionnaireObj.HealthDetails.Questions[questionNumber].options.length;i++){
			if($scope.QuestionnaireObj.HealthDetails.Questions[questionNumber].options[i].details == "true"){ 					
			   $scope[radioQuestion] = true;
			   return;
			   //$scope.apply();
		   }
		}
	}
}
else
{
	if($scope.QuestionnaireObj.Other.Questions[questionNumber-1].details == 'Yes'){
		for(var i=0;i<$scope.QuestionnaireObj.Other.Questions[questionNumber].options.length;i++){
			if($scope.QuestionnaireObj.Other.Questions[questionNumber].options[i].details == "true"){ 					
			   $scope[radioQuestion] = true;
			   return;
			   //$scope.apply();
		   }
		}
	}
}
}

$scope.previousPolicyFlag = false;

$rootScope.checkPrevious = function(param){
	$scope.previousPolicyFlag = param;
	if($scope.previousPolicyFlag){
		UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        angular.element('#' + $scope.selectedTabId + "_a").trigger('click');

        $timeout(function () {
            $scope.updateErrorCount('MainInsuredQuestionnaire');
            $scope.refresh();
        }, 50)
	} else {
		 $timeout(function () {
            $scope.updateErrorCount('MainInsuredQuestionnaire');
            $scope.refresh();
        }, 50)
	}	
}

$scope.clearRowValues = function(num){
	if($scope.policyNumbers != undefined && $scope.policyNumbers != "undefined"){
		var found = $scope.policyNumbers.some(function (el) {
			return el.number === $scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[num].orderId;
		});
		
		if(found){
			$scope.policyNumbers.splice(num,1);
		}
	}
		$scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[num].orderId = '';
		$scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[num].basicSumAssured = '';
		$scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[num].companyName = '';
		$scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[num].effectiveDate = "";
			
	}

$scope.prevErorrCountUpdate = function(){
    if($scope.PreviousPolicyQuestionnaire!==undefined){
    if($scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[0]!==undefined){        
        if(parseInt($scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[0].basicSumAssured)===0){
            $scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[0].basicSumAssured='';
        }        
    }
    if($scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[1]!==undefined){
        if($scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[1].detailsOther == '1'){
        if(parseInt($scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[1].basicSumAssured)===0){
            $scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[1].basicSumAssured='';
        }
        }
    }
    if($scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[2]!==undefined){
        if($scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[2].detailsOther == '2'){
        if(parseInt(PreviousPolicyQuestionnaire.PreviouspolicyHistory[2].basicSumAssured)===0){
            $scope.PreviousPolicyQuestionnaire.PreviouspolicyHistory[2].basicSumAssured='';
        }
        }
    }
    }
	$timeout(function () {
            $scope.updateErrorCount('MainInsuredQuestionnaire');
            $scope.refresh();
        }, 50)
}

$scope.disableEnable = function(ids){
	if(ids == '2B'){
		$scope.disableField1 = false;
		$scope.disableField2 = true;
		$scope.Question2textbox = "";
	}else if(ids == '2C'){
		$scope.disableField1 = true;
		$scope.disableField2 = false;
		$scope.Question2dropdown = "";
	}else{
		$scope.disableField1 = true;
		$scope.disableField2 = true;
		$scope.Question2textbox = "";
		$scope.Question2dropdown = "";
	}
  }
	
 $scope.spanDisabled = function(){
	 $('span').addClass('pointer-none');
 }
}