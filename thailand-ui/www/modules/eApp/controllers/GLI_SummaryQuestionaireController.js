/*
 *Copyright 2015, LifeEngage 
 */
/*Name:SummaryController
 * For implementing GLI specific changes
 */
storeApp.controller('GLI_SummaryQuestionaireController', GLI_SummaryQuestionaireController);
GLI_SummaryQuestionaireController.$inject = ['$timeout', '$route', 'globalService', '$rootScope', '$scope', '$location', '$compile', '$routeParams', 'DataService', 'RuleService', 'LookupService', 'DocumentService', 'FnaVariables', 'GLI_FnaService', '$translate', 'UtilityService', '$debounce', 'AutoSave', '$controller', 'EappService', 'EappVariables', 'UserDetailsService', 'PersistenceMapping', 'GLI_DataService', 'GLI_EappService', 'GLI_RuleService', 'GLI_EappVariables', '$filter'];

function GLI_SummaryQuestionaireController($timeout, $route, globalService, $rootScope, $scope, $location, $compile, $routeParams, DataService, RuleService, LookupService, DocumentService, FnaVariables, GLI_FnaService, $translate, UtilityService, $debounce, AutoSave, $controller, EappService, EappVariables, UserDetailsService, PersistenceMapping, GLI_DataService, GLI_EappService, GLI_RuleService, GLI_EappVariables, $filter) {
    $controller('SummaryController', {
        $timeout: $timeout,
        $route: $route,
        globalService: globalService,
        $rootScope: $rootScope,
        $scope: $scope,
        $location: $location,
        $compile: $compile,
        $routeParams: $routeParams,
        DataService: DataService,
        RuleService: RuleService,
        LookupService: LookupService,
        DocumentService: DocumentService,
        FnaVariables: FnaVariables,
        FnaService: GLI_FnaService,
        $translate: $translate,
        UtilityService: UtilityService,
        $debounce: $debounce,
        AutoSave: AutoSave
    });
    var signature = "Signature";
    var outputFormat = "image/png";
    $scope.appDateFormat = rootConfig.appDateFormat;
    $scope.gliFormatedDate = "";
    $scope.appDateTimeFormat = rootConfig.appDateTimeFormat;
    $scope.isSPAJNoAllocated = false;
    $scope.isRDSUser = UserDetailsService.getRDSUser();
    $scope.setSaveproposalDisableFlag = true;
    $scope.showErrorCount = true;
    $scope.healthRiderAvailable = false;
    if (!(rootConfig.isDeviceMobile) && (typeof $scope.DocumentType == "undefined")) {
        $scope.DocumentType = "Signature#QuestSignature";
    }
    $scope.spajNumber = EappVariables.EappKeys.Key21;
    $scope.isDeviceWeb = true;
    if (!$scope.spajNumber) {
        $scope.spajNumber = "******";
    } else {
        $scope.isSPAJNoAllocated = true;
    }
    if (rootConfig.isDeviceMobile) {
        //disabling canvas based on the declaration check
        $scope.questionaireCanvas = true;
        //disabling summary page buttons based on the signature check
        $scope.questionaireButtonDisable = false;
    } else {
        $scope.questionaireCanvas = false;
        $scope.questionaireButtonDisable = false;
    }
    $rootScope.selectedPage = "Summary";
    var signaturePadQuestion;
    //to translte rider isnured type
    $scope.translate = function (riderType) {
        return $translate.instant("eapp." + riderType);
    }
    $rootScope.editPageNavigation = [];
    $scope.summaryEdit = function (linkTab, linkSubTab, subTab, subIndex) {
        // isRootNav will check for whether the nav
        // click is coming from direct
        // main root tabs/subtabs
        $scope.isRootNav = false;
        $scope.eAppParentobj.FromSummaryFlag = true;
        $scope.refresh();
        $rootScope.editPageNavigation = [subTab, subIndex];
        $('#' + linkTab).find("li a[rel='" + linkSubTab + "']").parent('li').trigger('click');
    };


$scope.questionnaireRedirect= function(linkTab, linkSubTab, subTab, subIndex){

    $timeout(function(){
		$('#questionnaireTabName').trigger('click');		
	},10);
    
}

    //creating beneficaiaries with inusrance engagement letter
    function displayInsuranceEngagement() {
        //pushing those beneficiary who has the insurance engagemnt letter.In UI apply ngrepeat to this array
        $scope.InsuranceEngagementbeneficiaryList = [];
        $scope.showEngagementLetter = false;
        for (var beneficiaryObj in $scope.LifeEngageProduct.Beneficiaries) {
            var sampleBeneficiary = $scope.LifeEngageProduct.Beneficiaries[beneficiaryObj];
            if (sampleBeneficiary.hasEngagementLetter == "true" || sampleBeneficiary.hasEngagementLetter == true) {
                $scope.showEngagementLetter = true;
                $scope.InsuranceEngagementbeneficiaryList.push(sampleBeneficiary);
            }
        }
        $scope.refresh();
        $scope.updateErrorCount($scope.selectedTabId);
    }
    //chekcing nationality of maininsured,proposer,additional isnured for showing specific declaration
    function nationalityCheckForDeclaration() {
        $scope.haveForeignerQuestioanare = false;
        if ($scope.LifeEngageProduct.Proposer.BasicDetails.nationality != "Indonesia") {
            $scope.haveForeignerQuestioanare = true;
        }
        if ($scope.LifeEngageProduct.Insured.BasicDetails.nationality && $scope.LifeEngageProduct.Insured.BasicDetails.nationality != "Indonesia") {
            $scope.haveForeignerQuestioanare = true;
        }
    }

    $scope.Initialize = function () {
        $scope.QuestionnaireObj = angular.copy($scope.LifeEngageProduct.Insured.Questionnaire);
        $scope.payerQuestionnaireObj = angular.copy($scope.LifeEngageProduct.Payer.Questionnaire);
        $scope.FactaQuestionnaireObj = angular.copy($scope.LifeEngageProduct.Proposer.Questionnaire);
        $scope.PrevPolicyQuestionnaireObj = angular.copy($scope.LifeEngageProduct.Insured.Questionnaire);
  
        $(window).scrollTop(0);
        
$scope.LifeEngageProduct.PreviouspolicyHistory[0].summaryeffectiveDate =InternalToExternalSummary($scope.LifeEngageProduct.PreviouspolicyHistory[0].effectiveDate);
        if($scope.LifeEngageProduct.PreviouspolicyHistory[1] && $scope.LifeEngageProduct.PreviouspolicyHistory[1].orderId &&
         $scope.LifeEngageProduct.PreviouspolicyHistory[1].orderId !=undefined && $scope.LifeEngageProduct.PreviouspolicyHistory[1].orderId != ''){
		   $scope.perviousPolValueEmpty = true;
	   }  else{
		   $scope.perviousPolValueEmpty = false;
	   }

        if($scope.LifeEngageProduct.PreviouspolicyHistory[2] && $scope.LifeEngageProduct.PreviouspolicyHistory[2].orderId &&
         $scope.LifeEngageProduct.PreviouspolicyHistory[2].orderId !=undefined &&  $scope.LifeEngageProduct.PreviouspolicyHistory[2].orderId != ''){
		   $scope.perviousPolValuetwoEmpty = true;
	   }  else{
		   $scope.perviousPolValuetwoEmpty = false;
	   }

        angular.forEach($scope.FactaQuestionnaireObj.Other.Questions, function (value, Key) {
            if($scope.FactaQuestionnaireObj.Other.Questions[Key].details == "NoLonger"){
                $scope.FactaQuestionnaireObj.Other.Questions[Key].details = "Yes, but No longer";
            }

        })

        if(EappVariables.EappKeys.Key15 == 'Submitted' || (typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null)){
            $("#QuestionnaireSummarySubTab .Summary-Edit").addClass("displayNone");
        }else{
            $("QuestionnaireSummarySubTab .Summary-Edit").removeClass("displayNone");
        }

        var ParentQuestionvalue = $(".questionnaireParentQuestion div").html();
        var questionaireSubCategoriesValue = $(".questionaireSubCategories div").html();  
        var trimquestionaireSubCategoriesValue=  $.trim(questionaireSubCategoriesValue);                           
       // var btmpadDivEmpty =   document.getElementsByClassName("btmPadd11").innerHTML;
        if(ParentQuestionvalue =="" || ParentQuestionvalue == undefined){
            $(".questionnaireParentQuestion").css("display", "none");
        }else{
            $(".questionnaireParentQuestion").css("display", "block");
        }
        // if(btmpadDivEmpty == " "){
        //       $(".btmPadd11").css("display", "none");
        // }else{
        //      $(".btmPadd11").css("display", "block");
        // }
        // if(trimquestionaireSubCategoriesValue == "" || trimquestionaireSubCategoriesValue == undefined){
        //     $(".questionaireSubCategories").css("display", "none");
        // }else{
        //      $(".questionaireSubCategories").css("display", "block");
        // }

        
        $timeout(function(){
            $scope.RidersInIllustration = $scope.LifeEngageProduct.Product.RiderDetails;
            for(var i = 0;i<$scope.RidersInIllustration.length;i++){
                if($scope.RidersInIllustration[i].riderPlanName == "HS Extra" 
                    || $scope.RidersInIllustration[i].riderPlanName == "HB(A)"  
                    || $scope.RidersInIllustration[i].riderPlanName == "CI Extra"
                    || $scope.RidersInIllustration[i].riderPlanName == "DD") {
                    $scope.healthRiderAvailable = true;
                }

                if($scope.RidersInIllustration[i].riderPlanName == "PB"){
                    $scope.showPayerDetails = true;
                }
                
            }
        },50)


        $scope.$parent.disableProceedButtonCommom = false;
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
        }
        $scope.LifeEngageProduct = EappVariables.getEappModel();
        $scope.familyHistory = angular.copy($scope.LifeEngageProduct.Insured.Questionnaire.FamilyHealthHistory);
      //  for (var i = 0; i < $scope.familyHistory.length; i++) {
        //    $scope.familyHistory[i].relationshipWithLifeAssured = translateMessages($translate, //$scope.familyHistory[i].relationshipWithLifeAssured);
       // }
        //$scope.eAppParentobj.PreviousPage   = "QuestionnaireSummarySubTab_unload";
        $scope.eAppParentobj.PreviousPage = "";
        if (EappVariables.EappKeys.Key15 == "Confirmed" && $scope.$parent.LifeEngageProduct.Declaration.confirmSignature == true) {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = true;
        } else {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
        }
        if (typeof $scope.DocumentType == "undefined") {
            $scope.DocumentType = "Signature#QuestSignature";
        }
        //function call to show/hide questiannoare tab
        if (typeof $scope.LifeEngageProduct.showHideQuestaionnaireTab == "undefined") {
            $scope.LifeEngageProduct.showHideQuestaionnaireTab = summayQuestionnaireTabShowHide();
            $scope.$parent.LifeEngageProduct.showHideQuestaionnaireTab = $scope.LifeEngageProduct.showHideQuestaionnaireTab;
        }
        if ($scope.LifeEngageProduct.Declaration.questionaireSignDate && $scope.LifeEngageProduct.Declaration.questionaireSignDate != "") {
            if (!$scope.isRDSUser) {
                $scope.spajQuestionnaireSignDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.questionaireSignDate), $scope.appDateTimeFormat);
            } else {
                $scope.questionaireSignDate = $scope.LifeEngageProduct.Declaration.questionaireSignDate;
            }
        }
        $scope.LifeEngageProduct.LastVisitedUrl = "4,2,'QuestionnaireSummarySubTab',true,'QuestionnaireSummarySubTab','eAppSummaryQuestionnaire'";
        $scope.eAppParentobj.nextPage = "4,3,'DeclarationAndAuthorizationTab',true,'DeclarationAndAuthorizationTab',''";
        $scope.eAppParentobj.CurrentPage = "";
        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[1] < 2 && subIndex[0] <= $scope.LifeEngageProduct.LastVisitedUrl[0]) {
                $scope.LifeEngageProduct.LastVisitedIndex = "4,2";
            }
        }
        //For disabling and enabling the tabs.And also to know the last visited page.---ends
        $timeout(function () {
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission") {
                $scope.setSaveproposalDisableFlag = false;
                UtilityService.disableAllActionElements();
            }else if (typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) {
                UtilityService.disableAllActionElements();
            }
            UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
            $scope.updateErrorCount($scope.selectedTabId);
            $scope.showErrorCount = true;
            if (!rootConfig.isDeviceMobile && $scope.isRDSUser) {
                $scope.isDeviceWeb = false;
            }

            $(window).scrollTop(0);
        }, 0);

        // For disabling and enabling Foreigner Questionnaire summary depending on nationality
//        if ($scope.LifeEngageProduct.Proposer.CustomerRelationship.isPayorDifferentFromInsured == 'Yes') {
//            var insuredNationality = $scope.LifeEngageProduct.Insured.BasicDetails.nationality;
//        } else {
//            var insuredNationality = $scope.LifeEngageProduct.Proposer.BasicDetails.nationality;
//        }
        // Hide Main Insured and additional insured tab in questionnaire summary if all questions with No option selected
//        if (typeof insuredNationality != "undefined" && insuredNationality != "") {
//            $scope.isInsuredForeigner = false;
//            if (insuredNationality != "Indonesia") {
//                $scope.isInsuredForeigner = true;
//            }
//        }
        //chekcing nationality of maininsured,proposer,additional isnured for showing specific declaration
        nationalityCheckForDeclaration();
        $scope.DocumentType = "Signature#QuestSignature";
        if ($scope.LifeEngageProduct.Declaration.questionaireDeclaration != "Yes" && (rootConfig.isDeviceMobile)) {
            $scope.questionaireCanvas = true;
        }
        $scope.agentDetail = UserDetailsService.getUserDetailsModel();
        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        $scope.updateErrorCount($scope.selectedTabId);
        $scope.showErrorCount = true;
        if ($scope.LifeEngageProduct.Declaration.isFinalSubmit) {
            $scope.isSTPSuccess = true;
            $scope.isSTPExecuted = true;
            $scope.STPMessage = translateMessages($translate, "stpRulePassed");
        } else {
            $scope.isSTPSuccess = false;
            $scope.isSTPExecuted = false;
        }
        $scope.proposalId = EappVariables.EappKeys.Key4;
        $scope.medicalFlag = hasMedicalQues();
        //calling function to retrieve insurance engagement forms.
        displayInsuranceEngagement();
    };
    var signatureString;
    /*$scope.loadSignature= function(base64)
         {
             if(base64 && base64!=''){
            	 signatureString=base64;
             }
             if(angular.element('#questionaireCanvas')[0])
             {
            	 signaturePadQuestion=new SignaturePad(angular.element('#questionaireCanvas')[0].querySelector("canvas"));
                 var dpr = window.devicePixelRatio;
                 window.devicePixelRatio = 1;
                 if(signaturePadQuestion){
					signaturePadQuestion.clear();	
                	if(signatureString)
                		signaturePadQuestion.fromDataURL(signatureString);
                 }
                 else{
                 	if(!(rootConfig.isDeviceMobile)){
                 		$scope.signaturePad.clear();
                 	}
				 }
                 window.devicePixelRatio = dpr;

                 if (signatureString && (rootConfig.isDeviceMobile)) {
                     $scope.questionaireButtonDisable =true;
                     $scope.questionaireCanvas=true;
                 }
                 else{
                	 if($scope.LifeEngageProduct.Declaration.questionaireSignDate && $scope.LifeEngageProduct.Declaration.questionaireSignDate!=''){
                		 $scope.questionaireButtonDisable =false;
                         $scope.questionaireCanvas=false;
                	 }
                 }
                 signature='';
                 $scope.refresh();
             }
             else
             {
                 // $timeout($scope.loadSignature(),10);

             }
         }*/
    $scope.okClick = function () {};
    //checking hasMedicalQues>0
    function hasMedicalQues() {
        var medicalArr = [];
        $scope.medicalFlag = false;
        if ($scope.LifeEngageProduct.hasMedicalQuestions && ($scope.LifeEngageProduct.hasMedicalQuestions != "0" || $scope.LifeEngageProduct.hasMedicalQuestions != 0) && $scope.LifeEngageProduct.hasMedicalQuestions != "") {
            medicalArr = JSON.parse($scope.LifeEngageProduct.hasMedicalQuestions).selectedArr;
        }
        if (medicalArr.length > 0) {
            $scope.medicalFlag = true;
        } else {
            $scope.medicalFlag = false;
        }
        return ($scope.medicalFlag);
    };

    // Not using
    /*$scope.isPolicyHolderVisible = function () {
        if ($scope.LifeEngageProduct.hasInsuranceEngagementLetter == true) {
            return true;
        } else {
            var returnData;
            returnSectionVisible($scope.LifeEngageProduct.isProposerForeigner, function (returnValueProposerForeigner) {
                if (returnValueProposerForeigner == false) {
                    returnSectionVisible($scope.LifeEngageProduct.hasBeneficialOwnerForm, function (returnValueBeneficialOwnerForm) {
                        returnData = returnValueBeneficialOwnerForm;
                    });
                } else {
                    returnData = returnValueProposerForeigner;
                }
            });
            return returnData;
        }
    };*/

    function returnSectionVisible(section, successcallback) {
        if (section && section != "") {
            if (JSON.parse(section).selectedArr.length > 0) {
                successcallback(true);
            } else {
                successcallback(false);
            }
        } else {
            successcallback(false);
        }
    };
    $scope.clearSignature = function (signatureId) {
        if (signatureId == "questionaireCanvas") {
            $scope.questionaireButtonDisable = false;
            if ($scope.LifeEngageProduct.Declaration.questionaireDeclaration == 'Yes') $scope.questionaireCanvas = false;
            if ((rootConfig.isDeviceMobile)) {
                signaturePadQuestion.clear();
            } else {
                $scope.signaturePad.clear();
            }
        }
        $scope.refresh();
    };
    $scope.getSignature = function (pageName, successCallback) {
        if ((rootConfig.isDeviceMobile)) {
            EappService.getSignature(EappVariables.EappKeys.TransTrackingID, signature, function (sigdata) {
                EappVariables.Signature = sigdata;
                if (sigdata.length > 0) {
                    for (var i = 0; i < sigdata.length; i++) {
                        if (sigdata[i].documentName.indexOf("Signature") >= 0) {
                            signatureType = sigdata[i].documentName.split("_")[2];
                            if (signatureType.localeCompare(pageName) == 0) {
                                if ($scope.setSaveproposalDisableFlag) {
                                    $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
                                }
                                $scope.$parent.refresh();
                                EappService.convertImgToBase64(sigdata[i].base64string, successCallback, outputFormat);
                            } else {
                                if (i == (sigdata.length - 1)) {
                                    successCallback()
                                }
                            }
                        }
                    }
                } else {
                    successCallback();
                }
            });
        } else {
            var requirementObject = CreateRequirementFile();
            requirementObject.RequirementFile.identifier = EappVariables.EappKeys.TransTrackingID;
            for (var i = 0; i < EappVariables.EappModel.Requirements.length; i++) {
                if (EappVariables.EappModel.Requirements[i].requirementType === "Signature") {
                    for (var j = 0; j < EappVariables.EappModel.Requirements[i].Documents.length; j++) {
                        var value = $scope.DocumentType.split("#");
                        if (EappVariables.EappModel.Requirements[i].Documents[j].documentName.indexOf(value[1]) > 0) {
                            requirementObject.RequirementFile.documentName = EappVariables.EappModel.Requirements[i].Documents[j].documentName;
                            requirementObject.RequirementFile.requirementName = EappVariables.EappModel.Requirements[i].requirementName;
                            PersistenceMapping.clearTransactionKeys();
                            EappService.mapKeysforPersistence();
                            var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
                            DataService.getDocumentsForRequirement(transactionObj, getDocsSuccess, $scope.errorCallback);
                        }
                    }
                }
            }
        }
    }
    $scope.errorCallback = function () {}

    function getDocsSuccess(sigdata) {
        EappVariables.Signature = sigdata[0].base64string;
        if ($scope.signaturePad) $scope.signaturePad.fromDataURL(sigdata[0].base64string);
    }
    $scope.confirmProposal = function (sigData) {
        if ($scope.LifeEngageProduct.Insured.IncomeDetails && !($scope.LifeEngageProduct.Insured.IncomeDetails.annualIncome)) {
            $scope.LifeEngageProduct.Insured.IncomeDetails.annualIncome = "";
        }
        // added for prev button in payment
        // authorisation
        $scope.confirmForPaymentAuth = true;
        var requirementType = "Signature";
        var index = "";
        var requirementName = "Signature";
        var partyIdentifier = "";
        var isMandatory = "";
        var docArray = [];
        var documentType = $scope.DocumentType;
        if ((rootConfig.isDeviceMobile)) {
            EappService.getRequirementFilePath(sigData, index, requirementName, requirementType, documentType, function (filePath) {
                if ((rootConfig.isDeviceMobile) || rootConfig.isOfflineDesktop) {
                    filePath = "../" + filePath;
                }
                EappService.saveRequirement(filePath, requirementType, partyIdentifier, isMandatory, docArray, documentType, function () {
                    if ($scope.canvasId == "questionaireCanvas") {
                        $scope.questionaireCanvas = true;
                        $scope.questionaireButtonDisable = true;
                    }
                    $scope.refresh();
                }, $scope.onConfirmError);
            });
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
        } else {
            EappService.saveRequirement(sigData, requirementType, partyIdentifier, isMandatory, docArray, documentType, function () {
                $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
            }, function () {
                $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
            });
        }
    };
    $rootScope.validationBeforesave = function (value, successcallback) {
        $scope.validateBeforeSaveDetails(value, successcallback);
    };
    $scope.SPAJAllocation = function (sucess) {
        // SPAJ allocation logic
        if (rootConfig.isDeviceMobile) {
            PersistenceMapping.clearTransactionKeys();
            var transactionObj = PersistenceMapping.mapScopeToPersistence({});
            GLI_DataService.retrieveSPAJCount(transactionObj.Key11, function (count) {
                if (count > 0) {
                    var agentCode = transactionObj.Key11; // TODO: previously it was $scope.agentDetail.agentCode; but,sometimes getting undefind dirty fix changed to KEy11
                    GLI_DataService.retrieveSPAJNo(agentCode, function (data) {
                        if (!EappVariables.EappKeys.Key21 || EappVariables.EappKeys.Key21 == '') {
                            EappVariables.EappKeys.Key21 = data[0].SPAJNo;
                            $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp.spajAllocatedMessage") + " " + data[0].SPAJNo, translateMessages($translate, "fna.ok"));
                        }
//                        EappVariables.EappKeys.Key15 = "Confirmed";
                        //EappVariables.eAppProceedingDate = UtilityService.getFormattedDate();
                        $scope.isSPAJNoAllocated = true;
                        $scope.spajNumber = EappVariables.EappKeys.Key21;
                        $scope.refresh();
                        $scope.LifeEngageProduct.LastVisitedUrl = "4,3,'DeclarationAndAuthorizationTab',true,'DeclarationAndAuthorizationTab',''";
                        sucess();
                    }, $scope.onRetrieveAgentProfileError);
                } else {
                    $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp.SPAJExhaustMessage"), translateMessages($translate, "fna.ok"), $scope.okClick);
                }
            }, $scope.onRetrieveAgentProfileError);
        } else {
            if ($scope.isRDSUser) {
                if (!EappVariables.EappKeys.Key21 || EappVariables.EappKeys.Key21 == '') {
                    EappVariables.EappKeys.Key21 = $scope.LifeEngageProduct.Product.policyDetails.policyNumber;
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp.spajAllocatedMessage") + " " + $scope.LifeEngageProduct.Product.policyDetails.policyNumber, translateMessages($translate, "fna.ok"));
                    $scope.spajNumber = EappVariables.EappKeys.Key21;
                }
            } else {
                PersistenceMapping.clearTransactionKeys();
                var transactionObj = PersistenceMapping.mapScopeToPersistence({});
                transactionObj.Key18 = 1;
                GLI_DataService.retrieveSPAJNumbers(transactionObj, function (data) {
                    if (!EappVariables.EappKeys.Key21 || EappVariables.EappKeys.Key21 == '') {
                        EappVariables.EappKeys.Key21 = data.spajNos[0];
                        $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp.spajAllocatedMessage") + " " + data.spajNos[0], translateMessages($translate, "fna.ok"));
                        $scope.spajNumber = EappVariables.EappKeys.Key21;
                    }
                }, $scope.onRetrieveAgentProfileError);
            }
//            EappVariables.EappKeys.Key15 = "Confirmed";
            //EappVariables.eAppProceedingDate = UtilityService.getFormattedDate();
            $scope.refresh();
            $scope.LifeEngageProduct.LastVisitedUrl = "4,3,'DeclarationAndAuthorizationTab',true,'DeclarationAndAuthorizationTab',''";
            sucess();
        }
    }
    var load = $scope.$parent.$on('QuestionnaireSummarySubTab', function (event, args) {
        $scope.selectedTabId = args[0];
        if ($scope.LifeEngageProduct.LastVisitedIndex && args[1]) {
            $scope.LifeEngageProduct.LastVisitedIndex = args[1];
        }
        $scope.Initialize();
        $rootScope.validationBeforesave = function (value, successcallback) {
            $scope.validateBeforeSaveDetails(value, successcallback);
        };
    });
    $scope.validateBeforeSaveDetails = function (value, successcallback) {
        if (rootConfig.isDeviceMobile) {
            successcallback();
        } else {
            successcallback();
        }
    }
    $scope.confirmDeclaration = function (canvasId) {
        $scope.canvasId = canvasId;
        if (canvasId == "questionaireCanvas") {
            if ((rootConfig.isDeviceMobile)) {
                if ($scope.errorCount == 0 && ($scope.LifeEngageProduct.Declaration.questionaireSignPlace && $scope.LifeEngageProduct.Declaration.questionaireSignPlace != "")) {
                    if (signaturePadQuestion && !signaturePadQuestion.isEmpty()) {
                        $scope.LifeEngageProduct.Declaration.questionaireSignDate = UtilityService.getFormattedDate();
                        $scope.spajQuestionnaireSignDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.questionaireSignDate), $scope.appDateTimeFormat);
                        //if check added for spaj should be signed before confirming qstionare.
                        if ($scope.LifeEngageProduct.Declaration.spajSignDate != '') {
                            $scope.confirmProposal(signaturePadQuestion.toDataURL());
                        } else {
                            $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.mandatorySignNeededInSPAJ"), translateMessages($translate, "fna.ok"));
                        }
                    } else {
                        $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.declarationAgreedMessage"), translateMessages($translate, "fna.ok"));
                    }
                } else {
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp.placeOfSign"), translateMessages($translate, "fna.ok"));
                }
            } else {
                if ($scope.LifeEngageProduct.Declaration.questionaireDeclaration == 'Yes' && $scope.errorCount == 0) {
                    if (!$scope.isRDSUser) {
                        $scope.LifeEngageProduct.Declaration.questionaireSignDate = UtilityService.getFormattedDate();
                        if ($scope.isDeviceWeb) {
                            $scope.spajQuestionnaireSignDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.questionaireSignDate), $scope.appDateTimeFormat);
                        }
                    }
                    $scope.LifeEngageProduct.Declaration.confirmSignature = false;
                    if ($scope.signaturePad) $scope.confirmProposal($scope.signaturePad.toDataURL());
                }
            }
        }
    };
    $scope.updateErrorCount = function (id) {
        $rootScope.updateErrorCount(id);
        $scope.selectedTabId = id;
        $scope.eAppParentobj.errorCount = $scope.errorCount;
    }
    $scope.showErrorPopup = function (id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
    }

    /*
     * Used to show or hide the questionnaire tab under summary based on additonal questions selected or not.---starts
     */
    function summayQuestionnaireTabShowHide() {
        $scope.arrayIndex = 0;
        var questionnaireCheckValues = [$scope.LifeEngageProduct.isProposerForeigner
			                                , $scope.LifeEngageProduct.hasInsuredQuestions
			                                , $scope.LifeEngageProduct.hasBeneficialOwnerForm];
        if ($scope.LifeEngageProduct.hasInsuranceEngagementLetter == true || $scope.LifeEngageProduct.hasInsuranceEngagementLetter == "true") {
            return true;
        } else {
            var returnValue;
            loopArrayQuestionnaire(questionnaireCheckValues[$scope.arrayIndex], function (dataReturn) {
                returnValue = dataReturn;
            });
            return returnValue;
        }
    };

    function loopArrayQuestionnaire(arrayValue, successcallback) {
        var questionnaireCheckValues = [$scope.LifeEngageProduct.isProposerForeigner
			                                , $scope.LifeEngageProduct.hasInsuredQuestions
			                                , $scope.LifeEngageProduct.hasBeneficialOwnerForm];
        checkLength(arrayValue, function (returnValue) {
            if (returnValue) {
                successcallback(true);
            } else {
                if ($scope.arrayIndex == questionnaireCheckValues.length - 1) {
                    successcallback(false);
                } else {
                    $scope.arrayIndex++;
                    loopArrayQuestionnaire(questionnaireCheckValues[$scope.arrayIndex], successcallback);
                }
            }
        });
    };
    /*
     * Used to show or hide the questionnaire tab under summary based on additonal questions selected or not.---ends
     */
    var questionaireCanvasWatch = $scope.$watch('LifeEngageProduct.Declaration.questionaireDeclaration', function (value) {
        if ($scope.selectedTabId == "QuestionnaireSummarySubTab") {
            if (value == 'Yes') {
                $scope.questionaireCanvas = false;
            } else {
                if (signaturePadQuestion) {
                    signaturePadQuestion.clear();
                    $scope.questionaireCanvas = true;
                } else {
                    if (!(rootConfig.isDeviceMobile) && $scope.signaturePad) {
                        $scope.signaturePad.clear();
                    }
                }
            }
            $scope.refresh();
        }
    }, true);
    $scope.$on("$destroy", function () {
        if (this != null && typeof this !== 'undefined') {
            questionaireCanvasWatch();
            if (this.$$destroyed) return;
            while (this.$$childHead) {
                this.$$childHead.$destroy();
            }
            if (this.$broadcast) {
                this.$broadcast('$destroy');
            }
            this.$$destroyed = true;
        }
    });
    
    $scope.translateMappingFatca1=function(value){
        if(value==='Yes')
        return translateMessages($translate, "eapp_vt.fatca1yes");
        else if(value==='No')
        return translateMessages($translate, "eapp_vt.fatca1no");
    }
    
    $scope.translateMappingFatca2=function(value){
        if(value==='Yes')
        return translateMessages($translate, "eapp_vt.fatca2yes");
        else if(value==='No')
        return translateMessages($translate, "eapp_vt.fatca2no");
        else if(value==='Yes, but No longer')
        return translateMessages($translate, "eapp.FatcaQuestion2Option");
    }
    
    $scope.translateMappingFatca3=function(value){
        if(value==='Yes')
        return translateMessages($translate, "eapp_vt.fatca3yes");
        else if(value==='No')
        return translateMessages($translate, "eapp_vt.fatca3no");
    }
    
    $scope.translateMappingFatca4=function(value){
        if(value==='Yes')
        return translateMessages($translate, "eapp_vt.fatca4yes");
        else if(value==='No')
        return translateMessages($translate, "eapp_vt.fatca4no");
    }
    
    $scope.translateMappingFatca1Subquestion=function(value){
        if(value==='Yes')
        return translateMessages($translate, "eapp_vt.fatca1.1yes");
        else if(value==='No')
        return translateMessages($translate, "eapp_vt.fatca1.1no");
    }
    
    $scope.translateMapping=function(value){
        if(value==='Yes')
        return translateMessages($translate, "eapp_vt.questionnaireOptionYes");
        else
        return translateMessages($translate, "eapp_vt.questionnaireOptionNo");
    }
    
    $scope.translateInsuranceCompany=function(value){        
        if(value!==undefined && value!==""){
            for (var i = 0; i < $rootScope.insuranceCompany.length; i++) {
                if (value === $rootScope.insuranceCompany[i].code) {
                    value = $rootScope.insuranceCompany[i].value;
                }
            }        
        }        
        return value;
    }
    
    $scope.translateCompanyName=function(value){        
        if(value!==undefined && value!==""){
            for (var i = 0; i < $rootScope.companyName.length; i++) {
                if (value === $rootScope.companyName[i].code.trim()) {
                    value = $rootScope.companyName[i].value;
                }
            }        
        }        
        return value;
    }
    
    $scope.translateLabel=function(value){
        return translateMessages($translate, value);
    }
    
    //Display date in dd/mm/yyyy format
    $scope.formatDate=function(dob){
        return getFormattedDateDDMMYYYYThai(dob);
    }
} // Controller scope ends here