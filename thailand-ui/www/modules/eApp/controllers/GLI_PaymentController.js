'use strict';
storeApp.controller('GLI_PaymentController', GLI_PaymentController);
GLI_PaymentController.$inject = ['$rootScope', '$scope', 'DataService', 'GLI_DataService', 'LookupService', 'DocumentService', 'ProductService', '$compile', '$routeParams', '$location', '$translate', '$debounce', 'UtilityService', 'PersistenceMapping', 'AutoSave', 'globalService', 'EappVariables', 'EappService', 'PaymentService', '$timeout', 'GLI_EappService', 'UserDetailsService'];

function GLI_PaymentController($rootScope, $scope, DataService, GLI_DataService, LookupService, DocumentService, ProductService, $compile, $routeParams, $location, $translate, $debounce, UtilityService, PersistenceMapping, AutoSave, globalService, EappVariables, EappService, PaymentService, $timeout, GLI_EappService, UserDetailsService) {
    $scope.paymentinfo;
    $scope.numberPattern = rootConfig.numberPattern;
    $scope.alphanumericPattern = rootConfig.alphanumericPattern;
    $scope.showModel = false;
    $scope.isSingleTopup = false;
    $scope.payerNames = [];
    $scope.popupMessage = "";
    $scope.supportingbanks = ["BCA", "BNI", "Niaga"];
    $scope.creditCardDetail = {};
    $scope.creditCardDetail.creditCardNumberone = "";
    $scope.creditCardDetail.creditCardNumbertwo = "";
    $scope.creditCardDetail.creditCardNumberthree = "";
    $scope.creditCardDetail.creditCardNumberfour = "";
    $scope.mandiriCreditCardNumber1 = "";
    $scope.mandiriCreditCardNumber2 = "";
    $scope.mandiriCreditCardNumber3 = "";
    $scope.mandiriCreditCardNumber4 = "";
    $scope.onlineMessage = "";
    $rootScope.selectedPage = "Payment";
    $scope.isDeviceMobile = rootConfig.isDeviceMobile;
    $scope.submitPayment = false;
    $scope.companyCode="Generali Life(001)";
		
		/*condition for split payment radio button disable start*/
		if($scope.LifeEngageProduct.Payment.PaymentInfo[0].splitPaymentTst=="No" && $scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit == "SplitOne"){
			angular.element(document.querySelector("#eAppSplitPayment label:nth-of-type(2) .iradio")).addClass("eAppSplitPaymentDeactive");
			angular.element(document.querySelector("#eAppSplitPayment label:nth-of-type(1) .iradio")).addClass("eAppSplitonePaymentDeactive");
		}
	
		if($scope.LifeEngageProduct.Payment.PaymentInfo[0].splitPaymentTst=="Yes" && $scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit == "SplitOne" && $scope.LifeEngageProduct.splitOneAmountValidate!=$scope.LifeEngageProduct.Payment.PaymentInfo[0].premiumAmt){
			angular.element(document.querySelector("#eAppSplitPayment label:nth-of-type(2) .iradio")).addClass("eAppSplitonePaymentDeactive");
		}
		if($scope.LifeEngageProduct.Payment.PaymentInfo[0].paySplit == "SplitOne" && $scope.LifeEngageProduct.Payment.PaymentInfo[1].paySplit == "SplitTwo" ){
			angular.element(document.querySelector("#eAppSplitPayment label:nth-of-type(1) .iradio")).addClass("eAppSplitPaymentDeactive");
		}
		if(EappVariables.EappModel.Payment.FinancialInfo.skipPayment=="Yes")
		{
		angular.element(document.querySelectorAll("#eAppSkipPayment label:nth-of-type(1) .iradio")).addClass("eAppSkipPaymentDeactive");
		angular.element(document.querySelectorAll("#eAppSkipPayment label:nth-of-type(2) .iradio")).addClass("eAppSplitonePaymentDeactive");
		angular.element(document.querySelectorAll("#eAppSplitPayment label:nth-of-type(2) .iradio")).addClass("eAppSplitPaymentDeactive");	
		angular.element(document.querySelectorAll("#eAppSplitPayment label:nth-of-type(1) .iradio")).addClass("eAppSplitonePaymentDeactive");	
		}
		//redirect step 1 to step2 or Document upload
	$rootScope.step2ScreenLoad= function () {
		
		if($rootScope.skipOptionSelected==true) {
		 EappVariables.EappModel.Payment.FinancialInfo.skipPayment="Yes";	
		 $scope.savePayment();	
		 $scope.paintView(6,0,'DocumentUpload',true,'DocumentUpload','');
		} else {
		angular.element(document.querySelector("#PaymentDetailsSubTab .selector-icon li:nth-child(1)")).addClass("activated");
		angular.element(document.querySelector("#PaymentDetailsSubTab .selector-text li:nth-child(1)")).addClass("activated");
		$scope.paintView(5,2,'step2Payment',true,'tabsinner18','');
		}
		
	}
	/*condition for split payment radio button disable end */
    $scope.checkForBtnName = function () {
        return 'payNowOffline'
    }
        
    $scope.onSelectSplit= function() {
         $rootScope.splitOptionSelected=true;
    }
    
     $scope.onDisSelectSplit= function() {
        $rootScope.splitOptionSelected=false;
    }
	
	$scope.onSelectSkip= function() {
         $rootScope.skipOptionSelected=true;
		// $scope.paintView(6,0,'DocumentUpload',true,'DocumentUpload','');
    }
    
     $scope.onDisSelectSkip= function() {
        $rootScope.skipOptionSelected=false;
    }
    
    $scope.updatePaymentOptions = function () {
        $scope.paymentOptions = [];
        if ($scope.paymentinfo.paymentMethod == "OnlineOnline") {
            $scope.paymentOptions = $scope.onlinePaymentOptions;
        } else if ($scope.paymentinfo.paymentMethod == "Offline") {
            $scope.paymentOptions = $scope.offlinePaymentOptions;
        }
        $scope.paymentinfo.paymentType = "";
        $scope.updateErrorCount();
    }
    $scope.subTypeCheck = function () {
        for (i = 0; i <= $scope.paymentOptions.length; i++) {
            if ($scope.paymentinfo.paymentType == $scope.paymentOptions[i].code) {
                $scope.subPaymentType = $scope.paymentOptions[i].type;
                $scope.submitPayment = true;
                break;
            }
        }
    }
    $scope.onPaymentTypeChange = function () {
        $scope.subTypeCheck();
        if ($scope.paymentinfo.paymentType == "virtual_account") {
            $scope.paymentinfo.accountIdentifier = "00074-10-" + $scope.spajNo;
            $scope.paymentinfo.payerName = "PT. AJ. Generali Indonesia";
        } else if ($scope.paymentinfo.paymentType == "credit_card" && $scope.renewalPayemntType == "CreditCard") {
            $scope.paymentinfo.cardNumber = $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardNumber;
            if ($scope.LifeEngageProduct.Payment.RenewalPayment && $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName)
                $scope.paymentinfo.payerName = $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName.trim();
        } else if ($scope.paymentinfo.paymentType == "bank_auto_debit" && $scope.renewalPayemntType == "DebitAccount") {
            $scope.paymentinfo.bankName = $scope.LifeEngageProduct.Payment.RenewalPayment.bankName;
            $scope.paymentinfo.branchName = $scope.LifeEngageProduct.Payment.RenewalPayment.branchName;
            $scope.paymentinfo.payerName = $scope.LifeEngageProduct.Payment.RenewalPayment.accHolderName;
            $scope.paymentinfo.accountIdentifier = $scope.LifeEngageProduct.Payment.RenewalPayment.accountNumber;
        } else {
            $scope.paymentinfo.accountIdentifier = "";
            $scope.paymentinfo.cardNumber = "";
            $scope.paymentinfo.payerName = "";
        }
        $scope.paymentinfo.receiptNumber = "";
        $scope.paymentinfo.notes = "";
        $timeout(function () {
            $scope.updateErrorCount($scope.selectedTabId);
        }, 0);

    }
    /*$scope.updateMandiriCreditCardData = function () {
        if (typeof $scope.mandiriCreditCardNumber1 == 'undefined' || typeof $scope.mandiriCreditCardNumber2 == 'undefined' || typeof $scope.mandiriCreditCardNumber3 == 'undefined' || typeof $scope.mandiriCreditCardNumber4 == 'undefined') {

        } else {
            var creditCardStr = $scope.mandiriCreditCardNumber1 + $scope.mandiriCreditCardNumber2 + $scope.mandiriCreditCardNumber3 + $scope.mandiriCreditCardNumber4;
            if (creditCardStr) {
                $scope.paymentinfo.cardNumber = creditCardStr;
            }
        }
    };*/
    function updateCreditCardData() {
        if (typeof $scope.creditCardDetail.creditCardNumberone == 'undefined' || typeof $scope.creditCardDetail.creditCardNumbertwo == 'undefined' || typeof $scope.creditCardDetail.creditCardNumberthree == 'undefined' || typeof $scope.creditCardDetail.creditCardNumberfour == 'undefined') {

        } else {
            var creditCardStr = $scope.creditCardDetail.creditCardNumberone + $scope.creditCardDetail.creditCardNumbertwo + $scope.creditCardDetail.creditCardNumberthree + $scope.creditCardDetail.creditCardNumberfour;
            if (creditCardStr) {
                $scope.paymentinfo.cardNumber = creditCardStr;
            }
        }

    };

    $scope.updateErrorCount = function (id) {
        updateCreditCardData();
        var _errorCount = 0;
        var _errors = [];
        if ($scope.paymentinfo && $scope.paymentinfo.paymentType && $scope.paymentinfo.paymentType == "credit_card") {
            if (!$scope.paymentinfo.payerName || $scope.paymentinfo.payerName == "") {
                _errorCount++;
                var error = {};
                error.message = translateMessages($translate, "eapp.nameOfCreditcardHolderRequiredValidationMessage");
                error.key = 'ccpayerName';
                _errors.push(error);
            }
            if (!$scope.paymentinfo.cardNumber || $scope.paymentinfo.cardNumber == "" || $scope.paymentinfo.cardNumber.length != 16) {
                _errorCount++;
                var error = {};
                error.key = 'creditCardNumber_1';
                error.message = translateMessages($translate, "eapp.creditCardNumberRequiredValidationMessage");
                _errors.push(error);
            }

        }
      
       
//        if ($scope.LifeEngageProduct.Declaration.confirmAgentSignature != false) {
//            _errorCount++;
//            var error = {};
//            error.message = translateMessages($translate, "ACRNotSigned");
//            error.key = 'ACRSign';
//            _errors.push(error);
//        }
        $scope.dynamicErrorCount = _errorCount;
        if (!$scope.dynamicErrorMessages) {
            $scope.dynamicErrorMessages = [];
        }
        $scope.dynamicErrorMessages = _errors;

        $timeout(function () {
            $rootScope.updateErrorCount($scope.selectedTabId);
        }, 0);
        $scope.selectedTabId = id;
    }
    $scope.onChangeAmount = function () {
        alert($scope.amountCash);
    }
    
    $scope.MakePayment = function () {
    	var sdf= false;
        $scope.lePopupCtrl.showWarning(
						translateMessages($translate,
								"lifeEngage"),
						translateMessages($translate,
								"pageNavigationText"),  
						translateMessages($translate,
								"fna.navok"),$scope.okPressed,translateMessages($translate,
								"general.navproceed"),$scope.backToListing);
    }

    function initPaymentDetails() {
        if ($scope.LifeEngageProduct.Payment && $scope.LifeEngageProduct.Payment.PaymentInfo) {
            $scope.paymentinfo = $scope.LifeEngageProduct.Payment.PaymentInfo
        } else {
            $scope.paymentinfo = EappVariables.EappModel.Payment.PaymentInfo;
        }
        if ($scope.totalPremium && $scope.LifeEngageProduct.Payment.PaymentInfo.status != "Payment done") {
            $scope.paymentinfo.cash = $scope.totalPremium;
        }
        $scope.paymentOptions = rootConfig.paymentDetail.PaymentMethods;
        $scope.proposalNo = EappVariables.EappKeys.Key21;
    }

    $scope.isEligible = function (option, product, amount, renewalType) {
        var _isEligible = false;
        if (option.supportingProducts.indexOf(product) != -1 && (!option.renewal || option.renewal.indexOf(renewalType) != -1) &&
            option.minAmount <= amount && (option.maxAmount >= amount || option.maxAmount == -1)) {
            _isEligible = true;
        }
        return _isEligible;
    }

    $scope.successCallback = function (data) {
        $scope.paymentinfo.transactionRefNumber = data.PaymentInfo.transactionId;
        $scope.LifeEngageProduct.Payment.PaymentInfo = $scope.paymentinfo;
        if (rootConfig.isDeviceMobile && rootConfig.isDeviceMobile != "false") {
            var payWindow = window.open(data.PaymentInfo.redirect_url, '_blank', 'location=yes');
            payWindow.addEventListener('exit', function (event) {
                $scope.onPayWindowClose();
            });
        } else {
            $scope.LifeEngageProduct.Payment.PaymentInfo = $scope.paymentinfo;
            EappService.saveTransaction(function () {
                $scope.onSaveSuccess(data.PaymentInfo.redirect_url);
            }, $scope.onConfirmError, false);
        }
    };
    /** Function to call redirection on desktop */
    $scope.onSaveSuccess = function (url) {
        window.location.href = url;
    };
    $scope.onSaveError = function () {};

    /* $scope.createChargeData = function () {
         var chargeData = {
             "TransactionData": {
                 "PaymentInfo": {
                     "policyholder_name": "",
                     "contract_no": 0,
                     "purpose_of_payment": "",
                     "amount": ""
                 }
             }
         };
         chargeData.TransactionData.PaymentInfo.policyholder_name = $scope.LifeEngageProduct.Proposer.BasicDetails.firstName + "" + $scope.LifeEngageProduct.Proposer.BasicDetails.lastName;
         chargeData.TransactionData.PaymentInfo.contract_no = EappVariables.EappKeys.Key21;
         chargeData.TransactionData.PaymentInfo.purpose_of_payment = "Insurance Premium";
         chargeData.TransactionData.PaymentInfo.amount = $scope.amount;

         return chargeData;

     }*/

     $rootScope.redirectDocupload = function () {
		EappVariables.EappKeys.Key34 = "PaymentCompleted";
		$scope.savePayment();
		$scope.LifeEngageProduct.LastVisitedUrl = "5,0,'Payment',false,'tabsinner16',''"; 
	    $scope.eAppParentobj.CurrentPage = "";
	        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
	            $scope.LifeEngageProduct.LastVisitedIndex = "5,0";
	            $scope.$parent.LifeEngageProduct.LastVisitedIndex = "5,0";
	        }	
		$scope.paintView(6,0,'DocumentUpload',true,'DocumentUpload','');
	}

    $scope.paymentSubmit = function () {
        if ($scope.paymentinfo.status == "Payment Done") {
            $scope.save();
        }
    }

    $scope.validationSuccessCallback = function (data) {
        var msg = translateMessages($translate, "eapp_vt.paymentSuccessMsg");
        msg = msg.replace("{{proposalno}}", $scope.proposalNo);
        $rootScope.showHideLoadingImage(false);
        if (rootConfig.isDeviceMobile) {
            if (data == 200) {
                var newDate = new Date();
                $scope.paymentinfo.date = UtilityService.getFormattedDate(newDate);
                $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), msg, translateMessages($translate, "fna.ok"), $scope.save);
            } else {
                if ($scope.paymentinfo.paymentType == "cards" || $scope.paymentinfo.paymentType == "pos" || $scope.paymentinfo.paymentType == "manual_bank_deposit") {
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.paymentErrorMsg1"), translateMessages($translate, "fna.ok"));
                } else {
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.paymentErrorMsg"), translateMessages($translate, "fna.ok"));
                }
            }
        } else {
            if (data == 200) {
                var newDate = new Date();
                $scope.paymentinfo.date = UtilityService.getFormattedDate(newDate);
                $scope.paymentinfo.status = "Payment done";
                EappVariables.EappKeys.Key15 = "PaymentDone";
                EappVariables.EappModel.Payment.PaymentInfo = $scope.paymentinfo;
                if (EappVariables.EappKeys.Key15 == "PaymentDone") {
                    $('#Payment button[id="btnpayNow"]').attr('disabled', true);
                }
                $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), msg, translateMessages($translate, "fna.ok"), $scope.save);
            } else {
                if ($scope.paymentinfo.paymentType == "pos" || $scope.paymentinfo.paymentType == "manual_bank_deposit") {
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.paymentErrorMsg1"), translateMessages($translate, "fna.ok"));
                } else if ($scope.paymentinfo.paymentType == "cards") {
                    var chargeDataWeb = [{
                        "key": "policyOwner",
                        "value": $scope.policyOwnerName
                    }, {
                        "key": "policyNumber",
                        "value": $scope.policyNumber
                    }, {
                        "key": "paymentDes",
                        "value": $scope.paymentDes
                    }, {
                        "key": "amount",
                        "value": $scope.amountOnline
                    }, {
                        "key": "fromfunction",
                        "value": "OMMI"
                    }];
                    var url = rootConfig.paymentOnlineURL;
                    var payWindow = window.open('payWeb.html', '_blank', 'location=yes');
                    payWindow.onload = function () {
                        payWindow.init(chargeDataWeb, url);
                    }
                } else {
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.paymentErrorMsg"), translateMessages($translate, "fna.ok"));
                }

                //onbeforeunload

                /*if($scope.paymentinfo.paymentType == "cards" || $scope.paymentinfo.paymentType == "pos" || $scope.paymentinfo.paymentType == "manual_bank_deposit"){
                		$scope.lePopupCtrl.showWarning(translateMessages($translate,"lifeEngage"),translateMessages($translate,"eapp_vt.paymentErrorMsg1"),translateMessages($translate,"fna.ok"));
                }
                else{
                	$scope.lePopupCtrl.showWarning(translateMessages($translate,"lifeEngage"),translateMessages($translate,"eapp_vt.paymentErrorMsg"),translateMessages($translate,"fna.ok"));
                }*/
            }
        }
    }

    function validateOnlinePayment() {
        PersistenceMapping.clearTransactionKeys();
        var transactionObj = PersistenceMapping.mapScopeToPersistence();
        transactionObj.ApplicationNo = EappVariables.EappKeys.Key21;
        transactionObj.PremiumAmount = $scope.totalPremium;
        transactionObj.PaymentOption = "Credit Card";
        transactionObj.Type = "PaymentValidation";
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);

        PaymentService.validateOnlinePayment(transactionObj, $scope.validationSuccessCallback, $scope.errorInPaymentCallback);
    }

    function validateRecieptNo() {
        PersistenceMapping.clearTransactionKeys();
        var transactionObj = PersistenceMapping.mapScopeToPersistence();
        transactionObj.ApplicationNo = EappVariables.EappKeys.Key21;
        transactionObj.Key1 = $scope.paymentinfo.receiptNumber;
        transactionObj.PremiumAmount = $scope.paymentinfo.cash;
        transactionObj.Key3 = transactionObj.Key11;
        transactionObj.Key11 = "";
        transactionObj.Key14 = "";
        transactionObj.Type = "PaymentValidation";
        switch ($scope.paymentinfo.paymentType) {
            case 'cash':
                transactionObj.PaymentOption = 'Cash';
                break;
            case 'manual_bank_deposit':
                transactionObj.PaymentOption = 'Manual bank deposit';
                break;
            case 'pos':
                transactionObj.PaymentOption = 'POS';
                break;
            default:
                break;
        }
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
        PaymentService.validateReciptNumber(transactionObj, $scope.validationSuccessCallback, $scope.errorInPaymentCallback);
    }

    $scope.paynowOffline = function () {
        if ($scope.LifeEngageProduct.Product.ProductDetails.productCode != "1") {
            if ($scope.paymentinfo.paymentType == "virtual_payment") {
                $scope.save();
            } else {
                if ($scope.errorCount > 0) {
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "enterMandatoryIllustration"), translateMessages($translate, "fna.ok"));
                } else {
                    if ($scope.paymentinfo.cash !== "") {
                        if ($scope.paymentinfo.cash < $scope.totalPremium) {
                            var amountLessThanPremiumErrorMsg = translateMessages($translate, "eapp_vt.amountLessThanPremiumErrorMsg");
                            amountLessThanPremiumErrorMsg = amountLessThanPremiumErrorMsg.replace("{{amount}}", $scope.totalPremium);
                            $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), amountLessThanPremiumErrorMsg, translateMessages($translate, "fna.ok"));
                        } else {
                            validateRecieptNo();
                        }
                    }
                }
            }
        } else {
            $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "CIB1DiscontinuedMsg"), translateMessages($translate, "fna.ok"));
        }
    }
    $scope.onPayWindowClose = function (payWindow) {
        validateRecieptNo();
    };
    $scope.paynow = function () {
        if ($scope.LifeEngageProduct.Product.ProductDetails.productCode != "1") {
            if (!$scope.paymentinfo.paymentType && $scope.paymentinfo.paymentType == "") {
                return;
                //Show some popup
            }
            if ($scope.errorCount > 0) {
                $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "enterMandatory"), translateMessages($translate, "fna.ok"));
            } else {
                if ($scope.isUserOnline()) {
                    if ($scope.LifeEngageProduct.Proposer.BasicDetails.fullName) {
                        $scope.policyOwnerName = $scope.LifeEngageProduct.Proposer.BasicDetails.fullName;
                    } else {
                        $scope.policyOwnerName = $scope.LifeEngageProduct.Proposer.BasicDetails.firstName + " " + $scope.LifeEngageProduct.Proposer.BasicDetails.lastName;
                    }
                    $scope.policyNumber = EappVariables.EappKeys.Key21;
                    $scope.paymentDes = "opt1";
                    $scope.amountOnline = $scope.totalPremium;
                    var chargeData = [{
                        "key": "policyOwner",
                        "value": $scope.policyOwnerName
                    }, {
                        "key": "policyNumber",
                        "value": $scope.policyNumber
                    }, {
                        "key": "paymentDes",
                        "value": $scope.paymentDes
                    }, {
                        "key": "amount",
                        "value": $scope.amountOnline
                    }, {
                        "key": "fromfunction",
                        "value": "OMMI"
                    }];
                    if (rootConfig.isDeviceMobile) {
                        $scope.doPaymentOnDevice(chargeData);
                    } else {
                        validateOnlinePayment();
                    }
                }
            }
        } else {
            $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "CIB1DiscontinuedMsg"), translateMessages($translate, "fna.ok"));
        }
    };

    $scope.doPaymentOnDevice = function (chargeData) {
        var url = rootConfig.paymentOnlineURL;
        var index = 0;
        var payment = 'var form =document.createElement("form");form.setAttribute("method","post");form.setAttribute("id","paymentForm");form.setAttribute("action","' + url + '");';
        var filedString;
        for (var data in chargeData) {
            filedString = "";
            filedString += ' var hiddenField' + index + ' = document.createElement("input");';
            filedString += 'hiddenField' + index + '.setAttribute("name", "' + chargeData[data].key + '");';
            filedString += 'hiddenField' + index + '.setAttribute("value", "' + chargeData[data].value + '");';
            filedString += 'hiddenField' + index + '.setAttribute("type", "hidden");';

            filedString += 'form.appendChild(hiddenField' + index + ');';
            payment += filedString;
            index++;
        };
        payment += 'document.body.appendChild(form);document.querySelector("#paymentForm").submit();';
        var payWindow = window.open('pay.html', '_blank', 'location=yes');

        payWindow.addEventListener('loadstop', function (event) {
            payWindow.executeScript({
                code: payment
            });
            payment = "";
        });
        payWindow.addEventListener('loaderror', function (event) {});

        payWindow.addEventListener('exit', function (event) {
            $scope.statusSuccesspayment();
        });
    }
    $scope.doPaymentOnWeb = function (chargeData) {
        var url = rootConfig.paymentOnlineURL;
        var payWindow = window.open('pay.html', '_blank', 'location=yes');
        var form = payWindow.document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("id", "paymentForm");
        form.setAttribute("action", url);
        var index = 0;
        for (var data in chargeData) {

            var hiddenField = payWindow.document.createElement("input");
            hiddenField.setAttribute("name", chargeData[data].key);
            hiddenField.setAttribute("value", chargeData[data].value);
            hiddenField.setAttribute("type", "hidden");

            form.appendChild(hiddenField);
            index++;
        };
        payWindow.document.body.appendChild(form);
        payWindow.document.querySelector("#paymentForm").submit();

    }

    $scope.statusSuccesspayment = function () {
        validateOnlinePayment();
    }
    $scope.statusSuccessCallback = function (data) {
        var msg = "";
        $rootScope.showHideLoadingImage(false);
        if (data.PaymentInfo && data.PaymentInfo.paymentRequestStatus && data.PaymentInfo.paymentRequestStatus == 'COMPLETED') {
            msg = translateMessages($translate, "eapp.payemntSuccessMessage");
            $scope.paymentinfo.status = "COMPLETED";
            EappVariables.EappModel.Payment.PaymentInfo = $scope.paymentinfo;
            $rootScope.showHideLoadingImage(true);
            EappService.saveTransaction(function () {
                $rootScope.showHideLoadingImage(false);
            }, $scope.onConfirmError, false);


        } else {
            msg = translateMessages($translate, "eapp.payemntFailMessage");
        }
        msg = msg.replace("{{amount}}", $scope.amount);
        $scope.popupMessage = msg.replace("{{spajno}}", $scope.spajNo);
        $scope.showModel = true;

    };

    /*$scope.populatePayorList = function () {
        $scope.payerNames = [];
        var name = $scope.mergeName($scope.LifeEngageProduct.Proposer.BasicDetails.firstName, $scope.LifeEngageProduct.Proposer.BasicDetails.lastName);
        $scope.payerNames.push(name);
        if ($scope.LifeEngageProduct.Proposer.CustomerRelationship &&
            $scope.LifeEngageProduct.Proposer.CustomerRelationship.isPayorDifferentFromInsured &&
            $scope.LifeEngageProduct.Proposer.CustomerRelationship.isPayorDifferentFromInsured == "Yes") {
            var name = $scope.mergeName($scope.LifeEngageProduct.Insured.BasicDetails.firstName, $scope.LifeEngageProduct.Insured.BasicDetails.lastName);
            if ($scope.payerNames.indexOf(name) == -1) {
                $scope.payerNames.push(name);
            }

        }
        if ($scope.LifeEngageProduct.Beneficiaries) {
            for (var index = 0; index < $scope.LifeEngageProduct.Beneficiaries.length; index++) {
                var name = $scope.mergeName($scope.LifeEngageProduct.Beneficiaries[index].BasicDetails.firstName, $scope.LifeEngageProduct.Beneficiaries[index].BasicDetails.lastName);
                if ($scope.payerNames.indexOf(name) == -1) {
                    $scope.payerNames.push(name);
                }
            }
        }
        if ($scope.LifeEngageProduct.Payment.FinancialInfo.premiumPayer == "Other") {
            if ($scope.LifeEngageProduct.Proposer.AdditionalQuestioniare && typeof ($scope.LifeEngageProduct.Proposer.AdditionalQuestioniare.Questions[51].details) != "undefined") {
                var name = $scope.LifeEngageProduct.Proposer.AdditionalQuestioniare.Questions[51].details;
                if ($scope.payerNames.indexOf(name) == -1) {
                    $scope.payerNames.push(name);
                }
            }

        }

    };*/

    $scope.mergeName = function (fn, ln) {
        var name = fn;
        if (ln) {
            name = name + " " + ln;
        }
        return name.trim();
    }

    /*$scope.checkRenewalPayment = function () {
        $scope.renewalPayemntType = $scope.LifeEngageProduct.Payment.RenewalPayment.paymentType;
        if ($scope.renewalPayemntType == "CreditCard") {
            $scope.paymentinfo.cardNumber = $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardNumber;
            if ($scope.LifeEngageProduct.Payment.RenewalPayment && $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName)
                $scope.paymentinfo.payerName = $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName.trim();
        } else if ($scope.renewalPayemntType == "DebetAccount") {
            $scope.paymentinfo.bankName = $scope.LifeEngageProduct.Payment.RenewalPayment.bankName;
            $scope.paymentinfo.branchName = $scope.LifeEngageProduct.Payment.RenewalPayment.branchName;
            $scope.paymentinfo.payerName = $scope.LifeEngageProduct.Payment.RenewalPayment.accHolderName;
            $scope.paymentinfo.accountIdentifier = $scope.LifeEngageProduct.Payment.RenewalPayment.accountNumber;
        }
    }*/

    $scope.closeModel = function () {
        $scope.showModel = false;
    }

    $scope.isReadOnly = function () {
        return false;
    }
    $scope.save = function () {
        if (!$scope.paymentinfo.paymentType && $scope.paymentinfo.paymentType == "") {
            return;
            //Show some popup
        }

        $scope.paymentinfo.status = "Payment done";
        EappVariables.EappKeys.Key15 = "PaymentDone";
        EappVariables.EappModel.Payment.PaymentInfo = $scope.paymentinfo;
        EappService.saveTransaction($scope.processedToDocument, $scope.onConfirmError, false);
    }

    $scope.savePayment = function(){
        EappService.saveTransaction($scope.processedToDocument, $scope.onConfirmError, false);
    }

    $scope.processedToDocument = function (data) {
        if ($scope.LifeEngageProduct.Payment.PaymentInfo.status == "Payment done") {
            $('#Payment button[id="btnpayNow"]').attr('disabled', true);
        }
        $rootScope.showHideLoadingImage(true, "Loading. Please Wait...", $translate);
        $scope.selectedTabId = "Payment";
        GLI_EappService.checkRequirementNull($scope.LifeEngageProduct.Requirements, $scope.selectedTabId, function (returnReq) {
            if (returnReq == 0) {
                $scope.runRuleToGetRequirement();
            } else {
                $scope.isRequirementRuleExecuted = false;
                $scope.LifeEngageProduct.LastVisitedUrl = "6,0,'DocumentUpload',false,'tabsinner18',''";
                $scope.saveProposal();
                $rootScope.validationBeforesave = function (value, successcallback) {
                    successcallback();
                };
            }
        });
    };

    $scope.errorCallback = function (data) {
        $rootScope.showHideLoadingImage(false);
    };

    $scope.errorInPaymentCallback = function (data) {
        $rootScope.showHideLoadingImage(false);
        $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.paymentInvalidErrorMsg"), translateMessages($translate, "fna.ok"));
    };

    $scope.isUserOnline = function () {
        var userOnline = true;
        if ((rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
            if (navigator.network.connection.type == Connection.NONE) {
                userOnline = false;
            }
        }
        if ((rootConfig.isOfflineDesktop)) {
            if (!navigator.onLine) {
                userOnline = false;
            }
        }
        return userOnline;
    }


    $scope.Initialize = function () {
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_Payment#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_Payment#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step2Payment#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step2Payment#tabDetails_1');
        } 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step3Payment#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step3Payment#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_BeneficiarySubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_BeneficiarySubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_financilaDetailsSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_financilaDetailsSubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_mainInsuredSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_mainInsuredSubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PolicyHolderSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PolicyHolderSubTab#tabDetails_1');
        }		
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
		}
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            //EappVariables.setEappModel($scope.LifeEngageProduct);
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
        }
        
        if(typeof EappVariables.EappKeys.Key34 != "undefined" && (EappVariables.EappKeys.Key34== "PaymentCompleted" || EappVariables.EappKeys.Key34=="DocuploadCompleted")) {
			angular.element(document.querySelector(".site-overlay")).addClass("hideSplitOne");
			$timeout(function () {
				$scope.initailPaintView(5,3,'step3Payment',true,'tabsinner16','');
				angular.element(document.querySelector(".site-overlay")).removeClass("hideSplitOne");
			},500)
			$scope.initailPaintView(5,3,'step3Payment',true,'tabsinner16','');
			return;
        }
        
        if($scope.LifeEngageProduct.splitPaymentSelection=="" ||$scope.LifeEngageProduct.splitPaymentSelection==undefined){
            $scope.LifeEngageProduct.splitPaymentSelection="No";
            $rootScope.splitOptionSelected=false;
        }
        if(($scope.LifeEngageProduct.skipPaymentSelection=="" ||$scope.LifeEngageProduct.skipPaymentSelection==undefined) 
            && ($scope.LifeEngageProduct.Payment.FinancialInfo.skipPayment != "" && $scope.LifeEngageProduct.Payment.FinancialInfo.skipPayment != undefined)) {
            $scope.LifeEngageProduct.skipPaymentSelection=$scope.LifeEngageProduct.Payment.FinancialInfo.skipPayment;
        }   
         if($scope.LifeEngageProduct.skipPaymentSelection=="" ||$scope.LifeEngageProduct.skipPaymentSelection==undefined){
            $scope.LifeEngageProduct.skipPaymentSelection="No";
            $rootScope.skipOptionSelected=false;
        }
        //Getting the illustration status
        if (rootConfig.isDeviceMobile) {
            GLI_DataService.getRelatedBIForEapp(EappVariables.EappKeys.Key3, function (IllustrationData) {
                $scope.LifeEngageProduct.Product.ProductDetails.IllustrationStatus = angular.copy(IllustrationData[0].Key15);
                $scope.updateErrorCount($scope.selectedTabId);
            });
        } else {
            var transactionObj = {};
            PersistenceMapping.clearTransactionKeys();
            transactionObj = PersistenceMapping.mapScopeToPersistence({});
            transactionObj.Key1 = EappVariables.EappKeys.Key1;
            transactionObj.Key2 = EappVariables.EappKeys.Key2;
            transactionObj.Key3 = EappVariables.EappKeys.Key24;
            transactionObj.Key4 = EappVariables.EappKeys.Key4;
            transactionObj.Key5 = EappVariables.EappKeys.Key5;
            transactionObj.Key7 = EappVariables.EappKeys.Key7;
            transactionObj.Key15 = EappVariables.EappKeys.Key15;
            transactionObj.Key21 = EappVariables.EappKeys.Key21;
            transactionObj.Key24 = EappVariables.EappKeys.Key24;
            transactionObj.Key26 = EappVariables.EappKeys.Key26;
            transactionObj.Type = "illustration";
        /*    DataService.getListingDetail(transactionObj, function (IllustrationData) {
                $scope.LifeEngageProduct.Product.ProductDetails.IllustrationStatus = angular.copy(IllustrationData[0].Key15);
                $scope.updateErrorCount($scope.selectedTabId);
            }, function () {
                alert("Error");
            });*/
        }
        $scope.updateErrorCount($scope.selectedTabId);
        //For disabling and enabling the tabs.And also to know the last visited page.---starts
        //$rootScope.selectedPage = "ProposerDetails";
        
       
        
        $scope.LifeEngageProduct.LastVisitedUrl = "5,0,'Payment',false,'tabsinner16',''";	
    //    $scope.LifeEngageProduct.LastVisitedUrl = "5,0,'Payment',false,'tabsinner16',''";
        $scope.eAppParentobj.nextPage = "6,0,'DocumentUpload',false,'tabsinner18',''";
        $scope.eAppParentobj.CurrentPage = "";
        if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission" && EappVariables.EappKeys.Key15 != "Confirmed") {
            UtilityService.disableAllActionElements();
        }else if (typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34=== "PaymentCompleted") {
                UtilityService.disableAllActionElements();
        } else {
            UtilityService.removeDisableElements('Payment');
        }
        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[0] <= $scope.LifeEngageProduct.LastVisitedUrl[0]) {
                $scope.LifeEngageProduct.LastVisitedIndex = "5,0";
            }
        }
        
        EappVariables.setEappModel($scope.LifeEngageProduct);
        $scope.prodPremium = 0;
        $scope.riderPremium = 0;
        // calculating total premium
        if ($scope.LifeEngageProduct.Product.ProductDetails.initialPremium) {
            $scope.prodPremium = $scope.LifeEngageProduct.Product.ProductDetails.initialPremium;
        }
        if ($scope.LifeEngageProduct.Product.RiderDetails.length > 0) {
            for (i = 0; i < $scope.LifeEngageProduct.Product.RiderDetails.length; i++) {
                $scope.riderPremium = $scope.LifeEngageProduct.Product.RiderDetails[i].initialPremium;
            }
        }
        //$scope.totalPremium = $scope.prodPremium + $scope.riderPremium;
        $scope.totalPremium = $scope.LifeEngageProduct.Product.ProductDetails.totalInitialPremium;
        //For disabling and enabling the tabs.And also to know the last visited page.---ends

        $scope.eAppParentobj.PreviousPage = "";
        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        angular.element('#' + $scope.selectedTabId + "_a").trigger('click');
        initPaymentDetails();
        $timeout(function () {
            if ($scope.LifeEngageProduct.Payment.PaymentInfo.status == "Payment done") {
                $('#Payment input[type="radio"]').attr('disabled', true);
                $('#Payment input[type="text"]').attr('disabled', 'disabled');
                $('#Payment .iradio').attr('disabled', 'disabled');
            } else {
                $('#Payment input[type="radio"]').attr('disabled', false);
                $('#Payment input[type="text"]').removeAttr("disabled");
                $('#Payment .iradio').removeAttr("disabled");
            }
        }, 0);
        // formatting Date on Tab navigation 
        $rootScope.stringFormattedDate = function () {}
    }
    $scope.showErrorPopup = function (id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
    }

    //PreliminaryDataController Load Event and Load Event Handler
    $scope.$parent.$on('Payment', function (event, args) {
        $scope.selectedTabId = "Payment";
        $scope.Initialize();
        $rootScope.validationBeforesave = function (value, successcallback) {
            successcallback();
        };
    });

    $scope.runRuleToGetRequirement = function () {
    	$rootScope.showHideLoadingImage(true, 'reqRuleProgress', $translate);
        var transactionObj = RequirementRule();
      //Proposer Details
        if ($scope.LifeEngageProduct.Proposer) {
            //Proposer Questionnaire sections
        	if($scope.LifeEngageProduct.Proposer.Questionnaire.Other){
        		if ($scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions.length > 0) {

        			if ($scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[0]) {
        				transactionObj.QuestionDetails.FatcaQuestions1Ans = $scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[0].details;
        			}
        			if ($scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[1]) {
        				transactionObj.QuestionDetails.FatcaQuestions11Ans = $scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[1].details;
        			}
        			if ($scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[2]) {
        				transactionObj.QuestionDetails.FatcaQuestions12Ans = $scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[2].details;
        			}
        			if ($scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[3]) {
        				transactionObj.QuestionDetails.FatcaQuestions13Ans = $scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[3].details;
        			}
        			if ($scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[4]) {
        				transactionObj.QuestionDetails.FatcaQuestions2Ans = $scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[4].details;
        			}
        			if ($scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[5]) {
        				transactionObj.QuestionDetails.FatcaQuestions3Ans = $scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[5].details;
        			}
        			if ($scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[6]) {
        				transactionObj.QuestionDetails.FatcaQuestions4Ans = $scope.LifeEngageProduct.Proposer.Questionnaire.Other.Questions[6].details;
        			}

        		}
        	}
        }
        //Insured Details
        if ($scope.LifeEngageProduct.Insured !== undefined && $scope.LifeEngageProduct.Insured.BasicDetails !== undefined) {
        		transactionObj.InsuredDetails.partyIdentifier = UtilityService.getUUID();
        		EappVariables.EappModel.Insured.BasicDetails.UUID = transactionObj.InsuredDetails.partyIdentifier;
                transactionObj.InsuredDetails.age = calculateAge($scope.LifeEngageProduct.Insured.BasicDetails.dob);
				transactionObj.InsuredDetails.insuredNationality = $scope.LifeEngageProduct.Insured.BasicDetails.nationality;
                transactionObj.InsuredDetails.nationality = $scope.LifeEngageProduct.Insured.BasicDetails.nationality;
                transactionObj.InsuredDetails.isInsuredSameAsPayer = $scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer;
        }
        
       //Payer Details
        if ($scope.LifeEngageProduct.Insured !== undefined && $scope.LifeEngageProduct.Insured.CustomerRelationship !== undefined) {
        		transactionObj.PayerDetails.partyIdentifier = UtilityService.getUUID();
        		EappVariables.EappModel.Payer.BasicDetails.UUID = transactionObj.PayerDetails.partyIdentifier;
                transactionObj.PayerDetails.payerRelationshipWithInsured = $scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer;
        }
        
       //Product Details
	   
        if ($scope.LifeEngageProduct.Product !== undefined && $scope.LifeEngageProduct.Product.policyDetails !== undefined) {
                transactionObj.ProductDetails.sumAssured = $scope.LifeEngageProduct.Product.policyDetails.sumInsured;
        }
        
      //Bank Details
        if ($scope.LifeEngageProduct.Payment) {
        	/*if($scope.LifeEngageProduct.Payment.PaymentInfo){
                transactionObj.BankDetails.modeOfPayment = $scope.LifeEngageProduct.Payment.PaymentInfo.paymentType;
				transactionObj.PaymentDetails.ETRNumber = "Online";
        	}*/
        	if($scope.LifeEngageProduct.Payment.RenewalPayment){
                transactionObj.BankDetails.renewalMode = $scope.LifeEngageProduct.Payment.RenewalPayment.paymentFrequency;
                transactionObj.BankDetails.renewalMethod = $scope.LifeEngageProduct.Payment.RenewalPayment.paymentType;
        	}
//        	if($scope.LifeEngageProduct.Payment.PaymentInfo){
//                transactionObj.BankDetails.modeOfPayment = $scope.LifeEngageProduct.Payment.PaymentInfo.paymentType;
//                //transactionObj.PaymentDetails.ETRNumber = $scope.LifeEngageProduct.Payment.PaymentInfo.etrGeneration;
//				transactionObj.PaymentDetails.ETRNumber = "Online";
//        	}
        	
        	if($scope.LifeEngageProduct.Payment.PaymentInfo[0].splitPaymentTst!=undefined && $scope.LifeEngageProduct.Payment.PaymentInfo[0].splitPaymentTst=="Yes"){
        		transactionObj.BankDetails.modeOfPayment = $scope.LifeEngageProduct.Payment.PaymentInfo[0].paymentType;
        		transactionObj.BankDetails.modeOfPayment2 = $scope.LifeEngageProduct.Payment.PaymentInfo[1].paymentType;
        		transactionObj.isSplitPayment = "Yes";
        	}else {
        		transactionObj.BankDetails.modeOfPayment = $scope.LifeEngageProduct.Payment.PaymentInfo[0].paymentType;
        		transactionObj.BankDetails.modeOfPayment2 = "";
        		transactionObj.isSplitPayment = "No";
        	}
        	transactionObj.PaymentDetails.ETRNumber = "Online";
        }

        //Application Number
        if($scope.LifeEngageProduct.ApplicationNo.length == 11){
        	transactionObj.ProductDetails.PhysicalApplicationNumber = "No";
        } else{
        	transactionObj.ProductDetails.PhysicalApplicationNumber = "Yes";
        }
        
        EappService.runRequirementRule(transactionObj, function (data) {            			
            $scope.isRequirementRuleExecuted = false;
            $scope.LifeEngageProduct.LastVisitedUrl = "6,0,'DocumentUpload',false,'tabsinner18',''";
            $scope.saveProposal();
            $rootScope.validationBeforesave = function (value, successcallback) {
                successcallback();                
                 $rootScope.loadDocumentSection(); 
            };
        }, $scope.requirementRuleError);
    };

    $scope.requirementRuleError = function () {
        $rootScope.showHideLoadingImage(false);
        EappService.requirementRuleError();
    };
    $rootScope.validationBeforesave = function (value, successcallback) {
        successcallback();
    };

    //calculating age for Additional Insured
    function calculateAge(dob) {
        if (dob !== "" && dob != undefined && dob !== null && dob !== "Invalid Date") {
            var dobDate = new Date(dob);
            if ((dobDate === "NaN" || dobDate === "Invalid Date")) {
                dobDate = new Date(dob.split("-").join("/"));
            }
            var todayDate = new Date();
            var yyyy = todayDate.getFullYear().toString();
            var mm = (todayDate.getMonth() + 1).toString();
            var dd = todayDate.getDate().toString();
            var formatedDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);

            if (dobDate > todayDate) {
                return;
            } else {
                var age = todayDate.getFullYear() - dobDate.getFullYear();
                if (todayDate.getMonth() < dobDate.getMonth() - 1 || (dobDate.getMonth() - 1 === todayDate.getMonth() && todayDate.getMonth() < dobDate.getDate())) {
                    age--;
                }
                if (dob === undefined || dob === "") {
                    age = "-";
                }
                return age;
            }
        }
    };

} //