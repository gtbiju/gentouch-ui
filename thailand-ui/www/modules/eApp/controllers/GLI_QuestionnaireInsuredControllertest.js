'use strict';
/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:GLI_QuestionnaireInsuredControllertest
 CreatedDate:23/12/2015
 Description:GLI_QuestionnaireInsuredControllertest 
 */

storeApp.controller('GLI_QuestionnaireInsuredControllertest', GLI_QuestionnaireInsuredControllertest);
GLI_QuestionnaireInsuredControllertest.$inject = ['$rootScope', '$scope', 'DataService', 'LookupService', 'DocumentService', 'ProductService', '$compile', '$routeParams', '$location', '$translate', '$debounce', 'UtilityService', 'PersistenceMapping', 'AutoSave', 'globalService', 'EappVariables', 'EappService', 'GLI_EappVariables', '$timeout', 'GLI_EappService', 'GLI_EvaluateService'];

function GLI_QuestionnaireInsuredControllertest($rootScope, $scope, DataService, LookupService, DocumentService, ProductService, $compile, $routeParams, $location, $translate, $debounce, UtilityService, PersistenceMapping, AutoSave, globalService, EappVariables, EappService, GLI_EappVariables, $timeout, GLI_EappService, GLI_EvaluateService) {
$scope.ShortEappModal = false;
$rootScope.selectedPage = "eappshortquestionnaire";
    $scope.selectedTabId = 'eappshortquestionnaire';
    $scope.LifeEngageProduct.LastVisitedUrl = "";
    $scope.eAppParentobj.nextPage = "";
    $scope.eAppParentobj.CurrentPage = "";
    $scope.showErrorCount = true;
    $scope.QuestionnaireObj = {};
    $scope.shortEappHealthRiderAvailable=false;
    $rootScope.shortEappHealthRiderAvailable=$scope.shortEappHealthRiderAvailable;       
    $scope.DynamicGenerateETRpopup = false; //temporary variable used for hiding and showing Questions 19.a or 19 b for individual and corporate clients
    var unload = $scope.$parent.$on('eappshortquestionnaire_unload', function (event) {
       var data = $scope.$parent;
       angular.element(document.querySelector('#eappshortquestionnaire')).empty();
       $scope.$destroy();
       $scope.$parent = data;
        load();
        unload();
   });
      
        $scope.ShortEappModelpopup = function () {
         if($scope.LifeEngageProduct.Insured.BasicDetails.nationality!='THA'){
            $scope.DynamicGenerateETRpopup = true;
            angular.element(document.querySelector(".site-overlay")).addClass("modal-backdrop fade in");
                    angular.element(document.querySelector("#ShortEappModal")).addClass("modal le-popup-control hide fade auto-height animated fadeInDownBig in");
                    angular.element(document.querySelector("#ShortEappModal .span5")).addClass("taxInputValue");
                    $("#ShortEappModal .modal").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
         }
                   
        }

            $scope.removeShortEappModelpopup = function(){
                $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[6].detailsOther = "";
            }

            
            
            $scope.confirmTaxID = function () {
                var taxIDvalue = $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[6].detailsOther;
              if($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[6].detailsOther != "" && $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[6].detailsOther.length>0){
                 $scope.DynamicGenerateETRpopup=false;
                    angular.element(document.querySelector(".site-overlay")).removeClass("modal-backdrop fade in");
                    $("#updateManualETRAction .modal").modal({
                        backdrop: true,
                        keyboard: true
                    });
               //$scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[6].detailsOther = taxIDvalue;
              }  
            }

//cancel Manual E-TR popup
    $scope.cancelETRnoManual = function () {
        $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[6].details="No";
        $scope.DynamicGenerateETRpopup=false;
        $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[6].detailsOther = "";
        angular.element(document.querySelector(".site-overlay")).removeClass("modal-backdrop fade in");
        $("#updateManualETRAction .modal").modal({
            backdrop: true,
            keyboard: true
        });
    }
 	 $scope.$parent.ShowHideDialogueEapp = function (riderName) {
      
        $scope.$parent.IsEappVisible = $scope.$parent.IsEappVisible ? false : true;
        var eappriderArray = {
            'EapptranslatedTooltip': ''
        };
        eappriderArray.translatedTooltip = translateMessages($translate, " " + riderName);
        $scope.$parent.EapptoolTip = eappriderArray.translatedTooltip;
    };
    $rootScope.shorteappsubtab = false;
    $rootScope.shorteappInsured = false;
    $rootScope.shorteappPayer = false;
    $rootScope.shorteappFatca = false;
 

        $rootScope.selectedPage = "eappshortquestionnaire";
        $scope.LifeEngageProduct.LastVisitedUrl = "3,6,'eappshortquestionnaire',true,'eappshortquestionnaire',''";
        $scope.eAppParentobj.nextPage = "3,1,'MainInsuredQuestionnaire',true,'HealthDetailsSubTab','eAppQuestionnaire'";
     

      if($scope.LifeEngageProduct.Insured.BasicDetails.gender =="Male"){
         $scope.Insureddisable = "No";
         $scope.shortEappOption5 =  "No";
    } else{
        $scope.Insureddisable = "Yes";
        $scope.shortEappOption5 =  "No";
    }     

    
     
 	if(!$scope.LifeEngageProduct.Product.policyDetails.hasPBRider) {
        $scope.PBrideransweryesoption =  "No";   
    }else{
        $scope.PBrideransweryesoption =  "Yes";
    }

    if($scope.PBrideransweryesoption == "No"){
          $scope.Payerdisable = true;
          $scope.shortEappOption5 = true;
    }else{
        $scope.Payerdisable = false;
        $scope.shortEappOption5 = false;
    }


$scope.LifeEngageProduct.isFatca =true;

$rootScope.editPageNavigation = [];

$scope.setTabActive = function(){

if($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[0].details=='No' && $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[1].details=='No' && $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[2].details=='No'){
    
    $rootScope.editPageNavigation = ['LifeStyleQuestions', '0'];

}

if($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[3].details=='No' && $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[4].details=='No'){
    
    $rootScope.editPageNavigation = ['IllnessTreatmentHistory', '1'];
	}
}

$scope.clearingSubQuestions = function(questionSet1,questionSet2,questionSet3){

	if(questionSet2.length > 0){
		for(var i = 2; i < questionSet2.length; i++) {
			    $scope.LifeEngageProduct[questionSet2[0]].Questionnaire[questionSet2[1]].Questions[questionSet2[i]].details = "";
		}
	}

	if(questionSet1.length > 0){
		for(var i = 0; i < questionSet1.length; i++) {
			var datePatternValidation = "datePatternValidation"+"_"+ questionSet1[i];  
			var patternShow = "patternShow" + "_" + questionSet1[i];
			$scope[datePatternValidation] ="";
			$scope[patternShow]=false;
		}
	}
	
	 //EappVariables.setEappModel($scope.LifeEngageProduct);
	 
   $timeout(function () {
		$scope.updateErrorCount('MainInsuredQuestionnaire');
		$scope.refresh();
	}, 50)

}

    $scope.clearCarousel=function(illnessStart,illnessEnd,model,healthStart,healthEnd)
    {      
    for(var i=0;i<=illnessStart.length;i++)
    {
        for(var j=illnessStart[i];j<=illnessEnd[i];j++)
        {
            if(model=="Insured")
            {
            $scope.LifeEngageProduct.Insured.Questionnaire.Other.Questions[j].details='';   
             if(j==274) 
             {
                $scope.LifeEngageProduct.Insured.Questionnaire.Other.Questions[j].detailsOther='';
                $scope.LifeEngageProduct.Insured.Questionnaire.Other.Questions[j].unit='';
             }              
            }
        else if(model=="Payer")
        {
             $scope.LifeEngageProduct.Payer.Questionnaire.Other.Questions[j].details=''; 
              if(j==274) 
             {
                $scope.LifeEngageProduct.Payer.Questionnaire.Other.Questions[j].detailsOther='';
                $scope.LifeEngageProduct.Payer.Questionnaire.Other.Questions[j].unit='';
             }   
        }
    }
    }

if(healthStart.length>0){
    for(var h=0;h<=healthStart.length;h++)
    {
        for(var k=healthStart[h];k<=healthEnd[h];k++)
        {
            if(model=="Insured")
            $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[k].details='';   
            else if(model=="Payer")
             $scope.LifeEngageProduct.Payer.Questionnaire.HealthDetails.Questions[k].details='';   
        }
    }


}
}


        $scope.RidersInIllustration = $scope.LifeEngageProduct.Product.RiderDetails;
		for(var i = 0;i<$scope.RidersInIllustration.length;i++){
			if($scope.RidersInIllustration[i].riderPlanName == "HS Extra" 
				|| $scope.RidersInIllustration[i].riderPlanName == "HB(A)"  
				|| $scope.RidersInIllustration[i].riderPlanName == "CI Extra"
				|| $scope.RidersInIllustration[i].riderPlanName == "DD") {
				$scope.shortEappHealthRiderAvailable = true;
				$rootScope.shortEappHealthRiderAvailable=$scope.shortEappHealthRiderAvailable;
                $scope.LifeEngageProduct.Product.policyDetails.hasDDRider=true;
			}
		}





 $scope.clearCarouselSelection=function(illnessStart,illnessEnd,model,healthStart,healthEnd)
    {
    
    for(var i=0;i<=illnessStart.length;i++)
    {
        for(var j=0;j<=illnessEnd[i];j++)
        {
            if(model=="Insured")
            $scope.LifeEngageProduct.Insured.Questionnaire.Other.Questions[illnessStart[i]].options[j].details='';   
            else if(model=="Payer")
             $scope.LifeEngageProduct.Payer.Questionnaire.Other.Questions[illnessStart[i]].options[j].details='';   
        }
    }

if(healthStart.length>0){
    for(var h=0;h<=healthStart.length;h++)
    {
        for(var k=0;k<=healthEnd[h];k++)
        {
            if(model=="Insured")
            $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[healthStart[h]].options[k].details='';   
            else if(model=="Payer")
             $scope.LifeEngageProduct.Payer.Questionnaire.HealthDetails.Questions[healthStart[h]].options[k].details='';    
        }
    }
}
}

    // $scope.clearDataSummary=function(start,end,model)
    // {
    //         for(var i = start; i<= end;i++)
	//         {
    //         if(model=="Insured")
    //         $scope.LifeEngageProduct.Insured.Questionnaire.Other.Questions[i].details='';   
    //         else if(model=="Payer")
    //          $scope.LifeEngageProduct.Payer.Questionnaire.Other.Questions[i].details='';   
	//         }	  
          
    // }



    $scope.yesNoValidationChk = function(qArray1, qArray2){
	
	if(qArray1.length > 0){
		if(qArray1[2] == 'Yes'){
			$rootScope.illnessYesNoflag = qArray1[3];
			$rootScope.checkIllnessOptionflag = qArray1[4];
			for(var t=5; t<qArray1.length; t++){
                if($scope.LifeEngageProduct[qArray1[0]].Questionnaire[qArray1[1]].Questions[qArray1[t]].detailsQuestionInfo  == "")
                {
				$scope.LifeEngageProduct[qArray1[0]].Questionnaire[qArray1[1]].Questions[qArray1[t]].details = "Yes";
                $scope.LifeEngageProduct[qArray1[0]].Questionnaire[qArray1[1]].Questions[qArray1[t]].detailsQuestionInfo  = "Yes";
                }                
			}
		}else{
			$rootScope.illnessYesNoflag = qArray1[3]; 
			$rootScope.checkIllnessOptionflag = qArray1[4];
			for(var t=5; t<qArray1.length; t++){
				$scope.LifeEngageProduct[qArray1[0]].Questionnaire[qArray1[1]].Questions[qArray1[t]].details = "No";   
                $scope.LifeEngageProduct[qArray1[0]].Questionnaire[qArray1[1]].Questions[qArray1[t]].detailsQuestionInfo  = "";
			}
		}	
	}
	
	//HealthDetails
if($rootScope.shortEappHealthRiderAvailable){
    
	if(qArray2.length > 0){
		if(qArray2[2] == 'Yes'){
			$rootScope.HealthYesNoflag = qArray2[3]; 
			$rootScope.checkHealthOptionFlag = qArray2[4]; 
			for(var t=5; t<qArray2.length; t++){
                if($scope.LifeEngageProduct[qArray2[0]].Questionnaire[qArray2[1]].Questions[qArray2[t]].detailsQuestionInfo  =="")
                {
				$scope.LifeEngageProduct[qArray2[0]].Questionnaire[qArray2[1]].Questions[qArray2[t]].details = "Yes";
                $scope.LifeEngageProduct[qArray2[0]].Questionnaire[qArray2[1]].Questions[qArray2[t]].detailsQuestionInfo  = "Yes";
                }
			}
		}else{
			$rootScope.HealthYesNoflag = qArray2[3];
			$rootScope.checkHealthOptionFlag = qArray2[4]; 			
			for(var t=5; t<qArray2.length; t++){
				$scope.LifeEngageProduct[qArray2[0]].Questionnaire[qArray2[1]].Questions[qArray2[t]].details = "No";
			}
		}	
	}
}
	
	EappVariables.getEappModel().Insured.Questionnaire = angular.copy($scope.LifeEngageProduct.Insured.Questionnaire);
	EappVariables.getEappModel().Payer.Questionnaire = angular.copy($scope.LifeEngageProduct.Payer.Questionnaire);
}

      $rootScope.validationBeforesave = function (value, successcallback) {
         
            var insuredData=$scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions;
			var payerData=$scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions;
		
			if(insuredData[0].details=='No' && insuredData[1].details=='No' && insuredData[2].details=='No'
			&& insuredData[3].details=='No' && insuredData[7].details=='No' && insuredData[4].details=='No' && insuredData[5].details=='No' &&
			insuredData[6].details=='No'){
				$scope.LifeEngageProduct.isInsured=false;
			}
			else{
			$scope.LifeEngageProduct.isInsured=true;
			}
		
			if(payerData[0].details=='No' && payerData[1].details=='No' && payerData[7].details=='No'  && payerData[2].details=='No'
			&& payerData[3].details=='No' && payerData[4].details=='No'){
				$scope.LifeEngageProduct.isPayer=false;
			}
			else{
				$scope.LifeEngageProduct.isPayer=true;
			}
                $scope.LifeEngageProduct.isFatca =true;
		

        EappVariables.setEappModel($scope.LifeEngageProduct);

        successcallback();
    };


         var load = $scope.$parent.$on('eappshortquestionnaire', function (event, args) {
        initialize();
         $rootScope.validationBeforesave = function (value, successcallback) {
             EappVariables.setEappModel($scope.LifeEngageProduct);
            successcallback();
        };
    });


    // $scope.setValue=function(index,value){
    // $scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[index].details=value;
    // }
	

function initialize() {
       
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
        }
        $scope.LifeEngageProduct = angular.copy(EappVariables.getEappModel());
        
        if (!$scope.LifeEngageProduct.Proposer.Questions) {
            $scope.LifeEngageProduct.Proposer.Questions = [];
        }
        if (!$scope.LifeEngageProduct.Proposer.Questions[0]) {
            $scope.LifeEngageProduct.Proposer.Questions[0] = {};
        }
        if (!$scope.LifeEngageProduct.Proposer.Questions[0].questionID) {
            $scope.LifeEngageProduct.Proposer.Questions[0].questionID = "301";
        }
        if (!$scope.LifeEngageProduct.Proposer.Questions[0].option) {
            $scope.LifeEngageProduct.Proposer.Questions[0].option = "";
        }
        
        $scope.eAppParentobj.PreviousPage = "eappshortquestionnaire_unload";
        $scope.PreviouspolicyHistoryList = [];
        $scope.remainingPreviousPolicyHistory = [];
        $scope.updateQuestionNo = function () {
            return ("7.");
        }
        if($scope.LifeEngageProduct.Insured.BasicDetails.isNationalityThai==true){
			$scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions.Questions[6].details="No";
			$scope.LifeEngageProduct.Insured.BasicDetails.isNationalityThai=false;
		}
		$timeout(function () {
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission") {
                UtilityService.disableAllActionElements();
            }else if (typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) {
                UtilityService.disableAllActionElements();
            }
        }, 50);

       if (rootConfig.isDeviceMobile) {
            $scope.QuestionnaireObj = angular.copy(EappVariables.QuestionModel);
        } else {
            $scope.QuestionnaireObj = angular.copy($scope.LifeEngageProduct.Proposer.Questionnaire);
        }

        $scope.eAppParentobj.CurrentPage = "eappshortquestionnaire";
        //For disabling and enabling the tabs.And also to know the last visited page.---starts
      //  $scope.LifeEngageProduct.LastVisitedUrl = "3,3,'eAppFatcaQuestionnaireJson',false,'FatcaQuestionnaire',''";
        
        $scope.eAppParentobj.nextPage = "3,1,'MainInsuredQuestionnaire',true,'HealthDetailsSubTab','eAppQuestionnaire'"; 
        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[1] < 2 && subIndex[0] <= $scope.LifeEngageProduct.LastVisitedUrl[0]) {
                $scope.LifeEngageProduct.LastVisitedIndex = "3,2";
            }
        }
        if (EappVariables.EappKeys.Key15 == "Confirmed" && $scope.$parent.LifeEngageProduct.Declaration.confirmSignature == true) {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = true;
       } else {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
        }
        
         $timeout(function () {
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission") {
                UtilityService.disableAllActionElements();
            }
        }, 0);
        
        //For disabling and enabling the tabs.And also to know the last visited page.---ends
        var model = "LifeEngageProduct";
        if ((rootConfig.autoSave.eApp) && (typeof EappVariables.EappKeys.Key15 == "undefined" || EappVariables.EappKeys.Key15 == "New" || EappVariables.EappKeys.Key15 == "Confirmed")) {
            if (AutoSave.destroyWatch) {
                AutoSave.destroyWatch();
            }
            AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
        } else if (AutoSave.destroyWatch) {
            AutoSave.destroyWatch();
        }

        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        angular.element('#' + $scope.selectedTabId + "_a").trigger('click');
          $timeout(function () {
            $scope.updateErrorCount('eappshortquestionnaire');
            $scope.refresh();
        }, 50)
    }


    }