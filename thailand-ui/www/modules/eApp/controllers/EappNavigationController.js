﻿'use strict';

storeApp.controller('EappNavigationController', ['$rootScope', '$scope',
'DataService', 'LookupService',
'DocumentService', 'ProductService',
   '$compile', '$routeParams', '$location',
   '$translate', '$debounce', 'UtilityService',
     'PersistenceMapping', 'AutoSave', 'globalService',
      'EappVariables', 'EappService',
function($rootScope, $scope, DataService, LookupService, DocumentService,
 ProductService, $compile, $routeParams, $location,
  $translate, $debounce, UtilityService, PersistenceMapping,
   AutoSave, globalService, EappVariables, EappService) {
	
	$scope.Initialize = function() {
		EappVariables.clearEappVariables();
		$scope.LifeEngageProduct = EappVariables.getEappModel();
		$scope.isPainted = false;
		$scope.eAppTabNavStyle = [];
		$scope.docview = false;
		$scope.tabview = false;
		$scope.innertabs = [];
		$scope.innertabs[0] = 'active';
		$scope.isRequirementRuleExecuted =true;
		$scope.innertabsVertical = [];
		$scope.innertabsVertical[0] = 'selected';
		$scope.showProposal = false;
		$scope.isPopupDisplayed = false;
		$scope.isAutoSave = false;
		$scope.eAppUIJson = rootConfig.eAppJson;
		EappVariables.clearEappVariables();
		EappVariables.EappKeys.Key5 = $routeParams.productId;
		EappVariables.EappKeys.Key4 = $routeParams.proposalId == 0 || $routeParams.proposalId == "-" ? "" : $routeParams.proposalId;
		EappVariables.EappKeys.Id = $routeParams.transactionId;
		EappVariables.EappKeys.Key3 = $routeParams.illustrationId == 0 || $routeParams.illustrationId == "-" ? "" : $routeParams.illustrationId;
		EappVariables.EappKeys.Key1 = EappVariables.leadId==undefined?"":EappVariables.leadId;
	    EappVariables.EappKeys.Key2 = EappVariables.fnaId==undefined?"":EappVariables.fnaId;
		$rootScope.transactionId = $routeParams.transactionId;
		$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);


	};
	$scope.paintInitialView = function() {
		LEDynamicUI.paintUI(rootConfig.template, $scope.eAppUIJson, "buyOnlineTabs", "#buyOnlineTabs", true, function(callbackId, data) {
			$scope.paintView(0,'InsuredDetailsSubTab',false,'tabsinner1');
			$scope.eAppTabNavStyle[$scope.eAppTabNavStyle.length -1] = $scope.Key15 != 'Final' || $scope.isRequirementRuleExecuted ? 'disabled':"";
			$scope.eAppTabNavStyle[$scope.eAppTabNavStyle.length -2] = $scope.Key15 != 'Final'? 'disabled':"";
			// call insured tab first
		}, $scope, $compile);
	}
	
	 //To be committed in LE
	$scope.$on('$viewContentLoaded', function(){
		$scope.initialLoad();
	  });
	
	$scope.initialLoad=function(){
		if ($routeParams.proposalId > 0 || $routeParams.transactionId > 0 || EappVariables.EappKeys.Key4 ||EappVariables.EappKeys.Id) {
			$scope.Initialize();
			EappService.getEapp(function() {
				$scope.LifeEngageProduct = EappVariables.getEappModel();
				$scope.isSTPRuleExecuted=false;
				if(EappVariables.EappModel.Requirements){
					$scope.isRequirementRuleExecuted =EappVariables.EappModel.Requirements.length>1?false:true;
				}
				$scope.Key15 = EappVariables.EappKeys.Key15;
				$scope.paintInitialView();
			}, '');
		} else {
			$scope.Initialize();
			$scope.LifeEngageProduct = EappVariables.getEappModel();
			EappService.prepopulateData();//For Pre-populating Data in to the eApp Model
			$scope.isSTPRuleExecuted=false;
			if(EappVariables.EappModel.Requirements){
				$scope.isRequirementRuleExecuted =EappVariables.EappModel.Requirements.length>1?false:true;
			}
			$scope.Key15 = EappVariables.EappKeys.Key15;
			$scope.paintInitialView();
		}
	}


	
	 $scope.paintView = function(index, id, isInnerTab, selectedTab) {
		    if(selectedTab=='Payment'){
		    $scope.id='Payment'
		    }
		  if(selectedTab=='DocumentUpload' && 	$scope.tabview == true){
		     $scope.showBackBtn = true;
		    }else{
			
			$scope.showBackBtn = false;
			}
		 

		   if((selectedTab=="Payment" &&  $scope.Key15=='Final') || (selectedTab=="DocumentUpload"  && !$scope.isRequirementRuleExecuted) || (selectedTab!="DocumentUpload" && selectedTab!="Payment")) 
		   {
		        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
		        if (!isInnerTab) {
		            for (var i = 0; i < $scope.eAppTabNavStyle.length; i++) {
		                $scope.tabPosition = index + 1;
		                if (i < index) {
		                    $scope.eAppTabNavStyle[i] = 'activated';
		                } else {
		                    if ($scope.eAppTabNavStyle[i] != 'disabled')
		                        $scope.eAppTabNavStyle[i] = '';
		                }
		            }
		            $scope.eAppTabNavStyle[index] = 'activate';
		        }
		        $scope.paintSuccess = function() {
		            $scope.$emit(id, [selectedTab]);
		            $(".tabs").tabs();
		            $scope.tabActivate();
					if(EappVariables.EappKeys.Key15 == 'Final'){
						$("#InsuredDetailsSubTab :input").prop('disabled', true);
						$("#ProposerDetailsSubTab :input").prop('disabled', true);
						$("#BeneficiaryDetailsSubTab :input").prop('disabled', true);
						$("#AppointeeDetailsSubTab :input").prop('disabled', true);
						$("#PayerDetailsSubTab :input").prop('disabled', true);
						$("#ProductDetails :input").prop('disabled', true);
						$("#LifestyleDetailsSubTab :input").prop('disabled', true);
						$("#HealthDetailsSubTab :input").prop('disabled', true);
						$("#FamilyHealthHistorySubTab :input").prop('disabled', true);
						$("#PreviousPolicyHistorySubTab :input").prop('disabled', true);
						$("#Declaration :input").prop('disabled', true);
						$("#Summary :input").prop('disabled', true);
					}
		        };
		        $scope.tabActivate = function() {
		            for (var i = 0; i < $scope.innertabs.length; i++) {
		                $scope.innertabs[i] = '';
		            }

		            $scope.innertabs[index] = 'active';
		            if ($scope.id != id) {
		                $scope.id = id;
		                angular.element('#' + id + '_Tab').trigger('click');
		            }
		            
		        }
		        $scope.isPainted = true;
				 if((selectedTab=="DocumentUpload"  && !$scope.isRequirementRuleExecuted) || (selectedTab!="DocumentUpload" && selectedTab!="Payment")){
					if (angular.element('#' + id).text().trim().length == 0) {
						$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
						if ($scope.LifeEngageProduct.Product.templates == undefined) {
							$scope.LifeEngageProduct.Product.templates = LifeEngageObject().Product.templates;
						}
						var productJsonArray = [$scope.eAppUIJson, $scope.LifeEngageProduct.Product.templates.eAppInputTemplate];
						LEDynamicUI.paintUI(rootConfig.template, productJsonArray, id, '#' + id, true, $scope.paintSuccess, $scope, $compile)
					} else {
						$scope.tabActivate();
						$scope.$emit(id, [selectedTab]);
					}
				}
			$rootScope.showHideLoadingImage(false);
			}
			
		}
	 $scope.Save = function(){
	        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
	        if(EappVariables.EappKeys.Key15 != 'Final'){
	            EappVariables.EappKeys.Key15 = EappVariables
	                    .getStatusOptionsEapp()[0].status;
	        }
	        EappService.saveTransaction($scope.onSaveSuccess, $scope.onSaveError,$scope.isAutoSave);
	};
	
	$scope.saveProposal = function(isAutoSave) {
        $scope.isAutoSave = isAutoSave;
        $scope.Save();
    };
    
	$scope.onSaveSuccess = function(data) {
		$rootScope.showHideLoadingImage(false);
		if (!$scope.isAutoSave) {
			$rootScope.NotifyMessages(false, "saveTransactionsSuccess", $translate, EappVariables.EappKeys.Key4);
		}
	};
	$scope.onSaveError = function(msg) {
		$rootScope.showHideLoadingImage(false);
		if (!$scope.isAutoSave) {
			$rootScope.NotifyMessages(true, msg);
		}
	};
	
	$scope.backToDocumentUpload = function(){
        $scope.showBackBtn = false;
        $scope.docview = true; 
        $scope.tabview = false;
        $scope.$emit('DocumentUpload',["DocumentUpload"]);
    }
	
	/*$scope.downloadApplicationPDF = function() {
		$rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
		if ((rootConfig.isDeviceMobile)) {
			if (checkConnection()) {
				EappService.downloadApplicationPdf($scope.pdfSuccessCallback, $scope.pdfErrorCallback);
			} else {
				$rootScope.showHideLoadingImage(false);
				$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "networkNotAvailableError"), translateMessages($translate, "fna.ok"));
			}
		} else {
			EappService.downloadApplicationPdf($scope.pdfSuccessCallback, $scope.pdfErrorCallback);
		}

	};
	$scope.pdfErrorCallback = function() {
		$rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
		$rootScope.showHideLoadingImage(false);
	};
	$scope.pdfSuccessCallback = function() {
		$rootScope.showHideLoadingImage(false);
	};*/
}]);
