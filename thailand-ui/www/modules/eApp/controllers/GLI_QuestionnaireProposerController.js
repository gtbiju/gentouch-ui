'use strict';
/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:GLI_QuestionnaireProposerController
 CreatedDate:23/12/2015
 Description:GLI_QuestionnaireProposerController 
 */
storeApp.controller('GLI_QuestionnaireProposerController', GLI_QuestionnaireProposerController);
GLI_QuestionnaireProposerController.$inject = ['$rootScope', '$scope', 'DataService', 'LookupService', 'DocumentService', 'ProductService', '$compile', '$routeParams', '$location', '$translate', '$debounce', 'UtilityService', 'PersistenceMapping', 'AutoSave', 'globalService', 'EappVariables', 'EappService', 'GLI_EappVariables', '$timeout', 'GLI_EappService'];

function GLI_QuestionnaireProposerController($rootScope, $scope, DataService, LookupService, DocumentService, ProductService, $compile, $routeParams, $location, $translate, $debounce, UtilityService, PersistenceMapping, AutoSave, globalService, EappVariables, EappService, GLI_EappVariables, $timeout, GLI_EappService) {

    $rootScope.selectedPage = "PolicyHolderQuestionnaire";
    $scope.selectedTabId = 'PolicyHolderQuestionnaire';
    $scope.LifeEngageProduct.LastVisitedUrl = "";
    $scope.eAppParentobj.nextPage = "";
    $scope.eAppParentobj.CurrentPage = "";
    $scope.QuestionnaireObj = {};
    $scope.showErrorCount = true;    
    $scope.isDecweight=false;
    $scope.isIndividual = true; //temporary variable used for hiding and showing Questions 19.a or 19 b for individual and corporate clients
	$scope.alphabetWithSpacePatternIllustration =  rootConfig.alphabetWithSpacePatternIllustration;
	$scope.alphaNumericEappCustom =  rootConfig.alphaNumericEappCustom; 
   
   $scope.item={
  "id": "",
  "label": "" 
};
   
    $scope.items = [{
    id: 1,
  label: '1' 
}, {
  id: 2,
  label: '2'
}, 
{
  id:3,
  label: '3'
}, 
{
  id: 4,
  label: '4'
}, {
  id:5,
  label: '5'
},
 {
  id: 6,
  label: '6'
}, {
  id: 7,
  label: '7'
}, {
  id: 8,
  label: '8'
}, {
  id: 9,
  label: '9'
}
];
    var unload = $scope.$parent.$on('LifeStyleSubTab_unload', function (event) {
        var data = $scope.$parent;
        angular.element(document.querySelector('#PolicyHolderQuestionnaire')).empty();
        $scope.$destroy();
        $scope.$parent = data;
        load();
        unload();
    });

    //Controller Initialize method
    function initialize() {

        if($rootScope.editPageNavigation){
            $scope.popupTabsNav($rootScope.editPageNavigation[0],$rootScope.editPageNavigation[1],true);
            $rootScope.editPageNavigation = undefined;
        }
        $scope.$parent.disableProceedButtonCommom = false;
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
        }
        $scope.showErrorCount = true;
        $scope.IsPreviouspolicySection = false;
        
        
       
        
        
        $scope.LifeEngageProduct = angular.copy(EappVariables.getEappModel());

        if($('#Questionnaire .selector-icon li').length == '1'){
            $rootScope.hideInsuredLine = false;
        } else {
            $rootScope.hideInsuredLine = true;
        }

        
        if (!$scope.LifeEngageProduct.Payer.Questions) {
            $scope.LifeEngageProduct.Payer.Questions = [];
        }
        if (!$scope.LifeEngageProduct.Payer.Questions[0]) {
            $scope.LifeEngageProduct.Payer.Questions[0] = {};
        }
        if (!$scope.LifeEngageProduct.Payer.Questions[0].questionID) {
            $scope.LifeEngageProduct.Payer.Questions[0].questionID = "301";
        }
        if (!$scope.LifeEngageProduct.Payer.Questions[0].option) {
            $scope.LifeEngageProduct.Payer.Questions[0].option = "";
        }
        $scope.eAppParentobj.PreviousPage = "LifeStyleSubTab_unload";
        $scope.PreviouspolicyHistoryList = [];
        $scope.remainingPreviousPolicyHistory = [];
        $scope.updateQuestionNo = function () {
            return ("7.");
        }
        $timeout(function () {
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission") {
                UtilityService.disableAllActionElements();
            }else if (typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) {
                UtilityService.disableAllActionElements();
            }
        }, 50);

        if (rootConfig.isDeviceMobile) {
            $scope.QuestionnaireObj = angular.copy(EappVariables.QuestionModel);
			
			if($scope.QuestionnaireObj.Other.Questions[274].details != "" && $scope.QuestionnaireObj.Other.Questions[274].details != undefined && $scope.QuestionnaireObj.Other.Questions[274].detailsOther != "" && $scope.QuestionnaireObj.Other.Questions[274].unit != ""){
	       $scope.QuestionnaireObj.Other.Questions[274].details.id = $scope.QuestionnaireObj.Other.Questions[274].unit;
	        $scope.QuestionnaireObj.Other.Questions[274].details.label = $scope.QuestionnaireObj.Other.Questions[274].detailsOther;
} 


             if($scope.QuestionnaireObj.Other.Questions[274].details ==""){
            
            $scope.QuestionnaireObj.Other.Questions[274].details = $scope.item;
            var unit="";
            if($scope.QuestionnaireObj.Other.Questions[274].unit!="")
            {
            unit=parseInt($scope.QuestionnaireObj.Other.Questions[274].unit);
            }
            else{
            unit=$scope.QuestionnaireObj.Other.Questions[274].unit;
            }


            $scope.QuestionnaireObj.Other.Questions[274].details.id = unit;
		    $scope.QuestionnaireObj.Other.Questions[274].details.label = $scope.QuestionnaireObj.Other.Questions[274].detailsOther;
	
            }

        } else {
            $scope.QuestionnaireObj = angular.copy($scope.LifeEngageProduct.Payer.Questionnaire);
			
			if($scope.QuestionnaireObj.Other.Questions[274].details != "" && $scope.QuestionnaireObj.Other.Questions[274].details != undefined && $scope.QuestionnaireObj.Other.Questions[274].detailsOther != "" && $scope.QuestionnaireObj.Other.Questions[274].unit != ""){
	        $scope.QuestionnaireObj.Other.Questions[274].details.id = $scope.QuestionnaireObj.Other.Questions[274].unit;
	        $scope.QuestionnaireObj.Other.Questions[274].details.label = $scope.QuestionnaireObj.Other.Questions[274].detailsOther;
} 

            if($scope.QuestionnaireObj.Other.Questions[274].details ==""){
            
            $scope.QuestionnaireObj.Other.Questions[274].details = $scope.item;

            var unit="";
            if($scope.QuestionnaireObj.Other.Questions[274].unit!="")
            {
            unit=parseInt($scope.QuestionnaireObj.Other.Questions[274].unit);
            }
            else{
            unit=$scope.QuestionnaireObj.Other.Questions[274].unit;
            }
            $scope.QuestionnaireObj.Other.Questions[274].details.id = unit;
		    $scope.QuestionnaireObj.Other.Questions[274].details.label = $scope.QuestionnaireObj.Other.Questions[274].detailsOther;
	
            }

        }

        $scope.eAppParentobj.CurrentPage = "PolicyHolderQuestionnaire";
        //For disabling and enabling the tabs.And also to know the last visited page.---starts
        //$scope.LifeEngageProduct.LastVisitedUrl = "3,2,'PolicyHolderQuestionnaire',true,'LifeStyleSubTab','eAppProposerQuestionnaire'";
        $scope.LifeEngageProduct.LastVisitedUrl = "3,6,'eappshortquestionnaire',true,'eappshortquestionnaire',''";
        if ($scope.LifeEngageProduct.Proposer) {
            $scope.eAppParentobj.nextPage = "3,3,'eAppFatcaQuestionnaireJson',true,'FatcaQuestionnaire',''";
        } else {
            $scope.eAppParentobj.nextPage = "4,1,'SPAJsummary',true,'SPAJsummary',''";
        }
        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[1] < 2 && subIndex[0] <= $scope.LifeEngageProduct.LastVisitedUrl[0]) {
                $scope.LifeEngageProduct.LastVisitedIndex = "3,2";
            }
        }
        if (EappVariables.EappKeys.Key15 == "Confirmed" && $scope.$parent.LifeEngageProduct.Declaration.confirmSignature == true) {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = true;
        } else {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
        }
		
		 $timeout(function () {
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission") {
                UtilityService.disableAllActionElements();
            }
        }, 0);
		
        //For disabling and enabling the tabs.And also to know the last visited page.---ends
        var model = "LifeEngageProduct";
        if ((rootConfig.autoSave.eApp) && (typeof EappVariables.EappKeys.Key15 == "undefined" || EappVariables.EappKeys.Key15 == "New" || EappVariables.EappKeys.Key15 == "Confirmed")) {
            if (AutoSave.destroyWatch) {
                AutoSave.destroyWatch();
            }
            AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
        } else if (AutoSave.destroyWatch) {
            AutoSave.destroyWatch();
        }

        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
		 
		 
        
         $scope.LifeEngageProduct.Payer.Questionnaire=$scope.QuestionnaireObj;
         var lifeStyleQtnslength=$scope.LifeEngageProduct.Payer.Questionnaire.LifestyleDetails.Questions.length;
         var illnessQtnslength=$scope.LifeEngageProduct.Payer.Questionnaire.Other.Questions.length;       
         var Addlength= $scope.LifeEngageProduct.Payer.Questionnaire.AdditionalQuestioniare.Questions.length;
         
        if($scope.LifeEngageProduct.Payer.Questionnaire.LifestyleDetails.Questions[lifeStyleQtnslength-1].details=="Yes")
        {
            $("#headerText_payer0").removeClass("diableCheckMark");
         	$("#headertabs_payer0").removeClass("QuestionnaireCheckbox");
        }
		
         if($scope.LifeEngageProduct.Payer.Questionnaire.Other.Questions[illnessQtnslength-1].details=="Yes")
        {
		  $("#headerText_payer1").removeClass("diableCheckMark");
          $("#headertabs_payer1").removeClass("QuestionnaireCheckbox");
        }
        
         if($scope.LifeEngageProduct.Payer.Questionnaire.AdditionalQuestioniare.Questions[Addlength-1].details=="Yes")
        {
		  $("#headerText_payer2").removeClass("diableCheckMark");
          $("#headertabs_payer2").removeClass("QuestionnaireCheckbox");
        }       
                
          
         if($scope.QuestionnaireObj.LifestyleDetails.Questions[28].details=='Yes' && $scope.QuestionnaireObj.LifestyleDetails.Questions[30].details=="")
         {
         $scope.isIncWeight =true;           
         }
        else
         {
         $scope.isIncWeight =false;         
         }
               
        
        
        if ($scope.selectedTabId == 'PolicyHolderQuestionnaire') {
            $timeout(function () {
                $scope.updateErrorCount($scope.selectedTabId);
                $scope.eAppParentobj.errorCount = $scope.errorCount;
                $scope.refresh();
            }, 50)
        }            
         
    }

    $scope.updateErrorCount = function (id) {
        $rootScope.updateErrorCount(id);
        $scope.selectedTabId = id;
        $scope.eAppParentobj.errorCount = $scope.errorCount;
    }
	
	$scope.formatLabel = function (model, options, type) {
        if (options && options.length > 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code) {
                    return options[i].value;
                }
            }
        }
		 $timeout(function () {
            $scope.updateErrorCount('PolicyHolderQuestionnaire');
            $scope.refresh();
        }, 50)
}
 $scope.onSelectTypeHead = function (id) {
        $timeout(function () {
            $('#' + id).blur();
        }, 300);
    }

    $scope.checkErrorCountOnTabNav = function ($event, param) {
        var currentTab = $scope.selectedTabId;
        var clickedTab = 'tabsinner' + $scope.selectedSectionId + param;
        var currentTabErrorCount = $scope.errorCount;
        var clickedTabErrorCount = $scope.errorCount;
        if (currentTabErrorCount > 0) {
            if (clickedTabErrorCount == 0) {
                $scope.selectedTabId = clickedTab;
            } else {
                $scope.selectedTabId = currentTab;
                $event.stopPropagation();
                $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                    translateMessages($translate, "enterMandatoryFieldsForeApp"),
                    translateMessages($translate, "fna.ok"));
            }
        } else {
            $scope.selectedTabId = clickedTab;
        }
    }

    //HealthDetailsController Load Event and Load Event Handler
    var load = $scope.$parent.$on('PolicyHolderQuestionnaire', function (event, args) {
        initialize();
    });

    function questionnaireBeforeSaveFn(successcallback) {
        //Add previousPolicy history
        //$scope.LifeEngageProduct.PreviouspolicyHistory = [];
        //if ($scope.PreviouspolicyHistoryList.length > 0) {
        // angular.forEach($scope.PreviouspolicyHistoryList, function(value){                	
        //$scope.LifeEngageProduct.PreviouspolicyHistory.push(value);
        //});

        //}
        //if ($scope.remainingPreviousPolicyHistory.length > 0) {
        //  angular.forEach($scope.remainingPreviousPolicyHistory, function(value){                	
        //$scope.LifeEngageProduct.PreviouspolicyHistory.push(value);
        // });

        //}
        successcallback();
    }

    /*	 $scope.$on("$destroy", function() {
                     if (this.$$destroyed) return;     
    				 while (this.$$childHead) {      
    				 this.$$childHead.$destroy();    
    				 }     
    				 if (this.$broadcast)
    				{ 
    					this.$broadcast('$destroy');   
    				}    
    				 this.$$destroyed = true;                 
    				 $timeout.cancel( timer );                           
            });*/

    $rootScope.validationBeforesave = function (value, successcallback) {
        
        if (value == "save") {
            $scope.LifeEngageProduct.LastVisitedIndex = "3,2";
        }
        if (rootConfig.isDeviceMobile) {
            EappVariables.setEappModel($scope.LifeEngageProduct);
        }
        $scope.validateBeforeSaveDetails(value, successcallback);
    };

    $scope.validateBeforeSaveDetails = function (value, successcallback) {
        
           if($scope.QuestionnaireObj.Other.Questions[274].details != undefined && $scope.QuestionnaireObj.Other.Questions[274].details.label != "" && $scope.QuestionnaireObj.Other.Questions[274].details.id != ""){
			$scope.QuestionnaireObj.Other.Questions[274].detailsOther = $scope.QuestionnaireObj.Other.Questions[274].details.label;
			$scope.QuestionnaireObj.Other.Questions[274].unit = $scope.QuestionnaireObj.Other.Questions[274].details.id;
		    }
		
		
            if($rootScope.lifeStylePayerTickVisiblity){
            var length=$scope.QuestionnaireObj.LifestyleDetails.Questions.length;
            $scope.QuestionnaireObj.LifestyleDetails.Questions[length-1].details="Yes";
                $rootScope.lifeStylePayerTickVisiblity=undefined;
            }
        
           if($rootScope.illnessPayerTickVisiblity){
             var length=$scope.QuestionnaireObj.Other.Questions.length;
             $scope.QuestionnaireObj.Other.Questions[length-1].details="Yes";
                       $rootScope.illnessPayerTickVisiblity=undefined;
           }
        
           if($rootScope.addpayer){
             var length=$scope.QuestionnaireObj.AdditionalQuestioniare.Questions.length;
             $scope.QuestionnaireObj.AdditionalQuestioniare.Questions[length-1].details="Yes";
                       $rootScope.addpayer=undefined;
           }
        
        
        $scope.LifeEngageProduct.Payer.Questionnaire = angular.copy($scope.QuestionnaireObj);
        
           
        
        if ((rootConfig.isDeviceMobile)) {
            EappService.getEapp(function () {
                EappVariables.setEappModel($scope.LifeEngageProduct);
                questionnaireBeforeSaveFn(function () {
                    successcallback();
                });
            }, function () {});
        } else {
			EappVariables.setEappModel($scope.LifeEngageProduct);
            questionnaireBeforeSaveFn(function () {
                successcallback();
            });
        }  
      
    }
	
	$scope.ClearFieldsValues = function (carouselName,model,length,start,end,isCarousel){
      
model=model.split(".");
    var data=[];  
    angular.forEach(model,function(value,key) {    
    switch(key)
    {
    case 0:
        data= $scope[value];
        break;
    case 1:    
        data=data[value]; 
        break;
    case 2:		
		var strlen=value.split("[")[key-2].split("]")[key-2]; 	
		data=data[strlen]; 
        break;        
    } 
 });
    if(isCarousel)
	{		
	 for(var i = start; i<= end;i++)
	{
      data[i].details='';        
	}	
	}
	else
	{
		for(var i = start; i< (start+length);i++)
	{
      data[i].details='';        
	}  
}

if(carouselName!="")
{
    var datePatternValidation = "datePatternValidation"+"_"+ carouselName;    
    var patternShow = "patternShow" + "_" + carouselName;
    $scope[datePatternValidation] ="";
    $scope[patternShow]=false;    
}

   angular.forEach($scope.carouselDateFields,function(index,value){ 
       
    var datePatternValidation = "datePatternValidation"+"_"+ index;    
    var patternShow = "patternShow" + "_" + index;
     $scope[datePatternValidation] ="";
     $scope[patternShow]=false;
 });

      $timeout(function () {
            $scope.updateErrorCount('PolicyHolderQuestionnaire');
            $scope.refresh();
        }, 50)
}

$scope.ClearFieldsValuesForTexboxAlone = function (name,model,start,end,exceptionStart,exceptionEnd){
	
    model=model.split(".");
    var data=[];  
    angular.forEach(model,function(value,key) {    
    switch(key)
    {
    case 0:
        data= $scope[value];
        break;
    case 1:    
        data=data[value]; 
        break;
    case 2:		
		var strlen=value.split("[")[key-2].split("]")[key-2]; 	
		data=data[strlen]; 
        break;        
    } 
 });
    
	if(end!=undefined){
		for(var i = start; i<= end;i++) {
			data[i].details='';        
		}
	} else {
		data[start].details='';
	}
	if(exceptionStart!=undefined){
		for(var i = exceptionStart; i<= exceptionEnd;i++) {
			data[i].details='';        
		}
	}    
 
    var datePatternValidation = "datePatternValidation"+"_"+name;    
    var patternShow = "patternShow" + "_" + name;
    $scope[datePatternValidation] ="";
     $scope[patternShow]=false;

       $timeout(function () {
            $scope.updateErrorCount('PolicyHolderQuestionnaire');
            $scope.refresh();
        }, 50)
}

$scope.Q1=["HyperTensionDetailsSection","HeartDiseaseDetailsSection","CoronaryHeartDiseaseDetailsSection","CardiovascularDiseaseDetailsSection","CerebroVascularDiseaseDetailsSection","PartialOrTotalParalysisDetailsSection","DiabeticMellitusDetailsSection","ThyroidDiseaseDetailsSection"];

$scope.Q2=["CancerDiseaseDetailsSection","EnlargedLymphNodeDiseaseDetailsSection","TumorDiseaseDetailsSection","cystDetailsSection"];

$scope.Q3=["PancreatitisDetailsSection","KidneyDiseaseDetailsSection","JaundiceDiseaseDetailsSection","SplenomegalyDetailsSection","PepticUlcerDetailsSection","LiverDetailsSection","AlcoholismDetailsSection"];

$scope.Q4=["LungDiseasePneumoniaDetailsSection","TuberculosisDetailsSection","AsthmaDiseaseDetailsSection","pulmonaryDiseaseDetailsSection","EmphysemaDetailsSection","SleepApneaDetailsSection"];

$scope.Q5=["ImpairedVisionDetailsSection","RetinaDiseaseDetailsSection","GlaucomaDetailsSection"];

$scope.Q6=["ParkinsonsDetailsSection","AlzheimersDetailsSection","EpilepsyDetailsSection"];

$scope.Q7=["ArthritisDetailsSection","GoutDetailsSection","SclerodermaDetailsSection","SLEDetailsSection","BloodDiseaseDetailsSection"];

$scope.Q8=["PsychosisDetailsSection","NeurosisDetailsSection","DepressionDetailsSection","DownSyndromeDetailsSection","PhysicalDisabilitySection"];

$scope.Q9=["AIDSDiseaseDetailsSection","VenerealDiseaseDetailsSection"];

$scope.Q10=["ChestPainDetailsSection","PalpitationDetailsSection","AbnormalFatigueDetailsSection","MuscularWeaknessDetailsSection","AbnormalPhysicalMovementDetailsSection","SensoryNeuronDetailsSection"];

$scope.Q11=["ChronicStomachAcheDetailsSection","HematemesisOrHematocheziaDetailsSection","DropsyDetailsSection","ChronicDiarrheaDetailsSection","HematuriaDetailsSection","ChronicCoughDetailsSection","HemoptysisDetailsSection"];

$scope.Q12=["PalpableTumorDetailsSection","HeadacheDetailsSection",""];

$scope.Q13=["JointPainsDetailsSection","BruisesDetailsSection"];

$scope.Q14=["ImpairedVisionDetailsSection2","SlowDevelopmentDetailsSection","AttemptedToHurtDetailsSection"];

$scope.carouselDateFields=[];
$scope.tab=function(model,questions,start,end,length,exceptionStart,expectionEnd){	
	var tabCheckIndex=2;
	var tabIndex=1;
	
	if(model == "Other"){
		for(var i=0; i < $scope.QuestionnaireObj.Other.Questions[start-1].options.length; i++){
			$scope.QuestionnaireObj.Other.Questions[start-1].options[i].details = "false";
		}
	}else{
		for(var i=0; i < $scope.QuestionnaireObj.LifestyleDetails.Questions[start-1].options.length; i++){
			$scope.QuestionnaireObj.LifestyleDetails.Questions[start-1].options[i].details = "false";
		}
	}
			angular.forEach($scope.indexArrayList,function(value,key) {    					
				var model="";
				$scope.tabName=value;
				
			
			    if(questions.indexOf(value) > -1)
				{				
                       if($('.'+value).children("div .col-xs-3").children().attr('name')!=undefined) {
                      $scope.carouselGetDateName =  $('.'+value).children("div .col-xs-3").children().attr('name');
                    }
                     
                     if($scope.carouselDateFields.indexOf($scope.carouselGetDateName) == -1){
                         $scope.carouselDateFields.push($scope.carouselGetDateName);
                         }   

					$scope.indexArrayList[value]=false;
				}
                });	

				 angular.forEach($scope.selectedTabList,function(value,key) {    
				 var carouseltabcheckbox = value;
				 carouseltabcheckbox=carouseltabcheckbox.split("_")[tabCheckIndex];
				 if(questions.indexOf(carouseltabcheckbox) > -1)
				 {			     
				 $(value).removeClass("checkmarkActive");
				 var carouseltabindex=value.split("_")[tabIndex];
				 var carouseltabName=value.split("_")[tabCheckIndex];
				 $("#carousel_"+carouseltabindex+"_"+carouseltabName).removeClass("active");			  
				 }
				 });
				if(model=="Other") {
					model='QuestionnaireObj.Other.Questions';	
				} else if (model=="life") {
					model='QuestionnaireObj.LifestyleDetails.Questions';	
				}
				$scope.ClearFieldsValues("",model,length,start,end,true);
				if(exceptionStart!=0 || expectionEnd!=0)
				{					
				$scope.ClearFieldsValues("",model,length,exceptionStart,expectionEnd,true);
				}
					
			$timeout(function(){
					$scope.updateErrorCount('PolicyHolderQuestionnaire')
					$scope.refresh();
				},50)		
}

$scope.isLifeStyle=false;
$scope.isOther=true;

$scope.selectedTabList = [];
$scope.disableElement = function(index,status,rel,model,questionNumber,radioQtnNo,carouselName,length,start,end,isCarousel){
    
        var modelData = model;
	if($scope.tabedIndex == index +'-'+ rel){
			model=model.split(".");
			var tab = model[1];
			
			if ($('#tabIndex'+'_'+index+ "_" + rel).hasClass("checkmarkActive")) {  
				$('#tabIndex'+'_'+index+ "_" +rel).removeClass("checkmarkActive");
				$scope.carouselId = "";
				
				if($scope.indexArrayList.indexOf(rel) == -1){					
                    $scope.indexArrayList[rel] = false;
					$scope.indexArrayList.push(rel);
                } else {
					$scope.indexArrayList[rel] = false;
					 var totalCount=0;
                    for(var i=0;i<$scope.indexArrayList.length;i++){
                        if($scope.indexArrayList[rel]){
                            totalCount++;
                        }
                    }
                    if(totalCount==1){
                        var radioQuestion="radioQuestion" + radioQtnNo;
                        $scope[radioQuestion] = false;
                    }     
                }
                    $timeout(function(){
                           $scope.updateErrorCount('PolicyHolderQuestionnaire')
                           $scope.refresh();
                       },50)
					   
					  if (tab=="LifestyleDetails") {
						$scope.QuestionnaireObj.LifestyleDetails.Questions[questionNumber].options[index].details = 'false';
						$scope.isLifeStyle=true;
						$scope.isOther=false;
					} else if (tab=="Other") {			
						$scope.QuestionnaireObj.Other.Questions[questionNumber].options[index].details = 'false';
						$scope.isOther=true;
						scope.isLifeStyle=false;
				}
				}
				else{
					$('#tabIndex'+'_'+index+ "_" + rel).addClass("checkmarkActive");
					$scope.carouselId = rel;
					if($scope.indexArrayList.indexOf(rel) == -1){
                   		$scope.indexArrayList[rel] = true;
						$scope.indexArrayList.push(rel);
                	} else {
						$scope.indexArrayList[rel] = true;
					}
					
					if(tab=="LifestyleDetails") {
						$scope.QuestionnaireObj.LifestyleDetails.Questions[questionNumber].options[index].details = 'true';
						$scope.isLifeStyle=true;
						$scope.isOther=false;
					} else if(tab=="Other") {			
						$scope.QuestionnaireObj.Other.Questions[questionNumber].options[index].details = 'true';
						$scope.isOther=true;
						scope.isLifeStyle=false;
					}
				}
                $scope.ClearFieldsValues(carouselName,modelData,length,start,end,isCarousel);
				
			}
				
			else{
				//$('#tabIndex'+'_'+index+ "_" +rel).addClass("checkmarkActive");
				$('#tabIndex'+'_'+index+ "_" +rel).addClass("QuestionnaireCheckbox");
				$('#'+rel).addClass("QuestionnaireCheckbox");
				
				var tabId='#tabIndex'+'_'+index+ "_" + rel;
				//$scope.carouselId = rel;
				$scope.selectedTabList.push(tabId);
				
				if($scope.indexArrayList.indexOf(rel) == -1){
                    $scope.indexArrayList[rel] = true;
					$scope.indexArrayList.push(rel);
                } else {
					$scope.indexArrayList[rel] = true;
				}
				
			}
			if(isOther){
			 for(var i=0;i<$scope.QuestionnaireObj.Other.Questions[questionNumber].options.length;i++){
				if($scope.QuestionnaireObj.Other.Questions[questionNumber].options[i].details == "true"){ 
					var radioQuestion="radioQuestion" + radioQtnNo;
					   $scope[radioQuestion] = true;
					   return;
					}else{
						var  radioQuestion="radioQuestion" + radioQtnNo;
					   $scope[radioQuestion] = false;
					}
               }
			}
			$timeout(function(){
					$scope.updateErrorCount('PolicyHolderQuestionnaire')
					$scope.refresh();
				},50)
		}	

$scope.indexArrayList = [];
$scope.carosuelTab = function(rel, index, questionNumber,model,radioQtnNo){
		var  radioQuestion="radioQuestion" + radioQtnNo;
             $scope[radioQuestion] = true;
      
		if($scope.indexArrayList.indexOf(rel) == -1){
			$scope.indexArrayList[rel] = true;
			$scope.indexArrayList.push(rel);
    	} else {
			$scope.indexArrayList[rel] = true;
		}
		
		$timeout(function(){
					$scope.updateErrorCount('PolicyHolderQuestionnaire')
					$scope.refresh();
				},50)
	
		var carouselTabSection = 'carousel'+'-'+index+'-'+rel;
		$scope.carouselTabSection = carouselTabSection;
		model=model.split(".");
		var tab = model[1];
		$scope.carouselId = "";
		for (var i = 0; i <= 200; i++) {
			if (i == index) {				
				
				$scope.tabedIndex = index +'-'+ rel;
				$('#tabIndex'+'_'+i+'_'+rel).removeClass("QuestionnaireCheckbox");	
				$('#tabIndex'+'_'+index+'_'+rel).addClass("checkmarkActive");
				var tabIndexid='#tabIndex'+'_'+index+ "_" + rel;				
				$scope.selectedTabList.push(tabIndexid);
				
				if(tab=="LifestyleDetails")
					$scope.QuestionnaireObj.LifestyleDetails.Questions[questionNumber].options[index].details = 'true';
			 
				else if(tab=="Other")			
					$scope.QuestionnaireObj.Other.Questions[questionNumber].options[index].details = 'true';
						
			} else {
				
				$scope.tabedIndex = index +'-'+ rel;
				$('#tabIndex'+'_'+i+'_'+rel).addClass("QuestionnaireCheckbox");
			}
		}

		$scope.carouselId = rel;
	}	

	$scope.displayCheckMark=function(start,len){
		if( $scope.errorCount){
			$('#headerText_payer'+start).addClass("diableCheckMark");
				for(var i=0;i<=len;i++){
					if(i!=start)
						$('#headertabs_payer'+i).addClass("QuestionnaireCheckbox");
				}
		}
        else{
			// $('#headerText_payer'+start).removeClass("diableCheckMark");	
			for(var i=0;i<=len;i++){
					if(i!=start)
						$('#headertabs_payer'+i).removeClass("QuestionnaireCheckbox");
				}
		}
	}

$scope.displayCarouselErrorMessage=function(isCarousel,name){
    
	if(isCarousel){		
        $scope[name] = false;
    }
      $timeout(function () {
            $scope.updateErrorCount('PolicyHolderQuestionnaire');
            $scope.refresh();
        }, 50)
}


$scope.patternShow=false;

$scope.validateDate = function(input,name) 
{    

    var datePatternValidation = "datePatternValidation"+"_"+name;    
    var patternShow = "patternShow" + "_" + name;
   
    $scope[datePatternValidation]="";
    $scope[patternShow]=false;
   
if(input!=undefined)
    {
    var inputData = input.split('/');
 if(inputData.length>3){
      $scope[datePatternValidation] ="ng-invalid ng-invalid-pattern";
      $scope[patternShow]=true;
    }
    else
    {

    if(inputData.length > 2)
    input= inputData[1]+'/' + inputData[0] + '/' + inputData[2];
    var date = new Date(input);   
    
    if(date.getFullYear() >= 2500 && date.getFullYear() <= 2699)
    {     
    var datePattern = date.getDate() === +inputData[0] && date.getMonth() + 1 === +inputData[1] &&  date.getFullYear() === +inputData[2];
    if(!datePattern)	
     {   
     $scope[datePatternValidation] ="ng-invalid ng-invalid-pattern";
     $scope[patternShow]=true;
     }
    }		
       else
        {
            $scope[datePatternValidation]="ng-invalid ng-invalid-pattern";
            $scope[patternShow]=true; 
        }
    }
	
	// if(datePattern){

	// 		var day = (inputData[0].length < 2) ? 0+''+inputData[0] : inputData[0];	
	// 		var month = (inputData[1].length < 2) ? 0+''+inputData[1] : inputData[1];	
	// 		input= day+'/' + month + '/' + inputData[2];
	// 		$scope.QuestionnaireObj.Other.Questions[2].details = input;

		
}

            $timeout(function () {
            $scope.updateErrorCount('PolicyHolderQuestionnaire');
            $scope.refresh();
        }, 50)
}

$scope.checkCarouselSelect=function(questionNumber,radioQtnNo){
	var  radioQuestion="radioQuestion" + radioQtnNo;
	$scope[radioQuestion]=false;
	if($scope.QuestionnaireObj.Other.Questions[questionNumber-1].details == 'Yes'){
		for(var i=0;i<$scope.QuestionnaireObj.Other.Questions[questionNumber].options.length;i++){
			if($scope.QuestionnaireObj.Other.Questions[questionNumber].options[i].details == "true"){ 					
			   $scope[radioQuestion] = true;
			   return;
			   //$scope.apply();
		   }
		}
	}
}

$scope.checkWeightMandatory=function(){    
$scope.isIncWeight=true;
$scope.isBothWeight=false;
$scope.isDecweight=false;
}

$scope.mandatoryFieldCheck= function(Question){
$scope.isBothWeight=false;
$scope.isIncWeight=false;
$scope.isDecweight=false;

var radio =$scope.QuestionnaireObj.LifestyleDetails.Questions[28].details;
var incrWeight=$scope.QuestionnaireObj.LifestyleDetails.Questions[29].details;
var decWeight=$scope.QuestionnaireObj.LifestyleDetails.Questions[30].details;

    
if(radio=='Yes' && (incrWeight=='' || incrWeight== undefined) && (decWeight=='' || decWeight==undefined ))
$scope.isIncWeight=true;

else if(radio=='Yes' && (incrWeight!='' && incrWeight!=undefined) && (decWeight!='' && decWeight!=undefined))
 $scope.isBothWeight=true;    

     $timeout(function () {
            $scope.updateErrorCount('PolicyHolderQuestionnaire');
            $scope.refresh();
        }, 50)
}	

}