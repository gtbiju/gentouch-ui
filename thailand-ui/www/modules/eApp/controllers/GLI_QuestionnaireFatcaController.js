'use strict';
/*
* Copyright 2015, LifeEngage 
 */
/*
Name:GLI_QuestionnaireFatcaController
CreatedDate:23/12/2015
Description:GLI_QuestionnaireFatcaController 
 */
storeApp.controller('GLI_QuestionnaireFatcaController', GLI_QuestionnaireFatcaController);
GLI_QuestionnaireFatcaController.$inject = ['$rootScope', '$scope', 'DataService', 'LookupService', 'DocumentService', 'ProductService', '$compile', '$routeParams', '$location', '$translate', '$debounce', 'UtilityService', 'PersistenceMapping', 'AutoSave', 'globalService', 'EappVariables', 'EappService', 'GLI_EappVariables', '$timeout', 'GLI_EappService'];

function GLI_QuestionnaireFatcaController($rootScope, $scope, DataService, LookupService, DocumentService, ProductService, $compile, $routeParams, $location, $translate, $debounce, UtilityService, PersistenceMapping, AutoSave, globalService, EappVariables, EappService, GLI_EappVariables, $timeout, GLI_EappService) {

    $rootScope.selectedPage = "FatcaQuestionnaire";
    $scope.selectedTabId = 'eAppFatcaQuestionnaireJson';
    $scope.LifeEngageProduct.LastVisitedUrl = "";
    $scope.eAppParentobj.nextPage = "";
    $scope.eAppParentobj.CurrentPage = "";
    $scope.showErrorCount = true;
	$scope.QuestionnaireObj = {};
    $scope.isIndividual = true; //temporary variable used for hiding and showing Questions 19.a or 19 b for individual and corporate clients
   
   var unload = $scope.$parent.$on('FatcaQuestionnaire_unload', function (event) {
       var data = $scope.$parent;
       angular.element(document.querySelector('#eAppFatcaQuestionnaireJson')).empty();
       $scope.$destroy();
       $scope.$parent = data;
        load();
        unload();
   });

    //Controller Initialize method
     $rootScope.hideInsuredLine = true;
    function initialize() {
        $scope.$parent.disableProceedButtonCommom = false;
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
        }
        $scope.showErrorCount = true;
        $scope.IsPreviouspolicySection = false;
        $scope.LifeEngageProduct = angular.copy(EappVariables.getEappModel());
		
		if (!$scope.LifeEngageProduct.Proposer.Questions) {
            $scope.LifeEngageProduct.Proposer.Questions = [];
        }
        if (!$scope.LifeEngageProduct.Proposer.Questions[0]) {
            $scope.LifeEngageProduct.Proposer.Questions[0] = {};
        }
        if (!$scope.LifeEngageProduct.Proposer.Questions[0].questionID) {
            $scope.LifeEngageProduct.Proposer.Questions[0].questionID = "301";
        }
        if (!$scope.LifeEngageProduct.Proposer.Questions[0].option) {
            $scope.LifeEngageProduct.Proposer.Questions[0].option = "";
        }
		
        $scope.eAppParentobj.PreviousPage = "FatcaQuestionnaire_unload";
        $scope.PreviouspolicyHistoryList = [];
        $scope.remainingPreviousPolicyHistory = [];
        $scope.updateQuestionNo = function () {
            return ("7.");
        }
		
        $timeout(function () {
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission") {
                UtilityService.disableAllActionElements();
            }else if (typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) {
                UtilityService.disableAllActionElements();
            }
        }, 50);

       if (rootConfig.isDeviceMobile) {
            $scope.QuestionnaireObj = angular.copy(EappVariables.QuestionModel);
        } else {
            $scope.QuestionnaireObj = angular.copy($scope.LifeEngageProduct.Proposer.Questionnaire);
        }

        if($('#Questionnaire .selector-icon li').length == '1'){
            $rootScope.hideInsuredLine = false;
        } else {
            $rootScope.hideInsuredLine = true;
        }

        $scope.eAppParentobj.CurrentPage = "FatcaQuestionnaire";
        //For disabling and enabling the tabs.And also to know the last visited page.---starts
        //$scope.LifeEngageProduct.LastVisitedUrl = "3,3,'eAppFatcaQuestionnaireJson',false,'FatcaQuestionnaire',''";
        $scope.LifeEngageProduct.LastVisitedUrl = "3,6,'eappshortquestionnaire',true,'eappshortquestionnaire',''";
        $scope.eAppParentobj.nextPage = "4,1,'SPAJsummary',true,'SPAJsummary',''";
        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[1] < 2 && subIndex[0] <= $scope.LifeEngageProduct.LastVisitedUrl[0]) {
                $scope.LifeEngageProduct.LastVisitedIndex = "3,2";
            }
        }
        if (EappVariables.EappKeys.Key15 == "Confirmed" && $scope.$parent.LifeEngageProduct.Declaration.confirmSignature == true) {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = true;
       } else {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
        }
		
		 $timeout(function () {
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission") {
                UtilityService.disableAllActionElements();
            }
        }, 0);
		
        //For disabling and enabling the tabs.And also to know the last visited page.---ends
        var model = "LifeEngageProduct";
        if ((rootConfig.autoSave.eApp) && (typeof EappVariables.EappKeys.Key15 == "undefined" || EappVariables.EappKeys.Key15 == "New" || EappVariables.EappKeys.Key15 == "Confirmed")) {
            if (AutoSave.destroyWatch) {
                AutoSave.destroyWatch();
            }
            AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
        } else if (AutoSave.destroyWatch) {
            AutoSave.destroyWatch();
        }

        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        angular.element('#' + $scope.selectedTabId + "_a").trigger('click');
          $timeout(function () {
            $scope.updateErrorCount('eAppFatcaQuestionnaireJson');
            $scope.refresh();
        }, 50)
    }

    $scope.updateErrorCount = function (id) {
        $rootScope.updateErrorCount(id);
        $scope.selectedTabId = id;
        $scope.eAppParentobj.errorCount = $scope.errorCount;
		
		if($scope.QuestionnaireObj.Other.Questions[0].details == "Yes" && $scope.QuestionnaireObj.Other.Questions[1].details == "No" && $scope.QuestionnaireObj.Other.Questions[2].details == "No" && $scope.QuestionnaireObj.Other.Questions[3].details == "No"){
			$timeout(function () {
			    
				$('.pop-up-container').html('');
				if($scope.QuestionnaireObj.Other.Questions[4].details == "" && $scope.QuestionnaireObj.Other.Questions[5].details == "" && $scope.QuestionnaireObj.Other.Questions[6].details == "" && ($scope.QuestionnaireObj.Other.Questions[7].details == "" || $scope.QuestionnaireObj.Other.Questions[7].details == undefined)){
					$('#errorPopup').html('3');
					$('.pop-up-container').html('<ol><li><a>'+ translateMessages($translate, "FatcaCommonMsg") +' </a></li><li><a>'+  translateMessages($translate, "FatcaRadioErrorMsg") +'</a></li><li><a>'+ translateMessages($translate, "FatcaError5") +' </a></li></ol>');
                    
				}else if(($scope.QuestionnaireObj.Other.Questions[4].details == "" || $scope.QuestionnaireObj.Other.Questions[5].details == "" || $scope.QuestionnaireObj.Other.Questions[6].details == "") && $scope.QuestionnaireObj.Other.Questions[7].details!== "" && $scope.QuestionnaireObj.Other.Questions[7].details!== undefined){
                    $('#errorPopup').html('2');
				    $('.pop-up-container').html('<ol><li><a>'+ translateMessages($translate, "FatcaCommonMsg") +'</a></li><li><a>'+  translateMessages($translate, "FatcaRadioErrorMsg") +'</a></li></ol>');
                }else if(($scope.QuestionnaireObj.Other.Questions[4].details == "" || $scope.QuestionnaireObj.Other.Questions[5].details == "" || $scope.QuestionnaireObj.Other.Questions[6].details == "") && ($scope.QuestionnaireObj.Other.Questions[7].details== "" || $scope.QuestionnaireObj.Other.Questions[7].details== undefined)){
                    $('#errorPopup').html('2');
				    $('.pop-up-container').html('<ol><li><a>'+ translateMessages($translate, "FatcaCommonMsg") +'</a></li><li><a>'+  translateMessages($translate, "FatcaRadioErrorMsg") +'</a></li></ol>');
                }else if($scope.QuestionnaireObj.Other.Questions[7].details == "" || $scope.QuestionnaireObj.Other.Questions[7].details == undefined){
                    $('#errorPopup').html('2');
				    $('.pop-up-container').html('<ol><li><a>'+  translateMessages($translate, "FatcaRadioErrorMsg") +'</a></li><li><a>'+ translateMessages($translate, "FatcaError5") +'</a></li></ol>');
                }else{
					$('#errorPopup').html('1');
					$('.pop-up-container').html('<ol><li><a>'+  translateMessages($translate, "FatcaRadioErrorMsg") +'</a></li></ol>');
				}
				
		 }, 50)
		}else if(($scope.QuestionnaireObj.Other.Questions[0].details== "" || $scope.QuestionnaireObj.Other.Questions[1].details== "" || $scope.QuestionnaireObj.Other.Questions[2].details== "" || $scope.QuestionnaireObj.Other.Questions[3].details=="" || $scope.QuestionnaireObj.Other.Questions[4].details== "" || $scope.QuestionnaireObj.Other.Questions[5].details== "" || $scope.QuestionnaireObj.Other.Questions[6].details== "") && ($scope.QuestionnaireObj.Other.Questions[7].details == "" || $scope.QuestionnaireObj.Other.Questions[7].details == undefined)){
			 $timeout(function () {
			    $('#errorPopup').html('1');
				$('.pop-up-container').html('');
				$('.pop-up-container').html('<ol><li><a>'+ translateMessages($translate, "FatcaCommonMsg") +'</a></li><li><a>'+ translateMessages($translate, "FatcaError5") +' </a></li></ol>');
                $('#errorPopup').html('2');
		 }, 50)
		}else if($scope.QuestionnaireObj.Other.Questions[7].details == "" || $scope.QuestionnaireObj.Other.Questions[7].details == undefined){
            $('#errorPopup').html('1');
			$('.pop-up-container').html('<ol><li><a>'+ translateMessages($translate, "FatcaError5") +'</a></li></ol>');
        }else{
            $timeout(function () {
			    $('#errorPopup').html('1');
				$('.pop-up-container').html('');
				$('.pop-up-container').html('<ol><li><a>'+ translateMessages($translate, "FatcaCommonMsg") +'</a></li></ol>');
            }, 50)
        }
    }

    $scope.checkErrorCountOnTabNav = function ($event, param) {
        var currentTab = $scope.selectedTabId;
        var clickedTab = 'tabsinner' + $scope.selectedSectionId + param;
        var currentTabErrorCount = $scope.errorCount;
        var clickedTabErrorCount = $scope.errorCount;
        if (currentTabErrorCount > 0) {
            if (clickedTabErrorCount == 0) {
                $scope.selectedTabId = clickedTab;
            } else {
                $scope.selectedTabId = currentTab;
                $event.stopPropagation();
                $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                    translateMessages($translate, "enterMandatoryFieldsForeApp"),
                    translateMessages($translate, "fna.ok"));
            }
        } else {
            $scope.selectedTabId = clickedTab;
        }
    }

    //HealthDetailsController Load Event and Load Event Handler
    var load = $scope.$parent.$on('eAppFatcaQuestionnaireJson', function (event, args) {
        initialize();
    });

    //$scope.initialize();

    function questionnaireBeforeSaveFn(successcallback) {
        successcallback();
    }

    $rootScope.validationBeforesave = function (value, successcallback) {
        if (value == "save") {
            $scope.LifeEngageProduct.LastVisitedIndex = "3,2";
        }
        if (rootConfig.isDeviceMobile) {
            EappVariables.setEappModel($scope.LifeEngageProduct);
        }
        $scope.validateBeforeSaveDetails(value, successcallback);
    };

    $scope.validateBeforeSaveDetails = function (value, successcallback) {
        $scope.LifeEngageProduct.Proposer.Questionnaire = angular.copy($scope.QuestionnaireObj);
        if ((rootConfig.isDeviceMobile)) {
            EappService.getEapp(function () {
                EappVariables.setEappModel($scope.LifeEngageProduct);
                questionnaireBeforeSaveFn(function () {
                    successcallback();
                });
            }, function () {});
        } else {
			EappVariables.setEappModel($scope.LifeEngageProduct);
            questionnaireBeforeSaveFn(function () {
                successcallback();
            });
        }
    };
    
	$scope.disableRadio = function(){
			$scope.QuestionnaireObj.Other.Questions[1].details = "No";
			$scope.QuestionnaireObj.Other.Questions[2].details = "No";
			$scope.QuestionnaireObj.Other.Questions[3].details = "No";
		};
	$scope.clearRadioFields = function(){
			$scope.QuestionnaireObj.Other.Questions[1].details = "";
			$scope.QuestionnaireObj.Other.Questions[2].details = "";
			$scope.QuestionnaireObj.Other.Questions[3].details = "";
		};
	$scope.clearDynamicScopeValues = function(){
			$scope.yesCheckRadioBtn_factaQuestion1Part1 = "";
			$scope.yesCheckRadioBtn_factaQuestion1Part2 = "";
			$scope.yesCheckRadioBtn_factaQuestion1Part3 = "";
	};
		
	$scope.yesCheckRadio = function(name){
		 var yesCheckRadioBtn = "yesCheckRadioBtn"+"_"+name;
		if($scope.QuestionnaireObj.Other.Questions[1].details == "No" && $scope.QuestionnaireObj.Other.Questions[2].details == "No" && $scope.QuestionnaireObj.Other.Questions[3].details == "No"){
			$scope[yesCheckRadioBtn]= true;
		}else{
			$scope[yesCheckRadioBtn] = false;
		}
	}
			
}
