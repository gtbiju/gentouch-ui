'use strict';

storeApp.controller('ProductController', ['$rootScope', '$scope', 
                                          'DataService', 'LookupService', 
                                          'DocumentService', 'ProductService', 
                                          '$compile', '$routeParams', '$location', 
                                          '$translate', '$debounce', 'UtilityService', 
                                          'PersistenceMapping', 'AutoSave', 'globalService', 
                                          'EappVariables', 'EappService', '$timeout',
function($rootScope, $scope, DataService, LookupService, DocumentService,
        ProductService, $compile, $routeParams, $location, $translate, 
        $debounce, UtilityService, PersistenceMapping, AutoSave, globalService, 
        EappVariables, EappService, $timeout) {

    $scope.showErrorCount = true;
    $scope.numberPattern = rootConfig.numberPattern;
    $scope.Initialize = function() {
        $scope.LifeEngageProduct = EappVariables.getEappModel();
        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        $scope.updateErrorCount($scope.selectedTabId);
		var model = "LifeEngageProduct";
		if ((rootConfig.autoSave.eApp) && EappVariables.EappKeys.Key15 != 'Final') {
			if(AutoSave.destroyWatch){
				AutoSave.destroyWatch();
			}
			AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
		} else if(AutoSave.destroyWatch){
			AutoSave.destroyWatch();
		}
		
        $scope.addFundInformation = function() {
            var fundInformation = new Object();
            fundInformation.FundOrPortfolioName = "";
            fundInformation.AllocationPercentage = "";
            fundInformation.BonusOrDividend = "";
            fundInformation.PayoutOrSettlement = "";
            $scope.LifeEngageProduct.Product.FundInformation.push(fundInformation);
        };

        $scope.deleteFundInformation = function() {

            var assestRemoved = $scope.LifeEngageProduct.Product.FundInformation[$scope.index].id;
            $scope.LifeEngageProduct.Product.FundInformation.splice($scope.index, 1);
            $scope.updateErrorCount($scope.selectedTabId);
        };
        $scope.deleteValueTable = function(index, deleteHeader) {
            $scope.index = index;
            var message = "Are you sure you want to delete " + deleteHeader;
            // TODO add in resource
            $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), message, translateMessages($translate, "deleteConfirmButtonText"), $scope.deleteFundInformation, translateMessages($translate, "cancelButtonText"), $scope.cancel, $scope.cancel);
        };
        $scope.cancel = function() {
        };
    }
    $scope.updateErrorCount = function(id) {
        $rootScope.updateErrorCount(id);
        $scope.selectedTabId = id;
        // TODO refactor the code ; use uijson and handle bar for validation
        for (var i = 0; i < $scope.LifeEngageProduct.Product.FundInformation.length; i++) {
            var total = 0;
            var fundInformation = $scope.LifeEngageProduct.Product.FundInformation[i];
            if (fundInformation.AllocationPercentage) {
                total = parseInt(total) + parseInt(fundInformation.AllocationPercentage);
                if ((fundInformation.AllocationPercentage).search($scope.numberPattern) == -1 || (total > 100 || total < 0)) {
                $scope.errorCount = $scope.errorCount + 1;
                }
            }
            if (!fundInformation.FundOrPortfolioName) {
                $scope.errorCount = $scope.errorCount + 1;
            }
        }

    }
    $scope.showErrorPopup = function(id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
        // TODO refactor the code ; use uijson and handle bar for validation
        for (var i = 0; i < $scope.LifeEngageProduct.Product.FundInformation.length; i++) {
            var total = 0;
            var fundInformation = $scope.LifeEngageProduct.Product.FundInformation[i];
            if (fundInformation.AllocationPercentage) {
                total = parseInt(total) + parseInt(fundInformation.AllocationPercentage);
                if ((fundInformation.AllocationPercentage).search($scope.numberPattern) == -1 || (total > 100 || total < 0)) {
                    var error = {};
                    error.message = translateMessages($translate, "funcdAllocationRequiredValidationMessage");
                    error.key = "fundInfoAllocation" + i;
                    $scope.errorMessages.push(error);
                }
            }
            if (!fundInformation.FundOrPortfolioName) {
                var error = {};
                error.message = translateMessages($translate, "fundNameRequiredValidationMessage");
                error.key = "sampleFund" + i;
                $scope.errorMessages.push(error);

            }
        }
    }
    // ProductController Load Event and Load Event Handler
    /* $scope.$parent.$on('ProductDetails', function(event, args) {
        $scope.selectedTabId = args[0];
        $scope.Initialize();
    }); */
}]);
