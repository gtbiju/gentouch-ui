'use strict';

storeApp.controller('SummaryController', ['$rootScope', '$scope', 'DataService', '$routeParams', '$translate', '$debounce', 'UtilityService', 'PersistenceMapping', 'AutoSave', 'EappVariables', 'EappService', 
function($rootScope, $scope, DataService, $routeParams, $translate, $debounce, UtilityService, PersistenceMapping, AutoSave, EappVariables, EappService ) {

    $scope.Initialize = function() {
	   	 if (EappVariables.EappKeys.Key15==='Final') {
	         $scope.isSTPSuccess = true;
	         $scope.STPMessage = translateMessages($translate, "stpRulePassed");
	     }else{ 
	 	$scope.isSTPRuleExecuted=false;
	     $scope.LifeEngageProduct = EappVariables.getEappModel();
	     UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
	     $scope.showErrorCount = true;
	    
	     $scope.proposalId = EappVariables.EappKeys.Key4;
	     var signature ="Signature";
	     var outputFormat="image/png";
	   if (EappVariables.EappKeys.TransTrackingID) {
		    if((rootConfig.isDeviceMobile)){
	     EappService.getSignature(EappVariables.EappKeys.TransTrackingID,signature,function(sigdata){
	     EappVariables.Signature =sigdata;
	     if(sigdata[0]){
	      EappService.convertImgToBase64(sigdata[0].base64string, function(base64Img){
				 var dpr = window.devicePixelRatio;
				 window.devicePixelRatio = 1;
				 signaturePad.fromDataURL(base64Img);
				 window.devicePixelRatio = dpr;
	 	},outputFormat);
	 }
	     });
			}else{
	
			  var requirementObject = CreateRequirementFile();
				 requirementObject.RequirementFile.identifier = EappVariables.EappKeys.TransTrackingID;
				 for(var i=0; i< EappVariables.EappModel.Requirements.length;i++){
				   if(EappVariables.EappModel.Requirements[i].requirementType === "Signature"){
				     requirementObject.RequirementFile.requirementName = EappVariables.EappModel.Requirements[i].requirementName;
					 for(var j=0; j< EappVariables.EappModel.Requirements[i].Documents.length;j++){
				 requirementObject.RequirementFile.documentName = EappVariables.EappModel.Requirements[i].Documents[j].documentName;
				 }
		        PersistenceMapping.clearTransactionKeys();
			    EappVariables.EappKeys.Key15 = EappVariables.getStatusOptionsEapp()[0].status;
			    EappService.mapKeysforPersistence();		
			    var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
				DataService.getDocumentsForRequirement(transactionObj, function(sigdata) {
	         var documentArray = [];
				 EappVariables.Signature =sigdata[0].base64string;         
				 signaturePad.fromDataURL(sigdata[0].base64string);
	     });
	
				  }
				 }
	
			}
	     }
	    
			var model = "LifeEngageProduct";
			if ((rootConfig.autoSave.eApp) && EappVariables.EappKeys.Key15 !== 'Final') {
				if(AutoSave.destroyWatch){
					AutoSave.destroyWatch();
				}
				AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
			} else if(AutoSave.destroyWatch){
				AutoSave.destroyWatch();
			}
		}

 };
    	
    
    $scope.updateErrorCount = function(id) {
        $rootScope.updateErrorCount(id);
    };
    $scope.showErrorPopup = function(id) {
        $rootScope.showErrorPopup($scope.selectedTabId);

    };
    $scope.confirmDeclaration = function() {
        var isDeclared = $scope.LifeEngageProduct.Declaration.declarationAgreed;
        if (isDeclared === "true"
                                && !signaturePad.isEmpty()) {
            $scope.confirmProposal();
        } else {
            $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.declarationAgreedMessage"), translateMessages($translate, "fna.ok"));
        }
    };
    $scope.isAgeLessThan18 = function(age) {
                                var today = new Date();
                                var birthDate = new Date(age);
                                if (birthDate === 'Invalid Date') {
                                    age = age.split("-").join("/");
                                    birthDate = new Date(age);
                                }
                                var age = today.getFullYear()
                                        - birthDate.getFullYear();
                                var m = today.getMonth() - birthDate.getMonth();
                                if (m < 0
                                        || (m === 0 && today.getDate() < birthDate
                                                .getDate())) {
                                    age--;
                                }
                                return age;
                            };
    $scope.confirmProposal = function() {
    	$rootScope.showHideLoadingImage(true, "Confirming, Please wait", $translate);
        if (  $scope.LifeEngageProduct.Insured.IncomeDetails && !($scope.LifeEngageProduct.Insured.IncomeDetails.annualIncome)) {
            $scope.LifeEngageProduct.Insured.IncomeDetails.annualIncome = "";
        }
        // added for prev button in payment
        // authorisation
        $scope.confirmForPaymentAuth = true;

        $scope.LifeEngageProduct.Product.ProductDetails.minInsuredAge = 18;
        $scope.LifeEngageProduct.Product.ProductDetails.maxInsuredAge = 60;
        $scope.LifeEngageProduct.Product.ProductDetails.minSumAssured = 10000;
        $scope.LifeEngageProduct.Product.ProductDetails.maxSumAssured = 9999999999;
        $scope.LifeEngageProduct.Declaration.isFinalSubmit = true;



        /* Get signature element and pass as parameter to saveSignature */
        var sigElement = $('.sigPad').signaturePad();
        var sigData=signaturePad.toDataURL();
        var requirementType = "Signature";
        var index="";
        var requirementName = "Signature";
		var partyIdentifier="";
		var isMandatory="";
		var docArray=[];
		var documentType="Signature";
        /* Get signature element ends */

        /* Get signature element ends */
        if ((rootConfig.isDeviceMobile)) {
            EappService.getRequirementFilePath(sigData,index,requirementName,requirementType,documentType,function(filePath){
				if((rootConfig.isDeviceMobile) && rootConfig.isOfflineDesktop){
             		filePath = "../"+filePath;
				}
                EappService.saveRequirement(filePath,requirementType,partyIdentifier,isMandatory,docArray,documentType,function(){       
                    EappService.runSTPRule($scope.onConfirmSuccess, $scope.onConfirmError);         
                });     
            });
        } else {
            EappService.saveRequirement(sigData,requirementType,partyIdentifier,isMandatory,docArray,documentType,function(){         
                EappService.runSTPRule($scope.onConfirmSuccess, $scope.onConfirmError);         
                });
        }


    };
    $scope.saveSignature = function(successCallback, errorCallback) {
        // TODO
        successCallback();

    };
    $scope.onConfirmSuccess = function(data) {

       $scope.isSTPRuleExecuted = true;
        if (data.Key9 === "Declined") {
		$scope.$parent.Key15 = 'Review';
            $scope.LifeEngageProduct.Declaration.isFinalSubmit = false;
            $scope.$parent.isPaymentDisabled=true;
            $scope.STPMessage = translateMessages($translate, "stpRuleFailed");
            $scope.STPResults = data.ValidationResults;
            EappVariables.EappKeys.Key15 = EappVariables
                     .getStatusOptionsEapp()[2].status;

            $rootScope.showHideLoadingImage(false, "");
            EappService.saveTransaction(function(){
    			},function(){
    			},false);
        } else {
		$scope.$parent.Key15 = 'Final';

            EappVariables.EappKeys.Key15 = EappVariables
                                        .getStatusOptionsEapp()[1].status;
            if ((rootConfig.isDeviceMobile)) {
                EappService.saveTransaction(function() {
                    $scope.onSTPSuccess();
                }, $scope.onSaveError, false);
            } else {
                $scope.onSTPSuccess();
            }
        }
        $rootScope.showHideLoadingImage(false, "Confirming, Please wait", $translate);
    };
        $scope.clearSignature = function() {
        signaturePad.clear();
        };
    $scope.onSTPSuccess = function() {

        $scope.disableNextButton = false;
        $scope.STPMessage = translateMessages($translate, "stpRulePassed");
        $scope.$parent.isFinalSubmit = true;
        $scope.$parent.eAppTabNavStyle[$scope.eAppTabNavStyle.length - 2] = '';
        $rootScope.showHideLoadingImage(false, "");
    };
    $scope.onConfirmError = function(data) {
        // NotifyMessages utility for showing
        // success/error/validation message s
        // Implemented in js/common/utility.js
        if (!$scope.isAutoSave) {
            $rootScope.NotifyMessages(true, data);
        }

        $scope.$parent.eAppTabNavStyle[$scope.eAppTabNavStyle.length - 1] = 'disabled';
        $rootScope.showHideLoadingImage(false);
    };

    $scope.printClose = function() {
        $('.print_summary_container').css("display", "none");
        $("body").removeClass("hide_scroll");
        $('#page_loader').css("display", "none");
    };

    $scope.printDetails = function() {
        $('#page_loader').empty();
        var dynamic_height = $('body').height();
        $('#page_loader').css('height', dynamic_height).fadeIn(300);
        var flag = $('#page_loader').css("display");
        var itemtoReplaceContentOf = $('#Summary');

        $('.main_content_print').html(itemtoReplaceContentOf[0].innerHTML);
        if (flag === "block") {
            $('.print_summary_container').css("display", "block");
            $('.main_content_print').find('.hide_in_print_preview').css("display", "none");
     $('.main_content_print').find('.summary_inner_titles').removeClass('summary_inner_titles').addClass('print_pdf_inner_titles');
            $('.main_content_print').find('.summary_edit_btn').css("display", "none");
            $('.main_content_print').find('.summary_title_container').css("display", "none");
            $('.main_content_print').find('#btnConfirm').css("display", "none");
            $('.main_content_print').find('.esign_header').css("display", "none");
            $('.main_content_print').find('.e_sign_tab_btn').css("display", "none");

            var top = 40;
            var left = ($(window).width() - $('.print_summary_container').width()) / 2;
            var summary_height = $(window).height() - 80;
            $('.print_summary_container').height(summary_height);

            $('.print_summary_container').css({
                'top' : top + $(document).scrollTop(),
                'left' : left
            });
            $("body").removeClass("hide_scroll").addClass("hide_scroll");

        } else {

            $("body").removeClass("hide_scroll");
        }
    };

    $scope.callPrinter = function() {

        var writeDoc;
        var printWindow;
        var f = new Iframe();
        writeDoc = f.doc;
        printWindow = f.contentWindow || f;

        writeDoc.open();
        writeDoc.write("<html><body>");
        writeDoc.write("<link href='css/general-styles/all-components.css' rel='stylesheet' type='text/css'  media='print' />");
        writeDoc.write("<link rel='stylesheet' type='text/css' href='css/general-styles/print.css' media='print' />");
        writeDoc.write($('.print_summary_container').html() + "</body></html>");

        writeDoc.close();

        printWindow.focus();
        printWindow.print();

    };
    // SummaryController Load Event and Load Event Handler
   /* $scope.$parent.$on('Summary', function(event, args) {
        $scope.selectedTabId = args[0];
        $scope.Initialize();
    });*/
}]);
