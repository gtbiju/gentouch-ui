'use strict';

storeApp.controller('PayerController', PayerController);
PayerController.$inject = ['$rootScope', '$scope',
                                        'DataService', 'LookupService',
                                        'DocumentService', 'ProductService',
                                        '$compile', '$routeParams', '$location',
                                        '$translate', '$debounce', 'UtilityService',
                                        'PersistenceMapping', 'AutoSave', 'globalService',
                                        'EappVariables', 'EappService', '$timeout'];

function PayerController($rootScope, $scope, DataService, LookupService,
    DocumentService, ProductService, $compile, $routeParams,
    $location, $translate, $debounce, UtilityService,
    PersistenceMapping, AutoSave, globalService, EappVariables,
    EappService, $timeout) {
    $scope.mobnumberPattern = rootConfig.mobnumberPattern;
    $scope.namePattern = rootConfig.namePattern;
    $scope.zipPattern = rootConfig.zipPattern;
    $scope.numberPattern = rootConfig.numberPattern;
    $scope.emailPattern = rootConfig.emailPattern;
    $scope.showErrorCount = true;
    var isPayorDifferentFromInsuredWatch;
    var dobPayerWatch;

    function initialize() {
        $scope.LifeEngageProduct = EappVariables.getEappModel();
        $scope.validatePayerDOB = function (value, $rootScope) {
            $scope.payerDOBValidation = 0;
            if (null != value && value != "") {
                if ((new Date(value).getTime()) > (new Date().getTime())) {
                    $scope.payerDOBValidation = -1;
                    // $scope.updateErrorCount("tabsinner4");
                } else {
                    $scope.payerDOBValidation = 0;
                }
            }
        };

        isPayorDifferentFromInsuredWatch = $scope.$watch('LifeEngageProduct.Payer.CustomerRelationship.isPayorDifferentFromInsured',
            function (value) {
                if (value == 'Yes') {
                    if ($scope.LifeEngageProduct.Payer.BasicDetails == $scope.LifeEngageProduct.Insured.BasicDetails) {
                        $scope.LifeEngageProduct.Payer.BasicDetails = {};
                    }
                    $scope.checked = false;
                } else {
                    $scope.LifeEngageProduct.Payer.BasicDetails = $scope.LifeEngageProduct.Insured.BasicDetails;
                    $scope.LifeEngageProduct.Payer.CustomerRelationship.relationWithInsured = "";
                    $scope.checked = true;
                }

            }, true);

        dobPayerWatch = $scope.$watch('LifeEngageProduct.Payer.BasicDetails.dob', function (value) {
            $scope.validatePayerDOB(value, $rootScope);
        }, true);

        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        $scope.updateErrorCount($scope.selectedTabId);
        var model = "LifeEngageProduct";
        if ((rootConfig.autoSave.eApp) && EappVariables.EappKeys.Key15 != 'Final') {
            if (AutoSave.destroyWatch) {
                AutoSave.destroyWatch();
            }
            AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
        } else if (AutoSave.destroyWatch) {
            AutoSave.destroyWatch();
        }
    }
    $scope.updateErrorCount = function (id) {
        if ($scope.LifeEngageProduct.Payer.CustomerRelationship.isPayorDifferentFromInsured == "No") {
            $scope.errorCount = 0;
            $scope.LifeEngageProduct = EappVariables.getEappModel();
        } else {
            $rootScope.updateErrorCount(id);
            var payerDob = $scope.LifeEngageProduct.Payer.BasicDetails.dob;
            if (LEDate(payerDob) >= LEDate()) {
                $scope.errorCount = parseInt($scope.errorCount) + 1;
            }
            $scope.selectedTabId = id;
        }
    }
    $scope.showErrorPopup = function (id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
        var payerDob = $scope.LifeEngageProduct.Payer.BasicDetails.dob;
        if (LEDate(payerDob) >= LEDate()) {
            var error = {};
            error.message = translateMessages($translate, "payerDOBFutureDateValidationMessage");
            error.key = 'payerDOB';
            $scope.errorMessages.push(error);
        }

    }
    // PayerController Load Event and Load Event Handler
    $scope.$parent.$on('PayerDetailsSubTab', function (event, args) {
        $scope.selectedTabId = args[0];
        initialize();
    });
    $scope.$on("$destroy", function () {
        isPayorDifferentFromInsuredWatch();
        dobPayerWatch();
    });

} //Controller ends here