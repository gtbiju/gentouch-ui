'use strict';
storeApp.controller('GLI_DocumentUploadController', GLI_DocumentUploadController);
GLI_DocumentUploadController.$inject = ['$rootScope', '$scope', '$filter', 'DataService', '$routeParams', '$translate', '$debounce', 'UtilityService', 'AutoSave', 'EappVariables', 'EappService', '$timeout','UserDetailsService'];

function GLI_DocumentUploadController($rootScope, $scope, $filter, DataService, $routeParams, $translate, $debounce, UtilityService, AutoSave, EappVariables, EappService, $timeout,UserDetailsService) {

    $rootScope.docview = true;
    $rootScope.tabview = false;
	$rootScope.mainheaderview=true;
    $rootScope.selectedPage = "DocumentUpload";
    $scope.onSuccessOfSetTileProperties = function () {
		if ((EappVariables.EappKeys.Key15 && EappVariables.EappKeys.Key15 == "Submitted" && EappVariables.EappKeys.Key7 == "Submitted") ||(EappVariables.EappKeys.Key7 && EappVariables.EappKeys.Key7 != "" && EappVariables.EappKeys.Key7 != "Memo" && EappVariables.EappKeys.Key7 != "PendingUploaded")) {
			if(EappVariables.EappKeys.Key7 == "Pending Submission" && EappVariables.EappKeys.Key15=="Pending Submission"){
				UtilityService.removeDisableElements('DocumentUpload');
				$scope.$parent.setButtonDisabled = "false";
				if($scope.OnlyGAO){
					$('button#submitBtn-DocUpload').attr("disabled", "disabled");
				}
				if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
					$('button#submitBtn-DocUpload').removeAttr("disabled");
				}
			 }else{
				//UtilityService.disableAllActionElements();
                $('button#submitBtn-DocUpload').attr("disabled", "disabled");
				$('.backToDocUploadBtn').removeAttr("disabled");
                $('.Doc_tile_browseandUpload').addClass('disable-icons');
                $('.Doc_remove_attachment_icon').addClass('disable-icons');
                $scope.disableIcons = true;
				$scope.$parent.setButtonDisabled = "true";
			}
        } else {
            UtilityService.removeDisableElements('DocumentUpload');
            $scope.$parent.setButtonDisabled = "false";
			//$('button#submitBtn-DocUpload').removeAttr("disabled");
             if ($scope.$parent.totalDocCount == 0) {
                $('button#submitBtn-DocUpload').removeAttr("disabled");
            } else {
                $('button#submitBtn-DocUpload').attr("disabled", "disabled");
            } 
			if($scope.OnlyGAO){
				$('button#submitBtn-DocUpload').attr("disabled", "disabled");
			}
			if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
				$('button#submitBtn-DocUpload').removeAttr("disabled");
			}
        }
    }

    //Brocast event to disable tiles as per Key15 and Key7 after ng-repeat
    var onNgRepeatFinished = $rootScope.$on('ngRepeatFinished', function (event, args) {
        if ((EappVariables.EappKeys.Key15 && EappVariables.EappKeys.Key15 == "Submitted" && EappVariables.EappKeys.Key7 == "Submitted") ||
            (EappVariables.EappKeys.Key7 && EappVariables.EappKeys.Key7 != "" && EappVariables.EappKeys.Key7 != "Memo" && EappVariables.EappKeys.Key7 != "PendingUploaded" && EappVariables.EappKeys.Key7 != "Pending Submission")) {
            $('.items .tile').addClass('tile-disabled');
        } else {
            $('.items .tile').removeClass('tile-disabled');
        }
    });

    $scope.$on('$destroy', function () {
        onNgRepeatFinished();
    });
	//accordion start
	$scope.basicDocuments=false;
	$scope.memoDocuments=false;
	$scope.displaydocuments = function () {
		$scope.basicDocuments = !$scope.basicDocuments;
	}

	$scope.displayMemodocuments = function () {
		$scope.memoDocuments = !$scope.memoDocuments;
	}
	//accordion end
    $scope.Initialize = function () {
		if ((EappVariables.EappKeys.Key15=="" || EappVariables.EappKeys.Key15=="NULL" || EappVariables.EappKeys.Key15=="undefined" || EappVariables.EappKeys.Key15=="Pending Submission"))
		{
		$rootScope.backuploadbtn=false;
		$rootScope.returnbackuploadbtn=true;	
			
		}
		else
		{
			$rootScope.backuploadbtn=true;
			$rootScope.returnbackuploadbtn=false;
		}
	
       // debugger;
        $scope.displaydocuments();
        $scope.$parent.disableProceedButtonCommom = false;
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
        }
		//Memo section
		$scope.memoOpen=rootConfig.MemoOpen;
		$scope.memoResolved=rootConfig.MemoResolved;
        // if(rootConfig.Key7ValuesForMemoTile.indexOf(EappVariables.EappKeys.Key7)>-1){
        if(EappVariables.EappKeys.Key7!="Pending Submission" && EappVariables.EappKeys.Key7!="Submitted")
        {		
			$scope.showMemoDocs=true;
		}
        else{
			$scope.showMemoDocs=false;
		}
		$scope.showGeneralDocs=true;
        $scope.LifeEngageProduct = EappVariables.getEappModel();
        $scope.eAppParentobj.PreviousPage = "";
        $scope.eappKeys = EappVariables.EappKeys;
        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        $scope.showErrorCount = true;
        $scope.DocUploadPopUp = false;
        $scope.isModelChanged = false;
        $scope.disableBrowseAndDel = false;
        $scope.docSnapShot = [];
        $scope.selectedRequirement = [];
        EappVariables.Requirement = [];
        $scope.status = "";
        $scope.defaultImage = "";
        $scope.pdfImage = "css/generali-theme/img/dummy_img_pdf.png";
        $scope.deletebutton = true;
        $scope.hideBeneficiaryTiles = false;
        $rootScope.showHideLoadingImage(true, 'reqRuleProgress', $translate);
        
        
	    $scope.Requirementlist = EappVariables.EappModel.Requirements;


      	EappService.checkRequirementGAOSignature($scope.Requirementlist,'GAO', function (returnReq) {
            if (returnReq == 0) {
				$scope.OnlyGAO=true;
			}else{
				$scope.OnlyGAO=false;
			}
		});

        $timeout(function(){

            if(EappVariables.EappKeys.Key15 == 'Submitted'){
                $scope.disableInput = true;
            } else {
                $scope.disableInput = false;
            }

        },50)
        $scope.documentObject = {};
        $scope.$parent.showBackBtn = false;
        $scope.checkPopup = false;
        $scope.savedFiles = [];
        $scope.docDisabled = [];
        $scope.deletePopup = false;
        $scope.indexTile = 0;
        $scope.$parent.totalDocCount = 0;
        $scope.$parent.docCountFlagForSubmit = rootConfig.GVLNonSubmitFlag;
        //$scope.getlookUpDocuments();
        $scope.getDocumentTypeConfigurationFiles();

        /*Handling Accordion heading for PO = MI / PO = AI*/
        $scope.checkPOEqualsMIorAI = function (sectionVal) {
            if ($scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer == 'Self') {
                return translateMessages($translate, "general.policyHolderEqualsMainInsured");
            }
            var translatedsectionVal = translateMessages($translate, "general.PolicyHolder");
            return translatedsectionVal;
        }
        /**For Calculating the Mandaory Documents**/
        $scope.mandatoyDocCounnt = 0;
        for (var i = 0; i < $scope.Requirementlist.length; i++) {
            if ($scope.Requirementlist[i].requirementType != "Signature" && $scope.Requirementlist[i].isMandatory == "true") {
                $scope.mandatoyDocCounnt++;
            }
        }
        $scope.setTileProperties($scope.indexTile, $scope.onSuccessOfSetTileProperties);
        $scope.isDevice = false;
        if (!rootConfig.isDeviceMobile) {
            $scope.isWeb = true;
        } else {
            $scope.isWeb = false;
        }
		$rootScope.lastURL=$scope.LifeEngageProduct.LastVisitedUrl;
		$rootScope.lastURLindex=$scope.LifeEngageProduct.LastVisitedIndex;
		var nextPage = $scope.LifeEngageProduct.LastVisitedUrl.split(",");
		var index1 = parseInt(nextPage[0]);
		var subindex1 = parseInt(nextPage[1]);
		
        //For disabling and enabling the tabs.And also to know the last visited page.---starts
        $scope.LifeEngageProduct.LastVisitedUrl = "6,0,'DocumentUpload',false,'tabsinner18',''";
        $scope.eAppParentobj.CurrentPage = "";
        /* if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            $scope.LifeEngageProduct.LastVisitedIndex = index+','+subindex;
            $scope.$parent.LifeEngageProduct.LastVisitedIndex = index+','+subindex;
        } */
		if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[1] <= index1 && subIndex[0] <= subindex1) {
                 $scope.LifeEngageProduct.LastVisitedIndex = index+','+subindex;
            }
        }
        //For disabling and enabling the tabs.And also to know the last visited page.---ends

        if ((rootConfig.isDeviceMobile)) {
            var leFileUtils = new LEFileUtils();
            leFileUtils.getApplicationPath(function (documentsPath) {
                if ((rootConfig.isOfflineDesktop)) {
                    $scope.tileImagePath = "file://" + documentsPath;
                } else {
                    $scope.tileImagePath = "file://" + documentsPath + "/";
                }
            });
        } else {
            $scope.tileImagePath = rootConfig.mediaURL;
        }

        
        if (!(rootConfig.isOfflineDesktop) && rootConfig.isDeviceMobile) {
            $scope.isDevice = true;
        } else {
            $scope.isDevice = false;
        }
        var model = "LifeEngageProduct";
        if ((rootConfig.autoSave.eApp) && (typeof EappVariables.EappKeys.Key15 == "undefined" || EappVariables.EappKeys.Key15 == "New" || EappVariables.EappKeys.Key15 == "Confirmed")) {
            if (AutoSave.destroyWatch) {
                AutoSave.destroyWatch();
            }
            AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
        }
        $rootScope.showHideLoadingImage(false, "reqRuleProgress", $translate);
        // formatting Date on Tab navigation 
        $rootScope.stringFormattedDate = function () {}
    };
	
	//MemoShowdoc function
	 $scope.showMemoDocuments = function (requirement, index, sectionIndex) {
			$scope.restrictPopupText = false;
			$rootScope.GAOTileClick=true;
			$scope.showGeneralDocs=false;
			$scope.showMemoDocs=false;
			$scope.othersIdentify = false;
			$scope.memoTile = true;
            $scope.selectedRequirementIndex = $scope.LifeEngageProduct.Requirements.indexOf(requirement);
            $scope.selectedRequirement = requirement;
			$scope.documentTypesMemo=[];
			for(var i=0;i<$scope.selectedRequirement.Documents.length;i++){
				var obj={"code":""};
				obj.code=$scope.selectedRequirement.requirementSubType+(i+1);
				$scope.documentTypesMemo.push(obj);
			}
            $scope.mypopup = true;
            $rootScope.docview = false;
            $rootScope.tabview = true;
            $scope.$parent.showBackBtn = true;
            $scope.accordionIndex = sectionIndex;
            if ((rootConfig.isDeviceMobile)) {
                $scope.getStatusForDocuments();
            } 
            $scope.refresh();
    };
	
	//GAOShowdoc function
	 $scope.showGAODocuments = function (requirement, index, sectionIndex) {
            $scope.restrictPopupText = false;
			$rootScope.GAOTileClick=true;
			$scope.showGeneralDocs=false;
			$scope.showMemoDocs=false;
			$scope.othersIdentify = true;
			$scope.memoTile = false;
            $scope.selectedRequirementIndex = $scope.LifeEngageProduct.Requirements.indexOf(requirement);
            $scope.selectedRequirement = requirement;
			$scope.documentTypesMemo=[];
			var obj={"code":""};
			obj.code=$scope.selectedRequirement.requirementSubType;
			$scope.documentTypesMemo.push(obj);
            $scope.mypopup = true;
            $rootScope.docview = false;
            $rootScope.tabview = true;
            $scope.$parent.showBackBtn = true;
            $scope.accordionIndex = sectionIndex;
            if ((rootConfig.isDeviceMobile)) {
                $scope.getStatusForDocuments();
            } 
            $scope.refresh();
    };

    $scope.getStatusForDocuments = function () {
        EappService.getStatusForDocuments($scope.selectedRequirement, EappVariables.EappKeys.TransTrackingID,
            function (docData) {
                for (var i = 0; i < $scope.selectedRequirement.Documents.length; i++) {
                    for (var j = 0; j < docData.length; j++) {
                        if ($scope.selectedRequirement.Documents[i].documentName == docData[j].documentName) {
                            if (docData[j].STATUS === "Saved") {
                                $scope.selectedRequirement.Documents[i].Status = "sync_status_unsync";
                                break;
                            } else if (docData[j].STATUS === "Synced") {
                                $scope.selectedRequirement.Documents[i].Status = "sync_status_tick";
                            }
                            $scope.refresh();
                        }
                    }
                }
            },
            function (error) {});
    };


    $scope.getDocumentTypeConfigurationFiles = function () {
        LEDynamicUI.getUIJson(rootConfig.documentUIJson, false, function (DocumentTypeConfigurationFiles) {
            EappVariables.DocumentMastList = DocumentTypeConfigurationFiles.DocumentMastList;
            $scope.refresh();
        });
    };

    $scope.getlookUpDocuments = function () {
        EappService.lookUpDocuments(function () {
            $scope.codeLookUpType = EappVariables.DocumentType;
        },true);
    };

    $scope.setTileProperties = function (indexTile, successCallBack) {
        if (indexTile < $scope.Requirementlist.length) {
            EappService.getuploadStatus($scope.Requirementlist[indexTile], EappVariables.EappKeys.TransTrackingID, function (uploadStatus) {
                $scope.Requirementlist[indexTile].uploadStatus = uploadStatus;
                if (!(rootConfig.isOfflineDesktop) && !(rootConfig.isDeviceMobile)) {
                    $scope.syncStatuscol = false;
                    indexTile++;
                    $scope.setTileProperties(indexTile, $scope.onSuccessOfSetTileProperties);
                } else {
                    if ($scope.Requirementlist[indexTile].Documents.length > 0) {
                        EappService.getSyncStatus($scope.Requirementlist[indexTile], EappVariables.EappKeys.TransTrackingID, function (syncStatus) {
                            $scope.Requirementlist[indexTile].noData = syncStatus.noData;
                            $scope.Requirementlist[indexTile].unsyncedStatus = syncStatus.unsyncedStatus;
                            $scope.Requirementlist[indexTile].SyncedStatus = syncStatus.syncedStatus;
                            $scope.syncStatuscol = true;
                            indexTile++;
                            $scope.setTileProperties(indexTile, $scope.onSuccessOfSetTileProperties);
                        });
                    } else {
                        indexTile++;
                        $scope.setTileProperties(indexTile, $scope.onSuccessOfSetTileProperties);
                    }
                }
            });

        } else {
            var docCount = 0;
            for (var i = 0; i < $scope.Requirementlist.length; i++) {
                if ($scope.Requirementlist[i].uploadStatus == true && $scope.Requirementlist[i].requirementType !== "Signature" && $scope.Requirementlist[i].isMandatory == "true") {
                    docCount++;
                }
            }

            if ((rootConfig.isDeviceMobile)) {
                if (docCount > 0) {
                    $scope.$parent.totalDocCount = $scope.mandatoyDocCounnt - docCount;
                } else {
                    $scope.$parent.totalDocCount = $scope.mandatoyDocCounnt;
                }
                if ($scope.$parent.totalDocCount === 0) {
                    $scope.$parent.docCountFlagForSubmit = rootConfig.GVLSubmitFlag;
                }
            } else {
                if (docCount > 0) {
                    $scope.$parent.totalDocCount = $scope.mandatoyDocCounnt - docCount;
                } else {
                    $scope.$parent.totalDocCount = $scope.mandatoyDocCounnt;
                }
                if ($scope.$parent.totalDocCount === 0) {
                    $scope.$parent.docCountFlagForSubmit = rootConfig.GVLSubmitFlag;
                }
            }
            if (EappVariables.EappKeys.Key7 == "PendingUploaded" || EappVariables.EappKeys.Key7 == "Pending" || EappVariables.EappKeys.Key7 == "Memo") {
                $scope.$parent.totalDocCount = 0;
            }
            $scope.LifeEngageProduct.totalDocCount = $scope.$parent.totalDocCount;
            if ($scope.$parent.totalDocCount == 0 && $scope.$parent.userStatus != "false") {
                $scope.$parent.setButtonDisabled = "false";
            } else {
                $scope.$parent.setButtonDisabled = "true";
            }

            if ($scope.$parent.totalDocCount == 0) {
                $('button#submitBtn-DocUpload').removeAttr("disabled");
            } else {
                $('button#submitBtn-DocUpload').attr("disabled", "disabled");
            } 

            $scope.refresh();
            $rootScope.showHideLoadingImage(false, "reqRuleProgress", $translate);
        }

        successCallBack();
    };

    $scope.commentSubmit = function () {
        $scope.commentDisabled = false;
        $rootScope.showHideLoadingImage(true, "Submitting Comments", $translate);
        if (EappVariables.Requirement.length > 0) {
            for (var i = 0; i < EappVariables.Requirement.length; i++) {

                if (EappVariables.Requirement[i].base64string === $scope.docImage || EappVariables.Requirement[i].fileName == $scope.pdfFile) {
                    EappVariables.Requirement[i].description = $scope.comments;
                    EappService.updateCommentsForRequirementFiles(EappVariables.Requirement[i], function () {
                        $rootScope.showHideLoadingImage(false, "Submitting Comments", $translate);
                    });

                }
            }
        } else {
            $rootScope.showHideLoadingImage(false, "Submitting Comments", $translate);
        }

    };

    //Function to check whether ID issue date > 15 Years
    $scope.getDifferenceInIDIssueDate = function (idDate, successcallback) {
        var identityDate = new Date(idDate);
        var today = new Date();
        var showPopUp;
        today.setYear(today.getFullYear() - 15);
        if (identityDate.getFullYear() < today.getFullYear()) {
            showPopUp = true;
        } else if (identityDate.getFullYear() == today.getFullYear()) {
            if (identityDate.getMonth() < today.getMonth()) {
                showPopUp = true;
            } else if (identityDate.getMonth() == today.getMonth()) {
                if (identityDate.getDate() < today.getDate()) {
                    showPopUp = true;
                } else {
                    showPopUp = false;
                }
            } else {
                showPopUp = false;
            }
        } else {
            showPopUp = false;
        }
        successcallback(showPopUp);
    };


    $scope.showDocuments = function (requirement, index, sectionIndex) {
        if(requirement.requirementType == ""){
            $scope.restrictPopupText = true;
        } else {
            $scope.restrictPopupText = false;
        }
		$rootScope.GAOTileClick=false;
		$scope.showGeneralDocs=false;
		$scope.showMemoDocs=false;
		$scope.memoTile = false;
        if (requirement.Documents && requirement.Documents.length > 0 && requirement.Documents[requirement.Documents.length - 1].documentOptions.length === 0) {
            $scope.docCountLimit = true;
        } else {
            $scope.docCountLimit = false;
        }

        if (requirement.requirementType === "IdentityProof" && sectionIndex === 1 && requirement.partyIdentifier == $scope.LifeEngageProduct.Proposer.BasicDetails.UUID) {
            $scope.getDifferenceInIDIssueDate($scope.LifeEngageProduct.Proposer.BasicDetails.identityDate, function (showPop) {
                if (showPop === true) {
                    $rootScope.lePopupCtrl.showError(translateMessages($translate,
                        "lifeEngage"), translateMessages($translate,
                        "identityExpiry"), translateMessages($translate,
                        "fna.ok"), $scope.okClick);
                }
            });
        } else if (requirement.requirementType == "IdentityProof" && sectionIndex === 2 && requirement.partyIdentifier == $scope.LifeEngageProduct.Insured.BasicDetails.UUID) {
            $scope.getDifferenceInIDIssueDate($scope.LifeEngageProduct.Insured.BasicDetails.identityDate, function (showPop) {
                if (showPop === true) {
                    $rootScope.lePopupCtrl.showError(translateMessages($translate,
                        "lifeEngage"), translateMessages($translate,
                        "identityExpiry"), translateMessages($translate,
                        "fna.ok"), $scope.okClick);
                }
            });
        }
        if ((!EappVariables.EappKeys.Key15 || EappVariables.EappKeys.Key15 != "Submitted" || EappVariables.EappKeys.Key7 != "Submitted") &&
            (!EappVariables.EappKeys.Key7 || EappVariables.EappKeys.Key7 == "" || EappVariables.EappKeys.Key7 == "Pending Submission" || EappVariables.EappKeys.Key7 == "Pending" || EappVariables.EappKeys.Key7 == "PendingUploaded" || EappVariables.EappKeys.Key7 == "LASError")) {
            $scope.selectedRequirementIndex = $scope.LifeEngageProduct.Requirements.indexOf(requirement);
            $scope.selectedRequirement = requirement;
            $scope.mypopup = true;
            $rootScope.docview = false;
            $rootScope.tabview = true;
            $scope.$parent.showBackBtn = true;
            var initial = "initial";
            $scope.firstName = "";
            $scope.othersIdentify = true;
            if (requirement.partyName != "" && typeof requirement.partyName != "undefined") {
                $scope.firstName = requirement.partyName;
            }
            if (sectionIndex == 6) {
                $scope.firstName = requirement.requirementType + " " + "Documents";
                $scope.othersIdentify = false;
            }
            $scope.accordionIndex = sectionIndex;
            if ((rootConfig.isDeviceMobile)) {
                $scope.getStatusForDocuments();
            }
            $scope.refresh();
        } else {
            $scope.selectedRequirementIndex = $scope.LifeEngageProduct.Requirements.indexOf(requirement);
            $scope.selectedRequirement = requirement;
            $scope.mypopup = true;
            $rootScope.docview = false;
            $rootScope.tabview = true;
            $scope.$parent.showBackBtn = true;
            var initial = "initial";
            $scope.firstName = "";
            $scope.othersIdentify = true;
            if (requirement.partyName != "" && typeof requirement.partyName != "undefined") {
                $scope.firstName = requirement.partyName;
            }
            if (sectionIndex == 6) {
                $scope.firstName = requirement.requirementType + " " + "Documents";
                $scope.othersIdentify = false;
            }
            $scope.accordionIndex = sectionIndex;
            if ((rootConfig.isDeviceMobile)) {
                $scope.getStatusForDocuments();
            }
            $scope.refresh();
        }
    };

    /* method to add new document to the requirement*/
    $scope.addNewRequirementDocument = function (index) {
		 if($scope.selectedRequirement.requirementType=="Memo") { //rootConfig.MemoStatus
			var currentDate = new Date();
			var timeStamp = currentDate.getTime();
			var documentModel = (({
				documentName: $scope.selectedRequirement.requirementSubType+(index+2),
				documentOptions: [],
				documentProofSubmitted: "",
				documentStatus: "",
				documentUploadStatus: "",
				isMandatory: false,
				modifiableIndicator:true
			}));
			var obj={"code":""};
			obj.code=$scope.selectedRequirement.requirementSubType+(index+2);
			$scope.documentTypesMemo.push(obj);
			$scope.selectedRequirement.Documents.push(documentModel);
			 $rootScope.showHideLoadingImage(true, 'saveTransactionMessage', $translate);
			EappService.saveTransaction(function () {
				$rootScope.showHideLoadingImage(false);
			}, this.onSaveError, false); 
		 }else{
			var currentDate = new Date();
			var timeStamp = currentDate.getTime();
			var documentModel = (({
				documentName: "Agent1" + "_" + $scope.selectedRequirement.requirementType + "_" + $scope.selectedRequirement.requirementType + "_" + $scope.selectedRequirement.partyIdentifier + "_" + timeStamp,
				documentOptions: [],
				documentProofSubmitted: "",
				documentStatus: "",
				documentUploadStatus: "",
				isMandatory: false
			}));
			$scope.selectedRequirement.Documents.push(documentModel);
			$rootScope.showHideLoadingImage(true, 'saveTransactionMessage', $translate);
			EappService.saveTransaction(function () {
				$rootScope.showHideLoadingImage(false, "");
			}, this.onSaveError, false);
		}
    };

    /* method to remove the document from the requirement
       Also it will remove the pages added to the document.
       It will not remove the document if it is not mandatory.
       It will add a new empty document if there is no non-mandatory  documents
    */
    /* 	$scope.deleteRequirementDocument=function(index){
				if(!$scope.selectedRequirement.Documents[index].isMandatory){
				    if((rootConfig.isDeviceMobile)){
						EappService.getRequirementFilesToDelete(EappVariables.EappKeys.TransTrackingID, EappVariables.EappModel.Requirements[$scope.selectedRequirementIndex].Documents[index].documentName,function(reqFiles){
						
						 for(var i=0;i<reqFiles.length;i++){
						 reqFiles[i].status="Deleted";
						  EappService.updateRequirementFiles(reqFiles[i],function(){
						  
						  }, function(){
							
						});
					}
					
					},function(error) {
						});
						}
						EappVariables.EappModel.Requirements[$scope.selectedRequirementIndex].Documents[index].documentStatus ="Deleted";
						EappService.saveTransaction(function(){
						var currentDate = new Date();
						var timeStamp = currentDate.getTime();
						$scope.selectedRequirement.Documents= $filter('documentRequirementFilter')($scope.selectedRequirement);
							var lastIndex = $scope.selectedRequirement.Documents.length-1;
					if(lastIndex>=0 && $scope.selectedRequirement.Documents[$scope.selectedRequirement.Documents.length-1].isMandatory || lastIndex<0 ){
						// It will add a new empty document if there is no non-mandatory  documents 
						var documentModel = (({
							documentName: "Agent1" + "_" + $scope.selectedRequirement.requirementType + "_" + $scope.selectedRequirement.requirementType + "_" + $scope.selectedRequirement.partyIdentifier + "_" + timeStamp,
							documentOptions:[],
							documentProofSubmitted: "",
							documentStatus: "",
							isMandatory: false
								}));   
						$scope.selectedRequirement.Documents.push(documentModel);
						EappService.saveTransaction(function(){}, this.onSaveError, false);						
						$scope.refresh();						
					} 
						
						}, this.onSaveError, false);
					//$scope.selectedRequirement.Documents.splice(index, 1);
					// need to remove files also
				}
				
			};
 */
    $scope.deleteButtonClick = function (index) {
		if($scope.selectedRequirement.requirementType=="Memo") { //rootConfig.MemoStatus
			$scope.requirementName=$scope.selectedRequirement.Documents[index].documentName;
			for(var k=0;k<$scope.documentTypesMemo.length;k++){
				if($scope.documentTypesMemo[k].code.indexOf($scope.requirementName)!=-1)
					$scope.documentTypesMemo.splice(k,1);
			}
			if (!$scope.selectedRequirement.Documents[index].isMandatory) {
				if ((rootConfig.isDeviceMobile)) {
					$scope.deleteRequirementDocument(EappVariables.EappKeys.TransTrackingID,
						EappVariables.EappModel.Requirements[$scope.selectedRequirementIndex].Documents[index].documentName,
						function () {},
						function (e) {});
				}
				EappVariables.EappModel.Requirements[$scope.selectedRequirementIndex].Documents[index].documentStatus = "Deleted";
				$rootScope.showHideLoadingImage(true, 'saveTransactionMessage', $translate);
				EappService.saveTransaction(function () {
					$rootScope.showHideLoadingImage(false, "");
					var currentDate = new Date();
					var timeStamp = currentDate.getTime();
					$scope.selectedRequirement.Documents = $filter('documentRequirementFilter')($scope.selectedRequirement);
					var lastIndex = $scope.selectedRequirement.Documents.length - 1;
					if (lastIndex >= 0 && $scope.selectedRequirement.Documents[$scope.selectedRequirement.Documents.length - 1].isMandatory || lastIndex < 0) {
						/* It will add a new empty document if there is no non-mandatory  documents */
						var documentModel = (({
							documentName: $scope.requirementName,
							documentOptions: [],
							documentProofSubmitted: "",
							documentStatus: "",
							isMandatory: false,
							modifiableIndicator:true
						}));
						$scope.selectedRequirement.Documents.push(documentModel);
						$rootScope.showHideLoadingImage(true, 'saveTransactionMessage', $translate);
						EappService.saveTransaction(function () {
							var obj={"code":""};
							obj.code=$scope.selectedRequirement.requirementSubType+"1";
							$scope.documentTypesMemo.push(obj);
							$rootScope.showHideLoadingImage(false, "");
						}, function () {
							$rootScope.showHideLoadingImage(false, "");
						}, false);
						$scope.refresh();
					}

				}, this.onSaveError, false);
			}
		}else{
			if (!$scope.selectedRequirement.Documents[index].isMandatory) {
				if ((rootConfig.isDeviceMobile)) {
					$scope.deleteRequirementDocument(EappVariables.EappKeys.TransTrackingID,
						EappVariables.EappModel.Requirements[$scope.selectedRequirementIndex].Documents[index].documentName,
						function () {},
						function (e) {});
				}
				EappVariables.EappModel.Requirements[$scope.selectedRequirementIndex].Documents[index].documentStatus = "Deleted";
				$rootScope.showHideLoadingImage(true, 'saveTransactionMessage', $translate);
				EappService.saveTransaction(function () {
					$rootScope.showHideLoadingImage(false, "");
					var currentDate = new Date();
					var timeStamp = currentDate.getTime();
					$scope.selectedRequirement.Documents = $filter('documentRequirementFilter')($scope.selectedRequirement);
					var lastIndex = $scope.selectedRequirement.Documents.length - 1;
					if (lastIndex >= 0 && $scope.selectedRequirement.Documents[$scope.selectedRequirement.Documents.length - 1].isMandatory || lastIndex < 0) {
						/* It will add a new empty document if there is no non-mandatory  documents */
						var documentModel = (({
							documentName: "Agent1" + "_" + $scope.selectedRequirement.requirementType + "_" + $scope.selectedRequirement.requirementType + "_" + $scope.selectedRequirement.partyIdentifier + "_" + timeStamp,
							documentOptions: [],
							documentProofSubmitted: "",
							documentStatus: "",
							isMandatory: false
						}));
						$scope.selectedRequirement.Documents.push(documentModel);
						$rootScope.showHideLoadingImage(true, 'saveTransactionMessage', $translate);
						EappService.saveTransaction(function () {
							$rootScope.showHideLoadingImage(false, "");
						}, function () {
							$rootScope.showHideLoadingImage(false, "");
						}, false);
						$scope.refresh();
					}

				}, this.onSaveError, false);
			}
		}
    }
    $scope.deleteRequirementDocument = function (transtrackingId, documentName, successCallback) {

        function deleteRequirementFile(reqFiles, successCallback) {

            var reqFilesToDelete = angular.copy(reqFiles);
            $scope.deleteTypeFlag = "RowDelete";
            EappService.updateRequirementFiles(reqFiles[$scope.reqIndex], function () {
                $scope.reqIndex++;
                if ($scope.reqIndex < reqFiles.length) {
                    deleteRequirementFile(reqFilesToDelete, successCallback);
                } else {

                    successCallback();
                }
            }, function () {}, $scope.deleteTypeFlag);

        }
        $scope.reqIndex = 0;
        EappService.getRequirementFilesToDelete(transtrackingId, documentName, function (reqFiles) {
            if (reqFiles.length > 0) {
                deleteRequirementFile(reqFiles, successCallback);
            } else {
                successCallback();
            }
        }, function (error) {});
    }

    $scope.accordion_click = function (index) {
        $scope.accordionIndex = index;
    };

    $scope.uploadButtonClick = function (index) {
        
        //var documentNameUserName = $scope.selectedRequirement.Documents[index].documentName;
        // documentNameUserName.substring(0, documentNameUserName.lastIndexOf("_") + 1) +''+ 
        $scope.documentNameUserName = $scope.LifeEngageProduct.Insured.BasicDetails.firstName +' '+ $scope.LifeEngageProduct.Insured.BasicDetails.lastName;
        
        if($scope.selectedRequirement.partyIdentifier==='Payer'){
            $scope.documentNameUserName=$scope.LifeEngageProduct.Payer.BasicDetails.fullName;
        }
        $scope.DocUploadPopUp = true;
        $scope.docUploadIndex = index;
        $scope.docSnapShot = [];
        $scope.docImage = $scope.defaultImage;
		if($scope.selectedRequirement.requirementType=="Memo"){
			 $scope.memoTileSelected=true;
			 $scope.documentSelected = $scope.selectedRequirement.Documents[index].documentProofSubmitted;
			 $scope.documentSelectedMemo = $scope.selectedRequirement.Documents[index].documentName;
		}else if($scope.selectedRequirement.requirementType=="GAO"){
			 $scope.memoTileSelected=true;
			 $scope.documentSelected = $scope.selectedRequirement.Documents[index].documentProofSubmitted;
			 $scope.documentSelectedMemo = $scope.selectedRequirement.Documents[index].documentName;
		}else{
			$scope.memoTileSelected=false;
			$scope.documentSelected = $scope.selectedRequirement.Documents[index].documentProofSubmitted;
		}
        $scope.nameOfInsured = $scope.LifeEngageProduct.Insured.BasicDetails.firstName;
        $scope.imageIndex = 0;
        if ($scope.selectedRequirement.Documents[index].Status == "sync_status_tick") {
            $scope.disableBrowseAndDel = true;
        } else {
            $scope.disableBrowseAndDel = false;
        }
		if ((EappVariables.EappKeys.Key15 && EappVariables.EappKeys.Key15 == "Submitted" && EappVariables.EappKeys.Key7 && EappVariables.EappKeys.Key7 != "" && EappVariables.EappKeys.Key7 != "Memo" && EappVariables.EappKeys.Key7 != "PendingUploaded")) {
			$scope.disableBrowse = true;
			$scope.disableBrowseAndDel = true;
		}else{
			if(EappVariables.EappKeys.Key15 != "Submitted"){
				$scope.disableBrowse = false;
				$scope.disableBrowseAndDel = false;
			}else if(EappVariables.EappKeys.Key15 == "Submitted" && $scope.selectedRequirement.requirementType!='Memo'){
				$scope.disableBrowseAndDel = true;
				$scope.disableBrowse = true;
			}else{
				if((rootConfig.MemoStatusForDoc.indexOf($scope.selectedRequirement.partyIdentifier)>-1) || $scope.selectedRequirement.Documents[index].modifiableIndicator == false || $scope.selectedRequirement.Documents[index].modifiableIndicator == "false"){
					$scope.disableBrowseAndDel = true;
					$scope.disableBrowse = true;
				}else{
					$scope.disableBrowse = false;
					$scope.disableBrowseAndDel = false;
				}
			}
		}
        //Disabling Delete button for Pending cases for Web
       /*  if (!rootConfig.isDeviceMobile && (EappVariables.EappKeys.Key7 == "Pending" || EappVariables.EappKeys.Key7 == "PendingUploaded") && $scope.selectedRequirement.Documents[index].documentUploadStatus == true) {
            $scope.disableBrowseAndDel = true;
         }*/
        $rootScope.showHideLoadingImage(true, "eapp_vt.fetchRequirementFiles", $translate);
        EappService.getDocumentsForRequirement($scope.selectedRequirement, $scope.documentSelected,
            EappVariables.EappKeys.TransTrackingID,
            function () {
                $scope.savedFiles = angular.copy(EappVariables.Requirement);
                if (EappVariables.Requirement) {
                    $scope.selectedIndex = EappVariables.Requirement.length - 1;
                }
                if (EappVariables.Requirement.length > 0) {
                    for (var i = 0; i < EappVariables.Requirement.length; i++) {
                        $scope.documentObject = {};
                        if (EappVariables.Requirement[i] && EappVariables.Requirement[i].base64string) {
                            //$scope.docImage = EappVariables.Requirement[i].base64string;
                            $scope.docType = EappVariables.Requirement[i].fileName.split(".")[1];
                            // if ($scope.docType == "pdf") {
                            //     $scope.pdfFile = EappVariables.Requirement[i].fileName;
                            //     $scope.docImage = $scope.pdfImage;
                            // } else {
                                $scope.docImage = EappVariables.Requirement[i].base64string;
                            //}
                            $scope.comments = EappVariables.Requirement[i].description;
                            $scope.docSnapShot.push($scope.docImage);

                        }
                    }
                    if ($scope.docSnapShot.length > 0) {
                        $scope.showDeletePopup = true;
                    } else {
                        $scope.showDeletePopup = false;
                    }
                    $scope.imageIndex = $scope.docSnapShot.length;
                    $scope.refresh();
                } else {
                    $scope.imageIndex = 0;
                    $scope.comments = "";
                    $scope.refresh();
                }
                $rootScope.showHideLoadingImage(false, "eapp_vt.fetchRequirementFiles", $translate);
                $scope.refresh();
            },
            function (error) {
                $rootScope.showHideLoadingImage(false, "eapp_vt.fetchRequirementFiles", $translate);
            });
    };

    $scope.uploadPopupClose = function () {
        $scope.DocUploadPopUp = false;
        $scope.applicableDoclists = [];
        if (($scope.selectedRequirement.requirementType == "AgeProof" || $scope.selectedRequirement.requirementType == "IdProof" ||
                $scope.selectedRequirement.requirementType == "AddressProof") && ($scope.docSnapShot.length > 0) &&
            ($scope.docImage) && $scope.isModelChanged) {

            $scope.checkPopup = true;
            $scope.isModelChanged = false;
        } else {
            $scope.checkPopup = false;
        }

        if ($scope.docSnapShot.length > 0 && $scope.docImage && (EappVariables.Requirement.length != $scope.savedFiles.length)) {
            for (var i = 0; i < EappVariables.DocumentMastList.length; i++) {
                if (EappVariables.DocumentMastList[i].DocumentCode === $scope.documentSelected) {

                    for (var j = 0; j < EappVariables.DocumentMastList[i].Applicable.length; j++) {
                        var documentName = EappVariables.DocumentMastList[i].Applicable[j].Document;
                        documentName = documentName.replace(/\s/g, "");
                        if (documentName === "IdentityProof") {
                            documentName = "IdProof";
                        }
                        if ($scope.selectedRequirement.requirementType !== documentName) {
                            $scope.applicableDoclists.push(EappVariables.DocumentMastList[i].Applicable[j]);
                        }

                    }
                }
            }
        }
        if ($scope.savedFiles.length > EappVariables.Requirement.length || $scope.savedFiles.length < EappVariables.Requirement.length) {
            $scope.selectedRequirement.Documents[$scope.docUploadIndex].Status = "sync_status_unsync";
        }
    };

    $scope.checkPopupClose = function () {
        $scope.checkPopup = false;
        var selectedTypeArray = [];
        var checkedValue = "";
        var checkedNewVal = "";
        var index = $scope.docUploadIndex;
        $scope.savedFiles = angular.copy(EappVariables.Requirement);

        $("input:checkbox[id=popup_check_box]:checked").each(function () {
            selectedTypeArray.push($(this).val());
        });

        for (var i = 0; i < selectedTypeArray.length; i++) {

            checkedValue = selectedTypeArray[i];
            checkedNewVal = checkedValue.replace(/\s/g, "");
            if (checkedNewVal == "IdentityProof") {
                checkedNewVal = "IdProof";
            }

            for (var j = 0; j < EappVariables.EappModel.Requirements.length; j++) {
                if (checkedNewVal == EappVariables.EappModel.Requirements[j].requirementType) {
                    EappVariables.EappModel.Requirements[j].Documents[index].documentProofSubmitted = $scope.documentSelected;
                    if (EappVariables.EappModel.Requirements[j].partyIdentifier == $scope.selectedRequirement.partyIdentifier) {
                        for (var l = 0; l < EappVariables.EappModel.Requirements[j].Documents.length; k++) {
                            if ((rootConfig.isDeviceMobile)) {
                                $scope.deleteRequirementDocument(EappVariables.EappKeys.TransTrackingID, EappVariables.EappModel.Requirements[j].Documents[l].documentName, function () {

                                    for (var k = 0; k < $scope.savedFiles.length; k++) {

                                        EappService.copyUploadedFile($scope.savedFiles[k].base64string, index,
                                            EappVariables.EappModel.Requirements[j].requirementName,
                                            EappVariables.EappModel.Requirements[j].requirementType, $scope.documentSelected,
                                            $scope.savedFiles[k].description,
                                            function (filepath) {});

                                    }

                                })
                            } else {
                                $scope.pageIndex = 0;

                                $scope.deletePagesInWeb(EappVariables.EappModel.Requirements[j],
                                    EappVariables.EappModel.Requirements[j].Documents[l].pages, EappVariables.EappModel.Requirements[j].Documents[l]);

                            }
                        }

                    }

                }
            }

        }


    };
    /*$scope.deleteAllPages=function(){

    	for(var i=0; i< EappVariables.Requirement.length;i++){

    		EappService.updateRequirementFiles(EappVariables.Requirement[i],function(){

    	},function(){});

    }*/

    $scope.deletePagesInWeb = function (reqFiles, pages, documents, successCallback) {
        if (documents && documents.pages && documents.pages.length > 0) {
            EappService.deleteRequirementInWeb(reqFiles, pages[$scope.pageIndex], documents, function () {
                $scope.pageIndex++;
                if ($scope.pageIndex < pages.length) {

                    deletePagesInWeb(reqFiles);

                } else {
                    $scope.savedFileIndex = 0;
                    $scope.copyUploadedFile(reqFiles.requirementName, reqFiles.requirementType);

                }
            });
        } else {
            $scope.savedFileIndex = 0;
            $scope.copyUploadedFile(reqFiles.requirementName, reqFiles.requirementType);

        }
    }

    $scope.copyUploadedFile = function (requirementName, requirementType) {
        EappService.copyUploadedFile($scope.savedFiles[$scope.savedFileIndex].base64string, $scope.docUploadIndex,
            requirementName,
            requirementType, $scope.documentSelected,
            $scope.savedFiles[$scope.savedFileIndex].description,
            function (filepath) {
                $scope.savedFileIndex++;
                if ($scope.savedFileIndex < $scope.savedFiles.length) {
                    $scope.copyUploadedFile(requirementName, requirementType);
                }
            });

    };
    $scope.deletePopupVisible = function () {
        $scope.deletetilespopup = true;

    };
    
    $scope.deleteAllImages = function() {
    	$scope.deletetilespopup1 = true;
    }
    

    $scope.deleteAllPagesConfirm = function(){
    	 $rootScope.showHideLoadingImage(true, "Deleting Requirement Files, Please wait", $translate);
         $scope.deleteTypeFlag = "ImageDelete";
         if (EappVariables.Requirement.length == 1) {
             $scope.LifeEngageProduct.Requirements[$scope.selectedRequirementIndex].Documents[$scope.docUploadIndex].documentUploadStatus = false;
 			if(EappVariables.EappKeys.Key7 == "Memo" || EappVariables.EappKeys.Key7 == "PendingUploaded"){
 				var flagForDoc=false;
 				for (var j = 0; j < $scope.LifeEngageProduct.Requirements.length && !flagForDoc; j++) {
 					if ($scope.LifeEngageProduct.Requirements[j].requirementType == "Memo") {
 						for (var k = 0; k < $scope.LifeEngageProduct.Requirements[j].Documents.length; k++) {
 							if ($scope.LifeEngageProduct.Requirements[j].Documents[k].documentUploadStatus == true && $scope.LifeEngageProduct.Requirements[j].Documents[k].modifiableIndicator==true) {
 								flagForDoc=true;
 								break;
 							}
 						}
 					}
 				}
 				if(!flagForDoc){
 					EappVariables.EappKeys.Key7= "Memo";
 				}
 			}
         }
         if (EappVariables.Requirement.length > 0) {

      	   for (var i = 0; i < EappVariables.Requirement.length; i++) {
                     EappVariables.Requirement[i].status = "Deleted";
                     EappVariables.Requirement[i].identifier = EappVariables.EappKeys.TransTrackingID;
      	   }
      	   
      	 for (var i = 0; i < EappVariables.Requirement.length; i++) {  
      	 for (var j = 0; j < EappVariables.EappModel.Requirements.length; j++) {
             if (EappVariables.EappModel.Requirements[j].requirementType != "Signature") {
                 for (var k = 0; k < EappVariables.EappModel.Requirements[j].Documents.length; k++) {
                     if (EappVariables.EappModel.Requirements[j].Documents[k].pages) {
                         for (var x = 0; x < EappVariables.EappModel.Requirements[j].Documents[k].pages.length; x++) {
                             if (EappVariables.EappModel.Requirements[j].Documents[k].pages[x].fileName == EappVariables.Requirement[i].fileName) {
                                 EappVariables.EappModel.Requirements[j].Documents[k].documentUploadStatus = false;
                                 EappVariables.EappModel.Requirements[j].Documents[k].pages[x].documentStatus = "Deleted";
                             }
                         }
                     }

                 }
             }

         }
      	 }
      	   
      	   
      	    for (var i = 0; i < EappVariables.Requirement.length; i++) {
      	     
      	    	
               if (!rootConfig.isDeviceMobile) {
                         EappVariables.Requirement[i].proposalId = EappVariables.EappKeys.Key4;
                     }
                     EappService.updateRequirementFiles(EappVariables.Requirement[i], function () {
                         $scope.isModelChanged = true;
                         $scope.docSnapShot.splice($scope.selectedIndex, 1);
                         EappVariables.Requirement.splice(i, 1);
                         $scope.imageIndex -= 1;
                         if ($scope.docSnapShot.length === 0) {
                             $scope.docImage = $scope.defaultImage;
                             $scope.showDeletePopup = false;
                         } else {
                             $scope.docImage = $scope.docSnapShot[$scope.docSnapShot.length - 1];
                             $scope.selectedIndex = $scope.docSnapShot.length - 1;
                         }

                         $rootScope.showHideLoadingImage(false, "Deleting Requirement Files, Please wait", $translate);
                         $scope.refresh();
                     }, function () {}, $scope.deleteTypeFlag);
               //      break;

             }
         } else {
             $rootScope.showHideLoadingImage(false, "Deleting Requirement Files, Please wait", $translate);
         }
         $scope.deletetilespopup1 = false;
    }

    $scope.deletePage = function () {
        $rootScope.showHideLoadingImage(true, "Deleting Requirement Files, Please wait", $translate);
        $scope.deleteTypeFlag = "ImageDelete";
        if (EappVariables.Requirement.length == 1) {
            $scope.LifeEngageProduct.Requirements[$scope.selectedRequirementIndex].Documents[$scope.docUploadIndex].documentUploadStatus = false;
			if(EappVariables.EappKeys.Key7 == "Memo" || EappVariables.EappKeys.Key7 == "PendingUploaded"){
				var flagForDoc=false;
				for (var j = 0; j < $scope.LifeEngageProduct.Requirements.length && !flagForDoc; j++) {
					if ($scope.LifeEngageProduct.Requirements[j].requirementType == "Memo") {
						for (var k = 0; k < $scope.LifeEngageProduct.Requirements[j].Documents.length; k++) {
							if ($scope.LifeEngageProduct.Requirements[j].Documents[k].documentUploadStatus == true && $scope.LifeEngageProduct.Requirements[j].Documents[k].modifiableIndicator==true) {
								flagForDoc=true;
								break;
							}
						}
					}
				}
				if(!flagForDoc){
					EappVariables.EappKeys.Key7= "Memo";
				}
			}
        }
        if (EappVariables.Requirement.length > 0) {
            for (var i = 0; i < EappVariables.Requirement.length; i++) {
                if (EappVariables.Requirement[i].base64string === $scope.docImage || EappVariables.Requirement[i].fileName == $scope.pdfFile) {
                    EappVariables.Requirement[i].status = "Deleted";
                    EappVariables.Requirement[i].identifier = EappVariables.EappKeys.TransTrackingID;

                    for (var j = 0; j < EappVariables.EappModel.Requirements.length; j++) {
                        if (EappVariables.EappModel.Requirements[j].requirementType != "Signature") {
                            for (var k = 0; k < EappVariables.EappModel.Requirements[j].Documents.length; k++) {
                                if (EappVariables.EappModel.Requirements[j].Documents[k].pages) {
                                    for (var x = 0; x < EappVariables.EappModel.Requirements[j].Documents[k].pages.length; x++) {
                                        if (EappVariables.EappModel.Requirements[j].Documents[k].pages[x].fileName == EappVariables.Requirement[i].fileName) {
                                            EappVariables.EappModel.Requirements[j].Documents[k].pages[x].documentStatus = "Deleted";
                                        }
                                    }
                                }

                            }
                        }

                    }

                    if (!rootConfig.isDeviceMobile) {
                        EappVariables.Requirement[i].proposalId = EappVariables.EappKeys.Key4;
                    }
                    EappService.updateRequirementFiles(EappVariables.Requirement[i], function () {
                        $scope.isModelChanged = true;
                        $scope.docSnapShot.splice($scope.selectedIndex, 1);
                        EappVariables.Requirement.splice(i, 1);
                        $scope.imageIndex -= 1;
                        if ($scope.docSnapShot.length === 0) {
                            $scope.docImage = $scope.defaultImage;
                            $scope.showDeletePopup = false;
                        } else {
                            $scope.docImage = $scope.docSnapShot[$scope.docSnapShot.length - 1];
                            $scope.selectedIndex = $scope.docSnapShot.length - 1;
                        }

                        $rootScope.showHideLoadingImage(false, "Deleting Requirement Files, Please wait", $translate);
                        $scope.refresh();
                    }, function () {}, $scope.deleteTypeFlag);
                    break;
                }
            }
        } else {
            $rootScope.showHideLoadingImage(false, "Deleting Requirement Files, Please wait", $translate);
        }
        $scope.deletetilespopup = false;
    };


    $scope.cancelDeletePopup = function () {
        $scope.deletetilespopup = false;
    };
    
    $scope.cancelDeletePopupConfirm = function() {
    	$scope.deletetilespopup1 = false;
    };

    $scope.updateErrorCount = function (id) {
        $rootScope.updateErrorCount(id);
    };

    $scope.showErrorPopup = function (id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
    };

    $scope.deleteDocument = function (index) {
        EappService.deleteDocuments(index);
    };

    $scope.changePreviewAsSnapshot = function (snapshot, index) {
        $scope.selectedIndex = index;
        $scope.imageIndex = index + 1;
        for (var i = 0; i < EappVariables.Requirement.length; i++) {
            if (EappVariables.Requirement[i]) {
                var image = EappVariables.Requirement[i].base64string;
                var fileName = EappVariables.Requirement[i].fileName;
                if (image === snapshot) {
                    //$scope.docImage = image;
                    var imageType = EappVariables.Requirement[i].fileName.split(".")[1];
                    // if (imageType == "pdf") {
                    //     $scope.pdfFile = EappVariables.Requirement[i].fileName;
                    //     $scope.docImage = $scope.pdfImage;
                    // } else {
                        $scope.docImage = image;
                    //}
                    $scope.comments = EappVariables.Requirement[i].description;
                }
            }
        }
        $scope.refresh();
    };
    $scope.imageRightClick = function () {
        var index;
        if ($scope.selectedIndex !== EappVariables.Requirement.length - 1) {
            index = $scope.selectedIndex + 1;
        } else {
            index = $scope.selectedIndex;
        }
        var snapshot = EappVariables.Requirement[index].base64string;
        $scope.changePreviewAsSnapshot(snapshot, index);
    };
    $scope.imageLeftClick = function () {
        var index;
        if ($scope.selectedIndex !== 0) {
            index = $scope.selectedIndex - 1;
        } else {
            index = $scope.selectedIndex;
        }
        var snapshot = EappVariables.Requirement[index].base64string;
        $scope.changePreviewAsSnapshot(snapshot, index);
    };

    $scope.onImageClick = function (index) {
    	var j=0;
    	var imagesDeleted="No";
    	for (var i = 0; i < EappVariables.Requirement.length; i++) {
    		if(EappVariables.Requirement[i].status == "Deleted"){
    			imagesDeleted="Yes";
    		}
    	}
    	
    	if(imagesDeleted!=null && imagesDeleted=="Yes") {
    		for (var i = 0; i < EappVariables.Requirement.length; i++) {
    			if(EappVariables.Requirement[i].status == "Deleted"){
    				j++;
    			}else{
    				index = j;
    				break;
    			}
    		}
    	}
    		
        var snapshot = EappVariables.Requirement[index].base64string;
        $scope.changePreviewAsSnapshot(snapshot, index);
    };

    
    $scope.browseFile = function (isImage, isPhoto, fileObj) {
        var index = $scope.docUploadIndex;
        var requirementName = $scope.selectedRequirement.requirementName;
        var requirementType = $scope.selectedRequirement.requirementType;

        if (fileObj) {
            $rootScope.showHideLoadingImage(true, 'uploadingRequiremnt', $translate);
            $scope.LifeEngageProduct.Requirements[$scope.selectedRequirementIndex].Documents[index].documentUploadStatus = true;
            EappVariables.EappModel.Requirements[$scope.selectedRequirementIndex].Documents[index].documentUploadStatus = "true";
			 if (EappVariables.EappKeys.Key7 == "Memo") {
				EappVariables.EappKeys.Key7 = "PendingUploaded";
				//$scope.LifeEngageProduct.Requirements[$scope.selectedRequirementIndex].Documents[index].documentStatus = "Resubmitted";
				/* if (!rootConfig.isDeviceMobile) 
					EappService.saveTransaction(function () {}, function () {}, false); */
			}else if(EappVariables.EappKeys.Key7 == "PendingUploaded"){
				//$scope.LifeEngageProduct.Requirements[$scope.selectedRequirementIndex].Documents[index].documentStatus = "Resubmitted";
			} 
            EappService.getRequirementFilePath(fileObj, index, requirementName, requirementType, $scope.documentSelected,
                function () {
                    $timeout(function () {
                    	if (rootConfig.isDeviceMobile){
                    		$rootScope.showHideLoadingImage(true, 'uploadingRequiremnt', $translate);
                        }
                    	
                        var DocumentPath = EappVariables.DocumentPath;
                        if (!(rootConfig.isDeviceMobile)) {
                            var fileName = fileObj.name;
                            var fileType = fileName.split(".")[1];
                           /* if (fileType == "pdf") {
                                $scope.docImage = $scope.pdfImage;
                            } else {*/
                                $scope.docImage = EappVariables.DocumentPath;
                           // }
                        } else {
                            $scope.docImage = EappVariables.DocumentPath;
                        }

                        $scope.isModelChanged = true;
                        $scope.docSnapShot.push($scope.docImage);
                        $scope.imageIndex = $scope.imageIndex + 1;
                        $scope.selectedIndex = EappVariables.Requirement.length - 1;
                        $scope.pdfFile = EappVariables.Requirement[$scope.selectedIndex].fileName;
                        if ($scope.docSnapShot.length > 0) {
                            $scope.showDeletePopup = true;
                        } else {
                            $scope.showDeletePopup = false;
                        }
                        $scope.comments = "";

                        //Modified for Generali. Changing key values for Resubmission
                       /*  if (EappVariables.EappKeys.Key7 == "Memo") {
                            EappVariables.EappKeys.Key7 = "PendingUploaded";
                            $scope.LifeEngageProduct.Requirements[$scope.selectedRequirementIndex].Documents[index].documentStatus = "Resubmitted";
                        }else if(EappVariables.EappKeys.Key7 == "PendingUploaded"){
							 $scope.LifeEngageProduct.Requirements[$scope.selectedRequirementIndex].Documents[index].documentStatus = "Resubmitted";
						} */
                        $rootScope.showHideLoadingImage(false, 'uploadingRequiremnt', $translate);
                        $scope.refresh();
                    }, 1000);

                },
                function (error) {
                    $scope.DocUploadPopUp = false;
                    if (EappVariables.Requirement.length == 0) {
                        $scope.LifeEngageProduct.Requirements[$scope.selectedRequirementIndex].Documents[index].documentUploadStatus = false;
                        EappVariables.EappModel.Requirements[$scope.selectedRequirementIndex].Documents[index].documentUploadStatus = false;
                    }
                    if (error == "ext") {
                        $rootScope.lePopupCtrl.showError(
                            translateMessages($translate,
                                "lifeEngage"),
                            translateMessages($translate,
                                "invalidPopupMesage"),
                            translateMessages($translate,
                                "fna.ok"),
                            function () {
                                $scope.DocUploadPopUp = true;
                            });
                    } else if (error == "size") {

                        $rootScope.lePopupCtrl.showError(
                            translateMessages($translate,
                                "lifeEngage"),
                            translateMessages($translate,
                                "invalidSizeMesage"),
                            translateMessages($translate,
                                "fna.ok"),
                            function () {
                                $scope.DocUploadPopUp = true;
                            });
                    }
                    $rootScope.showHideLoadingImage(false, 'uploadingRequiremnt', $translate);
                    $scope.refresh();

                });
        }
        if (rootConfig.isDeviceMobile){
        	$rootScope.showHideLoadingImage(false);
        }
    };

    $scope.saveFileReqIE = function (base64Data) {
        EappService.saveFileReqIE(base64Data, $scope.docUploadIndex, $scope.selectedRequirement.requirementName, $scope.selectedRequirement.requirementType, $scope.documentSelected,
            function () {
                $timeout(function () {

                    var DocumentPath = EappVariables.DocumentPath;
                    $scope.docImage = "data:image/png;base64," + EappVariables.DocumentPath;
                    $scope.docSnapShot.push($scope.docImage);
                    $scope.imageIndex = $scope.imageIndex + 1;
                    $scope.comments = "";
                    $scope.refresh();
                }, 5000);
            },
            function (error) {});


    };

    $scope.isSafariCheck = function () {

        $scope.isSafari = false;
        $scope.isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
        if ($scope.isSafari) {
            var safari = true;
            var userAgent = navigator.userAgent.toLowerCase();
            userAgent = userAgent.substring(userAgent.indexOf('version/') + 8);
            userAgent = userAgent.substring(0, userAgent.indexOf(' '));
            var version = userAgent;
            if (version < "7") {
                if (swfobject.hasFlashPlayerVersion("1")) {
                    safari = true;
                } else {
                    safari = false;
                    $scope.showIEMessage = "This Safari version does not support document upload.";
                }
            } else {
                safari = false;
            }
        } else {

            safari = false;
        }
        return safari;
    }

    //DocumentUploadController Load Event and Load Event Handler
   var load = $scope.$parent.$on('DocumentUpload', function (event, args) {
        $rootScope.showHideLoadingImage(true, "reqRuleProgress", $translate);
        $scope.selectedTabId = args[0];
        if (EappVariables.getDocTypeFormIdForPending.length == 0) {
            LEDynamicUI.getUIJson(rootConfig.eAppConfigJson, false, function (eAppConfigJson) {
                EappVariables.getDocTypeFormIdForPending = eAppConfigJson.docForPendingCase;
                $scope.Initialize();
            });
        } else {
            $scope.Initialize();
        }
		load();
    });

    $scope.$parent.$on('DocumentUpload', function (event, args) {

         $rootScope.showHideLoadingImage(true, "reqRuleProgress", $translate);
        $scope.selectedTabId = args[0];
        if (EappVariables.getDocTypeFormIdForPending.length == 0) {
            LEDynamicUI.getUIJson(rootConfig.eAppConfigJson, false, function (eAppConfigJson) {
                EappVariables.getDocTypeFormIdForPending = eAppConfigJson.docForPendingCase;
                $scope.InitializeForReturnToDoc();
            });
        } else {
            $scope.InitializeForReturnToDoc();
        }

    });

	$scope.$parent.$on('DocReturn', function (event, args) {
		$scope.InitializeForReturnToDoc();
	});

    $rootScope.loadDocumentSection =function(){        
        $scope.InitializeForReturnToDoc();
    }
	$scope.InitializeForReturnToDoc = function () {
        
        // debugger;
        $scope.$parent.disableProceedButtonCommom = false;
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
        }
		//Memo section
		$scope.memoOpen=rootConfig.MemoOpen;
		$scope.memoResolved=rootConfig.MemoResolved;
	    // if(rootConfig.Key7ValuesForMemoTile.indexOf(EappVariables.EappKeys.Key7)>-1){
        if(EappVariables.EappKeys.Key7!="Pending Submission" && EappVariables.EappKeys.Key7!="Submitted")
        {    	
			$scope.showMemoDocs=true;
		}
        else{
			$scope.showMemoDocs=false;
		}
		$scope.showGeneralDocs=true;
        $scope.LifeEngageProduct = EappVariables.getEappModel();
        $scope.eAppParentobj.PreviousPage = "";
        $scope.eappKeys = EappVariables.EappKeys;
        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        $scope.showErrorCount = true;
        $scope.DocUploadPopUp = false;
        $scope.isModelChanged = false;
        $scope.disableBrowseAndDel = false;
        $scope.docSnapShot = [];
        $scope.selectedRequirement = [];
        EappVariables.Requirement = [];
        $scope.status = "";
        $scope.defaultImage = "";
        $scope.pdfImage = "css/generali-theme/img/dummy_img_pdf.png";
        $scope.deletebutton = true;
        $scope.hideBeneficiaryTiles = false;
        $rootScope.showHideLoadingImage(true, 'reqRuleProgress', $translate);
        $scope.Requirementlist = EappVariables.EappModel.Requirements;
		EappService.checkRequirementGAOSignature($scope.Requirementlist,'GAO', function (returnReq) {
            if (returnReq == 0) {
				$scope.OnlyGAO=true;
			}else{
				$scope.OnlyGAO=false;
			}
		});


        $timeout(function(){
              
         if(EappVariables.EappModel.Requirements){
            for (var j = 0; j < EappVariables.EappModel.Requirements.length; j++) {
    
                        for (var l = 0; l < EappVariables.EappModel.Requirements[j].Documents.length; l++) {
                            
                            if(EappVariables.EappModel.Requirements[j].Documents[l].isMandatory && EappVariables.EappModel.Requirements[j].Documents[l].documentUploadStatus && $scope.$parent.totalDocCount == 0){

                                $scope.LifeEngageProduct.totalDocCount = $scope.$parent.totalDocCount;
                                

                            }
                        }
            }
        }

        },100)
        
        $scope.documentObject = {};
        $scope.$parent.showBackBtn = false;
        $scope.checkPopup = false;
        $scope.savedFiles = [];
        $scope.docDisabled = [];
        $scope.deletePopup = false;
        $scope.indexTile = 0;
        $scope.$parent.totalDocCount = 0;
        $scope.$parent.docCountFlagForSubmit = rootConfig.GVLNonSubmitFlag;
       // $scope.getlookUpDocuments();
        $scope.getDocumentTypeConfigurationFiles();

        /*Handling Accordion heading for PO = MI / PO = AI*/
        $scope.checkPOEqualsMIorAI = function (sectionVal) {
            if ($scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer == 'Self') {
                return translateMessages($translate, "general.policyHolderEqualsMainInsured");
            }
            var translatedsectionVal = translateMessages($translate, "general.PolicyHolder");
            return translatedsectionVal;
        }
        /**For Calculating the Mandaory Documents**/
        $scope.mandatoyDocCounnt = 0;
        for (var i = 0; i < $scope.Requirementlist.length; i++) {
            if ($scope.Requirementlist[i].requirementType != "Signature" && $scope.Requirementlist[i].isMandatory == "true") {
                $scope.mandatoyDocCounnt++;
            }
        }
        $scope.setTileProperties($scope.indexTile, $scope.onSuccessOfSetTileProperties);
        $scope.isDevice = false;
        if (!rootConfig.isDeviceMobile) {
            $scope.isWeb = true;
        } else {
            $scope.isWeb = false;
        }
        if ((rootConfig.isDeviceMobile)) {
            var leFileUtils = new LEFileUtils();
            leFileUtils.getApplicationPath(function (documentsPath) {
                if ((rootConfig.isOfflineDesktop)) {
                    $scope.tileImagePath = "file://" + documentsPath;
                } else {
                    $scope.tileImagePath = "file://" + documentsPath + "/";
                }
            });
        } else {
            $scope.tileImagePath = rootConfig.mediaURL;
        }

        if (!(rootConfig.isOfflineDesktop) && rootConfig.isDeviceMobile) {
            $scope.isDevice = true;
        } else {
            $scope.isDevice = false;
        }
        var model = "LifeEngageProduct";
        if ((rootConfig.autoSave.eApp) && (typeof EappVariables.EappKeys.Key15 == "undefined" || EappVariables.EappKeys.Key15 == "New" || EappVariables.EappKeys.Key15 == "Confirmed")) {
            if (AutoSave.destroyWatch) {
                AutoSave.destroyWatch();
            }
            AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
        }
        $rootScope.showHideLoadingImage(false, "reqRuleProgress", $translate);
        // formatting Date on Tab navigation 
        $rootScope.stringFormattedDate = function () {}
    };
    
    $scope.downloadFiles = function(url){
    
    	if($scope.docImage!=null){
    		url=$scope.docImage;
    	}
    	
    	if (!rootConfig.isDeviceMobile) {
    	
    		if(url.indexOf('data:image') != -1){
          
          var src = url.replace(/^data:image\/[^;]+/, 'data:application/octet-stream');
          window.open(src, '_blank');

        }
        if(url.indexOf('data:application') != -1){
            
            var src = url.replace(/^data:application\/[^;]+/, 'data:application/octet-stream');
            window.open(src, '_blank');

        }
    }else {
    	         var imageType = EappVariables.Requirement[$scope.selectedIndex].fileName.split(".")[1];
                 if (imageType == "doc" || imageType == "docx" ) {
                	 var src = url.replace(/^data:application\/[^;]+/, 'data:application/octet-stream');
                     window.open(src);
                 }else {
                	 var src = url.replace(/^data:image\/[^;]+/, 'data:application/octet-stream');
                     window.open(src, '_blank');
                 }
     }
   }
    
    $scope.LifeEngageProduct.Insured.BasicDetails.fullName=$scope.LifeEngageProduct.Insured.BasicDetails.firstName+" "+$scope.LifeEngageProduct.Insured.BasicDetails.lastName;
    
    $scope.LifeEngageProduct.Payer.BasicDetails.fullName=$scope.LifeEngageProduct.Payer.BasicDetails.firstName+" "+$scope.LifeEngageProduct.Payer.BasicDetails.lastName;
};