'use strict';

storeApp.controller('BeneficiaryController', ['$rootScope', '$scope', 'DataService', 
                                              'LookupService', 'DocumentService', 
                                              'ProductService', '$compile', '$routeParams', 
                                              '$location', '$translate', '$debounce', 
                                              'UtilityService', 'PersistenceMapping', 
                                              'AutoSave', 'globalService', 'EappVariables', 
                                              'EappService', '$timeout',
function($rootScope, $scope, DataService, LookupService, 
        DocumentService, ProductService, $compile, $routeParams,
        $location, $translate, $debounce, UtilityService,
        PersistenceMapping, AutoSave, globalService, EappVariables, 
        EappService, $timeout) {
    $scope.mobnumberPattern = rootConfig.mobnumberPattern;
    $scope.namePattern = rootConfig.namePattern;
    $scope.zipPattern = rootConfig.zipPattern;
    $scope.numberPattern = rootConfig.numberPattern;
    $scope.emailPattern = rootConfig.emailPattern;
    $scope.sharePattern = rootConfig.sharePattern;
    $scope.Initialize = function() {
        $scope.LifeEngageProduct = EappVariables.getEappModel();
        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        $scope.showErrorCount = false;
        $scope.addNewBeneficiary = false;
        $scope.addNew = false;
        $scope.editIndex;
        if ($scope.LifeEngageProduct.Beneficiaries.length > 4) {
            $scope.isBeneficiaryGreaterthanThree = true;
        } else {
            $scope.isBeneficiaryGreaterthanThree = false;
        }
		var model = "LifeEngageProduct";
		if ((rootConfig.autoSave.eApp) && EappVariables.EappKeys.Key15 != 'Final') {
			if(AutoSave.destroyWatch){
				AutoSave.destroyWatch();
			}
			AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
		} else if(AutoSave.destroyWatch){
			AutoSave.destroyWatch();
		}

        $scope.addBeneficiary = function() {
            $scope.addNewBeneficiary = true;
            $scope.addNew = true;
            $scope.showErrorCount = true;
            $scope.Beneficiary = {
                "BasicDetails" : {}
            };
            $scope.LifeEngageProduct.Beneficiaries.push($scope.Beneficiary);
            $scope.updateErrorCount($scope.selectedTabId);
            if ($scope.LifeEngageProduct.Beneficiaries.length > 4) {
                $scope.isBeneficiaryGreaterthanThree = true;
            }

        };
        $scope.saveBeneficiary = function() {
            if ($scope.edit == true) {
                $scope.edit = false;
            }
            if ($scope.LifeEngageProduct.Beneficiaries.length > 4) {
                $scope.isBeneficiaryGreaterthanThree = true;
            } else {
                $scope.isBeneficiaryGreaterthanThree = false;
            }

            $scope.addNewBeneficiary = false;
            $scope.addNew = false;
            $scope.Beneficiary = {};
            $scope.showErrorCount = false;
			$scope.showHideAppointee();
            $scope.updateErrorCount($scope.selectedTabId);
            $rootScope.NotifyMessages(false, "beneficiarySaveSuccess", $translate);
        };
        $scope.cancelBeneficiary = function() {
            $scope.Beneficiary = {};
            if ($scope.addNew == true) {
                $scope.LifeEngageProduct.Beneficiaries[$scope.LifeEngageProduct.Beneficiaries.length - 1] = {};
                $scope.LifeEngageProduct.Beneficiaries.splice($scope.LifeEngageProduct.Beneficiaries.length - 1, 1);
            } else {
                $scope.LifeEngageProduct.Beneficiaries[$scope.editIndex] = $scope.BeneficiaryTemp;
            }
            $scope.showHideAppointee();
            $scope.addNewBeneficiary = false;
            $scope.addNew = false;
            $scope.showErrorCount = false;
            $scope.refresh();
        };
        $scope.deleteBeneficiaryObj = function() {
            $scope.editIndex = $scope.index == $scope.editIndex ? {} : $scope.editIndex;
            var i = 0;
            $scope.addNewBeneficiary = false;
            $scope.addNew = false;
            $scope.LifeEngageProduct.Beneficiaries.splice($scope.index, 1);
            if ($scope.LifeEngageProduct.Beneficiaries.length > 4) {
                $scope.isBeneficiaryGreaterthanThree = true;
            } else {
                $scope.isBeneficiaryGreaterthanThree = false;
            }
            
            $scope.showHideAppointee();

            if ($rootScope.showAppointee == false) {
                $scope.LifeEngageProduct.Appointee = {};
            }
            $scope.updateErrorCount($scope.selectedTabId);
        };
        $scope.editBeneficiary = function(index) {
            $scope.addNewBeneficiary = true;
            $scope.addNew = false;
            $scope.edit = true;
            $scope.Beneficiary = {};
            $scope.Beneficiary = $scope.LifeEngageProduct.Beneficiaries[index];
			$scope.Beneficiary.BasicDetails.dob = LEDate($scope.Beneficiary.BasicDetails.dob);
            $scope.BeneficiaryTemp = JSON.parse(JSON.stringify($scope.LifeEngageProduct.Beneficiaries[index]));
            $scope.editIndex = index;
            $scope.showErrorCount = true;
            $scope.updateErrorCount($scope.selectedTabId);
        };
        $scope.IsBeneficiaryPermAddSame = function(id) {
            if ($scope.Beneficiary.ContactDetails.isPermanentAddressSameAsCurrentAddress) {
                $scope.Beneficiary.ContactDetails.permanentAddress = $scope.Beneficiary.CurrentAddress;
            } else {
                $scope.Beneficiary.ContactDetails.permanentAddress = {};
            }
            $scope.updateErrorCount(id);
        };
        $scope.isAgeLessThan18 = function(dob) {
            var age = getPartyAge(dob);
            if (age < 18) {
                return true;
            } else {
                return false;
            }
        };
        $scope.isEmpty = function(obj) {
            return angular.equals({}, obj);
        };
        $scope.deleteValueTable = function(index, deleteHeader) {
            $scope.index = index;
            var message = "Are you sure you want to delete " + deleteHeader;
            // TODO add in resource
            $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), message, translateMessages($translate, "deleteConfirmButtonText1"), $scope.deleteBeneficiaryObj, translateMessages($translate, "cancelButtonText"), $scope.cancel, $scope.cancel);
        };
        $scope.cancel = function() {
        };

		$scope.showHideAppointee();
    }
    $scope.updateErrorCount = function(id) {
        $rootScope.updateErrorCount(id);
        $scope.selectedTabId = id;
        if($scope.Beneficiary){
            var beneficieryDob = $scope.Beneficiary.BasicDetails.dob;
            if (LEDate(beneficieryDob) >= LEDate()) {
                $scope.errorCount = parseInt($scope.errorCount) + 1;
            }
            
            var checkboxes = angular.element("input[name='beneficiaryPermanentMethodOfCommunication']");
            var checkError = 0;

            if (!checkboxes.is(":checked")) {
				checkError = 1;
            }
            $scope.errorCount = $scope.errorCount + parseInt(checkError);
        }
        $scope.showHideAppointee();

    }
    $scope.showErrorPopup = function(id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
        var beneficieryDob = $scope.Beneficiary.BasicDetails.dob;
        if (LEDate(beneficieryDob) >= LEDate()) {
            var error = {};
            error.message = translateMessages($translate, "beneficiaryDOBFutureDateValidationMessage");
            error.key = 'beneficiaryDOB';
            $scope.errorMessages.push(error);
        }
        var checkboxes = angular.element("input[name='beneficiaryPermanentMethodOfCommunication']");
				if (!checkboxes.is(":checked")) {
				    var error = {};
				    error.message = translateMessages($translate,"beneficiaryPermanentMethodOfCommunicationRequiredValidationMessage");
                    error.key = checkboxes[0].id;
                    $scope.errorMessages.push(error);
				}
    }
    $scope.showHideAppointee = function(){
        if ($scope.LifeEngageProduct.Beneficiaries.length == 0) {
                $rootScope.showAppointee = false;
            }else{

            for (var i in $scope.LifeEngageProduct.Beneficiaries) {
                if ($scope.isAgeLessThan18($scope.LifeEngageProduct.Beneficiaries[i].BasicDetails.dob)) {
                    $rootScope.showAppointee = true;
                    return false;
                } else {
                    $rootScope.showAppointee = false;
                }
            }
            }
    }
    if (!((rootConfig.isDeviceMobile))) {
        $('.custdatepicker').datepicker({
            format : 'yyyy-mm-dd',
            autoclose : true
        }).on('change', function() {

            $timeout(function() {
                $scope.updateErrorCount($scope.selectedTabId);
            }, 10);

        });
    }
    $scope.datecheck = function(value) {
        var today = new Date();
        var birthDate = new Date(value);
        var age = today.getFullYear()- birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
                if (m < 0|| (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }
        $scope.dynamicErrorCount = 0;
        $scope.dynamicErrorMessages = [];
        if (age > 0 && age < 18) {
            $scope.showHideAppointee();
        } else {
            $scope.updateErrorCount('BeneficiaryDetailsSubTab');
        }
    };
    
    $scope.ddl = function(x) {
        return resources.mapping[x];
    };
    // BeneficiaryController Load Event and Load Event Handler
   /* $scope.$parent.$on('BeneficiaryDetailsSubTab', function(event, args) {
        $scope.selectedTabId = args[0];
        $scope.Initialize();
    });*/
}]);
