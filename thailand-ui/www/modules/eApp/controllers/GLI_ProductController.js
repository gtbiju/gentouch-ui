'use strict';
/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:GLI_ProductController
 CreatedDate:11/11/2015
 Description:GLI_ProductController 
 */

storeApp.controller('GLI_ProductController', GLI_ProductController);
GLI_ProductController.$inject = ['$controller', '$rootScope', '$scope', 'DataService', 'GLI_DataService', 'LookupService', 'DocumentService', 'ProductService', '$compile', '$routeParams', '$location', '$translate', '$debounce', 'UtilityService', 'PersistenceMapping', 'AutoSave', 'globalService', 'EappVariables', 'EappService', '$timeout'];

function GLI_ProductController($controller, $rootScope, $scope, DataService, GLI_DataService, LookupService, DocumentService, ProductService, $compile, $routeParams, $location, $translate, $debounce, UtilityService, PersistenceMapping, AutoSave, globalService, EappVariables, EappService, $timeout) {
    $controller('ProductController', {
        $rootScope: $rootScope,
        $scope: $scope,
        DataService: DataService,
        GLI_DataService: GLI_DataService,
        LookupService: LookupService,
        DocumentService: DocumentService,
        ProductService: ProductService,
        $compile: $compile,
        $routeParams: $routeParams,
        $location: $location,
        $translate: $translate,
        $debounce: $debounce,
        UtilityService: UtilityService,
        PersistenceMapping: PersistenceMapping,
        AutoSave: AutoSave,
        globalService: globalService,
        EappVariables: EappVariables,
        EappService: EappService,
        $timeout: $timeout
    });
    $scope.eAppParentobj.errorCount = 0;
    $scope.eAppParentobj.CurrentPage = "";
    $scope.totalFundOption = "100";
    $scope.showErrorCount = true;		
    $scope.numberPattern = rootConfig.numberPattern;
	
	 $scope.LifeEngageProduct.Product["riderValue"]=false;	
    //to translte rider isnured type
    $scope.translate = function (riderType, ubrichRiderTypeCheck) {
        if ($scope.LifeEngageProduct.Product.policyDetails.bpmCode == "UBR") {
            if (ubrichRiderTypeCheck && ubrichRiderTypeCheck != '') {
                return $translate.instant("eapp." + ubrichRiderTypeCheck);
            }
        } else {
            return $translate.instant("eapp." + riderType);
        }
    };

    function filterRidersArray(value, successcallback) {
		
        var arryObj = [];
        if ($scope.LifeEngageProduct.Product.RiderDetails.length > 0) {
            var data = $scope.LifeEngageProduct.Product.RiderDetails;
             for (i = 0; i < $scope.LifeEngageProduct.Product.RiderDetails.length; i++) {
                var arryObj = {
                    "insured": "",
                    "riderCode": "",
                    "sumInsured": "",
                    "policyTerm": "",
                    "premiumTerm": "",
                    "riderPremium": "",
                    "initialPremium": ""
                   }
                if ($scope.LifeEngageProduct.Product.RiderDetails[i].insured == 'Maininsured') {
                    arryObj.insured = $scope.LifeEngageProduct.Insured.BasicDetails.fullName;
                    arryObj.planName = $scope.LifeEngageProduct.Product.RiderDetails[i].riderCode;
                   arryObj.riderCode = $scope.LifeEngageProduct.Product.RiderDetails[i].riderCode;
				   if(arryObj.riderCode=="PB")
				   {					   
					   $scope.LifeEngageProduct.Product["riderValue"]=true;				   
				   }
                   if(arryObj.riderCode=="DD")	
                   {                       
                        $scope.LifeEngageProduct.Product.policyDetails.hasDDRider=true;
                   }	   
					   
                     arryObj.riderPremium = $scope.LifeEngageProduct.Product.RiderDetails[i].riderPremium;
                    arryObj.sumInsured = $scope.LifeEngageProduct.Product.RiderDetails[i].sumInsured;
                    if ($scope.LifeEngageProduct.Product.RiderDetails[i].planName === rootConfig.VGHRidePlanName) {
                        arryObj.policyTerm = "";
                        arryObj.premiumTerm = "";
                        arryObj.riderPlanName = $scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName + "eApp";
                    } else {
                        arryObj.policyTerm = $scope.LifeEngageProduct.Product.RiderDetails[i].policyTerm;
                        arryObj.premiumTerm = $scope.LifeEngageProduct.Product.RiderDetails[i].policyTerm;
                    }
                    if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName === "OutPatient") {
                        arryObj.initialPremium = "";
                        arryObj.planName = "VGHRiderOutPatient";
                    } else if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName === "Dental") {
                        arryObj.initialPremium = "";
                        arryObj.planName = "VGHRiderDental";
                    } else {
                        arryObj.initialPremium = $scope.LifeEngageProduct.Product.RiderDetails[i].initialPremium;
                    }
                    
                    if($scope.LifeEngageProduct.Product.RiderDetails[i].riderCode === 'DD_2551'){
			arryObj.planName = 'DD';
		}
                    
                    $scope.$parent.modifiedRidersList.push(arryObj);
                    
                     }
            }
            sortRiderList($scope.$parent.modifiedRidersList);
		} else {
            successcallback();
        }
    };
    //sort the RiderDetails in order
        function sortRiderList(combinedRiderData) {
        var eAppSortedProductsArray = [];
        for (var i = 0; i < rootConfig.eAppProductsRiders.length; i++) {
            for (var j = 0; j < combinedRiderData.length; j++) {
                if (combinedRiderData[j].planName === rootConfig.eAppProductsRiders[i]) {
                        if($scope.LifeEngageProduct.Product.ProductDetails.productId == '1010' && combinedRiderData[j].riderCode == 'HS Extra'){
                        var ridername = combinedRiderData[j];
                        var product=$scope.LifeEngageProduct.Product.RiderDetails.planCode;
                    }else{
                        eAppSortedProductsArray.push(combinedRiderData[j]);
                    }
                }
                
            }
        }
        if($scope.LifeEngageProduct.Product.ProductDetails.productId=="1010"){
            eAppSortedProductsArray.splice(2, 0, ridername);
        }
        
        $scope.$parent.modifiedRidersList = eAppSortedProductsArray;
    };
    $scope.Initialize = function () {
      
        $scope.riderdetailstable = true;
        if ($scope.LifeEngageProduct.Product.RiderDetails.length == 0){
        	$scope.riderdetailstable = false;
        }
        
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_Payment#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_Payment#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step2Payment#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step2Payment#tabDetails_1');
        } 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_BeneficiarySubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_BeneficiarySubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_financilaDetailsSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_financilaDetailsSubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_mainInsuredSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_mainInsuredSubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PolicyHolderSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PolicyHolderSubTab#tabDetails_1');
        }		
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6');
		}	
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
		}
		
        $scope.$parent.disableProceedButtonCommom = false;
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
        }
        $scope.$parent.modifiedRidersList = [];
        $scope.LifeEngageProduct = EappVariables.getEappModel();
        
//        if($scope.LifeEngageProduct.Payment.RenewalPayment.paymentFrequency=="Annual"){
//            $scope.LifeEngageProduct.Payment.RenewalPayment.paymentFrequency=translateMessages($translate,"illustrator.premiumFrequencyOption4");
//           
//        }
//        
//        if($scope.LifeEngageProduct.Payment.RenewalPayment.paymentFrequency=="Semi Annual"){
//            $scope.LifeEngageProduct.Payment.RenewalPayment.paymentFrequency=translateMessages($translate,"illustrator.premiumFrequencyOption3");
//           
//        }
        
        
        
        
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "3"){
        	$scope.productName = translateMessages($translate,"illustrator.genBumnan8Product");
        }
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "10"){
            $scope.productName = translateMessages($translate,"eapp_vt.GenCompleteHealth");
        }
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "4" && $scope.LifeEngageProduct.Product.policyDetails.premiumPeriod == "20"){
            $scope.productName = translateMessages($translate,"illustrator.genProLife20Product");
        }
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "4" && $scope.LifeEngageProduct.Product.policyDetails.premiumPeriod == "25"){
            $scope.productName = translateMessages($translate,"illustrator.genProLife25Product");
        }
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "6"){
            $scope.productName = translateMessages($translate,"GenSave20Plus");
        }
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "97"){
            $scope.productName = translateMessages($translate,"GenCancerSuperProtection");
        }
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "220"){
            $scope.productName = translateMessages($translate,"illustrator.Wholelife");
        }
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "306"){
            $scope.productName = translateMessages($translate,"GenSave10Plus");
        }
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "274"){
            $scope.productName = translateMessages($translate,"GenSave4Plus");
        }
        $scope.translateModeOfPayment($scope.LifeEngageProduct.Payment.RenewalPayment.paymentFrequency);
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == '220' && $scope.LifeEngageProduct.Product.ProductDetails.productId == '1220'){
        	$scope.productPremium = $scope.LifeEngageProduct.Product.premiumSummary.discountedPremium;
        } else {
        	$scope.productPremium = $scope.LifeEngageProduct.Product.policyDetails.premium;
        }
        $scope.eAppParentobj.PreviousPage = "";
        $timeout(function () {
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission") {
                UtilityService.disableAllActionElements();
            }else if (typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) {
                UtilityService.disableAllActionElements();
            }
        }, 0);
        if ($scope.LifeEngageProduct.Proposer.Declaration && $scope.LifeEngageProduct.Proposer.Declaration.dateandTime) {
            $scope.proposalDate = LEDate($scope.LifeEngageProduct.Proposer.Declaration.dateandTime);
        } else {
            $scope.proposalDate = "";
        }
        $scope.spajNumber = EappVariables.EappKeys.Key21;
        if (EappVariables.EappKeys.Key15 == "Confirmed" && $scope.$parent.LifeEngageProduct.Declaration.confirmSignature == true) {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = true;
        } else {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
        }
        //For disabling and enabling the tabs.And also to know the last visited page.---starts
        $rootScope.selectedPage = "ProductDetails";
        $scope.LifeEngageProduct.LastVisitedUrl = "2,0,'ProductDetails',false,'ProductDetails',''";
        $scope.eAppParentobj.nextPage = "3,6,'eappshortquestionnaire',true,'eappshortquestionnaire',''";
       // $scope.eAppParentobj.nextPage = "3,1,'MainInsuredQuestionnaire',true,'HealthDetailsSubTab','eAppQuestionnaire'";
       
        //$scope.eAppParentobj.nextPage = "5,0,'Payment',true,'Payment',''";

        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[0] <= $scope.LifeEngageProduct.LastVisitedUrl[0]) {
                $scope.LifeEngageProduct.LastVisitedIndex = "2,0";
            }
        }

        
        // For filtering RidersList - - List all rider taken for each Insured
        $timeout(function () {
            if ($scope.LifeEngageProduct.Product.RiderDetails.length > 0) {
                $scope.$parent.modifiedRidersList = [];
                filterRidersArray("Maininsured", function () {});
            }
        }, 0);
        //For disabling and enabling the tabs.And also to know the last visited page.---ends
        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        $scope.updateErrorCount($scope.selectedTabId);
        var model = "LifeEngageProduct";
        if ((rootConfig.autoSave.eApp) && (typeof EappVariables.EappKeys.Key15 == "undefined" || EappVariables.EappKeys.Key15 == "New" || EappVariables.EappKeys.Key15 == "Confirmed")) {
            if (AutoSave.destroyWatch) {
                AutoSave.destroyWatch();
            }
            AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
        } else if (AutoSave.destroyWatch) {
            AutoSave.destroyWatch();
        }
        $scope.addFundInformation = function () {
            var fundInformation = new Object();
            fundInformation.FundOrPortfolioName = "";
            fundInformation.AllocationPercentage = "";
            fundInformation.BonusOrDividend = "";
            fundInformation.PayoutOrSettlement = "";
            $scope.LifeEngageProduct.Product.FundInformation.push(fundInformation);

        };
        $scope.deleteFundInformation = function () {
            var assestRemoved = $scope.LifeEngageProduct.Product.FundInformation[$scope.index].id;
            $scope.LifeEngageProduct.Product.FundInformation.splice($scope.index, 1);
            $scope.updateErrorCount($scope.selectedTabId);
        };
        $scope.deleteValueTable = function (index, deleteHeader) {
            $scope.index = index;
            var message = "Are you sure you want to delete " + deleteHeader;
            //TODO add in resource
            $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), message, translateMessages($translate, "deleteConfirmButtonText"), $scope.deleteFundInformation, translateMessages($translate, "cancelButtonText"), $scope.cancel, $scope.cancel);
        };
        // For enabling ARMS section
        $scope.displayARMSSection = false;
        /*if($scope.LifeEngageProduct.Product.Risk.autoBalancing != 'undefined' && $scope.LifeEngageProduct.Product.Risk.autoTrading != 'undefined' ){	
        	if($scope.LifeEngageProduct.Product.Risk.autoBalancing == "Yes" || $scope.LifeEngageProduct.Product.Risk.autoTrading == "Yes"){
        		$scope.displayARMSSection = true;
        	}
        }*/
        $scope.cancel = function () {};
        // temp array for dispay product details in table
        var prodArray = {
            "insured": "",
            "riderPlanName": "",
            "sumInsured": "",
            "policyTerm": "",
            "premiumTerm": "",
            "initialPremium": ""
        };
        
        prodArray.insured = $scope.LifeEngageProduct.Insured.BasicDetails.fullName;
        prodArray.planName = $scope.LifeEngageProduct.Product.ProductDetails.productName;
        prodArray.riderPlanName = $scope.LifeEngageProduct.Product.ProductDetails.productName;
        prodArray.sumInsured = $scope.LifeEngageProduct.Product.policyDetails.sumInsured;
        prodArray.policyTerm = $scope.LifeEngageProduct.Product.ProductDetails.policyTerm;
        prodArray.premiumTerm = $scope.LifeEngageProduct.Product.ProductDetails.premiumTerm;
        prodArray.initialPremium = $scope.LifeEngageProduct.Product.ProductDetails.initialPremium;
       // $scope.$parent.modifiedRidersList.push(prodArray);
        $scope.LifeEngageProduct.Product.ProductDetails.totalInitialPremium = $scope.LifeEngageProduct.Product.ProductDetails.totalInitialPremium;
        if ($scope.LifeEngageProduct.Product.RiderDetails.length == 0) {
            //calcTotalPrem();
        }
        $scope.eAppParentobj.errorCount = $scope.errorCount;
    }
    /*function calcTotalPrem() {
        $scope.LifeEngageProduct.Product.ProductDetails.totalInitialPremium = 0;
        if ($scope.$parent.modifiedRidersList.length > 0) {
            for (i = 0; i < $scope.$parent.modifiedRidersList.length; i++) {
                $scope.LifeEngageProduct.Product.ProductDetails.totalInitialPremium += $scope.$parent.modifiedRidersList[i].initialPremium;
                $scope.refresh();
            }
        }
    }*/
    $scope.onRetrieveAgentProfileError = function () {
        $rootScope.lePopupCtrl.showError(translateMessages($translate,
            "lifeEngage"), translateMessages($translate,
            "general.agentProfileRetrieveError"), translateMessages($translate,
            "fna.ok"), $scope.okClick);
        $rootScope.showHideLoadingImage(false, "");
    };
    $scope.okClick = function () {};
    $scope.updateErrorCount = function (id) {
        $rootScope.updateErrorCount(id);
        $scope.selectedTabId = id;
        //TODO refactor the code ; use uijson and handle bar for validation
        /*for (var i = 0; i < $scope.LifeEngageProduct.Product.FundInformation.length; i++) {
        	var total = 0;
        	var fundInformation = $scope.LifeEngageProduct.Product.FundInformation[i];
        	if (fundInformation.AllocationPercentage) {
        		total = parseInt(total) + parseInt(fundInformation.AllocationPercentage);
        		if ((fundInformation.AllocationPercentage).search($scope.numberPattern) == -1 || (total > 100 || total < 0)) {
        		$scope.errorCount = $scope.errorCount + 1;
        		}
        	}
        	if (!fundInformation.FundOrPortfolioName) {
        		$scope.errorCount = $scope.errorCount + 1;
        	}
        }*/
        $scope.eAppParentobj.errorCount = $scope.errorCount;
        $scope.refresh();
    }
    $scope.showErrorPopup = function (id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
        //TODO refactor the code ; use uijson and handle bar for validation
        for (var i = 0; i < $scope.LifeEngageProduct.Product.FundInformation.length; i++) {
            var total = 0;
            var fundInformation = $scope.LifeEngageProduct.Product.FundInformation[i];
            if (fundInformation.AllocationPercentage) {
                total = parseInt(total) + parseInt(fundInformation.AllocationPercentage);
                if ((fundInformation.AllocationPercentage).search($scope.numberPattern) == -1 || (total > 100 || total < 0)) {
                    var error = {};
                    error.message = translateMessages($translate, "funcdAllocationRequiredValidationMessage");
                    error.key = "fundInfoAllocation" + i;
                    $scope.errorMessages.push(error);
                }
            }
            if (!fundInformation.FundOrPortfolioName) {
                var error = {};
                error.message = translateMessages($translate, "fundNameRequiredValidationMessage");
                error.key = "sampleFund" + i;
                $scope.errorMessages.push(error);

            }
        }
    };
    //ProductController Load Event and Load Event Handler
    $scope.$parent.$on('ProductDetails', function (event, args) {
        $scope.selectedTabId = args[0];
        $scope.Initialize();
        $rootScope.validationBeforesave = function (value, successcallback) {
            successcallback();
        };
    });
    $scope.$on("$destroy", function () {
        if (this != null && typeof this !== 'undefined') {
            if (this.$$destroyed) return;
            while (this.$$childHead) {
                this.$$childHead.$destroy();
            }
            if (this.$broadcast) {
                this.$broadcast('$destroy');
            }
            this.$$destroyed = true;
            $timeout.cancel(timer);
        }
    });
    $rootScope.validationBeforesave = function (value, successcallback) {
        successcallback();
    };
    
    $scope.translateModeOfPayment=function(value){
    	$scope.modePayment = "";
        if(value==='Annual'){
            //return translateMessages($translate, "Annual"); 
            $scope.modePayment = translateMessages($translate, "Annual"); 
        } else if(value==='SemiAnnual'){
        	$scope.modePayment = translateMessages($translate, "Semi Annual"); 
            //return translateMessages($translate, "Semi Annual"); 
        } else {
        	$scope.modePayment = translateMessages($translate, value);
            //return translateMessages($translate, value);
        }
        return $scope.modePayment;
    }
    
} // Contoller ends here