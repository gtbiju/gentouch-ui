/*
 *Copyright 2015, LifeEngage 
 */
/*Name:SummaryController
 * For implementing GLI specific changes
 */
storeApp.controller('GLI_SummarySPAJController', GLI_SummarySPAJController);
GLI_SummarySPAJController.$inject = ['$timeout', '$route', 'globalService', '$rootScope', '$scope', '$location', '$compile', '$routeParams', 'DataService', 'RuleService', 'LookupService', 'DocumentService', 'FnaVariables', 'GLI_FnaService', '$translate', 'UtilityService', '$debounce', 'AutoSave', '$controller', 'EappService', 'EappVariables', 'UserDetailsService', 'PersistenceMapping', 'GLI_DataService', 'GLI_EappService', 'GLI_RuleService', 'GLI_EappVariables', '$filter'];

function GLI_SummarySPAJController($timeout, $route, globalService, $rootScope, $scope, $location, $compile, $routeParams, DataService, RuleService, LookupService, DocumentService, FnaVariables, GLI_FnaService, $translate, UtilityService, $debounce, AutoSave, $controller, EappService, EappVariables, UserDetailsService, PersistenceMapping, GLI_DataService, GLI_EappService, GLI_RuleService, GLI_EappVariables, $filter) {
    $controller('SummaryController', {
        $timeout: $timeout,
        $route: $route,
        globalService: globalService,
        $rootScope: $rootScope,
        $scope: $scope,
        $location: $location,
        $compile: $compile,
        $routeParams: $routeParams,
        DataService: DataService,
        RuleService: RuleService,
        LookupService: LookupService,
        DocumentService: DocumentService,
        FnaVariables: FnaVariables,
        FnaService: GLI_FnaService,
        $translate: $translate,
        UtilityService: UtilityService,
        $debounce: $debounce,
        AutoSave: AutoSave
    });

    /*var unload=$scope.$parent.$on('SPAJsummary_unload', function(event) {
        	var data=$scope.$parent;
            angular.element( document.querySelector( '#SPAJsummary' ) ).empty();
            $scope.$destroy();
            $scope.$parent=data;
            load();
            unload(); 
		});*/

    $scope.appDateTimeFormat = rootConfig.appDateTimeFormat;
    var signature = "Signature";
    var outputFormat = "image/png";
    $scope.isSPAJNoAllocated = false;
    $scope.appDateFormat = rootConfig.appDateFormat;
    $scope.showErrorCount = true;
    $scope.isDeviceWeb = true;
    $scope.isRDSUser = UserDetailsService.getRDSUser();
    $scope.setSaveproposalDisableFlag = true;
    $scope.relationWithInsuredSummary = [];
    if (!(rootConfig.isDeviceMobile) && (typeof $scope.DocumentType == "undefined")) {
        $scope.DocumentType = "Signature#SPAJSignature";
    }

        $( ".product-summary a" ).click(function() {
        $("#RiderDetailsSummary").toggleClass( "main" );
        });

    $scope.spajNumber = EappVariables.EappKeys.Key21;
    if (!$scope.spajNumber) {
        $scope.spajNumber = "******";
    } else if (EappVariables.EappKeys.Key15 && EappVariables.EappKeys.Key15 == "Confirmed") {
        $scope.isSPAJNoAllocated = true;
    }
    if ((rootConfig.isDeviceMobile)) {
        //disabling canvas based on the declaration check
        $scope.spajSummaryCanvas = true;

        //disabling summary page buttons based on the signature check
        $scope.spajSummaryButtonDisable = false;
    } else {
        $scope.spajSummaryCanvas = false;

        $scope.spajSummaryButtonDisable = false;
    }
    $rootScope.selectedPage = "Summary";
    var signaturePadSpaj;
    //to translte rider isnured type
    $scope.translate = function (riderType, ubrichRiderTypeCheck) {
        if ($scope.LifeEngageProduct.Product.policyDetails.bpmCode == "UBR") {
            if (ubrichRiderTypeCheck && ubrichRiderTypeCheck != '') {
                return $translate.instant("eapp." + ubrichRiderTypeCheck);
            }
        } else {
            return $translate.instant("eapp." + riderType);
        }

    }
    /**
     * to translate client declarations
     */

    /* $scope.translateBasedOnProduct = function (textModel) {
        if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == 20 || $scope.LifeEngageProduct.Product.ProductDetails.productCode == 9 || $scope.LifeEngageProduct.Product.ProductDetails.productCode == 4) {
            return $translate.instant(textModel + "sameProducts");
        } else if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == 1 || $scope.LifeEngageProduct.Product.ProductDetails.productCode == 5) {
            return $translate.instant(textModel + $scope.LifeEngageProduct.Product.ProductDetails.productCode);
        }
    }

    $scope.translateSubDescriptionSummary = function (translatetext) {
        if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == 20 || $scope.LifeEngageProduct.Product.ProductDetails.productCode == 9 || $scope.LifeEngageProduct.Product.ProductDetails.productCode == 4) {
            return $translate.instant(translatetext + "sameProducts");
        } else {
            return $translate.instant(translatetext + $scope.LifeEngageProduct.Product.ProductDetails.productCode);
        }
    }
*/
    /**
     * to translate client declarations end
     */
    $scope.Initialize = function () {
        $scope.$parent.disableProceedButtonCommom = false;
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            //EappVariables.setEappModel($scope.LifeEngageProduct);
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
        }
        
        if(EappVariables.EappKeys.Key15 == 'Submitted' || (typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null)){
                $("#SPAJsummary .Summary-Edit").addClass("displayNone");
        }else{
            $("#SPAJsummary .Summary-Edit").removeClass("displayNone");
        }
        $(window).scrollTop(0);
        $scope.summaryProductName();
        
        $scope.BeneficiaryIdentityType = [];
        $scope.additionalInsuredGender = [];
        $scope.$parent.modifiedRidersList = [];
        $scope.LifeEngageProduct = EappVariables.getEappModel();
        $scope.spajNumber = EappVariables.EappKeys.Key21;
        //$scope.eAppParentobj.PreviousPage   = "SPAJsummary_unload";
        $scope.eAppParentobj.PreviousPage = "";
        if ($scope.LifeEngageProduct.Declaration.spajSignDate && $scope.LifeEngageProduct.Declaration.spajSignDate != "") {
            if (!$scope.isRDSUser) {
                $scope.spajSignDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.spajSignDate), $scope.appDateTimeFormat);
            } else {
                $scope.spajSignDate = $scope.LifeEngageProduct.Declaration.spajSignDate;
            }
        }




 



        // angular.forEach($scope.LifeEngageProduct.Insured.BasicDetails, function (value, Key) {

        //     if($scope.LifeEngageProduct.Insured.BasicDetails.[Key] == "" || $scope.LifeEngageProduct.Insured.BasicDetails.[Key] == undefined){
        //         $scope.LifeEngageProduct.Insured.BasicDetails.[Key] = '-';
        //     }

        // })


        //Display date in dd/mm/yyyy format
        $scope.formatDate=function(dob){
            return getFormattedDateDDMMYYYYThai(dob);
        }
        
        //Translate Relationship in Beneficiary
        $scope.translateRelationship=function(relation){
            return translateMessages($translate,"eapp_vt.relationship"+relation);
        }
        
        //Converting Occupation code to values        
         for (var i = 0; i < $rootScope.natureOfWork.length; i++) {
             if($scope.LifeEngageProduct.Insured.OccupationDetails!==undefined){
                 if($scope.LifeEngageProduct.Insured.OccupationDetails.length>1){
                     if($scope.LifeEngageProduct.Insured.OccupationDetails[1].natureofWork===$rootScope.natureOfWork[i].code){
                        $scope.natureOfWorkValueInsured2=$rootScope.natureOfWork[i].value;
                     }
                 }  
                 if($scope.LifeEngageProduct.Insured.OccupationDetails[0].natureofWork===$rootScope.natureOfWork[i].code){
                        $scope.natureOfWorkValueInsured1=$rootScope.natureOfWork[i].value;
                 }
             } 
             
             if($scope.LifeEngageProduct.Payer.OccupationDetails!==undefined){
                 if($scope.LifeEngageProduct.Payer.OccupationDetails.length>1){
                     if($scope.LifeEngageProduct.Payer.OccupationDetails[1].natureofWork===$rootScope.natureOfWork[i].code){
                        $scope.natureOfWorkValuePayer2=$rootScope.natureOfWork[i].value;
                     }
                 }else{
                     $scope.natureOfWorkValuePayer2="";
                 }  
                 if($scope.LifeEngageProduct.Payer.OccupationDetails[0].natureofWork===$rootScope.natureOfWork[i].code){
                        $scope.natureOfWorkValuePayer1=$rootScope.natureOfWork[i].value;
                 }
             }
         }
        
        //Converting Identity type code to values
        for (var i = 0; i < $rootScope.identityTypeApp.length; i++) {            
            if($scope.LifeEngageProduct.Insured.BasicDetails.identityProof===$rootScope.identityTypeApp[i].code){
                $scope.LifeEngageProduct.Insured.BasicDetails.identityProofValue=$rootScope.identityTypeApp[i].value;
            }
            if($scope.LifeEngageProduct.Payer.BasicDetails.identityProof===$rootScope.identityTypeApp[i].code){
                $scope.LifeEngageProduct.Payer.BasicDetails.identityProofValue=$rootScope.identityTypeApp[i].value;
            }
        }
        
        //Converting Bank name code to values
        for (var i = 0; i < $rootScope.bankname.length; i++) {            
            if($scope.LifeEngageProduct.Payment.RenewalPayment.bankName===$rootScope.bankname[i].code){
                $scope.LifeEngageProduct.Payment.RenewalPayment.bankNameValue=$rootScope.bankname[i].value;
            }
            if($scope.LifeEngageProduct.Payment.RefundPayment == undefined){
                $scope.LifeEngageProduct.Payment.RefundPayment = {};
            }
            if($scope.LifeEngageProduct.Payment.RefundPayment.bankName===$rootScope.bankname[i].code){
                $scope.LifeEngageProduct.Payment.RefundPayment.bankNameValue=$rootScope.bankname[i].value;
            }
        }  
        
        
        if($scope.LifeEngageProduct.Payment.RefundPayment.bankName==="" || $scope.LifeEngageProduct.Payment.RefundPayment.bankName===undefined){
            $scope.LifeEngageProduct.Payment.RefundPayment.bankNameValue="";
        }
        
        if($scope.LifeEngageProduct.Payment.RenewalPayment.bankName==="" || $scope.LifeEngageProduct.Payment.RenewalPayment.bankName===undefined){
            $scope.LifeEngageProduct.Payment.RenewalPayment.bankNameValue="";
        }

        //Converting Nationality code to type
        for (var i = 0; i < $rootScope.nationalityLsit.length; i++) {
            if ($scope.LifeEngageProduct.Insured.BasicDetails.nationality === $rootScope.nationalityLsit[i].code) {
                $scope.LifeEngageProduct.Insured.BasicDetails.nationalityValue = $rootScope.nationalityLsit[i].value;
            }
            if ($scope.LifeEngageProduct.Payer.BasicDetails.nationality === $rootScope.nationalityLsit[i].code) {
                $scope.LifeEngageProduct.Payer.BasicDetails.nationalityValue = $rootScope.nationalityLsit[i].value;
            }
        }
        
        //Converting Marital Status code to type
        for (var i = 0; i < $rootScope.maritalStatus.length; i++) {
            if ($scope.LifeEngageProduct.Insured.BasicDetails.maritalStatus === $rootScope.maritalStatus[i].code) {
                $scope.LifeEngageProduct.Insured.BasicDetails.maritalStatusValue = $rootScope.maritalStatus[i].value;
            }
            if ($scope.LifeEngageProduct.Payer.BasicDetails.maritalStatus === $rootScope.maritalStatus[i].code) {
                $scope.LifeEngageProduct.Payer.BasicDetails.maritalStatusValue = $rootScope.maritalStatus[i].value;
            }
        }
        
        //Converting Title code to type
        for (var i = 0; i < $rootScope.title.length; i++) {
            if ($scope.LifeEngageProduct.Insured.BasicDetails.title === $rootScope.title[i].code) {
                $scope.LifeEngageProduct.Insured.BasicDetails.titleValue = $rootScope.title[i].value;
            }
            if ($scope.LifeEngageProduct.Payer.BasicDetails.title === $rootScope.title[i].code) {
                $scope.LifeEngageProduct.Payer.BasicDetails.titleValue = $rootScope.title[i].value;
            }
        }
        
        //Converting Relationship code to type
         for (var i = 0; i < $rootScope.relationship.length; i++) {
             if($scope.LifeEngageProduct.Insured.CustomerRelationship!==undefined){
                if ($scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer === $rootScope.relationship[i].code) {
                    $scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposerValue = $rootScope.relationship[i].value;
                }
             }
 //            if ($scope.LifeEngageProduct.Payer.CustomerRelationship.relationshipWithProposer === $rootScope.relationshipWithPayer[i].code) {
 //                $scope.LifeEngageProduct.Payer.CustomerRelationship.relationshipWithProposerValue = $rootScope.relationshipWithPayer=[i].value;
 //            }
         }
        
        if($scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer==='Yes'){
            $scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer="";            
        }
        
        //Converting Convenient contact address & Copy from code to type
        for (var i = 0; i < $rootScope.convenientContact.length; i++) {
            if ($scope.LifeEngageProduct.Insured.ContactDetails.convenientContact === $rootScope.convenientContact[i].code) {
                $scope.LifeEngageProduct.Insured.ContactDetails.convenientContactValue = $rootScope.convenientContact[i].value;
            }
            if ($scope.LifeEngageProduct.Payer.ContactDetails.convenientContact === $rootScope.convenientContact[i].code) {
                $scope.LifeEngageProduct.Payer.ContactDetails.convenientContactValue = $rootScope.convenientContact[i].value;
            }
            
            if ($scope.LifeEngageProduct.Insured.ContactDetails.isCopyFrom === $rootScope.convenientContact[i].code) {
                $scope.LifeEngageProduct.Insured.ContactDetails.isCopyFromValue = $rootScope.convenientContact[i].value;
            }
            if ($scope.LifeEngageProduct.Payer.ContactDetails.isCopyFrom === $rootScope.convenientContact[i].code) {
                $scope.LifeEngageProduct.Payer.ContactDetails.isCopyFromValue = $rootScope.convenientContact[i].value;
            }            
        }       
        
        if ($scope.LifeEngageProduct.Payer.ContactDetails.isCopyFrom==="" || $scope.LifeEngageProduct.Payer.ContactDetails.isCopyFrom===undefined){
            $scope.LifeEngageProduct.Payer.ContactDetails.isCopyFrom="";
        }
        
        if ($scope.LifeEngageProduct.Insured.ContactDetails.isCopyFrom==="" || $scope.LifeEngageProduct.Insured.ContactDetails.isCopyFrom===undefined){
            $scope.LifeEngageProduct.Insured.ContactDetails.isCopyFrom="";
        }
        
        //Populating value of beneficiary relationship in summary
        for (var i = 0; i < $scope.LifeEngageProduct.Beneficiaries.length; i++) {
            if ($scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.relationWithInsured == 'Others') {
                $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.relationWithInsuredSPAJ = $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.relationWithInsuredOthers;
            } else {
                $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.relationWithInsuredSPAJ = $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.relationWithInsured;
            }
        }
        
        $scope.translateMapping=function(value){
            return translateMessages($translate, value); 
        }
        
        $scope.translateModeOfPayment=function(value){
            if(value==='Annual'){
                return translateMessages($translate, "Annual"); 
            }else if(value==='SemiAnnual'){
                return translateMessages($translate, "Semi Annual"); 
            }else{
                return translateMessages($translate, value);
            }
        }
        
        $scope.translatePaymentType=function(value){
            if(value==='Transfer/VirtualAccount'){
                return translateMessages($translate, "eapp_vt.cashChequePayment");
            }else if(value==='DebitAccount'){
                return translateMessages($translate, "eapp_vt.debitCardPayment");
            }else if(value==='CreditCard'){
                return translateMessages($translate, "eapp_vt.creditCardPayment");
            }
        }
        
        $scope.translateRefundPayment=function(value){
            if(value==='Cash'){
                return translateMessages($translate, "eapp_vt.cashChequePayment");
            }else if(value==='DebitAccount'){
                return translateMessages($translate, "eapp_vt.transferToBank");
            }
        }
        
        if($scope.LifeEngageProduct.Insured.BasicDetails.isValidForLife =='Yes'){
            $scope.LifeEngageProduct.Insured.BasicDetails.ValidForLife = translateMessages($translate, "eapp_vt.validForLife");
        }
        if($scope.LifeEngageProduct.Payer.BasicDetails.isValidForLife =='Yes'){
             $scope.LifeEngageProduct.Payer.BasicDetails.ValidForLife = translateMessages($translate, "eapp_vt.validForLife");
        }
        
         $scope.summaryProductName();
        
        //Populating value of identity type in summary
        if ($scope.LifeEngageProduct.Insured.BasicDetails.nationalIDType == 5) {
            $scope.LifeEngageProduct.Insured.BasicDetails.nationalIDTypeSPAJ = $scope.LifeEngageProduct.Insured.BasicDetails.nationalIDTypeDesc;
        } else {
            $scope.LifeEngageProduct.Insured.BasicDetails.nationalIDTypeSPAJ = $scope.LifeEngageProduct.Insured.BasicDetails.nationalIDTypeValue;
        }


        if ($scope.LifeEngageProduct.Proposer.BasicDetails.nationalIDType == 5) {
            $scope.LifeEngageProduct.Proposer.BasicDetails.nationalIDTypeSPAJ = $scope.LifeEngageProduct.Proposer.BasicDetails.nationalIDTypeDesc;
        } else {
            $scope.LifeEngageProduct.Proposer.BasicDetails.nationalIDTypeSPAJ = $scope.LifeEngageProduct.Proposer.BasicDetails.nationalIDTypeValue;
        }


        if (EappVariables.EappKeys.Key15 == "Confirmed" && $scope.$parent.LifeEngageProduct.Declaration.confirmSignature == true) {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = true;
        } else {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
        }
        if (typeof $scope.DocumentType == "undefined") {
            $scope.DocumentType = "Signature#SPAJSignature";
        }

        //Previous policy History Summary
        $scope.insuredPrevPolicyDetails = [];
        $scope.additionalInsuredOnePrevPolicyDetails = [];
        $scope.additionalInsuredTwoPrevPolicyDetails = [];
        $scope.additionalInsuredThreePrevPolicyDetails = [];
        $scope.additionalInsuredFourPrevPolicyDetails = [];
//        if ($scope.LifeEngageProduct.PreviouspolicyHistory && $scope.LifeEngageProduct.PreviouspolicyHistory.length > 0) {
//            for (i = 0; i < ($scope.LifeEngageProduct.PreviouspolicyHistory).length; i++) {
//
//                if ($scope.LifeEngageProduct.PreviouspolicyHistory[i].partyType == "Primary") {
//
//                    $scope.insuredPrevPolicyDetails.push($scope.LifeEngageProduct.PreviouspolicyHistory[i]);
//                }
//                if ($scope.LifeEngageProduct.PreviouspolicyHistory[i].partyType == "Additional" && $scope.LifeEngageProduct.PreviouspolicyHistory[i].partyId == 1) {
//                    $scope.additionalInsuredOnePrevPolicyDetails.push($scope.LifeEngageProduct.PreviouspolicyHistory[i]);
//                }
//
//                if ($scope.LifeEngageProduct.PreviouspolicyHistory[i].partyType == "Additional" && $scope.LifeEngageProduct.PreviouspolicyHistory[i].partyId == 2) {
//                    $scope.additionalInsuredTwoPrevPolicyDetails.push($scope.LifeEngageProduct.PreviouspolicyHistory[i]);
//                }
//                if ($scope.LifeEngageProduct.PreviouspolicyHistory[i].partyType == "Additional" && $scope.LifeEngageProduct.PreviouspolicyHistory[i].partyId == 3) {
//                    $scope.additionalInsuredThreePrevPolicyDetails.push($scope.LifeEngageProduct.PreviouspolicyHistory[i]);
//                }
//                if ($scope.LifeEngageProduct.PreviouspolicyHistory[i].partyType == "Additional" && $scope.LifeEngageProduct.PreviouspolicyHistory[i].partyId == 4) {
//                    $scope.additionalInsuredFourPrevPolicyDetails.push($scope.LifeEngageProduct.PreviouspolicyHistory[i]);
//                }
//            }
//        }


        //function call to show/hide questiannoare tab
        if (typeof $scope.LifeEngageProduct.showHideQuestaionnaireTab == "undefined") {
            $scope.LifeEngageProduct.showHideQuestaionnaireTab = summayQuestionnaireTabShowHide();
            $scope.$parent.LifeEngageProduct.showHideQuestaionnaireTab = $scope.LifeEngageProduct.showHideQuestaionnaireTab;
        }

        $timeout(function () {
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission") {
                $scope.setSaveproposalDisableFlag = false;
                if (EappVariables.EappKeys.Key15 == "Confirmed" && $scope.$parent.LifeEngageProduct.Declaration.confirmSignature == true) {
                    $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = true;
                } else {
                    $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
                }
                UtilityService.disableAllActionElements();
            }else if (typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) {
                $scope.setSaveproposalDisableFlag = false;
                if (EappVariables.EappKeys.Key15 == "Confirmed" && $scope.$parent.LifeEngageProduct.Declaration.confirmSignature == true) {
                    $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = true;
                } else {
                    $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
                }
                UtilityService.disableAllActionElements();
            }
            
            UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
            $scope.updateErrorCount($scope.selectedTabId);
            $scope.showErrorCount = true;
            $scope.refresh();

 
            if (rootConfig.isDeviceMobile == "false" && $scope.isRDSUser) {
                $scope.isDeviceWeb = false;
            }
        }, 0);

        //sort the RiderDetails in order
        function sortRiderList(combinedRiderData) {
        var eAppSortedProductsArray = [];
        for (var i = 0; i < rootConfig.eAppProductsRiders.length; i++) {
            for (var j = 0; j < combinedRiderData.length; j++) {
                if (combinedRiderData[j].planName === rootConfig.eAppProductsRiders[i]) {
                        if($scope.LifeEngageProduct.Product.ProductDetails.productId == '1010' && combinedRiderData[j].riderCode == 'HS Extra'){
                        var ridername = combinedRiderData[j];
                        var product=$scope.LifeEngageProduct.Product.RiderDetails.planCode;
                    }else{
                        eAppSortedProductsArray.push(combinedRiderData[j]);
                    }
                }
                
            }
        }
        if($scope.LifeEngageProduct.Product.ProductDetails.productId=="1010"){
            eAppSortedProductsArray.splice(3, 0, ridername);
        }

         $scope.$parent.modifiedRidersList = eAppSortedProductsArray;
                    
    };

        // setTimeout(function () {
        //     $scope.updateErrorCount($scope.selectedTabId);
        //     $scope.refresh();
        // }, 1600);

        var signatureString;
        $scope.loadSignature = function (base64) {
            if (base64 && base64 != '') {
                signatureString = base64;
            }
            if (angular.element('#spajSummaryCanvas')[0]) {
                signaturePadSpaj = new SignaturePad(angular.element('#spajSummaryCanvas')[0].querySelector("canvas"));
                var dpr = window.devicePixelRatio;
                window.devicePixelRatio = 1;
                if (signaturePadSpaj) {
                    signaturePadSpaj.clear();
                    if (signatureString)
                        signaturePadSpaj.fromDataURL(signatureString);
                } else {
                    if (!(rootConfig.isDeviceMobile) && $scope.signaturePad) {
                        $scope.signaturePad.clear();
                    }
                }
                window.devicePixelRatio = dpr;

                if (signatureString && (rootConfig.isDeviceMobile)) {
                    $scope.spajSummaryButtonDisable = true;
                    $scope.spajSummaryCanvas = true;
                } else {
                    if ($scope.LifeEngageProduct.Declaration.spajSignDate && $scope.LifeEngageProduct.Declaration.spajSignDate != '') {
                        $scope.spajSummaryButtonDisable = false;
                        $scope.spajSummaryCanvas = false;
                    }
                }
                signatureString = '';
                $scope.refresh();
            } else {
                $timeout($scope.loadSignature(), 10);

            }
        }

        // For filtering RidersList - - List all rider taken for each Insured



        //TO DO:Need to change the timeout function -this was done to remove clear of undefined error

        $scope.spajNumber = EappVariables.EappKeys.Key21;
        if (!$scope.spajNumber) {
            $scope.spajNumber = "******";
        } else if (EappVariables.EappKeys.Key15 && EappVariables.EappKeys.Key15 == "Confirmed") {
            $scope.isSPAJNoAllocated = true;
        }
        //For disabling and enabling the tabs.And also to know the last visited page.---starts
        $scope.LifeEngageProduct.LastVisitedUrl = "4,1,'SPAJsummary',true,'SPAJsummary',''";
        if ($scope.LifeEngageProduct.showHideQuestaionnaireTab) {
            $scope.eAppParentobj.nextPage = "4,2,'QuestionnaireSummarySubTab',true,'QuestionnaireSummarySubTab','eAppSummaryQuestionnaire'";
        } else {
            $scope.eAppParentobj.nextPage = "4,3,'DeclarationAndAuthorizationTab',true,'DeclarationAndAuthorizationTab',''";
        }
        $scope.eAppParentobj.CurrentPage = "";
        $scope.DocumentType = "Signature#SPAJSignature";
        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[0] < $scope.LifeEngageProduct.LastVisitedUrl[0]) {
                $scope.LifeEngageProduct.LastVisitedIndex = "4,1";
            }
        }

        //For disabling and enabling the tabs.And also to know the last visited page.---ends

        $scope.agentDetail = UserDetailsService.getUserDetailsModel();
        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        $scope.updateErrorCount($scope.selectedTabId);
        $scope.showErrorCount = true;
        if ($scope.LifeEngageProduct.Declaration.isFinalSubmit) {
            $scope.isSTPSuccess = true;
            $scope.isSTPExecuted = true;
            $scope.STPMessage = translateMessages($translate, "stpRulePassed");
        } else {
            $scope.isSTPSuccess = false;
            $scope.isSTPExecuted = false;
        }
        $scope.proposalId = EappVariables.EappKeys.Key4;

        if ($scope.LifeEngageProduct.Declaration.spajDeclaration != "Yes" && (rootConfig.isDeviceMobile)) {
            $scope.spajSummaryCanvas = true;
        }
        var model = "LifeEngageProduct";
        if ((rootConfig.autoSave.eApp)) {
            if (AutoSave.destroyWatch) {
                AutoSave.destroyWatch();
            }
            AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
        } else if (AutoSave.destroyWatch) {
            AutoSave.destroyWatch();
        }

        // temp array for dispay product details in table
        var prodArray = {
            "insured": "",
            "riderPlanName": "",
            "sumInsured": "",
            "policyTerm": "",
            "premiumTerm": "",
            "initialPremium": ""
        };
        prodArray.insured = $scope.LifeEngageProduct.Insured.BasicDetails.fullName;
        prodArray.planName = $scope.LifeEngageProduct.Product.ProductDetails.productName;
        prodArray.riderPlanName = $scope.LifeEngageProduct.Product.ProductDetails.productName;
        prodArray.sumInsured = $scope.LifeEngageProduct.Product.policyDetails.sumInsured;
        prodArray.policyTerm = $scope.LifeEngageProduct.Product.ProductDetails.policyTerm;
        prodArray.premiumTerm = $scope.LifeEngageProduct.Product.ProductDetails.premiumTerm;
        prodArray.initialPremium = $scope.LifeEngageProduct.Product.ProductDetails.initialPremium;
        //$scope.$parent.modifiedRidersList.push(prodArray);
        $scope.totalInitialPremium = $scope.LifeEngageProduct.Product.ProductDetails.totalInitialPremium;;
        if ($scope.LifeEngageProduct.Product.RiderDetails.length == 0) {
            //calcTotalPrem();
        }

        var arryObj = [];
        if ($scope.LifeEngageProduct.Product.RiderDetails.length > 0) {
            var data = $scope.LifeEngageProduct.Product.RiderDetails;
            for (i = 0; i < $scope.LifeEngageProduct.Product.RiderDetails.length; i++) {
                var arryObj = {
                    "insured": "",
                    "riderPlanName": "",
                    "riderCode": "",
                    "sumInsured": "",
                    "policyTerm": "",
                    "premiumTerm": "",
                    "riderPremium": "",
                    "initialPremium": ""
                }
                if ($scope.LifeEngageProduct.Product.RiderDetails[i].insured == 'Maininsured') {
                    arryObj.insured = $scope.LifeEngageProduct.Insured.BasicDetails.fullName;
                    arryObj.planName = $scope.LifeEngageProduct.Product.RiderDetails[i].riderCode;
                    arryObj.riderCode = $scope.LifeEngageProduct.Product.RiderDetails[i].riderCode;
                    arryObj.riderPlanName = $scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName;
                     arryObj.riderPremium = $scope.LifeEngageProduct.Product.RiderDetails[i].riderPremium;
                    arryObj.sumInsured = $scope.LifeEngageProduct.Product.RiderDetails[i].sumInsured;
                    if ($scope.LifeEngageProduct.Product.RiderDetails[i].planName === rootConfig.VGHRidePlanName) {
                        arryObj.policyTerm = "";
                        arryObj.premiumTerm = "";
                        arryObj.riderPlanName = $scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName + "eApp";
                    } else {
                        arryObj.policyTerm = $scope.LifeEngageProduct.Product.RiderDetails[i].policyTerm;
                        arryObj.premiumTerm = $scope.LifeEngageProduct.Product.RiderDetails[i].policyTerm;
                    }
                    if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName === "OutPatient") {
                        arryObj.initialPremium = "";
                        arryObj.planName = "VGHRiderOutPatient";
                    } else if ($scope.LifeEngageProduct.Product.RiderDetails[i].riderPlanName === "Dental") {
                        arryObj.initialPremium = "";
                        arryObj.planName = "VGHRiderDental";
                    } else {
                        arryObj.initialPremium = $scope.LifeEngageProduct.Product.RiderDetails[i].initialPremium;
                    }
                     if($scope.LifeEngageProduct.Product.RiderDetails[i].riderCode === 'DD_2551'){
			arryObj.planName = 'DD';
		}
                    
                    $scope.$parent.modifiedRidersList.push(arryObj);

                }

            }
            sortRiderList($scope.$parent.modifiedRidersList);
        }

    };

    /*function calcTotalPrem = function () {
        if ($scope.$parent.modifiedRidersList.length > 0) {
            for (i = 0; i < $scope.$parent.modifiedRidersList.length; i++) {
                $scope.totalInitialPremiumSummary = $scope.$parent.modifiedRidersList[i].initialPremium;
                $scope.refresh();
            }
        }
    }*/

    $scope.okClick = function () {};

    //function to hide the risk management section
    $scope.riskManagementAccordion = function () {
        var RiskJsonObj = $scope.LifeEngageProduct.Product.Risk;
        angular.forEach(RiskJsonObj, function (value, Key) {
            if (Key.indexOf("Value") >= 0) {
                if (value.length > 0) {
                    $scope.hideRiskSection = false;
                    return false;
                }
            } else {
                if (value == "Yes") {
                    $scope.hideRiskSection = false;
                    return false;
                }
            }
        })
        if ($scope.hideRiskSection != false) {
            return true;
            $scope.hideRiskSection = true;
        }
    };

    $scope.clearSignature = function (signatureId) {
        $scope.spajSummaryButtonDisable = false;
        if ($scope.LifeEngageProduct.Declaration.spajDeclaration == 'Yes') {
            $scope.spajSummaryCanvas = false;
        }
        if ((rootConfig.isDeviceMobile)) {
            signaturePadSpaj.clear();
        } else {
            if ($scope.signaturePad)
                $scope.signaturePad.clear();
        }

        $scope.refresh();
    };



    $scope.getSignature = function (pageName, successCallback) {
        if ((rootConfig.isDeviceMobile)) {
            EappService.getSignature(EappVariables.EappKeys.TransTrackingID, signature, function (sigdata) {
                EappVariables.Signature = sigdata;
                if (sigdata.length > 0) {
                    for (var i = 0; i < sigdata.length; i++) {
                        if (sigdata[i].documentName.indexOf("Signature") >= 0) {
                            signatureType = sigdata[i].documentName.split("_")[2];
                            if (signatureType.localeCompare(pageName) == 0) {
                                if ($scope.setSaveproposalDisableFlag) {
                                    $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
                                }
                                $scope.$parent.refresh();
                                EappService.convertImgToBase64(sigdata[i].base64string, successCallback, outputFormat);
                            } else {
                                if (i == (sigdata.length - 1)) {
                                    successCallback()
                                }
                            }
                        }
                    }
                    successCallback();
                } else {
                    successCallback();
                }
            });
        } else {
            var requirementObject = CreateRequirementFile();
            requirementObject.RequirementFile.identifier = EappVariables.EappKeys.TransTrackingID;
            for (var i = 0; i < EappVariables.EappModel.Requirements.length; i++) {
                if (EappVariables.EappModel.Requirements[i].requirementType === "Signature") {
                    for (var j = 0; j < EappVariables.EappModel.Requirements[i].Documents.length; j++) {
                        var value = $scope.DocumentType.split("#");
                        if (EappVariables.EappModel.Requirements[i].Documents[j].documentName.indexOf(value[1]) > 0) {
                            requirementObject.RequirementFile.documentName = EappVariables.EappModel.Requirements[i].Documents[j].documentName;
                            requirementObject.RequirementFile.requirementName = EappVariables.EappModel.Requirements[i].requirementName;
                            PersistenceMapping.clearTransactionKeys();
                            EappService.mapKeysforPersistence();
                            var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
                            DataService.getDocumentsForRequirement(transactionObj, getDocsSuccess, $scope.errorCallback);
                        }
                    }
                }
            }
        }
    }
    $scope.errorCallback = function () {

    }

    function getDocsSuccess(sigdata) {
        EappVariables.Signature = sigdata[0].base64string;
        $scope.signaturePad.fromDataURL(sigdata[0].base64string);
    }

    $scope.confirmProposal = function (sigData) {
        if ($scope.LifeEngageProduct.Insured.IncomeDetails && !($scope.LifeEngageProduct.Insured.IncomeDetails.annualIncome)) {
            $scope.LifeEngageProduct.Insured.IncomeDetails.annualIncome = "";
        }
        // added for prev button in payment
        // authorisation
        $scope.confirmForPaymentAuth = true;

        var requirementType = "Signature";
        var index = "";
        var requirementName = "Signature";
        var partyIdentifier = "";
        var isMandatory = "";
        var docArray = [];
        var documentType = $scope.DocumentType;

        if ((rootConfig.isDeviceMobile)) {
            EappService.getRequirementFilePath(sigData, index, requirementName, requirementType, documentType, function (filePath) {
                if ((rootConfig.isDeviceMobile) || rootConfig.isOfflineDesktop) {
                    filePath = "../" + filePath;
                }
                EappService.saveRequirement(filePath, requirementType, partyIdentifier, isMandatory, docArray, documentType, function () {
                    if ($scope.canvasId == "spajSummaryCanvas") {
                        $scope.spajSummaryCanvas = true;
                        $scope.spajSummaryButtonDisable = true;
                    }
                    $scope.refresh();
                }, $scope.onConfirmError);
            });
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
        } else {
            EappService.saveRequirement(sigData, requirementType, partyIdentifier, isMandatory, docArray, documentType, function () {
                $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
            }, function () {
                $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
            });
        }


    };
    var load = $scope.$parent.$on('SPAJsummary', function (event, args) {

        $scope.selectedTabId = args[0];
        if ($scope.LifeEngageProduct.LastVisitedIndex && args[1]) {
            $scope.LifeEngageProduct.LastVisitedIndex = args[1];
        }
        $scope.Initialize();
        $scope.summaryProductName();


        $rootScope.validationBeforesave = function (value, successcallback) {
            $scope.validateBeforeSaveDetails(value, successcallback);
        };

    });
    

    $rootScope.validationBeforesave = function (value, successcallback) {
        $scope.validateBeforeSaveDetails(value, successcallback);
    };

    $scope.validateBeforeSaveDetails = function (value, successcallback) {
        if ((rootConfig.isDeviceMobile)) {
            if ($scope.LifeEngageProduct.showHideQuestaionnaireTab) {
                successcallback();
            } else {
                successcallback();
            }
        } else {
            if ($scope.LifeEngageProduct.showHideQuestaionnaireTab) {
                successcallback();
            } else {
                successcallback();
            }
        }
    }


    $scope.summaryProductName = function(){
    	if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "10"){
    		$scope.LifeEngageProduct.Product.ProductDetails.productNameSummary = translateMessages($translate,"eapp_vt.GenCompleteHealth");
        }
    	if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "4" && $scope.LifeEngageProduct.Product.policyDetails.premiumPeriod == "20") {
            $scope.LifeEngageProduct.Product.ProductDetails.productNameSummary = translateMessages($translate,"illustrator.genProLife20Product");
        }
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "4" && $scope.LifeEngageProduct.Product.policyDetails.premiumPeriod == "25") {
        	$scope.LifeEngageProduct.Product.ProductDetails.productNameSummary = translateMessages($translate,"illustrator.genProLife25Product");
        }
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "6") {
        	$scope.LifeEngageProduct.Product.ProductDetails.productNameSummary = translateMessages($translate,"GenSave20Plus");
        } 
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "3"){
        	$scope.LifeEngageProduct.Product.ProductDetails.productNameSummary = translateMessages($translate,"illustrator.genbumnan8Header");
        } 
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "97"){
            $scope.LifeEngageProduct.Product.ProductDetails.productNameSummary = translateMessages($translate,"illustrator.GenCancerSuperProtection");
        } 
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "220"){
        	$scope.LifeEngageProduct.Product.ProductDetails.productNameSummary = translateMessages($translate,"illustrator.Wholelife");
        }
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "306") {
        	$scope.LifeEngageProduct.Product.ProductDetails.productNameSummary = translateMessages($translate,"GenSave10Plus");
        }
        if($scope.LifeEngageProduct.Product.ProductDetails.productCode == "274") {
        	$scope.LifeEngageProduct.Product.ProductDetails.productNameSummary = translateMessages($translate,"GenSave4Plus");
        }
    }
    
    $scope.SPAJAllocation = function (sucess) {
        // SPAJ allocation logic
        if ((rootConfig.isDeviceMobile)) {
            PersistenceMapping.clearTransactionKeys();
            var transactionObj = PersistenceMapping.mapScopeToPersistence({});
            GLI_DataService.retrieveSPAJCount(transactionObj.Key11, function (count) {
                if (count > 0) {
                    var agentCode = transactionObj.Key11; // TODO: previously it was $scope.agentDetail.agentCode; but,sometimes getting undefind dirty fix changed to KEy11
                    GLI_DataService.retrieveSPAJNo(agentCode, function (data) {

                        if (!EappVariables.EappKeys.Key21 || EappVariables.EappKeys.Key21 == '') {
                            EappVariables.EappKeys.Key21 = data[0].SPAJNo;
                            $scope.lePopupCtrl.showWarning(
                                translateMessages($translate,
                                    "lifeEngage"),
                                translateMessages($translate,
                                    "eapp.spajAllocatedMessage") + " " + data[0].SPAJNo,
                                translateMessages($translate,
                                    "fna.ok"));
                        }
//                        EappVariables.EappKeys.Key15 = "Confirmed";
                        //EappVariables.eAppProceedingDate = UtilityService.getFormattedDate();
                        $scope.isSPAJNoAllocated = true;
                        $scope.spajNumber = EappVariables.EappKeys.Key21;
                        $scope.refresh();
                        $scope.LifeEngageProduct.LastVisitedUrl = "4,3,'DeclarationAndAuthorizationTab',true,'DeclarationAndAuthorizationTab',''";
                        sucess();

                    }, $scope.onRetrieveAgentProfileError);
                } else {
                    $rootScope.lePopupCtrl.showError(translateMessages($translate,
                        "lifeEngage"), translateMessages($translate,
                        "eapp.SPAJExhaustMessage"), translateMessages($translate,
                        "fna.ok"), $scope.okClick);
                }
            }, $scope.onRetrieveAgentProfileError);
        } else {
            if ($scope.isRDSUser) {
                if (!EappVariables.EappKeys.Key21 || EappVariables.EappKeys.Key21 == '') {
                    EappVariables.EappKeys.Key21 = $scope.LifeEngageProduct.Product.policyDetails.policyNumber;
                    $scope.lePopupCtrl.showWarning(
                        translateMessages($translate,
                            "lifeEngage"),
                        translateMessages($translate,
                            "eapp.spajAllocatedMessage") + " " + $scope.LifeEngageProduct.Product.policyDetails.policyNumber,
                        translateMessages($translate,
                            "fna.ok"));
                    $scope.spajNumber = EappVariables.EappKeys.Key21;
                    /*$scope.LifeEngageProduct.LastVisitedUrl	= "4,3,'Declaration',true,'Declaration',''";
                    sucess();*/
                }
            } else {
                PersistenceMapping.clearTransactionKeys();
                var transactionObj = PersistenceMapping.mapScopeToPersistence({});
                transactionObj.Key18 = 1;
                GLI_DataService.retrieveSPAJNumbers(transactionObj, function (data) {
                    if (!EappVariables.EappKeys.Key21 || EappVariables.EappKeys.Key21 == '') {
                        EappVariables.EappKeys.Key21 = data.spajNos[0];
                        $scope.lePopupCtrl.showWarning(
                            translateMessages($translate,
                                "lifeEngage"),
                            translateMessages($translate,
                                "eapp.spajAllocatedMessage") + " " + data.spajNos[0],
                            translateMessages($translate,
                                "fna.ok"));
                        $scope.spajNumber = EappVariables.EappKeys.Key21;
                    }
                }, $scope.onRetrieveAgentProfileError);
            }
//            EappVariables.EappKeys.Key15 = "Confirmed";
            //EappVariables.eAppProceedingDate = UtilityService.getFormattedDate();
            $scope.refresh();
            $scope.LifeEngageProduct.LastVisitedUrl = "4,3,'DeclarationAndAuthorizationTab',true,'DeclarationAndAuthorizationTab',''";
            sucess();
        }
    }
    $scope.confirmDeclaration = function (canvasId) {
        $scope.canvasId = canvasId;
        if (canvasId == "spajSummaryCanvas") {
            if ((rootConfig.isDeviceMobile)) {
                if ($scope.errorCount == 0 && ($scope.LifeEngageProduct.Declaration.spajSignPlace && $scope.LifeEngageProduct.Declaration.spajSignPlace != "")) {
                    if (signaturePadSpaj && !signaturePadSpaj.isEmpty()) {
                        $scope.LifeEngageProduct.Declaration.spajSignDate = UtilityService.getFormattedDate();
                        $scope.spajSignDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.spajSignDate), $scope.appDateTimeFormat);
                        $scope.confirmProposal(signaturePadSpaj.toDataURL());
                    } else {
                        $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.declarationAgreedMessage"), translateMessages($translate, "fna.ok"));
                    }
                } else {
                    $scope.lePopupCtrl.showWarning(
                        translateMessages($translate,
                            "lifeEngage"),
                        translateMessages($translate,
                            "eapp.placeOfSign"),
                        translateMessages($translate,
                            "fna.ok"));
                }
            } else {
                if ($scope.LifeEngageProduct.Declaration.spajDeclaration == "Yes" && $scope.errorCount == 0) {
                    if (!$scope.isRDSUser) {
                        $scope.LifeEngageProduct.Declaration.spajSignDate = UtilityService.getFormattedDate();
                        if ($scope.isDeviceWeb) {
                            $scope.spajSignDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.spajSignDate), $scope.appDateTimeFormat);
                        }
                    }
                    if ($scope.signaturePad)
                        $scope.confirmProposal($scope.signaturePad.toDataURL());
                }
            }
        }
    };

    $scope.displayARMSSection = false;
    /*if($scope.LifeEngageProduct.Product.Risk.autoBalancing != 'undefined' && $scope.LifeEngageProduct.Product.Risk.autoTrading != 'undefined' ){	
    	if($scope.LifeEngageProduct.Product.Risk.autoBalancing == "Yes" || $scope.LifeEngageProduct.Product.Risk.autoTrading == "Yes"){
    		$scope.displayARMSSection = true;
    	}
    }*/
    $scope.$parent.displayARMSSection = false;
    /*if($scope.LifeEngageProduct.Product.Risk.autoBalancing != 'undefined' && $scope.LifeEngageProduct.Product.Risk.autoTrading != 'undefined' ){	
    	if($scope.LifeEngageProduct.Product.Risk.autoBalancing == "Yes" || $scope.LifeEngageProduct.Product.Risk.autoTrading == "Yes"){
    		$scope.$parent.displayARMSSection = true;
    	}
    }*/
    $scope.updateErrorCount = function (id) {
        $rootScope.updateErrorCount(id);
        $scope.selectedTabId = id;
        $scope.eAppParentobj.errorCount = $scope.errorCount;
    }
    $scope.showErrorPopup = function (id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
    }
    $scope.summaryEdit = function (linkTab, linkSubTab) {
        // isRootNav will check for whether the nav
        // click is coming from direct
        // main root tabs/subtabs
        $scope.isRootNav = false;
        $scope.eAppParentobj.FromSummaryFlag = true;
        $scope.refresh();
        if(linkSubTab==='BeneficiarySubTab'){
              $scope.paintView(1,7,'BeneficiarySubTab',false,'BeneficiarySubTab','');
        }else if(linkSubTab==='financilaDetailsSubTab'){
              $scope.paintView(1,8,'financilaDetailsSubTab',false,'financilaDetailsSubTab','');
        }else{
        $('#' + linkTab).find(
            "li a[rel='" + linkSubTab +
            "']").parent('li').trigger(
            'click');
        }

    };

    //Used to set the date for RDS user in Summary screen
    $scope.setDateForRDSUser = function (date) {
        if ($scope.isRDSUser) {
            var temp = getFormattedDateSummary(date);
            $scope.LifeEngageProduct.Declaration.spajSignDate = temp;
        }
    };


    /*
     * Used to show or hide the questionnaire tab under summary based on additonal questions selected or not.---starts
     */
    function summayQuestionnaireTabShowHide() {
        $scope.arrayIndex = 0;
        var questionnaireCheckValues = [$scope.LifeEngageProduct.isProposerForeigner,
			                                $scope.LifeEngageProduct.hasInsuredQuestions,
			                                $scope.LifeEngageProduct.hasBeneficialOwnerForm];

        if ($scope.LifeEngageProduct.hasInsuranceEngagementLetter == true || $scope.LifeEngageProduct.hasInsuranceEngagementLetter == "true") {
            return true;
        } else {
            var returnValue;
            loopArrayQuestionnaire(questionnaireCheckValues[$scope.arrayIndex], function (dataReturn) {
                returnValue = dataReturn;
            });
            return returnValue;
        }
    };

    function loopArrayQuestionnaire(arrayValue, successcallback) {
        var questionnaireCheckValues = [$scope.LifeEngageProduct.isProposerForeigner,
			                                $scope.LifeEngageProduct.hasInsuredQuestions,
			                                $scope.LifeEngageProduct.hasBeneficialOwnerForm];
        checkLength(arrayValue, function (returnValue) {
            successcallback(true);
        });
    };

    /*
     * Used to show or hide the questionnaire tab under summary based on additonal questions selected or not.---ends
     */

    /*$scope.ShowDate = function(model, format) {
    	var returnVal;
    	var parentModel = model.split('.');
    	var isParentModel = parentModel[0]+"."+parentModel[1];
    	if(eval ("$scope."+isParentModel)){
    		if(!(isEmptyOrNull(format))){
    		
    			for (var i = 0; i < $scope.LifeEngageProduct.Beneficiaries.length; i++) {
    					if(i==format){
    						returnVal=$scope.LifeEngageProduct.Beneficiaries[i].BasicDetails.dob;
    						break;
    					}
    			}
    		}
    		else{
    			eval ("returnVal=$scope." + model);
    		}
    		returnVal = new Date(Date.parse(returnVal))
    	
    		return returnVal != 'Invalid Date' ? (parseInt(returnVal
    			.getDate()))
    			+ "-"
    			+ (parseInt(returnVal.getMonth()) + 1)
    			+ "-" + returnVal.getFullYear()
    			: "";
    	}
    };*/

    /*$scope.showOptions = function(model) {
    	if(model){
    		var returnVal;
    		var parentModel = model.split('.');
    		var isParentModel = parentModel[0]+"."+parentModel[1];
    		var isParentModel1 = parentModel[0]+"."+parentModel[1];
    		var isParentModel2 = parentModel[0]+"."+parentModel[1]+"."+parentModel[2];
    		
    		if(eval ("$scope."+isParentModel1) && eval ("$scope."+isParentModel2)) {
    			if(eval ("$scope."+model)!=undefined){
    		   		eval ("returnVal=$scope." + model);
    			    	if(returnVal == 'Yes'){
    				    	return true;
    			   		}
    			    else{
    						return false;
    			  		}
    		   	}
    		   	else{
    		   		return false;
    		   	}
    		}	
    	}			  
    		
    };*/
    var spajSummaryCanvasWatch = $scope.$watch('LifeEngageProduct.Declaration.spajDeclaration', function (value) {
        if ($scope.selectedTabId == 'SPAJsummary') {
            if (value == 'Yes') {
                $scope.spajSummaryCanvas = false;
            } else {
                if (signaturePadSpaj) {
                    signaturePadSpaj.clear();
                    $scope.spajSummaryCanvas = true;
                } else {
                    if (!(rootConfig.isDeviceMobile) && $scope.signaturePad) {
                        $scope.signaturePad.clear();
                    }
                }
            }
            $scope.refresh();
        }
    }, true);



    $scope.$on("$destroy", function () {
        if (this != null && typeof this !== 'undefined') {
            spajSummaryCanvasWatch();

            if (this.$$destroyed) return;
            while (this.$$childHead) {
                this.$$childHead.$destroy();
            }
            if (this.$broadcast) {
                this.$broadcast('$destroy');
            }
            this.$$destroyed = true;
            //$timeout.cancel( timer );  
        }
    });    
}