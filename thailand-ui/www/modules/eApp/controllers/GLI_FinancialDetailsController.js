'use strict';
storeApp.controller('GLI_FinancialDetailsController', GLI_FinancialDetailsController);
GLI_FinancialDetailsController.$inject = ['$rootScope', '$scope', 'DataService', 'LookupService', 'DocumentService', 'ProductService', '$compile', '$routeParams', '$location', '$translate', '$debounce', 'UtilityService', 'PersistenceMapping', 'AutoSave', 'globalService', 'EappVariables', 'EappService', '$timeout', 'GLI_EvaluateService'];

function GLI_FinancialDetailsController($rootScope, $scope, DataService, LookupService, DocumentService, ProductService, $compile, $routeParams, $location, $translate, $debounce, UtilityService, PersistenceMapping, AutoSave, globalService, EappVariables, EappService, $timeout, GLI_EvaluateService) {

    $scope.numberPattern = rootConfig.numberPattern;
    $scope.alphanumericPattern = rootConfig.alphanumericPattern;
    $scope.alphanumericPatternfin = rootConfig.alphanumericPatternfin;
    $scope.numberPatternMob = rootConfig.numberPatternMob;    
    $scope.alphaNumericEappCustom=rootConfig.alphaNumericEappCustom;
    $scope.creditCardPattern = rootConfig.creditCardPattern;
    $scope.premiumPayorCode;
    $scope.updateErrorCount = function (id) {
        $rootScope.updateErrorCount(id);
        $scope.selectedTabId = id;
        $scope.eAppParentobj.errorCount = $scope.errorCount;
        EappVariables.setEappModel($scope.LifeEngageProduct);
        $scope.refresh();
    }
    //For counting no.of AdditionalQuestions selected
    $scope.callAdditionalQuestionCount = function (val, questionId) {
        if (val != 'Other') {
            val = 'No';
        }
        var curentArr = [];
        if ($scope.LifeEngageProduct.hasBeneficialOwnerForm && ($scope.LifeEngageProduct.hasBeneficialOwnerForm != "0" || $scope.LifeEngageProduct.hasBeneficialOwnerForm != 0) && $scope.LifeEngageProduct.hasBeneficialOwnerForm != "") {
            curentArr = JSON.parse($scope.LifeEngageProduct.hasBeneficialOwnerForm).selectedArr;
        }
        EappService.updateAdditionalQuestionCount(val, questionId, curentArr, function (modifiedArr) {
            var modifiedJson = {
                "selectedArr": []
            };
            modifiedJson.selectedArr = modifiedArr;
            $scope.LifeEngageProduct.hasBeneficialOwnerForm = JSON.stringify(modifiedJson);
        });
    }

    $scope.prepopulateCreditCardName = function (name, premiumPayer) {
        if (premiumPayer == 'Other') {
            $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName = name;
        }
    }

    //function to clear the text box if checkbix is unchecked.
    $scope.clearText = function (parentModel, Childmodel) {
        var parent = GLI_EvaluateService.evaluateString($scope, parentmodel);
        if (typeof parent != undefined && parent != "Yes") {
            var scopeChildmodel = GLI_EvaluateService.evaluateString($scope, "$scope." + Childmodel.substring(0, Childmodel.lastIndexOf('.')));
            var childSubModel = Childmodel.substring(Childmodel.lastIndexOf('.') + 1, Childmodel.length);
            scopeChildmodel[childSubModel] = "";
            $scope.refresh();
        }
    }

    $scope.showErrorPopup = function (id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
        var expiryDate = $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardExpiryDate;
    }

    $scope.validateDateFields = function () {
        if (!$scope.dynamicErrorMessages) {
            $scope.dynamicErrorMessages = [];
            $scope.dynamicErrorCount = 0;
        }
        var _errors = [];
        var _errorCount = 0;
        var expiryDate = $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardExpiryDate;
        if (LEDate(expiryDate) <= LEDate()) {
            var error = {};
            _errorCount++;
            error.message = translateMessages($translate, "eapp.expiryDateValidationMessage");
            error.key = 'expiryDate';
            _errors.push(error);
        }
        //Validating effective date
        var creditcardEffectiveDate = $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardEffectiveDate;
        if (LEDate(creditcardEffectiveDate) >= LEDate()) {
            _errorCount++;
            var error = {};
            error.message = translateMessages($translate, "eapp.effectiveDateValidationMessage");
            error.key = 'effectiveDate';
            _errors.push(error);
        }


        $scope.dynamicErrorCount = _errorCount;
        if (!$scope.dynamicErrorMessages) {
            $scope.dynamicErrorMessages = [];
        }
        $scope.dynamicErrorMessages = _errors;
        $rootScope.updateErrorCount('financilaDetailsSubTab');
    }

    $scope.Initialize = function () {
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_BeneficiarySubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_BeneficiarySubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_financilaDetailsSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_financilaDetailsSubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_mainInsuredSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_mainInsuredSubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PolicyHolderSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PolicyHolderSubTab#tabDetails_1');
        }		
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6');
		}	
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
		}
        $scope.viewToBeCustomized = 'financilaDetailsSubTab';
        $scope.LifeEngageProduct = EappVariables.getEappModel();
        $scope.eAppParentobj.PreviousPage = "";
        $timeout(function () {
            $scope.updateErrorCount($scope.selectedTabId);
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission") {
                UtilityService.disableAllActionElements();
            }else if (typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) {
                UtilityService.disableAllActionElements();
            }
        }, 0);
        $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
        // if ($scope.LifeEngageProduct.Proposer.CustomerRelationship.isPayorDifferentFromInsured == 'Yes') {
        //     $scope.isPayorDifferent = true;
        // } else {
        //     $scope.isPayorDifferent = false;
        // }
        //For disabling and enabling the tabs.And also to know the last visited page.---starts


         //For disabling and enabling the tabs.And also to know the last visited page.---starts
        $rootScope.selectedPage = "FinancialDetails";
        $scope.LifeEngageProduct.LastVisitedUrl = "1,8,'financilaDetailsSubTab',true,'financilaDetailsSubTab',''";
        $scope.eAppParentobj.nextPage   	=	"2,0,'ProductDetails',false,'ProductDetails',''";
        //$scope.eAppParentobj.nextPage = "2,0,'ProductDetails',false,'ProductDetails',''";
        $scope.eAppParentobj.CurrentPage = "";
        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[1] < 8 && subIndex[0] <= $scope.LifeEngageProduct.LastVisitedUrl[0]) {
                $scope.LifeEngageProduct.LastVisitedIndex = "1,8";
            }
        }

        $(window).scrollTop(0); 
        //For disabling and enabling the tabs.And also to know the last visited page.---ends
        
        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        angular.element('#' + $scope.selectedTabId + "_a").trigger('click');
        $scope.updateErrorCount($scope.selectedTabId);
        $scope.payerList = [];
        populatePayorList();
        $scope.onPremiumPayerChange();
        if (typeof $scope.LifeEngageProduct.Payment.FinancialInfo.premiumPayer != "undefined") {
            $scope.premiumPayorCode = $scope.LifeEngageProduct.Payment.FinancialInfo.premiumPayer;
        }
        $scope.eAppParentobj.errorCount = $scope.errorCount;
        var model = "LifeEngageProduct";
        if ((rootConfig.autoSave.eApp) && (typeof EappVariables.EappKeys.Key15 == "undefined" || EappVariables.EappKeys.Key15 == "New" || EappVariables.EappKeys.Key15 == "Confirmed")) {
            if (AutoSave.destroyWatch) {
                AutoSave.destroyWatch();
            }
            AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
        } else if (AutoSave.destroyWatch) {
            AutoSave.destroyWatch();
        }
    }
    //function for populating payor list dropdown.This implementation have to be changed
    function populatePayorList() {
        $scope.payerList = [];
        var payerObj = new Object();
        payerObj.code = "Proposer";
        payerObj.value = $scope.LifeEngageProduct.Proposer.BasicDetails.firstName + ' ' + $scope.LifeEngageProduct.Proposer.BasicDetails.lastName;
        $scope.payerList.push(payerObj);
        // if ($scope.LifeEngageProduct.Proposer.CustomerRelationship.isPayorDifferentFromInsured == "Yes") {
        //     var payerObjMI = new Object();
        //     payerObjMI.code = "MainInsured";
        //     payerObjMI.value = $scope.LifeEngageProduct.Insured.BasicDetails.firstName + ' ' + $scope.LifeEngageProduct.Insured.BasicDetails.lastName;
        //     $scope.payerList.push(payerObjMI);
        // }
        var payerObjO = new Object();
        payerObjO.code = "Other";
        payerObjO.value = "Other";
        $scope.payerList.push(payerObjO);
    }
    //retrieving  premium payer name
    $scope.getLookUpData = function (data, value) {
        return data.filter(function (data) {
            return angular.lowercase(data.code) == angular.lowercase(value)
        });
    }

    //clearing off other values depending on the radio button.
    $scope.onPaymentTypeChange = function (payment_Type) {
        if (payment_Type == 'CreditCard') {
            $scope.LifeEngageProduct.Payment.RenewalPayment.bankName = "";
            $scope.LifeEngageProduct.Payment.RenewalPayment.branchName = "";
            $scope.LifeEngageProduct.Payment.RenewalPayment.accHolderName = "";
            $scope.LifeEngageProduct.Payment.RenewalPayment.accountNumber = "";
            if ($scope.LifeEngageProduct.Payment.FinancialInfo.premiumPayer == 'Other')
                $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName = $scope.LifeEngageProduct.Proposer.AdditionalQuestioniare.Questions[51].details;
            else {
                $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName = $scope.LifeEngageProduct.Payment.FinancialInfo.premiumPayer;
            }
        } else if (payment_Type == 'DebitAccount') {
            $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardExpiryDate = "";
            $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardNumber = "";
            $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardEffectiveDate = "";
            $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName = "";
            $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardType = "";
            if ($scope.LifeEngageProduct.Payment.FinancialInfo.premiumPayer == 'Other')
                $scope.LifeEngageProduct.Payment.RenewalPayment.accHolderName = $scope.LifeEngageProduct.Proposer.AdditionalQuestioniare.Questions[51].details;
            else {
                $scope.LifeEngageProduct.Payment.RenewalPayment.accHolderName = $scope.LifeEngageProduct.Payment.FinancialInfo.premiumPayer;
            }
        } else if (payment_Type == 'Transfer/VirtualAccount') {
            $scope.LifeEngageProduct.Payment.RenewalPayment.bankName = "";
            $scope.LifeEngageProduct.Payment.RenewalPayment.branchName = "";
            $scope.LifeEngageProduct.Payment.RenewalPayment.accHolderName = "";
            $scope.LifeEngageProduct.Payment.RenewalPayment.accountNumber = "";
            $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardExpiryDate = "";
            $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardNumber = "";
            $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardEffectiveDate = "";
            $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName = "";
            $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardType = "";
        } else if(payment_Type == 'transferToBank'){

            $scope.LifeEngageProduct.Payment.RefundPayment.bankName = "";
            $scope.LifeEngageProduct.Payment.RefundPayment.branchName = "";
            $scope.LifeEngageProduct.Payment.RefundPayment.accHolderName = "";
            $scope.LifeEngageProduct.Payment.RefundPayment.accountNumber = "";

        }else if(payment_Type == 'Cash'){

            $scope.LifeEngageProduct.Payment.RefundPayment.bankName = "";
            $scope.LifeEngageProduct.Payment.RefundPayment.branchName = "";
            $scope.LifeEngageProduct.Payment.RefundPayment.accHolderName = "";
            $scope.LifeEngageProduct.Payment.RefundPayment.accountNumber = "";

        }
        EappVariables.setEappModel($scope.LifeEngageProduct);
        $rootScope.updateErrorCount('financilaDetailsSubTab');
        $scope.refresh();
    }

    $scope.clearDataOnPremiumPayerChange = function () {
        $scope.LifeEngageProduct.Payment.FinancialInfo.sourceOfIncomePayer = "";
        $scope.LifeEngageProduct.Payment.FinancialInfo.grossIncomePayer = "";
        $scope.LifeEngageProduct.Payment.FinancialInfo.sourceOfIncomeProposer = "";
        $scope.LifeEngageProduct.Payment.FinancialInfo.grossIncomeProposer = "";
        $rootScope.updateErrorCount('financilaDetailsSubTab');
    };


    //autopopulating names with respect to the dropdown values 
    $scope.onPremiumPayerChange = function () {
        for (var payer in $scope.payerList) {
            if ($scope.payerList[payer].code == $scope.premiumPayorCode) {
                $scope.LifeEngageProduct.Payment.FinancialInfo.premiumPayerType = $scope.payerList[payer].code;
                $scope.LifeEngageProduct.Payment.FinancialInfo.premiumPayer = $scope.premiumPayorCode;
                if ($scope.payerList[payer].code != "Proposer" && $scope.payerList[payer].code != "") {
                    $scope.isPayorDifferentPayer = true;
                } else {
                    $scope.isPayorDifferentPayer = false;
                }
            }
        }
        if ($scope.LifeEngageProduct.Payment.RenewalPayment.paymentType == 'CreditCard') {
            if ($scope.LifeEngageProduct.Payment.FinancialInfo.premiumPayer == 'Other')
                $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName = $scope.LifeEngageProduct.Proposer.AdditionalQuestioniare.Questions[51].details;
            else {
                $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName = $scope.LifeEngageProduct.Payment.FinancialInfo.premiumPayer;
            }
        } 
        $rootScope.updateErrorCount('financilaDetailsSubTab');
    }
    //function added for checing mandatory error message for insurance purpose
    $scope.dynamicErrorForInsurancePurpose = function () {
        if (!$scope.dynamicErrorMessages) {
            $scope.dynamicErrorMessages = [];
            $scope.dynamicErrorCount = 0;
        }
        var _errors = [];
        var _errorCount = 0;
        if ($scope.LifeEngageProduct.Payment.FinancialInfo.insurancePurposeProtection != 'Yes' && $scope.LifeEngageProduct.Payment.FinancialInfo.insurancePurposeInvestment != 'Yes' &&
            $scope.LifeEngageProduct.Payment.FinancialInfo.insurancePurposeEducation != 'Yes' && $scope.LifeEngageProduct.Payment.FinancialInfo.insurancePurposeEducation != 'Yes' &&
            $scope.LifeEngageProduct.Payment.FinancialInfo.insurancePurposePensionFund != 'Yes' && $scope.LifeEngageProduct.Payment.FinancialInfo.insurancePurposeBusiness != 'Yes' &&
            $scope.LifeEngageProduct.Payment.FinancialInfo.insurancePurposeOther != 'Yes') {
            _errorCount++;
            var error = {};
            error.message = translateMessages(
                $translate,
                "eapp.insurancePurposeRequired");
            error.key = 'purposeofInsurance';
            _errors.push(error);
        }
        $scope.dynamicErrorCount = _errorCount;
        if (!$scope.dynamicErrorMessages) {
            $scope.dynamicErrorMessages = [];
        }
        $scope.dynamicErrorMessages = _errors;
        $scope.updateErrorCount('financilaDetailsSubTab');

    }

    //GLI_FinancialDetailsController Load Event and Load Event Handler
    $scope.$parent.$on('financilaDetailsSubTab', function (event, args) {
        $scope.selectedTabId = args[0];
        $scope.Initialize();
        //checking insurance purpose mandatory
       // $scope.dynamicErrorForInsurancePurpose();
        $rootScope.validationBeforesave = function (value, successcallback) {
            successcallback();
        };
    });
    $scope.$on("$destroy", function () {
        if (this != null && typeof this !== 'undefined') {
            if (this.$$destroyed) return;
            while (this.$$childHead) {
                this.$$childHead.$destroy();
            }
            if (this.$broadcast) {
                this.$broadcast('$destroy');
            }
            this.$$destroyed = true;
            $timeout.cancel(timer);
        }
    });
    $rootScope.validationBeforesave = function (value, successcallback) {
        successcallback();
    };
    
    //Temporary fix until finding the rootcause
    if($scope.LifeEngageProduct.Payment.RenewalPayment.paymentFrequency==='Semi Annual'){
        $scope.LifeEngageProduct.Payment.RenewalPayment.paymentFrequency='SemiAnnual';
    }
};