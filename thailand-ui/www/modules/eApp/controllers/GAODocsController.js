/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:GAODocsController
 CreatedDate:24/4/2018 - 24th April 2018
 Description:Eapp GAO docs controller
 */
storeApp.controller('GAODocsController',[
	'$rootScope','$scope','$translate','$window',
function($rootScope, $scope, $translate, $window) {
	if($window.docsArray && $window.docsArray.length>0){
		$scope.docs = $window.docsArray;
	}else{
		//$scope.docs = getDocs();
	}
	$scope.showPreview = function(index){
		$scope.selectedDoc = "";
		if($scope.docs[index].type=="pdf" || $scope.docs[index].type=="doc" || $scope.docs[index].type=="docx"){
			$scope.selectedDoc=angular.copy($scope.docs[index].base64);
			var link = document.createElement('a');
			link.href = $scope.docs[index].pdfBase64;
			link.download=$scope.docs[index].fileName;
			link.click();
		}else{
			$scope.selectedDoc = angular.copy($scope.docs[index].base64);
		}
		$scope.$apply();
		$("#myimage").elevateZoom({
			  zoomType : "inner",
			  scrollZoom : true,
			  cursor : "crosshair"
		}); 
	}
	}]);
