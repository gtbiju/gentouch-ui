'use strict';

storeApp.controller('InsuredController', ['$rootScope', '$scope', 
                                          'DataService', 'LookupService', 
                                          'DocumentService', 'ProductService', 
                                          '$compile', '$routeParams', '$location', 
                                          '$translate', '$debounce', 'UtilityService', 
                                          'PersistenceMapping', 'AutoSave', 'globalService', 
                                          'EappVariables', 'EappService', '$timeout',
function($rootScope, $scope, DataService, LookupService, DocumentService,
        ProductService, $compile, $routeParams, $location, $translate, 
        $debounce, UtilityService, PersistenceMapping, AutoSave, globalService,
        EappVariables, EappService, $timeout) {

    $scope.mobnumberPattern = rootConfig.mobnumberPattern;
    $scope.namePattern = rootConfig.namePattern;
    $scope.zipPattern = rootConfig.zipPattern;
    $scope.numberPattern = rootConfig.numberPattern;
    $scope.emailPattern = rootConfig.emailPattern;
    var isPermanentAddressSameAsCurrentAddressWatch;
    var relationshipWithProposerWatch;
    $scope.showErrorCount = true;
    $scope.innertabsVertical = [];
	$scope.innertabsVertical[0] = 'selected';
    $scope.Initialize = function() {

        $scope.LifeEngageProduct = EappVariables.getEappModel();
        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        angular.element('#' + $scope.selectedTabId + "_a").trigger('click');
         for ( var i = 0; i < $scope.innertabsVertical.length; i++){
             if($scope.innertabsVertical[i]=='selected'){
                  $scope.updateErrorCount($scope.selectedTabId,i);
                  break;
             }   
         }
		var model = "LifeEngageProduct";
		if ((rootConfig.autoSave.eApp) && EappVariables.EappKeys.Key15 != 'Final') {
			if(AutoSave.destroyWatch){
				AutoSave.destroyWatch();
			}
			AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
		} else if(AutoSave.destroyWatch){
			AutoSave.destroyWatch();
		}

      

        $scope.IsInsuredPermAddSame = function(id) {
            if ($scope.LifeEngageProduct.Insured.ContactDetails.isPermanentAddressSameAsCurrentAddress) {
                $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress =
                        $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress;
            } else {
                $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress = {};
            }
            $scope.refresh();
            $scope.updateErrorCount(id);
        };
       if(!$scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer){
        var insuedName = $scope.LifeEngageProduct.Insured.BasicDetails.firstName + 
                $scope.LifeEngageProduct.Insured.BasicDetails.lastName;
        var proposerName = $scope.LifeEngageProduct.Proposer.BasicDetails.firstName +
                $scope.LifeEngageProduct.Proposer.BasicDetails.lastName;
        var insuredDob =""; 
       if(insuredDob)
           insuredDob = LEDate($scope.LifeEngageProduct.Insured.BasicDetails.dob);
        var proposerDOB ="";
        if(proposerDOB)
            proposerDOB=LEDate($scope.LifeEngageProduct.Proposer.BasicDetails.dob);
       if ( (insuedName && proposerName && (insuedName != proposerName) ) &&  ( insuredDob && proposerDOB &&!(insuredDob - proposerDOB)) ) {
        $scope.LifeEngageProduct.Insured.CustomerRelationship.isPayorDifferentFromInsured = "Yes";
            $scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer = "Others";
       
        } else {
                $scope.LifeEngageProduct.Insured.CustomerRelationship.isPayorDifferentFromInsured = "No";
            $scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer = "Self";
        }
       }
        $scope.refresh();
    }
    $scope.updateErrorCount = function(id,index) {
        $rootScope.updateErrorCount(id);
        $scope.selectedTabId = id;
        var insuredDob = $scope.LifeEngageProduct.Insured.BasicDetails.dob;
        if (LEDate(insuredDob) >= LEDate()) {
            $scope.errorCount = parseInt($scope.errorCount) + 1;
        }
        // TODO change this to one variable
        if ((id == "tabsinner3") && $scope.LifeEngageProduct.Insured.ContactDetails.methodOfCommunication.isEmail == 
                "No" && $scope.LifeEngageProduct.Insured.ContactDetails.methodOfCommunication.isSMS == "No" 
                && $scope.LifeEngageProduct.Insured.ContactDetails.methodOfCommunication.isPost == "No") {
            $scope.errorCount = parseInt($scope.errorCount) + 1;
        }
        if ($scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer != "Others") {
            $scope.LifeEngageProduct.Proposer = angular.copy($scope.LifeEngageProduct.Insured);
        }
        for ( var i = 0; i < $scope.innertabsVertical.length; i++) {
									if (i == index) {
										$scope.innertabsVertical[i] = 'selected';
									} else {
										$scope.innertabsVertical[i] = '';
									}
								}
    }
    $scope.onChangeRelation = function(){
     if ($scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer == "Others") {
      $scope.LifeEngageProduct.Proposer = new LifeEngageObject().Proposer;
     }
    }
    $scope.showErrorPopup = function(id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
        var insuredDob = $scope.LifeEngageProduct.Insured.BasicDetails.dob
        if (LEDate(insuredDob) >= LEDate()) {
            var error = {};
            error.message = translateMessages($translate, "insuredDOBFutureDateValidationMessage");
            error.key = 'lifeAssuredDOB';
            $scope.errorMessages.push(error);
        }
        // TODO change this to one variable
        if (($scope.selectedTabId == "tabsinner3") && 
                $scope.LifeEngageProduct.Insured.ContactDetails.methodOfCommunication.isEmail == "No" 
                && $scope.LifeEngageProduct.Insured.ContactDetails.methodOfCommunication.isSMS == "No" 
                && $scope.LifeEngageProduct.Insured.ContactDetails.methodOfCommunication.isPost == "No") {
            var error = {};
            error.message = translateMessages($translate, "insuredMethodOfCommunicationRequiredValidationMessage");
            error.key = 'insuredmethodOfCommunication1';
            $scope.errorMessages.push(error);
        }

    }
    // InsuredController Load Event and Load Event Handler
   /* $scope.$parent.$on('InsuredDetailsSubTab', function(event, args) {
        $scope.selectedTabId = args[0];
        if($scope.innertabsVertical[0]=='' && $scope.innertabsVertical[1]=='' && $scope.innertabsVertical[2]=='' && $scope.innertabsVertical[3]==''){
        $scope.innertabsVertical[0] = 'selected'
        }
		
	    $scope.innertabsVertical[0] = 'selected';
        $scope.Initialize();
    });*/
    $scope.$on("$destroy",function() {
    	//isPermanentAddressSameAsCurrentAddressWatch();
        //relationshipWithProposerWatch();
    });
}]);
