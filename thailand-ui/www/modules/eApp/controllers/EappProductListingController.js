﻿storeApp
		.controller(
				'EappProductListingController',
				[
						'$rootScope',
						'$scope',
						'DataService',
						'LookupService',
						'DocumentService',
						'$compile',
						'$routeParams',
						'UtilityService',
						'PersistenceMapping',
						function($rootScope, $scope, DataService,
								LookupService, DocumentService, $compile,
								$routeParams, UtilityService,
								PersistenceMapping) {
							var clientId = 'Agent1';

							$scope.mapKeysforPersistence = function() {
								PersistenceMapping.Key4 = ($scope.proposalId != null && $scope.proposalId != 0) ? $scope.proposalId
										: "";
								PersistenceMapping.Key5 = $scope.productId != null ? $scope.productId
										: "1";
								if (!(rootConfig.isDeviceMobile)) {
									if ($scope.proposalId == null
											|| $scope.proposalId == 0) {
										PersistenceMapping.creationDate  = UtilityService
												.getFormattedDateTime();
									}
								}
								EappVariables.eAppKeys.Key15 = EappVariables
										.getStatusOptionsEapp()[0].status;
								$scope.status = EappVariables.eAppKeys.Key15;
								PersistenceMapping.Type = "eApp";
							}

							$scope.onGetListingsSuccess = function(data) {
								$scope.Proposals = data;
								$scope.refresh();
							}

							$scope.onGetListingsError = function(data) {
								$scope.Proposals = data;
								$scope.refresh();
							}

							// Calling the dataservice to get the product object
							if (clientId != 0) {
								PersistenceMapping.clearTransactionKeys();
								$scope.mapKeysforPersistence();
								var transactionObj = PersistenceMapping
										.mapScopeToPersistence({});
								delete transactionObj.Id;

								DataService.getListings(transactionObj,
										$scope.onGetListingsSuccess,
										$scope.onGetListingsError);
							}
							$scope.refresh();
						} ]);