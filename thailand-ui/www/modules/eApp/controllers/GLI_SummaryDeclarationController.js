/*
 *Copyright 2015, LifeEngage 
 */
/*Name:SummaryController
 * For implementing GLI specific changes
 */
storeApp.controller('GLI_SummaryDeclarationController', GLI_SummaryDeclarationController);
GLI_SummaryDeclarationController.$inject = ['$timeout', '$route', 'globalService','AgentService', '$rootScope', '$scope', '$location', '$compile', '$routeParams', 'DataService', 'RuleService', 'LookupService', 'DocumentService', 'FnaVariables', 'GLI_FnaService', '$translate', 'UtilityService', '$debounce', 'AutoSave', '$controller', 'EappService', 'EappVariables', 'UserDetailsService', 'PersistenceMapping', 'GLI_DataService', 'GLI_EappService', 'GLI_RuleService', 'GLI_EappVariables', '$filter'];

function GLI_SummaryDeclarationController($timeout, $route, globalService,AgentService,$rootScope, $scope, $location, $compile, $routeParams, DataService, RuleService, LookupService, DocumentService, FnaVariables, GLI_FnaService, $translate, UtilityService, $debounce, AutoSave, $controller, EappService, EappVariables, UserDetailsService, PersistenceMapping, GLI_DataService, GLI_EappService, GLI_RuleService, GLI_EappVariables, $filter) {
    $controller('SummaryController', {
        $timeout: $timeout,
        $route: $route,
        globalService: globalService,
        $rootScope: $rootScope,
        $scope: $scope,
        $location: $location,
        $compile: $compile,
        $routeParams: $routeParams,
        DataService: DataService,
        RuleService: RuleService,
        LookupService: LookupService,
        DocumentService: DocumentService,
        FnaVariables: FnaVariables,
        FnaService: GLI_FnaService,
        $translate: $translate,
        UtilityService: UtilityService,
        $debounce: $debounce,
        AutoSave: AutoSave
    });

    /* var unload=$scope.$parent.$on('Declaration_unload', function(event) {
        	var data=$scope.$parent;
            angular.element( document.querySelector( '#Declaration' ) ).empty();
            $scope.$destroy();
            $scope.$parent=data;
            load();
            unload(); 
		});*/

    $scope.alphaNumericEappCustom= rootConfig.alphabetWithSpacePatternIllustration;
    var signature = "Signature";
    var outputFormat = "image/png";
    $scope.isProposalNoAllocated = false;
    $scope.$parent.showProceed = false;
    $scope.isRDSUser = UserDetailsService.getRDSUser();
    $scope.lastVisitedDate = "";
    $scope.appDateTimeFormat = rootConfig.appDateTimeFormat;
    $scope.setSaveproposalDisableFlag = false;
    $scope.alphabeticPattern = rootConfig.namePatternBI;
    $scope.showErrorCount = true;
    $rootScope.firstinitialLoad = true;
    $rootScope.SignatureAvailable = false;
    $rootScope.enableSignaturPad = false;
    $scope.insuredSignatureAvailable = false;
    $scope.gardianSignatureAvailable = false;
    $scope.agentSignatureAvailable = false;
    $scope.witnessSignatureAvailable = false;
    $scope.$parent.disableProceed = false;
    $scope.clearsignaturePadwithDisableOk = false;
    if (!(rootConfig.isDeviceMobile) && (typeof $scope.DocumentType == "undefined")) {
        $scope.DocumentType = "Signature#PolicyHolderSignature";
    }
    $scope.isDeviceWeb = true;
    $scope.proposalNumber = EappVariables.EappKeys.Key21;
    if (!$scope.proposalNumber) {
        $scope.proposalNumber = "******";
    } else {
        $scope.isProposalNoAllocated = true;
        $scope.$parent.showProceed = true;
    }
    if ((rootConfig.isDeviceMobile)) {
        //disabling canvas based on the declaration check
        // $scope.agentStatementCanvas=true;
        //disabling summary page buttons based on the signature check
        // $scope.agentStatementButtonDisable =false;
        $scope.declarationPolicyHolderCanvasDisable = true;
        $scope.declarationPolicyHolderButtonDisable = true;
        $scope.declarationMainInsuredCanvasDisable = true;
        $scope.declarationMainInsuredButtonDisable = true;
        $scope.declarationAgentCanvasDisable = true;
        $scope.declarationAgentButtonDisable = true;
        $scope.declarationWitnessCanvasDisable = true;
        $scope.declarationWitnessButtonDisable = true;
    } else {
        // $scope.agentStatementCanvas=false;
        //  $scope.agentStatementButtonDisable =false;
        $scope.declarationPolicyHolderCanvasDisable = false;
        $scope.declarationPolicyHolderButtonDisable = false;
        $scope.declarationMainInsuredCanvasDisable = false;
        $scope.declarationMainInsuredButtonDisable = false;
        $scope.declarationAgentCanvasDisable = false;
        $scope.declarationAgentButtonDisable = false;
        $scope.declarationWitnessCanvasDisable = false;
        $scope.declarationWitnessButtonDisable = false;
    }
    $rootScope.selectedPage = "Summary";
    $scope.signaturePadPolicyHolder;
    $scope.signaturePadMainInsured;
    $scope.signaturePadAgent;
    $scope.signaturePadWitness;
   // $scope.signaturePadWitness;
    // $scope.disableOkBtnPolicyHolder = true;
    //to translte rider isnured type
    $scope.translate = function (riderType) {
        return $translate.instant("eapp." + riderType);
    }
    /**
     * to translate client declarations
     */

    $scope.translateBasedOnProduct = function (textModel) {
        if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == 20 || $scope.LifeEngageProduct.Product.ProductDetails.productCode == 9 || $scope.LifeEngageProduct.Product.ProductDetails.productCode == 4) {
            return $translate.instant(textModel + "sameProducts");
        } else if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == 1 || $scope.LifeEngageProduct.Product.ProductDetails.productCode == 5) {
            return $translate.instant(textModel + $scope.LifeEngageProduct.Product.ProductDetails.productCode);
        }
    }

    $scope.translateSubDescriptionSummary = function (translatetext) {
        if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == 20 || $scope.LifeEngageProduct.Product.ProductDetails.productCode == 9 || $scope.LifeEngageProduct.Product.ProductDetails.productCode == 4) {
            return $translate.instant(translatetext + "sameProducts");
        } else {
            return $translate.instant(translatetext + '1');
        }
    }
    /**
     * to translate client declarations end
     */

    //unused code
    /*$scope.checkQuestionOptionInArray = function(questionArray){
    	var healthMainQuestions = eval  (questionArray.HealthDetails.Questions);
    	var lifeMainQuestions = eval (questionArray.LifestyleDetails.Questions);
    	var otherMainQuestions = eval (questionArray.Other.Questions);
    	for(i=0; i<healthMainQuestions.length;i++){
    		if(healthMainQuestions[i].option=="Yes"){
    			return true;
    			break;
    		}
    	}
    	for(i=0; i<lifeMainQuestions.length;i++){
    		if(lifeMainQuestions[i].option=="Yes"){
    			return true;
    			break;
    		}
    	}
    	for(i=0; i<otherMainQuestions.length;i++){
    		if(otherMainQuestions[i].option=="Yes"){
    			return true;
    			break;
    		}
    	}					
    	return false; 
    }*/





    $scope.okClick = function () {};
    var numDaysBetween = function (d1, d2) {
        var diff = Math.round(d1.getTime() - d2.getTime());
        return diff / (1000 * 60 * 60 * 24);
    };
    $scope.enableOkBtn = function (inputType, canvasId, index) {
        if (canvasId == "signaturePadPolicyHolder") {

            if($scope.clearsignaturePadwithDisableOk){
                 $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");
            }else{

                    $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').removeAttr("disabled");

                                if ($scope.signaturePadPolicyHolder && !$scope.signaturePadPolicyHolder.isEmpty()) {
                        $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').removeAttr("disabled");
                    } else {
                        //$scope.disableOkBtnPolicyHolder = true;
                        $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");
                    }
                }
            // var enteredDate = LEDate($scope.LifeEngageProduct.Proposer.Declaration.date);
            // today = new Date();
            // if ($scope.LifeEngageProduct.Proposer.Declaration.date) {
            //     var noOfDays = numDaysBetween(today, enteredDate);
            //     if (noOfDays >= 0 && noOfDays < 1) {
            //         // if ($scope.LifeEngageProduct.Proposer.Declaration.place && $scope.LifeEngageProduct.Proposer.Declaration.time) {
            //         if($scope.LifeEngageProduct.Proposer.Declaration.time) {
            //             if ($scope.signaturePadPolicyHolder && !$scope.signaturePadPolicyHolder.isEmpty()) {
            //                 $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').removeAttr("disabled");
            //             } else {
            //                 //$scope.disableOkBtnPolicyHolder = true;
            //                 $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");
            //             }
            //         } else {
            //             $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");
            //         }
            //     } else {
            //         $('.custdatepicker').blur();
            //         $('.cusTimepicker').blur();
            //         $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");
            //         if (inputType == 'date') {
            //             $rootScope.lePopupCtrl.showError(translateMessages($translate,
            //                 "lifeEngage"), translateMessages($translate,
            //                 "maxDays"), translateMessages($translate,
            //                 "fna.ok"), $scope.okClick);
            //             //$scope.disableOkBtnPolicyHolder = true;
            //         }
            //     }
            // } else {
            //     //$scope.disableOkBtnPolicyHolder = true;
            //     $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");
            // }
        }
        if (canvasId == "signaturePadMainInsured") {

            if($scope.clearsignaturePadwithDisableOk){
                $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').attr("disabled", "disabled");


            }else{
                    // $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').removeAttr("disabled");

                    if ($scope.signaturePadMainInsured && !$scope.signaturePadMainInsured.isEmpty()) {
                            $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').removeAttr("disabled");
                        } else {
                            //$scope.disableOkBtnPolicyHolder = true;
                            $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').attr("disabled", "disabled");
                        }

                }

            // var enteredDate = LEDate($scope.LifeEngageProduct.Insured.Declaration.date);
            // today = new Date();
            // if ($scope.LifeEngageProduct.Insured.Declaration.date) {
            //     var noOfDays = numDaysBetween(today, enteredDate);
            //     if (noOfDays >= 0 && noOfDays < 1) {
            //        // if ($scope.LifeEngageProduct.Insured.Declaration.place && $scope.LifeEngageProduct.Insured.Declaration.time) {
            //             if($scope.LifeEngageProduct.Proposer.Declaration.time) {
            //             if ($scope.signaturePadMainInsured && !$scope.signaturePadMainInsured.isEmpty()) {
            //                 $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').removeAttr("disabled");
            //             } else {
            //                 //$scope.disableOkBtnPolicyHolder = true;
            //                 $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').attr("disabled", "disabled");
            //             }
            //         } else {
            //             $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').attr("disabled", "disabled");
            //         }
            //     } else {
            //         $('.custdatepicker').blur();
            //         $('.cusTimepicker').blur();
            //         $scope.LifeEngageProduct.Insured.Declaration.dateandTime = "";
            //         $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').attr("disabled", "disabled");
            //         if (inputType == 'date') {
            //             $rootScope.lePopupCtrl.showError(translateMessages($translate,
            //                 "lifeEngage"), translateMessages($translate,
            //                 "maxDays"), translateMessages($translate,
            //                 "fna.ok"), $scope.okClick);
            //         }
            //     }
            // } else {
            //     //$scope.disableOkBtnPolicyHolder = true;
            //     $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').attr("disabled", "disabled");
            // }
        }
        if (canvasId == "signaturePadAgent") {

            if($scope.clearsignaturePadwithDisableOk){
                $('#DeclarationAndAuthorizationTab button#okBtnAgent').attr("disabled", "disabled");

            }else{

           

            // $('#DeclarationAndAuthorizationTab button#okBtnAgent').removeAttr("disabled");

                      if ($scope.signaturePadAgent && !$scope.signaturePadAgent.isEmpty()) {
                            $('#DeclarationAndAuthorizationTab button#okBtnAgent').removeAttr("disabled");
                        } else {
                            //$scope.disableOkBtnPolicyHolder = true;
                            $('#DeclarationAndAuthorizationTab button#okBtnAgent').attr("disabled", "disabled");
                        }
                }

            // var enteredDate = LEDate($scope.LifeEngageProduct.AgentInfo.Declaration.date);
            // today = new Date();
            // if ($scope.LifeEngageProduct.AgentInfo.Declaration.date) {
            //     var noOfDays = numDaysBetween(today, enteredDate);
            //     if (noOfDays >= 0 && noOfDays < 1) {
            //         //if ($scope.LifeEngageProduct.AgentInfo.Declaration.place && $scope.LifeEngageProduct.AgentInfo.Declaration.time) {
            //              if($scope.LifeEngageProduct.Proposer.Declaration.time) {
            //             if ($scope.signaturePadAgent && !$scope.signaturePadAgent.isEmpty()) {
            //                 $('#DeclarationAndAuthorizationTab button#okBtnAgent').removeAttr("disabled");
            //             } else {
            //                 //$scope.disableOkBtnPolicyHolder = true;
            //                 $('#DeclarationAndAuthorizationTab button#okBtnAgent').attr("disabled", "disabled");
            //             }
            //         } else {
            //             $('#DeclarationAndAuthorizationTab button#okBtnAgent').attr("disabled", "disabled");
            //         }
            //     } else {
            //         $('.custdatepicker').blur();
            //         $('.cusTimepicker').blur();
            //         $('#DeclarationAndAuthorizationTab button#okBtnAgent').attr("disabled", "disabled");
            //         if (inputType == 'date') {
            //             $rootScope.lePopupCtrl.showError(translateMessages($translate,
            //                 "lifeEngage"), translateMessages($translate,
            //                 "maxDays"), translateMessages($translate,
            //                 "fna.ok"), $scope.okClick);
            //         }
            //     }
            // } else {
            //     //$scope.disableOkBtnPolicyHolder = true;
            //     $('#DeclarationAndAuthorizationTab button#okBtnAgent').attr("disabled", "disabled");
            // }
        }


                if (canvasId == "signaturePadWitness") {

                    if($scope.clearsignaturePadwithDisableOk){
                        $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");

                    }else{
                        $('#DeclarationAndAuthorizationTab button#okBtnWitness').removeAttr("disabled");
                    }


                    

            // var enteredDate = LEDate($scope.LifeEngageProduct.signatureWitness.Declaration.date);
            // today = new Date();
            // if ($scope.LifeEngageProduct.signatureWitness.Declaration.date) {
            //     var noOfDays = numDaysBetween(today, enteredDate);
            //     if (noOfDays >= 0 && noOfDays < 1) {
            //         //if ($scope.LifeEngageProduct.signatureWitness.Declaration.place && $scope.LifeEngageProduct.signatureWitness.Declaration.time) {
            //              if($scope.LifeEngageProduct.Proposer.Declaration.time) {
            //             if ($scope.signaturePadWitness && !$scope.signaturePadWitness.isEmpty()) {
            //                 $('#DeclarationAndAuthorizationTab button#okBtnWitness').removeAttr("disabled");
            //             } else {
            //                 //$scope.disableOkBtnPolicyHolder = true;
            //                 $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
            //             }
            //         } else {
            //             $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
            //         }
            //     } else {
            //         $('.custdatepicker').blur();
            //         $('.cusTimepicker').blur();
            //         $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
            //         if (inputType == 'date') {
            //             $rootScope.lePopupCtrl.showError(translateMessages($translate,
            //                 "lifeEngage"), translateMessages($translate,
            //                 "maxDays"), translateMessages($translate,
            //                 "fna.ok"), $scope.okClick);
            //         }
            //     }
            // } else {
            //     //$scope.disableOkBtnPolicyHolder = true;
            //     $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
            // }
        }

        $scope.refresh();
    }
    //To disable specific button to address issue on different flows
    function removeDisableSpecificElementsInScope() {
        $('#Declaration button#clearBtnPolicyHolder').removeAttr("disabled");

    };
    $scope.initialEnableDisableSpecificElementsInScope = function () {
        $scope.declarationPolicyHolderButtonDisable = false;
        $scope.declarationPolicyHolderCanvasDisable = false;
        $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");
        $('#Declaration button#clearBtnPolicyHolder').removeAttr("disabled");
        $scope.declarationMainInsuredButtonDisable = false;
        $scope.declarationMainInsuredCanvasDisable = false;
        $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').attr("disabled", "disabled");
        $('#Declaration button#clearBtnMainInsured').removeAttr("disabled");
        $scope.declarationAgentButtonDisable = false;
        $scope.declarationAgentCanvasDisable = false;
        $('#DeclarationAndAuthorizationTab button#okBtnAgent').attr("disabled", "disabled");
        $('#Declaration button#clearBtnAgent').removeAttr("disabled");
        $scope.declarationWitnessButtonDisable = false;
        $scope.declarationWitnessCanvasDisable = false;
        $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
        $('#Declaration button#clearBtnWitness').removeAttr("disabled");
    };

    function disableSpecificElementsInScope() {
        $('#DeclarationAndAuthorizationTab button#clearBtnPolicyHolder').attr("disabled", "disabled");
    };

    $scope.disableSpecificElementsOnConfirmation = function () {
        /*	if(!$scope.$parent.LifeEngageProduct.Declaration.confirmSignature){
        		$('input[type="text"]').attr("disabled", "disabled");
        		$('input[type="date"]').attr("disabled", "disabled");
        		$('select').not('.languageSelector').attr("disabled", "disabled");
        		$('input[type="radio"]').attr("disabled", "disabled");
        		$('.iradio').addClass('disabled');
        		$('input[type="checkbox"]').attr("disabled", "disabled");
        		$('.icheckbox').addClass('disabled');
        	}*/
    };

    $scope.Initialize = function () {


        $scope.agencyName='';
        $scope.licenseNumber='';
        $scope.isBranchAdminOrGAO=false;
        
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd;
        } 
        if(mm<10){
            mm='0'+mm;
        } 
        var today = yyyy+'-'+mm+'-'+dd;
        $scope.applicationDate = today;

         $(window).scrollTop(0);

        if($scope.LifeEngageProduct.Insured.Declaration == undefined){
            $scope.LifeEngageProduct.Insured.Declaration = {};
        }
     
        if($scope.LifeEngageProduct.Insured.Declaration.dateandTime == undefined || $scope.LifeEngageProduct.Insured.Declaration.dateandTime ==""){
                $scope.LifeEngageProduct.Insured.Declaration.dateandTime = InternalToExternalSummary($scope.applicationDate);
                $scope.loadApplicationDate = true;

            // $scope.LifeEngageProduct.Insured.Declaration.dateandTime = $scope.applicationDate;
        }else{
            if($rootScope.firstinitialLoad){
            	if($scope.LifeEngageProduct.Insured.Declaration.dateandTime.indexOf(" ") >0){
            		var splitTime = $scope.LifeEngageProduct.Insured.Declaration.dateandTime.split(" ");
            		$scope.LifeEngageProduct.Insured.Declaration.dateandTime = splitTime[0];
            	}
                $scope.LifeEngageProduct.Insured.Declaration.dateandTime = InternalToExternalSummary( $scope.LifeEngageProduct.Insured.Declaration.dateandTime);
                $rootScope.firstinitialLoad = false;
            }
             $scope.loadApplicationDate = true;
        }

        if($scope.LifeEngageProduct.Insured.Declaration.dateandTime && $scope.LifeEngageProduct.Insured.Declaration.dateandTime.indexOf('/') == 2){
                $scope.LifeEngageProduct.Insured.Declaration.dateandTime = ExternalToInternal($scope.LifeEngageProduct.Insured.Declaration.dateandTime);
            }
        
        // else{
            
        //         	var test = $scope.LifeEngageProduct.Insured.Declaration.dateandTime.split('-');
        //             test[0] = parseInt(test[0]);
        //             if(test[0] <= 2018){
        //                 $scope.LifeEngageProduct.Insured.Declaration.dateandTime = getFormattedDateThaiSignature($scope.LifeEngageProduct.Insured.Declaration.dateandTime);
        //             }else{
        //                     $scope.LifeEngageProduct.Insured.Declaration.dateandTime =  retrivesummaryifonThai($scope.LifeEngageProduct.Insured.Declaration.dateandTime);
        //                 }

        //     }


        $('#SummaryApplicationDate').attr("disabled", "disabled");
        $('#SummaryApplicationDate').attr('disabled', true);

        
        //$scope.LifeEngageProduct.Insured.Declaration.applicationDate = getFormattedShashDateEnglish($scope.LifeEngageProduct.Insured.Declaration.date);
        $scope.$parent.disableProceedButtonCommom = false;
        $scope.dateTimeDisable = true;
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            //EappVariables.setEappModel($scope.LifeEngageProduct);
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
        }
        $scope.LifeEngageProduct = EappVariables.getEappModel();

        $scope.LifeEngageProduct["signatureWitness"] = {
				"agentId" : "",
				"agentName" : "",
				"agencyName" : "",
				"agentHomeNumber" : "",
				"agentMobileNumber" : "",
				"agentEmailID" : "",
				"Declaration": {
					"dateandTime": "",
					"place":""
				}
        };

        
        //$scope.eAppParentobj.PreviousPage   = "Declaration_unload";
        $scope.eAppParentobj.PreviousPage = "";
        /*if($scope.LifeEngageProduct.Declaration.Questions[6].details != ""){
        	$scope.lastVisitedDate = $scope.LifeEngageProduct.Declaration.Questions[6].details;
        }*/
        if (EappVariables.EappKeys.Key15 == "Confirmed" && $scope.$parent.LifeEngageProduct.Declaration.confirmSignature == false) {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
        } else {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = true;
        }
        if ($scope.LifeEngageProduct.Declaration.agentSignDate && $scope.LifeEngageProduct.Declaration.agentSignDate != "") {
            if (!$scope.isRDSUser) {
                $scope.spajAgentDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.agentSignDate), $scope.appDateTimeFormat);
            } else {
                $scope.agentSignDate = $scope.LifeEngageProduct.Declaration.agentSignDate;
            }
        }

        if ($scope.LifeEngageProduct.Proposer.Declaration && $scope.LifeEngageProduct.Proposer.Declaration.dateandTime) {
            $scope.LifeEngageProduct.Proposer.Declaration.date = LEDate($scope.LifeEngageProduct.Proposer.Declaration.dateandTime);
            $scope.LifeEngageProduct.Proposer.Declaration.time = LEDate($scope.LifeEngageProduct.Proposer.Declaration.dateandTime);
        } else {
            if (!$scope.LifeEngageProduct.Proposer.Declaration || !$scope.LifeEngageProduct.Proposer.Declaration.place) {
                $scope.LifeEngageProduct.Proposer.Declaration = {};
            }
            $scope.LifeEngageProduct.Proposer.Declaration.date = new Date();
            $scope.LifeEngageProduct.Proposer.Declaration.time = new Date();
        }
        if ($scope.LifeEngageProduct.Insured.Declaration && $scope.LifeEngageProduct.Insured.Declaration.dateandTime) {
            $scope.LifeEngageProduct.Insured.Declaration.date = LEDate($scope.LifeEngageProduct.Insured.Declaration.dateandTime);
            $scope.LifeEngageProduct.Insured.Declaration.time = LEDate($scope.LifeEngageProduct.Insured.Declaration.dateandTime);
        } else {
            if (!$scope.LifeEngageProduct.Insured.Declaration || !$scope.LifeEngageProduct.Insured.Declaration.place) {
                $scope.LifeEngageProduct.Insured.Declaration = {};
            }
            $scope.LifeEngageProduct.Insured.Declaration.date = new Date();
            $scope.LifeEngageProduct.Insured.Declaration.time = new Date();
        }
        
        if ($scope.LifeEngageProduct.AgentInfo.Declaration && $scope.LifeEngageProduct.AgentInfo.Declaration.dateandTime) {
            $scope.LifeEngageProduct.AgentInfo.Declaration.date = LEDate($scope.LifeEngageProduct.AgentInfo.Declaration.dateandTime);
            $scope.LifeEngageProduct.AgentInfo.Declaration.time = LEDate($scope.LifeEngageProduct.AgentInfo.Declaration.dateandTime);
        } else {
            if (!$scope.LifeEngageProduct.AgentInfo.Declaration) {
                $scope.LifeEngageProduct.AgentInfo.Declaration = {};
            }
            if (!$scope.LifeEngageProduct.AgentInfo.Declaration.place) {
                $scope.LifeEngageProduct.AgentInfo.Declaration = {};
            }
            $scope.LifeEngageProduct.AgentInfo.Declaration.date = new Date();
            $scope.LifeEngageProduct.AgentInfo.Declaration.time = new Date();
        }


        //function call to show/hide questiannoare tab
        if (typeof $scope.LifeEngageProduct.showHideQuestaionnaireTab == "undefined") {
            $scope.LifeEngageProduct.showHideQuestaionnaireTab = summayQuestionnaireTabShowHide();
            $scope.$parent.LifeEngageProduct.showHideQuestaionnaireTab = $scope.LifeEngageProduct.showHideQuestaionnaireTab;
        }


        $timeout(function () {            
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission" && EappVariables.EappKeys.Key15 != "Confirmed") {
                $scope.setSaveproposalDisableFlag = false;
                $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = true;
                UtilityService.disableAllActionElements();
                $scope.$parent.disableProceed = true;
            }else {
                $scope.setSaveproposalDisableFlag = true;
                UtilityService.removeDisableSpecificElements('DeclarationAndAuthorizationTab');
                if (!$scope.agentStatementButtonDisable) {
                    removeDisableSpecificElementsInScope();
                    $scope.$parent.disableProceed = false;
                }
            }
            if (EappVariables.EappKeys.Key15 == "Confirmed" && $scope.$parent.LifeEngageProduct.Declaration.confirmSignature == true) {
                if (!$scope.LifeEngageProduct.policyHolderConfirmed && !$scope.LifeEngageProduct.mainInsuredConfirmed && !$scope.LifeEngageProduct.agentConfirmed) {
                    UtilityService.removeDisableElements('DeclarationAndAuthorizationTab');
                }
            }
            $scope.dateTimeDisable = true;
            $('#poliyHolderDate').attr("disabled", "disabled");
            $('#poliyHolderTime').attr("disabled", "disabled");
            $('#mainInsuredDate').attr("disabled", "disabled");
            $('#mainInsuredTime').attr("disabled", "disabled");
            $('#additionalInsuredDate0').attr("disabled", "disabled");
            $('#additionalInsuredTime0').attr("disabled", "disabled");
            $('#additionalInsuredDate1').attr("disabled", "disabled");
            $('#additionalInsuredTime1').attr("disabled", "disabled");
            $('#additionalInsuredDate2').attr("disabled", "disabled");
            $('#additionalInsuredTime2').attr("disabled", "disabled");
            $('#additionalInsuredDate3').attr("disabled", "disabled");
            $('#additionalInsuredTime3').attr("disabled", "disabled");
            $('#agentDate').attr("disabled", "disabled");
            $('#agentTime').attr("disabled", "disabled");
            $('#signatureDate').attr("disabled", "disabled");
            $('#signatureTime').attr("disabled", "disabled");
            UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
            $scope.updateErrorCount($scope.selectedTabId);
            $scope.showErrorCount = true;
            $scope.initialEnableDisableSpecificElementsInScope();
            if (EappVariables.EappKeys.TransTrackingID) {
                $scope.getSignature();
            }
            $scope.disableSpecificElementsOnConfirmation();
            if (rootConfig.isDeviceMobile == "false" && $scope.isRDSUser) {
                $scope.isDeviceWeb = false;
            }
			if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
				
                $scope.isBranchAdminOrGAO=true;
                $scope.declarationPolicyHolderCanvasDisable = true;
				$scope.declarationPolicyHolderButtonDisable = true;
				$('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");
				$('#DeclarationAndAuthorizationTab button#clearBtnPolicyHolder').attr("disabled", "disabled");
				$scope.declarationMainInsuredCanvasDisable = true;
				$scope.declarationMainInsuredButtonDisable = true;
				$('#DeclarationAndAuthorizationTab button#okBtnMainInsured').attr("disabled", "disabled");
				$('#DeclarationAndAuthorizationTab button#clearBtnMainInsured').attr("disabled", "disabled");
				$scope.declarationAgentCanvasDisable = true;
				$scope.declarationAgentButtonDisable = true;
				$('#DeclarationAndAuthorizationTab button#okBtnAgent').attr("disabled", "disabled");
				$('#DeclarationAndAuthorizationTab button#clearBtnAgent').attr("disabled", "disabled");
				$scope.declarationWitnessCanvasDisable = true;
				$scope.declarationWitnessButtonDisable = true;
				$scope.declarationSignatureWitnessCanvasDisable=true;
				$scope.declarationSignatureWitnessDisable = true;
				$('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
				$('#DeclarationAndAuthorizationTab button#clearBtnWitness').attr("disabled", "disabled");
             

                PersistenceMapping.clearTransactionKeys();
		        var transactionObj = PersistenceMapping.mapScopeToPersistence({});
			    transactionObj.Key11 = GLI_EappVariables.agentForGAO;
		        AgentService.retrieveAgentProfile(transactionObj, $scope.onRetrieveAgentProfileData, $scope.onRetrieveAgentError);
								
			}
            if((EappVariables.EappKeys.Key34 != undefined && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) || EappVariables.EappKeys.Key15 == 'Submitted' || $scope.LifeEngageProduct.paymentSignStatus == 'Inprogress' || $rootScope.enableSignaturPad == false){
                $('#DeclarationAndAuthorizationTab button#clearBtnMainInsured').attr("disabled", "disabled");
                $('#DeclarationAndAuthorizationTab button#clearBtnPolicyHolder').attr("disabled", "disabled");
                $('#DeclarationAndAuthorizationTab button#clearBtnAgent').attr("disabled", "disabled");
                $('#DeclarationAndAuthorizationTab button#clearBtnWitness').attr("disabled", "disabled");
                $('#ApplicationDate').attr('disabled', 'disabled');
                $('#DeclarationAndAuthorizationTab button#summaryProceedButton').attr("disabled", "disabled");	
                // $scope.declarationMainInsuredCanvasDisable = true;
                // $scope.declarationPolicyHolderCanvasDisable = true;
                // $scope.declarationAgentCanvasDisable = true;
                // $scope.declarationSignatureWitnessCanvasDisable = true;
                $('.signature-pad canvas').attr("disabled", "disabled");
                 $('.signature-pad canvas').addClass("pointer-None");
            }
                        
            if((EappVariables.EappKeys.Key34 != undefined && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) || EappVariables.EappKeys.Key15 == 'Submitted'){
                $('.Declaration-btn').attr("disabled", "disabled");
            }else{
                $('.Declaration-btn').removeAttr("disabled");
            } 
            //  $('#ApplicationDate').attr('disabled', 'disabled');

            if($scope.LifeEngageProduct.ApplicationNo.length == 7){
                $('#ApplicationDate').removeAttr("disabled");
            }else{
                $('#ApplicationDate').attr("disabled", "disabled");
                
            }
            
            if(EappVariables.EappKeys.Key34 != undefined && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null || EappVariables.EappKeys.Key15 == 'Submitted'){
                $('#InsuredApplicationNo').attr('disabled', true);
                $('#GardianName').attr('disabled', true);
                $('#LegalAgentOfApplicant').attr('disabled', true);
                $('#WitnessName').attr('disabled', true);  
                $('#FatherorMother').attr('disabled', true);  
                $('#ApplicationDate').attr('disabled', true);  
                $('#SummaryPlace').attr('disabled', true);  
                $scope.declarationPolicyHolderCanvasDisable = true;
                $scope.declarationMainInsuredCanvasDisable = true;
                $scope.declarationAgentCanvasDisable = true;
                $scope.declarationWitnessCanvasDisable = true;
            }
            
            if($scope.LifeEngageProduct.Insured.BasicDetails.age < 20){
                $rootScope.radioValidations();
            }  
           
            
        }, 0);




        $scope.proposalNumber = EappVariables.EappKeys.Key21;
        if (!$scope.proposalNumber) {
            $scope.proposalNumber = "******";
        } else if (EappVariables.EappKeys.Key15 && EappVariables.EappKeys.Key15 == "Confirmed") {
            $scope.isProposalNoAllocated = true;
        }
        //For disabling and enabling the tabs.And also to know the last visited page.---starts
        $scope.LifeEngageProduct.LastVisitedUrl = "4,3,'DeclarationAndAuthorizationTab',true,'DeclarationAndAuthorizationTab',''";
        $scope.eAppParentobj.nextPage = "5,0,'Payment',true,'tabsinner16',''";
        $scope.eAppParentobj.CurrentPage = "";
        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[1] < 3 && subIndex[0] <= $scope.LifeEngageProduct.LastVisitedUrl[0]) {
                $scope.LifeEngageProduct.LastVisitedIndex = "4,3";
            }
        }

        if ($scope.LifeEngageProduct.Declaration.clientdeclarationAgreed != "Yes" && (rootConfig.isDeviceMobile)) {
            // $scope.agentStatementCanvas =true;
        }
        //For disabling and enabling the tabs.And also to know the last visited page.---ends
        $scope.agentDetail = UserDetailsService.getUserDetailsModel();
        //Agent Name mapping for Written By
        $scope.LifeEngageProduct.Insured.Declaration.writtenBy=angular.copy($scope.agentDetail.agentName);
        
        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        $scope.updateErrorCount($scope.selectedTabId);
        $scope.showErrorCount = true;
        if ($scope.LifeEngageProduct.Declaration.isFinalSubmit) {
            $scope.isSTPSuccess = true;
            $scope.isSTPExecuted = true;
            $scope.STPMessage = translateMessages($translate, "stpRulePassed");
        } else {
            $scope.isSTPSuccess = false;
            $scope.isSTPExecuted = false;
        }
        $scope.proposalId = EappVariables.EappKeys.Key4;
    };


    $scope.ApplicationDateBlur = function(){
        $scope.AplicationEditableDateBlur = true;
    }

    //to auto-populate place from proposer to insureds  
    $scope.copyPlaceFromProposer = function () {
        if ($scope.LifeEngageProduct.Proposer.Declaration && $scope.LifeEngageProduct.Insured.Declaration && $scope.LifeEngageProduct.Proposer.Declaration.place && !$scope.LifeEngageProduct.Insured.Declaration.place) {
            $scope.LifeEngageProduct.Insured.Declaration.place = angular.copy($scope.LifeEngageProduct.Proposer.Declaration.place);
            if ($scope.LifeEngageProduct.AgentInfo.Declaration && !$scope.LifeEngageProduct.AgentInfo.Declaration.place) {
                $scope.LifeEngageProduct.AgentInfo.Declaration.place = angular.copy($scope.LifeEngageProduct.Proposer.Declaration.place);
            }
        }
    };

    var signatureString;

    $scope.loadSignature = function (base64, signatureType, success) {
        
        if (base64 && base64 != '') {
            signatureString = base64;
        }
        if (angular.element('#signaturePadPolicyHolder')[0] && signatureType == 'PolicyHolderSignature') {
            $scope.LifeEngageProduct.policyHolderConfirmed = false;
            if (base64) {
                $scope.signaturePadPolicyHolder = new SignaturePad(angular.element('#signaturePadPolicyHolder')[0].querySelector("canvas"));
                $scope.gardianSignatureAvailable = true;
                $scope.signaturePadPolicyHolder.onEnd = function () {
                    $scope.enableOkBtn('sign', 'signaturePadPolicyHolder');
                };
                var dpr = window.devicePixelRatio;
                window.devicePixelRatio = 1;
                if ($scope.signaturePadPolicyHolder && signatureType == 'PolicyHolderSignature') {
                    $scope.signaturePadPolicyHolder.clear();
                    if (signatureString)
                        $scope.signaturePadPolicyHolder.fromDataURL(signatureString);
                    $scope.LifeEngageProduct.policyHolderConfirmed = true;
                } else {
                    if (!(rootConfig.isDeviceMobile) && $scope.signaturePad) {
                        $scope.signaturePad.clear();
                    }
                }
                window.devicePixelRatio = dpr;


                if (signatureString && $scope.LifeEngageProduct.Proposer.Declaration.date && $scope.LifeEngageProduct.Proposer.Declaration.place) {

                    $scope.setSaveproposalDisableFlag = false;
                    //$scope.agentStatementButtonDisable =true;
                    //$scope.agentStatementCanvas=true;
                    $scope.declarationPolicyHolderCanvasDisable = true;
                    $scope.declarationPolicyHolderButtonDisable = true;
                    $('#DeclarationAndAuthorizationTab button#clearBtnPolicyHolder').attr("disabled", "disabled");
                    $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");
                } else {
                    //if($scope.LifeEngageProduct.Declaration.policyHolderDate && $scope.LifeEngageProduct.Declaration.policyHolderDate!=''){
                    //$scope.agentStatementButtonDisable =false;
                    //$scope.agentStatementCanvas=false;

                     if((EappVariables.EappKeys.Key34 != undefined && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) || EappVariables.EappKeys.Key15 == 'Submitted'){
                         $scope.declarationPolicyHolderCanvasDisable = true;
                    $scope.declarationPolicyHolderButtonDisable = true;
                    }else{
                        $scope.declarationPolicyHolderCanvasDisable = false;
                        $scope.declarationPolicyHolderButtonDisable = false;
                    }

                    $('#Declaration button#clearBtnPolicyHolder').removeAttr("disabled");
                    $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");
                    if (!$scope.LifeEngageProduct.policyHolderConfirmed) {
                        $scope.LifeEngageProduct.Proposer.Declaration.date = new Date();
                        $scope.LifeEngageProduct.Proposer.Declaration.time = new Date();
                    }
                    //}
                }
                /* if(!$scope.declarationPolicyHolderButtonDisable){
                        removeDisableSpecificElementsInScope();
        			} else{
        					disableSpecificElementsInScope();
        			}*/
                //$scope.enableOkBtn('signaturePadPolicyHolder');
                signature = '';
                $scope.refresh();
            } else {
                $scope.signaturePadPolicyHolder = new SignaturePad(angular.element('#signaturePadPolicyHolder')[0].querySelector("canvas"));
                $scope.signaturePadPolicyHolder.onEnd = function () {
                    $scope.clearsignaturePadwithDisableOk = false;
                    $scope.enableOkBtn('sign', 'signaturePadPolicyHolder');
                };
            }
        }
        if (angular.element('#signaturePadMainInsured')[0] && signatureType == 'MainInsuredSignature') {
            $scope.LifeEngageProduct.mainInsuredConfirmed = false;
            if (base64) {
                $scope.signaturePadMainInsured = new SignaturePad(angular.element('#signaturePadMainInsured')[0].querySelector("canvas"));
                $scope.insuredSignatureAvailable = true;
                $scope.signaturePadMainInsured.onEnd = function () {
                    $scope.enableOkBtn('sign', 'signaturePadMainInsured');
                };
                var dpr = window.devicePixelRatio;
                window.devicePixelRatio = 1;
                if ($scope.signaturePadMainInsured && signatureType == 'MainInsuredSignature') {
                    $scope.signaturePadMainInsured.clear();
                    if (signatureString)
                        $scope.signaturePadMainInsured.fromDataURL(signatureString);
                    $scope.LifeEngageProduct.mainInsuredConfirmed = true;
                } else {
                    if (!(rootConfig.isDeviceMobile) && $scope.signaturePad) {
                        $scope.signaturePad.clear();
                    }
                }
                window.devicePixelRatio = dpr;


                if (signatureString && $scope.LifeEngageProduct.Insured.Declaration.date && $scope.LifeEngageProduct.Insured.Declaration.place) {

                    $scope.setSaveproposalDisableFlag = false;
                    //$scope.agentStatementButtonDisable =true;
                    //$scope.agentStatementCanvas=true;

                    $scope.declarationMainInsuredCanvasDisable = true;
                    $scope.declarationMainInsuredButtonDisable = true;
                   // $('#DeclarationAndAuthorizationTab button#clearBtnMainInsured').attr("disabled", "disabled");
                    $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').attr("disabled", "disabled");
                } else {
                    //if($scope.LifeEngageProduct.Declaration.mainInsuredDate && $scope.LifeEngageProduct.Declaration.mainInsuredDate!=''){
                    //$scope.agentStatementButtonDisable =false;
                    //$scope.agentStatementCanvas=false;
                if((EappVariables.EappKeys.Key34 != undefined && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) || EappVariables.EappKeys.Key15 == 'Submitted'){
                              $scope.declarationMainInsuredCanvasDisable = true;
                                $scope.declarationMainInsuredButtonDisable = true;
                    }else{
                              $scope.declarationMainInsuredCanvasDisable = false;
                                $scope.declarationMainInsuredButtonDisable = false;
                    }
                    $scope.declarationMainInsuredCanvasDisable = false;
                    $scope.declarationMainInsuredButtonDisable = false;
                    $('#Declaration button#clearBtnMainInsured').removeAttr("disabled");
                    $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').attr("disabled", "disabled");
                    if (!$scope.LifeEngageProduct.mainInsuredConfirmed) {
                        $scope.LifeEngageProduct.Insured.Declaration.date = new Date();
                        $scope.LifeEngageProduct.Insured.Declaration.time = new Date();
                    }
                    //}
                }
                /*if(!$scope.declarationMainInsuredButtonDisable){
        				//$scope.removeDisableSpecificElementsInScope();
                    	$('#DeclarationAndAuthorizationTab button#clearBtnMainInsured').removeAttr("disabled");
        			} else{
        					//disableSpecificElementsInScope();
        				$('#DeclarationAndAuthorizationTab button#clearBtnMainInsured').attr("disabled", "disabled");
        			}*/
                // $scope.enableOkBtn('signaturePadMainInsured');
                signature = '';
                $scope.refresh();
            } else {
                $scope.signaturePadMainInsured = new SignaturePad(angular.element('#signaturePadMainInsured')[0].querySelector("canvas"));
                
                $scope.signaturePadMainInsured.onEnd = function () {
                    $scope.clearsignaturePadwithDisableOk = false;
                    $scope.enableOkBtn('sign', 'signaturePadMainInsured');
                };
            }
        }

        if (angular.element('#signaturePadAgent')[0] && signatureType == 'AgentSignature') {
            $scope.LifeEngageProduct.agentConfirmed = false;
            if (base64) {
                $scope.signaturePadAgent = new SignaturePad(angular.element('#signaturePadAgent')[0].querySelector("canvas"));
                $scope.agentSignatureAvailable = true;
                $scope.signaturePadAgent.onEnd = function () {
                    $scope.enableOkBtn('sign', 'signaturePadAgent');
                };
                var dpr = window.devicePixelRatio;
                window.devicePixelRatio = 1;
                if ($scope.signaturePadAgent && signatureType == 'AgentSignature') {
                    $scope.signaturePadAgent.clear();
                    if (signatureString)
                        $scope.signaturePadAgent.fromDataURL(signatureString);
                    $scope.LifeEngageProduct.agentConfirmed = true;
                } else {
                    if (!(rootConfig.isDeviceMobile) && $scope.signaturePad) {
                        $scope.signaturePad.clear();
                    }
                }
                window.devicePixelRatio = dpr;


                if (signatureString && $scope.LifeEngageProduct.AgentInfo.Declaration.date && $scope.LifeEngageProduct.AgentInfo.Declaration.place) {

                    $scope.setSaveproposalDisableFlag = false;
                    //$scope.agentStatementButtonDisable =true;
                    //$scope.agentStatementCanvas=true;
                    $scope.declarationAgentCanvasDisable = true;
                    $scope.declarationAgentButtonDisable = true;
                    $('#DeclarationAndAuthorizationTab button#clearBtnAgent').attr("disabled", "disabled");
                    $('#DeclarationAndAuthorizationTab button#okBtnAgent').attr("disabled", "disabled");
                } else {
                    //if($scope.LifeEngageProduct.Declaration.agentDate && $scope.LifeEngageProduct.AgentInfo.Declaration.dateandTime!=''){
                    //$scope.agentStatementButtonDisable =false;
                    //$scope.agentStatementCanvas=false;

                if((EappVariables.EappKeys.Key34 != undefined && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) || EappVariables.EappKeys.Key15 == 'Submitted'){
                                $scope.declarationAgentCanvasDisable = true;
                                $scope.declarationAgentButtonDisable = true;
                    }else{
                                $scope.declarationAgentCanvasDisable = false;
                                $scope.declarationAgentButtonDisable = false;
                    }

                    $('#Declaration button#clearBtnAgent').removeAttr("disabled");
                    $('#DeclarationAndAuthorizationTab button#okBtnAgent').attr("disabled", "disabled");
                    if (!$scope.LifeEngageProduct.agentConfirmed) {
                        $scope.LifeEngageProduct.AgentInfo.Declaration.date = new Date();
                        $scope.LifeEngageProduct.AgentInfo.Declaration.time = new Date();
                    }
                    //}
                }
                /* if(!$scope.declarationAgentButtonDisable){
        				//$scope.removeDisableSpecificElementsInScope();
                    	$('#DeclarationAndAuthorizationTab button#clearBtnAgent').removeAttr("disabled");
        			} else{
        					//disableSpecificElementsInScope();
        				$('#DeclarationAndAuthorizationTab button#clearBtnAgent').attr("disabled", "disabled");
        			}*/
                //$scope.enableOkBtn('signaturePadAgent');
                signature = '';
                $scope.refresh();
            } else {
                $scope.signaturePadAgent = new SignaturePad(angular.element('#signaturePadAgent')[0].querySelector("canvas"));
                $scope.signaturePadAgent.onEnd = function () {
                    $scope.clearsignaturePadwithDisableOk = false;
                    $scope.enableOkBtn('sign', 'signaturePadAgent');
                };
            }
        }


         if (angular.element('#signaturePadWitness')[0] && signatureType == 'signatureWitness') {
            $scope.LifeEngageProduct.signatureWitnessConfirmed = false;
            if (base64) {
                $scope.signaturePadWitness = new SignaturePad(angular.element('#signaturePadWitness')[0].querySelector("canvas"));
                $scope.witnessSignatureAvailable = true;
                $scope.signaturePadWitness.onEnd = function () {
                    $scope.clearsignaturePadwithDisableOk = false;
                    $scope.enableOkBtn('sign', 'signaturePadWitness');
                };
                var dpr = window.devicePixelRatio;
                window.devicePixelRatio = 1;
                if ($scope.signaturePadWitness && signatureType == 'signatureWitness') {
                    $scope.signaturePadWitness.clear();
                    if (signatureString)
                        $scope.signaturePadWitness.fromDataURL(signatureString);
                    $scope.LifeEngageProduct.signatureWitnessConfirmed = true;
                } else {
                    if (!(rootConfig.isDeviceMobile) && $scope.signaturePad) {
                        $scope.signaturePad.clear();
                    }
                }
                window.devicePixelRatio = dpr;


                if (signatureString && $scope.LifeEngageProduct.AgentInfo.Declaration.date && $scope.LifeEngageProduct.AgentInfo.Declaration.place) {

                    $scope.setSaveproposalDisableFlag = false;
                    //$scope.agentStatementButtonDisable =true;
                    //$scope.agentStatementCanvas=true;
                    $scope.declarationSignatureWitnessCanvasDisable = true;
                    $scope.declarationSignatureWitnessDisable = true;
                    $('#DeclarationAndAuthorizationTab button#clearBtnWitness').attr("disabled", "disabled");
                    $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
                } else {
                    //if($scope.LifeEngageProduct.Declaration.agentDate && $scope.LifeEngageProduct.AgentInfo.Declaration.dateandTime!=''){
                    //$scope.agentStatementButtonDisable =false;
                    //$scope.agentStatementCanvas=false;


                if((EappVariables.EappKeys.Key34 != undefined && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) || EappVariables.EappKeys.Key15 == 'Submitted'){
                            $scope.declarationSignatureWitnessCanvasDisable = true;
                            $scope.declarationSignatureWitnessDisable = true;
                    }else{
                            $scope.declarationSignatureWitnessCanvasDisable = false;
                            $scope.declarationSignatureWitnessDisable = false;
                    }

                    $('#Declaration button#clearBtnWitness').removeAttr("disabled");
                    $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
                    if (!$scope.LifeEngageProduct.agentConfirmed) {
                        $scope.LifeEngageProduct.AgentInfo.Declaration.date = new Date();
                        $scope.LifeEngageProduct.AgentInfo.Declaration.time = new Date();
                    }
                    //}
                }
                /* if(!$scope.declarationAgentButtonDisable){
        				//$scope.removeDisableSpecificElementsInScope();
                    	$('#DeclarationAndAuthorizationTab button#clearBtnAgent').removeAttr("disabled");
        			} else{
        					//disableSpecificElementsInScope();
        				$('#DeclarationAndAuthorizationTab button#clearBtnAgent').attr("disabled", "disabled");
        			}*/
                //$scope.enableOkBtn('signaturePadWitness');
                signature = '';
                $scope.refresh();
            } else {
                $scope.signaturePadWitness = new SignaturePad(angular.element('#signaturePadWitness')[0].querySelector("canvas"));
                
                $scope.signaturePadWitness.onEnd = function () {
                    $scope.enableOkBtn('sign', 'signaturePadWitness');
                };
            }
        }
        enableDisableButton();
        if (success) {
            success();
        }
    }
    $scope.setDefaultDuration = function () {
        if ($scope.LifeEngageProduct.Declaration.Questions[0].option == 'Yes') {
            $scope.LifeEngageProduct.Declaration.Questions[0].options[0].details = 0;
        } else {
            $scope.LifeEngageProduct.Declaration.Questions[0].options[0].details = '';
        }
        $scope.refresh();
        setTimeout(function () {
            $scope.updateErrorCount('Declaration');
            $scope.refresh();
        }, 0);
    }

    $scope.clearSignature = function (signatureId) {

        if (signatureId == "signaturePadPolicyHolder") {

            $scope.clearsignaturePadwithDisableOk = true;
            $scope.gardianSignatureAvailable = false;
            $scope.declarationPolicyHolderButtonDisable = false;
            // if($scope.LifeEngageProduct.Declaration.clientdeclarationAgreed=='Yes'  && !$scope.LifeEngageProduct.Declaration.agentSignDate && $scope.LifeEngageProduct.Declaration.agentSignDate=='')
           // if (!$scope.LifeEngageProduct.Proposer.Declaration.date && $scope.LifeEngageProduct.Proposer.Declaration.date == '') {
                $scope.declarationPolicyHolderCanvasDisable = false;
           // }
            if ($scope.signaturePadPolicyHolder) {
                $scope.signaturePadPolicyHolder.clear();
                $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");
            } else {
                if (!(rootConfig.isDeviceMobile) && $scope.signaturePad) {
                    $scope.signaturePad.clear();
                }
            }
            return false;
        }
        if (signatureId == "signaturePadMainInsured") {
            $scope.clearsignaturePadwithDisableOk = true;
            $scope.insuredSignatureAvailable = false;
            $scope.declarationMainInsuredButtonDisable = false;
            // if($scope.LifeEngageProduct.Declaration.clientdeclarationAgreed=='Yes'  && !$scope.LifeEngageProduct.Declaration.agentSignDate && $scope.LifeEngageProduct.Declaration.agentSignDate=='')
           // if (!$scope.LifeEngageProduct.Insured.Declaration.date && $scope.LifeEngageProduct.Insured.Declaration.date == '') {
                $scope.declarationMainInsuredCanvasDisable = false;
           // }
            if ($scope.signaturePadMainInsured) {
                $scope.signaturePadMainInsured.clear();
                $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').attr("disabled", "disabled");
            } else {
                if (!(rootConfig.isDeviceMobile) && $scope.signaturePad) {
                    $scope.signaturePad.clear();
                }
            }
            return false;
        }
        if (signatureId == "signaturePadAgent") {

            $scope.clearsignaturePadwithDisableOk = true;
            $scope.agentSignatureAvailable = false;
            $scope.declarationAgentCanvasDisable = false;
            // if($scope.LifeEngageProduct.Declaration.clientdeclarationAgreed=='Yes'  && !$scope.LifeEngageProduct.Declaration.agentSignDate && $scope.LifeEngageProduct.Declaration.agentSignDate=='')
           // if (!$scope.LifeEngageProduct.AgentInfo.Declaration.date && $scope.LifeEngageProduct.AgentInfo.Declaration.date == '') {
                $scope.declarationAgentCanvasDisable = false;
          // }
            if ($scope.signaturePadAgent) {
                $scope.signaturePadAgent.clear();
                $('#DeclarationAndAuthorizationTab button#okBtnAgent').attr("disabled", "disabled");
            } else {
                if (!(rootConfig.isDeviceMobile) && $scope.signaturePad) {
                    $scope.signaturePad.clear();
                }
            }
            return false;
        }

        if (signatureId == "signaturePadWitness") {

            $scope.clearsignaturePadwithDisableOk = true;
            $scope.witnessSignatureAvailable = false;
            $scope.declarationSignatureWitnessCanvasDisable = false;
            // if($scope.LifeEngageProduct.Declaration.clientdeclarationAgreed=='Yes'  && !$scope.LifeEngageProduct.Declaration.agentSignDate && $scope.LifeEngageProduct.Declaration.agentSignDate=='')
           // if (!$scope.LifeEngageProduct.signatureWitness.Declaration.date && $scope.LifeEngageProduct.signatureWitness.Declaration.date == '') {
                $scope.declarationSignatureWitnessCanvasDisable = false;
           // }
            if ($scope.signaturePadWitness) {
                $scope.signaturePadWitness.clear();
                $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
            } else {
                if (!(rootConfig.isDeviceMobile) && $scope.signaturePad) {
                    $scope.signaturePad.clear();
                }
            }
            return false;
        }

        // if (signatureId == "signaturePadWitness") {
        //     $scope.declarationWitnessCanvasDisable = false;
        //     // if($scope.LifeEngageProduct.Declaration.clientdeclarationAgreed=='Yes'  && !$scope.LifeEngageProduct.Declaration.agentSignDate && $scope.LifeEngageProduct.Declaration.agentSignDate=='')
        //     if (!$scope.LifeEngageProduct.AgentInfo.Declaration.date && $scope.LifeEngageProduct.AgentInfo.Declaration.date == '') {
        //         $scope.declarationWitnessCanvasDisable = false;
        //     }
        //     if ($scope.signaturePadWitness) {
        //         $scope.signaturePadWitness.clear();
        //         $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
        //     } else {
        //         if (!(rootConfig.isDeviceMobile) && $scope.signaturePadWitness) {
        //             $scope.signaturePad.clear();
        //         }
        //     }
        // }
        $scope.refresh();
    };
    
    $scope.renderSuccess = function (i) {
        i++;
        if (i < EappVariables.Signature.length) {
            $scope.sigantureRender(i, EappVariables.Signature[i], $scope.renderSuccess);
        }

    }
    $scope.getSignature = function (successCallback) {
        if ((rootConfig.isDeviceMobile)) {
            EappService.getSignature(EappVariables.EappKeys.TransTrackingID, signature, function (sigdata) {
                EappVariables.Signature = sigdata;
                var base64;
                $scope.loadSignature(base64, 'PolicyHolderSignature');
                $scope.loadSignature(base64, 'MainInsuredSignature');
                $scope.loadSignature(base64, 'AgentSignature');
                $scope.loadSignature(base64, 'signatureWitness');
                if (sigdata.length > 0) {
                    var i = 0;
                    $scope.sigantureRender(i, sigdata[i], $scope.renderSuccess);
                }
            });
        } else {

            var base64;
            $scope.loadSignature(base64, 'PolicyHolderSignature');
            $scope.loadSignature(base64, 'MainInsuredSignature');
            $scope.loadSignature(base64, 'AgentSignature');
            $scope.loadSignature(base64, 'signatureWitness');
            var requirementObject = CreateRequirementFile();
            requirementObject.RequirementFile.identifier = EappVariables.EappKeys.TransTrackingID;
            if (EappVariables.EappModel.Requirements.length > 0) {
                for (var i = 0; i < EappVariables.EappModel.Requirements.length; i++) {
                    if (EappVariables.EappModel.Requirements[i].requirementType === "Signature") {
                        for (var j = 0; j < EappVariables.EappModel.Requirements[i].Documents.length; j++) {
                            var value = $scope.DocumentType.split("#");
                            //if(EappVariables.EappModel.Requirements[i].Documents[j].documentName.indexOf(value[1])>0){
                            requirementObject.RequirementFile.documentName = EappVariables.EappModel.Requirements[i].Documents[j].documentName;
                            requirementObject.RequirementFile.requirementName = EappVariables.EappModel.Requirements[i].requirementName;
                            PersistenceMapping.clearTransactionKeys();
                            EappService.mapKeysforPersistence();
                            var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
                            DataService.getDocumentsForRequirement(transactionObj, function (sigdata) {
                                getDocsSuccess(sigdata, i)
                            }, $scope.errorCallback);
                            //}
                        }
                        break;
                    }
                }
            }
        }
    }

    function getDocsSuccessCallBack (index) {
        index++
        var requirementObject = CreateRequirementFile();
        requirementObject.RequirementFile.identifier = EappVariables.EappKeys.TransTrackingID;
        if (index < EappVariables.EappModel.Requirements.length) {
            for (var i = index; i < EappVariables.EappModel.Requirements.length; i++) {
                if (EappVariables.EappModel.Requirements[i].requirementType === "Signature") {
                    for (var j = 0; j < EappVariables.EappModel.Requirements[i].Documents.length; j++) {
                        var value = $scope.DocumentType.split("#");
                        //if(EappVariables.EappModel.Requirements[i].Documents[j].documentName.indexOf(value[1])>0){
                        requirementObject.RequirementFile.documentName = EappVariables.EappModel.Requirements[i].Documents[j].documentName;
                        requirementObject.RequirementFile.requirementName = EappVariables.EappModel.Requirements[i].requirementName;
                        PersistenceMapping.clearTransactionKeys();
                        EappService.mapKeysforPersistence();
                        var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
                        DataService.getDocumentsForRequirement(transactionObj, function (sigdata) {
                            getDocsSuccess(sigdata, i);
                        }, $scope.errorCallback);
                        //}
                    }
                    break;
                }
                $rootScope.showHideLoadingImage(false, "");
            }
        }
    }
    $scope.errorCallback = function () {
        $rootScope.showHideLoadingImage(false, "");
    }
    $scope.sigantureRender = function (index, sigdata, successCall) {
        if (sigdata) {
            if (sigdata.documentName.indexOf("Signature") >= 0) {
                signatureType = sigdata.documentName.split("_")[2];
                //if(signatureType.localeCompare(pageName)==0){
                if ($scope.setSaveproposalDisableFlag) {
                    $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
                    $scope.disableSpecificElementsOnConfirmation();
                }
                $scope.$parent.refresh();
                //EappService.convertImgToBase64(sigdata[i].base64string, successcallback, outputFormat);

                EappService.convertImgToBase64(sigdata.base64string, function (base64String) {

                    $scope.loadSignature(base64String, signatureType, function () {
                        successCall(index);
                    });

                }, outputFormat);

                //}
                //else{
                //	if(i==(sigdata.length-1)){
                //		successCallback()
                //	}
                //}
            }
        } else {
            successCall(index);
        }

    }
    function getDocsSuccess (sigdata, index) {
        $scope.sigantureRender(index, sigdata[0], getDocsSuccessCallBack);
        if ($scope.signaturePad && sigdata) {
            $scope.signaturePad.fromDataURL(sigdata[0].base64string);
        }
    }

    function enableDisableButton() {

        if ($scope.LifeEngageProduct.policyHolderConfirmed == true && $scope.LifeEngageProduct.mainInsuredConfirmed == true && $scope.LifeEngageProduct.agentConfirmed == true) {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
        } else {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = true;
        }
        $scope.LifeEngageProduct.Declaration.confirmSignature = $scope.$parent.LifeEngageProduct.Declaration.confirmSignature;
    }
    $scope.confirmProposal = function (sigData) {

        if ($scope.LifeEngageProduct.Insured.IncomeDetails && !($scope.LifeEngageProduct.Insured.IncomeDetails.annualIncome)) {
            $scope.LifeEngageProduct.Insured.IncomeDetails.annualIncome = "";
        }
        // added for prev button in payment
        // authorisation
        $scope.confirmForPaymentAuth = true;

        var requirementType = "Signature";
        var index = "";
        var requirementName = "Signature";
        var partyIdentifier = "";
        var isMandatory = "";
        var docArray = [];
        var documentType = $scope.DocumentType;
        var documentTypeChk = "";

        if ((rootConfig.isDeviceMobile)) {
            if ($scope.canvasId == "signaturePadPolicyHolder") {
                documentType = "Signature#PolicyHolderSignature";
                if(sigData){
                    $scope.gardianSignatureAvailable = true;
                }else{
                    $scope.gardianSignatureAvailable = false;
                }
                $scope.LifeEngageProduct.policyHolderConfirmed = false;
                EappService.getRequirementFilePath(sigData, index, requirementName, requirementType, documentType, function (filePath) {
                    if ((rootConfig.isDeviceMobile) || rootConfig.isOfflineDesktop) {
                        //filePath = "../" + filePath;
                    }
                    EappService.saveRequirement(filePath, requirementType, partyIdentifier, isMandatory, docArray, documentType, function () {
                        $scope.declarationPolicyHolderCanvasDisable = true;
                        $scope.declarationPolicyHolderButtonDisable = true;
                        $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");
                        $('#DeclarationAndAuthorizationTab button#clearBtnPolicyHolder').attr("disabled", "disabled");
                        $scope.LifeEngageProduct.policyHolderConfirmed = true;
                        enableDisableButton();
                        $("#summaryProceedButton").removeAttr('disabled');
                        $rootScope.showHideLoadingImage(false);
                        $scope.refresh();
                    }, $scope.onConfirmError);
                }, $scope.onConfirmError);
                $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");

            } else if ($scope.canvasId == "signaturePadMainInsured") {
                documentType = "Signature#MainInsuredSignature";
                 if(sigData){
                    $scope.insuredSignatureAvailable = true;
                }else{
                    $scope.insuredSignatureAvailable = false;
                }
                $scope.LifeEngageProduct.mainInsuredConfirmed = false;
                EappService.getRequirementFilePath(sigData, index, requirementName, requirementType, documentType, function (filePath) {
                    if ((rootConfig.isDeviceMobile) || rootConfig.isOfflineDesktop) {
                        // filePath = "../" + filePath;
                    }
                    EappService.saveRequirement(filePath, requirementType, partyIdentifier, isMandatory, docArray, documentType, function () {
                        $scope.declarationMainInsuredCanvasDisable = true;
                        $scope.declarationMainInsuredButtonDisable = true;
                        $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').attr("disabled", "disabled");
                       // $('#DeclarationAndAuthorizationTab button#clearBtnMainInsured').attr("disabled", "disabled");
                        $scope.LifeEngageProduct.mainInsuredConfirmed = true;
                        enableDisableButton();
                        $("#summaryProceedButton").removeAttr('disabled');
                        $rootScope.showHideLoadingImage(false);
                        $scope.refresh();
                        }, $scope.onConfirmError);
                }, $scope.onConfirmError);
                $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').attr("disabled", "disabled");
            } else if ($scope.canvasId == "signaturePadAgent") {
                documentType = "Signature#AgentSignature";
                 if(sigData){
                    $scope.agentSignatureAvailable = true;
                }else{
                    $scope.agentSignatureAvailable = false;
                }
                $scope.LifeEngageProduct.agentConfirmed = false;
                EappService.getRequirementFilePath(sigData, index, requirementName, requirementType, documentType, function (filePath) {
                    if ((rootConfig.isDeviceMobile) || rootConfig.isOfflineDesktop) {
                       // filePath = "../" + filePath;
                    }
                    EappService.saveRequirement(filePath, requirementType, partyIdentifier, isMandatory, docArray, documentType, function () {
                        $scope.declarationAgentCanvasDisable = true;
                        $scope.declarationAgentButtonDisable = true;
                        $('#DeclarationAndAuthorizationTab button#okBtnAgent').attr("disabled", "disabled");
                        $('#DeclarationAndAuthorizationTab button#clearBtnAgent').attr("disabled", "disabled");
                        $scope.LifeEngageProduct.agentConfirmed = true;
                        enableDisableButton();
                        $("#summaryProceedButton").removeAttr('disabled');
                        $rootScope.showHideLoadingImage(false);
                        $scope.refresh();
                   }, $scope.onConfirmError);
                }, $scope.onConfirmError);
                $('#DeclarationAndAuthorizationTab button#okBtnAgent').attr("disabled", "disabled");
            }
            else if ($scope.canvasId == "signaturePadWitness") {
                documentType = "Signature#signatureWitness";
                    if(sigData){
                        $scope.witnessSignatureAvailable = true;
                    }else{
                        $scope.witnessSignatureAvailable = false;
                    }
                $scope.LifeEngageProduct.signatureWitnessConfirmed = false;
                EappService.getRequirementFilePath(sigData, index, requirementName, requirementType, documentType, function (filePath) {
                    if ((rootConfig.isDeviceMobile) || rootConfig.isOfflineDesktop) {
                       // filePath = "../" + filePath;
                    }
                    EappService.saveRequirement(filePath, requirementType, partyIdentifier, isMandatory, docArray, documentType, function () {
                        $scope.declarationSignatureWitnessCanvasDisable = true;
                        $scope.declarationSignatureWitnessDisable = true;
                        $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
                        $('#DeclarationAndAuthorizationTab button#clearBtnWitness').attr("disabled", "disabled");
                        $scope.LifeEngageProduct.signatureWitnessConfirmed = true;
                        enableDisableButton();
                        $("#summaryProceedButton").removeAttr('disabled');
                        $rootScope.showHideLoadingImage(false);
                        $scope.refresh();
                        }, $scope.onConfirmError);
                }, $scope.onConfirmError);
                $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
                
            } 
            // else if ($scope.canvasId == "signaturePadWitness") {
            //     documentType = "Signature#WitnessSignature";
            //     $scope.LifeEngageProduct.witnessConfirmed = false;
            //     EappService.getRequirementFilePath(sigData, index, requirementName, requirementType, documentType, function (filePath) {
            //         if ((rootConfig.isDeviceMobile) || rootConfig.isOfflineDesktop) {
            //            // filePath = "../" + filePath;
            //         }
            //         EappService.saveRequirement(filePath, requirementType, partyIdentifier, isMandatory, docArray, documentType, function () {
            //             $scope.declarationWitnessCanvasDisable = true;
            //             $scope.declarationWitnessButtonDisable = true;
            //             $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
            //             $('#DeclarationAndAuthorizationTab button#clearBtnWitness').attr("disabled", "disabled");
            //             $scope.LifeEngageProduct.witnessConfirmed = true;
            //             enableDisableButton();
            //             $("#summaryProceedButton").removeAttr('disabled');
            //             $rootScope.showHideLoadingImage(false);
            //             $scope.refresh();
            //         }, $scope.onConfirmError);
            //     }, $scope.onConfirmError);
            // }
        } else {
            if ($scope.canvasId == "signaturePadPolicyHolder") {
                documentType = "Signature#PolicyHolderSignature";
                documentTypeChk = "PolicyHolderSignature";
                if(sigData){
                    $scope.gardianSignatureAvailable = true;
                }else{
                    $scope.gardianSignatureAvailable = false;
                }
                $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");
            } else if ($scope.canvasId == "signaturePadMainInsured") {
                documentType = "Signature#MainInsuredSignature";
                documentTypeChk = "MainInsuredSignature";
                if(sigData){
                    $scope.insuredSignatureAvailable = true;
                }else{
                    $scope.insuredSignatureAvailable = false;
                }
            } else if ($scope.canvasId == "signaturePadAgent") {
                documentType = "Signature#AgentSignature";
                documentTypeChk = "AgentSignature";
                if(sigData){
                    $scope.agentSignatureAvailable = true;
                }else{
                    $scope.agentSignatureAvailable = false;
                }
            } 
            else if ($scope.canvasId == "signaturePadWitness") {
                documentType = "Signature#signatureWitness";
                documentTypeChk = "signatureWitness";
                if(sigData){
                    $scope.witnessSignatureAvailable = true;
                }else{
                    $scope.witnessSignatureAvailable = false;
                }
            }

            for (var q = $scope.LifeEngageProduct.Requirements.length - 1; q >= 0; q--) {
                if (documentTypeChk === $scope.LifeEngageProduct.Requirements[q].requirementSubType) {
                    $scope.LifeEngageProduct.Requirements.splice(q, 1);
                }
            }

            EappService.saveRequirement(sigData, requirementType, partyIdentifier, isMandatory, docArray, documentType, function () {
                $scope.disableOnOkclick();
            }, function () {
                $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
                $("#summaryProceedButton").removeAttr('disabled');
                $rootScope.showHideLoadingImage(false);
                //$scope.onConfirmError();
                if ($scope.canvasId == "signaturePadWitness" && sigData) {
                    $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
                }


            });
        }
        $scope.disableSpecificElementsOnConfirmation();
    };

    $rootScope.validationBeforesave = function (value, successcallback) {
        $scope.validateBeforeSaveDetails(value, successcallback);
    };
    $scope.onConfirmError = function () {
        if ($scope.canvasId == "signaturePadPolicyHolder") {
            $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').removeAttr("disabled");
            $('#DeclarationAndAuthorizationTab button#clearBtnPolicyHolder').removeAttr("disabled");
        } else if ($scope.canvasId == "signaturePadMainInsured") {
            $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').removeAttr("disabled");
            $('#DeclarationAndAuthorizationTab button#clearBtnMainInsured').removeAttr("disabled");
        } else if ($scope.canvasId == "signaturePadAgent") {
            $('#DeclarationAndAuthorizationTab button#okBtnAgent').removeAttr("disabled");
            $('#DeclarationAndAuthorizationTab button#clearBtnAgent').removeAttr("disabled");
        }
        else if ($scope.canvasId == "signaturePadWitness") {
            $('#DeclarationAndAuthorizationTab button#okBtnWitness').removeAttr("disabled");
            $('#DeclarationAndAuthorizationTab button#clearBtnWitness').removeAttr("disabled");
        }
        $("#summaryProceedButton").removeAttr('disabled');
        $rootScope.showHideLoadingImage(false);
        $scope.refresh();

        if($scope.gardianSignatureAvailable){
            $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");
        }
        if($scope.insuredSignatureAvailable){
            $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').attr("disabled", "disabled");
        }
        if($scope.agentSignatureAvailable){
             $('#DeclarationAndAuthorizationTab button#okBtnAgent').attr("disabled", "disabled");
        }
        if($scope.witnessSignatureAvailable){
             $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
        }
    }
    $scope.disableOnOkclick = function () {
        if ($scope.canvasId == "signaturePadPolicyHolder") {
            $scope.declarationPolicyHolderCanvasDisable = true;
            $scope.declarationPolicyHolderButtonDisable = true;
            $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");
            // $('#DeclarationAndAuthorizationTab button#clearBtnPolicyHolder').attr("disabled", "disabled");
            $scope.LifeEngageProduct.policyHolderConfirmed = true;
            $scope.$parent.saveFromSignatureConfirm = "true";
            $scope.saveProposal(false);
        } else if ($scope.canvasId == "signaturePadMainInsured") {
            $scope.declarationMainInsuredCanvasDisable = true;
            $scope.declarationMainInsuredButtonDisable = true;
            $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').attr("disabled", "disabled");
           // $('#DeclarationAndAuthorizationTab button#clearBtnMainInsured').attr("disabled", "disabled");
            $scope.LifeEngageProduct.mainInsuredConfirmed = true;
            $scope.$parent.saveFromSignatureConfirm = "true";
            $scope.saveProposal(false);
        } else if ($scope.canvasId == "signaturePadAgent") {
            $scope.declarationAgentCanvasDisable = true;
            $scope.declarationAgentButtonDisable = true;
            $('#DeclarationAndAuthorizationTab button#okBtnAgent').attr("disabled", "disabled");
           // $('#DeclarationAndAuthorizationTab button#clearBtnAgent').attr("disabled", "disabled");
            $scope.LifeEngageProduct.agentConfirmed = true;
            $scope.$parent.saveFromSignatureConfirm = "true";
            $scope.saveProposal(false);
        }
        else if ($scope.canvasId == "signaturePadWitness") {
            $scope.declarationSignatureWitnessCanvasDisable = true;
            $scope.declarationSignatureWitnessDisable = true;
            $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
           // $('#DeclarationAndAuthorizationTab button#clearBtnWitness').attr("disabled", "disabled");
            $scope.LifeEngageProduct.signatureWitnessConfirmed = true;
            $scope.$parent.saveFromSignatureConfirm = "true";
            $scope.saveProposal(false);
        }

        enableDisableButton();
        $scope.refresh();
    }

    function subDateCal() {
        $scope.subDate = LEDate($scope.LifeEngageProduct.Proposer.Declaration.date);

        var date = new Date($scope.subDate);
        var newdate = new Date(date);

        newdate.setDate(newdate.getDate() + 4);
        $scope.submissionDeadline = date;
        var dd = newdate.getDate();
        var mm = newdate.getMonth() + 1;
        var yyyy = newdate.getFullYear();

        $scope.dateToshow = dd + '/' + mm + '/' + yyyy;

    }
    $scope.SPAJAllocation = function (sucess) {
        // SPAJ allocation logic
        if (rootConfig.isDeviceMobile) {
            if (!$scope.$parent.LifeEngageProduct.Declaration.confirmSignature) {
                subDateCal();
                if (EappVariables.EappKeys.Key15 == "New" || EappVariables.EappKeys.Key15 == undefined || EappVariables.EappKeys.Key15 == "undefined") {
//                    EappVariables.EappKeys.Key15 = "Confirmed";
                }
                EappVariables.EappKeys.Key26 = getFormattedDateSummary($scope.submissionDeadline);
                // if (!EappVariables.EappKeys.Key21 || EappVariables.EappKeys.Key21 == '') {
                //     $scope.lePopupCtrl.showWarning(
                //         translateMessages($translate,
                //             "lifeEngage"),
                //         translateMessages($translate,
                //             "proposalAccept") + EappVariables.EappKeys.Key21 + translateMessages($translate, "proposalSubmit") + $scope.dateToshow + "",
                //         translateMessages($translate,
                //             "fna.ok"));
                // }
            }

            //EappVariables.eAppProceedingDate = UtilityService.getFormattedDate();
            $scope.isProposalNoAllocated = true;
            $scope.$parent.showProceed = true;
            $scope.proposalNumber = EappVariables.EappKeys.Key21;
            $scope.refresh();
            $scope.LifeEngageProduct.LastVisitedUrl = "4,3,'DeclarationAndAuthorizationTab',true,'DeclarationAndAuthorizationTab',''";
            sucess();
        } else {
            if (!$scope.$parent.LifeEngageProduct.Declaration.confirmSignature) {
                if (EappVariables.EappKeys.Key15 == "New" || EappVariables.EappKeys.Key15 == undefined || EappVariables.EappKeys.Key15 == "undefined") {
//                    EappVariables.EappKeys.Key15 = "Confirmed";
                }
                // if ($scope.$parent.saveFromSignatureConfirm === "false") {

                //     subDateCal();
                //     $scope.lePopupCtrl.showWarning(
                //         translateMessages($translate,
                //             "lifeEngage"),
                //         translateMessages($translate,
                //             "proposalAccept") + EappVariables.EappModel.ApplicationNo + translateMessages($translate, "proposalSubmit") + $scope.dateToshow + "",
                //         translateMessages($translate,
                //             "fna.ok"));

                // }
            }
            $scope.proposalNumber = EappVariables.EappModel.ApplicationNo;
            $scope.isProposalNoAllocated = true;
            $scope.$parent.showProceed = true;
            $scope.$parent.isProposalNoAllocated = true;
            //EappVariables.eAppProceedingDate = UtilityService.getFormattedDate();
            $scope.refresh();
            $scope.LifeEngageProduct.LastVisitedUrl = "4,3,'DeclarationAndAuthorizationTab',true,'DeclarationAndAuthorizationTab',''";
            sucess();
        }
    }


     $scope.removeElement = function(){
        $('.list-signature-errors').remove();
        $('.msg-content-popup span').removeClass('span-hide');
         $('#eSignature,#summaryProceedButton,.btn_container_tlr_fluid').removeClass('backdropclick');
    } 

    $scope.validateBeforeSaveDetails = function (value, successcallback) {
        $scope.LifeEngageProduct.LastVisitedUrl = "4,4,'Declaration',true,'Declaration',''";		

		if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin) {
			
            if($scope.LifeEngageProduct.Insured.Declaration.dateandTime && $scope.LifeEngageProduct.Insured.Declaration.dateandTime.indexOf('/') == 2){
                $scope.LifeEngageProduct.Insured.Declaration.dateandTime = ExternalToInternal($scope.LifeEngageProduct.Insured.Declaration.dateandTime);
            }
            $scope.SPAJAllocation(function (){
                successcallback();
            });			
		} 
		else {
            if($scope.LifeEngageProduct.ApplicationNo.length == 7){
                            if($scope.LifeEngageProduct.Insured.Declaration.dateandTime && $scope.LifeEngageProduct.Insured.Declaration.dateandTime.indexOf('/') == 2){
                                $scope.LifeEngageProduct.Insured.Declaration.dateandTime = ExternalToInternal($scope.LifeEngageProduct.Insured.Declaration.dateandTime);
                            }
                        // $('.msg-content-popup').removeClass('validation-message'); 
                        // $('.msg-content-popup').html(translateMessages($translate,"eapp_vt.summaryApplicationConfirmPopup"));
                            $scope.SPAJAllocation(function (){
                                successcallback();
                            });

            }else{
                    var signatureMsg=translateMessages($translate, "eapp_vt.checkSign");
                            if($rootScope.enableSignaturPad == false){
                                $rootScope.showHideLoadingImage(false, "");
                                $("#summaryProceedButton").removeAttr('disabled');
                                            $rootScope.lePopupCtrl.showError(translateMessages($translate,
                                                    "lifeEngage"), translateMessages($translate,
                                                    "eapp_vt.previewPdfPrompt"), translateMessages($translate,
                                                    "fna.ok"));
                                                    return;
                            } 
                        

                        if(!$scope.insuredSignatureAvailable && $scope.LifeEngageProduct.Insured.BasicDetails.age >= 12){
                        // var signatureMsg="eapp_vt.checkSign";
                                $("#summaryProceedButton").removeAttr('disabled');
                                $rootScope.showHideLoadingImage(false, "");

                                $('.msg-content-popup span').addClass('span-hide');
                                // $scope.lePopupCtrl.showWarning(translateMessages($translate,
                                // "lifeEngage"), translateMessages($translate,
                                // $('.validation-message').html(translateMessages($translate, signatureMsg))), translateMessages($translate,
                                // "fna.ok"));


                                $scope.lePopupCtrl.showWarning(
                                            translateMessages($translate,
                                                    "lifeEngage"),
                                            translateMessages($translate,
                                                    $('.msg-content-popup').append(signatureMsg)),  
                                            translateMessages($translate,
                                                    "fna.ok"),$scope.removeElement); 

                                $('#eSignature,#summaryProceedButton,.btn_container_tlr_fluid').addClass('backdropclick');

                

                                if($scope.insuredSignatureAvailable || !($scope.LifeEngageProduct.Insured.BasicDetails.age >= 12)){
                                    $('.list-signature-errors .error-1').hide();
                                }
                                if($scope.gardianSignatureAvailable || !($scope.LifeEngageProduct.Insured.BasicDetails.age < 20)){
                                    $('.list-signature-errors .error-2').hide();
                                }
                                if($scope.agentSignatureAvailable){
                                    $('.list-signature-errors .error-3').hide();
                                }
                                if($scope.witnessSignatureAvailable){
                                    $('.list-signature-errors .error-4').hide();
                                }else if($scope.LifeEngageProduct.Insured.BasicDetails.TrineeAgent != 'Yes' && (($scope.LifeEngageProduct.AgentInfo.securityNo != $scope.LifeEngageProduct.Payer.BasicDetails.IDcard) || ($scope.LifeEngageProduct.AgentInfo.securityNo != $scope.LifeEngageProduct.Insured.BasicDetails.IDcard))){
                                    $('.list-signature-errors .error-4').hide();
                                }
                        
                                
                        }else if(!$scope.gardianSignatureAvailable && $scope.LifeEngageProduct.Insured.BasicDetails.age < 20){
                                //var signatureMsg="eapp_vt.checkSign";

                                $("#summaryProceedButton").removeAttr('disabled');
                                $rootScope.showHideLoadingImage(false, "");
                                $('.msg-content-popup span').addClass('span-hide');
                                $scope.lePopupCtrl.showWarning(
                                            translateMessages($translate,
                                                    "lifeEngage"),
                                            translateMessages($translate,
                                                    $('.msg-content-popup').append(signatureMsg)),  
                                            translateMessages($translate,
                                                    "fna.ok"),$scope.removeElement); 
                                    $('#eSignature,#summaryProceedButton,.btn_container_tlr_fluid').addClass('backdropclick');

                                if($scope.insuredSignatureAvailable || !($scope.LifeEngageProduct.Insured.BasicDetails.age >= 12)){
                                    $('.list-signature-errors .error-1').hide();
                                }
                                if($scope.gardianSignatureAvailable || !($scope.LifeEngageProduct.Insured.BasicDetails.age < 20)){
                                    $('.list-signature-errors .error-2').hide();
                                }
                                if($scope.agentSignatureAvailable){
                                    $('.list-signature-errors .error-3').hide();
                                }
                                if($scope.witnessSignatureAvailable){
                                    $('.list-signature-errors .error-4').hide();
                                }else if($scope.LifeEngageProduct.Insured.BasicDetails.TrineeAgent != 'Yes' && (($scope.LifeEngageProduct.AgentInfo.securityNo != $scope.LifeEngageProduct.Payer.BasicDetails.IDcard) || ($scope.LifeEngageProduct.AgentInfo.securityNo != $scope.LifeEngageProduct.Insured.BasicDetails.IDcard))){
                                    $('.list-signature-errors .error-4').hide();
                                }
                            

                        }else if(!$scope.agentSignatureAvailable){
                            // var signatureMsg="eapp_vt.checkSign";

                                $("#summaryProceedButton").removeAttr('disabled');
                                $rootScope.showHideLoadingImage(false, "");
                                $('.msg-content-popup span').addClass('span-hide');
                                $scope.lePopupCtrl.showWarning(
                                            translateMessages($translate,
                                                    "lifeEngage"),
                                            translateMessages($translate,
                                                    $('.msg-content-popup').append(signatureMsg)),  
                                            translateMessages($translate,
                                                    "fna.ok"),$scope.removeElement); 
                                $('#eSignature,#summaryProceedButton,.btn_container_tlr_fluid').addClass('backdropclick');

                                if($scope.insuredSignatureAvailable || !($scope.LifeEngageProduct.Insured.BasicDetails.age >= 12)){
                                    $('.list-signature-errors .error-1').hide();
                                }
                                if($scope.gardianSignatureAvailable || !($scope.LifeEngageProduct.Insured.BasicDetails.age < 20)){
                                    $('.list-signature-errors .error-2').hide();
                                }
                                if($scope.agentSignatureAvailable){
                                    $('.list-signature-errors .error-3').hide();
                                }
                                if($scope.witnessSignatureAvailable){
                                    $('.list-signature-errors .error-4').hide();
                                }else if($scope.LifeEngageProduct.Insured.BasicDetails.TrineeAgent != 'Yes' && (($scope.LifeEngageProduct.AgentInfo.securityNo != $scope.LifeEngageProduct.Payer.BasicDetails.IDcard) || ($scope.LifeEngageProduct.AgentInfo.securityNo != $scope.LifeEngageProduct.Insured.BasicDetails.IDcard))){
                                    $('.list-signature-errors .error-4').hide();
                                }
                        

                        }else if( !$scope.witnessSignatureAvailable && ($scope.LifeEngageProduct.Insured.BasicDetails.TrineeAgent == 'Yes' || (($scope.LifeEngageProduct.AgentInfo.securityNo == $scope.LifeEngageProduct.Payer.BasicDetails.IDcard) || ($scope.LifeEngageProduct.AgentInfo.securityNo == $scope.LifeEngageProduct.Insured.BasicDetails.IDcard)))){

                            //  var signatureMsg="eapp_vt.checkSign";
                                $("#summaryProceedButton").removeAttr('disabled');
                                $rootScope.showHideLoadingImage(false, "");
                                $('.msg-content-popup span').addClass('span-hide');
                                $rootScope.showHideLoadingImage(false, "");
                                $scope.lePopupCtrl.showWarning(
                                            translateMessages($translate,
                                                    "lifeEngage"),
                                            translateMessages($translate,
                                                    $('.msg-content-popup').append(signatureMsg)),  
                                            translateMessages($translate,
                                                    "fna.ok"),$scope.removeElement); 
                                            
                                $('#eSignature,#summaryProceedButton,.btn_container_tlr_fluid').addClass('backdropclick');
                                if($scope.insuredSignatureAvailable || !($scope.LifeEngageProduct.Insured.BasicDetails.age >= 12)){
                                    $('.list-signature-errors .error-1').hide();
                                }
                                if($scope.gardianSignatureAvailable || !($scope.LifeEngageProduct.Insured.BasicDetails.age < 20)){
                                    $('.list-signature-errors .error-2').hide();
                                }
                                if($scope.agentSignatureAvailable){
                                    $('.list-signature-errors .error-3').hide();
                                }
                                if($scope.witnessSignatureAvailable){
                                    $('.list-signature-errors .error-4').hide();
                                }else if($scope.LifeEngageProduct.Insured.BasicDetails.TrineeAgent != 'Yes' && (($scope.LifeEngageProduct.AgentInfo.securityNo != $scope.LifeEngageProduct.Payer.BasicDetails.IDcard) || ($scope.LifeEngageProduct.AgentInfo.securityNo != $scope.LifeEngageProduct.Insured.BasicDetails.IDcard))){
                                    $('.list-signature-errors .error-4').hide();
                                }
                            

                        } else{ 
                            
                            if($scope.LifeEngageProduct.Insured.Declaration.dateandTime && $scope.LifeEngageProduct.Insured.Declaration.dateandTime.indexOf('/') == 2){
                                $scope.LifeEngageProduct.Insured.Declaration.dateandTime = ExternalToInternal($scope.LifeEngageProduct.Insured.Declaration.dateandTime);
                            }
                        // $('.msg-content-popup').removeClass('validation-message'); 
                        // $('.msg-content-popup').html(translateMessages($translate,"eapp_vt.summaryApplicationConfirmPopup"));
                            $scope.SPAJAllocation(function (){
                                successcallback();
                            });

                        }
            }
		}
    }

    var load = $scope.$parent.$on('DeclarationAndAuthorizationTab', function (event, args) {
        $scope.selectedTabId = args[0];
        if ($scope.LifeEngageProduct.LastVisitedIndex && args[1]) {
            $scope.LifeEngageProduct.LastVisitedIndex = args[1];
        }
        $scope.Initialize();
        $rootScope.validationBeforesave = function (value, successcallback) {
            $scope.validateBeforeSaveDetails(value, successcallback);
        };
    });

    function formatTime(time) {
        var timeArray = time.split(":");
        var formatedTime = "";
        for (var i = 0; i < timeArray.length; i++) {
            if (timeArray[i].length == 1) {
                timeArray[i] = "0" + timeArray[i];
            }
            if (i != timeArray.length - 1) {
                formatedTime = formatedTime + timeArray[i] + ":";
            } else {
                formatedTime = formatedTime + timeArray[i];
            }
        }
        return formatedTime;
    };
    $scope.confirmDeclaration = function (canvasId) {
        $("#summaryProceedButton").attr("disabled", "disabled");
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
        $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = true;
        $scope.canvasId = canvasId;
        $scope.updateErrorCount('DeclarationAndAuthorizationTab');
        if (canvasId == "signaturePadPolicyHolder") {
            $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').attr("disabled", "disabled");
           // $('#DeclarationAndAuthorizationTab button#clearBtnPolicyHolder').attr("disabled", "disabled");
            var poliyHolderDate = "";
            var poliyHolderTime = "";
            $scope.LifeEngageProduct.Proposer.Declaration.time = new Date();
            if ($scope.LifeEngageProduct.Proposer.Declaration.date && $scope.LifeEngageProduct.Proposer.Declaration.time) {
                if (typeof $scope.LifeEngageProduct.Proposer.Declaration.time == "object") {
                    poliyHolderTime = formatForTimeControl($scope.LifeEngageProduct.Proposer.Declaration.time, false);
                } else {
                    poliyHolderTime = formatTime($scope.LifeEngageProduct.Proposer.Declaration.time);
                }
                poliyHolderDate = formatForDateControl($scope.LifeEngageProduct.Proposer.Declaration.date);
            }
            if (poliyHolderDate && poliyHolderTime) {
                $scope.LifeEngageProduct.Proposer.Declaration.dateandTime = poliyHolderDate + " " + poliyHolderTime;
            }
            if (rootConfig.isDeviceMobile) {
                // if ($scope.errorCount > 0) {
                //     $("#summaryProceedButton").removeAttr('disabled');
                //     $rootScope.showHideLoadingImage(false);
                //     $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').removeAttr("disabled");
                //     $('#DeclarationAndAuthorizationTab button#clearBtnPolicyHolder').removeAttr("disabled");
                //     $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                //         translateMessages($translate, "enterMandatoryFieldsForeApp"),
                //         translateMessages($translate, "fna.ok"));
                // } else {
                    if ($scope.signaturePadPolicyHolder && !$scope.signaturePadPolicyHolder.isEmpty()) {
                        $scope.LifeEngageProduct.Declaration.agentSignDate = UtilityService.getFormattedDate();

                        $scope.spajAgentDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.agentSignDate), $scope.appDateTimeFormat);
                        $scope.confirmProposal($scope.signaturePadPolicyHolder.toDataURL());
                    } else {
                        $("#summaryProceedButton").removeAttr('disabled');
                        $rootScope.showHideLoadingImage(false);
                        $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').removeAttr("disabled");
                        $('#DeclarationAndAuthorizationTab button#clearBtnPolicyHolder').removeAttr("disabled");
                        $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.declarationAgreedMessage"), translateMessages($translate, "fna.ok"));
                    }
               // }
            } else {
               // if ($scope.errorCount == 0) {
                    $scope.LifeEngageProduct.Declaration.agentSignDate = UtilityService.getFormattedDate();
                    $scope.spajAgentDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.agentSignDate), $scope.appDateTimeFormat);
                    if ($scope.signaturePadPolicyHolder)
                        $scope.confirmProposal($scope.signaturePadPolicyHolder.toDataURL());
               // } 
                
                
                // else {
                //     $("#summaryProceedButton").removeAttr('disabled');
                //     $rootScope.showHideLoadingImage(false);
                //     $('#DeclarationAndAuthorizationTab button#okBtnPolicyHolder').removeAttr("disabled");
                //     $('#DeclarationAndAuthorizationTab button#clearBtnPolicyHolder').removeAttr("disabled");
                //     $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                //     translateMessages($translate, "enterMandatoryFieldsForeApp"),
                //     translateMessages($translate, "fna.ok"));
                // }
            }
        }
        if (canvasId == "signaturePadMainInsured") {
            $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').attr("disabled", "disabled");
            //$('#DeclarationAndAuthorizationTab button#clearBtnMainInsured').attr("disabled", "disabled");
            var mainInsuredDate = "";
            var mainInsuredTime = "";
            //$scope.LifeEngageProduct.Insured.Declaration.time = new Date();
            if ($scope.LifeEngageProduct.Insured.Declaration.date && $scope.LifeEngageProduct.Insured.Declaration.time) {
                if (typeof $scope.LifeEngageProduct.Insured.Declaration.time == "object") {
                    mainInsuredTime = formatForTimeControl($scope.LifeEngageProduct.Insured.Declaration.time, false);
                } else {
                    mainInsuredTime = formatTime($scope.LifeEngageProduct.Insured.Declaration.time);
                }
                mainInsuredDate = formatForDateControl($scope.LifeEngageProduct.Insured.Declaration.date);
            }
            // if (mainInsuredDate && mainInsuredTime) {
            //     $scope.LifeEngageProduct.Insured.Declaration.dateandTime = mainInsuredDate + " " + mainInsuredTime;
            // }
            if ((rootConfig.isDeviceMobile)) {
                // if ($scope.errorCount > 0) {
                //     $("#summaryProceedButton").removeAttr('disabled');
                //     $rootScope.showHideLoadingImage(false);
                //     $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').removeAttr("disabled");
                //     $('#DeclarationAndAuthorizationTab button#clearBtnMainInsured').removeAttr("disabled");
                //     $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                //         translateMessages($translate, "enterMandatoryFieldsForeApp"),
                //         translateMessages($translate, "fna.ok"));
                // } else {
                    if ($scope.signaturePadMainInsured && !$scope.signaturePadMainInsured.isEmpty()) {
                        $scope.LifeEngageProduct.Declaration.agentSignDate = UtilityService.getFormattedDate();

                        $scope.spajAgentDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.agentSignDate), $scope.appDateTimeFormat);
                        $scope.confirmProposal($scope.signaturePadMainInsured.toDataURL());
                    } else {
                        $("#summaryProceedButton").removeAttr('disabled');
                        $rootScope.showHideLoadingImage(false);
                        $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').removeAttr("disabled");
                        $('#DeclarationAndAuthorizationTab button#clearBtnMainInsured').removeAttr("disabled");
                        $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.declarationAgreedMessage"), translateMessages($translate, "fna.ok"));
                    }
               // }
            } else {
               // if ($scope.errorCount == 0) {
                    $scope.LifeEngageProduct.Declaration.agentSignDate = UtilityService.getFormattedDate();
                    $scope.spajAgentDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.agentSignDate), $scope.appDateTimeFormat);
                    if ($scope.signaturePadMainInsured)
                        $scope.confirmProposal($scope.signaturePadMainInsured.toDataURL());
                // } else {
                //     $("#summaryProceedButton").removeAttr('disabled');
                //     $rootScope.showHideLoadingImage(false);
                //     $('#DeclarationAndAuthorizationTab button#okBtnMainInsured').removeAttr("disabled");
                //     $('#DeclarationAndAuthorizationTab button#clearBtnMainInsured').removeAttr("disabled");
                //     $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                //     translateMessages($translate, "enterMandatoryFieldsForeApp"),
                //     translateMessages($translate, "fna.ok"));
                // }
            }
        }
        if (canvasId == "signaturePadAgent") {
            
            $('#DeclarationAndAuthorizationTab button#okBtnAgent').attr("disabled", "disabled");
           // $('#DeclarationAndAuthorizationTab button#clearBtnAgent').attr("disabled", "disabled");
            // var agentDate = "";
            // var agentTime = "";
            // $scope.LifeEngageProduct.AgentInfo.Declaration.time = new Date();
            // if ($scope.LifeEngageProduct.AgentInfo.Declaration.date && $scope.LifeEngageProduct.AgentInfo.Declaration.time) {
            //     if (typeof $scope.LifeEngageProduct.AgentInfo.Declaration.time == "object") {
            //         agentTime = formatForTimeControl($scope.LifeEngageProduct.AgentInfo.Declaration.time, false);
            //     } else {
            //         agentTime = formatTime($scope.LifeEngageProduct.AgentInfo.Declaration.time);
            //     }
            //     agentDate = formatForDateControl($scope.LifeEngageProduct.AgentInfo.Declaration.date);
            // }
            // if (agentDate && agentTime) {
            //     $scope.LifeEngageProduct.AgentInfo.Declaration.dateandTime = agentDate + " " + agentTime;
            // }
            if (rootConfig.isDeviceMobile) {
                // if ($scope.errorCount > 0) {
                //     $("#summaryProceedButton").removeAttr('disabled');
                //     $rootScope.showHideLoadingImage(false);
                //     $('#DeclarationAndAuthorizationTab button#okBtnAgent').removeAttr("disabled");
                //     $('#DeclarationAndAuthorizationTab button#clearBtnAgent').removeAttr("disabled");
                //     $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                //         translateMessages($translate, "enterMandatoryFieldsForeApp"),
                //         translateMessages($translate, "fna.ok"));
                // } else {
                    if ($scope.signaturePadAgent && !$scope.signaturePadAgent.isEmpty()) {
                        $scope.LifeEngageProduct.Declaration.agentSignDate = UtilityService.getFormattedDate();
                        $scope.spajAgentDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.agentSignDate), $scope.appDateTimeFormat);

                        $scope.confirmProposal($scope.signaturePadAgent.toDataURL());
                    } else {
                        $("#summaryProceedButton").removeAttr('disabled');
                        $rootScope.showHideLoadingImage(false);
                        $('#DeclarationAndAuthorizationTab button#okBtnAgent').removeAttr("disabled");
                        $('#DeclarationAndAuthorizationTab button#clearBtnAgent').removeAttr("disabled");
                        $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.declarationAgreedMessage"), translateMessages($translate, "fna.ok"));
                    }
               // }
            } else {
               // if ($scope.errorCount == 0) {
                    $scope.LifeEngageProduct.Declaration.agentSignDate = UtilityService.getFormattedDate();
                    $scope.spajAgentDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.agentSignDate), $scope.appDateTimeFormat);
                    //$scope.LifeEngageProduct.Declaration.confirmSignature = false;
                    if ($scope.signaturePadAgent)
                        $scope.confirmProposal($scope.signaturePadAgent.toDataURL());
                // } else {
                //     $("#summaryProceedButton").removeAttr('disabled');
                //     $rootScope.showHideLoadingImage(false);
                //     $('#DeclarationAndAuthorizationTab button#okBtnAgent').removeAttr("disabled");
                //     $('#DeclarationAndAuthorizationTab button#clearBtnAgent').removeAttr("disabled");
                //         $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                //         translateMessages($translate, "enterMandatoryFieldsForeApp"),
                //         translateMessages($translate, "fna.ok"));
                // }
            }
        }
        


         if (canvasId == "signaturePadWitness") {
            $('#DeclarationAndAuthorizationTab button#okBtnWitness').attr("disabled", "disabled");
           // $('#DeclarationAndAuthorizationTab button#clearBtnWitness').attr("disabled", "disabled");
            var agentDate = "";
            var agentTime = "";
            $scope.LifeEngageProduct.signatureWitness.Declaration.time = new Date();
            if ($scope.LifeEngageProduct.signatureWitness.Declaration.date && $scope.LifeEngageProduct.signatureWitness.Declaration.time) {
                if (typeof $scope.LifeEngageProduct.signatureWitness.Declaration.time == "object") {
                    agentTime = formatForTimeControl($scope.LifeEngageProduct.signatureWitness.Declaration.time, false);
                } else {
                    agentTime = formatTime($scope.LifeEngageProduct.signatureWitness.Declaration.time);
                }
                agentDate = formatForDateControl($scope.LifeEngageProduct.signatureWitness.Declaration.date);
            }
            if (agentDate && agentTime) {
                $scope.LifeEngageProduct.signatureWitness.Declaration.dateandTime = agentDate + " " + agentTime;
            }
            if (rootConfig.isDeviceMobile) {
                // if ($scope.errorCount > 0) {
                //     $("#summaryProceedButton").removeAttr('disabled');
                //     $rootScope.showHideLoadingImage(false);
                //     $('#DeclarationAndAuthorizationTab button#okBtnWitness').removeAttr("disabled");
                //     $('#DeclarationAndAuthorizationTab button#clearBtnWitness').removeAttr("disabled");
                //     $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                //         translateMessages($translate, "enterMandatoryFieldsForeApp"),
                //         translateMessages($translate, "fna.ok"));
                // } else {
                    if ($scope.signaturePadWitness && !$scope.signaturePadWitness.isEmpty()) {
                        $scope.LifeEngageProduct.Declaration.agentSignDate = UtilityService.getFormattedDate();
                        $scope.spajAgentDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.agentSignDate), $scope.appDateTimeFormat);

                        $scope.confirmProposal($scope.signaturePadWitness.toDataURL());
                    } else {
                        $("#summaryProceedButton").removeAttr('disabled');
                        $rootScope.showHideLoadingImage(false);
                        $('#DeclarationAndAuthorizationTab button#okBtnWitness').removeAttr("disabled");
                        $('#DeclarationAndAuthorizationTab button#clearBtnWitness').removeAttr("disabled");
                        $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "general.declarationAgreedMessage"), translateMessages($translate, "fna.ok"));
                    }
               // }
            } else {
               // if ($scope.errorCount == 0) {
                    $scope.LifeEngageProduct.Declaration.agentSignDate = UtilityService.getFormattedDate();
                    $scope.spajAgentDateDevice = $filter('date')(LEDate($scope.LifeEngageProduct.Declaration.agentSignDate), $scope.appDateTimeFormat);
                    //$scope.LifeEngageProduct.Declaration.confirmSignature = false;
                    if ($scope.signaturePadWitness)
                        $scope.confirmProposal($scope.signaturePadWitness.toDataURL());
                // } else {
                //     $("#summaryProceedButton").removeAttr('disabled');
                //     $rootScope.showHideLoadingImage(false);
                //     $('#DeclarationAndAuthorizationTab button#okBtnWitness').removeAttr("disabled");
                //     $('#DeclarationAndAuthorizationTab button#clearBtnWitness').removeAttr("disabled");
                //         $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                //         translateMessages($translate, "enterMandatoryFieldsForeApp"),
                //         translateMessages($translate, "fna.ok"));
                // }
            }
        }

        if($scope.AplicationEditableDateBlur){
            $scope.LifeEngageProduct.Insured.Declaration.dateandTime = InternalToExternalSummary($scope.LifeEngageProduct.Insured.Declaration.dateandTime);
            $scope.AplicationEditableDateBlur = false;
            if(!$scope.loadApplicationDate){
                if($scope.LifeEngageProduct.Insured.Declaration.dateandTime && $scope.LifeEngageProduct.Insured.Declaration.dateandTime.indexOf('/') == 2){
                 $scope.LifeEngageProduct.Insured.Declaration.dateandTime = ExternalToInternal($scope.LifeEngageProduct.Insured.Declaration.dateandTime);
                }
            }

        }
        if($scope.loadApplicationDate){
        	if($scope.LifeEngageProduct.Insured.Declaration.dateandTime && $scope.LifeEngageProduct.Insured.Declaration.dateandTime.indexOf('/') == 2){
        		$scope.LifeEngageProduct.Insured.Declaration.dateandTime = ExternalToInternal($scope.LifeEngageProduct.Insured.Declaration.dateandTime);
             }
            $scope.loadApplicationDate = false;
        }
        

            // var datesplit = $scope.LifeEngageProduct.Insured.Declaration.dateandTime.split('/');
            // datesplit[0] = parseInt(datesplit[0]);
            // if(datesplit[0] > 2018){
            //     $scope.LifeEngageProduct.Insured.Declaration.dateandTime = summaryGetFormattedDateThai($scope.LifeEngageProduct.Insured.Declaration.dateandTime);
            // }else{
            //     $scope.LifeEngageProduct.Insured.Declaration.dateandTime =  summaryifonThai(ApplicationDateVal);
            // }

        $scope.refresh();
    };

    $scope.updateErrorCount = function (id) {
        $rootScope.updateErrorCount(id);
        $scope.selectedTabId = id;
        $scope.eAppParentobj.errorCount = $scope.errorCount;
    }
    $scope.showErrorPopup = function (id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
    }
    $scope.summaryEdit = function (linkTab, linkSubTab) {
        // isRootNav will check for whether the nav
        // click is coming from direct
        // main root tabs/subtabs
        if (!$scope.isProposalNoAllocated) {
            $scope.isRootNav = false;
            $scope.refresh();
            $("#registration_tabs a[rel='" + linkTab + "']")
                .parent('div').trigger('click');

            $('#' + linkTab).find(
                "li a[rel='" + linkSubTab +
                "']").parent('li').trigger(
                'click');
        }

    };

    //Used to set the date for RDS user in Summary screen
    $scope.setDateForRDSUser = function (date) {
        if ($scope.isRDSUser) {
            var temp = getFormattedDateSummary(date);
            $scope.LifeEngageProduct.Declaration.agentSignDate = temp;
        }
    };

    /*
     * Used to show or hide the questionnaire tab under summary based on additonal questions selected or not.---starts
     */
    function summayQuestionnaireTabShowHide() {
        $scope.arrayIndex = 0;
        var questionnaireCheckValues = [$scope.LifeEngageProduct.isProposerForeigner,
			                                $scope.LifeEngageProduct.hasInsuredQuestions,
			                                $scope.LifeEngageProduct.hasBeneficialOwnerForm];

        if ($scope.LifeEngageProduct.hasInsuranceEngagementLetter == true || $scope.LifeEngageProduct.hasInsuranceEngagementLetter == "true") {
            return true;
        } else {
            var returnValue;
            loopArrayQuestionnaire(questionnaireCheckValues[$scope.arrayIndex], function (dataReturn) {
                returnValue = dataReturn;
            });
            return returnValue;
        }
    };

    $scope.onRetrieveAgentProfileData = function(data) {
        
        $scope.agencyName=data.AgentDetails.agentName;    
        $scope.licenseNumber=data.AgentDetails.licenseNumber;
    }

    function loopArrayQuestionnaire(arrayValue, successcallback) {
        var questionnaireCheckValues = [$scope.LifeEngageProduct.isProposerForeigner,
			                                $scope.LifeEngageProduct.hasInsuredQuestions,
			                                $scope.LifeEngageProduct.hasBeneficialOwnerForm];
        checkLength(arrayValue, function (returnValue) {
            if (returnValue) {
                successcallback(true);
            } else {
                if ($scope.arrayIndex == questionnaireCheckValues.length - 1) {
                    successcallback(false);
                } else {
                    $scope.arrayIndex++;
                    loopArrayQuestionnaire(questionnaireCheckValues[$scope.arrayIndex], successcallback);
                }
            }
        });
    };

    /*
     * Used to show or hide the questionnaire tab under summary based on additonal questions selected or not.---ends
     */

    /*$scope.ShowDate = function(model, format) {
    	var returnVal;
    	var parentModel = model.split('.');
    	var isParentModel = parentModel[0]+"."+parentModel[1];
    	if(eval ("$scope."+isParentModel)){
    		if(!(isEmptyOrNull(format))){
    		
    			for (var i = 0; i < $scope.LifeEngageProduct.Beneficiaries.length; i++) {
    					if(i==format){
    						returnVal=$scope.LifeEngageProduct.Beneficiaries[i].BasicDetails.dob;
    						break;
    					}
    			}
    		}
    		else{
    			eval ("returnVal=$scope." + model);
    		}
    		returnVal = new Date(Date.parse(returnVal))
    	
    		return returnVal != 'Invalid Date' ? (parseInt(returnVal
    			.getDate()))
    			+ "-"
    			+ (parseInt(returnVal.getMonth()) + 1)
    			+ "-" + returnVal.getFullYear()
    			: "";
    	}
    };*/

    /*$scope.showOptions = function(model) {
    	if(model){
    		var returnVal;
    		var parentModel = model.split('.');
    		var isParentModel = parentModel[0]+"."+parentModel[1];
    		var isParentModel1 = parentModel[0]+"."+parentModel[1];
    		var isParentModel2 = parentModel[0]+"."+parentModel[1]+"."+parentModel[2];

    		if(eval ("$scope."+isParentModel1) && eval ("$scope."+isParentModel2)) {
    			if(eval ("$scope."+model)!=undefined){
    		   		eval ("returnVal=$scope." + model);
    			    	if(returnVal == 'Yes'){
    				    	return true;
    			   		}
    			    else{
    						return false;
    			  		}
    		   	}
    		   	else{
    		   		return false;
    		   	}
    		}	
    	}			  
    		
    };*/





 $rootScope.radioValidations=function() {       
        $scope.dynamicErrorMessages = [];
        $scope.dynamicErrorCount = 0;
        var _errors = [];
        var _errorCount = 0;    
  
        if(($scope.LifeEngageProduct.Declaration.guardianType===undefined || $scope.LifeEngageProduct.Declaration.guardianType==="") && $scope.LifeEngageProduct.ApplicationNo.length!== 7){
                _errorCount++;
                var error = {};
                $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                error.message = translateMessages($translate,"eapp_vt.guardianTypeValidation");            
                error.key = "summaryGaurdianradio";
                _errors.push(error);
                $scope.dynamicErrorMessages = _errors;
            
        }
        $scope.dynamicErrorMessages = _errors;     
        $scope.updateErrorCount('DeclarationAndAuthorizationTab');
        $scope.refresh();
         
    }

    var agentStatementCanvasWatch = $scope.$watch('LifeEngageProduct.Declaration.clientdeclarationAgreed', function (value) {
        if ($scope.selectedTabId == "DeclarationAndAuthorizationTab") {
            if (value == 'Yes' && !$scope.LifeEngageProduct.Declaration.agentSignDate && $scope.LifeEngageProduct.Declaration.agentSignDate == '') {
                $scope.agentStatementCanvas = false;
            } else {
                if ($scope.signaturePadPolicyHolder) {
                    $scope.signaturePadPolicyHolder.clear();
                    $scope.agentStatementCanvas = true;
                } else {
                    if (!(rootConfig.isDeviceMobile) && $scope.signaturePad) {
                        $scope.signaturePad.clear();
                    }
                }
            }
            $scope.refresh();
        }
    }, true);

    $scope.onSelectTypeHead = function (id) {
        $timeout(function () {
            $('#' + id).blur();
        }, 300);
    }
    $scope.formatLabel = function (model, options, type) {
        if (options && options.length > 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code) {
                    return options[i].value;
                }
            }
        } else {
            if (type == 'city') {
                $scope.state = $rootScope.state;
                for (var i = 0; i < $scope.state.length; i++) {
                    if (model === $scope.state[i].code) {
                        return $scope.state[i].value;
                    }
                }
            } else if (type == 'district') {
                $scope.district = $rootScope.district;
                for (var i = 0; i < $scope.district.length; i++) {
                    if (model === $scope.district[i].code) {
                        return $scope.district[i].value;
                    }
                }
            } else if (type == 'ward') {
                $scope.ward = $rootScope.wardOrHamlet;
                for (var i = 0; i < $scope.ward.length; i++) {
                    if (model === $scope.ward[i].code) {
                        return $scope.ward[i].value;
                    }
                }
            } else if (type == 'nationalityLsit') {
                $scope.nationalityLsit = $rootScope.nationalityLsit;
                for (var i = 0; i < $scope.nationalityLsit.length; i++) {
                    if (model === $scope.nationalityLsit[i].code) {
                        return $scope.nationalityLsit[i].value;
                    }
                }
            }
        }
    };


    $scope.$on("$destroy", function () {
        if (this != null && typeof this !== 'undefined') {
            agentStatementCanvasWatch();
            if (this.$$destroyed) return;
            while (this.$$childHead) {
                this.$$childHead.$destroy();
            }
            if (this.$broadcast) {
                this.$broadcast('$destroy');
            }
            this.$$destroyed = true;
            //$timeout.cancel( timer );  
        }
    });
    
}