/*
 Name:GLI_OthersController
 CreatedDate:21/12/2015
 Description:GLI_OthersController 
 */

storeApp.controller('OthersController', OthersController);
OthersController.$inject = ['$rootScope',
		'$scope',
		'DataService',
		'LookupService',
		'DocumentService',
		'ProductService',
		'$compile',
		'$routeParams',
		'$location',
		'$translate',
		'$debounce',
		'UtilityService',
		'PersistenceMapping',
		'AutoSave',
		'globalService',
		'EappVariables',
		'EappService',
		'$timeout'];

function OthersController($rootScope, $scope, DataService, LookupService,
    DocumentService, ProductService, $compile, $routeParams,
    $location, $translate, $debounce, UtilityService,
    PersistenceMapping, AutoSave, globalService, EappVariables,
    EappService, $timeout) {
    $rootScope.selectedPage = "OthersQuestionnaire";
    $scope.addPreviousPolicyHistory = function () {
        $scope.alreadyAdded = false;
        var previousPolicyHistory = {};
        previousPolicyHistory = {
            "insured": "",
            "companyName": "",
            "basePlan": "",
            "basicSumAssured": "",
            "statusDetails": "",
            "status": "",
            "reason": "",
            "partyType": "",
            "partyId": ""
        };
        var parentValue = ($scope.LifeEngageProduct.PreviouspolicyHistory.length) + 1;
        $scope.LifeEngageProduct.PreviouspolicyHistory.push(previousPolicyHistory);
        //update error count logic
    }
    $scope.$on("$destroy", function () {
        if (this != null && typeof this !== 'undefined') {
            if (this.$$destroyed) return;
            while (this.$$childHead) {
                this.$$childHead.$destroy();
            }
            if (this.$broadcast) {
                this.$broadcast('$destroy');
            }
            this.$$destroyed = true;
            $timeout.cancel(timer);
        }
    });

} // controller ends here