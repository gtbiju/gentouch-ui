'use strict';
/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:GLI_ProposerController
 CreatedDate:11/11/2015
 Description:GLI_ProposerController 
 */

storeApp.controller('GLI_ProposerController', GLI_ProposerController);
GLI_ProposerController.$inject = ['$controller', '$rootScope', '$scope', 'DataService', 'LookupService', 'DocumentService', 'ProductService', '$compile', '$routeParams', '$location', '$translate', '$debounce', 'UtilityService', 'PersistenceMapping', 'AutoSave', 'globalService', 'EappVariables', 'EappService', 'GLI_EappVariables', '$timeout', 'GLI_EappService', 'UserDetailsService', 'GLI_EvaluateService'];

function GLI_ProposerController($controller, $rootScope, $scope, DataService, LookupService, DocumentService, ProductService, $compile, $routeParams, $location, $translate, $debounce, UtilityService, PersistenceMapping, AutoSave, globalService, EappVariables, EappService, GLI_EappVariables, $timeout, GLI_EappService, UserDetailsService, GLI_EvaluateService) {
    $controller('ProposerController', {
        $rootScope: $rootScope,
        $scope: $scope,
        DataService: DataService,
        LookupService: LookupService,
        DocumentService: DocumentService,
        ProductService: ProductService,
        $compile: $compile,
        $routeParams: $routeParams,
        $location: $location,
        $translate: $translate,
        $debounce: $debounce,
        UtilityService: UtilityService,
        PersistenceMapping: PersistenceMapping,
        AutoSave: AutoSave,
        globalService: globalService,
        EappVariables: EappVariables,
        EappService: EappService,
        $timeout: $timeout
    });
    $scope.alphanumericPattern = rootConfig.alphanumericPattern;
    $scope.alphabetWithSpacePatternIllustration=rootConfig.alphabetWithSpacePatternIllustration;
    $scope.emailPattern = rootConfig.emailPattern;
    $scope.eappSpecialCharacters = rootConfig.eappSpecialCharacters;
    $scope.numberPatternMob = rootConfig.numberPatternMob;
    $scope.numberPattern = rootConfig.numberPattern;
    $scope.identityTypeApp = $rootScope.identityTypeApp;    
    $scope.convenientContact=$rootScope.convenientContact;
    $scope.passportEappPattern=rootConfig.passportEappPattern;
    $scope.copyFromLookup=angular.copy($rootScope.convenientContact);
    


    $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.ApplicationNo = 0;
    $scope.dynamicErrorMessages = [];
    $scope.dynamicErrorCount = 0;
    $scope.sameAsInsured = false;
    $scope.isRDSUser = UserDetailsService.getRDSUser();
    var _errors = [];
    var _errorCount = 0;
    $scope.showPopUp = false;
    $scope.disableField = false;
    $scope.onSelectTypeHead = function (id) {
        $timeout(function () {
            $('#' + id).blur();
        }, 300);
    }

    $scope.isDisplay = false;
    $scope.ShowOthersType = function (fieldValue) {
        if (parseInt(fieldValue) == 5) {
            $scope.isDisplay = true;
        } else {
            $scope.isDisplay = false;
        }
    }

    $scope.formatLabel = function (model, options, type) {
        if (options && options.length > 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code) {
                    return options[i].value;
                }
            }
        } else {
            if (type == 'city') {
                $scope.state = $rootScope.state;
                for (var i = 0; i < $scope.state.length; i++) {
                    if (model === $scope.state[i].code) {
                        return $scope.state[i].value;
                    }
                }
            } else if (type == 'ward') {
                $scope.ward = $rootScope.wardOrHamlet;
                for (var i = 0; i < $scope.ward.length; i++) {
                    if (model === $scope.ward[i].code) {
                        return $scope.ward[i].value;
                    }
                }
            } else if (type == 'nationalityLsit') {
                $scope.nationalityLsit = $rootScope.nationalityLsit;
                for (var i = 0; i < $scope.nationalityLsit.length; i++) {
                    if (model === $scope.nationalityLsit[i].code) {
                        return $scope.nationalityLsit[i].value;
                    }
                }
            } else if (type == 'OccupationCategory') {
                $scope.OccupationCategory = $rootScope.occupationList;
                for (var i = 0; i < $scope.OccupationCategory.length; i++) {
                    if (model === $scope.OccupationCategory[i].code) {
                        return $scope.OccupationCategory[i].value;
                    }
                }
            }
        }
    };

    $scope.convertToString = function (model) {
        $scope.annualIncome = model.toString();
        $scope.LifeEngageProduct.Proposer.BasicDetails.annualIncome = $scope.annualIncome;
    }

    //For clearing out district/ward fields on updating town/district fields
    $scope.markAsEdited = function (field) {
        
    }

    //Identity Date popup
    $scope.checkDate = function () {
        var identityDate = new Date($scope.LifeEngageProduct.Proposer.BasicDetails.identityDate);
        var today = new Date();
        today.setYear(today.getFullYear() - 15);
        if (identityDate.getFullYear() < today.getFullYear()) {
            $scope.showPopUp = true;
        } else if (identityDate.getFullYear() == today.getFullYear()) {
            if (identityDate.getMonth() < today.getMonth()) {
                $scope.showPopUp = true;
            } else if (identityDate.getMonth() == today.getMonth()) {
                if (identityDate.getDate() < today.getDate()) {
                    $scope.showPopUp = true;
                } else {
                    $scope.showPopUp = false;
                }
            } else {
                $scope.showPopUp = false;
            }
        } else {
            $scope.showPopUp = false;
        }

        if ($scope.showPopUp == true) {
            $('.custdatepicker').blur();
            $rootScope.lePopupCtrl.showError(
                translateMessages($translate, "lifeEngage"),
                translateMessages($translate, "eapp_vt.idExpired"),
                translateMessages($translate, "lms.ok"));
        }
    }

    $scope.getValueCode = function (code_model, array_list) {
        if (code_model) {
            var _lastIndex = code_model.lastIndexOf('.');
            if (_lastIndex > 0) {
                var parentModel = code_model.substring(0, _lastIndex);
                var childModel = code_model.substring(_lastIndex + 1, code_model.length);
                var newChild = childModel + "Value";
                var scopeVar = GLI_EvaluateService.evaluateString($scope, parentModel);
                var inputCode = GLI_EvaluateService.evaluateString($scope, code_model);
                var outputValue = getLookUpDataFromCode(array_list, inputCode);
                if (outputValue && outputValue != "" && outputValue.length > 0) {
                    scopeVar[newChild] = outputValue[0].value;
                }
                $scope.refresh();
            }
        }
    }
    //For counting no.of AdditionalQuestions selected
    $scope.callAdditionalQuestionCount = function (val, questionId) {
        if (val == "indonesia" || val == "Indonesia") {
            val = 'No';
        } else {
            val = 'Other';
        }
        if ($scope.LifeEngageProduct.Proposer.CustomerRelationship.isPayorDifferentFromInsured) {
            var curentArr = [];
            if ($scope.LifeEngageProduct.isProposerForeigner && ($scope.LifeEngageProduct.isProposerForeigner != "0" || $scope.LifeEngageProduct.isProposerForeigner != 0) && $scope.LifeEngageProduct.isProposerForeigner != "") {
                curentArr = JSON.parse($scope.LifeEngageProduct.isProposerForeigner).selectedArr;
            }
            EappService.updateAdditionalQuestionCount(val, questionId, curentArr, function (modifiedArr) {
                var modifiedJson = {
                    "selectedArr": []
                };
                modifiedJson.selectedArr = modifiedArr;
                $scope.LifeEngageProduct.isProposerForeigner = JSON.stringify(modifiedJson);
            });
        } else {
            var curentArr = [];
            if ($scope.LifeEngageProduct.hasInsuredQuestions && ($scope.LifeEngageProduct.hasInsuredQuestions != "0" || $scope.LifeEngageProduct.hasInsuredQuestions != 0) && $scope.LifeEngageProduct.hasInsuredQuestions != "") {
                curentArr = JSON.parse($scope.LifeEngageProduct.hasInsuredQuestions).selectedArr;
            }
            EappService.updateAdditionalQuestionCount(val, questionId, curentArr, function (modifiedArr) {
                var modifiedJson = {
                    "selectedArr": []
                };
                modifiedJson.selectedArr = modifiedArr;
                $scope.LifeEngageProduct.hasInsuredQuestions = JSON.stringify(modifiedJson);
            });
        }
    }

    // validating typeahead field
    $scope.updateDynmaicErrMessage = function (erroKey) {
        for (var i = 0; i < $scope.dynamicErrorMessages.length; i++) {
            if ($scope.dynamicErrorMessages[i].key == erroKey) {
                $scope.dynamicErrorMessages.splice(i, 1);
                $scope.dynamicErrorCount = $scope.dynamicErrorCount - 1;
            }
        }
        $scope.updateErrorCount('PolicyHolderSubTab');
    }

    function invalidNationality(errorKey) {
        var error = {};
        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
        error.message = translateMessages(
            $translate,
            "lms.leadDetailsSectionnationalityRequiredValidationMessage");
        error.key = errorKey;
        _errors.push(error);

    }

    function invalidResidentCountry(errorKey) {
        var error = {};
        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
        error.message = translateMessages(
            $translate,
            "lms.leadDetailsSectionCountryofbirthRequiredValidationMessage");
        error.key = errorKey;
        _errors.push(error);

    }

    function invalidOccupation(errorKey) {
        var error = {};
        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
        error.message = translateMessages(
            $translate,
            "lms.leadDetailsSectionoccupationRequiredValidationMessage");
        error.key = errorKey;
        _errors.push(error);

    }

    function invalidCity(errorKey) {
        var error = {};
        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
        error.message = translateMessages(
            $translate,
            "eapp.validCityErrorMessage");
        error.key = errorKey;
        _errors.push(error);

    }

    function setToModel(model, value) {
        var _lastIndex = model.lastIndexOf('.');
        if (_lastIndex > 0) {
            var parentModel = model.substring(0, _lastIndex);
            var childmodel = model.substring(_lastIndex + 1, model.length);
            var scopeVar = GLI_EvaluateService.evaluateString($scope, parentModel);
            scopeVar[childmodel] = value;
        }
    }
    // validating typeahead field
    $scope.validateField = function (model, list, fieldTobeValidated, errorKey, child_dependent) {
        if (model != "" && model != null && model != undefined) {
            var isFieldValid = GLI_EappService.validateSmartSearch(model, list);
            if (isFieldValid == false) {
                $scope.updateDynmaicErrMessage(errorKey);
//                if (fieldTobeValidated == "nationality") {
//                    $scope.isForeigner = false;
//                    invalidNationality(errorKey);
//                } else if (fieldTobeValidated == "country") {
//                    invalidResidentCountry(errorKey);
//                } else if (fieldTobeValidated == "occupation") {
//                    invalidOccupation(errorKey);
//                } else if (fieldTobeValidated == "city") {
//                    if (child_dependent) {
//                        setToModel(child_dependent, '');
//                    }
//                    invalidCity(errorKey);
//                }
                $scope.dynamicErrorMessages = _errors;
            } else {
                if (fieldTobeValidated == "nationality" && isFieldValid != "Indonesia") {
                    $scope.isForeigner = true;
                } else if (fieldTobeValidated == "nationality" && isFieldValid == "Indonesia") {
                    $scope.isForeigner = false;
                }
                $scope.updateDynmaicErrMessage(errorKey);
            }
            $scope.refresh();
            $scope.updateErrorCount('PolicyHolderSubTab');
        }
    }
    $scope.getAgeFromDOB = function (proposerdob) {
        var age;
        age = getPartyAge(proposerdob);
        $scope.LifeEngageProduct.Proposer.BasicDetails.age = age;
    }

    $scope.onFieldChange = function () {

        $timeout(function () {
            $scope.updateErrorCount("PolicyHolderSubTab");
        }, 50);
    }




    $scope.Initialize = function () {
       if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_Payment#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_Payment#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step2Payment#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step2Payment#tabDetails_1');
        } 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_BeneficiarySubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_BeneficiarySubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_financilaDetailsSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_financilaDetailsSubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_mainInsuredSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_mainInsuredSubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PolicyHolderSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PolicyHolderSubTab#tabDetails_1');
        }		
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6');
		}	
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
		}
		
        $scope.viewToBeCustomized = 'PolicyHolderSubTab';
        $scope.$parent.disableProceedButtonCommom = false;
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            //EappVariables.setEappModel($scope.LifeEngageProduct);
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
        }
        if ($scope.LifeEngageProduct.Proposer.BasicDetails.annualIncome) {
            $scope.annualIncome = $scope.LifeEngageProduct.Proposer.BasicDetails.annualIncome.toString();
            $scope.LifeEngageProduct.Proposer.BasicDetails.annualIncome = $scope.annualIncome;
        }

        $scope.LifeEngageProduct = EappVariables.getEappModel();
        //For disabling and enabling the tabs.And also to know the last visited page.---starts


                //To solve pre-populated data(country & nationality) getting cleared on tab navigation before save
        if (rootConfig.isDeviceMobile) {
                if($scope.LifeEngageProduct.Payer.BasicDetails == undefined){
                    $scope.LifeEngageProduct.Payer.BasicDetails = {};
                }            
        }
        /* FNA changes made by LE Team starts */
        if($scope.LifeEngageProduct.Payer.CurrentAddress && $scope.LifeEngageProduct.Payer.classNameAttached!=undefined && ($scope.LifeEngageProduct.Payer.ContactDetails.isPermanentAddressSameAsCurrentAddress==undefined || $scope.LifeEngageProduct.Payer.ContactDetails.isPermanentAddressSameAsCurrentAddress==='No')){
                 $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress=$scope.LifeEngageProduct.Payer.CurrentAddress;
            }
        /* FNA changes made by LE Team ends */
        $rootScope.selectedPage = "ProposerDetails";
        $scope.LifeEngageProduct.LastVisitedUrl = "1,2,'PolicyHolderSubTab',true,'PolicyHolderSubTab',''";
        $scope.eAppParentobj.CurrentPage = "";
        $scope.eAppParentobj.nextPage = "1,7,'BeneficiarySubTab',true,'BeneficiarySubTab',''";

        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[1] < 2 && subIndex[0] <= $scope.LifeEngageProduct.LastVisitedUrl[0]) {
                $scope.LifeEngageProduct.LastVisitedIndex = "1,2";
            }
        }

        $(window).scrollTop(0); 
        // //For disabling and enabling the tabs.And also to know the last visited page.---ends
        // if (EappVariables.prepopulatedProposerData.CustomerRelationship.relationWithInsured == "Self") {
        //     var Question = {};
        //     if ($scope.LifeEngageProduct.Proposer.OccupationDetails.nameofInstitution) {
        //         var nameofInstitution = $scope.LifeEngageProduct.Proposer.OccupationDetails.nameofInstitution;
        //     }
        //     if ($scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.companyOrSchoolAddress) {
        //         var companyOrSchoolAddress = $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.companyOrSchoolAddress;
        //     }
        //     if ($scope.LifeEngageProduct.Proposer.OccupationDetails.natureofWork) {
        //         var natureofWork = $scope.LifeEngageProduct.Proposer.OccupationDetails.natureofWork;
        //     }
        //     if ($scope.LifeEngageProduct.Proposer.ContactDetails.homeNumber1) {
        //         var homeNumber = $scope.LifeEngageProduct.Proposer.ContactDetails.homeNumber1;
        //     }
        //     if ($scope.LifeEngageProduct.Proposer.ContactDetails.emailId) {
        //         var emailId = $scope.LifeEngageProduct.Proposer.ContactDetails.emailId;
        //     }
        //     if ($scope.LifeEngageProduct.Proposer.ContactDetails.officeNumber.number) {
        //         var officeNumber = $scope.LifeEngageProduct.Proposer.ContactDetails.officeNumber.number;
        //     }
        //     if ($scope.LifeEngageProduct.Proposer.Questions && $scope.LifeEngageProduct.Proposer.Questions[0]) {
        //         Question = $scope.LifeEngageProduct.Proposer.Questions[0];
        //     }

        //     $scope.LifeEngageProduct.Proposer = angular.copy($scope.LifeEngageProduct.Insured);
        //     if (nameofInstitution && $scope.LifeEngageProduct.Insured.OccupationDetails.nameofInstitution == "") {
        //         $scope.LifeEngageProduct.Proposer.OccupationDetails.nameofInstitution = nameofInstitution;
        //         nameofInstitution = "";
        //     }
        //     if (companyOrSchoolAddress && $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.companyOrSchoolAddress == "") {
        //         $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.companyOrSchoolAddress = companyOrSchoolAddress;
        //         companyOrSchoolAddress = "";
        //     }
        //     if (natureofWork && $scope.LifeEngageProduct.Insured.OccupationDetails.natureofWork == "") {
        //         $scope.LifeEngageProduct.Proposer.OccupationDetails.natureofWork = natureofWork;
        //         natureofWork = "";
        //     }
        //     if (homeNumber && $scope.LifeEngageProduct.Insured.ContactDetails.homeNumber1 == "") {
        //         $scope.LifeEngageProduct.Proposer.ContactDetails.homeNumber1 = homeNumber;
        //         homeNumber = "";
        //     }
        //     if (emailId && $scope.LifeEngageProduct.Insured.ContactDetails.emailId == "") {
        //         $scope.LifeEngageProduct.Proposer.ContactDetails.emailId = emailId;
        //         emailId = "";
        //     }
        //     if (officeNumber && $scope.LifeEngageProduct.Payer.ContactDetails.officeNumber.number == "") {
        //         $scope.LifeEngageProduct.Proposer.ContactDetails.officeNumber.number = officeNumber;
        //         officeNumber = "";
        //     }
        //     if (Question) {
        //         $scope.LifeEngageProduct.Proposer.Questions = [];
        //         $scope.LifeEngageProduct.Proposer.Questions.push(Question);
        //         Question = {};
        //     }
        //     $scope.sameAsInsured = true;

        // } else {
        //     $scope.sameAsInsured = false;
        // }
        $scope.LifeEngageProduct.Proposer.BasicDetails.clientType = "Individual";
        $scope.eAppParentobj.PreviousPage = "";
        $timeout(function () {
            
            //Mobile number prepopulate
            if(($scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber1==undefined || $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber1=="") && ($scope.LifeEngageProduct.Insured.BasicDetails.illustrationInsSameAsPayer===$scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer)){
                $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber1 = EappVariables.prepopulatedProposerData.ContactDetails.mobileNumber1;
            }
            
            if($scope.LifeEngageProduct.Payer.BasicDetails.maritalStatus==undefined || $scope.LifeEngageProduct.Payer.BasicDetails.maritalStatus==""){
                
            }
                        
             $scope.prependZero();
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission") {
                UtilityService.disableAllActionElements();
            }else if (typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) {
                UtilityService.disableAllActionElements();
            }
            $scope.updateErrorCount($scope.selectedTabId);
        }, 100);
        if (EappVariables.EappKeys.Key15 == "Confirmed" && $scope.$parent.LifeEngageProduct.Declaration.confirmSignature == true) {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = true;
        } else {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
        }

        if (typeof $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress == "undefined") {
            $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress = {};
        }
        var nationality = $scope.LifeEngageProduct.Payer.BasicDetails.nationality;
        // if (typeof nationality != "undefined" && nationality != "") {
        //     // Foreigner questions display
        //     $scope.isForeigner = false;
        //     if (nationality != "Indonesia") {
        //         $scope.isForeigner = true;
        //     }

        //     // $scope.setFACTAQuestionInitially($scope.LifeEngageProduct.Proposer.BasicDetails.nationality, 'nationality');
        //     // $scope.callAdditionalQuestionCount($scope.LifeEngageProduct.Proposer.BasicDetails.nationality, 'proposerNationality');
        // }
        if (typeof $scope.LifeEngageProduct.Proposer.BasicDetails.countryofResidence != "undefined" && $scope.LifeEngageProduct.Proposer.BasicDetails.countryofResidence != "") {
            $scope.setFACTAQuestionInitially($scope.LifeEngageProduct.Proposer.BasicDetails.countryofResidence, 'countryofResidence');
        }
        $scope.LifeEngageProduct.Declaration.submittedBy = "Yes";

        //Used to disable the fields which is prepopulated from BI or FNA
        $scope.PrepopulatedDisabledEappPayerData = angular.copy(EappVariables.prepopulatedProposerData);
        $scope.PrepopulatedDisabledEappInsuredData = angular.copy(EappVariables.prepopulatedInsuredData);
        
        if($scope.LifeEngageProduct.Payer.BasicDetails.IDcard==="" || $scope.LifeEngageProduct.Payer.BasicDetails.IDcard===undefined){
            $scope.LifeEngageProduct.Payer.BasicDetails.IDcard="";
        } 
		if(($scope.PrepopulatedDisabledEappInsuredData.BasicDetails.isInsuredSameAsPayer===$scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer) && $scope.LifeEngageProduct.Payer.BasicDetails.gender==='Male' && $scope.maritalStatus.length==4){
            setTimeout(function(){$scope.spliceWidowInMaritalDropdown();},1000);            
        }

        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        angular.element('#' + $scope.selectedTabId + "_a").trigger('click');
        $scope.updateErrorCount($scope.selectedTabId);
        $scope.IsProposerPermAddSame = function (id) {
            if ($scope.LifeEngageProduct.Proposer.ContactDetails.isPermanentAddressSameAsCurrentAddress) {
                $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress = $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress;
            } else {
                $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress = {};
            }
        };
        $scope.eAppParentobj.errorCount = $scope.errorCount;
        //$scope.occupationRule('ProposerDetails','LifeEngageProduct.Proposer.OccupationDetails.occupationCategoryValue');
        var model = "LifeEngageProduct";
        if ((rootConfig.autoSave.eApp) && (typeof EappVariables.EappKeys.Key15 == "undefined" || EappVariables.EappKeys.Key15 == "New" || EappVariables.EappKeys.Key15 == "Confirmed")) {
            if (AutoSave.destroyWatch) {
                AutoSave.destroyWatch();
            }
            AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
        } else if (AutoSave.destroyWatch) {
            AutoSave.destroyWatch();
        }

        //Default to Thai
        if($scope.LifeEngageProduct.Payer.BasicDetails.nationality===undefined || $scope.LifeEngageProduct.Payer.BasicDetails.nationality===""){
            $scope.LifeEngageProduct.Payer.BasicDetails.nationality='THA';
            $scope.foreignerInd=false;
        }

        $scope.prependZero();        
        $scope.dynamicValidations();        
        setTimeout(function(){$scope.validateNationality();},1000);

    }

    $scope.setCategoryCode = function (model, occupation) {
        if (model) {
            var _lastIndex = model.lastIndexOf('.');
            if (_lastIndex > 0) {
                var parentModel = model.substring(0, _lastIndex);
                var value = GLI_EvaluateService.evaluateString($scope, model);
                var scopeVar = GLI_EvaluateService.evaluateString($scope, parentModel);
                /* var value=eval ('$scope.' + model);
                 var scopeVar=eval ('$scope.' + parentModel);*/
                if (occupation) {
                    var value = getLookUpDataFromCode(occupation, value);
                    if (value && value != "" && value.length > 0) {
                        scopeVar['occupationCategoryValue'] = value[0].value;
                        //scopeVar['natureofWork']= "";
                    }
                }

            }
        }
    }
    $scope.IsResidentAddrSame = function (model, tabsinner) {
//        if (model == "Yes") {
//            $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.city = $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.city;
//            $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.district = $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.district;
//            $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.ward = $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.ward;
//            $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.houseNo = $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.houseNo;
//            $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.street = $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.street;
//            $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.country = $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.country;
//            $scope.disableField = true;
//        } else {
//            $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.houseNo = "";
//            $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.street = "";
//            $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.district = "";
//            $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.city = "";
//            $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.ward = "";
//            $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.country = "";
//            $scope.disableField = false;
//        }
        $scope.refresh();
        $timeout(function () {
            $scope.updateErrorCount("PolicyHolderSubTab");
        }, 50);

    }

    $scope.updateErrorCount = function (id) {            
        $('.custdatepicker').on('changeDate change blur',function () {             
            $scope.updateNationality();
        });        
        
        if(EappVariables.EappKeys.Key15 != "Pending Submission" && EappVariables.EappKeys.Key15 !=undefined && EappVariables.EappKeys.Key15 !="") {
            $timeout(function () {
                $rootScope.updateErrorCount(id);
            }, 1000);        
        }else{
            $rootScope.updateErrorCount(id);
        }  
        $scope.selectedTabId = id;
        $scope.eAppParentobj.errorCount = $scope.errorCount;
        EappVariables.setEappModel($scope.LifeEngageProduct);
        $scope.refresh();
    }
    $scope.insTabsErrorCount = function (id, index) {
        selectedtab = id;
        $scope.updateErrorCount(id);
        $scope.showErrorCount = true;
        for (var i = 0; i < $scope.innertabsVertical.length; i++) {
            if (i == index) {
                $scope.innertabsVertical[i] = 'selected';
            } else {
                $scope.innertabsVertical[i] = '';
            }
        }
    };
    $scope.showErrorPopup = function (id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
    }

    //function to set the FACTA questions based on nationality and country of birth
    //and clearing the selected data if user changes the nationality or country of birth
    $scope.isForeigner = false;
    $scope.setFACTAQuestion = function (model, dataBasedOn) {
        if (dataBasedOn == "nationality") {
            if (model == "United States of America") {
                $scope.LifeEngageProduct.Proposer.Questions[0].option = "Yes";
            } else {
                $scope.LifeEngageProduct.Proposer.Questions[0].option = "";
            }
        } else if (dataBasedOn == "countryofResidence") {
            if (model == "United States of America") {
                $scope.LifeEngageProduct.Proposer.Questions[1].option = "Yes";
            } else {
                $scope.LifeEngageProduct.Proposer.Questions[1].option = "";
            }
        }
    }

    //function to set the FACTA questions based on nationality and country of birth in initilaLoad
    $scope.setFACTAQuestionInitially = function (model, dataBasedOn) {
        if (dataBasedOn == "nationality") {
            if (model == "United States of America") {
                $scope.LifeEngageProduct.Proposer.Questions[0].option = "Yes";
            }
        } else if (dataBasedOn == "countryofResidence") {
            if (model == "United States of America") {
                $scope.LifeEngageProduct.Proposer.Questions[1].option = "Yes";
            }
        }
    }

    //ProposerController Load Event and Load Event Handler
    $scope.$parent.$on('PolicyHolderSubTab', function (event, args) {
        $scope.selectedTabId = args[0];
        $scope.Initialize();
        $rootScope.validationBeforesave = function (value, successcallback) {
            if (value == "save") {
                $scope.LifeEngageProduct.LastVisitedIndex = "1,2";
            }
            if (rootConfig.isDeviceMobile) {
                EappVariables.setEappModel($scope.LifeEngageProduct);
            }
            successcallback();
        };
    });
    $scope.$on("$destroy", function () {
        if (this != null && typeof this !== 'undefined') {
            if (this && this.$$destroyed) return;
            while (this.$$childHead) {
                this.$$childHead.$destroy();
            }
            if (this.$broadcast) {
                this.$broadcast('$destroy');
            }
            this.$$destroyed = true;
            $timeout.cancel(timer);
        }
    });

    $rootScope.validationBeforesave = function (value, successcallback) {
        successcallback();
    };
    $scope.getLookUpData = function (data, value) {
        return data.filter(function (data) {
            return angular.lowercase(data.value) == angular.lowercase(value)
        });
    }

    function getLookUpDataFromCode(data, value) {
        return data.filter(function (data) {
            return angular.lowercase(data.code) == angular.lowercase(value)
        });
    }
    $scope.checkCorrespondanceAddress = function (model) {
        var flag = false;
        switch (model) {
            case "addressLine1":
                if ($scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.addressLine1 != $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.addressLine1) {
                    flag = true;
                }
                break;
            case "addressLine2":
                if ($scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.addressLine2 != $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.addressLine2) {
                    flag = true;
                }
                break;
            case "city":
                if ($scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.city != $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.city) {
                    flag = true;
                }
                break;
            case "state":
                if ($scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.state != $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.state) {
                    flag = true;
                }
                break;
            case "zipCode":
                if ($scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.zipCode != $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.zipCode) {
                    flag = true;
                }
                break;

        }
        if (flag == true) {
            $scope.LifeEngageProduct.Proposer.ContactDetails.isPermanentAddressSameAsCurrentAddress = "No";
        }
    };
    /*$scope.occupationRule = function(person,model){
			$scope.occupaionQuestionnaire ="";			
			var data= {
					    "person":person
			          };

			if(model){
				var _lastIndex=model.lastIndexOf('.');
				if(_lastIndex>0){
				   var parentModel=model.substring(0,_lastIndex);
				   var value=eval ('$scope.' + model);
				   var scopeVar=eval ('$scope.' + parentModel);	
				   if($scope.OccupationCategory){
				   		var value = $scope.getLookUpData($scope.OccupationCategory,value);			   	
				    	if(value && value != "" && value.length > 0){
				   			scopeVar['occupationCategory']=value[0].code;
				    	}	
				   }			
				   			   
				}
			}
			if(scopeVar['occupationCategory']){
				GLI_EappService.runOccupationRule($scope,data,function(result){	
                	if(result && result!=null){
						$scope.occupaionQuestionnaire = result;
						$scope.refresh();
					}				
				});
			}
			
		}*/




        /* -------------------------------- Ashok for Payer --------------------------------*/


    
  $scope.clearFieldCurrent=function(val){
                    
                    if(val=='address' && $scope.editMode && $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.city){                       
                        $scope.allowFormatLabel=true;
                        $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.city="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.state="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.provinceCode="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.zipcode="";                             
                        $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.cityValue="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.stateValue="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.wardOrHamletValue="";
                    }else if(val=='address' && !$scope.editMode && $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.city){
                        $scope.allowFormatLabel=true;
                        $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.city="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.state="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.provinceCode="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.zipcode="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.cityValue="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.stateValue="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.wardOrHamletValue="";
                    }
    };
    $scope.clearFieldPermanent=function(val){
                    
                    if(val=='address' && $scope.editMode && $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.city){                     
                        $scope.allowFormatLabel=true;
                        $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.city="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.state="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.provinceCode="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.zipcode="";                            $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.cityValue="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.stateValue="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.wardOrHamletValue="";
                    }else if(val=='address' && !$scope.editMode && $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.city){
                        $scope.allowFormatLabel=true;
                        $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.city="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.state="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.provinceCode="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.zipcode="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.cityValue="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.stateValue="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.wardOrHamletValue="";
                    }
    };
    $scope.clearFieldOffice=function(val){
                    
                    if(val=='address' && $scope.editMode && $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.city){                        
                        $scope.allowFormatLabel=true;
                        $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.city="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.state="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.provinceCode="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.zipcode="";                             $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.cityValue="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.stateValue="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.wardOrHamletValue="";
                    }else if(val=='address' && !$scope.editMode && $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.city){
                        $scope.allowFormatLabel=true;
                        $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.city="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.state="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.provinceCode="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.zipcode="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.cityValue="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.stateValue="";
                        $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.wardOrHamletValue="";
                    }
    };
    $scope.formatLabelCurrentAddress = function (model, options, type) {        
                    if (options && options.length >= 1) {
                        for (var i = 0; i < options.length; i++) {
                            if (model === options[i].code && type !== 'district') {
                                if (type == 'wardOrHamlet') {
                                    var splitWard=[];
                                    var optionValue=options[i].value;
                                    if(optionValue.indexOf(',')>0){
                                        $rootScope.splitInd=true;
                                         optionValue=optionValue.substring(0,options[i].value.length-1);
                                         splitWard=optionValue.split(',');
                                         $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.wardOrHamletValue = splitWard[0];
                                    }else{
                                    $rootScope.splitInd=false;
                                    $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.wardOrHamletValue = options[i].value;
                                    }
                                    $rootScope.updateAddress=true;                                    
                                }
                                return options[i].value;
                            }
                            if (type == 'district' && ($scope.allowFormatLabel || $rootScope.updateAddress)){
                                $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.cityValue = options[0].value;
                                $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.stateValue = options[1].value;   
                                $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.zipcodeValue = options[2].value;
                                $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.city = options[0].code;
                                $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.state = options[1].code;
                                //Province code mapping for LA - currentAddress   
                                if($scope.LifeEngageProduct.Payer.ContactDetails.currentAddress!==undefined){
                                if($scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.state!==undefined && $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.state!==""){
                                    for (var i = 0; i < $rootScope.provinceCode.length; i++) {
                                        if ($scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.state === $rootScope.provinceCode[i].code) {
                                            $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.provinceCode = $rootScope.provinceCode[i].value;
                                        }
                                    }
                                }    
                                }
                                $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.zipcode = options[2].value;
                                $rootScope.updateAddress=false;
                                return options[0].value;
                            }

                            if(type == 'wardOrHamlet' && !$scope.allowFormatLabel && ($rootScope.updateAddress|| $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress && $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.city)){
                                if($rootScope.splitInd && !$scope.editMode){
                                    $rootScope.splitInd=false;
                                    if($scope.LifeEngageProduct.Payer.ContactDetails.currentAddress){
                                        return $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.wardOrHamletValue+","+$scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.cityValue;
                                    }
                                    
                                }else{
                                    return $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.wardOrHamletValue;
                                }
                            }else if(type == 'district' && ((!$scope.editMode && !$scope.allowFormatLabel)||($scope.editMode && !$scope.allowFormatLabel)||(!$scope.editMode && $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.ward))){
                                return model;
                            }
                        }
                    }
                };

                if($scope.LifeEngageProduct.Payer.ContactDetails.officeAddress == undefined){
                    $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress ={};
                }
    
    $scope.formatLabelOfficeAddres = function (model, options, type) {        
                    if (options && options.length >= 1) {
                        for (var i = 0; i < options.length; i++) {
                            if (model === options[i].code && type !== 'district') {
                                if (type == 'wardOrHamlet') {
                                    var splitWard=[];
                                    var optionValue=options[i].value;
                                    if(optionValue.indexOf(',')>0){
                                        $rootScope.splitInd=true;
                                         optionValue=optionValue.substring(0,options[i].value.length-1);
                                         splitWard=optionValue.split(',');
                                         $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.wardOrHamletValue = splitWard[0];
                                    }else{
                                    $rootScope.splitInd=false;
                                    $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.wardOrHamletValue = options[i].value;
                                    }
                                    $rootScope.updateAddress=true;                                    
                                }
                                return options[i].value;
                            }
                            if (type == 'district' && ($scope.allowFormatLabel || $rootScope.updateAddress)){
                                $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.cityValue = options[0].value;
                                $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.stateValue = options[1].value;
                                $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.zipcodeValue = options[2].value;
                                $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.city = options[0].code;
                                $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.state = options[1].code;
                                //Province code mapping for LA - officeAddress   
                                if($scope.LifeEngageProduct.Payer.ContactDetails.officeAddress!==undefined){
                                if($scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.state!==undefined && $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.state!==""){
                                    for (var i = 0; i < $rootScope.provinceCode.length; i++) {
                                        if ($scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.state === $rootScope.provinceCode[i].code) {
                                            $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.provinceCode = $rootScope.provinceCode[i].value;
                                        }
                                    }
                                }    
                                }
                                $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.zipcode = options[2].value;
                                $rootScope.updateAddress=false;
                                return options[0].value;
                            }

                            if(type == 'wardOrHamlet' && !$scope.allowFormatLabel && ($rootScope.updateAddress|| $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress && $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.city)){
                                if($rootScope.splitInd && !$scope.editMode){
                                    $rootScope.splitInd=false;
                                    if($scope.LifeEngageProduct.Payer.ContactDetails.officeAddress && $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress){
                                        return $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.wardOrHamletValue+","+$scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.cityValue;

                                    }
                                    
                                }else{
                                    return $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.wardOrHamletValue;
                                }
                            }else if(type == 'district' && ((!$scope.editMode && !$scope.allowFormatLabel)||($scope.editMode && !$scope.allowFormatLabel)||(!$scope.editMode && $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress.ward))){
                                return model;
                            }
                        }
                    }
                };

                if($scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress == undefined){
                    $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress = {};

                }
    
    $scope.formatLabelPermanentAddress = function (model, options, type) {        
                    if (options && options.length >= 1) {
                        for (var i = 0; i < options.length; i++) {
                            if (model === options[i].code && type !== 'district') {
                                if (type == 'wardOrHamlet') {
                                    var splitWard=[];
                                    var optionValue=options[i].value;
                                    if(optionValue.indexOf(',')>0){
                                        $rootScope.splitInd=true;
                                         optionValue=optionValue.substring(0,options[i].value.length-1);
                                         splitWard=optionValue.split(',');
                                         $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.wardOrHamletValue = splitWard[0];
                                    }else{
                                    $rootScope.splitInd=false;
                                    $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.wardOrHamletValue = options[i].value;
                                    }
                                    $rootScope.updateAddress=true;                                    
                                }
                                return options[i].value;
                            }
                            if (type == 'district' && ($scope.allowFormatLabel || $rootScope.updateAddress)){
                                $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.cityValue = options[0].value;
                                $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.stateValue = options[1].value;
                                $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.zipcodeValue = options[2].value;
                                $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.city = options[0].code;
                                $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.state = options[1].code;
                                //Province code mapping for LA - permanentAddress   
                                if($scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress!==undefined){
                                if($scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.state!==undefined && $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.state!==""){
                                    for (var i = 0; i < $rootScope.provinceCode.length; i++) {
                                        if ($scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.state === $rootScope.provinceCode[i].code) {
                                            $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.provinceCode = $rootScope.provinceCode[i].value;
                                        }
                                    }
                                }    
                                }
                                $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.zipcode = options[2].value;
                                $rootScope.updateAddress=false;
                                return options[0].value;
                            }

                            if(type == 'wardOrHamlet' && !$scope.allowFormatLabel && ($rootScope.updateAddress|| $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.city)){
                                if($rootScope.splitInd && !$scope.editMode){
                                    $rootScope.splitInd=false;
                                    if($scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress){
                                        return $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.wardOrHamletValue+","+$scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.cityValue;
                                    }
                                    
                                }else{
                                    return $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.wardOrHamletValue;
                                }
                            }else if(type == 'district' && ((!$scope.editMode && !$scope.allowFormatLabel)||($scope.editMode && !$scope.allowFormatLabel)||(!$scope.editMode && $scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress.ward))){
                                return model;
                            }
                        }
                    }
                };
    
    $scope.copyFromHouseholdRegAddress=function(value){
        if(value==='Yes'){
            $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress=angular.copy($scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress);
        }else{
            $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.cityValue = "";
            $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.stateValue = "";
            $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.zipcodeValue = "";
            $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.city = "";
            $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.state = "";
            $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.zipcode = "";
            $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.ward="";
            $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.wardOrHamletValue="";
            $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.streetName="";
            $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.lane="";
            $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.mooNo="";
            $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.buildingName="";
            $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.houseNo="";
            
            if($scope.LifeEngageProduct.Payer.ContactDetails.isCopyFrom!==undefined){
                if($scope.LifeEngageProduct.Payer.ContactDetails.isCopyFrom==='HouseholdRegistrationAddress'){
                    $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress=angular.copy($scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress);
                }else if($scope.LifeEngageProduct.Payer.ContactDetails.isCopyFrom==='CurrentAddress'){
                    $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress=angular.copy($scope.LifeEngageProduct.Payer.ContactDetails.currentAddress);
                }
            }
        }        
    }
    
    $scope.copyFrom=function(value){
        if(value==="" || value===undefined || value===null){
            $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress={};
        }else if(value==='HouseholdRegistrationAddress'){
            $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress=angular.copy($scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress);
        }else{
            $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress=angular.copy($scope.LifeEngageProduct.Payer.ContactDetails.currentAddress);
        }        
    }
    
    //Default to House hold Address
//    if($scope.LifeEngageProduct.Payer.ContactDetails.isCopyFrom===undefined || $scope.LifeEngageProduct.Payer.ContactDetails.isCopyFrom===""){
//       // $scope.LifeEngageProduct.Payer.ContactDetails.isCopyFrom='houseHoldAddress';    
//            $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress=angular.copy($scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress);
//        
//    }
    
    if($scope.LifeEngageProduct.Payer.BasicDetails.TrineeAgent === undefined || $scope.LifeEngageProduct.Payer.BasicDetails.TrineeAgent === ""){
        $scope.LifeEngageProduct.Payer.BasicDetails.TrineeAgent ="No";
    }
    
    $scope.validateDuplicateOccupation=function(){
         
        if($scope.LifeEngageProduct.Payer.OccupationDetails!==undefined){
            if($scope.LifeEngageProduct.Payer.OccupationDetails[1]!==undefined){
                if($scope.LifeEngageProduct.Payer.OccupationDetails[0].natureofWork === $scope.LifeEngageProduct.Payer.OccupationDetails[1].natureofWork){
							
                    $scope.LifeEngageProduct.Payer.OccupationDetails[1]="";
				    $scope.lePopupCtrl.showWarning(
								translateMessages($translate,
								"lifeEngage"),
								translateMessages($translate,
								"illustrator.duplicateOccupation"),
								translateMessages($translate,
								"fna.ok"));                            
                }
            }
        }      
        $scope.refresh();
        $scope.dynamicValidations();
    }
    $scope.updateDatepickerBox=function(){   
        $('.custdatepicker').on('changeDate change blur',function () { 
            $('.datepicker').hide();
            $scope.futureDateValidation();
                    });
        
        $scope.refresh();
    }
        
    $scope.futureDateValidation=function(){
        if($scope.LifeEngageProduct.Payer.BasicDetails.identityExpDate!==undefined && $scope.LifeEngageProduct.Payer.BasicDetails.identityExpDate!=="" && $scope.LifeEngageProduct.Payer.BasicDetails.identityExpDate!==null){
            if($scope.LifeEngageProduct.Payer.BasicDetails.identityExpDate < getFormattedDateYYYYMMDDEapp(new Date())){
                $scope.dynamicValidations();
            }
		}
        $scope.updateErrorCount('PolicyHolderSubTab');   
        $scope.refresh();
    }
    
    $scope.dynamicValidations=function() {       
        $scope.dynamicErrorMessages = [];
        $scope.dynamicErrorCount = 0;
        var _errors = [];
        var _errorCount = 0;        
        
        $scope.updateDatepickerBox();
        
        
        if($scope.LifeEngageProduct.Insured.ContactDetails.homeNumber===undefined || $scope.LifeEngageProduct.Insured.ContactDetails.homeNumber===""){
            if($scope.LifeEngageProduct.Insured.ContactDetails.homeExtension!==undefined && $scope.LifeEngageProduct.Insured.ContactDetails.homeExtension!==""){
                _errorCount++;
                var error = {};
                $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                error.message = translateMessages($translate,"eapp_vt.extensionWithoutNumberHome");            
                error.key = "PayerContacthomeNumber";
                _errors.push(error);
                $scope.dynamicErrorMessages = _errors;
            }
        }
        
        if($scope.LifeEngageProduct.Payer.ContactDetails.officeNumber===undefined){
            $scope.LifeEngageProduct.Payer.ContactDetails.officeNumber={};
        }
        if($scope.LifeEngageProduct.Payer.ContactDetails.officeNumber.number===undefined || $scope.LifeEngageProduct.Payer.ContactDetails.officeNumber.number===""){
            if($scope.LifeEngageProduct.Payer.ContactDetails.officeNumber.extension!==undefined && $scope.LifeEngageProduct.Payer.ContactDetails.officeNumber.extension!==""){
                _errorCount++;
                var error = {};
                $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                error.message = translateMessages($translate,"eapp_vt.extensionWithoutNumberOffice");            
                error.key = "ConvenientofficeNumber";
                _errors.push(error);
                $scope.dynamicErrorMessages = _errors;
            }
        }
        
        if($scope.LifeEngageProduct.Payer.BasicDetails.firstName && $scope.LifeEngageProduct.Payer.BasicDetails.firstName.length <3 ){     
                        _errorCount++;
                        var error = {};
                        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                        error.message = translateMessages($translate,"lms.leadDetailsSectionleadFirstNameLengthValidationMessage");            
                        error.key = "lifeMainAssuredFirstName";
                        _errors.push(error);
                        $scope.dynamicErrorMessages = _errors;					                    
		          }
        
        if($scope.citizenIdIdentityProof!==undefined && $scope.citizenIdIdentityProof!=="" && $scope.LifeEngageProduct.Payer.BasicDetails.nationality==='THA'){
            $scope.LifeEngageProduct.Payer.BasicDetails.IDcard=$scope.citizenIdIdentityProof;
        }
        
        if($scope.passportIdentityProof!==undefined && $scope.passportIdentityProof!=="" && $scope.LifeEngageProduct.Payer.BasicDetails.nationality!=='THA'){
            $scope.LifeEngageProduct.Payer.BasicDetails.IDcard=$scope.passportIdentityProof;
        }
        
        //Company Name of occupation code 2
//        if($scope.LifeEngageProduct.Payer.OccupationDetails!==undefined){
//            if($scope.LifeEngageProduct.Payer.OccupationDetails[1]!==undefined){
//                if($scope.LifeEngageProduct.Payer.OccupationDetails[1].natureofWork!==undefined && $scope.LifeEngageProduct.Payer.OccupationDetails[1].natureofWork!==""){
//                    if($scope.LifeEngageProduct.Payer.OccupationDetails[1].companyName===undefined || $scope.LifeEngageProduct.Payer.OccupationDetails[1].companyName===""){
//                        _errorCount++;
//                        var error = {};
//                        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
//                        error.message = translateMessages($translate,"eapp_vt.eappCompanyNameMandatory1Payer");            
//                        error.key = "MainPayerCompanyName1";
//                        _errors.push(error);
//                        $scope.dynamicErrorMessages = _errors;
//                    }
//                }
//            }
//        }
        
        //Annual income of occupation code 2
        if($scope.LifeEngageProduct.Payer.OccupationDetails!==undefined){
            if($scope.LifeEngageProduct.Payer.OccupationDetails[1]!==undefined){
                if($scope.LifeEngageProduct.Payer.OccupationDetails[1].natureofWork!==undefined && $scope.LifeEngageProduct.Payer.OccupationDetails[1].natureofWork!==""){
                    if($scope.LifeEngageProduct.Payer.OccupationDetails[1].annualIncome===undefined || $scope.LifeEngageProduct.Payer.OccupationDetails[1].annualIncome==="" || isNaN($scope.LifeEngageProduct.Payer.OccupationDetails[1].annualIncome)){
                        _errorCount++;
                        var error = {};
                        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                        error.message = translateMessages($translate,"eapp_vt.eappAnnulIncomeValidationMessage2Payer");            
                        error.key = "annualIncome1";
                        _errors.push(error);
                        $scope.dynamicErrorMessages = _errors;
                    }
                }
            }
        }
        
        if($scope.LifeEngageProduct.Payer.OccupationDetails!==undefined){
            if($scope.LifeEngageProduct.Payer.OccupationDetails[1]!==undefined){
                if($scope.LifeEngageProduct.Payer.OccupationDetails[1].natureofWork===undefined || $scope.LifeEngageProduct.Payer.OccupationDetails[1].natureofWork===""){
                    if($scope.LifeEngageProduct.Payer.OccupationDetails[1].annualIncome!==undefined && $scope.LifeEngageProduct.Payer.OccupationDetails[1].annualIncome!=="" && !isNaN($scope.LifeEngageProduct.Payer.OccupationDetails[1].annualIncome)){
                        _errorCount++;
                        var error = {};
                        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                        error.message = translateMessages($translate,"eapp_vt.annualIncomeWithoutOccupation");            
                        error.key = "annualIncome1";
                        _errors.push(error);
                        $scope.dynamicErrorMessages = _errors;
                    }
                }
            }
        }
        
        if($scope.LifeEngageProduct.Payer.ContactDetails.officeAddress===undefined){
            $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress={};
        }

        if($scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.ward!==undefined && $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress.ward!=="" && ($scope.LifeEngageProduct.Payer.ContactDetails.isPermanentAddressSameAsCurrentAddress===undefined || $scope.LifeEngageProduct.Payer.ContactDetails.isPermanentAddressSameAsCurrentAddress==="")){
            $scope.LifeEngageProduct.Payer.ContactDetails.isPermanentAddressSameAsCurrentAddress='No';
        }
        
        if(!($scope.LifeEngageProduct.Payer.ContactDetails.homeNumber===undefined || $scope.LifeEngageProduct.Payer.ContactDetails.homeNumber==="")){
        if($scope.LifeEngageProduct.Payer.ContactDetails.homeNumber && $scope.LifeEngageProduct.Payer.ContactDetails.homeNumber.length >9 ||
        $scope.LifeEngageProduct.Payer.ContactDetails.homeNumber && $scope.LifeEngageProduct.Payer.ContactDetails.homeNumber.length < 9){
            _errorCount++;
            var error = {};
            $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
            error.message = translateMessages($translate,"eapp_vt.eAppInsuredConvenientHomeNumber");            
            error.key = "PayerContacthomeNumber";
            _errors.push(error);
            $scope.dynamicErrorMessages = _errors;
        }
        }

        if($scope.LifeEngageProduct.Payer.ContactDetails.officeNumber===undefined){
            $scope.LifeEngageProduct.Payer.ContactDetails.officeNumber={};
        }
        if($scope.LifeEngageProduct.Payer.ContactDetails.officeNumber && 
        $scope.LifeEngageProduct.Payer.ContactDetails.officeNumber.number && $scope.LifeEngageProduct.Payer.ContactDetails.officeNumber.number.length >9 ||
        $scope.LifeEngageProduct.Payer.ContactDetails.officeNumber.number && $scope.LifeEngageProduct.Payer.ContactDetails.officeNumber.number.length < 9){
            _errorCount++;
            var error = {};
            $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
            error.message = translateMessages($translate,"eapp_vt.eAppInsuredConvenientofficeNumber");            
            error.key = "ConvenientofficeNumber";
            _errors.push(error);
            $scope.dynamicErrorMessages = _errors;
        }

        if($scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber1 && $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber1.length >10 ||
        $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber1 && $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber1.length < 10){
            _errorCount++;
            var error = {};
            $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
            error.message = translateMessages($translate,"eapp_vt.eAppInsuredConvenientmobileNumber1");            
            error.key = "ConvenientmobileNumber1";
            _errors.push(error);
            $scope.dynamicErrorMessages = _errors;
        }

        if($scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber2 && $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber2.length >10 ||
        $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber2 && $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber2.length < 10){
            _errorCount++;
            var error = {};
            $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
            error.message = translateMessages($translate,"eapp_vt.eAppInsuredConvenientmobileNumber2");            
            error.key = "ConvenientmobileNumber2";
            _errors.push(error);
            $scope.dynamicErrorMessages = _errors;
        }
            
        if($scope.citizenIdIdentityProof && $scope.LifeEngageProduct.Payer.BasicDetails.nationality==='THA' && $scope.validateCitizenId()){
            _errorCount++;
            var error = {};
            $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
            error.message = translateMessages($translate, "lms.invalidCitizenId");
            error.key = 'IDcard';
            _errors.push(error);
            $scope.dynamicErrorMessages = _errors;
        } 
        
        if($scope.LifeEngageProduct.Payer.BasicDetails.firstName && $scope.LifeEngageProduct.Payer.BasicDetails.firstName.length <3 ){     
            _errorCount++;
            var error = {};
            $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
            error.message = translateMessages($translate, "lms.leadDetailsSectionleadFirstNameLengthValidationMessage");
            error.key = 'lifeMainAssuredFirstName';
            _errors.push(error);     
            $scope.dynamicErrorMessages = _errors;
        }
        
        if($scope.LifeEngageProduct.Payer.BasicDetails.identityExpDate!==undefined && $scope.LifeEngageProduct.Payer.BasicDetails.identityExpDate!=="" && $scope.LifeEngageProduct.Payer.BasicDetails.identityExpDate!==null){
            if($scope.LifeEngageProduct.Payer.BasicDetails.identityExpDate < getFormattedDateYYYYMMDDEapp(new Date())){
                _errorCount++;
                var error = {};
                $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                error.message = translateMessages($translate, "eapp_vt.identityExpPastDate");
                error.key = 'lifeMainAssuredIdentityDate';
                _errors.push(error);     
                $scope.dynamicErrorMessages = _errors;
            }
		}
        
        if($scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer==='No' && $scope.LifeEngageProduct.Insured.BasicDetails.IDcard===$scope.citizenIdIdentityProof && $scope.LifeEngageProduct.Payer.BasicDetails.nationality=='THA' && $scope.LifeEngageProduct.Insured.BasicDetails.nationality=='THA'){
                _errorCount++;
                var error = {};
                $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                error.message = translateMessages($translate, "eapp_vt.duplicateCitizenIdInsuredPayer");
                error.key = 'IDcard';
                _errors.push(error);     
                $scope.dynamicErrorMessages = _errors;
        }
		
        
        $scope.dynamicErrorCount = _errorCount;
        if (!$scope.dynamicErrorMessages) {
            $scope.dynamicErrorMessages = [];
        }
        $scope.dynamicErrorMessages = _errors;   
        $scope.updateErrorCount('PolicyHolderSubTab');    
        $scope.refresh();
         
    }
    
    $scope.validateCitizenId=function(){
        var idCardSubstr=$scope.citizenIdIdentityProof.substring(0,12);
        var count=0;
        var len=$scope.citizenIdIdentityProof.length+1;
        var rem=0;
        var quotient=0;
        var min=0;
        var nDigit=0;
                    
        for(var i=0;i<idCardSubstr.length;i++)
         {
            len=len-1;                            
            count=count+parseInt($scope.citizenIdIdentityProof.substring(i,i+1),10)*len;
         }
                    
         quotient = Math.floor(count/11);
         rem=count%11;
         min=11-rem;
         nDigit=parseInt($scope.citizenIdIdentityProof.substring(12,13),10);
          
         if(min>9){
             min=min%10;
         }
        
         if(nDigit!==min)
         {
            return true;
         }
    }; 
    
    $scope.updateAutomatically=function(){
        if($scope.LifeEngageProduct.Payer.ContactDetails.isPermanentAddressSameAsCurrentAddress!==undefined){
            if($scope.LifeEngageProduct.Payer.ContactDetails.isPermanentAddressSameAsCurrentAddress==='Yes'){
                $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress=angular.copy($scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress);
            }           
        }
        
        if($scope.LifeEngageProduct.Payer.ContactDetails.isCopyFrom!==undefined){
            if($scope.LifeEngageProduct.Payer.ContactDetails.isCopyFrom==='HouseholdRegistrationAddress'){
                $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress=angular.copy($scope.LifeEngageProduct.Payer.ContactDetails.permanentAddress);
            }else if($scope.LifeEngageProduct.Payer.ContactDetails.isCopyFrom==='CurrentAddress'){
                $scope.LifeEngageProduct.Payer.ContactDetails.officeAddress=angular.copy($scope.LifeEngageProduct.Payer.ContactDetails.currentAddress);
            }
        }
        $scope.dynamicValidations();
    }
    
    //Occupation Nature of work
//    if(EappVariables.prepopulatedProposerData.OccupationDetails && ($scope.LifeEngageProduct.Insured.BasicDetails.illustrationInsSameAsPayer===$scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer)){
//       if(EappVariables.prepopulatedProposerData.OccupationDetails.length===1){
//
//       if($scope.LifeEngageProduct.Payer.OccupationDetails[0].natureofWork===undefined || $scope.LifeEngageProduct.Payer.OccupationDetails[0].natureofWork===""){
//           $scope.LifeEngageProduct.Payer.OccupationDetails[0].natureofWork=EappVariables.prepopulatedProposerData.OccupationDetails[0].occupationCode;
//       }
//                                                                        }
//        
//       if(EappVariables.prepopulatedProposerData.OccupationDetails.length===2){
//           
//        if($scope.LifeEngageProduct.Payer.OccupationDetails[0].natureofWork===undefined || $scope.LifeEngageProduct.Payer.OccupationDetails[0].natureofWork===""){
//           $scope.LifeEngageProduct.Payer.OccupationDetails[0].natureofWork=EappVariables.prepopulatedProposerData.OccupationDetails[0].occupationCode;
//        }
//        if($scope.LifeEngageProduct.Payer.OccupationDetails[1].natureofWork===undefined || $scope.LifeEngageProduct.Payer.OccupationDetails[1].natureofWork===""){
//            $scope.LifeEngageProduct.Payer.OccupationDetails[1].natureofWork=EappVariables.prepopulatedProposerData.OccupationDetails[1].occupationCode;
//        }        
//    }
//    }
    
    $scope.validateMaritalStatus=function(){
        if($scope.LifeEngageProduct.Payer.BasicDetails.gender==='Male' && $scope.LifeEngageProduct.Payer.BasicDetails.maritalStatus==='Widow'){
            $scope.LifeEngageProduct.Payer.BasicDetails.maritalStatus="";
        }
        $scope.updateErrorCount('PolicyHolderSubTab');
        $scope.refresh();  
    }

    $scope.prependZero = function(){
        var prependZero;

        if($scope.LifeEngageProduct.Payer.ContactDetails.homeNumber && $scope.LifeEngageProduct.Payer.ContactDetails.homeNumber.indexOf(0) != 0){
                
            prependZero = '0'+ $scope.LifeEngageProduct.Payer.ContactDetails.homeNumber;
            $scope.LifeEngageProduct.Payer.ContactDetails.homeNumber = prependZero;

         }
        else if($scope.LifeEngageProduct.Payer.ContactDetails.officeNumber && $scope.LifeEngageProduct.Payer.ContactDetails.officeNumber.number && $scope.LifeEngageProduct.Payer.ContactDetails.officeNumber.number.indexOf(0) != 0){

            prependZero = '0'+ $scope.LifeEngageProduct.Payer.ContactDetails.officeNumber.number;
            $scope.LifeEngageProduct.Payer.ContactDetails.officeNumber.number = prependZero;

        }else if($scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber1 && $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber1.indexOf(0) != 0){

                 prependZero = '0'+ $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber1;
                $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber1 = prependZero;

        } 
        if($scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber2 && $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber2.indexOf(0) != 0){

            prependZero = '0'+ $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber2;
            $scope.LifeEngageProduct.Payer.ContactDetails.mobileNumber2 = prependZero;
        }

        $scope.dynamicValidations();
        $scope.updateErrorCount('PolicyHolderSubTab');
        $scope.refresh();   
    }
    
    
    
    $scope.updateExpiryDate=function(){
        if($scope.LifeEngageProduct.Payer.BasicDetails.isValidForLife){
            $scope.LifeEngageProduct.Payer.BasicDetails.identityExpDate="";
        }
    }
    
    $scope.clearField=function(val){
        if(val==='id'){
            $scope.LifeEngageProduct.Payer.BasicDetails.IDcard="";
        }else if(val==='DateOfExpiry'){
            $scope.LifeEngageProduct.Payer.BasicDetails.identityExpDate="";
            $scope.LifeEngageProduct.Payer.BasicDetails.IDcard="";
            $scope.LifeEngageProduct.Payer.BasicDetails.identityProof="";
            $scope.citizenIdIdentityProof="";
            $scope.passportIdentityProof="";
        }
        
        $scope.dynamicValidations();
        $scope.updateErrorCount('PolicyHolderSubTab');
        $scope.refresh();
    }
    
    $scope.calculateAge = function (dob, id) {
        var age = "";
        if (dob != "" && dob != undefined &&
            dob != null) {
            var dobDate = new Date(dob);
            /*
             * In I.E it will accept date format in '/'
             * separator only
             */
            if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
                dobDate = new Date(dob.split("-").join(
                    "/"));
            }
            var todayDate = new Date();
            if (formatForDateControl(dob) >= formatForDateControl(new Date())) {
                //$scope.Illustration.Insured.BasicDetails.age = 0;
                age = 0;
            } else {
                var curd = new Date(todayDate
                    .getFullYear(), todayDate
                    .getMonth(), todayDate
                    .getDate());
                var cald = new Date(dobDate
                    .getFullYear(), dobDate
                    .getMonth(), dobDate.getDate());
                var dife = $scope.ageCalculation(curd,
                    cald);

                if (age < 1) {
                    var datediff = curd.getTime() -
                        cald.getTime();
                    var days = (datediff / (24 * 60 * 60 * 1000)) / 365;
                    days = Number(Math.round(days +
                            'e3') +
                        'e-3');
                    age = 0;
                }
                if (dife[1] == "false") {
                    age = dife[0];
                } else {
                    age = dife[0] + 1;
                }

                if (dob == undefined || dob == "") {
                    age = "-";
                }
                //$scope.Illustration.Insured.BasicDetails.age = age;
            }
        }
        //else {
        //$scope.Illustration.Insured.BasicDetails.age = "";
        // }
        if (id == 'insuredDOB') {
            $scope.LifeEngageProduct.Payer.BasicDetails.age = age;
        } else if (id == 'payerDOB') {
            $scope.LifeEngageProduct.Payer.BasicDetails.age = age;
        }
         $scope.onFieldChange();
//        $scope.validateInsuredAge();        
    };
    
    $scope.updateNationality=function(){
        //Splice when insured age is less than 16
    if($scope.LifeEngageProduct.Payer.BasicDetails.age<16){
        $scope.payerIdentityTypeApp=angular.copy($scope.identityTypeApp);
        $scope.payerIdentityTypeApp.splice(6,1);              
        $scope.payerIdentityTypeApp.splice(5,1);   
        $scope.payerIdentityTypeApp.splice(4,1);  
            
        $scope.validateNationality();       
    }else{
        $scope.payerIdentityTypeApp=angular.copy($scope.identityTypeApp);
        $scope.payerIdentityTypeApp.splice(3,1);
        $scope.validateNationality();
    }
    }

    $scope.updateDate = function () {
        $scope.calculateAge($scope.LifeEngageProduct.Payer.BasicDetails.dob, 'insuredDOB');
        $scope.refresh();
    }
    
    //Calculating age in years, month , days 
    $scope.ageCalculation = function (currDate, dobDate) {
        var date1 = new Date(currDate);
        var date2 = new Date(dobDate);
        var y1 = date1.getFullYear(),
            m1 = date1
            .getMonth(),
            d1 = date1.getDate(),
            y2 = date2
            .getFullYear(),
            m2 = date2.getMonth(),
            d2 = date2
            .getDate();

        var nextDOB;
        if (m1 < m2) {
            nextDOB = new Date(date2.setYear(date1.getFullYear()));
        } else if ((m1 == m2) && (d1 < d2)) {
            nextDOB = new Date(date2.setYear(date1.getFullYear()));
        } else {
            nextDOB = new Date(date2.setYear(date1.getFullYear() + 1));
        }
        var marginDate = new Date(date1.setMonth(date1.getMonth() + 6));

        if (m1 < m2) {
            y1--;
            m1 += 12;
        }

        if (nextDOB.getTime() > marginDate.getTime()) {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() - birthDate.getFullYear();
            var mnth = today.getMonth() - birthDate.getMonth();
            if (mnth < 0 || (mnth === 0 && today.getDate() < birthDate.getDate())) {
                ageVal--;
            }
            return [ageVal, "false"];
        } else {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() - birthDate.getFullYear();
            var mnth = today.getMonth() - birthDate.getMonth();
            if (mnth < 0 || (mnth === 0 && today.getDate() < birthDate.getDate())) {
                ageVal--;
            }
            return [ageVal, "true"];
        }

    }
    
    
    //Splice Office Address for Copy From dropdown
    if($scope.copyFromLookup!==undefined){
        $scope.copyFromLookup.splice(1,1);
        
        $scope.refresh();
    }
    
    //Splice Widow from Marital status when Gender is male
    $scope.spliceWidowInMaritalDropdown=function(){
        if($scope.LifeEngageProduct.Payer.BasicDetails.gender==='Male' && $scope.maritalStatus.length==4){
            $scope.customMaritalStatus=angular.copy($scope.maritalStatus);        
            $scope.customMaritalStatus.splice(2,1);       
            
            $scope.refresh();
        }else{
            $scope.customMaritalStatus=angular.copy($scope.maritalStatus);   
        }
    }
    
    if($scope.LifeEngageProduct.Payer.BasicDetails.gender!=='' && $scope.LifeEngageProduct.Payer.BasicDetails.gender!==undefined){
        $scope.spliceWidowInMaritalDropdown();
    }
    
    $scope.validateNationality=function(){            
        if($scope.LifeEngageProduct.Payer.BasicDetails.nationality!=="" && $scope.LifeEngageProduct.Payer.BasicDetails.nationality!==undefined){
            if($scope.LifeEngageProduct.Payer.BasicDetails.nationality!=='THA'){
                $scope.defaultIdentityTypeApp=angular.copy($scope.payerIdentityTypeApp);
                var length=$scope.defaultIdentityTypeApp.length;
                for(var i=0;i<length;i++){
                    for(var j=0;j<$scope.defaultIdentityTypeApp.length;j++){
                        if($scope.defaultIdentityTypeApp[j].code!=='PP'){
                            $scope.defaultIdentityTypeApp.splice(j,1);
                        }
                    }
                }
                $scope.LifeEngageProduct.Payer.BasicDetails.identityProof='PP';
            }else{
                $scope.defaultIdentityTypeApp=$scope.payerIdentityTypeApp;
            }
        }else{
            $scope.defaultIdentityTypeApp=$scope.payerIdentityTypeApp;
        }
        if($scope.LifeEngageProduct.Payer.BasicDetails.nationality==='THA' || $scope.LifeEngageProduct.Payer.BasicDetails.nationality===undefined || $scope.LifeEngageProduct.Payer.BasicDetails.nationality===""){
            $scope.foreignerInd=false;
        }else{
            $scope.foreignerInd=true;
        }
//        $scope.dynamicValidations();
//        $scope.updateErrorCount('PolicyHolderSubTab');
        $scope.refresh();
    }
    
    //Splice when insured age is less than 16
    if($scope.LifeEngageProduct.Payer.BasicDetails.age<16){
        $scope.payerIdentityTypeApp=angular.copy($scope.identityTypeApp);
        $scope.payerIdentityTypeApp.splice(6,1);              
        $scope.payerIdentityTypeApp.splice(5,1);   
        $scope.payerIdentityTypeApp.splice(4,1);           
            
        $scope.validateNationality();
    }else{
        $scope.payerIdentityTypeApp=angular.copy($scope.identityTypeApp);
        $scope.payerIdentityTypeApp.splice(3,1);
        $scope.validateNationality();
    }
            
    if($scope.LifeEngageProduct.Payer.BasicDetails.nationality==='THA' || $scope.LifeEngageProduct.Payer.BasicDetails.nationality===undefined || $scope.LifeEngageProduct.Payer.BasicDetails.nationality===""){
            $scope.foreignerInd=false;
    }else{
        $scope.foreignerInd=true;
    }

    if($scope.LifeEngageProduct.Payer.BasicDetails.IDcard!==undefined && $scope.LifeEngageProduct.Payer.BasicDetails.IDcard!==""){
            if($scope.LifeEngageProduct.Payer.BasicDetails.nationality==='THA' || $scope.LifeEngageProduct.Payer.BasicDetails.nationality===undefined || $scope.LifeEngageProduct.Payer.BasicDetails.nationality===""){
                $scope.citizenIdIdentityProof=$scope.LifeEngageProduct.Payer.BasicDetails.IDcard;
            }else if($scope.LifeEngageProduct.Payer.BasicDetails.nationality!=='THA'){
                $scope.passportIdentityProof=$scope.LifeEngageProduct.Payer.BasicDetails.IDcard;
            }
        }   
    
    $scope.validateInsuredAge=function(){  
        $rootScope.errorFlagTitle=false;
        
        if($scope.titleGenderPairPayer!==undefined && $scope.errorFlag!==true && $scope.LifeEngageProduct.Payer.BasicDetails.gender!==undefined && $scope.LifeEngageProduct.Payer.BasicDetails.gender!=="" && $scope.LifeEngageProduct.Payer.BasicDetails.title!==undefined && $scope.LifeEngageProduct.Payer.BasicDetails.title!=="" & $scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer==="No"){            
                if($scope.titleGenderPairPayer[0].code!=="Both" && $scope.titleGenderPairPayer!=="Both"){
                    if($scope.titleGenderPairPayer[0].code!==$scope.LifeEngageProduct.Payer.BasicDetails.gender && $scope.titleGenderPairPayer!==$scope.LifeEngageProduct.Payer.BasicDetails.gender){
                        $rootScope.errorFlagTitle=true;
//                        $scope.leadDetShowPopUpMsg = true;
//                        $scope.enterMandatory = translateMessages($translate, "illustrator.titleGenderMismatchPayer");
                        
                    }
                }
        }         
    } 
}