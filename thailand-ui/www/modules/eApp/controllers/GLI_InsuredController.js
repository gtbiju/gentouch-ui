'use strict';
/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:GLI_InsuredController
 CreatedDate:16/11/2015
 Description:GLI_InsuredController
 */
storeApp.controller('GLI_InsuredController', GLI_InsuredController);
GLI_InsuredController.$inject = ['$rootScope', '$scope', 'DataService', 'GLI_DataService', 'LookupService', 'DocumentService', 'ProductService', '$compile', '$routeParams', '$location', '$translate', '$debounce', 'UtilityService', 'PersistenceMapping', 'AutoSave', 'globalService', 'EappVariables', 'EappService', 'GLI_EappVariables', 'GLI_EappService', '$timeout', 'DataLookupService', '$controller', 'GLI_EvaluateService','GLI_DataLookupService'];
function GLI_InsuredController($rootScope, $scope, DataService, GLI_DataService, LookupService, DocumentService, ProductService, $compile, $routeParams, $location, $translate, $debounce, UtilityService, PersistenceMapping, AutoSave, globalService, EappVariables, EappService, GLI_EappVariables, GLI_EappService, $timeout, DataLookupService, $controller, GLI_EvaluateService,GLI_DataLookupService) {
    $controller('InsuredController', {
        $rootScope: $rootScope,
        $scope: $scope,
        DataService: DataService,
        GLI_DataService: GLI_DataService,
        LookupService: LookupService,
        DocumentService: DocumentService,
        ProductService: ProductService,
        $compile: $compile,
        $routeParams: $routeParams,
        $location: $location,
        $translate: $translate,
        $debounce: $debounce,
        UtilityService: UtilityService,
		DataLookupService:GLI_DataLookupService,
        PersistenceMapping: PersistenceMapping,
        AutoSave: AutoSave,
        globalService: globalService,
        EappVariables: EappVariables,
        EappService: EappService,
        $timeout: $timeout
    });
    $scope.LifeEngageProduct.Insured['isSameAsPayerModified'] = false;
	$scope.LifeEngageProduct.Insured.BasicDetails.isNationalityThai=false;
    $scope.alphanumericPattern = rootConfig.alphanumericPattern;
    $scope.alphabetWithSpacePatternIllustration=rootConfig.alphabetWithSpacePatternIllustration;
    $scope.alphabeticPattern = rootConfig.alphabeticPattern;
    $scope.eappSpecialCharacters = rootConfig.eappSpecialCharacters;
    $scope.alphanumericPattern = rootConfig.alphanumericPattern;
    $scope.alphanumericPatterng = rootConfig.alphanumericPatterng;
    $scope.passportEappPattern=rootConfig.passportEappPattern;
    $scope.identityTypeApp = $rootScope.identityTypeApp;    
    $scope.convenientContact=$rootScope.convenientContact;
    $scope.copyFromLookup=angular.copy($rootScope.convenientContact);
    $scope.$parent.$on('mainInsuredSubTab', function (event, args) {
        $scope.selectedTabId = args[0];
        $scope.isReasonMandatory = false;
		 var typeNameList = [
			{ "type": "STATE", "isCacheable": true },
			{ "type": "DISTRICT", "isCacheable": true },
			{ "type": "DOCUMENT_IDENTITY", "isCacheable": true },
			{ "type": "DOCUMENT_IDENTITY_PAYER", "isCacheable": true },
			{ "type": "PAYMENTRECORDS", "isCacheable": true },
			{ "type": "FATCA", "isCacheable": true },
			{ "type": "HEALTHRECORD", "isCacheable": true },
			{ "type": "PREVIOUSPOLICY", "isCacheable": true },
			{ "type": "CONSENTLETTER", "isCacheable": true },
			{ "type": "FINANCIALRECORDS", "isCacheable": true },
			{ "type": "FORIGNERQUESTIONNAIRE", "isCacheable": true },
			{ "type": "OTHERS", "isCacheable": true },
			{ "type": "PHYSICALAPPFORMS", "isCacheable": true },
			{ "type": "COMPANYNAME", "isCacheable": true },
			{ "type": "INSURANCECOMPANY", "isCacheable": true },
			{ "type": "PROVINCECODE", "isCacheable": true },
			{ "type": "ISSUEBANK", "isCacheable": true },
			{ "type": "ISSUEBRANCH", "isCacheable": true },
			{ "type": "ISSUEBRANCHCODE", "isCacheable": true },
			{ "type": "RECEIVEBRANCH", "isCacheable": true },
			{ "type": "RECEIVEBRANCHCODE", "isCacheable": true },
			{ "type": "PAYINCASHBRANCHCODE", "isCacheable": true },
			{ "type": "PBOCC_CLASS", "isCacheable": true },
			{ "type": "PAYMENTBANKNAME", "isCacheable": true },
			{ "type": "PAYMENTOPTION", "isCacheable": true },
			{ "type": "NATUREOFWORK", "isCacheable": true },
			
			{ "type": "IDENTITYTYPE", "isCacheable": true },
			{ "type": "BENEFICIARYIDENTITY", "isCacheable": true },
			{ "type": "CONVENIENTCONTACT", "isCacheable": true },
			{ "type": "MARITALSTATUS", "isCacheable": true },
			{ "type": "BANKNAME", "isCacheable": true },
			{ "type": "NATIONALITY", "isCacheable": true },
			{ "type": "WARD", "isCacheable": true }];
			

		GLI_DataLookupService.getMultipleLookUpData(typeNameList, function (lookUpList) {
			$scope.Initialize();
			$scope.onGenderChangeInsured($scope.LifeEngageProduct.Insured.BasicDetails.gender);
			$rootScope.validationBeforesave = function (value, successcallback) {
				if (value == "save") {
					$scope.LifeEngageProduct.LastVisitedIndex = "1,1";
				}
				if (rootConfig.isDeviceMobile) {
					EappVariables.setEappModel($scope.LifeEngageProduct);
				} 
				successcallback();
			};
		}, function () {
			console.log("Error in multilookup data call");	
		});
    });
    function getLookUpDataFromCode (data, value) {
        return data.filter(function (data) {
            return angular.lowercase(data.code) == angular.lowercase(value)
        });
    }
    $scope.onSelectTypeHead = function (id) {
        $timeout(function () {
            $('#' + id).blur();
        }, 300);
    }
    $scope.convertToString = function (model) {
        $scope.annualIncome = model.toString();
        $scope.LifeEngageProduct.Insured.BasicDetails.annualIncome = $scope.annualIncome;
    }
    $scope.formatLabel = function (model, options, type) {
        if (options && options.length > 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code) {
                    return options[i].value;
                }
            }
        } else {
            if (type == 'city') {
                $scope.state = $rootScope.state;
                for (var i = 0; i < $scope.state.length; i++) {
                    if (model === $scope.state[i].code) {
                        return $scope.state[i].value;
                    }
                }
            } else if (type == 'ward') {
                $scope.ward = $rootScope.wardOrHamlet;
                for (var i = 0; i < $scope.ward.length; i++) {
                    if (model === $scope.ward[i].code) {
                        return $scope.ward[i].value;
                    }
                }
            } else if (type == 'nationalityLsit') {
                $scope.nationalityLsit = $rootScope.nationalityLsit;
                for (var i = 0; i < $scope.nationalityLsit.length; i++) {
                    if (model === $scope.nationalityLsit[i].code) {
                        return $scope.nationalityLsit[i].value;
                    }
                }            
            } else if (type == 'OccupationCategory') {
                $scope.OccupationCategory = $rootScope.occupationList;
                for (var i = 0; i < $scope.OccupationCategory.length; i++) {
                    if (model === $scope.OccupationCategory[i].code) {
                        return $scope.OccupationCategory[i].value;
                    }
                }
            }
        }
    };
    $scope.getValueCode = function (code_model, array_list) {
        if (code_model) {
            var _lastIndex = code_model.lastIndexOf('.');
            if (_lastIndex > 0) {
                var parentModel = code_model.substring(0, _lastIndex);
                var childModel = code_model.substring(_lastIndex + 1, code_model.length);
                var newChild = childModel + "Value";
                var scopeVar = GLI_EvaluateService.evaluateString($scope, parentModel);
                var inputCode = GLI_EvaluateService.evaluateString($scope, code_model);
                /*(var scopeVar=eval ('$scope.' + parentModel);	
                var inputCode = eval ('$scope.' + code_model);*/
                var outputValue = getLookUpDataFromCode(array_list, inputCode);
                if (outputValue && outputValue != "" && outputValue.length > 0) {
                    scopeVar[newChild] = outputValue[0].value;
                }
                $scope.refresh();
            }
        }
    }

    //For clearing out district/ward fields on updating town/district fields
    $scope.markAsEdited = function (field) {
        //existing function not required
    }

    //For counting no.of AdditionalQuestions selected
    $scope.callAdditionalQuestionCount = function (val, questionId) {
        if (val == "indonesia" || val == "Indonesia") {
            val = 'No';
        } else {
            val = 'Other';
        }
        var curentArr = [];
        if ($scope.LifeEngageProduct.hasInsuredQuestions && ($scope.LifeEngageProduct.hasInsuredQuestions != "0" || $scope.LifeEngageProduct.hasInsuredQuestions != 0) && $scope.LifeEngageProduct.hasInsuredQuestions != "") {
            curentArr = JSON.parse($scope.LifeEngageProduct.hasInsuredQuestions).selectedArr;
        }
        EappService.updateAdditionalQuestionCount(val, questionId, curentArr, function (modifiedArr) {
            var modifiedJson = {
                "selectedArr": []
            };
            modifiedJson.selectedArr = modifiedArr;
            $scope.LifeEngageProduct.hasInsuredQuestions = JSON.stringify(modifiedJson);
        });
    }

    $rootScope.validationBeforesave = function (value, successcallback) {
        successcallback();
    };
    $scope.dynamicErrorMessages = [];
    $scope.dynamicErrorCount = 0;
    var _errors = [];
    var _errorCount = 0;
    $scope.insTabsErrorCount = function (id, index) {
        selectedtab = id;
        $scope.updateErrorCount(id);
        $scope.showErrorCount = true;
        $rootScope.selectedPage = "InsuredDetails";
        $scope.emailPattern = rootConfig.emailPattern;
        $scope.showDescriptionOthersForOccupation = false;
        $scope.showPopUp = false;

        for (var i = 0; i < $scope.innertabsVertical.length; i++) {
            if (i == index) {
                $scope.innertabsVertical[i] = 'selected';
            } else {
                $scope.innertabsVertical[i] = '';
            }
        }
    };

    $scope.isForeigner = false;
    $scope.isDisplay = false;
    $scope.ShowOthersType = function (fieldValue) {
        if (parseInt(fieldValue) == 5) {
            $scope.isDisplay = true;
        } else {
            $scope.isDisplay = false;
        }
    }

    //Identity Date popup
    $scope.checkDate = function () {
        var identityDate = new Date($scope.LifeEngageProduct.Insured.BasicDetails.identityDate);
        var today = new Date();
        today.setYear(today.getFullYear() - 15);
        if (identityDate.getFullYear() < today.getFullYear()) {
            $scope.showPopUp = true;
        } else if (identityDate.getFullYear() == today.getFullYear()) {
            if (identityDate.getMonth() < today.getMonth()) {
                $scope.showPopUp = true;
            } else if (identityDate.getMonth() == today.getMonth()) {
                if (identityDate.getDate() < today.getDate()) {
                    $scope.showPopUp = true;
                } else {
                    $scope.showPopUp = false;
                }
            } else {
                $scope.showPopUp = false;
            }
        } else {
            $scope.showPopUp = false;
        }

        if ($scope.showPopUp == true) {
            $('.custdatepicker').blur();
            $rootScope.lePopupCtrl.showError(
                translateMessages($translate, "lifeEngage"),
                translateMessages($translate, "eapp_vt.idExpired"),
                translateMessages($translate, "lms.ok"));
        }
    }
//    $scope.IsResidentAddrSame = function (model, tabsinner) {
//        if (model == "Yes") {
//            $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.city = $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.city;
//            $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.district = $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.district;
//            $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.ward = $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.ward;
//            $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.houseNo = $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.houseNo;
//            $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.street = $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.street;
//            $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.country = $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.country;
//        } else {
//            $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress = {};
//        }
//        $scope.refresh();
//        $scope.updateErrorCount(tabsinner);
//
//    }
    
    $scope.setCategoryCode = function (model, occupation) {
        if (model) {
            var _lastIndex = model.lastIndexOf('.');
            if (_lastIndex > 0) {
                var parentModel = model.substring(0, _lastIndex);
                var value = GLI_EvaluateService.evaluateString($scope, model);
                var scopeVar = GLI_EvaluateService.evaluateString($scope, parentModel);
                /* var value=eval ('$scope.' + model);
                 var scopeVar=eval ('$scope.' + parentModel);*/
                if (occupation) {
                    var value = getLookUpDataFromCode(occupation, value);
                    if (value && value != "" && value.length > 0) {
                        scopeVar['occupationCategoryValue'] = value[0].value;
                        //scopeVar['natureofWork']="";
                    }
                }
            }
        }
    }

    $scope.updateErrorCount = function (id) {
        
        if(EappVariables.EappKeys.Key15 != "Pending Submission" && EappVariables.EappKeys.Key15 !=undefined && EappVariables.EappKeys.Key15 !="") {
            $timeout(function () {
                $rootScope.updateErrorCount(id);
            }, 1000);        
        }else{
            $rootScope.updateErrorCount(id);
        }
                
        $scope.eAppParentobj.errorCount = $scope.errorCount;
        $scope.selectedTabId = id;
        EappVariables.setEappModel($scope.LifeEngageProduct);
        $scope.refresh();
    }

    $scope.onFieldChange = function () {        
        $timeout(function () {
            $scope.updateErrorCount("mainInsuredSubTab");
        }, 50);
    }



    $scope.Initialize = function () {
       if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_Payment#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_Payment#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step2Payment#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step2Payment#tabDetails_1');
		}	
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step3Payment#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_step3Payment#tabDetails_1');		
			
        } if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_BeneficiarySubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_BeneficiarySubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_financilaDetailsSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_financilaDetailsSubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_mainInsuredSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_mainInsuredSubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PolicyHolderSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PolicyHolderSubTab#tabDetails_1');
        }		
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
		}
        $scope.viewToBeCustomized = 'mainInsuredSubTab';
        $scope.$parent.disableProceedButtonCommom = false;
        if ($scope.LifeEngageProduct.Insured.BasicDetails.annualIncome) {
            $scope.annualIncome = $scope.LifeEngageProduct.Insured.BasicDetails.annualIncome.toString();
            $scope.LifeEngageProduct.Insured.BasicDetails.annualIncome = $scope.annualIncome;
        }
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            //EappVariables.setEappModel($scope.LifeEngageProduct);
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
        }
//        if(EappVariables.getEappModel().Insured.Questionnaire.LifestyleDetails.Questions.length < 43){
//            EappVariables.getEappModel().Insured.Questionnaire = GLI_EappVariables.InsuredQuestionnaire;
//		    EappVariables.getEappModel().Payer.Questionnaire = GLI_EappVariables.PayerQuestionnaire;
//            EappVariables.getEappModel().Proposer.Questions = GLI_EappVariables.ProposerQuestions;
//        }
        $scope.LifeEngageProduct = EappVariables.getEappModel();
        
        $scope.eAppParentobj.PreviousPage = "";

        //To solve pre-populated data(country & nationality) getting cleared on tab navigation before save
//        if (rootConfig.isDeviceMobile) {
//            if ($scope.LifeEngageProduct.Insured.BasicDetails.countryofResidence == "") {
//                EappService.populateIndependentLookupDate('COUNTRY_LIST', 'BirthCountry', 'LifeEngageProduct.Insured.BasicDetails.countryofResidence', true, "lifeMainAssuredBirthCountry", "mainInsuredSubTab", $scope);
//            }
//            if ($scope.LifeEngageProduct.Insured.BasicDetails.nationality == "") {
//                EappService.populateIndependentLookupDate('COUNTRY_LIST', 'Nationality', 'LifeEngageProduct.Insured.BasicDetails.nationality', true, "lifeMainAssuredNationality", "mainInsuredSubTab", $scope);
//            }
//        }


        $timeout(function () {
            if(!$scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber1){
                 $scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber1 = $scope.PrepopulatedDisabledEappInsuredData.ContactDetails.mobileNumber1;
            }
            if($scope.LifeEngageProduct.ApplicationNo == undefined || $scope.LifeEngageProduct.ApplicationNo == ""){
                $scope.LifeEngageProduct.ApplicationNo = 0;
            }
            
            $scope.prependZero();
            
            $scope.updateErrorCount($scope.selectedTabId);
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission") {
                UtilityService.disableAllActionElements();
            }else if (typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) {
                UtilityService.disableAllActionElements();
            }
        }, 50);

        if (EappVariables.EappKeys.Key15 == "Confirmed" && $scope.$parent.LifeEngageProduct.Declaration.confirmSignature == true) {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = true;
        } else {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
        }
        if ($scope.LifeEngageProduct.Insured.CustomerRelationship) {
            $scope.setReasonMandatory($scope.LifeEngageProduct.Insured.CustomerRelationship.relationWithInsured);
        }

        //For disabling and enabling the tabs.And also to know the last visited page.---starts
        $rootScope.selectedPage = "InsuredDetails";
        $scope.LifeEngageProduct.LastVisitedUrl = "1,1,'mainInsuredSubTab',true,'mainInsuredSubTab',''";
        $scope.eAppParentobj.nextPage = "1,2,'PolicyHolderSubTab',true,'PolicyHolderSubTab',''";
        $scope.eAppParentobj.CurrentPage = "";
        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[1] < 1 && subIndex[0] <= $scope.LifeEngageProduct.LastVisitedUrl[0]) {
                $scope.LifeEngageProduct.LastVisitedIndex = "1,1";
            }
        }
        //For disabling and enabling the tabs.And also to know the last visited page.---ends

        //Used to disable the fields which is prepopulated from BI or FNA
        $scope.PrepopulatedDisabledEappInsuredData = angular.copy(EappVariables.prepopulatedInsuredData);
        
        if($scope.LifeEngageProduct.Insured.BasicDetails.lastName==="" || $scope.LifeEngageProduct.Insured.BasicDetails.lastName===undefined){
            $scope.LifeEngageProduct.Insured.BasicDetails.lastName=EappVariables.prepopulatedInsuredData.BasicDetails.lastName;
        }
        
        if($scope.LifeEngageProduct.Payer.BasicDetails.IDcard==="" || $scope.LifeEngageProduct.Payer.BasicDetails.IDcard===undefined){
            $scope.LifeEngageProduct.Payer.BasicDetails.IDcard="";
        }

        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        angular.element('#' + $scope.selectedTabId + "_a").trigger('click');
        $scope.updateErrorCount($scope.selectedTabId);
        /*if ($scope.LifeEngageProduct.Insured.ContactDetails.isPermanentAddressSameAsCurrentAddress=='Yes') {
        	$scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress = $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress;
        }*/
        // $scope.occupationRule('InsuredDetails','LifeEngageProduct.Insured.OccupationDetails.occupationCategoryValue');

        var model = "LifeEngageProduct";
        if ((rootConfig.autoSave.eApp) && (typeof EappVariables.EappKeys.Key15 == "undefined" || EappVariables.EappKeys.Key15 == "New" || EappVariables.EappKeys.Key15 == "Confirmed")) {
            if (AutoSave.destroyWatch) {
                AutoSave.destroyWatch();
            }
            AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
        } else if (AutoSave.destroyWatch) {
            AutoSave.destroyWatch();
        }
        //Default to Thai
        if($scope.LifeEngageProduct.Insured.BasicDetails.nationality===undefined || $scope.LifeEngageProduct.Insured.BasicDetails.nationality===""){
            $scope.LifeEngageProduct.Insured.BasicDetails.nationality='THA';
            $scope.foreignerInd=false;
        }

        if($scope.LifeEngageProduct.Insured.ContactDetails.homeExtension==='' || $scope.LifeEngageProduct.Insured.ContactDetails.homeExtension==undefined){
                        $scope.LifeEngageProduct.Insured.ContactDetails.homeExtension="";
                }        
        $scope.prependZero();
        $scope.dynamicValidations();        
        setTimeout(function(){$scope.validateNationality();},1000);
    }



    $scope.onGenderChangeInsured = function (gender) {
        var dataOfRelationShipWithPH = EappService.getRelationShipWithPolicyHolder();
        if (typeof gender != "undefined" || gender != "") {
            var genderOfInsured = gender;
        } else {
            var genderOfInsured = "Male";
        }
        $scope.relationshipWithPH;
        for (key in dataOfRelationShipWithPH[0]) {
            if (key == genderOfInsured) {
                $scope.relationshipWithPH = dataOfRelationShipWithPH[0][key];
                break;
            }
        }
    };

    // validating typeahead field
    $scope.updateDynmaicErrMessage = function (erroKey) {
        for (var i = 0; i < $scope.dynamicErrorMessages.length; i++) {
            if ($scope.dynamicErrorMessages[i].key == erroKey) {
                $scope.dynamicErrorMessages.splice(i, 1);
                $scope.dynamicErrorCount = $scope.dynamicErrorCount - 1;
            }
        }
        $scope.updateErrorCount('mainInsuredSubTab');
    }
    function invalidNationality(errorKey) {
        var error = {};
        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
        error.message = translateMessages(
            $translate,
            "lms.leadDetailsSectionnationalityRequiredValidationMessage");
        error.key = errorKey;
        _errors.push(error);

    }
    function invalidResidentCountry (errorKey) {
        var error = {};
        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
        error.message = translateMessages(
            $translate,
            "lms.leadDetailsSectionCountryofbirthRequiredValidationMessage");
        error.key = errorKey;
        _errors.push(error);

    }
    function invalidOccupation(errorKey) {
        var error = {};
        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
        error.message = translateMessages(
            $translate,
            "lms.leadDetailsSectionoccupationRequiredValidationMessage");
        error.key = errorKey;
        _errors.push(error);

    }
    function invalidCity (errorKey) {
        var error = {};
        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
        error.message = translateMessages(
            $translate,
            "eapp.validCityErrorMessage");
        error.key = errorKey;
        _errors.push(error);

    }

    function setToModel(model, value) {
        var _lastIndex = model.lastIndexOf('.');
        if (_lastIndex > 0) {
            var parentModel = model.substring(0, _lastIndex);
            var childmodel = model.substring(_lastIndex + 1, model.length);
            var scopeVar = GLI_EvaluateService.evaluateString($scope, parentModel);
            //var scopeVar=eval ('$scope.' + parentModel);	
            scopeVar[childmodel] = value;
        }
    }
    // validating typeahead field
    $scope.validateField = function (model, list, fieldTobeValidated, errorKey, child_dependent) {
        if (model != "" && model != null && model != undefined) {
            var isFieldValid = GLI_EappService.validateSmartSearch(model, list);
            if (isFieldValid == false) {
                $scope.updateDynmaicErrMessage(errorKey);
//                if (fieldTobeValidated == "nationality") {
//                    $scope.isForeigner = false;
//                    invalidNationality(errorKey);
//                } else if (fieldTobeValidated == "country") {
//                    invalidResidentCountry(errorKey);
//                } else if (fieldTobeValidated == "occupation") {
//                    invalidOccupation(errorKey);
//                } else if (fieldTobeValidated == "city") {
//                    if (child_dependent) {
//                        setToModel(child_dependent, '');
//                    }
//                    invalidCity(errorKey);
//                }
                $scope.dynamicErrorMessages = _errors;
            } else {
                if (fieldTobeValidated == "nationality" && isFieldValid != "Indonesia") {
                    $scope.isForeigner = true;
                } else if (fieldTobeValidated == "nationality" && isFieldValid == "Indonesia") {
                    $scope.isForeigner = false;
                }
                $scope.updateDynmaicErrMessage(errorKey);
            }
            $scope.refresh();
            $scope.updateErrorCount('mainInsuredSubTab');
        }
    }

    $scope.IsInsuredHomeAddSame = function (model, tabsinner) {
        if (model == "Yes") {
            $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress = angular.copy($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress);
            $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.homeNumber1 = $scope.LifeEngageProduct.Insured.ContactDetails.homeNumber1;
        } else {
            $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress = {};
            $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.homeNumber1 = "";
        }
        $scope.refresh();
        $timeout(function () {
            $scope.updateErrorCount(tabsinner);
        }, 50);
    };


    $scope.checkHomeAddress = function (model) {
        var flag = false;
        switch (model) {
            case "addressLine1":
                if ($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.addressLine1 != $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.addressLine1) {
                    flag = true;
                }
                break;
            case "addressLine2":
                if ($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.addressLine2 != $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.addressLine2) {
                    flag = true;
                }
                break;
            case "city":
                if ($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.city != $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.city) {
                    flag = true;
                }
                break;
            case "state":
                if ($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.state != $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.state) {
                    flag = true;
                }
                break;
            case "zipCode":
                if ($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.zipCode != $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.zipCode) {
                    flag = true;
                }
                break;
            case "homeNumber1":
                if ($scope.LifeEngageProduct.Insured.ContactDetails.homeNumber1 != $scope.LifeEngageProduct.Proposer.ContactDetails.homeNumber1) {
                    flag = true;
                }
                break;
        }
        if (flag == true) {
            $scope.LifeEngageProduct.Insured.ContactDetails.isPermanentAddressSameAsCurrentAddress = "No";
        }
    };

    //to set reason for insurance mandatory
    $scope.setReasonMandatory = function (data) {
        if (data == "Siblings" || data == "Others") {
            $scope.isReasonMandatory = true;
        } else {
            $scope.isReasonMandatory = false;
        }
    };

    //update the age from dob
    $scope.getAgeFromDOB = function (proposerdob) {
        var age;
        age = getPartyAge(proposerdob);
        $scope.LifeEngageProduct.Insured.BasicDetails.age = age;
    };
    $scope.getLookUpData = function (data, value) {
        return data.filter(function (data) {
            return angular.lowercase(data.value) == angular.lowercase(value)
        });
    }
    /*$scope.occupationRule = function(person,model){
			$scope.occupaionQuestionnaire  = "";
			var data= {
					"person":person
			};
			if(model){
				var _lastIndex=model.lastIndexOf('.');
				if(_lastIndex>0){
				   var parentModel=model.substring(0,_lastIndex);
				   var value=eval ('$scope.' + model);
				   var scopeVar=eval ('$scope.' + parentModel);
				   if($scope.OccupationCategory){
				   		var value = $scope.getLookUpData($scope.OccupationCategory,value);
				    	if( value && value != "" && value.length > 0){
				  	 		scopeVar['occupationCategory']=value[0].code;
				  		}
				   }		   
				}
			}
		    if(scopeVar['occupationCategory']) {
		    	GLI_EappService.runOccupationRule($scope,data,function(result){			
                	if(result && result!=null){	
						$scope.occupaionQuestionnaire = result;	
						$scope.refresh();				
					}				
				});
		    }
		   
		}*/

    //To solve pre-populated data getting cleared issue on tab navigation(country of residence & Nationality) before save
    $scope.getValue = function (model) {
        if ($scope.isVariableExists(model)) {
            return GLI_EvaluateService.evaluateString($scope, model);
        }
    };

    $scope.setValuetoScope = function (model, value) {
        var _lastIndex = model.lastIndexOf('.');
        if (_lastIndex > 0) {
            var parentModel = model.substring(0, _lastIndex);
            var scopeVar = $scope.getValue(parentModel);
            var remain_parentModel = model.substring(_lastIndex + 1, model.length);
            scopeVar[remain_parentModel] = value;
        }
        $scope.refresh();
    };

    $scope.isVariableExists = function (variable) {
        variable = variable.split('.')
        var obj = $scope[variable.shift()];
        while (obj && variable.length) obj = obj[variable.shift()];
        return obj;
    };

    $scope.$on("$destroy", function () {
        if (this != null && typeof this !== 'undefined') {
            if (this.$$destroyed) return;
            while (this.$$childHead) {
                this.$$childHead.$destroy();
            }
            if (this.$broadcast) {
                this.$broadcast('$destroy');
            }
            this.$$destroyed = true;
            $timeout.cancel(timer);
        }
    });
    
    $scope.clearFieldCurrent=function(val){
                    
                    if(val=='address' && $scope.editMode && $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.city){						
						$scope.allowFormatLabel=true;
                        $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.city="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.state="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.provinceCode="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.zipcode="";                             
						$scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.cityValue="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.stateValue="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.wardOrHamletValue="";
					}else if(val=='address' && !$scope.editMode && $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.city){
						$scope.allowFormatLabel=true;
                        $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.city="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.state="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.provinceCode="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.zipcode="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.cityValue="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.stateValue="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.wardOrHamletValue="";
                    }
    };
    $scope.clearFieldPermanent=function(val){
                    
                    if(val=='address' && $scope.editMode && $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.city){						
						$scope.allowFormatLabel=true;
                        $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.city="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.state="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.provinceCode="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.zipcode="";                            $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.cityValue="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.stateValue="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.wardOrHamletValue="";
					}else if(val=='address' && !$scope.editMode && $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.city){
						$scope.allowFormatLabel=true;
                        $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.city="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.state="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.provinceCode="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.zipcode="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.cityValue="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.stateValue="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.wardOrHamletValue="";
                    }
    };
    $scope.clearFieldOffice=function(val){
                    
                    if(val=='address' && $scope.editMode && $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.city){						
						$scope.allowFormatLabel=true;
                        $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.city="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.state="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.provinceCode="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.zipcode="";                             $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.cityValue="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.stateValue="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.wardOrHamletValue="";
					}else if(val=='address' && !$scope.editMode && $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.city){
						$scope.allowFormatLabel=true;
                        $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.city="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.state="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.provinceCode="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.zipcode="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.cityValue="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.stateValue="";
                        $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.wardOrHamletValue="";
                    }
    };
	$scope.formatLabelCurrentAddress = function (model, options, type) {        
                    if (options && options.length >= 1) {
                        for (var i = 0; i < options.length; i++) {
                            if (model === options[i].code && type !== 'district') {
                                if (type == 'wardOrHamlet') {
                                    var splitWard=[];
                                    var optionValue=options[i].value;
                                    if(optionValue.indexOf(',')>0){
										$rootScope.splitInd=true;
       		   			                 optionValue=optionValue.substring(0,options[i].value.length-1);
       		   			                 splitWard=optionValue.split(',');
                                         $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.wardOrHamletValue = splitWard[0];
                                    }else{
                                    $rootScope.splitInd=false;
                                    $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.wardOrHamletValue = options[i].value;
                                    }
                                    $rootScope.updateAddress=true;                                    
                                }
                                return options[i].value;
                            }
                            if (type == 'district' && ($scope.allowFormatLabel || $rootScope.updateAddress)){
                                $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.cityValue = options[0].value;
                                $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.stateValue = options[1].value;
                                $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.zipcodeValue = options[2].value;
                                $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.city = options[0].code;
                                $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.state = options[1].code;
                                
                                //Province code mapping for LA - currentAddress   
                                if($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress!==undefined){
                                if($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.state!==undefined && $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.state!==""){
                                    for (var i = 0; i < $rootScope.provinceCode.length; i++) {
                                        if ($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.state === $rootScope.provinceCode[i].code) {
                                            $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.provinceCode = $rootScope.provinceCode[i].value;
                                        }
                                    }
                                }    
                                }
                                $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.zipcode = options[2].value;
                                $rootScope.updateAddress=false;
                                return options[0].value;
                            }

                            if(type == 'wardOrHamlet' && !$scope.allowFormatLabel && ($rootScope.updateAddress || $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.city)){
								if($rootScope.splitInd && !$scope.editMode){
                                    $rootScope.splitInd=false;
									return $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.wardOrHamletValue+","+$scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.cityValue;
								}else{
									return $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.wardOrHamletValue;
								}
							}else if(type == 'district' && ((!$scope.editMode && !$scope.allowFormatLabel)||($scope.editMode && !$scope.allowFormatLabel)||(!$scope.editMode && $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.ward))){
								return model;
							}
                        }
                    }
                };
    
    $scope.formatLabelOfficeAddres = function (model, options, type) {        
                    if (options && options.length >= 1) {
                        for (var i = 0; i < options.length; i++) {
                            if (model === options[i].code && type !== 'district') {
                                if (type == 'wardOrHamlet') {
                                    var splitWard=[];
                                    var optionValue=options[i].value;
                                    if(optionValue.indexOf(',')>0){
										$rootScope.splitInd=true;
       		   			                 optionValue=optionValue.substring(0,options[i].value.length-1);
       		   			                 splitWard=optionValue.split(',');
                                         $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.wardOrHamletValue = splitWard[0];
                                    }else{
                                    $rootScope.splitInd=false;
                                    $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.wardOrHamletValue = options[i].value;
                                    }
                                    $rootScope.updateAddress=true;                                    
                                }
                                return options[i].value;
                            }
                            if (type == 'district' && ($scope.allowFormatLabel || $rootScope.updateAddress)){
                                $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.cityValue = options[0].value;
                                $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.stateValue = options[1].value;
                                $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.zipcodeValue = options[2].value;
                                $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.city = options[0].code;
                                $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.state = options[1].code;
                                //Province code mapping for LA - officeAddress   
                                if($scope.LifeEngageProduct.Insured.ContactDetails.officeAddress!==undefined){
                                if($scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.state!==undefined && $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.state!==""){
                                    for (var i = 0; i < $rootScope.provinceCode.length; i++) {
                                        if ($scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.state === $rootScope.provinceCode[i].code) {
                                            $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.provinceCode = $rootScope.provinceCode[i].value;
                                        }
                                    }
                                }    
                                }
                                $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.zipcode = options[2].value;
                                $rootScope.updateAddress=false;
                                return options[0].value;
                            }

                            if(type == 'wardOrHamlet' && !$scope.allowFormatLabel && ($rootScope.updateAddress || $scope.LifeEngageProduct.Insured.ContactDetails && $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress && $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.city)){
								if($rootScope.splitInd && !$scope.editMode){
                                    $rootScope.splitInd=false;
									return $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.wardOrHamletValue+","+$scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.cityValue;
								}else{
									return $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.wardOrHamletValue;
								}
							}else if(type == 'district' && ((!$scope.editMode && !$scope.allowFormatLabel)||($scope.editMode && !$scope.allowFormatLabel)||(!$scope.editMode && $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress.ward))){
								return model;
							}
                        }
                    }
                };
    
    $scope.formatLabelPermanentAddress = function (model, options, type) {        
                    if (options && options.length >= 1) {
                        for (var i = 0; i < options.length; i++) {
                            if (model === options[i].code && type !== 'district') {
                                if (type == 'wardOrHamlet') {
                                    var splitWard=[];
                                    var optionValue=options[i].value;
                                    if(optionValue.indexOf(',')>0){
										$rootScope.splitInd=true;
       		   			                 optionValue=optionValue.substring(0,options[i].value.length-1);
       		   			                 splitWard=optionValue.split(',');
                                         $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.wardOrHamletValue = splitWard[0];
                                    }else{
                                    $rootScope.splitInd=false;
                                    $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.wardOrHamletValue = options[i].value;
                                    }
                                    $rootScope.updateAddress=true;                                    
                                }
                                return options[i].value;
                            }
                            if (type == 'district' && ($scope.allowFormatLabel || $rootScope.updateAddress)){
                                $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.cityValue = options[0].value;
                                $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.stateValue = options[1].value;
                                $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.zipcodeValue = options[2].value;
                                $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.city = options[0].code;
                                $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.state = options[1].code;
                                //Province code mapping for LA - permanentAddress   
                                if($scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress!==undefined){
                                if($scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.state!==undefined && $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.state!==""){
                                    for (var i = 0; i < $rootScope.provinceCode.length; i++) {
                                        if ($scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.state === $rootScope.provinceCode[i].code) {
                                            $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.provinceCode = $rootScope.provinceCode[i].value;
                                        }
                                    }
                                }    
                                }
                                $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.zipcode = options[2].value;
                                $rootScope.updateAddress=false;
                                return options[0].value;
                            }

                            if(type == 'wardOrHamlet' && !$scope.allowFormatLabel && ($rootScope.updateAddress || $scope.LifeEngageProduct.Insured.ContactDetails && $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress && $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.city)){
								if($rootScope.splitInd && !$scope.editMode){
                                    $rootScope.splitInd=false;
									return $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.wardOrHamletValue+","+$scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.cityValue;
								}else{
									return $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.wardOrHamletValue;
								}
							}else if(type == 'district' && ((!$scope.editMode && !$scope.allowFormatLabel)||($scope.editMode && !$scope.allowFormatLabel)||(!$scope.editMode && $scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress.ward))){
								return model;
							}
                        }
                    }
                };
    
    $scope.copyFromHouseholdRegAddress=function(value){
        if(value==='Yes'){
            $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress=angular.copy($scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress);
        }else{
            $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.cityValue = "";
            $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.stateValue = "";
            $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.zipcodeValue = "";
            $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.city = "";
            $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.state = "";
            $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.zipcode = "";
            $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.ward="";
            $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.wardOrHamletValue="";
            $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.streetName="";
            $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.lane="";
            $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.mooNo="";
            $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.buildingName="";
            $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.houseNo="";
            
            if($scope.LifeEngageProduct.Insured.ContactDetails.isCopyFrom!==undefined){
                if($scope.LifeEngageProduct.Insured.ContactDetails.isCopyFrom==='HouseholdRegistrationAddress'){
                    $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress=angular.copy($scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress);
                }else if($scope.LifeEngageProduct.Insured.ContactDetails.isCopyFrom==='CurrentAddress'){
                    $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress=angular.copy($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress);
                }
            }
        }        
    }
    
    $scope.copyFrom=function(value){
        if(value==="" || value===undefined || value===null){
            $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress={};
        }else if(value==='HouseholdRegistrationAddress'){
            $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress=angular.copy($scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress);
        }else{
            $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress=angular.copy($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress);
        }        
    }
    
    //Default to House hold Address
//    if($scope.LifeEngageProduct.Insured.ContactDetails.isCopyFrom===undefined || $scope.LifeEngageProduct.Insured.ContactDetails.isCopyFrom===""){
//       // $scope.LifeEngageProduct.Insured.ContactDetails.isCopyFrom='houseHoldAddress';    
//            $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress=angular.copy($scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress);
//        
//    }
    
    if($scope.LifeEngageProduct.Insured.BasicDetails.TrineeAgent === undefined || $scope.LifeEngageProduct.Insured.BasicDetails.TrineeAgent === ""){
        $scope.LifeEngageProduct.Insured.BasicDetails.TrineeAgent ="No";
    }

    $scope.updateDatepickerBox=function(){
        $('.custdatepicker').on('changeDate change blur',function () { 
            $('.datepicker').hide();
            $scope.futureDateValidation();
                    });
        
        $scope.refresh();
    }
    
    $scope.futureDateValidation=function(){
        if($scope.LifeEngageProduct.Insured.BasicDetails.identityExpDate!==undefined && $scope.LifeEngageProduct.Insured.BasicDetails.identityExpDate!=="" && $scope.LifeEngageProduct.Insured.BasicDetails.identityExpDate!==null){
            if($scope.LifeEngageProduct.Insured.BasicDetails.identityExpDate < getFormattedDateYYYYMMDDEapp(new Date())){
                $scope.dynamicValidations();
            }
		}
        
        $scope.updateErrorCount('mainInsuredSubTab');
        $scope.refresh();
    }
    $scope.dynamicValidations=function() {       
        $scope.dynamicErrorMessages = [];
        $scope.dynamicErrorCount = 0;
        var _errors = [];
        var _errorCount = 0;        
        
        $scope.updateDatepickerBox();
        
        if($scope.LifeEngageProduct.Insured.ContactDetails.homeNumber===undefined || $scope.LifeEngageProduct.Insured.ContactDetails.homeNumber===""){
            if($scope.LifeEngageProduct.Insured.ContactDetails.homeExtension!==undefined && $scope.LifeEngageProduct.Insured.ContactDetails.homeExtension!==""){
                _errorCount++;
                var error = {};
                $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                error.message = translateMessages($translate,"eapp_vt.extensionWithoutNumberHome");            
                error.key = "homeNumber";
                _errors.push(error);
                $scope.dynamicErrorMessages = _errors;
            }
        }
        
        if($scope.LifeEngageProduct.Insured.ContactDetails.officeNumber.number===undefined || $scope.LifeEngageProduct.Insured.ContactDetails.officeNumber.number===""){
            if($scope.LifeEngageProduct.Insured.ContactDetails.officeNumber.extension!==undefined && $scope.LifeEngageProduct.Insured.ContactDetails.officeNumber.extension!==""){
                _errorCount++;
                var error = {};
                $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                error.message = translateMessages($translate,"eapp_vt.extensionWithoutNumberOffice");            
                error.key = "ConvenientofficeNumber";
                _errors.push(error);
                $scope.dynamicErrorMessages = _errors;
            }
        }
        
        if($scope.citizenIdIdentityProof!==undefined && $scope.citizenIdIdentityProof!=="" && $scope.LifeEngageProduct.Insured.BasicDetails.nationality==='THA'){
            $scope.LifeEngageProduct.Insured.BasicDetails.IDcard=$scope.citizenIdIdentityProof;
        }
        
        if($scope.passportIdentityProof!==undefined && $scope.passportIdentityProof!=="" && $scope.LifeEngageProduct.Insured.BasicDetails.nationality!=='THA'){
            $scope.LifeEngageProduct.Insured.BasicDetails.IDcard=$scope.passportIdentityProof;
        }
        
        //Company Name of occupation code 2
//        if($scope.LifeEngageProduct.Insured.OccupationDetails!==undefined){
//            if($scope.LifeEngageProduct.Insured.OccupationDetails[1]!==undefined){
//                if($scope.LifeEngageProduct.Insured.OccupationDetails[1].natureofWork!==undefined && $scope.LifeEngageProduct.Insured.OccupationDetails[1].natureofWork!==""){
//                    if($scope.LifeEngageProduct.Insured.OccupationDetails[1].companyName===undefined || $scope.LifeEngageProduct.Insured.OccupationDetails[1].companyName===""){
//                        _errorCount++;
//                        var error = {};
//                        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
//                        error.message = translateMessages($translate,"eapp_vt.eappCompanyNameMandatory1");            
//                        error.key = "MainInsuredCompanyName1";
//                        _errors.push(error);
//                        $scope.dynamicErrorMessages = _errors;
//                    }
//                }
//            }
//        }
        
        //Annual income of occupation code 2
        if($scope.LifeEngageProduct.Insured.OccupationDetails!==undefined){
            if($scope.LifeEngageProduct.Insured.OccupationDetails[1]!==undefined){
                if($scope.LifeEngageProduct.Insured.OccupationDetails[1].natureofWork!==undefined && $scope.LifeEngageProduct.Insured.OccupationDetails[1].natureofWork!==""){
                    if($scope.LifeEngageProduct.Insured.OccupationDetails[1].annualIncome===undefined || $scope.LifeEngageProduct.Insured.OccupationDetails[1].annualIncome==="" || isNaN($scope.LifeEngageProduct.Insured.OccupationDetails[1].annualIncome)){
                        _errorCount++;
                        var error = {};
                        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                        error.message = translateMessages($translate,"eapp_vt.eappAnnulIncomeValidationMessage2");            
                        error.key = "annualIncome1";
                        _errors.push(error);
                        $scope.dynamicErrorMessages = _errors;
                    }
                }
            }
        }
        
        if($scope.LifeEngageProduct.Insured.ContactDetails.officeAddress===undefined){
            $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress={};
        }

        if($scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress===undefined){
			$scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress={};
		}

        if($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress===undefined){
			$scope.LifeEngageProduct.Insured.ContactDetails.currentAddress={};
		}

        if($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.ward!==undefined && $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.ward!=="" && ($scope.LifeEngageProduct.Insured.ContactDetails.isPermanentAddressSameAsCurrentAddress===undefined || $scope.LifeEngageProduct.Insured.ContactDetails.isPermanentAddressSameAsCurrentAddress==="")){
            $scope.LifeEngageProduct.Insured.ContactDetails.isPermanentAddressSameAsCurrentAddress='No';
        }
        
        if(!($scope.LifeEngageProduct.Insured.ContactDetails.homeNumber===undefined || $scope.LifeEngageProduct.Insured.ContactDetails.homeNumber==="")){
            if($scope.LifeEngageProduct.Insured.ContactDetails.homeNumber && $scope.LifeEngageProduct.Insured.ContactDetails.homeNumber.length >9 ||
            $scope.LifeEngageProduct.Insured.ContactDetails.homeNumber && $scope.LifeEngageProduct.Insured.ContactDetails.homeNumber.length < 9){
                _errorCount++;
                var error = {};
                $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                error.message = translateMessages($translate,"eapp_vt.eAppInsuredConvenientHomeNumber");            
                error.key = "homeNumber";
                _errors.push(error);
                $scope.dynamicErrorMessages = _errors;
            }
        }

        if($scope.LifeEngageProduct.Insured.ContactDetails.officeNumber.number && $scope.LifeEngageProduct.Insured.ContactDetails.officeNumber.number.length >9 ||
        $scope.LifeEngageProduct.Insured.ContactDetails.officeNumber.number && $scope.LifeEngageProduct.Insured.ContactDetails.officeNumber.number.length < 9){
            _errorCount++;
            var error = {};
            $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
            error.message = translateMessages($translate,"eapp_vt.eAppInsuredConvenientofficeNumber");            
            error.key = "ConvenientofficeNumber";
            _errors.push(error);
            $scope.dynamicErrorMessages = _errors;
        }

        if($scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber1 && $scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber1.length >10 ||
        $scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber1 && $scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber1.length < 10){
            _errorCount++;
            var error = {};
            $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
            error.message = translateMessages($translate,"eapp_vt.eAppInsuredConvenientmobileNumber1");            
            error.key = "ConvenientmobileNumber1";
            _errors.push(error);
            $scope.dynamicErrorMessages = _errors;
        }

        if($scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber2 && $scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber2.length >10 ||
        $scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber2 && $scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber2.length < 10){
            _errorCount++;
            var error = {};
            $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
            error.message = translateMessages($translate,"eapp_vt.eAppInsuredConvenientmobileNumber2");            
            error.key = "ConvenientmobileNumber2";
            _errors.push(error);
            $scope.dynamicErrorMessages = _errors;
        }
            
		if($scope.citizenIdIdentityProof && $scope.LifeEngageProduct.Insured.BasicDetails.nationality==='THA' && $scope.validateCitizenId()){
            _errorCount++;
            var error = {};
            $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
            error.message = translateMessages($translate, "lms.invalidCitizenId");
            error.key = 'IDcard';
            _errors.push(error);
            $scope.dynamicErrorMessages = _errors;
		} 
            
        if($scope.LifeEngageProduct.Insured.BasicDetails.firstName && $scope.LifeEngageProduct.Insured.BasicDetails.firstName.length <3 ){     
            _errorCount++;
            var error = {};
            $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
            error.message = translateMessages($translate, "lms.leadDetailsSectionleadFirstNameLengthValidationMessage");
            error.key = 'lifeMainAssuredFirstName';
            _errors.push(error);     
            $scope.dynamicErrorMessages = _errors;
        }
        
        if($scope.LifeEngageProduct.Insured.BasicDetails.identityExpDate!==undefined && $scope.LifeEngageProduct.Insured.BasicDetails.identityExpDate!=="" && $scope.LifeEngageProduct.Insured.BasicDetails.identityExpDate!==null){
            if($scope.LifeEngageProduct.Insured.BasicDetails.identityExpDate < getFormattedDateYYYYMMDDEapp(new Date())){
                _errorCount++;
                var error = {};
                $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                error.message = translateMessages($translate, "eapp_vt.identityExpPastDate");
                error.key = 'lifeMainAssuredIdentityDate';
                _errors.push(error);     
                $scope.dynamicErrorMessages = _errors;
            }
		}
        
        if($scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer==='No' && $scope.LifeEngageProduct.Payer.BasicDetails.IDcard===$scope.citizenIdIdentityProof && $scope.LifeEngageProduct.Payer.BasicDetails.nationality=='THA' && $scope.LifeEngageProduct.Insured.BasicDetails.nationality=='THA'){
                _errorCount++;
                var error = {};
                $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                error.message = translateMessages($translate, "eapp_vt.duplicateCitizenIdInsuredPayer");
                error.key = 'IDcard';
                _errors.push(error);     
                $scope.dynamicErrorMessages = _errors;
        }
        
        if($scope.LifeEngageProduct.Beneficiaries!==undefined){
            for(var u=0;u<$scope.LifeEngageProduct.Beneficiaries.length;u++){
                if(($scope.LifeEngageProduct.Beneficiaries[u].BasicDetails.nationalIDType==='NID' || $scope.LifeEngageProduct.Beneficiaries[u].BasicDetails.nationalIDType==='CID') && $scope.LifeEngageProduct.Insured.BasicDetails.nationality=='THA' && $scope.LifeEngageProduct.Beneficiaries[u].BasicDetails.identityProof===$scope.citizenIdIdentityProof){
                    _errorCount++;
                    var error = {};
                    $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                    error.message = translateMessages($translate, "eapp_vt.duplicateCitizenIdInsuredBeneficiary");
                    error.key = 'IDcard';
                    _errors.push(error);     
                    $scope.dynamicErrorMessages = _errors;
                }
            }
        }        
        
        $scope.dynamicErrorCount = _errorCount;
        if (!$scope.dynamicErrorMessages) {
            $scope.dynamicErrorMessages = [];
        }
        $scope.dynamicErrorMessages = _errors;   
        // $rootScope.updateErrorCount('mainInsuredSubTab');    
        $scope.updateErrorCount('mainInsuredSubTab');
        $scope.refresh();
         
    }
    
    $scope.validateCitizenId=function(){
		var idCardSubstr=$scope.citizenIdIdentityProof.substring(0,12);
		var count=0;
		var len=$scope.citizenIdIdentityProof.length+1;
		var rem=0;
		var quotient=0;
		var min=0;
		var nDigit=0;
                    
        for(var i=0;i<idCardSubstr.length;i++)
         {
			len=len-1;                            
			count=count+parseInt($scope.citizenIdIdentityProof.substring(i,i+1),10)*len;
         }
                    
         quotient = Math.floor(count/11);
         rem=count%11;
         min=11-rem;
         nDigit=parseInt($scope.citizenIdIdentityProof.substring(12,13),10);
          
         if(min>9){
             min=min%10;
         }
        
         if(nDigit!==min)
         {
            return true;
		 }
    }; 
    
    $scope.updateAutomatically=function(){
        if($scope.LifeEngageProduct.Insured.ContactDetails.isPermanentAddressSameAsCurrentAddress!==undefined){
            if($scope.LifeEngageProduct.Insured.ContactDetails.isPermanentAddressSameAsCurrentAddress==='Yes'){
                $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress=angular.copy($scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress);
            }           
        }
        
        if($scope.LifeEngageProduct.Insured.ContactDetails.isCopyFrom!==undefined){
            if($scope.LifeEngageProduct.Insured.ContactDetails.isCopyFrom==='HouseholdRegistrationAddress'){
                $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress=angular.copy($scope.LifeEngageProduct.Insured.ContactDetails.permanentAddress);
            }else if($scope.LifeEngageProduct.Insured.ContactDetails.isCopyFrom==='CurrentAddress'){
                $scope.LifeEngageProduct.Insured.ContactDetails.officeAddress=angular.copy($scope.LifeEngageProduct.Insured.ContactDetails.currentAddress);
            }
        }
        
        $scope.dynamicValidations();
    }
    
//    //Occupation Nature of work
//    if(EappVariables.prepopulatedInsuredData.OccupationDetails){
//       if(EappVariables.prepopulatedInsuredData.OccupationDetails.length===1){
//
//       if($scope.LifeEngageProduct.Insured.OccupationDetails[0].natureofWork===undefined || $scope.LifeEngageProduct.Insured.OccupationDetails[0].natureofWork===""){
//           $scope.LifeEngageProduct.Insured.OccupationDetails[0].natureofWork=EappVariables.prepopulatedInsuredData.OccupationDetails[0].occupationCode;
//       }
//                                                                        }
//        
//       if(EappVariables.prepopulatedInsuredData.OccupationDetails.length===2){
//           
//        if($scope.LifeEngageProduct.Insured.OccupationDetails[0].natureofWork===undefined || $scope.LifeEngageProduct.Insured.OccupationDetails[0].natureofWork===""){
//           $scope.LifeEngageProduct.Insured.OccupationDetails[0].natureofWork=EappVariables.prepopulatedInsuredData.OccupationDetails[0].occupationCode;
//        }
//        if($scope.LifeEngageProduct.Insured.OccupationDetails[1].natureofWork===undefined || $scope.LifeEngageProduct.Insured.OccupationDetails[1].natureofWork===""){
//            $scope.LifeEngageProduct.Insured.OccupationDetails[1].natureofWork=EappVariables.prepopulatedInsuredData.OccupationDetails[1].occupationCode;
//        }        
//    }
//    }
    
    
    $scope.mapRelationshipWithProposer=function(){

        if($scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer==='Yes'){
            $scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer="";
            $scope.LifeEngageProduct.Payer=angular.copy($scope.LifeEngageProduct.Insured);
        }else{
            $scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer="";
           // $scope.isInsuredSameAsPayerInitial = true;
            $scope.LifeEngageProduct.Payer['createFlow'] = true;
            
            if($scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer == "No"){

            if($scope.LifeEngageProduct.Insured.BasicDetails.illustrationInsSameAsPayer =="Yes"){
                        $scope.isInsuredSameAsPayerInitial = false;
                        $scope.isInsuredSameAsPayerFlag = false;
                        $scope.LifeEngageProduct.Payer.CustomerRelationship ={};
                        $scope.LifeEngageProduct.Payer.BasicDetails ={};
                        $scope.LifeEngageProduct.Payer.OccupationDetails = [];
                        $scope.LifeEngageProduct.Payer.ContactDetails = {};
                        $scope.LifeEngageProduct.Payer.HealthDetails = {};
                        $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress = {};
                        $scope.LifeEngageProduct.Payer.Questionnaire = EappVariables.PayerQuestionnaire;
            }else if($scope.LifeEngageProduct.Insured.BasicDetails.illustrationInsSameAsPayer =="No" && EappVariables.prepopulatedInsuredData.BasicDetails.age<16){
                $scope.isInsuredSameAsPayerFlag = true;
                $scope.LifeEngageProduct.Payer.BasicDetails = angular.copy(EappVariables.prepopulatedProposerData.BasicDetails);
                $scope.LifeEngageProduct.Payer.OccupationDetails = angular.copy(EappVariables.prepopulatedProposerData.OccupationDetails);
                $scope.LifeEngageProduct.Payer.ContactDetails =angular.copy(EappVariables.prepopulatedProposerData.ContactDetails);
                $scope.LifeEngageProduct.Payer.Questionnaire = EappVariables.PayerQuestionnaire;
            }else{
                $scope.isInsuredSameAsPayerFlag = true;
                $scope.LifeEngageProduct.Insured.BasicDetails.illustrationInsSameAsPayer ="Yes";
                $scope.LifeEngageProduct.Payer.CustomerRelationship ={};
                $scope.LifeEngageProduct.Payer.BasicDetails ={};
                $scope.LifeEngageProduct.Payer.OccupationDetails = [];
                $scope.LifeEngageProduct.Payer.ContactDetails = {};
                $scope.LifeEngageProduct.Payer.HealthDetails = {};
                $scope.LifeEngageProduct.Payer.ContactDetails.currentAddress = {};
                $scope.LifeEngageProduct.Payer.Questionnaire = EappVariables.PayerQuestionnaire;
                EappVariables.prepopulatedProposerData=angular.copy($scope.LifeEngageProduct.Payer);
            }
        
        }
        }
        EappVariables.setEappModel($scope.LifeEngageProduct);

        $scope.onFieldChange();
    }   
    
    if($scope.LifeEngageProduct.Insured.BasicDetails.isInsuredSameAsPayer == "No"){

            if($scope.LifeEngageProduct.Insured.BasicDetails.illustrationInsSameAsPayer =="Yes"){
                        
            } else if($scope.LifeEngageProduct.Insured.BasicDetails.illustrationInsSameAsPayer =="No" && EappVariables.prepopulatedInsuredData.BasicDetails.age<16){
                $scope.isInsuredSameAsPayerFlag = true;
                var identityProofID=angular.copy($scope.LifeEngageProduct.Payer.BasicDetails.identityProof);
                var identityProofIDCard=angular.copy($scope.LifeEngageProduct.Payer.BasicDetails.IDcard);
                
                $scope.LifeEngageProduct.Payer.BasicDetails = angular.copy(EappVariables.prepopulatedProposerData.BasicDetails);
                
                $scope.LifeEngageProduct.Payer.BasicDetails.identityProof=angular.copy(identityProofID);
                
				if(identityProofIDCard!==undefined && identityProofIDCard!==""){
					$scope.LifeEngageProduct.Payer.BasicDetails.IDcard=angular.copy(identityProofIDCard);	
				}				
                
                $scope.LifeEngageProduct.Payer.OccupationDetails = angular.copy(EappVariables.prepopulatedProposerData.OccupationDetails);
                $scope.LifeEngageProduct.Payer.ContactDetails =angular.copy(EappVariables.prepopulatedProposerData.ContactDetails);
            }else{
                $scope.isInsuredSameAsPayerFlag = true;
                var identityProofID=angular.copy($scope.LifeEngageProduct.Payer.BasicDetails.identityProof);
                var identityProofIDCard=angular.copy($scope.LifeEngageProduct.Payer.BasicDetails.IDcard);
				
                $scope.LifeEngageProduct.Payer.BasicDetails = angular.copy(EappVariables.prepopulatedProposerData.BasicDetails);
                
                $scope.LifeEngageProduct.Payer.BasicDetails.identityProof=angular.copy(identityProofID);
                
				if(identityProofIDCard!==undefined && identityProofIDCard!==""){
					$scope.LifeEngageProduct.Payer.BasicDetails.IDcard=angular.copy(identityProofIDCard);	
				}
                
                $scope.LifeEngageProduct.Payer.OccupationDetails = angular.copy(EappVariables.prepopulatedProposerData.OccupationDetails);
                $scope.LifeEngageProduct.Payer.ContactDetails =angular.copy(EappVariables.prepopulatedProposerData.ContactDetails);
//                $scope.LifeEngageProduct.Payer.CustomerRelationship ={};
            }        
            EappVariables.setEappModel($scope.LifeEngageProduct);            
        }
        
    $scope.validateMaritalStatus=function(){
        if($scope.LifeEngageProduct.Insured.BasicDetails.gender==='Male' && $scope.LifeEngageProduct.Insured.BasicDetails.maritalStatus==='Widow'){
            $scope.LifeEngageProduct.Insured.BasicDetails.maritalStatus="";
        }
        $scope.updateErrorCount('mainInsuredSubTab');
        $scope.refresh(); 
    }

   

    $scope.prependZero = function(){
        var prependZero;

        if($scope.LifeEngageProduct.Insured.ContactDetails.homeNumber && $scope.LifeEngageProduct.Insured.ContactDetails.homeNumber.indexOf(0) != 0){
                
                prependZero = '0'+ $scope.LifeEngageProduct.Insured.ContactDetails.homeNumber;
                $scope.LifeEngageProduct.Insured.ContactDetails.homeNumber = prependZero;

         } else if($scope.LifeEngageProduct.Insured.ContactDetails.officeNumber.number && $scope.LifeEngageProduct.Insured.ContactDetails.officeNumber.number.indexOf(0) != 0){

                prependZero = '0'+ $scope.LifeEngageProduct.Insured.ContactDetails.officeNumber.number;
                $scope.LifeEngageProduct.Insured.ContactDetails.officeNumber.number = prependZero;

        } else if($scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber1 && $scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber1.indexOf(0) != 0){

                prependZero = '0'+ $scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber1;
                $scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber1 = prependZero;

        } 
        if($scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber2 && $scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber2.indexOf(0) != 0){

            prependZero = '0'+ $scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber2;
            $scope.LifeEngageProduct.Insured.ContactDetails.mobileNumber2 = prependZero;
        }
       
        $scope.dynamicValidations();
        $scope.updateErrorCount('mainInsuredSubTab');
        $scope.refresh();       
    }
    
    
    
    $scope.updateExpiryDate=function(){
        if($scope.LifeEngageProduct.Insured.BasicDetails.isValidForLife){
            $scope.LifeEngageProduct.Insured.BasicDetails.identityExpDate="";
        }
    }
    
    $scope.clearField=function(val){
        if(val==='id'){
            $scope.LifeEngageProduct.Insured.BasicDetails.IDcard="";
        }else if(val==='DateOfExpiry'){
            $scope.LifeEngageProduct.Insured.BasicDetails.identityExpDate="";
            $scope.LifeEngageProduct.Insured.BasicDetails.IDcard="";
            $scope.LifeEngageProduct.Insured.BasicDetails.identityProof="";
            $scope.citizenIdIdentityProof="";
            $scope.passportIdentityProof="";
            if($scope.LifeEngageProduct.Insured.Questionnaire){
                if($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare){
                    if($scope.LifeEngageProduct.Insured.Questionnaire.AdditionalQuestioniare.ShortQuestions){
                        $scope.LifeEngageProduct.Insured.BasicDetails.isNationalityThai=true;
                       
                    }
                }
            } 
        }
                
        $scope.dynamicValidations();
        $scope.updateErrorCount('mainInsuredSubTab');
        $scope.refresh();
    }
    
    $scope.calculateAge = function (dob, id) {
        var age = "";
        if (dob != "" && dob != undefined &&
            dob != null) {
            var dobDate = new Date(dob);
            /*
             * In I.E it will accept date format in '/'
             * separator only
             */
            if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
                dobDate = new Date(dob.split("-").join(
                    "/"));
            }
            var todayDate = new Date();
            if (formatForDateControl(dob) >= formatForDateControl(new Date())) {
                //$scope.Illustration.Insured.BasicDetails.age = 0;
                age = 0;
            } else {
                var curd = new Date(todayDate
                    .getFullYear(), todayDate
                    .getMonth(), todayDate
                    .getDate());
                var cald = new Date(dobDate
                    .getFullYear(), dobDate
                    .getMonth(), dobDate.getDate());
                var dife = $scope.ageCalculation(curd,
                    cald);

                if (age < 1) {
                    var datediff = curd.getTime() -
                        cald.getTime();
                    var days = (datediff / (24 * 60 * 60 * 1000)) / 365;
                    days = Number(Math.round(days +
                            'e3') +
                        'e-3');
                    age = 0;
                }
                if (dife[1] == "false") {
                    age = dife[0];
                } else {
                    age = dife[0] + 1;
                }

                if (dob == undefined || dob == "") {
                    age = "-";
                }
                //$scope.Illustration.Insured.BasicDetails.age = age;
            }
        }
        //else {
        //$scope.Illustration.Insured.BasicDetails.age = "";
        // }
        if (id == 'lifeMainAssuredDOB') {
            $scope.LifeEngageProduct.Insured.BasicDetails.age = age;
        } else if (id == 'payerDOB') {
            $scope.LifeEngageProduct.Payer.BasicDetails.age = age;
        }
         $scope.onFieldChange();
//        $scope.validateInsuredAge();
    };
    
    //Calculating age in years, month , days 
    $scope.ageCalculation = function (currDate, dobDate) {
        var date1 = new Date(currDate);
        var date2 = new Date(dobDate);
        var y1 = date1.getFullYear(),
            m1 = date1
            .getMonth(),
            d1 = date1.getDate(),
            y2 = date2
            .getFullYear(),
            m2 = date2.getMonth(),
            d2 = date2
            .getDate();

        var nextDOB;
        if (m1 < m2) {
            nextDOB = new Date(date2.setYear(date1.getFullYear()));
        } else if ((m1 == m2) && (d1 < d2)) {
            nextDOB = new Date(date2.setYear(date1.getFullYear()));
        } else {
            nextDOB = new Date(date2.setYear(date1.getFullYear() + 1));
        }
        var marginDate = new Date(date1.setMonth(date1.getMonth() + 6));

        if (m1 < m2) {
            y1--;
            m1 += 12;
        }

        if (nextDOB.getTime() > marginDate.getTime()) {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() - birthDate.getFullYear();
            var mnth = today.getMonth() - birthDate.getMonth();
            if (mnth < 0 || (mnth === 0 && today.getDate() < birthDate.getDate())) {
                ageVal--;
            }
            return [ageVal, "false"];
        } else {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() - birthDate.getFullYear();
            var mnth = today.getMonth() - birthDate.getMonth();
            if (mnth < 0 || (mnth === 0 && today.getDate() < birthDate.getDate())) {
                ageVal--;
            }
            return [ageVal, "true"];
        }

    }
    
    //Splice Widow from Marital status when Gender is male
    if($scope.LifeEngageProduct.Insured.BasicDetails.gender==='Male'){
        $scope.customMaritalStatus=angular.copy($scope.maritalStatus);        
        $scope.customMaritalStatus.splice(2,1);              
    }else{
        $scope.customMaritalStatus=angular.copy($scope.maritalStatus);   
    }
    
    //Splice Office Address for Copy From dropdown
    if($scope.copyFromLookup!==undefined){
        $scope.copyFromLookup.splice(1,1);
        
        $scope.refresh();
    }
    
    $scope.validateNationality=function(){    
        if($scope.LifeEngageProduct.Insured.BasicDetails.nationality!=="" && $scope.LifeEngageProduct.Insured.BasicDetails.nationality!==undefined){
            if($scope.LifeEngageProduct.Insured.BasicDetails.nationality!=='THA'){
                $scope.defaultIdentityTypeApp=angular.copy($scope.customIdentityTypeApp);
                var length=$scope.defaultIdentityTypeApp.length;
                for(var i=0;i<length;i++){
                    for(var j=0;j<$scope.defaultIdentityTypeApp.length;j++){
                        if($scope.defaultIdentityTypeApp[j].code!=='PP'){
                            $scope.defaultIdentityTypeApp.splice(j,1);
                        }
                    }
                }
                $scope.LifeEngageProduct.Insured.BasicDetails.identityProof='PP';
            }else{
                $scope.defaultIdentityTypeApp=$scope.customIdentityTypeApp;
            }
        }else{
            $scope.defaultIdentityTypeApp=$scope.customIdentityTypeApp;
        }
        if($scope.LifeEngageProduct.Insured.BasicDetails.nationality==='THA' || $scope.LifeEngageProduct.Insured.BasicDetails.nationality===undefined || $scope.LifeEngageProduct.Insured.BasicDetails.nationality===""){
            $scope.foreignerInd=false;
        }else{
            $scope.foreignerInd=true;
        }
//        $scope.dynamicValidations();
//        $scope.updateErrorCount('mainInsuredSubTab');
        $scope.refresh();
    }
    
    //Splice when insured age is less than 16
    if($scope.LifeEngageProduct.Insured.BasicDetails.age<16){
        $scope.customIdentityTypeApp=angular.copy($scope.identityTypeApp);        
        $scope.customIdentityTypeApp.splice(6,1);              
        $scope.customIdentityTypeApp.splice(5,1);   
        $scope.customIdentityTypeApp.splice(4,1);  
            
        $scope.validateNationality();
    }else{
        $scope.customIdentityTypeApp=angular.copy($scope.identityTypeApp);
        $scope.customIdentityTypeApp.splice(3,1);
            
        $scope.validateNationality();
    }
    
    if($scope.LifeEngageProduct.Insured.BasicDetails.nationality==='THA' || $scope.LifeEngageProduct.Insured.BasicDetails.nationality===undefined || $scope.LifeEngageProduct.Insured.BasicDetails.nationality===""){
            $scope.foreignerInd=false;
    }else{
        $scope.foreignerInd=true;
    }

    if($scope.LifeEngageProduct.Insured.BasicDetails.IDcard!==undefined && $scope.LifeEngageProduct.Insured.BasicDetails.IDcard!==""){
            if($scope.LifeEngageProduct.Insured.BasicDetails.nationality==='THA' || $scope.LifeEngageProduct.Insured.BasicDetails.nationality===undefined || $scope.LifeEngageProduct.Insured.BasicDetails.nationality===""){
                $scope.citizenIdIdentityProof=$scope.LifeEngageProduct.Insured.BasicDetails.IDcard;
            }else if($scope.LifeEngageProduct.Insured.BasicDetails.nationality!=='THA'){
                $scope.passportIdentityProof=$scope.LifeEngageProduct.Insured.BasicDetails.IDcard;
            }
        }
}