'use strict';
storeApp.controller('PaymentController', PaymentController);
PaymentController.$inject = ['$rootScope', '$scope', 'DataService', 'GLI_DataService', 'LookupService', 'DocumentService', 'ProductService', '$compile', '$routeParams', '$location', '$translate', '$debounce', 'UtilityService', 'PersistenceMapping', 'AutoSave', 'globalService', 'EappVariables', 'EappService', 'PaymentService', '$timeout', 'GLI_EappService', 'UserDetailsService'];

function PaymentController($rootScope, $scope, DataService, GLI_DataService, LookupService, DocumentService, ProductService, $compile, $routeParams, $location, $translate, $debounce, UtilityService, PersistenceMapping, AutoSave, globalService, EappVariables, EappService, PaymentService, $timeout, GLI_EappService, UserDetailsService) {
    $scope.paymentinfo;
    $scope.numberPattern = rootConfig.numberPattern;
    $scope.alphanumericPattern = rootConfig.alphanumericPattern;
    $scope.showModel = false;
    $scope.isSingleTopup = false;
    $scope.payerNames = [];
    $scope.popupMessage = "";
    $scope.supportingbanks = ["BCA", "BNI", "Niaga"];
    $scope.creditCardDetail = {};
    $scope.creditCardDetail.creditCardNumberone = "";
    $scope.creditCardDetail.creditCardNumbertwo = "";
    $scope.creditCardDetail.creditCardNumberthree = "";
    $scope.creditCardDetail.creditCardNumberfour = "";
    $scope.mandiriCreditCardNumber1 = "";
    $scope.mandiriCreditCardNumber2 = "";
    $scope.mandiriCreditCardNumber3 = "";
    $scope.mandiriCreditCardNumber4 = "";
    $scope.onlineMessage = "";
    $rootScope.selectedPage = "Payment";
    $scope.isDeviceMobile = rootConfig.isDeviceMobile;
    $scope.submitPayment = false;

    $scope.checkForBtnName = function () {
        return 'payNowOffline'
    }
    $scope.updatePaymentOptions = function () {
        $scope.paymentOptions = [];
        if ($scope.paymentinfo.paymentMethod == "OnlineOnline") {
            $scope.paymentOptions = $scope.onlinePaymentOptions;
        } else if ($scope.paymentinfo.paymentMethod == "Offline") {
            $scope.paymentOptions = $scope.offlinePaymentOptions;
        }
        $scope.paymentinfo.paymentType = "";
        $scope.updateErrorCount();
    }
    $scope.subTypeCheck = function () {
        for (i = 0; i <= $scope.paymentOptions.length; i++) {
            if ($scope.paymentinfo.paymentType == $scope.paymentOptions[i].code) {
                $scope.subPaymentType = $scope.paymentOptions[i].type;
                $scope.submitPayment = true;
                break;
            }
        }
    }
    $scope.onPaymentTypeChange = function () {
        $scope.subTypeCheck();
        if ($scope.paymentinfo.paymentType == "virtual_account") {
            $scope.paymentinfo.accountIdentifier = "00074-10-" + $scope.spajNo;
            $scope.paymentinfo.payerName = "PT. AJ. Generali Indonesia";
        } else if ($scope.paymentinfo.paymentType == "credit_card" && $scope.renewalPayemntType == "CreditCard") {
            $scope.paymentinfo.cardNumber = $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardNumber;
            if ($scope.LifeEngageProduct.Payment.RenewalPayment && $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName)
                $scope.paymentinfo.payerName = $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName.trim();
        } else if ($scope.paymentinfo.paymentType == "bank_auto_debit" && $scope.renewalPayemntType == "DebitAccount") {
            $scope.paymentinfo.bankName = $scope.LifeEngageProduct.Payment.RenewalPayment.bankName;
            $scope.paymentinfo.branchName = $scope.LifeEngageProduct.Payment.RenewalPayment.branchName;
            $scope.paymentinfo.payerName = $scope.LifeEngageProduct.Payment.RenewalPayment.accHolderName;
            $scope.paymentinfo.accountIdentifier = $scope.LifeEngageProduct.Payment.RenewalPayment.accountNumber;
        } else {
            $scope.paymentinfo.accountIdentifier = "";
            $scope.paymentinfo.cardNumber = "";
            $scope.paymentinfo.payerName = "";
        }
        $scope.paymentinfo.receiptNumber = "";
        $scope.paymentinfo.notes = "";
        $timeout(function () {
            $scope.updateErrorCount($scope.selectedTabId);
        }, 0);

    }
    /*$scope.updateMandiriCreditCardData = function () {
        if (typeof $scope.mandiriCreditCardNumber1 == 'undefined' || typeof $scope.mandiriCreditCardNumber2 == 'undefined' || typeof $scope.mandiriCreditCardNumber3 == 'undefined' || typeof $scope.mandiriCreditCardNumber4 == 'undefined') {

        } else {
            var creditCardStr = $scope.mandiriCreditCardNumber1 + $scope.mandiriCreditCardNumber2 + $scope.mandiriCreditCardNumber3 + $scope.mandiriCreditCardNumber4;
            if (creditCardStr) {
                $scope.paymentinfo.cardNumber = creditCardStr;
            }
        }
    };*/
    function updateCreditCardData() {
        if (typeof $scope.creditCardDetail.creditCardNumberone == 'undefined' || typeof $scope.creditCardDetail.creditCardNumbertwo == 'undefined' || typeof $scope.creditCardDetail.creditCardNumberthree == 'undefined' || typeof $scope.creditCardDetail.creditCardNumberfour == 'undefined') {

        } else {
            var creditCardStr = $scope.creditCardDetail.creditCardNumberone + $scope.creditCardDetail.creditCardNumbertwo + $scope.creditCardDetail.creditCardNumberthree + $scope.creditCardDetail.creditCardNumberfour;
            if (creditCardStr) {
                $scope.paymentinfo.cardNumber = creditCardStr;
            }
        }

    };

    $scope.updateErrorCount = function (id) {
        updateCreditCardData();
        var _errorCount = 0;
        var _errors = [];
        if ($scope.paymentinfo && $scope.paymentinfo.paymentType && $scope.paymentinfo.paymentType == "credit_card") {
            if (!$scope.paymentinfo.payerName || $scope.paymentinfo.payerName == "") {
                _errorCount++;
                var error = {};
                error.message = translateMessages($translate, "eapp.nameOfCreditcardHolderRequiredValidationMessage");
                error.key = 'ccpayerName';
                _errors.push(error);
            }
            if (!$scope.paymentinfo.cardNumber || $scope.paymentinfo.cardNumber == "" || $scope.paymentinfo.cardNumber.length != 16) {
                _errorCount++;
                var error = {};
                error.key = 'creditCardNumber_1';
                error.message = translateMessages($translate, "eapp.creditCardNumberRequiredValidationMessage");
                _errors.push(error);
            }

        }
        if ($scope.LifeEngageProduct.Product.ProductDetails.IllustrationStatus != "Confirmed") {
            _errorCount++;
            var error = {};
            error.message = translateMessages($translate, "illustrationNotSigned");
            error.key = 'BISign';
            _errors.push(error);
        }
        if ($scope.LifeEngageProduct.Declaration.confirmSignature != false || EappVariables.EappKeys.Key15 == "New" || EappVariables.EappKeys.Key15 == undefined || EappVariables.EappKeys.Key15 == "undefined" || EappVariables.EappKeys.Key15 == "") {
            _errorCount++;
            var error = {};
            error.message = translateMessages($translate, "declarationNotSigned");
            error.key = 'declarationSign';
            _errors.push(error);
        }
        if ($scope.LifeEngageProduct.Declaration.confirmAgentSignature != false) {
            _errorCount++;
            var error = {};
            error.message = translateMessages($translate, "ACRNotSigned");
            error.key = 'ACRSign';
            _errors.push(error);
        }
        $scope.dynamicErrorCount = _errorCount;
        if (!$scope.dynamicErrorMessages) {
            $scope.dynamicErrorMessages = [];
        }
        $scope.dynamicErrorMessages = _errors;

        $timeout(function () {
            $rootScope.updateErrorCount($scope.selectedTabId);
        }, 0);
        $scope.selectedTabId = id;
    }
    $scope.onChangeAmount = function () {
        alert($scope.amountCash);
    }

    function initPaymentDetails() {
        if ($scope.LifeEngageProduct.Payment && $scope.LifeEngageProduct.Payment.PaymentInfo) {
            $scope.paymentinfo = $scope.LifeEngageProduct.Payment.PaymentInfo
        } else {
            $scope.paymentinfo = EappVariables.EappModel.Payment.PaymentInfo;
        }
        if ($scope.totalPremium && $scope.LifeEngageProduct.Payment.PaymentInfo.status != "Payment done") {
            $scope.paymentinfo.cash = $scope.totalPremium;
        }
        $scope.paymentOptions = rootConfig.paymentDetail.PaymentMethods;
        $scope.proposalNo = EappVariables.EappKeys.Key21;
    }

    $scope.isEligible = function (option, product, amount, renewalType) {
        var _isEligible = false;
        if (option.supportingProducts.indexOf(product) != -1 && (!option.renewal || option.renewal.indexOf(renewalType) != -1) &&
            option.minAmount <= amount && (option.maxAmount >= amount || option.maxAmount == -1)) {
            _isEligible = true;
        }
        return _isEligible;
    }

    $scope.successCallback = function (data) {
        $scope.paymentinfo.transactionRefNumber = data.PaymentInfo.transactionId;
        $scope.LifeEngageProduct.Payment.PaymentInfo = $scope.paymentinfo;
        if (rootConfig.isDeviceMobile && rootConfig.isDeviceMobile != "false") {
            var payWindow = window.open(data.PaymentInfo.redirect_url, '_blank', 'location=yes');
            payWindow.addEventListener('exit', function (event) {
                $scope.onPayWindowClose();
            });
        } else {
            $scope.LifeEngageProduct.Payment.PaymentInfo = $scope.paymentinfo;
            EappService.saveTransaction(function () {
                $scope.onSaveSuccess(data.PaymentInfo.redirect_url);
            }, $scope.onConfirmError, false);
        }
    };
    /** Function to call redirection on desktop */
    $scope.onSaveSuccess = function (url) {
        window.location.href = url;
    };
    $scope.onSaveError = function () {};

    /* $scope.createChargeData = function () {
         var chargeData = {
             "TransactionData": {
                 "PaymentInfo": {
                     "policyholder_name": "",
                     "contract_no": 0,
                     "purpose_of_payment": "",
                     "amount": ""
                 }
             }
         };
         chargeData.TransactionData.PaymentInfo.policyholder_name = $scope.LifeEngageProduct.Proposer.BasicDetails.firstName + "" + $scope.LifeEngageProduct.Proposer.BasicDetails.lastName;
         chargeData.TransactionData.PaymentInfo.contract_no = EappVariables.EappKeys.Key21;
         chargeData.TransactionData.PaymentInfo.purpose_of_payment = "Insurance Premium";
         chargeData.TransactionData.PaymentInfo.amount = $scope.amount;

         return chargeData;

     }*/

    $scope.paymentSubmit = function () {
        if ($scope.paymentinfo.status == "Payment Done") {
            $scope.save();
        }
    }

    $scope.validationSuccessCallback = function (data) {
        var msg = translateMessages($translate, "eapp_vt.paymentSuccessMsg");
        msg = msg.replace("{{proposalno}}", $scope.proposalNo);
        $rootScope.showHideLoadingImage(false);
        if (rootConfig.isDeviceMobile) {
            if (data == 200) {
                var newDate = new Date();
                $scope.paymentinfo.date = UtilityService.getFormattedDate(newDate);
                $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), msg, translateMessages($translate, "fna.ok"), $scope.save);
            } else {
                if ($scope.paymentinfo.paymentType == "cards" || $scope.paymentinfo.paymentType == "pos" || $scope.paymentinfo.paymentType == "manual_bank_deposit") {
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.paymentErrorMsg1"), translateMessages($translate, "fna.ok"));
                } else {
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.paymentErrorMsg"), translateMessages($translate, "fna.ok"));
                }
            }
        } else {
            if (data == 200) {
                var newDate = new Date();
                $scope.paymentinfo.date = UtilityService.getFormattedDate(newDate);
                $scope.paymentinfo.status = "Payment done";
                EappVariables.EappKeys.Key15 = "PaymentDone";
                EappVariables.EappModel.Payment.PaymentInfo = $scope.paymentinfo;
                if (EappVariables.EappKeys.Key15 == "PaymentDone") {
                    $('#Payment button[id="btnpayNow"]').attr('disabled', true);
                }
                $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), msg, translateMessages($translate, "fna.ok"), $scope.save);
            } else {
                if ($scope.paymentinfo.paymentType == "pos" || $scope.paymentinfo.paymentType == "manual_bank_deposit") {
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.paymentErrorMsg1"), translateMessages($translate, "fna.ok"));
                } else if ($scope.paymentinfo.paymentType == "cards") {
                    var chargeDataWeb = [{
                        "key": "policyOwner",
                        "value": $scope.policyOwnerName
                    }, {
                        "key": "policyNumber",
                        "value": $scope.policyNumber
                    }, {
                        "key": "paymentDes",
                        "value": $scope.paymentDes
                    }, {
                        "key": "amount",
                        "value": $scope.amountOnline
                    }, {
                        "key": "fromfunction",
                        "value": "OMMI"
                    }];
                    var url = rootConfig.paymentOnlineURL;
                    var payWindow = window.open('payWeb.html', '_blank', 'location=yes');
                    payWindow.onload = function () {
                        payWindow.init(chargeDataWeb, url);
                    }
                } else {
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.paymentErrorMsg"), translateMessages($translate, "fna.ok"));
                }

                //onbeforeunload

                /*if($scope.paymentinfo.paymentType == "cards" || $scope.paymentinfo.paymentType == "pos" || $scope.paymentinfo.paymentType == "manual_bank_deposit"){
                		$scope.lePopupCtrl.showWarning(translateMessages($translate,"lifeEngage"),translateMessages($translate,"eapp_vt.paymentErrorMsg1"),translateMessages($translate,"fna.ok"));
                }
                else{
                	$scope.lePopupCtrl.showWarning(translateMessages($translate,"lifeEngage"),translateMessages($translate,"eapp_vt.paymentErrorMsg"),translateMessages($translate,"fna.ok"));
                }*/
            }
        }
    }

    function validateOnlinePayment() {
        PersistenceMapping.clearTransactionKeys();
        var transactionObj = PersistenceMapping.mapScopeToPersistence();
        transactionObj.ApplicationNo = EappVariables.EappKeys.Key21;
        transactionObj.PremiumAmount = $scope.totalPremium;
        transactionObj.PaymentOption = "Credit Card";
        transactionObj.Type = "PaymentValidation";
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);

        PaymentService.validateOnlinePayment(transactionObj, $scope.validationSuccessCallback, $scope.errorInPaymentCallback);
    }

    function validateRecieptNo() {
        PersistenceMapping.clearTransactionKeys();
        var transactionObj = PersistenceMapping.mapScopeToPersistence();
        transactionObj.ApplicationNo = EappVariables.EappKeys.Key21;
        transactionObj.Key1 = $scope.paymentinfo.receiptNumber;
        transactionObj.PremiumAmount = $scope.paymentinfo.cash;
        transactionObj.Key3 = transactionObj.Key11;
        transactionObj.Key11 = "";
        transactionObj.Key14 = "";
        transactionObj.Type = "PaymentValidation";
        switch ($scope.paymentinfo.paymentType) {
            case 'cash':
                transactionObj.PaymentOption = 'Cash';
                break;
            case 'manual_bank_deposit':
                transactionObj.PaymentOption = 'Manual bank deposit';
                break;
            case 'pos':
                transactionObj.PaymentOption = 'POS';
                break;
            default:
                break;
        }
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
        PaymentService.validateReciptNumber(transactionObj, $scope.validationSuccessCallback, $scope.errorInPaymentCallback);
    }

    $scope.paynowOffline = function () {
        if ($scope.LifeEngageProduct.Product.ProductDetails.productCode != "1") {
            if ($scope.paymentinfo.paymentType == "virtual_payment") {
                $scope.save();
            } else {
                if ($scope.errorCount > 0) {
                    $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "enterMandatoryIllustration"), translateMessages($translate, "fna.ok"));
                } else {
                    if ($scope.paymentinfo.cash !== "") {
                        if ($scope.paymentinfo.cash < $scope.totalPremium) {
                            var amountLessThanPremiumErrorMsg = translateMessages($translate, "eapp_vt.amountLessThanPremiumErrorMsg");
                            amountLessThanPremiumErrorMsg = amountLessThanPremiumErrorMsg.replace("{{amount}}", $scope.totalPremium);
                            $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), amountLessThanPremiumErrorMsg, translateMessages($translate, "fna.ok"));
                        } else {
                            validateRecieptNo();
                        }
                    }
                }
            }
        } else {
            $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "CIB1DiscontinuedMsg"), translateMessages($translate, "fna.ok"));
        }
    }
    $scope.onPayWindowClose = function (payWindow) {
        validateRecieptNo();
    };
    $scope.paynow = function () {
        if ($scope.LifeEngageProduct.Product.ProductDetails.productCode != "1") {
            if (!$scope.paymentinfo.paymentType && $scope.paymentinfo.paymentType == "") {
                return;
                //Show some popup
            }
            if ($scope.errorCount > 0) {
                $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "enterMandatory"), translateMessages($translate, "fna.ok"));
            } else {
                if ($scope.isUserOnline()) {
                    if ($scope.LifeEngageProduct.Proposer.BasicDetails.fullName) {
                        $scope.policyOwnerName = $scope.LifeEngageProduct.Proposer.BasicDetails.fullName;
                    } else {
                        $scope.policyOwnerName = $scope.LifeEngageProduct.Proposer.BasicDetails.firstName + " " + $scope.LifeEngageProduct.Proposer.BasicDetails.lastName;
                    }
                    $scope.policyNumber = EappVariables.EappKeys.Key21;
                    $scope.paymentDes = "opt1";
                    $scope.amountOnline = $scope.totalPremium;
                    var chargeData = [{
                        "key": "policyOwner",
                        "value": $scope.policyOwnerName
                    }, {
                        "key": "policyNumber",
                        "value": $scope.policyNumber
                    }, {
                        "key": "paymentDes",
                        "value": $scope.paymentDes
                    }, {
                        "key": "amount",
                        "value": $scope.amountOnline
                    }, {
                        "key": "fromfunction",
                        "value": "OMMI"
                    }];
                    if (rootConfig.isDeviceMobile) {
                        $scope.doPaymentOnDevice(chargeData);
                    } else {
                        validateOnlinePayment();
                    }
                }
            }
        } else {
            $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "CIB1DiscontinuedMsg"), translateMessages($translate, "fna.ok"));
        }
    };

    $scope.doPaymentOnDevice = function (chargeData) {
        var url = rootConfig.paymentOnlineURL;
        var index = 0;
        var payment = 'var form =document.createElement("form");form.setAttribute("method","post");form.setAttribute("id","paymentForm");form.setAttribute("action","' + url + '");';
        var filedString;
        for (var data in chargeData) {
            filedString = "";
            filedString += ' var hiddenField' + index + ' = document.createElement("input");';
            filedString += 'hiddenField' + index + '.setAttribute("name", "' + chargeData[data].key + '");';
            filedString += 'hiddenField' + index + '.setAttribute("value", "' + chargeData[data].value + '");';
            filedString += 'hiddenField' + index + '.setAttribute("type", "hidden");';

            filedString += 'form.appendChild(hiddenField' + index + ');';
            payment += filedString;
            index++;
        };
        payment += 'document.body.appendChild(form);document.querySelector("#paymentForm").submit();';
        var payWindow = window.open('pay.html', '_blank', 'location=yes');

        payWindow.addEventListener('loadstop', function (event) {
            payWindow.executeScript({
                code: payment
            });
            payment = "";
        });
        payWindow.addEventListener('loaderror', function (event) {});

        payWindow.addEventListener('exit', function (event) {
            $scope.statusSuccesspayment();
        });
    }
    $scope.doPaymentOnWeb = function (chargeData) {
        var url = rootConfig.paymentOnlineURL;
        var payWindow = window.open('pay.html', '_blank', 'location=yes');
        var form = payWindow.document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("id", "paymentForm");
        form.setAttribute("action", url);
        var index = 0;
        for (var data in chargeData) {

            var hiddenField = payWindow.document.createElement("input");
            hiddenField.setAttribute("name", chargeData[data].key);
            hiddenField.setAttribute("value", chargeData[data].value);
            hiddenField.setAttribute("type", "hidden");

            form.appendChild(hiddenField);
            index++;
        };
        payWindow.document.body.appendChild(form);
        payWindow.document.querySelector("#paymentForm").submit();

    }

    $scope.statusSuccesspayment = function () {
        validateOnlinePayment();
    }
    $scope.statusSuccessCallback = function (data) {
        var msg = "";
        $rootScope.showHideLoadingImage(false);
        if (data.PaymentInfo && data.PaymentInfo.paymentRequestStatus && data.PaymentInfo.paymentRequestStatus == 'COMPLETED') {
            msg = translateMessages($translate, "eapp.payemntSuccessMessage");
            $scope.paymentinfo.status = "COMPLETED";
            EappVariables.EappModel.Payment.PaymentInfo = $scope.paymentinfo;
            $rootScope.showHideLoadingImage(true);
            EappService.saveTransaction(function () {
                $rootScope.showHideLoadingImage(false);
            }, $scope.onConfirmError, false);


        } else {
            msg = translateMessages($translate, "eapp.payemntFailMessage");
        }
        msg = msg.replace("{{amount}}", $scope.amount);
        $scope.popupMessage = msg.replace("{{spajno}}", $scope.spajNo);
        $scope.showModel = true;

    };

    /*$scope.populatePayorList = function () {
        $scope.payerNames = [];
        var name = $scope.mergeName($scope.LifeEngageProduct.Proposer.BasicDetails.firstName, $scope.LifeEngageProduct.Proposer.BasicDetails.lastName);
        $scope.payerNames.push(name);
        if ($scope.LifeEngageProduct.Proposer.CustomerRelationship &&
            $scope.LifeEngageProduct.Proposer.CustomerRelationship.isPayorDifferentFromInsured &&
            $scope.LifeEngageProduct.Proposer.CustomerRelationship.isPayorDifferentFromInsured == "Yes") {
            var name = $scope.mergeName($scope.LifeEngageProduct.Insured.BasicDetails.firstName, $scope.LifeEngageProduct.Insured.BasicDetails.lastName);
            if ($scope.payerNames.indexOf(name) == -1) {
                $scope.payerNames.push(name);
            }

        }
        if ($scope.LifeEngageProduct.Beneficiaries) {
            for (var index = 0; index < $scope.LifeEngageProduct.Beneficiaries.length; index++) {
                var name = $scope.mergeName($scope.LifeEngageProduct.Beneficiaries[index].BasicDetails.firstName, $scope.LifeEngageProduct.Beneficiaries[index].BasicDetails.lastName);
                if ($scope.payerNames.indexOf(name) == -1) {
                    $scope.payerNames.push(name);
                }
            }
        }
        if ($scope.LifeEngageProduct.Payment.FinancialInfo.premiumPayer == "Other") {
            if ($scope.LifeEngageProduct.Proposer.AdditionalQuestioniare && typeof ($scope.LifeEngageProduct.Proposer.AdditionalQuestioniare.Questions[51].details) != "undefined") {
                var name = $scope.LifeEngageProduct.Proposer.AdditionalQuestioniare.Questions[51].details;
                if ($scope.payerNames.indexOf(name) == -1) {
                    $scope.payerNames.push(name);
                }
            }

        }

    };*/

    $scope.mergeName = function (fn, ln) {
        var name = fn;
        if (ln) {
            name = name + " " + ln;
        }
        return name.trim();
    }

    /*$scope.checkRenewalPayment = function () {
        $scope.renewalPayemntType = $scope.LifeEngageProduct.Payment.RenewalPayment.paymentType;
        if ($scope.renewalPayemntType == "CreditCard") {
            $scope.paymentinfo.cardNumber = $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardNumber;
            if ($scope.LifeEngageProduct.Payment.RenewalPayment && $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName)
                $scope.paymentinfo.payerName = $scope.LifeEngageProduct.Payment.RenewalPayment.creditcardName.trim();
        } else if ($scope.renewalPayemntType == "DebetAccount") {
            $scope.paymentinfo.bankName = $scope.LifeEngageProduct.Payment.RenewalPayment.bankName;
            $scope.paymentinfo.branchName = $scope.LifeEngageProduct.Payment.RenewalPayment.branchName;
            $scope.paymentinfo.payerName = $scope.LifeEngageProduct.Payment.RenewalPayment.accHolderName;
            $scope.paymentinfo.accountIdentifier = $scope.LifeEngageProduct.Payment.RenewalPayment.accountNumber;
        }
    }*/

    $scope.closeModel = function () {
        $scope.showModel = false;
    }

    $scope.isReadOnly = function () {
        return false;
    }
    $scope.save = function () {
        if (!$scope.paymentinfo.paymentType && $scope.paymentinfo.paymentType == "") {
            return;
            //Show some popup
        }

        $scope.paymentinfo.status = "Payment done";
        EappVariables.EappKeys.Key15 = "PaymentDone";
        EappVariables.EappModel.Payment.PaymentInfo = $scope.paymentinfo;
        EappService.saveTransaction($scope.processedToDocument, $scope.onConfirmError, false);
    }

    $scope.processedToDocument = function (data) {
        if ($scope.LifeEngageProduct.Payment.PaymentInfo.status == "Payment done") {
            $('#Payment button[id="btnpayNow"]').attr('disabled', true);
        }
        $rootScope.showHideLoadingImage(true, "Loading. Please Wait...", $translate);
        $scope.selectedTabId = "Payment";
        GLI_EappService.checkRequirementNull($scope.LifeEngageProduct.Requirements, $scope.selectedTabId, function (returnReq) {
            if (returnReq == 0) {
                $scope.runRuleToGetRequirement();
            } else {
                $scope.isRequirementRuleExecuted = false;
                $scope.LifeEngageProduct.LastVisitedUrl = "6,0,'DocumentUpload',false,'tabsinner18',''";
                $scope.saveProposal();
                $rootScope.validationBeforesave = function (value, successcallback) {
                    successcallback();
                };
            }
        });
    };

    $scope.errorCallback = function (data) {
        $rootScope.showHideLoadingImage(false);
    };

    $scope.errorInPaymentCallback = function (data) {
        $rootScope.showHideLoadingImage(false);
        $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.paymentInvalidErrorMsg"), translateMessages($translate, "fna.ok"));
    };

    $scope.isUserOnline = function () {
        var userOnline = true;
        if ((rootConfig.isDeviceMobile) && !(rootConfig.isOfflineDesktop)) {
            if (navigator.network.connection.type == Connection.NONE) {
                userOnline = false;
            }
        }
        if ((rootConfig.isOfflineDesktop)) {
            if (!navigator.onLine) {
                userOnline = false;
            }
        }
        return userOnline;
    }


    $scope.Initialize = function () {
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            //EappVariables.setEappModel($scope.LifeEngageProduct);
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex;
        }
        //Getting the illustration status
        if (rootConfig.isDeviceMobile) {
            GLI_DataService.getRelatedBIForEapp(EappVariables.EappKeys.Key3, function (IllustrationData) {
                $scope.LifeEngageProduct.Product.ProductDetails.IllustrationStatus = angular.copy(IllustrationData[0].Key15);
                $scope.updateErrorCount($scope.selectedTabId);
            });
        } else {
            var transactionObj = {};
            PersistenceMapping.clearTransactionKeys();
            transactionObj = PersistenceMapping.mapScopeToPersistence({});
            transactionObj.Key1 = EappVariables.EappKeys.Key1;
            transactionObj.Key2 = EappVariables.EappKeys.Key2;
            transactionObj.Key3 = EappVariables.EappKeys.Key24;
            transactionObj.Key4 = EappVariables.EappKeys.Key4;
            transactionObj.Key5 = EappVariables.EappKeys.Key5;
            transactionObj.Key7 = EappVariables.EappKeys.Key7;
            transactionObj.Key15 = EappVariables.EappKeys.Key15;
            transactionObj.Key21 = EappVariables.EappKeys.Key21;
            transactionObj.Key24 = EappVariables.EappKeys.Key24;
            transactionObj.Key26 = EappVariables.EappKeys.Key26;
            transactionObj.Type = "illustration";
            DataService.getListingDetail(transactionObj, function (IllustrationData) {
                $scope.LifeEngageProduct.Product.ProductDetails.IllustrationStatus = angular.copy(IllustrationData[0].Key15);
                $scope.updateErrorCount($scope.selectedTabId);
            }, function () {
                alert("Error");
            });
        }
        $scope.updateErrorCount($scope.selectedTabId);
        //For disabling and enabling the tabs.And also to know the last visited page.---starts
        //$rootScope.selectedPage = "ProposerDetails";
        $scope.LifeEngageProduct.LastVisitedUrl = "5,0,'Payment',false,'tabsinner16',''";
        $scope.eAppParentobj.nextPage = "6,0,'DocumentUpload',false,'tabsinner18',''";
        $scope.eAppParentobj.CurrentPage = "";
        if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "New" && EappVariables.EappKeys.Key15 != "Confirmed") {
            UtilityService.disableAllActionElements();
        } else {
            UtilityService.removeDisableElements('Payment');
        }
        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[0] <= $scope.LifeEngageProduct.LastVisitedUrl[0]) {
                $scope.LifeEngageProduct.LastVisitedIndex = "5,0";
            }
        }
        $scope.prodPremium = 0;
        $scope.riderPremium = 0;
        // calculating total premium
        if ($scope.LifeEngageProduct.Product.ProductDetails.initialPremium) {
            $scope.prodPremium = $scope.LifeEngageProduct.Product.ProductDetails.initialPremium;
        }
        if ($scope.LifeEngageProduct.Product.RiderDetails.length > 0) {
            for (i = 0; i < $scope.LifeEngageProduct.Product.RiderDetails.length; i++) {
                $scope.riderPremium = $scope.LifeEngageProduct.Product.RiderDetails[i].initialPremium;
            }
        }
        //$scope.totalPremium = $scope.prodPremium + $scope.riderPremium;
        $scope.totalPremium = $scope.LifeEngageProduct.Product.ProductDetails.totalInitialPremium;
        //For disabling and enabling the tabs.And also to know the last visited page.---ends

        $scope.eAppParentobj.PreviousPage = "";
        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        angular.element('#' + $scope.selectedTabId + "_a").trigger('click');
        initPaymentDetails();
        $timeout(function () {
            if ($scope.LifeEngageProduct.Payment.PaymentInfo.status == "Payment done") {
                $('#Payment input[type="radio"]').attr('disabled', true);
                $('#Payment input[type="text"]').attr('disabled', 'disabled');
                $('#Payment .iradio').attr('disabled', 'disabled');
            } else {
                $('#Payment input[type="radio"]').attr('disabled', false);
                $('#Payment input[type="text"]').removeAttr("disabled");
                $('#Payment .iradio').removeAttr("disabled");
            }
        }, 0);
        // formatting Date on Tab navigation 
        $rootScope.stringFormattedDate = function () {}
    }
    $scope.showErrorPopup = function (id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
    }

    //PreliminaryDataController Load Event and Load Event Handler
    $scope.$parent.$on('Payment', function (event, args) {
        $scope.selectedTabId = "Payment";
        $scope.Initialize();
        $rootScope.validationBeforesave = function (value, successcallback) {
            successcallback();
        };
    });

    $scope.runRuleToGetRequirement = function () {
        $rootScope.showHideLoadingImage(true, 'reqRuleProgress', $translate);
        var transactionObj = RequirementRule();
        //Proposer Details
        if ($scope.LifeEngageProduct.Proposer) {
            transactionObj.ProposerDetails.partyIdentifier = UtilityService.getUUID();
            EappVariables.EappModel.Proposer.BasicDetails.UUID = transactionObj.ProposerDetails.partyIdentifier;
            transactionObj.ProposerDetails.age = calculateAge($scope.LifeEngageProduct.Proposer.BasicDetails.dob);
            transactionObj.ProposerDetails.nationality = $scope.LifeEngageProduct.Proposer.BasicDetails.nationality;

            //Check whether Birth place or country selected as US
            transactionObj.ProposerDetails.countryofResidence = $scope.LifeEngageProduct.Proposer.BasicDetails.countryofResidence;
            transactionObj.ProposerDetails.countryinPermanentAddress = $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.country;
            transactionObj.ProposerDetails.countryinCurrentAddress = $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.country;

            //Proposer US Nationality Question
            for (var t = 0; t < rootConfig.addressValuesTobeVerify.length; t++) {
                if ($scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.houseNo.indexOf(rootConfig.addressValuesTobeVerify[t]) != -1 || $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.houseNo.indexOf(rootConfig.addressValuesTobeVerify[t]) != -1 || $scope.LifeEngageProduct.Proposer.ContactDetails.currentAddress.street.indexOf(rootConfig.addressValuesTobeVerify[t]) != -1 || $scope.LifeEngageProduct.Proposer.ContactDetails.permanentAddress.street.indexOf(rootConfig.addressValuesTobeVerify[t]) != -1) {
                    transactionObj.ProposerDetails.usaInAddressValue = "true";
                }
            }
            //Proposer Phone number validation
            var poMobNum = $scope.LifeEngageProduct.Proposer.ContactDetails.mobileNumber1.substring(0, 3);
            var poHomeNum = "";
            var poOfficeNum = "";
            if ($scope.LifeEngageProduct.Proposer.ContactDetails.homeNumber1) {
                poHomeNum = $scope.LifeEngageProduct.Proposer.ContactDetails.homeNumber1.substring(0, 3);
            }
            if ($scope.LifeEngageProduct.Proposer.ContactDetails.officeNumber && $scope.LifeEngageProduct.Proposer.ContactDetails.officeNumber.number) {
                poOfficeNum = $scope.LifeEngageProduct.Proposer.ContactDetails.officeNumber.number.substring(0, 3);
            }
            for (var u = 0; u < rootConfig.phoneNumberToBeVerify.length; u++) {
                if (poMobNum.indexOf(rootConfig.phoneNumberToBeVerify[u]) != -1 || poHomeNum.indexOf(rootConfig.phoneNumberToBeVerify[u]) != -1 || poOfficeNum.indexOf(rootConfig.phoneNumberToBeVerify[u]) != -1) {
                    transactionObj.ProposerDetails.usaInPhoneNumberValue = "true";
                }
            }
            //Proposer Questionnaire sections
            if ($scope.LifeEngageProduct.Proposer.Questions.length > 0) {
                transactionObj.ProposerDetails.QuestionnaireQ19aAns = $scope.LifeEngageProduct.Proposer.Questions[0].option;
                if ($scope.LifeEngageProduct.Proposer.Questions[1]) {
                    transactionObj.ProposerDetails.QuestionnaireQ19bAns = $scope.LifeEngageProduct.Proposer.Questions[1].option;
                }
            } else {
                transactionObj.ProposerDetails.QuestionnaireQ19aAns = "";
                transactionObj.ProposerDetails.QuestionnaireQ19bAns = "";
            }
        }
        //Insured Details
        if ($scope.LifeEngageProduct.Insured) {
            if ($scope.LifeEngageProduct.Insured.CustomerRelationship.relationshipWithProposer != "Self") {
                transactionObj.InsuredDetails.partyIdentifier = UtilityService.getUUID();
                EappVariables.EappModel.Insured.BasicDetails.UUID = transactionObj.InsuredDetails.partyIdentifier;
                transactionObj.InsuredDetails.age = calculateAge($scope.LifeEngageProduct.Insured.BasicDetails.dob);
                transactionObj.InsuredDetails.nationality = $scope.LifeEngageProduct.Insured.BasicDetails.nationality;
            } else {
                transactionObj.InsuredDetails.partyIdentifier = transactionObj.ProposerDetails.partyIdentifier;
                EappVariables.EappModel.Insured.BasicDetails.UUID = transactionObj.InsuredDetails.partyIdentifier;
                transactionObj.InsuredDetails.age = calculateAge($scope.LifeEngageProduct.Proposer.BasicDetails.dob);
                transactionObj.InsuredDetails.nationality = $scope.LifeEngageProduct.Proposer.BasicDetails.nationality;
            }
            //Questionnaire sections
            if ($scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions.length > 0) {
                transactionObj.InsuredDetails.QuestionnaireQ10Ans = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[5].option;
                transactionObj.InsuredDetails.QuestionnaireQ14Ans = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[9].option;
                transactionObj.InsuredDetails.QuestionnaireQ15aAns = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[10].option;
                transactionObj.InsuredDetails.QuestionnaireQ15bAns = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[11].option;
                transactionObj.InsuredDetails.QuestionnaireQ15cAns = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[12].option;
                transactionObj.InsuredDetails.QuestionnaireQ15dAns = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].option;

                if ($scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[0] && $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[0].details) {
                    transactionObj.InsuredDetails.QuestionnaireQ15dOpt1Ans = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[0].details;
                } else {
                    transactionObj.InsuredDetails.QuestionnaireQ15dOpt1Ans = "";
                }
                if ($scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[1] && $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[1].details) {
                    transactionObj.InsuredDetails.QuestionnaireQ15dOpt2Ans = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[1].details;
                } else {
                    transactionObj.InsuredDetails.QuestionnaireQ15dOpt2Ans = "";
                }
                if ($scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[2] && $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[2].details) {
                    transactionObj.InsuredDetails.QuestionnaireQ15dOpt3Ans = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[2].details;
                } else {
                    transactionObj.InsuredDetails.QuestionnaireQ15dOpt3Ans = "";
                }
                if ($scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[3] && $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[3].details) {
                    transactionObj.InsuredDetails.QuestionnaireQ15dOpt4Ans = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[3].details;
                } else {
                    transactionObj.InsuredDetails.QuestionnaireQ15dOpt4Ans = "";
                }
                if ($scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[4] && $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[4].details) {
                    transactionObj.InsuredDetails.QuestionnaireQ15dOpt5Ans = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[4].details;
                } else {
                    transactionObj.InsuredDetails.QuestionnaireQ15dOpt5Ans = "";
                }
                if ($scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[5] && $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[5].details) {
                    transactionObj.InsuredDetails.QuestionnaireQ15dOpt6Ans = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[13].options[5].details;
                } else {
                    transactionObj.InsuredDetails.QuestionnaireQ15dOpt6Ans = "";
                }

                transactionObj.InsuredDetails.QuestionnaireQ15eAns = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[14].option;
                transactionObj.InsuredDetails.QuestionnaireQ15fAns = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[15].option;

                if ($scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[15].options[0] && $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[15].options[0].details) {
                    transactionObj.InsuredDetails.QuestionnaireQ15fOpt1Ans = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[15].options[0].details;
                } else {
                    transactionObj.InsuredDetails.QuestionnaireQ15fOpt1Ans = "";
                }
                if ($scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[15].options[1] && $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[15].options[1].details) {
                    transactionObj.InsuredDetails.QuestionnaireQ15fOpt2Ans = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[15].options[1].details;
                } else {
                    transactionObj.InsuredDetails.QuestionnaireQ15fOpt2Ans = "";
                }
                if ($scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[15].options[2] && $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[15].options[2].details) {
                    transactionObj.InsuredDetails.QuestionnaireQ15fOpt3Ans = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[15].options[2].details;
                } else {
                    transactionObj.InsuredDetails.QuestionnaireQ15fOpt3Ans = "";
                }
                if ($scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[15].options[3] && $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[15].options[3].details) {
                    transactionObj.InsuredDetails.QuestionnaireQ15fOpt4Ans = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[15].options[3].details;
                } else {
                    transactionObj.InsuredDetails.QuestionnaireQ15fOpt4Ans = "";
                }
                if ($scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[15].options[4] && $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[15].options[4].details) {
                    transactionObj.InsuredDetails.QuestionnaireQ15fOpt5Ans = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[15].options[4].details;
                } else {
                    transactionObj.InsuredDetails.QuestionnaireQ15fOpt5Ans = "";
                }

                transactionObj.InsuredDetails.QuestionnaireQ15gAns = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[16].option;
                transactionObj.InsuredDetails.QuestionnaireQ15hAns = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[17].option;
                transactionObj.InsuredDetails.QuestionnaireQ15iAns = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[18].option;
                transactionObj.InsuredDetails.QuestionnaireQ15jAns = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[19].option;
                transactionObj.InsuredDetails.QuestionnaireQ15kAns = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[20].option;
                transactionObj.InsuredDetails.QuestionnaireQ15lAns = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[21].option;
                transactionObj.InsuredDetails.QuestionnaireQ16aAns = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[22].option;
                transactionObj.InsuredDetails.QuestionnaireQ16bAns = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[23].option;
                transactionObj.InsuredDetails.QuestionnaireQ17aAns = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[24].option;
                transactionObj.InsuredDetails.QuestionnaireQ17bAns = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[25].option;
                transactionObj.InsuredDetails.QuestionnaireQ18Ans = $scope.LifeEngageProduct.Insured.Questionnaire.HealthDetails.Questions[26].option;
            }

            //Sum Assured calculation
            var LA1sumInsured = parseFloat($scope.LifeEngageProduct.Product.policyDetails.sumInsured);
            for (var y = 0; y < $scope.LifeEngageProduct.Product.RiderDetails.length; y++) {
                //check whether rider selected by Main Insured
                if ($scope.LifeEngageProduct.Product.RiderDetails[y].insuredType == "LA1") {
                    LA1sumInsured = LA1sumInsured + parseFloat($scope.LifeEngageProduct.Product.RiderDetails[y].sumInsured);
                }
            }
            transactionObj.InsuredDetails.sumAssured = LA1sumInsured;
        }

        //Need to remove the below AdditionalInsured section once we release the FGLI Document rule
        var incrementAddInsuredVal = 1;
        for (var i = 0; i < 4; i++) {
            var additionalInsured = {};
            additionalInsured.partyIdentifier = "";
            transactionObj["AdditionalInsured" + incrementAddInsuredVal] = additionalInsured;
            incrementAddInsuredVal++;
        }

        //New Business Payment section
        transactionObj.NewBusinessPayment.paymentType = $scope.LifeEngageProduct.Payment.PaymentInfo.paymentType;

        //Declaration Details
        transactionObj.Declaration.submittedBy = $scope.LifeEngageProduct.Declaration.submittedBy;
        transactionObj.Declaration.isMainInsuredKeyPerson = "No";

        transactionObj.Requirements = [];
        EappService.runRequirementRule(transactionObj, function (data) {
            $scope.isRequirementRuleExecuted = false;
            $scope.LifeEngageProduct.LastVisitedUrl = "6,0,'DocumentUpload',false,'tabsinner18',''";
            $scope.saveProposal();
            $rootScope.validationBeforesave = function (value, successcallback) {
                successcallback();                
                 $rootScope.loadDocumentSection(); 
            };
        }, $scope.requirementRuleError);     
    };

    $scope.requirementRuleError = function () {
        $rootScope.showHideLoadingImage(false);
        EappService.requirementRuleError();
    };
    $rootScope.validationBeforesave = function (value, successcallback) {
        successcallback();
    };

    //calculating age for Additional Insured
    function calculateAge(dob) {
        if (dob !== "" && dob != undefined && dob !== null && dob !== "Invalid Date") {
            var dobDate = new Date(dob);
            if ((dobDate === "NaN" || dobDate === "Invalid Date")) {
                dobDate = new Date(dob.split("-").join("/"));
            }
            var todayDate = new Date();
            var yyyy = todayDate.getFullYear().toString();
            var mm = (todayDate.getMonth() + 1).toString();
            var dd = todayDate.getDate().toString();
            var formatedDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);

            if (dobDate > todayDate) {
                return;
            } else {
                var age = todayDate.getFullYear() - dobDate.getFullYear();
                if (todayDate.getMonth() < dobDate.getMonth() - 1 || (dobDate.getMonth() - 1 === todayDate.getMonth() && todayDate.getMonth() < dobDate.getDate())) {
                    age--;
                }
                if (dob === undefined || dob === "") {
                    age = "-";
                }
                return age;
            }
        }
    };

} //