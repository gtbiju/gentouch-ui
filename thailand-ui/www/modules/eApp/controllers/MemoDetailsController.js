/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:MemoDetailsController
 CreatedDate:12/3/2018 - 12th March 2018
 Description:Eapp Memo details controller
 */
storeApp.controller('MemoDetailsController',[
	'$rootScope','$scope','$compile','$routeParams','UtilityService','PersistenceMapping','DataService','$translate','dateFilter','$debounce','AutoSave','$location','GLI_EappVariables','UserDetailsService','GLI_DataService','GLI_EappService',
function($rootScope, $scope, $compile, $routeParams,UtilityService, PersistenceMapping,DataService, $translate, dateFilter,$debounce, AutoSave,$location,GLI_EappVariables,UserDetailsService,GLI_DataService,GLI_EappService) {
	$scope.selectedpage = "MemoDetailsPage";
	//Function to navigate to e-App doc upload screen/page
	$scope.proceedToDocUpload = function(){
		$rootScope.fromMemo=true;
		if(GLI_EappVariables.EappKeys.Key1=="")
			GLI_EappVariables.EappKeys.Key1="0";
		$location.path('/Eapp/'+GLI_EappVariables.EappKeys.Key5+"/"+GLI_EappVariables.EappKeys.Key4+"/"+GLI_EappVariables.EappKeys.Key3+"/"+GLI_EappVariables.EappKeys.Id+"/"+GLI_EappVariables.EappKeys.Key1+"/"+GLI_EappVariables.EappKeys.Key2+"/"+GLI_EappVariables.EappKeys.Key24);
		$scope.refresh();
	};
	//Function to navigate to e-App listing screen/page
	$scope.proceedtoEappListing = function(){
		$location.path("/MyAccount/eApp/0/0");
		$scope.refresh();
	};
	//Function to print/download pdf to print 
	$scope.proceedToPrint = function(){
		if (rootConfig.isDeviceMobile) {
			if (checkConnection()) {
				$rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
				PersistenceMapping.clearTransactionKeys();
				var transactionObj = PersistenceMapping.mapScopeToPersistence();
				transactionObj.Type = "Memo";
				transactionObj.Key21 = $scope.applicationNumber;
				transactionObj.TransactionData = {};
				transactionObj.TransactionData.MemoDetails=GLI_EappVariables.getEappModel().MemoDetails;
				DataService.memoDownloadPDF(transactionObj,"-", function() {      
					if($('body').hasClass('ipad')){
						$rootScope.showHideLoadingImage(false);
						$scope.refresh();
					}else{
						$rootScope.showHideLoadingImage(false);
						$rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"),translateMessages($translate,"pdfDownloadTransactionsSuccess"),translateMessages($translate,"fna.ok"));
						$scope.refresh();
					}
				},function(data,status){
					$rootScope.showHideLoadingImage(false);
					$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"), translateMessages($translate,"pdfDownloadTransactionsError"), translateMessages($translate,"fna.ok"));
				});
			} else {
				$rootScope.showHideLoadingImage(false);
				$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"), translateMessages($translate,"eapp.emailGoOnlineMessage"), translateMessages($translate,"fna.ok"));
			}
		} else {
			$rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
			PersistenceMapping.clearTransactionKeys();
			var transactionObj = PersistenceMapping.mapScopeToPersistence();
			transactionObj.Type = "Memo";
			transactionObj.Key21 = $scope.applicationNumber;
			DataService.downloadPDF(transactionObj, "-", function () {
				$rootScope.showHideLoadingImage(false);
				$scope.refresh();
			}, function (error) {
				$rootScope.showHideLoadingImage(false);
				$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"), translateMessages($translate,"pdfDownloadTransactionsError"), translateMessages($translate,"fna.ok"));
			});
		}
	};
	//Function to check the validity of email ids
	$scope.checkValidityOfEmail = function(successCallBack,errorCallBack){
		var status = true;
		var emailPattern = new RegExp(rootConfig.emailPattern);
		if($scope.emailToId==""){
			$scope.emailToError = translateMessages($translate, "illustrator.validEmailValidationMessage");
			status = false;
		}else{
			var tomailIds = $scope.emailToId.split(';');		
			for (var i = 0; i < tomailIds.length; i++) {		 
				if(!emailPattern.test(tomailIds[i])){	
					$scope.emailToError =  translateMessages($translate, "illustrator.validEmailValidationMessage");
					status = false;
					break;
				}
			}      			
		}
		/* if($scope.emailCcId==""){
			$scope.emailCcError = translateMessages($translate, "illustrator.validEmailValidationMessage");
			status = false;
		}else {*/
		if($scope.emailCcId!=""){
			var CcmailIds = $scope.emailCcId.split(';');		
			for (var i = 0; i < CcmailIds.length; i++) {		 
				if(!emailPattern.test(CcmailIds[i])){	
					$scope.emailCcError =  translateMessages($translate, "illustrator.validEmailValidationMessage");
					status = false;
					break;
				}
			}      			
		}
		if(status)
			successCallBack(status);
		else
			errorCallBack(status);
	};
	//Function to send email when clicking send button in email popup
	$scope.emailSendClick = function(){
		$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
		$scope.emailToError = "";
		$scope.emailCcError = "";
		$scope.checkValidityOfEmail(function(data){
			if (rootConfig.isDeviceMobile) {
				if (checkConnection()) {
					emailSendClickProceed();
				}else{
					$rootScope.showHideLoadingImage(false);
					$scope.cancelEmailPopup();
					$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"), translateMessages($translate,"eapp.emailGoOnlineMessage"), translateMessages($translate,"fna.ok"));
				}
			}else{
				emailSendClickProceed();
			}
		},function(data){
			$rootScope.showHideLoadingImage(false);
		});
	};
	//Function to proceed email button click
	function emailSendClickProceed (){
		var emailObj = {};				
		emailObj.toMailIds = $scope.emailToId;
		emailObj.ccMailIds = $scope.emailCcId; 
		emailObj.mailSubject = $scope.emailSubject;	
		emailObj.language = "th";	
		emailObj.leadName = GLI_EappVariables.leadName;	
		PersistenceMapping.clearTransactionKeys();
		var transactionObjForMailReq = PersistenceMapping.mapScopeToPersistence();
		transactionObjForMailReq.Key18="true";
		transactionObjForMailReq.Type = "Memo";
		transactionObjForMailReq.Key21 = $scope.applicationNumber;
		transactionObjForMailReq.Key4 = GLI_EappVariables.EappKeys.Key4;
		transactionObjForMailReq.TransactionData = {};
		transactionObjForMailReq.TransactionData.Email=emailObj;
		transactionObjForMailReq.TransactionData.MemoDetails=GLI_EappVariables.getEappModel().MemoDetails;
		var userDetails = UserDetailsService.getUserDetailsModel();
		var transactionObjForMailReqArray=[];
		transactionObjForMailReqArray.push(transactionObjForMailReq);
		GLI_DataService.memoEmailCall(transactionObjForMailReqArray,function(data) {
			//Trigger mail call
			PersistenceMapping.clearTransactionKeys();
			var transactionObjForMailReq = PersistenceMapping.mapScopeToPersistence({});
			transactionObjForMailReq.Type = "Memo";
			var userDetails = UserDetailsService.getUserDetailsModel();
			UtilityService.emailServiceCall(userDetails,transactionObjForMailReq);
			
			$rootScope.showHideLoadingImage(false);
			$scope.cancelEmailPopup();
			$rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "illustrator.emailOkButtonMessage"), translateMessages($translate,"fna.ok"));
			$scope.refresh();
		},function(data){
			$rootScope.showHideLoadingImage(false);
			$scope.cancelEmailPopup();
			$rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"),translateMessages($translate, "illustrator.emailSendingErrorMessage") , translateMessages($translate,"fna.ok"));
			$scope.refresh();
		});
	};
	//Function to close/cancel the email popup
	$scope.cancelEmailPopup = function(){
		$scope.emailpopup = false;
		$scope.emailToId = "";
		$scope.emailCcId = "";
		$scope.emailSubject = "";
		$scope.emailToError = "";
		$scope.emailCcError = "";
	};
	//Function to show the email popup
	$scope.showEmailPopup = function() {
		$scope.emailpopup = true;
		$scope.refresh();
	};
	//Function executed on clicking the email button in memo listing page
	$scope.proceedToMail = function(){
		if (rootConfig.isDeviceMobile) {
			if (checkConnection()) {
				$scope.userDetails = UserDetailsService.getUserDetailsModel();
				$scope.emailToId = GLI_EappVariables.getEappModel().Insured.ContactDetails.emailId; //insured mailId
				$scope.emailCcId = $scope.userDetails.emailId; // agent mialId
				$scope.emailSubject = translateMessages($translate, "eapp.emailSubjectMemo")+$scope.insuredName+" "+$scope.applicationNumber;
				$scope.showEmailPopup();
			}else{
				$scope.emailpopup = false; 
				$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"), translateMessages($translate,"eapp.emailGoOnlineMessage"), translateMessages($translate,"fna.ok"));
			}
		}else{
			$scope.userDetails = UserDetailsService.getUserDetailsModel();
			$scope.emailToId = GLI_EappVariables.getEappModel().Insured.ContactDetails.emailId; //insured mailId
			$scope.emailCcId = $scope.userDetails.emailId; // agent mialId
			$scope.emailSubject = translateMessages($translate, "eapp.emailSubjectMemo")+$scope.insuredName+" "+$scope.applicationNumber;
			$scope.showEmailPopup();
		}
	};
	$scope.initialLoad = function(){
		$scope.appDateFormat = rootConfig.appDateFormat;
		$scope.applicationNumber=GLI_EappVariables.EappKeys.Key21;
		$scope.insuredName=GLI_EappVariables.leadName;
		$scope.memoList=GLI_EappVariables.getEappModel().MemoDetails;
		$rootScope.showHideLoadingImage(false);
	};
	$scope.$on('$viewContentLoaded', function(){
		$scope.initialLoad();
	});							
}]);

//Function to send email when clicking send button in email popup
//Memo mail same as SI logic		
	/*$scope.emailSendClick = function(){
		$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
		$scope.emailToError = "";
		$scope.emailCcError = "";
		$scope.checkValidityOfEmail(function(data){
			$scope.cancelEmailPopup();
			if(checkConnection()) {
				PersistenceMapping.clearTransactionKeys();
				PersistenceMapping.Key4 = GLI_EappVariables.EappKeys.Key4;
				PersistenceMapping.Key24 = GLI_EappVariables.EappKeys.Key24;
				PersistenceMapping.Id = GLI_EappVariables.EappKeys.Id;
				PersistenceMapping.TransTrackingID = GLI_EappVariables.EappKeys.TransTrackingID;
				PersistenceMapping.Type = "eApp";
				var transactionObj = PersistenceMapping.mapScopeToPersistence();
				DataService.getListingDetail(transactionObj, function(EappData){
					var emailObj = {};				
					emailObj.toMailIds = $scope.emailToId;
					emailObj.ccMailIds = $scope.emailCcId; 
					emailObj.mailSubject = $scope.emailSubject;	
					GLI_EappService.setEmailEappflagAndSaveTransactions(emailObj,EappData,function(data){
						$rootScope.showHideLoadingImage(false);
						$rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "illustrator.emailOkButtonMessage"), translateMessages($translate,"fna.ok"));
						$scope.refresh();
					},function(data){
						$rootScope.showHideLoadingImage(false);
						//show error
					});
				}, function(){
					$rootScope.showHideLoadingImage(false);
					//show error
				});
			}else{
				$rootScope.showHideLoadingImage(false);
				$scope.cancelEmailPopup();
				$rootScope.lePopupCtrl.showError(translateMessages($translate,"lifeEngage"), translateMessages($translate,"eapp.emailGoOnlineMessage"), translateMessages($translate,"fna.ok"));
			}
		},function(data){
			$rootScope.showHideLoadingImage(false);
		});
	};*/
	
	//Function to check if user is online or not
/* 	$scope.isUserOnline = function(){
		var userOnline = true;
		if ((rootConfig.isDeviceMobile)&& !(rootConfig.isOfflineDesktop)){
			if (navigator.network.connection.type == Connection.NONE) {
				userOnline = false;
			}
		}
		if ((rootConfig.isOfflineDesktop)) {
			if (!navigator.onLine) {
				userOnline = false;
			}
		}
		return userOnline;
	}; */