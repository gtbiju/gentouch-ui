'use strict';
/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:GLI_BeneficiaryController
 CreatedDate:11/08/2016
 Description:GLI_BeneficiaryController 
 */
storeApp.controller('GLI_BeneficiaryController', GLI_BeneficiaryController);
GLI_BeneficiaryController.$inject = ['$rootScope', '$scope', 'DataService', 'LookupService', 'DocumentService', 'ProductService', '$compile', '$routeParams', '$location', '$translate', '$debounce', 'UtilityService', 'PersistenceMapping', 'AutoSave', 'globalService', 'EappVariables', 'EappService', 'GLI_EappVariables', 'GLI_EappService', 'GLI_RuleService', '$timeout', '$controller'];

function GLI_BeneficiaryController($rootScope, $scope, DataService, LookupService, DocumentService, ProductService, $compile, $routeParams, $location, $translate, $debounce, UtilityService, PersistenceMapping, AutoSave, globalService, EappVariables, EappService, GLI_EappVariables, GLI_EappService, GLI_RuleService, $timeout, $controller) {
    $controller('BeneficiaryController', {
        $rootScope: $rootScope,
        $scope: $scope,
        DataService: DataService,
        LookupService: LookupService,
        DocumentService: DocumentService,
        ProductService: ProductService,
        $compile: $compile,
        $routeParams: $routeParams,
        $location: $location,
        $translate: $translate,
        $debounce: $debounce,
        UtilityService: UtilityService,
        PersistenceMapping: PersistenceMapping,
        AutoSave: AutoSave,
        globalService: globalService,
        EappVariables: EappVariables,
        EappService: EappService,
        $timeout: $timeout
    });
    $scope.LifeEngageProduct.Insured.ContactDetails.currentAddress.ApplicationNo = 0;
    $scope.alphabetWithSpacePattern = rootConfig.alphabetWithSpacePattern;
    $scope.alphabetWithSpacePatternIllustration=rootConfig.alphabetWithSpacePatternIllustration;
    $scope.alphanumericPattern=rootConfig.alphanumericPattern;
    $scope.alphanumericPatterng=rootConfig.alphanumericPatterng;
    $scope.passportEappPattern=rootConfig.passportEappPattern;
    $scope.diablePrepopulatedData = false;
    $scope.copyFromDataFlag = false;
    $scope.disableSave = false;
    $scope.disableButtons = false;
    $scope.identityNoPattern = rootConfig.eappSpecialCharacters;
    $scope.eAppBeneficiaryCount = rootConfig.eAppBeneficiaryCount;
    //populating the relationship with policyholder drop down based on gender
    $scope.onGenderChangeInsured = function (gender) {
        var dataOfRelationShipWithPH = EappService.getRelationShipWithPolicyHolder();
        if (typeof gender != "undefined" || gender != "") {
            var genderOfBeneficiary = gender;
        } else {
            var genderOfBeneficiary = "Male";
        }
        // if ($scope.LifeEngageProduct.Proposer.CustomerRelationship.isPayorDifferentFromInsured == 'Yes') {
        //     var genderInsured = $scope.LifeEngageProduct.Insured.BasicDetails.gender;
        //     var statusOfInsured = $scope.LifeEngageProduct.Insured.BasicDetails.maritalStatus;
        // } else {
        //     var genderInsured = $scope.LifeEngageProduct.Proposer.BasicDetails.gender;
        //     var statusOfInsured = $scope.LifeEngageProduct.Proposer.BasicDetails.maritalStatus;
        // }
        $scope.relationshipWithPH;
        for (key in dataOfRelationShipWithPH[0]) {
            if (key == genderOfBeneficiary) {
                $scope.relationshipWithPH = dataOfRelationShipWithPH[0][key];
                break;
            }
        }
        // if (statusOfInsured == "Widow/Widower") {
        //     $scope.relationshipWithPH.splice(0, 1);
        // } else if (genderOfBeneficiary == "Male" && genderInsured == "Male") {
        //     $scope.relationshipWithPH.splice(0, 1);
        // } else if (genderOfBeneficiary == "Female" && genderInsured == "Female") {
        //     $scope.relationshipWithPH.splice(0, 1);
        // }

    };
    //function to fetch the drop down for data from
    $scope.fetchDDForCopyDataFrom = function (index) {
        $scope.copyDataFromDD = EappService.getCopyDataFrom();
        for (var i = 2; i < $scope.copyDataFromDD.length; i++) {
            $scope.copyDataFromDD.splice(i, 1);
            i--;
            $scope.showDD = true;
        }
        if (!$scope.edit) {
            if ($scope.LifeEngageProduct.Beneficiaries.length > 0) {
                for (var i = 0; i < $scope.LifeEngageProduct.Beneficiaries.length; i++) {
                    if ($scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.isSameAsPolicyHolder) {
                        for (var j = 0; j < $scope.copyDataFromDD.length; j++) {
                            if ($scope.copyDataFromDD[j].code == $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.isSameAsPolicyHolder) {
                                var position = j;
                                break;
                            }
                        }
                        $scope.copyDataFromDD.splice(position, 1);
                    }
                }
            }
        }
    }
    $scope.getDDForBeneficiary = function (ddValues, index, successcallback) {
        if ($scope.LifeEngageProduct.Beneficiaries.length > 0) {
            for (i = 0; i < $scope.LifeEngageProduct.Beneficiaries.length; i++) {
                if (typeof $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship != "undefined" &&
                    typeof $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.isSameAsPolicyHolder != "undefined" && i != index) {
                    for (var j = 0; j < ddValues.length; j++) {
                        if ($scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.isSameAsPolicyHolder == ddValues[j].code) {
                            ddValues.splice(j, 1);
                            j--;
                        }
                    }
                }
                if (i == $scope.LifeEngageProduct.Beneficiaries.length - 1) {
                    successcallback();
                }
            }
        } else {
            successcallback();
        }
    }
    $scope.copyDataFromPartyToBeneficiary = function (party) {
        switch (party) {
            case "Insured":
                $scope.copyfunction("Insured", $scope.LifeEngageProduct.Insured);
                break;
            case "PolicyHolder":
                $scope.copyfunction("PolicyHolder", $scope.LifeEngageProduct.Proposer);
                break;
            default:
                clearBeneficiary();
                break;    
        }
    }
    
    function clearBeneficiary () {
        $scope.Beneficiary.BasicDetails = {};
        $scope.diablePrepopulatedData = false;
    }
    $scope.copyfunction = function (party, model) {
        $scope.Beneficiary.BasicDetails = {};
        $scope.Beneficiary.Questions[0].option = "";
        $scope.Beneficiary.Questions[1].option = "";
        $scope.Beneficiary.BasicDetails.firstName = model.BasicDetails.firstName;
        $scope.Beneficiary.BasicDetails.lastName = model.BasicDetails.lastName;
        $scope.Beneficiary.BasicDetails.gender = model.BasicDetails.gender;
        $scope.onGenderChangeInsured($scope.Beneficiary.BasicDetails.gender);
        $scope.Beneficiary.BasicDetails.dob = model.BasicDetails.dob;
        if (model.BasicDetails.identityProof) {
            $scope.Beneficiary.BasicDetails.identityProof = model.BasicDetails.identityProof;
        }
        if (model.BasicDetails.nationalIDType) {
            $scope.Beneficiary.BasicDetails.nationalIDType = model.BasicDetails.nationalIDType;
        }
        if (party == "Insured") {
            //$scope.Beneficiary.CustomerRelationship.relationWithInsured = model.CustomerRelationship.relationshipWithProposer;
        } else if (party == "PolicyHolder") {
            //$scope.Beneficiary.Questions[0].option=model.Questions[0].option;
            // $scope.Beneficiary.Questions[1].option=model.Questions[1].option;
        } else {
            $scope.Beneficiary.CustomerRelationship.refPartyID = model.partyId;
        }
        $scope.diablePrepopulatedData = true;
        $scope.copyDataFromPartyToBenFlag = true;
        $scope.updateErrorCount($scope.selectedTabId);
    }

    //cancel click on popup when delete is clicked
    $scope.cancel = function () {};


    $scope.dynamicValidations=function() { 

        $scope.dynamicErrorMessages = [];
        $scope.dynamicErrorCount = 0;
        var _errors = [];
        var _errorCount = 0;  

         if($scope.Beneficiary.BasicDetails.nationalIDType == undefined &&
          $scope.Beneficiary.BasicDetails.identityProof){                    
            _errorCount++;
            var error = {};
            error.message = translateMessages($translate,"eapp_vt.eAppBeneficiaryIdcardValidation1");            
            error.key = "idCard";
            _errors.push(error);
            $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
            $scope.dynamicErrorMessages = _errors;
        }
        
        if($scope.Beneficiary.BasicDetails.identityProof && $scope.Beneficiary.BasicDetails.nationalIDType=='NID' && $scope.validateCitizenId()){
           _errorCount++;
            var error = {};
            error.message = translateMessages($translate,"lms.invalidCitizenId");            
            error.key = "idCard";
            _errors.push(error);
            $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
            $scope.dynamicErrorMessages = _errors;
		}
        
       if($scope.Beneficiary.BasicDetails.identityProof!==undefined && $scope.Beneficiary.BasicDetails.identityProof!==''){
        if($scope.Beneficiary.BasicDetails.nationalIDType=='NID' && $scope.LifeEngageProduct.Insured.BasicDetails.IDcard===$scope.Beneficiary.BasicDetails.identityProof && $scope.LifeEngageProduct.Insured.BasicDetails.nationality=='THA'){
            _errorCount++;
            var error = {};
            error.message = translateMessages($translate,"eapp_vt.duplicateCitizenIdInsuredBeneficiary");            
            error.key = "idCard";
            _errors.push(error);
            $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
            $scope.dynamicErrorMessages = _errors;
        } 
       }
        
      $scope.isDuplicateNationalId=false;
        
      if($scope.Beneficiary.BasicDetails.identityProof!==undefined && $scope.Beneficiary.BasicDetails.identityProof!==''){
        if($scope.LifeEngageProduct.Beneficiaries){
            if($scope.LifeEngageProduct.Beneficiaries.length>=1){
                for(var v=0;v<$scope.LifeEngageProduct.Beneficiaries.length;v++){
                    if($scope.Beneficiary.BasicDetails.nationalIDType=='NID' && $scope.LifeEngageProduct.Beneficiaries[v].BasicDetails.identityProof===$scope.Beneficiary.BasicDetails.identityProof && v!=$scope.Beneficiary.BeneficiaryId-1 && $scope.LifeEngageProduct.Beneficiaries[v].BasicDetails.nationalIDType===$scope.Beneficiary.BasicDetails.nationalIDType){
                        $scope.isDuplicateNationalId=true;
                    }
                }
            }
            
            if($scope.isDuplicateNationalId){
                 _errorCount++;
                var error = {};
                error.message = translateMessages($translate,"eapp_vt.duplicateNationalId");            
                error.key = "idCard";
                _errors.push(error);
                $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                $scope.dynamicErrorMessages = _errors;
            }
        }        
      }
        
        $scope.dynamicErrorCount = _errorCount;
        if (!$scope.dynamicErrorMessages) {
            $scope.dynamicErrorMessages = [];
        }
        $scope.dynamicErrorMessages = _errors;      
        $scope.updateErrorCount('BeneficiarySubTab');
        $scope.refresh();

    }


  
    //delete beneficiary function call
    $scope.deleteBeneficiaryObj = function () {
        $scope.editIndex = $scope.index == $scope.editIndex ? {} : $scope.editIndex;
        var i = 0;
        $scope.addNewBeneficiary = false;
        $scope.addNew = false;
        
        $scope.LifeEngageProduct.Beneficiaries.splice($scope.index, 1);
        //$scope.setBeneficiryASPolicyHolderCheck();
        $scope.showErrorCount = false;
        $scope.showRelQuest = false;
        EappVariables.showRelQuest = false;
        for (var i = 0; i < $scope.LifeEngageProduct.Beneficiaries.length; i++) {
            if ($scope.LifeEngageProduct.Beneficiaries[i].insuredReasonAndExplanation) {
                $scope.LifeEngageProduct.Beneficiaries[i].insuredReasonAndExplanation = "";
            }
            $scope.LifeEngageProduct.Beneficiaries[i].BeneficiaryId = i + 1;
        }
        //calling insurance engagemnt rule while deleting a beneficiary.
        //$scope.runInsuranceEngagementRule($scope.successInsuranceEngagementRule);

        $scope.eAppParentobj.errorCount = 0;

        if($scope.LifeEngageProduct.splitBeneficiaryShareEqually == 'Yes'){
            $scope.updateShareDetails($scope.LifeEngageProduct.splitBeneficiaryShareEqually);
        }

        // if ($scope.LifeEngageProduct.Beneficiaries.length == 0) {
        //     $scope.eAppParentobj.errorCount = 0;
        // } else {
        //     $scope.updateErrorCount($scope.selectedTabId);
        // }
        if($scope.LifeEngageProduct.Beneficiaries.length===0){
            $rootScope.beneficiarySelected=false;
        }else{
            $rootScope.beneficiarySelected=true;
        }
        EappVariables.setEappModel($scope.LifeEngageProduct);
        $rootScope.refresh();
        
    };

    //delete beneficiary button click and popup
    $scope.deleteValueTable = function (index, deleteHeader) {
       
        $scope.index = index;

        $scope.Beneficiary = {};
        $scope.Beneficiary = $scope.LifeEngageProduct.Beneficiaries[index];
        
        var message = translateMessages($translate, "deleteBeneficiarytext1") + translateMessages($translate, deleteHeader) + translateMessages($translate, "deleteBeneficiarytext2");
        
        $scope.refresh();
        //TODO add in resource
        $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"), message, translateMessages($translate, "cancelButtonText"), $scope.cancel, translateMessages($translate, "deleteConfirmButtonText"), $scope.deleteBeneficiaryObj, $scope.cancel);
    };

    // added to check whether save button is clicked before save and proceed button click
    var alreadyRunletterRule;
    $scope.relationshipChange = function (model) {
        model['alreadyRunletterRule'] = false;
    }
    //edit beneficiary
    $scope.editBeneficiary = function (index) {
        $scope.addNewBeneficiary = true;
        $scope.addNew = false;
        $scope.edit = true;
        $scope.Beneficiary = {};
        $scope.Beneficiary = $scope.LifeEngageProduct.Beneficiaries[index];
        $scope.PrepopulatedBenefData = {
            "BasicDetails": {
                "gender": ""
            }
        };
        if ($scope.Beneficiary.CustomerRelationship.isSameAsPolicyHolder) {
            $scope.copyFromDataFlag = true;
            $scope.diablePrepopulatedData = true;
        } else {
            $scope.copyFromDataFlag = false;
            $scope.diablePrepopulatedData = false;
        }


        if ($scope.PrepopulatedDisabledEappBeneficiaryData && $scope.PrepopulatedDisabledEappBeneficiaryData.length != 0) {
            if ($scope.PrepopulatedDisabledEappBeneficiaryData[0].BasicDetails.firstName == $scope.LifeEngageProduct.Beneficiaries[index].BasicDetails.firstName) {
                if ($scope.PrepopulatedDisabledEappBeneficiaryData[0].isInsured == true) {
                    $scope.LifeEngageProduct.Beneficiaries[index].CustomerRelationship.isSameAsPolicyHolder = "Insured";
                }
                if ($scope.PrepopulatedDisabledEappBeneficiaryData[0].isPayer == true) {
                    $scope.LifeEngageProduct.Beneficiaries[index].CustomerRelationship.isSameAsPolicyHolder = "PolicyHolder";
                }

                for (var i = 0; i < $scope.PrepopulatedDisabledEappBeneficiaryData.length; i++) {
                    $scope.diablePrepopulatedData = true;
                    if ($scope.Beneficiary.id && $scope.Beneficiary.id == $scope.PrepopulatedDisabledEappBeneficiaryData[i].id) {
                        $scope.PrepopulatedBenefData = angular.copy($scope.PrepopulatedDisabledEappBeneficiaryData[i]);
                        break;
                    }
                }
            } else {
                $scope.diablePrepopulatedData = false;
            }
        }
        //if($scope.copyDataFromPartyToBenFlag == true){
        //	$scope.diablePrepopulatedData = true;
        //}
        if (typeof $scope.Beneficiary.isPayer != undefined && typeof $scope.Beneficiary.isBeneficiary != undefined &&
            $scope.Beneficiary.isPayer == true && $scope.Beneficiary.isBeneficiary == true) {
            $scope.policyHolderAddedAsBnenficiary = true;
        } else {
            $scope.policyHolderAddedAsBnenficiary = false;
        }
        $scope.relationshipWithPH = "";
        $scope.onGenderChangeInsured($scope.Beneficiary.BasicDetails.gender);
        $scope.fetchDDForCopyDataFrom(index);
        
        if (typeof $scope.Beneficiary.Questions == "undefined") {
            var obj = [{
                "questionID": "1",
                "option": ""
					}, {
                "questionID": "2",
                "option": ""
					}];
            $scope.Beneficiary["Questions"] = obj;
        }
		
        if($scope.Beneficiary.BeneficiaryId===undefined){
            $scope.Beneficiary.BeneficiaryId=index+1;
        }
        
        if($scope.Beneficiary.BasicDetails.nationalIDType==='CID'){
            $scope.Beneficiary.BasicDetails.nationalIDType='NID';
        }
        $scope.BeneficiaryTemp = JSON.parse(JSON.stringify($scope.LifeEngageProduct.Beneficiaries[index]));
        $scope.editIndex = index;
        $scope.showErrorCount = true;
        $scope.refresh();
        $scope.checkRelations($scope.Beneficiary.CustomerRelationship.relationWithInsured);
        $scope.ShowOthersType($scope.Beneficiary.BasicDetails.nationalIDType);
        $timeout(function () {
            $scope.updateErrorCount($scope.selectedTabId);
        }, 50);
    };

    //save beneficiary
    $scope.saveBeneficiary = function () {

        if ($scope.edit == true) {
            $scope.edit = false;
        }



        //$scope.showErrorCount = false;
        if ($scope.LifeEngageProduct.Beneficiaries.length > 0) {
            var father_count = 0,
                mother_count = 0;
            for (var index in $scope.LifeEngageProduct.Beneficiaries) {
                if (index != "undefined" && $scope.LifeEngageProduct.Beneficiaries[index].CustomerRelationship) {
                    if ($scope.LifeEngageProduct.Beneficiaries[index].CustomerRelationship.relationWithInsured == "Father") {
                        father_count++;
                    } else if ($scope.LifeEngageProduct.Beneficiaries[index].CustomerRelationship.relationWithInsured == "Mother") {
                        mother_count++;
                    }
                }
            }

            if (mother_count > 1) {
                $scope.lePopupCtrl.showWarning(
                    translateMessages($translate,
                        "lifeEngage"),
                    translateMessages($translate,
                        "eapp_vt.parentValidationMother"),
                    translateMessages($translate,
                        "fna.ok"));
                $scope.LifeEngageProduct.Beneficiaries[$scope.LifeEngageProduct.Beneficiaries.length - 1].CustomerRelationship.relationWithInsured = "";
                
                $scope.refresh();
                $scope.updateErrorCount($scope.selectedTabId);
                $scope.disableSave = true;
                
            } else if (father_count > 1) {
                $scope.lePopupCtrl.showWarning(
                    translateMessages($translate,
                        "lifeEngage"),
                    translateMessages($translate,
                        "eapp_vt.parentValidationFather"),
                    translateMessages($translate,
                        "fna.ok"));
                $scope.LifeEngageProduct.Beneficiaries[$scope.LifeEngageProduct.Beneficiaries.length - 1].CustomerRelationship.relationWithInsured = "";
                
                $scope.refresh();
                $scope.updateErrorCount($scope.selectedTabId);
                $scope.disableSave = true;
                
            } else {
                $scope.addNewBeneficiary = false;
                $scope.addNew = false;
                $scope.Beneficiary = {};
                if ($scope.LifeEngageProduct.Product.ProductDetails.productCode == 1) {
                    for (i = 0; i < $scope.LifeEngageProduct.Beneficiaries.length; i++) {
                        // added to check whether save button is clicked before save and proceed button click
                        if ($scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship) {
                            $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.alreadyRunletterRule = true;
                        }
                    }
                    $scope.updateErrorCount($scope.selectedTabId);

                } else {
                    $scope.getBeneficiaryUpdated();
                }
            }

        } else {
            $scope.getBeneficiaryUpdated();
        }
        $scope.diablecopyFromData = true;
    };

    $scope.getBeneficiaryUpdated = function () {
        LEDynamicUI.getUIJson(rootConfig.eAppConfigJson, false, function (eAppConfigJson) {
            // var relationshipWithInsured;
            // if ($scope.LifeEngageProduct.Proposer.CustomerRelationship.isPayorDifferentFromInsured != "Yes") {
            //     relationshipWithInsured = EappVariables.EappModel.Proposer.BasicDetails.maritalStatus;
            // } else {
            //     relationshipWithInsured = EappVariables.EappModel.Insured.BasicDetails.maritalStatus;
            // }
            // EappVariables.beneficiaryPreDefinedList = eAppConfigJson["beneficiaryAttachmentList"];
            // var currentRelList = EappVariables.beneficiaryPreDefinedList[relationshipWithInsured];
            var benficiaryRelList = [];
            var joinBenficiaryRelList = [];
            for (i = 0; i < $scope.LifeEngageProduct.Beneficiaries.length; i++) {
                if ($scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship) {
                    benficiaryRelList.push($scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.relationWithInsured);
                }
                // added to check whether save button is clicked before save and proceed button click
                if ($scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship) {
                    $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.alreadyRunletterRule = true;
                }
            }
            if (benficiaryRelList.length > 0) {
                joinBenficiaryRelList = benficiaryRelList.join("/");
            }
            //calling insurance engagement rule
//            $scope.runInsuranceEngagementRule($scope.successInsuranceEngagementRule);
        });

    }

    //creating beneficaiaries with inusrance engagement letter
    $scope.displayInsuranceEngagement = function () {
        //pushing those beneficiary who has the insurance engagemnt letter.In UI apply ngrepeat to this array
        $scope.InsuranceEngagementbeneficiaryList = [];
        $scope.showEngagementLetter = false;
        for (var beneficiaryObj in $scope.LifeEngageProduct.Beneficiaries) {
            var sampleBeneficiary = $scope.LifeEngageProduct.Beneficiaries[beneficiaryObj];
            if (sampleBeneficiary.hasEngagementLetter == "true" || sampleBeneficiary.hasEngagementLetter == true) {
                $scope.showEngagementLetter = true;
                $scope.InsuranceEngagementbeneficiaryList.push(sampleBeneficiary);
            }
        }

        $scope.refresh();
        $scope.updateErrorCount($scope.selectedTabId);
    }
    //success call back of insurance engagement rule
    $scope.successInsuranceEngagementRule = function (output) {
        var beneficiaryIndex = 1;
        //do..while used for comparing rule output beneficaryid and input id.so that variable con be correctly assigned for coresponding beneficiary
        do {
            var outputCheck = "Beneficiary" + beneficiaryIndex;
            if (GLI_EvaluateService.stringEvaluate(output[outputCheck]).hasEngagementLetter && GLI_EvaluateService.stringEvaluate(output[outputCheck]).hasEngagementLetter == true) {
                for (var mainBeneficiary in $scope.LifeEngageProduct.Beneficiaries) {
                    if (GLI_EvaluateService.stringEvaluate(output[outputCheck]).partyIdentifier == $scope.LifeEngageProduct.Beneficiaries[mainBeneficiary].BeneficiaryId) {
                        $scope.LifeEngageProduct.Beneficiaries[mainBeneficiary].hasEngagementLetter = true;
                    }
                }
            } else {
                for (var mainBeneficiary1 in $scope.LifeEngageProduct.Beneficiaries) {
                    if (GLI_EvaluateService.stringEvaluate(output[outputCheck]).partyIdentifier == $scope.LifeEngageProduct.Beneficiaries[mainBeneficiary1].BeneficiaryId) {
                        $scope.LifeEngageProduct.Beneficiaries[mainBeneficiary1].hasEngagementLetter = false;
                    }
                }
            }
            beneficiaryIndex++;
        } while (beneficiaryIndex < 5);

        //pushing those beneficiary who has the insurance engagemnt letter.In UI apply ngrepeat to this array
        $scope.InsuranceEngagementbeneficiaryList = [];
        $scope.showEngagementLetter = false;
        for (var processedBeneficiary in $scope.LifeEngageProduct.Beneficiaries) {
            var sampleBeneficiary = angular.copy($scope.LifeEngageProduct.Beneficiaries[processedBeneficiary]);
            if (sampleBeneficiary.hasEngagementLetter) {
                $scope.showEngagementLetter = true;
                $scope.InsuranceEngagementbeneficiaryList.push(sampleBeneficiary);
            }
        }
        $scope.refresh();
        $scope.updateErrorCount($scope.selectedTabId);
    }
    $scope.insuranceEngagementError = function () {
        $scope.updateErrorCount($scope.selectedTabId);
    }
    $scope.runInsuranceEngagementRule = function (successCallbackRule) {
        var transactionObj = {};
        var ruleInfo = {};
        ruleInfo.ruleName = rootConfig.insuranceEngagementRule;
        ruleInfo.offlineDB = rootConfig.insuranceEngagementDB;
        GLI_EappService.convertToCanonicalInsuranceEngagementLetter($scope.LifeEngageProduct, function (canonicalInputTorule) {
            transactionObj = canonicalInputTorule;
            GLI_RuleService.runInsuranceEngagement(transactionObj, ruleInfo, successCallbackRule, $scope.insuranceEngagementError);
        });
    }
    $scope.errCheck = function (id) {
        var elem = document.querySelector('#' + id);
        if (elem.attributes.class.value.indexOf('ng-invalid-pattern') > -1) {
            $scope.errArr.push(id);
        }
    }
    //cancel button click
    $scope.cancelBeneficiary = function () {
        
        if ($scope.addNew == true) {
            $scope.LifeEngageProduct.Beneficiaries[$scope.LifeEngageProduct.Beneficiaries.length - 1] = {};
            $scope.LifeEngageProduct.Beneficiaries.splice($scope.LifeEngageProduct.Beneficiaries.length - 1, 1);
        } else {
            $scope.LifeEngageProduct.Beneficiaries[$scope.editIndex] = $scope.BeneficiaryTemp;
        }
        if ($scope.errArr.length > 0) {
            for (var i = 0; i <= $scope.errArr.length; i++) {
                angular.element('#' + $scope.errArr[i]).val("");
            }
        }

        if($scope.LifeEngageProduct.splitBeneficiaryShareEqually == 'Yes'){
            $scope.updateShareDetails($scope.LifeEngageProduct.splitBeneficiaryShareEqually);
        }

        //$scope.setBeneficiryASPolicyHolderCheck();
        $scope.addNewBeneficiary = false;
        $scope.addNew = false;
        $scope.showErrorCount = false;
        $scope.eAppParentobj.errorCount = 0;
        
        if($scope.LifeEngageProduct.Beneficiaries.length===0){
            $rootScope.beneficiarySelected=false;
        }else{
            $rootScope.beneficiarySelected=true;
        }
        EappVariables.setEappModel($scope.LifeEngageProduct);
        $scope.refresh();
    }

    $scope.formatForShare = function (share) {
        var shareTemp;
        if (typeof (share) == "undefined" || share == "") {
            shareTemp = '';
        } else {
            shareTemp = parseInt(share);
        }
        if (shareTemp != '') {
            shareTemp = shareTemp + '%';
        }
        return (shareTemp);
    }

    //adding new beneficiary button click
    //check if there is already (10)4(new requirement) beneficirires. if not it will add else it will not
    $scope.addBeneficiary = function () {
        //$scope.showRelQuest = false;
        $scope.showNationalId = true;
        $scope.diablePrepopulatedData = false;
        $scope.diablecopyFromData = false;
        $scope.copyFromDataFlag = false;
        EappVariables.showRelQuest = false;
        //configuring the number of beneficiaries that can be added
        if ($scope.LifeEngageProduct.Beneficiaries.length >= rootConfig.eAppBeneficiaryCount) {
            $scope.isBeneficiaryGreaterthanTen = true;
        } else {
            $scope.addNewBeneficiary = true;
            $scope.addNew = true;
            $scope.showErrorCount = true;

            //this tag is used to disable the fileds. if not defined as empty is gets undefined and will be always disabled.
            //disabling of prepopulated data
            //For beneficiary the same json object is used.Hence needed to define.
            //Need to check for other solution.
            $scope.PrepopulatedBenefData = {
                "BasicDetails": {
                    "gender": ""
                }
            };

            $scope.Beneficiary = {
                "BasicDetails": {},
                "CustomerRelationship": {},
                "isSameAsPolicyHolderFlag": false,
                "insuredReasonAndExplanation": "",
                "Questions": [{
                    "questionID": "301",
                    "option": ""
								}, {
                    "questionID": "2",
                    "option": ""
								}]
            };
            //added the partyid for new beneficiary added
            $scope.Beneficiary.BeneficiaryId = $scope.LifeEngageProduct.Beneficiaries.length + 1;
            $scope.relationshipWithPH = "";
            $scope.policyHolderAddedAsBnenficiary = false;
            $scope.fetchDDForCopyDataFrom($scope.Beneficiary.BeneficiaryId);
            $scope.checkRelations();
            if ($scope.SameASPolicyHolder == true) {
                $scope.Beneficiary.isSameAsPolicyHolderFlag = true;
                $scope.SameASPolicyHolder = false;
            }
            $scope.LifeEngageProduct.Beneficiaries.push($scope.Beneficiary);
            $timeout(function () {
                $scope.updateErrorCount($scope.selectedTabId);
            }, 0);
            $scope.refresh();
        }
        if($scope.LifeEngageProduct.Beneficiaries.length===0){
            $rootScope.beneficiarySelected=false;
        }else{
            $rootScope.beneficiarySelected=true;
        }
        EappVariables.setEappModel($scope.LifeEngageProduct);
    };

    if($scope.LifeEngageProduct.Beneficiaries.length===0){
        $rootScope.beneficiarySelected=false;
    }else{
        $rootScope.beneficiarySelected=true;
    }
    
    //For counting no.of AdditionalQuestions selected
    $scope.hasInsuranceEngagementLetter = function (val) {
        var str = val;
        if (str == 'true' || str == true) {
            $scope.LifeEngageProduct.hasInsuranceEngagementLetter = true;
        } else {
            $scope.LifeEngageProduct.hasInsuranceEngagementLetter = false;
        }
    }

    //For DOB validation
    $scope.validateDob = function () {
        $scope.dynamicErrorMessages = [];
        $scope.dynamicErrorCount = 0;
        var _errors = [];
        var _errorCount = 0;

        if ($scope.Beneficiary.BasicDetails.dob != "" && typeof $scope.Beneficiary.BasicDetails.dob != "undefined") {
            if (formatForDateControl($scope.Beneficiary.BasicDetails.dob) > formatForDateControl(new Date())) {
                var error = {};
                _errorCount++;
                error.message = translateMessages(
                    $translate,
                    "eapp_vt.beneficiaryDOBValidationMessage");
                // meetingDateLesserThanFollowup
                error.key = 'beneficiaryDateOfBirth';
                _errors.push(error);
            }
        }

        $scope.dynamicErrorCount = _errorCount;
        if (!$scope.dynamicErrorMessages) {
            $scope.dynamicErrorMessages = [];
        }
        $scope.dynamicErrorMessages = _errors;
        $rootScope.updateErrorCount('BeneficiarySubTab');
    }

   

    //For error count
    $scope.updateErrorCount = function (id) {
        $rootScope.updateErrorCount(id);
        $scope.selectedTabId = id;
        $scope.eAppParentobj.errorCount = $scope.errorCount;
        if ($scope.errorCount > 0) {
            $scope.disableSave = true;
        } else {
            $scope.disableSave = false;
        }
        EappVariables.setEappModel($scope.LifeEngageProduct);
        $scope.refresh();
    }
    //For error count
    $scope.insTabsErrorCount = function (id, index) {
        selectedtab = id;
        $scope.updateErrorCount(id);
        $scope.showErrorCount = true;
        for (var i = 0; i < $scope.innertabsVertical.length; i++) {
            if (i == index) {
                $scope.innertabsVertical[i] = 'selected';
            } else {
                $scope.innertabsVertical[i] = '';
            }
        }
    };
    //LE logic -to check if beneficiary dob is future date or not is not required in Generali
    //Hence customizing the code accordingly.
    $scope.showErrorPopup = function (id) {
        $rootScope.showErrorPopup($scope.selectedTabId);
    }
    //date to be shown in table in beneficiary format change function
    $scope.formatForDateControlBeneficiary = function (date) {
        if (typeof date != "undefined" && date != "") {
            now = LEDate(date);
            year = "" + now.getFullYear();
            month = "" + (now.getMonth() + 1);
            if (month.length == 1) {
                month = "0" + month;
            }
            day = "" + now.getDate();
            if (day.length == 1) {
                day = "0" + day;
            }
            return day + "-" + month + "-" + year;
        }
    }
    //Full name to be shown in table in beneficiary tab
    $scope.formatForFullName = function (firstName, lastName) {
        var firstNameTemp;
        var lastNameTemp;
        if (typeof (firstName) == "undefined" || firstName == "") {
            firstNameTemp = '';
        } else {
            firstNameTemp = firstName;
        }

        if (typeof (lastName) == "undefined" || lastName == "") {
            lastNameTemp = '';
        } else {
            lastNameTemp = lastName;
        }
        return (firstNameTemp + " " + lastNameTemp);
    }

    //function to set the flag if the beneficiary is added as policy holder 
    //For the prepopulated beneficiary,$scope.LifeEngageProduct.Beneficiaries[i].isPayer=="true" && $scope.LifeEngageProduct.Beneficiaries[i].isBeneficiary=="true"
    //check is used
    //For the beneficiary added in this page,$scope.LifeEngageProduct.Beneficiaries[i].isSameAsPolicyHolderFlag check is used.
    $scope.setBeneficiryASPolicyHolderCheck = function () {
        $scope.policyHolderAddedAsBeneficiaryCheck = false;
        if ($scope.LifeEngageProduct.Beneficiaries.length > 0) {
            for (var i = 0; i < $scope.LifeEngageProduct.Beneficiaries.length; i++) {
                if (typeof $scope.LifeEngageProduct.Beneficiaries[i].isPayer != "undefined" && typeof $scope.LifeEngageProduct.Beneficiaries[i].isBeneficiary != "undefined") {
                    if ($scope.LifeEngageProduct.Beneficiaries[i].isPayer == true && $scope.LifeEngageProduct.Beneficiaries[i].isBeneficiary == true) {
                        $scope.policyHolderAddedAsBeneficiaryCheck = true;
                        $scope.LifeEngageProduct.Beneficiaries[i].Questions = [];
                        if ($scope.LifeEngageProduct.Proposer.Questions) {
                            $scope.LifeEngageProduct.Beneficiaries[i].Questions.push($scope.LifeEngageProduct.Proposer.Questions[0]);
                            $scope.LifeEngageProduct.Beneficiaries[i].Questions.push($scope.LifeEngageProduct.Proposer.Questions[1]);
                        }
                        break;
                    }
                }
                /*else{
                						if(typeof $scope.LifeEngageProduct.Beneficiaries[i].isSameAsPolicyHolderFlag!="undefined" && $scope.LifeEngageProduct.Beneficiaries[i].isSameAsPolicyHolderFlag==true){
                							$scope.policyHolderAddedAsBeneficiaryCheck=true;
                							$scope.LifeEngageProduct.Beneficiaries[i].Questions = [];
                							$scope.LifeEngageProduct.Beneficiaries[i].Questions.push($scope.LifeEngageProduct.Proposer.Questions[0]);
                							$scope.LifeEngageProduct.Beneficiaries[i].Questions.push($scope.LifeEngageProduct.Proposer.Questions[1]);
                							break;
                						}
                					}*/
            }
        }
        if ($scope.policyHolderAddedAsBeneficiaryCheck == true) {
            $scope.SameASPolicyHolder = false;
        }
        //  else {
        //     if ($scope.LifeEngageProduct.Proposer.CustomerRelationship.isPayorDifferentFromInsured == 'Yes') {
        //         $scope.SameASPolicyHolder = true;
        //     } else {
        //         $scope.SameASPolicyHolder = false;
        //     }
        // }
    }

    //to copy the policy holder details to beneficiary if policy holder same as beneficiary checked
    $scope.CopyDetailsOfPolicyHolder = function (ind, val) {
        if (val == "Insured") {
            $scope.LifeEngageProduct.Beneficiaries[ind].BasicDetails.firstName = $scope.LifeEngageProduct.Insured.BasicDetails.firstName;
            $scope.LifeEngageProduct.Beneficiaries[ind].BasicDetails.gender = $scope.LifeEngageProduct.Insured.BasicDetails.gender;
            //$scope.LifeEngageProduct.onGenderChangeInsured($scope.Beneficiary.BasicDetails.gender);
            $scope.LifeEngageProduct.Beneficiaries[ind].BasicDetails.dob = $scope.LifeEngageProduct.Insured.BasicDetails.dob;
            if ($scope.LifeEngageProduct.Insured.BasicDetails.identityProof) {
                $scope.LifeEngageProduct.Beneficiaries[ind].BasicDetails.identityProof = $scope.LifeEngageProduct.Insured.BasicDetails.identityProof;
            }
            if ($scope.LifeEngageProduct.Insured.BasicDetails.nationalIDType) {
                $scope.LifeEngageProduct.Beneficiaries[ind].BasicDetails.nationalIDType = $scope.LifeEngageProduct.Insured.BasicDetails.nationalIDType;
            }
            $scope.updateErrorCount($scope.selectedTabId);
        }
        if (val == "PolicyHolder") {
            $scope.LifeEngageProduct.Beneficiaries[ind].BasicDetails.firstName = $scope.LifeEngageProduct.Proposer.BasicDetails.firstName;
            $scope.LifeEngageProduct.Beneficiaries[ind].BasicDetails.gender = $scope.LifeEngageProduct.Proposer.BasicDetails.gender;
            $scope.LifeEngageProduct.Beneficiaries[ind].BasicDetails.dob = $scope.LifeEngageProduct.Proposer.BasicDetails.dob;
            if ($scope.LifeEngageProduct.Proposer.BasicDetails.identityProof) {
                $scope.LifeEngageProduct.Beneficiaries[ind].BasicDetails.identityProof = $scope.LifeEngageProduct.Proposer.BasicDetails.identityProof;
            }
            if ($scope.LifeEngageProduct.Proposer.BasicDetails.nationalIDType) {
                $scope.LifeEngageProduct.Beneficiaries[ind].BasicDetails.nationalIDType = $scope.LifeEngageProduct.Proposer.BasicDetails.nationalIDType;
            }
            $scope.updateErrorCount($scope.selectedTabId);
        }
        $scope.copyFromDataFlag = true;
    }

    //initilaize
    $scope.Initialize = function () {


        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_BeneficiarySubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_BeneficiarySubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_financilaDetailsSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_financilaDetailsSubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_mainInsuredSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_mainInsuredSubTab#tabDetails_1');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsStartbar#popupTabs_6');
        }
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PolicyHolderSubTab#tabDetails_1']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PolicyHolderSubTab#tabDetails_1');
        }		
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6');
		}	
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
		}
		
        if($scope.LifeEngageProduct.splitBeneficiaryShareEqually === undefined || $scope.LifeEngageProduct.splitBeneficiaryShareEqually === ""){
            $scope.LifeEngageProduct.splitBeneficiaryShareEqually = "Yes";
        }

        $scope.$parent.disableProceedButtonCommom = false;
        if ($scope.LifeEngageProduct.LastVisitedIndex != "") {
            EappVariables.EappModel.LastVisitedIndex = $scope.LifeEngageProduct.LastVisitedIndex; //UAT Bug fix: 400 - Details are not updated after editing from Summary screen
        }
        $scope.LifeEngageProduct = EappVariables.getEappModel();
        

        // $scope.updateErrorCount('BeneficiarySubTab');
        /*// Temp fix for Ben duplicate issue
        if($scope.LifeEngageProduct.Beneficiaries.length==2){
        	if($scope.LifeEngageProduct.Beneficiaries[0].BasicDetails.firstName == $scope.LifeEngageProduct.Beneficiaries[1].BasicDetails.firstName
        		&& $scope.LifeEngageProduct.Beneficiaries[0].BasicDetails.age == $scope.LifeEngageProduct.Beneficiaries[1].BasicDetails.age && $scope.LifeEngageProduct.Beneficiaries[0].BasicDetails.fullName == $scope.LifeEngageProduct.Beneficiaries[1].BasicDetails.fullName){
        			$scope.LifeEngageProduct.Beneficiaries.splice(0,1);
        	}
        } */

        // Beneficiary add check - new impl
        $scope.selBenLookUp = {};
        var options = [{
                "selBenTranslate": "Insured",
                "selBenValue": "Insured"
					}, {
                "selBenTranslate": "Policy Holder",
                "selBenValue": "Policy Holder"
					}
				];
        $scope.selBenLookUp.Options = options;
        $scope.errArr = [];
        $scope.eAppParentobj.PreviousPage = "";
        $timeout(function () {
            if (typeof EappVariables.EappKeys.Key15 != "undefined" && EappVariables.EappKeys.Key15 != "Pending Submission") {
                UtilityService.disableAllActionElements();
                $scope.disableButtons = true;
                $scope.refresh();
            }else if (typeof EappVariables.EappKeys.Key34 != "undefined" && EappVariables.EappKeys.Key34 != "" && EappVariables.EappKeys.Key34!==null) {
                UtilityService.disableAllActionElements();
            }
        }, 0);
        if (EappVariables.EappKeys.Key15 == "Confirmed" && $scope.$parent.LifeEngageProduct.Declaration.confirmSignature == true) {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = true;
        } else {
            $scope.$parent.LifeEngageProduct.Declaration.confirmSignature = false;
        }
        //For disabling and enabling the tabs.And also to know the last visited page.---starts
        $rootScope.selectedPage = "BeneficiaryDetails";
        $scope.LifeEngageProduct.LastVisitedUrl = "1,7,'BeneficiarySubTab',true,'BeneficiarySubTab',''";
        $scope.eAppParentobj.nextPage   	=	"1,8,'financilaDetailsSubTab',true,'financilaDetailsSubTab',''";
        //$scope.eAppParentobj.nextPage = "2,0,'ProductDetails',false,'ProductDetails',''";
        $scope.eAppParentobj.CurrentPage = "";
        if ($scope.LifeEngageProduct.LastVisitedIndex && $scope.LifeEngageProduct.LastVisitedIndex != "") {
            var subIndex = $scope.LifeEngageProduct.LastVisitedIndex.split(',');
            if (subIndex[1] < 7 && subIndex[0] <= $scope.LifeEngageProduct.LastVisitedUrl[0]) {
                $scope.LifeEngageProduct.LastVisitedIndex = "1,7";
            }
        }
        
        $(window).scrollTop(0); 
        //For disabling and enabling the tabs.And also to know the last visited page.---ends

        //Used to disable the fields which is prepopulated from BI or FNA
        $scope.PrepopulatedDisabledEappBeneficiaryData = angular.copy(EappVariables.prepopulatedBeneficiaryData);


        //if($scope.LifeEngageProduct.Beneficiaries.CustomerRelationship.isSameAsPolicyHolder){
        //		$scope.copyDataFromPartyToBenFlag = true;
        //}



//        if ($scope.LifeEngageProduct.Beneficiaries.length > 0) {
//            for (var i = 0; i < $scope.LifeEngageProduct.Beneficiaries.length; i++) {
//
//                if($scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.relationWithInsured){
//
//                    $scope.rejectPGP($scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.relationWithInsured);
//                    
//                }
//                if ($scope.LifeEngageProduct.Beneficiaries[i].isBeneficiary == true) {
//                    if ($scope.LifeEngageProduct.Beneficiaries[i].isInsured == true) {
//                        $scope.CopyDetailsOfPolicyHolder(i, "Insured");
//                    } else if ($scope.LifeEngageProduct.Beneficiaries[i].isPayer == true) {
//                        $scope.CopyDetailsOfPolicyHolder(i, "PolicyHolder");
//                    } else {
//                        $scope.copyFromDataFlag = true;
//                    }
//                }
//            }
//        }
        
//        $scope.translateRelationship=function(value){
//            var relationshipTable;
//            if(value !== undefined && value !== ""){
//                relationshipTable="eapp_vt.relationship"+value;   
//            }
//             
//            return translateMessages($translate, relationshipTable);              
//        }
                
        for (var i = 0; i < $scope.LifeEngageProduct.Beneficiaries.length; i++) {
			
			// Bug 4169 - start
		    delete $scope.LifeEngageProduct.Beneficiaries[i].ContactDetails;
		    // Bug 4169 - end
			
            /* $scope.name = $scope.LifeEngageProduct.Beneficiaries[i].BasicDetails.firstName;
            if($scope.name.indexOf(" ") < 0){
            $scope.LifeEngageProduct.Beneficiaries[i].BasicDetails.firstName = $scope.formatForFullName($scope.LifeEngageProduct.Beneficiaries[i].BasicDetails.firstName,$scope.LifeEngageProduct.Beneficiaries[i].BasicDetails.lastName);
            }*/
        }

        $scope.setBeneficiryASPolicyHolderCheck();
        /* FNA changes by LE Team for Defect 4259 Starts */

        for (var i = 0; i < $scope.LifeEngageProduct.Beneficiaries.length; i++) {
            
            if($scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.relationWithInsured=="Son" || $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.relationWithInsured=="Daughter"){
                $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.relationWithInsured="Child";
            }
        }
        /* FNA changes by LE Team for Defect 4259 Ends */
        if ($scope.LifeEngageProduct.Beneficiaries.length > 0) {
            $scope.LifeEngageProduct.Beneficiaries = $scope.LifeEngageProduct.Beneficiaries.sort(function (a, b) {
                return parseInt(a.BeneficiaryId) - parseInt(b.BeneficiaryId)
            });
        }
        UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
        $scope.showRelQuest = false;
        $scope.showErrorCount = false;
        $scope.addNewBeneficiary = false;
        //checking for beneficiaries to show insurance engagement while loading the page.
        //$scope.displayInsuranceEngagement();
        $scope.addNew = false;
        $scope.editIndex;
        $scope.eAppParentobj.errorCount = $scope.errorCount;
        EappVariables.showRelQuest = false;
        var model = "LifeEngageProduct";
        if ((rootConfig.autoSave.eApp) && (typeof EappVariables.EappKeys.Key15 == "undefined" || EappVariables.EappKeys.Key15 == "New" || EappVariables.EappKeys.Key15 == "Confirmed")) {
            if (AutoSave.destroyWatch) {
                AutoSave.destroyWatch();
            }
            AutoSave.setupWatchForScope(model, DataService, UtilityService, "", $scope, $debounce, $translate, $routeParams, "");
        } else if (AutoSave.destroyWatch) {
            AutoSave.destroyWatch();
        }
         /* Defect -3760 - starts */
        if($scope.LifeEngageProduct.splitBeneficiaryShareEqually == 'Yes' && $scope.PrepopulatedDisabledEappBeneficiaryData!=undefined){
            $scope.Beneficiary = $scope.LifeEngageProduct.Beneficiaries;
            $scope.updateShareDetails($scope.LifeEngageProduct.splitBeneficiaryShareEqually);
        }
         /* Defect -3760 - ends */
    }

    //BeneficiaryController Load Event and Load Event Handler
    $scope.$parent.$on('BeneficiarySubTab', function (event, args) {
        $scope.selectedTabId = args[0];
        $scope.Initialize();
        $rootScope.validationBeforesave = function (value, successcallback) {
            $scope.validateBeforeSaveDetails(value, successcallback);
        }
    });

    $scope.validateBeforeSaveDetails = function (value, successcallback) {
        //$scope.hasInsuranceEngagementLetter($scope.showEngagementLetter);
        if ($scope.LifeEngageProduct.Beneficiaries.length > 0) {
            var flag = false;
            for (var i = 0; i < $scope.LifeEngageProduct.Beneficiaries.length; i++) {
                if (typeof $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship == "undefined" || typeof $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.share == "undefined" || $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.share == "") {
                    flag = true;
                    break;
                }
            }
            if (flag == true) {
                $scope.$parent.disableProceedButtonCommom = false;
                $rootScope.showHideLoadingImage(false);
                $scope.lePopupCtrl.showWarning(
                    translateMessages($translate,
                        "lifeEngage"),
                    translateMessages($translate,
                        "beneficiaryMandatoryDetials"),
                    translateMessages($translate,
                        "fna.ok"));
            } else {
                var shareSum = 0;
                $scope.shareCheckFlag = true;
                for (var i = 0; i < $scope.LifeEngageProduct.Beneficiaries.length; i++) {
                    shareSum = shareSum + parseInt($scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.share);
                    if ($scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.share == 0) {
                        $scope.shareCheckFlag = false;
                    }
                }
                if ($scope.shareCheckFlag) {
                    if (shareSum != 100) {
                        $scope.$parent.disableProceedButtonCommom = false;
                        $rootScope.showHideLoadingImage(false);
                        $scope.lePopupCtrl.showWarning(
                            translateMessages($translate,
                                "lifeEngage"),
                            translateMessages($translate,
                                "beneficiaryShareValidationMesssage"),
                            translateMessages($translate,
                                "fna.ok"));
                    } else {
                        // added to check whether save button is clicked before save and proceed button click
                        var tempvar = true;
                        for (var index in $scope.LifeEngageProduct.Beneficiaries) {
                            var benef = $scope.LifeEngageProduct.Beneficiaries[index];

                            if ($scope.InsuranceEngagementbeneficiaryList && $scope.InsuranceEngagementbeneficiaryList.length > 0) {
                                if ($scope.InsuranceEngagementbeneficiaryList[index] && $scope.InsuranceEngagementbeneficiaryList[index].insuredReasonAndExplanation) {
                                    benef.insuredReasonAndExplanation = $scope.InsuranceEngagementbeneficiaryList[index].insuredReasonAndExplanation;

                                }
                            }
                            if (benef!==undefined){
								if (benef.CustomerRelationship!==undefined){
									if (benef.CustomerRelationship.alreadyRunletterRule == false) {
										tempvar = false;
									}
								}
							}
                        }
                        if (tempvar == false) {
                            $scope.$parent.disableProceedButtonCommom = false;
                            $rootScope.showHideLoadingImage(false);
                            $scope.lePopupCtrl.showWarning(
                                translateMessages($translate,
                                    "lifeEngage"),
                                translateMessages($translate,
                                    "beneficiarySaveValidationMesssage"),
                                translateMessages($translate,
                                    "fna.ok"));
                        } else {
                            successcallback();
                        }
                    }
                } else {
                    $scope.$parent.disableProceedButtonCommom = false;
                    $rootScope.showHideLoadingImage(false);
                    $scope.lePopupCtrl.showWarning(
                        translateMessages($translate,
                            "lifeEngage"),
                        translateMessages($translate,
                            "beneficiaryShareZeroValidationMesssage"),
                        translateMessages($translate,
                            "fna.ok"));
                }
            }
        } else {
            successcallback();
        }
    }
    var sharePercentage = 100;
    var sharePercentageSplit;
    $scope.updateShareDetails = function(param){
        
        if(param == 'Yes'){
            if($scope.LifeEngageProduct.Beneficiaries){
                
                angular.forEach($scope.LifeEngageProduct.Beneficiaries, function(value,key, index){
                      
                            if(angular.isDefined(sharePercentage)){
   
                                sharePercentageSplit = parseInt(sharePercentage / $scope.LifeEngageProduct.Beneficiaries.length);

                                  for (i = 0; i < $scope.LifeEngageProduct.Beneficiaries.length; i++) {
                                        if (typeof $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship != "undefined" &&
                                            typeof $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.share != "undefined") {

                                                $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.share = sharePercentageSplit;
                                                
                                            if($scope.Beneficiary!==undefined){
                                                if($scope.Beneficiary.CustomerRelationship===undefined){
                                                    $scope.Beneficiary.CustomerRelationship={};
                                                }
                                                
                                                $scope.Beneficiary.CustomerRelationship.share = sharePercentageSplit;
                                            }                                                

                                            } else {
                                                 /* Defect -3760 - starts */
                                                $scope.LifeEngageProduct.Beneficiaries[i].CustomerRelationship.share = sharePercentageSplit;
                                                 /* Defect -3760 - ends */
                                                if($scope.Beneficiary.CustomerRelationship===undefined){
                                                    $scope.Beneficiary.CustomerRelationship={};
                                                }
                                                if($scope.Beneficiary!==undefined){
                                                    if($scope.Beneficiary.CustomerRelationship===undefined){
                                                    $scope.Beneficiary.CustomerRelationship={};
                                                    }
                                                
                                                    $scope.Beneficiary.CustomerRelationship.share = sharePercentageSplit;
                                                }
                                            }

                                            if(i == 2){

                                                var objFirst = $scope.LifeEngageProduct.Beneficiaries[0].CustomerRelationship.share;
                                                var objSecond = $scope.LifeEngageProduct.Beneficiaries[1].CustomerRelationship.share;
                                                var objThree = $scope.LifeEngageProduct.Beneficiaries[2].CustomerRelationship.share;

                                                $scope.LifeEngageProduct.Beneficiaries[0].CustomerRelationship.share = Math.round(objFirst) + 1;
                                                $scope.LifeEngageProduct.Beneficiaries[1].CustomerRelationship.share = Math.round(objSecond);
                                                $scope.LifeEngageProduct.Beneficiaries[2].CustomerRelationship.share = Math.round(objThree);
                                                if($scope.Beneficiary!==undefined){
                                                    if($scope.Beneficiary.CustomerRelationship===undefined){
                                                    $scope.Beneficiary.CustomerRelationship={};
                                                    }
                                                
                                                    $scope.Beneficiary.CustomerRelationship.share = Math.round(objThree);
                                                }                                                

                                            } 

                                            if(i == 3){

                                                var objFirst = $scope.LifeEngageProduct.Beneficiaries[0].CustomerRelationship.share;
                                                $scope.LifeEngageProduct.Beneficiaries[0].CustomerRelationship.share = Math.round(objFirst) - 1;
                                            }
                                  }
                                
                            }
                            

                        

                });

            }

        }
        
    }

    $scope.validateCitizenId=function(){

        var idCardSubstr=$scope.Beneficiary.BasicDetails.identityProof.substring(0,12);
		var count=0;
		var len=$scope.Beneficiary.BasicDetails.identityProof.length+1;
		var rem=0;
		var quotient=0;
		var min=0;
		var nDigit=0;
                    
        for(var i=0;i<idCardSubstr.length;i++)
         {
			len=len-1;                            
			count=count+parseInt($scope.Beneficiary.BasicDetails.identityProof.substring(i,i+1),10)*len;
         }
                    
         quotient = Math.floor(count/11);
         rem=count%11;
         min=11-rem;
         nDigit=parseInt($scope.Beneficiary.BasicDetails.identityProof.substring(12,13),10);
           
         if(min>9){
             min=min%10;
         }
        
         if(nDigit!==min){
                return true;
		 }

    }; 


        $scope.nameLengthValidation = function(id){
            if($scope.Beneficiary.BasicDetails.firstName  && $scope.Beneficiary.BasicDetails.firstName.length < 3 ){     

                        $scope.dynamicErrorMessages = [];
                        $scope.dynamicErrorCount = 0;
                        var _errors = [];
                        var _errorCount = 0;
                        var error = {};
                        _errorCount++;
                        error.message = translateMessages(
                            $translate,
                            "lms.leadDetailsSectionleadFirstNameLengthValidationMessage");
                        // meetingDateLesserThanFollowup
                        error.key = id;
                        _errors.push(error);
                                                         
                                
                } 
                    $scope.dynamicErrorCount = _errorCount;
                    if (!$scope.dynamicErrorMessages) {
                        $scope.dynamicErrorMessages = [];
                    }
                    $scope.dynamicErrorMessages = _errors;
                    $scope.refresh();
                    $scope.updateErrorCount($scope.selectedTabId);
        }



     $scope.onSelectTypeHead = function (id) {
        $timeout(function () {
            $('#' + id).blur();
        }, 300);
    }

    $scope.formatLabel = function (model, options, type) {
        if (options && options.length > 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code) {
                    return options[i].value;
                }
            }
        } else {
            if (type == 'city') {
                $scope.state = $rootScope.state;
                for (var i = 0; i < $scope.state.length; i++) {
                    if (model === $scope.state[i].code) {
                        return $scope.state[i].value;
                    }
                }
            } else if (type == 'district') {
                $scope.district = $rootScope.district;
                for (var i = 0; i < $scope.district.length; i++) {
                    if (model === $scope.district[i].code) {
                        return $scope.district[i].value;
                    }
                }
            } else if (type == 'ward') {
                $scope.ward = $rootScope.wardOrHamlet;
                for (var i = 0; i < $scope.ward.length; i++) {
                    if (model === $scope.ward[i].code) {
                        return $scope.ward[i].value;
                    }
                }
            } else if (type == 'nationalityLsit') {
                $scope.nationalityLsit = $rootScope.nationalityLsit;
                for (var i = 0; i < $scope.nationalityLsit.length; i++) {
                    if (model === $scope.nationalityLsit[i].code) {
                        return $scope.nationalityLsit[i].value;
                    }
                }
            } else if (type == 'OccupationCategory') {
                $scope.OccupationCategory = $rootScope.occupationList;
                for (var i = 0; i < $scope.OccupationCategory.length; i++) {
                    if (model === $scope.OccupationCategory[i].code) {
                        return $scope.OccupationCategory[i].value;
                    }
                }
            }
        }
    };

    $scope.clearField=function(val){
        if(val==='id'){
            $scope.Beneficiary.BasicDetails.identityProof="";
        }
       
        $scope.refresh();
    }
   
    $scope.$on("$destroy", function () {
        if (this != null && typeof this !== 'undefined') {
            if (this.$$destroyed) return;
            while (this.$$childHead) {
                this.$$childHead.$destroy();
            }
            if (this.$broadcast) {
                this.$broadcast('$destroy');
            }
            this.$$destroyed = true;
            $timeout.cancel(timer);
        }
    });
    $rootScope.validationBeforesave = function (value, successcallback) {
        //EappVariables.setEappModel($scope.LifeEngageProduct);
        $scope.validateBeforeSaveDetails(value, successcallback);
    }
    
    $scope.LoadNationalID = function(){
        $scope.showNationalId = true;
    }
   $scope.ShowOthersType = function(param){

       if(param !== 'NID' && param !== null){
           $scope.showNationalId = false;
       }else{
            $scope.showNationalId = true;
       }

   }
   $scope.requiredFlag = true;
   $scope.checkRelations = function(param, id){
       if((['Father','Mother','Spouse','Child']).indexOf(param) != -1){
           $scope.requiredFlag = false;
       }else {
           $scope.requiredFlag = true;
       }

        $timeout(function () {
            $scope.updateErrorCount($scope.selectedTabId);
        }, 50);
            $scope.refresh();
       
   }
    $scope.LoadNationalID();
};