/*
 * Copyright 2015, LifeEngage 
 */
//DataService for save,retrieve, sysn
angular
		.module('lifeEngage.IllustratorVariables', [])
		.service(
				"IllustratorVariables",['$http',
				function($http) {
					var IllustratorVariables = {
						"illustrationInputTemplate" : "",
						"illustrationOutputTemplate" : "",
						"productDetails" : "",
						"illustrationCreationDate" : "",
						"selectedPage" : "",
						"illustratorModel" : "",
						"illustratorId" : "0",
						"illustrationAttachmentModel" : "",
						"fnaId" : "0",
						"leadId":"0",
						"transTrackingID" : "",
						"getIllustratorModel" : function() {
							if (!IllustratorVariables.illustratorModel) {
								IllustratorVariables.illustratorId = "0";
								IllustratorVariables.illustrationCreationDate = "";
								IllustratorVariables.illustrationInputTemplate = "";
								IllustratorVariables.illustrationOutputTemplate = "";
								IllustratorVariables.selectedPage = "";
								IllustratorVariables.RequirementFile=[];
								return new IllustrationObject();
							} else {
								var convertDateToText = function(data, type) {
									var transactionObj = {}
									transactionObj.TransactionData = data;
									transactionObj.Type = type;
									if (!angular.equals(
											transactionObj.TransactionData, {})
											&& !angular
													.equals(
															transactionObj.TransactionData,
															'{}')
											&& (transactionObj.Type == "illustration")) {
								
										if (transactionObj.Type == "illustration"
												&& typeof transactionObj.TransactionData.Insured != "undefined") {
											if (typeof transactionObj.TransactionData.Insured.BasicDetails.dob != "undefined"
													&& transactionObj.TransactionData.Insured.BasicDetails.dob != "") {
												transactionObj.TransactionData.Insured.BasicDetails.dob = formatForDateControl(transactionObj.TransactionData.Insured.BasicDetails.dob)
											}
											if (typeof transactionObj.TransactionData.Payer.BasicDetails.dob != "undefined"
													&& transactionObj.TransactionData.Payer.BasicDetails.dob != "") {
												transactionObj.TransactionData.Payer.BasicDetails.dob = formatForDateControl(transactionObj.TransactionData.Payer.BasicDetails.dob)

											}
//											if (typeof transactionObj.TransactionData.AdditionalInsured != "undefined"){
//												for(var i=0; i<transactionObj.TransactionData.AdditionalInsured.length; i++){
//														if(typeof transactionObj.TransactionData.AdditionalInsured[i].BasicDetails.dob != "undefined"
//															&& transactionObj.TransactionData.AdditionalInsured[i].BasicDetails.dob != "") {
//																transactionObj.TransactionData.AdditionalInsured[i].BasicDetails.dob = formatForDateControl(transactionObj.TransactionData.AdditionalInsured[i].BasicDetails.dob);
//														}
//												}
//											}
										}
									}
									return transactionObj.TransactionData;

								}
								var illustratorMod = convertDateToText(
										IllustratorVariables.illustratorModel,
										'illustration');
								illustratorMod = JSON.stringify(illustratorMod);
								return JSON.parse(illustratorMod);
							}
						},
						"setIllustratorModel" : function(value) {
							IllustratorVariables.illustratorModel = value;

						},
						"getIllustrationAttachmentModel" : function() {
							if (!IllustratorVariables.illustrationAttachmentModel) {
								return new IllustrationAttachment();
							} else {
								var illustrationAttachmentMod = JSON
										.stringify(IllustratorVariables.illustrationAttachmentModel);
								return JSON.parse(illustrationAttachmentMod);
							}
						},
						"setIllustrationAttachmentModel" : function(value) {
							IllustratorVariables.illustrationAttachmentModel = value;

						},
						"clearIllustrationVariables" : function() {
							IllustratorVariables.setIllustratorModel(null);
							IllustratorVariables.setIllustrationAttachmentModel(null);
							IllustratorVariables.fnaId = "";
							IllustratorVariables.illustratorId = "0";
							IllustratorVariables.transTrackingID = "";

						},
						"illustratorKeys" : {
							"Key12" : "",
							"Key15" : "",
							"creationDate" : "",
							"modifiedDate" : ""
						},
						"getProductTypes" :function() {
							var productTypes=[{"productType":"WholeLife"},{"productType": "Pension"},{"productType":"Endowment"},{"productType":"Investment"}];
							return JSON.parse(JSON.stringify(productTypes));
						}, 
						"getStatusOptions" :function() {
							var statusOptions=[{"status":"Initial"},{"status": "Final"},{"status": "Saved"}];
							return JSON.parse(JSON.stringify(statusOptions));
						}
					};

					return IllustratorVariables;

				}]);
