/*
 * Copyright 2015, LifeEngage 
 */
angular
		.module('lifeEngage.GLI_IllustratorVariables', [])
		.service(
				"GLI_IllustratorVariables",['$http','IllustratorVariables',
				function($http,IllustratorVariables) {
					IllustratorVariables.illustrationStatus = "";
					IllustratorVariables.channelId = "";
					IllustratorVariables.searchBiValue = "";
					IllustratorVariables.riderMatrix=[];
					IllustratorVariables.insForRiderObj=[];
					IllustratorVariables.searchBiKey = "";
					IllustratorVariables.totalRiderList = [];
					IllustratorVariables.leadEmailId = "";
					IllustratorVariables.transactionId = "";
					IllustratorVariables.mainInsTypeName ="Maininsured";
					IllustratorVariables.illustrationFromKMC = false;
					IllustratorVariables.illustrationFromIllusPersonal = false;
					IllustratorVariables.editFlowPackage = "";
					IllustratorVariables.isValidationSuccess = false; 
					IllustratorVariables.multipleInsuredRiders =[];
                    IllustratorVariables.leadName = "";	
					IllustratorVariables.leadData = {};
                    IllustratorVariables.fromFNAChoosePartyScreenFlow = false;
					IllustratorVariables.fromIllustrationAfterSave = false;
                    IllustratorVariables.fromChooseAnotherProduct = false;
                    IllustratorVariables.illustrationNumberRDS = "";
					IllustratorVariables.illustrationConfirmedDate="";
					IllustratorVariables.addInsChildCount = 0;
					IllustratorVariables.addInsSelfCount = 0;
					IllustratorVariables.addInsSpouseCount = 0;
                    IllustratorVariables.insuredList =[];
                    IllustratorVariables.getMainInsuredType = function() {
						var mainInsuredTypeArr  = [{
							"1" : [ {
								"minAge" : "0",
								"maxAge" : "17",
								"value" : "Child"
							}, {
								"minAge" : "18",
								"maxAge" : "24",
								"value" : ""
							}, {
								"minAge" : "25",
								"maxAge" : "",
								"value" : "Adult/Married"

							} ],
							"4" : [ {
								"minAge" : "0",
								"maxAge" : "17",
								"value" : "Child"
							}, {
								"minAge" : "18",
								"maxAge" : "24",
								"value" : ""
							}, {
								"minAge" : "25",
								"maxAge" : "",
								"value" : "Adult/Married"

							}],
							"5" : [ {
								"minAge" : "0",
								"maxAge" : "17",
								"value" : "Child"
							},{
								"minAge" : "18",
								"maxAge" : "24",
								"value" : ""
							}, {
								"minAge" : "25",
								"maxAge" : "",
								"value" : "Adult/Married"
							} ],
							"20" : [ {
								"minAge" : "0",
								"maxAge" : "17",
								"value" : "Child"
							},{
								"minAge" : "18",
								"maxAge" : "25",
								"value" : ""
							}, {
								"minAge" : "25",
								"maxAge" : "",
								"value" : "Adult/Married"
							} ],
							"9" : [ {
								"minAge" : "0",
								"maxAge" : "17",
								"value" : "Child"
							},{
								"minAge" : "18",
								"maxAge" : "24",
								"value" : ""
							}, {
								"minAge" : "25",
								"maxAge" : "",
								"value" : "Adult/Married"
							} ]
						}]
					return JSON.parse(JSON.stringify(mainInsuredTypeArr));
				},
				IllustratorVariables.clearIllustrationVariables = function() {
					IllustratorVariables.setIllustratorModel(null);
					IllustratorVariables.setIllustrationAttachmentModel(null);
					IllustratorVariables.fnaId = "";
					IllustratorVariables.illustratorId = "0";
					IllustratorVariables.transTrackingID = "";
					IllustratorVariables.illustrationNumberRDS = "";
					IllustratorVariables.leadName = "";
					IllustratorVariables.fromChooseAnotherProduct = false;
					IllustratorVariables.transactionId = 0; 
				};
				return IllustratorVariables;
				}]);
			