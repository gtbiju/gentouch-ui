angular.module('lifeEngage.GLI_IllustratorService', []).factory(
	"GLI_IllustratorService",['$http', '$location', '$translate', 'UtilityService', 'PersistenceMapping','IllustratorService','$rootScope','DataService','SyncService','UserDetailsService','IllustratorVariables','$routeParams','DocumentService','globalService',
		function($http, $location, $translate, UtilityService, PersistenceMapping,IllustratorService,$rootScope,DataService,SyncService,UserDetailsService,IllustratorVariables,$routeParams,DocumentService,globalService) {
		//To Enable/Disable tabs on editing a transaction after the status is changed to Completed
		IllustratorService['onFieldChange'] = function($scope,IllustratorVariables,$rootScope){
			//added undefined check
			if(IllustratorVariables){
				if(IllustratorVariables.illustrationStatus == "Completed"){
					IllustratorVariables.illustrationStatus = "Draft";
					if($scope.child){
						$scope.child.setIllustrationTabs();
					}
					IllustratorService.clearingIllustrationData($scope,IllustratorVariables,$rootScope);
				}
				else if(IllustratorVariables.illustrationStatus == "Draft"){						
					IllustratorService.clearingIllustrationData($scope,IllustratorVariables,$rootScope);
				}
				else if(IllustratorVariables.illustrationStatus == "Confirmed"){
					$scope.disableSignaturepad= true;
				}
			}
		};
		
		IllustratorService.clearingIllustrationData = function($scope,IllustratorVariables,$rootScope)
		{
			$rootScope.premiumSummaryOutput = false;
			$scope.benefitSummaryData = {};
			if($scope.Illustration)
			{
				$scope.Illustration.IllustrationOutput = {};					
				$scope.Illustration.BenefitSummary = {};	                            
				$scope.Illustration.Email =  {};
				if($scope.premiumSummary)		
					$scope.Illustration.BenefitSummary = $scope.premiumSummary;	
				if($scope.Illustration.Product){
					//$scope.Illustration.Product.premiumSummary = {};
					$scope.Illustration.Product.premiumSummary.sumInsured = "";				
					$scope.Illustration.Product.premiumSummary.paymentMode = "";
					$scope.Illustration.Product.premiumSummary.totalPremium = "";
					$scope.Illustration.Product.premiumSummary.basicPremium = "";
					$scope.Illustration.Product.premiumSummary.riderPremium = "";
					$scope.Illustration.Product.premiumSummary.summaryBasicCoveragePeriod = "";
				}
				$scope.Key18 = ""; 
			}
			if($scope.IllustrationAttachment)
			{
				$scope.IllustrationAttachment.Upload.Signature[0].pages[0]={};
				$scope.IllustrationAttachment.Upload.Signature[0].pages[1]={};
			}	
		};
			
		IllustratorService.convertToCanonicalInput = function(hierarchicalObj, IllustratorVariables, container) {
			var canonicalObj = {};
			if (hierarchicalObj.Product.Extension !== null && typeof hierarchicalObj.Product.Extension != "undefined") {
				var channelObj={
						"id" : "",
						"name" : ""
				}
				if (hierarchicalObj.Product.Extension.channelID !== null && typeof hierarchicalObj.Product.Extension.channelID != "undefined") {
					channelObj.id= IllustratorVariables.channelId;
					if(IllustratorVariables.channelId=="9082"){
						channelObj.name="Agency";
					}else if(IllustratorVariables.channelId=='9084'){
						channelObj.name= "Exim"
					}else if(IllustratorVariables.channelId=='9085'){
						channelObj.name= "TCB"
					}
				} else if(IllustratorVariables.channelId != null && typeof IllustratorVariables.channelId != "undefined") {
					channelObj.id= IllustratorVariables.channelId;
					if(IllustratorVariables.channelId=="9082"){
						channelObj.name="Agency";
					}else if(IllustratorVariables.channelId=='9084'){
						channelObj.name= "Exim"
					}else if(IllustratorVariables.channelId=='9085'){
						channelObj.name= "TCB"
					}
				}
			}
			
			if (hierarchicalObj.Insured !== null && typeof hierarchicalObj.Insured != "undefined") {
				var insuredGender =  getGender(hierarchicalObj.Insured.BasicDetails.gender);
				var insuredObj = {
					"name" : hierarchicalObj.Insured.BasicDetails.firstName + " " + hierarchicalObj.Insured.BasicDetails.lastName,
					"age" : hierarchicalObj.Insured.BasicDetails.age,
					"sex" : insuredGender,
					"insuredType" : "Maininsured",
					"isLIEqualsPH" : hierarchicalObj.Insured.BasicDetails.isInsuredSameAsPayer,
					"occupationClass" : hierarchicalObj.Insured.BasicDetails.occupationClassCode
				};
			}
			
			if (hierarchicalObj.Payer !== null && typeof hierarchicalObj.Payer != "undefined") {
				var payorAgeDays;
				var payorGender = getGender(hierarchicalObj.Payer.BasicDetails.gender);
				payorAgeDays = hierarchicalObj.Payer.BasicDetails.age;
				var payorObj = {
					"name" : hierarchicalObj.Payer.BasicDetails.firstName + " " + hierarchicalObj.Payer.BasicDetails.lastName,
					"age" : payorAgeDays,
					"sex" : payorGender,
					"occupationClass": hierarchicalObj.Payer.BasicDetails.occupationClassCode
				};
			}
			
			if(hierarchicalObj.Product.ProductDetails.productId !== null && typeof hierarchicalObj.Product.ProductDetails.productId != "undefined"){
				hierarchicalObj.Product.ProductDetails.productCode=hierarchicalObj.Product.ProductDetails.productId;
			}

			if (hierarchicalObj.Product.policyDetails !== null && typeof hierarchicalObj.Product.policyDetails != "undefined") {
				// Premium Mode
				if(hierarchicalObj.Product.policyDetails.premiumFrequency == "Annual"){
					hierarchicalObj.Product.policyDetails.premiumFrequency = "6001";
				}
				else if(hierarchicalObj.Product.policyDetails.premiumFrequency == "Semi Annual"){
					hierarchicalObj.Product.policyDetails.premiumFrequency = "6002";
				} else if(hierarchicalObj.Product.policyDetails.premiumFrequency == "Halfyearly"){
					hierarchicalObj.Product.policyDetails.premiumFrequency = "6002";
				} else if(hierarchicalObj.Product.policyDetails.premiumFrequency == "Half Yearly"){
					hierarchicalObj.Product.policyDetails.premiumFrequency = "6002";
				}
				else if(hierarchicalObj.Product.policyDetails.premiumFrequency == "Quarterly"){
					hierarchicalObj.Product.policyDetails.premiumFrequency = "6003";
				}
				else if(hierarchicalObj.Product.policyDetails.premiumFrequency == "Monthly"){
					hierarchicalObj.Product.policyDetails.premiumFrequency = "6004";
				}
				else {
					hierarchicalObj.Product.policyDetails.premiumFrequency = "";
				}
				
				if(hierarchicalObj.Product.policyDetails.packages!= undefined && hierarchicalObj.Product.policyDetails.packages!= "" ){
						if(hierarchicalObj.Product.policyDetails.packages.indexOf("Package A") > -1){
							hierarchicalObj.Product.policyDetails.packages = "A";
						}else if(hierarchicalObj.Product.policyDetails.packages.indexOf("Package B") > -1){
							hierarchicalObj.Product.policyDetails.packages = "B";
						}else if(hierarchicalObj.Product.policyDetails.packages.indexOf("Package C") > -1){
							hierarchicalObj.Product.policyDetails.packages = "C";
						}else if(hierarchicalObj.Product.policyDetails.packages.indexOf("Package D") > -1){
							hierarchicalObj.Product.policyDetails.packages = "D";
						}else if(hierarchicalObj.Product.policyDetails.packages.indexOf("Plan 1") > -1){
                            hierarchicalObj.Product.policyDetails.packages = "Plan 1";
                        }else if(hierarchicalObj.Product.policyDetails.packages.indexOf("Plan 2") > -1){
                            hierarchicalObj.Product.policyDetails.packages = "Plan 2";
                        }else if(hierarchicalObj.Product.policyDetails.packages.indexOf("Plan 3") > -1){
                            hierarchicalObj.Product.policyDetails.packages = "Plan 3";
                        }else if(hierarchicalObj.Product.policyDetails.packages.indexOf("Plan 4") > -1){
                            hierarchicalObj.Product.policyDetails.packages = "Plan 4";
                        }
						
				} else {
					hierarchicalObj.Product.policyDetails.packages ="";
				}
				
				if(hierarchicalObj.Insured.OccupationDetails !== null &&  hierarchicalObj.Insured.OccupationDetails != "undefined"){
					var occCode1;
					var occCode2
					if(Object.keys(hierarchicalObj.Insured.OccupationDetails).length != 0){
						if(hierarchicalObj.Insured.OccupationDetails.length > 1){
							occCode1=hierarchicalObj.Insured.OccupationDetails[0].occupationCode
							occCode2=hierarchicalObj.Insured.OccupationDetails[1].occupationCode
						} else {
							occCode1=hierarchicalObj.Insured.OccupationDetails[0].occupationCode
							occCode2="";
						}
					}
				}
				/*var premiumPeriod = "";
				if(hierarchicalObj.Product.policyDetails.premiumPeriod !== "undefined" &&  hierarchicalObj.Product.policyDetails.premiumPeriod !== null){
					premiumPeriod = hierarchicalObj.Product.policyDetails.premiumPeriod;
				}
				if(hierarchicalObj.Product.policyDetails.premiumTerm !== "undefined" && hierarchicalObj.Product.policyDetails.premiumTerm !== null){
					premiumPeriod = hierarchicalObj.Product.policyDetails.premiumTerm;
				}*/

				var policyObj = {
					"premiumMode" : hierarchicalObj.Product.policyDetails.premiumFrequency,
					"productCode" : hierarchicalObj.Product.ProductDetails.productCode,
					"occCode1" : occCode1,
					"occCode2" : occCode2,
					"sumAssured" : hierarchicalObj.Product.policyDetails.sumInsured,
					"isSumAssured" : hierarchicalObj.Product.policyDetails.isSumAssured,
					"premium" : hierarchicalObj.Product.policyDetails.premium,
					"isPremium" : hierarchicalObj.Product.policyDetails.isPremium,
					"packageName" : hierarchicalObj.Product.policyDetails.packages,
					"taxRate" : hierarchicalObj.Product.policyDetails.tax,
					"premiumPayingTerm" : hierarchicalObj.Product.policyDetails.premiumPeriod
				};
			}
			
			
			riderObj={};
			if(hierarchicalObj.Product.RiderDetails.length > 0){
				for(var rIdx=0; rIdx<IllustratorVariables.totalRiderList.length; rIdx++){
					riderObj[IllustratorVariables.totalRiderList[rIdx]]= new Object();
					riderObj[IllustratorVariables.totalRiderList[rIdx]]={
								"id": "",
								"uniqueRiderName": "",
								"isRequired": "",
								"term": "",
								"sumAssured": "",
								"type": ""
						}
					}
				
				/*for(var i=0; i<hierarchicalObj.Product.RiderDetails.length; i++){
					for (j=0 ; j< IllustratorVariables.insuredList.length; j++){
						if(hierarchicalObj.Product.RiderDetails[i].insuredType == IllustratorVariables.insuredList[j]){
							if(riderObj[hierarchicalObj.Product.RiderDetails[i].planName]){
								riderObj[hierarchicalObj.Product.RiderDetails[i].planName][IllustratorVariables.insuredList[j]].id= hierarchicalObj.Product.RiderDetails[i].id;
								riderObj[hierarchicalObj.Product.RiderDetails[i].planName][IllustratorVariables.insuredList[j]].uniqueRiderName= hierarchicalObj.Product.RiderDetails[i].uniqueRiderName;
								riderObj[hierarchicalObj.Product.RiderDetails[i].planName][IllustratorVariables.insuredList[j]].isRequired= hierarchicalObj.Product.RiderDetails[i].isRequired;
								riderObj[hierarchicalObj.Product.RiderDetails[i].planName][IllustratorVariables.insuredList[j]].term= parseInt(hierarchicalObj.Product.RiderDetails[i].policyTerm);
								riderObj[hierarchicalObj.Product.RiderDetails[i].planName][IllustratorVariables.insuredList[j]].sumAssured= parseInt(hierarchicalObj.Product.RiderDetails[i].sumInsured);
								if(rootConfig.ridersWithPlanOption && rootConfig.ridersWithPlanOption.indexOf(hierarchicalObj.Product.RiderDetails[i].planName) != -1){
									riderObj[hierarchicalObj.Product.RiderDetails[i].planName][IllustratorVariables.insuredList[j]].planOption = parseInt(hierarchicalObj.Product.RiderDetails[i].sumInsured);
									riderObj[hierarchicalObj.Product.RiderDetails[i].planName][IllustratorVariables.insuredList[j]].sumAssured = "";
								}
								riderObj[hierarchicalObj.Product.RiderDetails[i].planName][IllustratorVariables.insuredList[j]].type= hierarchicalObj.Product.RiderDetails[i].insured;
							}		
						}
					}
				}*/
				//debugger;
				for(var i=0; i<hierarchicalObj.Product.RiderDetails.length; i++){
					for (j=0 ; j< IllustratorVariables.totalRiderList.length; j++){
						//var str = hierarchicalObj.Product.RiderDetails[i].insured;
						//var subStr = IllustratorVariables.totalRiderList[j];
						//IllustratorVariables.totalRiderList[j] = str + subStr;
						if(hierarchicalObj.Product.RiderDetails[i].shortName){
							if(hierarchicalObj.Product.RiderDetails[i].shortName == IllustratorVariables.totalRiderList[j]){
								riderObj[hierarchicalObj.Product.RiderDetails[i].shortName].id= hierarchicalObj.Product.RiderDetails[i].id;
								riderObj[hierarchicalObj.Product.RiderDetails[i].shortName].uniqueRiderName= hierarchicalObj.Product.RiderDetails[i].uniqueRiderName;
								riderObj[hierarchicalObj.Product.RiderDetails[i].shortName].isRequired= hierarchicalObj.Product.RiderDetails[i].isRequired;
								riderObj[hierarchicalObj.Product.RiderDetails[i].shortName].term= hierarchicalObj.Product.RiderDetails[i].policyTerm;
								riderObj[hierarchicalObj.Product.RiderDetails[i].shortName].sumAssured= hierarchicalObj.Product.RiderDetails[i].sumInsured;
								if(rootConfig.ridersWithPlanOption && rootConfig.ridersWithPlanOption.indexOf(hierarchicalObj.Product.RiderDetails[i].shortName) != -1){
									riderObj[hierarchicalObj.Product.RiderDetails[i].shortName].planOption = parseInt(hierarchicalObj.Product.RiderDetails[i].sumInsured);
									riderObj[hierarchicalObj.Product.RiderDetails[i].shortName].sumAssured = "";
								}
								riderObj[hierarchicalObj.Product.RiderDetails[i].shortName].type= hierarchicalObj.Product.RiderDetails[i].insured;
							}
						}
					}
				}
			}
			angular.extend(canonicalObj, riderObj);
			canonicalObj.InsuredDetails = insuredObj;
			canonicalObj.PayorDetails = payorObj;
			canonicalObj.PolicyDetails = policyObj;
			canonicalObj.Channel = channelObj;
			
			return canonicalObj;
		};

			

			
			IllustratorService['mapKeysforPersistence'] = function($scope,
					IllustratorVariables) {
			// Since in persistancemapping, fnaId is set from
			// fnavariables
			PersistenceMapping.Key1 = (IllustratorVariables.leadId != null && IllustratorVariables.leadId != 0) ? IllustratorVariables.leadId:"";
				PersistenceMapping.Key2 = (IllustratorVariables.fnaId != null && IllustratorVariables.fnaId != 0) ? IllustratorVariables.fnaId
						: "";
				PersistenceMapping.Key3 = (IllustratorVariables.illustratorId != "" && IllustratorVariables.illustratorId != 0) ? IllustratorVariables.illustratorId
						: "";
				PersistenceMapping.Key4 = ($scope.proposalId != null && $scope.proposalId != 0) ? $scope.proposalId
						: "";
			if($scope.Illustration.PrivacyLaw.userConsentFlag == 'Yes'){
				PersistenceMapping.Key5 = $scope.productId != null ? $scope.productId : $scope.Illustration.Product.ProductDetails.productId;
			}
			else {
				PersistenceMapping.Key5 = $scope.productId != null ? $scope.productId : "1";
			}
			PersistenceMapping.Key6 = $scope.Illustration.Insured.BasicDetails.firstName + " " + $scope.Illustration.Insured.BasicDetails.lastName;
			PersistenceMapping.Key7 = IllustratorVariables.fromChooseAnotherProduct;
			PersistenceMapping.Key8 = ($scope.Illustration.Insured.ContactDetails.mobileNumber1)?($scope.Illustration.Insured.ContactDetails.mobileNumber1):"";
			PersistenceMapping.Key10 = "FullDetails";
			if (!(rootConfig.isDeviceMobile)) {
				if (IllustratorVariables.illustratorId == null || IllustratorVariables.illustratorId == 0) {
					PersistenceMapping.Key13 = IllustratorVariables.illustrationConfirmedDate!=null?IllustratorVariables.illustrationConfirmedDate:"";                       
					IllustratorVariables.illustrationCreationDate = UtilityService.getFormattedDate();
				} else if (IllustratorVariables.illustrationCreationDate) {
					PersistenceMapping.Key13 = IllustratorVariables.illustrationCreationDate;
				}
			}
			if(!rootConfig.isDeviceMobile && !rootConfig.isOfflineDesktop){
				PersistenceMapping.Key12 = "";
			}else{
				PersistenceMapping.Key12 = IllustratorVariables.illustratorKeys.Key12;
			}
			PersistenceMapping.Key14 = UtilityService.getFormattedDate();
				PersistenceMapping.Key15 = IllustratorVariables.illustrationStatus != "" ? IllustratorVariables.illustrationStatus
						: "Draft";
			IllustratorVariables.illustrationStatus = PersistenceMapping.Key15;
			PersistenceMapping.Type = "illustration";
				PersistenceMapping.Key16 = $scope.syncStatus
 						!= null ? $scope.syncStatus : "";
			PersistenceMapping.Key19 = $scope.Illustration.Product.ProductDetails.productName;
			PersistenceMapping.Key20 = ($scope.Illustration.Insured.ContactDetails.emailId)?$scope.Illustration.Insured.ContactDetails.emailId :"";
			PersistenceMapping.Key21 = (typeof $scope.Illustration.Product.policyDetails !="undefined" && typeof $scope.Illustration.Product.policyDetails.sumInsured!="undefined" && $scope.Illustration.Product.policyDetails.sumInsured.toString())?($scope.Illustration.Product.policyDetails.sumInsured.toString()):"";
			PersistenceMapping.Key22 = IllustratorVariables.leadName;	
			PersistenceMapping.Key23 = IllustratorVariables.leadEmailId;
			PersistenceMapping.Key24 = IllustratorVariables.illustrationNumberRDS;
			PersistenceMapping.Key25  = (typeof $scope.Illustration.Product.policyDetails !="undefined" && typeof $scope.Illustration.Product.premiumSummary.totalPremium!="undefined" && $scope.Illustration.Product.premiumSummary.totalPremium.toString())?($scope.Illustration.Product.premiumSummary.totalPremium.toString()):"";
			PersistenceMapping.Key26 = $scope.Illustration.Insured.BasicDetails.nickName;
			PersistenceMapping.Key27 = 0;
			PersistenceMapping.Key28 = $scope.Illustration.Insured.BasicDetails.age;
			PersistenceMapping.Key29 = $scope.Illustration.Insured.BasicDetails.gender;
			PersistenceMapping.Key30 = (typeof $scope.Illustration.Product.policyDetails !="undefined" && typeof $scope.Illustration.Product.policyDetails.premiumPeriod!="undefined" && $scope.Illustration.Product.policyDetails.premiumPeriod.toString())?($scope.Illustration.Product.policyDetails.premiumPeriod.toString()):"";
			if (IllustratorVariables.transTrackingID == null || IllustratorVariables.transTrackingID == 0) {
				PersistenceMapping.TransTrackingID = UtilityService.getTransTrackingID();
				IllustratorVariables.transTrackingID = PersistenceMapping.TransTrackingID;
			}else{
				PersistenceMapping.TransTrackingID = IllustratorVariables.transTrackingID;
			}
		};
			
			IllustratorService['saveTransactions'] = function($scope,
					$rootScope, dataService, $translate,
					utilityService, IllustratorVariables, $routeParams,
				isAutoSave) {
			this.save = function(successCallback, errorCallback) {
				/* FNA Changes by LE Team Bug 3800 - starts */ 
				if (IllustratorVariables.transactionId !== undefined && IllustratorVariables.transactionId !== "") {
					$rootScope.transactionId = IllustratorVariables.transactionId;
				}
				/* FNA Changes by LE Team Bug 3800 - ends */ 

				//-	On editing a transaction after synching, the syncStatus will be saved as �NotSynced�
				if ((rootConfig.isDeviceMobile) && !(IllustratorVariables.illustratorId == null || IllustratorVariables.illustratorId == 0)){
					$scope.syncStatus = "NotSynced";
				}
				PersistenceMapping.clearTransactionKeys();
					IllustratorService.mapKeysforPersistence($scope,
							IllustratorVariables);
					 //var selectedLanguage = (rootConfig.locale == "vt_VT") ? "vt": "en";
				var selectedLanguage = (localStorage["locale"] == "th_TH") ? "th" : "en";
				if( $scope.Illustration.Product.templates )
					$scope.Illustration.Product.templates.selectedLanguage = selectedLanguage;
				var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
				if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
					var key11Data=transactionObj.Key11;
					transactionObj.Key11=IllustratorVariables.agentForGAO;
					transactionObj.Key38=IllustratorVariables.agentNameForGAO;
					transactionObj.Key36=key11Data;
					transactionObj.Key37=UserDetailsService.getAgentRetrieveData().AgentDetails.agentType;
					transactionObj.Key35=UserDetailsService.getAgentRetrieveData().AgentDetails.unit;
				}else{
					transactionObj.Key36=(typeof IllustratorVariables.GAOId !="undefined")?IllustratorVariables.GAOId:"";
					transactionObj.Key37=(typeof IllustratorVariables.agentType!="undefined")?IllustratorVariables.agentType:"";
					transactionObj.Key38=(typeof IllustratorVariables.agentNameForGAO!="undefined")?IllustratorVariables.agentNameForGAO:"";
					transactionObj.Key35=(typeof IllustratorVariables.GAOOfficeCode!="undefined")?IllustratorVariables.GAOOfficeCode:"";
				}
				$scope.onSaveSuccess = function(data) {
					if ((rootConfig.isDeviceMobile)) {
						if (data != null) {
								$rootScope.transactionId = ($rootScope.transactionId == null || $rootScope.transactionId == 0) ? data
										: $rootScope.transactionId;
						}
						$scope.status = "Saved";
						/* FNA Changes by LE Team Bug 3800 - starts */ 
						IllustratorVariables.transactionId = data;
						/* FNA Changes by LE Team Bug 3800 - ends */ 
					} else {
					IllustratorVariables.illustratorId = data.Key3;
					/* FNA Changes by LE Team Bug 3800 - starts */ 
					IllustratorVariables.transactionId = data.Key3;
					/* FNA Changes by LE Team Bug 3800 - ends */ 
						$scope.syncStatus = "Synced";
					}
					$routeParams.transactionId = $rootScope.transactionId;
					$routeParams.illustrationId = IllustratorVariables.illustratorId;
					if ((successCallback && typeof successCallback == "function")) {
						successCallback(isAutoSave);
					}

					$scope.refresh();
				};
				$scope.onSaveError = function(data) {
					if (!isAutoSave) {
						$rootScope.NotifyMessages(true, data);
					}
					$scope.refresh();
					$rootScope.showHideLoadingImage(false);

					if ((errorCallback && typeof errorCallback == "function")) {
						errorCallback();
					}
				};
					dataService.saveTransactions(transactionObj,
							$scope.onSaveSuccess, $scope.onSaveError); 
							
			};

		};
			
		IllustratorService.getEmailObj = function(transactionObjList,objectToAdd, successcallback){
			var objsToSync = [];//used in case of user is online and flag online_emailBI is set true
			var isComplete = false;
			$.each(transactionObjList, function(i, obj) {
			   if(obj) {
					obj.Key18 = true;                               
					obj.TransactionData.Email = objectToAdd;
					if ((rootConfig.isDeviceMobile)) {
						obj.TransactionData =  angular.toJson(obj.TransactionData);
                       }
                       else{
						obj.TransactionData =  obj.TransactionData; 
						obj.TransactionData.Insured.BasicDetails.fullName =  obj.Key6; 
					}
                       
					//If online mail sending needed keeping list of ids for selected sync
					objsToSync.push(obj.Id);
					if(i == (transactionObjList.length-1)){
						isComplete= true;
					}
				}
			}); 
			if(isComplete){
				successcallback(objsToSync);
			}
		}
			
		//Added for setting an additional object to transaction object  - Email BI
            IllustratorService.setEmailBIflagAndSaveTransactions = function(objectToAdd,transactionObjList,dataService
                            ,successCallback,errorCallback) {
            IllustratorService.getEmailObj(transactionObjList, objectToAdd, function(objsToSync){
	            dataService.saveTransactionObjectsMultiple(transactionObjList,function() {
	                if(rootConfig.online_emailBI) {
						var syncConfig = [{
							"module" :"illustration",
							"operation" : "syncTo",
							"selectedIds" : objsToSync                                  
						}];
						if ((rootConfig.isDeviceMobile) && checkConnection()) {
							if(objsToSync.length > 0) {
								//call selected sync                                                                                
	                                SyncService.initialize(syncConfig,
	                                function(){
									//Call Email Service to mail BI.                                
									PersistenceMapping.clearTransactionKeys();
									var transactionObjForMailReq = PersistenceMapping.mapScopeToPersistence({});
									transactionObjForMailReq.Type = "illustration";
									var userDetails = UserDetailsService.getUserDetailsModel();
									UtilityService.emailServiceCall(userDetails,transactionObjForMailReq, function(){
										successCallback();
									}, errorCallback);
	                                    
								},function(){
									errorCallback;
								});                                                                                     
								SyncService.runSyncThread();
							}                                   
	                        }
	                        else{
							PersistenceMapping.clearTransactionKeys();
							var transactionObjForMailReq = PersistenceMapping.mapScopeToPersistence({});
							transactionObjForMailReq.Type = "illustration";
							var userDetails = UserDetailsService.getUserDetailsModel();
							UtilityService.emailServiceCall(userDetails,transactionObjForMailReq);
							successCallback(true);
						}
	                } 
	                successCallback();
	                    
	            },errorCallback);
            });
        };
            
		//Added for setting an additional object to transaction object  - Email BI - Illustration Output Screen
		IllustratorService.emailAndSaveTransactionsBI = function(objectToAdd,$scope,transactionObjList,dataService,successCallback,errorCallback) {
			if((rootConfig.isDeviceMobile)){
				PersistenceMapping.clearTransactionKeys();
				IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
				PersistenceMapping.dataIdentifyFlag = false;
				PersistenceMapping.Type = "illustration";
				var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
				transactionObj.Key18 = true;                               
				transactionObj.TransactionData.Email = objectToAdd;
				if(checkConnection()) {
					DataService.saveOnline(transactionObj, function(data) {
						IllustratorVariables.illustratorId = data.Key3;
						IllustratorVariables.illustratorKeys.Key12 = data.Key12;
                                IllustratorVariables.illustratorKeys.Key16 = 'Synced';
							$scope.IllustrationAttachment = IllustratorVariables.getIllustrationAttachmentModel();
							if (!jQuery.isEmptyObject($scope.IllustrationAttachment.Upload.Signature[0])) {
								var fileContent = $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent;
								if (fileContent.length > 2) {
									var docData = [];
									docData.push(angular.copy($scope.IllustrationAttachment.Upload.Signature[0]));
									var docDetails = {
										Documents : docData[0]
									};
									PersistenceMapping.clearTransactionKeys();
									IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
									PersistenceMapping.dataIdentifyFlag = false;
									var docDetailsWithKeys = PersistenceMapping.mapScopeToPersistence(docDetails);
									DocumentService.uploadDocuments(docDetailsWithKeys, "1", function(data, count) {
										var userDetails = UserDetailsService.getUserDetailsModel();
										UtilityService.emailServiceCall(userDetails,transactionObj, function(){}, errorCallback);
										IllustratorVariables.illustratorKeys.Key18 = false;
										var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, false);
										obj.save(function(isAutoSave) {
											successCallback();
										},function(error){ 
											errorCallback(); 
										});
									});
								} else {
									PersistenceMapping.clearTransactionKeys();
									IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
									PersistenceMapping.dataIdentifyFlag = false;
									var userDetails = UserDetailsService.getUserDetailsModel();
									UtilityService.emailServiceCall(userDetails,transactionObj, function(){}, errorCallback);
									IllustratorVariables.illustratorKeys.Key18 = false;
									var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, false);
									obj.save(function(isAutoSave) {
											successCallback();
									},function(error){ 
											errorCallback(); 
									});
								}    
						}
					},function(error){ 
										errorCallback(); 
					});  
							/*var transactionsToSync = [];
							transactionsToSync.push({
								"Key" : "illustration",
								"Value" : transactionObj.Id,
								"syncType": "indSyncBI"
					 });
							IllustratorService.individualSync($scope, IllustratorVariables, transactionsToSync, function() {
									var userDetails = UserDetailsService.getUserDetailsModel();
									UtilityService.emailServiceCall(userDetails, transactionObj, function(){}, errorCallback);
									IllustratorVariables.illustratorKeys.Key18 = false;
									var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, false);
									obj.save(function(isAutoSave) {
										successCallback();
									},function(error) { 
										errorCallback(); 
									});
							},function(data){$scope.documentError(data);},$scope.syncProgressCallback,$scope.syncModuleProgress);*/
							}
                    	else{
					$scope.Illustration.Email = objectToAdd;
					IllustratorVariables.illustratorKeys.Key18 = true;
					var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, false);
					obj.save(function(isAutoSave) {
						//alert("email placed");
						successCallback();
					},function(error) { 
						errorCallback(); 
					});
				} 
                    } 
                    else
                    {                   	
				PersistenceMapping.clearTransactionKeys();
				IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
				PersistenceMapping.dataIdentifyFlag = false;
				PersistenceMapping.Type = "illustration";
				var transactionObject = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
				transactionObject.Key18 = true;                               
				transactionObject.TransactionData.Email = objectToAdd;
				var transactionObjList = [];
				transactionObjList.push(transactionObject);
				dataService.saveTransactionObjectsMultiple(transactionObjList,function(data) {
					IllustratorVariables.illustratorId = data.Key3;
					IllustratorVariables.illustratorKeys.Key12 = data.Key12;
					IllustratorVariables.illustratorKeys.Key16 = 'Synced';
					$scope.IllustrationAttachment = IllustratorVariables.getIllustrationAttachmentModel();
					if (!jQuery.isEmptyObject($scope.IllustrationAttachment.Upload.Signature[0])) {
						var fileContent = $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent;
						if (fileContent.length > 2) {
							var docData = [];
							docData.push(angular.copy($scope.IllustrationAttachment.Upload.Signature[0]));
							var docDetails = {
								Documents : docData[0]
							};
							PersistenceMapping.clearTransactionKeys();
							IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
							PersistenceMapping.dataIdentifyFlag = false;
							var docDetailsWithKeys = PersistenceMapping.mapScopeToPersistence(docDetails);
							DocumentService.uploadDocuments(docDetailsWithKeys, "1", function(data, count) {
								var userDetails = UserDetailsService.getUserDetailsModel();
										var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
								UtilityService.emailServiceCall(userDetails,transactionObj, function(){}, errorCallback);
								IllustratorVariables.illustratorKeys.Key18 = false;
								var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, false);
								obj.save(function(isAutoSave) {
									successCallback();
								},function(error) { 
									errorCallback(); 
								});
							});  
						} else {
							PersistenceMapping.clearTransactionKeys();
							IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
							PersistenceMapping.dataIdentifyFlag = false;
							var transactionObjects = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
							var userDetails = UserDetailsService.getUserDetailsModel();
							UtilityService.emailServiceCall(userDetails,transactionObjects, function(){}, errorCallback);
							IllustratorVariables.illustratorKeys.Key18 = false;
							var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, false);
							obj.save(function(isAutoSave) {
								successCallback();
							},function(error) { 
								errorCallback(); 
							});
						}
								   
					}
	       				 
				});
			}
            };

			
			IllustratorService.checkExpiryDate = function(illustrationDate)
			{
			var numOfDays 	= rootConfig.numOfDaysForExpiry;		
			var currentDate = new Date();
			var pastDate 	= new Date();		
			illustrationDate = new Date(illustrationDate); 
			pastDate.setDate(currentDate.getDate()- numOfDays);
			
			if (illustrationDate < pastDate)
				IllustratorService.isExpired = true;
			else
				IllustratorService.isExpired = false;
			return IllustratorService.isExpired;
		};
		//Function check whether MI equals any of the AI or AI equals any other Insureds
		IllustratorService.checkMIORAIEquals = function(MIAIArray, successCallback){
			var duplicateKey;
			for(var i = 0; i < MIAIArray.length; i++){
				for(var j = 0; j < MIAIArray.length; j++){
					if(i != j){
						if(MIAIArray[i].idNumber.length > 0 && MIAIArray[j].idNumber.length > 0 && MIAIArray[i].idNumber == MIAIArray[j].idNumber){
								successCallback(true);
								j = MIAIArray.length;
								i = MIAIArray.length;							
						}else if(MIAIArray[i].firstName == MIAIArray[j].firstName && MIAIArray[i].lastName == MIAIArray[j].lastName && MIAIArray[i].dob == MIAIArray[j].dob && MIAIArray[i].gender == MIAIArray[j].gender ){
							successCallback(true);
							j = MIAIArray.length;
							i = MIAIArray.length;
						}
						else{
							duplicateKey = false;
						}
					}
				}
				if(i == MIAIArray.length - 1){
					successCallback(duplicateKey);
				}
			}				
		};
			// Fetching Illustration for PRINT and DOWNLOAD
			IllustratorService.fetchIllustrationsForPDF = function(objsToFetch, dataService, successCallback, errorCallback) {
	                PersistenceMapping.clearTransactionKeys();
	                PersistenceMapping.Type = 'illustration';
	                var searchCriteria = {
	                    searchCriteriaRequest: {
	                        selIds: "",                        
	                        command: ""
	                    }
	                };
	                var selectedIdArray = [];                
	                if ((rootConfig.isDeviceMobile)) {
	                    $.each(objsToFetch, function(i, obj) {
	                        selectedIdArray.push(obj.Id);
	                    });
	                    searchCriteria.searchCriteriaRequest.selectedIds = selectedIdArray;
			
	                } else {
	                    $.each(objsToFetch, function(i, obj) {
	                        selectedIdArray.push(obj.Key3);
	                    });                   
	                    var command = "EmailTriggering";
	                    searchCriteria.searchCriteriaRequest.selectedIds = selectedIdArray;                    
	                    searchCriteria.searchCriteriaRequest.command = command;
	                    // populate key3 (Illustration Id)in case of Web in
	                    // selIds and call service
	                }
	                PersistenceMapping.dataIdentifyFlag = false;
	                var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);	                
	                DataService.retrieveIllustrationForPDF(transactionObj,successCallback, errorCallback);
			
	            };
			
	IllustratorService.saveRequirementSuccess = function(i,$scope){
		i++;
		if(i < IllustratorVariables.illustratorModel.Requirements.length){
			IllustratorService.saveRequirementFiles(i, IllustratorVariables.illustratorModel.Requirements[i].filePath, IllustratorVariables.illustratorModel.Requirements[i].requirementName, IllustratorVariables.illustratorModel.Requirements[i].requirementType, IllustratorVariables.illustratorModel.Requirements[i].description, IllustratorVariables.illustratorModel.Requirements[i].requirementName.split('_')[0],$scope, IllustratorService.saveRequirementSuccess)
		}else{
			$scope.Illustration = IllustratorVariables.getIllustratorModel();
			IllustratorVariables.setIllustratorModel($scope.Illustration);
		}
	};

	IllustratorService.getRequirementFilePath = function(filedata, requirementName, requirementType, documentType, successCallback) {
		if(rootConfig.isLogDebug){
			LoggerService.writeLog(log,"IllustratorService","Inside getRequirementFilePath() method","","");
		}
	    if (rootConfig.isDeviceMobile) {
	            DocumentService.base64AsFile(filedata, function(filePath) {
	                successCallback(filePath);
	            }, function(error) {});
	        }
	};
	
	/* IllustratorService.saveRequirement = function(filePath, requirementType, partyIdentifier, isMandatory, docArray, documentTypes,$scope,
     		$rootScope, dataService, $translate,
     		utilityService, IllustratorVariables, $routeParams,
     		isAutoSave, successCallback, errorCallback) {
		 
		var successBack=successCallback;
		var errorBack=errorCallback;
     	var requirementObj = Requirement();
     	var currentDate = new Date();
     	var timeStamp = currentDate.getTime();
     	var description = "";
     	if(IllustratorVariables.illustratorModel.Requirements){
     		var docIndex = 0
     		requirementObj.requirementType = requirementType;
     		requirementObj.requirementSubType= documentTypes;
     		requirementObj.requirement = requirementType;
     		requirementObj.requirementName = documentTypes+ "_" + timeStamp;
     		requirementObj.partyIdentifier = partyIdentifier;
     		requirementObj.isMandatory = isMandatory;
     		requirementObj.Documents[docIndex].documentName = documentTypes + "_" + requirementType + "_" + timeStamp;
     		requirementObj.Documents[docIndex].documentType = requirementType;
     		requirementObj.Documents[docIndex].documentProofSubmitted = "Signature";
     		requirementObj.Documents[docIndex].documentStatus = "";
     		requirementObj.Documents[docIndex].date = currentDate;
     		IllustratorVariables.illustratorModel.Requirements.push(requirementObj);
     	} 
     	PersistenceMapping.Key15 = IllustratorVariables.illustratorKeys.Key15 != null ? IllustratorVariables.illustratorKeys.Key15 : "InProgress"; 
     	PersistenceMapping.Key3=IllustratorVariables.illustratorId;
     	PersistenceMapping.Type="illustration";
     	IllustratorService.mapKeysforPersistence($scope,IllustratorVariables);
     	var transactionObj = PersistenceMapping.mapScopeToPersistence(IllustratorVariables.illustratorModel);
     	DataService.saveTransactions(transactionObj,function() {
     		IllustratorService.saveRequirementFiles(filePath, requirementObj.requirementName, requirementType, description, documentTypes,$scope, function(){
     			$scope.Illustration = IllustratorVariables.getIllustratorModel();
     			IllustratorVariables.setIllustratorModel($scope.Illustration);
     			successBack();
     			});
     	}, function(error) {});

     };
     
     IllustratorService.saveRequirementFiles = function(filePath, requirementName, requirementType, description, documentType,$scope, successCallback) {

         var requirementObject = CreateRequirementFile();
         var fileName = "Signature";
         var currentDate = new Date();
         var timeStamp = currentDate.getTime();
         requirementObject.RequirementFile.identifier = IllustratorVariables.transTrackingID;
         if (typeof IllustratorVariables.RequirementFile != "undefined" && IllustratorVariables.RequirementFile.length >0 &&
                 documentType == "Signature") {
             requirementObject.RequirementFile.requirementName = IllustratorVariables.RequirementFile[0].requirementName;
             requirementObject.RequirementFile.documentName = IllustratorVariables.RequirementFile[0].documentName;
             requirementObject.RequirementFile.fileName = IllustratorVariables.RequirementFile[0].fileName;
         } else {
             requirementObject.RequirementFile.requirementName = IllustratorVariables.illustratorModel.Requirements[0].requirementName
             requirementObject.RequirementFile.documentName = IllustratorVariables.illustratorModel.Requirements[0].Documents[0].documentName;
             requirementObject.RequirementFile.fileName = documentType + "_" +
                     fileName + "_" + timeStamp + '.JPG';
         }
         for (var reqIndx=0; reqIndx<IllustratorVariables.illustratorModel.Requirements.length; reqIndx++){
         	if (documentType==IllustratorVariables.illustratorModel.Requirements[reqIndx].requirementSubType){
         		requirementObject.RequirementFile.requirementName = IllustratorVariables.illustratorModel.Requirements[reqIndx].requirementName;
                  requirementObject.RequirementFile.documentName = IllustratorVariables.illustratorModel.Requirements[reqIndx].Documents[0].documentName;;
                  requirementObject.RequirementFile.fileName = documentType + "_" + fileName + "_" + timeStamp + '.JPG';
         	}
         }
        
         requirementObject.RequirementFile.status = "Saved"
         if (!eval(rootConfig.isDeviceMobile)) {
             requirementObject.RequirementFile.contentType = "base64";
         }
         requirementObject.RequirementFile.base64string = filePath;
         requirementObject.RequirementFile.description = description;
         if (eval(rootConfig.isDeviceMobile)) {
         	log.info("Before Saving Requirement Files - Illustration TranstrackingID : "+requirementObject.RequirementFile.identifier+
         			" Requirement Name : "+requirementObject.RequirementFile.requirementName+
         			" File Name : "+requirementObject.RequirementFile.fileName+" => BIIndividualSync", "Success");
             DataService.saveRequirementFiles(requirementObject.RequirementFile, function() {
             	log.info("Save Requirement Files - Success => BIIndividualSync", "Success");
                	IllustratorVariables.RequirementFile.push(requirementObject.RequirementFile);
                 successCallback();
             }, function(error) {
             	log.error("Save Requirement Files - Failed => BIIndividualSync", "Failure");
             });
         } else{
     		PersistenceMapping.clearTransactionKeys();
     	    IllustratorVariables.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
     		IllustratorService.mapKeysforPersistence($scope,IllustratorVariables);
     	    var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
     	    DataService.saveRequirementFiles(transactionObj, function() {
     	    	log.info("Save Requirement Files - Success => BIIndividualSync", "Success");
     			IllustratorVariables.RequirementFile.push(requirementObject.RequirementFile);
     			$scope.Illustration= IllustratorVariables.getIllustratorModel();
     			IllustratorVariables.setIllustratorModel($scope.Illustration);	
     			successCallback();
     	    } , function(e){
     	    	log.error("Save Requirement Files - Failed => BIIndividualSync", "Failure");
     			
     		});
     	}
     };
*/
	
	
	
	
IllustratorService.saveRequirement = function(filePath, requirementType, partyIdentifier, isMandatory, docArray, documentTypes,$scope,
            $rootScope, dataService, $translate,
            utilityService, IllustratorVariables, $routeParams,
            isAutoSave, successCallback, errorCallback,requirmentSelect) {
			
var requirementObj = globalService.Requirement();
if(rootConfig.isLogDebug){
	LoggerService.writeLog(log,"IllustratorService","Inside saveRequirement() method",requirementObj,"");
}
var currentDate = new Date();
var timeStamp = currentDate.getTime();
var description = "";
var fileName = "Signature";


if(IllustratorVariables.illustratorModel.Requirements && IllustratorVariables.illustratorModel.Requirements.length == 0){
    var docIndex = 0;
    requirementObj.requirementType = requirementType;
	requirementObj.requirementSubType= documentTypes;
    requirementObj.requirement = requirementType;
    requirementObj.requirementName = requirmentSelect + "_" + requirementType + "_" + timeStamp;
    requirementObj.partyIdentifier = partyIdentifier;
    requirementObj.isMandatory = isMandatory;
    requirementObj.Documents[docIndex].documentName = requirmentSelect + "_" + requirementType + "_" + timeStamp;
    requirementObj.Documents[docIndex].documentType = requirementType;
    requirementObj.Documents[docIndex].documentProofSubmitted = "Signature";
    requirementObj.Documents[docIndex].documentStatus = "";
    requirementObj.Documents[docIndex].date = currentDate;
    requirementObj.Documents[docIndex].pages[0].fileName=documentTypes + "_" + fileName + "_" + timeStamp + '.JPG';
    IllustratorVariables.illustratorModel.Requirements.push(requirementObj);
    PersistenceMapping.Key15 = IllustratorVariables.illustratorKeys.Key15 != null ? IllustratorVariables.illustratorKeys.Key15 : "Inprogress";
    PersistenceMapping.Key3=IllustratorVariables.illustratorId;
    PersistenceMapping.Type="Illustration";
    IllustratorService.mapKeysforPersistence($scope,IllustratorVariables);
    var transactionObj = PersistenceMapping.mapScopeToPersistence(IllustratorVariables.illustratorModel);
	if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
		var key11Data=transactionObj.Key11;
		transactionObj.Key11=IllustratorVariables.agentForGAO;
		transactionObj.Key38=IllustratorVariables.agentNameForGAO;
		transactionObj.Key36=key11Data;
		transactionObj.Key37=UserDetailsService.getAgentRetrieveData().AgentDetails.agentType;
		transactionObj.Key35=UserDetailsService.getAgentRetrieveData().AgentDetails.unit;
	}else{
		transactionObj.Key36=(typeof IllustratorVariables.GAOId !="undefined")?IllustratorVariables.GAOId:"";
		transactionObj.Key37=(typeof IllustratorVariables.agentType!="undefined")?IllustratorVariables.agentType:"";
		transactionObj.Key38=(typeof IllustratorVariables.agentNameForGAO!="undefined")?IllustratorVariables.agentNameForGAO:"";
		transactionObj.Key35=(typeof IllustratorVariables.GAOOfficeCode!="undefined")?IllustratorVariables.GAOOfficeCode:"";
	}
    DataService.saveTransactions(transactionObj,function(data) {
		if(!rootConfig.isDeviceMobile && !rootConfig.isOfflineDesktop)
		IllustratorVariables.illustratorKeys.Key12 = data.Key12;
		IllustratorService.saveRequirementFiles(filePath, requirementObj.requirementName, requirementType, description, documentTypes,$scope,successCallback,errorCallback);
			}, function(error) {
				if(errorCallback){
					errorCallback();
				}
			});
}  else if(IllustratorVariables.illustratorModel.Requirements.length==1){
   	var requirementObj1 = globalService.RequirementAgent();
	var docIndex = 0;
	requirementObj1.requirementType = requirementType;
	requirementObj1.requirementSubType= documentTypes;
	requirementObj1.requirement = requirementType;
	requirementObj1.requirementName = requirmentSelect + "_" + requirementType + "_" + timeStamp;
	requirementObj1.partyIdentifier = partyIdentifier;
	requirementObj1.isMandatory = isMandatory;
	requirementObj1.Documents[docIndex].documentName = requirmentSelect + "_" + requirementType + "_" + timeStamp;
	requirementObj1.Documents[docIndex].documentType = requirementType;
	requirementObj1.Documents[docIndex].documentProofSubmitted = "Signature";
	requirementObj1.Documents[docIndex].documentStatus = "";
	requirementObj1.Documents[docIndex].date = currentDate;
	requirementObj1.Documents[docIndex].pages[0].fileName=documentTypes + "_" + fileName + "_" + timeStamp + '.JPG';
	IllustratorVariables.illustratorModel.Requirements.push(requirementObj1);
	PersistenceMapping.Key15 = IllustratorVariables.illustratorKeys.Key15 != null ? IllustratorVariables.illustratorKeys.Key15 : "Inprogress";
	PersistenceMapping.Key3=IllustratorVariables.illustratorId;
	PersistenceMapping.Type="Illustration";
	IllustratorService.mapKeysforPersistence($scope,IllustratorVariables);
	var transactionObj = PersistenceMapping.mapScopeToPersistence(IllustratorVariables.illustratorModel);
	if(UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeGAO || UserDetailsService.getAgentRetrieveData().AgentDetails.agentType==rootConfig.agentTypeBranchAdmin){
		var key11Data=transactionObj.Key11;
		transactionObj.Key11=IllustratorVariables.agentForGAO;
		transactionObj.Key38=IllustratorVariables.agentNameForGAO;
		transactionObj.Key36=key11Data;
		transactionObj.Key37=UserDetailsService.getAgentRetrieveData().AgentDetails.agentType;
		transactionObj.Key35=UserDetailsService.getAgentRetrieveData().AgentDetails.unit;
	}else{
		transactionObj.Key36=(typeof IllustratorVariables.GAOId !="undefined")?IllustratorVariables.GAOId:"";
		transactionObj.Key37=(typeof IllustratorVariables.agentType!="undefined")?IllustratorVariables.agentType:"";
		transactionObj.Key38=(typeof IllustratorVariables.agentNameForGAO!="undefined")?IllustratorVariables.agentNameForGAO:"";
		transactionObj.Key35=(typeof IllustratorVariables.GAOOfficeCode!="undefined")?IllustratorVariables.GAOOfficeCode:"";
	}
	DataService.saveTransactions(transactionObj,function(data) {
			if(!rootConfig.isDeviceMobile && !rootConfig.isOfflineDesktop)
			IllustratorVariables.illustratorKeys.Key12 = data.Key12;
			IllustratorService.saveRequirementFiles1(filePath, requirementObj1.requirementName, requirementType, description, documentTypes,$scope,successCallback,errorCallback);
				}, function(error) {
					if(errorCallback){
						errorCallback();
					}
				});
} else if(IllustratorVariables.illustratorModel.Requirements.length >1) {
	
	var requirementObj1 = globalService.RequirementAgent();
	var docIndex = 0;
	requirementObj1.requirementType = requirementType;
	requirementObj1.requirement = requirementType;
	requirementObj1.requirementName = requirmentSelect + "_" + requirementType + "_" + timeStamp;
	requirementObj1.partyIdentifier = partyIdentifier;
	requirementObj1.isMandatory = isMandatory;
	requirementObj1.Documents[docIndex].documentName = requirmentSelect + "_" + requirementType + "_" + timeStamp;
	requirementObj1.Documents[docIndex].documentType = requirementType;
	requirementObj1.Documents[docIndex].documentProofSubmitted = "Signature";
	requirementObj1.Documents[docIndex].documentStatus = "";
	requirementObj1.Documents[docIndex].date = currentDate;
	IllustratorService.saveRequirementFiles2(filePath, requirementObj1.requirementName, requirementType, description, documentTypes,$scope,successCallback,errorCallback,requirmentSelect);
}
};


IllustratorService.saveRequirementFiles = function(filePath, requirementName, requirementType, description, documentType,$scope, successCallback,errorCallback,requirmentSelect) {
if(rootConfig.isLogDebug){
	LoggerService.writeLog(log,"IllustratorService","Inside saveRequirementFiles() method","","");
}
var requirementObject = globalService.RequirementFile();
var fileName = "Signature";
var currentDate = new Date();
var timeStamp = currentDate.getTime();
requirementObject.RequirementFile.identifier = IllustratorVariables.transTrackingID;

if (typeof IllustratorVariables.RequirementFile != "undefined" && IllustratorVariables.RequirementFile.length >0 &&
        documentType == "Signature") {
    requirementObject.RequirementFile.requirementName = IllustratorVariables.RequirementFile[0].requirementName;
    requirementObject.RequirementFile.documentName = IllustratorVariables.RequirementFile[0].documentName;
    requirementObject.RequirementFile.fileName = IllustratorVariables.RequirementFile[0].fileName;
} else {
    requirementObject.RequirementFile.requirementName = IllustratorVariables.illustratorModel.Requirements[0].requirementName;
    requirementObject.RequirementFile.documentName = IllustratorVariables.illustratorModel.Requirements[0].Documents[0].documentName;
    requirementObject.RequirementFile.fileName =  documentType + "_" + fileName + "_" + timeStamp + '.JPG';
}
requirementObject.RequirementFile.status = "Saved";
if (!(rootConfig.isDeviceMobile)) {
    requirementObject.RequirementFile.contentType = "base64";
}
requirementObject.RequirementFile.base64string = filePath;
requirementObject.RequirementFile.description = description;
if (rootConfig.isDeviceMobile) {
    DataService.saveRequirementFiles(requirementObject.RequirementFile, function() {
       	IllustratorVariables.RequirementFile.push(requirementObject.RequirementFile);
        $scope.Illustration = IllustratorVariables.getIllustratorModel();
        IllustratorVariables.setIllustratorModel($scope.Illustration);
        if(successCallback){
        	successCallback();
		}
    }, function(error) {
    	if(errorCallback){
    		errorCallback();
		}
    });
} else{
	PersistenceMapping.clearTransactionKeys();
    IllustratorVariables.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
	IllustratorService.mapKeysforPersistence($scope,IllustratorVariables);
    var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
    DataService.saveRequirementFiles(transactionObj, function() {
		IllustratorVariables.RequirementFile.push(requirementObject.RequirementFile);
		$scope.Illustration= IllustratorVariables.getIllustratorModel();
		IllustratorVariables.setIllustratorModel($scope.Illustration);
		if(successCallback){
			successCallback();
		}
    } , function(e){
    	if(errorCallback){
    		errorCallback();
		}
		
	});
}
};



IllustratorService.saveRequirementFiles1 = function(filePath, requirementName, requirementType, description, documentType,$scope, successCallback,errorCallback) {
	if(rootConfig.isLogDebug){
		LoggerService.writeLog(log,"IllustratorService","Inside saveRequirementFiles() method","","");
	}
	var requirementObjectAgt = globalService.RequirementFile();
	var fileName = "Signature";
	var currentDate = new Date();
	var timeStamp = currentDate.getTime();
	requirementObjectAgt.RequirementFile.identifier = IllustratorVariables.transTrackingID;

	if (typeof IllustratorVariables.RequirementFile != "undefined" && IllustratorVariables.RequirementFile.length >0 &&
	        documentType == "Signature") {
		requirementObjectAgt.RequirementFile.requirementName = IllustratorVariables.RequirementFile[0].requirementName;
		requirementObjectAgt.RequirementFile.documentName = IllustratorVariables.RequirementFile[0].documentName;
		requirementObjectAgt.RequirementFile.fileName = IllustratorVariables.RequirementFile[0].fileName;
	}else {
		//var indexFile = IllustratorVariables.illustratorModel.Requirements.length-1;
		requirementObjectAgt.RequirementFile.requirementName = IllustratorVariables.illustratorModel.Requirements[1].requirementName;
		requirementObjectAgt.RequirementFile.documentName = IllustratorVariables.illustratorModel.Requirements[1].Documents[0].documentName;
		requirementObjectAgt.RequirementFile.fileName = documentType + "_" + fileName + "_" + timeStamp + '.JPG';
	}
	requirementObjectAgt.RequirementFile.status = "Saved";
	if (!(rootConfig.isDeviceMobile)) {
		requirementObjectAgt.RequirementFile.contentType = "base64";
	}
	requirementObjectAgt.RequirementFile.base64string = filePath;
	requirementObjectAgt.RequirementFile.description = description;
	if (rootConfig.isDeviceMobile) {
	    DataService.saveRequirementFiles(requirementObjectAgt.RequirementFile, function() {
	       	IllustratorVariables.RequirementFile.push(requirementObjectAgt.RequirementFile);
	        $scope.Illustration = IllustratorVariables.getIllustratorModel();
	        IllustratorVariables.setIllustratorModel($scope.Illustration);
	        if(successCallback){
	        	successCallback();
			}
	    }, function(error) {
	    	if(errorCallback){
	    		errorCallback();
			}
	    });
	} else{
		PersistenceMapping.clearTransactionKeys();
	    IllustratorVariables.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
		IllustratorService.mapKeysforPersistence($scope,IllustratorVariables);
	    var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObjectAgt);
	    DataService.saveRequirementFiles(transactionObj, function() {
			IllustratorVariables.RequirementFile.push(requirementObjectAgt.RequirementFile);
			$scope.Illustration= IllustratorVariables.getIllustratorModel();
			IllustratorVariables.setIllustratorModel($scope.Illustration);
			if(successCallback){
				successCallback();
			}
	    } , function(e){
	    	if(errorCallback){
	    		errorCallback();
			}
			
		});
	}
	}; 
	
	IllustratorService.saveRequirementFiles2 = function(filePath, requirementName, requirementType, description, documentType,$scope, successCallback,errorCallback,requirmentSelect) {
		if(rootConfig.isLogDebug){
			LoggerService.writeLog(log,"IllustratorService","Inside saveRequirementFiles() method","","");
		}
		var requirementObjectAgt = globalService.RequirementFile();
		var fileName = "Signature";
		var currentDate = new Date();
		var timeStamp = currentDate.getTime();
		requirementObjectAgt.RequirementFile.identifier = IllustratorVariables.transTrackingID;

		/*for(i=0;i<IllustratorVariables.illustratorModel.Requirements.length;i++) {
			var protoType = IllustratorVariables.illustratorModel.Requirements[i].requirementName;
			var typeService = protoType.split("_")[0];
			if(typeService == requirmentSelect )
				index =i;
		}*/
		
		 for (var reqIndx=0; reqIndx<IllustratorVariables.illustratorModel.Requirements.length; reqIndx++){
         	if (documentType==IllustratorVariables.illustratorModel.Requirements[reqIndx].requirementSubType){
         		requirementObjectAgt.RequirementFile.requirementName = IllustratorVariables.illustratorModel.Requirements[reqIndx].requirementName;
         		requirementObjectAgt.RequirementFile.documentName = IllustratorVariables.illustratorModel.Requirements[reqIndx].Documents[0].documentName;
         		//requirementObjectAgt.RequirementFile.fileName = documentType + "_" + fileName + "_" + timeStamp + '.JPG';
         		if(IllustratorVariables.illustratorModel.Requirements[reqIndx].Documents[0].pages!==undefined){
                    requirementObjectAgt.RequirementFile.fileName = IllustratorVariables.illustratorModel.Requirements[reqIndx].Documents[0].pages[0].fileName;
				}
	     	}
         }
		
		
		
		requirementObjectAgt.RequirementFile.status = "Saved";
		if (!(rootConfig.isDeviceMobile)) {
			requirementObjectAgt.RequirementFile.contentType = "base64";
		}
		requirementObjectAgt.RequirementFile.base64string = filePath;
		requirementObjectAgt.RequirementFile.description = description;
		if (rootConfig.isDeviceMobile) {
		    DataService.saveRequirementFiles(requirementObjectAgt.RequirementFile, function() {
		    	/*	IllustratorVariables.RequirementFile.push(requirementObjectAgt.RequirementFile);
		        $scope.Illustration = IllustratorVariables.getIllustratorModel();
		        IllustratorVariables.setIllustratorModel($scope.Illustration);
		       */ if(successCallback){
		        	successCallback();
				}
		    }, function(error) {
		    	if(errorCallback){
		    		errorCallback();
				}
		    });
		} else{
			PersistenceMapping.clearTransactionKeys();
		    IllustratorVariables.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
			IllustratorService.mapKeysforPersistence($scope,IllustratorVariables);
		    var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObjectAgt);
		    DataService.saveRequirementFiles(transactionObj, function() {
				IllustratorVariables.RequirementFile.push(requirementObjectAgt.RequirementFile);
				
				if(successCallback){
					successCallback();
				}
		    } , function(e){
		    	if(errorCallback){
		    		errorCallback();
				}
				
			});
		}
		}; 	

IllustratorService.individualSync = function($scope, IllustratorVariables, transactionsToSync, successCallback, errorCallback,syncProgressCallback,syncModuleProgress) {
    PersistenceMapping.clearTransactionKeys();
    IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
    var syncConfig = [];
    if( transactionsToSync && transactionsToSync.length > 0){
        for ( var i = 0; i <  transactionsToSync.length ; i++){
            var objsToSync = [];
            objsToSync.push(transactionsToSync[i].Value);
            var transactionObj = {
                "module": transactionsToSync[i].Key,
                "operation": "syncTo",
                "selectedIds": objsToSync,
				"syncType":"indSyncBI"
            }
            syncConfig.push(transactionObj);
        }
    }
    //call selected sync                                                                                
    SyncService.initialize(syncConfig, function(syncFailed, syncWarning, syncDocFailed, syncDocDownloadFailed,syncModuleProgress) {
        if(!syncFailed && !syncWarning && !syncDocFailed && !syncDocDownloadFailed ) {
            successCallback();
        } else {
            if(syncDocFailed || syncDocDownloadFailed){
                errorCallback("docUploadFailed");
            }else{
                errorCallback();                                
            }
        }
    }, function(syncMessage,progress){
        syncModuleProgress(syncMessage,progress);
    }, function(data,type){
        //Sync From
        SyncService.runSyncThread();
    }, function(data,type){
        var statusCode = "";
        if(data.serverAnswer && data.serverAnswer.Response && data.serverAnswer.Response.ResponsePayload && data.serverAnswer.Response.ResponsePayload.Transactions){
		
		
			if(data.serverAnswer.Response.ResponsePayload.Transactions.length > 0){
            	statusCode = data.serverAnswer.Response.ResponsePayload.Transactions[0].Key16 ;
                if( statusCode == "SUCCESS"){                          
                    //Sync To
                	successCallback();
                    //SyncService.runSyncThread();
                }else{
                    errorCallback( data.serverAnswer.Response.ResponsePayload.Transactions[0] );
                }
            } else {
            	successCallback();
            } 
           // statusCode = data.serverAnswer.Response.ResponsePayload.Transactions[0].Key16 ;
            //if( statusCode == "SUCCESS"){                          
                //Sync To
            //	successCallback();
                //SyncService.runSyncThread();
           // }else{
           //     errorCallback( data.serverAnswer.Response.ResponsePayload.Transactions[0] );
           // }
			//successCallback();
        } else {
            errorCallback();
        }                             
    },syncProgressCallback);          
    SyncService.runSyncThread();
};


IllustratorService.deleteRequirementFiles = function(tableName,data,Signature,successCallback,errorCallback) {
	if((rootConfig.isDeviceMobile)){
		DataService.deleteRequirementFiles(tableName,data,Signature,function(sig){
		//	successCallback(sig);
		});
	}
};
	

IllustratorService.getSignature = function(data,Signature,$scope,successCallback) {
	if((rootConfig.isDeviceMobile)){
		DataService.getSignature(data,Signature,function(sigdata){
			successCallback(sigdata);
		});
	}
	else{
		var requirementObject = RequirementFile();
		requirementObject.RequirementFile.identifier = data.transTrackingID;
		requirementObject.RequirementFile.requirementName = data.illustratorModel.Requirements[0].requirementName;
		requirementObject.RequirementFile.documentName = data.illustratorModel.Requirements[0].Documents[0].documentName;
		if(data.illustratorModel.Requirements[0].Documents[0].pages[0].fileName !=undefined){
		requirementObject.RequirementFile.fileName=data.illustratorModel.Requirements[0].Documents[0].pages[0].fileName;
		}
		requirementObject.RequirementFile.status="Saved";
		PersistenceMapping.clearTransactionKeys();
		data.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
		IllustratorService.mapKeysforPersistence($scope,data);
		var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
		DataService.getDocumentFileForRequirement(transactionObj,function(sigdata){
			IllustratorVariables.RequirementFile =sigdata;
			successCallback(sigdata);
		});
	}	
};


IllustratorService.getSignature1 = function(data,Signature,$scope,successCallback) {
	if((rootConfig.isDeviceMobile)){
		DataService.getSignature(data,Signature,function(sigdata){
			successCallback(sigdata);
		});
	}
	else{
		var requirementObject = RequirementFile();
		requirementObject.RequirementFile.identifier = data.transTrackingID;
		requirementObject.RequirementFile.requirementName = data.illustratorModel.Requirements[1].requirementName;
		requirementObject.RequirementFile.documentName = data.illustratorModel.Requirements[1].Documents[0].documentName;
		if(data.illustratorModel.Requirements[1].Documents[0].pages[0].fileName !=undefined){
		requirementObject.RequirementFile.fileName=data.illustratorModel.Requirements[1].Documents[0].pages[0].fileName;
		}
		requirementObject.RequirementFile.status="Saved";
		PersistenceMapping.clearTransactionKeys();
		data.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
		IllustratorService.mapKeysforPersistence($scope,data);
		var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
		DataService.getDocumentFileForRequirement(transactionObj,function(sigdata){
			IllustratorVariables.RequirementFile =sigdata;
			successCallback(sigdata);
		});
	}	
};

/*IllustratorService.getSignature = function(data,Signature,$scope,successCallback) {
	if(eval(rootConfig.isDeviceMobile)){
		DataService.getSignature(data,Signature,function(sigdata){
			successCallback(sigdata);
		});
	}
	else{
		IllustratorVariables.RequirementFile=[];
		var requirementObject = CreateRequirementFile();
		requirementObject.RequirementFile.identifier = data.transTrackingID;
		requirementObject.RequirementFile.requirementName = data.illustratorModel.Requirements[0].requirementName;
		requirementObject.RequirementFile.documentName = data.illustratorModel.Requirements[0].Documents[0].documentName;
		requirementObject.RequirementFile.fileName=data.illustratorModel.Requirements[0].Documents[0].pages[0].fileName;
		requirementObject.RequirementFile.status="Saved";
		PersistenceMapping.clearTransactionKeys();
		data.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
		IllustratorService.mapKeysforPersistence($scope,data);
		var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);

		DataService.getDocumentFileForRequirement(transactionObj,function(sigdata){
			IllustratorVariables.RequirementFile.push(sigdata);
			
			
			var requirementObjectNew = CreateRequirementFile();
			requirementObjectNew.RequirementFile.identifier = data.transTrackingID;
			requirementObjectNew.RequirementFile.requirementName = data.illustratorModel.Requirements[1].requirementName!=null?data.illustratorModel.Requirements[1].requirementName:"";
			requirementObjectNew.RequirementFile.documentName = data.illustratorModel.Requirements[1].Documents[0].documentName!=null?data.illustratorModel.Requirements[1].Documents[0].documentName:"";
			requirementObjectNew.RequirementFile.fileName=data.illustratorModel.Requirements[1].Documents[0].pages[0].fileName!=null?data.illustratorModel.Requirements[1].Documents[0].pages[0].fileName:"";
    		requirementObjectNew.RequirementFile.status="Saved";
    		PersistenceMapping.clearTransactionKeys();
    		data.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
    		IllustratorService.mapKeysforPersistence($scope,data);
    		var transactionObjNew = PersistenceMapping.mapScopeToPersistence(requirementObjectNew);
    		
    		DataService.getDocumentFileForRequirement(transactionObjNew,function(sigdataNew){
    			IllustratorVariables.RequirementFile.push(sigdataNew);
    			successCallback(IllustratorVariables.RequirementFile);
    		});
		});
	}	
};
*/

IllustratorService.convertImgToBase64=function(url, callback, outputFormat){

	var img = new Image();

	img.crossOrigin = 'Anonymous';
	img.onload = function(){
		var canvas = document.createElement('CANVAS'),
		ctx = canvas.getContext('2d'), dataURL;
		canvas.height = this.height;
		canvas.width = this.width;
		ctx.drawImage(this, 0, 0);
		dataURL = canvas.toDataURL(outputFormat);
		callback(dataURL);
		canvas = null; 
	};
	img.src = url;

}


var proposalData;
IllustratorService.setSelectedProposalData = function (data) {
    proposalData = {};
    if (data && data.length) {
        proposalData = data[0];
    }
}

IllustratorService.getSelectedProposalData = function () {
    var parsedData;
    if(proposalData && proposalData.TransactionData) {
       parsedData = JSON.parse(unescape(proposalData.TransactionData)); 
    }
     
   return  parsedData;
}
	return IllustratorService;
	  
}]);