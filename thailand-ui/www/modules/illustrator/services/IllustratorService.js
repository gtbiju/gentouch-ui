angular
    .module('lifeEngage.IllustratorService', [])
    .factory(
        "IllustratorService",['$http', '$location', '$translate', 'UtilityService',
            'PersistenceMapping', 'DataService', 'SyncService',
            'UserDetailsService', 'DocumentService', 'IllustratorVariables',
        function($http, $location, $translate, UtilityService,
            PersistenceMapping, DataService, SyncService,
            UserDetailsService, DocumentService,IllustratorVariables) {
            var IllustratorService = new Object();
            IllustratorService.mapKeysforPersistence = function($scope,
                IllustratorVariables) {
                // Since in persistancemapping, fnaId is set from
                // fnavariables
                PersistenceMapping.Key1 = (IllustratorVariables.leadId != null && IllustratorVariables.leadId != 0) ? IllustratorVariables.leadId : "";
                PersistenceMapping.Key2 = (IllustratorVariables.fnaId != null && IllustratorVariables.fnaId != 0) ? IllustratorVariables.fnaId : "";
                PersistenceMapping.Key3 = (IllustratorVariables.illustratorId != "" && IllustratorVariables.illustratorId != 0) ? IllustratorVariables.illustratorId : "";
                PersistenceMapping.Key4 = ($scope.proposalId != null && $scope.proposalId != 0) ? $scope.proposalId : "";
                PersistenceMapping.Key5 = $scope.productId != null ? $scope.productId : "1";
                PersistenceMapping.Key6 = $scope.Illustration.Insured.BasicDetails.firstName;
                PersistenceMapping.Key8 = ($scope.Illustration.Insured.ContactDetails.mobileNumber1) ? ($scope.Illustration.Insured.ContactDetails.mobileNumber1) : "";
                PersistenceMapping.Key10 = "FullDetails";
                if (!(rootConfig.isDeviceMobile)) {
                    if (IllustratorVariables.illustratorId == null || IllustratorVariables.illustratorId == 0) {
                        PersistenceMapping.creationDate = UtilityService
                            .getFormattedDateTime();
                        IllustratorVariables.illustrationCreationDate = UtilityService
                            .getFormattedDateTime();
						IllustratorVariables.illustratorKeys.creationDate=UtilityService
                            .getFormattedDateTime();
                    } else if (IllustratorVariables.illustrationCreationDate) {
                        PersistenceMapping.creationDate = IllustratorVariables.illustratorKeys.creationDate;
                    }
                }else{
					if($rootScope.transactionId==null || typeof $rootScope.transactionId=='undefined' || $rootScope.transactionId.length==0 || $rootScope.transactionId=="0"){
						PersistenceMapping.creationDate = UtilityService
                            .getFormattedDateTime();
						IllustratorVariables.illustratorKeys.creationDate=UtilityService
                            .getFormattedDateTime();
					}else{
						PersistenceMapping.creationDate = IllustratorVariables.illustratorKeys.creationDate;
                	}
				}
                PersistenceMapping.Key12 = IllustratorVariables.illustratorKeys.Key12;
                PersistenceMapping.Key15 = $scope.status != null ? $scope.status : IllustratorVariables.illustratorKeys.Key15 != "" ? IllustratorVariables.illustratorKeys.Key15 : IllustratorVariables
                    .getStatusOptions()[0].status;
                PersistenceMapping.Type = "illustration";
                PersistenceMapping.Key16 = $scope.syncStatus != null ? $scope.syncStatus : "";
                PersistenceMapping.Key19 = $scope.Illustration.Product.ProductDetails.productName;
                if (IllustratorVariables.transTrackingID == null || IllustratorVariables.transTrackingID == 0) {
                    PersistenceMapping.TransTrackingID = UtilityService
                        .getTransTrackingID();
                    IllustratorVariables.transTrackingID = PersistenceMapping.TransTrackingID;
                } else {
                    PersistenceMapping.TransTrackingID = IllustratorVariables.transTrackingID;
                }
            };
			IllustratorService.getSignature = function(data,Signature,$scope,successCallback) {
				if(eval(rootConfig.isDeviceMobile)){
					DataService.getSignature(data,Signature,function(sigdata){
						successCallback(sigdata);
					});
				}
				else{
					var requirementObject = RequirementFile();
					requirementObject.RequirementFile.identifier = data.transTrackingID;
					requirementObject.RequirementFile.requirementName = data.illustratorModel.Requirements[0].requirementName;
					requirementObject.RequirementFile.documentName = data.illustratorModel.Requirements[0].Documents[0].documentName;
					requirementObject.RequirementFile.fileName=data.illustratorModel.Requirements[0].Documents[0].pages[0].fileName;
					requirementObject.RequirementFile.status="Saved";
					PersistenceMapping.clearTransactionKeys();
					data.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
					IllustratorService.mapKeysforPersistence($scope,data);
					var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
		 
					DataService.getDocumentFileForRequirement(transactionObj,function(sigdata){
						IllustratorVariables.RequirementFile =sigdata;
						successCallback(sigdata);
					});
				}	
			};
			IllustratorService.convertImgToBase64=function(url, callback, outputFormat){
	
				var img = new Image();
		
				img.crossOrigin = 'Anonymous';
				img.onload = function(){
					var canvas = document.createElement('CANVAS'),
					ctx = canvas.getContext('2d'), dataURL;
					canvas.height = this.height;
					canvas.width = this.width;
					ctx.drawImage(this, 0, 0);
					dataURL = canvas.toDataURL(outputFormat);
					callback(dataURL);
					canvas = null; 
				};
				img.src = url;
	
			}
            IllustratorService.saveTransactions = function($scope,
                $rootScope, dataService, $translate,
                utilityService, IllustratorVariables, $routeParams,
                isAutoSave) {
                this.save = function(successCallback, errorCallback) {
                    // - On editing a transaction after synching, the
                    // syncStatus will be saved as �NotSynced�
                    if ((rootConfig.isDeviceMobile) && !(IllustratorVariables.illustratorId == null || IllustratorVariables.illustratorId == 0)) {
                        $scope.syncStatus = "NotSynced";
                    }
                    PersistenceMapping.clearTransactionKeys();
                    IllustratorService.mapKeysforPersistence($scope,
                        IllustratorVariables);
                    var transactionObj = PersistenceMapping
                        .mapScopeToPersistence($scope.Illustration);

                    $scope.onSaveSuccess = function(data) {
                        if ((rootConfig.isDeviceMobile)) {
                            if (data != null) {
                                $rootScope.transactionId = ($rootScope.transactionId == null || $rootScope.transactionId == 0) ? data : $rootScope.transactionId;
                            }
                            //$scope.status = "Final";
                        } else {
                            IllustratorVariables.illustratorId = data.Key3;
                            $scope.syncStatus = "Synced";
                        }
                        $routeParams.transactionId = $rootScope.transactionId;
                        $routeParams.illustrationId = IllustratorVariables.illustratorId;
                        if ((successCallback && typeof successCallback == "function")) {
                            successCallback(isAutoSave);
                        }

                        $scope.refresh();
                    };
                    $scope.onSaveError = function(data) {
                        if (!isAutoSave) {
                            $rootScope.NotifyMessages(true, data);
                        }
                        $scope.refresh();
                        $rootScope.showHideLoadingImage(false);

                        if ((errorCallback && typeof errorCallback == "function")) {
                            errorCallback();
                        }
                    };
                    dataService.saveTransactions(transactionObj,
                        $scope.onSaveSuccess, $scope.onSaveError);
                };

            };

            IllustratorService.initializeIllustratorScopeVariables = function(
                $scope, $rootScope, $routeParams,
                IllustratorVariables) {
                $scope.productId = $routeParams.productId;
                $rootScope.transactionId = $routeParams.transactionId;
                IllustratorVariables.illustratorId = ($routeParams.illustrationId && $routeParams.illustrationId != 0) ? $routeParams.illustrationId : IllustratorVariables.illustratorId;
                $scope.errorCount = '';
                $scope.errorMessages = [];
                $scope.isPopupDisplayed = false;
                $scope.namePattern = rootConfig.namePattern;
                $scope.panPattern = rootConfig.panPattern;
                $scope.zipPattern = rootConfig.zipPattern;
                $scope.adharPattern = rootConfig.adharPattern;
                $scope.emailPattern = rootConfig.emailPattern;
                $scope.sharePattern = rootConfig.sharePattern;
                $scope.mobnumberPattern = rootConfig.mobnumberPattern;
                $scope.numberPattern = rootConfig.numberPattern;
                $scope.agePattern = rootConfig.agePattern;
                $rootScope.enableMyIllustrationTab = false;
                $scope.tableChartView;
                $scope.isValidationRuleExecuted = false;
                $scope.showHeaderImages = false;
            };

            IllustratorService.cloneListing = function(index, $scope,
                proposal) {
                DataService.getListingDetail(proposal, function(data) {
                    $scope.onListingSuccess(index, data);
                }, $scope.onListingError);
            };

            IllustratorService.cloneSave = function($scope, traData,
                successCallBack) {
                DataService.saveTransactions(traData, successCallBack,
                    $scope.onSaveError);
            };

            // Added for full details fecth of selected objects in Email
            // BI
            IllustratorService.fetchIllustrationsForEmail = function(
                objsToFetch, dataService, successCallback,
                errorCallback) {
                PersistenceMapping.clearTransactionKeys();
                PersistenceMapping.Type = 'illustration';
                var searchCriteria = {
                    searchCriteriaRequest: {
                        selIds: "",                        
                        command: ""
                    }
                };
                var selectedIdArray = [];                
                if ((rootConfig.isDeviceMobile)) {
                    $.each(objsToFetch, function(i, obj) {
                        selectedIdArray.push(obj.Id);
                    });
                    searchCriteria.searchCriteriaRequest.selectedIds = selectedIdArray;

                } else {
                    $.each(objsToFetch, function(i, obj) {
                        selectedIdArray.push(obj.Key3);
                    });                   
                    var command = "EmailTriggering";
                    searchCriteria.searchCriteriaRequest.selectedIds = selectedIdArray;                    
                    searchCriteria.searchCriteriaRequest.command = command;
                    // populate key3 (Illustration Id)in case of Web in
                    // selIds and call service
                }
                PersistenceMapping.dataIdentifyFlag = false;
                var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
                dataService.fetchTransactionForIds(transactionObj,successCallback, errorCallback);

            };

            // Added for setting an additional object to transaction
            // object - Email BI
            IllustratorService.setEmailBIflagAndSaveTransactions = function(
                objectToAdd, transactionObjList, dataService,
                successCallback, errorCallback) {
                var objsToSync = []; // used in case of user is online
                // and flag online_emailBI is set
                // true
                $.each(transactionObjList, function(i, obj) {
                    if (obj) {
                        obj.Key18 = true;
                        obj.TransactionData.Email = objectToAdd;
						if ((rootConfig.isDeviceMobile)){
                            obj.TransactionData = angular.toJson(obj.TransactionData);
                             // If online mail sending needed keeping list of
                             // ids for selected sync
                            objsToSync.push(obj.Id);
						}
						else {
						    objsToSync.push(obj.Key3);
						}
                    }
                });
                dataService
                    .saveTransactionObjectsMultiple(
                        transactionObjList,
                        function() {
                            if (rootConfig.online_emailBI) {
                                var syncConfig = [{
                                    "module": "illustration",
                                    "operation": "syncTo",
                                    "selectedIds": objsToSync
                                }];
                                if ((rootConfig.isDeviceMobile) && checkConnection()) {
                                    if (objsToSync.length > 0) {
                                        // call selected sync
                                        SyncService
                                            .initialize(
                                                syncConfig,
                                                function() {
                                                    // Call
                                                    // Email
                                                    // Service
                                                    // to
                                                    // mail
                                                    // BI.
                                                    PersistenceMapping
                                                        .clearTransactionKeys();
                                                    var transactionObjForMailReq = PersistenceMapping
                                                        .mapScopeToPersistence({});
                                                    transactionObjForMailReq.Type = "illustration";
                                                    var userDetails = UserDetailsService
                                                        .getUserDetailsModel();
                                                    UtilityService
                                                        .emailServiceCall(
                                                            userDetails,
                                                            transactionObjForMailReq);
                                                    // Email
                                                    // Service
                                                    // to
                                                    // mail
                                                    // BI
                                                    // ends.
                                                    successCallback(true);
                                                },
                                                function() {
                                                    errorCallback;
                                                });
                                        SyncService
                                            .runSyncThread();
                                    }
                                } else if ((rootConfig.isDeviceMobile) && !checkConnection()) {
                                    successCallback();
                                } else { //WEB
								    PersistenceMapping.clearTransactionKeys();
                                    var transactionObjForMailReq = PersistenceMapping.mapScopeToPersistence({});
                                    transactionObjForMailReq.Type = "illustration";
                                    var userDetails = UserDetailsService.getUserDetailsModel();
                                    UtilityService.emailServiceCall(userDetails,transactionObjForMailReq);
                                    successCallback(true);
                                }

                            } else {
                                successCallback();
                            }
                        }, errorCallback);
            };

            IllustratorService.mapKeysforPersistenceCloning = function(
                data) {
                PersistenceMapping.Key3 = "";
                PersistenceMapping.Type = "illustration";
                PersistenceMapping.Key5 = data[0].Key5;
                PersistenceMapping.Key15 = data[0].Key15;
                PersistenceMapping.Key16 = "";
                PersistenceMapping.Key19 = data[0].Key19;
                PersistenceMapping.TransTrackingID = UtilityService
                    .getTransTrackingID();
            };
			IllustratorService.getRequirementFilePath = function(filedata, requirementName, requirementType, documentType, successCallback) {
    if (eval(rootConfig.isDeviceMobile)) {
       
            DocumentService.base64AsFile(filedata, function(filePath) {
                successCallback(filePath);
            }, function(error) {});

       
        }
  
}
IllustratorService.saveRequirement = function(filePath, requirementType, partyIdentifier, isMandatory, docArray, documentTypes,$scope,
                $rootScope, dataService, $translate,
                utilityService, IllustratorVariables, $routeParams,
                isAutoSave, successCallback, errorCallback) {
    var requirementObj = Requirement();
    var currentDate = new Date();
    var timeStamp = currentDate.getTime();
    var description = "";
    if(IllustratorVariables.illustratorModel.Requirements && IllustratorVariables.illustratorModel.Requirements.length == 0){
        var docIndex = 0
        requirementObj.requirementType = requirementType;
        requirementObj.requirement = requirementType;
        requirementObj.requirementName = "Agent1" + "_" + requirementType + "_" + timeStamp;
        requirementObj.partyIdentifier = partyIdentifier;
        requirementObj.isMandatory = isMandatory;
        requirementObj.Documents[docIndex].documentName = "Agent1" + "_" + requirementType + "_" + requirementType + "_" + timeStamp;
        //requirementObj.Documents[docIndex].document = "Signature";
        requirementObj.Documents[docIndex].documentType = requirementType;
        requirementObj.Documents[docIndex].documentProofSubmitted = "Signature";
        requirementObj.Documents[docIndex].documentStatus = "";
        requirementObj.Documents[docIndex].date = currentDate;

        IllustratorVariables.illustratorModel.Requirements.push(requirementObj);
    }  else if(IllustratorVariables.illustratorModel.Requirements.length >0){
        
        requirementObj = IllustratorVariables.illustratorModel.Requirements[0];
    }
    
        PersistenceMapping.Key15 = IllustratorVariables.illustratorKeys.Key15 != null ? IllustratorVariables.illustratorKeys.Key15 : "InProgress"; 
		PersistenceMapping.Key3=IllustratorVariables.illustratorId;
		PersistenceMapping.Type="illustration";
		IllustratorService.mapKeysforPersistence($scope,IllustratorVariables);
        var transactionObj = PersistenceMapping
                    .mapScopeToPersistence(IllustratorVariables.illustratorModel);
        DataService.saveTransactions(transactionObj,function() {
        IllustratorService.saveRequirementFiles(filePath, requirementObj.requirementName, requirementType, description, documentTypes,$scope, function(){	$scope.Illustration = IllustratorVariables.getIllustratorModel();
        IllustratorVariables.setIllustratorModel($scope.Illustration);});
    			}, function(error) {});

}
IllustratorService.saveRequirementFiles = function(filePath, requirementName, requirementType, description, documentType,$scope, successCallback) {

    var requirementObject = RequirementFile();
    var fileName = "Signature";
    var currentDate = new Date();
    var timeStamp = currentDate.getTime();
    requirementObject.RequirementFile.identifier = IllustratorVariables.transTrackingID;

    if (typeof IllustratorVariables.RequirementFile != "undefined" && IllustratorVariables.RequirementFile.length >0 &&
            documentType == "Signature") {
        requirementObject.RequirementFile.requirementName = IllustratorVariables.RequirementFile[0].requirementName;
        requirementObject.RequirementFile.documentName = IllustratorVariables.RequirementFile[0].documentName;
        requirementObject.RequirementFile.fileName = IllustratorVariables.RequirementFile[0].fileName;
    } else {
        requirementObject.RequirementFile.requirementName = IllustratorVariables.illustratorModel.Requirements[0].requirementName
        requirementObject.RequirementFile.documentName = IllustratorVariables.illustratorModel.Requirements[0].Documents[0].documentName;
        requirementObject.RequirementFile.fileName = "Agent1" + "_" + requirementType + "_" + documentType + "_" +
                fileName + "_" + timeStamp + '.JPG';
    }
   
    requirementObject.RequirementFile.status = "Saved"
    if (!eval(rootConfig.isDeviceMobile)) {
        requirementObject.RequirementFile.contentType = "base64";
    }
    requirementObject.RequirementFile.base64string = filePath;
    requirementObject.RequirementFile.description = description;
    if (eval(rootConfig.isDeviceMobile)) {
        DataService.saveRequirementFiles(requirementObject.RequirementFile, function() {
           	IllustratorVariables.RequirementFile.push(requirementObject.RequirementFile);
            successCallback();
        }, function(error) {});
    } else{
		PersistenceMapping.clearTransactionKeys();
	    IllustratorVariables.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
		IllustratorService.mapKeysforPersistence($scope,IllustratorVariables);
	    var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
	    DataService.saveRequirementFiles(transactionObj, function() {
			IllustratorVariables.RequirementFile.push(requirementObject.RequirementFile);
			$scope.Illustration= IllustratorVariables.getIllustratorModel();
			IllustratorVariables.setIllustratorModel($scope.Illustration);						               
	    } , function(e){
			
		});
	}
}
            return IllustratorService;

        }]);