﻿/*
 * Copyright 2015, LifeEngage 
 */
﻿/*
	 * Name:ProductsController CreatedDate:6/2/2014 Description:Displays all
	 * active products and takes user to product specific Illustration Pages.
	 */
storeApp.controller('ProductsController',['$rootScope','$scope','$location','$translate','$routeParams','IllustratorVariables','ProductService','FnaVariables','UtilityService','globalService',function($rootScope, $scope, $location, $translate, $routeParams, IllustratorVariables , ProductService,FnaVariables, UtilityService,globalService) {
if($routeParams.isProductListing == "true"){
$rootScope.moduleHeader ="productListing";
}else{
$rootScope.moduleHeader ="illustrator.Illustrator";
}
$rootScope.moduleVariable = translateMessages($translate,
$rootScope.moduleHeader);

var allproductsList;
$rootScope.showHideLoadingImage(false);
// $scope.Illustration = IllustratorVariables.getIllustratorModel();
IllustratorVariables.illustratorId = ($routeParams.illustrationId && $routeParams.illustrationId != 0) ? $routeParams.illustrationId : IllustratorVariables.illustratorId;
$rootScope.transactionId = $routeParams.transactionId;
$scope.selectedproduct = $routeParams.productId;


$scope.getAllProductsSuccess = function(data) {
// allproductsList = [{"id":36,"name":"Education Super
// Savings","code":"SPS","productGroup":"ULIP","definition":"","description":"Education
// Super Savings
// plan","image":"img_education.png","Collaterals":[{"id":56,"fileName":"shiksha_plus_super_logo.png","docType":"Image","fileType":"PNG","version":"","createdDate":null,"updatedDate":null},{"id":57,"fileName":"shiksha_plus_super_brochure.pdf","docType":"Brochure","fileType":"PDF","version":"","createdDate":null,"updatedDate":null},{"id":58,"fileName":"shiksha_plus_super_leaflet.pdf","docType":"Leaflet","fileType":"PDF","version":"","createdDate":null,"updatedDate":null}]},{"id":24,"name":"Pension
// Elite","code":"FYPP","productGroup":"ULIP","definition":"","description":"Pension
// Elite","image":"img_pension_elite.png","Collaterals":[{"id":13,"fileName":"FYPP_logo.png","docType":"Image","fileType":"PNG","version":"","createdDate":null,"updatedDate":null},{"id":14,"fileName":"FYPP_brochure.pdf","docType":"Brochure","fileType":"PDF","version":"","createdDate":null,"updatedDate":null},{"id":15,"fileName":"FYPP_leaflet.pdf","docType":"Leaflet","fileType":"PDF","version":"","createdDate":null,"updatedDate":null}]},{"id":30,"name":"Life
// Forever","code":"WLPS","productGroup":"Traditional","definition":"","description":"Life
// Forever","image":"img_life_forever.png","Collaterals":[{"id":21,"fileName":"wholelife_super_logo.png","docType":"Image","fileType":"PNG","version":"","createdDate":null,"updatedDate":null},{"id":22,"fileName":"whole_life_super_brochure.pdf","docType":"Brochure","fileType":"PDF","version":"","createdDate":null,"updatedDate":null},{"id":23,"fileName":"whole_life_super_leaflet.pdf","docType":"Leaflet","fileType":"PDF","version":"","createdDate":null,"updatedDate":null}]},{"id":41,"name":"Income
// For
// Life","code":"LTIS","productGroup":"Traditional","definition":"","description":"Income
// For
// Life","image":"img_income_for_life.png","Collaterals":[{"id":61,"fileName":"life_time_income_solution_brochure.pdf","docType":"Brochure","fileType":"PDF","version":"","createdDate":null,"updatedDate":null},{"id":62,"fileName":"life_time_income_solution_leaflet.pdf","docType":"Leaflet","fileType":"PDF","version":"","createdDate":null,"updatedDate":null},{"id":63,"fileName":"LTIS_logo.png","docType":"Image","fileType":"PNG","version":"","createdDate":null,"updatedDate":null}]}];
allproductsList = data.Products;
$rootScope.showHideLoadingImage(false);
for ( var x = 0; x < allproductsList.length; x += 2) {
$scope.productsListEven.push(allproductsList[x]);
}
for ( var x = 1; x < allproductsList.length; x += 2) {
$scope.productsListOdd.push(allproductsList[x]);
}
$rootScope.showHideLoadingImage(false);
$scope.refresh();
}

$scope.getAllProductsFailure = function() {
$rootScope.showHideLoadingImage(false);
$scope.refresh();
}

$scope.getIllustration = function(product) {
	if(IllustratorVariables.leadId && IllustratorVariables.leadId !=0 ){
		UtilityService.previousPage = 'LMS';
		$location.path('/fnaChooseParties/0/'+product.id);
		
	}else{
		IllustratorVariables.clearIllustrationVariables();
		// IllustratorVariables.setIllustratorModel($scope.Illustration);
		$location.path('/Illustrator/'+ product.id +'/0/'+IllustratorVariables.illustratorId + '/'+$rootScope.transactionId);

	}
}

$scope.initialLoad = function(){
	$scope.productsListEven = [];

	$scope.productsListOdd = [];

	var transactionObj = {
	"CheckDate": new Date().getMonth() + 1 + "-" + new Date().getDate() + "-" + new Date().getFullYear(),
	"Products": {
	"id": "",
	"code": "",
	"carrierCode": 1
	}
	}
	ProductService.getAllActiveProducts(transactionObj, $scope.getAllProductsSuccess,
			$scope.getAllProductsFailure);

}

$scope.$on('$viewContentLoaded', function(){
	$scope.initialLoad();
  });

}]);
