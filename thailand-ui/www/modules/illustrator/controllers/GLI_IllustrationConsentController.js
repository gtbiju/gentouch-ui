/*
 Name : GLI_IllustrationConsentController
 CreatedDate:26/08/2020
 Description:Controller for Consent and OTP References
 */

'use strict';
storeApp.controller('GLI_IllustrationConsentController', GLI_IllustrationConsentController);
GLI_IllustrationConsentController.$inject = [
	'$rootScope',
	 'globalService',
	 '$route',
	 'GLI_IllustratorService',
	 'GLI_IllustratorVariables',
	 '$scope',
	 '$compile',
	 '$routeParams',
	 'UtilityService',
	 'GLI_DataService',
	 'DataService',
	 'DocumentService',
	 '$translate',
	 '$location',
	 'dateFilter',
	 'IllustratorVariables',
	 'IllustratorService',
	 '$filter',
	 '$debounce',
	 'AutoSave',
	 'FnaVariables',
	 'UserDetailsService',
	 'PersistenceMapping',
	 '$controller',
	 'GLI_EvaluateService', 
	 '$timeout'
 ];
function GLI_IllustrationConsentController($rootScope, globalService, $route, GLI_IllustratorService,
	    GLI_IllustratorVariables, $scope, $compile,
	    $routeParams, UtilityService, GLI_DataService,
	    DataService, DocumentService, $translate,
	    $location, dateFilter, IllustratorVariables,
	    IllustratorService, $filter, $debounce, AutoSave,
	    FnaVariables, UserDetailsService, PersistenceMapping, $controller, GLI_EvaluateService, $timeout) {

	$scope.surname = "";
	$scope.MobileNumber = "";
	$scope.otp1 = "";
	$scope.otp2 = "";
	$scope.otp3 = "";
	$scope.otp4 = "";
	$scope.otp5 = "";
	$scope.otp6 = "";
	$scope.consentCheckBox = "No";
	$scope.showResendBtn = false;
	$scope.validateOtpBtn = false;
	$scope.otpValidated = false;
	$scope.thaiLanguage = false;
	$scope.engLanguage = false;

	$scope.surnameRegex = rootConfig.alphabetWithSpacePatternIllustration;
	$scope.mobileNumberRegex = rootConfig.tendigitmobilenumberPattern;
	
	var connectionStatus;
	$rootScope.showHideLoadingImage(false);

	UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
	$scope.Illustration = IllustratorVariables.getIllustratorModel();
	
    $scope.initialLoad = function () {
		$rootScope.selectedPage = "consentPage";
		if($scope.Illustration.Product.templates.selectedLanguage == 'th'){
			$scope.thaiLanguage = true;
		} else {
			$scope.engLanguage = true;
		}
	}
	
	/*
	* Update error count on blur.
	*/
	$scope.onBlur = function () {
		$rootScope.updateErrorCount('consent');
	}
	
	$timeout(function () {
		$rootScope.updateErrorCount('consent');
	}, 50);

	$scope.syncProgressCallback = function (currentIndex, totalCount) {
		$rootScope.showHideLoadingImage(false, "");
		$scope.refresh();
	};
	$scope.syncModuleProgress = function (syncMessage, progress) {
		$scope.syncMessage = syncMessage;
		$scope.refresh();
	}
	$scope.consentNetworkLink = function () {
		var url = rootConfig.consentnetworkURL;
		if (!rootConfig.isDeviceMobile) {
			window.open(url, '_blank');
		}
		else {
			window.open(url, '_system');
		}
	}
	
    $scope.requestOTP = function() {
		//test start
		if ((rootConfig.isDeviceMobile)) {
			connectionStatus =  (rootConfig.isDeviceMobile == true ? checkConnection() : true);
			if(connectionStatus) {
				$rootScope.showHideLoadingImage(true, 'eapp.SavingInprogress', $translate);
				PersistenceMapping.clearTransactionKeys();
				PersistenceMapping.dataIdentifyFlag = false;
				PersistenceMapping.Type = "illustration";
				$scope.Illustration = IllustratorVariables.getIllustratorModel();
				var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);

						var transactionsToSync = [];
						transactionsToSync.push({
							"Key" : "illustration",
							"Value" : transactionObj.Id,
							"syncType": "indSyncBI"
						});
		// IllustratorService.individualSync($scope, IllustratorVariables, transactionsToSync, function() {
		// 						$rootScope.showHideLoadingImage(true, 'reqRuleProgress', $translate);			
		// 						sendOTP();
						IllustratorService.individualSync($scope, IllustratorVariables, transactionsToSync, function() {
							// GLI_DataService.getListings(transactionObj,function(data){
								var transactionObj = PersistenceMapping.mapScopeToPersistence(IllustratorVariables.illustratorModel);
								DataService.getListingDetail(transactionObj, function (data) {
									$rootScope.showHideLoadingImage(true, 'reqRuleProgress', $translate);
									transactionObj.Key3 = data[0].Key3;
									transactionObj.TransTrackingID = data[0].TransTrackingID;
									transactionObj.TransactionData = {};
									transactionObj.TransactionData.TransactionInfo = {};
									transactionObj.TransactionData.UserInfo = {};
									transactionObj.TransactionData.TransactionInfo.context = 'illustration';
									transactionObj.TransactionData.TransactionInfo.transactionType = 'consentotp';
									transactionObj.TransactionData.TransactionInfo.sisNumber = data[0].Key3;
									transactionObj.TransactionData.UserInfo.Contacts = [];
									transactionObj.TransactionData.UserInfo.Contacts.push({
										type: "smsContact",
										detail: $scope.MobileNumber.toString(),
										surname: $scope.surname
									});
									DataService.sendOTP(transactionObj, generateOtpSuccess, generateOtpError);
								} ,function (){
									$rootScope.showHideLoadingImage(false);
								}); 
							// },function(){
							// 	$rootScope.showHideLoadingImage(false);
							// });
						},function(data){
						},$scope.syncProgressCallback,$scope.syncModuleProgress);
			}
			else {
				$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "eapp_vt.checkOnlineForPayment"),
						translateMessages($translate, "illustrator.okMessageInCaps"));
				$rootScope.showHideLoadingImage(false);
			}
		}
		else {
			sendOTP();
		}
	}

	function sendOTP() {
		if ((rootConfig.isDeviceMobile)) {
			var transactionObj = PersistenceMapping.mapScopeToPersistence(IllustratorVariables.illustratorModel);
			DataService.getListingDetail(transactionObj, function (data) {
				$rootScope.showHideLoadingImage(true, 'reqRuleProgress', $translate);
				transactionObj.Key3 = data[0].Key3;
				transactionObj.Key5 = data[0].Key5;
				transactionObj.TransTrackingID = data[0].TransTrackingID;
				transactionObj.TransactionData = {};
				transactionObj.TransactionData.TransactionInfo = {};
				transactionObj.TransactionData.UserInfo = {};
				transactionObj.TransactionData.TransactionInfo.context = 'illustration';
				transactionObj.TransactionData.TransactionInfo.transactionType = 'consentotp';
				transactionObj.TransactionData.TransactionInfo.sisNumber = data[0].Key3;
				transactionObj.TransactionData.UserInfo.Contacts = [];
				transactionObj.TransactionData.UserInfo.Contacts.push({
					type: "smsContact",
					detail: $scope.MobileNumber.toString(),
					surname: $scope.surname
				});
				DataService.sendOTP(transactionObj, generateOtpSuccess, generateOtpError);
			}, function () {
				$rootScope.showHideLoadingImage(false);
			});
		}
		else {
			var transactionObj = PersistenceMapping.mapScopeToPersistence(IllustratorVariables.illustratorModel);
			IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
			var transactionObj = PersistenceMapping.mapScopeToPersistence(IllustratorVariables.illustratorModel);
			$rootScope.showHideLoadingImage(true, 'reqRuleProgress', $translate);
			// transactionObj.Key3 = data[0].Key3;
			transactionObj.TransTrackingID = transactionObj.TransTrackingID;
			transactionObj.TransactionData = {};
			transactionObj.TransactionData.TransactionInfo = {};
			transactionObj.TransactionData.UserInfo = {};
			transactionObj.TransactionData.TransactionInfo.context = 'illustration';
			transactionObj.TransactionData.TransactionInfo.transactionType = 'consentotp';
			transactionObj.TransactionData.TransactionInfo.sisNumber = transactionObj.Key3;
			transactionObj.TransactionData.UserInfo.Contacts = [];
			transactionObj.TransactionData.UserInfo.Contacts.push({
				type: "smsContact",
				detail: $scope.MobileNumber.toString(),
				surname: $scope.surname
			});
			DataService.sendOTP(transactionObj, generateOtpSuccess, generateOtpError);
		}
		// generateOtpSuccess("SUCCESS");
	}

	function generateOtpSuccess(data){
		$rootScope.showHideLoadingImage(false, 'reqRuleProgress', $translate);
		if(data.otpStatus.statusCodeType == "SUCCESS") {  //&& statusCode == "000"
			$scope.showResendBtn = true;
			$scope.validateOtpBtn = true;
			$scope.transactionUid = data.transactionUid;
			$scope.referCode = data.refCode;
            $rootScope.lePopupCtrl.paymentSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "otpSuccessMsg"), translateMessages($translate, "fna.ok"));
		}
		else if(statusVerify == "FAILURE") { //OTP is expired
            $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
                translateMessages($translate, "unKnownError"),
                translateMessages($translate, "illustrator.okMessageInCaps"));
		}
		// else if(statusVerify == "ERROR") { //OTP is incorrect
        //     $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
        //         translateMessages($translate, "validateOTPError"),
        //         translateMessages($translate, "illustrator.okMessageInCaps"));
		// }
		else {
            $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                translateMessages($translate, "unKnownError"),
                translateMessages($translate, "illustrator.okMessageInCaps"));
        }
		$rootScope.showHideLoadingImage(false, 'reqRuleProgress', $translate);
	}

	function generateOtpError(data){
		$rootScope.showHideLoadingImage(false, 'reqRuleProgress', $translate);
	}

	$scope.otpAppend = function () {
		$scope.otpModel = $scope.otp1 + $scope.otp2 + $scope.otp3 + $scope.otp4 + $scope.otp5 + $scope.otp6;
		if($scope.otpModel.toString().length == 6){
			$scope.showValidateButn = true;	
		}
		else {
			$scope.showValidateButn = false;	
		}
	}
	
	$scope.validateOtp = function(){
		if($scope.otpModel && $scope.otpModel.toString().length == 6){
			$rootScope.showHideLoadingImage(true, 'reqRuleProgress', $translate);
			connectionStatus =  (rootConfig.isDeviceMobile == true ? checkConnection() : true);

			if(connectionStatus) {
				PersistenceMapping.clearTransactionKeys();
			// 	IllustratorService.mapKeysforPersistence($scope,IllustratorVariables);
            //  var transactionObj = PersistenceMapping.mapScopeToPersistence(IllustratorVariables.illustratorModel);
				// IllustratorService.mapKeysforPersistence($scope,IllustratorVariables);
             var transactionObj = PersistenceMapping.mapScopeToPersistence(IllustratorVariables.illustratorModel);
				transactionObj.TransactionData = {};
				transactionObj.TransactionData.TransactionInfo = {};
				transactionObj.TransactionData.UserInfo = {};
				transactionObj.TransactionData.TransactionInfo.context = 'illustration';
				transactionObj.TransactionData.TransactionInfo.sisNumber = transactionObj.Key3;
				transactionObj.TransactionData.TransactionInfo.transactionType = 'consentotp';
				transactionObj.TransactionData.TransactionInfo.authorizationCode = $scope.otpModel;
				transactionObj.TransactionData.TransactionInfo.transactionUUID = $scope.transactionUid;
				transactionObj.TransactionData.TransactionInfo.referenceCode = $scope.referCode;
				transactionObj.TransactionData.UserInfo.Contacts = [];
				transactionObj.TransactionData.UserInfo.Contacts.push({
					type: "smsContact",
					detail: $scope.MobileNumber.toString(),
				});
				DataService.validateOTP(transactionObj, validateOtpSuccess, validateOtpError);
				// validateOtpSuccess("SUCCESS");
			}
			else {
				$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "eapp_vt.userOfflineOTPValidateError"),
					translateMessages($translate, "illustrator.okMessageInCaps"));
				$rootScope.showHideLoadingImage(false);
			}
		}
		else{
				$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "Please enter the valid OTP"),
							translateMessages($translate, "illustrator.okMessageInCaps"));
					$rootScope.showHideLoadingImage(false);
		}
	}

	function validateOtpSuccess(data) {
		if(data.otpStatus.statusCodeType == "SUCCESS") { //check statusverify code as 000
			$scope.otpValidated = true;
			$scope.showResendBtn = false;   
			$rootScope.lePopupCtrl.paymentSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "validateOtpSuccessMsg"), translateMessages($translate, "fna.ok"));
		}else if(data.otpStatus.statusCodeType == "ERROR") { //OTP is expired The OTP is incorrect.
            $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
                translateMessages($translate, "resendOtpBlockedError"),
                translateMessages($translate, "illustrator.okMessageInCaps"));
        }else if(data.otpStatus.statusCodeType == "FAILURE") { //OTP is incorrect
            $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
                translateMessages($translate, "validateOTPError"),
                translateMessages($translate, "illustrator.okMessageInCaps"));
        }else {
            $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                translateMessages($translate, "unKnownError"),
                translateMessages($translate, "illustrator.okMessageInCaps"));
        }		
		$rootScope.showHideLoadingImage(false, 'reqRuleProgress', $translate);
	}
	function validateOtpError() {
         $rootScope.showHideLoadingImage(false);
	}
	
	$scope.submitFullSIS = function () {
		$rootScope.showHideLoadingImage(true, 'reqRuleProgress', $translate);
		if($scope.consentCheckBox == 'Yes' && $scope.otpValidated){
			$scope.Illustration = IllustratorVariables.getIllustratorModel();
			$scope.Illustration.PrivacyLaw.userConsentFlag = 'Yes';
			$scope.Illustration.Insured.BasicDetails.firstName = $scope.name;
			$scope.Illustration.Insured.BasicDetails.lastName = $scope.surname;
			$scope.Illustration.Insured.ContactDetails.mobileNumber1 = $scope.MobileNumber;
			IllustratorVariables.setIllustratorModel($scope.Illustration);
			PersistenceMapping.clearTransactionKeys();
			// IllustratorService.mapKeysforPersistence($scope,IllustratorVariables);
            //  var transactionObj = PersistenceMapping.mapScopeToPersistence(IllustratorVariables.illustratorModel);

        var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);        
			obj.save(function (isAutoSave) {
				$rootScope.showHideLoadingImage(true, 'reqRuleProgress', $translate);
				if ((rootConfig.isDeviceMobile)) {
					var transactionObj = PersistenceMapping.mapScopeToPersistence(IllustratorVariables.illustratorModel);
					DataService.getListingDetail(transactionObj, function (data) {
						$rootScope.showHideLoadingImage(false);
						// $rootScope.showHideLoadingImage(true, 'reqRuleProgress', $translate);
						// transactionObj.Key3 = data[0].Key3;
						// navigateToSIfullPage(data[0]);
						$location.path('/Illustrator/'+data[0].TransactionData.Product.ProductDetails.productId+'/0/'+data[0].Key3+'/'+data[0].Id);
					}, function () {
						$rootScope.showHideLoadingImage(false);
					});
				}
				else {
					IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
					var transactionObj = PersistenceMapping.mapScopeToPersistence(IllustratorVariables.illustratorModel);
					navigateToSIfullPage(transactionObj);
				}
			}); 
		}
		else{
				$rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),translateMessages($translate, "consentDeclaration"),
							translateMessages($translate, "illustrator.okMessageInCaps"));
					$rootScope.showHideLoadingImage(false);
		}
	}

	$scope.resendOtp = function() {
		$rootScope.showHideLoadingImage(true, 'reqRuleProgress', $translate);
		sendOTP();
	}
	
	/* 
	function resendOtpSuccess(data){
		if(data.otpStatus.statusCodeType == "SUCCESS") {
            $rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "eapp_vt.otpSuccessMsg"), translateMessages($translate, "fna.ok"));
        }
		$rootScope.showHideLoadingImage(false, 'reqRuleProgress', $translate);
	}*/
	// function resendOtpError(data){
	// 	$rootScope.showHideLoadingImage(false, 'reqRuleProgress', $translate);
	// }
	
	function navigateToSIfullPage (data) {
		$rootScope.showHideLoadingImage(false); 
		if((rootConfig.isDeviceMobile)){
			$location.path('/Illustrator/'+data.TransactionData.Product.ProductDetails.productId+'/0/'+data.Key3+'/'+data.Id);
		}
		else{
			$location.path('/Illustrator/'+data.TransactionData.Product.ProductDetails.productId+'/0/'+data.Key3+'/0');
		}
		
	}

   /* $scope.$parent.$on('PrivacyLawPage', function (event, args) {
        $scope.selectedTabId = "PrivacyLawPage";
        $scope.Initialize();
    });*/
    
    $scope.$on('$viewContentLoaded', function(){
		$scope.selectedTabId = "consent";
        $scope.initialLoad();
        $rootScope.page_loader_quiz = true;
	});	
	
	
} // Controller ends here

