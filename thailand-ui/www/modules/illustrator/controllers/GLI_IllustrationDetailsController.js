storeApp.controller('GLI_IllustrationDetailsController', GLI_IllustrationDetailsController);
GLI_IllustrationDetailsController.$inject = ['$rootScope',
		 'globalService',
		 '$route',
		 'GLI_IllustratorService',
		 'GLI_IllustratorVariables',
		 '$scope',
		 '$compile',
		 '$routeParams',
		 'UtilityService',
		 'GLI_DataService',
		 'DataService',
		 'DocumentService',
		 '$translate',
		 '$location',
		 'dateFilter',
		 'IllustratorVariables',
		 'IllustratorService',
		 '$filter',
		 '$debounce',
		 'AutoSave',
		 'FnaVariables',
		 'UserDetailsService',
		 'PersistenceMapping',
		 '$controller',
		 'GLI_EvaluateService','$timeout'];

function GLI_IllustrationDetailsController($rootScope, globalService, $route, GLI_IllustratorService,
    GLI_IllustratorVariables, $scope, $compile,
    $routeParams, UtilityService, GLI_DataService,
    DataService, DocumentService, $translate,
    $location, dateFilter, IllustratorVariables,
    IllustratorService, $filter, $debounce, AutoSave,
    FnaVariables, UserDetailsService, PersistenceMapping, $controller, GLI_EvaluateService,$timeout) {

    $controller(
        'IllustrationDetailsController', {
            $rootScope: $rootScope,
            globalService: globalService,
            $route : $route,
            IllustratorService: GLI_IllustratorService,
            DataService: DataService,
            IllustratorVariables: GLI_IllustratorVariables,
            $scope: $scope,
            $compile: $compile,
            $routeParams: $routeParams,
            UtilityService: UtilityService,
            DataService: GLI_DataService,
            DataService: DataService,
            DocumentService: DocumentService,
            $translate: $translate,
            $location: $location,
            dateFilter: dateFilter,
            $debounce: $debounce,
            AutoSave: AutoSave,
            FnaVariables: FnaVariables,
            PersistenceMapping: PersistenceMapping
        });
    $rootScope.hideLanguageSetting = false;
    $scope.emailpopup = false;
    $rootScope.populationBIdisableFlag = false;
    $scope.disableSavebutton = false;
    $scope.isRDSUser = UserDetailsService.getRDSUser();
    $rootScope.isFromIllustration = true;
    $scope.isFromIllustration = $rootScope.isFromIllustration;
    $scope.benefRiderTblCommon = [];
    $scope.showSinaturePadFor = rootConfig.showSinaturePadFor;
    $scope.showErrorForSignature = false;
    $scope.clickSaveForOnce = false;
//    $scope.enableProceedToEapp=true;
	var sigDataClient;
	var sigDataAgent;
	$scope.disableHSRiders = false;
	//var sigDataLifeAssured;
    /*Get Agent Details*/
    $scope.userDetails = UserDetailsService.getUserDetailsModel();
    $scope.agentName = $scope.userDetails.agentName;
    $scope.agentCode = $scope.userDetails.agentCode;
    $scope.agentType = $scope.userDetails.agentType;
    $scope.sisVersion = "1.0";
//    $scope.disableCheckboxConfirm = false;
    $scope.disableSignaturepad = false;
    $scope.disableClientSignaturepad = false;
    $scope.disableConfirmbutton = false;
	IllustratorVariables.RequirementFile=[];
	$scope.languageAssignment = "";
	//$scope.isJellyBean=false;
	/*FNA changes by LE Team >>> starts*/
    $scope.navigatedFromFNA = IllustratorVariables.fromFNAChoosePartyScreenFlow;
    /*FNA changes by LE Team >>> ends*/
    
	/* OIC Changes Start */
    $scope.basicBenefitList = rootConfig.illustrationBasicBenefitList;
    $scope.riderBenefitList = rootConfig.illustrationRiderBenefitList;
	
	$scope.accordions = function(event){
		if($(event.target).hasClass('active')){
			$(event.target).removeClass('active');
		} else {
			$(event.target).addClass('active');
		}
		if($(event.target).next('.formsPanelIllustration').css('display') == 'block'){
			$(event.target).next('.formsPanelIllustration').removeClass('active');
		} else {
			$(event.target).next('.formsPanelIllustration').addClass('active');
		}
	}
    /* OIC Changes End */
	
	//Showing old signature pad for JellyBean devices and new signature pad for all others.
	//Commenting for FGLI since there is no siganture pad				
	var userAgent = navigator.userAgent.toLowerCase();
    var isAndroid = userAgent.indexOf("android") > -1;
	$scope.isJellyBean=false;
    var version = 0;
    	if(isAndroid){
    	   if (navigator.userAgent.match(/Android [\d+\.]{3,5}/)) {

    			var versionstr = navigator.userAgent.match(/Android [\d+\.]{3,5}/)[0].replace('Android ','')
    			 version = parseFloat(versionstr);
    	   }
    	}
    		if((version>=4.1 && version<=4.3)){
    			$scope.isJellyBean=true;
    		}
    		if($scope.isJellyBean){
    			var oldwrapper = angular.element("#old-signature-pad");
    			var oldcanvas = oldwrapper[0].querySelector("canvas");
    			$('.sigPadIllustration').signaturePad({
    				drawOnly : true,
    				lineWidth : 0
    			});
    			$('.sigPadIllustration').parents("*").css(
    				"overflow", "visible");
    		}else{
    			var signaturePadClient = new SignaturePad(angular.element("#clientSigNew")[0].querySelector("canvas"));
    			var signaturePadAgent = new SignaturePad(angular.element("#agentSigNew")[0].querySelector("canvas"));
    			///var signaturePadLifeAssured = new SignaturePad(angular.element("#lifeAssuredSig")[0].querySelector("canvas"));
    		}
	
    //Variable to disable the controls if the status is Confirmed or Completed with an eApp
    if (IllustratorVariables.illustrationStatus == "Confirmed" || (IllustratorVariables.illustrationStatus == "Completed" && $rootScope.haseApp == true)) {
            $scope.disableSignaturepad = true;
	        $scope.disableClientSignaturepad = true;
	        $scope.disableCanvasClientpad = true;
	        $scope.disableCanvasAgentpad =true;
            $rootScope.disableControl = true;
        } else {
            $rootScope.disableControl = false;
        }
    
     // FNA changes by LE Team - Starts>>
    $scope.chooseProductRecommendation=function(){
       $scope.lePopupCtrl.showWarning(
                        translateMessages($translate,
                                "lifeEngage"),
                        translateMessages($translate,
                                "pageNavigationText"),  
                        translateMessages($translate,
                                "fna.navok"),$scope.okPressed,translateMessages($translate,
                                "general.navproceed"),$scope.proceedToProductRecommendationScreen);     
    }

    $scope.proceedToProductRecommendationScreen=function(){
        IllustratorVariables.fromChooseAnotherProduct = false;
        $location.path("/fnaRecomendedProducts");  
    }

    $scope.choosePartyScreen=function(){
       $scope.lePopupCtrl.showWarning(
                        translateMessages($translate,
                                "lifeEngage"),
                        translateMessages($translate,
                                "pageNavigationText"),  
                        translateMessages($translate,
                                "fna.navok"),$scope.okPressed,translateMessages($translate,
                                "general.navproceed"),$scope.proceedToPartyScreen);     
    }

    $scope.proceedToPartyScreen=function(){
        IllustratorVariables.fromChooseAnotherProduct = false;
        $location.path("/fnaChooseParties/0/" + $routeParams.productId + "/0");  
    }
    // FNA changes by LE Team - Ends>>
    
    $scope.chooseAnotherProduct=function(){     
        $scope.lePopupCtrl.showWarning(
						translateMessages($translate,
								"lifeEngage"),
						translateMessages($translate,
								"pageNavigationText"),  
						translateMessages($translate,
								"fna.navok"),$scope.okPressed,translateMessages($translate,
								"general.navproceed"),$scope.proceedToProductBasket);        
    }
    
    $scope.proceedToProductBasket=function(){
		IllustratorVariables.transactionId = $rootScope.transactionId;
        IllustratorVariables.fromChooseAnotherProduct = true;
        $location.path("/products/0/0/0/0/false");  
    }
    
	$scope.errorCallback = function () {
        $rootScope.showHideLoadingImage(false, "");
    }
	//Commenting for FGLI since there is no siganture pad	
	function sigantureRender(index,sigdata,successCall){
		if(sigdata){
			IllustratorService.convertImgToBase64(sigdata.base64string, function(base64Img){
			//retrieving signature based on jellybean and other devices.
				if($scope.isJellyBean){
					var canvas = angular.element("#sign");
					var ctx = canvas[0].getContext("2d");
					var image = new Image();
					image.onload = function() {
						ctx.drawImage(image, 0, 0);
					};
					image.src = base64Img;
				}else{
					//signaturePad.fromDataURL(base64Img);
					var signatureType = sigdata.fileName.split("_")[0];
					if(signatureType == "ClientSignature"){
						var dpr = window.devicePixelRatio;
                        window.devicePixelRatio = 1;
						signaturePadClient.fromDataURL(base64Img);
						window.devicePixelRatio = dpr;
					}else if(signatureType == "AgentSignature"){
						var dpr = window.devicePixelRatio;
                        window.devicePixelRatio = 1;
						signaturePadAgent.fromDataURL(base64Img);
						window.devicePixelRatio = dpr;
					}else{
						signaturePadLifeAssured.fromDataURL(base64Img);
					}
					successCall(index);
				}
			},outputFormat);
		}else{
			successCall(index);
		}
	}
	function renderSuccess(i){
		i++;
		if(i<IllustratorVariables.RequirementFile.length){
			sigantureRender(i,IllustratorVariables.RequirementFile[i],renderSuccess);
		}
	}
	
	$scope.getDocsSuccessCallBack = function(index){
		/*index++
		var requirementObject = CreateRequirementFile();
        requirementObject.RequirementFile.identifier =  IllustratorVariables.transTrackingID;
        if(index < IllustratorVariables.illustratorModel.Requirements.length){
            for(var i =index; i<IllustratorVariables.illustratorModel.Requirements.length; i++){
            	if(IllustratorVariables.illustratorModel.Requirements[i].requirementType === "Signature"){
            		for(var j=0; j< IllustratorVariables.illustratorModel.Requirements[i].Documents.length;j++){
								requirementObject.RequirementFile.requirementName = IllustratorVariables.illustratorModel.Requirements[i].requirementName;
								requirementObject.RequirementFile.documentName = IllustratorVariables.illustratorModel.Requirements[i].Documents[j].documentName;
								requirementObject.RequirementFile.fileName=IllustratorVariables.illustratorModel.Requirements[i].Documents[j].fileName;
								requirementObject.RequirementFile.status="Saved";
								PersistenceMapping.clearTransactionKeys();
								//IllustratorVariables.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
								IllustratorService.mapKeysforPersistence($scope,IllustratorVariables);
								var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
            				DataService.getDocumentFileForRequirement(transactionObj,function(sigdata){
                                    $scope.getDocsSuccess(sigdata, i);},$scope.errorCallback);
            			//}
            		}
            		break;
            	}
            	$rootScope.showHideLoadingImage(false, "");
            }
        }*/
	}
	$scope.getDocsSuccess = function(sigdata,index){
		sigantureRender(index,sigdata,$scope.getDocsSuccessCallBack);
	}
/*	if(eval(rootConfig.isDeviceMobile)){
	    var signature="Signature"
	    var outputFormat="image/png";
	    IllustratorService.getSignature(IllustratorVariables.transTrackingID,signature,"",function(sigdata){
			IllustratorVariables.RequirementFile = sigdata;
			if(sigdata.length>0){
				var i=0;
				sigantureRender(i,sigdata[i],renderSuccess);
			}
	    });
    } else{
    	if(IllustratorVariables.illustratorModel.Requirements && IllustratorVariables.illustratorModel.Requirements.length>0 &&
	    	IllustratorVariables.illustratorModel.Requirements && IllustratorVariables.illustratorModel.Requirements[0].Documents[0].pages[0]){
		    IllustratorService.getSignature(IllustratorVariables,signature,$scope,function(sigdata){
				IllustratorVariables.RequirementFile=[];
		    	IllustratorVariables.RequirementFile.push(sigdata);
		    	signaturePad.fromDataURL(sigdata.base64string);
		    });
	    }
		var requirementObject = CreateRequirementFile();
		if(IllustratorVariables.illustratorModel.Requirements.length > 0){
				for(var i = 0; i<IllustratorVariables.illustratorModel.Requirements.length; i++){
					if(IllustratorVariables.illustratorModel.Requirements[i].requirementType === "Signature"){
						for(var j=0; j< IllustratorVariables.illustratorModel.Requirements[i].Documents.length;j++){
								requirementObject.RequirementFile.identifier = IllustratorVariables.transTrackingID;
								requirementObject.RequirementFile.requirementName = IllustratorVariables.illustratorModel.Requirements[i].requirementName;
								requirementObject.RequirementFile.documentName = IllustratorVariables.illustratorModel.Requirements[i].Documents[j].documentName;
								requirementObject.RequirementFile.fileName=IllustratorVariables.illustratorModel.Requirements[i].Documents[j].fileName;
								requirementObject.RequirementFile.status="Saved";
								PersistenceMapping.clearTransactionKeys();
								//IllustratorVariables.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
								IllustratorService.mapKeysforPersistence($scope,IllustratorVariables);
								var transactionObj = PersistenceMapping.mapScopeToPersistence(requirementObject);
								DataService.getDocumentFileForRequirement(transactionObj,function(sigdata){
								$scope.getDocsSuccess(sigdata,i)},$scope.errorCallback);
							//}
						}
						break;
					}
					}
				}
    }
	*/
	
	
	if(rootConfig.isDeviceMobile){
	    var signature="Signature"
	    var outputFormat="image/png";
	    IllustratorService.getSignature(IllustratorVariables.transTrackingID,signature,"",function(sigdata){
			IllustratorVariables.RequirementFile = sigdata;
			if(sigdata.length>0){
				var i=0;
				sigantureRender(i,sigdata[i],renderSuccess);
			}
	    });
    } else{
    	if(IllustratorVariables.illustratorModel.Requirements && IllustratorVariables.illustratorModel.Requirements.length>0 &&
	    	IllustratorVariables.illustratorModel.Requirements){
		    IllustratorService.getSignature(IllustratorVariables,signature,$scope,function(sigdata){
				IllustratorVariables.RequirementFile=[];
		    	IllustratorVariables.RequirementFile.push(sigdata);
		    	//signaturePad.fromDataURL(sigdata.base64string);
		    });
	    }
    }
	
/*	$scope.disableSignaturepad = false;
    $scope.disableConfirmbutton = true;
    $scope.disableSavebutton = false;
    $scope.disableCheckboxConfirm = true;*/

    $rootScope.selectedPage = "Illustration Details";
    $scope.Illustration = IllustratorVariables.getIllustratorModel();
    /***
    Confirmation check box getting cleared when synced from illustration details after confirmation -Fix(SIT bug-1093) 
    ***/
//    if (IllustratorVariables.illustrationStatus == "Confirmed") {
//        $scope.Illustration.IllustrationOutput.checkBoxConfirm = 'Yes';
//    } else {
//        $scope.Illustration.IllustrationOutput.checkBoxConfirm = 'No';
//    }
//    $scope.checkconfirm($scope.Illustration.IllustrationOutput.checkBoxConfirm);

    $scope.Illustration.Insured.BasicDetails.fullName = $scope.Illustration.Insured.BasicDetails.firstName + ' ' + $scope.Illustration.Insured.BasicDetails.lastName;
    $scope.Illustration.Payer.BasicDetails.fullName = $scope.Illustration.Payer.BasicDetails.firstName + ' ' + $scope.Illustration.Payer.BasicDetails.lastName;

    $scope.premiumTermInYears = translateMessages($translate, "illustrator.glivnillustrationopbaoanbenefits1-1") + $scope.Illustration.IllustrationOutput.PolicyDetails.premiumPayingTerm + translateMessages($translate, "illustrator.glivnillustrationopbaoanbenefits1-2");
    $scope.dbAfter10YrsAfterRoundOff = Math.round($scope.Illustration.IllustrationOutput.dbAfter10Yrs);
    $scope.tenthPolicyTerm = translateMessages($translate, "illustrator.glivnillustrationopbaoanbenefits2-1") + $filter('formattedNumber')($scope.dbAfter10YrsAfterRoundOff) + translateMessages($translate, "illustrator.glivnillustrationopbaoanbenefits2-2");
    $scope.coronaryDisBenefitAfterRoundOff = Math.round($scope.Illustration.IllustrationOutput.coronaryDisBenefit);
    $scope.coronaryDisease = translateMessages($translate, "illustrator.glivnillustrationopbaoanbenefits3-1") + $filter('formattedNumber')($scope.coronaryDisBenefitAfterRoundOff) + translateMessages($translate, "illustrator.glivnillustrationopbaoanbenefits2-2");
    $scope.dlyHosCashAfterRoundOff = Math.round($scope.Illustration.IllustrationOutput.dlyHosCash);
    $scope.dailyHospitalCash = translateMessages($translate, "illustrator.glivnillustrationopbaoanbenefits4-1") + $filter('formattedNumber')($scope.dlyHosCashAfterRoundOff) + translateMessages($translate, "illustrator.glivnillustrationopbaoanbenefits4-2");
    $scope.totalBenAtMaturityAfterRoundOff = Math.round($scope.Illustration.IllustrationOutput.totalBenAtMaturity);
    $scope.totalBenAtMaturity = translateMessages($translate, "illustrator.glivnillustrationopbaoanbenefits6-1") + $filter('formattedNumber')($scope.totalBenAtMaturityAfterRoundOff) + translateMessages($translate, "illustrator.glivnillustrationopbaoanbenefits6-2");
    
    // For GenSave10Plus Graph
	if($scope.Illustration.IllustrationOutput.ADBRider && $scope.Illustration.IllustrationOutput.ADBRider.sumAssured){
		$scope.adbRiderSumAssuredGraph = $filter('formattedNumber')($scope.Illustration.IllustrationOutput.ADBRider.sumAssured);
	}
	if($scope.Illustration.IllustrationOutput.sumInsuredFinal){
		$scope.sumInsuredFinalGraph = $filter('formattedNumber')($scope.Illustration.IllustrationOutput.sumInsuredFinal);
	}
	if($scope.Illustration.IllustrationOutput.totalLivingBenefit){
		$scope.totalLivingBenefitGraph = $filter('formattedNumber')($scope.Illustration.IllustrationOutput.totalLivingBenefit);
	}
    
	$scope.taxOutput = translateMessages($translate, "illustrator.illustrationOutputBenefitTableTotalTaxDeductionRate") + $scope.Illustration.Product.policyDetails.tax + translateMessages($translate, "illustrator.illustrationOutputBenefitTableTotalTaxDeductionRate-1");
	
    if ($scope.dailyHosCash != "") {
        $scope.dailyHosCash = $filter('formattedNumber')($scope.Illustration.IllustrationOutput.dlyHosCash) + translateMessages($translate, "illustrator.glivnIlustrationopMainInsBenefTblBenef6-2Val");
    }
    if ($scope.optHosCashWithText != "") {
        $scope.optHosCashWithText = $filter('formattedNumber')($scope.Illustration.IllustrationOutput.optHosCash) + translateMessages($translate, "illustrator.glivnIlustrationopMainInsBenefTblBenef6-7Val");
    }
    if ($scope.dailyHosCashFrom6Days != "") {
        $scope.dailyHosCashFrom6Days = $filter('formattedNumber')($scope.Illustration.IllustrationOutput.dlyHosCashFrom6Days) + translateMessages($translate, "illustrator.glivnIlustrationopMainInsBenefTblBenef6-3Val");
    }
    if ($scope.icuHospitalCash != "") {
        $scope.icuHospitalCash = $filter('formattedNumber')($scope.Illustration.IllustrationOutput.icuHosCash) + translateMessages($translate, "illustrator.glivnIlustrationopMainInsBenefTblBenef6-4Val");
    }
    if ($scope.Illustration.IllustrationOutput.dbB475NotRecvLSCIBen) {
        $scope.dailyHosCashECI = $filter('formattedNumber')($scope.Illustration.IllustrationOutput.dbB475NotRecvLSCIBen) + translateMessages($translate, "illustrator.glivnIlustrationopMainInsBenefTblBenef4-1Val");
    }
    if ($scope.Illustration.IllustrationOutput.firstLateStageCI) {
        $scope.optHosCashWithTextECI = $filter('formattedNumber')($scope.Illustration.IllustrationOutput.firstLateStageCI) + translateMessages($translate, "illustrator.glivnIlustrationopMainInsBenefTblBenef3-1Val");
    }
    if ($scope.Illustration.IllustrationOutput.dbB475RecvFLSCIBen) {
        $scope.dailyHosCashFrom6DaysECI = $filter('formattedNumber')($scope.Illustration.IllustrationOutput.dbB475RecvFLSCIBen) + translateMessages($translate, "illustrator.glivnIlustrationopMainInsBenefTblBenef4-2Val");
    }
    if ($scope.Illustration.IllustrationOutput.hrr11TblChosenSAStd) {
        $scope.maxSumAssuredValueStd = angular.copy($scope.Illustration.IllustrationOutput.hrr11TblChosenSAStd);
        $scope.maxSumAssuredValueStd = $filter('formattedNumber')($scope.maxSumAssuredValueStd);
    }
    if ($scope.Illustration.IllustrationOutput.hrr11TblChosenSAExec) {
        $scope.maxSumAssuredValueExec = angular.copy($scope.Illustration.IllustrationOutput.hrr11TblChosenSAExec);
        $scope.maxSumAssuredValueExec = $filter('formattedNumber')($scope.maxSumAssuredValueExec);
    }
    if ($scope.Illustration.IllustrationOutput.hrr11TblChosenSAPrem) {
        $scope.maxSumAssuredValuePrem = angular.copy($scope.Illustration.IllustrationOutput.hrr11TblChosenSAPrem);
        $scope.maxSumAssuredValuePrem = $filter('formattedNumber')($scope.maxSumAssuredValuePrem);
    }
    $scope.benefitsOfMainInsured = translateMessages($translate, "illustrator.glivnIlustrationopMainInsBenefTblBenefMainIns") + " " + $scope.Illustration.Insured.BasicDetails.fullName;
    $scope.premiumFrequency = '';
    if ($scope.Illustration.IllustrationOutput.PolicyDetails.premiumMode) {
        if ($scope.Illustration.IllustrationOutput.PolicyDetails.premiumMode == '6001') {
            $scope.premiumFrequency = translateMessages($translate, "illustrator.glivnIlustrationopPremFreqTblYear");
        } else if ($scope.Illustration.IllustrationOutput.PolicyDetails.premiumMode == '6002') {
            $scope.premiumFrequency = translateMessages($translate, "illustrator.glivnIlustrationopPremFreqTblHalfyear");
        } else if ($scope.Illustration.IllustrationOutput.PolicyDetails.premiumMode == '6003') {
            $scope.premiumFrequency = translateMessages($translate, "illustrator.glivnIlustrationopPremFreqTblQuart");
        }
    }
    $scope.opvitabaoanbenefits1 = $filter('formattedNumber')($scope.Illustration.IllustrationOutput.compProtectFund);
    $scope.opvitabaoanbenefits2 = $filter('formattedNumber')($scope.Illustration.IllustrationOutput.cashBenPayableFund);
    $scope.opvitabaoanbenefits3 = $filter('formattedNumber')($scope.Illustration.IllustrationOutput.oldAgeSplCashBenFund);
    $scope.opvitabaoanbenefits5 = $filter('formattedNumber')($scope.Illustration.IllustrationOutput.familyLegacyFund);
    if ($scope.Illustration.IllustrationOutput.selectedRiderTbl && $scope.Illustration.IllustrationOutput.selectedRiderTbl.length > 0) {
	    for (var t = 0; t < $scope.Illustration.IllustrationOutput.selectedRiderTbl.length; t++) {
	        $scope.Illustration.IllustrationOutput.selectedRiderTbl[t].productNameActual = angular.copy($scope.Illustration.IllustrationOutput.selectedRiderTbl[t].productName);
	        $scope.Illustration.IllustrationOutput.selectedRiderTbl[t].productNameActual = translateMessages($translate, $scope.Illustration.IllustrationOutput.selectedRiderTbl[t].productNameActual);
	        if ($scope.Illustration.IllustrationOutput.selectedRiderTbl[t].policyYr === "Annual Renewal") {
	            $scope.Illustration.IllustrationOutput.selectedRiderTbl[t].policyYrActual = angular.copy($scope.Illustration.IllustrationOutput.selectedRiderTbl[t].policyYr);
	            $scope.Illustration.IllustrationOutput.selectedRiderTbl[t].policyYr = translateMessages($translate, "AnnualRenewal");
	        }
	    }
    }
    $scope.policyTermPremiumTable = translateMessages($translate, $scope.Illustration.IllustrationOutput.vitaPolicyTermDisplay);
    if ($scope.Illustration.IllustrationOutput.hrr11TerritoryDisplayVietnam) {
        $scope.hrr11TerritoryDisplayVietnam = translateMessages($translate, $scope.Illustration.IllustrationOutput.hrr11TerritoryDisplayVietnam);
    }
    if ($scope.Illustration.IllustrationOutput.hrr11TerritoryDisplayAsia) {
        $scope.hrr11TerritoryDisplayAsia = translateMessages($translate, $scope.Illustration.IllustrationOutput.hrr11TerritoryDisplayAsia);
    }
    //This function merges two arrays and provides translations for VITA-Golden Health-Executive Plan table	
    // Removed from scope
    function customizeBenefitScheduleArray(benefitScheduleArray) {
        for (var i = 0; i < benefitScheduleArray.length; i++) {
            benefitScheduleArray[i].actualLabel = angular.copy(benefitScheduleArray[i].label);
            benefitScheduleArray[i].label = translateMessages($translate, benefitScheduleArray[i].label);
            benefitScheduleArray[i].subLimit = translateMessages($translate, benefitScheduleArray[i].prefix) + " " + $filter('formattedNumber')(benefitScheduleArray[i].benefitValue) + " " + translateMessages($translate, benefitScheduleArray[i].suffix);
            benefitScheduleArray[i].subLimit = translateMessages($translate, benefitScheduleArray[i].subLimit);
            //Logic to merge Rows in VITA Rider table
            if (rootConfig.mergeNeededRows.indexOf(benefitScheduleArray[i].actualLabel) != -1) {
                benefitScheduleArray[i].mergeRows = "mergeRows";
            } else if (rootConfig.mergeNeededRowsFirst.indexOf(benefitScheduleArray[i].actualLabel) != -1) {
                benefitScheduleArray[i].mergeRows = "mergeRowsFirst";
            } else {
                benefitScheduleArray[i].mergeRows = "";
            }
            //Removing the 0 values from the VITA Rider table
            if (benefitScheduleArray[i].subLimit === "  " || benefitScheduleArray[i].subLimit === " ") {
                benefitScheduleArray[i].subLimit = "";
            }
        }
        return benefitScheduleArray;
    }

    $scope.vitaStandardTableData = [];
    $scope.vitaStdOutputTableData = [];
    $scope.vitaExecutiveTableData = [];
    $scope.vitaExecutiveOPTableData = [];
    $scope.vitaPremierTableData = [];
    $scope.vitaPremierOPTableData = [];
    $scope.vitaPremierOPDentalTableData = [];
    if ($scope.Illustration.IllustrationOutput.vitaStandard11TableData && $scope.Illustration.IllustrationOutput.vitaStandard11TableData.length > 0 && $scope.Illustration.IllustrationOutput.vitaStandard12TableData.length > 0) {
        $scope.initialvitaStandardTableData = ($scope.Illustration.IllustrationOutput.vitaStandard11TableData).concat($scope.Illustration.IllustrationOutput.vitaStandard12TableData);
        $scope.vitaStandardTableData = angular.copy($scope.initialvitaStandardTableData);
        $scope.vitaStandardTableData = customizeBenefitScheduleArray($scope.vitaStandardTableData);
    }
    if ($scope.Illustration.IllustrationOutput.vitaStdOutput11TableData && $scope.Illustration.IllustrationOutput.vitaStdOutput11TableData.length > 0 && $scope.Illustration.IllustrationOutput.vitaStdOutput12TableData.length > 0) {
        $scope.initialvitaStdOutputTableData = ($scope.Illustration.IllustrationOutput.vitaStdOutput11TableData).concat($scope.Illustration.IllustrationOutput.vitaStdOutput12TableData);
        $scope.vitaStdOutputTableData = angular.copy($scope.initialvitaStdOutputTableData);
        $scope.vitaStdOutputTableData = customizeBenefitScheduleArray($scope.vitaStdOutputTableData);
    }
    if ($scope.Illustration.IllustrationOutput.vitaExecutive11TableData && $scope.Illustration.IllustrationOutput.vitaExecutive11TableData.length > 0 && $scope.Illustration.IllustrationOutput.vitaExecutive12TableData.length > 0) {
        $scope.initialvitaExecutiveTableData = ($scope.Illustration.IllustrationOutput.vitaExecutive11TableData).concat($scope.Illustration.IllustrationOutput.vitaExecutive12TableData);
        $scope.vitaExecutiveTableData = angular.copy($scope.initialvitaExecutiveTableData);
        $scope.vitaExecutiveTableData = customizeBenefitScheduleArray($scope.vitaExecutiveTableData);
    }
    if ($scope.Illustration.IllustrationOutput.vitaExecutiveOP11TableData && $scope.Illustration.IllustrationOutput.vitaExecutiveOP11TableData.length > 0 && $scope.Illustration.IllustrationOutput.vitaExecutiveOP12TableData.length > 0) {
        $scope.initialvitaExecutiveOPTableData = ($scope.Illustration.IllustrationOutput.vitaExecutiveOP11TableData).concat($scope.Illustration.IllustrationOutput.vitaExecutiveOP12TableData);
        $scope.vitaExecutiveOPTableData = angular.copy($scope.initialvitaExecutiveOPTableData);
        $scope.vitaExecutiveOPTableData = customizeBenefitScheduleArray($scope.vitaExecutiveOPTableData);
    }
    if ($scope.Illustration.IllustrationOutput.vitaPremier11TableData && $scope.Illustration.IllustrationOutput.vitaPremier11TableData.length > 0 && $scope.Illustration.IllustrationOutput.vitaPremier12TableData.length > 0) {
        $scope.initialvitaPremierTableData = ($scope.Illustration.IllustrationOutput.vitaPremier11TableData).concat($scope.Illustration.IllustrationOutput.vitaPremier12TableData);
        $scope.vitaPremierTableData = angular.copy($scope.initialvitaPremierTableData);
        $scope.vitaPremierTableData = customizeBenefitScheduleArray($scope.vitaPremierTableData);
    }
    if ($scope.Illustration.IllustrationOutput.vitaPremierOP11TableData && $scope.Illustration.IllustrationOutput.vitaPremierOP11TableData.length > 0 && $scope.Illustration.IllustrationOutput.vitaPremierOP12TableData.length > 0) {
        $scope.initialvitaPremierOPTableData = ($scope.Illustration.IllustrationOutput.vitaPremierOP11TableData).concat($scope.Illustration.IllustrationOutput.vitaPremierOP12TableData);
        $scope.vitaPremierOPTableData = angular.copy($scope.initialvitaPremierOPTableData);
        $scope.vitaPremierOPTableData = customizeBenefitScheduleArray($scope.vitaPremierOPTableData);
    }
    if ($scope.Illustration.IllustrationOutput.vitaPremierOPDental11TableData && $scope.Illustration.IllustrationOutput.vitaPremierOPDental11TableData.length > 0 && $scope.Illustration.IllustrationOutput.vitaPremierOPDental12TableData.length > 0) {
        $scope.initialvitaPremierOPDentalTableData = ($scope.Illustration.IllustrationOutput.vitaPremierOPDental11TableData).concat($scope.Illustration.IllustrationOutput.vitaPremierOPDental12TableData);
        $scope.vitaPremierOPDentalTableData = angular.copy($scope.initialvitaPremierOPDentalTableData);
        $scope.vitaPremierOPDentalTableData = customizeBenefitScheduleArray($scope.vitaPremierOPDentalTableData);
    }
    //This function Merges MI or AI with its Party fullname for Benefit Illustration Rider table
    $scope.benefitsOfMIorAITranslate = function (data) {
        if (data == "LA1") {
            return translateMessages($translate, "illustrator.partyTblInsuredForBenefitIllusRiderTbl") + " " + $scope.Illustration.Insured.BasicDetails.fullName;
        } else if ($scope.Illustration.IllustrationOutput.selectedRiderTbl) {
            for (var t = 0; t < $scope.Illustration.IllustrationOutput.selectedRiderTbl.length; t++) {
                if ($scope.Illustration.IllustrationOutput.selectedRiderTbl[t].laNum == data) {
                    return translateMessages($translate, "illustrator.partyTblAddInsuredForBenefitIllusRiderTbl") + " " + $scope.Illustration.IllustrationOutput.selectedRiderTbl[t].LifeAssured;
                }
            }
        }
        return data;
    }
    
    //Need to change after getting rule value for GenComplete Health
	if ($scope.Illustration.Product.ProductDetails.productId == '1010' && $scope.Illustration.Product.ProductDetails.productCode == '10') {
		$scope.Illustration.IllustrationOutput.basePremiumFinal = parseInt($scope.Illustration.IllustrationOutput.basePremiumFinal);
		$scope.Illustration.IllustrationOutput.CIPremiumCover = parseInt($scope.Illustration.IllustrationOutput.CIPremiumCover);
		$scope.Illustration.IllustrationOutput.ADDPremiumCover = parseInt($scope.Illustration.IllustrationOutput.ADDPremiumCover);
		$scope.Illustration.IllustrationOutput.HBPremiumCover = parseInt($scope.Illustration.IllustrationOutput.HBPremiumCover);
		$scope.Illustration.IllustrationOutput.HSPremiumCover = parseInt($scope.Illustration.IllustrationOutput.HSPremiumCover);
		if ($scope.Illustration.IllustrationOutput.OPDPremiumCover) {
			$scope.Illustration.IllustrationOutput.OPDPremiumCover = parseInt($scope.Illustration.IllustrationOutput.OPDPremiumCover);
		} else {
			$scope.Illustration.IllustrationOutput.OPDPremiumCover = "";
		}
		$scope.Illustration.IllustrationOutput.totalPremiumCover = Number($scope.Illustration.IllustrationOutput.basePremiumFinal) + Number($scope.Illustration.IllustrationOutput.CIPremiumCover) + Number($scope.Illustration.IllustrationOutput.ADDPremiumCover) + Number($scope.Illustration.IllustrationOutput.HBPremiumCover) + Number($scope.Illustration.IllustrationOutput.HSPremiumCover) + Number($scope.Illustration.IllustrationOutput.OPDPremiumCover);
	}

    //Providing Frequency value for Rider Table
    $scope.getFrequencyVal = function () {
        if ($scope.Illustration.IllustrationOutput.PolicyDetails.premiumMode == "6001") {
            return translateMessages($translate, "illustrator.glivnIlustrationopPolicyTblYearPrem");
        } else if ($scope.Illustration.IllustrationOutput.PolicyDetails.premiumMode == "6002") {
            return translateMessages($translate, "illustrator.glivnIlustrationopPolicyTblHalfYearPrem");
        } else if ($scope.Illustration.IllustrationOutput.PolicyDetails.premiumMode == "6003") {
            return translateMessages($translate, "illustrator.glivnIlustrationopPolicyTblQuarPrem");
        } else {
            return translateMessages($translate, "illustrator.glivnIlustrationopPolicyTblYearPrem");
        }
        return translateMessages($translate, "illustrator.glivnIlustrationopPolicyTblYearPrem");
    }


    //$scope.Illustration.IllustrationOutput.policyDetailsTbl[0].LifeAssured = $scope.Illustration.Insured.BasicDetails.fullName;
    $scope.currentDateFormatted = getFormattedDateDDMMYYYY($scope.Illustration.Payer.BasicDetails.currentDate);


    // Setting isMinor vaiable to load the Illustration Output Sum Insured table based on age
    if ($scope.Illustration.Insured.BasicDetails.age >= 18) {
        $scope.isMinor = true;
    } else {
        $scope.isMinor = false;
    }

    PersistenceMapping.clearTransactionKeys();
    IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
    PersistenceMapping.Type = "illustration";
    var transactionObject = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
    if ($scope.isRDSUser) {
        $scope.illustrationNumber = transactionObject.Key24;
    } else {
        $scope.illustrationNumber = transactionObject.Key3;
    }
    $scope.refresh();

    $('#agentSigNew').signaturePad({
        drawOnly: true,
        lineWidth: 0
    });

    //$('#agentSigNew').parents("*").css("overflow", "visible");

    $('#clientSigNew').signaturePad({
        drawOnly: true,
        lineWidth: 0
    });

    //$('#clientSigNew').parents("*").css("overflow", "visible");


    if (IllustratorVariables.illustrationStatus == "Confirmed") {
        $scope.disableSignaturepad = true;
    }

    $scope.translateLabel = function (value) {
        return $translate.instant(value);
    }

    // Common method for navigating to choose party screen for web and device
		$scope.navigateToChooseParty = function(fnaData){
			if(fnaData != undefined){
				if (fnaData && fnaData[0] && fnaData[0].TransactionData) {
					FnaVariables.setFnaModel({
						'FNA' : JSON.parse(JSON.stringify(fnaData[0].TransactionData))
					});
					$scope.FNAObject = FnaVariables.getFnaModel();
					FnaVariables.transTrackingID = fnaData[0].TransTrackingID;
					FnaVariables.leadId = fnaData[0].Key1;
					globalService.setParties($scope.FNAObject.FNA.parties);
					UtilityService.previousPage = 'IllustrationPersonal';
					if(IllustratorVariables.illustratorId =="" || typeof IllustratorVariables.illustratorId == "undefined"){
						IllustratorVariables.illustratorId = "0";
					}
					$location.path("/fnaChooseParties/0/0/"+ IllustratorVariables.illustratorId);
					$scope.refresh();
				}
			}
		}

    $scope.proceedToChooseParty = function(){
		if (rootConfig.isDeviceMobile) {
			DataService.getRelatedFNA(IllustratorVariables.fnaId, function(fnaData) {
				$scope.navigateToChooseParty(fnaData);
			});
		}else{
			
			PersistenceMapping.clearTransactionKeys();
			var  searchCriteria = {
					 searchCriteriaRequest:{
			"command" : "RelatedTransactions",
			"modes" :['FNA'],
			"value" : IllustratorVariables.fnaId
			}
			};
			PersistenceMapping.Type = "FNA";
			var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
			transactionObj.TransTrackingID = IllustratorVariables.fnaId;
			
			DataService.getFilteredListing(transactionObj, function(fnaData) {
				$scope.navigateToChooseParty(fnaData);
			});
		}
	}
	
	$scope.closePopupForErrorMsg = function () {
		$scope.errorMessageForInsured = undefined;	
		$scope.errorMessageForAgent = undefined;
		$scope.showErrorForSignature = false;
	};
    // ADDITIONAL INSURED DATA(dynamic generation of additional insured data table)
    
    // Status changed to "Confirmed" on clicking confirm button
    $scope.confirmSignature = function () {
		angular.element(document.querySelector(".site-overlay")).addClass("hideSplitOne");
		var divElement = angular.element(document.querySelector('.hideSplitOne'));
		var htmlElement = angular.element('<span class="loadmsg">Loading..<span>');
		divElement.append(htmlElement);
		$compile(divElement)($scope); 

		$timeout(function () {
			angular.element(document.querySelector(".site-overlay")).removeClass("hideSplitOne");
		},1500);	

        $rootScope.showHideLoadingImage(true, 'saveTransactionMessage', $translate);
        var illuDate = $scope.Illustration.Product.premiumSummary.validatedDate;
        var isExpired = GLI_IllustratorService
            .checkExpiryDate(illuDate);							

        if (($scope.disableClientSignaturepad == false || ($scope.disableClientSignaturepad == true  && signaturePadClient.isEmpty()))
        	 && ($scope.disableSignaturepad == false || ($scope.disableSignaturepad == true && signaturePadAgent.isEmpty()))) {
			 $rootScope.showHideLoadingImage(false, "");
			 $scope.showErrorForSignature = true;
			 $scope.errorMessageForInsured = translateMessages($translate, "eapp_vt.checkSignForSIInsured"); 
			 $scope.errorMessageForAgent = translateMessages($translate, "eapp_vt.checkSignForSIAgent"); 
			
		} else if ($scope.disableSignaturepad == false || ($scope.disableSignaturepad == true && signaturePadAgent.isEmpty())) {
       		$rootScope.showHideLoadingImage(false, "");   			
   			$scope.showErrorForSignature = true;			
			$scope.errorMessageForAgent = translateMessages($translate, "eapp_vt.checkSignForSIAgent"); 
		
       	} else if ($scope.disableClientSignaturepad == false || ($scope.disableClientSignaturepad == true  && signaturePadClient.isEmpty())) {
       		$rootScope.showHideLoadingImage(false, "");
			$scope.showErrorForSignature = true;
			$scope.errorMessageForInsured = translateMessages($translate, "eapp_vt.checkSignForSIInsured"); 
			
		} else { 
			$scope.showErrorForSignature = false;			
			if (isExpired) {
            	$rootScope.showHideLoadingImage(false, "");
            
            	$rootScope.lePopupCtrl.showError(
                	translateMessages($translate, "lifeEngage"),
                	translateMessages($translate, "illustrator.expiryDateMessageIllustration"),
                	translateMessages($translate, "illustrator.popUpClose"));
        	} else {
	            $rootScope.showHideLoadingImage(true, 'proceedingToEapp', $translate);
	            IllustratorVariables.illustrationStatus = "Confirmed";			
				IllustratorVariables.setIllustratorModel($scope.Illustration);
				$scope.refresh();
				var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
				obj.save(function (isAutoSave) {				             
	               $scope.$emit('saveCallback', IllustratorVariables);
					/*
					* Not Needed for Thailand Implementation - Starts
					*/
					
					/*var leadId = 0;
	                leadId = IllustratorVariables.leadId;
	                if (UtilityService.leadPage == 'LeadSpecificListing' || (leadId != '' && leadId != undefined)) {
	                    $location.path("/MyAccount/Illustration/" + leadId + '/0');
	                } else if (UtilityService.leadPage == 'GenericListing') {
	                    $location.path("/MyAccount/Illustration/0/0");
	                }*/
					/*
					* Not Needed for Thailand Implementation - Ends
					*/
					$scope.refresh();

				});
            	//converting signature image to base64 and sending for saving based on jellybean and other devices
				//Commenting for FGLI since there is no signature pad
				$scope.isSignature = true;
				var requirementType = "Signature";
				var documentType="ClientSignature";
				var partyIdentifier="";
	            var isMandatory="";
	            var docArray=[];
				if(!IllustratorVariables.RequirementFile.length >0) {
	    			var requirementName = "Signature";
				} else {
					var requirementName = IllustratorVariables.RequirementFile[0].requirementName;
				}
	    	/*	if (rootConfig.isDeviceMobile) {
	    								IllustratorService.getRequirementFilePath(sigDataClient,requirementName,requirementType,documentType,function(filePathClient){
	    								IllustratorService.saveRequirement(filePathClient,requirementType,partyIdentifier,isMandatory,docArray,documentType,$scope, $rootScope, DataService,
	    									$translate, UtilityService,
	    									IllustratorVariables, $routeParams, false, function(){
												documentType="AgentSignature";
												IllustratorService.getRequirementFilePath(sigDataAgent,requirementName,requirementType,documentType,function(filePathAgent){
													IllustratorService.saveRequirement(filePathAgent,requirementType,partyIdentifier,isMandatory,docArray,documentType,$scope, $rootScope, DataService,
														$translate, UtilityService,IllustratorVariables, $routeParams, false, function(){
														$rootScope.showHideLoadingImage(false, "");
													});
												});
											});     
	    								});
	    							 }else{
										IllustratorService.saveRequirement(sigDataClient,requirementType,partyIdentifier,isMandatory,docArray,documentType,$scope, $rootScope, DataService,
											$translate, UtilityService,
											IllustratorVariables, $routeParams,false, function(){
											documentType="AgentSignature";
											IllustratorService.saveRequirement(sigDataAgent,requirementType,partyIdentifier,isMandatory,docArray,documentType,$scope, $rootScope, DataService,
												$translate, UtilityService,
												IllustratorVariables, $routeParams,false, function(){
												$rootScope.showHideLoadingImage(false, "");
											});
										});
									}
								 if($scope.isJellyBean){
									var isEmpty=false;
									var sigElementClientSig = $('#clientSig').signaturePad();
									var sigDataEmptyClient = sigElementClientSig.getSignatureString();
									var sigElementAgentSig = $('#agentSig').signaturePad();
									var sigDataEmptyAgent = sigElementAgentSig.getSignatureString();
									
									if(sigDataEmptyClient == "[]" || sigDataEmptyAgent == "[]"){
										isEmpty=true;
										$rootScope.lePopupCtrl.showError("",translateMessages($translate,"illustrationSignMessage"),translateMessages($translate,"general.ok"));
										//$rootScope.lePopupCtrl.showError("",translateMessages($translate,"illustrationSignMessage"),translateMessages($translate,"general.ok"));
									}else{
										$rootScope.showHideLoadingImage(true, "confirming", $translate);
										IllustratorVariables.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
										var canvas = angular.element("#sign");
										var sigData = canvas[0].toDataURL();
										isEmpty=false;
									}
									
								 }else{
									 if (signaturePadClient.isEmpty() || signaturePadAgent.isEmpty()) {
										 isEmpty=true;
									//	$rootScope.lePopupCtrl.showError("",translateMessages($translate,"illustrationSignMessage"),translateMessages($translate,"general.ok"));
									} else {
										isEmpty=false;
										$rootScope.showHideLoadingImage(true, "confirming", $translate);
										IllustratorVariables.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
										var sigElement = $('.sigPadIllustration')
										.signaturePad();
										var sigData = signaturePad.toDataURL();
									}
								 }
								if(!isEmpty){
									$scope.isSignature = true;
									var requirementType = "Signature";
									var documentType="Signature";
									var partyIdentifier="";
	                                var isMandatory="";
	                                var docArray=[];
									if(!IllustratorVariables.RequirementFile.length >0) {
	    								var requirementName = "Signature";
									} else {
									    var requirementName = IllustratorVariables.RequirementFile[0].requirementName;
									}
	    							 if (eval(rootConfig.isDeviceMobile)) {
	    								IllustratorService.getRequirementFilePath(sigData,requirementName,requirementType,documentType,function(filePath){
	    								IllustratorService.saveRequirement(filePath,requirementType,partyIdentifier,isMandatory,docArray,documentType,$scope, $rootScope, DataService,
	    									$translate, UtilityService,
	    									IllustratorVariables, $routeParams
	    								);     
	    								});
	    							 }else{
										IllustratorService.saveRequirement(sigData,requirementType,partyIdentifier,isMandatory,docArray,documentType,$scope, $rootScope, DataService,
											$translate, UtilityService,
											IllustratorVariables, $routeParams);
								 }
								} */
								 
							//}
    							//$rootScope.showHideLoadingImage(false);
								/*$timeout(function(){
									$rootScope.showHideLoadingImage(false);
								}, 1000);*/
	        	}
	        $scope.disableSignaturepad = true;
	        $scope.disableConfirmbutton = true;
	//        $scope.disableCheckboxConfirm = true;
	        $rootScope.disableControl = true;
    	}
		
	}
	$scope.navigateToConsentPage = function () {
		$location.path('/consentPage');
	}
	$scope.consentDeclaration = function () {
		var url = rootConfig.summaryDeclaration;
		if (!rootConfig.isDeviceMobile) {
			window.open(url, '_blank');
		}
		else {
			window.open(url, '_system');
		}
	}

    $scope.loadSignature = function () {
 
    	var padCheckFirstload = $scope.signaturerequirement!=null ?$scope.signaturerequirement:"";
		var padFirstKey = padCheckFirstload.split('_');
		
    	
		if(padFirstKey[0]=="Client"){
        if (angular.element("#clientSigNew")) {
			var wrapper = angular.element("#clientSigNew");
				var canvas = wrapper[0].querySelector("canvas");
				var dpr = window.devicePixelRatio;
				window.devicePixelRatio = 1;
				if($scope.signatureBase64string != undefined) {
					signaturePadClient.fromDataURL($scope.signatureBase64string);
					$scope.disableClientSignaturepad = true;
					$scope.disableCanvasClientpad=true;
				}
				window.devicePixelRatio = dpr;

		} else {
			$timeout($scope.loadSignature(), 10);
		}
		}else if (padFirstKey[0]=="Agent"){
			   if (angular.element("#agentSigNew")) {
	    			var wrapper = angular.element("#agentSigNew");
	    				var canvas = wrapper[0].querySelector("canvas");
	    				var dpr = window.devicePixelRatio;
	    				window.devicePixelRatio = 1;
	    				if($scope.signatureBase64string != undefined) {
	    						signaturePadAgent.fromDataURL($scope.signatureBase64string);
	    						$scope.disableSignaturepad = true;
	    						$scope.disableCanvasAgentpad=true;
	    				}
	    				window.devicePixelRatio = dpr;

	    		} else {
	    			$timeout($scope.loadSignature(), 10);
	    		}
		}
    };
    
    
    $scope.loadSignature1 = function () {
      
            if (angular.element("#agentSigNew")) {
    			var wrapper = angular.element("#agentSigNew");
    				var canvas = wrapper[0].querySelector("canvas");
    				var dpr = window.devicePixelRatio;
    				window.devicePixelRatio = 1;
    				if($scope.signatureBase64string != undefined) {
    			//	$scope.signature.fromDataURL($scope.signatureBase64string);
    					signaturePadAgent.fromDataURL($scope.signatureBase64string);
    				}
    				window.devicePixelRatio = dpr;

    		} else {
    			$timeout($scope.loadSignature(), 10);
    		}
        };
        
	 

    /* Extending function for conditional check of loadingof signature */
    $scope.onPaintUISuccess = function (callBackId) {
        $rootScope.showHideLoadingImage(false);
        $scope.refresh();
       /* if (IllustratorVariables.illustrationStatus == "Confirmed") {
			$scope.loadSignature();
		 }else{
			$scope.clearSignature('clientSig');
			$scope.clearSignature('agentSig');
			$scope.clearSignature('lifeAssuredSig');
			
        }*/
		//Commenting for FGLI since there is no siganture pad	
	/*	if(IllustratorVariables.illustrationStatus != "Confirmed"){
			$scope.clearSignature('clientSigNew');
			$scope.clearSignature('agentSigNew');
			//$scope.clearSignature('lifeAssuredSig');
		}*/
    }
    $scope.setSavedSigIllustration = function () {
        if (!jQuery.isEmptyObject($scope.IllustrationAttachment.Upload.Signature)) {
            $scope.sigPadApiClient = $('#clientSigNew').signaturePad({
                displayOnly: true
            });
            $scope.sigPadApiAgent = $('#agentSigNew').signaturePad({
                displayOnly: true
            });
            /*$scope.sigPadApiLifeAssured = $('#lifeAssuredSig').signaturePad({
                displayOnly: true
            });*/
            if ($scope.IllustrationAttachment.Upload.Signature[0].pages.length == 1) {
                $scope.IllustrationAttachment.Upload.Signature[0].pages[1] = {};
                //$scope.IllustrationAttachment.Upload.Signature[0].pages[2] = {};
            }
            $scope.setImageToScope($scope.IllustrationAttachment.Upload.Signature[0]);
        }
			 }
    
    $scope.setImageToScope = function(imageToBind) {
		 var docObj = $scope.mapDocScopeToPersistence(imageToBind);
		 if((rootConfig.isDeviceMobile)) {
		 	for(var i = 0; i < $scope.IllustrationAttachment.Upload.Signature[0].pages.length; i++) {
				 if (docObj.documentObject.pages[i].fileContent && docObj.documentObject.pages[i].fileType) {
					 $scope.setSignature(docObj.documentObject.pages[i].fileContent, docObj.documentObject.pages[i].fileType);
				 }
		 	}
		 } else {
			 if(docObj.fileObj) {
			 	for(var i = 0; i < $scope.IllustrationAttachment.Upload.Signature[0].pages.length; i++) {
					 if(docObj.fileObj.pages[i].fileContent && docObj.fileObj.pages[i].fileContent.length > 0) {
						 $scope.setSignature(docObj.fileObj.pages[i].fileContent, docObj.fileObj.pages[i].fileType);
					 }
			 	}
			 }
		 }
		 $scope.refresh();
	 }

    $scope.setSignature = function (signatureData, signatureType) {
        var sig = JSON.parse(signatureData);
        if (signatureType == 'clientSig') {
            $scope.sigPadApiClient.regenerate(sig);
        } else if (signatureType == 'agentSig') {
            $scope.sigPadApiAgent.regenerate(sig);
        }
        $scope.refresh();
		 }
    

    $scope.clearSignature = function (signature_type) {
    	$scope.enableProceedToEapp=false;
      if (IllustratorVariables.illustrationStatus!== "Confirmed" && !(IllustratorVariables.illustrationStatus == "Completed" && $rootScope.haseApp == true)) {
    	
        if (rootConfig.isDeviceMobile) {
            $scope.disableConfirmbutton = true;
			if($scope.isJellyBean){
				if(signature_type == "clientSig"){
					$('#clientSigNew').signaturePad().clearCanvas();
				}else{
					$('#agentSigNew').signaturePad().clearCanvas();
				}
			}else{
				var tableName = "RequirementFiles";
				if(signature_type == "clientSig"){
					$('#clientSigNew').signaturePad().clearCanvas();
					signature="Signature";
					var fileName="Client_Signature"
					outputFormat="image/png";					
					IllustratorService.deleteRequirementFiles(tableName,IllustratorVariables.transTrackingID,fileName,"","",function(sig){
						
					});		
					signaturePadClient.clear();
					sigDataClient = "";
					$scope.disableCanvasClientpad=false;
                    $scope.disableClientSignaturepad = false;			
				}else{
					$('#agentSigNew').signaturePad().clearCanvas();
					var fileName="Agent_Signature"
                    IllustratorService.deleteRequirementFiles(tableName,IllustratorVariables.transTrackingID,fileName,"","",function(sig){
						
					});	
					signaturePadAgent.clear();
					sigDataAgent = "";
					$scope.disableCanvasAgentpad=false;
                   	$scope.disableSignaturepad = false;
				}
			}
            
        } else {
            if ($scope.isRDSUser) {
                $scope.disableConfirmbutton = false;
            } else {
                $scope.disableConfirmbutton = true;
            }
			if(signature_type == "clientSig"){
				$('#clientSigNew').signaturePad().clearCanvas();
				signaturePadClient.clear();
				sigDataClient = "";
				$scope.disableCanvasClientpad = false;
                $scope.disableClientSignaturepad = false;
			} else{
				$('#agentSigNew').signaturePad().clearCanvas();
				signaturePadAgent.clear();
				sigDataAgent = "";
				$scope.disableCanvasAgentpad = false;
                $scope.disableSignaturepad = false;
			}
        }
      }else {
	        $scope.disableSignaturepad = true;
	        $scope.disableClientSignaturepad = true;
	        $scope.disableCanvasClientpad = true;
	        $scope.disableCanvasAgentpad =true;
      }
    }


    // Function used for mapping two signatures to corresponding models
    if (rootConfig.isDeviceMobile) {
        $scope.disableConfirmbutton = true;
    } else {
        if ($scope.isRDSUser) {
            if (IllustratorVariables.illustrationStatus == "Confirmed") {
                $scope.disableConfirmbutton = true;
            } else {
                $scope.disableConfirmbutton = false;
            }
        } else {
            $scope.disableConfirmbutton = true;
        }
    }

    // mapping signature to eaap_attachment on ok click of each signature
   /* $scope.confirmSignatureLocally = function (button_id, signature_id) {
        var sigElement = $('#' + signature_id).signaturePad();
        var sigData = sigElement.getSignatureString();
        var currAgentId = $rootScope.agentId == null ? $rootScope.username : $rootScope.agentId;
        var document = {};
        document = $scope.mapDocScopeToPersistence($scope.IllustrationAttachment.Upload.Signature[0]);
        var d = new Date();
        var n = d.getTime();
        var name = "document";
        var newFileName = "Agent1" + n + "_" + name;
        if (rootConfig.isDeviceMobile) {
            if (!document.documentName || (document.documentName && document.documentName == '')) {
                document.documentName = newFileName;
                document.documentType = "Signature";
                document.documentObject.documentName = newFileName;
                document.documentObject.documentType = "Signature";
                document.documentObject.documentStatus = "Saved";
            }

            if ((document.documentObject.pages[0].fileContent == '[]' || document.documentObject.pages[0].fileContent == '' || !document.documentObject.pages[0].fileContent || document.documentObject.pages[0].fileContent != sigData || typeof document.documentObject.pages[0].fileType == undefined) && (document.documentObject.pages[0].fileType == signature_id || document.documentObject.pages[0].fileType == "" || typeof document.documentObject.pages[0].fileType == "undefined")) {
                document.documentObject.pages[0].fileName = newFileName;
                document.documentObject.pages[0].fileContent = sigData;
                document.documentObject.pages[0].fileType = signature_id;
            } else if (document.documentObject.pages[0].fileType != signature_id && document.documentObject.pages[0].fileContent != '' && (document.documentObject.pages[1].fileContent == '[]' || document.documentObject.pages[1].fileContent == '')) {
                document.documentObject.pages[1].fileName = newFileName;
                document.documentObject.pages[1].fileContent = sigData;
                document.documentObject.pages[1].fileType = signature_id;
            } 
            $scope.document = document;
        } else {
            if (!angular.equals({}, $scope.IllustrationAttachment.Upload.Signature[0])) {
                //if (!jQuery.isEmptyObject($scope.IllustrationAttachment.Upload.Signature[0])) {
                if (($scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent == '[]' || $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent == '' || !$scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent || $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent != sigData || typeof $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileType == undefined) && ($scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileType == signature_id || $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileType == "" || typeof $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileType == "undefined")) {
                    if ($scope.IllustrationAttachment.Upload.Signature[0].documentName == "") {
                        $scope.IllustrationAttachment.Upload.Signature[0].documentName = newFileName;
                    }
                    $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent = sigData;
                    $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileName = newFileName;
                    $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileType = signature_id;
                    $scope.IllustrationAttachment.Upload.Signature[0].documentStatus = "Saved";
                }
                //}
                else if ($scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileType != signature_id && $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent != '' && ($scope.IllustrationAttachment.Upload.Signature[0].pages[1].fileContent == '[]' || $scope.IllustrationAttachment.Upload.Signature[0].pages[1].fileContent == '')) {
                    $scope.IllustrationAttachment.Upload.Signature[0].pages[1].fileContent = sigData;
                    $scope.IllustrationAttachment.Upload.Signature[0].pages[1].fileName = newFileName;
                    $scope.IllustrationAttachment.Upload.Signature[0].pages[1].fileType = signature_id;
                    $scope.IllustrationAttachment.Upload.Signature[0].documentStatus = "Saved";
                } else if ($scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileType != signature_id && $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent != '' && $scope.IllustrationAttachment.Upload.Signature[0].pages[1].fileType != signature_id && $scope.IllustrationAttachment.Upload.Signature[0].pages[1].fileContent != '') {
                    $scope.IllustrationAttachment.Upload.Signature[0].pages[2].fileContent = sigData;
                    $scope.IllustrationAttachment.Upload.Signature[0].pages[2].fileName = newFileName;
                    $scope.IllustrationAttachment.Upload.Signature[0].pages[2].fileType = signature_id;
                    $scope.IllustrationAttachment.Upload.Signature[0].documentStatus = "Saved";
                }
                $scope.document = document;
            }
        }
        if ($scope.Illustration.Product.ProductDetails.productCode == rootConfig.showSinaturePadFor) {
            if (checkIfFileContentIsValid($scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent) &&
                checkIfFileContentIsValid($scope.IllustrationAttachment.Upload.Signature[0].pages[1].fileContent)) {
                $scope.disableConfirmbutton = false;
            }
        } else {
            if (checkIfFileContentIsValid($scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent) &&
                checkIfFileContentIsValid($scope.IllustrationAttachment.Upload.Signature[0].pages[1].fileContent)) {
                $scope.disableConfirmbutton = false;
            }
        }
        IllustratorVariables.setIllustrationAttachmentModel($scope.IllustrationAttachment);
		if(signature_id == "clientSig"){
			sigDataClient = signaturePadClient.toDataURL();
		} else{
			sigDataAgent = signaturePadAgent.toDataURL();
		}
		if ($scope.Illustration.Product.ProductDetails.productCode == rootConfig.showSinaturePadFor) {
            if (sigDataClient && sigDataAgent) {
                $scope.disableConfirmbutton = false;
            }
        } else {
            if (sigDataClient && sigDataAgent) {
                $scope.disableConfirmbutton = false;
            }
        }
		
		$scope.isSignature = true;
		var requirementType = "Signature";
		var documentType="ClientSignature";
		var partyIdentifier="";
        var isMandatory="";
        var docArray=[];
		if(!IllustratorVariables.RequirementFile.length >0) {
			var requirementName = "Signature";
		} else {
			var requirementName = IllustratorVariables.RequirementFile[0].requirementName;
		}
				IllustratorService.saveRequirement(sigDataClient,requirementType,partyIdentifier,isMandatory,docArray,documentType,$scope, $rootScope, DataService,
				$translate, UtilityService,
				IllustratorVariables, $routeParams,$scope.saveReqSuccess,$scope.saveReqErr,$scope.saveReqSuccess,$scope.saveReqErr);
				$scope.refresh();
	}*/
	
	//Working code 
	$scope.confirmSignatureLocally = function(button_id, signature_id) {
		
		var sigElement = $('#' + signature_id).signaturePad();
		var signatureBase64string = signaturePadClient.toDataURL();
        if (!(rootConfig.isDeviceMobile)) {
            $rootScope.showHideLoadingImage(true,"confirming", $translate);
        }
		
			IllustratorVariables.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
			$scope.isSignature = true;
			var requirementType = "Signature";
			var requirmentSelect = "Client";
			var documentType="ClientSignature";
			var partyIdentifier="";
            var isMandatory="";
            var docArray=[];
			if(!IllustratorVariables.RequirementFile.length >0) {
				var requirementName = "Signature";
			} else {
			    var requirementName = IllustratorVariables.RequirementFile[0].requirementName;
			}
			if(!signaturePadClient.isEmpty()){
				if (rootConfig.isDeviceMobile) {
					IllustratorService.getRequirementFilePath(signatureBase64string,requirementName,requirementType,documentType,function(filePath){
						IllustratorService.saveRequirement(filePath,requirementType,partyIdentifier,isMandatory,docArray,documentType,$scope, $rootScope, DataService,
							$translate, UtilityService,
							IllustratorVariables, $routeParams,false,$scope.saveReqSuccess,$scope.saveReqErr,requirmentSelect
						);
					});
				}else{
					IllustratorService.saveRequirement(signatureBase64string,requirementType,partyIdentifier,isMandatory,docArray,documentType,$scope, $rootScope, DataService,
						$translate, UtilityService,
					IllustratorVariables, $routeParams,false,$scope.saveReqSuccess,$scope.saveReqErr,requirmentSelect);
			 	}
				$scope.disableClientSignaturepad = true;
			} else {
				$rootScope.showHideLoadingImage(false,"");
			}
		$scope.enableProceedToEapp=false;
		$scope.refresh();
	};
	
	
	
$scope.confirmSignatureAgent = function(button_id, signature_id) {
		
		var sigElement = $('#' + signature_id).signaturePad();
		var signatureBase64string = signaturePadAgent.toDataURL();
	    if (!(rootConfig.isDeviceMobile)) {
	        $rootScope.showHideLoadingImage(true,"confirming", $translate);
	    }
		
			IllustratorVariables.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
			$scope.isSignature = true;
			var requirementType = "Signature";
			var documentType="AgentSignature";
			var requirmentSelect = "Agent";
			var partyIdentifier="";
            var isMandatory="";
            var docArray=[];
			if(!IllustratorVariables.RequirementFile.length >0) {
				var requirementName = "Signature";
			} else {
			    var requirementName = IllustratorVariables.RequirementFile[0].requirementName;
			}
			if(!signaturePadAgent.isEmpty()){
				if (rootConfig.isDeviceMobile) {
					IllustratorService.getRequirementFilePath(signatureBase64string,requirementName,requirementType,documentType,function(filePath){
						IllustratorService.saveRequirement(filePath,requirementType,partyIdentifier,isMandatory,docArray,documentType,$scope, $rootScope, DataService,
							$translate, UtilityService,
							IllustratorVariables, $routeParams,false,$scope.saveReqSuccess,$scope.saveReqErr,requirmentSelect
						);
					});
				 }else{
					IllustratorService.saveRequirement(signatureBase64string,requirementType,partyIdentifier,isMandatory,docArray,documentType,$scope, $rootScope, DataService,
						$translate, UtilityService,IllustratorVariables, $routeParams,false,$scope.saveReqSuccess,$scope.saveReqErr,requirmentSelect);
			 	}
				$scope.disableSignaturepad = true;
			} else {
				$rootScope.showHideLoadingImage(false,"");
			}
			$scope.enableProceedToEapp=false;
			$scope.refresh();
			
	};
	
	
	/*$scope.confirmSignatureLocally = function(button_id, signature_id) {
		 if($scope.isJellyBean){
			 var sigElement = $('#' + signature_id).signaturePad();
			 var sigData = sigElement.getSignatureString();
			 var currAgentId = $rootScope.agentId == null ? $rootScope.username : $rootScope.agentId;
			 var document = {};
			 document = $scope.mapDocScopeToPersistence($scope.IllustrationAttachment.Upload.Signature[0]);
			 var d = new Date();
			 var n = d.getTime();
			 var name = "document";
			 var newFileName = "Agent1" + n + "_" + name;
			 if (eval(rootConfig.isDeviceMobile)) {
				 if (!document.documentName || (document.documentName && document.documentName == '')) {
					 document.documentName = newFileName;
					 document.documentType = "Signature";
					 document.documentObject.documentName = newFileName;
					 document.documentObject.documentType = "Signature";
					 document.documentObject.documentStatus = "Saved";
				 } Currently invalid scenario as we wont be able to edit the signature after confirming
				 else {
					 DataService.getDocument(document,function(doc) {
								 document.id = doc[0].Id;
								 document.parentId = doc[0].ParentId;
								 document.documentType = doc[0].DocumentType;
								 document.documentName = doc[0].DocumentName;
								 document.documentStatus = doc[0].DocumentStatus;
								 document.date = doc[0].CreatedDate;
								 document.documentObject.documentName = doc[0].DocumentName;
								 document.documentObject.documentType = doc[0].DocumentType;
								 document.documentObject.documentStatus = doc[0].DocumentStatus;
							 }, $scope.operationError);
				 }

				 if ((document.documentObject.pages[0].fileContent == '[]' || document.documentObject.pages[0].fileContent == '' || !document.documentObject.pages[0].fileContent || document.documentObject.pages[0].fileContent != sigData || typeof document.documentObject.pages[0].fileType == undefined) && (document.documentObject.pages[0].fileType == signature_id ||document.documentObject.pages[0].fileType=="" || typeof document.documentObject.pages[0].fileType == "undefined")) {
					 document.documentObject.pages[0].fileName = newFileName;
					 document.documentObject.pages[0].fileContent = sigData;
					 document.documentObject.pages[0].fileType = signature_id;
				 } else if (document.documentObject.pages[0].fileType != signature_id && document.documentObject.pages[0].fileContent != '') {
					 document.documentObject.pages[1].fileName = newFileName;
					 document.documentObject.pages[1].fileContent = sigData;
					 document.documentObject.pages[1].fileType = signature_id;
				 }
				 $scope.document = document;
			 } else {
				 //if (!jQuery.isEmptyObject($scope.IllustrationAttachment.Upload.Signature[0])) {
					 if($scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent != sigData) {
						 if(($scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent == '[]' || $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent == '' || !$scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent || $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent != sigData || typeof $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileType == undefined) && ($scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileType == signature_id ||$scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileType=="" || typeof $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileType == "undefined")) {
							 if($scope.IllustrationAttachment.Upload.Signature[0].documentName == "") {
								 $scope.IllustrationAttachment.Upload.Signature[0].documentName = newFileName;
							 }
							 $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent = sigData;
							 $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileName = newFileName;
							 $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileType = signature_id;
							 $scope.IllustrationAttachment.Upload.Signature[0].documentStatus = "Saved";
						 }
					 }
					 if($scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileType != signature_id && $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent != ''){
						 $scope.IllustrationAttachment.Upload.Signature[0].pages[1].fileContent = sigData;
						 $scope.IllustrationAttachment.Upload.Signature[0].pages[1].fileName = newFileName;
						 $scope.IllustrationAttachment.Upload.Signature[0].pages[1].fileType = signature_id;
						 $scope.IllustrationAttachment.Upload.Signature[0].documentStatus = "Saved";
					 }
					 $scope.document = document;
				// }
			 }
			 if ($scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent != ''
					 && $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent != '[]'
					 && $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent != undefined
					 && $scope.IllustrationAttachment.Upload.Signature[0].pages[1].fileContent != ''
					 && $scope.IllustrationAttachment.Upload.Signature[0].pages[1].fileContent != '[]'
					 && $scope.IllustrationAttachment.Upload.Signature[0].pages[1].fileContent != undefined) {
				 $scope.disableConfirmbutton = false;
			 }
			 IllustratorVariables.setIllustrationAttachmentModel($scope.IllustrationAttachment);
		 }else{
					 IllustratorVariables.illustratorKeys.Key15 = IllustratorVariables.getStatusOptions()[1].status;
					 if(signature_id=="clientSig"){
						 if (!(signaturePadClient.isEmpty())) {
							 $scope.sigDataClient = signaturePadClient.toDataURL();
							 $scope.clientSigAdded=true;
						 }
					 }else if(signature_id=="agentSig"){
						 if (!(signaturePadAgent.isEmpty())) {
							 $scope.sigDataAgent = signaturePadAgent.toDataURL();
							 $scope.agentSigAdded=true;
						 }
					 }
			 if (eval(rootConfig.isDeviceMobile)) {
				 $scope.newSigDataArr=[];

					 if (!(signaturePadClient.isEmpty())) {
						 $scope.newSigDataArr.push($scope.sigDataClient);
					 }
					 if(!(signaturePadAgent.isEmpty())){
						 $scope.newSigDataArr.push($scope.sigDataAgent);
					 }
			 }else{
				 $scope.newSigDataArrWeb=[];
					 if (!(signaturePadClient.isEmpty())) {
						 var sigDataClientObj={
								 "type":"clientSig",
								 "data":$scope.sigDataClient
						 }
						 $scope.newSigDataArrWeb.push(sigDataClientObj);
					 }
					 if(!(signaturePadAgent.isEmpty())){
						 var sigDataAgentObj={
								 "type":"agentSig",
								 "data":$scope.sigDataAgent
						 }
						 $scope.newSigDataArrWeb.push(sigDataAgentObj);
					 }
			 }
					 if($scope.clientSigAdded &&  $scope.agentSigAdded){
						 $scope.disableConfirmbutton = false;
					 }
					// $scope.confirmSignature1();
			 $timeout(function(){
				 $rootScope.showHideLoadingImage(false);
			 }, 1000);
			 $scope.refresh();

		 }
	 }
*/
	
	 /*$scope.confirmSignature = function() {
			 if($scope.isJellyBean){
				 if (eval(rootConfig.isDeviceMobile)) {
					 $scope.onLocalFileWriteSuccess($scope.document, function(fileData, results) {
						if(results != null && results.insertId != null) {
							fileData.id = results.insertId != null ? results.insertId : null;
						}
						$scope.IllustrationAttachment.Upload.Signature[0] = fileData.documentObject;
						IllustratorVariables.setIllustrationAttachmentModel($scope.IllustrationAttachment);
						var obj = new GLI_IllustratorService.saveTransactions($scope,$rootScope,DataService,$translate,UtilityService,IllustratorVariables,$routeParams,false);
						obj.save(function(isAutoSave) {
							$rootScope.NotifyMessages(false,"successMessage",$translate);
							$scope.refresh();
						});
					 }, $scope.operationError);
				 } else {
					 if($scope.IllustrationAttachment.Upload.Signature[0].documentStatus != "Synced") {
						 $rootScope.showHideLoadingImage(true,'saveTransactionMessage',$translate);
						 var docData = [];
						 docData.push(angular.copy($scope.IllustrationAttachment.Upload.Signature[0]));
						 var docDetails = {
								 Documents : docData[0]
						 };
						 PersistenceMapping.clearTransactionKeys();
						 IllustratorService.mapKeysforPersistence($scope,IllustratorVariables);
						 PersistenceMapping.dataIdentifyFlag = false;
						 var docDetailsWithKeys = PersistenceMapping
						 .mapScopeToPersistence(docDetails);
						 var uploadDocIndex = 0;
						 DocumentService.uploadDocuments(docDetailsWithKeys, uploadDocIndex, function(data) {
							 log.trace("Web - Upload Documents Service => BIIndividualSync", "Success", data.TransTrackingID);
							 $scope.IllustrationAttachment.Upload.Signature[0].documentStatus = "Synced";
							 IllustratorVariables.setIllustrationAttachmentModel($scope.IllustrationAttachment);
							 $rootScope.showHideLoadingImage(false, "");
						}, $scope.operationError);
					 } else {

					 }
				}

			 }else{
				 IllustratorVariables.newSignFlow="true";
				 $scope.isSignature = true;
				 var requirementType = "Signature";
				 var documentType;
				 var partyIdentifier="";
				 var isMandatory="";
				 var docArray=[];
				 IllustratorVariables.RequirementFile=[]
				 var requirementName = "Signature";
				 if (eval(rootConfig.isDeviceMobile)) {
					 documentType='clientSignature';
					 IllustratorService.getRequirementFilePath($scope.sigDataClient,requirementName,requirementType,documentType,function(filePathClient){
						 log.info("Get Requirement File Path - base64 Signature : "+ $scope.sigDataClient +" Document Type: "+documentType+"=> BIIndividualSync", "Success");
						 if(eval(rootConfig.isDeviceMobile) && rootConfig.isOfflineDesktop){
							 filePathClient = "../"+filePathClient;
						 }
						 IllustratorService.saveRequirement(filePathClient,requirementType,partyIdentifier,isMandatory,docArray,documentType,$scope, $rootScope, DataService,
								 $translate, UtilityService,
								 IllustratorVariables, $routeParams, false, function(){
							 documentType='agentSignature';
							 IllustratorService.getRequirementFilePath($scope.sigDataAgent,requirementName,requirementType,documentType,function(filePathAgent){
								 log.info("Get Requirement File Path - base64 Signature : "+ $scope.sigDataAgent +" Document Type: "+documentType+"=> BIIndividualSync", "Success");
								 if(eval(rootConfig.isDeviceMobile) && rootConfig.isOfflineDesktop){
									 filePathAgent = "../"+filePathAgent;
								 }
								 IllustratorService.saveRequirement(filePathAgent,requirementType,partyIdentifier,isMandatory,docArray,documentType,$scope, $rootScope, DataService,
										 $translate, UtilityService,
										 IllustratorVariables, $routeParams,false, function(){

								 }, function(){

								 });
							 });

						 }, function(){

						 });
					 });
				 }else{
					 var docDataToUpload='';
					 if($scope.newSigDataArrWeb[0].type=="clientSig"){
						 documentType='clientSignature';
						 docDataToUpload=$scope.newSigDataArrWeb[0].data;
					 }
						 IllustratorService.saveRequirement(docDataToUpload,requirementType,partyIdentifier,isMandatory,docArray,documentType,$scope, $rootScope, DataService,
							 $translate, UtilityService,IllustratorVariables, $routeParams, false, function(){
							 if($scope.newSigDataArrWeb[0].type=="clientSig"){
								 documentType='clientSignature';
								 docDataToUpload=$scope.newSigDataArrWeb[0].data;
							 }
							
							 IllustratorService.saveRequirement(docDataToUpload,requirementType,partyIdentifier,isMandatory,docArray,documentType,$scope, $rootScope, DataService,
									 $translate, UtilityService,
									 IllustratorVariables, $routeParams, false, function(){
								 if($scope.newSigDataArrWeb[1].type=="agentSig"){
									 documentType='agentSignature';
									 docDataToUpload=$scope.newSigDataArrWeb[1].data;
								 }
							 });
						 });
				 }

			 }
			
		 $scope.disableSignaturepad = true;
		 $scope.disableConfirmbutton=true;
	} */
	
	
	
	
	$scope.saveReqSuccess = function(){
		$rootScope.showHideLoadingImage(false);
	};
	$scope.saveReqErr = function(){
		$rootScope.showHideLoadingImage(false);
	};

    function checkIfFileContentIsValid(fileContent) {
        if (fileContent != '' &&
            fileContent != '[]' &&
            fileContent != undefined) {
            return true;
        } else {
            return false;
        }
    }


    $scope.getDisplayValue = function (jsonArray) {
        var isVisible = false;
        if (jsonArray.length > 0) {

            isVisible = true;
        } else {

            isVisible = false;
        }
        return isVisible;
    }

    // Overrided saveillustration_info to avoid unwanted signature save on save&exit button
    $scope.saveIllustrationInfo = function () {
        if (IllustratorVariables.illustrationStatus!== "Confirmed" && !(IllustratorVariables.illustrationStatus == "Completed" && $rootScope.haseApp == true)) {
        $rootScope.showHideLoadingImage(true, 'saveTransactionMessage', $translate);
        /* Defect -3760 - starts*/
        globalService.setCPInsured($scope.Illustration.Insured);
		globalService.setCPPayer($scope.Illustration.Payer);
		globalService.setCPPrivacyLaw($scope.Illustration.PrivacyLaw);
		
        /* Defect -3760 - ends*/
        IllustratorVariables.setIllustratorModel($scope.Illustration);
        $scope.clickSaveForOnce = true;
        $scope.enableProceedToEapp=true;
        if(IllustratorVariables.illustrationStatus == "Draft" && !signaturePadClient.isEmpty() && !signaturePadAgent.isEmpty() && $scope.enableProceedToEapp == true){
        	IllustratorVariables.illustrationStatus = "Completed";
        }
        if(IllustratorVariables.illustrationStatus == "Completed" && (signaturePadClient.isEmpty() || signaturePadAgent.isEmpty())){
        	IllustratorVariables.illustrationStatus = "Draft";
        }
    	
        $scope.refresh();
        var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);        
        obj.save(function (isAutoSave) {   
            $rootScope.showHideLoadingImage(false); 
            if ((rootConfig.isDeviceMobile)) {
                        
                        if (!isAutoSave) {
                            $rootScope.NotifyMessages(false, "saveIllustrationSuccessOffline", $translate);
                        }
                    } else {
                        if (!isAutoSave) {
                            $rootScope.NotifyMessages(false, "saveIllustrationSuccessOffline", $translate);
                        }
                    }
//            var leadId = 0;
//            leadId = IllustratorVariables.leadId;
//            if (UtilityService.leadPage == 'LeadSpecificListing' || leadId) {
//                $location.path("/MyAccount/Illustration/" + leadId + '/0');
//            } else if (UtilityService.leadPage == 'GenericListing') {
//                $location.path("/MyAccount/Illustration/0/0");
//            }
//            $scope.refresh();
        }); 
        }
    }
    
    $scope.exitIllustrationFromIllustrationInfo = function () {
        $scope.lePopupCtrl.showWarning(
						translateMessages($translate,
								"lifeEngage"),
						translateMessages($translate,
								"pageNavigationText"),  
						translateMessages($translate,
								"fna.navok"),$scope.okPressed,translateMessages($translate,
								"general.navproceed"),$scope.backToListing);
    }
    
    $scope.backToListing=function(){
        var leadId = 0;
        leadId = IllustratorVariables.leadId;
        if (UtilityService.leadPage == 'LeadSpecificListing' || (leadId != '' && leadId != undefined)) {
            $location.path("/MyAccount/Illustration/" + leadId + '/0');
        } else if (UtilityService.leadPage == 'GenericListing') {
            $location.path("/MyAccount/Illustration/0/0");
        }else{
            $location.path("/MyAccount/Illustration/0/0");
        }
        $scope.refresh();
    }

    $scope.printPDFfile = function () {
      if(IllustratorVariables.illustrationStatus!== "Confirmed" && IllustratorVariables.illustrationStatus!== "Completed" && !$scope.clickSaveForOnce){
            $scope.leadDetShowPopUpMsg = true;
            $scope.enterMandatory = translateMessages($translate, "illustrator.saveBeforePrint");
        } else {
	        if ($scope.Illustration.Product.templates != undefined &&
	            $scope.Illustration.Product.templates != null &&
	            $scope.Illustration.Product.templates.illustrationPdfTemplate != undefined &&
	            $scope.Illustration.Product.templates.illustrationPdfTemplate != null &&
	            $scope.Illustration.Product.templates.illustrationPdfTemplate.length != 0) {
	            var templatelength = $scope.Illustration.Product.templates.illustrationPdfTemplate.length;
	            var selectedLanguage = "th";
	            for (var i = 0; i < templatelength; i++) {
	            	$scope.Illustration.Product.templates.illustrationPdfTemplate[i].language = selectedLanguage
	                //if ($scope.Illustration.Product.templates.illustrationPdfTemplate[i].language != selectedLanguage)
	                /*$scope.templateId = $scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateId;*/
	               if (rootConfig.isDeviceMobile) {
						if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='GenProLife.jasper'){
							$scope.templateId = '1008';
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='GenBumnan.jasper'){
							$scope.templateId = '1004';
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='GenCompletec.jasper'){
							$scope.templateId = '1005';
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='GenSave.jasper'){
							$scope.templateId = '1010';
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='1P05.jasper'){
	                        $scope.templateId = "1029";
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='4P10.jasper'){
	                        $scope.templateId = "1031";
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='4P04.jasper'){
	                        $scope.templateId = "1033";
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='4N85.jasper'){
							$scope.templateId = '1025';
	               		}
	               } else {
	            	  // $scope.templateId = $scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateId;
	            	   	if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='GenProLife.jasper'){
							$scope.templateId = '1008';
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='GenBumnan.jasper'){
							$scope.templateId = '1004';
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='GenCompletec.jasper'){
							$scope.templateId = '1005';
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='GenSave.jasper'){
							$scope.templateId = '1010';
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='1P05.jasper'){
						   $scope.templateId = "1029";
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='4P10.jasper'){
						   $scope.templateId = "1031";
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='4P04.jasper'){
	                        $scope.templateId = "1033";
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='4N85.jasper'){
							$scope.templateId = '1025';
	               		}
	               }
	            }
	        }
	       // $scope.Illustration.Product.templates.illustrationPdfTemplate[0].language = $scope.languageAssignment;
        
	        if ($scope.templateId == undefined || $scope.templateId == null || $scope.templateId == "") {
	            $scope.templateId = 0;
	        }
	        $rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
	        $scope.$apply();
	        PersistenceMapping.clearTransactionKeys();
	        IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
	        PersistenceMapping.dataIdentifyFlag = false;
	        PersistenceMapping.Type = "illustration";
	        var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
	        if ((rootConfig.isDeviceMobile)) {
	            if (checkConnection()) {
					$rootScope.showHideLoadingImage(true,'downloadProgress', $translate);
					var transactionsToSync = [];
	                transactionsToSync.push({
	                    "Key" : "illustration",
	                    "Value" : transactionObj.Id,
						"syncType": "indSyncBI"
	                });
	                IllustratorService.individualSync($scope, IllustratorVariables, transactionsToSync, function() {
						GLI_DataService.getListings(transactionObj,function(BIData){
							transactionObj.Key3 = BIData[0].Key3;
							transactionObj.TransactionData.Product.templates.selectedLanguage = "th";
							GLI_DataService.printPDF(transactionObj, $scope.templateId, function (data) {
								$rootScope.showHideLoadingImage(false);
								$scope.refresh();
							}, function (error) {
								$rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
								$rootScope.showHideLoadingImage(false);
								$scope.refresh();
							});
						},function(){
							$rootScope.showHideLoadingImage(false);
						});
	                },function(data){
						$scope.documentError(data);
					},$scope.syncProgressCallback,$scope.syncModuleProgress);
	                // pdf base64 string - transactionObj.TransactionData.IllustrationOutput.base64PdfString
	            } else {
	                $rootScope.showHideLoadingImage(false);
	                $scope.leadDetShowPopUpMsg = true;
	                $scope.enterMandatory = translateMessages($translate, "illustrator.userNotOnline");
	                $scope.refresh();
	            }
	        } else {
	            GLI_DataService.downloadPDF(transactionObj, $scope.templateId, function (data) {
	                $rootScope.showHideLoadingImage(false);
	                $scope.refresh();
	            }, function (error) {
	                $rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
	                $rootScope.showHideLoadingImage(false);
	                $scope.refresh();
	            });
	        }
	    }
    }
    
    //Functional flow from initialLoad method
    // Removed From $scope
    function continueWithInitialLoad () {
        //Creating Customer Details Table
        $scope.customerPartiesData = [];
        //Adding Payor details
        if ($scope.Illustration.Payer && $scope.Illustration.Payer.BasicDetails) {
            var payerData = {};
            payerData.party = translateMessages($translate, "illustrator.partyTblPayorNew");
            payerData.fullName = $scope.Illustration.Payer.BasicDetails.fullName;
            payerData.ageLabel = translateMessages($translate, "illustrator.glivnIlustrationopPartyTblAge");
            payerData.genderLabel = translateMessages($translate, "illustrator.glivnIlustrationopPartyTblGenderNew");
            payerData.occClassLabel = translateMessages($translate, "illustrator.glivnIlustrationopPartyTblOccClassNew");
            payerData.age = $scope.Illustration.Payer.BasicDetails.age;
            payerData.gender = translateMessages($translate, $scope.Illustration.Payer.BasicDetails.gender);
            payerData.dob = getFormattedDateDDMMYYYY($scope.Illustration.Payer.BasicDetails.dob);
//            if ($scope.Illustration.Payer.OccupationDetails) {
//                payerData.occCls = $scope.Illustration.Payer.OccupationDetails.jobClass;
//            }
            $scope.customerPartiesData.push(payerData);
        }
        //Adding Main Insured details
        if ($scope.Illustration.Insured && $scope.Illustration.Insured.BasicDetails) {
            var insuredData = {};
            insuredData.party = translateMessages($translate, "illustrator.partyTblInsuredNew");
            insuredData.fullName = $scope.Illustration.Insured.BasicDetails.fullName;
            insuredData.age = $scope.Illustration.Insured.BasicDetails.age;
            insuredData.gender = translateMessages($translate, $scope.Illustration.Insured.BasicDetails.gender);
            insuredData.ageLabel = translateMessages($translate, "illustrator.glivnIlustrationopPartyTblAge");
            insuredData.genderLabel = translateMessages($translate, "illustrator.glivnIlustrationopPartyTblGenderNew");
            insuredData.occClassLabel = translateMessages($translate, "illustrator.glivnIlustrationopPartyTblOccClassNew");
            insuredData.dob = getFormattedDateDDMMYYYY($scope.Illustration.Insured.BasicDetails.dob);
//            if ($scope.Illustration.Payer.OccupationDetails) {
//                insuredData.occCls = $scope.Illustration.Insured.OccupationDetails.jobClass;
//            }
            $scope.customerPartiesData.push(insuredData);
        }

        $scope.canvasWidthforProduct = "350";
        $scope.canvasHeightforProduct = "160";
        if($scope.Illustration.Product.ProductDetails.productId == '1003' && $scope.Illustration.Product.ProductDetails.productCode == '3'){
            $scope.illustratorProductHeader1AfterAppend = translateMessages($translate, "illustrator.illustrationOutputProductHeaderGB8");
        } else if ($scope.Illustration.Product.ProductDetails.productId == '1010' && $scope.Illustration.Product.ProductDetails.productCode == '10') {
            $scope.illustratorProductHeader1AfterAppend = translateMessages($translate, "illustrator.illustrationOutputProductHeaderCHS8080");
        } else if ($scope.Illustration.Product.ProductDetails.productId == '1097' && $scope.Illustration.Product.ProductDetails.productCode == '97') {
            $scope.illustratorProductHeader1AfterAppend = translateMessages($translate, "illustrator.illustrationOutputProductHeader4N85");
        } else if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4') {
            if($scope.Illustration.Product.policyDetails.premiumPeriod=='20'){
            	$scope.illustratorProductHeader1AfterAppend = translateMessages($translate, "illustrator.illustrationOutputProductHeaderGenPro")+" "+$scope.Illustration.Product.policyDetails.premiumPeriod;
            }else if($scope.Illustration.Product.policyDetails.premiumPeriod=='25'){
            	$scope.illustratorProductHeader1AfterAppend = translateMessages($translate, "illustrator.illustrationOutputProductHeaderGenPro")+" "+$scope.Illustration.Product.policyDetails.premiumPeriod;
            }
        } else if ($scope.Illustration.Product.ProductDetails.productId == '1006' && $scope.Illustration.Product.ProductDetails.productCode == '6') {
            $scope.illustratorProductHeader1AfterAppend = translateMessages($translate, "illustrator.illustrationOutputProductHeaderGS20P");
        } else if ($scope.Illustration.Product.ProductDetails.productId == '1220' && $scope.Illustration.Product.ProductDetails.productCode == '220') {
            $scope.illustratorProductHeader1AfterAppend = translateMessages($translate, "illustrator.illustrationOutputProductHeader1P05");
        } else if ($scope.Illustration.Product.ProductDetails.productId == '1306' && $scope.Illustration.Product.ProductDetails.productCode == '306') {
            $scope.illustratorProductHeader1AfterAppend = translateMessages($translate, "illustrator.illustrationOutputProductHeaderGS10P");
        } else if ($scope.Illustration.Product.ProductDetails.productId == '1274' && $scope.Illustration.Product.ProductDetails.productCode == '274') {
            $scope.illustratorProductHeader1AfterAppend = translateMessages($translate, "illustrator.illustrationOutputProductHeaderGS4P");
        }
        if ($scope.Illustration.IllustrationOutput.quarterlyPremium && $scope.Illustration.IllustrationOutput.quarterlyPremium === "") {
            $scope.Illustration.IllustrationOutput.quarterlyPremium = translateMessages($translate, "illustrator.noValue");
        }
       // $scope.glivnillustrationopbaoanbenefitsheaderAppend = $scope.productNameForIllustrationOutput + translateMessages($translate, "illustrator.glivnillustrationopbaoanbenefitsheader");
        //Rider Tables population - needs to optimise
        if($scope.Illustration.IllustrationOutput.pdfRiderTbl)
        $scope.pdfRiderTbl = angular.copy($scope.Illustration.IllustrationOutput.pdfRiderTbl);
        $scope.benefRiderTblLA1 = [];
        $scope.benefRiderTblLA2 = [];
        $scope.benefRiderTblLA3 = [];
        $scope.benefRiderTblLA4 = [];
        $scope.benefRiderTblLA5 = [];
        if ( $scope.pdfRiderTbl && $scope.pdfRiderTbl.length > 0) {
            for (var t = 0; t < $scope.pdfRiderTbl.length; t++) {
                for (var u = 0; u < IllustratorVariables.insuredList.length; u++) {
                    if ($scope.pdfRiderTbl[t].partyName == IllustratorVariables.insuredList[u]) {
                        var tempRiderData = {};
                        tempRiderData.riderName = $scope.pdfRiderTbl[t].productName;
                        tempRiderData.sumAssured = $scope.pdfRiderTbl[t].sumAssured;
                        var riderDescVal = "illustrator." + $scope.pdfRiderTbl[t].riderCode;
                        if ($scope.pdfRiderTbl[t].productName != "") {
                            var riderNameVal = "illustrator." + $scope.pdfRiderTbl[t].productName;
                            tempRiderData.riderName = translateMessages($translate, riderNameVal);

                            if (typeof ($scope.pdfRiderTbl[t + 1]) != 'undefined' && $scope.pdfRiderTbl[t + 1].productName == "") {
                                if (t == 0) {
                                    tempRiderData.riderCheck = true;
                                } else if ((typeof ($scope.pdfRiderTbl[t - 1]) != 'undefined' && $scope.pdfRiderTbl[t - 1].productName != "") || (typeof ($scope.pdfRiderTbl[t - 1]) != 'undefined' && $scope.pdfRiderTbl[t - 1].productName == "")) {
                                    if ($scope.pdfRiderTbl[t - 1].partyName != $scope.pdfRiderTbl[t].partyName) {
                                        tempRiderData.riderCheck = true;
                                    } else {
                                        tempRiderData.riderCheckTop = true;
                                    }
                                }
                            } else if (typeof ($scope.pdfRiderTbl[t + 1]) != 'undefined' && $scope.pdfRiderTbl[t + 1].productName != "") {
                                if (t == 0) {
                                    tempRiderData.riderCheck = true;
                                } else if (typeof ($scope.pdfRiderTbl[t - 1]) != 'undefined' && $scope.pdfRiderTbl[t - 1].productName == "") {
                                    if ($scope.pdfRiderTbl[t - 1].partyName != $scope.pdfRiderTbl[t].partyName) {

                                    } else {
                                        tempRiderData.riderCheckTop = true;
                                    }
                                } else {
                                    tempRiderData.riderCheckTop = true;
                                }
                            } else if (typeof ($scope.pdfRiderTbl[t + 1]) == "undefined") {
                                tempRiderData.riderCheckTop = true;
                            }
                        } else {
                            tempRiderData.riderName = '';
                            tempRiderData.riderCheck = true;
                        }

                        tempRiderData.riderDescr = translateMessages($translate, riderDescVal);
                        if ($scope.pdfRiderTbl[t].partyName == "LA1") {
                            $scope.benefRiderTblLA1.push(tempRiderData);
                        }
                        if ($scope.pdfRiderTbl[t].partyName == "LA2") {
                            $scope.benefRiderTblLA2.push(tempRiderData);
                        }
                        if ($scope.pdfRiderTbl[t].partyName == "LA3") {
                            $scope.benefRiderTblLA3.push(tempRiderData);
                        }
                        if ($scope.pdfRiderTbl[t].partyName == "LA4") {
                            $scope.benefRiderTblLA4.push(tempRiderData);
                        }
                        if ($scope.pdfRiderTbl[t].partyName == "LA5") {
                            $scope.benefRiderTblLA5.push(tempRiderData);
                        }
                    }
                }
            }
        }
        
        if ($scope.Illustration.IllustrationOutput.summaryTableData && $scope.Illustration.IllustrationOutput.summaryTableData.length > 0) {
            for (sumTbleDta in $scope.Illustration.IllustrationOutput.summaryTableData) {
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "Insurance") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.illustrationOutputInsurance");
                }
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "GenProLIfe 20") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.GenProLife20Out");
                }
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "GenProLIfe 25") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.GenProLife25Out");
                }
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "Wholelife 90/5") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.Wholelife 90/5");
                }
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "Additional contract") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.additionalContract");
				}
				if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "CNC Illustration page") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.CNCRider");
                }
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "WP Illustration page") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.WPRiderSummary");
                }
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "ADB Illustration page") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.ADBRiderSummary");
                }
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "ADBRCC Illustration page") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.RCCADBRiderSummary");
                }
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "ADD Illustration page") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.ADDRidersSummary");
                }
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "ADDRCC Illustration page") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.RCCADDRiderSummary");
                }
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "AI Illustration page") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.AIRiderSummary");
                }
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "AIRCC Illustration page") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.RCCAIRiderSummary");
                }
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "HB Illustration page") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.HBRidersSummary");
                }
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "HS Illustration page") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.HSRidersSummary");
                }
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "OPD Illustration page") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.OPDRidersSummary");
                }
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "PB Illustration page") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.PBRiderSummary");
                }
                if ($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "DD Illustration page") {
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = translateMessages($translate, "illustrator.DDRiderSummary");
                }
                if($scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 == "Total premiums"){
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col4 = $scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1;
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col1 = "";
                	$scope.Illustration.IllustrationOutput.summaryTableData[sumTbleDta].col4 = translateMessages($translate, "illustrator.outputTotalPremium");
                }
            }
        }
        
        /*if ($scope.Illustration.IllustrationOutput.uniqueRiderName && $scope.Illustration.IllustrationOutput.uniqueRiderName.length > 0) {
            for (unqRdrDta in $scope.Illustration.IllustrationOutput.uniqueRiderName) {
            	if($scope.Illustration.IllustrationOutput.uniqueRiderName[unqRdrDta] == "MaininsuredHSRiders"){
            		$scope.disableHSRiders = true;
            	}
            }
        }*/
        
        //Dynamic Table for Summary Tab Ends

        LEDynamicUI.getUIJson(IllustratorVariables.illustrationOutputTemplate, false, function (modifiedIllustrationoutputTemplate) {
            var dynamicPremiumTables = IllustratorVariables.dynamicPremiumTableForIllustraion;
            angular.forEach(dynamicPremiumTables, function (dynamicPremiumValue) {
                var count = 0;
                var dynamicPremiumTableIndex = dynamicFundTableIndex(modifiedIllustrationoutputTemplate, dynamicPremiumValue);
                if (dynamicPremiumTableIndex != undefined) {
                    var templatePremiumArray = modifiedIllustrationoutputTemplate.Views[1].sections[0].subsections[0].controlGroups[0].controls[dynamicPremiumTableIndex];
                    var templateHeaderArray = eval(templatePremiumArray.headers);
                    angular.forEach(templateHeaderArray, function (modifiedValue, modifiedKey) {
                        //Updating the colspan and header values of annualized premium table based on number of additional insured 
                        if (modifiedKey === 0) {
                            angular.forEach($scope.Illustration.IllustrationOutput.VGHRider, function (value) {
                                for (var k = 0; k < rootConfig.uniqueRiderNameIllus.length; k++) {
                                    if (value.isRequired === "Yes" && value.uniqueRiderName == rootConfig.uniqueRiderNameIllus[k]) {
                                        count++;
                                    }
                                }
                            });
                            modifiedValue.row[1].colspan = (Number(modifiedValue.row[1].colspan) + count).toString();
                        }
                        if (modifiedKey === 1) {
                            if ($scope.Illustration.IllustrationOutput.vitaRiderTable1Data && $scope.Illustration.IllustrationOutput.vitaRiderTable1Data.length > 0) {
                                if ($scope.Illustration.IllustrationOutput.vitaRiderTable1Data[0].secondName) {
                                    createHeader = {
                                        'order': 2,
                                        'translateid': $scope.Illustration.IllustrationOutput.vitaRiderTable1Data[0].secondName
                                    };
                                    modifiedValue.row.push(createHeader);
                                }
                                if ($scope.Illustration.IllustrationOutput.vitaRiderTable1Data[0].thirdName) {
                                    createHeader = {
                                        'order': 2,
                                        'translateid': $scope.Illustration.IllustrationOutput.vitaRiderTable1Data[0].thirdName
                                    };
                                    modifiedValue.row.push(createHeader);
                                }
                                if ($scope.Illustration.IllustrationOutput.vitaRiderTable1Data[0].fourthName) {
                                    createHeader = {
                                        'order': 2,
                                        'translateid': $scope.Illustration.IllustrationOutput.vitaRiderTable1Data[0].fourthName
                                    };
                                    modifiedValue.row.push(createHeader);
                                }
                                if ($scope.Illustration.IllustrationOutput.vitaRiderTable1Data[0].fifthName) {
                                    createHeader = {
                                        'order': 2,
                                        'translateid': $scope.Illustration.IllustrationOutput.vitaRiderTable1Data[0].fifthName
                                    };
                                    modifiedValue.row.push(createHeader);
                                }
                            }
                        }
                    });
                    templatePremiumArray.headers = JSON.stringify(templateHeaderArray);
                    var templatesubHeadersArray = eval(templatePremiumArray.subHeaders);
                    if ($scope.Illustration.IllustrationOutput.vitaRiderTable1Data && $scope.Illustration.IllustrationOutput.vitaRiderTable1Data.length > 0) {
                        if ($scope.Illustration.IllustrationOutput.vitaRiderTable1Data[0].secondPremium) {
                            templatesubHeadersArray.push({
                                map: "secondPremium"
                            });
                        }
                        if ($scope.Illustration.IllustrationOutput.vitaRiderTable1Data[0].thirdPremium) {
                            templatesubHeadersArray.push({
                                map: "thirdPremium"
                            });
                        }
                        if ($scope.Illustration.IllustrationOutput.vitaRiderTable1Data[0].fourthPremium) {
                            templatesubHeadersArray.push({
                                map: "fourthPremium"
                            });
                        }
                        if ($scope.Illustration.IllustrationOutput.vitaRiderTable1Data[0].fifthPremium) {
                            templatesubHeadersArray.push({
                                map: "fifthPremium"
                            });
                        }
                    }
                    templatePremiumArray.subHeaders = JSON.stringify(templatesubHeadersArray);
                    modifiedIllustrationoutputTemplate.Views[1].sections[0].subsections[0].controlGroups[0].controls[dynamicPremiumTableIndex] = templatePremiumArray;
                }
            });
            //Smart-Table Integration
            customizedPersonalDetailsUITemplate = modifiedIllustrationoutputTemplate;
            for (var k = 0; k < customizedPersonalDetailsUITemplate.Views.length; k++) {
                for (var l = 0; l < customizedPersonalDetailsUITemplate.Views[k].sections.length; l++) {
                    for (var m = 0; m < customizedPersonalDetailsUITemplate.Views[k].sections[l].subsections.length; m++) {
                        for (var n = 0; n < customizedPersonalDetailsUITemplate.Views[k].sections[l].subsections[m].controlGroups.length; n++) {
                            var controls = customizedPersonalDetailsUITemplate.Views[k].sections[l].subsections[m].controlGroups[n].controls;
                            for (var i = 0; i < controls.length; i++) {
                                if (controls[i].control == "customTable") {
                                    var combinedTblname = "headerNeed" + controls[i].name;
                                    $scope[combinedTblname] = controls[i].headerNeed;
                                    if (controls[i].ngshow) {
                                        var ngShowTbl = "ngshow" + controls[i].name;
                                        $scope[ngShowTbl] = controls[i].ngshow;
                                    }
                                    if (controls[i].type) {
                                        var tblType = "tableType" + controls[i].name;
                                        $scope[tblType] = controls[i].type;
                                    }
                                    var tblMappingOrder = "mappingOrder" + controls[i].name;
                                    $scope[tblMappingOrder] = controls[i].mappingOrder;
                                    //code to add table specific Header columns
                                    if (controls[i].headers) {
                                        var tblHeaders = "headers" + controls[i].name;
                                        eval("$scope[tblHeaders] = " + controls[i].headers.split("'").join('"'));
                                    }
                                    //Adding sub-header maaping
                                    if (controls[i].subHeaders) {
                                        var tblSubHeaders = "subHeaders" + controls[i].name;
                                        eval("$scope[tblSubHeaders] = " + controls[i].subHeaders.split("'").join('"'));
                                    }
                                    //table values mapping section
                                    if (controls[i].tableValuesSrc == "fromModel") {
                                        var tableValueRows = JSON.stringify(controls[i].tableValues);
                                        var tblValueRows = "tableValues" + controls[i].name;
                                        eval("$scope[tblValueRows] = " + tableValueRows.split("'").join('"'));
                                    } else {
                                        var tblValueRows = "tableValues" + controls[i].name;
                                        eval("$scope[tblValueRows] = " + controls[i].tableValues.split("'").join('"'));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            //Passing the newly configured output template to LEDynamicUI for painting
            LEDynamicUI.paintUIFromJsonObject(rootConfig.template, modifiedIllustrationoutputTemplate, "IllustrationDetails", "#IllustrationDetails", true, function () {
                LEDynamicUI.paintUI(rootConfig.template, IllustratorVariables.illustrationOutputTemplate, "HeaderDetails", "#fixedHeaderSection", true, $scope.onPaintUISuccess, $scope, $compile);
                $scope.showHeaderImages = true;
            }, $scope, $compile);
        });
    }

    // Extended initialLoad function from parent controller.
    $scope.initialLoad = function () {
	    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		}    
	    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		}
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5');
		}
	    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5']) {
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5');
		}
	    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5');
		}
	    if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5');
		}
		if($scope.Illustration.PrivacyLaw && $scope.Illustration.PrivacyLaw.userConsentFlag){
            if($scope.Illustration.PrivacyLaw.userConsentFlag == "Yes"){
                $scope.showPrivacyLaw = true;
            }
            else {
                $scope.showPrivacyLaw = false;   
            }
        }    
        else {
            $scope.showPrivacyLaw = true;
        } 
        if (IllustratorVariables.totalRiderList.length == 0 || IllustratorVariables.multipleInsuredRiders.length == 0 || IllustratorVariables.insuredList.length == 0 || IllustratorVariables.dynamicPremiumTableForIllustraion == undefined) {
            LEDynamicUI.getUIJson(rootConfig.illustrationConfigJson, false, function (illustrationConfigJson) {
                IllustratorVariables.totalRiderList = illustrationConfigJson.productRidersList;

                //Riders applicable for Multiple Insured are configured in the illustrationConfigJson.ridersForMutlipleInsured
                IllustratorVariables.multipleInsuredRiders = illustrationConfigJson.ridersForMutlipleInsured;
                //Dynamic Premium Table
                IllustratorVariables.dynamicPremiumTableForIllustraion = illustrationConfigJson.dynamicPremiumTableForIllustraion;
                //Dynamic Fund Tables are adding from the Illustration Config file
                IllustratorVariables.dynamicTablesForIllustraion = illustrationConfigJson.dynamicTablesForIllustraionOuput;

                //Dynamic Health Tables
                IllustratorVariables.dynamicHealthTableForIllustraion = illustrationConfigJson.dynamicTablesForHealthIllustraionOuput;

                //Dynamic Medical Tables
                IllustratorVariables.dynamicMedicalTableForIllustraion = illustrationConfigJson.dynamicTablesForMedicalIllustraionOuput;
                //Adding Illustration Output Constant values from JSON to Illustrator variables
                IllustratorVariables.staticTableDetails = illustrationConfigJson.illustrationOutputStaticTables;
                //Pulling Insured List from the configjson
                IllustratorVariables.insuredList = illustrationConfigJson.insuredList;
                IllustratorVariables.productCodeCI = illustrationConfigJson.productCodeCI;
                continueWithInitialLoad();
            });
        } else {
            continueWithInitialLoad();
        }
        IllustratorVariables.RequirementFile=[];
		if(rootConfig.isDeviceMobile){
		        signature="Signature";
				outputFormat="image/png";
				IllustratorService.getSignature(IllustratorVariables.transTrackingID,signature,"",function(sigdata){
					if(sigdata && sigdata[0]){
						IllustratorVariables.RequirementFile.push(sigdata[0]);
						IllustratorService.convertImgToBase64(sigdata[0].base64string, function(base64Img){
							$scope.signatureBase64string=base64Img;
								$scope.loadSignature();
						},outputFormat);
					}

					if(sigdata && sigdata.length == 2){
						IllustratorVariables.RequirementFile.push(sigdata[1]);
						IllustratorService.convertImgToBase64(sigdata[1].base64string, function(base64Img){
							$scope.signatureBase64string=base64Img;
								$scope.loadSignature();
						},outputFormat);
					}
				});
		} else{
		
			if(IllustratorVariables.illustratorModel.Requirements[0] !=null && IllustratorVariables.illustratorModel.Requirements[0]!="") {
					IllustratorService.getSignature(IllustratorVariables,signature,$scope,function(sigdata){
					IllustratorVariables.RequirementFile=[];
					IllustratorVariables.RequirementFile.push(sigdata);
						$scope.signatureBase64string=sigdata.base64string;
						$scope.signaturerequirement=sigdata.requirementName;
						$scope.loadSignature();
					});
			}
			if(IllustratorVariables.illustratorModel.Requirements[1] !=null && IllustratorVariables.illustratorModel.Requirements[1]!="") {
					IllustratorService.getSignature1(IllustratorVariables,signature,$scope,function(sigdata){
						IllustratorVariables.RequirementFile=[];
						IllustratorVariables.RequirementFile.push(sigdata);
						$scope.signatureBase64string=sigdata.base64string;
						$scope.signaturerequirement=sigdata.requirementName;
						$scope.loadSignature();
						});
			}
				
		}
			if (IllustratorVariables.illustrationStatus == "Confirmed") {
		        $scope.disableSignaturepad = true;
		        $scope.disableClientSignaturepad = true;
		        $scope.disableCanvasClientpad = true;
		        $scope.disableCanvasAgentpad =true;
		    }
		
    }

    //For finding the index of the control that needs to be spliced
    // Removed from $scope
    function dynamicFundTableIndex (modifiedIllustrationoutputTemplate, finderKey) {
        var templateArray = modifiedIllustrationoutputTemplate.Views[1].sections[0].subsections[0].controlGroups[0].controls;
        for (var i = 0; i < templateArray.length; i++) {
            if ((templateArray[i].id) && (templateArray[i].id == finderKey)) {
                return i;
            }
        }
    };

    //Identify the Funds availabe and passing it as fundKey to splice it from selectedFundToIterate array
    // Removed from $scope
    function getSpliceFundKey (selectedFundToIterate, selectedRow, successcallback) {
        var arrLength = Object.keys(selectedFundToIterate).length;
        for (var i = 0; i < arrLength; i++) {
            var fundKey = Object.keys(selectedFundToIterate)[i];
            if (fundKey == selectedRow) {
                successcallback(fundKey);
                break;
            } else {
                if (i == arrLength - 1) {
                    $scope.spliceIdList.push(selectedRow);
                }
            }
        }
    };
	$scope.documentError = function(transacData) {
			//$scope.hideSyncPopup();
			$rootScope.showHideLoadingImage(false);
			if(transacData && transacData == "docUploadFailed"){
				$rootScope.lePopupCtrl.showSuccess(translateMessages($translate,"lifeEngage"),translateMessages($translate,"general.docSyncFailedTryLater"),translateMessages($translate,"illustrator.popUpClose"),$scope.okClick);
			}else if(transacData && transacData.Key16 && transacData.Key16 == "REJECTED" && transacData.Type && transacData.Type != ""){
				var errMsg =  transacData.Type + "RejectedErrorDuringSync";
				$rootScope.lePopupCtrl.showSuccess(translateMessages($translate,"lifeEngage"),translateMessages($translate,errMsg),translateMessages($translate,"illustrator.popUpClose"),$scope.okClick);
			}else if(transacData && transacData.Key16 && transacData.Key16 == "FAILURE" && transacData.Type && transacData.Type != ""){
				var failureMsg =  transacData.Type + "FailureErrorDuringSync";
				$rootScope.lePopupCtrl.showSuccess(translateMessages($translate,"lifeEngage"),translateMessages($translate,failureMsg),translateMessages($translate,"illustrator.popUpClose"),$scope.okClick);
			}else{
				$rootScope.lePopupCtrl.showSuccess(translateMessages($translate,"lifeEngage"),translateMessages($translate,"general.errorDuringSync"),translateMessages($translate,"illustrator.popUpClose"),$scope.okClick);
			}
	};
	$scope.syncProgressCallback = function(currentIndex,totalCount){
			// set the progress percentage and message
			//$scope.syncProgress=progress;
			$rootScope.showHideLoadingImage(false, "");
			//$scope.syncMessage= translateMessages($translate,"uploadingDocuments")+currentIndex + translateMessages($translate,"uploadOf")+totalCount + translateMessages($translate,"documents") ;
			/*if(currentIndex==totalCount){
				$rootScope.showHideLoadingImage(true,'paintUIMessage', $translate);
				$scope.hideSyncPopup();
			}*/
			$scope.refresh();
	};
	$scope.syncModuleProgress = function(syncMessage,progress){
			// set the progress percentage and message
			$rootScope.showHideLoadingImage(false, "");
			$scope.syncMessage=syncMessage;
			$scope.refresh();
	}
    $scope.downloadApplicationPDF = function () {
        if(IllustratorVariables.illustrationStatus!== "Confirmed" && IllustratorVariables.illustrationStatus!== "Completed" && !$scope.clickSaveForOnce){
            $scope.leadDetShowPopUpMsg = true;
            $scope.enterMandatory = translateMessages($translate, "illustrator.saveBeforeDownload");
        } else {
	        if ($scope.Illustration.Product.templates != undefined &&
	            $scope.Illustration.Product.templates != null &&
	            $scope.Illustration.Product.templates.illustrationPdfTemplate != undefined &&
	            $scope.Illustration.Product.templates.illustrationPdfTemplate != null &&
	            $scope.Illustration.Product.templates.illustrationPdfTemplate.length != 0) {
	            var templatelength = $scope.Illustration.Product.templates.illustrationPdfTemplate.length;
	            var selectedLanguage = "th";
	            for (var i = 0; i < templatelength; i++) {
	            	$scope.Illustration.Product.templates.illustrationPdfTemplate[i].language = selectedLanguage
	                //if ($scope.Illustration.Product.templates.illustrationPdfTemplate[i].language == selectedLanguage)
	                /*$scope.templateId = $scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateId;*/
	               if (rootConfig.isDeviceMobile) {
						if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='GenProLife.jasper'){
							$scope.templateId = '1008';
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='GenBumnan.jasper'){
							$scope.templateId = '1004';
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='GenCompletec.jasper'){
							$scope.templateId = '1005';
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='GenSave.jasper'){
							$scope.templateId = '1010';
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='1P05.jasper'){
	                        $scope.templateId = "1029";
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='4P10.jasper'){
	                        $scope.templateId = "1031";
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='4P04.jasper'){
	                        $scope.templateId = "1033";
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='4N85.jasper'){
							$scope.templateId = '1025';
	               		}
	               } else {
	            	  //$scope.templateId = $scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateId;
	            	   if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='GenProLife.jasper'){
							$scope.templateId = '1008';
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='GenBumnan.jasper'){
							$scope.templateId = '1004';
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='GenCompletec.jasper'){
							$scope.templateId = '1005';
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='GenSave.jasper'){
							$scope.templateId = '1010';
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='1P05.jasper'){
	                       $scope.templateId = "1029";
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='4P10.jasper'){
	                       $scope.templateId = "1031";
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='4P04.jasper'){
	                        $scope.templateId = "1033";
						} else if($scope.Illustration.Product.templates.illustrationPdfTemplate[i].templateName=='4N85.jasper'){
							$scope.templateId = '1025';
	               		}
	               }
	            }
	        }
	        if ($scope.templateId == undefined ||
	            $scope.templateId == null ||
	            $scope.templateId == "") {
	            $scope.templateId = 0;
	        }

	        $rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
	        PersistenceMapping.clearTransactionKeys();
	        IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
	        PersistenceMapping.dataIdentifyFlag = false;
	        PersistenceMapping.Type = "illustration";
	        var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
	        if (rootConfig.isDeviceMobile) {
	            if (checkConnection()) {
	                /*DataService.saveOnline(transactionObj, function (data) {
	                    IllustratorVariables.illustratorId = data.Key3;
	                    IllustratorVariables.illustratorKeys.Key12 = data.Key12;
	                    var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
	                    obj.save(function (isAutoSave) {
	                        if (!jQuery.isEmptyObject($scope.IllustrationAttachment.Upload.Signature[0])) {
	                            var fileContent = $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent;
	                            if (fileContent.length > 2) {
	                                var docData = [];
	                                docData.push(angular.copy($scope.IllustrationAttachment.Upload.Signature[0]));
	                                var docDetails = {
	                                    Documents: docData[0]
	                                };
	                                PersistenceMapping.clearTransactionKeys();
	                                IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
	                                PersistenceMapping.dataIdentifyFlag = false;
	                                var docDetailsWithKeys = PersistenceMapping.mapScopeToPersistence(docDetails);
	                                DocumentService.uploadDocuments(docDetailsWithKeys, "1", function (data, count) {
	                                    DataService.downloadPDF(transactionObj, $scope.templateId, function (data) {
	                                        if ($('body').hasClass('ipad')) {
	                                            $rootScope.showHideLoadingImage(false);
	                                            $scope.refresh();
	                                        } else {
	                                            $rootScope.showHideLoadingImage(false);
	                                            $rootScope.lePopupCtrl
	                                                .showSuccess(translateMessages(
	                                                        $translate, "lifeEngage"),
	                                                    translateMessages($translate,
	                                                        "pdfDownloadTransactionsSuccess"),
	                                                    translateMessages($translate,
	                                                        "illustrator.popUpClose"));
	                                            $scope.refresh();
	                                        }

	                                    }, function (error) {
	                                        $rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
	                                        $rootScope.showHideLoadingImage(false);
	                                        $scope.refresh();
	                                    });
	                                });
	                            } else {
	                                DataService.downloadPDF(transactionObj, $scope.templateId, function (data) {
	                                    $rootScope.showHideLoadingImage(false);
	                                    if ($('body').hasClass('ipad')) {
	                                        $rootScope.showHideLoadingImage(false);
	                                        $scope.refresh();
	                                    } else {
	                                        $rootScope.showHideLoadingImage(false);
	                                        $rootScope.lePopupCtrl
	                                            .showSuccess(translateMessages(
	                                                    $translate, "lifeEngage"),
	                                                translateMessages($translate,
	                                                    "pdfDownloadTransactionsSuccess"),
	                                                translateMessages($translate,
	                                                    "illustrator.popUpClose"));
	                                        $scope.refresh();
	                                    }
	                                    $scope.refresh();
	                                }, function (error) {
	                                    $rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
	                                    $rootScope.showHideLoadingImage(false);
	                                    $scope.refresh();
	                                });
	                            }
	                        }
	                    });
	                }, $scope.error);*/
					$rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
					var transactionsToSync = [];
					transactionsToSync.push({
	                    "Key" : "illustration",
	                    "Value" : transactionObj.Id,
						"syncType": "indSyncBI"
	                });
	                IllustratorService.individualSync($scope, IllustratorVariables, transactionsToSync, function() {
						GLI_DataService.getListings(transactionObj,function(BIData){
							transactionObj.Key3 = BIData[0].Key3;
							transactionObj.TransactionData.Product.templates.selectedLanguage = "th";
							DataService.downloadPDF(transactionObj, $scope.templateId, function(data) {      
								if($('body').hasClass('ipad')){
									$rootScope.showHideLoadingImage(false);
									$scope.refresh();
								}
								else{
									$rootScope.showHideLoadingImage(false);
									$rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"),translateMessages($translate,"illustrator.pdfDownloadTransactionsSuccess"),translateMessages($translate,"illustrator.popUpClose"));
									$scope.refresh();
								}
							},function(error){
								$rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
								$rootScope.showHideLoadingImage(false);
								$scope.refresh();
							});
						},function(){
							$rootScope.showHideLoadingImage(false);
						});
	                },function(data){
						$scope.documentError(data);
					},$scope.syncProgressCallback,$scope.syncModuleProgress);
	            } else {
	                $rootScope.showHideLoadingImage(false);
	                $scope.leadDetShowPopUpMsg = true;
	                $scope.enterMandatory = translateMessages($translate, "illustrator.userNotOnline");
	                $scope.refresh();
	            }
	        } else {
	            DataService.downloadPDF(transactionObj, $scope.templateId, function (data) {
	                $rootScope.showHideLoadingImage(false);
	                $scope.refresh();
	            }, function (error) {
	                $rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
	                $rootScope.showHideLoadingImage(false);
	                $scope.refresh();
	            });
	        }
	    }
    };

    $scope.getValueFormatted = function (val) {
        if (isNaN(val)) {
            return val;
        } else {
            return $scope.checkDecimal(val);
        }
    };

    $scope.getValue = function (row, key) {
        value = row[key.map];
        if (isNaN(value)) {
            return value;
        } else {
            return $scope.checkDecimal(value);
        }
    };

    $scope.checkDecimal = function (number) {
        if (isNaN(number)) {
            return number;
        } else if (typeof number != "undefined" &&
            number != null &&
            number.toString().indexOf('.') > 0) {
            number = parseInt(number);
        }
        return number;
    };

    //Dynamic Medical Table stucture creation

    $scope.showEmailPopup = function () {
        if(IllustratorVariables.illustrationStatus!== "Confirmed" && IllustratorVariables.illustrationStatus!== "Completed" && !$scope.clickSaveForOnce){
            $scope.leadDetShowPopUpMsg = true;
            $scope.enterMandatory = translateMessages($translate, "illustrator.saveBeforeEmail");
        }else{
	        if($scope.isUserOnline()) {
		        $scope.emailpopup = true;
		        if (IllustratorVariables.leadEmailId && IllustratorVariables.leadEmailId != "" && IllustratorVariables.leadEmailId != "undefined") {
		            $scope.emailToId = IllustratorVariables.leadEmailId;
		        } else if ($scope.Illustration.Insured.ContactDetails.emailId && $scope.Illustration.Insured.ContactDetails.emailId != "" && $scope.Illustration.Insured.ContactDetails.emailId != "undefined") {
		            $scope.emailToId = $scope.Illustration.Insured.ContactDetails.emailId;
		        } else if ($scope.Illustration.Payer.ContactDetails.emailId && $scope.Illustration.Payer.ContactDetails.emailId != "" && $scope.Illustration.Payer.ContactDetails.emailId != "undefined") {
		            $scope.emailToId = $scope.Illustration.Payer.ContactDetails.emailId;
		        } else {
		            $scope.emailToId = "";
		        }
	            var productLabel="";
	            if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===6){
	                productLabel="GenSave20Plus_emailSubject";
	            }else if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===4){
	                productLabel="GenProLife_emailSubject";
	            }else if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===3){
	                productLabel="GenBumnan_emailSubject";
	            }else if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===10){
	                productLabel="GenCompleteHealth_emailSubject";
	            }
	                                    
	            if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===10 || parseInt($scope.Illustration.Product.ProductDetails.productCode)===6){
					$scope.emailSubject = translateMessages($translate, "illustrator.GeneraliThailandEmailSubject")+translateMessages($translate,productLabel)+" "+translateMessages($translate, "illustrator.GeneraliThailandEmailSubject1")+" "+ $scope.Illustration.Insured.BasicDetails.fullName;
	            }else{
		             $scope.emailSubject = translateMessages($translate, "illustrator.GeneraliThailandEmailSubject")+translateMessages($translate,productLabel)+" "+ $scope.Illustration.Product.policyDetails.premiumPeriod +" "+translateMessages($translate, "illustrator.GeneraliThailandEmailSubject1")+" "+ $scope.Illustration.Insured.BasicDetails.fullName;
	            }
		            
		//		$scope.emailSubject = translateMessages($translate, "illustrator.GeneraliThailandEmailSubject")+translateMessages($translate,$scope.Illustration.Product.ProductDetails.productName) +" "+ $scope.Illustration.Product.policyDetails.premiumPeriod +" "+ $scope.Illustration.Insured.BasicDetails.fullName;
		        $scope.userDetails = UserDetailsService.getUserDetailsModel();
		        $scope.emailccId = $scope.userDetails.emailId;
		        $scope.refresh();
		    }else{
	            $scope.emailpopup = false;
	            $scope.leadDetShowPopUpMsg = true;
	            $scope.enterMandatory = translateMessages($translate, "illustrator.emailBINoConnectionErrorMessage");
	        }
	    }
    }
    
    $scope.isUserOnline = function(){
        var userOnline = true;
        if ((rootConfig.isDeviceMobile)&& !(rootConfig.isOfflineDesktop)){
            if (navigator.network.connection.type == Connection.NONE) {
				userOnline = false;
            }
        }
        if ((rootConfig.isOfflineDesktop)) {
            if (!navigator.onLine) {
				userOnline = false;
            }
        }
        return userOnline;
    }
    
    $scope.closePopup = function () {
            $scope.leadDetShowPopUpMsg = false;
        };

    $scope.emailPopupOkClick = function () {
        $scope.validateMailIds(function (emailIdStatus) {
            if (emailIdStatus) {
                $scope.emailpopup = false;
                $scope.emailSendId = $scope.emailToId;
                $rootScope.showHideLoadingImage(true, translateMessages($translate, "illustrator.pleaseWait"));
                PersistenceMapping.clearTransactionKeys();
                IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
                PersistenceMapping.dataIdentifyFlag = false;
                PersistenceMapping.Type = "illustration";
                var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
                $scope.doValidationForEmailSending(transactionObj, function () {
                    $scope.saveEmailSendFlagAndData($scope.showSuccessPopup, $scope.showErrorPopup);
                });
            }
        });
    }

    $scope.doValidationForEmailSending = function (transactions, successcallback) {
        if ((rootConfig.isDeviceMobile)) {
            $scope.emailToId = "";
            //$scope.emailccId =""; 
        }
        $scope.illustartionsToMail = [];
        $scope.illustartionsToMail.push(transactions);
        $scope.leadName = transactions.Key22;
        if ((rootConfig.isDeviceMobile)) {
            if (transactions.Key23 && transactions.Key23 != "" && transactions.Key23 != null) {
                $scope.emailToId = transactions.Key23;
            } else {
                $scope.emailToId = $scope.emailSendId;
            }
        } else {
            if (transactions.Key23 && transactions.Key23 != "" && transactions.Key23 != null) {
                $scope.emailToId = transactions.Key23;
            } else {
                $scope.emailToId = $scope.emailSendId;
            }
        }
        $scope.userDetails = UserDetailsService.getUserDetailsModel();
        //CC mail id should be agent id if empty
        if ($scope.emailccId == "" || $scope.emailccId == undefined) {
            $scope.emailccId = $scope.userDetails.emailId;
        }

        $scope.agentName = $scope.userDetails.agentName;
        $scope.agentEmailId = $scope.userDetails.emailId;
        //cc id needs to be populated from User Details object agent - mailid - yet to implement maild id return from service
        $scope.emailpopupError = "";
        $scope.emailToError = "";
        $scope.emailCcError = "";
		$scope.emailSubjectError = "";
        successcallback();
    }

    $scope.validateMailIds = function (emailCallback) {
        $scope.emailToError = "";
        $scope.emailCcError = "";
		$scope.emailSubjectError = "";
        var status = true;
        var re = rootConfig.emailPattern;
        var EMAIL_REGEXP = new RegExp(re);
        if ($scope.emailToId == "" || $scope.emailToId == undefined) {
            $scope.emailToError = translateMessages($translate, "illustrator.validEmailValidationMessage");
            status = false;
        } else if($scope.emailSubject && $scope.emailSubject == ""){
			$scope.emailSubjectError = translateMessages($translate, "illustrator.validEmailValidationMessage");;
		} else {
            var emailIdsToValidate = [];
            emailIdsToValidate.push($scope.emailToId);
            if ($scope.emailccId != "") {
                if ($scope.emailccId != undefined) {
                    emailIdsToValidate.push($scope.emailccId);
                }
            }
            for (var t = 0; t < emailIdsToValidate.length; t++) {
                var toEmailId = emailIdsToValidate[t];
                if (toEmailId.charAt(toEmailId.length - 1) == ";") {
                    toEmailId = toEmailId.substring(0, (toEmailId.length - 1));
                }

                var tomailIds = toEmailId.split(';');
                for (var idCount = 0; idCount < tomailIds.length; idCount++) {
                    if (!EMAIL_REGEXP.test(tomailIds[idCount])) {
                        if (t === 0) {
                            $scope.emailToError = translateMessages($translate, "illustrator.validEmailValidationMessage");
                        } else {
                            $scope.emailCcError = translateMessages($translate, "illustrator.validEmailValidationMessage");
                        }
                        status = false;
                        break;
                    }
                }
            }
        }
        emailCallback(status);
    }

    $scope.saveEmailSendFlagAndData = function (successCallback, errorCallBack) {
        var emailObj = {};
        emailObj.toMailIds = $scope.emailToId;

        if ($scope.emailccId) {
            emailObj.ccMailIds = $scope.emailccId;
        } else {
            emailObj.ccMailIds = "";
        }

        if ($scope.agentName) {
            emailObj.agentName = $scope.agentName;
        } else {
            emailObj.agentName = "";
        }

        if ($scope.agentEmailId) {
            emailObj.agentEmailId = $scope.agentEmailId;
        } else {
            emailObj.agentEmailId = "";
        }

        if ($scope.leadName) {
            emailObj.leadName = $scope.leadName;
        } else {
            emailObj.leadName = "";
        }
		
		if ($scope.emailSubject) {
            emailObj.mailSubject = $scope.emailSubject;
        } else {
            emailObj.mailSubject = "";
        }

        var selectedLanguage = "th";
        emailObj.language = selectedLanguage;
        IllustratorService.emailAndSaveTransactionsBI(emailObj, $scope, $scope.illustartionsToMail, DataService, $scope.successBIEmailCallback, $scope.errorBIEmailCallBack);
    }

    $scope.successBIEmailCallback = function () {
        $scope.showSuccessPopup();
    }
    $scope.errorBIEmailCallBack = function () {
        $scope.showErrorPopup();
    }

    $scope.showSuccessPopup = function () {
        $rootScope.showHideLoadingImage(false);
        $scope.emailpopup = false;
        $scope.emailToId = "";
        $scope.emailccId = "";
        $scope.leadName = "";
        $scope.agentName = "";
        $scope.agentEmailId = "";
        $scope.selectedProposal = [];
        $scope.illustartionsToMail = [];

        if (!(rootConfig.isDeviceMobile)) {
//            $rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "illustrator.emailOkButtonMessage"), translateMessages($translate, "illustrator.popUpClose"));
            $scope.leadDetShowPopUpMsg = true;
            $scope.enterMandatory = translateMessages($translate, "illustrator.emailOkButtonMessage");
            $scope.refresh();
        } else {
            if (checkConnection()) {
//                $rootScope.lePopupCtrl.showSuccess(translateMessages($translate, "lifeEngage"), translateMessages($translate, "illustrator.emailOkButtonMessage"), translateMessages($translate, "illustrator.popUpClose"));
                $scope.leadDetShowPopUpMsg = true;
                $scope.enterMandatory = translateMessages($translate, "illustrator.emailOkButtonMessage");
                $scope.refresh();
            } else {
                $scope.emailpopup = false;
//                $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"), translateMessages($translate, "illustrator.emailBINoConnectionErrorMessage"), translateMessages($translate, "illustrator.popUpClose"));
                $scope.leadDetShowPopUpMsg = true;
                $scope.enterMandatory = translateMessages($translate, "illustrator.emailBINoConnectionErrorMessage");
            }
        }
        $scope.refresh();
    }

    $scope.showErrorPopup = function (status, errorMessage) {
        if (errorMessage == "") {
            errorMessage = "emailBIErrorMessage";
        }
        var httpErrorMessage = "illustrator." + errorMessage;
        $rootScope.showHideLoadingImage(false);
        $scope.emailpopup = false;
        $scope.emailToId = "";
        $scope.emailccId = "";
        $scope.leadName = "";
        $scope.agentName = "";
        $scope.agentEmailId = "";
        $scope.selectedProposal = [];
        $scope.illustartionsToMail = [];

        if (checkConnection()) {
            var emailErrorMessage = translateMessages($translate, "illustrator.emailSendingErrorMessage");
            $rootScope.lePopupCtrl.showSuccess(translateMessages(
                $translate, "lifeEngage"), emailErrorMessage, translateMessages($translate,
                "illustrator.popUpClose"));
            $scope.refresh();
        } else {
            $scope.emailpopup = false;
            $rootScope.lePopupCtrl.showError(translateMessages(
                $translate, "lifeEngage"), translateMessages(
                $translate, "illustrator.emailBINoConnectionErrorMessage"), translateMessages($translate,
                "illustrator.popUpClose"));
        }
        $scope.refresh();

    }

    $scope.updatePremiumTablePosition = function () {
        var returnClass;
        if ($scope.Illustration.IllustrationOutput.vitaRiderTable2Data && $scope.Illustration.IllustrationOutput.vitaRiderTable2Data.length == 0 && $scope.Illustration.IllustrationOutput.vitaRiderTable3Data && $scope.Illustration.IllustrationOutput.vitaRiderTable3Data.length == 0) {
            returnClass = "offset4 span4";
        } else if ($scope.Illustration.IllustrationOutput.vitaRiderTable3Data && $scope.Illustration.IllustrationOutput.vitaRiderTable3Data.length == 0) {
            returnClass = "offset1 span4";
        } else {
            returnClass = "span4";
        }

        return returnClass
    }

    $scope.cancelEmailPopup = function () {
        $scope.selectedProposal = [];
        $scope.illustartionsToMail = [];
        $scope.emailpopup = false;
        $scope.emailToId = "";
        $scope.emailccId = "";
        $scope.leadName = "";
        $scope.agentName = "";
        $scope.agentEmailId = "";
    }
    
    $scope.$on("$destroy", function () {
        if (this != null && typeof this !== 'undefined') {
            if (plot) {
                plot.destroy();
            }
            if (plot2) {
                plot2.destroy();
            }
            if (plot3) {
                plot3.destroy();
            }
            if (plot4) {
                plot4.destroy();
            }
            if (plot5) {
                plot5.destroy();
            }
            if (plot6) {
                plot6.destroy();
            }
            if (plot7) {
                plot7.destroy();
            }
            if (this.$$destroyed) return;
            while (this.$$childHead) {
                this.$$childHead.$destroy();
            }
            if (this.$broadcast) {
                this.$broadcast('$destroy');
            }
            this.$$destroyed = true;
        }
    });
	
    $scope.changingClientPadStatus = function () {
    	$scope.disableClientSignaturepad = false;
    }
    
    $scope.changingAgentPadStatus = function () {
    	$scope.disableSignaturepad = false;
    }
    
	var planName="illustrator.PLAN"+$scope.Illustration.IllustrationOutput.HSSumAssuredCover;
    $scope.dynamicPlan=translateMessages($translate, planName);
	
} // Controller ends here