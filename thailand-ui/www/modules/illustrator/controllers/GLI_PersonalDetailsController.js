/*
 * Copyright 2015, LifeEngage 
 */
/*Name:GLI_PersonalDetailsController 
 CreatedDate:7/15/2015
 Description:illustrator Personal info capturing controller for generali.*/

storeApp.controller('GLI_PersonalDetailsController', GLI_PersonalDetailsController);
GLI_PersonalDetailsController.$inject = ['$rootScope',
			'GLI_IllustratorService',
			'GLI_IllustratorVariables',
			'$scope',
            '$parse',
            '$window',
			'$compile',
			'$timeout',
			'$routeParams',
			'$route',
			'$location',
			'ProductService',
			'LookupService',
			'UtilityService',
			'DataService',
			'DocumentService',
			'$translate',
			'dateFilter',
			'IllustratorVariables',
			'IllustratorService',
			'PersistenceMapping',
			'$debounce',
			'AutoSave',
			'FnaVariables',
			'globalService',
			'$controller',
			'DataLookupService',
			'GLI_globalService',
			'UserDetailsService',
			'AgentService',
			'GLI_DataService',
			'GLI_EvaluateService', 'GLI_IllustratorService','MediaService','GLI_DataLookupService'];

function GLI_PersonalDetailsController($rootScope, GLI_IllustratorService, GLI_IllustratorVariables, $scope,$parse, $window,
    $compile, $timeout, $routeParams, $route, $location, ProductService,
    LookupService, UtilityService, DataService, DocumentService,
    $translate, dateFilter, IllustratorVariables, IllustratorService,
    PersistenceMapping, $debounce, AutoSave, FnaVariables,
    globalService, $controller, DataLookupService, GLI_globalService,
    UserDetailsService, AgentService, GLI_DataService, GLI_EvaluateService, GLI_IllustratorService,MediaService, GLI_DataLookupService) {

    $controller('PersonalDetailsController', {
        $rootScope: $rootScope,
        IllustratorService: GLI_IllustratorService,
        IllustratorVariables: GLI_IllustratorVariables,
        $scope: $scope,
        $parse: $parse,
        $window: $window,
        $compile: $compile,
        $timeout: $timeout,
        $routeParams: $routeParams,
        $route: $route,
        $location: $location,
        ProductService: ProductService,
        LookupService: LookupService,
        UtilityService: UtilityService,
        DataService: DataService,
        DocumentService: DocumentService,
        $translate: $translate,
        dateFilter: dateFilter,
        PersistenceMapping: PersistenceMapping,
        $debounce: $debounce,
        AutoSave: AutoSave,
        FnaVariables: FnaVariables,
        globalService: globalService,
        DataLookupService:GLI_DataLookupService,
        GLI_globalService: GLI_globalService
    });

    //Variable for enabling and disabling Main Insured Type field
    //$scope.disableMainInsuredType = false;
    //$scope.disableAdditionalInsured = false;
    //$rootScope.isFromIllustration = true;
    $scope.isFromIllustration = $rootScope.isFromIllustration;
    $rootScope.editIllustrator = false;
    $scope.SaveTrans=false;
    $scope.showSecondary=false;
    $scope.showSecondaryPayor=false;
    $scope.allowCodeUpdate=false;
    $rootScope.hideLanguageSetting = false;
//    $scope.enableProceedToEapp=false;
    /*testPattern - Need to be changed later*/
    $scope.testPattern=/^[\u0E00-\u0E7Fa-zA-Z' ]{0,30}$/;
    $scope.emailRegex = /^[A-Za-z0-9._%+-]+@[A-Za-z]+(\.([A-Za-z]){2,4})?\.([A-Za-z]{2,4})$/;
//    $scope.mobileRegex=/^((?!(0))[0-9]{9,10})$/;
    
    //$scope.child = {};
    $scope.payerAge;
    $scope.insuredAge;
    //Error message changes
    $scope.dynamicErrorMessages = [];
    $scope.dynamicErrorCount = 0;
    $rootScope.selectedPage = "Personal Details";
    IllustratorVariables.illustrationFromIllusPersonal = true;
    var currentTab;
    $scope.namePattern = rootConfig.namePatternBI;
    $scope.alphanumericPattern = rootConfig.alphanumericPattern;
    $scope.alphabetWithSpacePattern = rootConfig.alphabetWithSpacePattern;
    $scope.agePattern = rootConfig.agePattern;
    $scope.alphabetWithSpacePatternIllustration = rootConfig.alphabetWithSpacePatternIllustration;
    $scope.numberPatternMob = rootConfig.numberPatternMobBi;    
    //Setting additional insured type as none initialy.
    if (($scope.Illustration.Insured.BasicDetails.mainInsuredType == '' && $scope.Illustration.Insured.BasicDetails.mainInsuredType) || !$scope.Illustration.Insured.BasicDetails.mainInsuredType) {
        $scope.additionalInsuredType = '';
    }
    if ($routeParams.productId == '0') {
        $routeParams.productId = "";
    }
    if ($routeParams.illustrationId == '0') {
        $routeParams.illustrationId = "";
    }
    if ($routeParams.transactionId == '0') {
        $routeParams.transactionId = "";
    }
    /*if($rootScope.declinedRCCADB == undefined && $rootScope.declinedRCCAI == undefined && $rootScope.declinedRCCADD == undefined){
    	$rootScope.declinedRCCADB = false;
    	$rootScope.declinedRCCAI = false;
		$rootScope.declinedRCCADD = false;
    }
    if (IllustratorVariables.illustrationStatus != "Confirmed" || IllustratorVariables.illustrationStatus != "Completed" ) { 
    	$rootScope.declinedRCCADB = false;
		$rootScope.declinedRCCAI = false;
		$rootScope.declinedRCCADD = false;
    }*/
    
    if (IllustratorVariables.illustrationStatus != "Confirmed" || IllustratorVariables.illustrationStatus != "Completed" ) { 
    	if($scope.Illustration.Insured.OccupationDetails && $scope.Illustration.Insured.OccupationDetails[0].occupationCode != ""){
    		$scope.previousOccupation = $scope.Illustration.Insured.OccupationDetails[0].occupationCode;
    	}
    }
    
    $scope.updatePairingRiders = function(model){
    	if($scope.Illustration.Insured.OccupationDetails && $scope.Illustration.Insured.OccupationDetails[0].occupationCode != ""){
    		if(model != undefined){
    			if($scope.previousOccupation != model){
        			$rootScope.declinedRCCADB = false;
    				$rootScope.declinedRCCAI = false;
    				$rootScope.declinedRCCADD = false;
        		}
    		}
    	}
    }
    
    /*FNA changes by LE Team >>> starts*/
    $scope.navigatedFromFNA = IllustratorVariables.fromFNAChoosePartyScreenFlow;
    /*FNA changes by LE Team >>> ends*/
    // Common method for navigating to choose party screen for web and device
    function navigateToChooseParty(fnaData) {
        if (fnaData != undefined) {
            if (fnaData && fnaData[0] && fnaData[0].TransactionData) {
                FnaVariables.setFnaModel({
                    'FNA': JSON.parse(JSON.stringify(fnaData[0].TransactionData))
                });
                $scope.FNAObject = FnaVariables.getFnaModel();
                FnaVariables.transTrackingID = fnaData[0].TransTrackingID;
                FnaVariables.leadId = fnaData[0].Key1;
                globalService.setParties($scope.FNAObject.FNA.parties);
                UtilityService.previousPage = 'IllustrationPersonal';
                if (IllustratorVariables.illustratorId == "" || typeof IllustratorVariables.illustratorId == "undefined") {
                    IllustratorVariables.illustratorId = "0";
                }
                $location.path("/fnaChooseParties/0/0/" + IllustratorVariables.illustratorId);
                $scope.refresh();
            }
        }
    }
    // Common method for navigating to choose party screen for web and device
    function navigateToLeadChooseParty(leadData) {
        if (leadData && leadData[0] && leadData[0].TransactionData) {
            var parties = [];
            var party = new globalService.party();
            /*party.BasicDetails = leadData[0].TransactionData.Lead.BasicDetails;
            party.ContactDetails = leadData[0].TransactionData.Lead.ContactDetails;
            party.OccupationDetails = leadData[0].TransactionData.Lead.OccupationDetails;
            */
            party.BasicDetails.firstName = leadData[0].Key2;
            party.BasicDetails.lastName = leadData[0].Key3;
            party.BasicDetails.fullName = leadData[0].Key2 + " " + leadData[0].Key3;
			party.BasicDetails.nickName = leadData[0].TransactionData.Lead.BasicDetails.nickName;
            party.ContactDetails.mobileNumber1 = leadData[0].TransactionData.Lead.ContactDetails.mobileNumber1;
            party.ContactDetails.homeNumber = leadData[0].TransactionData.Lead.ContactDetails.homeNumber; // data.Key4;
            party.ContactDetails.emailId = leadData[0].Key20;
            party.BasicDetails.dob = leadData[0].Key7;
            party.BasicDetails.gender = leadData[0].TransactionData.Lead.BasicDetails.gender;
            party.ContactDetails.currentAddress.houseNo = leadData[0].TransactionData.Lead.ContactDetails.currentAddress.addressLine1;
            party.ContactDetails.currentAddress.state = leadData[0].TransactionData.Lead.ContactDetails.currentAddress.state;
            
            party.ContactDetails.currentAddress.city = leadData[0].TransactionData.Lead.ContactDetails.currentAddress.city;
            party.ContactDetails.currentAddress.cityValue = leadData[0].TransactionData.Lead.ContactDetails.currentAddress.cityValue;
            
            party.ContactDetails.currentAddress.streetName = leadData[0].TransactionData.Lead.ContactDetails.currentAddress.streetName;
            
            party.ContactDetails.currentAddress.ward = leadData[0].TransactionData.Lead.ContactDetails.currentAddress.ward;
            party.ContactDetails.currentAddress.wardOrHamletValue = leadData[0].TransactionData.Lead.ContactDetails.currentAddress.wardOrHamletValue;
                        
            party.ContactDetails.currentAddress.zipcode = leadData[0].TransactionData.Lead.ContactDetails.currentAddress.zipcode;
            // occupationCategory For occupation code  & occupationCategoryValue for occupation category name
            /*party.OccupationDetails.occupationCategory = leadData[0].TransactionData.Lead.OccupationDetails.occupationCategory;
            party.OccupationDetails.occupationCategoryValue = leadData[0].TransactionData.Lead.OccupationDetails.occupationCategoryValue;
            party.OccupationDetails.description = leadData[0].TransactionData.Lead.OccupationDetails.description;
            party.OccupationDetails.jobCode = leadData[0].TransactionData.Lead.OccupationDetails.jobCode;
            party.OccupationDetails.jobClass = leadData[0].TransactionData.Lead.OccupationDetails.jobCode;
            party.OccupationDetails.descriptionOthers = leadData[0].TransactionData.Lead.OccupationDetails.descriptionOthers;*/
            party.BasicDetails.incomeRange = leadData[0].TransactionData.Lead.BasicDetails.incomeRange;
            party.BasicDetails.nationality = leadData[0].TransactionData.Lead.BasicDetails.nationality;
            party.BasicDetails.countryofResidence = leadData[0].TransactionData.Lead.ContactDetails.birthAddress.country;
            party.BasicDetails.identityProof = leadData[0].TransactionData.Lead.BasicDetails.identityProof;
            party.BasicDetails.dob = leadData[0].TransactionData.Lead.BasicDetails.dob;
			party.BasicDetails.IDcard = leadData[0].TransactionData.Lead.BasicDetails.IDcard;

            party.id = "myself";
            party.type = "Lead";
            party.isInsured = false;
            party.isPayer = false;
            party.isBeneficiary = false;
            party.isAdditionalInsured = false;
            party.fnaBenfPartyId = "";
            party.fnaInsuredPartyId = "";
            party.fnaPayerPartyId = "";
            parties.push(party);
            globalService.setParties(parties);
            var insured = $scope.Illustration.Insured;
            globalService.setInsured(insured);
            var payer = $scope.Illustration.Payer;
            globalService.setPayer(payer);
            var beneficiaries = $scope.Illustration.Beneficiaries;
            globalService.setBenfFromIllustrn(beneficiaries);
            var additionalInsured = $scope.Illustration.AdditionalInsured;
            globalService.setAddInsuredFromIllustrn(additionalInsured);
            globalService.setAdditionalInsured(additionalInsured);
            UtilityService.previousPage = 'IllustrationPersonal';
            if (IllustratorVariables.illustratorId == "" || typeof IllustratorVariables.illustratorId == "undefined") {
                IllustratorVariables.illustratorId = "0";
            }

            $location.path("/fnaChooseParties/0/" + $routeParams.productId + "/" + IllustratorVariables.illustratorId);
            $scope.refresh();
        }
    }

    $scope.proceedToChooseParty = function () {
        if ((rootConfig.isDeviceMobile)) {
            DataService.getRelatedFNA(IllustratorVariables.fnaId, function (fnaData) {
                navigateToChooseParty(fnaData);
            });
        } else {

            PersistenceMapping.clearTransactionKeys();
            var searchCriteria = {
                searchCriteriaRequest: {
                    "command": "RelatedTransactions",
                    "modes": ['FNA'],
                    "value": IllustratorVariables.fnaId
                }
            };
            PersistenceMapping.Type = "FNA";
            var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
            transactionObj.TransTrackingID = IllustratorVariables.fnaId;

            DataService.getFilteredListing(transactionObj, function (fnaData) {
                navigateToChooseParty(fnaData);
            });
        }
    }

    $scope.populatePolicyHolderToMainInsured = function (model) {

        $scope.Illustration.Insured = angular.copy(globalService.getInsured());
        if (model == "Self") {
            $scope.Illustration.Insured.BasicDetails.firstName = $scope.Illustration.Payer.BasicDetails.firstName;
            $scope.Illustration.Insured.BasicDetails.lastName = $scope.Illustration.Payer.BasicDetails.lastName;
            $scope.Illustration.Insured.BasicDetails.fullName = $scope.Illustration.Payer.BasicDetails.firstName + " " + $scope.Illustration.Payer.BasicDetails.lastName;
            $scope.Illustration.Insured.BasicDetails.dob = $scope.Illustration.Payer.BasicDetails.dob;
            $scope.Illustration.Insured.BasicDetails.age = $scope.Illustration.Payer.BasicDetails.age;
            $scope.Illustration.Insured.BasicDetails.gender = $scope.Illustration.Payer.BasicDetails.gender;
            //$scope.Illustration.Insured.OccupationDetails.description = $scope.Illustration.Payer.OccupationDetails.description;
            //$scope.Illustration.Insured.OccupationDetails.jobClass = $scope.Illustration.Payer.OccupationDetails.jobClass;
            $scope.Illustration.Insured.IdentityDetails.idNo = $scope.Illustration.Payer.IdentityDetails.idNo;
            $scope.Illustration.Insured.IdentityDetails.idIssueDate = $scope.Illustration.Payer.IdentityDetails.idIssueDate;
            $scope.Illustration.Insured.IdentityDetails.idIssuePlace = $scope.Illustration.Payer.IdentityDetails.idIssuePlace;
            $scope.Illustration.Insured.BasicDetails.riskInformation.smoke = $scope.Illustration.Payer.BasicDetails.riskInformation.smoke;
            $scope.Illustration.Insured.ContactDetails.mobileNumber1 = $scope.Illustration.Payer.ContactDetails.mobileNumber1;
            $scope.Illustration.Insured.ContactDetails.currentAddress.houseNo = $scope.Illustration.Payer.ContactDetails.houseNo;
            $scope.Illustration.Insured.ContactDetails.currentAddress.street = $scope.Illustration.Payer.ContactDetails.street;
            $scope.Illustration.Insured.ContactDetails.currentAddress.city = $scope.Illustration.Payer.ContactDetails.city;
            $scope.Illustration.Insured.ContactDetails.currentAddress.district = $scope.Illustration.Payer.ContactDetails.district;
            $scope.Illustration.Insured.ContactDetails.currentAddress.ward = $scope.Illustration.Payer.ContactDetails.ward;
            $scope.Illustration.Insured.BasicDetails.nationality = $scope.Illustration.Payer.BasicDetails.nationality;
            $scope.Illustration.Insured.BasicDetails.countryofResidence = $scope.Illustration.Payer.BasicDetails.countryofResidence;
            $scope.Illustration.Insured.ContactDetails.currentAddress.addressLine1 = $scope.Illustration.Payer.ContactDetails.addressLine1;
            $scope.Illustration.Insured.ContactDetails.emailId = $scope.Illustration.Payer.ContactDetails.emailId;
            $scope.Illustration.Insured.BasicDetails.incomeRange = $scope.Illustration.Payer.BasicDetails.incomeRange;
            $scope.Illustration.Insured.CustomerRelationship.relationShipWithPO = model;
        } else if ($scope.Illustration.Insured.isInsured != undefined && $scope.Illustration.Insured.isInsured == true) {

            $scope.Illustration.Insured = angular.copy(globalService.getInsured());
            $scope.Illustration.Insured.CustomerRelationship.relationShipWithPO = model;
            if (model == "Spouse") {
                $scope.onGenderChangeMainInsuredToPolicyHolder();
            }
        } else {
            $scope.Illustration.Insured.BasicDetails.firstName = "";
            $scope.Illustration.Insured.BasicDetails.lastName = "";
            $scope.Illustration.Insured.BasicDetails.fullName =
                $scope.Illustration.Insured.BasicDetails.dob = "";
            $scope.Illustration.Insured.BasicDetails.age = "";
            $scope.Illustration.Insured.BasicDetails.gender = "";
            //$scope.Illustration.Insured.OccupationDetails.description = "";
            //$scope.Illustration.Insured.OccupationDetails.jobClass = "";
            $scope.Illustration.Insured.IdentityDetails.idNo = "";
            $scope.Illustration.Insured.IdentityDetails.idIssueDate = "";
            $scope.Illustration.Insured.IdentityDetails.idIssuePlace = "";
            $scope.Illustration.Insured.BasicDetails.riskInformation.smoke = "";
            $scope.Illustration.Insured.ContactDetails.mobileNumber1 = "";
            $scope.Illustration.Insured.ContactDetails.currentAddress.houseNo = "";
            $scope.Illustration.Insured.ContactDetails.currentAddress.street = "";
            $scope.Illustration.Insured.ContactDetails.currentAddress.city = "";
            $scope.Illustration.Insured.ContactDetails.currentAddress.district = "";
            $scope.Illustration.Insured.ContactDetails.currentAddress.ward = "";
            $scope.Illustration.Insured.BasicDetails.nationality = "";
            $scope.Illustration.Insured.BasicDetails.countryofResidence = "";
            $scope.Illustration.Insured.ContactDetails.emailId = "";
            $scope.Illustration.Insured.BasicDetails.incomeRange = "";
            $scope.Illustration.Insured.CustomerRelationship.relationShipWithPO = model;
        }

        if (model == "Spouse") {
            $scope.onGenderChangePolicyHolder();
        }
    }


    //Customize fields for Generali Thailand
    $scope.customizeForPersonalDetails = function (personalDetailsJSON, personalDetailsUITemplate) {
        if (typeof personalDetailsJSON.productName == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'productName');
        }
        if (typeof personalDetailsJSON.insuredFirstName == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'insuredFirstName')
        }
        if (typeof personalDetailsJSON.insuredLastName == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'insuredLastName')
        }
        if (typeof personalDetailsJSON.insuredNickName == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'insuredNickName');
        }
        if (typeof personalDetailsJSON.insuredTitle == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'insuredTitle');
        }
        if (typeof personalDetailsJSON.insuredGender == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'insuredGender');
        }
        if (typeof personalDetailsJSON.insuredDOB == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'insuredDOB');
        }
        if (typeof personalDetailsJSON.insuredAge == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'insuredAge');
        }
        if (typeof personalDetailsJSON.occupationCodeInsured == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'occupationCodeInsured');
        }
        if (typeof personalDetailsJSON.occupationInsured == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'occupationInsured');
        }
        if (typeof personalDetailsJSON.occupationJobInsured == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'occupationJobInsured');
        }
        if (typeof personalDetailsJSON.occupationIndustryInsured == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'occupationIndustryInsured');
        }
        if (typeof personalDetailsJSON.occupationClassInsured == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'occupationClassInsured');
        }
        if (typeof personalDetailsJSON.contactNumberInsured == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'contactNumberInsured');
        }
        if (typeof personalDetailsJSON.lineIDInsured == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'lineIDInsured');
        }
        if (typeof personalDetailsJSON.emailIDInsured == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'emailIDInsured');
        }
        if (typeof personalDetailsJSON.insuredSameAsPayer == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'insuredSameAsPayer');
        }
        if (typeof personalDetailsJSON.payorFirstName == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'payorFirstName');
        }
        if (typeof personalDetailsJSON.payorLastName == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'payorLastName');
        }
        if (typeof personalDetailsJSON.payorTitle == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'payorTitle');
        }
        if (typeof personalDetailsJSON.payorNickName == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'payorNickName');
        }
        if (typeof personalDetailsJSON.payorGender == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'payorGender');
        }
        if (typeof personalDetailsJSON.policyHolderDOB == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'policyHolderDOB');
        }
        if (typeof personalDetailsJSON.payorAge == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'payorAge');
        }
        if (typeof personalDetailsJSON.occupationCodePayor == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'occupationCodePayor');
        }
        if (typeof personalDetailsJSON.occupationIndustryPayor == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'occupationIndustryPayor');
        }
        if (typeof personalDetailsJSON.occupationPayor == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'occupationPayor');
        }
        if (typeof personalDetailsJSON.occupationJobPayor == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'occupationJobPayor');
        }
        if (typeof personalDetailsJSON.occupationSubJobPayor == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'occupationSubJobPayor');
        }
        if (typeof personalDetailsJSON.contactNumberPayor == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'contactNumberPayor');
        }
        if (typeof personalDetailsJSON.lineIDPayor == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'lineIDPayor');
        }
        if (typeof personalDetailsJSON.emailIDPayor == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'emailIDPayor');
        }
        if (typeof personalDetailsJSON.natureofWorkInsured == 'undefined') {
            personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'natureOfWorkInsured');
        }
        return personalDetailsUITemplate;
    }

    function returnGender(gender) {
        if (gender == 'Male')
            return 'Female';
        else if (gender == 'Female')
            return 'Male';
        else
            return "";
    }

    // changing the gender of additional insured(if any) according to main insured gender.
    $scope.onGenderChangeMainInsured = function () {
        if ($scope.Illustration.Insured.CustomerRelationship && $scope.Illustration.Insured.CustomerRelationship.relationShipWithPO == 'Spouse') {
            if ($scope.Illustration.Payer.BasicDetails.gender != "") {
                var gender = returnGender($scope.Illustration.Payer.BasicDetails.gender);
                if (gender != 0) {
                    $scope.Illustration.Insured.BasicDetails.gender = gender;
                    $scope.disableInsuredGender = true;
                }
            }
        }
    }

    $scope.$watch('Illustration.Insured.BasicDetails.isInsuredSameAsPayer', function(value) {
        if(value=="No" &&  $rootScope.navFlag==true && $scope.Illustration.Insured.BasicDetails.age<16){
                 $scope.onChangeSameAsPayer("No");
                $rootScope.navFlag==false; 
                 }else{
                     $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer=value;
                 }
        }, true);
    
    $scope.onChangeSameAsPayer = function (sameAsPayer) {        
        if (sameAsPayer == "Yes") {
                $scope.Illustration.Payer = angular.copy($scope.Illustration.Insured);            
                $scope.samePayor = true;
                $scope.updateErrorDynamically();
        } else {
            
                $scope.samePayor = false;
                $scope.Illustration.Payer.BasicDetails = "";            		   
    //            $scope.Illustration.Payer.OccupationDetails[0].occupationCode="";  
    //            $scope.Illustration.Payer.OccupationDetails[0].occupationCodeValue="";  
    //            $scope.Illustration.Payer.OccupationDetails[0].industry="";
    //            $scope.Illustration.Payer.OccupationDetails[0].industryValue="";
                $scope.updateErrorDynamically();   
               if($scope.Illustration.Payer.OccupationDetails){
                    for(var i=0;i<$scope.Illustration.Payer.OccupationDetails.length;i++){
                        if($scope.Illustration.Payer.OccupationDetails[i]!==undefined){
                            $scope.Illustration.Payer.OccupationDetails[i]="";            
                        }
                    }
                }
                $scope.Illustration.Payer.ContactDetails = "";            
                $scope.showSecondaryPayor=false;
        }
    }
        
    if(($scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer=="Yes" && IllustratorVariables.illustrationStatus) || $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer=="Yes"){
        $scope.samePayor = true;
    }else{
        $scope.samePayor=false;
    }
    
    $scope.updatePayor = function () {        
        if ($scope.samePayor || $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer=="Yes") {
            $scope.onChangeSameAsPayer("Yes");
        }
    }

     $scope.setTitleCode = function (model) {
        if ($scope.title && model && model !== "") {
            var filteredValue = UtilityService.getFilteredDataBasedOnInput(model, $scope.title, 'value');
            if (filteredValue && filteredValue.length) {
                $scope.Illustration.Insured.BasicDetails.title = filteredValue[0].code;
            }
            $rootScope.updateErrorCount($scope.viewToBeCustomized, true);
        }
    };
    
	 $scope.setTitlePayerCode = function (model) {
        if ($scope.title && model && model !== "") {
            var filteredValue = UtilityService.getFilteredDataBasedOnInput(model, $scope.title, 'value');
            if (filteredValue && filteredValue.length) {
                $scope.Illustration.Payer.BasicDetails.title = filteredValue[0].code;
            }
            $rootScope.updateErrorCount($scope.viewToBeCustomized, true);
        }
    };
	
    $scope.onGenderChangeAdditionalInuredToPolicyHolder = function (addInsuredGender) {
        var gender = returnGender(addInsuredGender);
        if (gender != 0) {
            if ($scope.Illustration.Insured.CustomerRelationship.relationShipWithPO == 'Self') {
                $scope.Illustration.Insured.BasicDetails.gender = gender;
                $scope.disableInsuredGender = true;
                $scope.Illustration.Payer.BasicDetails.gender = gender;
            } else {
                $scope.Illustration.Payer.BasicDetails.gender = gender;
            }
        }
    }

    $scope.onGenderChangePolicyHolder = function () {
        if ($scope.Illustration.Insured.CustomerRelationship && $scope.Illustration.Insured.CustomerRelationship.relationShipWithPO == 'Spouse') {
            var gender = returnGender($scope.Illustration.Payer.BasicDetails.gender);
            if (gender != 0) {
                $scope.Illustration.Insured.BasicDetails.gender = gender;
                $scope.disableInsuredGender = true;
            }
        }
    }

    $scope.onGenderChangeMainInsuredToPolicyHolder = function () {
        if ($scope.Illustration.Insured.CustomerRelationship && $scope.Illustration.Insured.CustomerRelationship.relationShipWithPO == 'Spouse') {
            var gender = returnGender($scope.Illustration.Payer.BasicDetails.gender);
            if (gender != 0) {
                $scope.Illustration.Insured.BasicDetails.gender = gender;
                //$scope.disablePayorGender=true;
            }
        }
    }

    var transactionObj = {
        "CheckDate": new Date().getMonth() + 1 + "-" + new Date().getDate() + "-" + new Date().getFullYear(),
        "Products": {
            "id": $routeParams.productId,
            "code": "",
            "carrierCode": 1
        }
    }
    $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
    $scope.refresh();

    $scope.onSelectTypeHead = function (id) {
        $timeout(function () {
            $('#' + id).blur();
        }, 300);
    }
    
    $scope.markAsEdited = function(id) {
        $timeout(function () {
            $('#' + id).blur();
        }, 300);
    }

    $scope.validateOccupationClassInsured = function () {
        if ($scope.Illustration.Insured.OccupationDetails.length<2) {    
            if($scope.Illustration.Insured.OccupationDetails[0]!==undefined){
            $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
            }
        } else if($scope.Illustration.Insured.OccupationDetails[0]!==undefined && $scope.Illustration.Insured.OccupationDetails[1]!==undefined){
            if (!isNaN($scope.Illustration.Insured.OccupationDetails[0].occupationClass) && !isNaN($scope.Illustration.Insured.OccupationDetails[1].occupationClass)) {
                if ($scope.Illustration.Insured.OccupationDetails[0].occupationClass > $scope.Illustration.Insured.OccupationDetails[1].occupationClass) {
                    $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
                } else {
                    $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[1].occupationClass;
                }
            } else if (isNaN($scope.Illustration.Insured.OccupationDetails[0].occupationClass) && isNaN($scope.Illustration.Insured.OccupationDetails[1].occupationClass)) {
                if ($scope.Illustration.Insured.OccupationDetails[0].occupationClass == "D") {
                    $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
                } else if ($scope.Illustration.Insured.OccupationDetails[1].occupationClass == "D") {
                    $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[1].occupationClass;
                } else {
                    $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
                }
            } else {
                if (isNaN($scope.Illustration.Insured.OccupationDetails[0].occupationClass)) {
                    $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
                }

                if (isNaN($scope.Illustration.Insured.OccupationDetails[1].occupationClass)) {
                    $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[1].occupationClass;
                }
            }
        }         
    }
       
    $scope.validateOccupationClassPayer = function () {
        if ($scope.Illustration.Payer.OccupationDetails.length<2) { 
            if($scope.Illustration.Payer.OccupationDetails[0]!==undefined){
            $scope.Illustration.Payer.BasicDetails.occupationClassCode = $scope.Illustration.Payer.OccupationDetails[0].occupationClass;
            }
        } else if($scope.Illustration.Payer.OccupationDetails[0]!==undefined && $scope.Illustration.Payer.OccupationDetails[1]!==undefined){
            if (!isNaN($scope.Illustration.Payer.OccupationDetails[0].occupationClass) && !isNaN($scope.Illustration.Payer.OccupationDetails[1].occupationClass)) {
                if ($scope.Illustration.Payer.OccupationDetails[0].occupationClass > $scope.Illustration.Payer.OccupationDetails[1].occupationClass) {
                    $scope.Illustration.Payer.BasicDetails.occupationClassCode = $scope.Illustration.Payer.OccupationDetails[0].occupationClass;
                } else {
                    $scope.Illustration.Payer.BasicDetails.occupationClassCode = $scope.Illustration.Payer.OccupationDetails[1].occupationClass;
                }
            } else if (isNaN($scope.Illustration.Payer.OccupationDetails[0].occupationClass) && isNaN($scope.Illustration.Payer.OccupationDetails[1].occupationClass)) {
                if ($scope.Illustration.Payer.OccupationDetails[0].occupationClass == "D") {
                    $scope.Illustration.Payer.BasicDetails.occupationClassCode = $scope.Illustration.Payer.OccupationDetails[0].occupationClass;
                } else if ($scope.Illustration.Payer.OccupationDetails[1].occupationClass == "D") {
                    $scope.Illustration.Payer.BasicDetails.occupationClassCode = $scope.Illustration.Payer.OccupationDetails[1].occupationClass;
                } else {
                    $scope.Illustration.Payer.BasicDetails.occupationClassCode = $scope.Illustration.Payer.OccupationDetails[0].occupationClass;
                }
            } else {
                if (isNaN($scope.Illustration.Payer.OccupationDetails[0].occupationClass)) {
                    $scope.Illustration.Payer.BasicDetails.occupationClassCode = $scope.Illustration.Payer.OccupationDetails[0].occupationClass;
                }

                if (isNaN($scope.Illustration.Payer.OccupationDetails[1].occupationClass)) {
                    $scope.Illustration.Payer.BasicDetails.occupationClassCode = $scope.Illustration.Payer.OccupationDetails[1].occupationClass;
                }
            }
        } 
    }
    
    /*Insured Details*/
    $scope.formatLabelOccupation = function (model, options, type, index) {
        var x = parseInt(index);
        
        if (options && options.length >= 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code && ((type !== 'industry' && ($scope.Illustration.Insured.OccupationDetails[x].industry==="" || $scope.Illustration.Insured.OccupationDetails[x].industry===undefined)) || (type !== 'industry' && $scope.allowCodeUpdate)) && !$scope.SaveTrans) {
                    if (type == 'occupationCode') {
                        $scope.Illustration.Insured.OccupationDetails[x].id=x;
                        $scope.Illustration.Insured.OccupationDetails[x].occupationCodeValue = options[i].value;
                        $scope.updateCode = true;
                    }
                    return options[i].value;
                }

                if (options.length > 1 && type == 'industry' && ($scope.allowCodeUpdate || $scope.updateCode) && !$scope.SaveTrans) {
                    $scope.Illustration.Insured.OccupationDetails[x].industryValue = options[0].value;
                    $scope.Illustration.Insured.OccupationDetails[x].descriptionValue = options[1].value;
                    $scope.Illustration.Insured.OccupationDetails[x].jobValue = options[2].value;
                    $scope.Illustration.Insured.OccupationDetails[x].subJobValue = options[3].value;
                    $scope.Illustration.Insured.OccupationDetails[x].occupationClassValue = options[4].value;
                    $scope.Illustration.Insured.OccupationDetails[x].natureofWorkValue = options[5].value;

                    $scope.Illustration.Insured.OccupationDetails[x].industry = options[0].value;
                    $scope.Illustration.Insured.OccupationDetails[x].description = options[1].value;
                    $scope.Illustration.Insured.OccupationDetails[x].job = options[2].value;
                    $scope.Illustration.Insured.OccupationDetails[x].subJob = options[3].value;
                    $scope.Illustration.Insured.OccupationDetails[x].occupationClass = options[4].value;
                    $scope.Illustration.Insured.OccupationDetails[x].natureofWork = options[5].value;
                                       
                    if(!angular.equals(undefined,$scope.Illustration.Insured.OccupationDetails)){  
                        
                    for(var i=0;i<$scope.Illustration.Insured.OccupationDetails.length,i<x;i++){
                        if(!angular.equals(undefined,$scope.Illustration.Insured.OccupationDetails[i])){
                            if(!angular.equals(undefined,$scope.Illustration.Insured.OccupationDetails[i].occupationCode) && $scope.Illustration.Insured.OccupationDetails[i].occupationCode!==''){
						  if($scope.Illustration.Insured.OccupationDetails[x].occupationCode == $scope.Illustration.Insured.OccupationDetails[i].occupationCode){
							
                            $scope.Illustration.Insured.OccupationDetails[x]=[];
                            $scope.leadDetShowPopUpMsg = true;
                            $scope.enterMandatory = translateMessages($translate, "illustrator.duplicateOccupation");
                         
						  }
                        }
                        }
					   }
                    }
					$scope.refresh();
                                                           
                    $scope.updateCode = false;
                    return options[0].value;
                }
            
            if(!angular.equals(undefined, $scope.Illustration.Insured.OccupationDetails[x]) && $scope.Illustration.Insured.OccupationDetails[x]!= ""){
                if (type == 'occupationCode' && ($scope.updateCode || $scope.Illustration.Insured.OccupationDetails[x].industry)) {
                    if(x===1){
                        $scope.showSecondary=true;
                    }else{
                        $scope.showSecondary=false;
                    }
                    if(!angular.equals(undefined, $scope.Illustration.Insured.OccupationDetails[x].occupationCodeValue)){
                        return $scope.Illustration.Insured.OccupationDetails[x].occupationCodeValue;
					}else{
						return $scope.Illustration.Insured.OccupationDetails[x].occupationCode;
					}
					
                } else if (type == 'industry' && ((!IllustratorVariables.illustrationStatus && !$scope.allowCodeUpdate) || (IllustratorVariables.illustrationStatus && !$scope.allowCodeUpdate) || (!IllustratorVariables.illustrationStatus && $scope.Illustration.Insured.OccupationDetails[x].occupationCode) || (IllustratorVariables.illustrationStatus && $scope.Illustration.Insured.OccupationDetails[x].occupationCode) || $scope.SaveTrans)) {
                    $scope.updateErrorDynamically();
                    
                    return model;
                }                
            }
            }
        }
    }

    $scope.formatLabelOccupationFromNOW = function (model, options, type, index) {
        var x = parseInt(index);
        
        if (options && options.length >= 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code && ((type !== 'industry' && ($scope.Illustration.Insured.OccupationDetails[x].occupationCode==="" || $scope.Illustration.Insured.OccupationDetails[x].occupationCode===undefined)) || (type !== 'industry' && $scope.allowCodeUpdate)) && !$scope.SaveTrans) {
                    if (type == 'natureOfWorkLinear') {
                        $scope.Illustration.Insured.OccupationDetails[x].id=x;
                        $scope.Illustration.Insured.OccupationDetails[x].natureofWorkValue = options[i].value;

                        $scope.updateCode = true;
                    }
                    return options[i].value;
                }

                if (options.length > 1 && type == 'occupationCode' && ($scope.allowCodeUpdate || $scope.updateCode) && !$scope.SaveTrans) {
                     $scope.Illustration.Insured.OccupationDetails[x].occupationCodeValue = options[0].value;
                    $scope.Illustration.Insured.OccupationDetails[x].industryValue = options[1].value;
                    $scope.Illustration.Insured.OccupationDetails[x].descriptionValue = options[2].value;
                    $scope.Illustration.Insured.OccupationDetails[x].jobValue = options[3].value;
                    $scope.Illustration.Insured.OccupationDetails[x].subJobValue = options[4].value;
                    $scope.Illustration.Insured.OccupationDetails[x].occupationClassValue = options[5].value;
                    
                    $scope.Illustration.Insured.OccupationDetails[x].occupationCode = options[0].value;
                    $scope.Illustration.Insured.OccupationDetails[x].industry = options[1].value;
                    $scope.Illustration.Insured.OccupationDetails[x].description = options[2].value;
                    $scope.Illustration.Insured.OccupationDetails[x].job = options[3].value;
                    $scope.Illustration.Insured.OccupationDetails[x].subJob = options[4].value;
                    $scope.Illustration.Insured.OccupationDetails[x].occupationClass = options[5].value;
                    
                                       
                    if(!angular.equals(undefined,$scope.Illustration.Insured.OccupationDetails) && $scope.Illustration.Insured.OccupationDetails[x]!= ""){  
                        
                    for(var i=0;i<$scope.Illustration.Insured.OccupationDetails.length,i<x;i++){
                        if(!angular.equals(undefined,$scope.Illustration.Insured.OccupationDetails[i])){
                            if(!angular.equals(undefined,$scope.Illustration.Insured.OccupationDetails[i].occupationCode) && $scope.Illustration.Insured.OccupationDetails[i].occupationCode!==''){
                          if($scope.Illustration.Insured.OccupationDetails[x].occupationCode == $scope.Illustration.Insured.OccupationDetails[i].occupationCode){
                            
                            $scope.Illustration.Insured.OccupationDetails[x]=[];
                            $scope.leadDetShowPopUpMsg = true;
                            $scope.enterMandatory = translateMessages($translate, "illustrator.duplicateOccupation");
                         
                          }
                        }
                        }
                       }
                    }
                    $scope.refresh();
                                                           
                    $scope.updateCode = false;
                    return options[0].value;
                }
            
            if(!angular.equals(undefined, $scope.Illustration.Insured.OccupationDetails[x])){
                if (type == 'natureOfWorkLinear' && ($scope.updateCode || $scope.Illustration.Insured.OccupationDetails[x].occupationCode)) {
                    if(x===1){
                        $scope.showSecondary=true;
                    }else{
                        $scope.showSecondary=false;
                    }
                    if(!angular.equals(undefined, $scope.Illustration.Insured.OccupationDetails[x].natureofWorkValue)){
                        return $scope.Illustration.Insured.OccupationDetails[x].natureofWorkValue;
                    }else{
                        return $scope.Illustration.Insured.OccupationDetails[x].natureofWork;
                    }
                    
                } else if (type == 'occupationCode' && ((!IllustratorVariables.illustrationStatus && !$scope.allowCodeUpdate) || (IllustratorVariables.illustrationStatus && !$scope.allowCodeUpdate) || (!IllustratorVariables.illustrationStatus && $scope.Illustration.Insured.OccupationDetails[x].occupationCode) || (IllustratorVariables.illustrationStatus && $scope.Illustration.Insured.OccupationDetails[x].occupationCode) || $scope.SaveTrans)) {
                    $scope.updateErrorDynamically();
                    
                    return model;
                }                
            }
            }
        }
    }

    $scope.addOccupation = function () {        
        if ($scope.Illustration.Insured.OccupationDetails[0].occupationCode!=="" && !angular.equals(undefined, $scope.Illustration.Insured.OccupationDetails[0].occupationCode)) {
            $scope.showSecondary = true;
        }
        if($scope.Illustration.Insured.OccupationDetails[2]!==undefined){
        	if ($scope.Illustration.Insured.OccupationDetails[2].occupationCode!=="" && !angular.equals(undefined, $scope.Illustration.Insured.OccupationDetails[2].occupationCode)) {
        		$scope.showSecondary = true;
        	}
        }
        $scope.updatePayor();
    }

    $scope.removeOccupation = function () {
        $scope.showSecondary = false;
        //$scope.Illustration.Insured.OccupationDetails[1].occupationCode='';
        $scope.Illustration.Insured.OccupationDetails[1]="";
        $scope.Illustration.Insured.OccupationDetails[3]="";
//        $scope.Illustration.OccupationInsured.splice(1, 1);
        $scope.updatePayor();
    }

    
    /*Payor Details*/
    $scope.formatLabelOccupationPayor = function (model, options, type, index) {
        var y = parseInt(index);
        if (options && options.length >= 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code && ((type !== 'industry' && ($scope.Illustration.Payer.OccupationDetails[y].industry==="" || $scope.Illustration.Payer.OccupationDetails[y].industry===undefined)) || (type !== 'industry' && $scope.allowCodeUpdatePayor)) && $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer=="No" && !$scope.SaveTrans) {
                    if (type == 'occupationCode') {
                        $scope.Illustration.Payer.OccupationDetails[y].id=y;
                        $scope.Illustration.Payer.OccupationDetails[y].occupationCodeValue = options[i].value;
                       
                        $scope.updateCodePayor = true;
                    }
                    return options[i].value;
                }
                if (options.length > 1 && type == 'industry' && ($scope.allowCodeUpdatePayor || $scope.updateCodePayor) && $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer=="No" && !$scope.SaveTrans) {
                    $scope.Illustration.Payer.OccupationDetails[y].industryValue = options[0].value;
                    $scope.Illustration.Payer.OccupationDetails[y].descriptionValue = options[1].value;
                    $scope.Illustration.Payer.OccupationDetails[y].jobValue = options[2].value;
                    $scope.Illustration.Payer.OccupationDetails[y].subJobValue = options[3].value;
                    $scope.Illustration.Payer.OccupationDetails[y].occupationClassValue = options[4].value;
                    $scope.Illustration.Payer.OccupationDetails[y].natureofWorkValue = options[5].value;


                    $scope.Illustration.Payer.OccupationDetails[y].industry = options[0].value;
                    $scope.Illustration.Payer.OccupationDetails[y].description = options[1].value;
                    $scope.Illustration.Payer.OccupationDetails[y].job = options[2].value;
                    $scope.Illustration.Payer.OccupationDetails[y].subJob = options[3].value;
                    $scope.Illustration.Payer.OccupationDetails[y].occupationClass = options[4].value;
                    $scope.Illustration.Payer.OccupationDetails[y].natureofWork = options[5].value;
                   
                    
                    if(!angular.equals(undefined,$scope.Illustration.Payer.OccupationDetails)){
                    for(var i=0;i<$scope.Illustration.Payer.OccupationDetails.length,i<y;i++){
						if(!angular.equals(undefined,$scope.Illustration.Payer.OccupationDetails[i])){
                            if($scope.Illustration.Payer.OccupationDetails[y].occupationCode == $scope.Illustration.Payer.OccupationDetails[i].occupationCode){
							
                            $scope.Illustration.Payer.OccupationDetails[y]=[];
                            $scope.leadDetShowPopUpMsg = true;
                            $scope.enterMandatory = translateMessages($translate, "illustrator.duplicateOccupation");

						  }
                        }
					   }
                    }
					$scope.refresh();
                    
                    $scope.updateCodePayor = false;
                    return options[0].value;
                }
            if(!angular.equals(undefined, $scope.Illustration.Payer.OccupationDetails[y]) && $scope.Illustration.Payer.OccupationDetails[y]!= ""){
                if (type == 'occupationCode' && ($scope.updateCodePayor || $scope.Illustration.Payer.OccupationDetails[y].industry)) {
                    if(y===1){
                        $scope.showSecondaryPayor=true;
                    }else{
                        $scope.showSecondaryPayor=false;
                    }
                    if(!angular.equals(undefined,$scope.Illustration.Payer.OccupationDetails[y].occupationCodeValue)){
                        return $scope.Illustration.Payer.OccupationDetails[y].occupationCodeValue;
                    }else{
                        return $scope.Illustration.Payer.OccupationDetails[y].occupationCode;
                    }
                } else if (type == 'industry' && ((!IllustratorVariables.illustrationStatus && !$scope.allowCodeUpdatePayor) || (IllustratorVariables.illustrationStatus && !$scope.allowCodeUpdatePayor) || (!IllustratorVariables.illustrationStatus && $scope.Illustration.Payer.OccupationDetails[y].occupationCode) || (IllustratorVariables.illustrationStatus && $scope.Illustration.Payer.OccupationDetails[y].occupationCode))) {
                    $scope.updateErrorDynamically();
                    return model;
                }
            }
            }
        }
    }

    $scope.formatLabelOccupationFromNOWPayor = function (model, options, type, index) {
        var y = parseInt(index);
        if (options && options.length >= 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code && ((type !== 'industry' && ($scope.Illustration.Payer.OccupationDetails[y].occupationCode==="" || $scope.Illustration.Payer.OccupationDetails[y].occupationCode===undefined)) || (type !== 'industry' && $scope.allowCodeUpdatePayor)) && $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer=="No" && !$scope.SaveTrans) {
                    if (type == 'natureOfWorkLinear') {
                        $scope.Illustration.Payer.OccupationDetails[y].id=y;
                        $scope.Illustration.Payer.OccupationDetails[y].natureofWorkValue = options[i].value;
                       
                        $scope.updateCodePayor = true;
                    }
                    return options[i].value;
                }
                if (options.length > 1 && type == 'occupationCode' && ($scope.allowCodeUpdatePayor || $scope.updateCodePayor) && $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer=="No" && !$scope.SaveTrans) {
                    $scope.Illustration.Payer.OccupationDetails[y].occupationCodeValue = options[0].value;
                    $scope.Illustration.Payer.OccupationDetails[y].industryValue = options[1].value;
                    $scope.Illustration.Payer.OccupationDetails[y].descriptionValue = options[2].value;
                    $scope.Illustration.Payer.OccupationDetails[y].jobValue = options[3].value;
                    $scope.Illustration.Payer.OccupationDetails[y].subJobValue = options[4].value;
                    $scope.Illustration.Payer.OccupationDetails[y].occupationClassValue = options[5].value;
                    

                    $scope.Illustration.Payer.OccupationDetails[y].occupationCode = options[0].value;
                    $scope.Illustration.Payer.OccupationDetails[y].industry = options[1].value;
                    $scope.Illustration.Payer.OccupationDetails[y].description = options[2].value;
                    $scope.Illustration.Payer.OccupationDetails[y].job = options[3].value;
                    $scope.Illustration.Payer.OccupationDetails[y].subJob = options[4].value;
                    $scope.Illustration.Payer.OccupationDetails[y].occupationClass = options[5].value;
                                       
                    
                    if(!angular.equals(undefined,$scope.Illustration.Payer.OccupationDetails)){
                    for(var i=0;i<$scope.Illustration.Payer.OccupationDetails.length,i<y;i++){
                        if(!angular.equals(undefined,$scope.Illustration.Payer.OccupationDetails[i])){
                            if($scope.Illustration.Payer.OccupationDetails[y].occupationCode == $scope.Illustration.Payer.OccupationDetails[i].occupationCode){
                            
                            $scope.Illustration.Payer.OccupationDetails[y]=[];
                            $scope.leadDetShowPopUpMsg = true;
                            $scope.enterMandatory = translateMessages($translate, "illustrator.duplicateOccupation");

                          }
                        }
                       }
                    }
                    $scope.refresh();
                    
                    $scope.updateCodePayor = false;
                    return options[0].value;
                }
            if(!angular.equals(undefined, $scope.Illustration.Payer.OccupationDetails[y]) && $scope.Illustration.Payer.OccupationDetails[y]!= ""){
                if (type == 'natureOfWorkLinear' && ($scope.updateCodePayor || $scope.Illustration.Payer.OccupationDetails[y].occupationCode)) {
                    if(y===1){
                        $scope.showSecondaryPayor=true;
                    }else{
                        $scope.showSecondaryPayor=false;
                    }
                    if(!angular.equals(undefined,$scope.Illustration.Payer.OccupationDetails[y].natureofWorkValue)){
                        return $scope.Illustration.Payer.OccupationDetails[y].natureofWorkValue;
                    }else{
                        return $scope.Illustration.Payer.OccupationDetails[y].natureofWork;
                    }
                } else if (type == 'occupationCode' && ((!IllustratorVariables.illustrationStatus && !$scope.allowCodeUpdatePayor) || (IllustratorVariables.illustrationStatus && !$scope.allowCodeUpdatePayor) || (!IllustratorVariables.illustrationStatus && $scope.Illustration.Payer.OccupationDetails[y].occupationCode) || (IllustratorVariables.illustrationStatus && $scope.Illustration.Payer.OccupationDetails[y].occupationCode))) {
                   
                    $scope.updateErrorDynamically();                    
                    return model;
                }
            }
            }
        }
    }

    $scope.addOccupationPayor = function () {
        if ($scope.Illustration.Payer.OccupationDetails[0]!=="" && !angular.equals(undefined, $scope.Illustration.Payer.OccupationDetails[0].occupationCode)) {
            $scope.showSecondaryPayor = true;
        }
        if (!angular.equals(undefined, $scope.Illustration.Payer.OccupationDetails[2]) && $scope.Illustration.Payer.OccupationDetails[2]!=="" && !angular.equals(undefined, $scope.Illustration.Payer.OccupationDetails[2].occupationCode)) {
            $scope.showSecondaryPayor = true;
        }
    }

    $scope.removeOccupationPayor = function () {
        $scope.showSecondaryPayor = false;
        //$scope.Illustration.Payer.OccupationDetails[1].occupationCode="";
        $scope.Illustration.Payer.OccupationDetails[1]="";        
        $scope.Illustration.Payer.OccupationDetails[3]="";
//        $scope.Illustration.OccupationPayer.splice(1, 1);
    }

    $scope.clearFields = function (val) {
        if (val == 'occupationCode' && $scope.Illustration.Insured.OccupationDetails[0].industry) {
            $scope.allowCodeUpdate = true;                                  
            $scope.Illustration.Insured.OccupationDetails[0]="";
            $scope.updateErrorDynamically();
        } else if (val == 'occupationCodeNew' && $scope.Illustration.Insured.OccupationDetails[1].industry) {
            $scope.allowCodeUpdate = true;
            $scope.Illustration.Insured.OccupationDetails[1]="";
            $scope.updateErrorDynamically();
        } else if (val == 'occupationCodePayor') {
            $scope.allowCodeUpdatePayor = true;
            $scope.Illustration.Payer.OccupationDetails[0]="";
            $scope.updateErrorDynamically();
        } else if (val == 'occupationCodePayorNew') {
            $scope.allowCodeUpdatePayor = true;
            $scope.Illustration.Payer.OccupationDetails[1]=""; 
            $scope.updateErrorDynamically();
        } else if (val == 'natureOfWorkLinear' && $scope.Illustration.Insured.OccupationDetails[2].occupationCode) {
            $scope.allowCodeUpdate = true;
            $scope.Illustration.Insured.OccupationDetails[2]=""; 
            $scope.updateErrorDynamically();
        } else if (val == 'natureOfWorkLinear2' && $scope.Illustration.Insured.OccupationDetails[3].occupationCode) {
            $scope.allowCodeUpdate = true;
            $scope.Illustration.Insured.OccupationDetails[3]=""; 
            $scope.updateErrorDynamically();
        } else if (val == 'natureOfWorkLinearPayor' && $scope.Illustration.Payer.OccupationDetails[2].occupationCode) {
            $scope.allowCodeUpdatePayor = true;
            $scope.Illustration.Payer.OccupationDetails[2]=""; 
            $scope.updateErrorDynamically();
        } else if (val == 'natureOfWorkLinearPayor2' && $scope.Illustration.Payer.OccupationDetails[3].occupationCode) {
            $scope.allowCodeUpdatePayor = true;
            $scope.Illustration.Payer.OccupationDetails[3]=[]; 
            $scope.updateErrorDynamically();
        }  
    };
    
    $scope.clearField=function(val,index){
        if (val == 'LinearInsured') {            
            $scope.Illustration.Insured.OccupationDetails[index]=[];
        }if (val == 'LinearOccupation') {                        
            var industry=$scope.Illustration.Insured.OccupationDetails[index].industry;
                       
            $scope.Illustration.Insured.OccupationDetails[index]=[];
            
            $scope.Illustration.Insured.OccupationDetails[index].industry = industry;          
        }else if (val == 'LinearJob') {
            var industry=$scope.Illustration.Insured.OccupationDetails[index].industry;
            var description=$scope.Illustration.Insured.OccupationDetails[index].description;            
            
            $scope.Illustration.Insured.OccupationDetails[index]=[];
            
            $scope.Illustration.Insured.OccupationDetails[index].industry = industry;
            $scope.Illustration.Insured.OccupationDetails[index].description = description;               
        }else if(val == 'LinearSubjob'){  
            var industry=$scope.Illustration.Insured.OccupationDetails[index].industry;
            var description=$scope.Illustration.Insured.OccupationDetails[index].description;
            var job=$scope.Illustration.Insured.OccupationDetails[index].job;
            
            $scope.Illustration.Insured.OccupationDetails[index]=[];
            
            $scope.Illustration.Insured.OccupationDetails[index].industry = industry;
            $scope.Illustration.Insured.OccupationDetails[index].description = description;    
            $scope.Illustration.Insured.OccupationDetails[index].job = job;
        }else if (val == 'LinearPayer') {            
            $scope.Illustration.Payer.OccupationDetails[index]=[];
        }if (val == 'LinearPayerOccupation') { 
            var industry=$scope.Illustration.Payer.OccupationDetails[index].industry;
            
            $scope.Illustration.Payer.OccupationDetails[index]=[];
            
            $scope.Illustration.Payer.OccupationDetails[index].industry=industry;
        }else if (val == 'LinearPayerJob') {
            var industry=$scope.Illustration.Payer.OccupationDetails[index].industry;
            var description=$scope.Illustration.Payer.OccupationDetails[index].description;
            
            $scope.Illustration.Payer.OccupationDetails[index]=[];
            
            $scope.Illustration.Payer.OccupationDetails[index].industry=industry;
            $scope.Illustration.Payer.OccupationDetails[index].description=description;
        }else if(val == 'LinearPayerSubjob'){      
            var industry=$scope.Illustration.Payer.OccupationDetails[index].industry;
            var description=$scope.Illustration.Payer.OccupationDetails[index].description;
            var job=$scope.Illustration.Payer.OccupationDetails[index].job;
            
            $scope.Illustration.Payer.OccupationDetails[index]=[];
            
            $scope.Illustration.Payer.OccupationDetails[index].industry=industry;
            $scope.Illustration.Payer.OccupationDetails[index].description=description;
            $scope.Illustration.Payer.OccupationDetails[index].job=job;
        }
    }
    $scope.formatLabel = function (model, options, type) {
        if (options && options.length >= 1) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code) {
                    return options[i].value;
                }
            }
        }
    };
    
    $scope.validateInsuredAge=function(){  
        $scope.errorFlag=false;
        
        if($scope.titleGenderPairPayer!==undefined && $scope.titleGenderPairInsured!==undefined && $scope.Illustration.Insured.BasicDetails.gender!==undefined && $scope.Illustration.Insured.BasicDetails.gender!=="" && $scope.Illustration.Payer.BasicDetails.gender!==undefined && $scope.Illustration.Payer.BasicDetails.gender!=="" && $scope.Illustration.Insured.BasicDetails.title!==undefined && $scope.Illustration.Insured.BasicDetails.title!=="" && $scope.Illustration.Payer.BasicDetails.title!==undefined && $scope.Illustration.Payer.BasicDetails.title!=="" && $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="No"){
            if($scope.titleGenderPairInsured[0].code!=="Both" && $scope.titleGenderPairInsured!=="Both" && $scope.titleGenderPairPayer[0].code!=="Both" && $scope.titleGenderPairPayer!=="Both"){
                if($scope.titleGenderPairInsured[0].code!==$scope.Illustration.Insured.BasicDetails.gender && $scope.titleGenderPairInsured!==$scope.Illustration.Insured.BasicDetails.gender && $scope.titleGenderPairPayer[0].code!==$scope.Illustration.Payer.BasicDetails.gender && $scope.titleGenderPairPayer!==$scope.Illustration.Payer.BasicDetails.gender){
                    $scope.errorFlag=true;
                    $scope.leadDetShowPopUpMsg = true;
                    $scope.enterMandatory = translateMessages($translate, "illustrator.titleGenderMismatch");
                    
                    $rootScope.showHideLoadingImage(false);
                }
            }
        }
        if($scope.titleGenderPairInsured!==undefined && $scope.errorFlag!==true && $scope.Illustration.Insured.BasicDetails.gender!==undefined && $scope.Illustration.Insured.BasicDetails.gender!=="" && $scope.Illustration.Insured.BasicDetails.title!==undefined && $scope.Illustration.Insured.BasicDetails.title!==""){            
                if($scope.titleGenderPairInsured[0].code!=="Both" && $scope.titleGenderPairInsured!=="Both"){
                    if($scope.titleGenderPairInsured[0].code!==$scope.Illustration.Insured.BasicDetails.gender && $scope.titleGenderPairInsured!==$scope.Illustration.Insured.BasicDetails.gender){
                        $scope.errorFlag=true;
                        $scope.leadDetShowPopUpMsg = true;
                        $scope.enterMandatory = translateMessages($translate, "illustrator.titleGenderMismatchInsured");
                        
                        $rootScope.showHideLoadingImage(false);
                    }
                }
        }
        
        if($scope.titleGenderPairPayer!==undefined && $scope.errorFlag!==true && $scope.Illustration.Payer.BasicDetails.gender!==undefined && $scope.Illustration.Payer.BasicDetails.gender!=="" && $scope.Illustration.Payer.BasicDetails.title!==undefined && $scope.Illustration.Payer.BasicDetails.title!=="" & $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="No"){            
                if($scope.titleGenderPairPayer[0].code!=="Both" && $scope.titleGenderPairPayer!=="Both"){
                    if($scope.titleGenderPairPayer[0].code!==$scope.Illustration.Payer.BasicDetails.gender && $scope.titleGenderPairPayer!==$scope.Illustration.Payer.BasicDetails.gender){
                        $scope.errorFlag=true;
                        $scope.leadDetShowPopUpMsg = true;
                        $scope.enterMandatory = translateMessages($translate, "illustrator.titleGenderMismatchPayer");
                        
                        $rootScope.showHideLoadingImage(false);
                    }
                }
        }        
        
        if($scope.Illustration.Insured.BasicDetails.age<16 && $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="Yes" && $scope.Illustration.Insured.BasicDetails.dob!=="" && $scope.Illustration.Insured.BasicDetails.dob!==undefined && $scope.errorFlag!==true){
            $scope.samePayor = false;
            $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer="No";
            $scope.onChangeSameAsPayer("No");
            $scope.errorFlag=true;
            $scope.updateErrorDynamically();
            $scope.leadDetShowPopUpMsg = true;
            $scope.enterMandatory = translateMessages($translate, "illustrator.insuredLessThan16");

            $rootScope.showHideLoadingImage(false);
        } 
        
        return $scope.errorFlag;
    }
    
    $scope.openPDF = function(pdfFile) {	
		if((rootConfig.isDeviceMobile) && (!rootConfig.isOfflineDesktop)){
            var fileFullPath = "";
            window.plugins.LEFileUtils.getApplicationPath(function (path) {
                fileFullPath = "file://" + path + "/" + pdfFile;
                cordova.exec(function () {}, function () {}, "PdfViewer", "showPdf", [fileFullPath]);
            }, function (e) {});
        }else if(((rootConfig.isDeviceMobile) && (rootConfig.isOfflineDesktop)) ||((!rootConfig.isDeviceMobile) && (!rootConfig.isOfflineDesktop))){
            MediaService.openPDF($scope, $window, pdfFile);
        }     
	}
    
    $scope.closeTC = function () {
        $scope.showInformation = false;
        $scope.overlayTC = false;
    };
        
    $scope.formatLabelOccupationLinear = function (model, options, type, index) {        
        var indexId = parseInt(index);
        $scope.setLinear=true;
        if (options && options.length >= 1 && !$scope.twoInsuredOccupation) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code) {
                    if (type == 'occupationCodeLinear' && !angular.equals(undefined, $scope.Illustration.Insured.OccupationDetails[indexId].subJob) && !$scope.SaveTrans) {	
                        $scope.Illustration.Insured.OccupationDetails[indexId].occupationCodeValue = options[i].value;
                        $scope.Illustration.Insured.OccupationDetails[indexId].id=indexId;
//						$scope.Illustration.Insured.OccupationDetails[indexId].occupationClassValue = options[i+1].value;
                        $scope.validateDuplicate(index);
                        
                    } else if (type === 'industryLinear' && !$scope.SaveTrans && ($scope.Illustration.Insured.OccupationDetails[indexId].description==='' || $scope.Illustration.Insured.OccupationDetails[indexId].description===undefined)) {	
                        $scope.Illustration.Insured.OccupationDetails[indexId].industryValue = options[i].value;
                    } else if (type == 'occupationLinear' && !angular.equals(undefined, $scope.Illustration.Insured.OccupationDetails[indexId].industry) && !$scope.SaveTrans) {
                        $scope.Illustration.Insured.OccupationDetails[indexId].descriptionValue = options[i].value;
                    } else if (type == 'jobLinear' && !angular.equals(undefined, $scope.Illustration.Insured.OccupationDetails[indexId].description) && !$scope.SaveTrans) {
                        $scope.Illustration.Insured.OccupationDetails[indexId].jobValue = options[i].value;
                    }else if (type == 'subjobLinear' && !angular.equals(undefined, $scope.Illustration.Insured.OccupationDetails[indexId].job) && !$scope.SaveTrans) {
                        $scope.Illustration.Insured.OccupationDetails[indexId].subJobValue = options[i].value;
                    }else if(type=='occupationClassLinear'){
                        $scope.Illustration.Insured.OccupationDetails[indexId].occupationClassValue = options[i].value;
                    }
                                        
                    return options[i].value;
                }
            }
        }
        $scope.twoInsuredOccupation=false;
    }
    
    $scope.validateDuplicate=function(index){       
            for(var i=0;i<$scope.Illustration.Insured.OccupationDetails.length,i<index;i++){ 
                if($scope.Illustration.Insured.OccupationDetails[i]){
                    if($scope.Illustration.Insured.OccupationDetails[index].occupationCode === $scope.Illustration.Insured.OccupationDetails[i].occupationCode){
							
                        $scope.Illustration.Insured.OccupationDetails[index]=[];
                        $scope.leadDetShowPopUpMsg = true;
                        $scope.enterMandatory = translateMessages($translate, "illustrator.duplicateOccupation");
//				        $scope.lePopupCtrl.showWarning(
//								translateMessages($translate,
//								"lifeEngage"),
//								translateMessages($translate,
//								"illustrator.duplicateOccupation"),
//								translateMessages($translate,
//								"fna.ok"));                            
						}
                    }
            }  
            $scope.updateErrorDynamically();
            $scope.refresh();
    }
    
    $scope.formatLabelOccupationLinearPayer = function (model, options, type, index) {
        var indexId = parseInt(index);
        $scope.setLinear=true;
        if (options && options.length >= 1 && !$scope.twoPayerOccupation) {
            for (var i = 0; i < options.length; i++) {
                if (model === options[i].code) {
                    if (type == 'occupationCodeLinear' && !angular.equals(undefined, $scope.Illustration.Payer.OccupationDetails[indexId].subJob) && !$scope.SaveTrans) {	
                        $scope.Illustration.Payer.OccupationDetails[indexId].occupationCodeValue = options[i].value;
                        $scope.Illustration.Payer.OccupationDetails[indexId].id=indexId;
//						$scope.Illustration.Payer.OccupationDetails[indexId].occupationClassValue = options[i+1].value;
                        $scope.validateDuplicatePayer(index);    
                        
                    } else if (type === 'industryLinear' && !$scope.SaveTrans && ($scope.Illustration.Payer.OccupationDetails[indexId].description==='' || $scope.Illustration.Payer.OccupationDetails[indexId].description===undefined)) {	
                        $scope.Illustration.Payer.OccupationDetails[indexId].industryValue = options[i].value;
                    } else if (type == 'occupationLinear' && !angular.equals(undefined, $scope.Illustration.Payer.OccupationDetails[indexId].industry) && !$scope.SaveTrans) {
                        $scope.Illustration.Payer.OccupationDetails[indexId].descriptionValue = options[i].value;
                    } else if (type == 'jobLinear' && !angular.equals(undefined, $scope.Illustration.Payer.OccupationDetails[indexId].description) && !$scope.SaveTrans) {
                        $scope.Illustration.Payer.OccupationDetails[indexId].jobValue = options[i].value;
                    }else if (type == 'subjobLinear' && !angular.equals(undefined, $scope.Illustration.Payer.OccupationDetails[indexId].job) && !$scope.SaveTrans) {
                        $scope.Illustration.Payer.OccupationDetails[indexId].subJobValue = options[i].value;
                    }else if(type=='occupationClassLinear'){
                        $scope.Illustration.Payer.OccupationDetails[indexId].occupationClassValue = options[i].value;
                    }
                                        
                    return options[i].value;
                }
            }
        }
        $scope.twoPayerOccupation=false;
    }
    
    $scope.validateDuplicatePayer=function(index){         
            for(var i=0;i<$scope.Illustration.Payer.OccupationDetails.length,i<index;i++){ 
                if($scope.Illustration.Payer.OccupationDetails[i]){
                    if($scope.Illustration.Payer.OccupationDetails[index].occupationCode === $scope.Illustration.Payer.OccupationDetails[i].occupationCode){
							
                        $scope.Illustration.Payer.OccupationDetails[index]=[];
                        $scope.leadDetShowPopUpMsg = true;
                        $scope.enterMandatory = translateMessages($translate, "illustrator.duplicateOccupation");
//				        $scope.lePopupCtrl.showWarning(
//								translateMessages($translate,
//								"lifeEngage"),
//								translateMessages($translate,
//								"illustrator.duplicateOccupation"),
//								translateMessages($translate,
//								"fna.ok"));                            
						}
                    }
            } 
            $scope.updateErrorDynamically();
            $scope.refresh();
    }

    $scope.evaluateString = function (value) {
        var val;
        var variableValue = $scope;
        val = value.split('.');
        if (value.indexOf('$scope') != -1) {
            val = val.shift();
        }
        for (var i in val) {
            variableValue = variableValue[val[i]];
        }
        return variableValue;
    };




    $scope.populateIndependentLookupDateInScope = function (type, fieldName, model, setDefault, field, currentTab, successCallBack, errorCallBack, cacheable) {
        if (!cacheable){
            cacheable = false;
        }
        $scope[fieldName] = [{
            "key": "",
            "value": "loading ...."
					               }];
        var options = {};
        options.type = type;
        DataLookupService.getLookUpData(options, function (data) {
            $scope[fieldName] = data;
            $rootScope[fieldName] = data;
            if (setDefault) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isDefault == 1) {
                        if (type == "NATIONALITY") {
                            if ($scope.LmsModel.Lead.BasicDetails.nationality == "") {
                                $scope.setValuetoScope(model, data[i].value);
                                break;
                            } else {
                                $scope.fromReset = false;
                            }
                        } else {
                            $scope.setValuetoScope(model, data[i].value);
                            break;
                        }
                    }
                }
            } else {
                if (model) {
                    var n = "$scope." + model;
                    var m = $scope.evaluateString(model);
                    //var m = eval (n);
                    if (m) {
                        $scope.setValuetoScope(model, m);
                    }
                }
            }
            if (typeof field != "undefined" && typeof currentTab != "undefined") {
                setTimeout(function () {
                    var dataEvaluvated = $scope.currentTab[field];
                    //var dataEvaluvated=eval ('$scope.'+currentTab+'.'+field);
                    if (typeof dataEvaluvated != "undefined") {
                        dataEvaluvated.$render();
                        //eval ('$scope.'+currentTab+'.'+field).$render();

                    }

                }, 0);
            }
            if (typeof $scope.lmsSectionId != "undefined") {
                var data_Evaluvated = $scope.lmsSectionId[fieldName];
                if (data_Evaluvated) {
                    data_Evaluvated.$render();
                    // eval ('$scope.'+$scope.lmsSectionId+'.'+fieldName).$render(); }
                }
            }
            $scope.refresh();
            successCallBack();
        }, function (errorData) {
            $scope[fieldName] = [];

        }, cacheable);
    }
   /* $scope.populateIndustryLookUp = function () {
        $scope.populateIndependentLookupDateInScope('INDUSTRY', 'industry', '', false, 'industry', currentTab, function () {
            $scope.populateOccupationLookUp();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateOccupationLookUp = function () {
        $scope.populateIndependentLookupDateInScope('OCCUPATION', 'occupation', '', false, 'occupation', currentTab, function () {
            $scope.populateOccupationListLookUp();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateOccupationListLookUp = function () {
        $scope.populateIndependentLookupDateInScope('JOB', 'job', '', false, 'job', currentTab, function () {
            $scope.populateOccupationClassLookUp();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateOccupationClassLookUp = function () {
        $scope.populateIndependentLookupDateInScope('SUBJOB', 'subjob', '', false, 'subjob', currentTab, function () {
            $scope.populateRelationshipLookUp();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateRelationshipLookUp = function () {
        $scope.populateIndependentLookupDateInScope('RELATIONSHIP', 'relationship', '', false, 'relationship', currentTab, function () {
            $scope.populateLinearIndustryLookUp();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateLinearIndustryLookUp = function () {
        $scope.populateIndependentLookupDateInScope('INDUSTRY_LINEAR', 'industryLinear', '', false, 'industryLinear', currentTab, function () {
            $scope.populateTitleGenderLookUp();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateTitleGenderLookUp = function () {
        $scope.populateIndependentLookupDateInScope('TITLE_GENDER', 'titleGenderRelationship', '', false, 'titleGenderRelationship', currentTab, function () {
            $scope.populateTitleLookUp();
            $scope.refresh();
        }, function () {});
    }
    
    $scope.populateTitleLookUp = function () {
        $scope.populateIndependentLookupDateInScope('TITLE', 'title', '', false, 'title', currentTab, function () {
            $scope.populateNatureOfWorkLinearLookUp();
            $scope.refresh();
        }, function () {});
    }

    $scope.populateNatureOfWorkLinearLookUp = function () {
        $scope.populateIndependentLookupDateInScope('NATUREOFWORK_LINEAR', 'natureOfWorkLinear', '', false, 'natureOfWorkLinear', currentTab, function () {
            $scope.populateNatureOfWorkOccupationLinearLookUp();
            $scope.refresh();
        }, function () {});
    }
    $scope.populateNatureOfWorkOccupationLinearLookUp = function () {
        $scope.populateIndependentLookupDateInScope('NATUREOFWORKLINEAR_OCODE', 'natureOfWorkOccupationLinear', '', false, 'natureOfWorkOccupationLinear', currentTab, function () {
            $scope.populateNatureOfWorkLookUp();
            $scope.refresh();
        }, function () {});
    }
    $scope.populateNatureOfWorkLookUp = function() {
        $scope.populateIndependentLookupDateInScope('NATUREOFWORK', 'natureOfWork', '', false, 'natureOfWork', currentTab, function() {
            initialLoadContinue();
            $scope.refresh();
        }, function() {});
    } */

    function initialLoadContinue() {
    	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
        }
		if($routeParams.productId == '1010'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6');
			} 
		} else if($routeParams.productId == '1003'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1004'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1006'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1220'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1306'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1274'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6');
			}
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
		}
        ProductService.getProduct(transactionObj, function (product) {
            $scope.Illustration.Product.ProductDetails.productCode = product.id;
            $scope.Illustration.Product.ProductDetails.productId = product.productId;
            $scope.Illustration.Product.ProductDetails.productName = product.name;
            if(product.id=='97'){
                $scope.Illustration.Product.ProductDetails.productName = 'GenCancerSuperProtection';
            }
         //   $scope.Illustration.Product.ProductDetails.productName = $scope.Illustration.Product.ProductDetails.productName.toUpperCase();
            $scope.Illustration.Product.ProductDetails.productType = "";
            $scope.Illustration.Product.ProductDetails.productGroup = product.productGroup;
            IllustratorVariables.productDetails = angular.copy(product);
            $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
            $scope.refresh();

            var personalDetailsJsonName;
            var pdfTemplateDetails = [];
            var eAppPdfTemplateDetails = [];
                        
            for (template in IllustratorVariables.productDetails.templates) {
                if (IllustratorVariables.productDetails.templates[template].templateType == '22001') {
                    personalDetailsJsonName = IllustratorVariables.productDetails.templates[template].templateName + '.json';
                } else if (IllustratorVariables.productDetails.templates[template].templateType == '22002') {
                    IllustratorVariables.illustrationInputTemplate = IllustratorVariables.productDetails.templates[template].templateName + '.json';
                } else if (IllustratorVariables.productDetails.templates[template].templateType == '22003') {
                    IllustratorVariables.illustrationOutputTemplate = IllustratorVariables.productDetails.templates[template].templateName + '.json';
                } else if (IllustratorVariables.productDetails.templates[template].templateType == '22004') {

                    var objPdfTemplate = {};

                    objPdfTemplate.templateId = IllustratorVariables.productDetails.templates[template].templateId;
                    objPdfTemplate.language = IllustratorVariables.productDetails.templates[template].language;
                    objPdfTemplate.templateName = IllustratorVariables.productDetails.templates[template].templateName;
                    eAppPdfTemplateDetails.push(objPdfTemplate);

                } else if (IllustratorVariables.productDetails.templates[template].templateType == '22005') {

                    var objPdfTemplate = {};

                    objPdfTemplate.templateId = IllustratorVariables.productDetails.templates[template].templateId;
                    objPdfTemplate.language = IllustratorVariables.productDetails.templates[template].language;
                    objPdfTemplate.templateName = IllustratorVariables.productDetails.templates[template].templateName;
                    pdfTemplateDetails.push(objPdfTemplate);
                } else if (IllustratorVariables.productDetails.templates[template].templateType == '22006') {
                    $scope.Illustration.Product.templates.eAppInputTemplate = IllustratorVariables.productDetails.templates[template].templateName + '.json';
                }
            }
            $scope.Illustration.Product.templates.illustrationPdfTemplate = pdfTemplateDetails;
            $scope.Illustration.Product.templates.eAppPdfTemplate = eAppPdfTemplateDetails;

            IllustratorVariables.setIllustratorModel($scope.Illustration);
            LEDynamicUI.getUIJson(rootConfig.illustrationConfigJson, false, function (illustrationConfigJson) {
                IllustratorVariables.totalRiderList = illustrationConfigJson.productRidersList;

                //Riders applicable for Multiple Insured are configured in the illustrationConfigJson.ridersForMutlipleInsured
                IllustratorVariables.multipleInsuredRiders = illustrationConfigJson.ridersForMutlipleInsured;
                IllustratorVariables.dynamicPremiumTableForIllustraion = illustrationConfigJson.dynamicPremiumTableForIllustraion;
                //Dynamic Fund Tables are adding from the Illustration Config file
                IllustratorVariables.dynamicTablesForIllustraion = illustrationConfigJson.dynamicTablesForIllustraionOuput;

                IllustratorVariables.nameTitles = illustrationConfigJson.insuredTitle;
                //Dynamic Health Tables
                IllustratorVariables.dynamicHealthTableForIllustraion = illustrationConfigJson.dynamicTablesForHealthIllustraionOuput;

                IllustratorVariables.relationshipWithPolicyHolder = illustrationConfigJson.additionalInsuredTypes;

                //Dynamic Medical Tables
                IllustratorVariables.dynamicMedicalTableForIllustraion = illustrationConfigJson.dynamicTablesForMedicalIllustraionOuput;
                //Adding Illustration Output Constant values from JSON to Illustrator variables
                IllustratorVariables.staticTableDetails = illustrationConfigJson.illustrationOutputStaticTables;
                IllustratorVariables.productCodeCI = illustrationConfigJson.productCodeCI;
                IllustratorVariables.insuredAge = illustrationConfigJson.insuredAge;
                IllustratorVariables.GenSave10PlusSumInsuredADB = illustrationConfigJson.GenSave10PlusSumInsuredADB;
                IllustratorVariables.GenSave4PlusSumInsuredADB = illustrationConfigJson.GenSave4PlusSumInsuredADB;
                $scope.updateAge($scope.Illustration.Insured.BasicDetails.dob, 'insuredDOB');

                if ($scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer != "undefined" || $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer != "") {
                    $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer = "Yes";
                    $scope.Illustration.Payer = angular.copy($scope.Illustration.Insured);
                }                
                
//                /*Map response to UI field*/
//                if(IllustratorVariables.illustrationStatus){
//                for(var i=0;i<$scope.Illustration.Payer.OccupationDetails.length;i++){
//                    var indexObj=$scope.Illustration.Payer.OccupationDetails[i].id;
//                    $scope.Illustration.Payer.OccupationDetails[indexObj]=$scope.Illustration.Payer.OccupationDetails[i];
//                }
//                for(var i=0;i<$scope.Illustration.Insured.OccupationDetails.length;i++){
//                    var indexObj=$scope.Illustration.Insured.OccupationDetails[i].id;
//                    $scope.Illustration.Insured.OccupationDetails[indexObj]=$scope.Illustration.Insured.OccupationDetails[i];
//                }
//                }
                $scope.loadIllustration(function () {
                	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
                    	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
                    }
            		if($routeParams.productId == '1010'){
            			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6']){
            				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6');
            			} 
            		} else if($routeParams.productId == '1003'){
            			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6']){
            				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6');
            			}
            		} else if($routeParams.productId == '1004'){
            			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6']){
            				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6');
            			} 
            		} else if($routeParams.productId == '1006'){
            			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6']){
            				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6');
            			}
            		} else if($routeParams.productId == '1220'){
            			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6']){
            				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6');
            			}
            		} else if($routeParams.productId == '1306'){
            			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6']){
            				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6');
            			}
            		} else if($routeParams.productId == '1274'){
            			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6']){
            				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6');
            			}
            		} 
            		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
            			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
            		} 
            		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
            			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
            		}
                    LEDynamicUI.getUIJson(personalDetailsJsonName, false, function (personalDetailsJSON) {
                        personalDetailsJSONLoop: for (i = 0; i < personalDetailsJSON.products.length; i++) {
                            if (personalDetailsJSON.products[i].id == transactionObj.Products.id) {
                                personalDetailsJSON = personalDetailsJSON.products[i];
                                break personalDetailsJSONLoop;
                            }
                        }
                        LEDynamicUI.getUIJson(rootConfig.illustratorUIJson, false, function (personalDetailsUITemplate) {
                            var customizedPersonalDetailsUITemplate = personalDetailsUITemplate;
                            // Change
                            // view
                            // id
                            // before
                            // customising.
                            personalDetailsUITemplate = $scope.changeViewId(personalDetailsUITemplate);
                            customizedPersonalDetailsUITemplate = $scope.customizeForPersonalDetails(personalDetailsJSON, personalDetailsUITemplate);
                            LEDynamicUI.paintUIFromJsonObject(rootConfig.template, customizedPersonalDetailsUITemplate, $scope.viewToBeCustomized, "#PersonalDetails", true, function () {
                                //$scope.onGenderChangeMainInsured();
                                //$scope.onGenderChangePolicyHolder();
                                /*if (!((rootConfig.isDeviceMobile))) {
                                   $('.custdatepicker').datepicker({
                                        format: 'yyyy-mm-dd',
                                        endDate: '+0d',
                                        autoclose: true
                                    }).on('change', function () {
                                        var elem = $(this).attr("ng-model");
                                        val = $(this).val();
                                        GLI_EvaluateService.bindModel($scope, elem, val);
                                        $scope.updateAge($(this).val(), $(this).attr("id"));
                                        $scope.updateAge($scope.Illustration.Insured.BasicDetails.dob, 'insuredDOB');
                                        $scope.refresh();
                                    });
                                }*/
                                // $rootScope.updateErrorCount($scope.viewToBeCustomized);
                                LEDynamicUI.paintUI(rootConfig.template, rootConfig.illustratorUIJson, "HeaderDetails", "#HeaderDetails", true, function () {
                                    //$scope.loadIllustration();
                                    if ($scope.viewToBeCustomized)
                                        $rootScope.updateErrorCount($scope.viewToBeCustomized);
                                    $rootScope.showHideLoadingImage(false);
                                    $scope.updatePayor();
                                    $timeout(function (){
                                        $scope.onFieldChange();
                                    },100);
                                    $scope.refresh();
                                }, $scope, $compile);
                            }, $scope, $compile);
                        });
                    });
                });
                // Need To Check 
            });
        }, function (data) {
            alert("Error in get product Service " + data);
        });
    }
    //extended loadillustration
    $scope.initialLoad = function () { 
    	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
        }
		if($routeParams.productId == '1010'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6');
			} 
		} else if($routeParams.productId == '1003'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6');
			}
		}
        else if($routeParams.productId == '1097'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_97#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_97#PersonalDetails_6');
			}
		}else if($routeParams.productId == '1004'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6');
			} 
		} else if($routeParams.productId == '1006'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1220'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1306'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1274'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6');
			}
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
		}  
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6'])
        {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6'])
        {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6'])
        {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6'])
        {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6'])
        {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
        }    
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4'])
        {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6'])
        {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6'])
        {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5'])
        {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5'])
        {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5'])
        {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5'])
        {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5');
        }
        $scope.Illustration.Insured.OccupationDetails[0]="";
        $scope.Illustration.Insured.OccupationDetails[1]="";
        $scope.Illustration.Insured.OccupationDetails[2]="";
        $scope.Illustration.Insured.OccupationDetails[3]="";

        $scope.Illustration.Payer.OccupationDetails[0]="";
        $scope.Illustration.Payer.OccupationDetails[1]="";
        $scope.Illustration.Payer.OccupationDetails[2]="";
        $scope.Illustration.Payer.OccupationDetails[3]="";
      
        $scope.updateErrorDynamically();
      /*  $scope.populateIndependentLookupDateInScope('OCODE', 'occupationCode', '', false, 'occupationCode', currentTab, function () {
            $scope.populateIndustryLookUp();            
        }, function () {

        }); */  
         var typeNameList = [
        { "type": "OCODE", "isCacheable": true },
        { "type": "INDUSTRY", "isCacheable": true },
        { "type": "OCCUPATION", "isCacheable": true },
        { "type": "JOB", "isCacheable": true },
        { "type": "SUBJOB", "isCacheable": true },
        { "type": "RELATIONSHIP", "isCacheable": true },
        { "type": "INDUSTRY_LINEAR", "isCacheable": true },
        { "type": "TITLE", "isCacheable": true },     
        { "type": "TITLE_GENDER", "isCacheable": true },
        { "type": "NATUREOFWORK_LINEAR", "isCacheable": true },
      //  { "type": "NATUREOFWORKLINEAR_OCODE", "isCacheable": true },
        { "type": "NATUREOFWORK", "isCacheable": true }];
    
        GLI_DataLookupService.getMultipleLookUpData(typeNameList, function (lookUpList) {
            initialLoadContinue();
        }, function () {
            console.log("Error in Multilookup data call");  
        });
    }
    $scope.loadIllustration = function (successcallback) {
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
        }
		if($routeParams.productId == '1010'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6');
			} 
		} else if($routeParams.productId == '1003'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1004'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6');
			} 
		} else if($routeParams.productId == '1006'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1220'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1306'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1274'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6');
			}
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
		}
        
        if (IllustratorVariables.transactionId != "" && IllustratorVariables.transactionId != 0 && $rootScope.transactionId == 0) {
            $rootScope.transactionId = IllustratorVariables.transactionId;
        }
        if ($scope.proposalId != 0 && typeof $scope.proposalId !== "undefined") {
            PersistenceMapping.clearTransactionKeys();
            IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
            var transactionObj = PersistenceMapping.mapScopeToPersistence({});
            DataService.getListingDetail(transactionObj, function (data) {
                $scope.onGetListingsSuccess(data, successcallback);
            }, $scope.onGetListingsError);
        } else if (IllustratorVariables.illustratorId != 0 && typeof IllustratorVariables.illustratorId !== "undefined" && UtilityService.previousPage != 'FNA') {
            PersistenceMapping.clearTransactionKeys();
            IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
            var transactionObj = PersistenceMapping.mapScopeToPersistence({});
            DataService.getListingDetail(transactionObj, function (data) {
                $scope.onGetListingsSuccess(data, successcallback);
            }, $scope.onGetListingsError);
        } else if ($rootScope.transactionId != 0 && typeof $rootScope.transactionId !== "undefined" && UtilityService.previousPage != 'FNA') {
            PersistenceMapping.clearTransactionKeys();
            IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
            var transactionObj = PersistenceMapping.mapScopeToPersistence({});
            DataService.getListingDetail(transactionObj, function (data) {
                $scope.onGetListingsSuccess(data, successcallback);
            }, $scope.onGetListingsError);
        } else {
            UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
            if ((IllustratorVariables.fnaId == 0 || IllustratorVariables.fnaId == '') && (IllustratorVariables.illustratorId == 0 || IllustratorVariables.fnaId == '') && $rootScope.transactionId == 0) {
                IllustratorVariables.illustrationStatus = "";
                IllustratorVariables.isValidationSuccess = false;
            }
            var proposalData = GLI_IllustratorService.getSelectedProposalData();
            if (proposalData) {
                IllustratorVariables.setIllustratorModel(proposalData);
                $scope.Illustration = IllustratorVariables.getIllustratorModel();
                GLI_IllustratorService.setSelectedProposalData();
            }
            if (UtilityService.previousPage == 'FNA' || UtilityService.previousPage == 'LMS' || UtilityService.previousPage == 'IllustrationPersonal') {
                IllustratorVariables.illustrationStatus = "";
				IllustratorVariables.fromChooseAnotherProduct = false;
                //$scope.Illustration.Insured = angular.copy(globalService.getInsured());
                if($scope.navigatedFromFNA){
                    $scope.Illustration.Insured = angular.copy(globalService.getInsured());
                }
                else{
		            /*FNA changes by LE Team >>> starts - Bug 4194*/
		            if (FnaVariables.flagFromCustomerProfileFNA && FnaVariables.initialLoadSIFromFNA){
                        if(FnaVariables.insuredFromFNA){                                      
                            $scope.Illustration.Insured.BasicDetails = FnaVariables.insuredFromFNA.BasicDetails;                        
                            $scope.Illustration.Insured.ContactDetails = FnaVariables.insuredFromFNA.ContactDetails;
                            $scope.Illustration.Insured.OccupationDetails = FnaVariables.insuredFromFNA.OccupationDetails;
                            $scope.Illustration.Insured.CustomerRelationship = {
                                "isPayorDifferentFromInsured": "No",
                                "relationShipWithPO": ""
                            }
                            $scope.Illustration.Insured.Declaration = {
                                "date" : "",
                                "place" : ""
                            }
                            $scope.Illustration.Insured.IdentityDetails = {
                                "idIssueDate" : "",
                                "idIssuePlace" : "",
                                "idNo" : ""
                            }
                            $scope.Illustration.Insured.IncomeDetails = {}
                            $scope.Illustration.Insured.SeverityDetails = {
                                "severity" : ""
                            }
                            $scope.Illustration.Insured.SourceDetails ={
                                "source" : ""
                            }
                            $scope.Illustration.Insured.id = "myself";
                            $scope.Illustration.Insured.isSaved = true;
                            $scope.Illustration.Insured.type = "FNAMyself";
                        }                 
                    } else {
                        $scope.Illustration.Insured = angular.copy(globalService.getInsuredIllustration());
                    }
	                /*FNA changes by LE Team >>> ends - Bug 4194*/
	            }
                $scope.Illustration.Payer = angular.copy(globalService.getPayer());
                if($scope.Illustration.Payer.CustomerRelationship){
                	if($scope.Illustration.Payer.CustomerRelationship.relationWithInsured){                		
                		$scope.Illustration.Payer.CustomerRelationship.relationWithInsured='';
                	}
                }
                /* Defect -3760 - starts*/
                //if ($scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer != "undefined" || $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer != "") {
                if($scope.Illustration.Insured.isPayer ==false && $scope.Illustration.Payer.isPayer !=undefined){    
                    //$scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer = "Yes";
                    //$scope.Illustration.Payer = angular.copy($scope.Illustration.Insured);
                    $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer="No";
                    if(!$scope.navigatedFromFNA) {
                        //$scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer="Yes";
                        $scope.onChangeSameAsPayer("No");
                    }
                    
                }
                else if ($scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer != "undefined" || $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer != "") {
                     $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer = "Yes";
                     $scope.Illustration.Payer = angular.copy($scope.Illustration.Insured);
                 }
                /* Defect -3760 - ends*/
                 if(!$scope.navigatedFromFNA) {

                    if (($scope.Illustration.Insured.BasicDetails.age !==undefined && $scope.Illustration.Insured.BasicDetails.age !== "") && $scope.Illustration.Insured.BasicDetails.dob) {
                        if($scope.Illustration.Insured.BasicDetails.age<16){
                            $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer = "No";                    
                            $scope.onChangeSameAsPayer("No");
                            $scope.updateErrorDynamically();
                        }
                    }
                }

            
                $scope.updateErrorDynamically();
                                
                /*Map response to UI field*/
//                if(IllustratorVariables.illustrationStatus){
//                for(var i=0;i<$scope.Illustration.Payer.OccupationDetails.length;i++){
//                    var indexObj=$scope.Illustration.Payer.OccupationDetails[i].id;
//                    $scope.Illustration.Payer.OccupationDetails[indexObj]=$scope.Illustration.Payer.OccupationDetails[i];
//                }
//                for(var i=0;i<$scope.Illustration.Insured.OccupationDetails.length;i++){
//                    var indexObj=$scope.Illustration.Insured.OccupationDetails[i].id;
//                    $scope.Illustration.Insured.OccupationDetails[indexObj]=$scope.Illustration.Insured.OccupationDetails[i];
//                }
//                }
                
                if($scope.Illustration.Payer.BasicDetails.title==='' || $scope.Illustration.Payer.BasicDetails.title===undefined){
                        $scope.Illustration.Payer.BasicDetails.title="";
                }
                
                if($scope.Illustration.Insured.BasicDetails.title==='' || $scope.Illustration.Insured.BasicDetails.title===undefined){
                        $scope.Illustration.Insured.BasicDetails.title="";
                }               

//                if ($scope.Illustration.Payer && $scope.Illustration.Payer.BasicDetails.firstName != "") {
//                    if ($scope.Illustration.Payer.isInsured == true) {
//                        $scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured = 'Yes';
//                    } else if ($scope.Illustration.Insured.isInsured == true) {
//                        $scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured = 'No';
//                    } else {
//                        $scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured = 'No';
//                    }
//                }
                //Need to change Hardcode value
                var date = new Date();
                $scope.FromDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                if ($scope.Illustration.Payer.BasicDetails.currentDate == "") {
                    $scope.Illustration.Payer.BasicDetails.currentDate = $scope.FromDate;
                } else {
                    $scope.Illustration.Payer.BasicDetails.currentDate = $scope.Illustration.Payer.BasicDetails.currentDate;
                }
                if ($scope.Illustration.Insured.BasicDetails.age >= 1) {
                    $scope.insuredAge = $scope.Illustration.Insured.BasicDetails.age;
                } else if ($scope.Illustration.Insured.BasicDetails.age < 1 && $scope.Illustration.Insured.BasicDetails.age != "") {
                    $scope.insuredAge = 0;
                } else {
                    $scope.insuredAge = "";
                }
                if ($scope.Illustration.Payer.BasicDetails.age >= 1) {
                    $scope.payerAge = $scope.Illustration.Payer.BasicDetails.age;
                } else if ($scope.Illustration.Payer.BasicDetails.age < 1 && $scope.Illustration.Payer.BasicDetails.age != "") {
                    $scope.payerAge = 0;
                } else {
                    $scope.payerAge = "";
                }
                $scope.Illustration.Beneficiaries = globalService.getSelectdBeneficiaries();
                $scope.updateErrorDynamically();
            }
            /*FNA changes by LE Team >>> starts*/
            if ($scope.navigatedFromFNA != true) {/*FNA changes by LE Team >>> - Bug 4194*/ 
                if(FnaVariables.flagFromCustomerProfileFNA != true){

                    $scope.Illustration.Insured.OccupationDetails[0]="";             
                    $scope.Illustration.Insured.OccupationDetails[1]="";
                    $scope.Illustration.Insured.OccupationDetails[2]="";
                    $scope.Illustration.Insured.OccupationDetails[3]="";
            
                    $scope.Illustration.Payer.OccupationDetails[0]="";
                    $scope.Illustration.Payer.OccupationDetails[1]="";
                    $scope.Illustration.Payer.OccupationDetails[2]="";
                    $scope.Illustration.Payer.OccupationDetails[3]="";
                }
            }
			/*FNA changes by LE Team >>> ends*/
            
            UtilityService.previousPage = 'IllustrationPersonal';
            successcallback();
        }
    }
    /*
     * Check whether Date of Birth is valid and calculate age from Date of Birth. 
     * Populating main insured type based on age
     */
    var error = {};
    $scope.updateAge = function (dob, id) {
        if (id.indexOf('_') > -1) {
            var dobIndex = id.substring(id.indexOf('_') + 1, id.length);
            id = id.substring(0, id.indexOf('_'));
        }
        if (dob != "" && dob != undefined && dob != null) {
            var dobDate = new Date(dob);
            /*
             * In I.E it will accept date format in '/'
             * separator only
             */
            if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
                dobDate = new Date(formatForDateControl(dob).split("-").join("/"));
            }

            var todayDate = new Date();
            if (dobDate > todayDate) {
                if (id == "insuredDOB") { // insured dob greater than current date
                    var error = {};
                    $scope.insuredAge = 0;
                    $scope.Illustration.Insured.BasicDetails.age = 0;
                    error.message = translateMessages($translate, "illustrator.illustratorInsuredDOBValidationMessage");
                    error.key = 'insuredDOB';
                    $scope.dynamicErrorMessages.push(error);

                } else if (id == "policyHolderDOB") { // policyholder dob greater than current date
                    var error = {};
                    $scope.payerAge = 0;
                    $scope.Illustration.Payer.BasicDetails.age = 0;
                    error.message = translateMessages($translate, "illustrator.illustratorPolicyHolderDOBValidationMessage");
                    error.key = 'policyHolderDOB';
                    $scope.dynamicErrorMessages.push(error);

                }

                $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                for (index in $scope.dynamicErrorMessages) {
                    for (var newindex = Number(index) + 1; newindex < $scope.dynamicErrorMessages.length; newindex++) {
                        if ($scope.dynamicErrorMessages[index].key == $scope.dynamicErrorMessages[newindex].key) {
                            $scope.updateDynmaicErrMessageByIndex(newindex);
                            newindex = Number(index);

                        }
                    }
                }
                if ($scope.viewToBeCustomized != undefined) {
                    $rootScope.updateErrorCount($scope.viewToBeCustomized, true);
                }
            } else {
                var curd = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate());
                var cald = new Date(dobDate.getFullYear(), dobDate.getMonth(), dobDate.getDate());
                var dife = $scope.ageCalculation(curd, cald);
                var age = "";
                if (age < 1) {
                    var datediff = curd.getTime() - cald.getTime();
                    var days = (datediff / (24 * 60 * 60 * 1000)) / 365;
                    days = Number(Math.round(days + 'e3') + 'e-3');
                    age = 0;
                }
                if (dife[1] == "false") {
                    age = dife[0];
                } else {
                    age = dife[0] + 1;
                }

                if (dob == undefined || dob == "") {
                    age = "-";
                }
                if (id == "insuredDOB") {

                    $scope.updateDynmaicErrMessage('insuredDOB');
                    if (age >= 1) {
                        $scope.Illustration.Insured.BasicDetails.age = age;
                        $scope.insuredAge = age;
                    } else {
                        $scope.Illustration.Insured.BasicDetails.age = age;
                        $scope.insuredAge = age;
                    }


                    var mainInsuredTypeArr = IllustratorVariables.getMainInsuredType();
                    var insuredTypesForProduct;
                    var keys = [];
                    for (var key in mainInsuredTypeArr[0]) {
                        if (key == $scope.Illustration.Product.ProductDetails.productCode) {
                            insuredTypesForProduct = mainInsuredTypeArr[0][key];
                        }
                    }
                    if (insuredTypesForProduct) {
                        for (var i = 0; i < insuredTypesForProduct.length; i++) {
                            if (age >= insuredTypesForProduct[i].minAge && (insuredTypesForProduct[i].maxAge == "" || age <= insuredTypesForProduct[i].maxAge)) {
                                if (insuredTypesForProduct[i].value != '') {
                                    $scope.Illustration.Insured.BasicDetails.mainInsuredType = insuredTypesForProduct[i].value;
                                    $scope.mainInsuredTypes = [{
                                        'value': insuredTypesForProduct[i].value,
                                        'code': insuredTypesForProduct[i].value
                                    }];
                                    $scope.disableMainInsuredType = true;
                                } else {
                                    $scope.disableMainInsuredType = false;
                                    if ($scope.Illustration.Insured.BasicDetails.mainInsuredType === undefined || $scope.Illustration.Insured.BasicDetails.mainInsuredType === "") {
                                        $scope.Illustration.Insured.BasicDetails.mainInsuredType = 'Child';
                                    }
                                }
                            }
                        }
                    }

                }
                if (id == "policyHolderDOB") {

                    $scope.updateDynmaicErrMessage('policyHolderDOB');
                    if (age >= 1) {
                        $scope.Illustration.Payer.BasicDetails.age = age;
                        $scope.payerAge = age;
                    } else {
                        $scope.Illustration.Payer.BasicDetails.age = age;
                        $scope.payerAge = age;
                    }

                }
            }
        } 
//        else if ((dob == "" || dob == undefined || dob == null) && id == "insuredDOB" && $scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == "No") {
//            $scope.Illustration.Insured.BasicDetails.age = "";
//            $scope.Illustration.Insured.BasicDetails.mainInsuredType = "";
//            $scope.disableMainInsuredType = false;
//            $scope.updateDynmaicErrMessage('insuredDOB');
//
//        } else if ((dob == "" || dob == undefined || dob == null) && id == "policyHolderDOB" && $scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == "Yes") {
//            $scope.Illustration.Payer.BasicDetails.age = '';
//            $scope.Illustration.Insured.BasicDetails.mainInsuredType = "";
//            $scope.disableMainInsuredType = false;
//            $scope.updateDynmaicErrMessage('insuredDOB');
//            $scope.updateDynmaicErrMessage('policyHolderDOB');
//
//        }
    }

    $scope.updateDynmaicErrMessage = function (erroKey) {
        for (var i = 0; i < $scope.dynamicErrorMessages.length; i++) {
            if ($scope.dynamicErrorMessages[i].key == erroKey) {
                $scope.dynamicErrorMessages.splice(i, 1);
                $scope.dynamicErrorCount = $scope.dynamicErrorCount - 1;
            }
        }
        if ($scope.viewToBeCustomized)
            $rootScope.updateErrorCount($scope.viewToBeCustomized, true);
    }
    $scope.updateDynmaicErrMessageByIndex = function (index) {
        $scope.dynamicErrorMessages.splice(index, 1);
        $scope.dynamicErrorCount = $scope.dynamicErrorCount - 1;
        if ($scope.viewToBeCustomized) {
            $rootScope.updateErrorCount($scope.viewToBeCustomized, true);
        }
    }
    //Calculating age in years, month , days 
    $scope.ageCalculation = function (currDate, dobDate) {
        date1 = new Date(currDate);
        date2 = new Date(dobDate);
        var y1 = date1.getFullYear(),
            m1 = date1
            .getMonth(),
            d1 = date1.getDate(),
            y2 = date2
            .getFullYear(),
            m2 = date2.getMonth(),
            d2 = date2
            .getDate();

        var nextDOB;
        if (m1 < m2) {
            nextDOB = new Date(date2.setYear(date1.getFullYear()));
        } else if ((m1 == m2) && (d1 < d2)) {
            nextDOB = new Date(date2.setYear(date1.getFullYear()));
        } else {
            nextDOB = new Date(date2.setYear(date1.getFullYear() + 1));
        }
        var marginDate = new Date(date1.setMonth(date1.getMonth() + 6));

        if (m1 < m2) {
            y1--;
            m1 += 12;
        }

        if (nextDOB.getTime() > marginDate.getTime()) {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() - birthDate.getFullYear();
            var mnth = today.getMonth() - birthDate.getMonth();
            if (mnth < 0 || (mnth === 0 && today.getDate() < birthDate.getDate())) {
                ageVal--;
            }
            return [ageVal, "false"];
        } else {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() - birthDate.getFullYear();
            var mnth = today.getMonth() - birthDate.getMonth();
            if (mnth < 0 || (mnth === 0 && today.getDate() < birthDate.getDate())) {
                ageVal--;
            }
            return [ageVal, "true"];
        }

    }

    //To Enable/Disable tabs on editing a transaction after the status is changed to Completed
    $scope.onFieldChange = function () {
        $rootScope.updateErrorCount($scope.viewToBeCustomized);
        GLI_IllustratorService.onFieldChange($scope, IllustratorVariables, $rootScope);
        $scope.updateErrorDynamically();
        $scope.refresh();
    }

    $scope.ageCalculation = function (currDate, dobDate) {
        date1 = new Date(currDate);
        date2 = new Date(dobDate);
        var y1 = date1.getFullYear(),
            m1 = date1
            .getMonth(),
            d1 = date1.getDate(),
            y2 = date2
            .getFullYear(),
            m2 = date2.getMonth(),
            d2 = date2
            .getDate();

        var nextDOB;
        if (m1 < m2) {
            nextDOB = new Date(date2.setYear(date1
                .getFullYear()));
        } else if ((m1 == m2) && (d1 < d2)) {
            nextDOB = new Date(date2.setYear(date1
                .getFullYear()));
        } else {
            nextDOB = new Date(date2.setYear(date1
                .getFullYear() + 1));
        }
        var marginDate = new Date(date1.setMonth(date1
            .getMonth() + 6));

        if (m1 < m2) {
            y1--;
            m1 += 12;
        }

        if (nextDOB.getTime() > marginDate.getTime()) {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() -
                birthDate.getFullYear();
            var mnth = today.getMonth() -
                birthDate.getMonth();
            if (mnth < 0 ||
                (mnth === 0 && today.getDate() < birthDate
                    .getDate())) {
                ageVal--;
            }
            return [ageVal, "false"];
        } else {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() -
                birthDate.getFullYear();
            var mnth = today.getMonth() -
                birthDate.getMonth();
            if (mnth < 0 ||
                (mnth === 0 && today.getDate() < birthDate
                    .getDate())) {
                ageVal--;
            }
            return [ageVal, "true"];
        }

    }
    $scope.calculateAge = function (dob, id) {
        var age = "";
        if (dob != "" && dob != undefined &&
            dob != null) {
            var dobDate = new Date(dob);
            /*
             * In I.E it will accept date format in '/'
             * separator only
             */
            if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
                dobDate = new Date(dob.split("-").join(
                    "/"));
            }
            var todayDate = new Date();
            if (formatForDateControl(dob) >= formatForDateControl(new Date())) {
                //$scope.Illustration.Insured.BasicDetails.age = 0;
                age = 0;
            } else {
                var curd = new Date(todayDate
                    .getFullYear(), todayDate
                    .getMonth(), todayDate
                    .getDate());
                var cald = new Date(dobDate
                    .getFullYear(), dobDate
                    .getMonth(), dobDate.getDate());
                var dife = $scope.ageCalculation(curd,
                    cald);

                if (age < 1) {
                    var datediff = curd.getTime() -
                        cald.getTime();
                    var days = (datediff / (24 * 60 * 60 * 1000)) / 365;
                    days = Number(Math.round(days +
                            'e3') +
                        'e-3');
                    age = 0;
                }
                if (dife[1] == "false") {
                    age = dife[0];
                } else {
                    age = dife[0] + 1;
                }

                if (dob == undefined || dob == "") {
                    age = "-";
                }
                //$scope.Illustration.Insured.BasicDetails.age = age;
            }
        }
        //else {
        //$scope.Illustration.Insured.BasicDetails.age = "";
        // }
        if (id == 'insuredDOB') {
            $scope.Illustration.Insured.BasicDetails.age = age;
        } else if (id == 'payerDOB') {
            $scope.Illustration.Payer.BasicDetails.age = age;
        }
         $scope.onFieldChange();
//        $scope.validateInsuredAge();
    };

    $scope.updateDate = function () {
        $scope.calculateAge($scope.Illustration.Insured.BasicDetails.dob, 'insuredDOB');
        $scope.refresh();
    }
    $scope.onGetListingsSuccess = function (data, successcallback) {
        //For showing premium summary output table in product details screen
        if (data[0].TransactionData.Product.premiumSummary && data[0].TransactionData.Product.premiumSummary.premiumSummaryOutput &&
            Object.keys(data[0].TransactionData.Product.premiumSummary.premiumSummaryOutput).length > 0) {
            IllustratorVariables.isValidationSuccess = true;
            $rootScope.premiumSummaryOutput = true;
        } else {
            IllustratorVariables.isValidationSuccess = false;
            $rootScope.premiumSummaryOutput = false;
        }
		IllustratorVariables.agentForGAO = data[0].Key11;
		IllustratorVariables.GAOId = data[0].Key36;
		IllustratorVariables.agentNameForGAO = data[0].Key38;
		IllustratorVariables.agentType= data[0].Key37;
		IllustratorVariables.GAOOfficeCode=data[0].Key35;
        //Insured Occupation swap issue
        if(data[0].TransactionData.Insured.OccupationDetails!==undefined){
            var tempInsuredObject=angular.copy(data[0].TransactionData.Insured.OccupationDetails);            
            if(tempInsuredObject.length>1){
                data[0].TransactionData.Insured.OccupationDetails=[];
                $scope.twoInsuredOccupation=true;
                for(var i=0;i<tempInsuredObject.length;i++){
                    if(parseInt(tempInsuredObject[i].id)===0 || parseInt(tempInsuredObject[i].id)===2){
                        data[0].TransactionData.Insured.OccupationDetails[0]=tempInsuredObject[i];
                    }
                    
                    if(parseInt(tempInsuredObject[i].id)===1 || parseInt(tempInsuredObject[i].id)===3){
                        data[0].TransactionData.Insured.OccupationDetails[1]=tempInsuredObject[i];
                    }
                }
            }
        }
        //Payer Occupation swap issue
        if(data[0].TransactionData.Payer.OccupationDetails!==undefined){
            var tempPayerObject=angular.copy(data[0].TransactionData.Payer.OccupationDetails);
            
            if(tempPayerObject.length>1){
                data[0].TransactionData.Payer.OccupationDetails=[];
                $scope.twoPayerOccupation=true;
                for(var i=0;i<tempPayerObject.length;i++){
                    if(parseInt(tempPayerObject[i].id)===0 || parseInt(tempPayerObject[i].id)===2){
                        data[0].TransactionData.Payer.OccupationDetails[0]=tempPayerObject[i];
                    }
                    
                    if(parseInt(tempPayerObject[i].id)===1 || parseInt(tempPayerObject[i].id)===3){
                        data[0].TransactionData.Payer.OccupationDetails[1]=tempPayerObject[i];
                    }
                }
            }
        }
		
		if(IllustratorVariables.fromChooseAnotherProduct == "false" || IllustratorVariables.fromChooseAnotherProduct == "" ){
			IllustratorVariables.fromChooseAnotherProduct = false;
		}

        if (data[0].TransactionData.Product.ProductDetails.productCode != $scope.Illustration.Product.ProductDetails.productCode || IllustratorVariables.fromChooseAnotherProduct) {
            data[0].Key3 = ""; //Clear Illustration ID
			data[0].Key15 = "Draft"; //Change Status
			data[0].Id = ""; //Clear Illustration ID for offline
            data[0].TransTrackingID=""; //Clear TransTrackingID
            data[0].TransactionData.IllustrationOutput = {};
            data[0].TransactionData.Product.RiderDetails = [];
            data[0].TransactionData.Product.premiumSummary.riderPremium = 0;
            data[0].TransactionData.Product.premiumSummary.basicPremium = 0;
            data[0].TransactionData.Product.premiumSummary.sumInsured  = 0;
            data[0].TransactionData.Product.premiumSummary.totalPremium = 0;
            data[0].TransactionData.Product.policyDetails.sumInsured = 0;
            data[0].TransactionData.Product.policyDetails.premium = 0;
            data[0].TransactionData.Product.policyDetails.premiumPeriod = ""; //Clear Premium Period
            data[0].TransactionData.Product.policyDetails.packages = ""; //Clear Packages
			data[0].TransactionData.Product.policyDetails.premiumFrequency = "Annual"; //Default the premium mode
			data[0].TransactionData.Product.policyDetails.tax = ""; //Clear Tax
			data[0].TransactionData.Requirements = []; //clear Signature data
            
            $scope.Illustration.Product.premiumSummary.RiderDetails = [];
            $scope.Illustration.Product.premiumSummary.riderPremium = 0;
            $scope.Illustration.Product.premiumSummary.basicPremium = 0;
            $scope.Illustration.Product.premiumSummary.sumInsured  = 0;
            $scope.Illustration.Product.premiumSummary.totalPremium = 0;
            $scope.Illustration.Product.policyDetails.sumInsured = 0;
            $scope.Illustration.Product.policyDetails.premium = 0;
            $scope.Illustration.Product.policyDetails.premiumPeriod = ""; //Clear Premium Period
            $scope.Illustration.Product.policyDetails.packages = ""; //Clear Packages
			$scope.Illustration.Product.policyDetails.premiumFrequency = "Annual"; //Default the premium mode
			$scope.Illustration.Product.policyDetails.tax = ""; //Clear Tax
			$scope.Illustration.Requirements = [];	//clear Signature data
			$rootScope.transactionId = ""; //Clear Illustration ID for offline
			
			IllustratorVariables.fromChooseAnotherProduct = false;
            
        }
		
		if (data[0].Key15 == "Completed" || data[0].Key15 == "Confirmed") {
            IllustratorVariables.isValidationSuccess = true;
        } else {
            IllustratorVariables.isValidationSuccess = false;
        }
        
        data[0].TransactionData.Product.ProductDetails.productCode = $scope.Illustration.Product.ProductDetails.productCode;
        data[0].TransactionData.Product.ProductDetails.productId = $scope.Illustration.Product.ProductDetails.productId;
        data[0].TransactionData.Product.ProductDetails.productName = $scope.Illustration.Product.ProductDetails.productName;
        
        if (data[0].TransactionData.Product.templates) {
            data[0].TransactionData.Product.templates.illustrationPdfTemplate = $scope.Illustration.Product.templates.illustrationPdfTemplate;
        }

        //To get the additional insured previous relationship and its used to clear all the riders associated with that person
        //if its changed
        if (data[0].TransactionData.Insured && data[0].TransactionData.Insured.CustomerRelationship && data[0].TransactionData.Insured.CustomerRelationship.relationShipWithPO && data[0].TransactionData.Insured.CustomerRelationship.relationShipWithPO == "Child") {
            data[0].TransactionData.Insured.CustomerRelationship.relationShipWithPO = "Child/Guarded Child";
        }
        IllustratorVariables.setIllustratorModel(data[0].TransactionData);
        IllustratorVariables.illustratorId = data[0].Key3;
        IllustratorVariables.fnaId = data[0].Key2;
        //IllustratorVariables.fromChooseAnotherProduct = data[0].Key7;
        IllustratorVariables.illustratorKeys.Key12 = data[0].Key12;
        IllustratorVariables.leadId = data[0].Key1;
        IllustratorVariables.leadName = data[0].Key22;
        IllustratorVariables.leadEmailId = data[0].Key23;
        IllustratorVariables.illustrationNumberRDS = data[0].Key24;
        if ((IllustratorVariables.fnaId == 0 || IllustratorVariables.fnaId == '') && (IllustratorVariables.illustratorId == 0 || IllustratorVariables.fnaId == '')) {
            $scope.Illustration.Payer = globalService.getParty('Lead');
        }

        //mapping key15 in edit flow
        IllustratorVariables.illustrationStatus = data[0].Key15;
        $scope.Illustration = IllustratorVariables.getIllustratorModel();

        if($scope.Illustration.PrivacyLaw && $scope.Illustration.PrivacyLaw.userConsentFlag){
            if($scope.Illustration.PrivacyLaw.userConsentFlag == "Yes"){
                $scope.showPrivacyLaw = true;
            }
            else {
                $scope.showPrivacyLaw = false;   
            }
        }    
        else {
            $scope.showPrivacyLaw = true;
        }  

//        /*Map response to UI field*/
//                if(IllustratorVariables.illustrationStatus){
//                for(var i=0;i<$scope.Illustration.Payer.OccupationDetails.length;i++){
//                    var indexObj=$scope.Illustration.Payer.OccupationDetails[i].id;
//                    $scope.Illustration.Payer.OccupationDetails[indexObj]=$scope.Illustration.Payer.OccupationDetails[i];
//                }
//                for(var i=0;i<$scope.Illustration.Insured.OccupationDetails.length;i++){
//                    var indexObj=$scope.Illustration.Insured.OccupationDetails[i].id;
//                    $scope.Illustration.Insured.OccupationDetails[indexObj]=$scope.Illustration.Insured.OccupationDetails[i];
//                }
//                }
        if ($scope.Illustration && $scope.Illustration.Insured && $scope.Illustration.Insured.CustomerRelationship && $scope.Illustration.Insured.CustomerRelationship.relationShipWithPO && $scope.Illustration.Insured.CustomerRelationship.relationShipWithPO == "Child") {
            $scope.Illustration.Insured.CustomerRelationship.relationShipWithPO = "Child/Guarded Child";
        }
        if ($scope.Illustration.Insured.BasicDetails.age >= 1) {
            $scope.insuredAge = $scope.Illustration.Insured.BasicDetails.age;
        } else if ($scope.Illustration.Insured.BasicDetails.age < 1 && $scope.Illustration.Insured.BasicDetails.age != "") {
            $scope.insuredAge = 0;
        } else {
            $scope.insuredAge = "";
        }
        if ($scope.Illustration.Payer.BasicDetails.age >= 1) {
            $scope.payerAge = $scope.Illustration.Payer.BasicDetails.age;
        } else if ($scope.Illustration.Payer.BasicDetails.age < 1 && $scope.Illustration.Payer.BasicDetails.age != "") {
            $scope.payerAge = 0;
        } else {
            $scope.payerAge = "";
        }
        PersistenceMapping.mapPersistenceToScope(data);
        $scope.mapKeysfromPersistence();
        stateOrCity = true;
        //Variable to disable the controls if the status is Confirmed or Completed with an eApp
        if (IllustratorVariables.illustrationStatus == "Confirmed" || (IllustratorVariables.illustrationStatus == "Completed" && $rootScope.haseApp == true)) {
            $rootScope.disableControl = true;
        } else {
            $rootScope.disableControl = false;
        }
        //$rootScope.showHideLoadingImage(false);
        //$scope.refresh();
        successcallback();

    }
    
    var OccupationInsured=[];
    var OccupationPayer=[];
    var OccupationInsuredDisplay=[];
    var OccupationPayerDisplay=[];
    $scope.getInsuredOccupation=function(){    
        OccupationInsured=[];
        if(!angular.equals(undefined,OccupationInsured))
                        {
                            OccupationInsured.slice();
                        }
        
        for(var i=0;i<4;i++){
        if(!angular.equals(undefined,$scope.Illustration.Insured.OccupationDetails[i])){
            if(!angular.equals(undefined,$scope.Illustration.Insured.OccupationDetails[i].occupationCode) && $scope.Illustration.Insured.OccupationDetails[i].occupationCode!==""){
            var OccupationDetailsArr = {};
            OccupationDetailsArr={
                            "id":i,
							"occupationCode":$scope.Illustration.Insured.OccupationDetails[i].occupationCode,
                            "description":$scope.Illustration.Insured.OccupationDetails[i].description,
							"job":($scope.Illustration.Insured.OccupationDetails[i].job.indexOf('NA')===-1)?$scope.Illustration.Insured.OccupationDetails[i].job:"NA",
							"subJob":($scope.Illustration.Insured.OccupationDetails[i].subJob.indexOf('NA')===-1)?$scope.Illustration.Insured.OccupationDetails[i].subJob:"NA",
							"occupationClass":$scope.Illustration.Insured.OccupationDetails[i].occupationClass,
							"industry":$scope.Illustration.Insured.OccupationDetails[i].industry,
							"occupationCodeValue":$scope.Illustration.Insured.OccupationDetails[i].occupationCodeValue,
                            "descriptionValue":$scope.Illustration.Insured.OccupationDetails[i].descriptionValue,
							"jobValue":$scope.Illustration.Insured.OccupationDetails[i].jobValue,
							"subJobValue":$scope.Illustration.Insured.OccupationDetails[i].subJobValue,
							"occupationClassValue":$scope.Illustration.Insured.OccupationDetails[i].occupationClassValue,
							"industryValue":$scope.Illustration.Insured.OccupationDetails[i].industryValue,
                            "natureofWork":$scope.Illustration.Insured.OccupationDetails[i].natureofWorkValue,
                            "natureofWorkValue":$scope.Illustration.Insured.OccupationDetails[i].natureofWorkValue
					};
                if(OccupationDetailsArr.id===3){
                    $scope.showSecondary=true;
                }
            OccupationInsured.push(OccupationDetailsArr);
        }
        }            
        }
        $scope.Illustration.Insured.OccupationDetails[2]="";
        $scope.Illustration.Insured.OccupationDetails[3]="";
        
        if($scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="Yes"){
            OccupationPayer=angular.copy(OccupationInsured);
        }else{
            $scope.getPayerOccupation();
        }
    }
    
    $scope.getPayerOccupation=function(){    
        OccupationPayer=[];    
        if(!angular.equals(undefined,OccupationPayer))
                        {
                            OccupationPayer.slice();
                        }
        for(var i=0;i<4;i++){
        if(!angular.equals(undefined,$scope.Illustration.Payer.OccupationDetails[i])){
            if(!angular.equals(undefined,$scope.Illustration.Payer.OccupationDetails[i].occupationCode) && $scope.Illustration.Payer.OccupationDetails[i].occupationCode!==""){
            var OccupationDetailsArr = {};
            OccupationDetailsArr={
                            "id":i,
							"occupationCode":$scope.Illustration.Payer.OccupationDetails[i].occupationCode,
                            "description":$scope.Illustration.Payer.OccupationDetails[i].description,
							"job":($scope.Illustration.Payer.OccupationDetails[i].job.indexOf('NA')===-1)?$scope.Illustration.Payer.OccupationDetails[i].job:"NA",
							"subJob":($scope.Illustration.Payer.OccupationDetails[i].subJob.indexOf('NA')===-1)?$scope.Illustration.Payer.OccupationDetails[i].subJob:"NA",
							"occupationClass":$scope.Illustration.Payer.OccupationDetails[i].occupationClass,
							"industry":$scope.Illustration.Payer.OccupationDetails[i].industry,
							"occupationCodeValue":$scope.Illustration.Payer.OccupationDetails[i].occupationCodeValue,
                            "descriptionValue":$scope.Illustration.Payer.OccupationDetails[i].descriptionValue,
							"jobValue":$scope.Illustration.Payer.OccupationDetails[i].jobValue,
							"subJobValue":$scope.Illustration.Payer.OccupationDetails[i].subJobValue,
							"occupationClassValue":$scope.Illustration.Payer.OccupationDetails[i].occupationClassValue,
							"industryValue":$scope.Illustration.Payer.OccupationDetails[i].industryValue,
                            "natureofWork":$scope.Illustration.Payer.OccupationDetails[i].natureofWorkValue,
                            "natureofWorkValue":$scope.Illustration.Payer.OccupationDetails[i].natureofWorkValue
					};
                if(OccupationDetailsArr.id===3){
                    $scope.showSecondaryPayor=true;
                }
            OccupationPayer.push(OccupationDetailsArr);
            }
        }
        }
        $scope.Illustration.Payer.OccupationDetails[2]="";
        $scope.Illustration.Payer.OccupationDetails[3]="";
    }
           
    $scope.onSaveOrTabNav=function(){
    	if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
        }
		if($routeParams.productId == '1010'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6');
			} 
		} else if($routeParams.productId == '1003'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1097'){
            if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_97#PersonalDetails_6']){
                localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_97#PersonalDetails_6');
            }
        }else if($routeParams.productId == '1004'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6');
			} 
		} else if($routeParams.productId == '1006'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1220'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1306'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1274'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6');
			}
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
		}
        OccupationInsuredDisplay=$scope.Illustration.Insured.OccupationDetails;
        OccupationPayerDisplay=$scope.Illustration.Payer.OccupationDetails;
        
        $scope.getInsuredOccupation();
        $scope.Illustration.Insured.OccupationDetails=[];
        $scope.Illustration.Payer.OccupationDetails=[];
        for(var i=0;i<OccupationPayer.length;i++){
            if(OccupationPayer[i].id!==undefined){
        $scope.Illustration.Payer.OccupationDetails[i]=angular.copy(OccupationPayer[i]);
            }
        }
        for(var i=0;i<OccupationInsured.length;i++){
            if(OccupationInsured[i].id!==undefined){
        $scope.Illustration.Insured.OccupationDetails[i]=angular.copy(OccupationInsured[i]);
            }
        }
//        $scope.Illustration.Insured.OccupationDetails[2].slice();
//        $scope.Illustration.Insured.OccupationDetails[3].slice();
        if($scope.Illustration.Insured.OccupationDetails[2]!==undefined){
        $scope.Illustration.Insured.OccupationDetails[2].slice();
        }
        if($scope.Illustration.Insured.OccupationDetails[3]!==undefined){
        $scope.Illustration.Insured.OccupationDetails[3].slice();
        }
        
        if($scope.Illustration.Payer.OccupationDetails[2]!==undefined){
        $scope.Illustration.Payer.OccupationDetails[2].slice();
        }
        if($scope.Illustration.Payer.OccupationDetails[3]!==undefined){
        $scope.Illustration.Payer.OccupationDetails[3].slice();
        }
//        $scope.Illustration.OccupationInsured=[];
//        $scope.Illustration.OccupationPayer=[];
        if (IllustratorVariables.illustrationStatus !== "Confirmed") { 
            $scope.validateOccupationClassInsured();
            $scope.validateOccupationClassPayer();
        }
        IllustratorVariables.setIllustratorModel($scope.Illustration);
    }
    $scope.closePopup = function () {
            $scope.leadDetShowPopUpMsg = false;
        };
    
    //For saving the illustration status as Draft
    $scope.saveIllustrationFromPersonalInfo = function () {                 
          
        $rootScope.showHideLoadingImage(true, 'saveTransactionMessage', $translate);
        $timeout(function () {
            
            if ($scope.errorCount == 0) {
                if(!$scope.validateInsuredAge()){
                $scope.onSaveOrTabNav();
                $scope.SaveTrans = true;
                /* Defect -3760 - starts*/                 
                globalService.setCPInsured($scope.Illustration.Insured);
                globalService.setCPPayer($scope.Illustration.Payer);
                globalService.setCPPrivacyLaw($scope.Illustration.PrivacyLaw);

                /* Defect -3760 - ends*/
                IllustratorVariables.setIllustratorModel($scope.Illustration);
				IllustratorVariables.fromChooseAnotherProduct = false;
                $scope.refresh();
                //$rootScope.transactionId = IllustratorVariables.transactionId;
                var obj = new GLI_IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
                obj.save(function (isAutoSave) {
                    if (rootConfig.isDeviceMobile) {
                        if ($route.current.params.transactionId == "0") {
                            var newParam = $route.current.params;
                            newParam.transactionId = $rootScope.transactionId.toString();
                            $route.updateParams(newParam);
                        }        
                    }

                    $rootScope.showHideLoadingImage(false);
                    $scope.refresh();
                    if ((rootConfig.isDeviceMobile)) {
                        
                        if (!isAutoSave) {
                            $rootScope.NotifyMessages(false, "saveIllustrationSuccessOffline", $translate);
                        }
                    } else {
                        if (!isAutoSave) {
                            $rootScope.NotifyMessages(false, "saveIllustrationSuccessOffline", $translate);
                        }
                    }
                    $scope.SaveTrans = false;
                });
            }
            } else {
                $scope.SaveTrans = true; 
                 $scope.getInsuredOccupation();
        $scope.Illustration.Insured.OccupationDetails=[];
        $scope.Illustration.Payer.OccupationDetails=[];
        for(var i=0;i<OccupationPayer.length;i++){
            if(OccupationPayer[i].id!==undefined){
        $scope.Illustration.Payer.OccupationDetails[i]=OccupationPayer[i];
            }
        }
        for(var i=0;i<OccupationInsured.length;i++){
            if(OccupationInsured[i].id!==undefined){
        $scope.Illustration.Insured.OccupationDetails[i]=OccupationInsured[i];
            }
        }
//        $scope.Illustration.Insured.OccupationDetails[2].slice();
//        $scope.Illustration.Insured.OccupationDetails[3].slice();
        if($scope.Illustration.Insured.OccupationDetails[2]!==undefined){
        $scope.Illustration.Insured.OccupationDetails[2].slice();
        }
        if($scope.Illustration.Insured.OccupationDetails[3]!==undefined){
        $scope.Illustration.Insured.OccupationDetails[3].slice();
        }
        
        if($scope.Illustration.Payer.OccupationDetails[2]!==undefined){
        $scope.Illustration.Payer.OccupationDetails[2].slice();
        }
        if($scope.Illustration.Payer.OccupationDetails[3]!==undefined){
        $scope.Illustration.Payer.OccupationDetails[3].slice();
        }
        if (IllustratorVariables.illustrationStatus !== "Confirmed") { 
            $scope.validateOccupationClassInsured();
            $scope.validateOccupationClassPayer();
        }
        IllustratorVariables.setIllustratorModel($scope.Illustration);
        var obj = new GLI_IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
        obj.save(function (isAutoSave) {
             if (rootConfig.isDeviceMobile) {
                  if ($route.current.params.transactionId == "0") {
                  var newParam = $route.current.params;
                  newParam.transactionId = $rootScope.transactionId.toString();
                  $route.updateParams(newParam);
                  }
            }
            if ($scope.viewToBeCustomized != undefined) {
                $rootScope.updateErrorCount($scope.viewToBeCustomized, true);
            }
            $scope.SaveTrans = false;
        });
                 $rootScope.showHideLoadingImage(false);
                  /*FNA changes by LE Team >>> starts - Bug 4194*/
                 FnaVariables.initialLoadSIFromFNA = false;
                  /*FNA changes by LE Team >>> ends - Bug 4194*/
		         $scope.leadDetShowPopUpMsg = true;
                 $scope.enterMandatory = translateMessages($translate, "enterMandatoryIllustration");

                 }
            
        }, 100);
        
    }
    
    //updating error count dynamically
    $scope.updateErrorDynamically = function () {
        
        var date = new Date();
        $scope.dynamicErrorCount = 0;
        $scope.dynamicErrorMessages = [];
        var currDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
        
        if(($scope.Illustration.Payer.BasicDetails.firstName==="" || $scope.Illustration.Payer.BasicDetails.firstName===undefined) && $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="No" && $scope.showPrivacyLaw){                                           
                    var error = {};
                    error.message = translateMessages($translate, "illustrator.firstNameRequiredValidationMessage");
                    error.key = "payorFirstName";
                    $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                    $scope.dynamicErrorMessages.push(error);        
                }else if($scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="No" && $scope.Illustration.Payer.BasicDetails.firstName) {
                    if(!$scope.testPattern.test($scope.Illustration.Payer.BasicDetails.firstName)){
                        var error = {};
                        error.message = translateMessages($translate, "lms.leadDetailsSectionleadFirstNamePatternValidationMessage");
                        error.key = "payorFirstName";
                        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                        $scope.dynamicErrorMessages.push(error);
                    }
                    
                    if($scope.Illustration.Payer.BasicDetails.firstName && $scope.Illustration.Payer.BasicDetails.firstName.length <3 ){     
					   var error = {};
                       error.message = translateMessages($translate, "lms.leadDetailsSectionleadFirstNameLengthValidationMessage");
                       error.key = 'payorFirstName';
                       $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                       $scope.dynamicErrorMessages.push(error);                   
		          }
                }
        
            if($scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="No" && $scope.Illustration.Payer.BasicDetails.lastName) {
                    if(!$scope.testPattern.test($scope.Illustration.Payer.BasicDetails.lastName)){
                        var error = {};
                        error.message = translateMessages($translate, "lms.leadDetailsSectionleadLastNamePatternValidationMessage");
                        error.key = "payorLastName";
                        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                        $scope.dynamicErrorMessages.push(error);
                    }
                                        
                }
                
                if(($scope.Illustration.Payer.BasicDetails.title==="" || $scope.Illustration.Payer.BasicDetails.title===undefined) && $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="No" && $scope.showPrivacyLaw){                                           
                    var error = {};
                    error.message = translateMessages($translate, "illustrator.titleRequiredValidationMessage");
                    error.key = "payorTitle";
                    $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                    $scope.dynamicErrorMessages.push(error);    
                }
        
                if(($scope.Illustration.Insured.BasicDetails.title==="" || $scope.Illustration.Insured.BasicDetails.title===undefined) && $scope.showPrivacyLaw){                                           
                    var error = {};
                    error.message = translateMessages($translate, "illustrator.titleRequiredValidationMessage");
                    error.key = "insuredTitle";
                    $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                    $scope.dynamicErrorMessages.push(error);    
                }
                
                if(($scope.Illustration.Payer.BasicDetails.gender==="" || $scope.Illustration.Payer.BasicDetails.gender===undefined) && $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="No"){                                           
                    var error = {};
                    error.message = translateMessages($translate, "illustrator.genderMandatoryValidationPayer");
                    error.key = "payorGender";
                    $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                    $scope.dynamicErrorMessages.push(error);    
                }
        
                if($scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="No" && $scope.Illustration.Payer.BasicDetails.nickName) {
                    if(!$scope.testPattern.test($scope.Illustration.Payer.BasicDetails.nickName)){
                        var error = {};
                        error.message = translateMessages($translate, "lms.leadDetailsSectionleadNickNamePatternValidationMessage");
                        error.key = "payorNickName";
                        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                        $scope.dynamicErrorMessages.push(error);
                    }
                }
                if($scope.Illustration.Payer.OccupationDetails[0]===undefined){
                    $scope.Illustration.Payer.OccupationDetails[0]="";
                }
                if($scope.Illustration.Payer.OccupationDetails[2]===undefined){
                    $scope.Illustration.Payer.OccupationDetails[2]="";
                }
                if($scope.Illustration.Payer.OccupationDetails[1]===undefined){
                    $scope.Illustration.Payer.OccupationDetails[1]="";
                }
                if($scope.Illustration.Payer.OccupationDetails[3]===undefined){
                    $scope.Illustration.Payer.OccupationDetails[3]="";
                }
        
                if($scope.Illustration.Insured.OccupationDetails[0]===undefined){
                    $scope.Illustration.Insured.OccupationDetails[0]="";
                }
                if($scope.Illustration.Insured.OccupationDetails[2]===undefined){
                    $scope.Illustration.Insured.OccupationDetails[2]="";
                }
                if($scope.Illustration.Insured.OccupationDetails[1]===undefined){
                    $scope.Illustration.Insured.OccupationDetails[1]="";
                }
                if($scope.Illustration.Insured.OccupationDetails[3]===undefined){
                    $scope.Illustration.Insured.OccupationDetails[3]="";
                }
        
                if((angular.equals(undefined,$scope.Illustration.Payer.OccupationDetails[0].occupationCode)|| $scope.Illustration.Payer.OccupationDetails[0].occupationCode==='' || angular.equals(undefined,$scope.Illustration.Payer.OccupationDetails[0])) && (angular.equals(undefined,$scope.Illustration.Payer.OccupationDetails[2]) || angular.equals(undefined,$scope.Illustration.Payer.OccupationDetails[2].occupationCode)|| $scope.Illustration.Payer.OccupationDetails[2].occupationCode==='') && $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="No"){                                           
                    var error = {};
                    error.message = translateMessages($translate, "illustrator.occupationDetailsRequiredValidationMessage");
                    error.key = "occupationCodePayor";
                    $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                    $scope.dynamicErrorMessages.push(error);       
                }
        
        
                if($scope.Illustration.Insured.OccupationDetails.length > 0 &&
                    (angular.equals(undefined,$scope.Illustration.Insured.OccupationDetails[0].occupationCode)|| $scope.Illustration.Insured.OccupationDetails[0].occupationCode==='' || angular.equals(undefined,$scope.Illustration.Insured.OccupationDetails[0]) || $scope.Illustration.Insured.OccupationDetails[0]==='') && (angular.equals(undefined,$scope.Illustration.Insured.OccupationDetails[2]) || angular.equals(undefined,$scope.Illustration.Insured.OccupationDetails[2].occupationCode)|| $scope.Illustration.Insured.OccupationDetails[2].occupationCode==='' || $scope.Illustration.Insured.OccupationDetails[2]==='')){                                           
                    var error = {};
                    error.message = translateMessages($translate, "illustrator.occupationDetailsRequiredValidationMessage");
                    error.key = "occupationCodeInsured";
                    $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                    $scope.dynamicErrorMessages.push(error);       
                }
                
                if(($scope.Illustration.Payer.ContactDetails.mobileNumber1==="" || $scope.Illustration.Payer.ContactDetails.mobileNumber1===undefined) && $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="No" && $scope.showPrivacyLaw){                                           
                    var error = {};
                    error.message = translateMessages($translate, "illustrator.mobilenumberRequiredValidationMessage");
                    error.key = "contactNumberPayor";
                    $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                    $scope.dynamicErrorMessages.push(error);       
                }else if($scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="No" && $scope.Illustration.Payer.ContactDetails.mobileNumber1) {
//                    if($scope.mobileRegex.test($scope.Illustration.Payer.ContactDetails.mobileNumber1)){
                    if($scope.Illustration.Payer.ContactDetails.mobileNumber1.length<9){
                        var error = {};
                        error.message = translateMessages($translate, "illustrator.mobilenumberPatternMessage");
                        error.key = "contactNumberPayor";
                        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                        $scope.dynamicErrorMessages.push(error);
                    }
                }
        
                if($scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="No" && $scope.Illustration.Payer.ContactDetails.emailId) {
                    if(!$scope.emailRegex.test($scope.Illustration.Payer.ContactDetails.emailId)){
                        var error = {};
                        error.message = translateMessages($translate, "illustrator.emailPatternMessage");
                        error.key = "emailIDPayor";
                        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                        $scope.dynamicErrorMessages.push(error);
                    }
                }
        
                if($scope.Illustration.Insured.BasicDetails.firstName && $scope.Illustration.Insured.BasicDetails.firstName.length <3 ){     var error = {};
                       error.message = translateMessages($translate, "lms.leadDetailsSectionleadFirstNameLengthValidationMessage");
                       error.key = 'insuredFirstName';
                       $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                       $scope.dynamicErrorMessages.push(error);                   
		          }
                    
                if($scope.Illustration.Insured.BasicDetails.nickName  && $scope.Illustration.Insured.BasicDetails.nickName.length <2){                                 
                      var error = {};
                      error.message = translateMessages($translate, "lms.leadDetailsSectionleadNickNameLengthValidationMessage");
                      error.key = 'insuredNickName';
                      $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                      $scope.dynamicErrorMessages.push(error); 
                }                
                
        
        if($scope.Illustration.Payer.BasicDetails.nickName  && $scope.Illustration.Payer.BasicDetails.nickName.length <2){                                 
                      var error = {};
                      error.message = translateMessages($translate, "lms.leadDetailsSectionleadNickNameLengthValidationMessage");
                      error.key = 'payorNickName';
                      $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                      $scope.dynamicErrorMessages.push(error); 
                }             
        
                if(angular.equals(undefined,$scope.Illustration.Payer.BasicDetails.dob) && $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="No"){                                           
                    var error = {};
                    error.message = translateMessages($translate, "illustrator.dobRequiredValidationMessage");
                    error.key = "policyHolderDOB";
                    $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                    $scope.dynamicErrorMessages.push(error);      
                }
        
                if($scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="No" && $scope.Illustration.Payer.BasicDetails.age) {
                    if($scope.Illustration.Payer.BasicDetails.age>99){
                        var error = {};
                        error.message = translateMessages($translate, "illustrator.AgeRestrictionMessage");
                        error.key = "payorAge";
                        $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                        $scope.dynamicErrorMessages.push(error);
                    }
                }
        
        for (index in $scope.dynamicErrorMessages) {
            for (var newindex = Number(index) + 1; newindex < $scope.dynamicErrorMessages.length; newindex++) {
                if ($scope.dynamicErrorMessages[index].key == $scope.dynamicErrorMessages[newindex].key) {
                    $scope.updateDynmaicErrMessageByIndex(newindex);
                    newindex = Number(index);

                }
            }
        }
        if ($scope.viewToBeCustomized != undefined) {
            $rootScope.updateErrorCount($scope.viewToBeCustomized, true);
        }
        $scope.refresh();
    }



    $scope.evaluateStringModel = function (formName, modelName, index) {
        var model = formName + '.' + modelName + index + '.$error.pattern';

        if ($scope.evaluateString(model)) {
            //if ($scope.$eval (model)) {
            $rootScope.updateErrorCount($scope.viewToBeCustomized, true);
            return true;
        } else {
            $rootScope.updateErrorCount($scope.viewToBeCustomized, true);
            return false;
        }

    }

    /*
     * Proceeding to product details tab from personal details on the click of
     * 'proceed' button.
     */


    $scope.proceedToProductDetails = function () {

    	$timeout(function () {
            $scope.updateErrorDynamically();
            var checkMIandAiDataValidation = [],
                miDataToMIAIDatavalidation = {},
                aiDataToMIAIDatavalidation = {};
            $scope.isAdditionalInsuredGreatSet = false;
            $scope.isAdditionalInsuredLessSet = false;

            if($scope.Illustration.Insured.BasicDetails.age<16 && $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="Yes"){
	            $scope.samePayor = false;
	            $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer="No";
	            $scope.onChangeSameAsPayer("No");
	            $scope.updateErrorDynamically();
	            $scope.leadDetShowPopUpMsg = true;
	            $scope.enterMandatory = translateMessages($translate, "illustrator.insuredLessThan16");
	            $rootScope.showHideLoadingImage(false);
	        } else {
	        	
	        	/*
	             * Age Validation For GenBunan 8 Product
	            */
	            if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===3){
	                if(!($scope.Illustration.Insured.BasicDetails.age>=IllustratorVariables.insuredAge.minAge1003 && $scope.Illustration.Insured.BasicDetails.age<=IllustratorVariables.insuredAge.maxAge1003)){
	                    $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                    $scope.leadDetShowPopUpMsg = true;
	                    $scope.enterMandatory = translateMessages($translate, "illustrator.GenBumnanAgeInvalid");
	                }else{
	                    $scope.onSaveOrTabNav();
	                    $scope.proceedToProductAfterValidation();
	                }
	            }
	            
	            /*
	             * Age Validation For GenCancer Care Product
	            */
	            if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===97){
	                if($scope.Illustration.Insured.BasicDetails.age>=0 && $scope.Illustration.Insured.BasicDetails.age<=IllustratorVariables.insuredAge.maxAge1097){
	                    var ageErrIndicator=false;
	                    if(parseInt($scope.Illustration.Insured.BasicDetails.age)===0){
	                        var dobDate = new Date($scope.Illustration.Insured.BasicDetails.dob);
	                        var todayDate = new Date();
	                        var ageInDays=getNumberOfDays($scope.Illustration.Insured.BasicDetails.dob);	                    
		                    if(ageInDays<29){
	                            ageErrIndicator=true;
	                            $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                            $scope.leadDetShowPopUpMsg = true;
	                            $scope.enterMandatory = translateMessages($translate, "illustrator.CancerCareAgeInvalid");
	                        }                        
	                    }
	                    if(!ageErrIndicator){
	                        $scope.onSaveOrTabNav();
	                        $scope.proceedToProductAfterValidation();
	                    }
	                } else {
	                    $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                    $scope.leadDetShowPopUpMsg = true;
	                    $scope.enterMandatory = translateMessages($translate, "illustrator.CancerCareAgeInvalid");
	                }               
	            }
	            
	            /*
	             * Age Validation For GenComplete Health Product
	            */
	            
	            /* Existing Logic Starts - Min Age Changed */
	            /*if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===10){
	                if($scope.Illustration.Insured.BasicDetails.age>=0 && $scope.Illustration.Insured.BasicDetails.age<=IllustratorVariables.insuredAge.maxAge1010){
	                    var ageErrIndicator=false;
	                    if(parseInt($scope.Illustration.Insured.BasicDetails.age)===0){
	                        var dobDate = new Date($scope.Illustration.Insured.BasicDetails.dob);
	                        var todayDate = new Date();
	                        var ageInDays=getNumberOfDays($scope.Illustration.Insured.BasicDetails.dob);	                    
		                    if(ageInDays<29){
	                            ageErrIndicator=true;
	                            $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                            $scope.leadDetShowPopUpMsg = true;
	                            $scope.enterMandatory = translateMessages($translate, "illustrator.CompleteHealthAgeInvalid");
	                        }                        
	                    }
	                    if(!ageErrIndicator){
	                        $scope.onSaveOrTabNav();
	                        $scope.proceedToProductAfterValidation();
	                    }
	                } else {
	                    $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                    $scope.leadDetShowPopUpMsg = true;
	                    $scope.enterMandatory = translateMessages($translate, "illustrator.CompleteHealthAgeInvalid");
	                }               
	            }*/
	            /* Existing Logic Ends - Min Age Changed */
	            
	            if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===10){
	                if(!($scope.Illustration.Insured.BasicDetails.age>IllustratorVariables.insuredAge.minAge1010 && $scope.Illustration.Insured.BasicDetails.age<=IllustratorVariables.insuredAge.maxAge1010)){
	                    $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                    $scope.leadDetShowPopUpMsg = true;
	                    $scope.enterMandatory = translateMessages($translate, "illustrator.CompleteHealthAgeInvalid");
	                } else {
	                    $scope.onSaveOrTabNav();
	                    $scope.proceedToProductAfterValidation();
	                }
	            }
	            
	            /*
	             * Age Validation For GenSave 20 Plus Product
	            */
	            if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===6){
	                if($scope.Illustration.Insured.BasicDetails.age>=0 && $scope.Illustration.Insured.BasicDetails.age<=IllustratorVariables.insuredAge.maxAge1006){
	                    var ageErrIndicator=false;
	                    if(parseInt($scope.Illustration.Insured.BasicDetails.age)===0){
	                        var dobDate = new Date($scope.Illustration.Insured.BasicDetails.dob);
	                        var todayDate = new Date();
	                        var ageInDays=getNumberOfDays($scope.Illustration.Insured.BasicDetails.dob);	                    
		                    if(ageInDays<29){
	                            ageErrIndicator=true;
	                            $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                            $scope.leadDetShowPopUpMsg = true;
	                            $scope.enterMandatory = translateMessages($translate, "illustrator.GenSave20PlusAgeInvalid");
	                        }                        
	                    }
	                    if(!ageErrIndicator){
	                        $scope.onSaveOrTabNav();
	                        $scope.proceedToProductAfterValidation();
	                    }
	                } else {
	                    $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                    $scope.leadDetShowPopUpMsg = true;
	                    $scope.enterMandatory = translateMessages($translate, "illustrator.GenSave20PlusAgeInvalid");
	                }               
	            }
                
	            /*
	             * Age Validation For GenProLife Product
	            */
	            if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===4){
	                if($scope.Illustration.Insured.BasicDetails.age>=0 && $scope.Illustration.Insured.BasicDetails.age<=IllustratorVariables.insuredAge.maxAge1004){
	                    var ageErrIndicator=false;
	                    if(parseInt($scope.Illustration.Insured.BasicDetails.age)===0){
	                        var dobDate = new Date($scope.Illustration.Insured.BasicDetails.dob);
	                        var todayDate = new Date();
	                        var ageInDays=getNumberOfDays($scope.Illustration.Insured.BasicDetails.dob);	                    
	                        
		                    if(ageInDays<29){
	                            ageErrIndicator=true;
	                            $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                            $scope.leadDetShowPopUpMsg = true;
	                            $scope.enterMandatory = translateMessages($translate, "illustrator.GenProLifeAgeInvalid");
	                        }                        
	                    }
	                    if(!ageErrIndicator){
	                        $scope.onSaveOrTabNav();
	                        $scope.proceedToProductAfterValidation();
	                    }
	                }else{
	                    $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                    $scope.leadDetShowPopUpMsg = true;
	                    $scope.enterMandatory = translateMessages($translate, "illustrator.GenProLifeAgeInvalid");
	                }               
	            }
            
	            /*
	             * Age Validation For Whole Life Product
	            */
	            if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===220){
	                if(!($scope.Illustration.Insured.BasicDetails.age>=IllustratorVariables.insuredAge.minAge1220 && $scope.Illustration.Insured.BasicDetails.age<=IllustratorVariables.insuredAge.maxAge1220)){
	                	$rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                	$scope.leadDetShowPopUpMsg = true;
	                	$scope.enterMandatory = translateMessages($translate, "illustrator.WholelifeAgeInvalid");  
	                } else {
	                    $scope.onSaveOrTabNav();
	                    $scope.proceedToProductAfterValidation();
	                }
	            }
            
	            /*
	             * Age Validation For GenSave 10 Plus Product
	            */
	            if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===306){
	                if($scope.Illustration.Insured.BasicDetails.age>=0 && $scope.Illustration.Insured.BasicDetails.age<=IllustratorVariables.insuredAge.maxAge1306){
	                    var ageErrIndicator=false;
	                    if(parseInt($scope.Illustration.Insured.BasicDetails.age)===0){
	                        var dobDate = new Date($scope.Illustration.Insured.BasicDetails.dob);
	                        var todayDate = new Date();
	                        var ageInDays=getNumberOfDays($scope.Illustration.Insured.BasicDetails.dob);	                    
		                    if(ageInDays<29){
	                            ageErrIndicator=true;
	                            $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                            $scope.leadDetShowPopUpMsg = true;
	                            $scope.enterMandatory = translateMessages($translate, "illustrator.GenSave10PlusAgeInvalid");
	                        }                        
	                    }
	                    if(!ageErrIndicator){
	                        $scope.onSaveOrTabNav();
	                        $scope.proceedToProductAfterValidation();
	                    }
	                } else {
	                    $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                    $scope.leadDetShowPopUpMsg = true;
	                    $scope.enterMandatory = translateMessages($translate, "illustrator.GenSave10PlusAgeInvalid");
	                }               
	            }
	            
	            /*
	             * Age Validation For GenSave 4 Plus Product
	            */
	            if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===274){
	                if($scope.Illustration.Insured.BasicDetails.age>=0 && $scope.Illustration.Insured.BasicDetails.age<=IllustratorVariables.insuredAge.maxAge1274){
	                    var ageErrIndicator=false;
	                    if(parseInt($scope.Illustration.Insured.BasicDetails.age)===0){
	                        var dobDate = new Date($scope.Illustration.Insured.BasicDetails.dob);
	                        var todayDate = new Date();
	                        var ageInDays=getNumberOfDays($scope.Illustration.Insured.BasicDetails.dob);	                    
		                    if(ageInDays<29){
	                            ageErrIndicator=true;
	                            $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                            $scope.leadDetShowPopUpMsg = true;
	                            $scope.enterMandatory = translateMessages($translate, "illustrator.GenSave4PlusAgeInvalid");
	                        }                        
	                    }
	                    if(!ageErrIndicator){
	                        $scope.onSaveOrTabNav();
	                        $scope.proceedToProductAfterValidation();
	                    }
	                } else {
	                    $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                    $scope.leadDetShowPopUpMsg = true;
	                    $scope.enterMandatory = translateMessages($translate, "illustrator.GenSave4PlusAgeInvalid");
	                }               
	            }
	        }
        }, 100);
    }
    
    // Customized method on the click of validate and illustrate buttons
    $scope.illustrationDetails = function (container, successCallback, errorCallback) {
        $rootScope.showHideLoadingImage(true, 'calculationProgress', $translate);
        $scope.clearRiderValue = false;
        IllustratorVariables.setIllustratorModel($scope.Illustration);
        PersistenceMapping.clearTransactionKeys();
        IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);

        var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
        var ruleInfo = {};
        if ((rootConfig.isDeviceMobile)) {
            var transactionData = $.parseJSON(transactionObj.TransactionData)
            transactionObj.TransactionData = transactionData;
        }

        //$rootScope.showHideLoadingImage(true, 'calculationProgress', $translate);

        transactionObj.TransactionData = GLI_IllustratorService.convertToCanonicalInput(transactionObj.TransactionData, IllustratorVariables, container);
        ruleInfo.offlineDB = IllustratorVariables.productDetails.offlineDB[0].ruleReference;
        if (container == "illustrate") {

            var illuDate = $scope.Illustration.Product.premiumSummary.validatedDate;
            var isExpired = GLI_IllustratorService.checkExpiryDate(illuDate);
            if (isExpired) {
                $rootScope.showHideLoadingImage(false);
                $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
                    translateMessages($translate, "illustrator.expiryDateMessageIllustration"),
                    translateMessages($translate, "illustrator.okMessageInCaps"));
                $rootScope.premiumSummaryOutput = false;

            } else {
                ruleInfo.ruleName = IllustratorVariables.productDetails.calcSpec[0].ruleReference;
                if (!(rootConfig.isDeviceMobile)) {
                    ruleInfo.ruleGroup = IllustratorVariables.productDetails.calcSpec[0].group;
                }
                if (!(transactionObj.TransactionData.Channel.id)) {
                    transactionObj.TransactionData.Channel.id = IllustratorVariables.channelId;
                    if (transactionObj.TransactionData.Channel.id == "9084") {
                        transactionObj.TransactionData.Channel.name = "Exim";
                    } else if (transactionObj.TransactionData.Channel.id == "9085") {
                        transactionObj.TransactionData.Channel.name = "TCB";
                    } else if (transactionObj.TransactionData.Channel.id == "9082") {
                        transactionObj.TransactionData.Channel.name = "Agency";
                    }
                }

                GLI_RuleService.runCustomRule(transactionObj, $scope.onGetIllustrationDataSuccess, $scope.onGetIllustrationError, ruleInfo, container);


            }
        }
    }
    /*
     * Illustration success call. Map data to scope variables.
     */
    $scope.onGetIllustrationDataSuccess = function (data) {
        //Setting the status to Completed on getting the illustration data
        if (IllustratorVariables.illustrationStatus != "Confirmed") {
            IllustratorVariables.illustrationStatus = "Completed";
        }
        $scope.isValidationRuleExecuted = false;
        $scope.Illustration.IllustrationOutput = data;
        $scope.Illustration.IllustrationOutput.isValidIllustration = IllustratorVariables.isInValidIllustration;
        if ($scope.Illustration.IllustrationOutput.selectedRiderTbl.length > 0) {
            for (totRidersIndex in $scope.Illustration.Product.RiderDetails) {
                for (initialPremiumIndex in $scope.Illustration.IllustrationOutput.selectedRiderTbl) {
                    if ($scope.Illustration.Product.RiderDetails[totRidersIndex].uniqueRiderName == $scope.Illustration.IllustrationOutput.selectedRiderTbl[initialPremiumIndex].uniqueRiderName) {
                        $scope.Illustration.Product.RiderDetails[totRidersIndex].initialPremium = $scope.Illustration.IllustrationOutput.selectedRiderTbl[initialPremiumIndex].yearlyPremium;
                    }
                }
            }
        }

        /*
         * Mapping bpmcode from illustration output.
         */
        for (var rIndx in $scope.Illustration.Product.RiderDetails) {
            if ($scope.Illustration.Product.RiderDetails[rIndx].planName === rootConfig.VGHRidePlanName && $scope.Illustration.IllustrationOutput.VGHRider.LA1.isRequired === "Yes") {
                $scope.Illustration.Product.RiderDetails[rIndx].shortName = $scope.Illustration.IllustrationOutput.VGHRider.LA1.chosenRiderPlanCd;
            }
        }

        var userDetails = UserDetailsService.getUserDetailsModel();
        $scope.Illustration.IllustrationOutput.agentName = userDetails.agentName;
        $scope.Illustration.IllustrationOutput.agentCode = userDetails.agentCode;
        var date = new Date();
        $scope.currentIllustrationDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
        $scope.Illustration.IllustrationOutput.illustratedDate = getFormattedDateDDMMYYYY($scope.currentIllustrationDate);
        $scope.Illustration.IllustrationOutput.productCode = IllustratorVariables.productDetails.code;


        IllustratorVariables.setIllustratorModel($scope.Illustration);
        if ((rootConfig.isDeviceMobile)) {
            $scope.saveIllustrationFromPersonalInfo(true, function () {
                $rootScope.showHideLoadingImage(false);
                $scope.refresh();
            }, function () {
                $rootScope.showHideLoadingImage(false);
                $rootScope.NotifyMessages(false, "saveFailure", $translate);
            });
        } else {
            $rootScope.showHideLoadingImage(false);
            $scope.refresh();
        }
        $scope.nav = Navigation($scope, "ProductDetails", "forward");
        $rootScope.pageNavigationIllus('Illustration Details');

    }

    $scope.onGetIllustrationError = function (data) {
        $scope.okBtnAction = function () {
            $scope.validationResults = "";
            $scope.onGetValidationDataSuccess(data);
        };
        $scope.isValidationRuleExecuted = false;
        IllustratorVariables.isValidationSuccess = false;
        $scope.Illustration.Product.premiumSummary.sumInsured = Math.round(data.sumAssured);
        if (typeof data.ValidationResults != "undefined" ||
            data.ValidationResults != null) {
            $scope.isValidationRuleExecuted = true;
            $scope.validationMessage = translateMessages(
                $translate,
                "illustrator.validationFailed");

            $scope.validationResults = data.ValidationResults;

            $scope.errorKeyArray = [];
            for (var key in $scope.validationResults) {

                var errorKeyValue = [];

                var errorKeyMsg = $scope.validationResults[key][Object.keys($scope.validationResults[key])[0]];
                if (errorKeyMsg.indexOf("#") > -1) {
                    var errorKeyValuePair = errorKeyMsg.split('#');
                    errorKeyValue = Object.keys($scope.validationResults[key]);
                    if (errorKeyValuePair != undefined || errorKeyValuePair != "") {
                        errorKeyValue.push(errorKeyValuePair[1]);
                    }

                    if (errorKeyValue[2] != "" && typeof errorKeyValue[2] != "undefined") {
                        errorKeyValue[2] = $filter('formattedNumber')(errorKeyValue[2]);
                        for (var i = 0; i < rootConfig.errorKeyListForbasicPremium.length; i++) {
                            if (rootConfig.errorKeyListForbasicPremium.indexOf(errorKeyValue[0]) != -1) {
                                var errMsg;
                                errMsg = translateMessages($translate, "illustrator." + errorKeyValue[0]);
                                $scope.validationResults[key] = errMsg;
                            } else {
                                $scope.validationResults[key] = $scope.getRiderErrMsg(errorKeyValue[0], $scope.validationResults[key].PartyName);
                                break;
                            }
                        }

                        var addTranslate = translateMessages($translate, "illustrator.is");
                        var termCheck = "Term";
                        if ((errorKeyValue[0].indexOf(termCheck) != -1) || (errorKeyMsg.indexOf("Age ") != -1)) {
                            /***
                             * Fix for UAT-283 (translation for year)
                             */
                            for (var i = 0; i < rootConfig.errorKeyListForYearTranslation.length; i++) {
                                if (errorKeyValue[0] == rootConfig.errorKeyListForYearTranslation[i]) {
                                    var addTranslate1 = translateMessages($translate, "fna.minYear");
                                    break;
                                } else {
                                    var addTranslate1 = translateMessages($translate, "fna.year");
                                }
                            }
                            if (errorKeyValue[2].indexOf("days") != -1) {
                                $scope.validationResults[key] = $scope.validationResults[key] + addTranslate + " " + errorKeyValue[2];
                            } else {
                                $scope.validationResults[key] = $scope.validationResults[key] + addTranslate + " " + errorKeyValue[2] + addTranslate1;
                            }
                        }
                        if ((errorKeyValue[2].indexOf(",") > -1) || (errorKeyValue[0].indexOf("MaxJuvSA") != -1) || (errorKeyValue[0].indexOf("MaxAdultSA") != -1)) {
                            var addTranslate1 = translateMessages($translate, "illustrator.currencyOption1");
                            $scope.validationResults[key] = $scope.validationResults[key] + addTranslate + " " + errorKeyValue[2] + addTranslate1;
                        }
                        $scope.errorKeyArray.push($scope.validationResults[key]);
                    }

                } else {
                    errorKeyValue = Object.keys($scope.validationResults[key])[0];
                    $scope.validationResults[key] = $scope.getRiderErrMsg(errorKeyValue, $scope.validationResults[key].PartyName);
                    $scope.errorKeyArray.push($scope.validationResults[key]);
                }
            }
        }
        $scope.errCount = Object.keys($scope.errorKeyArray).length;
        //$rootScope.showHideLoadingImage(false);
        //$scope.saveIllustrationFromPersonalInfo(true);	
        $scope.saveIllustrationFromPersonalInfo(true, function () {
            $rootScope.showHideLoadingImage(false);
            $scope.refresh();
        }, function () {
            $rootScope.showHideLoadingImage(false);
            $rootScope.NotifyMessages(false, "saveFailure", $translate);
        });
        $scope.refresh();
    }

    $scope.proceedToProductAfterValidation = function () {
        /* Defect -3760 - starts*/
        globalService.setCPInsured($scope.Illustration.Insured);
        globalService.setCPPayer($scope.Illustration.Payer);
        globalService.setCPPrivacyLaw($scope.Illustration.PrivacyLaw);
        /* Defect -3760 - ends*/

        $scope.Illustration.Payer.BasicDetails.fullName = $scope.Illustration.Payer.BasicDetails.firstName + " " + $scope.Illustration.Payer.BasicDetails.lastName;
        $scope.Illustration.Insured.BasicDetails.fullName = $scope.Illustration.Insured.BasicDetails.firstName + " " + $scope.Illustration.Insured.BasicDetails.lastName;
	 
        IllustratorVariables.setIllustratorModel($scope.Illustration);
        
        if($scope.Illustration.Insured.BasicDetails.occupationClassCode == undefined || $scope.Illustration.Insured.BasicDetails.occupationClassCode == ""){
	       	 if($scope.Illustration.Insured.OccupationDetails[0].occupationClass != undefined){
	       		 if($scope.Illustration.Insured.OccupationDetails[0].occupationClass != ""){
	       			 $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
	       		 }
	       	 } else {
	       		 $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClassValue;
	       	 }
        }
        GLI_IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);        
        /*if (IllustratorVariables.illustrationStatus != "Confirmed" || IllustratorVariables.illustrationStatus != "Completed" ) { 
        	$rootScope.declinedRCCADB = false;
    		$rootScope.declinedRCCAI = false;
    		$rootScope.declinedRCCADD = false;
        }*/
        $scope.nav = Navigation($scope, "PersonalDetails", "forward");        
        $rootScope.pageNavigationIllus('Product Details');
    }

    /*Populate Policy Holder Details to Main Insured and Additional Insured when changing to Self*/
    $scope.onChangePayer = function () {
        if (!(rootConfig.isDeviceMobile)) {
            $timeout(function () {
                $scope.prepopulateMainInsuredDate();
                $scope.updateErrorDynamically();
            }, 100);

        } else {
            /**
             * Fix for UAT bug-388 -Ocuupation class not getting updated on change
             */
            $timeout(function () {
                $scope.prepopulateMainInsuredDate();
                $scope.updateErrorDynamically();
            }, 300);
        }


    };

    $scope.prepopulateMainInsuredDate = function () {
        if ($scope.Illustration && $scope.Illustration.Insured && $scope.Illustration.Insured.CustomerRelationship && $scope.Illustration.Insured.CustomerRelationship.relationShipWithPO != "") {

            if ($scope.Illustration.Insured.CustomerRelationship.relationShipWithPO == "Self") {
                $scope.Illustration.Insured.BasicDetails.firstName = $scope.Illustration.Payer.BasicDetails.firstName;
                $scope.Illustration.Insured.BasicDetails.lastName = $scope.Illustration.Payer.BasicDetails.lastName;
                $scope.Illustration.Insured.BasicDetails.fullName = $scope.Illustration.Payer.BasicDetails.firstName + " " + $scope.Illustration.Payer.BasicDetails.lastName;
                $scope.Illustration.Insured.BasicDetails.dob = $scope.Illustration.Payer.BasicDetails.dob;
                $scope.Illustration.Insured.BasicDetails.age = $scope.Illustration.Payer.BasicDetails.age;
                $scope.Illustration.Insured.BasicDetails.gender = $scope.Illustration.Payer.BasicDetails.gender;
                //$scope.Illustration.Insured.OccupationDetails.description = $scope.Illustration.Payer.OccupationDetails.description;
                //$scope.Illustration.Insured.OccupationDetails.jobClass = $scope.Illustration.Payer.OccupationDetails.jobClass;
                $scope.Illustration.Insured.IdentityDetails.idNo = $scope.Illustration.Payer.IdentityDetails.idNo;
                $scope.Illustration.Insured.IdentityDetails.idIssueDate = $scope.Illustration.Payer.IdentityDetails.idIssueDate;
                $scope.Illustration.Insured.IdentityDetails.idIssuePlace = $scope.Illustration.Payer.IdentityDetails.idIssuePlace;
                $scope.Illustration.Insured.BasicDetails.riskInformation.smoke = $scope.Illustration.Payer.BasicDetails.riskInformation.smoke;
                $scope.Illustration.Insured.ContactDetails.mobileNumber1 = $scope.Illustration.Payer.ContactDetails.mobileNumber1;
                $scope.Illustration.Insured.ContactDetails.currentAddress.houseNo = $scope.Illustration.Payer.ContactDetails.currentAddress.houseNo;
                $scope.Illustration.Insured.ContactDetails.currentAddress.street = $scope.Illustration.Payer.ContactDetails.currentAddress.street;
                $scope.Illustration.Insured.ContactDetails.currentAddress.city = $scope.Illustration.Payer.ContactDetails.currentAddress.city;
                $scope.Illustration.Insured.ContactDetails.currentAddress.district = $scope.Illustration.Payer.ContactDetails.currentAddress.district;
                $scope.Illustration.Insured.ContactDetails.currentAddress.ward = $scope.Illustration.Payer.ContactDetails.currentAddress.ward;
                $scope.Illustration.Insured.BasicDetails.nationality = $scope.Illustration.Payer.BasicDetails.nationality;
                $scope.Illustration.Insured.BasicDetails.countryofResidence = $scope.Illustration.Payer.BasicDetails.countryofResidence;
                $scope.Illustration.Insured.ContactDetails.emailId = $scope.Illustration.Payer.ContactDetails.emailId;
                $scope.Illustration.Insured.BasicDetails.incomeRange = $scope.Illustration.Payer.BasicDetails.incomeRange;
            }
        }
        $scope.refresh();
    }
    $scope.exitIllustrationFromPersonalInfo = function () {
        $scope.lePopupCtrl.showWarning(
						translateMessages($translate,
								"lifeEngage"),
						translateMessages($translate,
								"pageNavigationText"),  
						translateMessages($translate,
								"fna.navok"),$scope.okPressed,translateMessages($translate,
								"general.navproceed"),$scope.backToListing);
    }

    // FNA changes by LE Team - Starts>>
    $scope.chooseProductRecommendation=function(){
       $scope.lePopupCtrl.showWarning(
                        translateMessages($translate,
                                "lifeEngage"),
                        translateMessages($translate,
                                "pageNavigationText"),  
                        translateMessages($translate,
                                "fna.navok"),$scope.okPressed,translateMessages($translate,
                                "general.navproceed"),$scope.proceedToProductRecommendationScreen);     
    }

    $scope.proceedToProductRecommendationScreen=function(){
        IllustratorVariables.fromChooseAnotherProduct = false;
        /* FNA Changes by LE Team Bug 3800 - starts */ 
        IllustratorVariables.fromIllustrationAfterSave = false;
        /* FNA Changes by LE Team Bug 3800 - ends */ 
        $location.path("/fnaRecomendedProducts");  
    }

    $scope.choosePartyScreen=function(){
       $scope.lePopupCtrl.showWarning(
                        translateMessages($translate,
                                "lifeEngage"),
                        translateMessages($translate,
                                "pageNavigationText"),  
                        translateMessages($translate,
                                "fna.navok"),$scope.okPressed,translateMessages($translate,
                                "general.navproceed"),$scope.proceedToPartyScreen);     
    }

    $scope.proceedToPartyScreen=function(){
        IllustratorVariables.fromChooseAnotherProduct = false;
        /* FNA Changes by LE Team Bug 3800 - starts */ 
        if ($scope.navigatedFromFNA && IllustratorVariables.transactionId !== undefined && IllustratorVariables.transactionId !== "") {
            IllustratorVariables.fromIllustrationAfterSave = true;
        }
        /* FNA Changes by LE Team Bug 3800 - ends */ 
        $location.path("/fnaChooseParties/0/" + $routeParams.productId + "/0");  
    }
    // FNA changes by LE Team - Ends>>
    
    $scope.chooseAnotherProduct=function(){     
        $scope.lePopupCtrl.showWarning(
						translateMessages($translate,
								"lifeEngage"),
						translateMessages($translate,
								"pageNavigationText"),  
						translateMessages($translate,
								"fna.navok"),$scope.okPressed,translateMessages($translate,
								"general.navproceed"),$scope.proceedToProductBasket);        
    }
    
    $scope.proceedToProductBasket=function(){
		IllustratorVariables.transactionId = $rootScope.transactionId;
        IllustratorVariables.fromChooseAnotherProduct = true;
         /*FNA changes by LE Team >>> starts - Bug 4194*/
        if (!$scope.navigatedFromFNA && (FnaVariables.flagFromCustomerProfileFNA == false)) {
            $location.path("/products/0/0/0/0/false");  
        } else if(!$scope.navigatedFromFNA && FnaVariables.flagFromCustomerProfileFNA){
            $location.path('/fnaAllProducts');  
        /*FNA changes by LE Team >>> starts - Bug 4194*/
        } else {
            $location.path('/fnaAllProducts');  
        }
    }
    
    $scope.backToListing=function(){
        var leadId = 0;
        leadId = IllustratorVariables.leadId;
        if (UtilityService.leadPage == 'LeadSpecificListing' || (leadId != '' && leadId != undefined)) {
            UtilityService.leadPage = 'LeadSpecificListing';
            $location.path("/MyAccount/Illustration/" + leadId + '/0');
        } else if (UtilityService.leadPage == 'GenericListing') {
            $location.path("/MyAccount/Illustration/0/0");
        }else{
            $location.path("/MyAccount/Illustration/0/0");
        }
        /*FNA changes by LE Team >>> starts - Bug 4194*/
        FnaVariables.flagFromCustomerProfileFNA = false;
        /*FNA changes by LE Team >>> ends - Bug 4194*/
        $scope.refresh();
    }

    $scope.$on("$destroy", function () {
        if (this != null && typeof this !== 'undefined') {
            if (this.$$destroyed) return;
            while (this.$$childHead) {
                this.$$childHead.$destroy();
            }
            if (this.$broadcast) {
                this.$broadcast('$destroy');
            }
            this.$$destroyed = true;
            $timeout.cancel(timer);
        }
    });
} // Controller ends here
