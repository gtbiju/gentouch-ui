/*
 *Name : GLI_ProductsController
 *For implementing GLI specific changes
 */
storeApp.controller('GLI_ProductsController', GLI_ProductsController);
GLI_ProductsController.$inject = ['$rootScope'
		, '$scope'
		, '$parse'
		, '$window'
		, '$location'
		, '$translate'
		, '$routeParams'
		, 'IllustratorVariables'
		, 'ProductService'
		, 'FnaVariables'
		, 'UtilityService'
		, 'UserDetailsService'
		, 'GLI_IllustratorVariables'
		, '$controller', 'MediaService', 'GLI_IllustratorService'];

function GLI_ProductsController($rootScope, $scope, $parse, $window, $location, $translate, $routeParams, IllustratorVariables, ProductService, FnaVariables, UtilityService, UserDetailsService, GLI_IllustratorVariables, $controller, MediaService, GLI_IllustratorService) {
    $controller('ProductsController', {
        $rootScope: $rootScope
        , $scope: $scope
        , $parse: $parse
        , $window: $window
        , $location: $location
        , $translate: $translate
        , $routeParams: $routeParams
        , IllustratorVariables: IllustratorVariables
        , ProductService: ProductService
        , FnaVariables: FnaVariables
        , UtilityService: UtilityService
        , UserDetailsService: UserDetailsService
        , IllustratorVariables: GLI_IllustratorVariables
    });
	$rootScope.hideLanguageSetting = false;
    $rootScope.enableRefresh=false;
    $scope.allproductsList = [];
    $rootScope.declinedRCCADB = false;
	$rootScope.declinedRCCAI = false;
	$rootScope.declinedRCCADD = false;
    $rootScope.selectedPage = "products/0/0/0/0/true";
    $scope.isRDSUser = UserDetailsService.getRDSUser();
    $scope.isTechomBank = false;
    $scope.isWeb = true;
	$scope.showProducts = false;	
	
    if ($scope.isRDSUser) {
        $scope.isWeb = false;
    }
    $scope.initialLoad = function () {
        $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
        $scope.productsListEven = [];
        $scope.productsListOdd = [];
        var transactionObj;
        $scope.isDeviceMobile = (rootConfig.isDeviceMobile);
        $scope.productsListOdd = [];
        if (rootConfig.isDeviceMobile) {
            transactionObj = {
                "CheckDate": new Date().getMonth() + 1 + "-" + new Date().getDate() + "-" + new Date().getFullYear()
                , "Products": {
                    "id": ""
                    , "code": ""
                    , "carrierId": 1
                    , "channelId": IllustratorVariables.channelId
					,"apiVersion":2
                }
            }
        }
        else {
            transactionObj = {
                "CheckDate": new Date().getMonth() + 1 + "-" + new Date().getDate() + "-" + new Date().getFullYear()
                , "Products": {
                    "id": ""
                    , "code": ""
                    , "carrierCode": 1
                    , "channelId": IllustratorVariables.channelId
					,"apiVersion":2
                }
            }
        }
        ProductService.getAllActiveProductsByFilter(transactionObj, $scope.getAllProductsSuccess, $scope.getAllProductsFailure);
    }
    $scope.getIllustration = function (product) {
		
		if(IllustratorVariables.fromChooseAnotherProduct == "false" || IllustratorVariables.fromChooseAnotherProduct == "" ){
			IllustratorVariables.fromChooseAnotherProduct = false;
		}
		
        /*FNA changes by LE Team>> starts*/
        IllustratorVariables.fromFNAChoosePartyScreenFlow = false;
         /*FNA changes by LE Team >>> starts - Bug 4194*/
        FnaVariables.flagFromCustomerProfileFNA = false;
         /*FNA changes by LE Team >>> ends - Bug 4194*/
        /*FNA changes by LE Team>> ends*/

        if (IllustratorVariables.leadId !== "undefined" && IllustratorVariables.leadId && IllustratorVariables.leadId != 0 && !IllustratorVariables.fromChooseAnotherProduct) {
        	if(!rootConfig.isDeviceMobile){
				IllustratorVariables.clearIllustrationVariables();
			}
            UtilityService.previousPage = 'LMS';
            $location.path('/Illustrator/' + product.productId + '/0/0/0');
            //$location.path('/fnaChooseParties/0/' + product.id + '/0');
        } else if(IllustratorVariables.fromChooseAnotherProduct){
            UtilityService.previousPage = 'IllustrationPersonal';
            $location.path('/Illustrator/' + product.productId + '/0/0/0');
        } else {
            IllustratorVariables.clearIllustrationVariables();
            IllustratorVariables.editFlowPackage = true;            
            // IllustratorVariables.setIllustratorModel($scope.Illustration);
            //$location.path('/Illustrator/' + product.id + '/0/' + IllustratorVariables.illustratorId + '/' + $rootScope.transactionId);
			// $location.path('/illustratorProduct/' + product.productId + '/0/' + IllustratorVariables.illustratorId + '/' + $rootScope.transactionId);
            $location.path('/Illustrator/' + product.productId + '/0/0/0');
        }
    }
    if (IllustratorVariables.illustrationFromKMC || IllustratorVariables.illustrationFromIllusPersonal) {
        $scope.showIllustrationBtn = true;
    }
    else {
        $scope.showIllustrationBtn = false;
    }
    // to check previous page
    if (IllustratorVariables.illustrationStatus == "" || IllustratorVariables.illustrationStatus == null) {
        if (!(rootConfig.isDeviceMobile) && IllustratorVariables.illustrationFromIllusPersonal == true) {
            $scope.showIllustrationBtn = true;
        }
        else {
            $scope.showIllustrationBtn = false;
        }
    }
    $scope.playVideo = function () {
        MediaService.playVideo($scope, 'iPLAN_Video.mp4');
    };
    $scope.illustrationProductListing = rootConfig.illustrationProductListing;
    $scope.getAllProductsSuccess = function (data) {

    	var applicableProducts = UserDetailsService.getApplicableProducts();
        for (prdIndx in data.Products) {
            var isProdSellable = false;
            for (var y = 0; y < applicableProducts.length; y++) {
                if (data.Products[prdIndx].code == applicableProducts[y].prdCode) {
                    isProdSellable = true;
                    break;
                }
            }
            if (!isProdSellable) {
                data.Products.splice(prdIndx);
            }
        }
		var sortedProducts = data.Products;
        var sortProducts = data.Products;
        var sortedProducts = [];
        for (var e = 0; e < Object.keys(data.Products).length; e++) {
            if (data.Products[e].id == "4") {
                var genProlife = data.Products[e];
            } else if (data.Products[e].id == "10") {
                var compHealth = data.Products[e];  
            } else if (data.Products[e].id == "97") {
                var genCancer = data.Products[e];  
            } else if (data.Products[e].id == "6") {
                var genSave20P = data.Products[e];
            } else if (data.Products[e].id == "3") {
                  var genBumnan = data.Products[e];
            } else if (data.Products[e].id == "220") {
                var wholeLife = data.Products[e];
            } else if (data.Products[e].id == "306") {
                var genSave10P = data.Products[e];
            } if (data.Products[e].id == "274") {
                var genSave4P = data.Products[e];
            }
        }
        
        sortedProducts.push(wholeLife);
        //sortedProducts.push(genSave4P);//Commented as part of descope of genSave4P
        //sortedProducts.push(genSave10P);//Commented as part of descope of genSave10P
        sortedProducts.push(genProlife);
        sortedProducts.push(compHealth);
        sortedProducts.push(genCancer);
        sortedProducts.push(genSave20P);
        sortedProducts.push(genBumnan);

        $scope.allproductsList = [];
        $scope.productsListEven = [];
        $scope.productsListOdd = [];
		$scope.uniqueProductGroupTitle = [];
        $scope.allproductsList = sortedProducts;
        IllustratorVariables.productDetails = $scope.allproductsList;
        IllustratorVariables.productDetails = JSON.stringify(IllustratorVariables.productDetails[0]);
        //IllustratorVariables.productDetails = JSON.parse(IllustratorVariables.productDetails);
        //$rootScope.showHideLoadingImage(false);
        for (var x = 0; x < $scope.allproductsList.length; x += 2) {
            $scope.productsListEven.push($scope.allproductsList[x]);
        }
        for (var y = 1; y < $scope.allproductsList.length; y += 2) {
            $scope.productsListOdd.push($scope.allproductsList[y]);
        }

		//Get unique product groups
		for(var x = 0; x < $scope.allproductsList.length; x++){
			if($scope.allproductsList[x].productGroup != undefined){
				if($scope.uniqueProductGroupTitle.indexOf($scope.allproductsList[x].productGroup) == -1){
					$scope.uniqueProductGroupTitle.push($scope.allproductsList[x].productGroup);
				}
			}
		}
        //Added path for product images, which is downloaded from Content admin
        if (rootConfig.isDeviceMobile) {
            var leFileUtils = new LEFileUtils();
            leFileUtils.getApplicationPath(function (documentsPath) {
                if (rootConfig.isOfflineDesktop) {
                    $scope.fileFullPath = "file://" + documentsPath;
                }
                else {
                    $scope.fileFullPath = "file://" + documentsPath + "/";
                }
            });
        }
        else {
            $scope.fileFullPath = rootConfig.mediaURL;
        }
        $rootScope.showHideLoadingImage(false);
        $scope.refresh();
    }
    $scope.evaluateString = function (value) {
        var val;
        var variableValue = $scope;
        val = value.split('.');
        if (value.indexOf('$scope') != -1) {
            val = val.shift();
        }
        for (var i in val) {
            variableValue = variableValue[val[i]];
        }
        return variableValue;
    };
    $scope.openPDF = function (index, productCode, type) {
        var escape = false
            , pdfFile;
        var productsLength = $scope.allproductsList.length;
        for (var k = 0; k < productsLength; k++) {
            if ($scope.allproductsList[k].code == productCode) {
                for (var l = 0; l < $scope.allproductsList[k].collaterals.length; l++) {
                    if ($scope.allproductsList[k].collaterals[l].docType == type) {
                            pdfFile = $scope.allproductsList[k].collaterals[l].fileName;
                        escape = true;
                        break;
                    }
                }
                if (escape) {
                    break;
                }
            }
        }
        if((rootConfig.isDeviceMobile) && (!rootConfig.isOfflineDesktop)){
            var fileFullPath = "";
            window.plugins.LEFileUtils.getApplicationPath(function (path) {
                fileFullPath = "file://" + path + "/" + pdfFile;
                cordova.exec(function () {}, function () {}, "PdfViewer", "showPdf", [fileFullPath]);
            }, function (e) {});
        }else if(((rootConfig.isDeviceMobile) && (rootConfig.isOfflineDesktop)) ||((!rootConfig.isDeviceMobile) && (!rootConfig.isOfflineDesktop))){
            MediaService.openPDF($scope, $window, pdfFile);
        }
    }
    $scope.playVideo = function (index, productCode, type) {
        var escapeVideo = false
            , videoFile;
        var productsLength = $scope.allproductsList.length;
        for (var k = 0; k < productsLength; k++) {
            if ($scope.allproductsList[k].code == productCode) {
                for (var l = 0; l < $scope.allproductsList[k].collaterals.length; l++) {
                    if ($scope.allproductsList[k].collaterals[l].docType == type) {
                        videoFile = $scope.allproductsList[k].collaterals[l].fileName;
                        escapeVideo = true;
                        break;
                    }
                }
                if (escapeVideo) {
                    break;
                }
            }
        }
        if (videoFile == "GENBUMNAN.mp4" || videoFile == "GENCOMPLETE.mp4" || videoFile == "GenProLife.mp4" || videoFile == "GENPROCASH.mp4") {
            MediaService.playVideo($scope, videoFile);
        }
    };
    $scope.playArmsVideo = function () {
        MediaService.playVideo($scope, 'arms.mp4');
    };
	
	
	/* Revamped product listing logic */
	
	$scope.listSelectedProducts = function(type){
		$scope.showProducts = true;
		$scope.uniqueProductGroup = [];
		for(var x = 0; x < $scope.allproductsList.length; x++){
			if($scope.allproductsList[x].productGroup == type){
				$scope.uniqueProductGroup.push($scope.allproductsList[x]);
			}
		}
		$scope.slideCount=Math.ceil($scope.uniqueProductGroup.length/4);
	}
	$scope.backToProductsGroup = function(){
		$scope.showProducts = false;	
	}
	/* Revamped product listing logic */
	
	$scope.showKeyFeatures = function(divID)
	{
		$scope.keyFeaturesOverlay = true;
		$(divID).css('display', 'inline');
	}
	$scope.hideKeyFeatures = function(divID)
	{
		$scope.keyFeaturesOverlay = false;
		$(divID).css('display', 'none');
	}
	
    $scope.$on("$destroy", function () {
        if (this != null && typeof this !== 'undefined') {
            if (this.$$destroyed) return;
            while (this.$$childHead) {
                this.$$childHead.$destroy();
            }
            if (this.$broadcast) {
                this.$broadcast('$destroy');
            }
            this.$$destroyed = true;
            //$timeout.cancel( timer );    
        }
    });
} // Controller ends here