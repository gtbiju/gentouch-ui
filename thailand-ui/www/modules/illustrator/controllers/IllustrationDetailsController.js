/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:IllustrationDetailsController
 CreatedDate:4/14/2013
 Description:illustrator illustration listing controller.
 */

storeApp.controller('IllustrationDetailsController', ['$rootScope', '$scope', '$compile', '$routeParams', 'UtilityService', 'DataService', 'DocumentService', '$translate', '$location', 'dateFilter', 'IllustratorVariables', 'IllustratorService', '$debounce', 'AutoSave', 'FnaVariables', 'PersistenceMapping', 'PlatformInfoService',
function IllustrationDetailsController($rootScope, $scope, $compile, $routeParams, UtilityService, DataService, DocumentService, $translate, $location, dateFilter, IllustratorVariables, IllustratorService, $debounce, AutoSave, FnaVariables, PersistenceMapping, PlatformInfoService) {
	$rootScope.moduleHeader = "illustrator.Illustrator";
	$rootScope.moduleVariable = translateMessages($translate, $rootScope.moduleHeader);
	$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
	$scope.refresh();
	$scope.Illustration = IllustratorVariables.getIllustratorModel();
	$scope.IllustrationAttachment = IllustratorVariables.getIllustrationAttachmentModel();
	$scope.selectedpage = "Illustration Details";
	$scope.dvAlertPopup = false;
	$scope.productId = $routeParams.productId;
	/*$('.sigPadIllustration').signaturePad({
		drawOnly : true,
		lineWidth : 0
	});
	$('.sigPadIllustration').parents("*").css("overflow", "visible");*/
	
	$scope.cancel = function() {
		$scope.dvAlertPopup = false;
	}
	$scope.mapDocScopeToPersistence = function(document) {
		var docObject = {};
		var newDate = new Date();
		var createdDate = Number(newDate.getMonth() + 1) + "-" + newDate.getDate() + "-" + newDate.getFullYear();
		docObject.id = document != null && document.id != null ? document.id : null;
		docObject.parentId = ($rootScope.transactionId != null && $rootScope.transactionId != 0) ? $rootScope.transactionId : null;
		docObject.documentType = ($scope.isImage && $scope.isPhoto) ? "Photograph" : ($scope.isSignature ? "Signature" : (document != null && document.documentType != null ? document.documentType : " "));
		if ((rootConfig.isDeviceMobile)) {
			docObject.documentName = document != null && document.documentName != null ? document.documentName : "";
			docObject.documentStatus = document != null && document.documentStatus != null ? document.documentStatus : " ";
			docObject.documentDescription = document != null && document.documentDescription != null ? document.documentDescription : " ";

			docObject.date = createdDate;
			docObject.documentObject = "";
			document.date = createdDate;
			docObject.documentObject = document != null ? document : "";
		} else {
			docObject.documentName = document != null && document.name != null ? document.name : "";
			docObject.documentStatus = " ";
			docObject.isSigned = document != null && document.isSigned != null ? document.isSigned : " ";
			docObject.date = createdDate;
			if (document != null) {
				docObject.fileObj = document;
			}
		}
		return docObject;
	}

	$scope.setImageToScope = function(imageToBind) {
		var docObj = $scope.mapDocScopeToPersistence(imageToBind);
		if ((rootConfig.isDeviceMobile)) {
			if (docObj.documentObject.pages[0].fileContent.length > 0)
				$scope.setSignature(docObj.documentObject.pages[0].fileContent);
		} else {
			if (docObj.fileObj) {
				if (docObj.fileObj.pages[0].fileContent.length > 0)
					$scope.setSignature(docObj.fileObj.pages[0].fileContent);
			}
		}
		$scope.refresh();
	}

	$scope.setSavedSigIllustration = function() {
		if (!jQuery.isEmptyObject($scope.IllustrationAttachment.Upload.Signature)) {
			$scope.sigPadApi = $('.sigPadIllustration').signaturePad({
				displayOnly : true
			});
			$scope.setImageToScope($scope.IllustrationAttachment.Upload.Signature[0]);
		}
	}

	$scope.setSignature = function(signatureData) {
		var sig = JSON.parse(signatureData);
		$scope.sigPadApi.regenerate(sig);
		$scope.refresh();
	}

	$scope.clearSignature = function() {
		var api = $('.sigPadIllustration').signaturePad({
			drawOnly : true
		});
		api.clearCanvas();
		$scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent = "";
	}

	$scope.loadSignature = function() {
		var sigElement = $('.sigPadIllustration');
		var sigPadIllustration = $('.sigPadIllustration').signaturePad({
			drawOnly : true
		});
		var scope = angular.element(sigElement[0]).scope();
		scope.$apply(function() {
			scope.sigPadApi = sigPadIllustration;
			$scope.setSavedSigIllustration();
		});
	}
	$scope.onPaintUISuccess = function(callBackId) {
        //dowload base64 for pdf
        if(!rootConfig.isDeviceMobile){
            PlatformInfoService.getPlatformInfo(
                function(platFormInfo) {
				    if(platFormInfo.browserName == "Microsoft Internet Explorer" && parseInt(platFormInfo.browserVersion)==9 ){	
				        $scope.downloadPdfwithFlash();
				    }
	
            });
		}
		$rootScope.showHideLoadingImage(false);
		$scope.refresh();
		$scope.loadSignature();
	}
	if ((rootConfig.isDeviceMobile)) {
		$scope.isDeviceMobile = true;
	} else {
		$scope.isDeviceMobile = false;
	}
	// IllustratorVariables.illustrationOutputTemplate =
	// 'modules/illustrator/uijson/WLPS_IllustrationOutput.js';
	// $scope.IllustrationOutput =
	// IllustratorVariables.IllustrationOutput;
	$scope.Illustration.age = updateAge($scope.Illustration.Insured.BasicDetails.dob);
	$scope.tableChartView = true;
	//For calling the PaintUI function
	$scope.initialLoad= function(){
		LEDynamicUI.paintUI(rootConfig.template, IllustratorVariables.illustrationOutputTemplate, "IllustrationDetails", "#IllustrationDetails", true, function() {
			LEDynamicUI.paintUI(rootConfig.template, rootConfig.illustratorUIJson, "HeaderDetails", "#HeaderDetails", true, $scope.onPaintUISuccess, $scope, $compile);
			$scope.showHeaderImages = true;
		}, $scope, $compile);
	}
    $scope.isShowFlash = false;
    if (!rootConfig.isDeviceMobile) {
        PlatformInfoService.getPlatformInfo(function (platFormInfo) {
            if (platFormInfo.browserName == "Microsoft Internet Explorer" && parseInt(platFormInfo.browserVersion) == 9) {
                $scope.isShowFlash = true;
                //	$scope.downloadPdfwithFlash();
            }
        });
    }
	$scope.showErrorCount = true;
	UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
	var model = "Illustration";
	if ((rootConfig.autoSave.illustration)) {
		AutoSave.setupWatchForScope(model, DataService, UtilityService, IllustratorService, $scope, $debounce, $translate, $routeParams, "");
	}
	/*
	 * Calculate age from Date of Birth.
	 */
	function updateAge(dob) {
		var dobDate = new Date(dob);
		/*
		 * In I.E and safari it will accept date format
		 * in '/' separator only
		 */
		if ((dobDate == "NaN" || dobDate == "Invalid Date") && dob != "" && dob != undefined && dob != null) {
			dobDate = new Date(dob.split("-").join("/"));
		}
		var todayDate = new Date();
		var age = todayDate.getFullYear() - dobDate.getFullYear();
		if (todayDate.getMonth() < dobDate.getMonth() - 1 || (dobDate.getMonth() - 1 === todayDate.getMonth() && todayDate.getMonth() < dobDate.getDate())) {
			age--;
		}
		return age;
	}


	$scope.confirmSignature = function() {
		$scope.saveSignature(function() {
			$rootScope.NotifyMessages(false, "successMessage", $translate);
			$scope.refresh();
		});
	}
	$scope.saveSignature = function(successCallback) {

		var sigElement = $('.sigPadIllustration').signaturePad();
		var sigData = sigElement.getSignatureString();
		$scope.isSignature = true;
		var currAgentId = $rootScope.agentId == null ? $rootScope.username : $rootScope.agentId;
		var document = {};
		document = $scope.mapDocScopeToPersistence($scope.IllustrationAttachment.Upload.Signature[0]);
		var d = new Date();
		var n = d.getTime();
		var name = "document";
		var newFileName = "Agent1" + n + "_" + name;
		if ((rootConfig.isDeviceMobile)) {
			if (!document.documentName) {
				document.documentName = newFileName;
				document.documentType = "Signature";
				document.documentObject.documentName = newFileName;
				document.documentObject.documentType = "Signature";
				document.documentObject.documentStatus = "Saved";
			} else {
				DataService.getDocument(document, function(doc) {
					document.id = doc[0].Id;
					document.parentId = doc[0].ParentId;
					document.documentType = doc[0].DocumentType;
					document.documentName = doc[0].DocumentName;
					document.documentStatus = doc[0].DocumentStatus;
					document.date = doc[0].CreatedDate;
					document.documentObject.documentName = doc[0].DocumentName;
					document.documentObject.documentType = doc[0].DocumentType;
					document.documentObject.documentStatus = doc[0].DocumentStatus;
				}, $scope.operationError);
			}

			if (document.documentObject.pages[0].fileContent != sigData) {
				$('#confirmSig').attr('disabled', 'disabled');
				document.documentObject.pages[0].fileName = newFileName;
				document.documentObject.pages[0].fileContent = sigData;
				$scope.onLocalFileWriteSuccess(document, function(fileData, results) {
					if (results != null && results.insertId != null) {
						fileData.id = results.insertId != null ? results.insertId : null;
					}
					$scope.IllustrationAttachment.Upload.Signature[0] = fileData.documentObject;
					IllustratorVariables.setIllustrationAttachmentModel($scope.IllustrationAttachment);
					$('#confirmSig').removeAttr('disabled');
					successCallback();
				}, $scope.operationError);
			} else {
				successCallback();
			}
		} else {
			if (!jQuery.isEmptyObject($scope.IllustrationAttachment.Upload.Signature[0])) {
				if ($scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent != sigData) {
					if ($scope.IllustrationAttachment.Upload.Signature[0].documentName == "") {
						$scope.IllustrationAttachment.Upload.Signature[0].documentName = newFileName;
					}
					$scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent = sigData;
					$scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileName = newFileName;
					$scope.IllustrationAttachment.Upload.Signature[0].documentStatus = "Saved";
				}
				if ($scope.IllustrationAttachment.Upload.Signature[0].documentStatus != "Synced") {
					$rootScope.showHideLoadingImage(true, 'saveTransactionMessage', $translate);
					var docData = [];
					docData.push(angular.copy($scope.IllustrationAttachment.Upload.Signature[0]));
					var docDetails = {
						Documents : docData[0]
					};
					PersistenceMapping.clearTransactionKeys();
					IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
					PersistenceMapping.dataIdentifyFlag = false;
					var docDetailsWithKeys = PersistenceMapping.mapScopeToPersistence(docDetails);
					var uploadDocIndex=0;
					DocumentService.uploadDocuments(docDetailsWithKeys, uploadDocIndex, function(data) {
						$scope.IllustrationAttachment.Upload.Signature[0].documentStatus = "Synced";
						IllustratorVariables.setIllustrationAttachmentModel($scope.IllustrationAttachment);
						$rootScope.showHideLoadingImage(false, "");
						successCallback();
					}, $scope.operationError);
				} else {
					successCallback();
				}
			}
		}
	}
	// Function called for saving signature
	$scope.onLocalFileWriteSuccess = function(document, successCallback, errorCallback) {
		if (!(successCallback && typeof successCallback == "function")) {
			successCallback = $scope.onLocalFileSaveSuccess;
		}
		if (!(errorCallback && typeof errorCallback == "function")) {
			errorCallback = $scope.operationError;
		}
		document.documentStatus = "Saved";
		if (document.parentId == null) {
			$scope.saveTransactions(false, function(data) {
				$rootScope.transactionId = data;
				document.parentId = $rootScope.transactionId;
				$scope.status = "Saved";
				$scope.refresh();
				DataService.saveFileLocally(document, successCallback, errorCallback);
			}, $scope.operationError);
		} else {
			DataService.getDocument(document, function(doc) {
				if (doc[0] != null && typeof doc[0] != "undefined") {
					document.id = doc[0].Id;
				}
				DataService.saveFileLocally(document, successCallback, errorCallback);
			}, $scope.operationError);

		}
	}
	// Function called on success of document save
	$scope.onLocalFileSaveSuccess = function(fileData, results) {
		if (results != null && results.insertId != null) {
			fileData.id = results.insertId != null ? results.insertId : null;
			if ($scope.isSignature) {
				$scope.IllustrationAttachment.Upload.Signature[0].pages[0] = fileData.documentObject;
			}
		}
		$rootScope.showHideLoadingImage(false, "");
		$scope.refresh();
	}

	$scope.saveIllustrationInfo = function() {
		IllustratorVariables.setIllustratorModel($scope.Illustration);
		$scope.saveSignature(function() {
			$rootScope.showHideLoadingImage(true, 'saveTransactionMessage', $translate);
			$scope.refresh();
			var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
			obj.save(function(isAutoSave) {
				$rootScope.showHideLoadingImage(false);
				/*
				 * if
				 * ((rootConfig.isDeviceMobile)) {
				 * if(!isAutoSave) {
				 * $rootScope.NotifyMessages(false,
				 * "saveIllustrationSuccessOffline",
				 * $translate); } } else {
				 * if(!isAutoSave) {
				 * $rootScope.NotifyMessages(false,
				 * "saveIllustrationSuccess",
				 * $translate,
				 * IllustratorVariables.illustratorId); } }
				 */
				$location.path("/MyAccount/Illustration/0/0");
			});
		});
	}
	/*
	 * Switching chart and table view in Illustration
	 * tab on click of respective buttons.
	 */
	$scope.illustrationView = function(value) {
		if (value == 'chart') {
			$scope.tableChartView = false;
			// $('.select_icon_disabled_1').removeClass().addClass('select_icon
			// select_icon_1');
			// $('.select_icon_2').removeClass().addClass('select_icon
			// select_icon_disabled_2');
		} else if (value == 'table') {
			$scope.tableChartView = true;
			// $('.select_icon_disabled_2').removeClass().addClass('select_icon
			// select_icon_2');
			// $('.select_icon_1').removeClass().addClass('select_icon
			// select_icon_disabled_1');
		}

	}
	$scope.operationError = function(e) {
		$rootScope.NotifyMessages(true, "docUploadTransactionsError", $translate);
		$rootScope.showHideLoadingImage(false, "");
	}
    $scope.downloadPdfwithFlash = function () {
        var base64data = "";
        PersistenceMapping.clearTransactionKeys();
        IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
        PersistenceMapping.dataIdentifyFlag = false;
        var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
        DataService.downloadBase64(transactionObj, function (response) {
            base64data = response.data.Response.ResponsePayload.Transactions[0].base64pdf;
            Downloadify.create('downloadify', {
                dataType: 'base64'
                , filename: function () {
                    return "Illustration_" + IllustratorVariables.illustratorId + ".pdf";
                }
                , data: function () {
                    return base64data;
                }
                , onComplete: function () {
                    //alert('Your File Has Been Saved!');
                }
                , onCancel: function () {
                    //alert('You have cancelled the saving of this file.');
                }
                , onError: function () {
                    //alert('You must put something in the File Contents or there will be nothing to save!'); 
                }
                , swf: './lib/vendor/downloadplugin/downloadify.swf'
                , downloadImage: './css/main-theme/img/download.png'
                , width: 100
                , height: 30
                , transparent: true
                , append: false
            });
        }, function (err) {
            //err
        });
    };
	$scope.downloadApplicationPDF = function() {
		$scope.templateId = $scope.Illustration.Product.templates.illustrationPdfTemplate;
		$rootScope.showHideLoadingImage(true, 'downloadProgress', $translate);
		PersistenceMapping.clearTransactionKeys();
		IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
		PersistenceMapping.dataIdentifyFlag = false;
		PersistenceMapping.Type = "illustration";
		var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
		if ((rootConfig.isDeviceMobile)) {
			if (checkConnection()) {
				DataService.saveOnline(transactionObj, function(data) {
					IllustratorVariables.illustratorId = data.Key3;
					var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
					obj.save(function(isAutoSave) {
						if (!jQuery.isEmptyObject($scope.IllustrationAttachment.Upload.Signature[0])) {
							var fileContent = $scope.IllustrationAttachment.Upload.Signature[0].pages[0].fileContent;
							if (fileContent.length > 2) {
								var docData = [];
								docData.push(angular.copy($scope.IllustrationAttachment.Upload.Signature[0]));
								var docDetails = {
									Documents : docData[0]
								};
								PersistenceMapping.clearTransactionKeys();
								IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
								PersistenceMapping.dataIdentifyFlag = false;
								var docDetailsWithKeys = PersistenceMapping.mapScopeToPersistence(docDetails);
								DocumentService.uploadDocuments(docDetailsWithKeys, "1", function(data, count) {
									DataService.downloadPDF(transactionObj, $scope.templateId, function(data) {
										$rootScope.showHideLoadingImage(false);
										$scope.refresh();
									},function(error){
										$rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
										$rootScope.showHideLoadingImage(false);
										$scope.refresh();
									});
								});
							} else {
								DataService.downloadPDF(transactionObj, $scope.templateId, function(data) {
									$rootScope.showHideLoadingImage(false);
									$scope.refresh();
								}, function(error) {
									$rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
									$rootScope.showHideLoadingImage(false);
									$scope.refresh();
								});
							}
						}
					});
				}, $scope.error);
			}else{
				$rootScope.showHideLoadingImage(false);
				$scope.dvAlertPopup = true;
				$scope.refresh();
			}
		} else {
			DataService.downloadPDF(transactionObj, $scope.templateId, function(data) {
				$rootScope.showHideLoadingImage(false);
				$scope.refresh();
			},function(error){
				$rootScope.NotifyMessages(true, "pdfDownloadTransactionsError", $translate);
				$rootScope.showHideLoadingImage(false);
				$scope.refresh();
			});
		}
	}
	$scope.$on("$destroy", function() {
		//piechart.destroy();
		if(linechart)
		linechart.destroy();
		//chartbinddata();
		//comparisonChart.destroy();
	});
	$scope.$on('$viewContentLoaded', function(){
		$scope.initialLoad();
	});
}]);
