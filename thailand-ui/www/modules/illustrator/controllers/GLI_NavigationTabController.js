storeApp.controller('GLI_NavigationTabController', GLI_NavigationTabController);
GLI_NavigationTabController.$inject = ['$scope', '$rootScope','GLI_IllustratorService','GLI_IllustratorVariables', 'PersistenceMapping','$compile', '$translate', '$location', '$routeParams', 'DataService', 'UtilityService', 'IllustratorVariables', 'IllustratorService','$controller'];

function GLI_NavigationTabController($scope, $rootScope, GLI_IllustratorService, GLI_IllustratorVariables, PersistenceMapping, $compile, $translate, $location, $routeParams, DataService, UtilityService, IllustratorVariables, IllustratorService, $controller) {
    $controller('NavigationTabController', {
        $scope: $scope,
        $rootScope: $rootScope,
        IllustratorService: GLI_IllustratorService,
        IllustratorVariables: GLI_IllustratorVariables,
        PersistenceMapping: PersistenceMapping,
        $compile: $compile,
        $translate: $translate,
        $location: $location,
        $routeParams: $routeParams,
        DataService: DataService,
        UtilityService: UtilityService
    });
    $rootScope.enableRefresh=false;
    var minAge1003=parseInt(20); 
    var maxAge1003=parseInt(50);
    var minAge1010=parseInt(0);
    var maxAge1010=parseInt(70);
    var maxAge1006=parseInt(60);    
    var maxAge1004=parseInt(65);
    var minAge1097=parseInt(0);
    var maxAge1097=parseInt(65);
	var minAge1220=parseInt(16);
    var maxAge1220=parseInt(70);
    var maxAge1306=parseInt(64); 
    var maxAge1274=parseInt(64); 
    var parentScope = $scope.$parent;
    parentScope.child = $scope;
    
    //Variable to disable the controls if the status is Confirmed or Completed with an eApp
    if (IllustratorVariables.illustrationStatus == "Confirmed" || (IllustratorVariables.illustrationStatus == "Completed" && $rootScope.haseApp == true)) {
        $rootScope.disableControl = true;
    } else {
        $rootScope.disableControl = false;
    }
    //over riding illTabNavigation
    $scope.illTabNavigation = function (index) {
        $scope.selectedtab = index;
        for (var i = 0; i < $scope.illTabNavStyle.length; i++) {
            if (i < index) {
                $scope.illTabNavStyle[i] = 'activated';
            } else if (i == index) {
                $scope.illTabNavStyle[i] = 'activate';
            } else {
                $scope.illTabNavStyle[i] = '';
            }
        }
        if (angular.equals(
                $scope.Illustration.IllustrationOutput, {})) {
            $scope.illTabNavStyle[2] = "disabled";
            if (index == 0)
                $scope.illTabNavStyle[1] = "disabled";
        }
    }

    $scope.setIllustrationTabs = function () {
        if (IllustratorVariables.illustrationStatus == "Confirmed" || (IllustratorVariables.illustrationStatus == "Completed" && $rootScope.haseApp == true)) {
            $rootScope.disableControl = true;
        } else {
            $rootScope.disableControl = false;
        }
        if ($scope.selectedpage == "Personal Details") {
            $scope.setTabNavigation(0);

        } else if ($scope.selectedpage == "Product Details") {
            $scope.setTabNavigation(1);
        } else {
            $scope.setTabNavigation(2);
        }

    }

    //Tabs Overiding
    $rootScope.pageNavigationIllus = function (rel, index) {
        IllustratorVariables.setIllustratorModel($scope.Illustration);
//        IllustratorVariables.insuredAge = illustrationConfigJson.insuredAge;
        var currenIllustrationtPage = $scope.selectedpage;
        if (rel != currenIllustrationtPage) {
            $rootScope.showHideLoadingImage(true,
                'paintUIMessage', $translate);
            IllustratorVariables.selectedpage = rel;
            $scope.saveIllustrationOnTabClick();
        } else {
            $rootScope.showHideLoadingImage(false);
        }
    }

    //extending paintNavigationTab
    $scope.paintNavigationTab = function () {
        $rootScope.showHideLoadingImage(true,
            'paintUIMessage', $translate);
        LEDynamicUI
            .paintUI(
                rootConfig.template,
                rootConfig.illustratorUIJson,
                "illustrationMainTabs",
                "#illustrationMainTabs",
                true,
                function (x) {
                    $scope.illTabNavStyle[0] = 'activate';
                    $scope.illTabNavStyle[1] = 'disabled';
                    $scope.illTabNavStyle[2] = 'disabled';
                    var IllustrationOutputWatch;
                    var errorCountWatch;
                    $scope
                        .setIllustrationTabs($scope.selectedpage);

                    IllustrationOutputWatch = $scope
                        .$watch(
                            'Illustration.IllustrationOutput',
                            function (
                                value) {
                                $scope
                                    .initTabs();
                            });

                    errorCountWatch = $scope
                        .$watch(
                            'errorCount',
                            function (
                                value) {

                                $scope
                                    .setIllustrationTabs($scope.selectedpage);
                            });
                }, $scope, $compile);

    }
	
    //Enabling/Disabling the according to the status
    $scope.setTabNavigation = function (selectedpageIndex) {
        if (selectedpageIndex == "0") {
            if ($scope.errorCount == 0) {
                $scope.disableProduct = false;
                $scope.illTabNavStyle[1] = '';
                $scope.disableIllus = true;
                $scope.illTabNavStyle[2] = 'disabled';
            } else if ($scope.errorCount != 0) {
                $scope.disableProduct = true;
                $scope.illTabNavStyle[1] = 'disabled';
                $scope.disableIllus = true;
                $scope.illTabNavStyle[2] = 'disabled';
            }
            if (IllustratorVariables.illustrationStatus == "Confirmed" || (IllustratorVariables.illustrationStatus == "Completed" && $rootScope.haseApp == true)) {
                $scope.disableIllus = false;
                $scope.illTabNavStyle[2] = '';
            }
        } else if (selectedpageIndex == "1") {
            if (IllustratorVariables.illustrationStatus == "Confirmed" || (IllustratorVariables.illustrationStatus == "Completed" && $rootScope.haseApp == true)) {
                $scope.disableIllus = false;
                $scope.illTabNavStyle[2] = '';
            } else if (IllustratorVariables.illustrationStatus == "Draft" || IllustratorVariables.illustrationStatus == "Completed") {
                $scope.disableIllus = true;
                $scope.illTabNavStyle[2] = 'disabled';
            }
        }

    }

    $scope.saveIllustrationOnTabClick = function () {
        $scope.errorFlag=false;
        
        if($scope.titleGenderPairPayer!==undefined && $scope.titleGenderPairInsured!==undefined && $scope.Illustration.Insured.BasicDetails.gender!==undefined && $scope.Illustration.Insured.BasicDetails.gender!=="" && $scope.Illustration.Payer.BasicDetails.gender!==undefined && $scope.Illustration.Payer.BasicDetails.gender!=="" && $scope.Illustration.Insured.BasicDetails.title!==undefined && $scope.Illustration.Insured.BasicDetails.title!=="" && $scope.Illustration.Payer.BasicDetails.title!==undefined && $scope.Illustration.Payer.BasicDetails.title!=="" && $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="No"){
            if($scope.titleGenderPairInsured[0].code!=="Both" && $scope.titleGenderPairInsured!=="Both" && $scope.titleGenderPairPayer[0].code!=="Both" && $scope.titleGenderPairPayer!=="Both"){
                if($scope.titleGenderPairInsured[0].code!==$scope.Illustration.Insured.BasicDetails.gender && $scope.titleGenderPairInsured!==$scope.Illustration.Insured.BasicDetails.gender && $scope.titleGenderPairPayer[0].code!==$scope.Illustration.Payer.BasicDetails.gender && $scope.titleGenderPairPayer!==$scope.Illustration.Payer.BasicDetails.gender){
                    $scope.errorFlag=true;
                    $scope.leadDetShowPopUpMsg = true;
                    $scope.enterMandatory = translateMessages($translate, "illustrator.titleGenderMismatch");
                    
                    $rootScope.showHideLoadingImage(false);
                }
            }
        }
        if($scope.titleGenderPairInsured!==undefined && $scope.errorFlag!==true && $scope.Illustration.Insured.BasicDetails.gender!==undefined && $scope.Illustration.Insured.BasicDetails.gender!=="" && $scope.Illustration.Insured.BasicDetails.title!==undefined && $scope.Illustration.Insured.BasicDetails.title!==""){            
                if($scope.titleGenderPairInsured[0].code!=="Both" && $scope.titleGenderPairInsured!=="Both"){
                    if($scope.titleGenderPairInsured[0].code!==$scope.Illustration.Insured.BasicDetails.gender && $scope.titleGenderPairInsured!==$scope.Illustration.Insured.BasicDetails.gender){
                        $scope.errorFlag=true;
                        $scope.leadDetShowPopUpMsg = true;
                        $scope.enterMandatory = translateMessages($translate, "illustrator.titleGenderMismatchInsured");
                        
                        $rootScope.showHideLoadingImage(false);
                    }
                }
        }
        
        if($scope.titleGenderPairPayer!==undefined && $scope.errorFlag!==true && $scope.Illustration.Payer.BasicDetails.gender!==undefined && $scope.Illustration.Payer.BasicDetails.gender!=="" && $scope.Illustration.Payer.BasicDetails.title!==undefined && $scope.Illustration.Payer.BasicDetails.title!=="" & $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="No"){            
                if($scope.titleGenderPairPayer[0].code!=="Both" && $scope.titleGenderPairPayer!=="Both"){
                    if($scope.titleGenderPairPayer[0].code!==$scope.Illustration.Payer.BasicDetails.gender && $scope.titleGenderPairPayer!==$scope.Illustration.Payer.BasicDetails.gender){
                        $scope.errorFlag=true;
                        $scope.leadDetShowPopUpMsg = true;
                        $scope.enterMandatory = translateMessages($translate, "illustrator.titleGenderMismatchPayer");
                        
                        $rootScope.showHideLoadingImage(false);
                    }
                }
        }
        
        if($scope.errorFlag!==true){
        if($scope.Illustration.Insured.BasicDetails.age<16 && $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="Yes"){                                
            $scope.leadDetShowPopUpMsg = true;
            $scope.enterMandatory = translateMessages($translate, "illustrator.insuredLessThan16");
            $rootScope.showHideLoadingImage(false);     
            $scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer="No";
            $rootScope.navFlag=true;
        }else{        
        var checkMIandAiDataValidation = [],
            miDataToMIAIDatavalidation = {},
            aiDataToMIAIDatavalidation = {};
	        if($scope.Illustration.Insured.BasicDetails.occupationClassCode == undefined || $scope.Illustration.Insured.BasicDetails.occupationClassCode == ""){
	        	if($scope.Illustration.Insured.OccupationDetails[0].occupationClass != undefined){
	        		if($scope.Illustration.Insured.OccupationDetails[0].occupationClass != ""){
	        			$scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
	       		 	}
	       	 	} else {
	       	 		$scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClassValue;
	       	 	}
	        }
        
            if (!(rootConfig.isDeviceMobile)) {
                $routeParams.transactionId = 0;
            }
            if ($routeParams.illustrationId == "") {
                $routeParams.illustrationId = 0
            }
            if (IllustratorVariables.selectedpage == "Illustration Details") {
                $location.path('/illustration/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
                $scope.showHeaderImages = true;
                //$rootScope.updateErrorCount("IllustrationDetails");
            } else if (IllustratorVariables.selectedpage == "Personal Details") {
                $location.path('/Illustrator/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
                //$rootScope.updateErrorCount("PersonalDetails");
            } else {
            	
                proceedToProductDetailFromNavigation();             
            }
            $scope.refresh();
        
    }
        }
    }   
        
     var OccupationInsured=[];
    var OccupationPayer=[];
    $scope.onSaveOrTabNavToProduct=function(){
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
        } 
        
        $scope.getInsuredOccupation();
        $scope.Illustration.Insured.OccupationDetails=[];
        $scope.Illustration.Payer.OccupationDetails=[];
        for(var i=0;i<OccupationPayer.length;i++){
            if(OccupationPayer[i].id!==undefined){
            	$scope.Illustration.Payer.OccupationDetails[i]=OccupationPayer[i];
            }
        }
        for(var i=0;i<OccupationInsured.length;i++){
            if(OccupationInsured[i].id!==undefined){
            	$scope.Illustration.Insured.OccupationDetails[i]=OccupationInsured[i];
            }
        }
//        $scope.Illustration.Insured.OccupationDetails[2].slice();
//        $scope.Illustration.Insured.OccupationDetails[3].slice();
        if($scope.Illustration.Insured.OccupationDetails[2]!==undefined){
        	$scope.Illustration.Insured.OccupationDetails[2].slice();
        }
        if($scope.Illustration.Insured.OccupationDetails[3]!==undefined){
        	$scope.Illustration.Insured.OccupationDetails[3].slice();
        }
        
        if($scope.Illustration.Payer.OccupationDetails[2]!==undefined){
        	$scope.Illustration.Payer.OccupationDetails[2].slice();
        }
        if($scope.Illustration.Payer.OccupationDetails[3]!==undefined){
        	$scope.Illustration.Payer.OccupationDetails[3].slice();
        }
//        $scope.Illustration.OccupationInsured=[];
//        $scope.Illustration.OccupationPayer=[];
        if (IllustratorVariables.illustrationStatus !== "Confirmed") { 
            $scope.validateOccupationClassInsured();
            $scope.validateOccupationClassPayer();
        }
        IllustratorVariables.setIllustratorModel($scope.Illustration);
    }

    var OccupationInsured=[];
    var OccupationPayer=[];
    var OccupationInsuredDisplay=[];
    var OccupationPayerDisplay=[];
    $scope.getInsuredOccupation=function(){    
        OccupationInsured=[];
        if(!angular.equals(undefined,OccupationInsured)) {
        	OccupationInsured.slice();
        }
        for(var i=0;i<4;i++){
        	if(!angular.equals(undefined,$scope.Illustration.Insured.OccupationDetails[i])){
        		if(!angular.equals(undefined,$scope.Illustration.Insured.OccupationDetails[i].occupationCode) && $scope.Illustration.Insured.OccupationDetails[i].occupationCode!==""){
        			var OccupationDetailsArr = {};
        			OccupationDetailsArr={
                            "id":i,
							"occupationCode":$scope.Illustration.Insured.OccupationDetails[i].occupationCode,
                            "description":$scope.Illustration.Insured.OccupationDetails[i].description,
							"job":($scope.Illustration.Insured.OccupationDetails[i].job.indexOf('NA')===-1)?$scope.Illustration.Insured.OccupationDetails[i].job:"NA",
							"subJob":($scope.Illustration.Insured.OccupationDetails[i].subJob.indexOf('NA')===-1)?$scope.Illustration.Insured.OccupationDetails[i].subJob:"NA",
							"occupationClass":$scope.Illustration.Insured.OccupationDetails[i].occupationClass,
							"industry":$scope.Illustration.Insured.OccupationDetails[i].industry,
							"occupationCodeValue":$scope.Illustration.Insured.OccupationDetails[i].occupationCodeValue,
                            "descriptionValue":$scope.Illustration.Insured.OccupationDetails[i].descriptionValue,
							"jobValue":$scope.Illustration.Insured.OccupationDetails[i].jobValue,
							"subJobValue":$scope.Illustration.Insured.OccupationDetails[i].subJobValue,
							"occupationClassValue":$scope.Illustration.Insured.OccupationDetails[i].occupationClassValue,
							"industryValue":$scope.Illustration.Insured.OccupationDetails[i].industryValue,
                            "natureofWork":$scope.Illustration.Insured.OccupationDetails[i].natureofWork,
                            "natureofWorkValue":$scope.Illustration.Insured.OccupationDetails[i].natureofWorkValue
					};
                if(OccupationDetailsArr.id===3){
                    $scope.showSecondary=true;
                }
                OccupationInsured.push(OccupationDetailsArr);
        	}
        }            
     }
    $scope.Illustration.Insured.OccupationDetails[2]="";
    $scope.Illustration.Insured.OccupationDetails[3]="";
    
    if($scope.Illustration.Insured.BasicDetails.isInsuredSameAsPayer==="Yes"){
        OccupationPayer=angular.copy(OccupationInsured);
    }else{
        $scope.getPayerOccupation();
    }
    }
    
    $scope.getPayerOccupation=function(){    
        OccupationPayer=[];    
        if(!angular.equals(undefined,OccupationPayer)){
        	OccupationPayer.slice();
        }
        for(var i=0;i<4;i++){
        	if(!angular.equals(undefined,$scope.Illustration.Payer.OccupationDetails[i])){
        		if(!angular.equals(undefined,$scope.Illustration.Payer.OccupationDetails[i].occupationCode) && $scope.Illustration.Payer.OccupationDetails[i].occupationCode!==""){
        			var OccupationDetailsArr = {};
        			OccupationDetailsArr={
                            "id":i,
							"occupationCode":$scope.Illustration.Payer.OccupationDetails[i].occupationCode,
                            "description":$scope.Illustration.Payer.OccupationDetails[i].description,
							"job":($scope.Illustration.Payer.OccupationDetails[i].job.indexOf('NA')===-1)?$scope.Illustration.Payer.OccupationDetails[i].job:"NA",
							"subJob":($scope.Illustration.Payer.OccupationDetails[i].subJob.indexOf('NA')===-1)?$scope.Illustration.Payer.OccupationDetails[i].subJob:"NA",
							"occupationClass":$scope.Illustration.Payer.OccupationDetails[i].occupationClass,
							"industry":$scope.Illustration.Payer.OccupationDetails[i].industry,
							"occupationCodeValue":$scope.Illustration.Payer.OccupationDetails[i].occupationCodeValue,
                            "descriptionValue":$scope.Illustration.Payer.OccupationDetails[i].descriptionValue,
							"jobValue":$scope.Illustration.Payer.OccupationDetails[i].jobValue,
							"subJobValue":$scope.Illustration.Payer.OccupationDetails[i].subJobValue,
							"occupationClassValue":$scope.Illustration.Payer.OccupationDetails[i].occupationClassValue,
							"industryValue":$scope.Illustration.Payer.OccupationDetails[i].industryValue,
                            "natureofWork":$scope.Illustration.Payer.OccupationDetails[i].natureofWork,
                            "natureofWorkValue":$scope.Illustration.Payer.OccupationDetails[i].natureofWorkValue
					};
	                if(OccupationDetailsArr.id===3){
	                    $scope.showSecondaryPayor=true;
	                }
	                OccupationPayer.push(OccupationDetailsArr);
        		}
        	}
        }
        $scope.Illustration.Payer.OccupationDetails[2]="";
        $scope.Illustration.Payer.OccupationDetails[3]="";
    }
    
    $scope.validateOccupationClassInsured = function () {
        if ($scope.Illustration.Insured.OccupationDetails.length<2) {    
            if($scope.Illustration.Insured.OccupationDetails[0]!==undefined){
            $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
            }
        } else if($scope.Illustration.Insured.OccupationDetails[0]!==undefined && $scope.Illustration.Insured.OccupationDetails[1]!==undefined){
            if (!isNaN($scope.Illustration.Insured.OccupationDetails[0].occupationClass) && !isNaN($scope.Illustration.Insured.OccupationDetails[1].occupationClass)) {
                if ($scope.Illustration.Insured.OccupationDetails[0].occupationClass > $scope.Illustration.Insured.OccupationDetails[1].occupationClass) {
                    $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
                } else {
                    $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[1].occupationClass;
                }
            } else if (isNaN($scope.Illustration.Insured.OccupationDetails[0].occupationClass) && isNaN($scope.Illustration.Insured.OccupationDetails[1].occupationClass)) {
                if ($scope.Illustration.Insured.OccupationDetails[0].occupationClass == "D") {
                    $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
                } else if ($scope.Illustration.Insured.OccupationDetails[1].occupationClass == "D") {
                    $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[1].occupationClass;
                } else {
                    $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
                }
            } else {
                if (isNaN($scope.Illustration.Insured.OccupationDetails[0].occupationClass)) {
                    $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
                }

                if (isNaN($scope.Illustration.Insured.OccupationDetails[1].occupationClass)) {
                    $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[1].occupationClass;
                }
            }
        }         
    }
       
    $scope.validateOccupationClassPayer = function () {
        if ($scope.Illustration.Payer.OccupationDetails.length<2) { 
            if($scope.Illustration.Payer.OccupationDetails[0]!==undefined){
            $scope.Illustration.Payer.BasicDetails.occupationClassCode = $scope.Illustration.Payer.OccupationDetails[0].occupationClass;
            }
        } else if($scope.Illustration.Payer.OccupationDetails[0]!==undefined && $scope.Illustration.Payer.OccupationDetails[1]!==undefined){
            if (!isNaN($scope.Illustration.Payer.OccupationDetails[0].occupationClass) && !isNaN($scope.Illustration.Payer.OccupationDetails[1].occupationClass)) {
                if ($scope.Illustration.Payer.OccupationDetails[0].occupationClass > $scope.Illustration.Payer.OccupationDetails[1].occupationClass) {
                    $scope.Illustration.Payer.BasicDetails.occupationClassCode = $scope.Illustration.Payer.OccupationDetails[0].occupationClass;
                } else {
                    $scope.Illustration.Payer.BasicDetails.occupationClassCode = $scope.Illustration.Payer.OccupationDetails[1].occupationClass;
                }
            } else if (isNaN($scope.Illustration.Payer.OccupationDetails[0].occupationClass) && isNaN($scope.Illustration.Payer.OccupationDetails[1].occupationClass)) {
                if ($scope.Illustration.Payer.OccupationDetails[0].occupationClass == "D") {
                    $scope.Illustration.Payer.BasicDetails.occupationClassCode = $scope.Illustration.Payer.OccupationDetails[0].occupationClass;
                } else if ($scope.Illustration.Payer.OccupationDetails[1].occupationClass == "D") {
                    $scope.Illustration.Payer.BasicDetails.occupationClassCode = $scope.Illustration.Payer.OccupationDetails[1].occupationClass;
                } else {
                    $scope.Illustration.Payer.BasicDetails.occupationClassCode = $scope.Illustration.Payer.OccupationDetails[0].occupationClass;
                }
            } else {
                if (isNaN($scope.Illustration.Payer.OccupationDetails[0].occupationClass)) {
                    $scope.Illustration.Payer.BasicDetails.occupationClassCode = $scope.Illustration.Payer.OccupationDetails[0].occupationClass;
                }

                if (isNaN($scope.Illustration.Payer.OccupationDetails[1].occupationClass)) {
                    $scope.Illustration.Payer.BasicDetails.occupationClassCode = $scope.Illustration.Payer.OccupationDetails[1].occupationClass;
                }
            }
        } 
    }
    
    $scope.closePopup = function () {
            $scope.leadDetShowPopUpMsg = false;
        };
    
    // Removed from $scope
    function proceedToProductDetailFromNavigation () {
        if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===3){
            if(!($scope.Illustration.Insured.BasicDetails.age>=minAge1003 && $scope.Illustration.Insured.BasicDetails.age<=maxAge1003)){
                $rootScope.showHideLoadingImage(false, "Loading,please wait..");
                $scope.leadDetShowPopUpMsg = true;
                $scope.enterMandatory = translateMessages($translate, "illustrator.GenBumnanAgeInvalid");
            } else {
            	if($scope.Illustration.Insured.BasicDetails.occupationClassCode == undefined || $scope.Illustration.Insured.BasicDetails.occupationClassCode == ""){
                	if($scope.Illustration.Insured.OccupationDetails[0].occupationClass != undefined){
                		if($scope.Illustration.Insured.OccupationDetails[0].occupationClass != ""){
                			$scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
               		 	}
               	 	} else {
               	 		$scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClassValue;
               	 	}
                }
                $scope.onSaveOrTabNavToProduct();
                var obj = new GLI_IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
                obj.save(function () {
                    if (!(rootConfig.isDeviceMobile)) {
			            $routeParams.transactionId = 0;
			        }
			        if ($routeParams.illustrationId == "") {
			            $routeParams.illustrationId = 0
			        }
			        if (IllustratorVariables.selectedpage == "Illustration Details") {
			            $location.path('/illustration/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
			            $scope.showHeaderImages = true;
			            //$rootScope.updateErrorCount("IllustrationDetails");
			        } else if (IllustratorVariables.selectedpage == "Personal Details") {
			            $location.path('/Illustrator/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
			            //$rootScope.updateErrorCount("PersonalDetails");
			        } else{
			            $location.path('/illustratorProduct/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
			        }
              });
            }
        }
            
        if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===97){
            if(!($scope.Illustration.Insured.BasicDetails.age>=minAge1097 && $scope.Illustration.Insured.BasicDetails.age<=maxAge1097)){
                $rootScope.showHideLoadingImage(false, "Loading,please wait..");
                $scope.leadDetShowPopUpMsg = true;
                $scope.enterMandatory = translateMessages($translate, "illustrator.CancerCareAgeInvalid");
            }else{
                //if (!(rootConfig.isDeviceMobile)) {
                if($scope.Illustration.Insured.BasicDetails.occupationClassCode == undefined || $scope.Illustration.Insured.BasicDetails.occupationClassCode == ""){
                    if($scope.Illustration.Insured.OccupationDetails[0].occupationClass != undefined){
                        if($scope.Illustration.Insured.OccupationDetails[0].occupationClass != ""){
                            $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
                        }
                    } else {
                        $scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClassValue;
                    }
                }
                //}
                $scope.onSaveOrTabNavToProduct();
                var obj = new GLI_IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
                obj.save(function () {
                    if (!(rootConfig.isDeviceMobile)) {
			            $routeParams.transactionId = 0;
			        }
			        if ($routeParams.illustrationId == "") {
			            $routeParams.illustrationId = 0
			        }
			        if (IllustratorVariables.selectedpage == "Illustration Details") {
			            $location.path('/illustration/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
			            $scope.showHeaderImages = true;
			            //$rootScope.updateErrorCount("IllustrationDetails");
			        } else if (IllustratorVariables.selectedpage == "Personal Details") {
			            $location.path('/Illustrator/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
			            //$rootScope.updateErrorCount("PersonalDetails");
			        } else{
			            $location.path('/illustratorProduct/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
			        }
               });
            }
        }
        /*
        if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===10){
            if($scope.Illustration.Insured.BasicDetails.age>=0 && $scope.Illustration.Insured.BasicDetails.age<=maxAge1010){
                var ageErrIndicator=false;
                if(parseInt($scope.Illustration.Insured.BasicDetails.age)===0){
                    var dobDate = new Date($scope.Illustration.Insured.BasicDetails.dob);
                    var todayDate = new Date();
                    var ageInDays=getNumberOfDays($scope.Illustration.Insured.BasicDetails.dob);                        
                    var ageInMonths = todayDate.getMonth()-dobDate.getMonth();
                    var ageInYear= todayDate.getFullYear()-dobDate.getFullYear();
                    
                    if(ageInDays<29){
                        ageErrIndicator=true;
                        $rootScope.showHideLoadingImage(false, "Loading,please wait..");
                        $scope.leadDetShowPopUpMsg = true;
                        $scope.enterMandatory = translateMessages($translate, "illustrator.CompleteHealthAgeInvalid");
                    }                        
                }
                if(!ageErrIndicator){
                	if($scope.Illustration.Insured.BasicDetails.occupationClassCode == undefined || $scope.Illustration.Insured.BasicDetails.occupationClassCode == ""){
                    	if($scope.Illustration.Insured.OccupationDetails[0].occupationClass != undefined){
                    		if($scope.Illustration.Insured.OccupationDetails[0].occupationClass != ""){
                    			$scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
                   		 	}
                   	 	} else {
                   	 		$scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClassValue;
                   	 	}
                    }
                    $scope.onSaveOrTabNavToProduct();
                    var obj = new GLI_IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
	                obj.save(function () {
	                    if (!(rootConfig.isDeviceMobile)) {
				            $routeParams.transactionId = 0;
				        }
				        if ($routeParams.illustrationId == "") {
				            $routeParams.illustrationId = 0
				        }
				        if (IllustratorVariables.selectedpage == "Illustration Details") {
				            $location.path('/illustration/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
				            $scope.showHeaderImages = true;
				            //$rootScope.updateErrorCount("IllustrationDetails");
				        } else if (IllustratorVariables.selectedpage == "Personal Details") {
				            $location.path('/Illustrator/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
				            //$rootScope.updateErrorCount("PersonalDetails");
				        } else{
				            $location.path('/illustratorProduct/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
				        }
				    });
                }
            } else{
                $rootScope.showHideLoadingImage(false, "Loading,please wait..");
                $scope.leadDetShowPopUpMsg = true;
                $scope.enterMandatory = translateMessages($translate, "illustrator.CompleteHealthAgeInvalid");
            }               
        }*/
        
        if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===10){
            if(!($scope.Illustration.Insured.BasicDetails.age>minAge1010 && $scope.Illustration.Insured.BasicDetails.age<=maxAge1010)){
                $rootScope.showHideLoadingImage(false, "Loading,please wait..");
                $scope.leadDetShowPopUpMsg = true;
                $scope.enterMandatory = translateMessages($translate, "illustrator.CompleteHealthAgeInvalid");
            } else {
            	if($scope.Illustration.Insured.BasicDetails.occupationClassCode == undefined || $scope.Illustration.Insured.BasicDetails.occupationClassCode == ""){
                	if($scope.Illustration.Insured.OccupationDetails[0].occupationClass != undefined){
                		if($scope.Illustration.Insured.OccupationDetails[0].occupationClass != ""){
                			$scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
               		 	}
               	 	} else {
               	 		$scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClassValue;
               	 	}
                }
                $scope.onSaveOrTabNavToProduct();
                var obj = new GLI_IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
                obj.save(function () {
                    if (!(rootConfig.isDeviceMobile)) {
			            $routeParams.transactionId = 0;
			        }
			        if ($routeParams.illustrationId == "") {
			            $routeParams.illustrationId = 0
			        }
			        if (IllustratorVariables.selectedpage == "Illustration Details") {
			            $location.path('/illustration/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
			            $scope.showHeaderImages = true;
			        } else if (IllustratorVariables.selectedpage == "Personal Details") {
			            $location.path('/Illustrator/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
			        } else{
			            $location.path('/illustratorProduct/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
			        }
              });
            }
        }
            
	    if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===6){
	        if($scope.Illustration.Insured.BasicDetails.age>=0 && $scope.Illustration.Insured.BasicDetails.age<=maxAge1006){
	            var ageErrIndicator=false;
	            if(parseInt($scope.Illustration.Insured.BasicDetails.age)===0){
	                var dobDate = new Date($scope.Illustration.Insured.BasicDetails.dob);
	                var todayDate = new Date();
	                var ageInDays=getNumberOfDays($scope.Illustration.Insured.BasicDetails.dob);                        
	                var ageInMonths = todayDate.getMonth()-dobDate.getMonth();
	                var ageInYear= todayDate.getFullYear()-dobDate.getFullYear();
	                
	                if(ageInDays<29){
	                    ageErrIndicator=true;
	                    $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                    $scope.leadDetShowPopUpMsg = true;
	                    $scope.enterMandatory = translateMessages($translate, "illustrator.GenSave20PlusAgeInvalid");
	                }                        
	            }
	            if(!ageErrIndicator){
	                $scope.onSaveOrTabNavToProduct();
	                var obj = new GLI_IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
		            obj.save(function () {
		                if (!(rootConfig.isDeviceMobile)) {
					        $routeParams.transactionId = 0;
					    }
					    if ($routeParams.illustrationId == "") {
					        $routeParams.illustrationId = 0
					    }
					    if (IllustratorVariables.selectedpage == "Illustration Details") {
					        $location.path('/illustration/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
					        $scope.showHeaderImages = true;
					        //$rootScope.updateErrorCount("IllustrationDetails");
					    } else if (IllustratorVariables.selectedpage == "Personal Details") {
					        $location.path('/Illustrator/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
					        //$rootScope.updateErrorCount("PersonalDetails");
					    } else{
					        $location.path('/illustratorProduct/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
					    }
		          });
	            }
	        } else {
	            $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	            $scope.leadDetShowPopUpMsg = true;
	            $scope.enterMandatory = translateMessages($translate, "illustrator.GenSave20PlusAgeInvalid");
	        }               
	    } 
            
        if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===4){
            if($scope.Illustration.Insured.BasicDetails.age>=0 && $scope.Illustration.Insured.BasicDetails.age<=maxAge1004){
                var ageErrIndicator=false;
                if(parseInt($scope.Illustration.Insured.BasicDetails.age)===0){
                    var dobDate = new Date($scope.Illustration.Insured.BasicDetails.dob);
                    var todayDate = new Date();
                    var ageInDays=getNumberOfDays($scope.Illustration.Insured.BasicDetails.dob);                        
                    var ageInMonths = todayDate.getMonth()-dobDate.getMonth();
                    var ageInYear= todayDate.getFullYear()-dobDate.getFullYear();
                    
                    if(ageInDays<29){
                        ageErrIndicator=true;
                        $rootScope.showHideLoadingImage(false, "Loading,please wait..");
                        $scope.leadDetShowPopUpMsg = true;
                        $scope.enterMandatory = translateMessages($translate, "illustrator.GenProLifeAgeInvalid");
                    }                        
                }
                
                if(!ageErrIndicator){
                    $scope.onSaveOrTabNavToProduct();
                    var obj = new GLI_IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
                    obj.save(function () {
	                    if (!(rootConfig.isDeviceMobile)) {
				            $routeParams.transactionId = 0;
				        }
				        if ($routeParams.illustrationId == "") {
				            $routeParams.illustrationId = 0
				        }
				        if (IllustratorVariables.selectedpage == "Illustration Details") {
				            $location.path('/illustration/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
				            $scope.showHeaderImages = true;
				            //$rootScope.updateErrorCount("IllustrationDetails");
				        } else if (IllustratorVariables.selectedpage == "Personal Details") {
				            $location.path('/Illustrator/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
				            //$rootScope.updateErrorCount("PersonalDetails");
				        } else{
				            $location.path('/illustratorProduct/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
				        }
                    });
                }
            } else {
                $rootScope.showHideLoadingImage(false, "Loading,please wait..");
                $scope.leadDetShowPopUpMsg = true;
                $scope.enterMandatory = translateMessages($translate, "illustrator.GenProLifeAgeInvalid");
            }               
        }
            
        /*
         * Age Validation For Whole Life Product
         */
        if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===220){
            if(!($scope.Illustration.Insured.BasicDetails.age >= minAge1220 && $scope.Illustration.Insured.BasicDetails.age <= maxAge1220)){
                $rootScope.showHideLoadingImage(false, "Loading,please wait..");
                $scope.leadDetShowPopUpMsg = true;
                $scope.enterMandatory = translateMessages($translate, "illustrator.WholelifeAgeInvalid");
            } else {
            	if($scope.Illustration.Insured.BasicDetails.occupationClassCode == undefined || $scope.Illustration.Insured.BasicDetails.occupationClassCode == ""){
                	if($scope.Illustration.Insured.OccupationDetails[0].occupationClass != undefined){
                		if($scope.Illustration.Insured.OccupationDetails[0].occupationClass != ""){
                			$scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClass;
               		 	}
               	 	} else {
               	 		$scope.Illustration.Insured.BasicDetails.occupationClassCode = $scope.Illustration.Insured.OccupationDetails[0].occupationClassValue;
               	 	}
                }
                $scope.onSaveOrTabNavToProduct();
                var obj = new GLI_IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
                obj.save(function () {
                    if (!(rootConfig.isDeviceMobile)) {
                    	$routeParams.transactionId = 0;
		            }
		            if ($routeParams.illustrationId == "") {
		                $routeParams.illustrationId = 0
		            }
		            if (IllustratorVariables.selectedpage == "Illustration Details") {
		                $location.path('/illustration/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
		                $scope.showHeaderImages = true;
		                //$rootScope.updateErrorCount("IllustrationDetails");
		            } else if (IllustratorVariables.selectedpage == "Personal Details") {
		                $location.path('/Illustrator/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
		                //$rootScope.updateErrorCount("PersonalDetails");
		            } else{
		                $location.path('/illustratorProduct/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
		            }
               });
            }
        }
        
        /*
         * Age Validation For GenSave 10 Plus Product
        */
        
        if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===306){
	        if($scope.Illustration.Insured.BasicDetails.age>=0 && $scope.Illustration.Insured.BasicDetails.age<=maxAge1306){
	            var ageErrIndicator=false;
	            if(parseInt($scope.Illustration.Insured.BasicDetails.age)===0){
	                var dobDate = new Date($scope.Illustration.Insured.BasicDetails.dob);
	                var todayDate = new Date();
	                var ageInDays=getNumberOfDays($scope.Illustration.Insured.BasicDetails.dob);                        
	                var ageInMonths = todayDate.getMonth()-dobDate.getMonth();
	                var ageInYear= todayDate.getFullYear()-dobDate.getFullYear();
	                if(ageInDays<29){
	                    ageErrIndicator=true;
	                    $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                    $scope.leadDetShowPopUpMsg = true;
	                    $scope.enterMandatory = translateMessages($translate, "illustrator.GenSave10PlusAgeInvalid");
	                }                        
	            }
	            if(!ageErrIndicator){
	                $scope.onSaveOrTabNavToProduct();
	                var obj = new GLI_IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
		            obj.save(function () {
		                if (!(rootConfig.isDeviceMobile)) {
					        $routeParams.transactionId = 0;
					    }
					    if ($routeParams.illustrationId == "") {
					        $routeParams.illustrationId = 0
					    }
					    if (IllustratorVariables.selectedpage == "Illustration Details") {
					        $location.path('/illustration/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
					        $scope.showHeaderImages = true;
					        //$rootScope.updateErrorCount("IllustrationDetails");
					    } else if (IllustratorVariables.selectedpage == "Personal Details") {
					        $location.path('/Illustrator/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
					        //$rootScope.updateErrorCount("PersonalDetails");
					    } else{
					        $location.path('/illustratorProduct/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
					    }
		          });
	            }
	        } else {
	            $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	            $scope.leadDetShowPopUpMsg = true;
	            $scope.enterMandatory = translateMessages($translate, "illustrator.GenSave10PlusAgeInvalid");
	        }               
	    } 
        
        /*
         * Age Validation For GenSave 4 Plus Product
        */
        
        if(parseInt($scope.Illustration.Product.ProductDetails.productCode)===274){
	        if($scope.Illustration.Insured.BasicDetails.age>=0 && $scope.Illustration.Insured.BasicDetails.age<=maxAge1274){
	            var ageErrIndicator=false;
	            if(parseInt($scope.Illustration.Insured.BasicDetails.age)===0){
	                var dobDate = new Date($scope.Illustration.Insured.BasicDetails.dob);
	                var todayDate = new Date();
	                var ageInDays=getNumberOfDays($scope.Illustration.Insured.BasicDetails.dob);                        
	                var ageInMonths = todayDate.getMonth()-dobDate.getMonth();
	                var ageInYear= todayDate.getFullYear()-dobDate.getFullYear();
	                if(ageInDays<29){
	                    ageErrIndicator=true;
	                    $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	                    $scope.leadDetShowPopUpMsg = true;
	                    $scope.enterMandatory = translateMessages($translate, "illustrator.GenSave4PlusAgeInvalid");
	                }                        
	            }
	            if(!ageErrIndicator){
	                $scope.onSaveOrTabNavToProduct();
	                var obj = new GLI_IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
		            obj.save(function () {
		                if (!(rootConfig.isDeviceMobile)) {
					        $routeParams.transactionId = 0;
					    }
					    if ($routeParams.illustrationId == "") {
					        $routeParams.illustrationId = 0
					    }
					    if (IllustratorVariables.selectedpage == "Illustration Details") {
					        $location.path('/illustration/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
					        $scope.showHeaderImages = true;
					        //$rootScope.updateErrorCount("IllustrationDetails");
					    } else if (IllustratorVariables.selectedpage == "Personal Details") {
					        $location.path('/Illustrator/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
					        //$rootScope.updateErrorCount("PersonalDetails");
					    } else{
					        $location.path('/illustratorProduct/' + $routeParams.productId + '/' + $routeParams.proposalId + '/' + $routeParams.illustrationId + '/' + $routeParams.transactionId);
					    }
		          });
	            }
	        } else {
	            $rootScope.showHideLoadingImage(false, "Loading,please wait..");
	            $scope.leadDetShowPopUpMsg = true;
	            $scope.enterMandatory = translateMessages($translate, "illustrator.GenSave4PlusAgeInvalid");
	        }               
	    } 
        
        $scope.$parent.disablePersonalDetailsButtons = false;
		$scope.$parent.disableProductDetailsButtons = false;
		$scope.$parent.disableIllustrationDetailsButtons = false;         
    }

    if (typeof IllustrationOutputWatch != "undefined") {
        $scope.$on("$destroy", function () {
            if (this != null && typeof this !== 'undefined') {
                if (this.$$destroyed) return;
                while (this.$$childHead) {
                    this.$$childHead.$destroy();
                }
                if (this.$broadcast) {
                    this.$broadcast('$destroy');
                }
                this.$$destroyed = true;
                //$timeout.cancel( timer );
                IllustrationOutputWatch();
                errorCountWatch();
            }
        });
    }

} // Controller ends here