﻿﻿﻿/*
 * Copyright 2015, LifeEngage 
 */
﻿/*
	 * Name:PersonalDetailsController CreatedDate:4/14/2013
	 * Description:illustrator Personal info capturing controller.
	 */

storeApp.controller('PersonalDetailsController', ['$rootScope', '$scope', '$compile', '$routeParams', '$location', 'ProductService', 'LookupService', 'UtilityService', 'DataService', 'DocumentService', '$translate', 'dateFilter', 'IllustratorVariables', 'IllustratorService', 'PersistenceMapping', '$debounce', 'AutoSave', 'FnaVariables', 'globalService','GLI_EvaluateService',
                                                  function($rootScope, $scope, $compile, $routeParams, $location, ProductService, LookupService, UtilityService, DataService, DocumentService, $translate, dateFilter, IllustratorVariables, IllustratorService, PersistenceMapping, $debounce, AutoSave, FnaVariables, globalService,GLI_EvaluateService) {

                                                  	$rootScope.moduleHeader = "illustrator.Illustrator";
                                                  	$rootScope.moduleVariable = translateMessages($translate, $rootScope.moduleHeader);

                                                  	$scope.Illustration = IllustratorVariables.getIllustratorModel();
                                                  	$scope.IllustrationAttachment = IllustratorVariables.getIllustrationAttachmentModel();

                                                  
                                                  	
                                                  	IllustratorVariables.setIllustratorModel($scope.Illustration);

                                                  	$scope.showErrorCount = true;
                                                  	IllustratorService.initializeIllustratorScopeVariables($scope, $rootScope, $routeParams, IllustratorVariables);
                                                  	UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);

                                                  	$scope.selectedpage = "Personal Details";
                                                  	// IllustratorVariables.illustratorId =
                                                  	// ($routeParams.illustrationId &&
                                                  	// $routeParams.illustrationId != 0) ?
                                                  	// $routeParams.illustrationId :
                                                  	// IllustratorVariables.illustratorId;
                                                  	// $scope.productId = $routeParams.productId;
                                                  	// $rootScope.transactionId =
                                                  	// $routeParams.transactionId;
                                                  	var model = "Illustration";
                                                  	if ((rootConfig.autoSave.illustration)) {
                                                  		AutoSave.setupWatchForScope(model, DataService, UtilityService, IllustratorService, $scope, $debounce, $translate, $routeParams, "");
                                                  	}
                                                  	
                                                  	/*
                                                  	 * if (!Modernizr.inputtypes['date']) {
                                                  	 * 
                                                  	 * $('.custdatepicker').datepicker() .on('changeDate', function () {
                                                  	 * $('.custdatepicker').datepicker('hide'); }); }
                                                  	 */
                                                  	

                                                  	// Country-State-City dropdown initialization and
                                                  	// keeping in to global array
                                                  	// filterItems
                                                  	$scope.init = function(model, dependent, control) {
                                                  		if (dependent != "none")
                                                  			filterItems.push(new countryFilter(model, dependent));
                                                  	}

                                                  	$scope.enableMyIllustration = function() {
                                                  		// $rootScope.enableMyIllustrationTab = true;
                                                  		$location.path("/MyAccount/Illustration/0/0");
                                                  		$scope.refresh();
                                                  	}
                                                  	// After downloading, set the document object from
                                                  	// LifeEngage to EappAttachment. Delete document
                                                  	// metadata contents from LifeEngage scope object
                                                  	$scope.onGetServerDocSuccess = function() {
                                                  		if ($scope.Illustration.Upload && $scope.Illustration.Upload.Signature && !angular.equals($scope.Illustration.Upload.Signature[0], {})) {
                                                  			var tempPhoto = angular.copy($scope.Illustration.Upload.Signature[0]);
                                                  			if (tempPhoto && tempPhoto.documentName != "") {
                                                  				var docDetails = {
                                                  					Documents : {
                                                  						documentName : tempPhoto.documentName
                                                  					}
                                                  				};
                                                  				PersistenceMapping.clearTransactionKeys();
                                                  				IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
                                                  				var docreq1 = PersistenceMapping.mapScopeToPersistence(docDetails);
                                                  				var arrayIndex = 0;
                                                  				DocumentService.startDownloadForEachTransaction(arrayIndex, docreq1, function(docDetails) {
                                                  					var page1 = docDetails;
                                                  					$scope.Illustration.Upload.Signature[0] = page1;
                                                  					$scope.Illustration.Upload.Signature[0].pages[0].base64string = page1.pages[0].base64string.replace(/\\/g, '');
                                                  					$scope.IllustrationAttachment.Upload.Signature = $scope.Illustration.Upload.Signature;
                                                  					// To set the doc
                                                  					// status so as to
                                                  					// to avoid
                                                  					// unnecessary
                                                  					// uploaddoc service
                                                  					// call
                                                  					$scope.IllustrationAttachment.Upload.Signature[0].documentStatus = "Synced";
                                                  					IllustratorVariables.setIllustrationAttachmentModel($scope.IllustrationAttachment);
                                                  					delete $scope.Illustration.Upload;
                                                  				});
                                                  			}

                                                  		}
                                                  	}
                                                  	$scope.mapKeysfromPersistence = function() {
                                                  		$rootScope.customerId = PersistenceMapping.Key1;
                                                  		$rootScope.agentId = PersistenceMapping.Key11;
                                                  		IllustratorVariables.illustrationCreationDate = PersistenceMapping.Key13;
                                                  		// $scope.status =PersistenceMapping.Key15 ;
                                                  		$scope.Illustration = PersistenceMapping.transactionData;
                                                  		IllustratorVariables.transTrackingID = PersistenceMapping.TransTrackingID;
                                                  		// After Sync From Server, Country and State
                                                  		// values are coming as String, but Number is
                                                  		// expected in the CustomDDL
                                                  		if ((rootConfig.isDeviceMobile)) {
                                                  			$scope.Illustration.Insured.BasicDetails.countryofResidence = $scope.Illustration.Insured.BasicDetails.countryofResidence;
                                                  			$scope.Illustration.Insured.BasicDetails.state = $scope.Illustration.Insured.BasicDetails.state;
                                                  			$scope.Illustration.Payer.BasicDetails.countryofResidence = $scope.Illustration.Payer.BasicDetails.countryofResidence;
                                                  			$scope.Illustration.Payer.BasicDetails.state = $scope.Illustration.Payer.BasicDetails.state;
                                                  			DataService.getDocumentsForTransaction($rootScope.transactionId, function(docData) {
                                                  				if (docData.length != 0) {
                                                  					var Signature = [];
                                                  					// Can
                                                  					// have
                                                  					// more
                                                  					// than
                                                  					// one
                                                  					// Signature
                                                  					for (var i = 0; i < docData.length; i++) {
                                                  						if (docData[i].DocumentType == 'Signature') {
                                                  							Signature.push(JSON.parse(docData[i].DocumentObject));
                                                  						}
                                                  					}
                                                  					if (Signature.length > 0) {
                                                  						$scope.IllustrationAttachment.Upload.Signature = Signature;
                                                  						IllustratorVariables.setIllustrationAttachmentModel($scope.IllustrationAttachment);
                                                  						Signature = [];
                                                  					}
                                                  				}
                                                  			}, function(error) {// errorCallback(error);
                                                  			});

                                                  		} else {
                                                  			$scope.onGetServerDocSuccess();
                                                  		}
                                                  		/*
                                                  		 * if ($scope.Illustration.Product.withdrawalDetails.length == 0) {
                                                  		 * $scope.Illustration.Product.withdrawalDetails = []; var
                                                  		 * withdrawalInformation = new Object();
                                                  		 * withdrawalInformation.withdrawalAmount = "";
                                                  		 * withdrawalInformation.withdrawalTerm = "";
                                                  		 * withdrawalInformation.withdrawalTermBegin = "";
                                                  		 * withdrawalInformation.withdrawalTermEnd = "";
                                                  		 * withdrawalInformation.withdrawalFrequency = "";
                                                  		 * withdrawalInformation.withdrawalType = "";
                                                  		 * $scope.Illustration.Product.withdrawalDetails.push(withdrawalInformation); }
                                                  		 */
                                                  		$scope.refresh();
                                                  	}

                                                  	$scope.onPaintUISuccess = function(callBackId) {
                                                  		if (!((rootConfig.isDeviceMobile))) {
                                                  			$('.custdatepicker').datepicker({
                                                  				format : 'yyyy-mm-dd',
                                                  				endDate : '+0d',
                                                  				autoclose : true
                                                  			}).on('change', function() {
                                                  				/*
                                                  				 * var elem = $(this).attr("ng-model"); val = $(this).val();
                                                  				 * eval ("$scope."+elem+"=val"); $scope.updateAge($(this).val(),
                                                  				 * $(this).attr("id")); if
                                                  				 * ($scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured ==
                                                  				 * 'Yes') { $scope.Illustration.Insured.BasicDetails.dob =
                                                  				 * $scope.Illustration.Payer.BasicDetails.dob;
                                                  				 * $scope.updateAge($scope.Illustration.Payer.BasicDetails.dob,
                                                  				 * 'insuredDOB'); } //$('.custdatepicker').datepicker('hide');
                                                  				 */
                                                  				$scope.refresh();
                                                  			});
                                                  		}
                                                  		$rootScope.updateErrorCount($scope.viewToBeCustomized);
                                                  		$scope.refresh();
                                                  	}
                                                  	var transactionObj = {
                                                  		"CheckDate" : new Date().getMonth() + 1 + "-" + new Date().getDate() + "-" + new Date().getFullYear(),
                                                  		"Products" : {
                                                  			"id" : $routeParams.productId,
                                                  			"code" : "",
                                                  			"carrierCode" : 1
                                                  		}
                                                  	}
                                                  	$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
                                                  	$scope.refresh();

                                                  	/*
                                                  	 * For splicing the personalDetailsUITemplate with respect to the
                                                  	 * spliceItemID .
                                                  	 */
                                                  	$scope.customizeUIJsonPersonal = function(personalDetailsUITemplate, spliceItemID) { customizeUIJsonPersonalLoop:
                                                  		for ( i = 0; i < personalDetailsUITemplate.Views.length; i++) {
                                                  			if (personalDetailsUITemplate.Views[i].id == $scope.viewToBeCustomized) {
                                                  				for ( j = 0; j < personalDetailsUITemplate.Views[i].sections.length; j++) {
                                                  					if (personalDetailsUITemplate.Views[i].sections[j].spliceId == spliceItemID) {
                                                  						personalDetailsUITemplate.Views[i].sections.splice(j, 1);
                                                  						break customizeUIJsonPersonalLoop;
                                                  					}
                                                  					for ( k = 0; k < personalDetailsUITemplate.Views[i].sections[j].subsections.length; k++) {
                                                  						if (personalDetailsUITemplate.Views[i].sections[j].subsections[k].spliceId == spliceItemID) {
                                                  							personalDetailsUITemplate.Views[i].sections[j].subsections.splice(k, 1);
                                                  							break customizeUIJsonPersonalLoop;
                                                  						}
                                                  						for ( l = 0; l < personalDetailsUITemplate.Views[i].sections[j].subsections[k].controlGroups.length; l++) {
                                                  							if (personalDetailsUITemplate.Views[i].sections[j].subsections[k].controlGroups[l].spliceId == spliceItemID) {
                                                  								personalDetailsUITemplate.Views[i].sections[j].subsections[k].controlGroups.splice(l, 1);
                                                  								break customizeUIJsonPersonalLoop;
                                                  							}
                                                  							for ( m = 0; m < personalDetailsUITemplate.Views[i].sections[j].subsections[k].controlGroups[l].controls.length; m++) {
                                                  								if (personalDetailsUITemplate.Views[i].sections[j].subsections[k].controlGroups[l].controls[m].spliceId == spliceItemID) {
                                                  									personalDetailsUITemplate.Views[i].sections[j].subsections[k].controlGroups[l].controls.splice(m, 1);
                                                  									break customizeUIJsonPersonalLoop;
                                                  								}

                                                  							}

                                                  						}
                                                  					}
                                                  				}
                                                  			}
                                                  		}
                                                  		return personalDetailsUITemplate;
                                                  	}
                                                  	/*
                                                  	 * Modifying viewId to make it unique. According to new LEDynamic concept,
                                                  	 * view once painted will be stored to local storage. Since product
                                                  	 * dependant UI pages are present in Illustrator, we need viewId to be
                                                  	 * unique, else it will show the view which is already there in
                                                  	 * localstorage.
                                                  	 * 
                                                  	 * Note: Key for localstorage is a combination of viewId and div element.
                                                  	 * Hence the need for keeping viewId unique.
                                                  	 */
                                                  	$scope.changeViewId = function(personalDetailsUITemplate) { changeViewIdLoop:
                                                  		for ( i = 0; i < personalDetailsUITemplate.Views.length; i++) {
                                                  			if (personalDetailsUITemplate.Views[i].id == 'PersonalDetails') {
                                                  				personalDetailsUITemplate.Views[i].id = 'PersonalDetails_' + IllustratorVariables.productDetails.id;
                                                  				$scope.viewToBeCustomized = personalDetailsUITemplate.Views[i].id;
                                                  				break changeViewIdLoop;
                                                  			}
                                                  		}
                                                  		return personalDetailsUITemplate;
                                                  	}

                                                  	$scope.customizeForPersonalDetails = function(personalDetailsJSON, personalDetailsUITemplate) {

                                                  		if ( typeof personalDetailsJSON.payorName == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'payorName')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.payorDOB == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'payorDOB')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.payorGender == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'payorGender')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.payorCountry == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'payorCountry')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.payorState == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'payorState')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.payorZipCode == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'payorZipCode')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.payerOccupationCategory == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'payerOccupationCategory')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.insuredSameAsOwner == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'insuredSameAsOwner')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.insuredName == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'insuredName')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.insuredGender == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'insuredGender')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.insuredDOB == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'insuredDOB')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.insuredCountry == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'insuredCountry')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.insuredState == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'insuredState')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.insuredOccupationCategory == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'insuredOccupationCategory')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.insuredVestingAge == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'insuredVestingAge')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.smokingClass == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'rateSmokingClass')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.drinkingClass == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'rateDrinkingClass')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.smokingClass == 'undefined' && typeof personalDetailsJSON.drinkingClass == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'riskInformation')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.annuityOption == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'annuityOption')
                                                  		} else {
                                                  			if (IllustratorVariables.productDetails.properties.annuityOption instanceof Array && (IllustratorVariables.productDetails.properties.annuityOption).length > 0) {
                                                  				$scope.annuityOptions = {};
                                                  				var options = [];
                                                  				for ( i = 0; i < IllustratorVariables.productDetails.properties.annuityOption.length; i++) {
                                                  					var annuityOptionObj = {
                                                  						"annuityOptionValue" : IllustratorVariables.productDetails.properties.annuityOption[i],
                                                  						"annuityOptionTranslate" : translateMessages($translate, IllustratorVariables.productDetails.properties.annuityOption[i])
                                                  					};
                                                  					options.push(annuityOptionObj);
                                                  				}
                                                  				$scope.annuityOptions.Options = options;
                                                  			} else if (IllustratorVariables.productDetails.properties.annuityOption instanceof Array && (IllustratorVariables.productDetails.properties.annuityOption).length == 0) {
                                                  				personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'annuityInformation');
                                                  			}
                                                  		}
                                                  		if ( typeof personalDetailsJSON.secondAnnuitantName == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'secondAnnuitantName')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.secondAnnuitantDOB == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'secondAnnuitantDOB')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.secondAnnuitantGender == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'secondAnnuitantGender')
                                                  		}
                                                  		if ( typeof personalDetailsJSON.annuityInformation == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'annuityInformation');
                                                  		}
                                                  		if ( typeof personalDetailsJSON.childDOB == 'undefined') {
                                                  			personalDetailsUITemplate = $scope.customizeUIJsonPersonal(personalDetailsUITemplate, 'childDOB');
                                                  		}
                                                  		return personalDetailsUITemplate;
                                                  	}
                                                  	/*
                                                  	 * Get Illustration details Succeess
                                                  	 */
                                                  	$scope.onGetListingsSuccess = function(data) {
                                                  		$scope.Illustration = IllustratorVariables.getIllustratorModel();
                                                  		if (data[0].TransactionData.Product.ProductDetails.productCode != $scope.Illustration.Product.ProductDetails.productCode) {
                                                  			data[0].TransactionData.IllustrationOutput = {};
                                                  		}
                                                  		data[0].TransactionData.Product.ProductDetails.productCode = $scope.Illustration.Product.ProductDetails.productCode;
                                                  		data[0].TransactionData.Product.ProductDetails.productName = $scope.Illustration.Product.ProductDetails.productName;
                                                  		data[0].TransactionData.Product.templates.illustrationPdfTemplate=$scope.Illustration.Product.templates.illustrationPdfTemplate;
                                                  		IllustratorVariables.setIllustratorModel(data[0].TransactionData);
                                                  		IllustratorVariables.illustratorId = data[0].Key3;
                                                  		IllustratorVariables.fnaId = data[0].Key2;
                                                  		IllustratorVariables.illustratorKeys.Key12 = data[0].Key12;
                                                  		IllustratorVariables.transTrackingID = data[0].TransTrackingID;
                                                  		// $scope.status =data[0].Key15;
                                                  		PersistenceMapping.mapPersistenceToScope(data);
                                                  		$scope.mapKeysfromPersistence();
                                                  		stateOrCity = true;
//                                                  		$scope.LoadLookUpItems();
                                                  		$rootScope.updateErrorCount($scope.viewToBeCustomized);
                                                  		$rootScope.showHideLoadingImage(false);
                                                  		$scope.refresh();

                                                  	}
                                                  	/*
                                                  	 * Get Illustration details Error
                                                  	 */
                                                  	$scope.onGetListingsError = function() {
                                                  		$rootScope.NotifyMessages(true, resources.onGetListingsError);
                                                  		$rootScope.showHideLoadingImage(false);
                                                  		$scope.refresh();
                                                  	}

                                                  	$scope.loadIllustration = function() {
                                                  		if ($scope.proposalId != 0 && typeof $scope.proposalId !== "undefined") {
                                                  			PersistenceMapping.clearTransactionKeys();
                                                  			IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
                                                  			var transactionObj = PersistenceMapping.mapScopeToPersistence({});

                                                  			DataService.getListingDetail(transactionObj, $scope.onGetListingsSuccess, $scope.onGetListingsError);
                                                  		} else if (IllustratorVariables.illustratorId != 0 && typeof IllustratorVariables.illustratorId !== "undefined") {
                                                  			PersistenceMapping.clearTransactionKeys();
                                                  			IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
                                                  			var transactionObj = PersistenceMapping.mapScopeToPersistence({});
                                                  			DataService.getListingDetail(transactionObj, $scope.onGetListingsSuccess, $scope.onGetListingsError);
                                                  		} else if ($rootScope.transactionId != 0 && typeof $rootScope.transactionId !== "undefined") {
                                                  			PersistenceMapping.clearTransactionKeys();
                                                  			IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
                                                  			var transactionObj = PersistenceMapping.mapScopeToPersistence({});
                                                  			DataService.getListingDetail(transactionObj, $scope.onGetListingsSuccess, $scope.onGetListingsError);
                                                  		} else {
                                                  			UtilityService.InitializeErrorDisplay($scope, $rootScope, $translate);
                                                  			$rootScope.showHideLoadingImage(false);
                                                  			$scope.refresh();
                                                  		}

                                                  	}

                                                  	$scope.initialLoad = function(){
                                                  		 if(UtilityService.previousPage == 'FNA'  || UtilityService.previousPage == 'LMS') {
                                                           	$scope.Illustration.Insured = globalService.getInsured();
                                                           	$scope.Illustration.Payer = globalService.getPayer();
                                                           	$scope.Illustration.Beneficiaries = globalService.getSelectdBeneficiaries();
                                                           }
                                                            UtilityService.previousPage = 'IllustrationPersonal';
                                                  		ProductService.getProduct(transactionObj, function(product) {
                                                      		$scope.Illustration.Product.ProductDetails.productCode = product.id;
                                                      		$scope.Illustration.Product.ProductDetails.productName = product.name;
                                                      		$scope.Illustration.Product.ProductDetails.productType = product.productFamily;
                                                      		$scope.Illustration.Product.ProductDetails.productGroup = product.productGroup;
                                                      		IllustratorVariables.productDetails = JSON.parse(JSON.stringify(product));
                                                      		$rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
                                                      		$scope.refresh();
                                                      		
                                                      		var personalDetailsJsonName;
                                                      		var pdfTemplateDetails = [];
                                                      		var eAppPdfTemplateDetails = [];
                                                      		for (template in IllustratorVariables.productDetails.templates) {
                                                      			if (IllustratorVariables.productDetails.templates[template].templateType == '22001') {
                                                      				personalDetailsJsonName = IllustratorVariables.productDetails.templates[template].templateName + '.json';
                                                      			} else if (IllustratorVariables.productDetails.templates[template].templateType == '22002') {
                                                      				IllustratorVariables.illustrationInputTemplate = IllustratorVariables.productDetails.templates[template].templateName + '.json';
                                                      			} else if (IllustratorVariables.productDetails.templates[template].templateType == '22003') {
                                                      				IllustratorVariables.illustrationOutputTemplate = IllustratorVariables.productDetails.templates[template].templateName + '.json';
                                                      			}
                                                      			else if (IllustratorVariables.productDetails.templates[template].templateType == '22004') {
                                                      				
                                                      				var objPdfTemplate = {};
                                                      				
                                                      				objPdfTemplate.templateId = IllustratorVariables.productDetails.templates[template].templateId;
                                                      				objPdfTemplate.language = IllustratorVariables.productDetails.templates[template].language;
                                                      				objPdfTemplate.templateName = IllustratorVariables.productDetails.templates[template].templateName;
                                                      				eAppPdfTemplateDetails.push(objPdfTemplate);
                                                      				
                                                      			}
                                                      			else if (IllustratorVariables.productDetails.templates[template].templateType == '22005') {
                                                      				
                                                      				var objPdfTemplate = {};
                                                      				
                                                      				objPdfTemplate.templateId = IllustratorVariables.productDetails.templates[template].templateId;
                                                      				objPdfTemplate.language = IllustratorVariables.productDetails.templates[template].language;
                                                      				objPdfTemplate.templateName = IllustratorVariables.productDetails.templates[template].templateName;
                                                      				pdfTemplateDetails.push(objPdfTemplate);
                                                      			}
                                                      			else if (IllustratorVariables.productDetails.templates[template].templateType == '22006') {
                                                      				$scope.Illustration.Product.templates.eAppInputTemplate = IllustratorVariables.productDetails.templates[template].templateName + '.json';
                                                      			}
                                                      		}
                                                      		$scope.Illustration.Product.templates.illustrationPdfTemplate = pdfTemplateDetails;
                                                      	    $scope.Illustration.Product.templates.eAppPdfTemplate = eAppPdfTemplateDetails;  
                                                      	    
                                                      		IllustratorVariables.setIllustratorModel($scope.Illustration);
                                                      		LEDynamicUI.getUIJson(personalDetailsJsonName, false, function(personalDetailsJSON) { personalDetailsJSONLoop:
                                                      			for ( i = 0; i < personalDetailsJSON.products.length; i++) {
                                                      				if (personalDetailsJSON.products[i].id == transactionObj.Products.id) {
                                                      					personalDetailsJSON = personalDetailsJSON.products[i];
                                                      					break personalDetailsJSONLoop;
                                                      				}
                                                      			}
                                                      			LEDynamicUI.getUIJson(rootConfig.illustratorUIJson, false, function(personalDetailsUITemplate) {
                                                      				var customizedPersonalDetailsUITemplate = personalDetailsUITemplate;
                                                      				// Change
                                                      				// view
                                                      				// id
                                                      				// before
                                                      				// customising.
                                                      				personalDetailsUITemplate = $scope.changeViewId(personalDetailsUITemplate);
                                                      				customizedPersonalDetailsUITemplate = $scope.customizeForPersonalDetails(personalDetailsJSON, personalDetailsUITemplate);
                                                      				LEDynamicUI.paintUIFromJsonObject(rootConfig.template, customizedPersonalDetailsUITemplate, $scope.viewToBeCustomized, "#PersonalDetails", true, function() {
                                                      					if (!((rootConfig.isDeviceMobile))) {
                                                      						$('.custdatepicker').datepicker({
                                                      							format : 'yyyy-mm-dd',
                                                      							endDate : '+0d',
                                                      							autoclose : true
                                                      						}).on('change', function() {
                                                      							var elem = $(this).attr("ng-model");
                                                      							val = $(this).val();
                                                      							GLI_EvaluateService.bindModel($scope,elem,val);
																				//eval ("$scope." + elem + "=val");
                                                      							$scope.updateAge($(this).val(), $(this).attr("id"));
                                                      							/*if ($scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == 'Yes') {
                                                      								$scope.Illustration.Insured.BasicDetails.dob = new Date($scope.Illustration.Payer.BasicDetails.dob);
                                                      								$scope.updateAge($scope.Illustration.Payer.BasicDetails.dob, 'insuredDOB');
                                                      							}*/
                                                      							// $('.custdatepicker').datepicker('hide');
                                                      							$scope.refresh();
                                                      						});
                                                      					}
                                                      					// $rootScope.updateErrorCount($scope.viewToBeCustomized);
                                                      					LEDynamicUI.paintUI(rootConfig.template, rootConfig.illustratorUIJson, "HeaderDetails", "#HeaderDetails", true, function() {
                                                      						$scope.loadIllustration();
                                                      						// $rootScope.showHideLoadingImage(false);
                                                      						$scope.refresh();
                                                      					}, $scope, $compile);
                                                      				}, $scope, $compile);
                                                      			});
                                                      		});
                                                      	}, function(data) {
                                                      		alert("Error in get product Service " + data);
                                                      	});
                                                  	}
                                                  	/*
                                                  	 * Calculate age from Date of Birth.
                                                  	 */
                                                  	$scope.updateAge = function(dob, id) {
                                                  		if (dob != "" && dob != undefined && dob != null) {
                                                  			var dobDate = new Date(dob);
                                                  			/*
                                                  			 * In I.E it will accept date format in '/' separator only
                                                  			 */
                                                  			if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
                                                  				dobDate = new Date(dob.split("-").join("/"));
                                                  			}
                                                  			var todayDate = new Date();
                                                  			var yyyy = todayDate.getFullYear().toString();
                                                  			var mm = (todayDate.getMonth() + 1).toString();
                                                  			// getMonth() is
                                                  			// zero-based
                                                  			var dd = todayDate.getDate().toString();
                                                  			var formatedDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
                                                  			if (dobDate > todayDate) {
                                                  				if (id == "insuredDOB") {
                                                					$scope.Illustration.Insured.BasicDetails.dob = new Date(formatedDate);
                                                				} else if (id == "payorDOB") {
                                                					$scope.Illustration.Payer.BasicDetails.dob = new Date(formatedDate);
                                                				} else if (id == "childDOB") {
                                                					$scope.Illustration.ChildDetails.dob = new Date(formatedDate);
                                                				} /*else if ($scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == "Yes") {
                                                					$scope.Illustration.Insured.BasicDetails.dob = new Date($scope.Illustration.Payer.BasicDetails.dob);
                                                				}*/

                                                  			} else {
                                                  				var age = todayDate.getFullYear() - dobDate.getFullYear();
                                                  				if (todayDate.getMonth() < dobDate.getMonth() - 1 || (dobDate.getMonth() - 1 === todayDate.getMonth() && todayDate.getMonth() < dobDate.getDate())) {
                                                  					age--;
                                                  				}
                                                  				if (dob == undefined || dob == "") {
                                                  					age = "-";
                                                  				}
                                                  				angular.element('#' + id + 'Summary').text(age);
                                                  				return age;
                                                  			}
                                                  		}
                                                  	}

                                                  	$scope.saveIllustrationFromPersonalInfo = function() {
                                                  		IllustratorVariables.setIllustratorModel($scope.Illustration);
                                                  		$rootScope.showHideLoadingImage(true, 'saveTransactionMessage', $translate);
                                                  		$scope.refresh();
                                                  		var obj = new IllustratorService.saveTransactions($scope, $rootScope, DataService, $translate, UtilityService, IllustratorVariables, $routeParams, false);
                                                  		obj.save(function(isAutoSave) {
                                                  			$rootScope.showHideLoadingImage(false);
                                                  			$scope.refresh();
                                                  			if ((rootConfig.isDeviceMobile)) {
                                                  				if (!isAutoSave) {
                                                  					$rootScope.NotifyMessages(false, "saveIllustrationSuccessOffline", $translate);
                                                  				}
                                                  			} else {
                                                  				if (!isAutoSave) {
                                                  					$rootScope.NotifyMessages(false, "saveIllustrationSuccessOffline", $translate);
                                                  				}
                                                  			}
                                                  		});
                                                  	}
                                                  	/*
                                                  	 * Populate Insured details if insured and owner are same.
                                                  	 */
                                                  	$scope.IsInsuredSameAsOwner = function(id) {
                                                  		if ($scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == 'Yes') {
                                                  			$scope.Illustration.Insured = globalService.getInsured();
                                                  			$scope.Illustration.Insured.BasicDetails.firstName = $scope.Illustration.Payer.BasicDetails.firstName;
                                                  			$scope.Illustration.Insured.BasicDetails.mobileNo = $scope.Illustration.Payer.BasicDetails.mobileNo;
                                                  			if($scope.Illustration.Payer.ContactDetails.mobileNumber1){
																$scope.Illustration.Insured.ContactDetails.mobileNumber1 = $scope.Illustration.Payer.ContactDetails.mobileNumber1;
                                                  			}
                                                  			//$scope.Illustration.Insured.BasicDetails.dob = new Date($scope.Illustration.Payer.BasicDetails.dob);
                                                  			$scope.Illustration.Insured.BasicDetails.gender = $scope.Illustration.Payer.BasicDetails.gender;
                                                  			$scope.Illustration.Insured.BasicDetails.countryofResidence = $scope.Illustration.Payer.BasicDetails.countryofResidence;
                                                  			$scope.Illustration.Insured.BasicDetails.state = $scope.Illustration.Payer.BasicDetails.state;
                                                  			$scope.Illustration.Insured.ContactDetails.emailId = $scope.Illustration.Payer.ContactDetails.emailId;		
                                                  			$scope.Illustration.Insured.OccupationDetails.occupationCategory = $scope.Illustration.Payer.OccupationDetails.occupationCategory;
                                                  			//$scope.updateAge($scope.Illustration.Insured.BasicDetails.dob, 'insuredDOB');
                                                  		} else {
                                                  		if(IllustratorVariables.fnaId != "" && IllustratorVariables.fnaId != 0){
                                                  				$scope.Illustration.Insured = "";
                                                  				angular.element('#insuredDOBSummary').text("");
                                                  			}else{
                                                  			$scope.Illustration.Insured.BasicDetails.firstName = "";
                                                  			$scope.Illustration.Insured.BasicDetails.mobileNo ="";
                                                  			if($scope.Illustration.Insured.ContactDetails.mobileNumber1){
                                                  			$scope.Illustration.Insured.ContactDetails.mobileNumber1 ="";
                                                  			}
                                                  			$scope.Illustration.Insured.BasicDetails.dob = "";
                                                  			$scope.Illustration.Insured.BasicDetails.gender = "";
                                                  			$scope.Illustration.Insured.BasicDetails.countryofResidence = "";
                                                  			$scope.Illustration.Insured.BasicDetails.state = "";
                                                  			$scope.Illustration.Insured.OccupationDetails.occupationCategory = "";
                                                  			angular.element('#insuredDOBSummary').text("");
                                                  			}
                                                  		}
                                                  		setTimeout(function() {
                                                  			$rootScope.updateErrorCount($scope.viewToBeCustomized);
                                                  		}, 500);

                                                  		// $scope.refresh();
                                                  	}
                                                  	/*
                                                  	 * Proceeding to product details tab from personal details on the click of
                                                  	 * 'proceed' button.
                                                  	 */
                                                  	$scope.proceedToProductDetails = function() {
                                                  		$rootScope.showHideLoadingImage(true, "Loading,please wait..");
                                                  		IllustratorVariables.setIllustratorModel($scope.Illustration);

                                                  		$scope.nav = Navigation($scope, "PersonalDetails", "forward");
                                                  		$rootScope.pageNavigationIllus('Product Details');
                                                  	}
                                                  	/*var firstNameWatch = $scope.$watch('Illustration.Payer.BasicDetails.firstName', function(value) {
                                                  		if ( typeof $scope.Illustration != "undefined") {
                                                  			if ($scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == "Yes") {
                                                  				$scope.Illustration.Insured.BasicDetails.firstName = $scope.Illustration.Payer.BasicDetails.firstName;
                                                  			} else {
                                                  				if (!$scope.Illustration.Insured.BasicDetails.firstName && $scope.Illustration.Insured.BasicDetails.firstName === "null" && $scope.Illustration.Insured.BasicDetails.firstName === "undefined") {
                                                  					$scope.Illustration.Insured.BasicDetails.firstName = '';
                                                  				}
                                                  			}
                                                  		}
                                                  	}, true);

                                                  	var genderWatch = $scope.$watch('Illustration.Payer.BasicDetails.gender', function(value) {
                                                  		if ( typeof $scope.Illustration != "undefined") {
                                                  			if ($scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == "Yes") {
                                                  				$scope.Illustration.Insured.BasicDetails.gender = $scope.Illustration.Payer.BasicDetails.gender;
                                                  			} else {
                                                  				if (!$scope.Illustration.Insured.BasicDetails.gender && $scope.Illustration.Insured.BasicDetails.gender === "null" && $scope.Illustration.Insured.BasicDetails.gender === "undefined") {
                                                  					$scope.Illustration.Insured.BasicDetails.gender = '';
                                                  				}
                                                  			}
                                                  		}
                                                  	}, true);

                                                  	var dobWatch = $scope.$watch('Illustration.Payer.BasicDetails.dob', function(value) {
                                                  		if ( typeof $scope.Illustration != "undefined") {
                                                  			if ($scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == "Yes") {
                                                  				$scope.Illustration.Insured.BasicDetails.dob = new Date($scope.Illustration.Payer.BasicDetails.dob);
                                                  			} else {
                                                  				if (!$scope.Illustration.Insured.BasicDetails.dob && $scope.Illustration.Insured.BasicDetails.dob === "null" && $scope.Illustration.Insured.BasicDetails.dob === "undefined") {
                                                  					$scope.Illustration.Insured.BasicDetails.dob = '';
                                                  				}
                                                  			}
                                                  		}
                                                  	}, true);

                                                  	var countryofResidenceWatch = $scope.$watch('Illustration.Payer.BasicDetails.countryofResidence', function(value) {
                                                  		if ( typeof $scope.Illustration != "undefined") {
                                                  			if ($scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == "Yes") {
                                                  				$scope.Illustration.Insured.BasicDetails.countryofResidence = $scope.Illustration.Payer.BasicDetails.countryofResidence;
                                                  			} else {
                                                  				if (!$scope.Illustration.Insured.BasicDetails.countryofResidence && $scope.Illustration.Insured.BasicDetails.countryofResidence === "null" && $scope.Illustration.Insured.BasicDetails.countryofResidence === "undefined") {
                                                  					$scope.Illustration.Insured.BasicDetails.countryofResidence = '';
                                                  				}
                                                  			}
                                                  		}
                                                  	}, true);

                                                  	var stateWatch = $scope.$watch('Illustration.Payer.BasicDetails.state', function(value) {
                                                  		if ( typeof $scope.Illustration != "undefined") {
                                                  			if ($scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == "Yes") {
                                                  				$scope.Illustration.Insured.BasicDetails.state = $scope.Illustration.Payer.BasicDetails.state;
                                                  			} else {
                                                  				if (!$scope.Illustration.Insured.BasicDetails.state && $scope.Illustration.Insured.BasicDetails.state === "null" && $scope.Illustration.Insured.BasicDetails.state === "undefined") {
                                                  					$scope.Illustration.Insured.BasicDetails.state = '';
                                                  				}
                                                  			}
                                                  		}
                                                  	}, true);

                                                  	var occupationCategoryWatch = $scope.$watch('Illustration.Payer.OccupationDetails.occupationCategory', function(value) {
                                                  		if ( typeof $scope.Illustration != "undefined") {
                                                  			if ($scope.Illustration.Payer.CustomerRelationship.isPayorDifferentFromInsured == "Yes") {
                                                  				$scope.Illustration.Insured.OccupationDetails.occupationCategory = $scope.Illustration.Payer.OccupationDetails.occupationCategory;
                                                  			} else {
                                                  				if (!$scope.Illustration.Insured.OccupationDetails.occupationCategory && $scope.Illustration.Insured.OccupationDetails.occupationCategory === "null" && $scope.Illustration.Insured.OccupationDetails.occupationCategory === "undefined") {
                                                  					$scope.Illustration.Insured.OccupationDetails.occupationCategory = '';
                                                  				}
                                                  			}
                                                  		}
                                                  	}, true);
													*/
                                                  	$scope.evaluateModel = function(formName, value) {
                                                  		var model = formName + '.' + value;
                                                  		if($scope.evaluateString(model)) {
														//if ($scope.$eval (model)) {
                                                  			$rootScope.updateErrorCount($scope.viewToBeCustomized, true);
                                                  			return true;
                                                  		} else {
                                                  			$rootScope.updateErrorCount($scope.viewToBeCustomized, true);
                                                  			return false;
                                                  		}

                                                  	}
                                                  	$scope.evaluateString = function(value) {
                                									var val;
                                                                    var variableValue = $scope;
                                                                    val = value.split('.');
                                                                    if(value.indexOf('$scope') != -1){
                                                                        val = val.shift();
                                                                    }
                                									for(var i in val){
                                                                        variableValue = variableValue[val[i]];
                                                                    }
                                                                    return variableValue;
                                                            };
                                                	$scope.$on('$viewContentLoaded', function(){
                                                		$scope.initialLoad();
                        							  });
                                                  	
                                                  	$scope.$on("$destroy", function() {
                                                  		//occupationCategoryWatch();
                                                  		//firstNameWatch();
                                                  		//genderWatch();
                                                  		//dobWatch(); 
                                                  		//countryofResidenceWatch();
                                                  		//stateWatch();
                                                  	});
                                                  }]);

                                                  function countryFilter(model, dependent) {
                                                  	this.model = model;
                                                  	this.dependent = dependent;
                                                  }
