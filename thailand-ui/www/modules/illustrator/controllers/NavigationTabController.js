﻿/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name:NavigationTabController.js
 CreatedDate:4/14/2013
 Description:illustrator navigation tab controller.
 */

storeApp
		.controller(
				'NavigationTabController',
				[
						'$scope',
						'$rootScope',
						'$compile',
						'$translate',
						'$location',
						'$routeParams',
						'DataService',
						'UtilityService',
						'IllustratorVariables',
						'IllustratorService',
						function($scope, $rootScope, $compile, $translate,
								$location, $routeParams, DataService,
								UtilityService, IllustratorVariables,
								IllustratorService) {
							var IllustrationOutputWatch;
							var errorCountWatch;
							$rootScope.moduleHeader = "illustrator.Illustrator";
							$rootScope.moduleVariable = translateMessages(
									$translate, $rootScope.moduleHeader);
							// $scope.style=[];
							$scope.stylePersonal = "";
							$scope.styleProduct = "";
							$scope.styleIllus = "";
							$scope.content = [];
							$scope.disability = [];
							$scope.illTabNavStyle = [];
							$scope.illTabNavigation = function(index) {
								$scope.selectedtab = index;
								for (var i = 0; i < $scope.illTabNavStyle.length; i++) {
									if (i < index) {
										$scope.illTabNavStyle[i] = 'activated';
									} else if (i == index) {
										$scope.illTabNavStyle[i] = 'activate';
									} else {
										$scope.illTabNavStyle[i] = '';
									}
								}
								if (angular.equals(
										$scope.Illustration.IllustrationOutput,
										{})) {
									$scope.illTabNavStyle[2] = "disabled";
								}
							}
							$scope.initTabs = function() {
								if ($scope.selectedpage == "Personal Details") {
									$scope.illTabNavigation(0);

								} else if ($scope.selectedpage == "Product Details") {
									$scope.illTabNavigation(1);
								} else {
									$scope.illTabNavigation(2);
								}

							}
							$scope.initTabs();
							$rootScope.pageNavigationIllus = function(index) {
								$rootScope.showHideLoadingImage(true,
										'paintUIMessage', $translate);
								IllustratorVariables.selectedpage = index;
								$scope.saveIllustrationOnTabClick();

							}
							$scope.onPaintUISuccess = function(callBackId) {
								// $scope.refresh();
								// $rootScope.showHideLoadingImage(false);
							}
							var current_clicked_li;

							$scope.paintNavigationTab = function() {
								$rootScope.showHideLoadingImage(true,
										'paintUIMessage', $translate);
								LEDynamicUI
										.paintUI(
												rootConfig.template,
												rootConfig.illustratorUIJson,
												"illustrationMainTabs",
												"#illustrationMainTabs",
												true,
												function(x) {
													$scope.illTabNavStyle[0] = 'activate';
													$scope.illTabNavStyle[1] = 'disabled';
													$scope.illTabNavStyle[2] = 'disabled';

													$scope
															.setIllustrationTabs($scope.selectedpage);

													IllustrationOutputWatch = $scope
															.$watch(
																	'Illustration.IllustrationOutput',
																	function(
																			value) {
																		$scope
																				.initTabs();
																	});

													errorCountWatch = $scope
															.$watch(
																	'errorCount',
																	function(
																			value) {

																		$scope
																				.setIllustrationTabs($scope.selectedpage);
																	});
													if ($scope.selectedpage == "Product Details") {

														$rootScope
																.showHideLoadingImage(false);
														$scope.refresh();
													}

												}, $scope, $compile);

							}

							$scope.setIllustrationTabs = function(selectedpage) {

								if ($scope.selectedpage == "Personal Details") {

									if ($scope.errorCount == 0) {

										$scope.disableProduct = false;

										$scope.illTabNavStyle[1] = '';
										$scope.illTabNavStyle[2] = 'disabled';

										if (!angular
												.equals(
														$scope.Illustration.IllustrationOutput,
														{})) {

											$scope.disableIllus = false;

											$scope.illTabNavStyle[1] = '';
											$scope.illTabNavStyle[2] = '';
										}
									} else {

										$scope.disableProduct = true;
										$scope.disableIllus = true;

										$scope.illTabNavStyle[1] = 'disabled';
										$scope.illTabNavStyle[2] = 'disabled';
									}
								}

								if ($scope.selectedpage == "Product Details") {

									if ($scope.errorCount == 0
											&& !angular
													.equals(
															$scope.Illustration.IllustrationOutput,
															{})) {

										$scope.disableIllus = false;

										$scope.illTabNavStyle[0] = 'activated';
										$scope.illTabNavStyle[1] = 'activate';
										$scope.illTabNavStyle[2] = '';
									} else {

										$scope.disableIllus = true;

										$scope.illTabNavStyle[0] = 'activated';
										$scope.illTabNavStyle[1] = 'activate';
										$scope.illTabNavStyle[2] = 'disabled';
									}
								}

								if ($scope.selectedpage == "Illustration Details") {
									$scope.disablePersonal = false;
									$scope.disableProduct = false;
									$scope.disableIllus = false;
									$scope.illTabNavStyle[0] = 'activated';
									$scope.illTabNavStyle[1] = 'activated';
									$scope.illTabNavStyle[2] = 'activated';
								}
							}

							$scope.saveIllustrationOnTabClick = function() {
								IllustratorVariables
										.setIllustratorModel($scope.Illustration);
								var obj = new IllustratorService.saveTransactions(
										$scope, $rootScope, DataService,
										$translate, UtilityService,
										IllustratorVariables, $routeParams,
										false);
								obj
										.save(function() {
											if (IllustratorVariables.selectedpage == "Illustration Details") {
												$location
														.path('/illustration/'
																+ $routeParams.productId
																+ '/'
																+ $routeParams.proposalId
																+ '/'
																+ $routeParams.illustrationId
																+ '/'
																+ $routeParams.transactionId);
												$scope.showHeaderImages = true;
												//$rootScope.updateErrorCount("IllustrationDetails");
											} else if (IllustratorVariables.selectedpage == "Personal Details") {
												$location
														.path('/Illustrator/'
																+ $routeParams.productId
																+ '/'
																+ $routeParams.proposalId
																+ '/'
																+ $routeParams.illustrationId
																+ '/'
																+ $routeParams.transactionId);
												//$rootScope.updateErrorCount("PersonalDetails");
											} else {
												$location
														.path('/illustratorProduct/'
																+ $routeParams.productId
																+ '/'
																+ $routeParams.proposalId
																+ '/'
																+ $routeParams.illustrationId
																+ '/'
																+ $routeParams.transactionId);
												//$rootScope.updateErrorCount("ProductDetails");
											}
											$scope.refresh();
										});
							}
							if(typeof IllustrationOutputWatch != "undefined"){
							$scope.$on("$destroy", function() {
								IllustrationOutputWatch();
								errorCountWatch();
							});
							}
						} ]);
