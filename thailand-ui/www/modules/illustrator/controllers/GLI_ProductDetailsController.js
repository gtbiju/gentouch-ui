/*
 * Copyright 2015, LifeEngage 
 */
/*
 Name : GLI_ProductDetailsController
 CreatedDate:25/06/2015
 Description:Extended controller for Generali BI product details
 */

storeApp.controller('GLI_ProductDetailsController', GLI_ProductDetailsController);
GLI_ProductDetailsController.$inject = ['GLI_IllustratorService', 'globalService', '$location', '$rootScope', 'GLI_IllustratorVariables', '$scope', '$compile', '$routeParams', 'UtilityService', 'PersistenceMapping', 'DataService', '$translate', 'dateFilter', '$filter', 'ProductService', 'GLI_RuleService', 'IllustratorVariables', 'IllustratorService', '$debounce', 'AutoSave', 'FnaVariables', 'UserDetailsService', '$controller', '$timeout', 'GLI_globalService', 'GLI_DataService', 'DataLookupService'];

function GLI_ProductDetailsController(GLI_IllustratorService, globalService, $location, $rootScope, GLI_IllustratorVariables, $scope, $compile, $routeParams, UtilityService, PersistenceMapping, DataService, $translate, dateFilter, $filter, ProductService, GLI_RuleService, IllustratorVariables, IllustratorService, $debounce, AutoSave, FnaVariables, UserDetailsService, $controller, $timeout, GLI_globalService, GLI_DataService, DataLookupService) {

    $controller('ProductDetailsController', {
        IllustratorService: GLI_IllustratorService,
        globalService: globalService,
        $scope: $scope,
        $rootScope: $rootScope,
        IllustratorVariables: GLI_IllustratorVariables,
        $compile: $compile,
        $routeParams: $routeParams,
        UtilityService: UtilityService,
        PersistenceMapping: PersistenceMapping,
        DataService: DataService,
        $translate: $translate,
        dateFilter: dateFilter,
        ProductService: ProductService,
        RuleService: GLI_RuleService,
        $debounce: $debounce,
        AutoSave: AutoSave,
        FnaVariables: FnaVariables,
        GLI_globalService: GLI_globalService
    });
    $rootScope.hideLanguageSetting = false;
    $rootScope.isFromIllustration = true;
    $scope.isFromIllustration = $rootScope.isFromIllustration;
    $scope.decimalNumberPattern = rootConfig.decimalNumberPattern;
    $scope.numberPattern = rootConfig.numberPattern;
    $scope.Illustration = IllustratorVariables.getIllustratorModel();
    $rootScope.selectedPage = "Product Details";
    $scope.enableProceedToEapp=false;
		
	if ($scope.Illustration.Product.ProductDetails.productId == '1010' && $scope.Illustration.Product.ProductDetails.productCode == '10') {
		$scope.initiallyDisabled = false;
		checkOPDRiderAndValues();
	}
    if($scope.Illustration.Product.ProductDetails.productId == '1306' && $scope.Illustration.Product.ProductDetails.productCode == '306' ||
       $scope.Illustration.Product.ProductDetails.productId == '1274' && $scope.Illustration.Product.ProductDetails.productCode == '274'){
    	$scope.defaultRiderDisabled = true;
    } else {
    	$scope.defaultRiderDisabled = false;
    }
	
	/*      if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4'){
		$scope.disabledForSingleProduct = false;
	} else {
		$scope.disabledForSingleProduct = true;
	}*/
	if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4' ||
	   $scope.Illustration.Product.ProductDetails.productId == '1006' && $scope.Illustration.Product.ProductDetails.productCode == '6' ||
	   $scope.Illustration.Product.ProductDetails.productId == '1220' && $scope.Illustration.Product.ProductDetails.productCode == '220'||
	   $scope.Illustration.Product.ProductDetails.productId == '1306' && $scope.Illustration.Product.ProductDetails.productCode == '306'){
			$scope.initiallyDisabled = true;
	}

    $scope.navigatedFromFNA = IllustratorVariables.fromFNAChoosePartyScreenFlow;
    
    if ($rootScope.premiumSummaryOutput == undefined || !(IllustratorVariables.isValidationSuccess)) {
        $rootScope.premiumSummaryOutput = false;
    }
    if (IllustratorVariables.illustrationStatus == "Confirmed") {
        $rootScope.premiumSummaryOutput = true;
    } else {
        $rootScope.premiumSummaryOutput = false;
    }
    if (IllustratorVariables.illustrationStatus == "Confirmed" || (IllustratorVariables.illustrationStatus == "Completed" && $rootScope.haseApp == true)) {
            $rootScope.disableControl = true;
    } else {
            $rootScope.disableControl = false;
    }
    //To enable Validate button for completed status illustration
    if (IllustratorVariables.illustrationStatus == "Completed") {
        $rootScope.premiumSummaryOutput = false;
    }
    $scope.additionalInsuredSpouse = true;
    $scope.premiumSummary = {};
    $scope.translateLabel = function (value) {
        return $translate.instant(value);
    }
    
// Common method for navigating to choose party screen for web and device
	$scope.navigateToChooseParty = function(fnaData){
		if(fnaData != undefined){
			if (fnaData && fnaData[0] && fnaData[0].TransactionData) {
				FnaVariables.setFnaModel({
					'FNA' : JSON.parse(JSON.stringify(fnaData[0].TransactionData))
				});
				$scope.FNAObject = FnaVariables.getFnaModel();
				FnaVariables.transTrackingID = fnaData[0].TransTrackingID;
				FnaVariables.leadId = fnaData[0].Key1;
				globalService.setParties($scope.FNAObject.FNA.parties);
				UtilityService.previousPage = 'IllustrationPersonal';
				if(IllustratorVariables.illustratorId =="" || typeof IllustratorVariables.illustratorId == "undefined"){
					IllustratorVariables.illustratorId = "0";
				}
				$location.path("/fnaChooseParties/0/0/"+ IllustratorVariables.illustratorId);
				$scope.refresh();
			}
		}
	}

     // FNA changes by LE Team - Starts>>
    $scope.chooseProductRecommendation=function(){
    	$scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                        			   translateMessages($translate, "pageNavigationText"),  
                        			   translateMessages($translate, "fna.navok"), 
                        			   $scope.okPressed, 
                        			   translateMessages($translate, "general.navproceed"), 
                        			   $scope.proceedToProductRecommendationScreen);     
    }

    $scope.proceedToProductRecommendationScreen=function(){
        IllustratorVariables.fromChooseAnotherProduct = false;
        $location.path("/fnaRecomendedProducts");  
    }

    $scope.choosePartyScreen=function(){
    	$scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                        			   translateMessages($translate, "pageNavigationText"),  
                        			   translateMessages($translate, "fna.navok"), 
                        			   $scope.okPressed, 
                        			   translateMessages($translate, "general.navproceed"), 
                        			   $scope.proceedToPartyScreen);     
    }

    $scope.proceedToPartyScreen=function(){
        IllustratorVariables.fromChooseAnotherProduct = false;
        $location.path("/fnaChooseParties/0/" + $routeParams.productId + "/0");  
    }
    // FNA changes by LE Team - Ends>>

    $scope.proceedToChooseParty = function(){
		if ((rootConfig.isDeviceMobile)) {
			product = GLI_globalService.getProduct();
			product.ProductDetails.productCode = $scope.Illustration.Product.ProductDetails.productCode;
			GLI_globalService.setProduct(product);
			DataService.getRelatedFNA(IllustratorVariables.fnaId, function(fnaData) {
				$scope.navigateToChooseParty(fnaData);
			});
		} else {
			PersistenceMapping.clearTransactionKeys();
			var  searchCriteria = {
					 searchCriteriaRequest:{
			"command" : "RelatedTransactions",
			"modes" :['FNA'],
			"value" : IllustratorVariables.fnaId
			}
			};
			PersistenceMapping.Type = "FNA";
			var transactionObj = PersistenceMapping.mapScopeToPersistence(searchCriteria);
			transactionObj.TransTrackingID = IllustratorVariables.fnaId;
			DataService.getFilteredListing(transactionObj, function(fnaData) {
				$scope.navigateToChooseParty(fnaData);
			});
		}
	}


    $scope.editFlowPackage = IllustratorVariables.editFlowPackage;

    //Overrided Method 
    $scope.getRiderIndex = function (uniqueName) {
        var riderIndex;
        for (k = 0; k < $scope.Illustration.Product.RiderDetails.length; k++) {
            if ($scope.Illustration.Product.RiderDetails[k].uniqueRiderName == uniqueName) {
                riderIndex = k;
                break;
            }
        }
        $scope.riderIndex = riderIndex;
        return [riderIndex];
    }


    $scope.getRiderErrMsg = function (value, insType) {
        var errMsg;
        errMsg = translateMessages($translate, "illustrator." + value);
        if (insType != undefined && insType == 'Maininsured') {
            errMsg = errMsg + " " + $scope.Illustration.Insured.BasicDetails.fullName;
        } 
        return errMsg;
    }
    
    $scope.getRiderErrMsgHospitalCash = function (value, insType) {
        var errMsg;
        errMsg = translateMessages($translate, "illustrator." + value);
        if (insType != undefined && insType == 'Maininsured') {
            errMsg = $scope.Illustration.Insured.BasicDetails.fullName + " " + errMsg;
        } 
        return errMsg;
    }

    //Method to return dispaly value
    $scope.getDisplayValueFromRiderCode = function (uniqueName) {
        var riderValue = $scope.getRiderIndex(uniqueName);
        if($scope.Illustration.Product.RiderDetails[riderValue].riderPlanName === 'DD_2551'){
			$scope.Illustration.Product.RiderDetails[riderValue].riderPlanName = 'DD';
		}
        return $scope.Illustration.Product.RiderDetails[riderValue].riderPlanName;
    }

    $scope.clearOffDataForIndividualRider = function (riderIndxVal, clearSA, enableRiderSA) {
        if (enableRiderSA != undefined && enableRiderSA === true) {
            $scope.clearOffData(riderIndxVal, clearSA);
            //Added to prepolate the value from db			
            $scope.callChangeMethod('CommonFunctions', 'onSumInsuredChangeiPlan', $scope.Illustration, '', '', '');
        }
    }
    
    //clearing of data
    $scope.clearOffData = function (data, clearSuminsured, clearAge) {
        if ($scope.Illustration.Product.RiderDetails[data[0]].isRequired != 'Yes') {}
        if (clearSuminsured == true) {
            if ($scope.Illustration.Product.RiderDetails[data[0]].isRequired != 'Yes') {
                $scope.Illustration.Product.RiderDetails[data[0]].sumInsured = '';
                $scope.Illustration.Product.RiderDetails[data[0]].policyTerm = '';
                $scope.Illustration.Product.RiderDetails[data[0]].riderPremium = '';
            }
        }
    }
	
    // Customized method for policy details
    $scope.customizeForPolicyDetails = function (product, productUIJson, successCallback) {
        PersistenceMapping.clearTransactionKeys();
        IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
        var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
        $scope.Key2 = transactionObj.Key2;
        // Product Name
        if (!product.name) {
            productUIJson = $scope.customizeUIJson(productUIJson, 'productName');
        }
        // Occupation Class
        if (!product.properties.occupationClass) {
            productUIJson = $scope.customizeUIJson(productUIJson, 'occupationClass');
        } 
        //Premium Period
        if (!product.properties.premiumPeriod) {
            productUIJson = $scope.customizeUIJson(productUIJson, 'premiumPeriod');
        } else {
			$scope.premiumPeriodOption = {};
			var options = [];
			product.properties.premiumPeriod = product.properties.premiumPeriod.sort(); 
			// Premium Period Population Based on Insured Age for GenProLife
			$scope.insuredAge =  calculateAgeTH($scope.Illustration.Insured.BasicDetails.dob);
			if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4'){
				if($scope.insuredAge > 9 &&  $scope.insuredAge < 61) {
					for (var i = 0; i < product.properties.premiumPeriod.length; i++) {
						var premiumPeriodOptionObj = {
							"premiumPeriodOption": product.properties.premiumPeriod[i]
						};
						options.push(premiumPeriodOptionObj);
					}
				} else if ($scope.insuredAge < 10) {
					if($scope.Illustration.Product.policyDetails.premiumPeriod==='20'){
						$scope.Illustration.Product.policyDetails.premiumPeriod="";
					}
					var premiumPeriodOptionObj = {"premiumPeriodOption": product.properties.premiumPeriod[1]};
					options.push(premiumPeriodOptionObj);
				} else if ($scope.insuredAge > 60) {
					if($scope.Illustration.Product.policyDetails.premiumPeriod==='20'){
						$scope.Illustration.Product.policyDetails.premiumPeriod="";
					}
					var premiumPeriodOptionObj = {"premiumPeriodOption": product.properties.premiumPeriod[1]};
					options.push(premiumPeriodOptionObj);
				}
			} else {
				for (var i = 0; i < product.properties.premiumPeriod.length; i++) {
					var premiumPeriodOptionObj = {
						"premiumPeriodOption": product.properties.premiumPeriod[i]
					};
					options.push(premiumPeriodOptionObj);
				}
			}
			
			/*for (var i = 0; i < product.properties.premiumPeriod.length; i++) {
				var premiumPeriodOptionObj = {
					"premiumPeriodOption": product.properties.premiumPeriod[i]
				};
				options.push(premiumPeriodOptionObj);
			}*/
			$scope.premiumPeriodOption.Options = options;
        }
        
        // premiumFrequency
        if (product.properties.premiumFrequency) {
            var tempPremiumArray = [];
            for (var j = 0; j < product.properties.premiumFrequency.length; j++) {
                if (product.properties.premiumFrequency[j] == "Annual") {
                    var yearly = product.properties.premiumFrequency[j];
                } else if (product.properties.premiumFrequency[j] == "Semi Annual") {
                    var halfYearly = product.properties.premiumFrequency[j];
                }  else if (product.properties.premiumFrequency[j] == "Quarterly") {
                    var quartely = product.properties.premiumFrequency[j];
                } else {
                    if (product.properties.premiumFrequency[j] == "Monthly") {
                        var monhtly = product.properties.premiumFrequency[j];
                    }
                }
            }
            tempPremiumArray.push(yearly);
            tempPremiumArray.push(halfYearly);
            tempPremiumArray.push(quartely);
            if (monhtly) {
                tempPremiumArray.push(monhtly);
            }
            product.properties.premiumFrequency = tempPremiumArray;
            if (product.properties.premiumFrequency instanceof Array && (product.properties.premiumFrequency).length > 0) {
                $scope.premiumFrequency = {};
                var options = [];
                for (i = 0; i < product.properties.premiumFrequency.length; i++) {
                    var premiumFrequencyObj = {
                        "premiumFrequencyTranslate": translateMessages($translate, product.properties.premiumFrequency[i]),
                        "premiumFrequencyValue": product.properties.premiumFrequency[i]
                    };
                    options.push(premiumFrequencyObj);
                }
                $scope.premiumFrequency.Options = options;
            } else if (product.properties.premiumFrequency instanceof Array && (product.properties.premiumFrequency).length == 0) {
                productUIJson = $scope.customizeUIJson(productUIJson, 'premiumFrequency');
            } else {
                $scope.premiumFrequency = {};
                var options = [];
                var premiumPayingTermObj = {
                    "premiumFrequencyTranslate": translateMessages($translate, product.properties.premiumFrequency[i]),
                    "premiumFrequencyValue": product.properties.premiumFrequency[i]
                };
                options.push(premiumPayingTermObj);
                $scope.premiumFrequency.Options = options;
            }
        } else {
            productUIJson = $scope.customizeUIJson(productUIJson, 'premiumFrequency');
        }
        
        //Packages
		if (!product.properties.packages) {
			productUIJson = $scope.customizeUIJson(productUIJson, 'packages');
		} else {
			$scope.packagesOption = {};
			var options = [];
			product.properties.packages = product.properties.packages.sort();
			$scope.insuredAge = calculateAgeTH($scope.Illustration.Insured.BasicDetails.dob);
			if ($scope.Illustration.Insured.OccupationDetails.length == 1 && ($scope.Illustration.Insured.OccupationDetails[0].occupationCode == "N018" || $scope.Illustration.Insured.OccupationDetails[0].occupationCodeValue == "N018")) {
				var packagesOptionObj1 = { "packagesOptionTranslate": translateMessages($translate, product.properties.packages[0]), "packagesOption": product.properties.packages[0] };
				options.push(packagesOptionObj1);
				var packagesOptionObj = { "packagesOptionTranslate": translateMessages($translate, product.properties.packages[2]), "packagesOption": product.properties.packages[2] };
				options.push(packagesOptionObj);
			}
			else if ($scope.insuredAge > 0 && $scope.insuredAge <= 10 && $scope.Illustration.Product.ProductDetails.productId != '1097') {
				var packagesOptionObj = { "packagesOptionTranslate": translateMessages($translate, product.properties.packages[2]), "packagesOption": product.properties.packages[2] };
				options.push(packagesOptionObj);
			}
			else if ($scope.insuredAge >= 11 && $scope.insuredAge <= 15 && $scope.Illustration.Product.ProductDetails.productId != '1097') {
				var packagesOptionObj1 = { "packagesOptionTranslate": translateMessages($translate, product.properties.packages[0]), "packagesOption": product.properties.packages[0] };
				options.push(packagesOptionObj1);
				var packagesOptionObj = { "packagesOptionTranslate": translateMessages($translate, product.properties.packages[2]), "packagesOption": product.properties.packages[2] };
				options.push(packagesOptionObj);
			}
			else if ($scope.insuredAge >= 65 && $scope.insuredAge <= 70 && $scope.Illustration.Product.ProductDetails.productId != '1097') {
				var packagesOptionObj1 = { "packagesOptionTranslate": translateMessages($translate, product.properties.packages[0]), "packagesOption": product.properties.packages[0] };
				options.push(packagesOptionObj1);
				var packagesOptionObj = { "packagesOptionTranslate": translateMessages($translate, product.properties.packages[1]), "packagesOption": product.properties.packages[1] };
				options.push(packagesOptionObj);
			}
			else {
				for (var i = 0; i < product.properties.packages.length; i++) {
					var packagesOptionObj = {
						"packagesOptionTranslate": translateMessages($translate, product.properties.packages[i]),
						"packagesOption": product.properties.packages[i]
					};
					options.push(packagesOptionObj);
				}
			}
			$scope.packagesOption.Options = options;
		}
        // Sum Assured
        if (!product.properties.sumAssured) {
            productUIJson = $scope.customizeUIJson(productUIJson, 'sumAssured');
        } 
        
        // Sum Assured
        if (!product.properties.premium) {
            productUIJson = $scope.customizeUIJson(productUIJson, 'premium');
        } 
        
        //Tax
        if (!product.properties.tax) {
            productUIJson = $scope.customizeUIJson(productUIJson, 'tax');
        } else {
			$scope.taxOption = {};
			//product.properties.tax = product.properties.tax.sort();
			var options = [];
			product.properties.tax.sort(function(a, b){
				return a - b
			});
			for (var i = 0; i < product.properties.tax.length; i++) {
				var taxOptionObj = {
					"taxOption": product.properties.tax[i]
				};
				options.push(taxOptionObj);
			}
			$scope.taxOption.Options = options;
        }
        
        // Discount Premium
        if (!product.properties.discountedPremium) {
            productUIJson = $scope.customizeUIJson(productUIJson, 'discountedPremium');
        } 

        successCallback(productUIJson);
    }
    
    function calculateAgeTH(dob) {
        var age = "";
        if (dob != "" && dob != undefined &&  dob != null) {
            var dobDate = new Date(dob);
            /*
             * In I.E it will accept date format in '/'
             * separator only
             */
            if ((dobDate == "NaN" || dobDate == "Invalid Date")) {
                dobDate = new Date(dob.split("-").join("/"));
            }
            var todayDate = new Date();
            if (formatForDateControl(dob) >= formatForDateControl(new Date())) {
                //$scope.Illustration.Insured.BasicDetails.age = 0;
                age = 0;
            } else {
                var curd = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate());
                var cald = new Date(dobDate.getFullYear(), dobDate.getMonth(), dobDate.getDate());
                var dife = ageCalculationTH(curd, cald);
                if (age < 1) {
                    var datediff = curd.getTime() - cald.getTime();
                    var days = (datediff / (24 * 60 * 60 * 1000)) / 365;
                    days = Number(Math.round(days + 'e3') + 'e-3');
                    age = 0;
                }
                if (dife[1] == "false") {
                    age = dife[0];
                } else {
                    age = dife[0] + 1;
                }

                if (dob == undefined || dob == "") {
                    age = "-";
                }
            }
        }
        return age;
    };
    
  //Calculating age in years, month , days 
    function ageCalculationTH (currDate, dobDate) {
        date1 = new Date(currDate);
        date2 = new Date(dobDate);
        var y1 = date1.getFullYear(),
            m1 = date1.getMonth(),
            d1 = date1.getDate(),
            y2 = date2.getFullYear(),
            m2 = date2.getMonth(),
            d2 = date2.getDate();

        var nextDOB;
        if (m1 < m2) {
            nextDOB = new Date(date2.setYear(date1.getFullYear()));
        } else if ((m1 == m2) && (d1 < d2)) {
            nextDOB = new Date(date2.setYear(date1.getFullYear()));
        } else {
            nextDOB = new Date(date2.setYear(date1.getFullYear() + 1));
        }
        var marginDate = new Date(date1.setMonth(date1.getMonth() + 6));

        if (m1 < m2) {
            y1--;
            m1 += 12;
        }

        if (nextDOB.getTime() > marginDate.getTime()) {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() - birthDate.getFullYear();
            var mnth = today.getMonth() - birthDate.getMonth();
            if (mnth < 0 || (mnth === 0 && today.getDate() < birthDate.getDate())) {
                ageVal--;
            }
            return [ageVal, "false"];
        } else {
            var today = new Date();
            var birthDate = new Date(dobDate);
            var ageVal = today.getFullYear() - birthDate.getFullYear();
            var mnth = today.getMonth() - birthDate.getMonth();
            if (mnth < 0 || (mnth === 0 && today.getDate() < birthDate.getDate())) {
                ageVal--;
            }
            return [ageVal, "true"];
        }
    }

    /*
     * Remove rider section from product json is no rider. 
     * Form rider object structue if riders are available.
     */
     // Removed from $scope
    function checkForRiders (product, productUIJson, successCallback) {

        if (typeof product.riders != 'undefined' && product.riders.length > 0) {
            $scope.hasRider = true;
            if ($scope.Illustration.Product.RiderDetails.length == 0) {
                for (var ridInx = 0; ridInx < product.riders.length; ridInx++) {
                    for (var insIndx in IllustratorVariables.insTypesForRiderObj) {
                        $scope.generateRiderObjects(product.riders[ridInx], IllustratorVariables.insTypesForRiderObj[insIndx]);
                    }
                }
            } else {
                for (var ridInx = 0; ridInx < product.riders.length; ridInx++) {
                    for (var insIndx in IllustratorVariables.insTypesForRiderObj) {
                        updateRiderObjects(product.riders[ridInx], IllustratorVariables.insTypesForRiderObj[insIndx]);
                    }
                }
            }
        } else {
            $scope.hasRider = false;
            productUIJson = $scope.customizeUIJson(productUIJson, 'riderDetailsCtrlGroup');
        }
        successCallback(productUIJson);
    }
    
    // Removed from $scope
    function getRiderNamesToDisplay (riderNameFromDB, insuredSelected) {
        var riderNameToDisplay;
        if (insuredSelected != IllustratorVariables.mainInsTypeName) {

            if ((insuredSelected.indexOf('Child') > -1) || (insuredSelected.indexOf('Parent') > -1)) {
                insuredSelected = insuredSelected.substring(0, insuredSelected.length - 1);
            }
            riderNameToDisplay = insuredSelected + ' ' + riderNameFromDB;
        } else {
            riderNameToDisplay = riderNameFromDB;
        }
        return riderNameToDisplay;
    }
    
    // Removed from $scope
    function getInsuredHierarchy (selectedInsured) {
        var insuredHierarchy;
        if (selectedInsured == IllustratorVariables.insTypesForRiderObj[0]) {
            insuredHierarchy = "LA1";
        }
        return insuredHierarchy;
    }
    /*
     * Function to generate rider object structure.
     * insSpecificRider - Contains a product specfic rider as per Product Service Response.
     * selectedInsured - Insured for which Rider Object to be formed (Parent1,Parent2,Child1,Child2,Child3,Spouse,Maininsured)
     * uniqueRiderName - same uniqueRiderName is formed in Rider.json, used in getRiderIndex method, which is used to identify exact rider onject from the array.
     */
    $scope.generateRiderObjects = function (insSpecificRider, selectedInsured) {
        var displayRiderName = getRiderNamesToDisplay(insSpecificRider.shortName, selectedInsured);
        var riderInformation = new Object();
        if (riderInformation[selectedInsured + insSpecificRider.planName] == undefined) {
            riderInformation[selectedInsured + insSpecificRider.planName] = new Object();
        }
        riderInformation[selectedInsured + insSpecificRider.planName].id = insSpecificRider.id;
        riderInformation[selectedInsured + insSpecificRider.planName].shortName = insSpecificRider.businessDescription;
        riderInformation[selectedInsured + insSpecificRider.planName].isDisabled = insSpecificRider.properties.isDisabled;
        riderInformation[selectedInsured + insSpecificRider.planName].riderCode = displayRiderName;
        riderInformation[selectedInsured + insSpecificRider.planName].riderPlanName = insSpecificRider.marketableName;
        riderInformation[selectedInsured + insSpecificRider.planName].sumInsured = "";
        riderInformation[selectedInsured + insSpecificRider.planName].initialPremium = "";
        riderInformation[selectedInsured + insSpecificRider.planName].policyTerm = "";
        riderInformation[selectedInsured + insSpecificRider.planName].isRequired = insSpecificRider.properties.riderIsRequired;
        riderInformation[selectedInsured + insSpecificRider.planName].insuredType = getInsuredHierarchy(selectedInsured);
        riderInformation[selectedInsured + insSpecificRider.planName].insured = selectedInsured;
        riderInformation[selectedInsured + insSpecificRider.planName].planName = insSpecificRider.planName;
        riderInformation[selectedInsured + insSpecificRider.planName].riderPremium = insSpecificRider.riderPremium;
        riderInformation[selectedInsured + insSpecificRider.planName].uniqueRiderName = selectedInsured + insSpecificRider.businessDescription;
        $scope.Illustration.Product.RiderDetails.push(riderInformation[selectedInsured + insSpecificRider.planName]);
    }


    /*
     * Function to update rider object structure.
     * insSpecificRider - Contains a product specfic rider as per Product Service Response.
     * selectedInsured - Insured for which Rider Object to be formed (Parent1,Parent2,Child1,Child2,Child3,Spouse,Maininsured)
     * uniqueRiderName - same uniqueRiderName is formed in Rider.json, used in getRiderIndex method, which is used to identify exact rider onject from the array.
     */
    // Removed from $scope
    function updateRiderObjects (insSpecificRider, selectedInsured) {
        var riderIndx = $scope.getRiderIndex(selectedInsured + insSpecificRider.planName);
        if ($scope.Illustration.Product.RiderDetails[riderIndx[0]]) {
            $scope.Illustration.Product.RiderDetails[riderIndx[0]].insuredType = getInsuredHierarchy(selectedInsured);
        }
    }

    // Customized method for painting product details
    $scope.customizeForProducts = function (product, productUIJson, successCallback) {
        var customizedProductJSON = {};
        $scope.customizeForPolicyDetails(product, productUIJson, function (customizedProductJSON) {
            checkForRiders(product, customizedProductJSON, function (customizedProductJSON) {
                successCallback(customizedProductJSON);
            });
        });
    }

    
    function custRiderJsonForMainIns  (mainInsRiders, riderJson) {
        $scope.hasMIRider = false;
        for (totRidrIndx in IllustratorVariables.totalRiderList) {
            for (mainRidrIndx in mainInsRiders) {
                if (IllustratorVariables.totalRiderList[totRidrIndx] == mainInsRiders[mainRidrIndx]) {
                    $scope.hasMIRider = true;
                    break;
                } else {
                    $scope.hasMIRider = false;
                }
            }
            if ($scope.hasMIRider == false) {
                riderJson = $scope.customizeUIJsonForRider(riderJson, ("Maininsured" + IllustratorVariables.totalRiderList[totRidrIndx]));
            }
        }
        return riderJson;
    }
    $scope.clearIndividualRiders = function (actaulRelRdier, riderJson) {
        $scope.delRider = false;
        for (riderIndex in $scope.Illustration.Product.RiderDetails) {
            if ($scope.Illustration.Product.RiderDetails[riderIndex].uniqueRiderName == actaulRelRdier) {
                $scope.Illustration.Product.RiderDetails[riderIndex].isRequired = 'No';
                $scope.Illustration.Product.RiderDetails[riderIndex].policyTerm = '';
                $scope.Illustration.Product.RiderDetails[riderIndex].sumInsured = '';
                $scope.delRider = true;
                break;
            }
        }
        return riderJson;
    }
    
    $scope.commonFunctionSuccess = function (output) {
        if ($scope.commonRiderName != '') {
            var sringnew = "Illustration.Product.RiderDetails[[$scope.getRiderIndex('" + $scope.commonRiderName + "')]]." + $scope.commonField;
            $scope.getValuenew(sringnew, output);
        } else {
            $scope.Illustration = output;
        }
        IllustratorVariables.setIllustratorModel($scope.Illustration);
        $scope.refresh();
        $timeout(function(){
			$rootScope.updateErrorCount($scope.viewToBeCustomized);
		}, 100);
        //call loadsummary only if value changes $scope.callLoadSummary is paased true from uijson only.
        /*if ($scope.callLoadSummary) {
            $scope.loadSummaryDetails(false);
        }*/
        $timeout(function() {
			$rootScope.updateErrorCount($scope.viewToBeCustomized);
		}, 100);
        $scope.callLoadSummary = false;
        $scope.refresh();
        
    }

    $scope.commonFunctionError = function (result) {
        
    }
    
    $scope.callForProduct = function(){
    	if($scope.Illustration.Product.ProductDetails.productId == '1010' && $scope.Illustration.Product.ProductDetails.productCode == '10'){            
    		  $scope.onChangeValidateValue();
    	}
        if($scope.Illustration.Product.ProductDetails.productId == '1097' && $scope.Illustration.Product.ProductDetails.productCode == '97'){            
    		  $scope.onChangeValidateValue();
    	}
    	if($scope.Illustration.Product.ProductDetails.productId == '1003' && $scope.Illustration.Product.ProductDetails.productCode == '3'){
    		$scope.Illustration.Product.policyDetails.isSumAssured = "Yes";
        	$scope.Illustration.Product.policyDetails.isPremium = "No";
        	$scope.loadSummaryDetails(false);
    	}
    	
    	if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4'){
            if($scope.Illustration.Product.policyDetails.sumInsured!==undefined && $scope.Illustration.Product.policyDetails.sumInsured!=="" && $scope.Illustration.Product.policyDetails.premiumPeriod!==undefined && $scope.Illustration.Product.policyDetails.premiumPeriod!==""){
    		$scope.Illustration.Product.policyDetails.isSumAssured = "Yes";
    		$scope.Illustration.Product.policyDetails.isPremium = "No";
            $scope.loadSummaryDetails(false);
    	   } 
    	} 
    	if($scope.Illustration.Product.ProductDetails.productId == '1220' && $scope.Illustration.Product.ProductDetails.productCode == '220'){
            if($scope.Illustration.Product.policyDetails.sumInsured!==undefined && $scope.Illustration.Product.policyDetails.sumInsured!=="" && $scope.Illustration.Product.policyDetails.premiumPeriod!==undefined && $scope.Illustration.Product.policyDetails.premiumPeriod!==""){
    		$scope.Illustration.Product.policyDetails.isSumAssured = "Yes";
    		$scope.Illustration.Product.policyDetails.isPremium = "No";
            $scope.loadSummaryDetails(false);
    	   } 
    	} 
        if($scope.Illustration.Product.ProductDetails.productId == '1006' && $scope.Illustration.Product.ProductDetails.productCode == '6'){
            if($scope.Illustration.Product.policyDetails.sumInsured!==undefined && $scope.Illustration.Product.policyDetails.sumInsured!=="" && $scope.Illustration.Product.policyDetails.premiumPeriod!==undefined && $scope.Illustration.Product.policyDetails.premiumPeriod!==""){
    		$scope.Illustration.Product.policyDetails.isSumAssured = "Yes";
    		$scope.Illustration.Product.policyDetails.isPremium = "No";
            $scope.loadSummaryDetails(false);
    	   } 
    	}
        if($scope.Illustration.Product.ProductDetails.productId == '1306' && $scope.Illustration.Product.ProductDetails.productCode == '306'){
            if($scope.Illustration.Product.policyDetails.sumInsured!==undefined && $scope.Illustration.Product.policyDetails.sumInsured!=="" && $scope.Illustration.Product.policyDetails.premiumPeriod!==undefined && $scope.Illustration.Product.policyDetails.premiumPeriod!==""){
    		$scope.Illustration.Product.policyDetails.isSumAssured = "Yes";
    		$scope.Illustration.Product.policyDetails.isPremium = "No";
            $scope.loadSummaryDetails(false);
    	   } 
    	}
        if($scope.Illustration.Product.ProductDetails.productId == '1274' && $scope.Illustration.Product.ProductDetails.productCode == '274'){
            if($scope.Illustration.Product.policyDetails.sumInsured!==undefined && $scope.Illustration.Product.policyDetails.sumInsured!=="" && $scope.Illustration.Product.policyDetails.premiumPeriod!==undefined && $scope.Illustration.Product.policyDetails.premiumPeriod!==""){
    		$scope.Illustration.Product.policyDetails.isSumAssured = "Yes";
    		$scope.Illustration.Product.policyDetails.isPremium = "No";
            $scope.loadSummaryDetails(false);
    	   } 
    	}
        if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4'){
			if ($scope.Illustration.Product.policyDetails.premiumPeriod == '25') {
				$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA25;
			}
			else {
				if($scope.Illustration.Product.policyDetails.premiumFrequency=="" || $scope.Illustration.Product.policyDetails.premiumFrequency==undefined){
					$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
				}else if($scope.Illustration.Product.policyDetails.premiumFrequency!=="Annual"){
					$scope.Illustration.Product.premiumSummary.summaryMinSA =IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSANotAnnual;
				}else{
					$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
				}
			}
        }
    }

    //execute rule function commonFunctions.Which wil return the sumasured and age values
    $scope.callChangeMethod = function (fileName, functionName, value, riderName, index, fieldTobePopulated, callLoadSummary) {
        $scope.callLoadSummary = callLoadSummary;
        $scope.commonRiderName = riderName;
        $scope.commonField = fieldTobePopulated;
        var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
                        
        var inputdataSample = angular.copy(value);

        if ((rootConfig.isDeviceMobile)) {
            if ($scope.Illustration && $scope.Illustration.Product && $scope.Illustration.Product.ProductDetails && $scope.Illustration.Product.ProductDetails.productCode == 10) {
                $timeout(function () {
                    GLI_RuleService.runCommonFunctionRule(transactionObj, $scope.commonFunctionSuccess, $scope.commonFunctionError, fileName, functionName, inputdataSample, riderName, index, fieldTobePopulated, callLoadSummary);
                }, 0);
            } else {
                GLI_RuleService.runCommonFunctionRule(transactionObj, $scope.commonFunctionSuccess, $scope.commonFunctionError, fileName, functionName, inputdataSample, riderName, index, fieldTobePopulated, callLoadSummary);
            }
        } else {
            if ($scope.Illustration && $scope.Illustration.Product && $scope.Illustration.Product.ProductDetails && $scope.Illustration.Product.ProductDetails.productCode == 10) {
                $timeout(function () {
                    GLI_RuleService.runCommonFunctionRule(transactionObj, $scope.commonFunctionSuccess, $scope.commonFunctionError, fileName, functionName, inputdataSample, riderName, index, fieldTobePopulated, callLoadSummary);
                }, 1500);
            } else {
                GLI_RuleService.runCommonFunctionRule(transactionObj, $scope.commonFunctionSuccess, $scope.commonFunctionError, fileName, functionName, inputdataSample, riderName, index, fieldTobePopulated, callLoadSummary);
            }
        }
    }
    
    $scope.loadDetailsFromRule = function(model){
    	if(model=="isPremium"){
			if($scope.Illustration.Product.policyDetails.premium!==undefined && $scope.Illustration.Product.policyDetails.premium!=="" && $scope.Illustration.Product.policyDetails.premiumPeriod!==undefined && $scope.Illustration.Product.policyDetails.premiumPeriod!==""){
				$scope.Illustration.Product.policyDetails.isPremium = "Yes";
				$scope.Illustration.Product.policyDetails.isSumAssured = "No";
				$scope.loadSummaryDetails(false);
			}
    	} 
    	if(model=="isSumAssured"){
			if($scope.Illustration.Product.policyDetails.sumInsured!==undefined && $scope.Illustration.Product.policyDetails.sumInsured!=="" && $scope.Illustration.Product.policyDetails.premiumPeriod!==undefined && $scope.Illustration.Product.policyDetails.premiumPeriod!==""){
				$scope.Illustration.Product.policyDetails.isSumAssured = "Yes";
				$scope.Illustration.Product.policyDetails.isPremium = "No";
				$scope.loadSummaryDetails(false);
			}
    	}	
    	
    }
    
    $scope.loadSummaryDetails = function(status){
		$rootScope.showHideLoadingImage(true, 'calculationProgress', $translate);
		var guideRuleInfo = {};
		var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
		if ((rootConfig.isDeviceMobile)) {
			var transactionData = $.parseJSON(transactionObj.TransactionData)
			transactionObj.TransactionData = transactionData;
		} 
		transactionObj.TransactionData = GLI_IllustratorService.convertToCanonicalInput(transactionObj.TransactionData, IllustratorVariables);
		guideRuleInfo.offlineDB = IllustratorVariables.productDetails.offlineDB[0].ruleReference;
		if(IllustratorVariables.productDetails.ruleSpec[0].ruleReference.indexOf('Guideline')>-1){
			guideRuleInfo.ruleName = IllustratorVariables.productDetails.ruleSpec[0].ruleReference;
			if(!(rootConfig.isDeviceMobile)){
				guideRuleInfo.ruleGroup = IllustratorVariables.productDetails.ruleSpec[0].group;
			}
		}
		// else if(IllustratorVariables.productDetails.ruleSpec[1].ruleReference.indexOf('Guideline')>-1){
        /*FNA changes by LE Team>> starts*/    
        else if(IllustratorVariables.productDetails.ruleSpec[1]!=undefined && IllustratorVariables.productDetails.ruleSpec[1].ruleReference.indexOf('Guideline')>-1){
        /*FNA changes by LE Team>> ends*/    
			guideRuleInfo.ruleName = IllustratorVariables.productDetails.ruleSpec[1].ruleReference;
			if(!(rootConfig.isDeviceMobile)){
				guideRuleInfo.ruleGroup = IllustratorVariables.productDetails.ruleSpec[1].group;
			}
		}
		//transactionObj.Key11 = "00007766";
		GLI_RuleService.runCustomRule(transactionObj, $scope.onGetSummarySuccess, $scope.onGetSummaryError, guideRuleInfo, "guideLine");
	}
    
    $scope.onGetSummarySuccess = function(summaryData){		
		$scope.Illustration.Product.policyDetails.sumInsured=$filter('formattedNumber')(summaryData.PolicyDetails.sumInsured);
		$scope.Illustration.Product.policyDetails.premium=$filter('formattedNumber')(summaryData.PolicyDetails.premium);
		
    	$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
        $scope.Illustration.Product.premiumSummary.summaryMaxSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].maxSA;
        
        if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4'){
			if ($scope.Illustration.Product.policyDetails.premiumPeriod == '25') {
				$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA25;
			}
			else {
				if($scope.Illustration.Product.policyDetails.premiumFrequency=="" || $scope.Illustration.Product.policyDetails.premiumFrequency==undefined){
					$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
				}else if($scope.Illustration.Product.policyDetails.premiumFrequency!=="Annual"){
					$scope.Illustration.Product.premiumSummary.summaryMinSA =IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSANotAnnual;
				}else{
					$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
				}
			}
        }
        
        if($scope.Illustration.Product.ProductDetails.productId == '1220' && $scope.Illustration.Product.ProductDetails.productCode == '220'){
            if($scope.Illustration.Product.policyDetails.premiumFrequency=="" || $scope.Illustration.Product.policyDetails.premiumFrequency==undefined){
                $scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
            }else if($scope.Illustration.Product.policyDetails.premiumFrequency!=="Annual"){
                $scope.Illustration.Product.premiumSummary.summaryMinSA =IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSANotAnnual;
            }else{
                $scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
            }
        }
        
        if(summaryData.PolicyDetails.sumAssured == "NaN"){
			$scope.Illustration.Product.policyDetails.sumInsured = 0;
		} else {
			$scope.Illustration.Product.policyDetails.sumInsured = summaryData.PolicyDetails.sumAssured;
		}
		if(summaryData.PolicyDetails.premium == "NaN"){
			$scope.Illustration.Product.policyDetails.premium = 0;
		} else {
			$scope.Illustration.Product.policyDetails.premium = summaryData.PolicyDetails.premium;
		}                
        if($scope.Illustration.Product.policyDetails.sumInsured!=""){
			$scope.loadonHospitalSales();
		}      
        if(summaryData.discountedPremium && summaryData.discountedPremium != "" && summaryData.discountedPremium != "NaN"){
        	$scope.Illustration.Product.premiumSummary.discountedPremium = summaryData.discountedPremium;
		} else {
        	$scope.Illustration.Product.premiumSummary.discountedPremium = "";
        }
		$timeout(function() {
			$rootScope.updateErrorCount($scope.viewToBeCustomized);
		}, 100);
		$scope.refresh();
		$rootScope.showHideLoadingImage(false);
	}

    $scope.onGetSummaryError = function(){
		$rootScope.showHideLoadingImage(false);
	}
    
	$scope.OnChaneValidation_ProLife = function(checkedOrUncheckedRaider){
		if($scope.Illustration.Product.ProductDetails.productId == 1004 && $scope.Illustration.Product.ProductDetails.productCode == 4) {
			if($scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].isRequired == 'Yes'){
				switch (checkedOrUncheckedRaider){
					case 'MaininsuredWPRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=$scope.Illustration.Product.policyDetails.sumInsured;
						break;
					case 'MaininsuredADBRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredRCCADBRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredADDRiders' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredRCCADDRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredAIRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredRCCAIRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;					
					case 'MaininsuredDDRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					default: 
						break;
				}
			} else if($scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].isRequired == 'No'){
				if(checkedOrUncheckedRaider == 'MaininsuredADBRider' || checkedOrUncheckedRaider == 'MaininsuredRCCADBRider') {			
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredADBRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredRCCADBRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredADBRider')], true);
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredRCCADBRider')], true);
				} else if (checkedOrUncheckedRaider == 'MaininsuredAIRider' || checkedOrUncheckedRaider == 'MaininsuredRCCAIRider'){
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredAIRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredRCCAIRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredAIRider')], true);
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredRCCAIRider')], true);
				} else if (checkedOrUncheckedRaider == 'MaininsuredADDRiders' || checkedOrUncheckedRaider == 'MaininsuredRCCADDRider'){
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredADDRiders',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredRCCADDRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredADDRiders')], true);
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredRCCADDRider')], true);
				}
			}
		}
		
		if($scope.Illustration.Product.ProductDetails.productId == 1006 && $scope.Illustration.Product.ProductDetails.productCode == 6) {
			if($scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].isRequired == 'Yes'){
				switch (checkedOrUncheckedRaider){
					case 'MaininsuredWPRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=$scope.Illustration.Product.policyDetails.sumInsured;
						break;
					case 'MaininsuredADBRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredRCCADBRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredADDRiders' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredRCCADDRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredAIRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredRCCAIRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;					
					case 'MaininsuredDDRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					default: 
						break;
				}
			} else if($scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].isRequired == 'No'){
				if(checkedOrUncheckedRaider == 'MaininsuredADBRider' || checkedOrUncheckedRaider == 'MaininsuredRCCADBRider') {			
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredADBRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredRCCADBRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredADBRider')], true);
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredRCCADBRider')], true);
				} else if (checkedOrUncheckedRaider == 'MaininsuredAIRider' || checkedOrUncheckedRaider == 'MaininsuredRCCAIRider'){
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredAIRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredRCCAIRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredAIRider')], true);
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredRCCAIRider')], true);
				} else if (checkedOrUncheckedRaider == 'MaininsuredADDRiders' || checkedOrUncheckedRaider == 'MaininsuredRCCADDRider'){
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredADDRiders',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredRCCADDRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredADDRiders')], true);
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredRCCADDRider')], true);
				}
			}
		}
		
		if($scope.Illustration.Product.ProductDetails.productId == 1220 && $scope.Illustration.Product.ProductDetails.productCode == 220) {
			if($scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].isRequired == 'Yes'){
				switch (checkedOrUncheckedRaider){
					case 'MaininsuredWPRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=$scope.Illustration.Product.policyDetails.sumInsured;
						break;
					case 'MaininsuredADBRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredRCCADBRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredADDRiders' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredRCCADDRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredAIRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredRCCAIRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;					
					case 'MaininsuredDDRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					default: 
						break;
				}
			} else if($scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].isRequired == 'No'){
				if(checkedOrUncheckedRaider == 'MaininsuredADBRider' || checkedOrUncheckedRaider == 'MaininsuredRCCADBRider') {			
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredADBRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredRCCADBRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredADBRider')], true);
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredRCCADBRider')], true);
				} else if (checkedOrUncheckedRaider == 'MaininsuredAIRider' || checkedOrUncheckedRaider == 'MaininsuredRCCAIRider'){
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredAIRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredRCCAIRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredAIRider')], true);
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredRCCAIRider')], true);
				} else if (checkedOrUncheckedRaider == 'MaininsuredADDRiders' || checkedOrUncheckedRaider == 'MaininsuredRCCADDRider'){
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredADDRiders',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredRCCADDRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredADDRiders')], true);
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredRCCADDRider')], true);
				}
			}
		}
		
		if($scope.Illustration.Product.ProductDetails.productId == 1306 && $scope.Illustration.Product.ProductDetails.productCode == 306) {
			if($scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].isRequired == 'Yes'){
				switch (checkedOrUncheckedRaider){
					case 'MaininsuredWPRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=$scope.Illustration.Product.policyDetails.sumInsured;
						break;
					case 'MaininsuredADBRider' : 
						if($scope.Illustration.Insured.BasicDetails.age <= IllustratorVariables.insuredAge.insuredMinimumAge){
							var adbSumInured = (3 * $scope.Illustration.Product.policyDetails.sumInsured)
							if(adbSumInured > Number(IllustratorVariables.GenSave10PlusSumInsuredADB.maximumSumInsuredAge16)){
								$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured = IllustratorVariables.GenSave10PlusSumInsuredADB.maximumSumInsuredAge16;
							} else {
								$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured = adbSumInured;
							}
						}
						if(($scope.Illustration.Insured.BasicDetails.age > IllustratorVariables.insuredAge.insuredMinimumAge) &&
						   ($scope.Illustration.Insured.BasicDetails.age <= IllustratorVariables.insuredAge.maxAge1306)){
							var maxadbSumInured = (3 * $scope.Illustration.Product.policyDetails.sumInsured)
							if(maxadbSumInured > Number(IllustratorVariables.GenSave10PlusSumInsuredADB.maximumSumInsuredAge64)){
								$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured = IllustratorVariables.GenSave10PlusSumInsuredADB.maximumSumInsuredAge64;
							} else {
								$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured = maxadbSumInured;
							}
						}
						break;
					case 'MaininsuredRCCADBRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredADDRiders' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredRCCADDRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredAIRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					case 'MaininsuredRCCAIRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;					
					case 'MaininsuredDDRider' : 
						$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured=100000;
						break;
					default: 
						break;
				}
			} else if($scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].isRequired == 'No'){
				if(checkedOrUncheckedRaider == 'MaininsuredADBRider') {			
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredADBRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredADBRider')], true);
				} else if(checkedOrUncheckedRaider == 'MaininsuredRCCADBRider') {			
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredRCCADBRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredRCCADBRider')], true);
				} else if (checkedOrUncheckedRaider == 'MaininsuredAIRider' || checkedOrUncheckedRaider == 'MaininsuredRCCAIRider'){
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredAIRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredRCCAIRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredAIRider')], true);
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredRCCAIRider')], true);
				} else if (checkedOrUncheckedRaider == 'MaininsuredADDRiders' || checkedOrUncheckedRaider == 'MaininsuredRCCADDRider'){
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredADDRiders',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredRCCADDRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredADDRiders')], true);
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredRCCADDRider')], true);
				}
			}
		}
		
		if($scope.Illustration.Product.ProductDetails.productId == 1274 && $scope.Illustration.Product.ProductDetails.productCode == 274) {
			if($scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].isRequired == 'Yes'){
				switch (checkedOrUncheckedRaider){
					case 'MaininsuredADBRider' : 
						if($scope.Illustration.Insured.BasicDetails.age <= IllustratorVariables.insuredAge.insuredMinimumAge){
							var adbSumInured = (3 * $scope.Illustration.Product.policyDetails.sumInsured)
							if(adbSumInured > Number(IllustratorVariables.GenSave4PlusSumInsuredADB.maximumSumInsuredAge16)){
								$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured = IllustratorVariables.GenSave4PlusSumInsuredADB.maximumSumInsuredAge16;
							} else {
								$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured = adbSumInured;
							}
						}
						if(($scope.Illustration.Insured.BasicDetails.age > IllustratorVariables.insuredAge.insuredMinimumAge) &&
						   ($scope.Illustration.Insured.BasicDetails.age <= IllustratorVariables.insuredAge.maxAge1274)){
							var maxadbSumInured = (3 * $scope.Illustration.Product.policyDetails.sumInsured)
							if(maxadbSumInured > Number(IllustratorVariables.GenSave4PlusSumInsuredADB.maximumSumInsuredAge64)){
								$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured = IllustratorVariables.GenSave4PlusSumInsuredADB.maximumSumInsuredAge64;
							} else {
								$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].sumInsured = maxadbSumInured;
							}
						}
						break;
					default: 
						break;
				}
			} else if($scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex(checkedOrUncheckedRaider,$scope.Illustration.Product.RiderDetails)]].isRequired == 'No'){
				if(checkedOrUncheckedRaider == 'MaininsuredADBRider') {			
					$scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredADBRider',$scope.Illustration.Product.RiderDetails)]].isRequired = 'No';
					$scope.clearOffData([$scope.getRiderIndex('MaininsuredADBRider')], true);
				}
			}
		}
	}
	
	
    $scope.onChangeValidateValue = function () {    
		if ($scope.Illustration.Product.ProductDetails.productId == '1010' && $scope.Illustration.Product.ProductDetails.productCode == '10') {
			if ($scope.Illustration.Product.policyDetails.packages == undefined) {
				$scope.Illustration.Product.policyDetails.sumInsured = "";
				$scope.Illustration.Product.policyDetails.premium = "";
				if ($scope.Illustration.Product.RiderDetails !== undefined) {
					for (var i = 0; i < $scope.Illustration.Product.RiderDetails.length; i++) {
						$scope.Illustration.Product.RiderDetails[i].sumInsured = "";
						$scope.Illustration.Product.RiderDetails[i].riderPremium = "";
					}
				}
			} else if (($scope.Illustration.Product.policyDetails.packages).indexOf('Package A') > -1) {
				$scope.Illustration.Product.policyDetails.sumInsured = 200000;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCIRider')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageA200000.CIRider;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADDRider')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageA200000.ADDRider;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRider')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageA200000.HSRider;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHBRider')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageA200000.HBRider;
			}
			else if (($scope.Illustration.Product.policyDetails.packages).indexOf('Package B') > -1) {
				$scope.Illustration.Product.policyDetails.sumInsured = 500000;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCIRider')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageB500000.CIRider;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADDRider')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageB500000.ADDRider;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRider')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageB500000.HSRider;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHBRider')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageB500000.HBRider;
			}
			else if (($scope.Illustration.Product.policyDetails.packages).indexOf('Package C') > -1) {
				$scope.Illustration.Product.policyDetails.sumInsured = 200000;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCIRider')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageC200000.CIRider;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADDRider')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageC200000.ADDRider;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRider')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageC200000.HSRider;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHBRider')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageC200000.HBRider;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageC200000.OPDRiders;

			}
			else if (($scope.Illustration.Product.policyDetails.packages).indexOf('Package D') > -1) {
				$scope.Illustration.Product.policyDetails.sumInsured = 500000;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCIRider')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageD500000.CIRider;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADDRider')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageD500000.ADDRider;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRider')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageD500000.HSRider;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHBRider')].sumInsured = IllustratorVariables.completeHealthPackageRiderPopulate.PackageD500000.HBRider;
			}  /*else if($scope.Illustration.Product.ProductDetails.productId == 1004 && $scope.Illustration.Product.ProductDetails.productCode == 4) {
				for(var k = 0 ; k<=$scope.Illustration.Product.RiderDetails.length-1; k++){
					$scope.Illustration.Product.RiderDetails[k].sumInsured = '';
					$scope.Illustration.Product.RiderDetails[k].isRequired = 'No';
				}
	 		}*/
            if($scope.Illustration.Product.RiderDetails!==undefined){
                for(var i=0;i<$scope.Illustration.Product.RiderDetails.length;i++){
                    $scope.Illustration.Product.RiderDetails[i].riderPremium="";
                }
            }
	        $rootScope.showHideLoadingImage(true, 'calculationProgress', $translate);
	        $scope.clearRiderValue = false;
	        IllustratorVariables.setIllustratorModel($scope.Illustration);
	        PersistenceMapping.clearTransactionKeys();
	        IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
	
	        var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
	        var ruleInfo = {};
	        if ((rootConfig.isDeviceMobile)) {
	            var transactionData = $.parseJSON(transactionObj.TransactionData)
	            transactionObj.TransactionData = transactionData;
	        }
	
	        //$rootScope.showHideLoadingImage(true, 'calculationProgress', $translate);
	
	        transactionObj.TransactionData = GLI_IllustratorService.convertToCanonicalInput(transactionObj.TransactionData, IllustratorVariables, 'validate');
	        ruleInfo.offlineDB = IllustratorVariables.productDetails.offlineDB[0].ruleReference;
	        
	        var illuDate = $scope.Illustration.Product.premiumSummary.validatedDate;
            var isExpired = GLI_IllustratorService.checkExpiryDate(illuDate);
            if (isExpired) {
                $rootScope.showHideLoadingImage(false);
                $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
                								 translateMessages($translate, "illustrator.expiryDateMessageIllustration"),
                								 translateMessages($translate, "illustrator.okMessageInCaps"));
                $rootScope.premiumSummaryOutput = false;

            } else {
                ruleInfo.ruleName = IllustratorVariables.productDetails.calcSpec[0].ruleReference;
                if (!(rootConfig.isDeviceMobile)) {
                    ruleInfo.ruleGroup = IllustratorVariables.productDetails.calcSpec[0].group;
                }
				if (!(transactionObj.TransactionData.Channel.id)) {
                    transactionObj.TransactionData.Channel.id = IllustratorVariables.channelId;
                    if (transactionObj.TransactionData.Channel.id == "9084") {
                        transactionObj.TransactionData.Channel.name = "Exim";
                    } else if (transactionObj.TransactionData.Channel.id == "9085") {
                        transactionObj.TransactionData.Channel.name = "TCB";
                    } else if (transactionObj.TransactionData.Channel.id == "9082") {
                        transactionObj.TransactionData.Channel.name = "Agency";
                    }
                }
				
				
                GLI_RuleService.runCustomRule(transactionObj, $scope.onGetValidationDataSuccess_common, $scope.onGetIllustrationError_common, ruleInfo, 'illustrator');
            }
	            //$timeout(callAtTimeout, 280);
	        $scope.Illustration.Product.premiumsummary="";
    	}
        //GenCancerSuperProtection
        	if($scope.Illustration.Product.ProductDetails.productId == '1097' && $scope.Illustration.Product.ProductDetails.productCode == '97'){
            if($scope.Illustration.Product.policyDetails.packages==undefined){
                $scope.Illustration.Product.policyDetails.sumInsured="";
                $scope.Illustration.Product.policyDetails.premium="";
                if($scope.Illustration.Product.RiderDetails!==undefined){
                for(var i=0;i<$scope.Illustration.Product.RiderDetails.length;i++){
                        $scope.Illustration.Product.RiderDetails[i].sumInsured="";
                        $scope.Illustration.Product.RiderDetails[i].riderPremium="";
                    }
                }
            }else if(($scope.Illustration.Product.policyDetails.packages).indexOf('Plan 1')>-1){
			$scope.Illustration.Product.policyDetails.sumInsured=700000;            						
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCNCRider')].sumInsured=IllustratorVariables.genCancerSuperProtectionPackageRiderPopulate.Plan1700000.CNCRider;
                
                $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredWPICRider')].sumInsured=IllustratorVariables.genCancerSuperProtectionPackageRiderPopulate.Plan1700000.WPICRider;
			
		}
		else if(($scope.Illustration.Product.policyDetails.packages).indexOf('Plan 2')>-1){
			$scope.Illustration.Product.policyDetails.sumInsured=1000000;			
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCNCRider')].sumInsured=IllustratorVariables.genCancerSuperProtectionPackageRiderPopulate.Plan21000000.CNCRider;
		                
           $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredWPICRider')].sumInsured=IllustratorVariables.genCancerSuperProtectionPackageRiderPopulate.Plan21000000.WPICRider; 
			
        }  
        else if(($scope.Illustration.Product.policyDetails.packages).indexOf('Plan 3')>-1){
			$scope.Illustration.Product.policyDetails.sumInsured=2000000;			
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCNCRider')].sumInsured=IllustratorVariables.genCancerSuperProtectionPackageRiderPopulate.Plan32000000.CNCRider;
		         
            
           $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredWPICRider')].sumInsured=IllustratorVariables.genCancerSuperProtectionPackageRiderPopulate.Plan32000000.WPICRider; 
			
        }
        else if(($scope.Illustration.Product.policyDetails.packages).indexOf('Plan 4')>-1){
			$scope.Illustration.Product.policyDetails.sumInsured=3000000;			
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCNCRider')].sumInsured=IllustratorVariables.genCancerSuperProtectionPackageRiderPopulate.Plan43000000.CNCRider;    
           
            
           $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredWPICRider')].sumInsured=IllustratorVariables.genCancerSuperProtectionPackageRiderPopulate.Plan43000000.WPICRider; 
			
        } 
                
        
            if($scope.Illustration.Product.RiderDetails!==undefined){
                for(var i=0;i<$scope.Illustration.Product.RiderDetails.length;i++){
                    $scope.Illustration.Product.RiderDetails[i].riderPremium="";
                }
            }
	        $rootScope.showHideLoadingImage(true, 'calculationProgress', $translate);
	        $scope.clearRiderValue = false;
	        IllustratorVariables.setIllustratorModel($scope.Illustration);
	        PersistenceMapping.clearTransactionKeys();
	        IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
	
	        var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
	        var ruleInfo = {};
	        if ((rootConfig.isDeviceMobile)) {
	            var transactionData = $.parseJSON(transactionObj.TransactionData)
	            transactionObj.TransactionData = transactionData;
	        }
	
	        //$rootScope.showHideLoadingImage(true, 'calculationProgress', $translate);
	
	        transactionObj.TransactionData = GLI_IllustratorService.convertToCanonicalInput(transactionObj.TransactionData, IllustratorVariables, 'validate');
	        ruleInfo.offlineDB = IllustratorVariables.productDetails.offlineDB[0].ruleReference;
	        
	        var illuDate = $scope.Illustration.Product.premiumSummary.validatedDate;
            var isExpired = GLI_IllustratorService.checkExpiryDate(illuDate);
            if (isExpired) {
                $rootScope.showHideLoadingImage(false);
                $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
                								 translateMessages($translate, "illustrator.expiryDateMessageIllustration"),
                								 translateMessages($translate, "illustrator.okMessageInCaps"));
                $rootScope.premiumSummaryOutput = false;

            } else {
                ruleInfo.ruleName = IllustratorVariables.productDetails.calcSpec[0].ruleReference;
                if (!(rootConfig.isDeviceMobile)) {
                    ruleInfo.ruleGroup = IllustratorVariables.productDetails.calcSpec[0].group;
                }
				if (!(transactionObj.TransactionData.Channel.id)) {
                    transactionObj.TransactionData.Channel.id = IllustratorVariables.channelId;
                    if (transactionObj.TransactionData.Channel.id == "9084") {
                        transactionObj.TransactionData.Channel.name = "Exim";
                    } else if (transactionObj.TransactionData.Channel.id == "9085") {
                        transactionObj.TransactionData.Channel.name = "TCB";
                    } else if (transactionObj.TransactionData.Channel.id == "9082") {
                        transactionObj.TransactionData.Channel.name = "Agency";
                    }
                }
				
				
                GLI_RuleService.runCustomRule(transactionObj, $scope.onGetValidationDataSuccess_common, $scope.onGetIllustrationError_common, ruleInfo, 'illustrator');
            }
	            $timeout(callAtTimeout, 280);
	        $scope.Illustration.Product.premiumsummary="";
    	}
    	if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4'){
    		for(var k = 0 ; k<=$scope.Illustration.Product.RiderDetails.length-1; k++){
				$scope.Illustration.Product.RiderDetails[k].sumInsured = '';
				$scope.Illustration.Product.RiderDetails[k].riderPremium="";
				$scope.Illustration.Product.RiderDetails[k].isRequired = 'No';
			}
            if($scope.Illustration.Product.policyDetails.sumInsured!==undefined && $scope.Illustration.Product.policyDetails.sumInsured!=="" && $scope.Illustration.Product.policyDetails.premiumPeriod!==undefined && $scope.Illustration.Product.policyDetails.premiumPeriod!==""){
            	$scope.Illustration.Product.policyDetails.isSumAssured = "Yes";
            	$scope.Illustration.Product.policyDetails.isPremium = "No";
            	$scope.loadSummaryDetails(false);
            }
			if($scope.Illustration.Product.policyDetails.premiumPeriod === '25' || $scope.Illustration.Product.policyDetails.premiumPeriod === undefined){
				$scope.Illustration.Product.policyDetails.tax = '0';
			}
    	}
    	if($scope.Illustration.Product.ProductDetails.productId == '1006' && $scope.Illustration.Product.ProductDetails.productCode == '6'){
    		for(var k = 0 ; k<=$scope.Illustration.Product.RiderDetails.length-1; k++){
				$scope.Illustration.Product.RiderDetails[k].sumInsured = '';
				$scope.Illustration.Product.RiderDetails[k].riderPremium="";
				$scope.Illustration.Product.RiderDetails[k].isRequired = 'No';
			}
            if($scope.Illustration.Product.policyDetails.sumInsured!==undefined && $scope.Illustration.Product.policyDetails.sumInsured!=="" && $scope.Illustration.Product.policyDetails.premiumPeriod!==undefined && $scope.Illustration.Product.policyDetails.premiumPeriod!==""){
            	$scope.Illustration.Product.policyDetails.isSumAssured = "Yes";
            	$scope.Illustration.Product.policyDetails.isPremium = "No";
            	$scope.loadSummaryDetails(false);
            }
    	}
    	if($scope.Illustration.Product.ProductDetails.productId == '1220' && $scope.Illustration.Product.ProductDetails.productCode == '220'){
    		for(var k = 0 ; k<=$scope.Illustration.Product.RiderDetails.length-1; k++){
				$scope.Illustration.Product.RiderDetails[k].sumInsured = '';
				$scope.Illustration.Product.RiderDetails[k].riderPremium="";
				$scope.Illustration.Product.RiderDetails[k].isRequired = 'No';
			}
            if($scope.Illustration.Product.policyDetails.sumInsured!==undefined && $scope.Illustration.Product.policyDetails.sumInsured!=="" && $scope.Illustration.Product.policyDetails.premiumPeriod!==undefined && $scope.Illustration.Product.policyDetails.premiumPeriod!==""){
            	$scope.Illustration.Product.policyDetails.isSumAssured = "Yes";
            	$scope.Illustration.Product.policyDetails.isPremium = "No";
            	$scope.loadSummaryDetails(false);
            }
    	}
    	if($scope.Illustration.Product.ProductDetails.productId == '1306' && $scope.Illustration.Product.ProductDetails.productCode == '306'){
    		for(var k = 0 ; k<=$scope.Illustration.Product.RiderDetails.length-1; k++){
				$scope.Illustration.Product.RiderDetails[k].sumInsured = '';
				$scope.Illustration.Product.RiderDetails[k].riderPremium="";
				$scope.Illustration.Product.RiderDetails[k].isRequired = 'No';
			}
            if($scope.Illustration.Product.policyDetails.sumInsured!==undefined && $scope.Illustration.Product.policyDetails.sumInsured!=="" && $scope.Illustration.Product.policyDetails.premiumPeriod!==undefined && $scope.Illustration.Product.policyDetails.premiumPeriod!==""){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].isRequired = 'Yes'
                $scope.Illustration.Product.policyDetails.isSumAssured = "Yes";
            	$scope.Illustration.Product.policyDetails.isPremium = "No";
            	$scope.loadSummaryDetails(false);
            }
    	}
    	if($scope.Illustration.Product.ProductDetails.productId == '1274' && $scope.Illustration.Product.ProductDetails.productCode == '274'){
    		for(var k = 0 ; k<=$scope.Illustration.Product.RiderDetails.length-1; k++){
				$scope.Illustration.Product.RiderDetails[k].sumInsured = '';
				$scope.Illustration.Product.RiderDetails[k].riderPremium="";
				$scope.Illustration.Product.RiderDetails[k].isRequired = 'No';
			}
            if($scope.Illustration.Product.policyDetails.sumInsured!==undefined && $scope.Illustration.Product.policyDetails.sumInsured!=="" && $scope.Illustration.Product.policyDetails.premiumPeriod!==undefined && $scope.Illustration.Product.policyDetails.premiumPeriod!==""){
            	$scope.Illustration.Product.policyDetails.isSumAssured = "Yes";
            	$scope.Illustration.Product.policyDetails.isPremium = "No";
            	$scope.loadSummaryDetails(false);
            }
    	}
    }
        
    $scope.customizeRiderJSON = function(product, riderJson){
		$scope.hasRiderValue = false;
		var selectedProductRiders = [];
		selectedProductRiders = IllustratorVariables.totalRiderList;
		for(z=0; z<selectedProductRiders.length; z++){
			var rider = selectedProductRiders[z];
			for (productRider in product.riders) {
				if(rider == product.riders[productRider].businessDescription){
					hasRiderValue = true;
					break;
				}
				else{
					hasRiderValue = false;
				}
			}
			if(hasRiderValue == false){
				riderJson = $scope.customizeUIJsonForRider(riderJson,"Maininsured"+rider);
			}
		}
		riderJson = $scope.riderSliceonAge(riderJson); 
		return riderJson;
	}
    
    $scope.riderSliceonAge = function(riderJson){
		var y = 0;
		$scope.checkInsuredAge = calculateAgeTH($scope.Illustration.Insured.BasicDetails.dob);
		$scope.insuredDays = getAgeByGivingDOB($scope.Illustration.Insured.BasicDetails.dob);
        if($scope.Illustration.Product.ProductDetails.productId == '1010' && $scope.Illustration.Product.ProductDetails.productCode == '10'){
            if($scope.insuredAge<11){
                riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredHBRider'));
                riderJson = $scope.clearIndividualRiders('MaininsuredHBRider', riderJson);
            }else if($scope.insuredAge>10){
                $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHBRider')].isRequired='Yes';
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHBRider')].riderPremium= $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHBRider')].riderPremium ? $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHBRider')].riderPremium : '';
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHBRider')].sumInsured= $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHBRider')].sumInsured ? $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHBRider')].sumInsured :'2000';
            }
        }
		$scope.mainInsuredADBRiderFlag = false;
		$scope.mainInsuredADDRidersFlag = false;
		$scope.mainInsuredHSRidersFlag = false;
		$scope.mainInuredPBRiderFlag = false;
		$scope.mainInuredAIRiderFlag = false;
		$scope.mainInsuredHBRidersFlag = false;
		$scope.mainInuredDDRiderFlag = false;
		$scope.mainInuredWPRiderFlag = false;
		$scope.mainInsuredOPDRidersFlag = false;
		if($scope.insuredDays=="days")
		{
			$scope.checkInsuredAge = 0;
			if(IllustratorVariables.riderAge.ADBRider[y].isMinAgeDays==true){
				$scope.insuredAgeDays = getAgeInDays($scope.Illustration.Insured.BasicDetails.dob);
				if($scope.insuredAgeDays<Number(IllustratorVariables.riderAge.ADBRider[y].minAge)){
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredADBRider'));
					riderJson = $scope.clearIndividualRiders('MaininsuredADBRider', riderJson);
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredRCCADBRider'));
					riderJson = $scope.clearIndividualRiders('MaininsuredRCCADBRider', riderJson);
					$scope.mainInsuredADBRiderFlag = true;
				}
			}
			if($scope.mainInsuredADBRiderFlag == false){
				if($scope.checkInsuredAge>Number(IllustratorVariables.riderAge.ADBRider[y].maxAge)){
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredADBRider'));
					riderJson = $scope.clearIndividualRiders('MaininsuredADBRider', riderJson);
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredRCCADBRider'));
					riderJson = $scope.clearIndividualRiders('MaininsuredRCCADBRider', riderJson);
				}
			}

			if(IllustratorVariables.riderAge.ADDRiders[y].isMinAgeDays==true){
				$scope.insuredAgeDays = getAgeInDays($scope.Illustration.Insured.BasicDetails.dob);
				if($scope.insuredAgeDays<Number(IllustratorVariables.riderAge.ADDRiders[y].minAge)){
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredADDRiders'));
					riderJson = $scope.clearIndividualRiders('MaininsuredADDRiders', riderJson);
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredRCCADDRider'));
					riderJson = $scope.clearIndividualRiders('MaininsuredRCCADDRider', riderJson);
					$scope.mainInsuredADDRidersFlag = true;
				}
			}
			if($scope.mainInsuredADDRidersFlag == false){
				if($scope.checkInsuredAge>Number(IllustratorVariables.riderAge.ADDRiders[y].maxAge)){
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredADDRiders'));
					riderJson = $scope.clearIndividualRiders('MaininsuredADDRiders', riderJson);
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredRCCADDRider'));
					riderJson = $scope.clearIndividualRiders('MaininsuredRCCADDRider', riderJson);
				}
			}
			if(IllustratorVariables.riderAge.HSRiders[y].isMinAgeDays==true){
				$scope.insuredAgeDays = getAgeInDays($scope.Illustration.Insured.BasicDetails.dob);
				if($scope.insuredAgeDays<Number(IllustratorVariables.riderAge.HSRiders[y].minAge)){
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredHSRiders'));
					riderJson = $scope.clearIndividualRiders('MaininsuredHSRiders', riderJson);
					$scope.mainInsuredHSRidersFlag = true;
				}
			}
			if($scope.mainInsuredHSRidersFlag == false){
				if($scope.checkInsuredAge>Number(IllustratorVariables.riderAge.HSRiders[y].maxAge)){
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredHSRiders'));
					riderJson = $scope.clearIndividualRiders('MaininsuredHSRiders', riderJson);
				}
			}
			if(IllustratorVariables.riderAge.OPDRiders[y].isMinAgeDays==true){
				$scope.insuredAgeDays = getAgeInDays($scope.Illustration.Insured.BasicDetails.dob);
				if($scope.insuredAgeDays<Number(IllustratorVariables.riderAge.OPDRiders[y].minAge)){
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredOPDRiders'));
					riderJson = $scope.clearIndividualRiders('MaininsuredOPDRiders', riderJson);
					$scope.mainInsuredOPDRidersFlag = true;
				}
			}
			if($scope.mainInsuredOPDRidersFlag == false){
				if($scope.checkInsuredAge>Number(IllustratorVariables.riderAge.OPDRiders[y].maxAge)){
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredOPDRiders'));
					riderJson = $scope.clearIndividualRiders('MaininsuredOPDRiders', riderJson);
				}
			}
			if(IllustratorVariables.riderAge.PBRider[y].isMinAgeDays==true){
				$scope.insuredAgeDays = getAgeInDays($scope.Illustration.Insured.BasicDetails.dob);
				if($scope.insuredAgeDays<Number(IllustratorVariables.riderAge.PBRider[y].minAge)){
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredPBRider'));
					riderJson = $scope.clearIndividualRiders('MaininsuredPBRider', riderJson);
					$scope.mainInuredPBRiderFlag = true;
				}
			}
			if($scope.mainInuredPBRiderFlag == false){
				if($scope.checkInsuredAge>=Number(IllustratorVariables.riderAge.PBRider[y].maxAge)){
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredPBRider'));
					riderJson = $scope.clearIndividualRiders('MaininsuredPBRider', riderJson);
				}
			}
			if($scope.checkInsuredAge<Number(IllustratorVariables.riderAge.AIRider[y].minAge)){
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredAIRider'));
				riderJson = $scope.clearIndividualRiders('MaininsuredAIRider', riderJson);
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredRCCAIRider'));
				riderJson = $scope.clearIndividualRiders('MaininsuredRCCAIRider', riderJson);
			}
			if($scope.checkInsuredAge<Number(IllustratorVariables.riderAge.HBRiders[y].minAge)){
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredHBRiders'));
				riderJson = $scope.clearIndividualRiders('MaininsuredHBRiders', riderJson);
			}
			if($scope.checkInsuredAge<Number(IllustratorVariables.riderAge.DDRider[y].minAge)){
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredDDRider'));
				riderJson = $scope.clearIndividualRiders('MaininsuredDDRider', riderJson);
			}
			if($scope.checkInsuredAge<Number(IllustratorVariables.riderAge.WPRider[y].minAge)){
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredWPRider'));
				riderJson = $scope.clearIndividualRiders('MaininsuredWPRider', riderJson);
			}
		} else {
			if($scope.checkInsuredAge>Number(IllustratorVariables.riderAge.ADBRider[y].maxAge)){
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredADBRider'));
				riderJson = $scope.clearIndividualRiders('MaininsuredADBRider', riderJson);
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredRCCADBRider'));
				riderJson = $scope.clearIndividualRiders('MaininsuredRCCADBRider', riderJson);
			}
			if($scope.checkInsuredAge>Number(IllustratorVariables.riderAge.ADDRiders[y].maxAge)){
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredADDRiders'));
				riderJson = $scope.clearIndividualRiders('MaininsuredADDRiders', riderJson);
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredRCCADDRider'));
				riderJson = $scope.clearIndividualRiders('MaininsuredRCCADDRider', riderJson);
			}
			if($scope.checkInsuredAge>Number(IllustratorVariables.riderAge.HSRiders[y].maxAge)){
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredHSRiders'));
				riderJson = $scope.clearIndividualRiders('MaininsuredHSRiders', riderJson);
			}
			if($scope.checkInsuredAge>Number(IllustratorVariables.riderAge.OPDRiders[y].maxAge)){
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredOPDRiders'));
				riderJson = $scope.clearIndividualRiders('MaininsuredOPDRiders', riderJson);
			}
			if($scope.checkInsuredAge>Number(IllustratorVariables.riderAge.PBRider[y].maxAge)){
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredPBRider'));
				riderJson = $scope.clearIndividualRiders('MaininsuredPBRider', riderJson);
			}
			if($scope.checkInsuredAge<Number(IllustratorVariables.riderAge.AIRider[y].minAge)){
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredAIRider'));
				riderJson = $scope.clearIndividualRiders('MaininsuredAIRider', riderJson);
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredRCCAIRider'));
				riderJson = $scope.clearIndividualRiders('MaininsuredRCCAIRider', riderJson);
				$scope.mainInuredAIRiderFlag = true;
			}
			if($scope.mainInuredAIRiderFlag == false){
				if($scope.checkInsuredAge>Number(IllustratorVariables.riderAge.AIRider[y].maxAge)){
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredAIRider'));
					riderJson = $scope.clearIndividualRiders('MaininsuredAIRider', riderJson);
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredRCCAIRider'));
					riderJson = $scope.clearIndividualRiders('MaininsuredRCCAIRider', riderJson);
				}
			}
			if($scope.checkInsuredAge<Number(IllustratorVariables.riderAge.HBRiders[y].minAge)){
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredHBRiders'));
				riderJson = $scope.clearIndividualRiders('MaininsuredHBRiders', riderJson);
				$scope.mainInsuredHBRidersFlag = true;
			}
			if($scope.mainInsuredHBRidersFlag == false){
				if($scope.checkInsuredAge>Number(IllustratorVariables.riderAge.HBRiders[y].maxAge)){
					rriderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredHBRiders'));
					riderJson = $scope.clearIndividualRiders('MaininsuredHBRiders', riderJson);
				}
			}
			if($scope.checkInsuredAge<Number(IllustratorVariables.riderAge.DDRider[y].minAge)){
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredDDRider'));
				riderJson = $scope.clearIndividualRiders('MaininsuredDDRider', riderJson);
				$scope.mainInuredDDRiderFlag = true;
			}
			if($scope.mainInuredDDRiderFlag == false){
				if($scope.checkInsuredAge>Number(IllustratorVariables.riderAge.DDRider[y].maxAge)){
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredDDRider'));
					riderJson = $scope.clearIndividualRiders('MaininsuredDDRider', riderJson);
				}
			}
			if($scope.checkInsuredAge<Number(IllustratorVariables.riderAge.WPRider[y].minAge)){
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredWPRider'));
				riderJson = $scope.clearIndividualRiders('MaininsuredWPRider', riderJson);
				$scope.mainInuredWPRiderFlag = true;
			}
			if($scope.mainInuredWPRiderFlag == false){
				if($scope.checkInsuredAge>Number(IllustratorVariables.riderAge.WPRider[y].maxAge)){
					riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredWPRider'));
					riderJson = $scope.clearIndividualRiders('MaininsuredWPRider', riderJson);
				}
			}
			if($scope.checkInsuredAge>=Number(IllustratorVariables.riderAge.PBRider[y].maxAge)){
				riderJson = $scope.customizeUIJsonForRider(riderJson,('MaininsuredPBRider'));
				riderJson = $scope.clearIndividualRiders('MaininsuredPBRider', riderJson);
			}
		}
		return riderJson;
	}
    
    $scope.customizeUIJsonForRider = function (productUIJson, spliceItemID) {
        loop: for (i = 0; i < productUIJson.Views.length; i++) {
            for (j = 0; j < productUIJson.Views[i].sections.length; j++) {
                if (productUIJson.Views[i].sections[j].spliceId == spliceItemID) {
                    productUIJson.Views[i].sections.splice(j, 1);
                    break loop;
                }
                for (k = 0; k < productUIJson.Views[i].sections[j].subsections.length; k++) {
                    if (productUIJson.Views[i].sections[j].subsections[k].spliceId == spliceItemID) {
                        productUIJson.Views[i].sections[j].subsections.splice(k, 1);
                        break loop;
                    }
                    for (l = 0; l < productUIJson.Views[i].sections[j].subsections[k].controlGroups.length; l++) {
                        if (productUIJson.Views[i].sections[j].subsections[k].controlGroups[l].spliceId == spliceItemID) {
                            productUIJson.Views[i].sections[j].subsections[k].controlGroups.splice(l, 1);
                            if (productUIJson.Views[i].sections[j].subsections[k].controlGroups.length == 0) {
                                productUIJson.Views[i].sections[j].subsections.splice(k, 1);
                            }
                            break loop;
                        }
                        for (m = 0; m < productUIJson.Views[i].sections[j].subsections[k].controlGroups[l].controls.length; m++) {
                            if (productUIJson.Views[i].sections[j].subsections[k].controlGroups[l].controls[m].spliceId == spliceItemID) {
                                productUIJson.Views[i].sections[j].subsections[k].controlGroups[l].controls.splice(m, 1);
                                break loop;
                            }
                            for (n = 0; n < productUIJson.Views[i].sections[j].subsections[k].controlGroups[l].controls[m].tableValues.length; n++) {
                                if (productUIJson.Views[i].sections[j].subsections[k].controlGroups[l].controls[m].tableValues[n].spliceId == spliceItemID) {
                                    productUIJson.Views[i].sections[j].subsections[k].controlGroups[l].controls[m].tableValues.splice(n, 1);
                                    break loop;
                                }
                            }
                        }

                    }
                }
            }
        }
        return productUIJson;
    }


    $scope.callChangeMethodForSI = function (fileName, functionName, value, riderName, index, fieldTobePopulated, callLoadSummary) {
        $scope.callLoadSummary = callLoadSummary;
        $scope.commonRiderName = riderName;
        $scope.commonField = fieldTobePopulated;
        var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
       
        if ((rootConfig.isDeviceMobile)) {
            //$timeout(function(){
            GLI_RuleService.runCommonFunctionRule(transactionObj, $scope.commonFunctionSuccess, $scope.commonFunctionError, fileName, functionName, value, riderName, index, fieldTobePopulated, callLoadSummary);
            //  },1500);
        } else {
            //$timeout(function(){
            GLI_RuleService.runCommonFunctionRule(transactionObj, $scope.commonFunctionSuccess, $scope.commonFunctionError, fileName, functionName, value, riderName, index, fieldTobePopulated, callLoadSummary);
            //},1000);
        }
        $scope.refresh();

    }
    
    $scope.populateIndependentLookupDateInScope = function (type, fieldName, model, setDefault, field, successCallBack, errorCallBack,cacheable) {
	if (!cacheable){
						cacheable = false;
					}
        $scope[fieldName] = [{
            "key": "",
            "value": "loading ...."
					               }];
        var options = {};
        options.type = type;
        DataLookupService.getLookUpData(options, function (data) {
            $scope[fieldName] = data;
            $rootScope[fieldName] = data;
            if (setDefault) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isDefault == 1) {
                        if (type == "NATIONALITY") {
                            if ($scope.LmsModel.Lead.BasicDetails.nationality == "") {
                                $scope.setValuetoScope(model, data[i].value);
                                break;
                            } else {
                                $scope.fromReset = false;
                            }
                        } else {
                            $scope.setValuetoScope(model, data[i].value);
                            break;
                        }
                    }
                }
            } else {
                if (model) {
                    var n = "$scope." + model;
                    var m = $scope.evaluateString(model);
                    //var m = eval (n);
                    if (m) {
                        $scope.setValuetoScope(model, m);
                    }
                }
            }
            $scope.refresh();
            successCallBack();
        }, function (errorData) {
            $scope[fieldName] = [];

        },cacheable);
    }

    // Extended initialLoad function from parent controller.
    $scope.initialLoad = function () {
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6']){
        	localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
        }
		if($routeParams.productId == '1010'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_10#PersonalDetails_6');
			} 
		} else if($routeParams.productId == '1003'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_3#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1097'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_97#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_97#PersonalDetails_6');
			} 
		} else if($routeParams.productId == '1004'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_4#PersonalDetails_6');
			} 
		} else if($routeParams.productId == '1006'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_6#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1220'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_220#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1306'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_306#PersonalDetails_6');
			}
		} else if($routeParams.productId == '1274'){
			if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6']){
				localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_PersonalDetails_274#PersonalDetails_6');
			}
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
		}
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
		} 
		if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6']){
			localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_callDetailsSection#popupContent_6');
		}  
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6']) {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#leadDetailContent_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6']) {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_leadDetailsSection#popupContent_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6']) {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#leadDetail_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6']) {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_popupTabsNavbar#popupTabs_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']) {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
        }    
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4']) {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_FNAList#fnaList_4');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6'])  {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_HeaderDetails#HeaderDetails_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6']) {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_filterSection#filterSection_6');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5']) {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsEducation#fnaCalculatorTabsEducation_5');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5']) {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsHealth#fnaCalculatorTabsHealth_5');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5']) {
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsProtectionGoal#fnaCalculatorTabsProtectionGoal_5');
        }
        if(localStorage['GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5']){
            localStorage.removeItem('GeneraliThailand_'+rootConfig.majorVersion+'.'+rootConfig.minorVersion+'_UI_fnaCalculatorTabsRetirementGoal#fnaCalculatorTabsRetirementGoal_5');
        }
		
		//OPD Rider deselct check 
		if ($scope.Illustration.Product.ProductDetails.productId != '1010' && $scope.Illustration.Product.ProductDetails.productCode != '10') {
			if ($scope.Illustration.Product.RiderDetails.length > 0) {
				$scope.onDisableOPDRider();
			}
		}

		LEDynamicUI.getUIJson(rootConfig.illustrationConfigJson, false, function (illustrationConfigJson) {
            IllustratorVariables.totalRiderList = illustrationConfigJson.productRidersList;
            IllustratorVariables.completeHealthPackageRiderPopulate = illustrationConfigJson.completeHealthPackageRiderPopulate;
            IllustratorVariables.genCancerSuperProtectionPackageRiderPopulate = illustrationConfigJson.genCancerSuperProtectionPackageRiderPopulate;
			//ratorVariables.genCancerSuperProtectionPackageRiderPopulate = illustrationConfigJson.genCancerSuperProtectionPackageRiderPopulate;
            //Riders applicable for Multiple Insured are configured in the illustrationConfigJson.ridersForMutlipleInsured
            IllustratorVariables.multipleInsuredRiders = illustrationConfigJson.ridersForMutlipleInsured;

            //Adding Illustration Output Constant values from JSON to Illustrator variables
            IllustratorVariables.staticTableDetails = illustrationConfigJson.illustrationOutputStaticTables;

            IllustratorVariables.riderMatrix = illustrationConfigJson.riderMatrix;

            IllustratorVariables.riderAge = illustrationConfigJson.riderAge;
            IllustratorVariables.insTypesForRiderObj = illustrationConfigJson.insForRiderObj;
            IllustratorVariables.insuredList = illustrationConfigJson.insuredList;
            IllustratorVariables.productCodeCI = illustrationConfigJson.productCodeCI;
            IllustratorVariables.productSummaryValueList = illustrationConfigJson.productSummaryValues;
            IllustratorVariables.insuredAge = illustrationConfigJson.insuredAge;
            IllustratorVariables.GenSave10PlusSumInsuredADB = illustrationConfigJson.GenSave10PlusSumInsuredADB;
            IllustratorVariables.GenSave4PlusSumInsuredADB = illustrationConfigJson.GenSave4PlusSumInsuredADB;
            LEDynamicUI.getUIJson(IllustratorVariables.illustrationInputTemplate, false, function (productUIJson) {
                var customizedProductObject = productUIJson;
                productUIJson = $scope.changeViewId(productUIJson);
                var productServiceResponse = JSON.parse(JSON.stringify(IllustratorVariables.productDetails));
                $scope.productSummaryDetails($scope.Illustration.Product.ProductDetails.productId);
                $scope.customizeForProducts(productServiceResponse, productUIJson, function (customisedReturnObj) {
                    customizedProductObject = customisedReturnObj;
                    $scope.cancerRdrDisbldForGenpro();
                    if($scope.Illustration.Product.policyDetails.tax == undefined || $scope.Illustration.Product.policyDetails.tax == ""){
                        $scope.Illustration.Product.policyDetails.tax='0';
                    }
					if ($scope.Illustration.Product.ProductDetails.productId == '1010' && $scope.Illustration.Product.ProductDetails.productCode == '10') {
						checkUncheckMethod();
					}
                    /*
                     * Paint product specific
                     * controls according to the
                     * selection of products from
                     * product basket.
                     */
                    $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
                    LEDynamicUI.paintUIFromJsonObject(rootConfig.template, customizedProductObject, $scope.viewToBeCustomized, "#ProductDetails", true, function (callBackId) {
                        //$scope.refresh();
                        //$rootScope.updateErrorCount(callBackId);
                        LEDynamicUI.paintUI(rootConfig.template, rootConfig.illustratorUIJson, "HeaderDetails", "#HeaderDetails", true, function () {
                            if (typeof IllustratorVariables.productDetails.riders != 'undefined' && IllustratorVariables.productDetails.riders.length > 0) {
                            	LEDynamicUI.getUIJson(rootConfig.riderJson, false, function (riderJson) {
                                   
										var customizedRiderJson = $scope.customizeRiderJSON(productServiceResponse, riderJson);
                                    	//var customizedRiderJson = riderJson;
                                        var riderDivId = "riders";
                                        LEDynamicUI.paintUIFromJsonObject(rootConfig.template, customizedRiderJson, riderDivId, "#" + riderDivId, true, function (callBackId) {
                                            //$scope.refresh();
                                        	 $scope.onHospitalBenefitPopulate();
                                        	 $scope.onHospitalSalesPopulate();
											 if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4' ||
                                        	    $scope.Illustration.Product.ProductDetails.productId == '1006' && $scope.Illustration.Product.ProductDetails.productCode == '6' ||
                                        		$scope.Illustration.Product.ProductDetails.productId == '1220' && $scope.Illustration.Product.ProductDetails.productCode == '220'||
                                        		$scope.Illustration.Product.ProductDetails.productId == '1306' && $scope.Illustration.Product.ProductDetails.productCode == '306'){
                                        		 	$scope.onPopulateOPDRider();
                                        	 }
                                            if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4'){
												if ($scope.Illustration.Product.policyDetails.premiumPeriod == '25') {
													$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA25;
												}
												else {
													if($scope.Illustration.Product.policyDetails.premiumFrequency=="" || $scope.Illustration.Product.policyDetails.premiumFrequency==undefined){
														$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
													}else if($scope.Illustration.Product.policyDetails.premiumFrequency!=="Annual"){
														$scope.Illustration.Product.premiumSummary.summaryMinSA =IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSANotAnnual;
													}else{
														$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
													}
												}
                                            }
                                            if($scope.Illustration.Product.ProductDetails.productId == '1220' && $scope.Illustration.Product.ProductDetails.productCode == '220'){
                                                if($scope.Illustration.Product.policyDetails.premiumFrequency=="" || $scope.Illustration.Product.policyDetails.premiumFrequency==undefined){
                                                    $scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
                                                }else if($scope.Illustration.Product.policyDetails.premiumFrequency!=="Annual"){
                                                    $scope.Illustration.Product.premiumSummary.summaryMinSA =IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSANotAnnual;
                                                }else{
                                                    $scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
                                                }
                                            }
											 if(($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4') || 
												($scope.Illustration.Product.ProductDetails.productId == '1006' && $scope.Illustration.Product.ProductDetails.productCode == '6') || 
												($scope.Illustration.Product.ProductDetails.productId == '1220' && $scope.Illustration.Product.ProductDetails.productCode == '220') || 
												($scope.Illustration.Product.ProductDetails.productId == '1306' && $scope.Illustration.Product.ProductDetails.productCode == '306')){
	                                        	 if(typeof $scope.Illustration.Product != "undefined" && $scope.Illustration.Product != ""){
	                                 				if(typeof $scope.Illustration.Product.RiderDetails != "undefined" && $scope.Illustration.Product.RiderDetails !=""){
	                                 					if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHBRiders')].sumInsured != "undefined" && 
	                                 					   $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHBRiders')].sumInsured != ""){
	                                 						if ($scope.evaluateString($scope.viewToBeCustomized + '.' + 'MaininsuredHBRidersSA') != undefined) {
	                                 							$scope.evaluateString($scope.viewToBeCustomized+'.'+'MaininsuredHBRidersSA').$render();
	                                 						}
	                                 					}
	                                 					if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].sumInsured != "undefined" && 
 	                                 					   $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].sumInsured != ""){
 	                                 						if ($scope.evaluateString($scope.viewToBeCustomized + '.' + 'MaininsuredHSRidersSA') != undefined) {
 	                                 							$scope.evaluateString($scope.viewToBeCustomized+'.'+'MaininsuredHSRidersSA').$render();
 	                                 							$scope.initiallyDisabled = false;
 	                                 						}
 	                                 					}
	                                 				}
	                                        	 }
                                 			} 
											 
											 if (($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4') ||
												 ($scope.Illustration.Product.ProductDetails.productId == '1006' && $scope.Illustration.Product.ProductDetails.productCode == '6') ||
												 ($scope.Illustration.Product.ProductDetails.productId == '1220' && $scope.Illustration.Product.ProductDetails.productCode == '220') ||
												 ($scope.Illustration.Product.ProductDetails.productId == '1306' && $scope.Illustration.Product.ProductDetails.productCode == '306')) {
												 if(typeof $scope.Illustration.Product != "undefined" && $scope.Illustration.Product != ""){
													 if(typeof $scope.Illustration.Product.RiderDetails != "undefined" && $scope.Illustration.Product.RiderDetails !=""){
														 if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].sumInsured != "undefined" && 
															$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].sumInsured != ""){
	                                 						 if ($scope.evaluateString($scope.viewToBeCustomized + '.' + 'MaininsuredOPDRidersSA') != undefined) {
	                                 							 $scope.evaluateString($scope.viewToBeCustomized+'.'+'MaininsuredOPDRidersSA').$render();
	                                 						 }
	                                 					}
													 }
												 }
											 }

											 
                                            //$rootScope.updateErrorCount(callBackId);
                                            // Need To Change.                                         	
                                            if(typeof $scope.Illustration.Product.policyDetails.premiumPeriod =='undefined' || $scope.Illustration.Product.policyDetails.premiumPeriod==undefined || $scope.Illustration.Product.policyDetails.premiumPeriod==''){
                                            	if($scope.Illustration.Product.ProductDetails.productId == '1010' && $scope.Illustration.Product.ProductDetails.productCode == '10'){
                                            		$scope.Illustration.Product.policyDetails.premiumPeriod='80';
                                            	}
                                                if($scope.Illustration.Product.ProductDetails.productId == '1097' && $scope.Illustration.Product.ProductDetails.productCode == '97'){
                                            		$scope.Illustration.Product.policyDetails.premiumPeriod='85';
                                            	}
                                            	if($scope.Illustration.Product.ProductDetails.productId == '1003' && $scope.Illustration.Product.ProductDetails.productCode == '3'){
                                            		$scope.Illustration.Product.policyDetails.premiumPeriod='8';
                                            	}	
                                                if($scope.Illustration.Product.ProductDetails.productId == '1006' && $scope.Illustration.Product.ProductDetails.productCode == '6'){
                                            		$scope.Illustration.Product.policyDetails.premiumPeriod='20';
                                            	}
                                                if($scope.Illustration.Product.ProductDetails.productId == '1220' && $scope.Illustration.Product.ProductDetails.productCode == '220'){
                                            		$scope.Illustration.Product.policyDetails.premiumPeriod='5';
                                            	}
                                                if($scope.Illustration.Product.ProductDetails.productId == '1306' && $scope.Illustration.Product.ProductDetails.productCode == '306'){
                                            		$scope.Illustration.Product.policyDetails.premiumPeriod='10';
                                            		$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].isRequired = 'Yes'
                                            		$scope.defaultRiderDisabled = true;
                                            	}
                                                if($scope.Illustration.Product.ProductDetails.productId == '1274' && $scope.Illustration.Product.ProductDetails.productCode == '274'){
                                            		$scope.Illustration.Product.policyDetails.premiumPeriod='4';
                                            		$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].isRequired = 'Yes'
                                                	$scope.defaultRiderDisabled = true;
                                            	}
                                                
                                            	if ($scope.evaluateString($scope.viewToBeCustomized + '.' + 'premiumPeriod') != undefined) {
                                                    $scope.evaluateString($scope.viewToBeCustomized + '.' + 'premiumPeriod').$render();
                                                    if(typeof $scope.Illustration.Product.policyDetails.premiumFrequency =='undefined' || $scope.Illustration.Product.policyDetails.premiumFrequency==undefined || $scope.Illustration.Product.policyDetails.premiumFrequency==''){
                        								$scope.Illustration.Product.policyDetails.premiumFrequency='Annual';
                        							}
                                                    if ($scope.evaluateString($scope.viewToBeCustomized + '.' + 'premiumFrequency') != undefined) {
                                                        $scope.evaluateString($scope.viewToBeCustomized + '.' + 'premiumFrequency').$render();
                                                    }
                                                    if($scope.Illustration.Product.ProductDetails.productId == '1010' && $scope.Illustration.Product.ProductDetails.productCode == '10'){
														if(typeof $scope.Illustration.Product.policyDetails.premiumFrequency !='undefined' || $scope.Illustration.Product.policyDetails.premiumFrequency!=undefined || $scope.Illustration.Product.policyDetails.premiumFrequency!=''){
															
															if ($scope.evaluateString($scope.viewToBeCustomized + '.' + 'packages') != undefined) {
																$scope.evaluateString($scope.viewToBeCustomized + '.' + 'packages').$render();
																$scope.callChangeMethod('CommonFunctions','onRegularFrequencyChange',$scope.Illustration,'','','',false);
															}
														}
                                                    }
                                                    if($scope.Illustration.Product.ProductDetails.productId == '1097' && $scope.Illustration.Product.ProductDetails.productCode == '97'){
														if(typeof $scope.Illustration.Product.policyDetails.premiumFrequency !='undefined' || $scope.Illustration.Product.policyDetails.premiumFrequency!=undefined || $scope.Illustration.Product.policyDetails.premiumFrequency!=''){
															
															if ($scope.evaluateString($scope.viewToBeCustomized + '.' + 'packages') != undefined) {
																$scope.evaluateString($scope.viewToBeCustomized + '.' + 'packages').$render();
																$scope.callChangeMethod('CommonFunctions','onRegularFrequencyChange',$scope.Illustration,'','','',false);
															}
														}
                                                    }
                                                    /* FNA changes by LE Team >> starts */
                                                    if(IllustratorVariables.fromFNAChoosePartyScreenFlow == true && globalService.getFNAGoal().result.ruleExecutionOutput.length > 0 && globalService.getFNAGoal().result.ruleExecutionOutput[0].futureNeedGapunit != undefined){
                                                        $scope.Illustration.Product.policyDetails.sumInsured = globalService.getFNAGoal().result.ruleExecutionOutput[0].futureNeedGapunit;
                                                        $scope.Illustration.Product.policyDetails.isSumAssured = "Yes";
                                                        $scope.Illustration.Product.policyDetails.isPremium = "No";
                                                        $scope.loadSummaryDetails(false);
                                                        $scope.onHospitalBenefitPopulate();
                                                        $scope.loadonHospitalSales();
                                                    }
                                                    /* FNA changes by LE Team >> ends */
                                                }
                                            	$timeout(function() {
                                                        $rootScope.updateErrorCount($scope.viewToBeCustomized);
						                        }, 200);
                                            }
                                            //$rootScope.updateErrorCount(callBackId);
                                              //$scope.refresh();
											if($scope.Illustration.Product.policyDetails.premiumPeriod && $scope.Illustration.Product.policyDetails.premiumPeriod!=''){
												if ($scope.evaluateString($scope.viewToBeCustomized + '.' + 'premiumPeriod') != undefined) {
													$scope.evaluateString($scope.viewToBeCustomized + '.' + 'premiumPeriod').$render();
													if($scope.Illustration.Product.policyDetails.premiumFrequency && $scope.Illustration.Product.policyDetails.premiumFrequency!=''){
                        								if ($scope.evaluateString($scope.viewToBeCustomized + '.' + 'premiumFrequency') != undefined) {
															$scope.evaluateString($scope.viewToBeCustomized + '.' + 'premiumFrequency').$render();
															if($scope.Illustration.Product.ProductDetails.productId == '1010' && $scope.Illustration.Product.ProductDetails.productCode == '10'){
																if($scope.Illustration.Product.policyDetails.packages && $scope.Illustration.Product.policyDetails.packages!=''){
																	if ($scope.evaluateString($scope.viewToBeCustomized + '.' + 'packages') != undefined) {
																		$scope.evaluateString($scope.viewToBeCustomized + '.' + 'packages').$render();
																		$scope.callChangeMethod('CommonFunctions','onRegularFrequencyChange',$scope.Illustration,'','','',false);
																	}
																}	
															}
                                                            if($scope.Illustration.Product.ProductDetails.productId == '1097' && $scope.Illustration.Product.ProductDetails.productCode == '97'){
																if($scope.Illustration.Product.policyDetails.packages && $scope.Illustration.Product.policyDetails.packages!=''){
																	if ($scope.evaluateString($scope.viewToBeCustomized + '.' + 'packages') != undefined) {
																		$scope.evaluateString($scope.viewToBeCustomized + '.' + 'packages').$render();
																		$scope.callChangeMethod('CommonFunctions','onRegularFrequencyChange',$scope.Illustration,'','','',false);
																	}
																}	
															}
														}
                                                    }
												}
												$timeout(function() {
                                                        $rootScope.updateErrorCount($scope.viewToBeCustomized);
						                        }, 200);
											}
											$rootScope.showHideLoadingImage(false);
                                                $rootScope.updateErrorCount($scope.viewToBeCustomized);
                                            $scope.refresh();
                                        }, $scope, $compile);
                                    });
                                    /*if($scope.Illustration.Product.ProductDetails.productId == '1010' && $scope.Illustration.Product.ProductDetails.productCode == '10'){
                                    	$scope.setPremiumValueForHealth();
                                	}*/
                                    //$scope.refresh();
                            }
                            $scope.changeViewId(productUIJson);
                        }, $scope, $compile);
                        //closing loading image for products which dont have rider
                        if (typeof IllustratorVariables.productDetails.riders == 'undefined' || IllustratorVariables.productDetails.riders.length < 1) {
                        	if(typeof $scope.Illustration.Product.policyDetails.premiumPeriod =='undefined' || $scope.Illustration.Product.policyDetails.premiumPeriod==undefined || $scope.Illustration.Product.policyDetails.premiumPeriod==''){
                            	if($scope.Illustration.Product.ProductDetails.productId == '1003' && $scope.Illustration.Product.ProductDetails.productCode == '3'){
                            		$scope.Illustration.Product.policyDetails.premiumPeriod='8';
                            	}
                            	if ($scope.evaluateString($scope.viewToBeCustomized + '.' + 'premiumPeriod') != undefined) {
                                    $scope.evaluateString($scope.viewToBeCustomized + '.' + 'premiumPeriod').$render();
                                    if(typeof $scope.Illustration.Product.policyDetails.premiumFrequency =='undefined' || $scope.Illustration.Product.policyDetails.premiumFrequency==undefined || $scope.Illustration.Product.policyDetails.premiumFrequency==''){
        								$scope.Illustration.Product.policyDetails.premiumFrequency='Annual';
        							}
                                    if ($scope.evaluateString($scope.viewToBeCustomized + '.' + 'premiumFrequency') != undefined) {
                                        $scope.evaluateString($scope.viewToBeCustomized + '.' + 'premiumFrequency').$render();
                                    }
                                     /* FNA changes by LE Team >> starts */
                                    if(IllustratorVariables.fromFNAChoosePartyScreenFlow == true && globalService.getFNAGoal().result.ruleExecutionOutput.length > 0 && globalService.getFNAGoal().result.ruleExecutionOutput[0].futureNeedGapunit != undefined){
                                        $scope.Illustration.Product.policyDetails.sumInsured = globalService.getFNAGoal().result.ruleExecutionOutput[0].futureNeedGapunit;
                                        $scope.Illustration.Product.policyDetails.isSumAssured = "Yes";
                                        $scope.Illustration.Product.policyDetails.isPremium = "No";
                                        $scope.loadSummaryDetails(false);
                                    }
                                     /* FNA changes by LE Team >> ends */
                                    $rootScope.showHideLoadingImage(false);
                                }
							}
                             /*FNA changes by LE Team >>> starts - Bug 4194*/
                            FnaVariables.flagFromCustomerProfileFNA = false;
                             /*FNA changes by LE Team >>> ends - Bug 4194*/
                            $rootScope.showHideLoadingImage(false);
                            $scope.refresh();
                            $timeout(function() {
                                    $rootScope.updateErrorCount($scope.viewToBeCustomized);
                            }, 200);
                        }
                        if($scope.Illustration.Product.ProductDetails.productId == '1306' && $scope.Illustration.Product.ProductDetails.productCode == '306'){
                        	if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].riderPremium == ""){
	                        	$scope.Illustration.Product.policyDetails.premiumPeriod='10';
								$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].isRequired = 'Yes'
								$scope.defaultRiderDisabled = true;
								if($scope.Illustration.Product.policyDetails.sumInsured!==undefined 
								   && $scope.Illustration.Product.policyDetails.sumInsured!=="" 
								   && $scope.Illustration.Product.policyDetails.premiumPeriod!==undefined 
								   && $scope.Illustration.Product.policyDetails.premiumPeriod!=="" 
								   && $scope.Illustration.Product.policyDetails.isSumAssured == 'Yes'){
									$scope.loadSummaryDetails(false);
								} else if($scope.Illustration.Product.policyDetails.premium!==undefined 
										  && $scope.Illustration.Product.policyDetails.premium!=="" 
									      && $scope.Illustration.Product.policyDetails.premiumPeriod!==undefined 
									      && $scope.Illustration.Product.policyDetails.premiumPeriod!=="" 
									      && $scope.Illustration.Product.policyDetails.isPremium == 'Yes'){
									$scope.loadSummaryDetails(false);
								}
                        	}
						}
                        if($scope.Illustration.Product.ProductDetails.productId == '1274' && $scope.Illustration.Product.ProductDetails.productCode == '274'){
                        	if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].riderPremium == ""){
	                        	$scope.Illustration.Product.policyDetails.premiumPeriod='4';
								$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].isRequired = 'Yes'
								$scope.defaultRiderDisabled = true;
								if($scope.Illustration.Product.policyDetails.sumInsured!==undefined 
								   && $scope.Illustration.Product.policyDetails.sumInsured!=="" 
								   && $scope.Illustration.Product.policyDetails.premiumPeriod!==undefined 
								   && $scope.Illustration.Product.policyDetails.premiumPeriod!=="" 
								   && $scope.Illustration.Product.policyDetails.isSumAssured == 'Yes'){
									$scope.loadSummaryDetails(false);
								} else if($scope.Illustration.Product.policyDetails.premium!==undefined 
										  && $scope.Illustration.Product.policyDetails.premium!=="" 
									      && $scope.Illustration.Product.policyDetails.premiumPeriod!==undefined 
									      && $scope.Illustration.Product.policyDetails.premiumPeriod!=="" 
									      && $scope.Illustration.Product.policyDetails.isPremium == 'Yes'){
									$scope.loadSummaryDetails(false);
								}
                        	}
						}
                        $scope.refresh();
                    }, $scope, $compile);
                });

            });
        });
    }

    $scope.productSummaryDetails = function (productCode) {
		if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.policyDetails.premiumFrequency == "Annual"){
			if ($scope.Illustration.Product.policyDetails.premiumPeriod == '25') {
				$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[productCode].minSA25;
			}
		}
		else {
			$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[productCode].minSA;
		}
		if(IllustratorVariables.productSummaryValueList[productCode].maxSA == "Unlimited"){ 
			$scope.Illustration.Product.premiumSummary.summaryMaxSA =  translateMessages($translate, "Unlimited")
		}else{
			$scope.Illustration.Product.premiumSummary.summaryMaxSA = IllustratorVariables.productSummaryValueList[productCode].maxSA;
		}
		
		// if(productCode == '1003'){
			// $scope.Illustration.Product.ProductDetails.productName = translateMessages($translate, "GENBUMNAN8");
        // }else if(productCode == '1010'){
			// $scope.Illustration.Product.ProductDetails.productName = translateMessages($translate, "COMPLETE HEALTH SOLUTION 8080");
		// }else{}
        //$scope.Illustration.Product.premiumSummary.summaryBasicCoveragePeriod = IllustratorVariables.productSummaryValueList[productCode].basCovPrd;
    }
   
   $scope.minSumAssuredPopulate = function(){
	   if(IllustratorVariables.illustrationStatus != "Confirmed"){
		   var finalValue = 0;
		   var removingComma;
		   if($scope.Illustration.Product.ProductDetails.productId == '1003'){
			   removingComma = $scope.Illustration.Product.premiumSummary.summaryMinSA.split(",");
			   finalValue = $scope.removingCommaFromValues(removingComma);
			   $scope.Illustration.Product.policyDetails.sumInsured = parseInt(finalValue);
			   $scope.onFieldChange();
			   $scope.loadDetailsFromRule('isSumAssured');
		   } else if($scope.Illustration.Product.ProductDetails.productId == '1010'){
			   $scope.Illustration.Product.policyDetails.packages='Package A (200,000)';
			   $scope.onChangeValidateValue();
		   } else if($scope.Illustration.Product.ProductDetails.productId == '1004'){
				removingComma = $scope.Illustration.Product.premiumSummary.summaryMinSA.split(",");
				finalValue = $scope.removingCommaFromValues(removingComma);
				$scope.Illustration.Product.policyDetails.sumInsured = parseInt(finalValue);
				$scope.onFieldChange();
				$scope.loadDetailsFromRule('isSumAssured');
				$scope.clearRiderValuesForHospitalSales();
			} else if($scope.Illustration.Product.ProductDetails.productId == '1006'){
				removingComma = $scope.Illustration.Product.premiumSummary.summaryMinSA.split(",");
				finalValue = $scope.removingCommaFromValues(removingComma);
				$scope.Illustration.Product.policyDetails.sumInsured = parseInt(finalValue);
				$scope.onFieldChange();
				$scope.loadDetailsFromRule('isSumAssured');
				$scope.clearRiderValuesForHospitalSales();
			} else if($scope.Illustration.Product.ProductDetails.productId == '1097'){
			   $scope.Illustration.Product.policyDetails.packages='Plan 1';
			   $scope.onChangeValidateValue();
		   } else if($scope.Illustration.Product.ProductDetails.productId == '1220'){
				removingComma = $scope.Illustration.Product.premiumSummary.summaryMinSA.split(",");
				finalValue = $scope.removingCommaFromValues(removingComma);
				$scope.Illustration.Product.policyDetails.sumInsured = parseInt(finalValue);
				$scope.onFieldChange();
				$scope.loadDetailsFromRule('isSumAssured');
				$scope.clearRiderValuesForHospitalSales();
			} else if($scope.Illustration.Product.ProductDetails.productId == '1306'){
				removingComma = $scope.Illustration.Product.premiumSummary.summaryMinSA.split(",");
				finalValue = $scope.removingCommaFromValues(removingComma);
				$scope.Illustration.Product.policyDetails.sumInsured = parseInt(finalValue);
				$scope.onFieldChange();
				$scope.loadDetailsFromRule('isSumAssured');
				$scope.clearRiderValuesForHospitalSales();
			} else if($scope.Illustration.Product.ProductDetails.productId == '1274'){
				removingComma = $scope.Illustration.Product.premiumSummary.summaryMinSA.split(",");
				finalValue = $scope.removingCommaFromValues(removingComma);
				$scope.Illustration.Product.policyDetails.sumInsured = parseInt(finalValue);
				$scope.onFieldChange();
				$scope.loadDetailsFromRule('isSumAssured');
			}
	   }
   }
   
   $scope.removingCommaFromValues = function (value) {
	   var finalValueWithoutComma = 0;
	   for(var i=0; i< value.length; i++) {
			finalValueWithoutComma = finalValueWithoutComma + value[i];
	    }
		return finalValueWithoutComma;
   }
   
    $scope.maxSumAssuredPopulate = function(){
		if(IllustratorVariables.illustrationStatus != "Confirmed"){
			if($scope.Illustration.Product.ProductDetails.productId == '1003'){
			$scope.Illustration.Product.policyDetails.sumInsured = 100000000;
			$scope.onFieldChange();
		    $scope.loadDetailsFromRule('isSumAssured');
			} else if($scope.Illustration.Product.ProductDetails.productId == '1010' && $scope.Illustration.Insured.BasicDetails.age > 5){
			   $scope.Illustration.Product.policyDetails.packages='Package B (500,000)';
			   $scope.onChangeValidateValue();
			} else if($scope.Illustration.Product.ProductDetails.productId == '1004'){
			   $scope.Illustration.Product.policyDetails.sumInsured = 100000000;
			   $scope.onFieldChange();
			   $scope.loadDetailsFromRule('isSumAssured');
			   $scope.clearRiderValuesForHospitalSales();
			} else if($scope.Illustration.Product.ProductDetails.productId == '1006'){
			   $scope.Illustration.Product.policyDetails.sumInsured = 100000000;
			   $scope.onFieldChange();
			   $scope.loadDetailsFromRule('isSumAssured');
			   $scope.clearRiderValuesForHospitalSales();
			} else if($scope.Illustration.Product.ProductDetails.productId == '1097'){
			   $scope.Illustration.Product.policyDetails.packages='Plan 4';
			   $scope.onChangeValidateValue();
			} else if($scope.Illustration.Product.ProductDetails.productId == '1220'){
			   $scope.Illustration.Product.policyDetails.sumInsured = 100000000;
			   $scope.onFieldChange();
			   $scope.loadDetailsFromRule('isSumAssured');
			   $scope.clearRiderValuesForHospitalSales();
			} else if($scope.Illustration.Product.ProductDetails.productId == '1306'){
			   $scope.Illustration.Product.policyDetails.sumInsured = 100000000;
			   $scope.onFieldChange();
			   $scope.loadDetailsFromRule('isSumAssured');
			   $scope.clearRiderValuesForHospitalSales();
			} else if($scope.Illustration.Product.ProductDetails.productId == '1274'){
			   $scope.Illustration.Product.policyDetails.sumInsured = 100000000;
			   $scope.onFieldChange();
			   $scope.loadDetailsFromRule('isSumAssured');
			}
		}
   }
    
   $scope.checkIsPairRider = function(value1, value2){
	    if($scope.Illustration.Product.ProductDetails.productId == '1306' && $scope.Illustration.Product.ProductDetails.productCode == '306'){
	    	/*if(value1=="MaininsuredADBRider"){
				if($rootScope.declinedRCCADB){
					$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].isRequired = 'No'
					$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].sumInsured = ''; 
				} else {
					$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].isRequired=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].isRequired;
					$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].sumInsured;
				}
			}*/
			if(value1=="MaininsuredRCCADBRider"){
				if($rootScope.declinedRCCADB){
					$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].isRequired = 'No'
					$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].sumInsured = ''; 
				} /*else {
					$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].isRequired=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].isRequired;
					$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].sumInsured;
				}*/
			}
	    } else {
	    	if(($scope.Illustration.Product.ProductDetails.productId != '1306' && $scope.Illustration.Product.ProductDetails.productCode != '306') &&
	    	   ($scope.Illustration.Product.ProductDetails.productId != '1274' && $scope.Illustration.Product.ProductDetails.productCode != '274')){
		    	if(value1=="MaininsuredADBRider"){
					if($rootScope.declinedRCCADB){
						$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].isRequired = 'No'
						$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].sumInsured = ''; 
					} else {
						$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].isRequired=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].isRequired;
						$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].sumInsured;
					}
				}
				if(value1=="MaininsuredRCCADBRider"){
					if($rootScope.declinedRCCADB){
						$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].isRequired = 'No'
						$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].sumInsured = ''; 
					} else {
						$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].isRequired=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].isRequired;
						$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].sumInsured;
					}
				}
	    	}
	    }
		
		if(value1=="MaininsuredAIRider"){
			if($rootScope.declinedRCCAI){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].isRequired = 'No'
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].sumInsured = ''; 
			} else {
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].isRequired=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredAIRider')].isRequired;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredAIRider')].sumInsured;
			}
		}
		if(value1=="MaininsuredRCCAIRider"){
			if($rootScope.declinedRCCAI){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].isRequired = 'No'
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].sumInsured = ''; 
			} else {
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredAIRider')].isRequired=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].isRequired;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredAIRider')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].sumInsured;
			}
		}
		if(value1=="MaininsuredADDRiders"){
			if($rootScope.declinedRCCADD){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].isRequired = 'No'
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].sumInsured = ''; 
			} else {
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].isRequired=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADDRiders')].isRequired;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADDRiders')].sumInsured;
			}
		}
		if(value1=="MaininsuredRCCADDRider"){
			if($rootScope.declinedRCCADD){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].isRequired = 'No'
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].sumInsured = '';
			} else {
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADDRiders')].isRequired=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].isRequired;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADDRiders')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].sumInsured;
			}
		}       
       if(value1=="MaininsuredCNCRider"){
			if($rootScope.declinedRCCADD){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCNCRider')].isRequired = 'No'
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCNCRider')].sumInsured = '';
			} else {
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCNCRider')].isRequired=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCNCRider')].isRequired;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCNCRider')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCNCRider')].sumInsured;
			}
		}
       if(value1=="MaininsuredWPICRider"){
			if($rootScope.declinedRCCADD){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredWPICRider')].isRequired = 'No'
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredWPICRider')].sumInsured = '';
			} else {
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredWPICRider')].isRequired=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredWPICRider')].isRequired;
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredWPICRider')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredWPICRider')].sumInsured;
			}
		}
	};
   
    $scope.populateSumInsuredPair = function(value1){
    	if($scope.Illustration.Product.ProductDetails.productId == '1306' && $scope.Illustration.Product.ProductDetails.productCode == '306'){
    		if(value1=="MaininsuredADBRider"){
				if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].sumInsured == undefined){
					$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].riderPremium="";
				}
			}
			if(value1=="MaininsuredRCCADBRider"){
	            if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].sumInsured == undefined){
					$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].riderPremium="";
				}
			}
    	} else if($scope.Illustration.Product.ProductDetails.productId == '1274' && $scope.Illustration.Product.ProductDetails.productCode == '274'){ 
    		if(value1=="MaininsuredADBRider"){
				if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].sumInsured == undefined){
					$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].riderPremium="";
				}
			}
    	} else {
			if(value1=="MaininsuredADBRider"){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].sumInsured;
				if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].sumInsured == undefined){
					$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].riderPremium="";
					$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].riderPremium="";
				}
			}
			if(value1=="MaininsuredRCCADBRider"){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].sumInsured;
	            if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].sumInsured == undefined){
					$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].riderPremium="";
					$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].riderPremium="";
				}
			}
    	}
    	//if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4'){
    		if(value1=="MaininsuredOPDRiders"){
    			if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].sumInsured == undefined){
    				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].riderPremium="";
    			}
    		}  
    	//}
		if(value1=="MaininsuredAIRider"){
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredAIRider')].sumInsured;
			if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredAIRider')].sumInsured == undefined){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredAIRider')].riderPremium="";
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].riderPremium="";
			}
		}
		if(value1=="MaininsuredRCCAIRider"){
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredAIRider')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].sumInsured;
			if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].sumInsured == undefined){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredAIRider')].riderPremium="";
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].riderPremium="";
			}
		}
		if(value1=="MaininsuredADDRiders"){
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADDRiders')].sumInsured;
			if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADDRiders')].sumInsured == undefined){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].riderPremium="";
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADDRiders')].riderPremium="";
			}
		}
		if(value1=="MaininsuredRCCADDRider"){
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADDRiders')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].sumInsured;
			if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].sumInsured == undefined){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].riderPremium="";
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADDRiders')].riderPremium="";
			}
		}
		if(value1=="MaininsuredHBRiders"){
			if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHBRiders')].sumInsured == undefined){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHBRiders')].riderPremium="";
			}
		}
		if(value1=="MaininsuredHSRiders"){
			if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].sumInsured == undefined){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].riderPremium="";
			}
		}        
        if(value1=="MaininsuredCNCRider"){
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCNCRider')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCNCRider')].sumInsured;
			if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCNCRider')].sumInsured == undefined){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCNCRider')].riderPremium="";
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCNCRider')].riderPremium="";
			}
		}
        if(value1=="MaininsuredWPICRider"){
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredWPICRider')].sumInsured=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredWPICRider')].sumInsured;
			if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredWPICRider')].sumInsured == undefined){
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredWPICRider')].riderPremium="";
				$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredWPICRider')].riderPremium="";
			}
		}
        $scope.updateErrorDynamically();
	 };
	 
	$scope.clearRiderValuesForHospitalSales = function(){
		if(($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4') ||
		   ($scope.Illustration.Product.ProductDetails.productId == '1006' && $scope.Illustration.Product.ProductDetails.productCode == '6') ||
		   ($scope.Illustration.Product.ProductDetails.productId == '1220' && $scope.Illustration.Product.ProductDetails.productCode == '220')){
				for(var k = 0 ; k<=$scope.Illustration.Product.RiderDetails.length-1; k++){
					$scope.Illustration.Product.RiderDetails[k].sumInsured = "";
					$scope.Illustration.Product.RiderDetails[k].riderPremium="";
					$scope.Illustration.Product.RiderDetails[k].isRequired = 'No';
				}
				$scope.onHospitalSalesPopulate();
				$scope.onPopulateOPDRider();
		}
		if($scope.Illustration.Product.ProductDetails.productId == '1306' && $scope.Illustration.Product.ProductDetails.productCode == '306'){
			for(var k = 0 ; k<=$scope.Illustration.Product.RiderDetails.length-1; k++){
				if($scope.Illustration.Product.RiderDetails[k].uniqueRiderName == "MaininsuredADBRider"){
					$scope.Illustration.Product.RiderDetails[k].sumInsured = "";
					$scope.Illustration.Product.RiderDetails[k].riderPremium="";
					$scope.OnChaneValidation_ProLife("MaininsuredADBRider");
				} else {
					$scope.Illustration.Product.RiderDetails[k].sumInsured = "";
					$scope.Illustration.Product.RiderDetails[k].riderPremium="";
					$scope.Illustration.Product.RiderDetails[k].isRequired = 'No';
				}
			}
			$scope.onHospitalSalesPopulate();
		}
		if($scope.Illustration.Product.ProductDetails.productId == '1274' && $scope.Illustration.Product.ProductDetails.productCode == '274'){
			for(var k = 0 ; k<=$scope.Illustration.Product.RiderDetails.length-1; k++){
				if($scope.Illustration.Product.RiderDetails[k].uniqueRiderName == "MaininsuredADBRider"){
					$scope.Illustration.Product.RiderDetails[k].sumInsured = "";
					$scope.Illustration.Product.RiderDetails[k].riderPremium="";
					$scope.OnChaneValidation_ProLife("MaininsuredADBRider");
				} 
			}
		}
	}
	 
	$scope.loadonHospitalSales = function () {
		if (($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4') ||
			($scope.Illustration.Product.ProductDetails.productId == '1006' && $scope.Illustration.Product.ProductDetails.productCode == '6') ||
			($scope.Illustration.Product.ProductDetails.productId == '1220' && $scope.Illustration.Product.ProductDetails.productCode == '220') ||
			($scope.Illustration.Product.ProductDetails.productId == '1306' && $scope.Illustration.Product.ProductDetails.productCode == '306') ||
			($scope.Illustration.Product.ProductDetails.productId == '1274' && $scope.Illustration.Product.ProductDetails.productCode == '274')) {
			$scope.clearRiderValuesForHospitalSales();
		}
	}

	$scope.cancerRdrDisbldForGenpro = function () {
		if ($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.policyDetails.premiumPeriod == '25') {
			if ($scope.Illustration.Product.policyDetails.sumInsured < 150000) {
				$scope.disableCancerRider = true;
			}
			else {
				$scope.disableCancerRider = false;
			}

			if($scope.Illustration.Product.policyDetails.premiumPeriod === '25'){
				$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA25;
			}
		}
		else {
			$scope.disableCancerRider = false;
			if($scope.Illustration.Product.policyDetails.premiumFrequency!=="Annual"){
				$scope.Illustration.Product.premiumSummary.summaryMinSA =IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSANotAnnual;
			}else{
				$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
			}
		}
	}

	function checkUncheckMethod() {
		if ($scope.Illustration.Product.policyDetails.packages) {
			for (var i = 0; i < $scope.Illustration.Product.RiderDetails.length; i++) {
				// if ($scope.Illustration.Product.RiderDetails[i].planName == 'OPD'  && !$scope.Illustration.Product.RiderDetails[i].sumInsured) {
				// 	$scope.Illustration.Product.RiderDetails[i].isRequired = "No";
				// }
				if ($scope.Illustration.Product.RiderDetails[i].planName == 'OPD' && ($scope.Illustration.Product.policyDetails.packages == 'Package A (200,000)' || $scope.Illustration.Product.policyDetails.packages == 'Package B (500,000)')) {
					$scope.Illustration.Product.RiderDetails[i].sumInsured = "";
					$scope.Illustration.Product.RiderDetails[i].riderPremium = "";
					$scope.Illustration.Product.RiderDetails[i].isRequired = 'No';
					$scope.Illustration.Product.RiderDetails[i].policyTerm = '';
				}
				else {
					$scope.Illustration.Product.RiderDetails[i].isRequired = "Yes";
				}
			}
		}
		else {
			for (var i = 0; i < $scope.Illustration.Product.RiderDetails.length; i++) {
				if ($scope.Illustration.Product.RiderDetails[i].planName == 'OPD') {
					$scope.Illustration.Product.RiderDetails[i].isRequired = "No";
				}
			}
		}
	}

 $scope.onPackageChangeForCHS = function () {
		 if ($scope.Illustration.Product.ProductDetails.productId == '1010' && $scope.Illustration.Product.ProductDetails.productCode == '10') {
			 checkOPDRiderAndValues();
			 checkUncheckMethod();
		 }
	 }

	function checkOPDRiderAndValues() {
		$scope.initiallyDisabled = false;
		$scope.disablePackageD = false;
		if ($scope.Illustration.Product.policyDetails.packages == 'Package C (200,000)') {
			$scope.outPatientBenefitDepartments = ["1000"];
			$scope.hideOPDRiderCHS = true;
		}
		else if ($scope.Illustration.Product.policyDetails.packages == 'Package D (500,000)') {
			$scope.outPatientBenefitDepartments = ["1000", "1500"];
			$scope.hideOPDRiderCHS = false;
			$scope.disablePackageD = true;
		}
		else {
			$scope.hideOPDRiderCHS = true;
		}

	}

	//For OPD rider disable when deselected case  
	$scope.onDisableOPDRider = function (index) {
		$scope.disableOPDRider = false;
		if (index) {
			if ($scope.Illustration.Product.RiderDetails[index[0]].isRequired == "No") {
				$scope.disableOPDRider = true;
			}
			else {
				$scope.disableOPDRider = false;
			}
		}
		else {
			for (var i = 0; i < $scope.Illustration.Product.RiderDetails.length; i++) {
				if ($scope.Illustration.Product.RiderDetails[i].planName == 'OPD' && $scope.Illustration.Product.RiderDetails[i].isRequired == "No") {
					$scope.disableOPDRider = true;
				}
			}
		}

	}
	 //End

	 $scope.onHospitalSalesPopulate = function(){
		 if($scope.Illustration.Insured.BasicDetails.age!==undefined && $scope.Illustration.Insured.BasicDetails.age!==""){
			 if($scope.Illustration.Insured.BasicDetails.age >= 0 && $scope.Illustration.Insured.BasicDetails.age < 11){
				 if($scope.Illustration.Product.policyDetails.sumInsured!==undefined && $scope.Illustration.Product.policyDetails.sumInsured!==""){
					 if($scope.Illustration.Product.policyDetails.sumInsured >= 1){
						 $scope.hospitalSalesRecords = ["1000", "2000"];
					 }					
				 }
			 }
			 if($scope.Illustration.Insured.BasicDetails.age >= 11 && $scope.Illustration.Insured.BasicDetails.age < 16){
				 if($scope.Illustration.Product.policyDetails.sumInsured!==undefined && $scope.Illustration.Product.policyDetails.sumInsured!==""){
					 if($scope.Illustration.Product.policyDetails.sumInsured > 0 && $scope.Illustration.Product.policyDetails.sumInsured < 500000){
						 $scope.hospitalSalesRecords = ["1000", "2000", "3000"];
					 }
					 if($scope.Illustration.Product.policyDetails.sumInsured > 499999 && $scope.Illustration.Product.policyDetails.sumInsured < 3000000){
						 $scope.hospitalSalesRecords = ["1000", "2000", "3000", "4000", "5000"];
					 }
					 if($scope.Illustration.Product.policyDetails.sumInsured > 2999999){
						 $scope.hospitalSalesRecords = ["1000", "2000", "3000", "4000", "5000", "10000"];
					 }
				 }
			 }
			 if($scope.Illustration.Insured.BasicDetails.age > 15 && $scope.Illustration.Insured.BasicDetails.age < 61){
				 if($scope.Illustration.Product.policyDetails.sumInsured!==undefined && $scope.Illustration.Product.policyDetails.sumInsured!==""){
					 if($scope.Illustration.Product.policyDetails.sumInsured > 0 && $scope.Illustration.Product.policyDetails.sumInsured < 500000){
						 $scope.hospitalSalesRecords = ["1000", "2000", "3000"];
					 }
					 if($scope.Illustration.Product.policyDetails.sumInsured > 499999 && $scope.Illustration.Product.policyDetails.sumInsured < 3000000){
						 $scope.hospitalSalesRecords = ["1000", "2000", "3000", "4000", "5000"];
					 }
					 if($scope.Illustration.Product.policyDetails.sumInsured > 2999999 && $scope.Illustration.Product.policyDetails.sumInsured < 5000000){
						 $scope.hospitalSalesRecords = ["1000", "2000", "3000", "4000", "5000", "10000"];
					 }
					 if($scope.Illustration.Product.policyDetails.sumInsured > 4999999 && $scope.Illustration.Product.policyDetails.sumInsured < 10000000){
						 $scope.hospitalSalesRecords = ["1000", "2000", "3000", "4000", "5000", "10000", "20000"];
					 }
					 if($scope.Illustration.Product.policyDetails.sumInsured > 9999999 && $scope.Illustration.Product.policyDetails.sumInsured < 20000000){
						 $scope.hospitalSalesRecords = ["1000", "2000", "3000", "4000", "5000", "10000", "20000", "30000"];
					 }
					 if($scope.Illustration.Product.policyDetails.sumInsured > 19999999){
						 $scope.hospitalSalesRecords = ["1000", "2000", "3000", "4000", "5000", "10000", "20000", "30000", "50000"];
					 }
				 }
			 }
			 if($scope.Illustration.Insured.BasicDetails.age > 60 && $scope.Illustration.Insured.BasicDetails.age < 71){
				 if($scope.Illustration.Product.policyDetails.sumInsured!==undefined && $scope.Illustration.Product.policyDetails.sumInsured!==""){
					 if($scope.Illustration.Product.policyDetails.sumInsured > 0 && $scope.Illustration.Product.policyDetails.sumInsured < 500000){
						 $scope.hospitalSalesRecords = ["1000", "2000", "3000"];
					 }
					 if($scope.Illustration.Product.policyDetails.sumInsured > 499999 && $scope.Illustration.Product.policyDetails.sumInsured < 3000000){
						 $scope.hospitalSalesRecords = ["1000", "2000", "3000", "4000", "5000"];
					 }
					 if($scope.Illustration.Product.policyDetails.sumInsured > 2999999 && $scope.Illustration.Product.policyDetails.sumInsured < 5000000){
						 $scope.hospitalSalesRecords = ["1000", "2000", "3000", "4000", "5000", "10000"];
					 }
					 if($scope.Illustration.Product.policyDetails.sumInsured > 4999999 && $scope.Illustration.Product.policyDetails.sumInsured < 20000000 ){
						 $scope.hospitalSalesRecords = ["1000", "2000", "3000", "4000", "5000", "10000", "20000"];
					 }
					 if($scope.Illustration.Product.policyDetails.sumInsured > 19999999){
						 $scope.hospitalSalesRecords = ["1000", "2000", "3000", "4000", "5000", "10000", "20000", "30000"];
					 }
				 }
			 }
		 } 
	 };
	 
	 $scope.onPopulateOPDRider = function(){
		 $scope.HS2000Disable = false;
		 //if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4'){
			 if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].sumInsured!==undefined || $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].sumInsured!==""){
				 if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].sumInsured==undefined || $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].sumInsured==""){
					 $scope.initiallyDisabled = true;
					 $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].isRequired = "No";
					 $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].sumInsured = "";
					 $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].premium = "";
				 }
			 }
			 if($scope.Illustration.Insured.BasicDetails.age >= 0 && $scope.Illustration.Insured.BasicDetails.age < 11){
				 if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].sumInsured!==undefined && $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].sumInsured!==""){
					 if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].sumInsured == Number(1000) || $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].sumInsured == Number(2000)){
						 $scope.initiallyDisabled = false;
						 $scope.outPatientBenefitDepartments = ["1000"];
						 if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].sumInsured == Number(2000)){
							 $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].sumInsured = "1000";
							 $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].isRequired = 'Yes';
							 $scope.HS2000Disable = true;

						 }
						 else {
							$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].isRequired = 'No';
							 $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].sumInsured = "";
							 $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].riderPremium="";
							 //$scope.initiallyDisabled = false;
						 }
					 }	
				 }
				 else {		
							$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].isRequired = 'No';
							$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].sumInsured = "";
							$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].riderPremium="";
							//$scope.initiallyDisabled = true;
				 }	
			 }
			 if($scope.Illustration.Insured.BasicDetails.age >= 11){
				 if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].sumInsured!==undefined && $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].sumInsured!==""){
					 if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].sumInsured < Number(5000)){
						 $scope.initiallyDisabled = false;
						 $scope.outPatientBenefitDepartments = ["1000"];
					 }	
				 }
				 if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].sumInsured!==undefined && $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].sumInsured!==""){
					 if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRiders')].sumInsured >= Number(5000)){
						 $scope.initiallyDisabled = false;
						 $scope.outPatientBenefitDepartments = ["1000", "1500"];
					 }	
				 }
			 }
		// }
	 }
	 
	 $scope.onHospitalBenefitPopulate = function(){
		if($scope.Illustration.Insured.OccupationDetails.length == 1 && ($scope.Illustration.Insured.OccupationDetails[0].occupationCode == "Q003" || $scope.Illustration.Insured.OccupationDetails[0].occupationCodeValue == "Q003")){
		   $scope.hospitalBenefitRecords = ["600","800","1000"];
	   } else if($scope.Illustration.Insured.OccupationDetails.length == 1 && ($scope.Illustration.Insured.OccupationDetails[0].occupationCode == "N035" || $scope.Illustration.Insured.OccupationDetails[0].occupationCodeValue == "N035")){
		   $scope.hospitalBenefitRecords = ["600","800","1000"];
	   } else if($scope.Illustration.Insured.OccupationDetails.length == 1 && ($scope.Illustration.Insured.OccupationDetails[0].occupationCode == "N018" || $scope.Illustration.Insured.OccupationDetails[0].occupationCodeValue == "N018")){
		   $scope.hospitalBenefitRecords = ["600","800","1000"];
	   } else {
		   $scope.hospitalBenefitRecords = ["600", "800", "1000", "1500", "2000", "2500", "3000", "4000"];
	   }
	}

    
     $scope.checkRiderCount = function () {
        if (IllustratorVariables.selctedMaininsuredVGHRider && IllustratorVariables.selctedMaininsuredHCRider) {
            var erroMessageRiderCount1 = $scope.getRiderErrMsgHospitalCash('riderCheckedValidation', 'Maininsured');
            $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
            								   erroMessageRiderCount1,
            								   translateMessages($translate, "general.ok"), 
            								   $scope.cancelBtnAction);
        } else if (IllustratorVariables.selctedSpouseVGHRider && IllustratorVariables.selctedSpouseHCRider) {
            var erroMessageRiderCount1 = $scope.getRiderErrMsgHospitalCash('riderCheckedValidation', 'Spouse');
            $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                                               erroMessageRiderCount1,
                                               translateMessages($translate, "general.ok"), 
                                               $scope.cancelBtnAction);
        } else if (IllustratorVariables.selctedChild1VGHRider && IllustratorVariables.selctedChild1HCRider) {
            var erroMessageRiderCount1 = $scope.getRiderErrMsgHospitalCash('riderCheckedValidation', 'Child1');
            $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                                               erroMessageRiderCount1,
                                               translateMessages($translate, "general.ok"), 
                                               $scope.cancelBtnAction);
        } else if (IllustratorVariables.selctedChild2VGHRider && IllustratorVariables.selctedChild2HCRider) {
            var erroMessageRiderCount1 = $scope.getRiderErrMsgHospitalCash('riderCheckedValidation', 'Child2');
            $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                                               erroMessageRiderCount1,
                                               translateMessages($translate, "general.ok"), 
                                               $scope.cancelBtnAction);
        } else if (IllustratorVariables.selctedChild3VGHRider && IllustratorVariables.selctedChild3HCRider) {
            var erroMessageRiderCount1 = $scope.getRiderErrMsgHospitalCash('riderCheckedValidation', 'Child3');
            $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                                               erroMessageRiderCount1,
                                               translateMessages($translate, "general.ok"), 
                                               $scope.cancelBtnAction);
        } else if (IllustratorVariables.selctedChild4VGHRider && IllustratorVariables.selctedChild4HCRider) {
            var erroMessageRiderCount1 = $scope.getRiderErrMsgHospitalCash('riderCheckedValidation', 'Child4');
            $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
            								   erroMessageRiderCount1,
            								   translateMessages($translate, "general.ok"), 
            								   $scope.cancelBtnAction);
        } else if (IllustratorVariables.selctedPayorVGHRider && IllustratorVariables.selctedPayorHCRider) {
            var erroMessageRiderCount1 = $scope.getRiderErrMsgHospitalCash('riderCheckedValidation', 'Payor');
            $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                                               erroMessageRiderCount1,
                                               translateMessages($translate, "general.ok"), 
                                               $scope.cancelBtnAction);
        } else if (IllustratorVariables.selctedVGHRider && $scope.Illustration.Product.policyDetails.premiumFrequency == "Quarterly") {
            var erroMessageRiderCount1 = translateMessages($translate, "illustrator.riderAndFrequencyCheckedValidation");
            $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                                               erroMessageRiderCount1,
                                               translateMessages($translate, "general.ok"), 
                                               $scope.cancelBtnAction);
        } else {
            $scope.callCheckRiderCount(function (count) {
                if (count > 14) { // There are more than 14 riders which are currently not yet supported by e-app module
                    var erroMessageRiderCount = translateMessages($translate, "illustrator.riderCountValidation");
                    $rootScope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
                    								   erroMessageRiderCount,
                    								   translateMessages($translate, "general.proceedInCaps"),
                    								   $scope.okValidateBtnAction,
                    								   translateMessages($translate, "illustrator.cancelInCaps"), 
                    								   $scope.cancelBtnAction);
                } else {
                    $scope.okValidateBtnAction();
                }
            })
        }
    }


    $scope.cancelBtnAction = function (data) {
       
    }
    $scope.callCheckRiderCount = function (successCallBack) {
        var count = 0;
        for (var i = 0; i < $scope.Illustration.Product.RiderDetails.length; i++) {
            if ($scope.Illustration.Product.RiderDetails[i].isRequired == "Yes") {
                count = count + 1;
            }
        }
        successCallBack(count);
    }
	
	 $scope.addedRiderValuesforPB = function () {
        var suminsuredValue = 0;
        for (var i = 0; i < $scope.Illustration.Product.RiderDetails.length; i++) {
           if($scope.Illustration.Product.RiderDetails[i].isRequired == "Yes" && $scope.Illustration.Product.RiderDetails[i].sumInsured !="") {
                suminsuredValue = suminsuredValue + $scope.Illustration.Product.RiderDetails[i].sumInsured;
            }
        }
		if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredPBRider')].isRequired == "Yes"){
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredPBRider')].sumInsured = suminsuredValue;
		}
    }

    $scope.riderCountValidation = function () {


    }
    $scope.okValidateBtnAction = function () {
		//$scope.addedRiderValuesforPB();
        $scope.illustrationDetails('validate');
    }
    // Customized method on the click of validate and illustrate buttons
    $scope.illustrationDetails = function (container, successCallback, errorCallback) {
        $rootScope.showHideLoadingImage(true, 'calculationProgress', $translate);
        $scope.clearRiderValue = false;
        globalService.setCPInsured($scope.Illustration.Insured);
        globalService.setCPPayer($scope.Illustration.Payer);
        IllustratorVariables.setIllustratorModel($scope.Illustration);
        PersistenceMapping.clearTransactionKeys();
        IllustratorService.mapKeysforPersistence($scope, IllustratorVariables);
        var transactionObj = PersistenceMapping.mapScopeToPersistence($scope.Illustration);
        var ruleInfo = {};
        if ((rootConfig.isDeviceMobile)) {
            var transactionData = $.parseJSON(transactionObj.TransactionData)
            transactionObj.TransactionData = transactionData;
        }
		if ($scope.Illustration.Product.ProductDetails.productCode == '10' && $scope.Illustration.Insured.BasicDetails.age < 11) {
			for (var i = 0; i < $scope.Illustration.Product.RiderDetails.length; i++) {
				if ($scope.Illustration.Product.RiderDetails[i].shortName == 'HBRider') {
					$scope.Illustration.Product.RiderDetails[i].isRequired = "No"
				}
			}
		}
        //$rootScope.showHideLoadingImage(true, 'calculationProgress', $translate);
        transactionObj.TransactionData = GLI_IllustratorService.convertToCanonicalInput(transactionObj.TransactionData, IllustratorVariables, container);
        ruleInfo.offlineDB = IllustratorVariables.productDetails.offlineDB[0].ruleReference;
        if (container == "validate") {
            function callAtTimeout() {
            	if(IllustratorVariables.productDetails.ruleSpec[0].ruleReference.indexOf('Validations')>-1){
        			ruleInfo.ruleName = IllustratorVariables.productDetails.ruleSpec[0].ruleReference;
        			if(!(rootConfig.isDeviceMobile)){
        				ruleInfo.ruleGroup = IllustratorVariables.productDetails.ruleSpec[0].group;
        			}
        		}
        		else if(IllustratorVariables.productDetails.ruleSpec[1].ruleReference.indexOf('Validations')>-1){
        			ruleInfo.ruleName = IllustratorVariables.productDetails.ruleSpec[1].ruleReference;
        			if(!(rootConfig.isDeviceMobile)){
        				ruleInfo.ruleGroup = IllustratorVariables.productDetails.ruleSpec[1].group;
        			}
        		}
                if (!(transactionObj.TransactionData.Channel.id)) {
                    transactionObj.TransactionData.Channel.id = IllustratorVariables.channelId;
                    if (transactionObj.TransactionData.Channel.id == "9084") {
                        transactionObj.TransactionData.Channel.name = "Exim";
                    } else if (transactionObj.TransactionData.Channel.id == "9085") {
                        transactionObj.TransactionData.Channel.name = "TCB";
                    } else if (transactionObj.TransactionData.Channel.id == "9082") {
                        transactionObj.TransactionData.Channel.name = "Agency";
                    }
                }
                GLI_RuleService.runCustomRule(transactionObj, $scope.onGetValidationDataSuccess, $scope.onGetIllustrationError, ruleInfo, container);
            }
            $timeout(callAtTimeout, 280);
        } else if (container == "illustrate") {
            var illuDate = $scope.Illustration.Product.premiumSummary.validatedDate;
            var isExpired = GLI_IllustratorService.checkExpiryDate(illuDate);
            if (isExpired) {
                $rootScope.showHideLoadingImage(false);
                $rootScope.lePopupCtrl.showError(translateMessages($translate, "lifeEngage"),
                								 translateMessages($translate, "illustrator.expiryDateMessageIllustration"),
                								 translateMessages($translate, "illustrator.okMessageInCaps"));
                $rootScope.premiumSummaryOutput = false;

            } else {
                ruleInfo.ruleName = IllustratorVariables.productDetails.calcSpec[0].ruleReference;
                if (!(rootConfig.isDeviceMobile)) {
                    ruleInfo.ruleGroup = IllustratorVariables.productDetails.calcSpec[0].group;
                }
				if (!(transactionObj.TransactionData.Channel.id)) {
                    transactionObj.TransactionData.Channel.id = IllustratorVariables.channelId;
                    if (transactionObj.TransactionData.Channel.id == "9084") {
                        transactionObj.TransactionData.Channel.name = "Exim";
                    } else if (transactionObj.TransactionData.Channel.id == "9085") {
                        transactionObj.TransactionData.Channel.name = "TCB";
                    } else if (transactionObj.TransactionData.Channel.id == "9082") {
                        transactionObj.TransactionData.Channel.name = "Agency";
                    }
                }
				
				//$scope.onGetIllustrationDataSuccess(data);
                GLI_RuleService.runCustomRule(transactionObj, $scope.onGetIllustrationDataSuccess, $scope.onGetIllustrationError, ruleInfo, container);
            }
        }
    }

    $scope.onGetValidationDataSuccess_common = function (data) {
        $scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
        $scope.Illustration.Product.premiumSummary.summaryMaxSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].maxSA; 
        
        if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4'){
			if($scope.Illustration.Product.policyDetails.premiumPeriod == '25') {
				$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA25;
			}
			else {
				if($scope.Illustration.Product.policyDetails.premiumFrequency=="" || $scope.Illustration.Product.policyDetails.premiumFrequency==undefined){
					$scope.Illustration.Product.premiumSummary.summaryMinSA
					 = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
				}else if($scope.Illustration.Product.policyDetails.premiumFrequency!=="Annual"){
					$scope.Illustration.Product.premiumSummary.summaryMinSA =IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSANotAnnual;
				}else{
					$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
				}
			}
        }
        
        if($scope.Illustration.Product.ProductDetails.productId == '1220' && $scope.Illustration.Product.ProductDetails.productCode == '220'){
            if($scope.Illustration.Product.policyDetails.premiumFrequency=="" || $scope.Illustration.Product.policyDetails.premiumFrequency==undefined){
                $scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
            }else if($scope.Illustration.Product.policyDetails.premiumFrequency!=="Annual"){
                $scope.Illustration.Product.premiumSummary.summaryMinSA =IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSANotAnnual;
            }else{
                $scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
            }
        }
        
		if(data.PolicyDetails.sumAssured == "NaN"){
			$scope.Illustration.Product.policyDetails.sumInsured = 0;
		} else {
			$scope.Illustration.Product.policyDetails.sumInsured = data.PolicyDetails.sumAssured;			
		}
		if(data.PolicyDetails.premium == "NaN"){
			$scope.Illustration.Product.policyDetails.premium = 0;			
		} else {            
			$scope.Illustration.Product.policyDetails.premium = data.modalPremium;			
		}  
		if(data.discountedPremium && data.discountedPremium != "" && data.discountedPremium != "NaN"){
        	$scope.Illustration.Product.premiumSummary.discountedPremium = data.discountedPremium;
        } else {
        	$scope.Illustration.Product.premiumSummary.discountedPremium = "";
        }
        /*if(data.riderCalSA){
	        if (data.riderCalSA.length > 0) {
	            for (totRiderIndex in $scope.Illustration.Product.RiderDetails) {
	                for (riderCal in data.riderCalSA) {
	                    if ($scope.Illustration.Product.RiderDetails[totRiderIndex].uniqueRiderName == data.riderCalSA[riderCal].uniqueRiderName) {
                            if(data.riderCalSA[riderCal].saValue!=="" && data.riderCalSA[riderCal].saValue!==undefined){
	                        $scope.Illustration.Product.RiderDetails[totRiderIndex].sumInsured = data.riderCalSA[riderCal].saValue;
                            }
	                    }
	                }
	            }
	        }
    	}*/
		if($scope.viewToBeCustomized != undefined && $scope.viewToBeCustomized != "" ){
			 $timeout(function() {
					$rootScope.updateErrorCount($scope.viewToBeCustomized);
			}, 200);
		}
		$rootScope.showHideLoadingImage(false);
        $scope.refresh();
    }
    
    $scope.onGetIllustrationError_common = function(data){
    	$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
        $scope.Illustration.Product.premiumSummary.summaryMaxSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].maxSA;
        
        if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4'){
			if ($scope.Illustration.Product.policyDetails.premiumPeriod == '25') {
				$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA25;
			}
			else {
				if($scope.Illustration.Product.policyDetails.premiumFrequency=="" || $scope.Illustration.Product.policyDetails.premiumFrequency==undefined){
					$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
				}else if($scope.Illustration.Product.policyDetails.premiumFrequency!=="Annual"){
					$scope.Illustration.Product.premiumSummary.summaryMinSA =IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSANotAnnual;
				}else{
					$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
				}
			}
        }
        
        if($scope.Illustration.Product.ProductDetails.productId == '1220' && $scope.Illustration.Product.ProductDetails.productCode == '220'){
            if($scope.Illustration.Product.policyDetails.premiumFrequency=="" || $scope.Illustration.Product.policyDetails.premiumFrequency==undefined){
                $scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
            }else if($scope.Illustration.Product.policyDetails.premiumFrequency!=="Annual"){
                $scope.Illustration.Product.premiumSummary.summaryMinSA =IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSANotAnnual;
            }else{
                $scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
            }
        }
        
		if(data.PolicyDetails.sumAssured == "NaN"){
			$scope.Illustration.Product.policyDetails.sumInsured = "";
		}
        if(data.PolicyDetails.premium == "NaN"){
			$scope.Illustration.Product.policyDetails.premium = "";
		}
		if(data.BaseOccCode!==undefined && data.BaseOccCode!==""){
			$scope.Illustration.Insured.BasicDetails.occupationClassCode = data.BaseOccCode[0];
		}
		if(data.totalRiderPremium == "NaN"){
        	$scope.Illustration.Product.premiumSummary.riderPremium = "";
        }
		if(data.discountedPremium && data.discountedPremium != "" && data.discountedPremium != "NaN"){
        	$scope.Illustration.Product.premiumSummary.discountedPremium = data.discountedPremium;
        } else {
        	$scope.Illustration.Product.premiumSummary.discountedPremium = "";
        }
        if($scope.Illustration.Product.RiderDetails!==undefined){
                for(var i=0;i<$scope.Illustration.Product.RiderDetails.length;i++){
                    $scope.Illustration.Product.RiderDetails[i].riderPremium="";
                }
            }
        
		$rootScope.showHideLoadingImage(false);
        $scope.refresh();
    }
    
    /*
     * Validation success call. Map data to premium summary fields.
     */
    $scope.onGetValidationDataSuccess = function (data) {
        $scope.benefitSummaryData = data;
        $scope.benefitSummaryShown = false;
        $scope.isValidationRuleExecuted = false;
        $scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
        $scope.Illustration.Product.premiumSummary.summaryMaxSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].maxSA;
        
        if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4'){
			if ($scope.Illustration.Product.policyDetails.premiumPeriod == '25') {
				$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA25;
			}
			else {
				if($scope.Illustration.Product.policyDetails.premiumFrequency=="" || $scope.Illustration.Product.policyDetails.premiumFrequency==undefined){
					$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
				}else if($scope.Illustration.Product.policyDetails.premiumFrequency!=="Annual"){
					$scope.Illustration.Product.premiumSummary.summaryMinSA =IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSANotAnnual;
				}else{
					$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
				}
			}
        }
        
        if($scope.Illustration.Product.ProductDetails.productId == '1220' && $scope.Illustration.Product.ProductDetails.productCode == '220'){
            if($scope.Illustration.Product.policyDetails.premiumFrequency=="" || $scope.Illustration.Product.policyDetails.premiumFrequency==undefined){
                $scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
            }else if($scope.Illustration.Product.policyDetails.premiumFrequency!=="Annual"){
                $scope.Illustration.Product.premiumSummary.summaryMinSA =IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSANotAnnual;
            }else{
                $scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
            }
        }
        //$scope.Illustration.Product.premiumSummary.summaryBasicCoveragePeriod = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].basCovPrd;
		
		$scope.Illustration.Product.premiumSummary.summaryBasicCoveragePeriod = data.protectionPeriodFinal;
		if(data.totalRiderPremium == "null"){
			$scope.Illustration.Product.premiumSummary.riderPremium = 0;
		}
		if(data.PolicyDetails.sumAssured == "NaN"){
			$scope.Illustration.Product.policyDetails.sumInsured = 0;
		} else {
			$scope.Illustration.Product.policyDetails.sumInsured = data.PolicyDetails.sumAssured;
			$scope.Illustration.Product.premiumSummary.sumInsured = data.PolicyDetails.sumAssured;
		}
		if($scope.Illustration.Product.ProductDetails.productId != '1220' && $scope.Illustration.Product.ProductDetails.productCode != '220'){
			if(data.PolicyDetails.premium == "NaN"){
				$scope.Illustration.Product.policyDetails.premium = 0;
				$scope.Illustration.Product.premiumSummary.basicPremium = 0;
			} else {
				$scope.Illustration.Product.policyDetails.premium = data.modalPremium;
				$scope.Illustration.Product.premiumSummary.basicPremium = data.modalPremium;
			}
        } else {
        	$scope.Illustration.Product.policyDetails.premium = data.modalPremium;
			$scope.Illustration.Product.premiumSummary.basicPremium = $scope.Illustration.Product.premiumSummary.discountedPremium;
        }
		if(data.BaseOccCode!= "undefined" && data.BaseOccCode != ""){
			$scope.Illustration.Insured.BasicDetails.occupationClassCode = data.BaseOccCode[0];
		}
        		
		$scope.Illustration.Product.premiumSummary.paymentMode = translateMessages($translate,$scope.Illustration.Product.policyDetails.premiumFrequency);
        if(data.totalPolicyPremium == "null") {
        	$scope.Illustration.Product.premiumSummary.totalPremium = 0;
        } else {
        	$scope.Illustration.Product.premiumSummary.totalPremium = data.totalPolicyPremium;
        }
        
        if($scope.Illustration.Product.ProductDetails.productId == "1003"){
        	$scope.Illustration.Product.premiumSummary.riderPremium = 0;
        } else if(data.totalRiderPremium == "null") {
        	$scope.Illustration.Product.premiumSummary.riderPremium = 0;
        } else {
        	$scope.Illustration.Product.premiumSummary.riderPremium = data.totalRiderPremium;
        }
        
        if($scope.Illustration.Product.premiumSummary.riderPremium=="NaN"){
            $scope.Illustration.Product.premiumSummary.riderPremium="";
        }
        /*if(data.riderCalSA){
	        if (data.riderCalSA.length > 0) {
	            for (totRiderIndex in $scope.Illustration.Product.RiderDetails) {
	                for (riderCal in data.riderCalSA) {
	                    if ($scope.Illustration.Product.RiderDetails[totRiderIndex].uniqueRiderName == data.riderCalSA[riderCal].uniqueRiderName) {
                            if(data.riderCalSA[riderCal].saValue!==undefined && data.riderCalSA[riderCal].saValue!==""){
                            	$scope.Illustration.Product.RiderDetails[totRiderIndex].sumInsured = data.riderCalSA[riderCal].saValue;
                            }
	                    }
	                }
	            }
	        }
    	}*/
        if (data.selectedRiderTbl) {
            if (data.selectedRiderTbl.length > 0) {
                for (ridersPremium in $scope.Illustration.Product.RiderDetails) {
                    for (riderPrem in data.selectedRiderTbl) {
                        if ($scope.Illustration.Product.RiderDetails[ridersPremium].uniqueRiderName == data.selectedRiderTbl[riderPrem].uniqueRiderName) {      
                            $scope.Illustration.Product.RiderDetails[ridersPremium].riderPremium = data.selectedRiderTbl[riderPrem].riderPremium;
                        }
                    }
                }
            }
        }
        
		if(data.PBRider != undefined && data.PBRider.isRequired == "Yes"){
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredPBRider')].sumInsured = data.PBRider.sumAssured;
		}
		
        // Need to change the below after rule change.
		if($scope.Illustration.Product.ProductDetails.productId == '1010' && $scope.Illustration.Product.ProductDetails.productCode == '10'){
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCIRider')].riderPremium=data.CIPremium;
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADDRider')].riderPremium=data.ADDPremium;
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHSRider')].riderPremium=data.HSPremium;
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredHBRider')].riderPremium=data.HBPremium;
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredOPDRiders')].riderPremium=data.OPDPremium;
        }
        
        if($scope.Illustration.Product.ProductDetails.productId == '1097' && $scope.Illustration.Product.ProductDetails.productCode == '97'){
			$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredCNCRider')].riderPremium=data.CNCPremium;
            $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredWPICRider')].riderPremium=data.WPICPremium;
	}
        /*if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4'){
            if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].isRequired=='Yes'){
                $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADDRiders')].riderPremium=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].riderPremium;                
            }
            
            if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].isRequired==='Yes'){
                $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredAIRider')].riderPremium=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].riderPremium;
            }
            
            if($scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].isRequired=='Yes'){
                $scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredADBRider')].riderPremium=$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].riderPremium;
            }
            
        }*/
		// Need to change the above after rule change.
		
        IllustratorVariables.isValidationSuccess = true;

        $rootScope.premiumSummaryOutput = true;
        var validatedDate = new Date();
        $scope.Illustration.Product.premiumSummary.validatedDate = validatedDate;

        $scope.saveIllustrationFromProductInfo(true, function () {
            $rootScope.showHideLoadingImage(false);
            //$scope.paintBenefitSummary();
            $scope.refresh();
        }, function () {
            $rootScope.showHideLoadingImage(false);
            $rootScope.NotifyMessages(false, "saveFailure", $translate);
            $scope.refresh();
        });
    }
    
    $scope.paintBenefitSummary= function(){
     	LEDynamicUI.getUIJson(rootConfig.benefitSummaryJson, false, function(benefitSummaryUIJson) {
     		var productServiceResponse = JSON.parse(JSON.stringify(IllustratorVariables.productDetails));
     		var customizedBenefitSummaryJson = benefitSummaryUIJson;
     		var viewToBeCustomizedBeforeBS = $scope.viewToBeCustomized;
     		$scope.viewToBeCustomized = "BenefitSummaryContent";
     		$scope.customizeBenefitSummaryJson(productServiceResponse,benefitSummaryUIJson,function(customizedBenefitSummaryJson){
     			$scope.viewToBeCustomized = viewToBeCustomizedBeforeBS;
         		LEDynamicUI.paintUIFromJsonObject(rootConfig.template, customizedBenefitSummaryJson, "BenefitSummaryContent", "#BenefitSummarySection", true, function() {
         		$scope.showHeaderImages = false;
         		$rootScope.showHideLoadingImage(false);
         		$scope.refresh();
         		}, $scope, $compile);
     		});
     	});
     }
   	
    /*
     * Illustration success call. Map data to scope variables.
     */
    $scope.onGetIllustrationDataSuccess = function (data) {
        //Setting the status to Completed on getting the illustration data
//        if (IllustratorVariables.illustrationStatus != "Confirmed") {
            IllustratorVariables.illustrationStatus = "Draft";
//        }
        $scope.isValidationRuleExecuted = false;
        $scope.Illustration.IllustrationOutput = data;	
		// Formatting graph values
		$scope.Illustration.IllustrationOutput.sumAssuredField = $filter('formattedNumber')(data.sumAssuredField);
		$scope.Illustration.IllustrationOutput.premiumModeField = $filter('formattedNumber')(data.premiumModeField);
		$scope.Illustration.IllustrationOutput.riderModeField = $filter('formattedNumber')(data.riderModeField);
		$scope.Illustration.IllustrationOutput.deathBenefitMinField = $filter('formattedNumber')(data.deathBenefitMinField);
		$scope.Illustration.IllustrationOutput.deathBenefitMaxField = $filter('formattedNumber')(data.deathBenefitMaxField);
		$scope.Illustration.IllustrationOutput.insuredAge = $filter('formattedNumber')(data.insuredAge);
		$scope.Illustration.IllustrationOutput.insuredAgeAfterEightYears = $filter('formattedNumber')(data.insuredAgeAfterEightYears);
		$scope.Illustration.IllustrationOutput.fifteenPerSA = $filter('formattedNumber')(data.fifteenPerSA);
		$scope.Illustration.IllustrationOutput.twentyPerSA = $filter('formattedNumber')(data.twentyPerSA);
		$scope.Illustration.IllustrationOutput.twentyFivePerSA = $filter('formattedNumber')(data.twentyFivePerSA);
		$scope.Illustration.IllustrationOutput.totalBenefitAmountField = $filter('formattedNumber')(data.totalBenefitAmountField);
		$scope.Illustration.IllustrationOutput.packageNameField = $filter('formattedNumber')(data.packageNameField);
		$scope.Illustration.IllustrationOutput.CISumAssuredCover = $filter('formattedNumber')(data.CISumAssuredCover);
		$scope.Illustration.IllustrationOutput.HBSumAssuredCover = $filter('formattedNumber')(data.HBSumAssuredCover);
		$scope.Illustration.IllustrationOutput.HSSumAssuredCover = $filter('formattedNumber')(data.HSSumAssuredCover);
		$scope.Illustration.IllustrationOutput.ADDSumAssuredCover = $filter('formattedNumber')(data.ADDSumAssuredCover);
        $scope.Illustration.IllustrationOutput.CNCSumAssuredCover = $filter('formattedNumber')(data.CNCSumAssuredCover);
		$scope.Illustration.IllustrationOutput.WPICSumAssuredCover = $filter('formattedNumber')(data.WPICSumAssuredCover);
		$scope.Illustration.IllustrationOutput.insuredAgeField = $filter('formattedNumber')(data.insuredAgeField);
		$scope.Illustration.IllustrationOutput.basePremiumFinalGraph = $filter('formattedNumber')(data.basePremiumFinal);
		$scope.Illustration.IllustrationOutput.riderPremiumFinal = $filter('formattedNumber')(data.riderPremiumFinal);
		$scope.Illustration.IllustrationOutput.sumAssured150PerField = $filter('formattedNumber')(data.sumAssured150PerField);
		$scope.Illustration.IllustrationOutput.sumAssured130PerField = $filter('formattedNumber')(data.sumAssured130PerField);
		$scope.Illustration.IllustrationOutput.sumAssured30PerField = $filter('formattedNumber')(data.sumAssured30PerField);
		$scope.Illustration.IllustrationOutput.sumAssured4PerField = $filter('formattedNumber')(data.sumAssured4PerField);
		$scope.Illustration.IllustrationOutput.sumAssured8PerField = $filter('formattedNumber')(data.sumAssured8PerField);
		$scope.Illustration.IllustrationOutput.sumAssured13PerField = $filter('formattedNumber')(data.sumAssured13PerField);
		$scope.Illustration.IllustrationOutput.sumAssured16PerField = $filter('formattedNumber')(data.sumAssured16PerField);
		
		$scope.Illustration.IllustrationOutput.insuredAge0Field = $filter('formattedNumber')(data.insuredAge0Field);
		$scope.Illustration.IllustrationOutput.insuredAge13Field = $filter('formattedNumber')(data.insuredAge13Field);
		$scope.Illustration.IllustrationOutput.insuredAge14Field = $filter('formattedNumber')(data.insuredAge14Field);
		$scope.Illustration.IllustrationOutput.insuredAge15Field = $filter('formattedNumber')(data.insuredAge15Field);
		$scope.Illustration.IllustrationOutput.insuredAge16Field = $filter('formattedNumber')(data.insuredAge16Field);
		$scope.Illustration.IllustrationOutput.insuredAge17Field = $filter('formattedNumber')(data.insuredAge17Field);
		$scope.Illustration.IllustrationOutput.insuredAge20Field = $filter('formattedNumber')(data.insuredAge20Field);
		$scope.Illustration.IllustrationOutput.sumAssured110PerField = $filter('formattedNumber')(data.sumAssured110PerField);
		$scope.Illustration.IllustrationOutput.sumAssured120PerField = $filter('formattedNumber')(data.sumAssured120PerField);
		$scope.Illustration.IllustrationOutput.sumAssured130PerField = $filter('formattedNumber')(data.sumAssured130PerField);
		$scope.Illustration.IllustrationOutput.sumAssured140PerField = $filter('formattedNumber')(data.sumAssured140PerField);
		$scope.Illustration.IllustrationOutput.sumAssured150PerField = $filter('formattedNumber')(data.sumAssured150PerField);
		
		switch ($scope.Illustration.Product.policyDetails.premiumFrequency){
			case "Annual" : 
				$scope.Illustration.IllustrationOutput.PremiumFrequency = "(" + translateMessages($translate, "Annual_Graph") + ")";
				break;
			case "Semi Annual" : 
				$scope.Illustration.IllustrationOutput.PremiumFrequency = "(" + translateMessages($translate, "Semi Annual_Graph") + ")";
				break;					
			case "Quarterly" : 
				$scope.Illustration.IllustrationOutput.PremiumFrequency = "(" + translateMessages($translate, "Quarterly_Graph") + ")";
				break;
			case "Monthly" : 
				$scope.Illustration.IllustrationOutput.PremiumFrequency = "(" + translateMessages($translate, "Monthly_Graph") + ")";
				break;
			default: 
				$scope.Illustration.IllustrationOutput.PremiumFrequency = "(" + $scope.Illustration.Product.policyDetails.premiumFrequency + ")";
				break;
		}
		
        $scope.Illustration.IllustrationOutput.isValidIllustration = IllustratorVariables.isInValidIllustration;
        if ($scope.Illustration.IllustrationOutput.selectedRiderTbl && $scope.Illustration.IllustrationOutput.selectedRiderTbl.length > 0) {
            for (totRidersIndex in $scope.Illustration.Product.RiderDetails) {
                for (initialPremiumIndex in $scope.Illustration.IllustrationOutput.selectedRiderTbl) {
                    if ($scope.Illustration.Product.RiderDetails[totRidersIndex].uniqueRiderName == $scope.Illustration.IllustrationOutput.selectedRiderTbl[initialPremiumIndex].uniqueRiderName) {
                        $scope.Illustration.Product.RiderDetails[totRidersIndex].initialPremium = $scope.Illustration.IllustrationOutput.selectedRiderTbl[initialPremiumIndex].yearlyPremium;
                    }
                }
            }
        }
        if($scope.Illustration.Product.ProductDetails.productId == '1010' && $scope.Illustration.Product.ProductDetails.productCode == '10'){
	        if(data.Package){
	        	$scope.Illustration.IllustrationOutput.packageTranslateName = translateMessages($translate, "illustrator."+data.Package);
	        }
        }
        if($scope.Illustration.Product.ProductDetails.productId == '1097' && $scope.Illustration.Product.ProductDetails.productCode == '97'){
	        if(data.Package){
	        	$scope.Illustration.IllustrationOutput.packageTranslateName = translateMessages($translate, "illustrator."+data.Package);
	        }
        }

        /*
         * Mapping bpmcode from illustration output.
         */
        if ($scope.Illustration.Product.RiderDetails && $scope.Illustration.Product.RiderDetails.length > 0) {
	        for (var rIndx in $scope.Illustration.Product.RiderDetails) {
	            if ($scope.Illustration.Product.RiderDetails[rIndx].planName === rootConfig.VGHRidePlanName && $scope.Illustration.IllustrationOutput.VGHRider.LA1.isRequired === "Yes") {
	                $scope.Illustration.Product.RiderDetails[rIndx].shortName = $scope.Illustration.IllustrationOutput.VGHRider.LA1.chosenRiderPlanCd;
	            }
	        }
        }
        
        if($scope.Illustration.IllustrationOutput.benefitTableData){			
			var benefitTableDataLength=$scope.Illustration.IllustrationOutput.benefitTableData.length;			
            if(benefitTableDataLength>1){
                $scope.Illustration.IllustrationOutput.benefitTableData[benefitTableDataLength-1].policyYear=translateMessages($translate, "Included");
            }
		}
        
       if($scope.Illustration.Product.RiderDetails){ 
          if($scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredWPRider',$scope.Illustration.Product.RiderDetails)]]){
              if($scope.Illustration.Product.RiderDetails[[$scope.getRiderIndex('MaininsuredWPRider',$scope.Illustration.Product.RiderDetails)]].isRequired==='Yes'){
            if($scope.Illustration.IllustrationOutput.summaryTableData){			
			     var summaryTableDataLength=$scope.Illustration.IllustrationOutput.summaryTableData.length;
			    for(i=0;i<summaryTableDataLength;i++) {
                    if($scope.Illustration.IllustrationOutput.summaryTableData[i].col1==='WP Illustration page'){
                        if(parseInt($scope.Illustration.IllustrationOutput.summaryTableData[i].col5)==0){
				            $scope.Illustration.IllustrationOutput.summaryTableData[i].col5=translateMessages($translate, "WPTable");
                        }
                    }
		        }
            }
        }}}
        
        var userDetails = UserDetailsService.getUserDetailsModel();
        $scope.Illustration.IllustrationOutput.agentName = userDetails.agentName;
        $scope.Illustration.IllustrationOutput.agentCode = userDetails.agentCode;
        var date = new Date();
        $scope.currentIllustrationDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
        $scope.Illustration.IllustrationOutput.illustratedDate = getFormattedDateDDMMYYYY($scope.currentIllustrationDate);
        $scope.Illustration.IllustrationOutput.productCode = IllustratorVariables.productDetails.code;

        IllustratorVariables.setIllustratorModel($scope.Illustration);
        if ((rootConfig.isDeviceMobile)) {
            $scope.saveIllustrationFromProductInfo(true, function () {
                $rootScope.showHideLoadingImage(false);
                $scope.refresh();
            }, function () {
                $rootScope.showHideLoadingImage(false);
                $rootScope.NotifyMessages(false, "saveFailure", $translate);
            });
        } else {
            $rootScope.showHideLoadingImage(false);
            $scope.refresh();
        }
        $scope.nav = Navigation($scope, "ProductDetails", "forward");
        $rootScope.pageNavigationIllus('Illustration Details');

    }
	
    $scope.onGetIllustrationError = function (data) {
        $scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
        $scope.Illustration.Product.premiumSummary.summaryMaxSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].maxSA;
        
        if($scope.Illustration.Product.ProductDetails.productId == '1004' && $scope.Illustration.Product.ProductDetails.productCode == '4'){
			if ($scope.Illustration.Product.policyDetails.premiumPeriod == '25') {
				$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA25;
			}
			else {
				if($scope.Illustration.Product.policyDetails.premiumFrequency=="" || $scope.Illustration.Product.policyDetails.premiumFrequency==undefined){
					$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
				}else if($scope.Illustration.Product.policyDetails.premiumFrequency!=="Annual"){
					$scope.Illustration.Product.premiumSummary.summaryMinSA =IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSANotAnnual;
				}else{
					$scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
				}
			}
        }
        
        if($scope.Illustration.Product.ProductDetails.productId == '1220' && $scope.Illustration.Product.ProductDetails.productCode == '220'){
            if($scope.Illustration.Product.policyDetails.premiumFrequency=="" || $scope.Illustration.Product.policyDetails.premiumFrequency==undefined){
                $scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
            }else if($scope.Illustration.Product.policyDetails.premiumFrequency!=="Annual"){
                $scope.Illustration.Product.premiumSummary.summaryMinSA =IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSANotAnnual;
            }else{
                $scope.Illustration.Product.premiumSummary.summaryMinSA = IllustratorVariables.productSummaryValueList[$scope.Illustration.Product.ProductDetails.productId].minSA;
            }
        }
        
        if(data.FlagADBRCC == false){
        	$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].isRequired = "No";
        	$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADBRider')].sumInsured = "";
        	$rootScope.declinedRCCADB = true;
        }
        if(data.FlagADDRCC == false){
        	$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].isRequired = "No";
        	$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCADDRider')].sumInsured = "";
        	$rootScope.declinedRCCADD = true;
        }
        if(data.FlagAIRCC == false){
        	$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].isRequired = "No";
        	$scope.Illustration.Product.RiderDetails[$scope.getRiderIndex('MaininsuredRCCAIRider')].sumInsured = "";
        	$rootScope.declinedRCCAI = true;
        }
		if(data.PolicyDetails.sumAssured == "NaN"){
			$scope.Illustration.Product.policyDetails.sumInsured = "";
		}
        if(data.PolicyDetails.premium == "NaN"){
			$scope.Illustration.Product.policyDetails.premium = "";
		}
		if(data.BaseOccCode!==undefined && data.BaseOccCode!==""){
			$scope.Illustration.Insured.BasicDetails.occupationClassCode = data.BaseOccCode[0];
		}        
		
        $scope.okBtnAction = function () {
            $scope.validationResults = "";
            $scope.onGetValidationDataSuccess(data);
        };        
        $scope.isValidationRuleExecuted = false;
        IllustratorVariables.isValidationSuccess = false;
        if (typeof data.ValidationResults != "undefined" ||
             data.ValidationResults != null) {
            $scope.isValidationRuleExecuted = true;
            $scope.validationMessage = translateMessages($translate, "illustrator.validationFailed");
            $scope.validationResults = data.ValidationResults;
            $scope.errorKeyArray = [];
            
			
            for (var key in $scope.validationResults) {
                var errorKeyValue = [];
                var errorKeyMsg = Object.keys($scope.validationResults);
                var errorKeyValue = $scope.validationResults[key].split('#');
                var mapMessage="";                
               
                if(errorKeyValue){ 
                    if(errorKeyValue.length>1){ 
                        var output="";
						output = translateMessages($translate,"illustrator." +key);                        
                        for(var i=1;i<errorKeyValue.length;i++){                            
                            errorKeyValue[i] = $filter('formattedNumber')(errorKeyValue[i]);                	        
                            output = output.replace("@@" + (i), errorKeyValue[i]);
                        }
                        if(output.indexOf("@@5")){
                            output = output.replace("@@5", $scope.Illustration.Product.policyDetails.premiumPeriod);
                        }
                    $scope.validationResults[key] =output;    
                    }else {                    
                    	$scope.validationResults[key] = translateMessages($translate, "illustrator."+(key));
                	}
                }else {                    
                    $scope.validationResults[key] = translateMessages($translate, "illustrator."+(key));
                }                               
               
            }
        }
        
        if($scope.Illustration.Product.RiderDetails!==undefined){
                for(var i=0;i<$scope.Illustration.Product.RiderDetails.length;i++){
                    $scope.Illustration.Product.RiderDetails[i].riderPremium="";
                }
            }
        
        //$scope.errCount = Object.keys($scope.errorKeyArray).length;
        $scope.errCount = Object.keys($scope.validationResults).length;
        //$rootScope.showHideLoadingImage(false);
        //$scope.saveIllustrationFromProductInfo(true);	
        $scope.saveIllustrationFromProductInfo(true, function () {
            $rootScope.showHideLoadingImage(false);
            $scope.refresh();
        }, function () {
            $rootScope.showHideLoadingImage(false);
            $rootScope.NotifyMessages(false, "saveFailure", $translate);
        });
        $scope.refresh();
    }

    $scope.cancelBtnAction = function () {

    }
    
    $scope.chooseAnotherProduct=function(){     
        $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
									   translateMessages($translate, "pageNavigationText"),  
									   translateMessages($translate, "fna.navok"), $scope.okPressed, 
									   translateMessages($translate, "general.navproceed"), 
									   $scope.proceedToProductBasket);        
    }
    
    $scope.proceedToProductBasket=function(){
		IllustratorVariables.transactionId = $rootScope.transactionId;
        IllustratorVariables.fromChooseAnotherProduct = true;
        $location.path("/products/0/0/0/0/false");  
    }
    
    $scope.exitIllustrationFromProductInfo = function () {
        $scope.lePopupCtrl.showWarning(translateMessages($translate, "lifeEngage"),
									   translateMessages($translate, "pageNavigationText"),  
									   translateMessages($translate, "fna.navok"), 
									   $scope.okPressed,
									   translateMessages($translate, "general.navproceed"), 
									   $scope.backToListing);
    }
    
    $scope.backToListing=function(){
        IllustratorVariables.setIllustratorModel($scope.Illustration);
        var leadId = 0;
        leadId = IllustratorVariables.leadId;
        if (UtilityService.leadPage == 'LeadSpecificListing' || (leadId != '' && leadId != undefined)) {
            $location.path("/MyAccount/Illustration/" + leadId + '/0');
        } else if (UtilityService.leadPage == 'GenericListing') {
            $location.path("/MyAccount/Illustration/0/0");
        }else{
            $location.path("/MyAccount/Illustration/0/0");
        }
        $scope.refresh();
    }
    //To Enable/Disable tabs on editing a transaction after the status is changed to Completed
    $scope.onFieldChange = function () {		
        GLI_IllustratorService.onFieldChange($scope, IllustratorVariables, $rootScope);
        $rootScope.updateErrorCount($scope.viewToBeCustomized);
    }
    
    /*
     * Update error count on blur.
     */
    $scope.onBlur = function () {        
        $scope.updateErrorDynamically();
        $rootScope.updateErrorCount($scope.viewToBeCustomized);        
    }
    
    $scope.updateErrorDynamically = function () {
        var date = new Date();
        $scope.dynamicErrorCount = 0;
        $scope.dynamicErrorMessages = [];
        var currDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
        
        var riderErrorFlag=false;
        if($scope.Illustration.Product.RiderDetails.length>0){
            for(var i=0;i<$scope.Illustration.Product.RiderDetails.length;i++){
                if($scope.Illustration.Product.RiderDetails[i].planName != "PB" && $scope.Illustration.Product.RiderDetails[i].isRequired=='Yes'){
                    if($scope.Illustration.Product.RiderDetails[i].sumInsured===undefined || $scope.Illustration.Product.RiderDetails[i].sumInsured===""){
                        riderErrorFlag=true;
                    }
                }
            }
        }
        
        if(riderErrorFlag && (parseInt($scope.Illustration.Product.ProductDetails.productCode)===4 || parseInt($scope.Illustration.Product.ProductDetails.productCode)===6 || parseInt($scope.Illustration.Product.ProductDetails.productCode)===220 || parseInt($scope.Illustration.Product.ProductDetails.productCode)===306 || parseInt($scope.Illustration.Product.ProductDetails.productCode)===10)){
                    var error = {};
                    error.message = translateMessages($translate, "illustrator.riderValidationErrorMessage");
                    error.key = "MaininsuredWPRiderSA";
                    $scope.dynamicErrorCount = $scope.dynamicErrorCount + 1;
                    $scope.dynamicErrorMessages.push(error);
        }
                
        for (index in $scope.dynamicErrorMessages) {
            for (var newindex = Number(index) + 1; newindex < $scope.dynamicErrorMessages.length; newindex++) {
                if ($scope.dynamicErrorMessages[index].key == $scope.dynamicErrorMessages[newindex].key) {
                    $scope.updateDynmaicErrMessageByIndex(newindex);
                    newindex = Number(index);
                }
            }
        }
        if ($scope.viewToBeCustomized != undefined) {
            $rootScope.updateErrorCount($scope.viewToBeCustomized, true);
        }
        $scope.refresh();
    }

    var error = {};
    $scope.dynamicErrorMessages = [];
    $scope.dynamicErrorCount = 0;

    $scope.updateDynmaicErrMessage = function (erroKey) {
        for (var i = 0; i < $scope.dynamicErrorMessages.length; i++) {
            if ($scope.dynamicErrorMessages[i].key == erroKey) {
                $scope.dynamicErrorMessages.splice(i, 1);
                $scope.dynamicErrorCount = $scope.dynamicErrorCount - 1;
            }
        }
        if ($scope.viewToBeCustomized)
            $rootScope.updateErrorCount($scope.viewToBeCustomized, true);
    }

    $scope.updateFlag = function () {
        $scope.editFlowPackage = true;
    }

    $scope.saveIllustrationFromProductInfo = function (hideNotifyMsgOnVal_Ill, saveSuccess, saveError) {
        $rootScope.showHideLoadingImage(true, 'saveTransactionMessage', $translate);
        if ($scope.errorCount == 0) {
            /* Defect -3760 - starts*/
            globalService.setCPInsured($scope.Illustration.Insured);
            globalService.setCPPayer($scope.Illustration.Payer);
            /* Defect -3760 - ends*/
            IllustratorVariables.editFlowPackage = false;
            $scope.editFlowPackage = IllustratorVariables.editFlowPackage;
            IllustratorVariables.fromChooseAnotherProduct = false;
            IllustratorVariables.setIllustratorModel($scope.Illustration);
            var obj = new IllustratorService.saveTransactions(
                $scope, $rootScope, DataService,
                $translate, UtilityService,
                IllustratorVariables, $routeParams,
                false);
            obj.save(function (isAutoSave) {
                $rootScope.showHideLoadingImage(false);
                $scope.refresh();
                if ((rootConfig.isDeviceMobile)) {
                    if (!isAutoSave) {
                        //this condition is not to show save notification  when we press validate and illustrate button
                        if (!hideNotifyMsgOnVal_Ill) {
                            $rootScope.NotifyMessages(false, "saveIllustrationSuccessOffline", $translate);
                        }
                    }
                } else {
                    if (!isAutoSave) {
                        if ($scope.isRDSUser) {
                            $rootScope.NotifyMessages(false, "saveIllustrationSuccessOffline", $translate);1
                        } else if(!hideNotifyMsgOnVal_Ill){
                            $rootScope.NotifyMessages(false, "saveIllustrationSuccessOffline", $translate);
                        }
                    }
                }
                if ((saveSuccess && typeof saveSuccess == "function")) {
                    $rootScope.showHideLoadingImage(true, 'paintUIMessage', $translate);
                    saveSuccess();
                }
            });
        } else {
            $rootScope.showHideLoadingImage(false);
            $scope.leadDetShowPopUpMsg = true;
            $scope.enterMandatory = translateMessages($translate, "enterMandatoryIllustration");
            //  $scope.lePopupCtrl.showWarning(
            //                translateMessages($translate, "lifeEngage"),
            //                translateMessages($translate, "enterMandatoryIllustration"),
            //                translateMessages($translate, "fna.ok"));
        }
    }

    $scope.valuateString = function (value) {
        var val;
        var variableValue;
        if (value.indexOf('.') != -1) {
            val = value.split('.');
            for (var i in val) {
                if (i == 0) {
                    variableValue = val[0];
                } else {
                    variableValue = variableValue[val[i]];
                }
            }
            return variableValue;
        } else if (!(value instanceof Array)) {
            return 0;
        }
    }

    $scope.evaluateString = function (value) {
        var val;
        var variableValue = $scope;
        val = value.split('.');
        if (value.indexOf('$scope') != -1) {
            val = val.shift();
        }
        for (var i in val) {
            variableValue = variableValue[val[i]];
        }
        return variableValue;
    };

    //setting default values from rule.This will be called in call change method function
    $scope.getValuenew = function (sringnew, result) {
        var _lastIndex = sringnew.lastIndexOf('.');
        if (_lastIndex > 0) {
            var parentModel = sringnew.substring(0, _lastIndex);
            //var scopeVar=eval ('$scope.' + (parentModel));
            var scopeVar = $scope.evaluateString(parentModel);
            var remain_parentModel = sringnew.substring(_lastIndex + 1, sringnew.length);
            if (remain_parentModel.indexOf('Coverage') > -1) {
                scopeVar[remain_parentModel] = result.maxCoverageAge;
            } else if (remain_parentModel.indexOf('sum') > -1) {
                scopeVar[remain_parentModel] = result.sumInsured;
            }
        }
    }

    $scope.ShowHideDialogue = function (riderName) {
        //If DIV is visible it will be hidden and vice versa.
        $scope.IsVisible = $scope.IsVisible ? false : true;
        //$scope.riderNameSelected = riderName[0][0].replace(/ /g, '');
        var riderArray = {
            'translatedTooltip': ''
        };
        riderArray.translatedTooltip = translateMessages($translate, "illustrator." + riderName);
        $scope.toolTip = riderArray.translatedTooltip;
    };
    
    $scope.closePopup = function () {
            $scope.leadDetShowPopUpMsg = false;
        };
    
    //validation messages ends
    $scope.$on("$destroy", function () {
        if (this != null && typeof this !== 'undefined') {
            if (plot) {
                plot.destroy();
            }
            if (plot2) {
                plot2.destroy();
            }
            if (plot3) {
                plot3.destroy();
            }
            if (plot4) {
                plot4.destroy();
            }
            if (plot5) {
                plot5.destroy();
            }
            if (plot6) {
                plot6.destroy();
            }
            if (plot7) {
                plot7.destroy();
            }
            if (this.$$destroyed) return;
            while (this.$$childHead) {
                this.$$childHead.$destroy();
            }
            if (this.$broadcast) {
                this.$broadcast('$destroy');
            }
            this.$$destroyed = true;
            $timeout.cancel(timer);
        }
    });

} // Controller ends here