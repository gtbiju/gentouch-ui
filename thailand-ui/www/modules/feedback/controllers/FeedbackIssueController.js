storeApp
		.controller(
				'FeedbackIssueController',
				[
						'$rootScope',
						'$scope',
						'$compile',
						'$routeParams',
						'DataService',
						'$translate',
						'$location',
						'$timeout',
						'UtilityService',
						'PersistenceMapping',
						'globalService',
						'UserDetailsService',
						'EmailService',
						'TemplateService',
						'PlatformInfoService',
						'DocumentService',
						'AgentService', 

						function FeedbackIssueController($rootScope, $scope,
								$compile, $routeParams, DataService,
								$translate, $location, $timeout,
								UtilityService, PersistenceMapping,
								globalService, UserDetailsService, EmailService, TemplateService,
								PlatformInfoService, DocumentService, AgentService) {

							

							$rootScope.showHideLoadingImage(true,"Loading Please Wait..");
							$rootScope.moduleVariable = translateMessages($translate, "feedback.header");
							$scope.feedbackIssueModel = FeedbackIssueModelObject();
							$scope.feedbackIssueModel.emailFooter = translateMessages(
									$translate, "feedback.emailFooter")
									+ " "
									+ rootConfig.clientID
									+ "."
									+ rootConfig.copyRights
									+ " "
									+ rootConfig.clientID;
							$scope.mobilenumberPattern = rootConfig.tendigitmobilenumberPattern;
							UtilityService.InitializeErrorDisplay($scope,
									$rootScope, $translate);

							$scope.type = "Feedback";
							$scope.$parent.currentModule = "Feedback";
							$scope.$parent.slider = [ false, false, false, false, false, false, false, true ];
							$scope.toggleFlag = false;
							$scope.addPhotoImage = [];
							$scope.showErrorCount = true;
							$scope.thumbnailEmailAttachments = [];
							$scope.emailAttachments = [];
							$scope.attachPlatformInfoChoice = true;
							$scope.photoCount = 0;
							$scope.isDeviceMobile = (rootConfig.isDeviceMobile);
							$scope.isOfflineDesktop = rootConfig.isOfflineDesktop;
							$scope.showAttachDeviceInfo = !($scope.isOfflineDesktop);
							$scope.attachTranslateName =  ($scope.isDeviceMobile == true ? translateMessages($translate,"feedback.feedbackattachdeviceinfo")
											: translateMessages($translate,"feedback.feedbackattachBrowserinfo"));	
							
							/**
							 * refresh : This method will call $scope.$apply(), referred by FeedbackIssueControler & LEDynamicUI
							 * Note : Don't move this method to bottom of the code, this method should load before LEDynamicUI.paintUI() called
							 */				
							
							
							LEDynamicUI
									.paintUI(
											rootConfig.template,
											rootConfig.feedbackIssueJson,
											"FeedbackIssueHeaderSection",
											"#FeedbackIssueHeaderSection",
											true,
											function() {

												LEDynamicUI
														.paintUI(
																rootConfig.template,
																rootConfig.feedbackIssueJson,
																"FeedbackIssueDetailsSection",
																"#FeedbackIssueDetailsSection",
																true,
																function() {

																	
																	//$timeout( function(){
																	    $rootScope.updateErrorCount('FeedbackIssueDetailsSection');
																	//}, 50);
																	LEDynamicUI.paintUI(rootConfig.template,
																					rootConfig.feedbackIssueJson,
																					"submitSection",
																					"#submitSection",
																					true,
																					function() {

																						$rootScope.showHideLoadingImage(false);

																					},
																					$scope,
																					$compile);
																	
																	$rootScope.showHideLoadingImage(false);		
																}, $scope,
																$compile);

											}, $scope, $compile);

							$scope.submitFeedback = function() {
													
							$scope.checkConnection =  ($scope.isDeviceMobile == true ? checkConnection()
																						: true); 							

								if ($scope.checkConnection) { 

									$rootScope.showHideLoadingImage(true,"Loading Please Wait..");
									$scope.feedbackIssueModel.type = $scope.type;
									$scope.feedbackIssueModel.composeHeader = ($scope.type == 'Feedback' ? translateMessages($translate,"feedback.feedBackComposeHeader")
											: translateMessages($translate,"feedback.issueComposeHeader"));
									$scope.feedbackIssueModel.templateID = "emailTemplate";										
									$scope.getAgentDetails(function() {										
										if ( typeof $scope.agent != 'undefined') {
											$scope.feedbackIssueModel.agentCode = $scope.agent.agentCode;
											$scope.feedbackIssueModel.agentName = $scope.agent.agentName;	
										}							
																		
									TemplateService
											.createHtmlTemplate(
													rootConfig.emailTemplate,
													$scope.feedbackIssueModel,
													function(
															compiledEmailTemplate) {

														if(!($scope.isOfflineDesktop) && $scope.attachPlatformInfoChoice) {
														
															PlatformInfoService.getPlatformInfo(function(platFormInfo) {
																
																if ($scope.isDeviceMobile) {
																
																		$scope.feedbackIssueModel.deviceInfo = platFormInfo;
																} else {
																	
																	$scope.feedbackIssueModel.browserInfo = platFormInfo;
																}
																	
																$scope.createAttachPlatformDetails(compiledEmailTemplate);
																
															});

														} else {
															$scope.prepareEmailContents(compiledEmailTemplate);
														} 

														
													});
								
								});

									
								} else {								
								    $scope.feedbackIssuePopupError(translateMessages($translate,"feedback.onlineAlert"));								
								}
							};
							
							$scope.getAgentDetails = function(AgentSuccess) {

							var transactionObj = PersistenceMapping.mapScopeToPersistence({});					
							if ($scope.isDeviceMobile) {							
							    DataService.retrieveAgentDetails(transactionObj, function(data) {
									$scope.agent = data[0];
									AgentSuccess();						
								}, function(error) {console.log(error)});
							
							} else {
							
							    AgentService.retrieveAgentProfile(transactionObj, function(data) {
								$scope.agent = data.AgentDetails;
								AgentSuccess();
							    }, function(error) {console.log(error)});
							
							}

							};

							$scope.createAttachPlatformDetails = function(compiledEmailTemplate) {

								$scope.feedbackIssueModel.templateID =  ($scope.isDeviceMobile == true ? 'attachDeviceFileTemplate'
										: 'attachBrowserFileTemplate'); 
									
								TemplateService.createHtmlTemplate(
									rootConfig.emailTemplate,
									$scope.feedbackIssueModel,
									function(attachPlatformCompiledTemplate) {
									   $scope.attachPlatformInfo(attachPlatformCompiledTemplate);
									   $scope.prepareEmailContents(compiledEmailTemplate);
									});
							}

							$scope.prepareEmailContents = function(compiledEmailTemplate) {
								$scope.attachThumbnail();
								$scope.emailObject = EmailObject();
								$scope.emailObject.toMailIds = rootConfig.emailTo;
								$scope.emailObject.ccMailIds = rootConfig.emailCc;
								$scope.emailObject.mailSubject = $scope.type
										+ " : " + $scope.subject;
								$scope.emailObject.emailAttachments = $scope.emailAttachments;
								$scope.emailObject.emailBody = compiledEmailTemplate;
								$scope.sendFeedbackIssueMail($scope.emailObject);

							}

							$scope.sendFeedbackIssueMail = function(emailObject) {
								$rootScope.showHideLoadingImage(false);						
								if($scope.agent.emailId && $scope.agent.emailId!=""){
								    
								}
								EmailService.sendEmail(emailObject, function(respData) {
									$scope.emailAttachments = [];
									//if(!($scope.isDeviceMobile) || $scope.isOfflineDesktop){														
									$scope.emailPopupMsg = (respData == 'Success' ? translateMessages($translate,"feedback.emailSuccess")
												: translateMessages($translate,"feedback.emailFailure"));
									respData == 'Success' ? $scope.feedbackIssuePopupSuccess($scope.emailPopupMsg)
												: $scope.feedbackIssuePopupError($scope.emailPopupMsg);									
									//}
									
								}, function() {
								
								    //if(!($scope.isDeviceMobile) || $scope.isOfflineDesktop){	
									$scope.feedbackIssuePopupError(translateMessages($translate,"feedback.emailFailure"));
								  //  }
									
								}, UserDetailsService.getUserDetailsModel().options);

							};

							$scope.attachPlatformInfo = function(
									attachPlatformCompiledTemplate) {
									
									$scope.encodeAttachPlatformInfo = window.btoa(attachPlatformCompiledTemplate);
									
									if ($scope.isDeviceMobile) {
									
									var base64String = "base64:DeviceInfo.txt//" + $scope.encodeAttachPlatformInfo;
									$scope.emailAttachments.push(base64String);
									
									} else {
									$scope.emailAttachments.push({"fileName":"BrowserInfo.txt","base64Value":$scope.encodeAttachPlatformInfo});
									
									}
								
							}

							$scope.attachThumbnail = function() {

								var thumbnailEmailAttachmentsTemp = $scope.thumbnailEmailAttachments;

								for (i = 0; i < thumbnailEmailAttachmentsTemp.length; i++) {
									$scope.emailAttachments
											.push(thumbnailEmailAttachmentsTemp[i]);
								}

							}

							$scope.feedbackInputReset = function() {
								$scope.subject = "";
								$scope.feedbackIssueModel.description = "";
								$scope.feedbackIssueModel.contactNumber = "";
								$scope.feedbackIssueModel.moduleName = "";
								$scope.feedbackIssueModel.screenName = "";
								$scope.addPhotoImage = [];
								$scope.emailAttachments = [];
								$scope.thumbnailEmailAttachments = [];
								$scope.photoCount = 0;
							};

							$scope.attachPhoto = function() {

								if ($scope.photoCount < rootConfig.feedbackPhotoCount) {
									EmailService
											.attachPhoto(
													function(imageData,
															selectedImage,
															attchment) {

														if ((imageData.length * 3 / 4) < rootConfig.maxImageSize) {

															$scope.imageSource = imageData;
															$scope.addPhotoImage
																	.push(selectedImage);
															$scope.photoCount++;
															$scope.thumbnailEmailAttachments
																	.push(attchment);

														} else {
														
														   $scope.feedbackIssuePopupError(translateMessages($translate,"feedback.maxFileSize"));

														}

													}, null);

								} else {
								
								$scope.feedbackIssuePopupError(translateMessages($translate,"feedback.maxFileUploaded"));
									
								}

							}

							$scope.deleteThumbnail = function(thumbnailIndex) {

								var thumbnailEmailAttLength = $scope.thumbnailEmailAttachments;

								$scope.thumbnailEmailAttachments.splice(
										thumbnailIndex, 1);
								$scope.addPhotoImage.splice(thumbnailIndex, 1);
								$scope.photoCount--;
							};

							

							$scope.onChangeType = function() {

								if ($scope.type == "Issue") {
									$scope.toggleFlag = true;
									$scope.feedbackInputReset();
									$scope.errorCountTimeOut();
									
								} else {
									$scope.toggleFlag = false;
									$scope.feedbackInputReset();
									$scope.errorCountTimeOut();
								}

							}
															
				// Function called on browse buttton  click
					$scope.browseFile = function(isImage, isPhoto, fileObj, isInsured) {
						
						var ext = fileObj.name.split('.').pop().toLowerCase();				
						if ($.inArray(ext, ['jpeg', 'jpg']) == -1) {
							$scope.feedbackIssuePopupError(translateMessages($translate,"feedback.validPhotoFormat"));
							return;
						} 
						
						if (fileObj.size > 2097152) {
							$scope.feedbackIssuePopupError(translateMessages($translate,"feedback.maxFileSize"));
							return;
						}
						
						if ($scope.photoCount < rootConfig.feedbackPhotoCount) {							
						$scope.isImage = isImage;
						$scope.isPhoto = isPhoto;
						var currAgentId = $rootScope.agentId == null ? $rootScope.username : $rootScope.agentId;
						var document = $scope.mapDocScopeToPersistence(fileObj);
						DocumentService.browseFile(document, currAgentId, $scope.onFileSelectSuccess, $scope.operationError);						
						} else {						
						$scope.feedbackIssuePopupError(translateMessages($translate,"feedback.maxFileUploaded"));								
					}
					};
					
					//document Object mapping for Browse file
					$scope.mapDocScopeToPersistence = function(document) {
						var docObject = {};	
						docObject.documentName = document != null && document.documentName != null ? document.documentName : "";
						docObject.documentObject = document;
						return docObject;
					};					
						
					//SuccessCallback function for browsing an image
					$scope.onFileSelectSuccess = function(document) {	
											
					var selectedImage = {id:'table', source: document.documentPath};
					$scope.addPhotoImage.push(selectedImage);					
					var base64Value = document.documentPath.split(",");
							
					$scope.thumbnailEmailAttachments.push({"fileName":document.documentDisplayName,"base64Value":base64Value[1]});
					
					$scope.photoCount++;
					$scope.refresh();	
					
					};
					
					$scope.operationError = function(e) {
						 $scope.feedbackIssuePopupError(translateMessages($translate,"feedback.emailFailure"));
					};
					
					$scope.feedbackIssuePopupError = function(popupMSG) {
						
						$rootScope.lePopupCtrl
											.showError(
													translateMessages(
															$translate,
															"lifeEngage"),
													popupMSG,
													translateMessages(
															$translate,
															"feedback.ok"));
								

					};
					
					$scope.feedbackIssuePopupSuccess = function(popupMSG) {
						
						$rootScope.lePopupCtrl
											.showSuccess(
													translateMessages(
															$translate,
															"lifeEngage"),
													popupMSG,
													translateMessages(
															$translate,
															"feedback.ok"));
								

					};
							
							$scope.errorCountTimeOut = function() {
								$timeout(function() {
									$rootScope.updateErrorCount('FeedbackIssueDetailsSection');
								}, 100);
							}

						} ]);